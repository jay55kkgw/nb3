<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
	<script type="text/javascript">
		$(document).ready(function(){
			// 列印
			$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"f_transfer_t_print",
					"jspTitle":"<spring:message code= "LB.FX_Exchange_Transfer" />",
					"CMTXTIME":"${transfer_result_data.data.CMTXTIME }",
					"CUSTACC":"${transfer_result_data.data.CUSTACC }",
					"str_PAYREMIT":"${transfer_result_data.data.str_PAYREMIT }",
					"display_PAYCCY":"${transfer_result_data.data.display_PAYCCY }",
					"CURAMT":"${transfer_result_data.data.CURAMT }",
					"BENACC":"${transfer_result_data.data.BENACC }",
					"display_REMITCY":"${transfer_result_data.data.display_REMITCY }",
					"SELFFLAG":"${transfer_result_data.data.SELFFLAG }",
					"MEMO1":"${transfer_result_data.data.MEMO1 }",
					"CMTRMEMO":"${transfer_result_data.data.CMTRMEMO }",
					"TOTBAL":"${transfer_result_data.data.TOTBAL }",
					"AVAAMT":"${transfer_result_data.data.AVAAMT }"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});


			// 交易單據
			$("#payproof").click(function(){
// 				window.open('${__ctx}/FCY/TRANSFER/fxcertpreview?ADTXNO=${transfer_result_data.data.ADTXNO }');
				window.open('${__ctx}/FCY/COMMON/fxcertpreview?TXID=F001&ADTXNO=${transfer_result_data.data.ADTXNO }');
			});

		});

		// 下載
	 	function formReset() {
			// 沒選擇項目則不Submit
	 		if ($('#actionBar').val()==""){
				return false;
	 		}
		 	
		 	// EXCEL
    		if ($('#actionBar').val()=="excel"){
        		// 顯示轉出金額
	    		if('${transfer_result_data.data.str_PAYREMIT}' == '1'){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/f_transfer_t_o_result.xls");
	    		}// 顯示轉入金額
				else if('${transfer_result_data.data.str_PAYREMIT}' == '2'){
	    			$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/f_transfer_t_i_result.xls");
			    }
			    
	 		}// .TXT
	 		else if ($('#actionBar').val()=="txt"){
	 			// 顯示轉出金額
	    		if('${transfer_result_data.data.str_PAYREMIT}' == '1'){
	    			$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/f_transfer_t_o_result.txt");
	    		}// 顯示轉入金額
				else if('${transfer_result_data.data.str_PAYREMIT}' == '2'){
					$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/f_transfer_t_i_result.txt");
			    }
				
	 		}
    		$("#formId").attr("target", "");
			$("#formId").submit();
			$('#actionBar').val("");
		}
		
	</script>
</head>
<body>
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 	<!--   IDGATE --> 		 
	<%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 買賣外幣/約定轉帳     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Exchange_Transfer" /></li>
		</ol>
	</nav>



	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">

		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
			
				<!-- 功能名稱 -->
	            <h2><spring:message code="LB.FX_Exchange_Transfer" /></h2>
	            <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
	            
				<!-- 交易流程階段 -->
<!-- 	            <div id="step-bar"> -->
<!-- 	                <ul> -->
<%-- 	                    <li class="finished"><spring:message code="LB.Enter_data" /></li> --%>
<%-- 	                    <li class="finished"><spring:message code="LB.Confirm_data" /></li> --%>
<%-- 	                    <li class="active"><spring:message code="LB.Transaction_complete" /></li> --%>
<!-- 	                </ul> -->
<!-- 	            </div> -->

				<!-- 下載 -->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<!-- 下載 -->
						<option value=""><spring:message code="LB.Downloads" /></option>
						<!-- 下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
						<!-- 下載txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				
				<form id="formId" action="${__ctx}/download" method="post">
					<!-- 下載用 -->
                    <input type="hidden" name="downloadFileName" value="<spring:message code="LB.FX_Exchange_Transfer" />" />
                    <input type="hidden" name="CMTXTIME" value="${transfer_result_data.data.CMTXTIME }" />
					<input type="hidden" name="CUSTACC" value="${transfer_result_data.data.CUSTACC }" />
					<input type="hidden" name="str_PAYREMIT" value="${transfer_result_data.data.str_PAYREMIT }" />
					<input type="hidden" name="display_PAYCCY" value="${transfer_result_data.data.display_PAYCCY }" />
					<input type="hidden" name="CURAMT" value="${transfer_result_data.data.CURAMT }" />
					<input type="hidden" name="BENACC" value="${transfer_result_data.data.BENACC }" />
					<input type="hidden" name="display_REMITCY" value="${transfer_result_data.data.display_REMITCY }" />
					<input type="hidden" name="SELFFLAG" value="${transfer_result_data.data.SELFFLAG }" />
					<input type="hidden" name="MEMO1" value="${transfer_result_data.data.MEMO1 }" />
					<input type="hidden" name="CMTRMEMO" value="${transfer_result_data.data.CMTRMEMO }" />
					<input type="hidden" name="TOTBAL" value="${transfer_result_data.data.TOTBAL }" />
					<input type="hidden" name="AVAAMT" value="${transfer_result_data.data.AVAAMT }" />
                    <input type="hidden" name="downloadType" id="downloadType" />
                    <input type="hidden" name="templatePath" id="templatePath" />
                    <!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="2"/><!-- headerRightEnd 資料列以前的右方界線 -->
					<input type="hidden" name="headerBottomEnd" value="11"/><!-- headerBottomEnd 資料列到第幾列 從0開始 -->
                    <!-- TXT下載用-總行數 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="12"/> 					
					<input type="hidden" name="txtHasRowData" value="false"/>
					<input type="hidden" name="txtHasFooter" value="false"/>
					
					<!-- 功能內容 -->
		            <div class="main-content-block row">
		                <div class="col-12">
		                
		                <!-- 交易成功 -->
						<c:if test="${transfer_result_data.result == true}">
		                    <h4 style="margin-top:10px;font-weight:bold;color:red">
								<spring:message code="LB.Transfer_successful" /><!-- 轉帳成功 -->
							</h4>
							<div class="ttb-input-block">
								<!-- 交易時間 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Trading_time" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.CMTXTIME }
									</span>
								</div>
								
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Payers_account_no" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.CUSTACC }
									</span>
								</div>
							
							<c:if test="${transfer_result_data.data.str_PAYREMIT eq '1' }">
							
								<!-- 轉出金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Deducted" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.display_PAYCCY }&nbsp;
		                            	${transfer_result_data.data.CURAMT }&nbsp;
		                            	<spring:message code="LB.Dollar" />
									</span>
								</div>
								
							</c:if>
							
								<!-- 轉入帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Payees_account_no" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.BENACC }
									</span>
								</div>
								
							<c:if test="${transfer_result_data.data.str_PAYREMIT eq '2' }">
							
								<!-- 轉入金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Buy" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.display_REMITCY }&nbsp;
		                            	${transfer_result_data.data.CURAMT }&nbsp;
		                            	<spring:message code="LB.Dollar" />
									</span>
								</div>
								
							</c:if>
								
							<c:if test="${transfer_result_data.data.SELFFLAG eq 'Y' }">
		                    
		                        <!-- 收款人附言 -->
		                        <div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.W0280" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.MEMO1 }
									</span>
								</div>
		                        
		                    </c:if>	
								
								<!-- 交易備註 -->
		                        <div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Transfer_note" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.CMTRMEMO }
									</span>
								</div>
								
								<!-- 轉出帳號帳上餘額 -->
		                        <div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Payers_account_balance" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.display_PAYCCY }&nbsp;
		                            	${transfer_result_data.data.TOTBAL }&nbsp;
		                            	<spring:message code="LB.Dollar" />
									</span>
								</div>
								
								<!-- 轉出帳號可用餘額 -->
		                        <div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Payers_available_balance" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.display_PAYCCY }&nbsp;
		                            	${transfer_result_data.data.AVAAMT }&nbsp;
		                            	<spring:message code="LB.Dollar" />
									</span>
								</div>
								
		                    </div>
		                    
							<!-- 列印 -->
							<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
		                    <!-- 交易單據 -->
		                    <input type="button" class="ttb-button btn-flat-orange" id="payproof" value="<spring:message code="LB.Transaction_document" />" />
		                    
		                    <!-- 下載選項移到選單 -->
<!-- 		                    <input type="button" class="ttb-button btn-flat-orange" id="CMEXCEL" value="下載Excel檔"/> -->
<!-- 		                    <input type="button" class="ttb-button btn-flat-orange" id="CMTXT" value="下載.txt檔"/> -->
		                    
						</c:if>
						
		                </div>
		            </div>
		            
     				<!-- 說明 -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.Transfer_PP3_D5" /></li>
			        </ol>
		        </form>
	        </section>    
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	
</body>
<c:if test="${showDialog == 'true'}">
 	<%@ include file="../index/txncssslog.jsp"%>
</c:if>
</html>