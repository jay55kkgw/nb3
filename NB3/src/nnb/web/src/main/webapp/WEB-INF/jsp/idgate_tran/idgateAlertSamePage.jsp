<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
    jQuery(document).ready(function() {
        $("#idgate-block-s").bind('keydown', function(event) {
            if (event.keyCode == 13) { // 如果按 enter     
                $("#idgateForwardS").click();
            }
        });
        //判斷是不是idgate使用者
        var idgateUserFlag = "${idgateUserFlag}";
        if(idgateUserFlag =="Y"){
        	console.log("Is IDGATE user");           
        }else{
        	console.log("Not IDGATE user");           
        	showIdgateSBlock();
        }
                
        //註冊btn
        $("#idgateForwardS").click(function (e){
        	//前往IDGATE申請路徑
            $("#idgate-block-s-form").attr("action","${__ctx}/ONLINE/SERVING/idgate_confirm");
            $("#idgate-block-s-form").submit();
        })
        $("#idgateKnowmoreS").click(function (e){
            //TODO 操作手冊路徑
            $("#idgate-block-s-form").attr("action","");
            $("#idgate-block-s-form").submit();
        })
        $("#idgateNexttimeS").click(function (e){
            closeIdgateSBlock();
        })
    });
    
    // 顯示showIdgateSBlock
    function showIdgateSBlock() {
        $("#windowMask").show(); 
        $("#idgate-block-s").show();
    }
    // 取消showIdgateSBlock
    function closeIdgateSBlock() {
        $("#idgate-block-s").hide();
        $('#windowMask').hide();
        unBlockUI(initBlockId);
    }
</script>


<!-- IDGATE pop訊息 -->
<section id="idgate-block-s" class="error-block" style="display: none">
    <div class="error-for-code">
        <!-- 為使交易更加快速安全，提供裝置認證交易機制 -->
        <p class="error-title">為使交易更加快速安全，提供裝置認證交易機制</p>
<!--         請輸入您的晶片金融卡密碼以繼續。 -->
<!--         <p class="error-content"><spring:message code="LB.X2059" /></p> -->
              
        <button id="idgateForwardS" class="btn-flat-orange ttb-pup-btn" >前往申請</button>
        <button id="idgateKnowmoreS" class="btn-flat-orange ttb-pup-btn" >了解詳情</button>
        <button id="idgateNexttimeS" class="btn-flat-gray ttb-pup-btn" >下次再說</button>
    </div>
</section>

<form id="idgate-block-s-form" action="" method="post">

</form> 