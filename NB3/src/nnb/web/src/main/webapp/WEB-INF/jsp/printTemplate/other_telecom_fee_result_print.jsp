<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
			
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						交易時間 -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<%-- 						${CMTXTIME} --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						黃金轉出帳號 -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<%-- 						${ACN} --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						扣款失敗手續費<br />(繳款金額) -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<%-- 						${TRNAMT} --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						轉出帳號 -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<%-- 						${SVACN} --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						轉出帳號帳戶餘額 -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<%-- 						新台幣${O_TOTBAL}元 --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						轉出帳號可用餘額 -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<%-- 						新台幣${ O_AVLBAL }元 --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
			<table class="print">
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D1097" /></td>
					<td>${CMQTIME}</td>
				</tr>
				<c:if test="${ R1.equals('V1') }">
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0168" /></td>
					<td>${TSFACN}</td>
				</tr>
				</c:if>
				<c:if test="${ R1.equals('V2') }">
				<tr>
					<td style="text-align: center"><spring:message code= "LB.W0639" /></td>
					<td>${CARDNUM}</td>
				</tr>
				</c:if>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.W0692" /></td>
					<td>${CUSIDN}</td>
				</tr>
			</table>
		</div>
		
		<br>
		<br>
		<div class="text-left">
			<spring:message code="LB.Description_of_page" />
			<ol class="list-decimal text-left">
	            <li><spring:message code="LB.Other_Telecom_Fee_P3_D1"/></li>
	            	<li><spring:message code="LB.Other_Telecom_Fee_P3_D2"/></li>
	                <li><spring:message code="LB.Other_Telecom_Fee_P3_D3"/></li>
			</ol>
		</div>
	</div>
	
</body>
</html>