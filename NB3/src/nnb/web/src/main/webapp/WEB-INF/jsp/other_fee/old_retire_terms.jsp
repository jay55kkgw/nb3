<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>


<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$('#but_Agree').prop('checked', false);
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
            $("#formId").attr("action","${__ctx}/OTHER/FEE/other_old_retire_confirm");
	 	  	$("#formId").submit();
		});
		//上一頁按鈕
		$("#CMBACK").click(function() {
// 			initBlockUI();
// 			fstop.getPage('${pageContext.request.contextPath}'+'/OTHER/FEE/other_old_retire','', '');
			var action = '${__ctx}/OTHER/FEE/other_old_retire';
			$("#back").val("Y");
			$("form").attr("action", action);
			$("form").submit();
		});
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 舊制勞工退休提繳費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0752" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
               	<form id="formId" method="post" action="">
					<input type="hidden" id="back" name="back" value="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                        	<div class="ttb-message">
								<p><spring:message code="LB.X0086"/></p>
                        		<span><font color="royalblue"><font size="3">
                            	下列為申請委託轉帳代繳(舊制)勞工退休準備金應遵守之約定條款內容，您如接受本約定條款則請按<span class="Cron">我同意約定條款</span>鍵，以完成申請作業，您如不同意條款內容，則請按<span class="Cron">回上頁</span>鍵，本行將不受理您的代扣繳申請。</font></font></span>
                        	</div>
                        	<ul class="ttb-result-list terms">
	                            <li data-num="一、">
	                            	<p>本人【公司】(以下簡稱立約人)委託臺灣中小企業銀行(以下簡稱貴行)自指定之存款帳戶(即指前頁之扣帳帳號，以下簡稱轉帳代繳帳戶)轉帳代繳勞工退休準備金(以下簡稱舊制準備金)，並自行依據勞動基準法第56條規定提撥之勞工退休準備金存款單或對帳單內容輸入代扣繳資料，如因代扣繳申請書內容輸入不全、錯誤或其他原因，致貴行無法辦理轉帳，則本約定書不生效力，所受損失由立約人自行負責。</p>
	                            </li>
	                            <li data-num="二、">
	                            	<p>事業單位依其報稅薪資總額、核備提撥率(2%至15%)及實際退休給付金額，自行匡計擬按月繳交之舊制準備金金額以備定期定額扣繳、提繳金額異動或停止轉帳繳款時，立約人需至原代收行申請變更。</p>
	                            </li>
	                            <li data-num="三、">
	                            	<p>立約人申請轉帳代繳本人【公司】或指定第三人舊制準備金，自貴行同意接受委託，將轉帳代繳檔案輸入磁帶送台灣銀行審核，並自申請日之次月起開始轉帳代繳。</p>
	                            </li>
	                            <li data-num="四、">
	                            	<p>貴行代繳義務，以立約人轉帳代繳帳戶可用餘額足敷當月份(每月20日)委託代繳之舊制準備金為限。轉帳代繳帳戶餘額不敷繳付時，貴行得於月底(如遇例假日則順延至次一營業日)再行扣繳乙次(即每月20日及月底轉帳代繳帳戶須保持足夠之餘額以供備付)，倘仍存款不足，則由提繳單位自行持<font face="新細明體">｢勞工退休準備金存款單｣</font>至指定之金融機構繳納，因此致提繳單位須負擔之滯納金或罰鍰，概由立約人負責。</p>
	                            </li>
	                            <li data-num="五、">
	                            	<p>立約人委託代繳舊制準備金，如轉帳代繳帳戶因遭法院強制執行或其他事故致無法代繳時，貴行得終止代繳之約定，因此致提繳單位須負擔之滯納金或罰鍰，概由立約人負責。</p>
	                            </li>
	                            <li data-num="六、">
	                            	<p>立約人在貴行另行指定轉帳代繳帳戶時，應註銷原委託約定再重新辦理代扣繳申請；並同意自貴行受理變更，自完成變更之次月起，由新帳戶轉帳代繳舊制準備金。</p>
	                            </li>
	                            <li data-num="七、">
	                            	<p>立約人委託代繳舊制準備金，在未終止委託前，不得藉故拒絕繳納舊制準備金，因此致提繳單位須負擔之滯納金或罰鍰，概由立約人負責。</p>
	                            </li>
	                            <li data-num="八、">
	                            	<p>立約人委託代繳舊制準備金，在未終止委託前，自行結清轉帳代繳帳戶時，視同當然終止代繳之約定，應繳納之舊制準備金需由提繳單位持<font face="新細明體">｢勞工退休準備金存款單｣</font>至指定之金融機構繳納，因此致提繳單位須負擔之滯納金或罰鍰，概由立約人負責。</p>
	                            </li>
	                            <li data-num="九、">
	                            	<p>貴行或立約人皆得隨時以書面通知對方終止代繳契約。立約人終止代繳時應填具註銷約定書，並自貴行接受終止委託，將轉帳代繳檔案資料傳送台灣銀行審核，並自完成變更通知之月份起，終止以該轉帳代繳轉帳代繳，因終止委託致提繳單位須負擔之滯納金或罰鍰，概由立約人負責。又，提繳金額異動同此約定。</p>
	                            </li>
	                            <li data-num="十、">
	                            	<p>立約人指定之轉帳代繳帳戶為支票存款帳戶者，倘因扣繳舊制準備金而致存款不足，發生退票情事，概由立約人自行負責。</p>
	                            </li>
	                            <li data-num="十一、">
	                            	<p>本委託代繳舊制準備金未約定事項，立約人悉依勞工退休準備金各項有關法令辦理之。</p>
	                            </li>
	                            <li data-num="十二、">
	                            	<p>倘貴行之電腦系統發生故障或電信中斷等因素致無法執行轉帳代繳時，貴行得順延至系統恢復正常，始予扣款，其因上開事由所致之損失及責任，由立約人自行負擔。</p>
	                            </li>
	                            <li data-num="十三、">
	                            	<p>貴行於同一日需自轉帳代繳帳戶執行多筆轉帳扣繳作業而立約人存款不足時，立約人同意由貴行自行選定扣款順序。</p>
	                            </li>
	                            <li data-num="十四、">
	                            	<p>立約人委託代繳舊制準備金之收據由台灣銀行寄發。</p>
	                            </li>
	                            <li data-num="十五、">
	                            	<p>立約人同意貴行得將立約人個人根據特定目的填列之相關基本資料提供貴行電腦處理及利用。</p>
	                            </li>
							</ul>
						</div>
  	   					<input class="ttb-button btn-flat-gray" type="button" value="<spring:message code="LB.Back_to_previous_page"/>" name="CMBACK" id="CMBACK">
                      	<input class="ttb-button btn-flat-orange" type="button" value="<spring:message code="LB.W1554"/>" name="CMSUBMIT" id="CMSUBMIT">
                    </div>
                </div>
                </form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>