<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function () {
			//alert('${card_autopay_apply.data.PAYMT_ACCT}');
			//alert('${card_autopay_apply.data.DBCODE}');
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("creatOutACNO();", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			
		});
		
		function creatOutACNO() {
			var options = {
				keyisval : true,
				selectID : '#PAYACN'
			};
			uri = '${__ctx}' + "/CREDIT/AUTOMATIC/getOutAcno_aj"
			console.log("getSelectData>>" + uri);
			rdata = {
				type : 'out_acno'
			};
			console.log("rdata>>" + rdata);
			data = fstop.getServerDataEx(uri, rdata, false);
			if (data != null && data.result == true) {
				fstop.creatSelect(data.data, options);
			}
		}
		
		function displayFields() {
			   
			  var main = document.getElementById("formId");	
			  var contract = document.getElementById("CONTRACT");
		   	  
		   	  //若為取消	
		      if (main.APPLYFLAG[1].checked) {
				 main.PAYACN.disabled = true;
				 main.PAYFLAG[0].disabled = true;
				 main.PAYFLAG[1].disabled = true; 
				 main.PAYFLAG[0].checked= false;
				 main.PAYFLAG[1].checked = false;
			     main.PAYFLAG[0].value = '03';
			     main.PAYFLAG[1].value = '03';		 
				 contract.style.display = "none";
			  }   
			  //若為申請/變更
			  else {   	 		 
					 contract.style.display = "";
					 var dbcode = main.ODBCODE.value;
			         var pay_act = main.CARDPAYACN.value;			 
					 main.PAYFLAG[0].disabled = false;
					 main.PAYFLAG[1].disabled = false;				 
		             if (main.APPLYFLAG[0].checked) {
				         main.PAYACN.disabled = false;
				         main.PAYFLAG[0].checked= true;
						 main.PAYFLAG[0].value = '01';
			             main.PAYFLAG[1].value = '02';				 
		             }			 
		             if (main.APPLYFLAG[2].checked) {
				         main.PAYACN.disabled = true;				 
		                 main.TSFACN.value=pay_act;
			      		 main.PAYFLAG[0].value = '04';
			      		 main.PAYFLAG[1].value = '05';
						if (pay_act == '99999999999' && dbcode!='') {
							if (dbcode == '01') {
								main.PAYFLAG[0].disabled = true;
								main.PAYFLAG[1].disabled = false;	      
								main.PAYFLAG[1].checked = true;	      	
							}
							if (dbcode == '02') {	      
								main.PAYFLAG[0].disabled = false;
								main.PAYFLAG[1].disabled = true;	      
								main.PAYFLAG[0].checked = true;	      		      		      
							}                  
						}
						if (dbcode=='01') {
							main.PAYFLAG[0].disabled = true;
							main.PAYFLAG[1].disabled = false;	          
							main.PAYFLAG[1].checked = true;      
							main.PAYFLAG[0].value = '05';
		                }				
						if (dbcode=='02') {
							main.PAYFLAG[0].disabled = false;
							main.PAYFLAG[1].disabled = true;	          
							main.PAYFLAG[0].checked = true;      
							main.PAYFLAG[0].value = '04';
		                }				
		             }
					checkFlag();
			  }	  	 
		   }
		   
		   function checkFlag() {
		   
			  var main = document.getElementById("formId");
		      if (main.APPLYFLAG[2].checked) {
				         main.PAYACN.disabled = true;	
		                 main.TSFACN.value=main.CARDPAYACN.value;				 
		      }		
			  var dbcode = main.ODBCODE.value;
			  var pay_act = main.CARDPAYACN.value;
		   
			  if (pay_act == '99999999999' && dbcode!='') {	  
			      main.APPLYFLAG[0].disabled = false;
			      main.APPLYFLAG[1].disabled = true;
			      main.APPLYFLAG[2].disabled = false;
			      //main.APPLYFLAG[0].checked  =true;	      
		          //申請
				  if (main.APPLYFLAG[0].checked) {		  
		            main.TSFACN.value=main.PAYACN.options[main.PAYACN.selectedIndex].value;
					main.PAYFLAG[0].disabled = false;
					main.PAYFLAG[1].disabled = false;	      			
			        main.PAYFLAG[0].value = '01';
			        main.PAYFLAG[1].value = '02';				
				  }
				  
			      //變更
			      if (main.APPLYFLAG[2].checked) {
			      	if (dbcode == '01') {
				  		main.PAYFLAG[0].disabled = true;
				  		main.PAYFLAG[1].disabled = false;	      
			      		main.PAYFLAG[1].checked = true;	      	
			      	}
		            if (dbcode == '02') {	      
				  		main.PAYFLAG[0].disabled = false;
				  		main.PAYFLAG[1].disabled = true;	      
			      		main.PAYFLAG[0].checked = true;	      		      		      
			      	}
			      		main.PAYFLAG[0].value = '04';
			      		main.PAYFLAG[1].value = '05';	
			      }	        
			  }	
			  else if (pay_act == '99999999999' && dbcode=='') {
		 	      main.APPLYFLAG[0].disabled = false;
			      main.APPLYFLAG[1].disabled = true;
			      main.APPLYFLAG[2].disabled = true;
			      main.APPLYFLAG[0].checked  =true;	      
		          main.PAYACN.disabled = false;
		          //申請
				  if (main.APPLYFLAG[0].checked) {   
		            main.TSFACN.value=main.PAYACN.options[main.PAYACN.selectedIndex].value;
					main.PAYFLAG[0].disabled = false;
					main.PAYFLAG[1].disabled = false;	      			
			        main.PAYFLAG[0].value = '01';
			        main.PAYFLAG[1].value = '02';				
				  }        	  
			  }
			  else if (pay_act == '' && dbcode!='') {	  
			      main.APPLYFLAG[0].disabled = false;
			      main.APPLYFLAG[1].disabled = true;
			      main.APPLYFLAG[2].disabled = false;
			      //main.APPLYFLAG[0].checked  =true;	      
		          //申請
				  if (main.APPLYFLAG[0].checked) {   
		            main.TSFACN.value=main.PAYACN.options[main.PAYACN.selectedIndex].value;
					main.PAYFLAG[0].disabled = false;
					main.PAYFLAG[1].disabled = false;	      			
			        main.PAYFLAG[0].value = '01';
			        main.PAYFLAG[1].value = '02';				
				  }	  
			      //變更
			      if (main.APPLYFLAG[2].checked) {
			      	if (dbcode == '01') {
				  		main.PAYFLAG[0].disabled = true;
				  		main.PAYFLAG[1].disabled = false;	      
			      		main.PAYFLAG[1].checked = true;	      	
			      	}
		            if (dbcode == '02') {	      
				  		main.PAYFLAG[0].disabled = false;
				  		main.PAYFLAG[1].disabled = true;	      
			      		main.PAYFLAG[0].checked = true;	      		    		      
			      	}
			      		main.PAYFLAG[0].value = '04';
			      		main.PAYFLAG[1].value = '05';	
			      }	      		     		  	      	      
			  }       
			  else if (pay_act == '' && dbcode=='') {
		 	      main.APPLYFLAG[0].disabled = false;
			      main.APPLYFLAG[1].disabled = true;
			      main.APPLYFLAG[2].disabled = true;
			      main.APPLYFLAG[0].checked  =true;	      
		          //申請
				  if (main.APPLYFLAG[0].checked) {   
		            main.TSFACN.value=main.PAYACN.options[main.PAYACN.selectedIndex].value;
					main.PAYFLAG[0].disabled = false;
					main.PAYFLAG[1].disabled = false;	      			
			        main.PAYFLAG[0].value = '01';
			        main.PAYFLAG[1].value = '02';				
				  }        	  
			  }
			  else if ((pay_act == main.PAYACN.options[main.PAYACN.selectedIndex].value) && (dbcode == '02')) {	  
			      main.APPLYFLAG[0].disabled = true;
			      main.APPLYFLAG[1].disabled = false;
			      main.APPLYFLAG[2].disabled = false;	      	      
			      main.APPLYFLAG[2].checked = true;
		          if (main.APPLYFLAG[2].checked) {	      
					main.PAYFLAG[0].disabled = false;
					main.PAYFLAG[1].disabled = true;	          
					main.PAYFLAG[0].checked = true;      
					main.PAYFLAG[0].value = '04';	 		  	      	      
		          }
				  
		          if (main.APPLYFLAG[1].checked) {	      
					main.PAYFLAG[0].disabled = true;
					main.PAYFLAG[1].disabled = true;	          
					main.PAYFLAG[1].checked = true;      
					main.PAYFLAG[1].value = '03';	 		  	      	      
		          }		  
				 
			  }       
			  else if ((pay_act == main.PAYACN.options[main.PAYACN.selectedIndex].value) && (dbcode == '01')) {	  
			      main.APPLYFLAG[0].disabled = true;
			      main.APPLYFLAG[1].disabled = false;
			      main.APPLYFLAG[2].disabled = false;	      	      
			      main.APPLYFLAG[2].checked = true;
			      
		          if (main.APPLYFLAG[2].checked) {
					main.PAYFLAG[0].disabled = true;
					main.PAYFLAG[1].disabled = false;	           
					main.PAYFLAG[1].checked = true;	   
					main.PAYFLAG[1].value = '05';	 	  	      	      
		          }
		          if (main.APPLYFLAG[1].checked) {
					main.PAYFLAG[0].disabled = true;
					main.PAYFLAG[1].disabled = true;	           
					main.PAYFLAG[0].checked = true;	     
					main.PAYFLAG[0].value = '03';	 	  	      	      
		          }		  
			  }
			  else if ((main.PAYACN.options[main.PAYACN.selectedIndex].value != '#') && 
			           (pay_act != main.PAYACN.options[main.PAYACN.selectedIndex].value)) {
						main.APPLYFLAG[0].disabled = false;
						main.APPLYFLAG[1].disabled = false;
		                main.APPLYFLAG[2].disabled = true;				
		 			 if (dbcode =='01') {
						main.PAYFLAG[0].checked = true;
						main.PAYFLAG[0].disabled =false;
						main.PAYFLAG[1].disabled = false;
					 }
		             else if (dbcode =='02') {
						main.PAYFLAG[1].checked = true;
						main.PAYFLAG[0].disabled =false;
						main.PAYFLAG[1].disabled = false;			  
					 }
			         main.PAYFLAG[0].value = '01';
			         main.PAYFLAG[1].value = '02';				 
			  }		
			  else if (main.PAYACN.options[main.PAYACN.selectedIndex].value == '#') {
	                main.APPLYFLAG[2].disabled = false;					 
			  }	
		}
		
		 function processQuery()
		   {  
			  var main = document.getElementById("formId");
			        	        
			  //欄位檢核
		   	  
		   	  if ((! main.APPLYFLAG[1].checked) && (! main.PAYACN.disabled) && ($('#PAYACN').val() == '#')){
		   			errorBlock(
							null, 
							null,
							['<spring:message code= 'LB.X0855' />'], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
		   		 return false;
		   	  }
		   	

		   	  if ((! main.APPLYFLAG[0].checked) && (! main.APPLYFLAG[1].checked) && (! main.APPLYFLAG[2].checked)) {
		   	     //alert('<spring:message code= "LB.Alert049" />');
		   	   	errorBlock(
							null, 
							null,
							['<spring:message code= 'LB.Alert049' />'], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
		   		 return false;
		   	  }
		   	  	 
			  if (main.PAYACN.disabled == false)
			  	  main.TSFACN.value = main.PAYACN.options[main.PAYACN.selectedIndex].value;	
				
		  
		      if ((main.PAYACN.options[main.PAYACN.selectedIndex].value != '#') && (main.APPLYFLAG[2].checked)) {
		      	  var pay_act = '${debit_apply.data.PAYMT_ACCT}';
		      
		          if (pay_act == '99999999999') {
		            //var flag = confirm('<spring:message code= "LB.Confirm018" />\n<spring:message code= "LB.Confirm019" />\n\n<spring:message code= "LB.Confirm020" />!');
		            errorBlock(
							null, 
							null,
							['<spring:message code= "LB.Confirm018" /><br><spring:message code= "LB.Confirm019" /><br><spring:message code= "LB.Confirm020" />!'], 
							'<spring:message code= "LB.Confirm" />', 
							'<spring:message code= "LB.Cancel" />'
						);
		            return false;
		          }
		      }
		      if (! main.APPLYFLAG[1].checked && 
			      		main.PAYAGREE.checked == false) {
			      		
					 //alert("<spring:message code= "LB.Alert050" />");
					 errorBlock(
								null, 
								null,
								['<spring:message code= 'LB.Alert050' />'], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
					 main.PAYAGREE.focus();
					 
			   	     return false;   				
				  }	
				        
			      main.CMSUBMIT.disabled = true;         	  
				  main.PAYFLAG.disabled = false;
				  	  		  	      	  	   
				  if (main.PAYFLAG[0].checked)	  		  	      	  	                           	 
			      	main.display_PAYFLAG.value = "1";
			      else
			        main.display_PAYFLAG.value = "2";
			    	 	  	  		  	      	  	                           	                           

				  console.log("submit~~");
				  
				  initBlockUI(); //遮罩
				  $("#formId").prop("action", "${__ctx}/CREDIT/AUTOMATIC/debit_apply_confirm");
				  $("#formId").submit();
			      				
			      
			      return false;
		      
		   }
		      
			$("#errorBtn1").click(function(){
				var main = document.getElementById("formId");	
				$('#error-block').hide();
				main.APPLYFLAG[0].checked = true;
            	checkFlag();
            	
            	return false;
		   })
		   
		   $("#errorBtn2").click(function(){
			   var main = document.getElementById("formId");	
			   $('#error-block').hide();
			   if (! main.APPLYFLAG[1].checked && 
			      		main.PAYAGREE.checked == false) {
			      		
					 //alert("<spring:message code= "LB.Alert050" />");
					 errorBlock(
								null, 
								null,
								['<spring:message code= 'LB.Alert050' />'], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
					 main.PAYAGREE.focus();
					 
			   	     return false;   				
				  }	
				        
			      main.CMSUBMIT.disabled = true;         	  
				  main.PAYFLAG.disabled = false;
				  	  		  	      	  	   
				  if (main.PAYFLAG[0].checked)	  		  	      	  	                           	 
			      	main.display_PAYFLAG.value = "1";
			      else
			        main.display_PAYFLAG.value = "2";
			    	 	  	  		  	      	  	                           	                           

				  console.log("submit~~");
				  
				  initBlockUI(); //遮罩
				  $("#formId").prop("action", "${__ctx}/CREDIT/AUTOMATIC/debit_apply_confirm");
				  $("#formId").submit();
			      				
			      
			      return false;
		   })
	</script>
</head>

<body>
	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 自動扣款申請/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0166" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code="LB.D0166" /><!-- 信用卡款自動扣繳申請/取消 -->
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId"  name="formId" action="" method="post">
					<input type="hidden" name="ADOPID" value="NI04">	
					<input type="hidden" id="BDCODE" name="DBCODE" value="">
  					 <input type="hidden" id="TSFACN" name="TSFACN" value="">
  					 <input type="hidden" name="ADOPID" value="NI04">
                     <input type="hidden" name="display_CCACNO" value="">
                     <input type="hidden" name="display_PAYFLAG" value="">
					 <input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}">
					 <input type="hidden" name="CARDPAYACN" value="${debit_apply.data.PAYMT_ACCT}">
	   				 <input type="hidden" name="ODBCODE" value="${debit_apply.data.DBCODE}">
						<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<!--交易時間區塊 -->
								<div class="ttb-input-item row">
									<!--交易時間  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Trading_time" /><!-- 交易時間 -->
										</label>
									</span>
									<span class="input-block">
										${debit_apply.data.CMQTIME}
									</span>
								</div>
								<!-- 扣款帳號區塊 -->
								<div class="ttb-input-item row">
									<!--扣款帳號  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.D0168" /><!-- 扣款帳號 -->
										</label>
									</span>
									<span class="input-block">
										<select id="PAYACN" name="PAYACN" class="custom-select select-input half-input"  onchange="checkFlag()">
											<!-- 請選擇帳號 -->
											<option value="#">---<spring:message code="LB.X0855" />---</option>
										</select>
									</span>
								</div>
								<!-- 申請項目區塊 -->
								<div class="ttb-input-item row">
									<!--申請項目  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.D0169" /><!-- 申請項目 -->
										</label>
									</span>
									<span class="input-block">
										<label class="radio-block">
											<input type="radio" name="APPLYFLAG" value="1"  checked onclick="displayFields()">
											<spring:message code="LB.Apply" /><!-- 申請 -->
											<span class="ttb-radio"></span>
										</label>
										
									<c:if test="${debit_apply.data.PAYMT_ACCT eq '99999999999'}">
										<label class="radio-block">
											<input type="radio" name="APPLYFLAG" value="2"  disabled>
												<spring:message code="LB.Cancel" /><!-- 取消 -->
											<span class="ttb-radio"></span>
										</label>
									</c:if>
									
									<c:if test="${debit_apply.data.PAYMT_ACCT != '99999999999'}">
										<label class="radio-block">
											<input type="radio" name="APPLYFLAG" value="2"  onclick="displayFields()">
											<spring:message code="LB.Cancel" /><!-- 取消 -->
											<span class="ttb-radio"></span>
										</label>&nbsp;&nbsp;&nbsp;
									</c:if>
										<label class="radio-block">
											<input type="radio" name="APPLYFLAG" value="3"  onclick="displayFields()">
											<spring:message code="LB.D0172" /><!-- 變更扣繳方式 -->
											<span class="ttb-radio"></span>
										</label>
									</span>
								</div>
								<!-- 扣繳方式區塊 -->
								<div class="ttb-input-item row">
									<!--扣繳方式  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Payment_method" /><!-- 扣繳方式 -->
										</label>
									</span>
									<span class="input-block">
										<label class="radio-block">
											<input type="radio" name="PAYFLAG" value="01" checked >
											<spring:message code="LB.D0174" /><!-- 應繳總額 -->
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block">
											<input type="radio" name="PAYFLAG" value="02"  >
											<spring:message code="LB.D0175" /><!-- 最低應繳額 -->
											<span class="ttb-radio"></span>
										</label>
									</span>
								</div>
								<!-- 約定條款區塊 -->
								<div class="ttb-input-item row" id="CONTRACT">
									<!--約定條款  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.D0176" /><!-- 約定條款 -->
										</label>
									</span>
									<span class="input-block">
										<label class="check-block">
										<input type="checkbox" name="PAYAGREE" value="Y">
										<spring:message code="LB.D0177" />
<!-- 										本人同意貴行就本人信用卡(含正、附卡)所產生之一切款項逕自本人在貴行開立之存款帳戶，依自動扣繳轉帳繳款授權所約定之扣繳方式辦理自動扣繳。 -->
										<span class="ttb-check"></span>
										</label>
									</span>
								</div>
							</div>
							<!-- 網頁顯示 button-->
								<!--網頁顯示 -->
								<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
								<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange" onclick="processQuery()">
							
						</div>
					</div>
					<ol class="description-list list-decimal">
						<!-- 		說明： -->
						<p><spring:message code="LB.Description_of_page"/></p>
							<li><spring:message code="LB.Debit_Apply_P1_D1" /></li>
							<li><spring:message code="LB.Debit_Apply_P1_D2" /></li>
							<li><spring:message code="LB.Debit_Apply_P1_D3" /></li>
					</ol>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
      
</body>

</html>