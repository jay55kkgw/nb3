<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${MSG}</font></div>
<br/><br/><br/>
<table class="print">
	<!-- 								資料時間 -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.Data_time" /></td>
		<td>${CMQTIME}</td>
	</tr>
	<!--								  預約編號     -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.Booking_number" /></td>
		<td>${FXSCHNO}</td>
	</tr>
	<!-- 					           	 週期    -->	
	<tr>
		<td style="text-align:center"><spring:message code="LB.Period" /></td>
		<td>
			<c:if test="${FXPERMTDATE == 'S'}">
				<spring:message code="LB.X1796" />
			</c:if>
			<c:if test="${FXPERMTDATE != 'S'}">
				<spring:message code="LB.X1797" /> ${FXPERMTDATE} <spring:message code="LB.D0586" />
			</c:if>
		</td>
	</tr>
	<!-- 					                生效日截止日	-->	
	<tr>
		<td style="text-align:center">
			<spring:message code="LB.Effective_date" />
			</br>
			<spring:message code="LB.Deadline" />
		</td>
		<td>
			${FXFDATE}							                	
            <c:if test="${FXTDATE != ''}">
            <br>${FXTDATE}
            </c:if>
		</td>
	</tr>
	<!-- 					            下次轉帳日     -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.Next_transfer_date" /></td>
		<td>${FXNEXTDATE}</td>
	</tr>
	<!-- 					            轉出帳號     -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.Payers_account_no" /></td>
		<td>${FXWDAC}</td>
	</tr>
	<!-- 					            轉入帳號/繳費稅代號    -->	
	<tr>
		<td style="text-align:center">
			<spring:message code="LB.Deducted" />
		</td>
		<td>${FXWDCURR}</br>${FXWDAMT}</td>
	</tr>
	<!-- 					            銀行名稱轉入帳號     -->		
	<tr>
		<td style="text-align:center">
			<spring:message code="LB.Bank_name" />
			</br>
			<spring:message code="LB.Payees_account_no" />
		</td>
		<td>
		${FXSVBH}
		<br>
		${FXSVAC}		
		</td>
	</tr>
	<!-- 					          轉入金額                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.Buy" /></td>
		<td>${FXSVCURR}</br>${FXSVAMT}</td>
	</tr>
	<!-- 					          交易方式                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.Transaction_type" /></td>
		<td>${TXTYPE}</td>
	</tr>
	<!-- 					            備註                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.Note" /></td>
		<td>${FXTXMEMO}</td>
	</tr>
</table>
<br/><br/>
<ol class="description-list list-decimal">
	<p><spring:message code="LB.Description_of_page" /></p>
	<li><span><spring:message code="LB.Reservation_P2_D1" /></span></li>
	<li><span><spring:message code="LB.Reservation_P2_D2" /></span></li>
</ol>
<!-- 	<div class="text-left"> -->
<!-- 		說明: -->
<!-- 		<ol class="list-decimal text-left"> -->
<!-- 			<li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
<!-- 			<li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
<!-- 		</ol> -->
<!-- 	</div>	 -->
</body>
</html>