<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 貸款資產總額 -->
<div class="account-left tab-pane fade" id="LOAN" role="tabpanel" aria-labelledby="tab-2">
    <div class="main-account">
        <span class="title"><spring:message code='LB.Loan_Outstanding'/><span class="description"></span></span>
        <span class="content" onclick='showEyeClick()'><img src='${__ctx}/img/eye-solid-orange.svg' /></span>
    </div>
    <div class="account-item">
        <div class="account-item-show">
            <span><spring:message code='LB.X2323'/></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/LOAN/QUERY/loan_detail','', '')" id="loan_overview_tw"></a></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/LOAN/QUERY/loan_detail','', '')" ><spring:message code='LB.W0953'/></a></span>       
        </div>
        
        <div id="loan_tw" class="account-item-hide">
            <span id="loan_balance_cuid_tw"></span>
            <span id="loan_balance_money_tw"></span>
            <span></span>
        </div>
    </div>
    <div class="account-item" id="loan_fx_outter">
    	<div class="account-item-show">
            <span><spring:message code='LB.X2324'/></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/LOAN/QUERY/loan_detail','', '')" id="loan_overview_fx"></a></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/LOAN/QUERY/loan_detail','', '')" ><spring:message code='LB.W0953'/></a></span>
        </div>
       
        
    </div>
	<div id="loan_fx" class="account-item-hide">
		<span></span>
		<span></span>
		<span></span>
	</div>
</div>

 
<script type="text/JavaScript">

function getLOAN_aj() {
	// 先清空資料初始化
	$("#loan_balance_cuid_tw").empty();
	$("#loan_balance_money_tw").empty();
	$("#loan_overview_tw").empty();
	$("#loan_overview_fx").empty();
	$("#loan_fx_outter .account-item-hide").remove('');
	
	console.log("getLOAN_aj.now: " + new Date());
	
	uri = '${__ctx}' + "/INDEX/summary_LOAN";

	fstop.getServerDataEx(uri, null, true, showLOAN);
}

function showLOAN(data) {
	// 先清空資料初始化
	$("#loan_balance_cuid_tw").empty();
	$("#loan_balance_money_tw").empty();
	$("#loan_overview_tw").empty();
	$("#loan_overview_fx").empty();
	$("#loan_fx_outter .account-item-hide").remove('');
	
	// 臺幣
// 	console.log(data.data.loan_tw_result);
	if (data.data.loan_tw_result != 'error' && data.data.loan_tw_result != 'null' && data.data.loan_tw_result != null) {
		
		var parsedataTw = JSON.parse(data.data.loan_tw_result);
		
		if (parsedataTw.TWNUM == '1') {
			// 臺幣帳戶
			$("#loan_overview_tw").html( parsedataTw.ACN );
			// 臺幣金額
			$("#loan_balance_cuid_tw").html( "<spring:message code='LB.NTD'/>" );
			$("#loan_balance_money_tw").html("<span class='accountST' account='" + parsedataTw.TOTALBAL_320 + "'>" + coverSoA(parsedataTw.TOTALBAL_320) + "</span> <spring:message code='LB.D0509'/>" );
			
		} else if (parsedataTw.TWNUM == '0' || parsedataTw.TWNUM == 'null' || parsedataTw.TWNUM == null) {
			// 查無資料
			console.log("showLoanTw.error...");
			$("#loan_overview_tw").prepend("<spring:message code='LB.D0017'/>");
			
		} else {
			// 臺幣筆數
			$("#loan_overview_tw").html( "<spring:message code='LB.D0360_1'/> " + parsedataTw.TWNUM + " <spring:message code='LB.Rows'/>" );
			// 臺幣金額
			$("#loan_balance_cuid_tw").html( "<spring:message code='LB.NTD'/>" );
			$("#loan_balance_money_tw").html("<span class='accountST' account='" + parsedataTw.TOTALBAL_320 + "'>" + coverSoA(parsedataTw.TOTALBAL_320) + "</span> <spring:message code='LB.D0509'/>" );
		}
		
	} else {
		console.log("showLoanTw.error...");
		$("#loan_overview_tw").prepend("<spring:message code='LB.D0017'/>");
	}
	
	// 外幣
// 	console.log(data.data.loan_fx_result);
	if (data.data.loan_fx_result != 'error' && data.data.loan_fx_result != 'null' && data.data.loan_fx_result != null) {
		var parsedataFx = JSON.parse(data.data.loan_fx_result);
		var crydata = parsedataFx.CRYMAP;
		// 滾外幣資訊
		crydata.forEach(function(cry){
			var newdiv = $("#loan_fx").clone();
			newdiv.show();
			newdiv[0].children[0].textContent = cry.AMTLNCCY + " " + cry.CUID_NAME;
			$(newdiv[0].children[1]).html("<span class='accountST' account='" + cry.FXTOTAMT + "'>" + coverSoA(cry.FXTOTAMT) + "</span>");
			$("#loan_fx_outter").append(newdiv);
		});
		
		if (parsedataFx.FXNUM == "1") {
			// 外幣帳戶
			$("#loan_overview_fx").html(parsedataFx.ACN);
			
		} else if (parsedataFx.FXNUM == '0' || parsedataFx.FXNUM == 'null' || parsedataFx.FXNUM == null) {
			// 查無資料
			console.log("showLoanFx.error...");
			$("#loan_overview_fx").prepend("<spring:message code='LB.D0017'/>");
			
		} else {
		    // 外幣筆數
		    $("#loan_overview_fx").html("<spring:message code='LB.D0360_1'/> " + parsedataFx.FXNUM + " <spring:message code='LB.Rows'/>");
		}
		
	} else {
		console.log("showLoanFx.error...");
		$("#loan_overview_fx").prepend("<spring:message code='LB.D0017'/>");
	}
	
	// 解遮罩
	unAcctsBlock('LOAN');
	
}


</script>
