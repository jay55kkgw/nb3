<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdentity.js"></script>
	
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
	

	<script type="text/javascript">
	var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
	//var myobj = null; // 自然人憑證用
	var urihost = "${__ctx}";
	$(document).ready(function() {
		// HTML載入完成後0.1秒開始遮罩
		setTimeout("initBlockUI()", 100);
		

		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		// 銀行代碼
		setTimeout("creatDpBHNO()",500);

		setTimeout("init()", 20);
		
		// 過3秒解遮罩
		setTimeout("unBlockUI(initBlockId)", 3000);

		datetimepickerEvent();
	});
// 	var isconfirm = false;
// 	$("#errorBtn1").click(function(){
// 		if(!isconfirm)
// 			return false;
// 		isconfirm = false;
// 		$("#naturalComponent")[0].click();
// 	});
// 	$("#errorBtn2").click(function(){
// 		isconfirm = false;
// 		$('#error-block').hide();
// 	});
	// 初始化自然人元件
// 	function initNatural() {
// 		var Brow1 = new ckBrowser1();
// 		if(!Brow1.isIE){	
// 			if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {
// 				myobj = initCapiAgentForChrome();
// 				try {
// 					if(sessionToken == null){
// 						myobj.initChrome();
// 						console.log("myobj.initChrome...");
// 					}
// 				} catch (e) {
// // 					if(confirm("<spring:message code= "LB.X1216" />")){
// // 						$("#naturalComponent")[0].click();
// // 					}
// 					isconfirm = true;
// 					errorBlock(
// 							null, 
// 							null,
// 							['<spring:message code= "LB.X1216" />'], 
// 							'<spring:message code= "LB.Confirm" />', 
// 							'<spring:message code= "LB.Cancel" />'
// 						);
// 				}			
// 			}
// 		} else {
// 			var strObject = "";
// 	    	// 使用 Script 判斷 win64 或 win32, 執行時間有點久
// 	 		if (navigator.platform.toLowerCase() == "win64"){
// 				// "Windows 64 位元";
// 	 			document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATLx64.cab" width="0px" height="0px"></object>';
// 			} else {
// 				// "Windows 32 位元 或 模擬 64 位元(WOW64)";
// 				document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATL.cab" width="0px" height="0px"></object>';
// 			}
// 		    myobj = document.myobj;
// 		}
// 		console.log("initNatural.finish...");
// 	}
	
	/**
	 * 初始化BlockUI
	 */
	function initBlockUI() {
		initBlockId = blockUI();
	}

	// 交易機制選項
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
	
		switch(fgtxway) {
			case '0':
//					alert("交易密碼(SSL)...");
    			$("form").submit();
				break;
				
			case '1':
//					alert("IKey...");
				var jsondc = $("#jsondc").val();
				
				// 遮罩後不給捲動
//					document.body.style.overflow = "hidden";

				// 呼叫IKEY元件
//					uiSignForPKCS7(jsondc);
				
				// 解遮罩後給捲動
//					document.body.style.overflow = 'auto';
				
				useIKey();
				break;
				
			case '2':
//					alert("晶片金融卡");

				// 遮罩後不給捲動
//					document.body.style.overflow = "hidden";
				
				// 呼叫讀卡機元件
				useCardReader();

				// 解遮罩後給捲動
//					document.body.style.overflow = 'auto';
				
		    	break;

			case '4':
//				自然人憑證
				useNatural();
		    	break;
			case '5':
				//跨行帳戶驗證
				var uri = '${__ctx}'+"/FISC/InterBank/verify_account_aj";
				var rdata = $("#formId").serializeArray();
				callInterBankVerifyAcc(uri, rdata, "InterBankVerifyOTP","refreshCapCode", "validateMaskInterBankAcc");
				break;			    	
			case '6':  	
				//本行帳戶驗證
				var uri = '${__ctx}'+"/DIGITAL/ACCOUNT/verify_lo_account_aj";
				var rdata = $("#formId").serializeArray();
				callInterBankVerifyAcc(uri, rdata, "LoBankVerifyOTP","refreshCapCode", "validateMaskLoBankAcc");
				break;	  
			default:
				alert("nothing...");
		}
		
	}

    function init(){
//	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
		$("#formId").validationEngine({
			validationEventTriggers:'keyup blur', 
			binded:true,
			scroll:true,
			addFailureCssClassToField:"isValid",
			promptPosition: "inline" });
		$("#CMSUBMIT").click(function(e){			
			console.log("submit~~");
			var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
			var capData = $("#formId").serializeArray();
			var capResult = fstop.getServerDataEx(capUri, capData, false);
			if (capResult.result) {
				if(!$('#formId').validationEngine('validate') && $('input[name="FGTXWAY"]:checked').val() == 0){
		        	e.preventDefault();
	 			}else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
    				var action = '${__ctx}/DIGITAL/ACCOUNT/upload_digital_identity_p2';
	    			$("form").attr("action", action);
	    			unBlockUI(initBlockId);
	    			processQuery();
	 			}
			} else {
				//alert("<spring:message code= "LB.X1082" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= "LB.X1082" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				changeCode();
			}
		});
		$("#CMBACK").click( function(e) {
			$("#formId").validationEngine('detach');
			console.log("submit~~");
			$("#formId").attr("action", "${__ctx}/ONLINE/APPLY/online_apply");
			$("#back").val('Y');
//          	initBlockUI();
            $("#formId").submit();
		});
		// 初始化自然人元件
		var acntype = "${result_data.data.ACNTYPE}";
		if (acntype == "A1") {
	    	// 初始化自然人元件
			initBlockUI(); 
			setTimeout("initNatural_EXCUTE()", 50);
		}
    }	
    
	// 刷新驗證碼
	function refreshCapCode() {
		console.log("refreshCapCode...");

		// 驗證碼
		$('input[name="capCode"]').val('');
		
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 刷新輸入欄位
	function changeCode() {
		console.log("changeCode...");
		
		// 清空輸入欄位
		$('input[name="capCode"]').val('');
		
		// 刷新驗證碼
		refreshCapCode();
	}

	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}

	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			refreshCapCode();
		});
	}
	//取得卡片主帳號結束
	function getMainAccountFinish(result){
		//成功
		if(result != "false"){
			var cardACN = result;
			if(cardACN.length > 11){
				cardACN = cardACN.substr(cardACN.length - 11);
			}
			$("#ACNNO").val(cardACN);
			$("#OUTACN").val(cardACN);
			$("#CHIP_ACN").val(cardACN);
			var UID = $("#CUSIDN").val();
//			var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//			var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
			
			var uri = urihost+"/COMPONENT/component_without_id_aj";
			var rdata = { ACN: cardACN, UID: UID };
			fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
		}
		//失敗
		else{
			showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",false);
		}
	}

	//	建立銀行代號下拉選單
	function creatDpBHNO(){
		//宣告銀行物件 
		var bkdataList = {}
		//2021/05/13 ， 業務單位email通知 
		//非同步名單，若要新增參加銀行，必須請數金部給需求，並依資訊部行程上線，非急緊上線！
		//update 2022/03/10	H571110000111	
		var accedingBK = ['004','005','006','007','008','009','011', '012','013','016','017',
									'021','048','052','054','101','102','103','108','114','118','132','146', '147','162','216','600','803',
									'805','806','807','808','809','810','812','815','816','822','824','826','952'];
		//var excludeBk = ['000','001','050'];
		
		var uri = '${__ctx}'+"/FISC/InterBank/getDpBHNO_aj";
		var rdata = {type: 'dpbhno' };
		var options = { keyisval:true ,selectID:'#DPBHNO'}
		var data = fstop.getServerDataEx(uri,rdata,false);
		if(data !=null && data.result == true ) {			
			//建立選單資料 
			$.each(data.data,function(value,text){
				if (accedingBK.indexOf(text.substr(0,3)) > -1) {					
					bkdataList[value] = text;
				}
			});
			fstop.creatSelect(bkdataList,options);			
		}
		
		$( "#DPBHNO" ).change(function() {
			var acno = $('#DPBHNO :selected').val();
			console.log("DPBHNO.acno>>"+acno);
			
			$( "#ATTIBK" ).val(acno.substr(0,3));
		});
	}

	function  InterBankVerifyOTP(data) {
		initBlockUI(); 
		$("#otpCommit").attr("disabled", true);
		$('#otp_error').hide();
		console.log(data);

		var uri = '${__ctx}'+"/FISC/InterBank/otpverify_aj";
		var rdata = {
				OTP : data,
				CUSIDN : $("#CUSIDN").val()
			};
		
		fstop.getServerDataEx(uri, rdata, true,checkOTPFish);
	}

	function checkOTPFish(otpresult) {
		unBlockUI(initBlockId);
		$("#otpCommit").attr("disabled", false);
		if (otpresult.result) {
			closeOTPDialog();
			initBlockUI(); 
			try{
				$("#OTPTOKEN").val(otpresult.data.otptoken);
			}catch(e){
				console.log(e);
			}
			$("#formId").submit();	
		} else {
			$('#otp_error').show();
		}
		
	}
	
	function LoBankVerifyOTP(data){
		initBlockUI(); 
		$("#otpCommit").attr("disabled", true);
		$('#otp_error').hide();
		console.log(data);

		var uri = '${__ctx}'+"/DIGITAL/ACCOUNT/otpverify_lo_account_aj";
		var rdata = {
				SMSOTP : data,
				CUSIDN : $("#CUSIDN").val()
			};
		
		fstop.getServerDataEx(uri, rdata, true,checkOTPFish);
	}

	//日曆
	function datetimepickerEvent(){

	    $(".BIRTHDATE").click(function(event) {
			$('#BIRTH').datetimepicker('show');
		});
		
		jQuery('.datetimepicker').datetimepicker({
			timepicker:false,
			closeOnDateSelect : true,
			scrollMonth : false,
			scrollInput : false,
		 	format:'Y/m/d',
		 	lang: '${transfer}'
		});
	}
	</script>
	<style>
	
		@media screen and (max-width:767px) {
		.ttb-button {
				width: 38%;
				left: 0px;
		}
		#CMBACK.ttb-button{
				border-color: transparent;
				box-shadow: none;
				color: #333;
				background-color: #fff;
		}
 

	}
</style>
</head>

<body>
	<div id="obj"></div>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component_u2.jsp"%>
	<%@ include file="../component/otp_component.jsp"%>
	
	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--數位存款帳戶 -->
			<li class="ttb-breadcrumb-item"><spring:message code= "LB.X0348" /></a></li>
			<!--修改數位存款帳戶資料 -->
			<li class="ttb-breadcrumb-item"><a href="#">補上傳身分證件</a></li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--基金線上預約開戶作業 -->
					補上傳身分證件
				</h2>
				<div id="step-bar">
                    <ul>
                        <li class="active">身分驗證</li>
                        <li class="">上傳證件</li>
                        <li class="">確認資料</li>
                        <li class="">完成補件</li>
                    </ul>
                </div>
				<form method="post" id="formId" name="formId">
					<input type="hidden" name="ADOPID" value="N204">
					<input type="hidden" id="NOTIEINSTALL" name="NOTIEINSTALL" value="">
					<input type="hidden" id="CUSIDN" name="CUSIDN" value="${ result_data.data.CUSIDN }">
					<input type="hidden" id="ACNTYPE" name="ACNTYPE" value="${ result_data.data.ACNTYPE }">
					<input type="hidden" id="NAME" name="NAME" value="${ result_data.data.NAME }">
					<input type="hidden" id="BRHCOD" name="BRHCOD" value="${ result_data.data.BRHCOD }">
					<input type="hidden" id="BRHNAME" name="BRHNAME" value="${ result_data.data.BRHNAME }">
					<input type="hidden" id="AUTHCODE" name="AUTHCODE" value="">
					<input type="hidden" id="AUTHCODE1" name="AUTHCODE1" value="">
					<input type="hidden" id="CertFinger" name="CertFinger" value="">
					<input type="hidden" id="CertB64" name="CertB64" value="">
					<input type="hidden" id="CertSerial" name="CertSerial" value="">
					<input type="hidden" id="CertSubject" name="CertSubject" value="">
					<input type="hidden" id="CertIssuer" name="CertIssuer" value="">
					<input type="hidden" id="CertNotBefore" name="CertNotBefore" value="">
					<input type="hidden" id="CertNotAfter" name="CertNotAfter" value="">
					<input type="hidden" id="HiCertType" name="HiCertType" value="">
					<input type="hidden" id="CUSIDN4" name="CUSIDN4" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					<input type="hidden" id="jsondc" name="jsondc" value='${ result_data.data.jsondc }'>	
					<input type="hidden" id="CHIP_ACN" name="CHIP_ACN" value="">
					<input type="hidden" id="UID" name="UID" value="${ result_data.data.CUSIDN }">
					<input type="hidden" id="ACN" name="ACN" value="">
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="OUTACN" name="OUTACN" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="TYPE" name="TYPE" value="11">
					<input type="hidden" id="back" name="back" value="">
					<input type="hidden" id="ATTIBK" name="ATTIBK" value="004">
					<input type="hidden" name="OTPTOKEN" id="OTPTOKEN" value="">
					
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
							
								<div class="ttb-message">
                                <p>身份驗證</p>
                            	</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Transaction_security_mechanism" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
	<!-- 									<div class="ttb-input"> -->
	<!-- 										<label> -->
	<%-- 								       		<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked /><spring:message code="LB.SSL_password" /> --%>
	<!-- 								       	</label> -->
	<!-- 										<input type="password" name = "CMPASSWORD" id="CMPASSWORD"  maxlength="8" class="text-input validate[required, pwvd[PW, CMPASSWORD], custom[onlyLetterNumber]]"> -->
	<!-- 										<input type="hidden" name = "PINNEW" id="PINNEW" value=""> -->
	<!-- 									</div> -->
	<!-- 									使用者是否可以使用IKEY -->
										<!-- 使用者是否可以使用IKEY -->
<%-- 										<c:if test = "${sessionScope.isikeyuser}"> --%>
											<!-- 即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森)-->
<%-- 											<c:if test = "${!result_data.TransferType.equals('NPD')}"> --%>
									
												<!-- IKEY -->
<!-- 												<div class="ttb-input"> -->
<!-- 													<label class="radio-block"> -->
<%-- 														<spring:message code="LB.Electronic_signature" /> --%>
<!-- 														<input type="radio" name="FGTXWAY" id="CMIKEY" value="1"> -->
<!-- 														<span class="ttb-radio"></span> -->
<!-- 													</label> -->
<!-- 												</div> -->
												
<%-- 											</c:if> --%>
<%-- 										</c:if> --%>
										
										<c:if test="${ result_data.data.ACNTYPE.equals('A1') }">
											<!-- 自然人憑證 -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.D0437" />
													
												 	<input type="radio" name="FGTXWAY" id="CMNPC" value="4" checked="checked">
														
													<span class="ttb-radio"></span>
												</label>
											</div>
										</c:if>
										<c:if test="${ result_data.data.ACNTYPE.equals('A2') }">
											<!-- 晶片金融卡 -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Financial_debit_card" />
													
												 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
														
													<span class="ttb-radio"></span>
												</label>
											</div>
										</c:if>
										<c:if test="${ result_data.data.ACNTYPE.equals('A3') }">
											<!-- 跨行金融帳戶認證 -->
											<div class="ttb-input">
												<label class="radio-block" for="CMFISCACC" onclick=""> 
													<spring:message code="LB.Fisc_inter-bank_acc" /> 
													<input type="radio" name="FGTXWAY" id="CMFISCACC" value="5" checked="checked"> 
													<span class="ttb-radio"></span>
												</label>
											</div>
										</c:if>	
										<c:if test="${ result_data.data.ACNTYPE.equals('A4') }">
											<!-- 本行金融帳戶認證 -->
											<div class="ttb-input">
												<label class="radio-block" for="LOISCACC" onclick=""> 
													自行帳戶+手機簡訊驗證
													<input type="radio" name="FGTXWAY" id="LOISCACC" value="6" checked="checked"> 
													<span class="ttb-radio"></span>
												</label>
											</div>																						
										</c:if>			
									</span>
								</div>
							</div>
							
							<c:if test="${ result_data.data.ACNTYPE.equals('A1') }">
								<div class="ttb-input-block">
									<!-- 自然人憑證PIN碼 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.D1540" /></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<input type="PASSWORD" class="text-input" maxLength="8" size="10" id="NPCPIN" name="NPCPIN" value=""/>
											</div>
										</span>
									</div>
								</div>
							</c:if>
							
							<c:if test="${ result_data.data.ACNTYPE.equals('A4') }">
								<div class="ttb-input-block">
									<!-- 自然人憑證PIN碼 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>存款帳號</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<input type="number"  inputmode="numeric" class="text-input validate[required, maxSize[11]]" 
													onkeyup="maxSizeCode()" name="LOATTIAC" id="LOATTIAC" value="" 
													placeholder="<spring:message code="LB.B0018" />" size="11" maxlength="11"  autocomplete="off"> 
											</div>
										</span>
									</div>
								</div>
							</c:if>
							<c:if test="${ result_data.data.ACNTYPE.equals('A3') }">
								<div class="ttb-input-block">
									<!-- 跨行金融帳戶認證 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.W1038" /></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<select id="DPBHNO" name="DPBHNO" class="custom-select select-input half-input"></select>
											</div>
											<div class="ttb-input">
												<input type="text"  class="text-input" name="ATTIAC" id="ATTIAC" value="" placeholder="<spring:message code="LB.Enter_Account" />" size="16" maxlength="16"  autocomplete="off"> 
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.D0069" /></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<input type="text" class="text-input validate[funcCallRequired[validate_cellPhone[手機號碼格式錯誤,ATMOBI]]]" maxLength="10" size="11" id="ATMOBI" name="ATMOBI" value="" />
												<span class="input-remarks">請輸入留存於他行的手機號碼</span>
											</div>
										</span>
									</div>
									<!--出生年月日-->
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
		                                    <label>
		                                        <h4><spring:message code= "LB.D0054" /></h4>
		                                    </label>
		                                </span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
												  <input type="text" class="text-input datetimepicker validate[funcCall[validate_CheckDate[<spring:message code= "LB.D0054" />,BIRTH,null,null]]]" id="BIRTH" name="BIRTH" value="" >
		                                          <span class="input-unit BIRTHDATE"><img src="${__ctx}/img/icon-7.svg"/></span>
		                                    </div>
		                                </span>
		                            </div>
								</div>
							</c:if>
							
							<!-- 驗證碼 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Captcha" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											
											<input type="text" class="text-input input-width-125" id="capCode" name="capCode" placeholder="請輸入驗證碼">
											<img name="kaptchaImage" src=""  class="verification-img"/>
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value=" <spring:message code= "LB.Regeneration_1" />" />
											<!-- 英文不分大小寫，限半型字 -->
											&nbsp;<span class="input-remarks">請注意： <spring:message code= "LB.D0033" /> </span>
										</div>
									</span>
								</div>
							</div>
							<!-- 確定 -->
							<input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318" />" onClick="back();" />
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080" />" class="ttb-button btn-flat-orange"/>
						</div>
					</div>
						
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>