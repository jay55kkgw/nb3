<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<style>
.spbutton {
    height: 25px;
    margin-top: 10px;
    margin-left: 3px;
    padding: 0 7px;
    font-size: .875rem;
    border: none;
    border-radius: 12.5px;
    color: #fff;
}
</style>
 <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- Email設定     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Email_Setting" /></li>
    <!-- 我的通訊錄     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.My_Address_Book" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	
	<main class="col-12"> 
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.My_Address_Book" /><!-- 我的通訊錄 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" action="${__ctx}/PERSONAL/SERVING/ADDRESSBOOK" method="post" autocomplete="off">
			<div class="main-content-block row">
				<div class="col-12">
				<div class="ttb-input-block">
					<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.System_time" /></h4></label></span> <!-- 系統時間 -->
								<span class="input-block">
									<div class="ttb-input">
										<p>${addbkInfo.data.CMQTIME}</p>
										<input type="hidden" name="SYSTIME" value="${addbkInfo.data.CMQTIME}" readonly="readonly"/>
									</div>
								</span>
					</div>
					<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.Favorite_name" /></h4></label></span> <!-- 好記名稱 -->
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" id="DPGONAME" name="DPGONAME" maxLength="15" class="text-input validate[required,custom[notSPCha]]" autocomplete="off">
									</div>
								</span>
							</div>
					<div class="ttb-input-item row">
						<span class="input-title"> <label><h4><spring:message code="LB.Mail_address" /></h4></label></span> <!-- 電子郵箱 -->
							<span class="input-block">
								<div class="ttb-input">
								<input type="text" id="DPABMAIL" name="DPABMAIL" class="text-input validate[required,custom[email]]" autocomplete="off">
							</div>
						</span>
					</div>
					<div class="text-center">
					<input id="CMSUBMIT"  type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.Confirm" />"/>
					<input type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.Re_enter" />" onclick="empty();"/>
					</div>
				<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
					<thead>
					<tr>
						<th data-title='<spring:message code="LB.Serial_number"/>' data-breakpoints="xs sm">
							<spring:message code="LB.Serial_number" /><!-- 序號 -->
						</th>
						<th data-title='<spring:message code="LB.Favorite_name"/>'>
							<spring:message code="LB.Favorite_name" /><!-- 好記名稱 -->
						</th>
						<th data-title='<spring:message code="LB.Mail_address"/>'>
							<spring:message code="LB.Mail_address" /><!-- 電子郵箱 -->
						</th>
						<th data-title=''>
						
						
						</tr>
					</thead>
					<tbody <c:if test="${addbkInfo.data.ADDRESSBOOKSIZE ='0'}">style="display:none"</c:if> >
					<c:forEach var="dataList" items="${addbkInfo.data.ADDRESSBOOK}" varStatus="order">
					<tr>
						<td class="text-center">${order.count}</td>
						<td class="text-center" id='name${order.count}'>${dataList.DPGONAME}</td>
						<td class="text-center" id='mail${order.count}'>${dataList.DPABMAIL}</td>
						<td class="text-center" id='btn${order.count}'>
						<!-- 修改 -->
						<input  type="button" class="spbutton btn-flat-orange" value="<spring:message code= "LB.X0432" />" onclick="chgdata('UPDATE','${order.count}','${dataList.DPADDBKID}');"/>
						<!-- 刪除 -->
						<input  type="button" class="spbutton btn-flat-orange" value="<spring:message code= "LB.X0433" />" onclick="chgdata('DELETE','${order.count}','${dataList.DPADDBKID}');"/></td>
					</tr>
					</c:forEach>
					
					</tbody>
				</table>
				
				</div>
			</div>	
			</div>
			<input type="hidden" id="DPADDBKID" name="DPADDBKID"> 
<%-- 			<input type="hidden" id="DPUSERID" name="DPUSERID" value="${addbkInfo.data.DPUSERID}"> --%>
			<input type="hidden" id="EXECUTEFUNCTION" name="EXECUTEFUNCTION" value="INSERT">
			</form>
		</section>
	</main> 
		<!-- 		main-content END -->
	</div>
		<!-- 	content row END -->
	
	<script type="text/javascript">
	
		$(document).ready(function() {
			//initFootable();
			setTimeout("initDataTable()",100);
			//表單驗證
			$("#formId").validationEngine({
				binded : false,
				promptPosition : "inline"
			});
			//submit
			$("#CMSUBMIT").click(function(e) {
				console.log("submit~~");
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI();
					$('#formId').submit();
				}
			});

		});
		var confirmType=0;
		$("#errorBtn1").click(function(){
			if(confirmType == 0){
				return false;
			}
			if(confirmType == 1){
				$('#CMSUBMIT').click();
			}
			confirmType=0;
		});
		$("#errorBtn2").click(function(){
			if(confirmType == 0){
				return false;
			}
			$('#error-block').hide();
			confirmType=0;
		});
		function chgdata(strAct, number,DPADDBKID) {
			if (strAct == 'DELETE') {
				
				$('#EXECUTEFUNCTION').val(strAct);
				$('#DPADDBKID').val(DPADDBKID);
				$('#DPABMAIL').val($("#mail" + number + "").text());
				$('#DPGONAME').val($("#name" + number + "").text());
				$("#DPABMAIL").removeClass("validate[required,custom[email]]");
				$("#DPGONAME").removeClass("validate[required,,custom[notSPCha]]");
// 				if(confirm("<spring:message code= "LB.Confirm029" />")){
// 					$('#CMSUBMIT').click();
// 				}else{
// 					//do nothing
// 				}
				confirmType = 1;
				errorBlock(
							null, 
							null,
							[ "<spring:message code= "LB.Confirm029" />"], 
							'<spring:message code= "LB.Confirm" />', 
							'<spring:message code= "LB.Cancel" />'
						);
			} else if (strAct == 'UPDATE') {
				$('#CMSUBMIT').val('<spring:message code= "LB.X1384" />');
				$('#EXECUTEFUNCTION').val(strAct);
				$('#DPADDBKID').val(DPADDBKID);
				$('#DPABMAIL').val($("#mail" + number + "").text());
				$('#DPGONAME').val($("#name" + number + "").text());
					

			} 
		}
		
		function empty(){
			$('#DPADDBKID').val("");
			$('#DPABMAIL').val("");
			$('#DPGONAME').val("");
			$('#EXECUTEFUNCTION').val("INSERT");
			$('#CMSUBMIT').val('<spring:message code="LB.Confirm" />');
		}
		
	</script>
	
	<%@ include file="../index/footer.jsp"%>
</body>
</html>