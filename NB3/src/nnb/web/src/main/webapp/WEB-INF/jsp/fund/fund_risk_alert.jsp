<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
});
function process(flag){
	var UID = "${UID}";
	var TXID = "${TXID}";
	
	if(flag == "Y"){
		if(UID.length == 10){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_invest_attr1?TXID=${TXID}");
		}
		else if(UID.length < 10){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_invest_attr3?TXID=${TXID}");
		}
	}
	else{
		if(TXID == "C021" || TXID == "C032"){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/query_fund_transfer_data");
		}
		else if(TXID == "C016"){
			$("#formID").attr("action","${__ctx}/FUND/PURCHASE/fund_purchase_select");
		}
		else if(TXID == "C017"){
			$("#formID").attr("action","${__ctx}/FUND/REGULAR/fund_regular_select");
		}
		else if(TXID == "C031"){
			$("#formID").attr("action","${__ctx}/FUND/RESERVE/PURCHASE/fund_reserve_purchase_select");
		}
		else if(TXID == "B019"){
			$("#formID").attr("action","${__ctx}/BOND/PURCHASE/bond_purchase_input");
		}
	}
	$("#formID").submit();
}
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!-- 基金交易 -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
			<!-- 投資屬性評估調查表 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0855"/></li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
					<form id="formID" method="post">
					<div class="main-content-block row">
						<div class="col-12">
						
									<!--內容-->
									<c:if test="${ALERTTYPE == '1'}">
										<div class="ttb-message">
										<p>
										<b><spring:message code= "LB.X1559" />：<br/>
											<spring:message code= "LB.X1560" />
										</b>
										</p>
	                                	</div>
									</c:if>
									<c:if test="${ALERTTYPE == '2'}">
									<div class="ttb-message">
										<p>
										<b><spring:message code= "LB.X1559" />：<br/>
											<spring:message code= "LB.X1561" />
										</b>
										</p>
									</div>
									</c:if>
									<c:if test="${ALERTTYPE == '3'}">
										<div class="ttb-message">
										<p>
										<b>
											<spring:message code= "LB.Remind_you" />，<font color="red"><b><spring:message code="LB.X1511" /></b></font>，<spring:message code="LB.X1512" /><spring:message code="LB.X1513" />」，<font color="red"><b><spring:message code= "LB.X1563" /></b></font><spring:message code="LB.X1517" />
										</b>
										</p>
	                                	</div>
									</c:if>

							<c:if test="${ALERTTYPE == '1'}">
	                  			<input type="button" id="reValueButton" value="<spring:message code= "LB.X1564" />" onclick="process('Y')" class="ttb-button btn-flat-orange"/>
	                			<input type="button" id="cancelButton" value="<spring:message code= "LB.X1565" />" onclick="process('N')" class="ttb-button btn-flat-orange"/>	
							</c:if>
							<c:if test="${ALERTTYPE == '2'}">
	                  			<input type="button" id="reValueButton" value="<spring:message code= "LB.X1564" />" onclick="process('Y')" class="ttb-button btn-flat-orange"/>
							</c:if>
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>