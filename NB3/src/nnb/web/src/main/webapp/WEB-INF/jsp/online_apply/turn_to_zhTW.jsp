<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後0.1秒開始遮罩
		setTimeout("initBlockUI()", 100);
		
		init();
		// 過0.5秒解遮罩
		setTimeout("unBlockUI(initBlockId)", 800);
	});

    function init(){
    	$("#CMSUBMIT").click(function(e){
			console.log("submit~~");
		    $("form").attr("action", '${__ctx}/${rev_url}?locale=zh_TW');
            $("#formId").submit();
		});
		$("#CMBACK").click( function(e) {
			console.log("submit~~");
			$("#formId").attr("action", "${__ctx}/${back_url}");
            $("#formId").submit();
		});
    }	
	</script>
</head>

<body>
 	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
		</ol>
	</nav>
	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code= "LB.X2172" />
				</h2>
				<form method="post" id="formId" name="formId">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
	                                <p><spring:message code= "LB.X2172" /></p>
	                            </div>
                            </div>
                            <c:if test="${ not empty back_url}">
								<input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code= "LB.X1962" />" />
							</c:if>
                            <c:if test="${ not empty rev_url}">
								<input type="button" id="CMSUBMIT" value="<spring:message code= "LB.Confirm" />" class="ttb-button btn-flat-orange"/>
							</c:if>
							<!-- 確定 -->
							<!-- 上一步 -->
							<!-- 下一步 -->
						</div>
					</div>				
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>