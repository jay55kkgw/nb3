<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 外幣資產總額 -->
<div class="account-left tab-pane fade" id="FCY" role="tabpanel" aria-labelledby="tab-FCY">
    <div class="main-account">
        <span class="title"><spring:message code='LB.X2319'/></span>
<!--         <span id="assets_fx" class="content"><i class="fas fa-eye"></i></span> -->
        <span class="content" onclick='showEyeClick()'><img src='${__ctx}/img/eye-solid-orange.svg' /></span>
    </div>
    <div id="balance_money_fx" class="account-item">
        <div class="account-item-show">
            <span><spring:message code='LB.X2320'/></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/FCY/ACCT/f_balance_query','', '')" id="balance_account_fx"></a></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/FCY/TRANSFER/f_transfer','', '')"><spring:message code='LB.X2322'/></a></span>
        </div>
    </div>
    <div class="account-item" id="deposit_money_fx">
    	<div class="account-item-show">
            <span><spring:message code='LB.X2321'/></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/FCY/ACCT/f_time_deposit_details','', '')" id="deposit_account_fx"></a></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/FCY/ACCT/TDEPOSIT/f_deposit_transfer','', '')"><spring:message code='LB.X2307'/></a></span>
        </div>
    </div>
</div>

 
<script type="text/JavaScript">

function getFX_aj() {
	// 先清空資料初始化
	$("#balance_money_fx div").remove(".account-item-hide");
	$("#deposit_money_fx div").remove(".account-item-hide");
	$("#balance_account_fx").empty();
	$("#deposit_account_fx").empty();
	
	console.log("getFX_aj.now: " + new Date());
	
	uri = '${__ctx}' + "/INDEX/summary_FX";

	fstop.getServerDataEx(uri, null, true, showFX);
}

function showFX(data) {
	// 避免打很多次回來同時顯示多次的資料，故再次清空資料
	$("#balance_money_fx div").remove(".account-item-hide");
	$("#deposit_money_fx div").remove(".account-item-hide");
	$("#balance_account_fx").empty();
	$("#deposit_account_fx").empty();
	
	console.log("showFX.now: " + new Date());
	// --------------------------------------------------- 外幣活存 --------------------------------------------------------
// 	console.log("showFX.summary_FX_balance: " + data.data.summary_FX_balance);
	if (data.data.summary_FX_balance != 'error' && data.data.summary_FX_balance != 'null' && data.data.summary_FX_balance != null) {
		// 外幣活存
		var summary_FX_balance =  JSON.parse(data.data.summary_FX_balance);
		var avasum =  summary_FX_balance.AVASum;
		// 資料總數
		var balance_account_fx = parseInt("0");
		var ancinfo =  summary_FX_balance.ACNInfo;
		
		console.log("showFX.avasum: " + avasum);
	    var trHTML  = '';
	    $.each(avasum, function (i, item) {
	    		// 資料總數
	//     		balance_account_fx += parseInt(item.COUNT); 
	    		// 總計金額  
	//     		 + item  
	            trHTML += '<div class="account-item-hide">' +
	            		  	'<span>'+ item.CUID + ' ' + item.CUID_NAME +'</span>' +
	            		  	'<span class="accountST" account="' + item.AVAILABLE + '">'+ coverSoA(item.AVAILABLE) +'</span>' +
	            		  	'<span></span>' +
	            		  '</div>'
	    });
	    
	//     balance_account_fx = Array.from(new Set(anclist)).length; // IE不支援
	//     balance_account_fx = new Set(anclist).size; // IE new Set有問題
	
		// 20201202正式套有問題--i、ar都undefined
// 	    const anclist = [];
// 	    $.each(ancinfo, function (i, item) {
// 	    	anclist.push(item.ACN);
// 	    });
// 		var unique = anclist.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
// 		balance_account_fx = unique.length;
		
	    var anclist_fx = [];
	    $.each(ancinfo, function (i, item) {
	    	// IE不支援includes
// 	    	if (!anclist_fx.includes(item.ACN)) {
	    	if (anclist_fx.indexOf(item.ACN) == -1) {
		    	anclist_fx.push(item.ACN);
	    	}
	    });
	    console.log("anclist_fx: " + anclist_fx);
	    
	    balance_account_fx = anclist_fx.length;
	    console.log("balance_account_fx: " + balance_account_fx);
	    
	    console.log("showFX.trHTML: " + trHTML);
	    $('#balance_money_fx').append(trHTML);
	    balance_account_fx == 1 ? $("#balance_account_fx").prepend(anclist_fx[0]) : $("#balance_account_fx").prepend("<spring:message code='LB.X2308'/>&nbsp;<spring:message code='LB.D0360_1'/> " + balance_account_fx + " <spring:message code='LB.X2310'/>");
	
	} else {
		console.log("showFX_balance.error...");
		$("#balance_account_fx").prepend("<spring:message code='LB.D0017'/>");
	}
	
// --------------------------------------------------- 外幣定存 --------------------------------------------------------
// 	console.log("showFX.summary_FX_deposit: " + data.data.summary_FX_deposit);
	if (data.data.summary_FX_deposit != 'error' && data.data.summary_FX_deposit != 'null' && data.data.summary_FX_deposit != null) {
		var summary_FX_deposit =  JSON.parse(data.data.summary_FX_deposit);
		// 資料總數
		var cmrecnum = summary_FX_deposit.CMRECNUM;
		// 總計金額 
		var fcytotmat = summary_FX_deposit.FCYTOTMAT;
		var fcytotmatH = '';
		  $.each(fcytotmat, function (i, item) {
			  var litem = item.split("/");
			  fcytotmatH += '<div class="account-item-hide">' +
			  					'<span>'+ litem[0] + " " + litem[1] +'</span>' +
			  					'<span class="accountST" account="' + litem[2] + '">'+ coverSoA(litem[2]) +'</span>' +
			  					'<span></span>' +
			  				'</div>'
	  	});
	// 	anclist = [];
	// 	var rec =  summary_FX_deposit.REC;
	// 	$.each(rec, function (i, item) {
	// 		anclist.push(item.ACN);
	// 	});
	// 	cmrecnum = Array.from(new Set(anclist)).length;
		console.log("showFX.fcytotmatH: " + fcytotmatH);
		// 外幣定存
		$("#deposit_money_fx").append(fcytotmatH);
		//資料總數
		cmrecnum == 1 ? $("#deposit_account_fx").prepend(summary_FX_deposit.REC[0].ACN) : $("#deposit_account_fx").prepend("<spring:message code='LB.X2309'/>&nbsp;<spring:message code='LB.D0360_1'/> " + cmrecnum + " <spring:message code='LB.Rows'/>");
		
	} else {
		console.log("showFX_deposit.error...");
		$("#deposit_account_fx").prepend("<spring:message code='LB.D0017'/>");
	}

	// 解遮罩
	unAcctsBlock('FCY');
}

</script>
