<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 我的資產 -->
<div class="col-12 p-lg-0">
	<p class="home-title"><spring:message code='LB.My_assets' /></p>
	<div id="nav-tabContent" class="main-content-block tab-content pie-tab-content">
		<!-- 資產 -->
		<div id="pie-1" class="row tab-pane fade show active">
			<div id="NTD_Demand" class="col-12 col-lg-5 col-md-5">
				<svg id="g1">
		            <defs>
		                <linearGradient id="grada1" x1="0%" y1="0%" x2="100%" y2="0%">
		                    <stop offset="0%" style="stop-color:#ffb9bb;stop-opacity:1" />
		                    <stop offset="100%" style="stop-color:#ff4747;stop-opacity:1" />
		                </linearGradient>
		                <linearGradient id="grada2" x1="0%" y1="0%" x2="100%" y2="0%">
		                    <stop offset="0%" style="stop-color:#9fe4d3;stop-opacity:1" />
		                    <stop offset="100%" style="stop-color:#4bafba;stop-opacity:1" />
		                </linearGradient>
		                <linearGradient id="grada3" x1="0%" y1="0%" x2="100%" y2="0%">
		                    <stop offset="0%" style="stop-color:#fffa7f;stop-opacity:1" />
		                    <stop offset="100%" style="stop-color:#ff8e00;stop-opacity:1" />
		                </linearGradient>
		                <linearGradient id="grada0" x1="0%" y1="0%" x2="100%" y2="0%">
							<stop offset="0%" style="stop-color:#f2f2f2;stop-opacity:1" />
							<stop offset="100%" style="stop-color:#f2f2f2;stop-opacity:1" />
						</linearGradient>
		            </defs>
		  	        <g transform="translate(150 , 125)"></g>
		        </svg>
<!-- 				<div id="flotcontainer" style="height: 300px; width: 100%;"></div> -->
			</div>
			<div class="col-12 col-lg-7 col-md-7 pie-chart-table-block">
				<table class="pie-chart-table">
					<thead>
						<tr class="text-center">
							<th><spring:message code='LB.D0973' /></th><!-- 類別 -->
							<th><spring:message code='LB.X2186' /></th><!-- 比例 -->
							<th><spring:message code='LB.X0408' /> NTD</th><!-- 金額 -->
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><spring:message code='LB.X2161' /></td>
							<td id="asset_tw" data-chart-per="0%" class="text-center"></td>
							<td id="asset_tw_val" ></td>
						</tr>
						<tr>
							<td><spring:message code='LB.X2162' /></td>
							<td id="asset_fx" data-chart-per="0%" class="text-center"></td>
							<td id="asset_fx_val" ></td>
						</tr>
						<tr>
							<td><spring:message code='LB.X2163' /></td>
							<td id="asset_gd" data-chart-per="0%" class="text-center"></td>
							<td id="asset_gd_val" ></td>
						</tr>
					</tbody>
				</table>
				<div class="d-flex">
					<span><spring:message code='LB.X2233' /></span>
					<button data-toggle="modal" data-target="#pie-chart-explanation1" role="button">
						<span>
							<spring:message code='LB.X2232' />
							<img src="${__ctx}/img/question-circle-solid.svg" />
						</span>
					</button>
				</div>
			</div>
		</div>
		
		<!-- 負債 -->
		<div id="pie-2" class="row tab-pane fade">
			<div id="NTD_Demand" class="col-12 col-lg-5 col-md-5">
				<svg id="g2">
		            <defs>
		               <linearGradient id="gradb1" x1="0%" y1="0%" x2="100%" y2="0%">
		                    <stop offset="0%" style="stop-color:#ffb9bb;stop-opacity:1" />
		                    <stop offset="100%" style="stop-color:#ff4747;stop-opacity:1" />
		                </linearGradient>
		                <linearGradient id="gradb2" x1="0%" y1="0%" x2="100%" y2="0%">
		                    <stop offset="0%" style="stop-color:#9fe4d3;stop-opacity:1" />
		                    <stop offset="100%" style="stop-color:#4bafba;stop-opacity:1" />
		                </linearGradient>
		                <linearGradient id="gradb3" x1="0%" y1="0%" x2="100%" y2="0%">
		                    <stop offset="0%" style="stop-color:#fffa7f;stop-opacity:1" />
		                    <stop offset="100%" style="stop-color:#ff8e00;stop-opacity:1" />
		                </linearGradient>
		                <linearGradient id="gradb0" x1="0%" y1="0%" x2="100%" y2="0%">
							<stop offset="0%" style="stop-color:#f2f2f2;stop-opacity:1" />
							<stop offset="100%" style="stop-color:#f2f2f2;stop-opacity:1" />
						</linearGradient>
		            </defs>
		  	        <g transform="translate(150 , 125)"></g>
		        </svg>
<!-- 				<div id="flotcontainer-2" style="height: 300px; width: 100%;"></div> -->
			</div>
			<div class="col-12 col-lg-7 col-md-7 pie-chart-table-block">
				<table class="pie-chart-table">
					<thead>
						<tr class="text-center">
							<th><spring:message code='LB.D0973' /></th><!-- 類別 -->
							<th><spring:message code='LB.X2186' /></th><!-- 比例 -->
							<th><spring:message code='LB.D0453' /></th><!-- 金額 -->
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><spring:message code='LB.X2164' /></td>
							<td id="liability_tw" data-chart-per="0%" class="text-center"></td>
							<td id="liability_tw_val" ></td>
						</tr>
						<tr>
							<td><spring:message code='LB.X2165' /></td>
							<td id="liability_fx" data-chart-per="0%" class="text-center"></td>
							<td id="liability_fx_val" ></td>
						</tr>
						<tr>
							<td><spring:message code='LB.X2166' /></td>
							<td id="liability_cc" data-chart-per="0%" class="text-center"></td>
							<td id="liability_cc_val" ></td>
						</tr>
					</tbody>
				</table>
				<div class="d-flex">
					<span><spring:message code='LB.X2233' /></span>
					<button data-toggle="modal" data-target="#pie-chart-explanation2" role="button">
						<span>
							<spring:message code='LB.X2232' />
							<img src="${__ctx}/img/question-circle-solid.svg" />
						</span>
					</button>
				</div>
			</div>
		</div>
		
		<!-- 資產/負債 切換按鈕 -->
		<nav class="nav card-select-block text-center d-flex col-12 col-lg-5 col-md-5" role="tablist">
			<input type="button" class="nav-item ttb-sm-btn active" id="pie2-tab-1" data-toggle="tab" href="#pie-1" role="tab" aria-selected="false" value="<spring:message code='LB.X2167' />" />
			<input type="button" class="nav-item ttb-sm-btn" id="pie2-tab-2" data-toggle="tab" href="#pie-2" role="tab" aria-selected="true" value="<spring:message code='LB.X2168' />" />
		</nav>
		
		<input type="button" class="ttb-button btn-flat-orange" id="ACCTOVERVIEW" value="<spring:message code="LB.Account_Overview" />"
			onclick="fstop.getPage('${pageContext.request.contextPath}'+'/OVERVIEW/initview','', '')"/>
	</div>
</div>

<!-- pie-chart-explanation -->
<section id="pie-chart-explanation1" class="modal fade more-info-block" role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<p class="ttb-pup-header"><spring:message code='LB.X2181'/></p>
			</div>
			<div class="modal-body w-auto m-0 pl-5 pr-5">
				<p><spring:message code='LB.X2167'/></p>
				<p>
					<ol class="list-decimal">
						<li><spring:message code='LB.X2182'/></li>
						<li><spring:message code='LB.X2183'/></li>
						<li><spring:message code='LB.X2184'/></li>
					</ol>
				</p>
			</div>
			<div class="modal-footer ttb-pup-footer">
				<input type="submit" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="<spring:message code='LB.X1572'/>" />
			</div>
		</div>
	</div>
</section>

<!-- pie-chart-explanation -->
<section id="pie-chart-explanation2" class="modal fade more-info-block" role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<p class="ttb-pup-header"><spring:message code='LB.X2181'/></p>
			</div>
			<div class="modal-body w-auto m-0 pl-5 pr-5">
				<p><spring:message code='LB.X2168'/></p>
				<p>
					<ol class="list-decimal">
						<li><spring:message code='LB.X2182'/></li>
						<li><spring:message code='LB.X2185'/></li>
					</ol>
				</p>
			</div>
			<div class="modal-footer ttb-pup-footer">
				<input type="submit" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="<spring:message code='LB.X1572'/>" />
			</div>
		</div>
	</div>
</section>

<script type="text/JavaScript">

// 取得資產負債圖資料
function getAssets(){
	console.log("getAssets.now: " + new Date());
	
	uri = '${__ctx}'+"/INDEX/assets_aj";
// 	console.log("assets_uri: " + uri);
	fstop.getServerDataEx(uri, null, true, showAssets);
}

// 顯示資產負債圖
function showAssets(data){
	console.log("showAssets.now: " + new Date());
// 	console.log("showAssets.data: " + JSON.stringify(data));

	if(data && data.result){
		
		// 資產 & 負債
		var asset_Sv_Bal_Amt = data.data.Sv_Bal_Amt!=null ? data.data.Sv_Bal_Amt : 0; // 臺幣存款餘額
		var asset_Syv_Bal_Amt = data.data.Syv_Bal_Amt!=null ? data.data.Syv_Bal_Amt : 0; // 外幣存款餘額
		var asset_Gd_Bal_Amt = data.data.Gd_Bal_Amt!=null ? data.data.Gd_Bal_Amt : 0; // 黃金存摺餘額
		var liability_Ln_Bal_Amt = data.data.Ln_Bal_Amt!=null ? data.data.Ln_Bal_Amt : 0; // 臺幣放款餘額
		var liability_Fl_Bal_Amt = data.data.Fl_Bal_Amt!=null ? data.data.Fl_Bal_Amt : 0; // 外幣放款餘額
		var liability_Ic_Bal_Amt = data.data.Ic_Bal_Amt!=null ? data.data.Ic_Bal_Amt : 0; // 信用卡餘額
		
		var asset_default = null;
		// 資產皆為0
		if(	data.data.Sv_Bal_Amt!=null && asset_Sv_Bal_Amt==0
				&& data.data.Syv_Bal_Amt!=null && asset_Syv_Bal_Amt==0
				&& data.data.Gd_Bal_Amt!=null && asset_Gd_Bal_Amt==0
		) {
			asset_default = 1; // 100%
		}

		var liability_default = null;
		// 資產皆為0
		if(	data.data.Ln_Bal_Amt!=null && liability_Ln_Bal_Amt==0
				&& data.data.Fl_Bal_Amt!=null && liability_Fl_Bal_Amt==0
				&& data.data.Ic_Bal_Amt!=null && liability_Ic_Bal_Amt==0
		) {
			liability_default = 1; // 100%
		}
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	    var data1=[{ type:"asset_tw", value:parseInt(asset_Sv_Bal_Amt) }, 
	        { type:"asset_fx", value:parseInt(asset_Syv_Bal_Amt) },
	        { type:"asset_gd", value:parseInt(asset_Gd_Bal_Amt) } ]; 
	        
	    var colors = ["#ffa500", "#00ffff", "#e06555", "#00ff9d"];
	    
        // Create an arc generator with configuration
        var arcGenerator = d3.arc();

		var sum=0;
		for ( var i=0; i<data1.length; i++ ) {
			sum += data1[i].value;
		}
		var startAngle=0;
		var endAngle=0;
		var inner;
		var outer;
		var fill;
		for ( var i=0; i<data1.length; i++ ) {
			startAngle=endAngle;
			endAngle += (data1[i].value*2*Math.PI)/sum;

			var radisRage = 90;
			var radisMinRage = radisRage-(i+1)*5;
			var radisMaxRage = radisRage+(i+1)*5;
			var radis={ inner:radisMinRage, outer:radisMaxRage };
			var inner = radis.inner;
			var outer = radis.outer;
			var color = colors[i];
			var pathData = arcGenerator({
				startAngle: startAngle,
				endAngle: endAngle,
				innerRadius: inner,
				outerRadius: outer
			});
            
            fill="url(#grada"+(i+1).toString()+")";
            d3.select("#g1").selectAll("g")
                .append('path')
                .attr('d', pathData)
                .attr("stroke", "white")
                .style("fill", fill)
                .attr("stroke-width", 0);
        }
		
		// 資產都為0顯示預設值
		if(asset_default!=null) {
			var pathData0 = arcGenerator({
				startAngle: 0,
				endAngle: 2*Math.PI,
				innerRadius: 85,
				outerRadius: 95
			});
			var fill0="url(#grada0)";
            d3.select("#g1").selectAll("g")
                .append('path')
                .attr('d', pathData0)
                .attr("stroke", "white")
                .style("fill", fill0)
                .attr("stroke-width", 0);
		}
        
        var title = "NTD " + "<spring:message code='LB.NTD' />"; // NTD 新台幣

        d3.select("#g1").selectAll("g")
            .append('text')
            .text(title)
            .attr("dominant-baseline", "middle")
            .attr("text-anchor","middle")
            .attr("x", "0").attr("y", "-20")
            .style("fill", "#4bafba").style("font-size", "1.25rem")
            .style("font-family", "monospace");
        
        var summary = '$'+sum.toLocaleString();

        d3.select("#g1").selectAll("g")
            .append('text')
            .text(summary)
            .attr("dominant-baseline", "middle")
            .attr("text-anchor","middle")
            .attr("x", "0").attr("y", "20")
            .style("fill", "#4bafba").style("font-size", "1.375rem")
            .style("font-family", "monospace");
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	    var data2=[{ type:"liability_tw", value:parseInt(liability_Ln_Bal_Amt) }, 
	        { type:"liability_fx", value:parseInt(liability_Fl_Bal_Amt) },
	        { type:"liability_cc", value:parseInt(liability_Ic_Bal_Amt) } ]; 
	        
	    var colors = ["#ffa500", "#00ffff", "#e06555", "#00ff9d"];
        
        // Create an arc generator with configuration
        var arcGenerator = d3.arc();

		var sum=0;
		for ( var i=0; i<data2.length; i++ ) {
			sum += data2[i].value;
		}
		var startAngle=0;
		var endAngle=0;
		var inner;
		var outer;
		var fill;
		for ( var i=0; i<data2.length; i++ ) {
			startAngle=endAngle;
			endAngle += (data2[i].value*2*Math.PI)/sum;

			var radisRage = 90;
			var radisMinRage = radisRage-(i+1)*5;
			var radisMaxRage = radisRage+(i+1)*5;
			var radis={ inner:radisMinRage, outer:radisMaxRage };
			var inner = radis.inner;
			var outer = radis.outer;
			var color = colors[i];
			var pathData = arcGenerator({
				startAngle: startAngle,
				endAngle: endAngle,
				innerRadius: inner,
				outerRadius: outer
			});
            
            fill="url(#gradb"+(i+1).toString()+")";
            d3.select("#g2").selectAll("g")
                .append('path')
                .attr('d', pathData)
                .attr("stroke", "white")
                .style("fill", fill)
                .attr("stroke-width", 0);
        }
        
		// 負債都為0顯示預設值
		if(liability_default!=null) {
			var pathData0 = arcGenerator({
				startAngle: 0,
				endAngle: 2*Math.PI,
				innerRadius: 85,
				outerRadius: 95
			});
			var fill0="url(#gradb0)";
            d3.select("#g2").selectAll("g")
                .append('path')
                .attr('d', pathData0)
                .attr("stroke", "white")
                .style("fill", fill0)
                .attr("stroke-width", 0);
		}
		
        var title = "NTD " + "<spring:message code='LB.NTD' />"; // NTD 新台幣

        d3.select("#g2").selectAll("g")
            .append('text')
            .text(title)
            .attr("dominant-baseline", "middle")
            .attr("text-anchor","middle")
            .attr("x", "0").attr("y", "-20")
            .style("fill", "#4bafba").style("font-size", "1.25rem")
            .style("font-family", "monospace");
        
        var summary = '$'+sum.toLocaleString();

        d3.select("#g2").selectAll("g")
            .append('text')
            .text(summary)
            .attr("dominant-baseline", "middle")
            .attr("text-anchor","middle")
            .attr("x", "0").attr("y", "20")
            .style("fill", "#4bafba").style("font-size", "1.375rem")
            .style("font-family", "monospace");
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// 顯示資產金額
		$("#asset_tw_val").text(fstop.fmoney(data.data.Sv_Bal_Amt.toString(), 2));
		$("#asset_fx_val").text(fstop.fmoney(data.data.Syv_Bal_Amt.toString(), 2));
		$("#asset_gd_val").text(fstop.fmoney(data.data.Gd_Bal_Amt.toString(), 2));
		$("#liability_tw_val").text(fstop.fmoney(data.data.Ln_Bal_Amt.toString(), 2));
		$("#liability_fx_val").text(fstop.fmoney(data.data.Fl_Bal_Amt.toString(), 2));
		$("#liability_cc_val").text(fstop.fmoney(data.data.Ic_Bal_Amt.toString(), 2));
		
		// 圓餅圖js
		$(function() {
			var data = [{
				label: "",
				data: 0
			}];
		});

		// 更改圓餅圖內文字顏色
		function labelFormatter(label, series) {
			// 資產資料
// 			console.log("labelFormatter.label: "+label);
			var label_show = label;
			switch (label) {
				// 臺幣存款餘額
				case "asset_tw":
					label = "<spring:message code='LB.X2161' />";
					$("#asset_tw").attr("data-chart-per", series.percent.toFixed(2)+'%');
					$("#asset_tw").html(series.percent.toFixed(2)+'%');
					break;
					
				// 外幣存款餘額
				case "asset_fx":
					label = "<spring:message code='LB.X2162' />";
					$("#asset_fx").attr("data-chart-per", series.percent.toFixed(2)+'%');
					$("#asset_fx").html(series.percent.toFixed(2)+'%');
					break;
					
				// 黃金存摺餘額
				case "asset_gd":
					label = "<spring:message code='LB.X2163' />";
					$("#asset_gd").attr("data-chart-per", series.percent.toFixed(2)+'%');
					$("#asset_gd").html(series.percent.toFixed(2)+'%');
					break;
					
				// 臺幣放款餘額
				case "liability_tw":
					label = "<spring:message code='LB.X2164' />";
					$("#liability_tw").attr("data-chart-per", series.percent.toFixed(2)+'%');
					$("#liability_tw").html(series.percent.toFixed(2)+'%');
					break;
					
				// 外幣放款餘額
				case "liability_fx":
					label = "<spring:message code='LB.X2165' />";
					$("#liability_fx").attr("data-chart-per", series.percent.toFixed(2)+'%');
					$("#liability_fx").html(series.percent.toFixed(2)+'%');
					break;
					
				// 信用卡餘額
				case "liability_cc":
					label = "<spring:message code='LB.X2166' />";
					$("#liability_cc").attr("data-chart-per", series.percent.toFixed(2)+'%');
					$("#liability_cc").html(series.percent.toFixed(2)+'%');
					break;
					
				default:
			}
			
			return "<div style='font-size:10pt; text-align:left; padding:2px; color:black;'>" 
				+ label + "<br/>" + series.percent.toFixed(2) // 百分比數值
				+ "%</div>";
		};
		
		// 資產圓餅圖
		$(function() {
			var data = [
				{ label: "asset_tw", data: asset_Sv_Bal_Amt, color: '#FF8000' }, 
				{ label: "asset_fx", data: asset_Syv_Bal_Amt, color: '#5fbbc0' }, 
				{ label: "asset_gd", data: asset_Gd_Bal_Amt, color: '#fa5f5f' }
			];
		
			// 控制圓餅圖樣式
			var options = {
				series: {
					pie: {
						innerRadius: 0.4,
						show: true,
						shadow: {
							top: 20,
							left: 100
						},
						label: {
							formatter: labelFormatter,
						},
					}
				},
				legend: {
					show: false,
				}
			};
			$.plot($("#flotcontainer"), data, options);
		});
		
		// 負債圓餅圖
		$(function() {
			var data = [
				{ label: "liability_tw", data: liability_Ln_Bal_Amt, color: '#FF8000' },
				{ label: "liability_fx", data: liability_Fl_Bal_Amt, color: '#5fbbc0' }, 
				{ label: "liability_cc", data: liability_Ic_Bal_Amt, color: '#fa5f5f' }
			];

			//控制圓餅圖樣式
			var options = {
				series: {
					pie: {
						// 加入innerRadius 就是甜甜圈
						innerRadius: 0.4,
						show: true,
						//陰影
						shadow: {
							top: 20,
							left: 100
						},
						label: {
							formatter: labelFormatter,
						},
					}
				},
				legend: {
					show: false,
				}
			};
			
			// 先呈現負債圓餅圖再隱藏
			$('#pie-2').addClass('active');
			$.plot($("#flotcontainer-2"), data, options);
			$('#pie-2').removeClass('active');
		});
	}
	
	console.log("showAssets.end: " + new Date());
}

</script>