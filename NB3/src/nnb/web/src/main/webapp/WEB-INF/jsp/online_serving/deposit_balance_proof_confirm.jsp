<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 存款餘額證明申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0485" /></li>
		</ol>
	</nav>

	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.D0485" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>					
			<form method="post" id="formId">
				<input type="hidden" id="ACN" name="ACN" value="${deposit_balance_proof_confirm.data.ACN}">	
				<input type="hidden" id="COPY" name="COPY" value="${deposit_balance_proof_confirm.data.COPY}">	
				<input type="hidden" id="PURPOSE" name="PURPOSE" value="${deposit_balance_proof_confirm.data.PURPOSE1}">	
				<input type="hidden" id="AMT" name="AMT" value="${deposit_balance_proof_confirm.data.AMT}">	
				<input type="hidden" id="DATE" name="DATE" value="${deposit_balance_proof_confirm.data.DATE1}">
				<input type="hidden" id="INQ_TYPE" name="INQ_TYPE" value="${deposit_balance_proof_confirm.data.INQ_TYPE}">
				<input type="hidden" id="CRY" name="CRY" value="${deposit_balance_proof_confirm.data.CRY}">
				<input type="hidden" id="FORM" name="FORM" value="${deposit_balance_proof_confirm.data.VER}">
				<input type="hidden" id="OUTACN" name="OUTACN" value="${deposit_balance_proof_confirm.data.FEEACN}">	
				<input type="hidden" id="FEAMT" name="FEAMT" value="${deposit_balance_proof_confirm.data.FEAMT}">	
				<input type="hidden" id="ENGNAME" name="ENGNAME" value="${deposit_balance_proof_confirm.data.ENGNAME}">
				<input type="hidden" id="TAKE_TYPE" name="TAKE_TYPE" value="${deposit_balance_proof_confirm.data.TAKE_TYPE}">
				<input type="hidden" id="MAILADDR" name="MAILADDR" value="${deposit_balance_proof_confirm.data.MAILADDR}">
				<input type="hidden" id="PINNEW" name="PINNEW"  value="">
				<input type="hidden" name="TYPE" value="04">					
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-input">
								<span class="input-block">
									 <div id="TitleBar">
									     <center><h5 style="color:red"><spring:message code="LB.D0104" /></h5>	</center>
									 </div>
								</span>
							</div>
						</div>					
						<div class="ttb-input-block">							
							<!-- 帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.D0487" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_confirm.data.ACN}</span>
									</div>
								</span>
							</div>
							
							<!-- 身份證/營利事業統一編號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.D0488" />/<spring:message code="LB.D0489" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_confirm.data.CUSIDN}</span>
									</div>
								</span>
							</div>								
							
							<!-- 申請份數 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0490" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_confirm.data.COPY}</span>
									</div>
								</span>
							</div>	
							 
							<!-- 申請用途 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0491" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_confirm.data.PURPOSE1}</span>
									</div>
								</span>
							</div>
							 
							<div class="ttb-input-item row">
								<!--證明日期  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.D0492" />
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_confirm.data.DATE1}</span>	
									</div>
								</span>
							</div>
							
							<!-- 證明書種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0495" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<c:if test="${deposit_balance_proof_confirm.data.INQ_TYPE == '1'}">
									<span><spring:message code="LB.D0496" /></span>					       			
					       			</c:if>
					       			<c:if test="${deposit_balance_proof_confirm.data.INQ_TYPE == '2'}">
									<span><spring:message code="LB.D0497" /></span>				       			
					       			</c:if>								
								</span>
							</div>
							
							<!-- 幣別 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.Currency" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_confirm.data.CRY}</span>
									</div>
								</span>
							</div>
							
							<!-- 證明金額 -->
							<c:if test="${deposit_balance_proof_confirm.data.INQ_TYPE == '2'}">
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0499" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_confirm.data.AMT}</span>
									</div>
								</span>
							</div>
							</c:if>
							
							
							<!-- 提供文件版本 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0500" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
									<c:if test="${deposit_balance_proof_confirm.data.VER == '1'}">
									<span><spring:message code="LB.D0394" /></span>					       			
					       			</c:if>
					       			<c:if test="${deposit_balance_proof_confirm.data.VER == '2'}">
									<span><spring:message code="LB.D0396" /></span>					       			
					       			</c:if>	
									</div>
								</span>
							</div>
							<!-- 英文戶名 -->
							<c:if test="${deposit_balance_proof_confirm.data.VER == '2'}">							
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.X0363" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_confirm.data.ENGNAME}</span>
									</div>
								</span>
							</div>
							</c:if>
							
							<!--領取方式 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0503" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
									<c:if test="${deposit_balance_proof_confirm.data.TAKE_TYPE == '1'}">
									<span><spring:message code="LB.D0504" /></span>					       			
					       			</c:if>
					       			<c:if test="${deposit_balance_proof_confirm.data.TAKE_TYPE == '2'}">
									<span>${deposit_balance_proof_confirm.data.MAILADDR}</span>					       			
					       			</c:if>	
									</div>
								</span>
							</div>
							
							<!--手續費扣帳帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0506" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_confirm.data.FEEACN}</span>
									</div>
								</span>
							</div>
							
							<!--手續費 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0507" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<span class="input-unit"><spring:message code="LB.D0508" /></span>
											${deposit_balance_proof_confirm.data.FEAMT}
											<span class="input-unit"><spring:message code="LB.Dollar" /></span>
										</span>
									</div>
								</span>
							</div>
							
							<!--交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> 
											<spring:message code="LB.SSL_password" /> 
											<input type="radio" id="FGTXWAY" name="FGTXWAY" checked="checked" value="0"> 
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<input id="CMPASSWORD" name="CMPASSWORD" type="password"
										    class="text-input validate[required]"
										    maxlength="8"
											placeholder="<spring:message code="LB.Please_enter_password"/>">
									</div>
								</span>
							</div>
							
						</div>						
						<input type="button" id="pageback" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />						
						<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
						<input type="button" class="ttb-button btn-flat-orange" id="CMSUBMIT" value="<spring:message code="LB.Confirm" />"/>						
					</div>
				</div>							
				</form>

<!-- 				</form> -->
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			
		});
		
		function init(){

			//上一頁按鈕
			$("#pageback").click(function() {

				var action = '${pageContext.request.contextPath}' + '${previous}'
				$('#back').val("Y");
				$("form").attr("action", action);
				initBlockUI();
				$("#formId").validationEngine('detach');
				$("form").submit();

			});			
			
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});			
			$("#CMSUBMIT").click(function(e){
				
				e = e || window.event;

				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}
				else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
	 				$("#formId").attr("action","${__ctx}/ONLINE/SERVING/deposit_balance_proof_result");
	 	  			$("#formId").submit(); 
	 			}		
	  		});
		}
		
		
 	</script>
</body>
</html>
