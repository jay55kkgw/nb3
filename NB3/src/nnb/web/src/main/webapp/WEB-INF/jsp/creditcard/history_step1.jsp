<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
$(document).ready(function(){
	initFootable();
});
	function onlineQuery(ctype, cno){ 
		initBlockUI();
		$('#QUERYTYPE').val("BHWSHtml");
		$('#formId').attr('action', '${__ctx}'+'/CREDIT/INQUIRY/card_history_online');
		$('#formId').submit();
	}

	function emailQuery(ctype, cno){
		var formId = document.getElementById("formId");
		var email='${sessionScope.dpmyemail}';
		var i18n = new Object();
		i18n['email_alert']='<spring:message code="LB.EmailAlert" />'
		if(email=="") {
			alert(i18n['email_alert']);
		}else {
			$('#QUERYTYPE').val("BHWSReissue");
			$('#formId').attr('action', '${__ctx}'+'/CREDIT/INQUIRY/card_history_email');    		   	    
			$('#formId').submit();
		}
	}
  
	function onlineQuerypayment(ctype, cno){
		var formId = document.getElementById("formId");
		$('#QUERYTYPE').val("BHWSHtmlImg");
		$('#formId').attr('action', '${__ctx}'+'/CREDIT/INQUIRY/card_history_onlinePay');
    	$('#formId').submit();
	}
  
	function DisableButtons(){	
		// 關閉按鈕btn1
		var btn1Buttons = document.getElementsByName("btn1");
		for (var i = 0; i < btn1Buttons.length; i++){
			btn1Buttons[i].disabled = true;
		}

		// 關閉按鈕btn2
		var btn2Buttons = document.getElementsByName("btn2");
		for (var i = 0; i < btn2Buttons.length; i++){
			btn2Buttons[i].disabled = true;
		}	

		// 關閉按鈕 btn3
		var btn3Buttons = document.getElementsByName("btn3");
		for (var i = 0; i < btn3Buttons.length; i++){
			btn3Buttons[i].disabled = true;
		}	
	}
		
	function goBackto(){
		
			$('#formId').attr('action', '${__ctx}'+'/CREDIT/INQUIRY/card_history');
			$('#formId').submit();
		
	}
</script>

</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 歷史帳單明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Historical_Statement_Detail_Inquiry" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	
	
	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<form id="formId" method="post" action="">
	    	<h2><spring:message code="LB.Historical_Statement_Detail_Inquiry" /></h2><!-- 當期帳單明細查詢 -->
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<input type="hidden" id="CARDTYPE" name="CARDTYPE" value="${card_history_step1.data.CARDTYPE}">
				<input type="hidden" id="FGPERIOD" name="FGPERIOD" value="${card_history_step1.data.FGPERIOD}">
				<input type="hidden" id="BILL_NO" name="BILL_NO" value="${card_history_step1.data.BILL_NO}">
				<input type="hidden" id="QUERYTYPE" name="QUERYTYPE" value="${card_history_step1.data.QUERYTYPE}">
				<input type="hidden" id="FUNCTION" name="FUNCTION" value="${card_history_step1.data.FUNCTION}"/>
				<input type="hidden" id="USEREMAIL" name="USEREMAIL" value="${sessionScope.dpmyemail}"/>
				<div class="main-content-block row">
					<div class="col-12">
						<div>
							<ul class="ttb-result-list ">
								<li>
									<p>
										<spring:message code="LB.Inquiry_time" /><!-- 查詢時間  -->：${card_history_step1.data.CMQTIME}
									</p>
									<p>
										<!-- 查詢期間  -->
										<spring:message code="LB.Inquiry_period" />：<!-- 查詢期間  -->
										<c:if test="${card_history_step1.data.FGPERIOD == 'CMCURMON'}">
					                    	<spring:message code="LB.This_period" /><!-- 本期 -->
					                    </c:if>
					                    <c:if test="${card_history_step1.data.FGPERIOD == 'CMLASTMON'}">
					                    	<spring:message code="LB.Previous_period" /><!-- 上期 -->
					                    </c:if>
					                    <c:if test="${card_history_step1.data.FGPERIOD == 'CMLAST2MON'}">
					                    	<spring:message code="LB.Last_period" /><!-- 上上期 -->
					                    </c:if>
									</p>
									<!-- 資料總數  -->
									<p>
										<spring:message code="LB.Total_records" /><!-- 資料總數  -->：1
									</p>
								</li>
							</ul>
							<table class="table">
							<thead>
								<tr><!-- 執行選項  -->
									<th>
										<spring:message code="LB.Execution_option" /><!-- 執行選項  -->
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<c:choose>
											<c:when test="${card_history_step1.data.CARDTYPE == '700'}">
												<input type="button" class="ttb-sm-btn btn-flat-orange" name="btn1" value="<spring:message code="LB.Online_to_inquiry_of_statement" />" onclick="onlineQuery('700', '')" style="margin:5px 30px"/>
												<input type="button" class="ttb-sm-btn btn-flat-orange" name="btn2" value="<spring:message code="LB.Online_to_email_of_statement" />" onclick="emailQuery('700', '')" style="margin:5px 30px"/>
											</c:when>
											<c:when test="${card_history_step1.data.CARDTYPE == 'ALL' && card_history_step1.data.bcardflag eq 'TRUE'}">
												<input type="button" class="ttb-sm-btn btn-flat-orange" name="btn1" value="<spring:message code="LB.Online_to_inquiry_of_statement" />" onclick="onlineQuery('ALL', '')" style="margin:5px 30px"/>
												<c:if test="${ card_history_step1.data.FGPERIOD eq 'CMCURMON'}">
													<input type="button" class="ttb-sm-btn btn-flat-orange" name="btn1" value="<spring:message code="LB.Online_to_inquiry_of_payment" />" onclick="onlineQuerypayment('ALL', '')" style="margin:5px 30px"/>
												</c:if>
												<input type="button" class="ttb-sm-btn btn-flat-orange" name="btn2" value="<spring:message code="LB.Online_to_email_of_statement" />" onclick="emailQuery('ALL', '')" style="margin:5px 30px"/>
											</c:when>
											<c:otherwise>
												<input type="button" class="ttb-sm-btn btn-flat-orange" name="btn1" value="<spring:message code="LB.Online_to_inquiry_of_statement" />" onclick="onlineQuery('999', '')" style="margin:5px 30px"/>
												<c:if test="${ card_history_step1.data.FGPERIOD eq 'CMCURMON' && card_history_step1.data.CARDTYPE != '700'}" >
													<input type="button" class="ttb-sm-btn btn-flat-orange" name="btn1" value="<spring:message code="LB.Online_to_inquiry_of_payment" />" onclick="onlineQuerypayment('999', '')" style="margin:5px 30px"/>
												</c:if>
												<input type="button" class="ttb-sm-btn btn-flat-orange" name="btn2" value="<spring:message code="LB.Online_to_email_of_statement" />" onclick="emailQuery('999', '')" style="margin:5px 30px"/>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</tbody>
						    </table>
		    			</div>
		    			<input type="button" class="ttb-button btn-flat-orange" onclick="goBackto();" value="<spring:message code="LB.Back_to_previous_page" />">
					</div>
				</div>
				</form>
		</section>
	</main><!-- main-content END --> 
	</div><!-- content row END -->

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>