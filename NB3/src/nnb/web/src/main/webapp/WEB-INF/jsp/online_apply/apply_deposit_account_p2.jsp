<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.twzipcode-1.6.1.js"></script>
	<style>
		.zipcode {
			display: none;
		}
	</style>
    <script type="text/javascript">
        $(document).ready(function () {
//             initFootable(); // 將.table變更為footable 
            init();
   			genDateList();
   			setCITY();
   			
        });

        function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline", scroll: false});
    		AddCityOptions1(document.getElementById("CITYCHA"));
    		
    		//子女人數 (預設設定)
   		    var CHILD = '${result_data.CHILD}' == '' ? '99' : '${result_data.CHILD}';
			for(var i = 0;i <= 20;i++){
				if(CHILD == i){
					$("#CHILD").append("<option value='" + i + "' selected>" + i + "人</option>");
				}else{
					$("#CHILD").append("<option value='" + i + "'>" + i + "人</option>");
				}
			}


    		//身分證為補發/換發/新發 (預設設定)
    		$("#TYPECHA").children().each(function(){
    		    if ($(this).val()=='${result_data.TYPECHA}'){
    		        $(this).prop("selected", true);
    		    }
    		});
    		//補換發縣市 (預設設定)
    		$("#CITYCHA").children().each(function(){
    		    if ($(this).val()=='${result_data.CITYCHA}'){
    		        $(this).prop("selected", true);
    		    }
    		});
    		//學歷 (預設設定)
    		$("#DEGREE").children().each(function(){
    		    if ($(this).val()=='${result_data.DEGREE}'){
    		        $(this).prop("selected", true);
    		    }
    		});
    		//婚姻狀況 (預設設定)
			$("#childHiddenId").hide();
   		    if ('${result_data.MARRY}' == '0'){
   		        $("#MARRY0").attr("checked", true);
   		    }
   		    if ('${result_data.MARRY}' == '1'){
   		        $("#MARRY1").attr("checked", true);
				$("#childHiddenId").show();
   		    }
   		    if ('${result_data.MARRY}' == '2'){
   		        $("#MARRY2").attr("checked", true);
				$("#childHiddenId").show();
   		    }

    		//職業 (預設設定)
    		$("#CAREER1").children().each(function(){
    		    if ($(this).val()=='${result_data.CAREER1}'){
    		        $(this).prop("selected", true);
    		    }
    		});

    		//職稱代號 (預設設定)
//     		$("#CAREER2").children().each(function(){
//     		    if ($(this).text()=='${result_data.CAREER2_str}'){
//     		        $(this).attr("selected", true);
//     		    }
//     		});
			//起家金帳號(預設設定)
			if ('${input_data.STAEDYN}' == 'Y'){
				$("#STAEDYN1").attr("checked", true);
			}
			if ('${input_data.STAEDYN}' == 'N'){
				$("#STAEDYN2").attr("checked", true);
			}

	    	$("#CMSUBMIT").click(function(e){
	    		//身分證為補發/換發/新發日期 ，填入validate_CMDATE2欄位驗證
	    		$("#validate_CMDATE2").val((parseInt($("#CMDATE2YY").val()) + 1911).toString() + "/"+$("#CMDATE2MM").val() + "/"+$("#CMDATE2DD").val());
	    		if($("#CMDATE2YY").val().length==1)
	    			$("#validate_CMDATE2").val("00"+$("#validate_CMDATE2").val());
	    		if($("#CMDATE2YY").val().length==2)
	    			$("#validate_CMDATE2").val("0"+$("#validate_CMDATE2").val());

	    		//身分證為補發/換發/新發日期 ，填入CHGE_DT隱藏欄位
	    		$("#CHGE_DT").val($("#CMDATE2YY").val() + $("#CMDATE2MM").val() + $("#CMDATE2DD").val());
	    		if($("#CMDATE2YY").val().toString().length==1)
	    			$("#CHGE_DT").val("00"+$("#CHGE_DT").val());
	    		if($("#CMDATE2YY").val().toString().length==2)
	    			$("#CHGE_DT").val("0"+$("#CHGE_DT").val());

				//出生日期 資料整理 ，填入BIRTHDAY隱藏欄位
	    		$("#BIRTHDAY").val($("#CCBIRTHDATEYY").val() + $("#CCBIRTHDATEMM").val() + $("#CCBIRTHDATEDD").val());
	    		if($("#CCBIRTHDATEYY").val().length==1)
	    			$("#BIRTHDAY").val("00"+$("#BIRTHDAY").val());
	    		if($("#CCBIRTHDATEYY").val().length==2)
	    			$("#BIRTHDAY").val("0"+$("#BIRTHDAY").val());
	    		console.log($("#BIRTHDAY").val());
	    		
	    		//身分證為補發/換發/新發 ，填入ID_CHGE隱藏欄位
	    		$("#ID_CHGE").val($("#TYPECHA").val());
	    		
	    		//補換發縣市 ，填入 CHGE_CY 隱藏欄位 與 CHGE_CYNAME 隱藏欄位
	    		$("#CHGE_CY").val($("#CITYCHA").val());
	    		$("#CHGE_CYNAME").val($("#CITYCHA").find(":selected").text());

	    		// 戶籍/居住地址，填入 POSTCOD1 隱藏欄位 與 PMTADR 隱藏欄位
	    		$("#POSTCOD1").val($("#ZIP1").val());
	    		$("#PMTADR").val($("#CITY1").val() + $("#ZONE1").val());
    			$("#PMTADR").val($("#PMTADR").val()+$("#ADDR1").val());

    			//通訊地址，填入 POSTCOD2 隱藏欄位 與 CTTADR 隱藏欄位
    			//如果SAMEADDRCHK為Y，則與 戶籍/居住地址 相同
    			if($("#SAMEADDRCHK").val() == 'Y'){
    	    		$("#POSTCOD2").val($("#POSTCOD1").val());
    	    		$("#CTTADR").val($("#PMTADR").val());
    			}else{
    	    		$("#POSTCOD2").val($("#ZIP2").val());
    	    		$("#CTTADR").val($("#CITY2").val() + $("#ZONE2").val());
        			$("#CTTADR").val($("#CTTADR").val() + $("#ADDR2").val());
    			}
    			
    			//個人/家庭年收入加入驗證
    			if($('#SALARY').val().length > 0)
    				$('#SALARY').addClass("validate[funcCallRequired[validate_CheckNumber['<spring:message code= "LB.D0625_1" />',SALARY]]");
    			else
    				$('#SALARY').removeClass("validate[funcCallRequired[validate_CheckNumber['<spring:message code= "LB.D0625_1" />',SALARY]]");
    			if($("#SALARY").val().length<=0)
    				$("#SALARY").val('0');
    			
    			//職業(文字)，填入 CAREER1_str 隱藏欄位
    			$("#CAREER1_str").val($("#CAREER1").find(":selected").text());
    			//職稱(文字)，填入 CAREER2_str 隱藏欄位
//     			$("#CAREER2_str").val($("#CAREER2").find(":selected").text());

    			//切割 TELNUM 至 ARACOD1 與 TELNUM1
    			var inputData = $("#TELNUM").val();
    			var TELNUM = matchPhone(inputData);
				if(TELNUM != null){
	    			$('#ARACOD1').val(TELNUM[0]);
	    			$('#TELNUM1').val(TELNUM[1]);
				}

				//居住地電話 ，如果不為空則加入validate
    			if($('#TELNUM').val().length > 0){
    				$('#TELNUM').addClass("validate[funcCallRequired[validate_OneInputPhone['居住地電話格式錯誤',TELNUM]]]");
    			}
    			else{
    				$('#TELNUM').removeClass("validate[funcCallRequired[validate_OneInputPhone['居住地電話格式錯誤',TELNUM]]]");
    			}
    			if($('#ARACOD1').val().length > 0){
    				$('#ARACOD1').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1217" />',ARACOD1]]");
    			}
    			else{
    				$('#ARACOD1').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1217" />',ARACOD1]]");
    			}
    			if($('#TELNUM1').val().length > 0){
    				$('#TELNUM1').addClass("validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.D1127" />',TELNUM1]]");
    			}
    			else{
    				$('#TELNUM1').removeClass("validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.D1127" />',TELNUM1]]");
    			}

    			//切割 BUSP 至 BUSZIP 與 BUSTEL12
				var inputData = $("#BUSPHOME").val();
				var BUSPHOME = matchPhone(inputData);
				if(BUSPHOME != null){
    				$('#ARACOD2').val(BUSPHOME[0]);
    				$('#PHONE_22').val(BUSPHOME[1]);
				}

    			if($("#PHONE_23").val().length>0)
    				$("#TELNUM2").val($("#PHONE_22").val()+"#"+$("#PHONE_23").val());
    			else
    				$("#TELNUM2").val($("#PHONE_22").val());
    			
    			//公司電話，如果不為空則加入validate
    			if($('#BUSPHOME').val().length > 0){
    				$('#BUSPHOME').addClass("validate[funcCallRequired[validate_OneInputPhone['公司電話格式錯誤',BUSPHOME]]]");
    			}
    			else{
    				$('#BUSPHOME').removeClass("validate[funcCallRequired[validate_OneInputPhone['公司電話格式錯誤',BUSPHOME]]]");
    			}
    			if($('#ARACOD2').val().length > 0)
    				$('#ARACOD2').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1218" />',ARACOD2]]");
    			else
    				$('#ARACOD2').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1218" />',ARACOD2]]");
    			if($('#PHONE_23').val().length > 0)
    				$('#PHONE_23').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1219" />',PHONE_23]]");
    			else
    				$('#PHONE_23').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1219" />',PHONE_23]]");
    			if($('#PHONE_22').val().length > 0)
    				$('#PHONE_22').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.D0094" />',PHONE_22]]");
    			else
    				$('#PHONE_22').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.D0094" />',PHONE_22]]");

    			//行動電話，如果不為空則加入validate
    			if($('#CELPHONE').val().length > 0)
    				$('#CELPHONE').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.D0069" />',CELPHONE]]");
    			else
    				$('#CELPHONE').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.D0069" />',CELPHONE]]");
    			
    			if($('#ARACOD3').val().length > 0)
    				$('#ARACOD3').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1220" />',ARACOD3]]");
    			else
    				$('#ARACOD3').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1220" />',ARACOD3]]");

    			//切割 FAXP 至 BUSZIP 與 BUSTEL12
				var inputData = $("#FAXP").val();
				var FAXP = matchPhone(inputData);
				if(FAXP != null){
	    			$('#ARACOD3').val(FAXP[0]);
	    			$('#FAX').val(FAXP[1]);
				}
				
    			//傳真，如果不為空則加入validate
    			if($('#FAXP').val().length > 0){
    				$('#FAXP').addClass("validate[funcCallRequired[validate_OneInputPhone['傳真格式錯誤',FAXP]]]");
    			}
    			else{
    				$('#FAXP').removeClass("validate[funcCallRequired[validate_OneInputPhone['傳真格式錯誤',FAXP]]]");
    			}
   				$("#validate_FAX").val('');
				$('#FAX').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1221" />',FAX]]");
				$('#validate_FAX').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1221" />234',validate_FAX]]");
    			if($('#FAX').val().length>0)
    			{
    				if(($('#FAX').val()).indexOf("#")==-1)
    				{	
    				// 傳真電話
    					$('#FAX').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1221" />',FAX]]");
    				}
    				else
    				{
    				// 傳真電話
	   					$('#validate_FAX').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1221" />234',validate_FAX]]");
	   					var splits = ($('#FAX').val()).split("#");
	   					$("#validate_FAX").val(splits[0]);
	   				}		
	   			}
    			//若選擇為起家金帳戶則移除未成年檢核
				if($("input:radio[name='STAEDYN']")[0].checked){
					$('#validate_BIRTHDATE').removeClass("validate[funcCallRequired[validate_age[<spring:message code= "LB.X1222" />,BIRTHDAY,TODAY,20,true]]]")
				}else {
					$('#validate_BIRTHDATE').addClass("validate[funcCallRequired[validate_age[<spring:message code= "LB.X1222" />,BIRTHDAY,TODAY,20,true]]]")
				}
				//轉換CUSIDN為大寫
    			$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
				
				e = e || window.event;
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
		        	var action = '${__ctx}/ONLINE/APPLY/apply_deposit_account_p3';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});
			//返回按鈕
			$("#CMBACK").click( function(e) {
				$("#formId").validationEngine('detach');
				console.log("submit~~");
    			$("#formId").attr("action", "${__ctx}/ONLINE/APPLY/apply_deposit_account_p1");
	            $("#formId").submit();
			});
			//重設按鈕
			$("#CMRESET").click(function(e){
				$("#formId")[0].reset();
				getTmr();
			});
        }

		function ValidateValue(textbox) {
			var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
			var textboxvalue = textbox.value;
			var index = textboxvalue.length - 1;
			var s = textbox.value.charAt(index);
			if (IllegalString.indexOf(s) >= 0) {
				s = textboxvalue.substring(0, index);
				textbox.value = s;
			}
		}
		
		function genDateList(){
			var today = new Date();
			var y = today.getFullYear() - 1911;
			var m = today.getMonth() + 1;
			var d = today.getDate();
			var CCBIRTHDATEYY = "${ result_data.CCBIRTHDATEYY }";
			var CCBIRTHDATEMM = "${ result_data.CCBIRTHDATEMM }";
			var CCBIRTHDATEDD = "${ result_data.CCBIRTHDATEDD }";
			var CMDATE2YY = "${ result_data.CMDATE2YY }";
			var CMDATE2MM = "${ result_data.CMDATE2MM }";
			var CMDATE2DD = "${ result_data.CMDATE2DD }";
			for(var i = y;i >= 1;i--){
				j = i.toString();
				for(k = 0;k <= 2 - i.toString().length;k++)j='0' + j;
				if(CCBIRTHDATEYY == i){
					$("#CCBIRTHDATEYY").append("<option value='" + j + "' selected>" + "<spring:message code= "LB.D0583" />" + i + "<spring:message code= "LB.Year" />" + "</option>");
				}else{
					$("#CCBIRTHDATEYY").append("<option value='" + j + "'>" + "<spring:message code= "LB.D0583" />" + i + "<spring:message code= "LB.Year" />" + "</option>");
				}
				if(CMDATE2YY == i){
					$("#CMDATE2YY").append("<option value='" + j + "' selected>" + "<spring:message code= "LB.D0583" />" + i + "<spring:message code= "LB.Year" />" + "</option>");
				}else{
					$("#CMDATE2YY").append("<option value='" + j + "' >" + "<spring:message code= "LB.D0583" />" + i + "<spring:message code= "LB.Year" />" + "</option>");
				}
			}
			for(var i = 1;i <= 12;i++){
				j = i.toString();
				for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
				if(CCBIRTHDATEMM == i){
					$("#CCBIRTHDATEMM").append("<option value='" + j + "' selected>" + i + "<spring:message code= "LB.Month" />" + "</option>");
				}else{
					$("#CCBIRTHDATEMM").append("<option value='" + j + "' >" + i + "<spring:message code= "LB.Month" />" + "</option>");
				}
				if(CMDATE2MM == i){
					$("#CMDATE2MM").append("<option value='" + j + "' selected>" + i + "<spring:message code= "LB.Month" />" + "</option>");
				}else{
					$("#CMDATE2MM").append("<option value='" + j + "' >" + i + "<spring:message code= "LB.Month" />" + "</option>");
				}
			}
			for(var i = 1;i <= 31;i++){
				j = i.toString();
				for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
				if(CCBIRTHDATEDD == i){
					$("#CCBIRTHDATEDD").append("<option value='" + j + "' selected>" + i + "<spring:message code= "LB.Day" />" + "</option>");
				}else{
					$("#CCBIRTHDATEDD").append("<option value='" + j + "' >" + i + "<spring:message code= "LB.Day" />" + "</option>");
				}
				if(CMDATE2DD == i){
					$("#CMDATE2DD").append("<option value='" + j + "' selected>" + i + "<spring:message code= "LB.Day" />" + "</option>");
				}else{
					$("#CMDATE2DD").append("<option value='" + j + "' >" + i + "<spring:message code= "LB.Day" />" + "</option>");
				}
			}
		}
		
		function doquery9(){
			if($("#SAMEADDRCHK").prop("checked")){
				$("#SAMEADDRCHK").val('Y');
	    		$("#CITY2").children().each(function(){
	    			console.log($(this).val());
	    		    if ($(this).val()==$("#CITY1").val()){
	    		        $(this).prop("selected", true); //或是給"selected"也可
	    		    }
	    		});
	        	$('#ZONE2').html("<option value='#'>--- <spring:message code= "LB.Select" /><spring:message code= "LB.D0060" /> ---</option> ");

	    		$("#ZONE1").children().each(function(){
	        		console.log($(this).val());
	        		if($(this).val() != $("#ZONE1").val()){
	        			$("#ZONE2").append("<option>" + $(this).val() + "</option>");
	        		}
	        		else{
	        			$("#ZONE2").append("<option selected>" + $(this).val() + "</option>");
	        		}
	    		});
	    		$("#ADDR2").val($("#ADDR1").val());
        		$("#ZIP21").html($("#ZIP1").val());
        		$("#ZIP2").val($("#ZIP1").val());
			}else{
				$("#SAMEADDRCHK").val('N');
			}
		}
		
		function showChild(){
			if($("#MARRY0").prop("checked")){
				$("#childHiddenId").hide();
			}else{
				$("#childHiddenId").show();
			}
		}
		
		function matchPhone(input){
			var phoneList = [];
			var re = new RegExp(/^\d{1,3}-\d{1,10}$/)
			if(re.test(input)){
				var vList1 = input.split("-");
				phoneList.push(vList1[0]);
				phoneList.push(vList1[1]);
				return phoneList;
			}
			return null;
		}
		
		function changeMatchPhone(input){
			var inputData = $("#"+input).val();
			var phoneList = matchPhone(inputData);
			if(phoneList != null)
				$("#"+input).val(phoneList[0] + '-' + phoneList[1]);
		}
		
		//程式設定初始化
		function setCITY(){

   			llo = "${__ctx}/js/";
			$('#twzipcode1').twzipcode({
                countyName: 'CITY1',
                districtName: 'ZONE1',
                zipcodeName: 'ZIP1',
                language:'zip_${pageContext.response.locale}',
                countySel: '${result_data.CITY1}',
                districtSel: '${result_data.ZONE1}'
			});
			$('#twzipcode2').twzipcode({
                countyName: 'CITY2',
                districtName: 'ZONE2',
                zipcodeName: 'ZIP2',
                language:'zip_${pageContext.response.locale}',
                countySel: '${result_data.CITY2}',
                districtSel: '${result_data.ZONE2}'
			});

		}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">開戶申請</a></li>
			<li class="ttb-breadcrumb-item"><a href="#">預約開立存款戶</a></li>
		</ol>
	</nav>
	<!-- header     -->
<!-- 	<header> -->
<%-- 		<%@ include file="../index/header.jsp"%> --%>
<!-- 	</header> -->
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					預約開立存款戶
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div id="step-bar">
					<ul>
						<li class="finished">注意事項與權益</li>
						<li class="active">開戶資料</li>
						<li class="">確認資料</li>
						<li class="">完成預約</li>
					</ul>
				</div>
                <form id="formId" method="post">
					<input type="hidden" name="action" value="forward">
					<input type="hidden" name="ADOPID" value="N201">	
					<input type="hidden" name="BIRTHDAY" id="BIRTHDAY" value="">	
					<input type="hidden" name="POSTCOD1" id="POSTCOD1" value="">	
					<input type="hidden" name="PMTADR" id="PMTADR" value="">	
					<input type="hidden" name="POSTCOD2" id="POSTCOD2" value="">	
					<input type="hidden" name="CTTADR" id="CTTADR" value="">	
					<input type="hidden" name="TELNUM2" id="TELNUM2" value="">	
					<input type="hidden" name="ID_CHGE" id="ID_CHGE" value="">	
					<input type="hidden" name="CHGE_DT" id="CHGE_DT" value="">	
					<input type="hidden" name="CHGE_CY" id="CHGE_CY" value="">
					<input type="hidden" name="CHGE_CYNAME" id="CHGE_CYNAME" value="">	
					<input type="hidden" name="CAREER1_str" id="CAREER1_str" value="">
					<input type="hidden" name="TODAY" id="TODAY" value="${ today }">
					<input type="hidden" name="HAS_PIC" id="HAS_PIC" value="Y">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 tab-content">
                     
							<div class="ttb-input-block">
								<div class="ttb-message">
									<p>開戶資料 (1/3)</p>
								</div>
								<div class="classification-block">
									<p>基本資料</p>
									<p>( <span class="high-light">*</span> 為必填)</p>
								</div>
<!-- 								中文戶名 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1105"/> <span class="high-light">*</span>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CUSNAME" name="CUSNAME" class="text-input validate[required,funcCall[validate_isChinese[輸入資料有誤,CUSNAME]]" value="${ result_data.CUSNAME }" maxLength="50" size="100" placeholder="請輸入中文戶名"/>
											<span class="input-remarks">請輸入與您身分證姓名相同的中文戶名，以利審核的進行。</span>
										</div>
									</span>
								</div>
								<!-- 身分證字號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.X1914"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CUSIDN" name="CUSIDN" class="text-input validate[required, funcCall[validate_checkSYS_IDNO[CUSIDN]]]"  value="${ result_data.CUSIDN }" maxLength="10" size="11" placeholder="請輸入身分證字號"/>
										</div>
									</span>
								</div>
<!-- 								出生日期** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0582"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											
											<select name="CCBIRTHDATEYY" id="CCBIRTHDATEYY" class="custom-select select-input input-width-100">
												<option value="#">
													---
												</option>
											</select>
											
											<select name="CCBIRTHDATEMM" id="CCBIRTHDATEMM" class="custom-select select-input input-width-60">
												<option value="#">
													---
												</option>
											</select>
											
											<select name="CCBIRTHDATEDD" id="CCBIRTHDATEDD" class="custom-select select-input input-width-60">
												<option value="#">
													---
												</option>
											</select>
      										
<!-- 												驗證用的span預設隱藏 -->
											<span id="hideblock_BIRTHDATE" >
<!-- 												驗證用的input -->
											<input id="validate_BIRTHDATE" name="validate_BIRTHDATE" type="text" class="text-input validate[funcCallRequired[validate_age[<spring:message code= "LB.X1222" />,BIRTHDAY,TODAY,20,true]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
											<input id="validate_CCBIRTHDATEYY" name="validate_CCBIRTHDATEYY" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[年,CCBIRTHDATEYY,#]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											<input id="validate_CCBIRTHDATEMM" name="validate_CCBIRTHDATEMM" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[月,CCBIRTHDATEMM,#]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											<input id="validate_CCBIRTHDATEDD" name="validate_CCBIRTHDATEDD" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[日,CCBIRTHDATEDD,#]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</div>
									</span>
								</div>
<!-- 								身分證發證資訊*** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											身分證發證資訊 <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="CMDATE2YY" id="CMDATE2YY" class="custom-select select-input input-width-100 w-auto validate[required]">
												<option value="#">
													---
												</option>
											</select>
											<select name="CMDATE2MM" id="CMDATE2MM" class="custom-select select-input input-width-60 w-auto validate[required]">
												<option value="#">
													---
												</option>
											</select>
											<select name="CMDATE2DD" id="CMDATE2DD" class="custom-select select-input input-width-60 w-auto validate[required]">
												<option value="#">
													---
												</option>
											</select>
												
											<input id="validate_CMDATE2YY" name="validate_CMDATE2YY" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[年,CMDATE2YY,#]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											<input id="validate_CMDATE2MM" name="validate_CMDATE2MM" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[月,CMDATE2MM,#]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											<input id="validate_CMDATE2DD" name="validate_CMDATE2DD" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[日,CMDATE2DD,#]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											
										</div>
										<div class="ttb-input">
											<select name="CITYCHA" id="CITYCHA" class="custom-select select-input input-width-125">
								          		<option value="#" >---<spring:message code="LB.W1670"/>---</option>
											</select>
											<select name="TYPECHA" id="TYPECHA" class="custom-select select-input input-width-100 validate[required]">
								          		<option value="1" ><spring:message code="LB.D1113"/></option>
								          		<option value="2" selected><spring:message code="LB.W1664"/></option>
								          		<option value="3"><spring:message code="LB.W1665"/></option>
											</select>
											<span class="input-remarks">請輸入與您身分證相同的發證資訊。</span>
											<input id="validate_CITYCHA" name="validate_CITYCHA" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[<spring:message code="LB.D1071" />,CITYCHA,#]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</div>
									</span>
								</div>
<!-- 								教育程度**** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											教育程度
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="DEGREE" id="DEGREE" class="custom-select select-input">
												<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.D1136"/> ---</option> 
									 			<option value="1"><spring:message code="LB.D0588"/></option>
									 			<option value="2"><spring:message code="LB.D0589"/></option>
									 			<option value="3"><spring:message code="LB.D0590"/></option>
									 			<option value="4"><spring:message code="LB.D0591"/></option>
									 			<option value="5"><spring:message code="LB.D1141"/></option>
									 			<option value="6"><spring:message code="LB.D0866"/></option>						
											</select>
										</div>
									</span>
								</div>
<!-- 								婚姻狀況 -->		
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0057"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.D0595"/></option>
												<input type="radio" name="MARRY" id="MARRY1" value="1" onclick="showChild()"/>
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block">
												<spring:message code="LB.D0596"/>
												<input type="radio" name="MARRY" id="MARRY0" value="0" onclick="showChild()"/>
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block">
												<spring:message code="LB.D0572"/></option>
												<input type="radio" name="MARRY" id="MARRY2" value="2" onclick="showChild()"/>
												<span class="ttb-radio"></span>
											</label>
<!-- 											<select name="MARRY" id="MARRY" class="select-input">  -->
<%-- 												<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.D0057"/> ---</option>  --%>
<%-- 									 			<option value="0"><spring:message code="LB.D0596"/></option> --%>
<%-- 									 			<option value="1"><spring:message code="LB.D0595"/></option> --%>
<%-- 									 			<option value="2"><spring:message code="LB.D0572"/></option>  	 						 --%>
<!-- 											</select> -->
										</div>
									</span>
								</div>
<!-- 								*子女人數 -->				
								<div class="ttb-input-item row" id="childHiddenId">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1149"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="CHILD" id="CHILD" class="custom-select select-input">
												<option value="">請輸入<spring:message code="LB.D1149"/></option> 
											</select>
<!-- 											<input type="text" id="CHILD" name="CHILD" class="text-input" value="" maxLength="2" size="3"/> -->
										</div>
										
									</span>
								</div>
<!-- 								家庭年收入****** -->	
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.X0170"/><spring:message code="LB.D0625_1"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="SALARY" name="SALARY" placeholder="請輸入個人/家庭年收入" class="text-input" value="${ result_data.SALARY }" maxLength="7" size="8" /><spring:message code="LB.D1144"/>
										</div>
									</span>
								</div>
<!-- 								********** -->	
								<div class="classification-block">
									<p>聯絡資訊</p>
									<p>( <span class="high-light">*</span> 為必填)</p>
								</div>
<!-- 								****E-mail*** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											E-mail <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="MAILADDR" name="MAILADDR" placeholder="請輸入E-mail" class="text-input validate[required,funcCall[validate_EmailCheck[MAILADDR]]]" value="${ result_data.MAILADDR }" maxLength="60" size="61"/>
											<span class="check-block" style="padding-left: 0px; font-size: inherit"><spring:message code="LB.X2619"></spring:message></span>
										</div>
									</span>
								</div>
<!-- 								***行動電話*** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0069"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CELPHONE" name="CELPHONE" class="text-input" value="${ result_data.CELPHONE }" placeholder="例如：0981212123" maxLength="10" size="11"/>

											<span id="hideblock_celphone" >
												<input id="validate_celphone" name="validate_celphone" type="text" value="#" class="text-input 
													validate[funcCallRequired[validate_chkCelPhome['<spring:message code= "LB.X1229" />',CELPHONE]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
<!-- 								***居住地電話*** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1127"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="TELNUM" name="TELNUM" class="text-input" value="${ result_data.TELNUM }" maxLength="14" size="14" placeholder="例如：02-7818218"/>
											<input type="text" id="ARACOD1" name="ARACOD1" class="text-input" value="" maxLength="3" size="4" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											<input type="text" id="TELNUM1" name="TELNUM1" class="text-input" value="" maxLength="10" size="11"
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</div>
									</span>
								</div>
<!-- 								****公司電話** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0094"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
<!-- 										<div class="ttb-input"> -->
<!-- 											<input type="text" id="ARACOD2" name="ARACOD2" class="text-input" value="" maxLength="3" size="4" style="width:15%"/>－ -->
<%-- 											<input type="text" id="PHONE_22" name="PHONE_22" class="text-input" value="" maxLength="10" size="11"/><spring:message code="LB.D0095"/>： --%>
<!-- 											<input type="text" id="PHONE_23" name="PHONE_23" class="text-input" value="" maxLength="4" size="5"/> -->
<!-- 										</div> -->
										<input type="text" id="BUSPHOME" name="BUSPHOME" class="text-input" value="${ result_data.BUSPHOME }" 
												placeholder="例如：02-7818218" maxLength="14" size="14" onchange="changeMatchPhone('BUSPHOME')"/>
										<spring:message code="LB.D0095"/>
										<input type="text" id="PHONE_23" name="PHONE_23" class="text-input card-input"  value="${ result_data.PHONE_23 }" maxLength="4" size="5"/>	
											
										<input type="text" id="ARACOD2" name="ARACOD2" value="" maxLength="3" size="4" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										<input type="text" id="PHONE_22" name="PHONE_22" value="" maxLength="10" size="11" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
<!-- 												驗證用的span預設隱藏 -->
										<input id="validate_phone" name="validate_phone" type="text" value="#" class="text-input 
												validate[funcCallRequired[validate_chkPhoneOne['<spring:message code= "LB.X1229" />',TELNUM,TELNUM2,CELPHONE]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
									</span>
								</div>
								
<!-- 								****傳真*** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1131"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											
											
											<input type="text" id="FAXP" name="FAXP" class="text-input" value="${ result_data.FAXP }" 
												placeholder="例如：02-7818218" maxLength="21" size="21" onchange="changeMatchPhone('FAXP')"/>
											<input type="text" id="ARACOD3" name="ARACOD3" value="" maxLength="3" size="4"
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											<input type="text" id="FAX" name="FAX" value="" maxLength="17" size="18"
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
<!-- 												驗證用的span預設隱藏 -->
											<span id="hideblock_FAX" >
												<input id="validate_FAX" name="validate_FAX" type="text" value="#" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
								
<!-- 								*****居住地址*** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0143"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
									
										<div class="ttb-input" id="twzipcode1">
											<span data-role="county" data-style="custom-select select-input input-width-125"></span>
											<span data-role="district" data-style="custom-select select-input input-width-100"></span>
											<span data-role="zipcode" data-style="zipcode"></span>
										</div>
										
<!-- 										<div class="ttb-input"> -->
<!-- 											<input type="hidden" name="ZIP1" id="ZIP1" value="" size=4 maxlength=3><font id="ZIP11"></font>&nbsp;			 -->
<!-- 											<select name="CITY1" id="CITY1" class="select-input validate[required]"  onchange="doquery2('#CITY1','#ZONE1')"> -->
<%-- 								          		<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.D0059"/> ---</option>   --%>
<%-- 								          		<c:forEach var="dataList" items="${city_data}"> --%>
<%-- 													<option>${dataList.CITY}</option> --%>
<%-- 												</c:forEach> --%>
<!-- 											</select> -->
<!-- 											<select name="ZONE1" id="ZONE1" class="select-input validate[required]" onchange="doquery3('#CITY1','#ZONE1','#ZIP11', '#ZIP1')"> -->
<%-- 								          		<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.D0060"/> ---</option>  --%>
<!-- 											</select> -->
											
											
<!-- 										</div> -->

										<div class="ttb-input">
											<input type="text" id="ADDR1" name="ADDR1" placeholder="請輸入通訊地址 "  class="text-input validate[required]" value="${ result_data.ADDR1 }" maxLength="24" size="42" />
											
											<span id="hideblock_ADDR1" >
												<input id="validate_CITY1" name="validate_CITY1" type="text" value="#" class="text-input 
													validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1118" />',CITY1,#]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												<input id="validate_ZONE1" name="validate_ZONE1" type="text" value="#" class="text-input 
													validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1490" />',ZONE1,#]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												<input id="validate_ADDR132" name="validate_ADDR132" type="text" value="#" class="text-input 
													validate[funcCallRequired[validate_requiredWith['<spring:message code= "LB.X1228" />',ADDR13]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
											
										</div>
									</span>
								</div>
<!-- 								***戶籍地址**** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0376"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="check-block" for="SAMEADDRCHK">同戶籍地址
												<input type="checkbox" name="SAMEADDRCHK" id="SAMEADDRCHK" onClick="doquery9()" onblur="doquery9()">
												<span class="ttb-check"></span>
											</label>
										</div>
										
										<div class="ttb-input" id="twzipcode2">
											<span data-role="county" data-style="custom-select select-input input-width-125 validate[required]"></span>
											<span data-role="district" data-style="custom-select select-input input-width-100 validate[required]"></span>
											<span data-role="zipcode" data-style="zipcode"></span>
										</div>
										
<!-- 										<div class="ttb-input"> -->
											
<!-- 											<input type="hidden" name="ZIP2" id="ZIP2" value="" size=4 maxlength=3><font id="ZIP21"></font>&nbsp;			 -->
<!-- 											<select name="CITY2" id="CITY2" class="select-input validate[required]"  onchange="doquery2('#CITY2','#ZONE2')"> -->
<%-- 								          		<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.D0059"/> ---</option>   --%>
<%-- 								          		<c:forEach var="dataList" items="${city_data}"> --%>
<%-- 													<option>${dataList.CITY}</option> --%>
<%-- 												</c:forEach> --%>
<!-- 											</select> -->
<!-- 											<select name="ZONE2" id="ZONE2" class="select-input validate[required]" onchange="doquery3('#CITY2','#ZONE2','#ZIP21', '#ZIP2')"> -->
<%-- 								          		<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.D0060"/> ---</option>  --%>
<!-- 											</select> -->
											
											
<!-- 										</div> -->
										<div class="ttb-input">
											<input type="text" id="ADDR2" name="ADDR2" placeholder="請輸入通訊地址 " class="text-input validate[required]" value="${ result_data.ADDR2 }" maxLength="24" size="42" />
											<span id="hideblock_ADDR2" >
												<input id="validate_CITY2" name="validate_CITY2" type="text" value="#" class="text-input 
													validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1118" />',CITY2,#]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												<input id="validate_ZONE2" name="validate_ZONE2" type="text" value="#" class="text-input 
													validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1490" />',ZONE2,#]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>

<!-- 								*職業資訊***** -->
								<div class="classification-block">
									<p>職業資訊</p>
									<p>( <span class="high-light">*</span> 為必填)</p>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										職業 -->
											<spring:message code="LB.D1132"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="CAREER1" id="CAREER1" class="custom-select select-input validate[required]">
												<option value="">--- <spring:message code="LB.Select"/><spring:message code="LB.D1132"/> ---</option> 
									 			<option value="061100"><spring:message code="LB.D0871"/></option>
									 			<option value="061200"><spring:message code="LB.D0872"/></option>
									 			<option value="061300"><spring:message code="LB.D0873"/></option>  	     	
									 			<option value="061400"><spring:message code="LB.D0874"/></option>  	     	
									 			<option value="061410"><spring:message code="LB.D0081"/></option>  	     	
									 			<option value="061500"><spring:message code="LB.D0876"/></option>  	     	
									 			<option value="0615A0"><spring:message code="LB.D0877"/></option>  	     	
									 			<option value="0615B0"><spring:message code="LB.D0878"/></option>  	     	
									 			<option value="0615C0"><spring:message code="LB.D0879"/></option>  	     	
									 			<option value="0615D0"><spring:message code="LB.D0880"/></option>  	     	
									 			<option value="0615E0"><spring:message code="LB.D0881"/></option>  	     	
									 			<option value="0615F0"><spring:message code="LB.D0882"/></option>  	     	
									 			<option value="0615G0"><spring:message code="LB.D0883"/></option>  	     	
									 			<option value="0615H0"><spring:message code="LB.D0884"/></option>  	     	
									 			<option value="0615I0"><spring:message code="LB.D0885"/></option>  	     	
									 			<option value="0615J0"><spring:message code="LB.D0886"/></option>  	     	
									 			
									 			<option value="061610"><spring:message code="LB.D0887"/></option>  	     	
									 			<option value="061620"><spring:message code="LB.D0888"/></option>  	  
									 			<option value="061630"><spring:message code="LB.D0889"/></option>
									 			<option value="061640"><spring:message code="LB.D0890"/></option>
									 			<option value="061650"><spring:message code="LB.D0891"/></option>
									 			<option value="061660"><spring:message code="LB.D0892"/></option>
									 			<option value="061670"><spring:message code="LB.D0893"/></option>
									 			<option value="061680"><spring:message code="LB.D0894"/></option>
									 			<option value="061690"><spring:message code="LB.D0082"/></option>
									 			<option value="061691"><spring:message code="LB.D0083"/></option>
									 			<option value="061692"><spring:message code="LB.D0897"/></option>
									 			
									 			<option value="061700"><spring:message code="LB.D0898"/></option>  	     	
									 			<option value="069999"><spring:message code="LB.D0899"/></option>  	     											
											</select>
										</div>
									</span>
								</div>
<!-- 								********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										職稱代號  -->
											<spring:message code="LB.D1133"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<!-- 											<select name="CAREER2" id="CAREER2" class="custom-select select-input validate[required]" value=""> -->
<!-- 												<option value="">請選擇職稱代號</option>  -->
<!-- 									 			<option value="1">上/中/少將</option> -->
<!-- 									 			<option value="1">院上/中/少將</option> -->
<!-- 									 			<option value="1">院/部/次長</option> -->
<!-- 									 			<option value="1">大專校長</option> -->
<!-- 									 			<option value="1">總裁</option> -->
<!-- 									 			<option value="1">醫/會計師</option> -->
<!-- 									 			<option value="1">導演</option> -->
<!-- 									 			<option value="1">中央民意代表</option> -->
<!-- 									 			<option value="1">中研院院士</option> -->
<!-- 									 			<option value="1">董/理事長</option> -->
<!-- 									 			<option value="1">律/建築師</option> -->
<!-- 									 			<option value="1">製作人</option> -->
									 			
<!-- 									 			<option value="2">上/中/少校</option> -->
<!-- 									 			<option value="2">地方縣市長</option> -->
<!-- 									 			<option value="2">地方民意代表</option> -->
<!-- 									 			<option value="2">教授/副教授</option> -->
<!-- 									 			<option value="2">主任</option> -->
<!-- 									 			<option value="2">總經理/幹事</option> -->
<!-- 									 			<option value="2">董監事</option> -->
<!-- 									 			<option value="2">電子/資訊/機械/土木技師</option> -->
<!-- 									 			<option value="2">音樂/戲劇/舞蹈表演</option> -->
									 			
<!-- 									 			<option value="3">上/中/少尉</option> -->
<!-- 									 			<option value="3">局/處/司長</option> -->
<!-- 									 			<option value="3">助理教授</option> -->
<!-- 									 			<option value="3">大專講師</option> -->
<!-- 									 			<option value="3">協理/處長/主任</option> -->
<!-- 									 			<option value="3">專業顧問</option> -->
<!-- 									 			<option value="3">飛機駕駛</option> -->
<!-- 									 			<option value="3">代書</option> -->
<!-- 									 			<option value="3">媒體記者/播報/主持</option> -->
									 			
<!-- 									 			<option value="4">士/官兵</option> -->
<!-- 									 			<option value="4">參事</option> -->
<!-- 									 			<option value="4">專門委員</option> -->
<!-- 									 			<option value="4">高中/國中/小學校長</option> -->
<!-- 									 			<option value="4">經理</option> -->
<!-- 									 			<option value="4">廠長</option> -->
<!-- 									 			<option value="4">藥劑/醫技/護理人員</option> -->
<!-- 									 			<option value="4">服裝/造型/美容設計</option> -->
									 			
<!-- 									 			<option value="5">警政首長/局長</option> -->
<!-- 									 			<option value="5">組/科/課長</option> -->
<!-- 									 			<option value="5">高中/國中/小學主任</option> -->
<!-- 									 			<option value="5">科/課長</option> -->
<!-- 									 			<option value="5">襄理</option> -->
<!-- 									 			<option value="5">機電/土木/汽車修護技工</option> -->
<!-- 									 			<option value="5">翻譯</option> -->
<!-- 									 			<option value="5">寫作</option> -->
<!-- 									 			<option value="5">攝影</option> -->
<!-- 									 			<option value="5">圖畫</option> -->
									 			
<!-- 									 			<option value="6">警局隊長/巡佐</option> -->
<!-- 									 			<option value="6">科員</option> -->
<!-- 									 			<option value="6">基層公務員</option> -->
<!-- 									 			<option value="6">高中/國中/小學教師</option> -->
<!-- 									 			<option value="6">專門技術人員</option> -->
<!-- 									 			<option value="6">廚師</option> -->
<!-- 									 			<option value="6">專業汽車駕駛</option> -->
<!-- 									 			<option value="6">計程車駕駛</option> -->
<!-- 									 			<option value="6">空服員</option> -->
<!-- 									 			<option value="6">船員</option> -->
									 			
<!-- 									 			<option value="7">基層警員</option> -->
<!-- 									 			<option value="7">法/檢察/書記官</option> -->
<!-- 									 			<option value="7">職/辦事/店員</option> -->
<!-- 									 			<option value="7">保全人員</option> -->
<!-- 									 			<option value="7">大樓管理員</option> -->
<!-- 									 			<option value="7">攤商/直銷/仲介</option> -->
									 			
<!-- 									 			<option value="8">軍警/公職約聘人員</option> -->
<!-- 									 			<option value="8">學生</option> -->
<!-- 									 			<option value="8">工友</option> -->
<!-- 									 			<option value="8">駕駛</option> -->
<!-- 									 			<option value="8">清潔人員</option> -->
<!-- 									 			<option value="8">職業運動員</option> -->
<!-- 									 			<option value="8">教練</option> -->
<!-- 									 			<option value="8">農/漁民</option> -->
<!-- 									 			<option value="8">屠宰飼養</option> -->
									 			
<!-- 									 			<option value="9">退役軍警</option> -->
<!-- 									 			<option value="9">退休公務員</option> -->
<!-- 									 			<option value="9">退休教員</option> -->
<!-- 									 			<option value="9">退休職員</option> -->
<!-- 									 			<option value="9">宗教服務</option> -->
<!-- 									 			<option value="9">家管</option> -->
<!-- 											</select> -->
											
<!--  											<input type="hidden" name="CAREER2_str" id="CAREER2_str" value=""> -->
											<label><input class="text-input validate[required]" value="${ result_data.CAREER2 }" name="SRCFUNDDESC" id="SRCFUNDDESC" type="text" size="12" placeholder="(請選擇職稱項目)" disabled/></label>
											<input type="button" value="選擇" class="ttb-sm-btn btn-flat-gray" name="CAREERNO" onClick="window.open('${__ctx}/ONLINE/APPLY/apply_deposit_account_job_name')">
											<input type="hidden" name="CAREER2" id="CAREER2" value="${ result_data.CAREER2 }">
											<input id="validate_CAREER2" name="validate_CAREER2" value="${ result_data.CAREER2 }" type="text" class="text-input validate[required]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</div>
									</span>
								</div>
								<!-- *任職機構 ***** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										任職機構 -->
											<spring:message code="LB.D1074"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="EMPLOYER" name="EMPLOYER"  placeholder="請輸入任職機構" class="text-input validate[required]" 
												value="${ result_data.EMPLOYER }" maxLength="30" size="30" onkeyup="ValidateValue(this)"
												onchange="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5]/g,'')"
												onpaste="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5]/g,'')"/>
										</div>
									</span>
								</div>

								<!-- ***** 起家金帳戶資訊 ***** -->
								<div class="classification-block">
									<p>起家金帳戶</p>
									<p>( <span class="high-light">*</span> 為必填)</p>
								</div>
								<!-- 起家金帳戶 -->
								<div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4>是否申請為起家金帳戶 <span class="high-light">*</span></h4>
	                                    </label>
	                                </span>
									<span class="input-block">
	                                    <div class="ttb-input">
	                                        <label class="radio-block" >同意申請
	                                            <input type="radio" name="STAEDYN" id="STAEDYN1" value="Y" onclick="show_STAEDSQ()"/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                        <label class="radio-block" >不同意
	                                            <input type="radio" name="STAEDYN" id="STAEDYN2" value="N" onclick="show_STAEDSQ()"/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                    </div>
										<div class="ttb-input">
	                                        <span class="input-error-remarks" id="COMFIRMSTAEDSQ_S1">起家金帳戶不得質借</span>
											<span class="input-error-remarks" id="COMFIRMSTAEDSQ_S2">申請起家金帳戶不得同時申請晶片金融卡</span>
										</div>
	                                    <span class="hideblock">
											<input id="validate_STAEDSQ_SHOW" name="STAEDSQ_SHOW" type="radio"
												   class="validate[funcCall[validate_Radio[是否申請為起家金帳戶 , STAEDYN]]]"
												   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;"/>
										</span>
	                                </span>
								</div>
<!-- 								********** -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<%-- 											★<spring:message code="LB.D1135"/> --%>
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<!-- 											<input type="hidden" name="ZIP3" id="ZIP3" value="" size=4 maxlength=3> -->
<!-- 											<select name="CITY3" id="CITY3" class="select-input validate[required]" onchange="doquery5('#CITY3','#ZONE3')"> -->
<%-- 								          		<option value="">--- <spring:message code="LB.Select"/><spring:message code="LB.D0059"/> ---</option>   --%>
<%-- 								          		<c:forEach var="dataList" items="${city_data}"> --%>
<%-- 													<option> ${dataList.CITY}</option> --%>
<%-- 												</c:forEach> --%>
<%-- 											</select><spring:message code="LB.D0060"/> --%>
<!-- 											<select name="ZONE3" id="ZONE3" class="select-input validate[required]"  onchange="doquery6('#CITY3','#ZONE3','#ZIP3' ,'#ZIP3')"> -->
<%-- 								          		<option value="">--- <spring:message code="LB.Select"/><spring:message code="LB.D0060"/> ---</option>  --%>
<!-- 											</select> -->
<!-- 											<select name="BHID" id="BHID" class="select-input validate[required]"> -->
<%-- 								          		<option value="">--- <spring:message code="LB.Select"/><spring:message code="LB.X0169"/> ---</option>   --%>
<%-- 								          		<c:forEach var="dataList" items="${bh_data}"> --%>
<%-- 													<option value="${dataList.ADBRANCHID}"> ${dataList.ADBRANCHNAME}</option> --%>
<%-- 												</c:forEach> --%>
<!-- 											</select> -->
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
<!-- <!-- 								********** --> 
<!-- 								<div class="ttb-input-item row"> -->
<!--  									<span class="input-title">  -->
<!--  									<label> -->
<!--  										<h4> -->
<%-- 											★<spring:message code="LB.D1115"/> --%>
<!--  										</h4> -->
<!--  									</label> -->
<!--  									</span> -->
<!--  									<span class="input-block"> -->
<!--  										<div class="ttb-input"> -->
<!--  											<label class="radio-block"> -->
<%--  												<spring:message code="LB.D1070_1"/> --%>
<!--  												<input type="radio" name="HAS_PIC" id="PIC_FLAG1" value="Y" checked /> -->
<!--  												<span class="ttb-radio"></span> -->
<!--  											</label>&nbsp; -->
<!--  											<label class="radio-block"> -->
<%--  												<spring:message code="LB.D1070_2"/> --%>
<!--  												<input type="radio" name="HAS_PIC" id="PIC_FLAG2" value="N" /> -->
<!--  												<span class="ttb-radio"></span> -->
<!--  											</label> -->
<!--  										</div> -->
<!--  									</span> -->
<!--  								</div> -->
								
                           	</div>
	                        <!--button 區域 -->
							<!-- 重新輸入 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318"/>" class="ttb-button btn-flat-gray">
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080"/>" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>