<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">

	// column's title i18n

	// n190-黃金存摺餘額查詢
	i18n['ACN'] = '<spring:message code="LB.Loan_account" />'; // 帳號
	i18n['TSFBAL'] = '<spring:message code="LB.W1433" />(<spring:message code="LB.W1435" />)'; // 可用餘額(公克)
	i18n['GDBAL'] = '<spring:message code="LB.Account_balance" />(<spring:message code="LB.W1435" />)'; // 帳戶餘額(公克)
	i18n['MKBAL'] = '<spring:message code="LB.W1436" />(<spring:message code="LB.Dollar_1" />)'; // 參考市值(元)
	i18n['AVGCOST'] = '<spring:message code="LB.W1437" />(<spring:message code="LB.Dollar_1" />)'; // 每公克原始投入+參考平均成本(元)
	i18n['QM'] = '<spring:message code="LB.Quick_Menu" />';//快速選單
	
	// DataTable Ajax tables
	var n190_columns = [
		{ "data":"ACN", "title":i18n['ACN'] },
		{ "data":"TSFBAL", "title":i18n['TSFBAL'] },
		{ "data":"GDBAL", "title":i18n['GDBAL'] },
		{ "data":"MKBAL", "title":i18n['MKBAL'] },
		{ "data":"AVGCOST", "title":i18n['AVGCOST'] },
		{ "data":"QM","title":i18n['QM'] }
	];
	
	//0置中 1置右
	var n190_align = [0,1,1,1,1,0];
	
	// Ajax_n190-黃金存摺餘額查詢
	function getMyAssets190() {
		$("#n190").show();
		$("#n190_title").show();
		createLoadingBox("n190_BOX",'');
		uri = '${__ctx}' + "/OVERVIEW/allacct_n190aj";
		console.log("allacct_n190aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax_n190, null, "n190");
	}

	// Ajax_callback
	function countAjax_n190(data){
		// 資料處理後呈現畫面
		if (data.result) {
			n190_rows = data.data.REC;
			//console.log(data);
			$("#n190_CUSIDN").val(data.data.CUSIDN);
			n190_rows.forEach(function(d,i){
				var options = [];
				//明細查詢
				options.push(new Option("<spring:message code='LB.W1442' />", "N191"));
				//單筆買進
				options.push(new Option("<spring:message code='LB.X0921' />", "N09001"));
				//單筆回售
				options.push(new Option("<spring:message code='LB.X0922' />", "N09002"));
				//定期定額申購
				options.push(new Option("<spring:message code='LB.W1553' />", "N09301"));
				//定期定額變更
				options.push(new Option("<spring:message code='LB.W1564' />", "N09302"));
				var qm = $("#actionBar").clone();
				qm.children()[0].id = "n190";
				qm.children()[0].name=i;
				qm.show();
				options.forEach(function(x){
					qm[0].children[0].options.add(x);
				});
				
				d["QM"]=qm.html();
			});
			n190_rows.push({"ACN":"<spring:message code= 'LB.X0923' />","TSFBAL":data.data.TOTAL_TSFBAL,"GDBAL":data.data.TOTAL_GDBAL,"MKBAL":data.data.TOTAL_MKBAL,"AVGCOST":"","QM":""});
		} else {
			// 錯誤訊息
			n190_columns = [ {
					"data" : "MSGCODE",
					"title" : '<spring:message code="LB.Transaction_code" />'
				}, {
					"data" : "MESSAGE",
					"title" : '<spring:message code="LB.Transaction_message" />'
				}
			];
			n190_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];
		}
		createTable("n190_BOX",n190_rows,n190_columns,n190_align);	
	}
	
</script>
<p id="n190_title" class="home-title" style="display: none"><spring:message code= "LB.X0971" /></p>
<div id="n190" class="main-content-block" style="display:none"> 
<div id="n190_BOX"></div>

<ol class="description-list text-left">
<!-- 備註:每公克原始投入參考平均成本計算如說明，且不含投資手續費成本，本項目僅供投資人參考。 -->
<li><spring:message code="LB.W1440"/><a href="${__ctx}/GOLD/PASSBOOK/gold_balance_query_explan" style="color:#007bff;" target=_blank><spring:message code="LB.W1441"/></a>
</li>
</ol>

<form id="formn190" method="post">
<input type="hidden" name="ACN" id="n190_ACN"/> 
<input type="hidden" name="CUSIDN" id="n190_CUSIDN" value=""/> 
<input type="hidden" name="FASTMENU" id="n190_FASTMENU" value=""/> 
</form>
</div>
