<link rel="stylesheet" type="text/css" href="${__ctx}/css/reset.css?a=${jscssDate}">
<link rel="icon" href="${__ctx}/img/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="${__ctx}/img/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="${__ctx}/css/bootstrap-4.1.1.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/font-awesome.min.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/loadingbox.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/footable.bootstrap.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/tbb_common.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/tbb_common_${pageContext.response.locale}.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/tbb_common_edit.css?a=${jscssDate}">  
<link rel="stylesheet" type="text/css" href="${__ctx}/css/hamburgers.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/fonts.css?a=${jscssDate}">
<script type="text/javascript" src="${__ctx}/js/jquery-3.5.1.min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/bootstrap-4.5.3.min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/popper-1.14.3.min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/CGJSCrypt_min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/fstop.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/footable.min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/utility.js?a=${jscssDate}"></script>
<%--  <script type="text/javascript" src="${__ctx}/js/TBB.js"></script> --%>
<script type="text/javascript" src="${__ctx}/js/TBB-${pageContext.response.locale}.js?a=${jscssDate}"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.dataTables.min.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/fixedColumns.dataTables.min.css?a=${jscssDate}">
<script type="text/javascript" src="${__ctx}/js/jquery.dataTables.min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/dataTables.fixedColumns.min.js?a=${jscssDate}"></script>
<!-- masonry -->
<%-- <script src="${__ctx}/js/masonry.pkgd.min.js"></script> --%>
<script src="${__ctx}/js/isotope.pkgd.min.js?a=${jscssDate}"></script>
<script src="${__ctx}/js/fit-columns.js?a=${jscssDate}"></script>
<script>
var dt_locale = "${pageContext.response.locale}";
</script>

<script type="text/JavaScript">
	var isE2E = '${isE2E}';

	// IE不支援jquery.startsWith，故新增
	String.prototype.startsWith = function(searchString, position) {
		position = position || 0;
		return this.substr(position, searchString.length) === searchString;
	};
	// IE不支援jquery.endsWith，故新增
	String.prototype.endsWith = function(searchString, position) {
		var subjectString = this.toString();
		if (typeof position !== 'number' || !isFinite(position)
				|| Math.floor(position) !== position
				|| position > subjectString.length) {
			position = subjectString.length;
		}
		position -= searchString.length;
		var lastIndex = subjectString.indexOf(searchString, position);
		return lastIndex !== -1 && lastIndex === position;
	};
	
	// 客製化alert視窗

	function errorBlock(errortitle, errorcontent, errorinfo, errorBtn1, errorBtn2) {
// 		$("#error-content0").html("");
		$('p[id^=error-]').html("");
		wait1=true;
		setTimeout("wait1=false;", 500);
		$('#error-block').show();
		
		deadOrAlive("error-title", errortitle);
		
		// 置左訊息
		errorBlockMsg("error-content", errorcontent);
		// 置中訊息
		errorBlockMsg("error-info", errorinfo);
		
		
 		deadOrAlive("errorBtn1", errorBtn1);
 		deadOrAlive("errorBtn2", errorBtn2);
		
		$("#errorBtn1").click(function() {
			$('#error-block').hide();
		});
	}
	
	function errorBlock2(errortitle, errorcontent, errorinfo, errorBtn1, errorBtn2) {
		$('p[id^=error-]').html("");
		wait1=true;
		setTimeout("wait1=false;", 500);
		$('#error-block-2').show();
		
		deadOrAlive("error-title-2", errortitle);
		
		// 置左訊息
		errorBlockMsg("error-content-2", errorcontent);
		// 置中訊息
		errorBlockMsg("error-info-2", errorinfo);

		$('#errorBtn2-2').css("background-color", "#aaaaaa");
		$('#errorBtn2-2').css("box-shadow", "0px 4px 10px #00000029");
		
 		deadOrAlive("errorBtn1-2", errorBtn1);
 		deadOrAlive("errorBtn2-2", errorBtn2);
		
		$("#errorBtn1-2").click(function() {
			$('#error-block-2').hide();
			$("#formID").submit();
		});
		
		$("#errorBtn2-2").click(function() {
			$('#error-block-2').hide();
		});
	}
	
	// errorBlock部位保留或刪除
	function deadOrAlive(key, value) {
		if( value!=null ) {
			$("#"+key).html(value);
			$("#"+key).show();
		}else{
			$("#"+key).hide();
		}
	}
	
	// 訊息處理
	function errorBlockMsg(type, strArray) {
		if( strArray!=null ) {
			strArray.forEach(function(str, index) {
				var before = index-1;
				if(index==0){
					deadOrAlive(type+index, str);
				} else {
					var text = document.createElement("p");
					text.id = type+index;
					text.class = type;
					$('#'+type+before).append(text);
					if(str.indexOf("&{red}")!=-1){
						text.style.color="red";
						str=str.replace("&{red}","");
					}
					deadOrAlive(type+index, str);
				}
			});
		}
	}
// 	function closeBlock(){
// 		if($('#error-block').css("display") != "none")
// 			{
// 			$('#error-block').hide();
// 			}
// 	}
	


</script>

<!-- LOADING遮罩區塊 -->
<div id="dialog_box" class="Dialog_box" style ="display: none;
	z-index: 9999;
    padding-top: 22%;
    height: 100%;
    top: 0;
    position: absolute;
    width: 100%;
    text-align: center;
    background-color: rgba(0,0,0,0.6);">
	<div class="sk-fading-circle">
		<div class="sk-circle1 sk-circle"></div>
		<div class="sk-circle2 sk-circle"></div>
		<div class="sk-circle3 sk-circle"></div>
		<div class="sk-circle4 sk-circle"></div>
		<div class="sk-circle5 sk-circle"></div>
		<div class="sk-circle6 sk-circle"></div>
		<div class="sk-circle7 sk-circle"></div>
		<div class="sk-circle8 sk-circle"></div>
		<div class="sk-circle9 sk-circle"></div>
		<div class="sk-circle10 sk-circle"></div>
		<div class="sk-circle11 sk-circle"></div>
		<div class="sk-circle12 sk-circle"></div>
	</div>
</div>

<!-- LOADING遮罩區塊 -->
<div id="loadingBox" class="Loading_box">
	<div class="sk-fading-circle">
		<div class="sk-circle1 sk-circle"></div>
		<div class="sk-circle2 sk-circle"></div>
		<div class="sk-circle3 sk-circle"></div>
		<div class="sk-circle4 sk-circle"></div>
		<div class="sk-circle5 sk-circle"></div>
		<div class="sk-circle6 sk-circle"></div>
		<div class="sk-circle7 sk-circle"></div>
		<div class="sk-circle8 sk-circle"></div>
		<div class="sk-circle9 sk-circle"></div>
		<div class="sk-circle10 sk-circle"></div>
		<div class="sk-circle11 sk-circle"></div>
		<div class="sk-circle12 sk-circle"></div>
	</div>
</div>

<section id="error-block" class="error-block" style="display:none">
	<div class="error-for-message">
		<p id="error-title" class="error-title" style="display:none"></p>
		<p id="error-content0" class="error-content"></p>
		<p id="error-info0" class="error-info"></p>
		<button id="errorBtn1" class="btn-flat-orange ttb-pup-btn" style="display:none"></button>
		<button id="errorBtn2" class="btn-flat-orange ttb-pup-btn" style="display:none"></button>
	</div>
</section>

<section id="error-block-2" class="error-block" style="display:none">
	<div class="error-for-message">
		<p id="error-title-2" class="error-title" style="display:none"></p>
		<p id="error-content-20" class="error-content"></p>
		<p id="error-info-20" class="error-info"></p>
		<button id="errorBtn1-2" class="btn-flat-orange ttb-pup-btn" style="display:none"></button>
		<button id="errorBtn2-2" class="btn-flat-orange ttb-pup-btn" style="display:none"></button>
	</div>
</section>
