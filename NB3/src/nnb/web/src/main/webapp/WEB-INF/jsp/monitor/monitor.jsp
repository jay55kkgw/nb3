<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
    <%@ include file="../__import_head_tag.jsp"%>
    <%@ include file="../__import_js.jsp" %>

    <style>
        td {

            width: 20%;
            text-align: center;
        }

        td input {
            text-align: center;
            color: black;
        }
    </style>
</head>

<body>
    <div class="content row">
        <main class="col-12">
            <section id="main-content" class="container">
                <form id="formId" method="post" action="">
                    <div class="main-content-block row">
                        <div class="col-12 tab-content">
                            <legend class="mt-3">
                                SYSTEM_STATUS:&nbsp;&nbsp;&nbsp;
                                <input type="text" id="sys_status" name="sys_status"
                                    style="color:white ;width: 300px ;text-align:center;" value="CHECKING..."
                                    readonly="readonly" />
                                <input type="text" id="SYSTIME" name="SYSTIME"
                                    style="width: 300px ;text-align:center;border:0px;" value="${SYSTIME}"
                                    readonly="readonly" />

                            </legend>
                            <br><br>
                            <center>
                                <table>
                                    <thead>
                                        <tr style="text-align:center;">
                                            <td>System name</td>
                                            <td>System health status</td>
                                            <td>DB connection</td>
                                            <td>MS to TMRA</td>
                                            <td>LAST boot time</td>
                                            <!-- <td>LOG Detail</td> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="header">MS_TW :</td>
                                            <td>
                                                <input type="text" id="ms1" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msdb1" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msteL1" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msboot1" name="app_status"
                                                    style="color:black;border:0px;" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <!-- <td>
											<input type="button" id="msdb12" width="100%" name="app_status"
												value="more" onclick="twdetail();"/>
										</td> -->
                                        <tr>
                                            <td class="header">MS_PS :</td>
                                            <td>
                                                <input type="text" id="ms2" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msdb2" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msteL2" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msboot2" name="app_status"
                                                    style="color:black;border:0px;" value=""
                                                    readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header">MS_PAY :</td>
                                            <td>
                                                <input type="text" id="ms3" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msdb3" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msteL3" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msboot3" name="app_status"
                                                    style="color:black;border:0px;" value=""
                                                    readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header">MS_OLS :</td>
                                            <td>
                                                <input type="text" id="ms4" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msdb4" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msteL4" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msboot4" name="app_status"
                                                    style="color:black;border:0px;" value=""
                                                    readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header">MS_OLA :</td>
                                            <td>
                                                <input type="text" id="ms5" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msdb5" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msteL5" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msboot5" name="app_status"
                                                    style="color:black;border:0px;" value=""
                                                    readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header">MS_LOAN :</td>
                                            <td>
                                                <input type="text" id="ms6" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msdb6" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msteL6" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msboot6" name="app_status"
                                                    style="color:black;border:0px;" value=""
                                                    readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header">MS_GOLD :</td>
                                            <td>
                                                <input type="text" id="ms7" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msdb7" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msteL7" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msboot7" name="app_status"
                                                    style="color:black;border:0px;" value=""
                                                    readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header">MS_FX :</td>
                                            <td>
                                                <input type="text" id="ms8" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msdb8" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msteL8" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msboot8" name="app_status"
                                                    style="color:black;border:0px;" value=""
                                                    readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header">MS_FUND :</td>
                                            <td>
                                                <input type="text" id="ms9" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msdb9" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msteL9" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msboot9" name="app_status"
                                                    style="color:black;border:0px;" value=""
                                                    readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header">MS_CC :</td>
                                            <td>
                                                <input type="text" id="ms10" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msdb10" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msteL10" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                                <input type="text" id="msboot10" name="app_status"
                                                    style="color:black;border:0px;" value=""
                                                    readonly="readonly" />
                                            </td>
                                        </tr>
                                        <%-- 									<tr> --%>
                                        <%-- 										<td class="header" >MS_TEST :</td> --%>
                                        <%-- 										<td> --%>
                                        <!-- 											<input type="text" id="test-serverstatus" name="app_status" -->
                                        <!-- 												 value="CHECKING" readonly="readonly"/> -->
                                        <%-- 										</td> --%>
                                        <%-- 										<td> --%>
                                        <!-- 											<input type="text" id="test-db" name="app_status" -->
                                        <!-- 												 value="CHECKING" readonly="readonly"/> -->
                                        <%-- 										</td> --%>
                                        <%-- 										<td> --%>
                                        <!-- 											<input type="text" id="test-tel" name="app_status" -->
                                        <!-- 												 value="CHECKING" readonly="readonly"/> -->
                                        <%-- 										</td> --%>
                                        <%-- 										<td> --%>
                                        <!-- 											<input type="text"id="test-boot" name="app_status" style="color:black;border:0px;" -->
                                        <!-- 												value="CHECKING" readonly="readonly"/> -->
                                        <%-- 										</td> --%>
                                        <%-- 									</tr> --%>
                                        <tr>
                                            <td class="header">TMRA :</td>
                                            <td>
                                                <input type="text" id="ms00" name="app_status" value=""
                                                    readonly="readonly" />
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <!--注意是小寫-->
                                                <input type="text" id="msboot00" name="tmra_status"
                                                    style="color:black;border:0px;" value=""
                                                    readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header">NB3_DB :</td>
                                            <td>
                                                <input type="text" id="NB_DB_status" name="NB_DB_status"
                                                    value="" readonly="readonly" />
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <!-- <tr>
										<td colspan='5'>
										</td>
									</tr>
									<tr>
										<td colspan='5'>
											<textarea name="w3review" class="col-8 mt-2" rows="10"  id="logoutput">
											</textarea>
										</td>
									</tr> -->
                                        <tr>
                                            <td class="header"></td>
                                            <td>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <label class="check-block">
                                                    <input type="checkbox" id="reload_check" class="" value="" checked>
                                                    自動刷新
                                                    <span class="ttb-check"></span>
                                                </label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>


                            </center>

                            <button type="button" id="reload" class="mt-2 ttb-button btn-flat-orange">刷新</button>
                        </div>
                    </div>
                </form>
            </section>
        </main>
    </div>
    <script type="text/javascript">
        var SystemCheckingFlag = 0;
        $(document).ready(function () {
            setTimeout("reloadwindo()", 60 * 1000);
            $('#reload').click(function () {
                window.location.reload();
                // HTML載入完成後開始遮罩
                setTimeout("initBlockUI()", 10);
            });
            //set default view
//             initialblock();
            $('#sys_status').css("background-color", "orange");
            $('#NB_DB_status').css("background-color", "orange");
            /**
             *gocheck
             *
             *@param
             *service name
             *tag id
             */
            checkingAjax("MS_TW", "1");
            checkingAjax("TMRA", "00");

            setTimeout(" checkingAjax('MS_PS', '2');", 100);
            setTimeout(" checkingAjax('MS_PAY', '3');", 100);
            setTimeout(" checkingAjax('MS_OLS', '4');", 500);
            setTimeout(" checkingAjax('MS_OLA', '5');", 500);

            setTimeout(" checkingAjax('MS_LOAN', '6');", 1000);
            setTimeout(" checkingAjax('MS_GOLD', '7');", 1000);
            setTimeout(" checkingAjax('MS_FX', '8');", 1500);
            setTimeout(" checkingAjax('MS_FUND', '9');", 1500);
            setTimeout(" checkingAjax('MS_CC', '10');", 2000);



        });

        function initialblock() {
            for (var i = 1; i < 11; i++) {
                if ($("#ms" + i).val() == "healthy") {
                    $("#ms" + i).css("background-color", "green");
                } else {
                    $("#ms" + i).css("background-color", "red");
                }
                if ($("#msteL" + i).val() == "true") {
                    $("#msteL" + i).css("background-color", "green");
                } else {
                    $("#msteL" + i).css("background-color", "red");
                }
                if ($("#msdb" + i).val() == "true") {

                    $("#msdb" + i).css("background-color", "green");
                } else {
                    $("#msdb" + i).css("background-color", "red");
                }
            }
            $("#ms00").css("background-color", "red");
            $('#sys_status').css("background-color", "red");
            $('#NB_DB_status').css("background-color", "red");
        }

        function reloadwindo() {
            if ($("#reload_check").is(':checked')) {
                initBlockUI();
                window.location.reload();

            } else setTimeout("reloadwindo()", 6000);
        }
        // function twdetail(){
        // 	$('#logoutput').val("");
        // }
        function checkingAjax(service, id) {
            var check_aj_uri = '${__ctx}' + "/MONITOR/service_checking_ajax";
            console.log("service: >>" + service);
            $('#ms' + id).css("background-color", "orange");
            $('#msdb' + id).css("background-color", "orange");
            $('#msteL' + id).css("background-color", "orange");                       
            var rdata = {
                service: service
            };
            $.ajax({
                type: "POST",
                async: true,
                url: check_aj_uri,
                data: rdata,
                dataType: "json",
                success: function (msg) {
                    SystemCheckingFlag++;
                    tempset(msg, id);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    SystemCheckingFlag++;
                    if (window.console) {
                        console.log("系統異常，請稍候再試，或洽相關資訊人員，error_code=" + jqXHR.status);
                        systemState();
                    }
                    if (callback) {
                        callback("系統異常，請稍候再試，或洽相關資訊人員，error_code=" + jqXHR.status, options);
                        systemState();
                    }
                }
            });
        }

        function tempset(msg, id) {
            if (msg.data != null) {
            	//setting value
                $('#ms' + id).val(msg.data.Service_Status.server_STATUS);
                $('#msdb' + id).val(msg.data.Service_Status.db_STATUS);
                $('#msteL' + id).val(msg.data.Service_Status.tel_STATUS);
                $('#msboot' + id).val(msg.data.Service_Status.boot_TIME);
                //change view
                if ($('#ms' + id).val() == "healthy") {
                    $('#ms' + id).css("background-color", "green");
                } else {
                    $('#ms' + id).css("background-color", "red");
                }
                if ($('#msdb' + id).val() == "true") {
                    $('#msdb' + id).val("connected");
                    $('#msdb' + id).css("background-color", "green");
                } else {
                    $('#msdb' + id).css("background-color", "red");
                }
                if ($('#msteL' + id).val() == "true") {
                    $('#msteL' + id).val("connected");
                    $('#msteL' + id).css("background-color", "green");
                } else {
                    $('#msteL' + id).css("background-color", "red");
                }
				//check dbconnect
                if (msg.data.turnDB == "true") {
                    $('#NB_DB_status').val("連線正常");
                    $('#NB_DB_status').css("background-color", "green");
                } else {
                    $('#NB_DB_status').val(msg.data.turnDB);
                    $('#NB_DB_status').css("background-color", "red");
                }
            }
            systemState();
        }

        function systemState() {
            console.log("SystemCheckingFlag >>" + SystemCheckingFlag);
            if (SystemCheckingFlag == 11) {
                //比對是否全部正常
                var systemNormalFlag = 0;
                for (var i = 1; i < 11; i++) {
                    if ($("#ms" + i).val() == "healthy" && $("#msdb" + i).val() == "connected" && $("#msteL" + i).val() == "connected") {
                        systemNormalFlag++;
                    }
                }
                if ($("#ms00").val() == "healthy") {
                    systemNormalFlag++;
                }
                if (systemNormalFlag == 11 && $('#NB_DB_status').val() == "連線正常") {
                    $("#sys_status").val("系統正常");
                    $('#sys_status').css("background-color", "green");

                } else {
                    $("#sys_status").val("系統異常");
                    $('#sys_status').css("background-color", "red");
                }
            }
        }
    </script>
</body>

</html>