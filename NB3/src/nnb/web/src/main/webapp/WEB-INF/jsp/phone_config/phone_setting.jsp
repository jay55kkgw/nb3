<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後0.1秒開始遮罩
		setTimeout("initBlockUI()", 100);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		setTimeout("init2()", 20);
		// 建立轉入帳號下拉選單
		creatOutACNO();
		// 過0.5秒解遮罩
		setTimeout("unBlockUI(initBlockId)", 800);
	});

	
	/**
	 * 初始化BlockUI
	 */
	function initBlockUI() {
		initBlockId = blockUI();
	}

    function init(){

    	$('input[type=radio][name=binddefault]').change(function() {
        	$("#validate_binddefault").val(this.value);
    	});
        
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
    	$("#CMSUBMIT").click(function(e){			
			console.log("submit~~");

			if(!chkclick()){
				errorBlock(null, null, ["請審閱條款及勾選"],
						'<spring:message code= "LB.Confirm" />', null);
			}
			else{
				if (!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}else{
	 				console.log("submit~~");
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	   				var action = '${__ctx}/PNONE/CONFIG/setting_confirm';
	   				$('#formId').attr("action", action);
	    			unBlockUI(initBlockId);
	    			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
	    			$('#formId').submit();
	 			}
			}
		});
    }	

//	建立約定轉出帳號下拉選單
	function creatOutACNO(){
		var options = { keyisval:false ,selectID:'#DPAGACNO'};
		uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getOutAcno_aj"
		console.log("getSelectData>>" + uri);
		rdata = {type: 'acno' };
		console.log("rdata>>" + rdata);
		data = fstop.getServerDataEx(uri,rdata,false);
		console.log("data>>", data);
		if(data !=null && data.result == true ){
			fstop.creatSelect(data.data,options);
		}else{
			errorBlock(
					null, 
					null,
					['<spring:message code= 'LB.Alert004' />'], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	}
    
    



	/**************************************/
	/*     			 發送簡訊   			  */
	/**************************************/
	var sec = 120;
	var the_Timeout;

	function countDown()
	{
	     var main = document.getElementById("formId");
	                
	      var counter = document.getElementById("CountDown");                      
	       counter.innerHTML = sec;
	       sec--;
	       
	       if (sec == -1) 
	       {              		
				$("#getotp").prop("disabled",false);
				$("#getotp").removeClass("btn-flat-gray");
				$("#getotp").addClass("btn-flat-orange");
			   sec =120;

	           return;                
	       }                                     
	           
	       //網頁倒數計時(120秒)              
	       the_Timeout = setTimeout("countDown()", 1000);    
	} 
	    //取得otp並發送簡訊
	    function getsmsotp() {
			var main = document.getElementById("formId");
	 		var cusidn = "${transfer_data.data.cusidn}";
			if(cusidn.length==0)
		    {
		    	errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.D0025' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		    	main.CUSIDN.focus();
		    	return false;
		    }				
			$("#getotp").prop("disabled",true);
			$("#getotp").removeClass("btn-flat-orange");
			$("#getotp").addClass("btn-flat-gray");
			sec = 120;
			countDown();
			$.ajax({
		        type :"POST",
		        url  : "${__ctx}/RESET/smsotp_ajax",
		        data : { 
		        	CUSIDN: cusidn,
		        	//因資料庫只有6碼，改為N070C
		        	ADOPID: 'N070C',
		        	MSGTYPE: 'PNTF'
		        },
		        async: false ,
		        dataType: "json",
		        success : function(msg) {
		        	callbackgetsmsotp(msg)
		        }
		    })
		}
		function callbackgetsmsotp(response) {
			//alert(response.responseText);
		    var main = document.getElementById("formId");
			//eval("var result = " + response.responseText);
			console.log(response);
			var msgCode = response[0].MSGCOD;
			var msgName = response[0].MSGSTR;
			var phone = response[0].phone;

			if(msgCode=="0000")
			{	
				phone = phone = phone.substring(0,6)+"***"+phone.substring(9);
			    //alert("<spring:message code= "LB.Alert198" />");
			    errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X2280' />"+phone+"<br/><spring:message code= 'LB.X2281' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			    main.OTP.focus();			
			}
			else
			{
				sec = 0;
	      		var counter = document.getElementById("CountDown");                      
	       		counter.innerHTML = sec;		
				$("#getotp").prop("disabled",false);
				$("#getotp").removeClass("btn-flat-gray");
				$("#getotp").addClass("btn-flat-orange");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.error_code' />" + ":" + msgCode + " " + msgName], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}				
		}

			
	
		/**************************************/
		/*     		手機轉帳服務約定條款	 	  */
		/**************************************/
			function agreeRead(data){
				var chk = "ReadFlag" + data.replace("read","");
				$("#"+chk).prop('checked',true);
			}
			function chkclick(){
				if($('#ReadFlag1').prop('checked')){
					return true;
				}
				else{
					return false;
				}
			}
			function init2() {
				$("input[name^='ReadFlag']").on('click', function() {
					var clickName = this.id;
					if($("#"+clickName).prop('checked')){
						if(clickName == "ReadFlag1"){
							$("#terms1").show();
						}
						return false;
					}
				});
			}
	</script>
</head>
<body>

		<!-- 手機門號轉帳服務約定條款 -->
		<section id="terms1" class="error-block" style="display:none">
		<div class="error-for-message" style="max-width:80%;">
			<p class="error-title" style="margin-bottom: 0px">手機門號轉帳服務約定條款</p>
<!-- 			<p class="error-content"> -->
				<ul class="ttb-result-list terms">
					<li data-num="" style="overflow-x: auto; padding-bottom: 0px;">
						<span class="input-subtitle subtitle-color">
						<div class="CN19-clause" style="width: 100%; border: 1px solid #D7D7D7; border-radius: 6px; overflow-x: auto;">
							<%@ include file="../term/N750C.jsp"%>
						</div>
					</li>
				</ul>
<!-- 			</p> -->
			<input type="button" id="read1" value="已審閱並同意" class="ttb-button btn-flat-orange " onclick="agreeRead(this.id);$('#terms1').hide();"/>
		</div>
	</section>

	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	<!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 手機門號收款帳號設定    -->
			<li class="ttb-breadcrumb-item active" aria-current="page">手機門號收款帳號設定</li>
		</ol>
	</nav>
	
	<!-- menu、登出窗格 -->
	<div class="content row">
		<c:if test="${sessionScope.cusidn != null}">
			<%@ include file="../index/menu.jsp"%>
		</c:if>
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					手機門號收款帳號設定
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
<!-- 				<div id="step-bar"> -->
<!-- 					<ul> -->
<%-- 						<li class="active"><spring:message code="LB.Enter_data" /></li> --%>
<%-- 						<li class=""><spring:message code="LB.Confirm_data" /></li> --%>
<%-- 						<li class=""><spring:message code="LB.Transaction_complete" /></li> --%>
<!-- 					</ul> -->
<!-- 				</div> -->
				<form method="post" id="formId">
					<!-- 不確定  -->
					<input type="hidden" name="ADOPID" id="ADOPID" value="MtpUser"/>
					<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
					<input type="hidden" name="CHIP_ACN" id="CHIP_ACN" value="">
					<input type="hidden" name="UID" id="UID" value="">
					<input type="hidden" name="ACN" id="ACN" value="">
					<input type="hidden" name="ISSUER" id="ISSUER" value="">
					<input type="hidden" name="ACNNO" id="ACNNO" value="">
					<input type="hidden" name="OUTACN" id="OUTACN" value="">
					<input type="hidden" name="iSeqNo" id="iSeqNo" value="">
					<input type="hidden" name="ICSEQ" id="ICSEQ" value="">
					<input type="hidden" name="TAC" id="TAC" value="">
					<input type="hidden" name="TRMID" id="TRMID" value="">
					<input type="hidden" name="TRANSEQ" id="TRANSEQ" value="2500">
					
					<!-- 功能參數 -->
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					<input type="hidden" name="mobilephone" id="mobilephone" value="${transfer_data.data.mobilephone}">
					
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<!-- 手機號碼 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											手機號碼
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.mobilephone}
										</div>
									</span>
								</div>
							</div>
							<!-- 綁定收款帳號 -->
	                        <div class="ttb-input-block">
		                    	<div class="ttb-input-item row">
		                             <span class="input-title">
		                                 <label>
		                                    	 綁定收款帳號
		                                 </label>
		                              </span>
		                              <span class="input-block">
		                                <div class="ttb-input">
											<select id="DPAGACNO" name="DPAGACNO" class="custom-select select-input half-input validate[required]">
											</select>
		                                </div>
		                              </span>
			                	</div>
							</div>
						 <!-- 預設收款帳戶 -->
						 <div class="ttb-input-block">
		                    	<div class="ttb-input-item row">
		                             <span class="input-title">
		                                 <label>
		                                 	同意作為預設收款帳號
		                                 </label>
		                              </span>
		                              <span class="input-block">
		                                <div class="ttb-input">
											<input type="radio" id="AGREE" name="binddefault" value="Y" checked>
												同意預設<br> <font color='red'>您的朋友只要輸入您的手機門號，無需輸入收款行，即可轉帳至您綁定之帳號。</font>
											<br>
											<br>
											<input type="radio" id="DISAGREE" name="binddefault" value="N" >
		                                		暫不預設<br><font color='red'>您的朋友需同時 輸入您的手機門號及收款行(050)，方可轉帳至您綁定之帳號。</font>
												<span id="hideblocka" name="hideblock">
													<input id="validate_binddefault" name="validate_binddefault" type="text" class="text-input validate[required]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" autocomplete="off" value ="Y"/>
			                           		 	</span>
		                                </div>
		                              </span>
			                	</div>
							</div>
							
                            <!-- OTP -->
								<div class="ttb-input-block">
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
		                                    <label>
		                                        <h4><spring:message code= "LB.X1574" /></h4>
		                                    </label>
		                                </span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <input class="text-input validate[required,funcCall[validate_CheckLenEqual['<spring:message code= "LB.Captcha" />',OTP,false,8,8]],funcCall[validate_CheckNumber['<spring:message code= "LB.Captcha" />',OTP]]]" type="text" name="OTP" id="OTP" maxlength="8" size="10">
		                                        <br>&nbsp;<spring:message code= "LB.X1575" />： <font id="CountDown" color="#FF0000"></font><spring:message code= "LB.Second" />
												<div class="login-input-block">
													<input type="button" name="getotp" id="getotp" class="ttb-sm-btn btn-flat-orange" onclick="getsmsotp()" value="<spring:message code= "LB.X1576" />" />
												</div>
		                                    </div>	
		                                </span>
		                            </div>
								</div>

							<!-- 手機門號轉帳服務約定條款 -->
							<div class="ttb-input-block">
		                    	<div class="ttb-input-item row">
		                             <span class="input-title">
		                                 <label>
		                                 
		                                 </label>
		                              </span>
		                              <span class="input-block">
		                                         <div class="ttb-input">
												<label class="check-block" for="ReadFlag1">
													<input type="checkbox" name="ReadFlag" id="ReadFlag1">
													<b>本人同意<font class="high-light">手機門號轉帳服務約定條款</font></b>
					                                <span class="ttb-check"></span>
												</label>
											</div>
		                              </span>
			                	</div>
							</div>
						
						<!-- 開始綁定 -->
						<input type="button" id="CMSUBMIT" value="開始綁定" class="ttb-button btn-flat-orange"/>
					</div>
				</div>
					<!-- 說明 -->
					<ol class="description-list list-decimal">
						<li>若您變更基本資料之手機門號，原已設定之綁定帳號將同時註銷，需重新申請。</li>
						<li>如欲變更手機門號，請依下列方式辦理變更基本資料之手機號碼：<br>
							(1)臨櫃：攜帶雙證件及印鑑親赴任一營業單位辦理。<br>
							(2)網路銀行：登入網路銀行後於個人服務->通訊資料變更->異動全行往來業務之基本資料項下使用晶片金融卡+讀卡機進行手機門號變更。<br>
							(3)行動銀行：登入行動銀行後於更多->設定->通訊地址與電話變更項下使用安控機制進行手機門號變更。
						</li>
			        </ol>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>