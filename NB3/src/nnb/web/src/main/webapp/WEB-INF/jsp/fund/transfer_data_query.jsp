<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="bs" value="${transfer_data_query.data}"></c:set>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

		function init() {
			
			datetimepickerEvent();

			// 初始化時隱藏span
			$("#hideblocka").hide();
			$("#hideblockb").hide();			
			
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
			
			$("#CMSUBMIT").click(function (e) {
				//打開驗證隱藏欄位
				$("#hideblocka").show();
				$("#hideblockb").show();
				//塞值進span內的input
				$("#Monthly_DateA").val($("#CMSDATE").val());
				$("#Monthly_DateB").val($("#CMEDATE").val());				
				
// 				// 日期驗證 
// 				if(checkTimeRange() == false )
// 				{
// 					return false;
// 				}
			   	  
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}
				else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				$("#formId").attr("action","${__ctx}/FUND/QUERY/transfer_data_query_result");
	 	  			$("#formId").submit(); 
	 			}
			});
		}
		
		// 日曆欄位參數設定
		function datetimepickerEvent() {

			$(".CMSDATE").click(function (event) {
				$('#CMSDATE').datetimepicker('show');
			});
			$(".CMEDATE").click(function (event) {
				$('#CMEDATE').datetimepicker('show');
			});

			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
		
		// TODO 將日期驗證功能做得更完善
		// 日期驗證
/*		function checkTimeRange() {
			console.log($('#CMEDATE').val());
			console.log($('#CMSDATE').val());
			var now = new Date(Date.now());
			var sixMm = 15778800000;
			var startT = new Date($('#CMSDATE').val());
			var endT = new Date($('#CMEDATE').val());
			var distance = now - startT;
			var range = endT - startT;
			var distanceE = now - endT;

			var limitS = new Date(now - sixMm);
			if (distance > sixMm) {
				var m = limitS.getMonth() + 1;
				var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
				// 起始日不能小於
				var msg = '<spring:message code="LB.Start_date_check_note_1" />' + time;
				alert(msg);
				return false;
			} else if (distance < 0) {
				var m = (now.getMonth()) + 1;
				var time = now.getFullYear() + '/' + m + '/' + now.getDate();
				// 起始日不能大於
				var msg = '起始日不能大於' + time;
				alert(msg);
				return false;
			} else {
				if (range < 0) {
					var msg = '終止日不能小於起始日';
					alert(msg);
					return false;
				} else if (distanceE < 0) {
					var m = now.getMonth() + 1;
					var time = now.getFullYear() + '/' + m + '/' + now.getDate();
					// 終止日不能大於
					var msg = '<spring:message code="LB.End_date_check_note_1" />' + time;
					alert(msg);
					return false;
				}
			}
			return true;
		}*/
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 基金交易資料查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0943" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 快速選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.W0943" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<form id="formId" method="post" action="">
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<!-- 查詢時間  -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code="LB.Inquiry_time" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-block"> 
												<div class="ttb-input">
													<span>${bs.CMQTIME }</span>
												</div>
											</span> 
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<!-- 查詢期間  -->
									<span class="input-title"> 
										<label><h4><spring:message code="LB.Inquiry_period_1" /></h4></label>
									</span> 
									<span class="input-block">
										<!-- 期間起日 -->
										<div class="ttb-input">	
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_start_date" />
											</span>
											<input type="text" id="CMSDATE" name="CMSDATE" value="${SystemDate }" class="text-input datetimepicker" maxlength="10" /> 
											<span class="input-unit CMSDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											
											<!-- 不在畫面上顯示的span -->
											<span id="hideblocka" >
											<!-- 驗證用的input -->
											<input id="Monthly_DateA" name="Monthly_DateA" type="text" class="text-input validate[required, verification_date[Monthly_DateA]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										<!-- 迄日 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_end_date" />
											</span>
											<input type="text" id="CMEDATE" name="CMEDATE" value="${SystemDate }" class="text-input datetimepicker" maxlength="10" /> 
											<span class="input-unit CMEDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span id="hideblockb" >
											<!-- 驗證用的input -->
											<input id="odate" name="odate" value="${SystemDate }"  type="text" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.X1445" />', odate, CMSDATE, CMEDATE, false, null, 6]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
							</div>
							
							<!-- 網頁顯示-->
							<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Display_as_web_page" />"/>
						</div>
					</div>
					<!-- 說明 -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><span><spring:message code="LB.Transfer_Data_Query_P1_D1" /></span></li>
					</ol>
				</form>
			</section>
		</main>
		<!-- main-content END -->
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>