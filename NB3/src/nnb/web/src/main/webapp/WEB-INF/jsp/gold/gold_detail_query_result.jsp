<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
    <%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <script type="text/javascript">
    //20190830刪除多餘JScode
        $(document).ready(function () {
            //initFootable(); // 將.table變更為footable 
            setTimeout("initDataTable()",100);
            init();
        });
        function init() {
            //列印
        	$("#printbtn").click(function(){
				var params = {
						"jspTemplateName":"gold_detail_query_print",
						//黃金存摺明細查詢
						"jspTitle":'<spring:message code="LB.W1442" />',
						"CMQTIME":"${gold_detail_query_result.data.CMQTIME}",
						"CMPERIOD":"${gold_detail_query_result.data.CMPERIOD}",
						"CMRECNUM":"${gold_detail_query_result.data.CMRECNUM}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/GOLD/PASSBOOK/gold_detail_query';
    			$('#back').val("Y");
    			$("form").attr("action", action);
    			initBlockUI();
    			$("form").submit();
    		});
        }
        
    	//選項
	 	function formReset() {
    		if ($('#actionBar').val()=="excel"){
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/gold_detail_query.xls");
	 		}else if ($('#actionBar').val()=="txt"){
				$("#downloadType").val("TXT");
			    $("#templatePath").val("/downloadTemplate/gold_detail_query.txt");
	 		}
    		$("#formId").attr("target", "");
			$("#formId").submit();
			$('#actionBar').val("");
		}
	 	function finishAjaxDownload(){
			$("#actionBar").val("");
			unBlockUI(initBlockId);
		}
    </script>
    <style>
    .first-title {
    max-width: 15%;
    min-width: 15%;
    width: 15%;
}
    .second-title {
    max-width: 20%;
    min-width: 20%;
    width: 20%;
}
    </style>
</head>

<body>
    <!-- header     -->
    <header>
        <%@ include file="../index/header.jsp"%>
    </header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 查詢服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.use_component_P1_D1-1" /></li>
    <!-- 黃金存摺明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1442" /></li>
		</ol>
	</nav>

    <!-- menu、登出窗格 -->
    <div class="content row">
        <!-- 功能選單內容 -->
        <%@ include file="../index/menu.jsp"%>
        <!-- 	快速選單及主頁內容 -->
        <main class="col-12">
            <!-- 		主頁內容  -->
            <section id="main-content" class="container">
            <!-- 黃金存摺明細查詢 -->
                <h2> <spring:message code="LB.W1442"/> </h2>
                <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>	
                <form id="formId" action="${__ctx}/download" method="post">
                <input type="hidden" id="back" name="back" value="">
                    <!-- 下載用 -->
                    <!-- 下載檔名:黃金存摺明細查詢 -->
							<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W1442" />"/>
                    <input type="hidden" name="CMQTIME" value="${gold_detail_query_result.data.CMQTIME}" />
                    <input type="hidden" name="CMPERIOD" value="${gold_detail_query_result.data.CMPERIOD}" />
                    <input type="hidden" name="CMRECNUM" value="${gold_detail_query_result.data.CMRECNUM}" />
                    <input type="hidden" name="downloadType" id="downloadType" />
                    <input type="hidden" name="templatePath" id="templatePath" />
                   <input type="hidden" name="hasMultiRowData" value="true"/>
                    <!-- EXCEL下載用 -->
                    <input type="hidden" name="headerRightEnd" value="8" />
                    <input type="hidden" name="headerBottomEnd" value="4" />
                    <input type="hidden" name="multiRowStartIndex" value="8" />
                    <input type="hidden" name="multiRowEndIndex" value="8" />
                    <input type="hidden" name="multiRowCopyStartIndex" value="6" />
                    <input type="hidden" name="multiRowCopyEndIndex" value="9" />
                    <input type="hidden" name="multiRowDataListMapKey" value="TABLE" />
                    <input type="hidden" name="rowRightEnd" value="8" />
                    <!-- TXT下載用 -->
                    <input type="hidden" name="txtHeaderBottomEnd" value="6"/>
					<input type="hidden" name="txtMultiRowStartIndex" value="11"/>
					<input type="hidden" name="txtMultiRowEndIndex" value="11"/>
					<input type="hidden" name="txtMultiRowCopyStartIndex" value="7"/>
					<input type="hidden" name="txtMultiRowCopyEndIndex" value="12"/>
					<input type="hidden" name="txtMultiRowDataListMapKey" value="TABLE"/>
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 tab-content">
                            <ul class="ttb-result-list">
                                <li>
                                    <!-- 查詢時間 -->
                                        <h3> <spring:message code="LB.Inquiry_time" /> : </h3>
                                        <p> ${gold_detail_query_result.data.CMQTIME }  </p>
                                </li>
                                <li>   
                                    <!-- 查詢期間 -->
                                    <h3><spring:message code="LB.Inquiry_period" /> :</h3>
                                    <p>${gold_detail_query_result.data.CMPERIOD }</p>
                                    <!-- 資料總數 : -->
                                </li>
                                <li>
                                    <h3><spring:message code="LB.Total_records" /> :</h3>
                                    <p>${gold_detail_query_result.data.CMRECNUM} <!--筆 --><spring:message code="LB.Rows" />
                                    </p>
                                </li>
                            </ul>
                            <!-- 表格區塊 -->
                            <c:forEach var="dataTable" items="${gold_detail_query_result.data.REC}">
                                <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                                    <thead>
                                     <ul class="ttb-result-list">
                            <li>
                                <h3><spring:message code="LB.Account" /></h3>
                                <p>${dataTable.ACN}</p>
                            </li>
                        </ul>
                                    <!-- 
                                        <tr>
                                            <th class="study-title">
                                                <spring:message code="LB.Account" />
                                            </th>
                                            <th class="text-left" colspan="7">${dataTable.ACN }</th>
                                        </tr>
                                     -->
                                        <tr>
                                            <!--日期-->
                                            <th data-title="<spring:message code="LB.W1155" />" class="first-title">
                                                <spring:message code="LB.W1155" />
                                            </th>
                                            <!-- 摘要 -->
                                             <th data-title="<spring:message code="LB.Summary_1" />" class="second-title" >
                                                <spring:message code="LB.Summary_1" />
                                            </th>
                                            <!--支出(公克)-->
                                            <th data-title="<spring:message code="LB.W0008" />(<spring:message code="LB.W1435" />)" data-breakpoints="xs sm">
                                                <spring:message code="LB.W0008" />(<spring:message code="LB.W1435" />)
                                            </th>
                                            <!--存入(公克) -->
                                            <th data-title="<spring:message code="LB.W1457" />(<spring:message code="LB.W1435" />)" data-breakpoints="xs sm">
                                                <spring:message code="LB.W1457" />(<spring:message code="LB.W1435" />)
                                            </th>
                                            <!-- 結存(公克) -->
                                            <th data-title="<spring:message code="LB.W0010" />(<spring:message code="LB.W1435" />)" data-breakpoints="xs sm">
                                                <spring:message code="LB.W0010" />(<spring:message code="LB.W1435" />)
                                            </th>
                                            <!-- 單價(元) -->
                                            <th data-title=" <spring:message code="LB.W1460" />(<spring:message code="LB.Dollar_1" />)" data-breakpoints="xs sm">
                                                <spring:message code="LB.W1460" />(<spring:message code="LB.Dollar_1" />)
                                            </th>
                                            <!-- 交易金額(元) -->
                                            <th data-title="<spring:message code="LB.W0016" />(<spring:message code="LB.Dollar_1" />)" data-breakpoints="xs sm" >
                                                <spring:message code="LB.W0016" />(<spring:message code="LB.Dollar_1" />)
                                            </th>
                                            <!-- 交易時間 -->
                                            <th data-title="<spring:message code="LB.Trading_time" />" data-breakpoints="xs sm">
                                                <spring:message code="LB.Trading_time" />
                                            </th>
                                        </tr>
                                    </thead>
<%--                                     <c:if test="${dataTable.TOPMSG != 'OKLR'}"> --%>
<%--                                		<tbody> --%>
<%--                                			<tr> --%>
<%--                                				<td>${dataTable.TOPMSG}</td> --%>
<%-- 											<td class="text-left">${dataTable.msgName}</td> --%>
<%--                                			</tr> --%>
<%--                                		</tbody> --%>
<%--                                 	</c:if> --%>
<%--                                 	<c:if test="${dataTable.TOPMSG == 'OKLR'}"> --%>
                                    <tbody>
                                        <c:forEach var="dataList" items="${dataTable.TABLE}">
                                            <tr>
                                                <!-- 日期-->
                                                <td class="text-center">${dataList.LSTLTD }</td>
                                                <!-- 摘要 -->
                                                <td class="text-center">${dataList.MEMO }</td>
                                                <!-- 支出-->
                                                <td class="text-center">${dataList.DPWDAMT }</td>
                                                <!-- 存入 -->
                                                <td class="text-right">${dataList.DPSVAMT }</td>
                                                <!-- 結存 -->
                                                <td class="text-right">${dataList.GDBAL }</td>
                                                <!-- 單價 -->
                                                <td class="text-right"> ${dataList.PRICE }</td>
                                                <!-- 交易金額 -->
                                                <td class="text-right">${dataList.TRNAMT }</td>
                                                <!--交易時間 -->
                                                <td class="text-center">${dataList.TIME }</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
<%--                                     </c:if> --%>
                                </table>
                            </c:forEach>
                            <!--button 區域 -->
	                        
	                            <!--回上頁 -->
	                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
	                            <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
	                            <!-- 列印  -->
	                            <spring:message code="LB.Print" var="printbtn"></spring:message>
	                            <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
	                        
                        </div>  
                    </div>
                </form>
            </section>
            <!-- 		main-content END -->
        </main>
    </div>
    <!-- content row END -->
    <%@ include file="../index/footer.jsp"%>
</body>

</html>