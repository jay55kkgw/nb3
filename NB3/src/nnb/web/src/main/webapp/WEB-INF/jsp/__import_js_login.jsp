<link rel="stylesheet" type="text/css" href="${__ctx}/css/reset.css?a=${jscssDate}">
<link rel="icon" href="${__ctx}/img/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="${__ctx}/img/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="${__ctx}/css/bootstrap-4.1.1.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/font-awesome.min.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/loadingbox.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/footable.bootstrap.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/tbb_common.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/tbb_common_${pageContext.response.locale}.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/hamburgers.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/fonts.css?a=${jscssDate}">
<script type="text/javascript" src="${__ctx}/js/jquery-3.5.1.min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/bootstrap-4.5.3.min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/popper-1.14.3.min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/CGJSCrypt_min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/fstop.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/footable.min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/utility.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/TBB-${pageContext.response.locale}.js?a=${jscssDate}"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.dataTables.min.css?a=${jscssDate}">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/fixedColumns.dataTables.min.css?a=${jscssDate}">
<script type="text/javascript" src="${__ctx}/js/jquery.dataTables.min.js?a=${jscssDate}"></script>
<script type="text/javascript" src="${__ctx}/js/dataTables.fixedColumns.min.js?a=${jscssDate}"></script>
<script src="${__ctx}/js/isotope.pkgd.min.js?a=${jscssDate}"></script>
<script src="${__ctx}/js/fit-columns.js?a=${jscssDate}"></script>
<style type="text/css" media="print">
@page {
	size:auto;
    margin: 5% ;
}
</style>
<script>
var dt_locale = "${pageContext.response.locale}";
</script>
<script type="text/JavaScript">
	var isE2E = '${isE2E}';

	String.prototype.startsWith = function(searchString, position) {
		position = position || 0;
		return this.substr(position, searchString.length) === searchString;
	};
	String.prototype.endsWith = function(searchString, position) {
		var subjectString = this.toString();
		if (typeof position !== 'number' || !isFinite(position)
				|| Math.floor(position) !== position
				|| position > subjectString.length) {
			position = subjectString.length;
		}
		position -= searchString.length;
		var lastIndex = subjectString.indexOf(searchString, position);
		return lastIndex !== -1 && lastIndex === position;
	};
	

	function errorBlock(errortitle, errorcontent, errorinfo, errorBtn1, errorBtn2) {
// 		$("#error-content0").html("");
		$('p[id^=error-]').html("");
		wait1=true;
		setTimeout("wait1=false;", 500);
		$('#error-block').show();
		
		deadOrAlive("error-title", errortitle);
		
		errorBlockMsg("error-content", errorcontent);
		errorBlockMsg("error-info", errorinfo);
		
		
 		deadOrAlive("errorBtn1", errorBtn1);
 		deadOrAlive("errorBtn2", errorBtn2);
		
		$("#errorBtn1").click(function() {
			$('#error-block').hide();
		});
		
	}
	function deadOrAlive(key, value) {
		if( value!=null ) {
			$("#"+key).html(value);
			$("#"+key).show();
		}else{
			$("#"+key).hide();
		}
	}
	function errorBlockMsg(type, strArray) {
		if( strArray!=null ) {
			strArray.forEach(function(str, index) {
				var before = index-1;
				if(index==0){
					deadOrAlive(type+index, str);
				} else {
					var text = document.createElement("p");
					text.id = type+index;
					text.class = type;
					$('#'+type+before).append(text);
					if(str.indexOf("&{red}")!=-1){
						text.style.color="red";
						str=str.replace("&{red}","");
					}
					deadOrAlive(type+index, str);
				}
			});
		}
	}
// 	function closeBlock(){
// 		if($('#error-block').css("display") != "none")
// 			{
// 			$('#error-block').hide();
// 			}
// 	}

</script>