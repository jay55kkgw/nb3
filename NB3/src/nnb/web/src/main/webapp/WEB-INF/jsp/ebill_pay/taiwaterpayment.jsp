<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.twzipcode-1.6.1.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<style>
		.zipcode {
			display: none;
		}
	</style>
	    <script type="text/javascript">
        $(document).ready(function () {
        	init();
        	genDateList();
        });
        function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline", scroll: false});
	    	$("#CMSUBMIT").click(function(e){
	    		
	    		var CCBIRTHDATEYY = $("#CCBIRTHDATEYY").val();
	    		if(CCBIRTHDATEYY.length==2)
	    			CCBIRTHDATEYY="0"+CCBIRTHDATEYY;
	    		if(CCBIRTHDATEYY.length==1)
	    			CCBIRTHDATEYY="00"+CCBIRTHDATEYY;
	    		
	    		$("#CMDATE1").val(CCBIRTHDATEYY + $("#CCBIRTHDATEMM").val() + $("#CCBIRTHDATEDD").val());
	    		$("#CMDATE2").val(CCBIRTHDATEYY + '/' + $("#CCBIRTHDATEMM").val() + '/' + $("#CCBIRTHDATEDD").val());
	    		
	    		var reg=/,/g;
	    	    $("#AMOUNT").val($("#AMOUNT").val().replace(reg, "")); 
	    		
				e = e || window.event;
    			//進行 validation 並送出
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
		        	var action = '${__ctx}/EBILL/PAY/taiwaterpayment_comfirm';
					$("form").attr("action", action);
					$("#CMSUBMIT").hide();
					$("#waitnotice").show();
	    			$("form").submit();
				}
			});
        }
        
		function genDateList(){
			var thisYear = new Date().getFullYear();
			var thisMonth = new Date().getMonth() + 1;
			var thisDay = new Date().getDate();
			var CCBIRTHDATEYY = thisYear - 1911;
			var CCBIRTHDATEMM = thisMonth;
			var CCBIRTHDATEDD = '';
			for(var i = thisYear - 1911;i > 0 ;i--){
				j = i.toString();
				for(k = 0;k <= 2 - i.toString().length;k++)j='0' + j;
				if(CCBIRTHDATEYY == j){
					$("#CCBIRTHDATEYY").append("<option value='" + j + "' selected>民國 " + i + "年</option>");
				}else{
					$("#CCBIRTHDATEYY").append("<option value='" + j + "' >民國" + i + "年</option>");
				}
			}
			for(var i = 1;i <= 12;i++){
				j = i.toString();
				for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
				if(CCBIRTHDATEMM == j){
					$("#CCBIRTHDATEMM").append("<option value='" + j + "' selected>" + i + "月 </option>");
				}else{
					$("#CCBIRTHDATEMM").append("<option value='" + j + "' >" + i + "月</option>");
				}
			}
			for(var i = 1;i <= 31;i++){
				j = i.toString();
				for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
				if(CCBIRTHDATEDD == j){
					$("#CCBIRTHDATEDD").append("<option value='" + j + "' selected>" + i + "日 </option>");
				}else{
					$("#CCBIRTHDATEDD").append("<option value='" + j + "' >" + i + "日 </option>");
				}
			}
		}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item">信用卡</li>
			<li class="ttb-breadcrumb-item">繳費區</li>
			<li class="ttb-breadcrumb-item">臺灣自來水水費</li>
		</ol>
	</nav>
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					臺灣企銀信用卡繳臺灣自來水水費
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post">
					<input type="hidden" name="back" id="back" value=>	
					<input type="hidden" name="CMDATE1" id="CMDATE1" value="">
					<input type="hidden" name="CMDATE2" id="CMDATE2" value="">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											繳費項目
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											臺灣自來水水費
										</div>
									</span>
								</div>
							</div>
							
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											代收期限
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											
											<select name="CCBIRTHDATEYY" id="CCBIRTHDATEYY" class="custom-select select-input input-width-100 validate[required]">
												<option value="#">
													---
												</option>
											</select>
											<!-- 年 -->
											<select name="CCBIRTHDATEMM" id="CCBIRTHDATEMM" class="custom-select select-input input-width-60 validate[required]">
												<option value="#">
													---
												</option>
											</select>
											<!-- 月 -->
											<select name="CCBIRTHDATEDD" id="CCBIRTHDATEDD" class="custom-select select-input input-width-60 validate[required]">
												<option value="#">
													---
												</option>
											</select>
											
											<input id="validate_CCBIRTHDATEYY" name="validate_CCBIRTHDATEYY" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[年,CCBIRTHDATEYY,#]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											<input id="validate_CCBIRTHDATEMM" name="validate_CCBIRTHDATEMM" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[月,CCBIRTHDATEMM,#]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											<input id="validate_CCBIRTHDATEDD" name="validate_CCBIRTHDATEDD" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[日,CCBIRTHDATEDD,#]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											銷帳編號
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="WAT_NO" name="WAT_NO"  placeholder="請輸入銷帳編號 " class="text-input validate[required,funcCall[validate_CheckNumber1['您所輸入的銷帳編號有誤，請重新檢查',WAT_NO,false]]]" maxLength="14" size="14"/>
											<input id="validate_WAT_NO" name="validate_WAT_NO" type="text" class="
												validate[funcCallRequired[validate_CheckNumWithDigit1['您所輸入的銷帳編號有誤，請重新檢查',WAT_NO,false,14]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											查核碼
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CHKCOD" name="CHKCOD"  placeholder="請輸入銷帳編號 " class="text-input validate[required,funcCall[validate_CheckNumber1['您所輸入的查核碼有誤，請重新檢查',CHKCOD,false]]]" maxLength="1" size="1"/>
											<input id="validate_CHKCOD" name="validate_CHKCOD" type="text" class="
												validate[funcCallRequired[validate_CheckNumWithDigit1['您所輸入的查核碼有誤，請重新檢查',CHKCOD,false,1]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											應繳款金額
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="AMOUNT" name="AMOUNT"  placeholder="請輸入銷帳編號 " class="text-input validate[required,funcCall[validate_CheckAmount1['您所輸入的應繳總金額有誤，請重新檢查',AMOUNT,true,1,99999999]]]" maxLength="8" size="8"/>
											<input id="validate_AMOUNT" name="validate_AMOUNT" type="text" class="
												validate[funcCallRequired[validate_CheckTaiwanWater['您所輸入的繳費資料內容有誤，請重新檢查',CMDATE2,WAT_NO,CHKCOD,AMOUNT]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											繳款方式
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											●使用信用卡
										</div>
									</span>
								</div>
							</div>
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080"/>" />
                            <div id='waitnotice' style="display:none"><center><br><font color="red">處理中，請稍候。<br>請勿再次執行，以免重複繳款！</font></center></div>
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>