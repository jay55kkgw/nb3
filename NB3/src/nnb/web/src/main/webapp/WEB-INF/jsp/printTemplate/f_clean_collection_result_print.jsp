<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<label><spring:message code="LB.Inquiry_time" /> <!-- 查詢時間 -->：</label>
	<label>${CMQTIME}</label>
	<br />
	<label><spring:message code="LB.Inquiry_period_1" /> <!-- 查詢期間 -->：</label>
	<label>${CMPERIOD}</label>
	<br />
	<label><spring:message code="LB.Total_records" /> <!-- 資料總數 -->：</label>
	<label>${COUNT}　<spring:message code="LB.Rows" /></label>
	
	<table class="print">
		<tr>
			<!-- 交易編號 -->
			<th style="text-align:center"><spring:message code="LB.Transaction_Number" /></th>
			<!-- 申請託收日 -->
			<th style="text-align:center" data-breakpoints="xs sm"><spring:message code="LB.W0235" /></th>
			<!-- 票據號碼 -->
			<th style="text-align:center" data-breakpoints="xs sm"><spring:message code="LB.W0236" /></th>
			<!-- 託收幣別 -->
			<th style="text-align:center" data-breakpoints="xs sm"><spring:message code="LB.W0237" /></th>
			<!-- 託收金額 -->
			<th style="text-align:center" data-breakpoints="xs sm"><spring:message code="LB.W0189" /></th>
			<!-- 國外入帳金額 -->
			<th style="text-align:center"><spring:message code="LB.W0239" /></th>
			<!-- 撥收日 -->
			<th style="text-align:center" data-breakpoints="xs sm"><spring:message code="LB.W0240" /></th>
			<!-- 狀態 -->
			<th style="text-align:center" data-breakpoints="xs sm"><spring:message code="LB.Status" /></th>						
		</tr>
		<c:forEach var="dataList" items="${dataListMap}">
		<tr>
			<td style="text-align:center">${dataList.RREFNO}</td>
			<td style="text-align:center">${dataList.RCREDATE}</td>
			<td style="text-align:center">${dataList.RCHQNO}</td>
			<td style="text-align:center">${dataList.RBILLCCY}</td>
			<td style="text-align: right">${dataList.RBILLAMT}</td>
			<td style="text-align: right">${dataList.RRETAMT}</td>
			<td style="text-align:center">${dataList.RVALDATE}</td>
			<td style="text-align:center">${dataList.RSTATE}</td>						               
		</tr>
		</c:forEach>
	</table>
	<br>
	<!-- 託收金額小計 -->
	<label><spring:message code="LB.W0209" />:</label>
	<br>
	<label>${CURRANCY_PRINT}</label>
	<br />
	<br />
</body>
</html>