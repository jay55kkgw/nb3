<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust:exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<table class="print">
		<tr>
<!-- 交易日期 -->
			<td><spring:message code= "LB.D0450" /></td>
		 	<td>${CMQTIME}</td>
		</tr>
		<tr>
		<!-- 台幣轉出帳號 -->
  			<td><spring:message code= "LB.W1496" /></td>
			<td>${SVACN}</td>
		</tr>
		<tr>
<!-- 黃金轉入帳號 -->
  			<td><spring:message code= "LB.W1497" /></td>
			<td>${ACN}</td>
		</tr>
		<tr>
<!-- 買進公克數 -->
  			<td><spring:message code= "LB.W1498" /></td>
<!-- 公克 -->
			<td>${TRNGDFormat}<spring:message code= "LB.W1435" /></td>
		</tr>
		<tr>
<!-- 牌告單價 -->
			<td><spring:message code= "LB.W1524" /></td>
<!-- 元 -->
<!-- 公克 -->
<!-- 新台幣 -->
		 	<td><spring:message code= "LB.NTD" />${PRICEFormat}<spring:message code= "LB.W1511" /></td>
		</tr>
		<tr>
<!-- 折讓後單價 -->
			<td><spring:message code= "LB.W1504" /></td>
<!-- 元 -->
<!-- 公克 -->
<!-- 新台幣 -->
		 	<td><spring:message code= "LB.NTD" />${DISPRICEFormat}<spring:message code= "LB.W1511" /></td>
		</tr>
		<tr>
<!-- 折讓率 -->
  			<td><spring:message code= "LB.W1506" /></td>
			<td>${PERDISFormat}％</td>
		</tr>
		<tr>
<!-- 手續費 -->
  			<td><spring:message code= "LB.D0507" /></td>
<!-- 元 -->
<!-- 新台幣 -->
			<td><spring:message code= "LB.NTD" />${TRNFEEFormat}<spring:message code= "LB.Dollar" /></td>
		</tr>
		<tr>
<!-- 總扣款金額 -->
  			<td><spring:message code= "LB.W1509" /></td>
<!-- 元 -->
<!-- 新台幣 -->
			<td><spring:message code= "LB.NTD" />${TRNAMTFormat}<spring:message code= "LB.Dollar" /></td>
		</tr>
	</table>
	<div>
		<p style="text-align:left;">
<!-- 說明 -->
			<spring:message code="LB.Description_of_page" /><br/>
			<!--電子簽章：為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章（載具i－key）拔除並登出系統。  -->
			<spring:message code="LB.Gold_Buy_P3_D1"/>
			
		</p>
	</div>
</body>
</html>