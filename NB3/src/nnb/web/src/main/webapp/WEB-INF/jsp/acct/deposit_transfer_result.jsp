<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<script type="text/javascript">
		$(document).ready(function () {
			//列印
			$("#printbtn").click(function () {
				var params = {
					"jspTemplateName": "deposit_transfer_result_print",
					"jspTitle": '<spring:message code="LB.Open_Taiwan_Currency_Time_Deposit"/>',
					"FGTXDATE": '${deposit_transfer_result.data.FGTXDATE}',
					"CMTXTIME": '${deposit_transfer_result.data.CMTXTIME}',
					"transfer_date": '${deposit_transfer_result.data.transfer_date}',
					"OUTACN": '${deposit_transfer_result.data.OUTACN}',
					"DPAGACNO_TEXT": '${deposit_transfer_result.data.DPAGACNO_TEXT}',
					"AMOUNT": '${deposit_transfer_result.data.AMOUNT}',
					"FDPNUM": '${deposit_transfer_result.data.FDPNUM}',
					"FDPACC": '${deposit_transfer_result.data.FDPACC}',
					"SDT": '${deposit_transfer_result.data.SDT}',
					"DUEDATE": '${deposit_transfer_result.data.DUEDATE}',
					"TERM_TEXT": '${deposit_transfer_result.data.TERM_TEXT}',
					"INTMTH": '${deposit_transfer_result.data.INTMTH}',
					"ITR": '${deposit_transfer_result.data.ITR}',
					"CODE": '${deposit_transfer_result.data.CODE}',
					"CODENAME": '${deposit_transfer_result.data.CODENAME}',
					"DPSVTYPENAME": '${deposit_transfer_result.data.DPSVTYPENAME}',
					"CMTRMEMO": '${deposit_transfer_result.data.CMTRMEMO}',
					"O_TOTBAL": '${deposit_transfer_result.data.O_TOTBAL}',
					"O_AVLBAL": '${deposit_transfer_result.data.O_AVLBAL}',
					"CMTRMEMO": '${deposit_transfer_result.data.CMTRMEMO}',
				};

				openWindowWithPost("${__ctx}/print",
					"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
					params);

			});
		});
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 	<!--   IDGATE -->
    <%@ include file="../idgate_tran/idgateAlert.jsp" %>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 轉入綜存定存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Open_Taiwan_Currency_Time_Deposit" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 主頁內容 -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 轉入臺幣綜存定存 -->
					<spring:message code="LB.Open_Taiwan_Currency_Time_Deposit" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
					<c:set var="BaseResultData" value="${deposit_transfer_result.data}"></c:set>
					<!-- 交易步驟 -->
					<div id="step-bar">
						<ul>
							<!-- 輸入資料 -->
							<li class="finished">
								<spring:message code="LB.Enter_data" />
							</li>
							<!-- 確認資料 -->
							<li class="finished">
								<spring:message code="LB.Confirm_data" />
							</li>
							<!-- 交易完成 -->
							<li class="active">
								<spring:message code="LB.Transaction_complete" />
							</li>
						</ul>
					</div>
					<!-- 交易步驟-END -->

						<c:if test="${BaseResultData.FGTXDATE == '1' && showDialog == 'true'}">
								 	<%@ include file="../index/txncssslog.jsp"%>
						</c:if>

					<!-- 即時交易結果 -->
					<c:if test="${BaseResultData.FGTXDATE == '1'}">
						<!-- 表單顯示區 -->
						<div class="main-content-block row">
							<div class="col-12 tab-content">
								<div class="ttb-input-block">
									<div class="ttb-message">
										<span><spring:message code= "LB.Transfer_successful" /></span>
									</div>
									<!-- 交易時間 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Trading_time" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>${BaseResultData.CMTXTIME}</span>
											</div>
										</span>
									</div>
									<!-- 轉帳日期 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Transfer_date" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>${BaseResultData.transfer_date }</span>
											</div>
										</span>
									</div>
									<!-- 轉出帳號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Payers_account_no" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>${BaseResultData.OUTACN}</span>
											</div>
										</span>
									</div>
									<!-- 轉入帳號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Payees_account_no" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>${BaseResultData.DPAGACNO_TEXT}</span>
											</div>
										</span>
									</div>
									<!-- 轉帳金額 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Amount" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span class="high-light">
													<!-- 新台幣 -->
													<spring:message code="LB.NTD" />
													<fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.AMOUNT }" />
												</span>
													<!--元 -->
												<span class="input-unit">
													<spring:message code="LB.Dollar" />
												</span>
											</div>
										</span>
									</div>
									<!-- 存單號碼 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Certificate_no" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.FDPNUM}
												</span>
											</div>
										</span>
									</div>
									<!-- 存款種類 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Deposit_type" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.FDPACC }
												</span>
											</div>
										</span>
									</div>
									<!-- 起存日 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Start_date" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.SDT }
												</span>
											</div>
										</span>
									</div>
									<!-- 到期日 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Maturity_date" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.DUEDATE }
												</span>
											</div>
										</span>
									</div>
									<!-- 計息方式 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Interest_calculation" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.INTMTH }
												</span>
											</div>
										</span>
									</div>
									<!-- 利率 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Interest_rate" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.ITR } %
												</span>
											</div>
										</span>
									</div>
									<!-- 到期轉期 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Roll-over_when_expiration" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.CODENAME}
												</span>
											</div>
										</span>
									</div>
									<!-- 轉存方式  //不轉期時不顯示轉存方式-->
									<c:if test="${BaseResultData.CODE != '0'}">
										<div class="ttb-input-item row">
											<span class="input-title">
												<label>
													<h4>
														<spring:message code="LB.Rollover_method" />
													</h4>
												</label>
											</span>
											<span class="input-block">
												<div class="ttb-input">
													<span>
														${BaseResultData.DPSVTYPENAME }
													</span>
												</div>
											</span>
										</div>
									</c:if>
									<!-- 交易備註 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Transfer_note" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.CMTRMEMO }
												</span>
											</div>
										</span>
									</div>
									<!-- 轉出帳號帳戶餘額 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Payers_account_balance" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<!-- 新台幣 -->
													<spring:message code="LB.NTD" />
													&nbsp;
													<fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.O_TOTBAL }" />
													&nbsp;
													<!--元 -->
													<spring:message code="LB.Dollar" />
												</span>
											</div>
										</span>
									</div>
									<!-- 轉出帳號可用餘額 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Payers_available_balance" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<!-- 新台幣 -->
													<spring:message code="LB.NTD" />
													&nbsp;
													<fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.O_AVLBAL }" />
													&nbsp;
													<!--元 -->
													<spring:message code="LB.Dollar" />
												</span>
											</div>
										</span>
									</div>
								</div>
									<!-- 列印 -->
									<spring:message code="LB.Print" var="printbtn"></spring:message>
									<input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
							</div>
						</div>
						<div class="text-left">
 							<ol class="description-list list-decimal">
							<!-- 		說明： -->
							<p><spring:message code="LB.Description_of_page" /></p>
								<li><spring:message code= "LB.Deposit_transfer_P3_D1" /></li>
								<li><spring:message code= "LB.Deposit_transfer_P3_D2" /></li>
							</ol>
						</div>
					</c:if>
					<!-- 預約交易結果 -->
					<c:if test="${BaseResultData.FGTXDATE == '2'}">
						<!-- 表單顯示區 -->
						<div class="main-content-block row">
							<div class="col-12 tab-content">
								<div class="ttb-input-block">
									<div class="ttb-message">
										<span><spring:message code= "LB.W0284" /></span>
									</div>
									<!-- 交易時間 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Trading_time" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.CMTXTIME}
												</span>
											</div>
										</span>
									</div>
									<!-- 轉帳日期 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Transfer_date" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.transfer_date }
												</span>
											</div>
										</span>
									</div>
									<!-- 轉出帳號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Payers_account_no" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.OUTACN}
												</span>
											</div>
										</span>
									</div>
									<!-- 轉入帳號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Payees_account_no" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.DPAGACNO_TEXT}
												</span>
											</div>
										</span>
									</div>
									<!-- 轉帳金額 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Amount" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<!-- 新台幣 -->
													<spring:message code="LB.NTD" />
													&nbsp;
													<fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.AMOUNT }" />
													&nbsp;
													<!--元 -->
													<spring:message code="LB.Dollar" />
												</span>
											</div>
										</span>
									</div>
									<!-- 存款種類 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Deposit_type" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.FDPACC }
												</span>
											</div>
										</span>
									</div>
									<!-- 存款期別或<br />指定到期日</td> -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Deposit_type" />
													<spring:message code= "LB.Deposit_period_or_Designated_due_date" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.TERM_TEXT}
												</span>
											</div>
										</span>
									</div>
									<!-- 計息方式 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Interest_calculation" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.INTMTH }
												</span>
											</div>
										</span>
									</div>
									<!-- 到期轉期 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Roll-over_when_expiration" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.CODENAME}
												</span>
											</div>
										</span>
									</div>
									<!-- 轉存方式  //不轉期時不顯示轉存方式-->
									<c:if test="${BaseResultData.CODE != '0'}">
										<div class="ttb-input-item row">
											<span class="input-title">
												<label>
													<h4>
														<spring:message code="LB.Rollover_method" />
													</h4>
												</label>
											</span>
											<span class="input-block">
												<div class="ttb-input">
													<span>
														${BaseResultData.DPSVTYPENAME }
													</span>
												</div>
											</span>
										</div>
									</c:if>
									<!-- 交易備註 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Transfer_note" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.CMTRMEMO }
												</span>
											</div>
										</span>
									</div>
								</div>
									<!-- 列印 -->
									<spring:message code="LB.Print" var="printbtn"></spring:message>
									<input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
							</div>
						</div>
						<div class="text-left">
							<!-- 		說明： -->
							<ol class="description-list list-decimal">
								<p><spring:message code="LB.Description_of_page" /></p>
								<!--                     <li>如預約之轉帳日期為曆法所無之日期，以該月之末日為轉帳日，例如：預約每月31日轉帳，因6月無31日，故會在6月30日轉帳。</li> -->
								<li>
									<span><spring:message code="LB.Deposit_transfer_PP3_D1" /></span>
								</li>
								<!--                     <li>預約轉帳請於轉帳日之前1日，存足款項於轉出帳號備扣。預約轉帳將於轉帳日與即時轉帳併計最高轉出限額。</li> -->
								<li>
									<span><spring:message code="LB.Deposit_transfer_PP3_D2" /></span>
								</li>
								<!--                     <li>預約成功不代表交易已完成，請於轉帳日利用『預約交易結果查詢』，以確認交易結果。</li> -->
								<li>
									<span><spring:message code="LB.Deposit_transfer_PP3_D3" /></span>
								</li>
							</ol>
						</div>
					</c:if>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>
