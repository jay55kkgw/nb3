<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

<script type="text/javascript">
$(document).ready(function () {
	mailCheck();
	authRules();
	
});

	//Email檢查
	function mailCheck(){
		var email = '${notifyInfo.data.DPMYEMAIL}';
	   	if(email == ""){
	   		//請先設定我的Email再來進行本交易
	   	//alert("<spring:message code= "LB.Alert195" />");
	   		errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert195' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	   	$('#type').val("DPSETUPE");
	   	$('#formId').attr("action","${__ctx}/PERSONAL/SERVING/mail_setting_choose");
	   	$('#formId').submit();
		}
	}
	//權限設定
	function authRules(){
		auth = '${notifyInfo.data.authority}';
		$("input[type=checkbox][value=1]").attr("checked","checked");
		//依照舊網銀規則下class(空的),以便disabled checkbox
		if(!auth=="ALL" && !auth=="ATMT"){
			$("input[type=checkbox][class='authcheck1']").prop("checked","");
			$("input[type=checkbox][class='authcheck1']").attr("disabled",true);
			$("input[type=checkbox][class='authcheck1']").addClass("ttb-check2");
		}else if (!auth=="ALL"){
			$("input[type=checkbox][class='authcheck2']").prop("checked","");
			$("input[type=checkbox][class='authcheck2']").attr("disabled",true);
			$("input[type=checkbox][class='authcheck2']").addClass("ttb-check2");
		}
	
	}

	//全選
	function selectAll(){
		$("input[type=checkbox]").prop("checked","checked")
	}

	//送出資料轉換
	function submitNotify(){
		var notifyData = [];
		//initBlockUI();
		$("input[type=checkbox]").each(function(){
			if(this.checked){
				//取得所選取的項目號碼
				notifyData.push($(this).attr('id').replace('cho',''));
			}
		});
		//將項目排序
		notifyData.sort(function(a,b){
			return a-b
		})
		$('#NOTIFYDATA').val(notifyData);
		$('#formId').submit();
	}


</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 通知服務設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Notification_Service" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.Notification_Service" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" action="${__ctx}/PERSONAL/SERVING/notification_service_result" method="post">
				<input type="hidden" id="type" name="type" value=""/> 
				<input type="hidden" name="EXECUTEFUNCTION" value="update">
				<input type="hidden" name="MYEMAIL" value="${notifyInfo.data.DPMYEMAIL}">
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
					<input type="hidden" id="NOTIFYDATA" name="NOTIFYDATA" value="">
						<!-- 主頁內容  -->
						<div class="ttb-input-block">
								<h4 id="titleMsg" style="text-align: center; color: red"><spring:message code="LB.Select_Item_to_be_Set" /></h4><!-- 請選擇欲設定的項目 -->
								<br/>
						<div class="ttb-input-item row">
								<span class="input-title">
									<h4><spring:message code="LB.Option_item" /></h4><!-- 選項 -->
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="check-block">
<!-- 										轉帳/繳費稅/匯出匯款成功通知 -->
										<spring:message code="LB.X1387" />
										<input type="checkbox"  class="authcheck1" id="cho1" name="DPNOTIFY1" value="${notifyInfo.data.DPNOTIFY1}"/>
										<span class="ttb-check"></span>
										</label>
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										代扣繳各項費用不足扣通知 -->
										<spring:message code="LB.X1388" />
										<input type="checkbox" class="authcheck1" id="cho2" name="DPNOTIFY2" value="${notifyInfo.data.DPNOTIFY2}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										定存即將到期通知 -->
										<spring:message code="LB.X1389" />
										<input type="checkbox" class="authcheck1" id="cho3" name="DPNOTIFY3" value="${notifyInfo.data.DPNOTIFY3}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										預約交易（轉帳/繳費稅/匯出匯款）即將扣帳通知 -->
										<spring:message code="LB.X1390" />
										<input type="checkbox" class="authcheck1" id="cho4" name="DPNOTIFY4" value="${notifyInfo.data.DPNOTIFY4}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										預約交易（轉帳/繳費稅/匯出匯款）結果通知 -->
										<spring:message code="LB.X1391" />
										<input type="checkbox" class="authcheck1" id="cho5" name="DPNOTIFY5" value="${notifyInfo.data.DPNOTIFY5}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										預約交易（轉帳/繳費稅/匯出匯款）到期通知 -->
										<spring:message code="LB.X1392" />
										<input type="checkbox" class="authcheck1" id="cho20" name="DPNOTIFY20" value="${notifyInfo.data.DPNOTIFY20}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										國外匯入匯款通知 -->
										<spring:message code="LB.X1393" />
										<input type="checkbox" class="authcheck2" id="cho12" name="DPNOTIFY12" value="${notifyInfo.data.DPNOTIFY12}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										個人戶借款寬限期滿通知 -->
										<spring:message code="LB.X1394" />
										<input type="checkbox" class="authcheck1" id="cho10" name="DPNOTIFY10" value="${notifyInfo.data.DPNOTIFY10}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										企業戶借款本金即將到期通知 -->
										<spring:message code="LB.X1395" />
										<input type="checkbox" class="authcheck1" id="cho13" name="DPNOTIFY13" value="${notifyInfo.data.DPNOTIFY13}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										基金申購贖回轉換交易成功通知 -->
										<spring:message code="LB.X1396" />
										<input type="checkbox"class="authcheck1" id="cho6" name="DPNOTIFY6" value="${notifyInfo.data.DPNOTIFY6}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										基金預約交易結果通知 -->
										<spring:message code="LB.X1397" />
										<input type="checkbox" class="authcheck1" id="cho7" name="DPNOTIFY7" value="${notifyInfo.data.DPNOTIFY7}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										基金定期定額約定變更交易成功通知 -->
										<spring:message code="LB.X1398" />
										<input type="checkbox" class="authcheck1" id="cho8" name="DPNOTIFY8" value="${notifyInfo.data.DPNOTIFY8}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										基金停利/停損點通知 -->
										<spring:message code="LB.X1399" />
										<input type="checkbox" class="authcheck1" id="cho14" name="DPNOTIFY14" value="${notifyInfo.data.DPNOTIFY14}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										基金自動贖回通知 -->
										<spring:message code="LB.X1400" />
										<input type="checkbox" class="authcheck1" id="cho21"  name="DPNOTIFY21" value="${notifyInfo.data.DPNOTIFY21}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										借款繳款不足扣通知 -->
										<spring:message code="LB.X1401" />
										<input type="checkbox" class="authcheck1" id="cho9"  name="DPNOTIFY9" value="${notifyInfo.data.DPNOTIFY9}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										信用卡繳款通知 -->
										<spring:message code="LB.X1402" />
										<input type="checkbox" id="cho11" name="DPNOTIFY11" value="${notifyInfo.data.DPNOTIFY11}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										網路銀行相關服務及優惠訊息 -->
										<spring:message code="LB.X1403" />
										<input type="checkbox"  id="cho15" name="DPNOTIFY15" value="${notifyInfo.data.DPNOTIFY15}"/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										黃金買進/回售/定期投資變更成功通知 -->
										<font style="color:#AAAAAA"><spring:message code="LB.X1404" /></font>
										<input type="checkbox" class="authcheck1" id="cho16" name="DPNOTIFY16" value="${notifyInfo.data.DPNOTIFY16}" onclick="return false" checked/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										黃金存摺預約交易結果通知 -->
										<font style="color:#AAAAAA"><spring:message code="LB.X1405" /></font>
										<input type="checkbox" class="authcheck1" id="cho17"  name="DPNOTIFY17" value="${notifyInfo.data.DPNOTIFY17}" onclick="return false" checked/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										黃金存摺定期定額即將扣款通知 -->
										<font style="color:#AAAAAA"><spring:message code="LB.X1406" /></font>
										<input type="checkbox" class="authcheck1" id="cho18"  name="DPNOTIFY18" value="${notifyInfo.data.DPNOTIFY18}" onclick="return false" checked/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										黃金存摺定期定額扣款結果通知 -->
										<font style="color:#AAAAAA"><spring:message code="LB.X1407" /></font>
										<input type="checkbox" class="authcheck1" id="cho19"  name="DPNOTIFY19" value="${notifyInfo.data.DPNOTIFY19}"  onclick="return false" checked/>
										<span class="ttb-check"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<h4><spring:message code="LB.Mail_address" /><br/>(<spring:message code="LB.My_Email" />)</h4>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										${notifyInfo.data.DPMYEMAIL}
									</div>
								</span>
							</div>
						</div>
						<input type="button" id="CMCHALL" class="ttb-button btn-flat-orange" value="<spring:message code="LB.All_select" />" onclick="selectAll()">
						<!-- 全選 -->
						<input type="reset" id="CMRESET" class="ttb-button btn-flat-gray no-l-disappear-btn" value="<spring:message code="LB.Re_enter" />"/>
						<!-- 重新輸入 -->
						<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" onclick="submitNotify()">
						<!-- 確定 -->
					</div>
				</div>
			</form>
		</section>
		</main>
	</div>

</body>
</html>