<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- Email設定     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Email_Setting" /></li>
    <!-- 我的通訊錄     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.My_Address_Book" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	
	<main class="col-12"> 
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.My_Address_Book" /><!--  -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" action="${__ctx}/PERSONAL/SERVING/mail_setting_choose" method="post">
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
						<!-- 主頁內容  -->
						<div class="ttb-input-block">
							<!-- 系統時間 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.System_time" /></h4></label></span> <!-- 系統時間 -->
								<span class="input-block">
									<div class="ttb-input">
										<p>${addressbook_result.data.CMQTIME}</p>
									</div>
								</span>
							</div>					
						</div>
						<!-- 設定結果 -->
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> <label><spring:message code="LB.Setting_result" /></label></span> <!-- 設定結果 -->
								<span class="input-block">
									<div class="ttb-input">
									<c:if test="${addressbook_result.data.result eq 'INSERT'}">
										<p><font color=red><spring:message code="LB.X0435" /></font></p><!--  -->
									</c:if>
									<c:if test="${addressbook_result.data.result eq 'UPDATE'}">
										<p><font color=red><spring:message code="LB.X0293" /></font></p><!--  -->
									</c:if>
									<c:if test="${addressbook_result.data.result eq 'DELETE'}">
										<p><font color=red><spring:message code="LB.X0436" /></font></p><!--  -->
									</c:if>
									</div>
								</span>
							</div>					
						</div>
						
						<!--  -->
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.Favorite_name" /></h4></label></span> <!--  -->
								<span class="input-block">
									<div class="ttb-input">
										<p>${addressbook_result.data.DPGONAME}</p>
									</div>
								</span>
							</div>					
						</div>
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.Mail_address" /></h4></label></span> <!-- 電子郵件 -->
								<span class="input-block">
									<div class="ttb-input">
										<p>${addressbook_result.data.DPABMAIL}</p>
									</div>
								</span>
							</div>					
						</div>
						
						<input id="CMSUBMIT"  type="submit" class="ttb-button btn-flat-orange" value="<spring:message code= "LB.X0434" />"/>
						
					</div>
				</div>
				<input type="hidden" name="type" value="DPSETUPA" />	
				</form>
		</section>
	</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
</body>
</html>