<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){
		$("#printbtn").click(function(){
			var i18n = new Object();
			//<!-- 信用卡未出帳單交易明細查詢 -->
			i18n['jspTitle']='<spring:message code= "LB.X1418" />';
			var params = {
				"jspTemplateName":"card_unbilled_mail_result_print",
				"jspTitle":i18n['jspTitle'],
				"CMQTIME":"${card_unbilled_mail_result.data.CMQTIME}",
				"CMMAIL":"${card_unbilled_mail_result.data.CMMAIL}"
			}
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		
		});
	});
		
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code= "LB.X1418" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" action="" method="post">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<h4 style="margin-top:10px;color: red;font-weight:bold;">
								<spring:message code= "LB.X1419" />
							</h4>
							<div class="ttb-input-block">
								<!--查詢時間:-->
								<div class="ttb-input-item row">
									<!--查詢時間:  -->
									<span class="input-title">
										<label>
											<spring:message code= "LB.Inquiry_time" />：
										</label>
									</span>
									<span class="input-block">
										${card_unbilled_mail_result.data.CMQTIME}
									</span>
								</div>
								<div class="ttb-input-item row">
									<!--信用卡當月交易明細傳送位址  -->
									<span class="input-title">
										<label>
											<spring:message code= "LB.X1420" />：
										</label>
									</span>
									<span class="input-block">
										${card_unbilled_mail_result.data.CMMAIL}
									</span>
								</div>
						</div>
								<!--列印 -->
								<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
      
</body>

</html>