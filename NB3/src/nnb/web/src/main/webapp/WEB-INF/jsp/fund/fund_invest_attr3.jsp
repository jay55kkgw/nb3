<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<!-- DIALOG會用到 -->
<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<style type="text/css">
#careerTable{
	border:solid 2px;
}
#careerTable tr{
	border:solid 2px;
}
#careerTable td{
	border:solid 2px;
}
 .ui-dialog-titlebar-close {
    display: none;
 }
</style>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#formID").validationEngine({binded: false,promptPosition: "inline", scroll: false});
	////KYC每日僅能填寫三次條件
	if(${todayUserKYCCountBoolean == false}){
		var message = "<spring:message code= 'LB.Alert093' />";
		callErrorBlock(message);
		
		// 複寫errorBtn1 事件	
		$("#errorBtn1").click(function(e) {
			$("#formID").validationEngine('detach');
	    	var action = '${__ctx}/INDEX/index';
			$("#formID").attr("action", action);
			$("#formID").submit();
		});
		$("#CMSUBMIT").prop("disabled",true);
	    $("#CMSUBMIT").removeClass("btn-flat-orange");
	}
	else{
		errorBlock(
				["<spring:message code= 'LB.X2375' />"], 
				["<li style='list-style:none;'>1.<spring:message code= 'LB.X2376' /></li>","<li style='list-style:none;padding-top: 20px'>2.<spring:message code= 'LB.X2377' /></li>","<li style='list-style:none;padding-top: 20px'>3.<spring:message code= 'LB.X2378' /></li>"],
				null, 
				'<spring:message code= "LB.Quit" />', 
				null
		);
		// 複寫errorBtn1 事件	
		$("#errorBtn1").click(function(e) {
			$('#error-block').hide();
			var email = '${sessionScope.dpmyemail}';
			if(email.length == 0){
				email = "<spring:message code= 'LB.X2599' />";
			}
			errorBlock(
					["<spring:message code= 'LB.X0152' />"], 
					["<li style='list-style:none;'><spring:message code= 'LB.X2594' /></li>","<li style='list-style:none;padding-top: 20px'><b><font color='red'><spring:message code= 'LB.X2595' />？</font><b></li>","<li style='list-style:none;padding-top: 20px'><spring:message code= 'LB.X2596' />&nbsp;<a href='#' style='color: blue;' onclick='gotoEmailSetting()''><spring:message code= 'LB.Internet_banking' /></a> / <spring:message code= 'LB.X2597' /></li>","<li style='list-style:none;padding-top: 20px'><spring:message code= 'LB.X2598' />。</li>","<li style='list-style:none;padding-top: 20px'><spring:message code= 'LB.Email' />："+email+"</li>"],
					null, 
					'<spring:message code= "LB.Confirm" />', 
					null
			);
		});
	}

//	
	$("#CMSUBMIT").click(function(e){
		
		
		 //欄位檢核
		  if (!CheckRadio("<spring:message code= 'LB.X1545' />", "Q1")){
			  $('input[name=Q1]').focus();
		  	  return false;
		  }

		  if (!CheckRadio("<spring:message code= 'LB.X1518' />", "Q2")){
			  $('input[name=Q2]').focus();			  
		  	  return false;
		  }
		  	  
		  if (!CheckRadio("<spring:message code= 'LB.X1519' />", "Q3")){
			  $('input[name=Q3]').focus();			  
		  	  return false;
		  }
		  	  
		  if (!CheckRadio("<spring:message code= 'LB.X1521' />", "Q4")){
			  $('input[name=Q4]').focus();
		  	  return false;
		  }
		  	  
		  if (!CheckRadio("<spring:message code= 'LB.X1522' />", "Q5")){
			  $('input[name=Q5]').focus();
		  	  return false;
		  }

		  if (!CheckRadio("<spring:message code= 'LB.X1523' />", "Q6")){
			  $('input[name=Q6]').focus();
		      return false;
		  }

		if($("#Q71").prop("checked") == false && $("#Q72").prop("checked") == false && $("#Q73").prop("checked") == false && $("#Q74").prop("checked") == false){
			var message = "<spring:message code= 'LB.Alert106' />";
			callErrorBlock(message);
			return false;      
		}
		//問題八
		  if (!CheckRadio("<spring:message code= 'LB.X1525' />", "Q8")){
			  $('input[name=Q8]').focus(); 
		  	  return false;
		  }
		
		if($("#Q91").prop("checked") == false && $("#Q92").prop("checked") == false && $("#Q93").prop("checked") == false && $("#Q94").prop("checked") == false){
			var message = "<spring:message code= 'LB.Alert107' />";
			callErrorBlock(message);
			return false;      
		}
		if($("#Q91").prop("checked") == true || $("#Q92").prop("checked") == true || $("#Q93").prop("checked") == true){	  
			if($("#Q94").prop("checked") == true){

				var message = "<spring:message code= 'LB.Alert108' />";
				callErrorBlock(message);
				return false;    
			}
		}
		  if (!CheckRadio("<spring:message code= 'LB.X1527' />", "Q10")){
			  $('input[name=Q10]').focus();
		  	  return false;
		  }
		  	  
		  if (!CheckRadio("<spring:message code= 'LB.X1546' />", "Q11")){
			  $('input[name=Q11]').focus();
		  	  return false;
		  }

		  if (!CheckRadio("<spring:message code= 'LB.X1528' />", "Q12")){
			  $('input[name=Q12]').focus();
		      return false;	  	  	  	  	  	  	  	  		      	  	  	  	  	  	  	  	  	  	
		  }
		  	  
		  if (!CheckRadio("<spring:message code= 'LB.X1547' />", "Q13")){
			  $('input[name=Q13]').focus();
		  	  return false;
		  }

		  if (!CheckRadio("<spring:message code= 'LB.X1529' />", "Q14")){
			  $('input[name=Q14]').focus();			  
		      return false;
		  }

		e = e || window.event;
		//進行 validation 並送出
		if (!$('#formID').validationEngine('validate')) {
			e.preventDefault();
		} else {
			$("#formID").validationEngine('detach');
			
		 	$("#formID").submit();
		}
	});
	
	$("#mailDialog").dialog({
		autoOpen:false,
		modal:true,
		closeOnEscape:false,
		width:850,
		height:200,
		position:{
			my:"center",at:"top-300px",of:window
		}
	});
	$("#CMSUBMIT_Y").click(function() {
		if('${sessionScope.dpmyemail}'!=''){
			$("#mailDialog").dialog("close");
		}else{
			$("#formID").attr("action", "${__ctx}/PERSONAL/SERVING/mail_setting_choose");
			$("#type").val("DPSETUPE");
			$("#formID").submit();
			$("#mailDialog").dialog("close");
			initBlockUI();
		}
	});
	
	$("#CMSUBMIT_N").click(function() {
		$("#formID").attr("action","${__ctx}/PERSONAL/SERVING/mail_setting_choose");
		$("#type").val("DPSETUPE");
		$("#formID").submit();
		$("#mailDialog").dialog("close");
		initBlockUI();
	});
	
});

//貴公司對金融商品的知識？（本題可複選，取得分最高者計算）
function checkQ9(){
	// 選了不認識
	if($("#Q94").prop("checked")) {
		// 其他選項不可選
		$("#Q91").prop("checked", false);
		$("#Q92").prop("checked", false);
		$("#Q93").prop("checked", false);
		
		$("#Q91").prop('disabled', true);
		$("#Q92").prop('disabled', true);
		$("#Q93").prop('disabled', true);
		
	} // 沒有選不認識
	else {
		// 可以選其他選項
		$("#Q91").prop('disabled', false);
		$("#Q92").prop('disabled', false);
		$("#Q93").prop('disabled', false);
	}
}
//建立只有離開按鈕的ErrorBlock
function callErrorBlock(message){
	errorBlock(
			null, 
			null,
			[message], 
			'<spring:message code= "LB.Quit" />', 
			null
	);
	
}
function gotoEmailSetting(){
	$('#error-block').hide();
	fstop.getPage('${pageContext.request.contextPath}'+'/PERSONAL/SERVING/mail_setting','', '');//導頁去改信箱
}

</script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 投資屬性評估調查表－（法人版）     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1378" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1378" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
				<form id="formID" action="${__ctx}/FUND/TRANSFER/fund_invest_attr4" method="post">
					<input type="hidden" name="TXID" value="${TXID}"/>
					<input type="hidden" name="RTC" value="${TXID}"/>
   					<input type="hidden" name="RISK" value="${RISK}"/>
   					<input type="hidden" id="type" name="type"/>
      				<div class="main-content-block row">
						<div class="col-12 terms-block questionnaire-block">
							<div class="ttb-message">
								<p><spring:message code="LB.W1378" /></p>
							</div>
							<p class="form-description"><spring:message code="LB.W1219" /></p>
							<div class="text-left">
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>1﹒<spring:message code="LB.W1325" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q1"/>A﹒<spring:message code="LB.W1326" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q1"/>B﹒<spring:message code="LB.W1327" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q1"/>C﹒<spring:message code="LB.W1328" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q1"/>D﹒<spring:message code="LB.W1329" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q1" name="Q1" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1545" />',Q1]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>2﹒<spring:message code="LB.W1330" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q2"/>A﹒<spring:message code="LB.W1331" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q2"/>B﹒<spring:message code="LB.W1332" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q2"/>C﹒<spring:message code="LB.W1333" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q2"/>D﹒<spring:message code="LB.W1334" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q2" name="Q2" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1518" />',Q2]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>3﹒<spring:message code="LB.W1335" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q3"/>A﹒<spring:message code="LB.W1336" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q3"/>B﹒<spring:message code="LB.W1337" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q3"/>C﹒<spring:message code="LB.D0572" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q3" name="Q3" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1519" />',Q3]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>4﹒<spring:message code="LB.W1339" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q4"/>A﹒<spring:message code="LB.W1340" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q4"/>B﹒<spring:message code="LB.W1341" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q4"/>C﹒<spring:message code="LB.W1342" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q4" name="Q4" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1521" />',Q4]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>5﹒<spring:message code="LB.W1343" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q5"/>A﹒<spring:message code="LB.W1344" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q5"/>B﹒<spring:message code="LB.W1345" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q5"/>C﹒<spring:message code="LB.W1346" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q5" name="Q5" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1522" />',Q5]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>6﹒<spring:message code="LB.W1347" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q6"/>A﹒<spring:message code="LB.W1348" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q6"/>B﹒<spring:message code="LB.W1349" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q6"/>C﹒<spring:message code="LB.W1350" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q6" name="Q6" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1523" />',Q6]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>7﹒<spring:message code="LB.W1351" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="check-block">
														<input type="checkbox" value="A" id="Q71" name="Q71"/>A﹒<spring:message code="LB.W1352" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="B" id="Q72" name="Q72"/>B﹒<spring:message code="LB.W1353" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="C" id="Q73" name="Q73"/>C﹒<spring:message code="LB.W1354" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="D" id="Q74" name="Q74"/>D﹒<spring:message code="LB.W1355" />
														<span class="ttb-check"></span>
													</label>
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>8﹒<spring:message code="LB.W1356" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q8"/>A﹒<spring:message code="LB.W1326" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q8"/>B﹒<spring:message code="LB.W1327" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q8"/>C﹒<spring:message code="LB.W1259" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q8" name="Q8" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1525" />',Q8]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
								<!--  貴公司對金融商品的知識？（本題可複選，取得分最高者計算） -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>9﹒<spring:message code="LB.W1357" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="check-block">
														<input type="checkbox" value="A" id="Q91" name="Q91" />
															A﹒<spring:message code="LB.W1358" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="B" id="Q92" name="Q92"/>
															B﹒<spring:message code="LB.W1359" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="C" id="Q93" name="Q93"/>
															C﹒<spring:message code="LB.W1360" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="D" id="Q94" name="Q94" onclick="checkQ9()"/>
															D﹒<spring:message code="LB.D0930" />
														<span class="ttb-check"></span>
													</label>
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>10﹒<spring:message code="LB.W1362" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q10"/>A﹒<spring:message code="LB.W1363" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q10"/>B﹒<spring:message code="LB.W1364" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q10"/>C﹒<spring:message code="LB.W1365" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q10" name="Q10" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1527" />',Q10]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>11﹒<spring:message code="LB.W1366" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q11"/>A﹒<spring:message code="LB.W1367" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q11"/>B﹒<spring:message code="LB.W1368" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q11"/>C﹒<spring:message code="LB.W1369" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q11"/>D﹒<spring:message code="LB.W1370" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q11" name="Q11" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1546" />',Q11]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>12﹒<spring:message code="LB.W1371" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q12"/>A﹒<spring:message code="LB.W1367" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q12"/>B﹒<spring:message code="LB.W1368" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q12"/>C﹒<spring:message code="LB.W1369" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q12"/>D﹒<spring:message code="LB.W1370" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q12" name="Q12" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1528" />',Q12]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>13﹒<spring:message code="LB.W1372" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q13"/>A﹒<spring:message code="LB.W1373" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q13"/>B﹒<spring:message code="LB.W1374" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q13"/>C﹒<spring:message code="LB.W1375" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q13"/>D﹒<spring:message code="LB.D1079" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q13" name="Q13" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1547" />',Q13]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>14﹒<spring:message code="LB.W1377" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q14"/>A﹒<spring:message code="LB.X2600" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q14"/>B﹒<spring:message code="LB.W1353" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q14"/>C﹒<spring:message code="LB.X2601" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q14"/>D﹒<spring:message code="LB.X2602" />
														<span class="ttb-radio"></span>
													</label>
													<!-- 不在畫面上顯示的span -->
<!-- 													<span class="hideblock"> -->
<!-- 														驗證用的input -->
<%-- 														<input id="validate_Q14" name="Q14" type="radio" class=" --%>
<%-- 															validate[funcCall[validate_Radio['<spring:message code="LB.X1529" />',Q14]]]" --%>
<!-- 															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 													</span> -->
												</div>
											</p>
										</div>
									</li>
								</ul>
							</div>

                  			<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>

						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<div id="mailDialog" title="電子郵件確認">
		<div style="text-align:center">
	<c:if test="${sessionScope.dpmyemail == ''}">
			<p id="MailShow">
				您的電子信箱目前為：未設定
			</p>
			<input type="button" id="CMSUBMIT_Y" value="前往設定" class="ttb-button btn-flat-orange"/>
			</div>
	</div>
	</c:if>
	<c:if test="${sessionScope.dpmyemail != ''}">
			<p id="MailShow">
				您的電子信箱目前為：${sessionScope.dpmyemail}
			</p>
				<input type="button" id="CMSUBMIT_N" value="不正確" class="ttb-button btn-flat-orange"/>
				<input type="button" id="CMSUBMIT_Y" value="正確" class="ttb-button btn-flat-orange"/>
		</div>
	</div>
	</c:if>
	
	
	<%@ include file="../index/footer.jsp"%>
</body>
</html>