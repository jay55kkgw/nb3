<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	//表單驗證
	$("#formID").validationEngine({binded:false,promptPosition:"inline"});
	
	$("#CMSUBMIT").click(function(){
		//表單驗證
		if(!$("#formID").validationEngine("validate")){
			e = e || window.event;//for IE
			e.preventDefault();
		}
		$("#formID").validationEngine("detach");
		
// 		if($("#newEmail").val() == ""){
// 			alert("請輸入Email");
// 			return false;
// 		}

// 		if(!checkEmail("newEmail","電子郵件帳號")){
// 			return false;
// 		}
		
		$("#formID").attr("action","${__ctx}/FUND/TRANSFER/change_fundemail");
		$("#formID").submit();
	});
	$("#cancelButton").click(function(){
		$("#formID").validationEngine("detach");
		$("#WANAEMAIL").val("NONEED");
		
		var TXID = "${result_data.data.TXID}";
		
		if(TXID == "C021" || TXID == "C032"){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/query_fund_transfer_data");
		}
		else if(TXID == "C016"){
			$("#formID").attr("action","${__ctx}/FUND/PURCHASE/fund_purchase_select");
		}
		else if(TXID == "C017"){
			$("#formID").attr("action","${__ctx}/FUND/REGULAR/fund_regular_select");
		}
		else if(TXID == "C024" || TXID == "C033"){
			$("#formID").attr("action","${__ctx}/FUND/REDEEM/fund_redeem_data");
		}
		else if(TXID == "C031"){
			$("#formID").attr("action","${__ctx}/FUND/RESERVE/PURCHASE/fund_reserve_purchase_select");
		}
		else if(TXID == "C111"){
			
		}
		$("#formID").submit();
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.Funds" /></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.W1064" /></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Funds_NonMenu" />Email<spring:message code= "LB.X0438" /></li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code= "LB.Funds_NonMenu" /> Email <spring:message code= "LB.X0438" /></h2>
				<form id="formID" method="post">
					<input type="hidden" name="TXID" value="${result_data.data.TXID}"/>
					<input type="hidden" name="DPUSERID" value="${result_data.data.UID}"/>
					<input type="hidden" id="WANAEMAIL" name="WANAEMAIL"/>
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												Email <spring:message code= "LB.X0438" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="newEmail" name="newEmail" class="ttb-input text-input validate[required,funcCall[validate_EmailCheck[newEmail]]]" maxLength="50" size="30"/>
										</div>
									</span>
								</div>
								<p>
									<!-- 為符合金管會106年5月2日金管銀國字第10600072470號函辦理網路銀行基金即時確認，請留存EMAIL，以便寄發確認通知。 -->
									<spring:message code="LB.X1961"/>
								</p>
							</div>
							<!--略過 -->
		               		<input type="button" id="cancelButton" value="<spring:message code="LB.X1782"/>" class="ttb-button btn-flat-gray"/>	
							<!-- 更新並繼續  -->
		               		<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X1960"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>