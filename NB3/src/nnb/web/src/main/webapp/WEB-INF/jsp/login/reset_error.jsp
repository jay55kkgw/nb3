<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
	<script type="text/javascript">

		$(document).ready(function() {
			$("#loginout").click(function() {
				initBlockUI();
				fstop.getPage('${pageContext.request.contextPath}'+'/login','', '');
			});		
		});

	</script>

</head>
<body>
	<main class="col-12">
		<section id="main-content" class="container">
			<h2><spring:message code="LB.Transaction_result" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
<%-- 				<form id="formId" method="post" action="${__ctx}/login"> --%>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-result-block">
							<%--                         <img src="${__ctx}/img/icon-success.svg"> --%>
						</div>
						<ul class="ttb-result-list">
							<li>
								<h3><spring:message code="LB.Transaction_code" /></h3>
								<p>${error.msgCode}</p>
							</li>
							<li>
								<h3><spring:message code="LB.Transaction_message" /></h3>
								<p>${error.message}</p>
							</li>
						</ul>
						
						<div class="text-left">
							<ol class="list-decimal text-left">
							</ol>
						</div>
						
						<button type="button" id="loginout"	class="ttb-button btn-flat-orange"><spring:message code="LB.Logout" /></button>

					</div>
				</div>
<!-- 				</form> -->
		</section>
	</main>
<body>
</html>