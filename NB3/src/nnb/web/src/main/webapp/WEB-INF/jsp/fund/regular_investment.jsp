<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩f
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			
			var mail = '${sessionScope.dpmyemail}';
// 		 	mail='';
			
			if( ''== mail){
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X2637' />"], 
						'<spring:message code= "LB.Confirm" />', 
						null
					);
				$("#errorBtn1").click(function(e) {
					$('#error-block').hide();
				});
			}
			
			shwd_prompt_init(false);
		});

		function init() {
// 			initFootable();
			//改用dataTable
			initDataTable();
			//表單驗證
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
		}

		// 點按鈕click
		function relieve(index) {
			// 塞資料
			$("#helperjson").val($("#" + index).val());
			//
			if (!$('#formId').validationEngine('validate')) {
				e = e || window.event; // for IE
				e.preventDefault();
			} else {
				$("#formId").validationEngine('detach');
				initBlockUI(); //遮罩
				$("#formId").submit();
			}
		}
		
		function backBtnClick() {
			top.location="/nb3/FUND/QUERY/regular_invest";
		}
	</script>
</head>

<body>
	<c:set var="SHWD" value="${regular_investment.data.SHWD}"></c:set>
    <%@ include file="fund_shwd.jsp"%>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 定期投資約定變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1057" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12">
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1057" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId"
					action="${__ctx}/FUND/ALTER/regular_investment_step1">
					<c:set var="BaseResultData" value="${regular_investment.data}"></c:set>
					<input type="hidden" id="helperjson" name="helperjson" value=''>
					<input type="hidden" id="CUSIDN" name="CUSIDN" value='${BaseResultData.CUSIDN}'>
					<input type="hidden" id="hideid_CUSIDN" name="hideid_CUSIDN" value='${BaseResultData.hideid_CUSIDN}'>
					<input type="hidden" id="CUSNAME" name="CUSNAME" value='${BaseResultData.CUSNAME}'>
					<input type="hidden" id="SALFLG" name="SALFLG" value='${BaseResultData.SALFLG}'>
					<div class="main-content-block row">
						<div class="col-12">
							<ul class="ttb-result-list">
								<li>
									<!-- 資料總數 -->
									<h3>
										<spring:message code="LB.Total_records" />
									</h3>
									<p>
										${BaseResultData.COUNT}
										<spring:message code="LB.Rows" />
									</p>
								</li>
								<!-- 身份證字號/統一編號-->
								<li>
									<h3>
										<spring:message code="LB.Id_no" />
									</h3>
									<p>
										${BaseResultData.hideid_CUSIDN}
									</p>
								</li>
								<!-- 姓名 -->
								<li>
									<h3>
										<spring:message code="LB.Name" /> 
									</h3>
									<p>
										${BaseResultData.hideid_NAME}
									</p>
								<li>
							</ul>
							<table class="stripe table-striped ttb-table dtable" style="text-align:center" data-toggle-column="first">
								<thead>
									<!-- 身份證字號/統一編號-->
<%-- 									<tr> --%>
<%-- 										<th> --%>
<%-- 											<spring:message code="LB.Id_no" /> --%>
<%-- 										</th> --%>
<%-- 										<th class="text-left" colspan="7"> --%>
<%-- 											${BaseResultData.hideid_CUSIDN} --%>
<%-- 										</th> --%>
<%-- 									<tr> --%>
<!-- 										姓名 -->
<%-- 									<tr> --%>
<%-- 										<th> --%>
<%-- 											<spring:message code="LB.Name" /> --%>
<%-- 										</th> --%>
<%-- 										<th class="text-left" colspan="7"> --%>
<%-- 											${BaseResultData.hideid_NAME} --%>
<%-- 										</th> --%>
<%-- 									<tr> --%>
									<tr>
										<!--信託帳號 -->
										<th nowrap><spring:message code="LB.W0944" /></th>
										<!-- 扣款標的 -->
										<th nowrap><spring:message code="LB.W1041" /></th>
										<!-- 類別 -->
										<th nowrap><spring:message code="LB.D0973" /></th>
										<!-- 扣款來源 -->
										<th nowrap><spring:message code="LB.D1623" /></th>
										<!-- 每次定額申購金額＋<br>不定額基準申購金額</th> -->
										<th><spring:message code="LB.W1043" />＋<br><spring:message code="LB.W1044" /></th>
										<!-- 扣款日 -->
										<th nowrap><spring:message code="LB.W1047" /></th>
										<!-- 暫停扣款起日<br>暫停扣款迄日 -->
										<th nowrap><spring:message code="LB.W1049" /><br><spring:message code="LB.W1050" /></th>
										<!--按鈕 -->
										<th nowrap><spring:message code="LB.Execution_option" /></th>
									</tr>
								</thead>
								<tbody <c:if test="${BaseResultData.COUNT == '0'}">style="display:none"</c:if> >
									<c:forEach varStatus="loop" var="dataList" items="${ BaseResultData.REC }">
										<tr>
										<input type="hidden" id="helperjson_${loop.index}" value='${dataList.helperjson}'>
											<td>${dataList.hide_CDNO }</td>
											<td class="text-center">${dataList.FUNDLNAME }</td>
											<td class="text-center"><spring:message code="${dataList.str_MIP }" /></td>
											<td class="text-center"><spring:message code="${dataList.str_DAMT }" /></td>
											<td class="text-center">
												${dataList.ADCCYNAME }<br/>
												${dataList.display_OPAYAMT }
											</td>
											<c:choose>
												<c:when test="${dataList.DAMT == '2'}">
													<td nowrap class="text-center">
														${dataList.OPAYDAY1 }
														${dataList.OPAYDAY2 }
														${dataList.OPAYDAY3 }
														${dataList.OPAYDAY4 }
														${dataList.OPAYDAY5 }
														${dataList.OPAYDAY6 }
													</td>
												</c:when>
												<c:otherwise>
													<td nowrap class="text-center">
														${dataList.OPAYDAY1 }
														${dataList.OPAYDAY2 }
														${dataList.OPAYDAY3 }
														${dataList.OPAYDAY4 }
														${dataList.OPAYDAY5 }
														${dataList.OPAYDAY6 }
														${dataList.OPAYDAY7 }
														${dataList.OPAYDAY8 }
														${dataList.OPAYDAY9 }
													</td>
												</c:otherwise>
											</c:choose>
											<td class="text-center">
												${dataList.display_STOPBEGDAY }
												<br>
												${dataList.display_STOPENDDAY }
											</td>
											<td nowrap>
											
										<c:choose>
												<c:when test="${dataList.b_displayFlag == 'Y'}">
												<!--執行選項 -->
												<input class="ttb-sm-btn btn-flat-orange" type="button" value="<spring:message code="LB.Confirm" />"
													name="CMSUBMIT" onclick="relieve('helperjson_${loop.index}')" />
												</c:when>
												<c:otherwise>
															<spring:message code="LB.X1628"/><br><spring:message code="LB.X1629"/>/<br><spring:message code="LB.X1630"/>
												</c:otherwise>
											</c:choose>
											
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						<input class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" type="button"
								value="<spring:message code="LB.X0420" />" onclick="backBtnClick()"/>
						</div>
					</div>
					<div >
							<ol class="description-list list-decimal">
						<!-- 						說明： -->
						<p>
							<spring:message code="LB.Description_of_page" />
						</p>
								<li>
									<span>
										<spring:message code="LB.Regular_Investment_P1_D1-1"/><br>
										<spring:message code="LB.Regular_Investment_P1_D1-2"/><br>
										<spring:message code="LB.Regular_Investment_P1_D1-3"/><br>
										<spring:message code="LB.Regular_Investment_P1_D1-4"/>
									</span>
								</li>
								<li>
									<span>
										<spring:message code="LB.Regular_Investment_P1_D2-1"/><br>
										<spring:message code="LB.Regular_Investment_P1_D2-2"/><br>
										<spring:message code="LB.Regular_Investment_P1_D2-3"/><br>
										<spring:message code="LB.Regular_Investment_P1_D2-4"/><br>
										<spring:message code="LB.C001_P1_D2-5"/>
									</span>
								</li>
								<li>
									<span>
										<spring:message code="LB.Regular_Investment_P1_D3"/>
									</span>
								</li>
						</ol>
					</div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>


</body>

</html>