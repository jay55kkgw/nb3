<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
	}
	

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	<!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 外匯存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0299" /></li>
		</ol>

	</nav>
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0299" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
				 <c:set var="BaseResultData" value="${closing_fcy_account_result.data}"></c:set>
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                             <div class="ttb-message">
                                <span><spring:message code="LB.D0482" /></span>
                            </div>
                            <!-- 交易日期-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Transaction_date" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.CMQTIME}</span>
									</div>
                                </span>		
                            </div>
								<!--轉出帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Payers_account_no" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.FYACN}</span>
									</div>
                                </span>		
                            </div>	
							
							<!--轉出金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Deducted"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                   <span>
                                   ${BaseResultData.CRY}
                                   ${BaseResultData.TDBAMT}
                                   </span>
                                    </div>
                                </span>
                            </div>
							
							<!--轉入帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Payees_account_no"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.TSFAN}</span>
									</div>
                                </span>		
                            </div>	
							<!--轉入金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Buy"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                   <span>
                                   ${BaseResultData.CURCODE}
                                   ${BaseResultData.ATRAMT}
                                   </span>
                                    </div>
                                </span>
                            </div>
							<!--使用匯率-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D0473"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                   <span>${BaseResultData.CHGROE}</span>
                                    </div>
                                </span>
                            </div>
							<!--結清帳面餘額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D0483"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                   <span>${BaseResultData.CURAMT}</span>
                                    </div>
                                </span>
                            </div>
						</div>
                    </div>
                </div>
				</form>
                <ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
                  	<li><strong style="FONT-WEIGHT: 400"><spring:message code="LB.Closing_Fcy_Account_P4_D1" /></strong> </li>        
                </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>