<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<!-- 查詢時間： -->
	<label><spring:message code="LB.Inquiry_time" />：</label>
	<label>${CMQTIME}</label>
	<br />
	<br />
	<!-- 資料總數： -->
	<label><spring:message code="LB.Total_records" />：</label>
	<label>${COUNT}&nbsp;<spring:message code="LB.Rows" /></label>
	<br />
	<br />
	
	<table class="print">
		<tr>
			<td><spring:message code="LB.Types_of_Credit_Card" /></td><!-- 卡別 -->
			<td><spring:message code="LB.Credit_card_number" /></td><!-- 卡號 -->
			<td><spring:message code="LB.Card_holder" /></td><!-- 持卡人 -->
		</tr>
		<c:forEach items="${dataListMap}" var="map">
			<tr>
				<td class="text-center">${map.TYPENAME}</td>
				<td class="text-center">
					${map.CCNCOVER}
					<c:if test="${map._PT_FLG == '0'}">
					（<spring:message code="LB.Attached_card" /> ）<!-- 附卡 -->
					</c:if>
				</td>
				<td class="text-center">${map.CR_NAME}</td>
			</tr>
		</c:forEach>
	</table>
	<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	<table class="print">
		<tr>
			<td style="text-align: center">
				<!-- 信用額度  -->
				<spring:message code="LB.Credit_line" />
			</td>
			<td style="text-align: center">
				<!-- 預借現金額度 -->
				<spring:message code="LB.Cash_advanced_line" />
			</td>
			<td style="text-align: center">
				<!-- 目前已使用額度  -->
				<spring:message code="LB.Current_balance" />
			</td>
			<td style="text-align: center">
				<!-- 已動用預借現金  -->
				<spring:message code="LB.Cash_advanceUsed" />
			</td>
			<td style="text-align: center">
				<!-- 本期循環信用利率  -->
				<spring:message code="LB.Current_revolving_interest" />
			</td>
			<td style="text-align: center">
				<!-- 自動扣繳帳號  -->
				<spring:message code="LB.Auto-pay_account_number" />
			</td>
			<td style="text-align: center">
				<!-- 扣繳方式  -->
				<spring:message code="LB.Payment_method" />
			</td>
		</tr>
		<tr>
			<td style="text-align: right">
				<!-- 信用額度  -->${CRLIMIT}
			</td>
			<td style="text-align: right">
				<!-- 預借現金額度 -->${CSHLIMIT}
			</td>
			<td style="text-align: right">
				<!-- 目前已使用額度  -->${ACCBAL}
			</td>
			<td style="text-align: right">
				<!-- 已動用預借現金  -->${CSHBAL}
			</td>
			<td style="text-align: right">
				<!-- 本期循環信用利率  -->${INT_RATE}%
			</td>
			<td class="text-center">
				<!-- 自動扣繳帳號  -->
				${PAYMT_ACCT}
			</td>
			<td class="text-center">
				<!-- 扣繳方式  -->
				${DBCODE}
			</td>
		</tr>
	</table>
	<div><br/></div>
	<div><br/></div>
	<div><br/></div>
	<p><spring:message code="LB.Overview_P1_note" />
		<!-- 註：「目前已使用額度」不含已刷卡授權通過，但商店尚未請款之交易金額；如須查詢「可用餘額」請洽本行客服人員 --></p>
	<br/>
	<table   class="print">
		<tr>
			<!-- 應繳總金額  -->
			<td><spring:message code="LB.The_total_amount_payable" /></td>
			<!-- 本期最低金額  -->
			<td><spring:message code="LB.Minimum_payment" /></td>
			<!-- 結帳日  -->
			<td><spring:message code="LB.Statement_date" /></td>
			<!-- 繳款截止日  -->
			<td><spring:message code="LB.Due_date" /></td>
		</tr>
		<tr>
			<td style="text-align: right">
			<!-- 應繳總金額  -->${CURR_BAL}
			</td>
			<td style="text-align: right">
			<!-- 本期最低金額  -->${TOTL_DUE}
			</td>
			<td class="text-center">
			<!-- 結帳日  -->
				${STMT_DAY}
			</td>
			<td class="text-center">
			<!-- 繳款截止日  -->
				${STOP_DAY}
			</td>
		</tr>
	</table>
	<div>
		<spring:message code="LB.Description_of_page" /><!-- 說明 -->
		<ol >
		<li><spring:message code="LB.Overview_P1_D1" />
			<!-- 若附卡持卡人查詢時，應繳總額、最低應繳金額、繳款截止日、扣帳帳號、本期已繳金額等欄位均顯示為 0。 --></li>
		<li><spring:message code="LB.Overview_P1_D2" />
			<!-- 各正、附卡帳單應繳總額係正卡身分證字號合併計算。 --></li>
		<li><spring:message code="LB.Overview_P1_D3" />
			<!-- 為提升服務品質，本行自100年5月起信用卡帳單將由現行各卡帳單改為「依正卡持卡人身分證號碼歸戶方式之整合式帳單」，故若您原約定之轉入帳號為本行信用卡卡號者，將於100年5月1日失效，造成不便，敬請見諒！ --></li>
		</ol>
	</div>
	<br />
</body>
</html>