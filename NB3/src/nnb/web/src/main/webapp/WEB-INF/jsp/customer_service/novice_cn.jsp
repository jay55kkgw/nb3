<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		$("#CMBACK").click(function(e){
			$("#formId").attr("action","${__ctx}/login");
			$("#formId").submit();
		});
	}
	
	function pdfdownload(data){
		window.open("${__ctx}/CUSTOMER/SERVICE/pdf_download?pdf="+data,"_black");
	}
</script>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">新手上路</a></li>
		</ol>
	</nav>
	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主页内容  -->
				<h2>
					新手上路
				</h2>
				<form id="formId" method="post" action="">
					<div class="card-block shadow-box terms-pup-blcok">
	                    <nav class="nav card-select-block text-center d-block" style="padding-top: 30px;" id="nav-tab" role="tablist">
	                        <input type="button" class="nav-item ttb-sm-btn active" id="nav-trans-1-tab" data-toggle="tab" href="#nav-trans-1" role="tab" aria-selected="false" value="首次登入" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-2-tab" data-toggle="tab" href="#nav-trans-2" role="tab" aria-selected="true" value="转账交易" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-3-tab" data-toggle="tab" href="#nav-trans-3" role="tab" aria-selected="true" value="账户余额查询" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-4-tab" data-toggle="tab" href="#nav-trans-4" role="tab" aria-selected="true" value="重设用户名称/签入密码" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-5-tab" data-toggle="tab" href="#nav-trans-5" role="tab" aria-selected="true" value="转入综存定存" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-6-tab" data-toggle="tab" href="#nav-trans-6" role="tab" aria-selected="true" value="买卖外币/约定转账" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-7-tab" data-toggle="tab" href="#nav-trans-7" role="tab" aria-selected="true" value="缴税" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-8-tab" data-toggle="tab" href="#nav-trans-8" role="tab" aria-selected="true" value="定期投资约定变更" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-9-tab" data-toggle="tab" href="#nav-trans-9" role="tab" aria-selected="true" value="定期投资申购" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-10-tab" data-toggle="tab" href="#nav-trans-10" role="tab" aria-selected="true" value="信用卡持卡总览" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-11-tab" data-toggle="tab" href="#nav-trans-11" role="tab" aria-selected="true" value="缴本行信用卡费" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-12-tab" data-toggle="tab" href="#nav-trans-12" role="tab" aria-selected="true" value="我的Email设定" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-13-tab" data-toggle="tab" href="#nav-trans-13" role="tab" aria-selected="true" value="常用账号设定" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-14-tab" data-toggle="tab" href="#nav-trans-14" role="tab" aria-selected="true" value="申请电子账单" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-15-tab" data-toggle="tab" href="#nav-trans-15" role="tab" aria-selected="true" value="国内台币汇入汇款通知设定" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-16-tab" data-toggle="tab" href="#nav-trans-16" role="tab" aria-selected="true" value="操作手册下载" />
	                    </nav>
	                    <div class="col-12 tab-content" id="nav-tabContent">
	                    	<!-- 首次登入 -->
	                    	<div class="ttb-input-block tab-pane fade show active" id="nav-trans-1" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">首次登入</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您首次登入后，变更签入密码及交易密码。</a>。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/firsttime_cn.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/firsttime-2_cn.png" alt="Second slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicatorWord" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>1.请输入旧的签入密码及新签入密码。</p>
	                                                <p>2.请输入旧的交易密码及新交易密码。</p>
	                                                <p>3.输入完后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>显示变更后之结果。</p>
	                                                <p>按『确定』后，回到登入页，再以变更后签入密码登入。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 转账交易 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-2" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">转账交易</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您实时或预约转账交易。</a>。</p>
	                                                </li>
	                                                <li>
	                                                    <p>已约定转入账号，可以『交易密码(SSL)』、『电子签章(载具i-Key)』、『芯片金融卡』、『装置推播认证』进行转账；未约定转入账号，请以『电子签章(载具i-Key)』、『芯片金融卡』、『装置推播认证』进行转账。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator2" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N070-1_cn.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N070-2_cn.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N070-5_cn.png" alt="Third slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator2word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>输入转出账号、转入账号、转账金额等必要字段后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator2" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator2" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>输入以黄色标记之转入账号，选择交易机制。若为芯片金融卡(需输入验证码)，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator2" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator2" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>显示交易结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator2" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator2" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 存款帐户查询 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-3" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">账户余额查询</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您查询所有台币活期账户之余额。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator3" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/balance_query_1_cn.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/balance_query_2_cn.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/balance_query_3_cn.png" alt="Second slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator3word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『台币服务』项目下『账户余额查询』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>显示所有台币活期帐户余额。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>点选快速选单可直接导引至其他台币服务交易。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 重设使用者名称/签入密码 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-4" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">重设使用者名称/签入密码</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您忘记使用者名称或签入密码时使用。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator4" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_1_cn.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_2_cn.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_3_cn.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_4_cn.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator4word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『忘记使用者名称/密码』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>点选『重设使用者名称/签入密码』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>1.请输入身分证字号、使用者名称及签入密码。</p>
	                                                <p>2.按『取得简讯验证码』，待手机收到简讯后，输入简讯验证码。</p>
	                                                <p>3.请输入图形验证码并确认插入本行芯片金融卡后，点选『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤四</h5>
	                                                <p>显示重设之结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 转入综存定存 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-5" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">转入综存定存</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您实时或预约转入综存定存。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator5" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_1_CN.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_2_CN.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_3_CN.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_4_CN.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator5word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『转入综存定存』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>输入转出账号、转入账号、转账金额等必要字段后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>输入以黄色标记之转入账号，选择交易机制。若为芯片金融卡(需输入验证码)，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤四</h5>
	                                                <p>显示交易结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 买卖外币/约定转账 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-6" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">买卖外币/约定转账</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您实时或预约换汇及转账交易。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator6" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_1_CN.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_2_CN.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_3_CN.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_4_CN.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_5_CN.png" alt="Five slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_6_CN.png" alt="Six slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_7_CN.png" alt="Seven slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator6word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『买卖外币/约定转账』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>详阅注意事项后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>输入转出账号、转出币别、转入账号、转入币别、转账金额等必要字段后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤四</h5>
	                                                <p>确认转账资料后，按『取得汇率/议价』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤五</h5>
	                                                <p>确认汇率数据后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤六</h5>
	                                                <p>输入以黄色标记之转入账号，选择交易机制，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤七</h5>
	                                                <p>显示交易结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 缴税 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-7" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">缴税</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您实时或预约缴税。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator7" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_1_CN.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_2_CN.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_3_CN.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_4_CN.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator7word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『缴税』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>输入转出账号、税别、缴税金额等必要字段后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>选择交易机制。若为芯片金融卡(需输入验证码)，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤四</h5>
	                                                <p>显示交易结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 定期投资约定变更 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-8" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">定期投资约定变更</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您基金定期投资约定变更。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator8" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_1_CN.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_2_CN.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_3_CN.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_4_CN.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_5_CN.png" alt="Five slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator8word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『定期投资约定变更』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>选择欲变更数据，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>选择变更项目，并输入必要字段，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤四</h5>
	                                                <p>确认数据，输入交易密码后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤五</h5>
	                                                <p>显示变更结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 定期投资申购 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-9" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">定期投资申购</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您基金定期定额申购。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator9" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_1_CN.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_2_CN.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_3_CN.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_4_CN.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_5_CN.png" alt="Five slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_6_CN.png" alt="Six slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_7_CN.png" alt="Seven slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator9word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『定期投资申购』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>输入基金公司名称、基金名称、申购金额等必要字段后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>阅读基金风险，勾选已阅读基金风险后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤四</h5>
	                                                <p>选择公开说明书/投资人须知取得方式后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤五</h5>
	                                                <p>阅读报酬说明，勾选已阅读后，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤六</h5>
	                                                <p>确认数据并输入交易密码，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤七</h5>
	                                                <p>显示交易结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 信用卡持卡总览 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-10" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">信用卡持卡总览</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您查看已持有之信用卡。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator10" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_overview_1_cn.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_overview_2_cn.png" alt="Second slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator10word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『信用卡持卡总览』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator10" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator10" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>显示查询之结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator10" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator10" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 缴本行信用卡费 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-11" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">缴本行信用卡费</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您缴交本行信用卡之账单。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator11" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_1_cn.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_2_cn.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_3_cn.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_4_cn.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator11word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『缴本行信用卡费』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>1.选择或输入『转出账号』、『转入信用卡账号』、『缴款金额』等必要字段。</p>
	                                                <p>2.输入完后，点选『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>输入以黄色标记之转入账号，选择交易机制。若为芯片金融卡(需输入验证码)，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤四</h5>
	                                                <p>显示缴费之结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 我的Email设定 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-12" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">我的Email设定</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您设定您个人的Email。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator12" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_1_cn.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_2_cn.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_3_cn.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_4_cn.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator12word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『Email设定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>选择『我的Email』，点选『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>输入您的电子邮箱，选择交易机制，按『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤四</h5>
	                                                <p>显示设置之结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 常用账号设定 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-13" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">常用账号设定</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您设定您常用的账号。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator13" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/account_setting_1_cn.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/account_setting_2_cn.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/account_setting_3_cn.png" alt="Third slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator13word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『常用账号设定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator13" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator13" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>输入『转入账号』及『好记名称』，点选『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator13" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator13" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>显示设置之结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator13" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator13" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 申请电子账单 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-14" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">申请电子账单</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您申请电子账单。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator14" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_1_cn.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_2_cn.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_3_cn.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_4_cn.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_5_cn.png" alt="Five slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator14word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『电子对账单申请』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>选择欲申请项目，点选『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>详阅约定条款，若同意请点选『同意并继续』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤四</h5>
	                                                <p>输入交易密码，点选『确定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤五</h5>
	                                                <p>显示申请之结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 国内台币汇入汇款通知设定 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-15" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">国内台币汇入汇款通知设定</h3>
	                                    <p>功能介绍</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您设定汇入汇款通知。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator15" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N935_cn_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N935_cn_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N935_cn_3.png" alt="Third slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator15word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤一</h5>
	                                                <p>点选『国内台币汇入汇款通知设定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator15" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator15" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤二</h5>
	                                                <p>勾选欲设定之『账号』及填写『金额』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator15" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator15" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步骤</p>
	                                                <h5>步骤三</h5>
	                                                <p>显示设置之结果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator15" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator15" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 操作手冊下載 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-16" role="tabpanel" aria-labelledby="nav-home-tab">
	                        	<h3 class="guide-function-title">操作手册下载</h3>
								<div style="display: flex">
									<div class="element-area" style="background-color: #ffffff">
										<!-- 登入 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('login')">
												<img src="${__ctx}/img/icon-ob_login.svg?a=${jscssDate}" style="border-radius:0%;">
												<span><spring:message code="LB.Login" /></span>
											</a>
										</div>
										<!-- 我的首頁 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('Myhome')">
												<img src="${__ctx}/img/menu-icon-01.svg?a=${jscssDate}">
												<span><spring:message code="LB.My_home" /></span>
											</a>
										</div>
										<!-- 帳戶總覽 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('AccountOverview')">
												<img src="${__ctx}/img/menu-icon-02.svg?a=${jscssDate}">
												<span><spring:message code="LB.Account_Overview" /></span>
											</a>
										</div>
										<!-- 臺幣服務 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('NTDService')">
												<img src="${__ctx}/img/menu-icon-03.svg?a=${jscssDate}">
												<span><spring:message code="LB.NTD_Services" /></span>
											</a>
										</div>
										<!-- 外幣服務 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('ForeignCurrency')">
												<img src="${__ctx}/img/menu-icon-04.svg?a=${jscssDate}">
												<span><spring:message code="LB.FX_Service" /></span>
											</a>
										</div>
										<!-- 繳費繳稅 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('PaymentofTaxes')">
												<img src="${__ctx}/img/menu-icon-05.svg?a=${jscssDate}">
												<span><spring:message code="LB.W0366" /></span>
											</a>
										</div>
										<!-- 貸款專區 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('LoanService')">
												<img src="${__ctx}/img/menu-icon-06.svg?a=${jscssDate}">
												<span><spring:message code="LB.Loan_Service" /></span>
											</a>
										</div>
									</div>
								</div>
								<div style="display: flex">
									<div class="element-area" style="background-color: #ffffff">
										<!-- 基金 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('FundsBond')">
												<img src="${__ctx}/img/menu-icon-07.svg?a=${jscssDate}">
												<span><spring:message code="LB.Funds" /></span>
											</a>
										</div>
										<!-- 黃金存摺 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('GoldPassbook')">
												<img src="${__ctx}/img/menu-icon-08.svg?a=${jscssDate}">
												<span><spring:message code="LB.W1428" /></span>
											</a>
										</div>
										<!-- 信用卡 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('CreditCard')">
												<img src="${__ctx}/img/menu-icon-09.svg?a=${jscssDate}">
												<span><spring:message code="LB.Credit_Card" /></span>
											</a>
										</div>
										<!-- 線上服務專區 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('OnlineServices')">
												<img src="${__ctx}/img/menu-icon-10.svg?a=${jscssDate}" style="border-radius:0%;">
												<span><spring:message code="LB.X0262" /></span>
											</a>
										</div>
										<!-- 個人服務 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('PersonalServices')">
												<img src="${__ctx}/img/menu-icon-11.svg?a=${jscssDate}">
												<span><spring:message code="LB.Personal_Service" /></span>
											</a>
										</div>
										<!-- 線上申請 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('Register')">
												<img src="${__ctx}/img/icon-03.svg?a=${jscssDate}">
												<span><spring:message code="LB.Register" /></span>
											</a>
										</div>
										<!-- 附件 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('Attachment')">
												<img src="${__ctx}/img/icon-attach.svg?a=${jscssDate}">
												<span>附件</span>
											</a>
										</div>
									</div>
								</div>
	                        </div>
	                        <input type="BUTTON" class="ttb-button btn-flat-gray" value="返回" name="CMBACK" id="CMBACK">
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	<script type="text/javascript">
		//步驟說明變換
		var carousel1 = $('#carouselGuideIndicator').carousel();
		var carousel2= $('#carouselGuideIndicatorWord').carousel();
		carousel1.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel2.carousel(to);
		});
	
		var carousel3 = $('#carouselGuideIndicator2').carousel();
		var carousel4= $('#carouselGuideIndicator2word').carousel();
		carousel3.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel4.carousel(to);
		});
	
		var carousel5 = $('#carouselGuideIndicator3').carousel();
		var carousel6= $('#carouselGuideIndicator3word').carousel();
		carousel5.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel6.carousel(to);
		});
	
		var carousel7 = $('#carouselGuideIndicator4').carousel();
		var carousel8= $('#carouselGuideIndicator4word').carousel();
		carousel7.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel8.carousel(to);
		});
		
		var carousel9 = $('#carouselGuideIndicator5').carousel();
		var carousel10= $('#carouselGuideIndicator5word').carousel();
		carousel9.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel10.carousel(to);
		});
	
		var carousel11 = $('#carouselGuideIndicator6').carousel();
		var carousel12= $('#carouselGuideIndicator6word').carousel();
		carousel11.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel12.carousel(to);
		});
	
		var carousel13 = $('#carouselGuideIndicator7').carousel();
		var carousel14= $('#carouselGuideIndicator7word').carousel();
		carousel13.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel14.carousel(to);
		});
	
		var carousel15 = $('#carouselGuideIndicator8').carousel();
		var carousel16= $('#carouselGuideIndicator8word').carousel();
		carousel15.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel16.carousel(to);
		});
	
		var carousel17 = $('#carouselGuideIndicator9').carousel();
		var carousel18= $('#carouselGuideIndicator9word').carousel();
		carousel17.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel18.carousel(to);
		});
		
		var carousel19 = $('#carouselGuideIndicator10').carousel();
		var carousel20= $('#carouselGuideIndicator10word').carousel();
		carousel19.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel20.carousel(to);
		});
		
		var carousel21 = $('#carouselGuideIndicator11').carousel();
		var carousel22= $('#carouselGuideIndicator11word').carousel();
		carousel21.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel22.carousel(to);
		});
		
		var carousel23 = $('#carouselGuideIndicator12').carousel();
		var carousel24= $('#carouselGuideIndicator12word').carousel();
		carousel23.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel24.carousel(to);
		});
		
		var carousel25 = $('#carouselGuideIndicator13').carousel();
		var carousel26= $('#carouselGuideIndicator13word').carousel();
		carousel25.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel26.carousel(to);
		});
		
		var carousel27 = $('#carouselGuideIndicator14').carousel();
		var carousel28= $('#carouselGuideIndicator14word').carousel();
		carousel27.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel28.carousel(to);
		});
		
		var carousel29 = $('#carouselGuideIndicator15').carousel();
		var carousel30= $('#carouselGuideIndicator15word').carousel();
		carousel29.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel30.carousel(to);
		});
	</script>
</body>
</html>
