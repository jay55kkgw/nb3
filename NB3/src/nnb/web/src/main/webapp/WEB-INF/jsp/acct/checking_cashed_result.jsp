<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	
	<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);	
			});
			function init(){
				//使用DataTable
				initDataTable();
				//上一頁按鈕
				$("#previous").click(function() {
					initBlockUI();
					fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/checking_cashed','', '');
				});
				
				$("#printbtn").click(function(){
					var params = {
							"jspTemplateName":"checking_cashed_result_print",
							"jspTitle":"<spring:message code='LB.Cashed_Note_Details'/>",
							"CMQTIME":"${checking_cashed_result.data.CMQTIME}",
							"cmdate":"${checking_cashed_result.data.cmedate}",
							"COUNT":"${checking_cashed_result.data.COUNT}"
						};
						openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});				
			}
			//選項
		 	function formReset() {
// 		 		initBlockUI();
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/checking_cashed.xls");
		 		}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/checking_cashed.txt");
		 		}else if ($('#actionBar').val()=="oldtxt"){
					$("#downloadType").val("OLDTXT");
					$("#templatePath").val("/downloadTemplate/checking_cashedOLD.txt");
		 		}
// 	    		ajaxDownload("${__ctx}/ajaxDownload","formId","finishAjaxDownload()");
				$("#formId").attr("target", "");
	            $("#formId").submit();
	            $('#actionBar').val("");
			}
		 	function processQuery(){
 				$("#reRec").attr("action","${__ctx}/NT/ACCT/checking_cashed_result");
 	  			$("#reRec").submit();
			}
		</script>
</head>
<body>
    <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 已兌現票據明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Cashed_Note_Details" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		<main class="col-12">	
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.Cashed_Note_Details" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 						下載為舊txt檔 -->
						<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
					</select>
				</div>
				<!-- 				繼續查詢 -->
				<c:if test="${checking_cashed_result.data.TOPMSG == 'OKOV'}">
					<div class="MessageBar"><spring:message code= "LB.X0076" />
	       			<input id="CMCONTINUEQ" type="button" value="<spring:message code= "LB.X0151" />" name="CMCONTINUEQ" class="ttb-sm-btn btn-flat-orange" onClick="processQuery()">
	       			</div>
	       		</c:if>	 
				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
						 	<!-- 查詢時間 -->
						 	<li>
                                <h3><spring:message code="LB.Inquiry_time" /> ：	</h3>
                                <p>${checking_cashed_result.data.CMQTIME }</p>
                            </li>
                            <!-- 查詢期間 -->
                             <li>
                                <h3><spring:message code="LB.Inquiry_period" /> ： </h3>
                                <p>${checking_cashed_result.data.cmedate }</p>
                            </li>
                             <li>
                                <h3><spring:message code="LB.Total_records" /> ： </h3>
                                <p>${checking_cashed_result.data.COUNT }&nbsp;<spring:message code="LB.Rows" /></p>
                            </li>
						</ul>
						
						<c:forEach var="labelList" items="${ checking_cashed_result.data.labelList }">					
							<ul class="ttb-result-list">
	                            <li>
	                                <h3><spring:message code="LB.Account" /></h3>
	                                <p>${ labelList.ACN }</p>
	                            </li>
	                        </ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
	<%-- 						<thead> --%>
	<%-- 							<tr> --%>
	<%-- 								<th><spring:message code="LB.Account" /></th> --%>
	<%-- 								<th>${ labelList.ACN }</th> --%>
	<%-- 							</tr> --%>
	<%-- 						</thead> --%>
								<thead>
									<tr>
										<th><spring:message code="LB.Change_date" /></th>								
										<th><spring:message code="LB.Summary_1" /></th>
										<th><spring:message code="LB.Withdrawal_amount" /></th>
										<th><spring:message code="LB.Deposit_amount" /></th>
										<th><spring:message code="LB.Loan_Outstanding_1" /></th>
										<th><spring:message code="LB.Checking_account" /></th>
										<th><spring:message code="LB.Data_content" /></th>
										<th><spring:message code="LB.Receiving_Bank" /></th>
										<th><spring:message code="LB.Trading_time" /></th>
									</tr>
								</thead>	
	<!--          列表區 -->
								<tbody>
		         					<c:forEach var="dataList" items="${ labelList.rowListMap }">
										<tr>            	
											<td class="text-center">${dataList.LTD }</td>
											<td class="text-center">${dataList.MEMO_C }</td>
											<td class="text-right">${dataList.DPWDAMT}</td>				          	               
											<td class="text-right">${dataList.DPSVAMT}</td>
											<td class="text-right">${dataList.DPIBAL}</td>                
											<td class="text-center">${dataList.CHKNUM }</td>
											<td class="text-center">${dataList.DATA1 }</td>
											<td class="text-center">${dataList.ORNBRH }</td>
											<td class="text-center">${dataList.SHOWTIME}</td>
										</tr>	
									</c:forEach>
									<!--dataTable 欄位必須對齊 -->
								    <tr>
										<!-- 總計金額 -->
						            	<th style="text-align:center">
						                	<strong><spring:message code="LB.Total_amount" /></strong>
						                </th>
						                <td></td>
										<!-- 支出 -->
						                <td class="text-right">${labelList.TOTAL_DPWDAMT }</td>
										<!-- 收入 -->
						                <td class="text-right">${labelList.TOTAL_DPSVAMT }</td>                    
										<!-- 餘額 -->
						                <td class="text-right">${labelList.LASTDPIBAL }</td>
						                <td></td>
						                <td></td>
						                <td></td>
						                <td></td>
						            </tr>
						        </tbody>
							</table>
						</c:forEach>
							<input id="previous" name="previous" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />	
							<input type="button" class="ttb-button btn-flat-orange" id = "printbtn" value="<spring:message code="LB.Print" />"/>
						</div>
					</div>
					<div class="text-left">
						<ol class="description-list list-decimal">
							<p>
								<spring:message code="LB.Description_of_page" />
							</p>
							<li><spring:message code="LB.Checking_cashed_P2_D1" /></li>
						</ol>
					</div>
				<form method="post" id="reRec">
					<input type="hidden" id="ACN" name="ACN" value="${checking_cashed_result.data.ACN}">
					<input type="hidden" id="FGPERIOD" name="FGPERIOD" value="${checking_cashed_result.data.FGPERIOD}">
					<input type="hidden" id="CMSDATE" name="CMSDATE" value="${checking_cashed_result.data.CMSDATE}">
					<input type="hidden" id="CMEDATE" name="CMEDATE" value="${checking_cashed_result.data.CMEDATE}">
					<input type="hidden" id="USERDATA_X50" name="USERDATA_X50" value="${checking_cashed_result.data.USERDATA_X50}">
				</form>
				<form method="post" id="formId" action="${__ctx}/download">
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="LB.Cashed_Note_Details"/>
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/>
					<input type="hidden" name="CMQTIME" value="${checking_cashed_result.data.CMQTIME}"/>
					<input type="hidden" name="cmdate" value="${checking_cashed_result.data.cmedate}"/>
					<input type="hidden" name="COUNT" value="${checking_cashed_result.data.COUNT}"/>
					<input type="hidden" name="hasMultiRowData" value="true"/>
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="1" />
                    <input type="hidden" name="headerBottomEnd" value="4" />
                    <input type="hidden" name="multiRowStartIndex" value="8" />
                    <input type="hidden" name="multiRowEndIndex" value="8" />
                    <input type="hidden" name="multiRowCopyStartIndex" value="5" />
                    <input type="hidden" name="multiRowCopyEndIndex" value="10" />
                    <input type="hidden" name="multiRowDataListMapKey" value="rowListMap" />
                    <input type="hidden" name="rowRightEnd" value="8" />
					<!-- TXT下載用 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="4"/>
					<input type="hidden" name="txtMultiRowStartIndex" value="11"/>
					<input type="hidden" name="txtMultiRowEndIndex" value="11"/>
					<input type="hidden" name="txtMultiRowCopyStartIndex" value="6"/>
					<input type="hidden" name="txtMultiRowCopyEndIndex" value="12"/>
					<input type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap"/>
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>