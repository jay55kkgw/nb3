<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
	<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
		<br/><br/>
		<div style="text-align:center">
			<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
		</div>
		<br/><br/><br/>
		<div style="text-align:center">
			<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
		</div>
		<br/><br/><br/>
		<label><spring:message code="LB.Inquiry_time" />：</label>
		<label>${CMQTIME}</label>
		<br/><br/>
		<label><spring:message code="LB.Total_records" />：</label>
		<label>${COUNT} <spring:message code="LB.Rows" /></label>
		<br/><br/>
		<table class="print">
			<tr>
				<!-- 案件編號 -->
				<td style="text-align:center"><spring:message code="LB.Loan_CaseNo" /></td>
				<!-- 卡片1名稱 -->
				<td style="text-align:center"><spring:message code= "LB.X0850" />1<spring:message code= "LB.X0059" /></td>
				<!-- 卡片2名稱 -->
				<td style="text-align:center"><spring:message code= "LB.X0850" />2<spring:message code= "LB.X0059" /></td>
				<!-- 卡片3名稱 -->
				<td style="text-align:center"><spring:message code= "LB.X0850" />3<spring:message code= "LB.X0059" /></td>
				<!-- 申請日期 -->
				<td style="text-align:center"><spring:message code= "LB.D0127" /></td>
				<!-- 審核狀況 -->
				<td style="text-align:center"><spring:message code= "LB.D0128" /></td>
				<!-- 申請書閱覽 -->
				<td style="text-align:center"><spring:message code= "LB.D0129" /></td>
			</tr>
			<c:forEach items="${dataListMap}" var="map">
				<tr>
					<td style="text-align: center">${map.RCVNO }</td>
					<td style="text-align: center">${map.CARDNAME1 }</td>
					<td style="text-align: center">${map.CARDNAME2 }</td>		
					<td style="text-align: center">${map.CARDNAME3 }</td>
					<td style="text-align: center">${map.LASTDATE }</td>
					<td style="text-align: center">
						<c:if test="${!map.STATUS.equals('')}">
							<spring:message code="${map.STATUS}"/>
						</c:if>
					</td>
					<td style="text-align: center">
						<input type="button" value="<spring:message code= "LB.X1239" />"/>
					</td>
				</tr>
			</c:forEach>
		</table>
		<br/><br/><br/><br/>
	</body>
</html>