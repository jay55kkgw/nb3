<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 變更密碼所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderChangePin2.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderChangePin.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
	
	    	//列印
	    	$("#PRINT").click(function(){
				var params = {
					"jspTemplateName":"financial_card_renew_step5_print",
					"jspTitle":'補申請晶片金融卡',
					"BRHNAME":"${financial_card_renew_step5.data.BRHNAME}",
					"BRHTEL":"${financial_card_renew_step5.data.BRHTEL}"
				};
				openWindowWithPost("${__ctx}/ONLINE/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
			$("#GOBAKE").click(function(e){
	        	var action = '${__ctx}/login';
				$("form").attr("action", action);
				$("form").submit();
			});
	    	
		}
	</script>
</head>

<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 數位存款帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0348" /></li>
    <!-- 數位存款帳戶補申請晶片金融卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1553" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 數位存款帳戶補申請晶片金融卡 -->
			<h2>補申請晶片金融卡</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<div id="step-bar">
            	<ul>
                	<li class="finished">身分驗證</li>
					<li class="finished">顧客權益</li>
					<li class="finished">申請資料</li>
					<li class="finished">確認資料</li>
					<li class="active">完成申請</li>
                </ul>
            </div>
			<form id="formId" method="post" action="" onSubmit="return processQuery()">
				<input type="hidden" name="ADOPID" value="NB31"> 
				<input type="hidden" name="CUSIDN" value="${financial_card_renew_step5.data.CUSIDN}"> 
				<input type="hidden" name="ACN" value="${financial_card_renew_step5.data.ACN}"> 
				<input type="hidden" name="TRNTYP" value="02">
				<div class="main-content-block row">
					<div class="col-12 terms-block">
							<!-- 完成修改數位存款帳戶資料交易 -->
							<div class="ttb-message"><p>完成申請</p></div>
						<div class="">
<%-- 							<c:if test="${financial_card_renew_step5.data.SEND == '1'}"> --%>
<!-- 								<div class="text-left"> -->
<%-- 								<spring:message code="LB.X1174"/><br><br> --%>
<%-- 								<spring:message code="LB.X1175"/><br> --%>
<%-- 								<spring:message code="LB.X1176"/></div> --%>
<%-- 							</c:if> --%>
<%-- 							<c:if test="${not(financial_card_renew_step5.data.SEND == '1')}"> --%>
								<div class="text-left">
									<spring:message code="LB.X1174"/><BR><BR>
									<!-- 感謝您申請本行數位存款帳戶晶片金融卡，本行將儘速辦理您的申請，並將晶片金融卡寄送至 -->
									<p>謝謝您完成『數位存款帳戶』補申請晶片金融卡，本行將儘速辦理您的申請。</p>
									<spring:message code="LB.X1176"/></font>
								</div>
<%-- 							</c:if> --%>
							
							
						</div>
						<input type="button" id="PRINT" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Print" />"  >
						<input type="button" id="GOBAKE" class="ttb-button btn-flat-orange" value="回首頁">
						<input type="button" class="ttb-button btn-flat-orange" value="繼續其他申請" name="CLOSEPAGE"  onClick="window.close();">
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>