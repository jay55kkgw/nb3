<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<nav id="footer-breadcrumb-nav">
	<ul>
<!-- 		<li><a><spring:message code="LB.Environment_setting"/></a></li> -->
<%-- 		<li><a href="${__ctx}/INDEX/safe" target="_blank"><spring:message code="LB.Network_security"/></a></li> --%>
<%-- 		<li><a href="${__ctx}/INDEX/errorcode" target="_blank"><spring:message code="LB.error_code"/></a></li> --%>
		<li><a>&nbsp;</a></li>
		<li class="top-btn"><button onclick="topFunction()" id="myBtn"
				title="Go to top">
				<img src="${__ctx}/img/top.svg?a=${jscssDate}" />
			</button></li>
	</ul>

</nav>
<footer>
	<div>
		<span>
			<p class="footer-p"><spring:message code="LB.X2245"/></p><!-- 臺灣中小企業銀行 -->
			<ul>
				<li><a href="https://www.tbb.com.tw/web/guest/-173" target="_blank"><spring:message code="LB.X2152"/></a></li><!-- 隱私權聲明 -->
				<li><a href="https://www.tbb.com.tw/web/guest/-551" target="_blank"><spring:message code="LB.X2153"/></a></li><!-- 安全政策 -->
				<li><a href="${__ctx}/CUSTOMER/SERVICE/common_problem?tag=0" target="_blank"><spring:message code="LB.X2244"/></a></li><!-- 系統/瀏覽器需求 -->
			</ul>
		</span>
		<div>
			<p>
				<spring:message code="LB.X2179"/>
				<a href="tel:0800-017-171"> 0800-01-7171</a>
				<spring:message code="LB.X2211" />
				<c:if test="${__i18n_locale eq 'en'}">,</c:if>
			</p><!-- 客服專線請撥 0800-01-7171 -->
			<p><a href="tel:02-2357-7171"> 02-2357-7171</a></p>
		</div>
		<a href="https://www.facebook.com/tbbdreamplus/" target="_blank">
			<span class="facebook-icon">&nbsp;</span>
		</a>
    </div>
</footer>
<script type="text/JavaScript">
var wait = false;
var wait1 = false;
	$(document).ready(function(){
		//避免頁面只有單一輸入框時，按enter會觸發submit兩次
		$(document).keydown(function(event){
 	 	    if(event.keyCode == 13) {
 	 	       	event.preventDefault();
 	 	       	return false;
 	 	    }
 	 	});
		
		//輸入框內按enter可觸發click事件
		$("form input[type=text]").keydown(function(event){
	    	if(event.keyCode == 13 && $('#error-block').is(":hidden") && !wait) {
	    		wait = true;
	    		$("#CMSUBMIT").click();
	    		setTimeout("wait=false;", 500);
	       	}
		});
		
		$("form input[type=password]").keydown(function(event){
	    	if(event.keyCode == 13 && $('#error-block').is(":hidden") && !wait) {
	    		wait = true;
	    		$("#CMSUBMIT").click();
	    		setTimeout("wait=false;", 500);
	       	}
		});
		
		$(document).keydown(function(event){
	    	if(event.keyCode == 13 && !$('#error-block').is(":hidden") && !wait1) {
	    		wait1 = true;
	    		// 會發生按ENTER觸發確認鍵後，來不及顯示遮罩就被隱藏，故註解
	    		$("#CMSUBMIT").blur();
	    		$("#errorBtn1").click();
	       	}
	    	if(event.keyCode == 27) {
	    		if(!$('#errorBtn2').is(":hidden") && !wait1){
	    			wait1 = true;
	    			$("#errorBtn2").click();
	    			$('#error-block').hide();
	    		}
	    		if($('#errorBtn2').is(":hidden")){
	    		$('#error-block').hide();
	    		}
	       	}
	    	
		});
		
// 		$("#errorBtn1").click(function(){
// 			setTimeout("wait1=false;", 500);
// 		});
// 		$("#errorBtn2").click(function(){
// 			setTimeout("wait1=false;", 500);
// 		});
		
		//判斷麵包屑房子圖片按鈕是否觸發，依登入狀況導回首頁
		$(".fa-home").click( function(e) {
			var loginid = '${sessionScope.cusidn}';
			if(loginid != null && loginid != ""){
 				window.location.href='${__ctx}/INDEX/index';
 			}
 			else{
 				window.location.href='${__ctx}/login';
 			}
		});
	});
	function topFunction() {
	    document.body.scrollTop = 0; // For Safari
	    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
	}
</script>