<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<style type="text/css">
.check-block,.radio-block{
	display: inline-block;
}

</style>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//開始查詢資料並完成畫面
	setTimeout("init()",20);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	var mail = '${sessionScope.dpmyemail}';
// 	mail='';
	if(${ConfirmFlag == true}){
		//alert("<spring:message code= "LB.Alert110" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert110' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
		$("#errorBtn1").click(function(e) {
			$('#error-block').hide();
			if( ''== mail){
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X2637' />"], 
						'<spring:message code= "LB.Confirm" />', 
						null
					);
				$("#errorBtn1").click(function(e) {
					$('#error-block').hide();
				});
			}
		});
	}else{
		if( ''== mail){
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X2637' />"], 
					'<spring:message code= "LB.Confirm" />', 
					null
				);
			$("#errorBtn1").click(function(e) {
				$('#error-block').hide();
			});
		}
	}
	
	if($("#SLSNO").val() == '' && "${EMPNO}" != '') {
		$("#SLSNO").val("${EMPNO}");
	}
	
	$("#CMSUBMIT").click(function(){
		cleanPayDay();
		var checkType = $("input[name=TYPE]:checked").val();
		if(checkType == "C" || checkType == "Y"){
			var SelectedRecordValue = $("#SelectedRecord").val();
			if(SelectedRecordValue.length != 0){
				var valueArray = SelectedRecordValue.split("|");
				
				for(var x=1;x<=valueArray.length;x++){
					$("#PAYDAY" + x).val(valueArray[x - 1]);
				}
			}
			else{
				//alert("<spring:message code= "LB.Alert121" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert121' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
				return false;
			}
		}
		else{
			var SelectedRecordOneValue = $("#SelectedRecord1").val();

			if(SelectedRecordOneValue.length != 0){
				var valueArray = SelectedRecordOneValue.split("|");
				
				for(var x=1;x<=valueArray.length;x++){
					$("#PAYDAY" + x).val(valueArray[x - 1]);
				}
			}
			else{
				//alert("<spring:message code= "LB.Alert121" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert121' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
				return false;
			}
		}

		var RISK7 = "${RISK7}";
		
		if(!CheckSelect("COMPANYCODE","<spring:message code= "LB.W1069" />","#")){
			return false;
		}
		if($("#PAYTYPE").val() == "3"){
			if(!CheckSelect("FUNDACN","<spring:message code= "LB.W0702" />","#")){
				return false;
			}
		}
		if(!CheckSelect("TRANSCODE","<spring:message code= "LB.W0025" />","#")){
			return false;
		}
		if(!CheckNumber("YIELD","<spring:message code= "LB.X1502" />",true)){
			return false;
		} 
		if(!CheckNumber("STOP","<spring:message code= "LB.X1503" />",true)){
			return false;
		}
		if(parseInt($("#STOP").val()) > 100){
			//alert("<spring:message code= "LB.Alert111" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert111' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;      	  
		}
		var invtypeValue = $("input[name=INVTYPE]:checked").val();
		
		if(invtypeValue == "1"){
			$("#MIP").val(""); 
			if(!CheckAmount("AMT3","<spring:message code= "LB.W1043" />",null,null)){
				return false;
			} 
		}
		else{
			$("#MIP").val("Y"); 
			if(!CheckAmount("AMT3","<spring:message code= "LB.W1044" />",null,null)){
				return false;
			}
		}
		
		var AMT3Int = parseInt($("#AMT3").val());
		
		if(checkType == "C" && AMT3Int > 5000000){
			//alert("<spring:message code= "LB.Alert112" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert112' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;
		}
		if(RISK7 == "4" && checkType == "C" && AMT3Int <= 3000000){
			//alert("<spring:message code= "LB.Alert113" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert113' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;
		}
		//外幣扣款
		if(checkType == "Y"){
			if($("#COUNTRYTYPE").val() == "B"){
				$("#COUNTRYTYPE").val("Y");
				$("#COUNTRYTYPE1").val("B");
			}	
			else{
				$("#COUNTRYTYPE").val("Y");
				$("#COUNTRYTYPE1").val("Y");
			}	        			        		
		}
		//台幣或信用卡扣款
		else{
			if($("#COUNTRYTYPE").val() == ""){
				$("#COUNTRYTYPE").val("N");
				$("#COUNTRYTYPE1").val("N");
			}
			else if($("#COUNTRYTYPE").val() == "B"){
				$("#COUNTRYTYPE").val("N");
				$("#COUNTRYTYPE1").val("B");
			}
			else{
				$("#COUNTRYTYPE").val("C");
				$("#COUNTRYTYPE1").val("C");
			}
		}
		
		var hasRisk = checkRisk($("#RISK").val(),$("#FDINVTYPE").val());
		//基金風險警告，投資屬性不合
 		if(hasRisk == true && RISK7.length == 0){
 			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_risk_alert?TXID=C017&ALERTTYPE=1");
 		}
 		//定期不定額約定條款
 		else if($("#MIP").val() == "Y"){
 			$("#COUNTRY").val($("#COUNTRYTYPE1").val());
 			$("#formID").attr("action","${__ctx}/FUND/REGULAR/fund_regular_confirm?TXID=C017");
 		}
		//基金風險確認書
 		else{
			$("#COUNTRY").val($("#COUNTRYTYPE1").val());
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_risk_confirm?TXID=C017");
 		}
		
 		if( $('#SLSNO').val() != '') {
			if(!CheckNumberAndLen("SLSNO", "<spring:message code= "LB.X2589" />", true, 6)) {
				return false;
			}
		}
		getSalaryAccountDataAndSubmit();
	});
	$("#resetButton").click(function(){
		$("#COMPANYCODE").val("#");
		$("#FUNDACN").val("#");
		$("#TRANSCODE").val("#");
		$("#AMT3").val("");
		$("input[name=PAYDAY]").prop("checked",false);
		$("input[name=PAYDAYC]").prop("checked",false);
		cleanPayDay();
		$("#YIELD").val("");
		$("#STOP").val("");
		CheckBoxTimes = 0;
	    $("#SelectedRecord").val("");
	    $("#SelectedRecord1").val("");
	});
});
function cleanPayDay(){
	$("#PAYDAY1").val("");
	$("#PAYDAY2").val("");
	$("#PAYDAY3").val("");
	$("#PAYDAY4").val("");
	$("#PAYDAY5").val("");
	$("#PAYDAY6").val("");
	$("#PAYDAY7").val("");
	$("#PAYDAY8").val("");
	$("#PAYDAY9").val("");
}
function init(){
	initFootable();
	changeACNTD('C');
	queryFundCompany('C');
	showDate('2')
	
}
function changeAMTDesc(){
	var invtypeValue = $("input[name=INVTYPE]:checked").val();
	
	if(invtypeValue == "1"){
		$("#AMTDESC").html("<spring:message code= "LB.W1043" />");
	}
	else{
		$("#AMTDESC").html("<spring:message code= "LB.W1044" />");
	}
	
	$("#COMPANYCODE").val("#");
}
function changeACNTD(acnType){
	if(acnType == "C"){
		$("#HTELPHONE").val($("#ACN1").val());
		$("#PAYTYPE").val("1");
	}
	else if(acnType == "Y"){
		$("#HTELPHONE").val($("#ACN2").val());
		$("#PAYTYPE").val("2");
	}
	
	$("#ACNTD").html($("#HTELPHONE").val());
}
function queryFundCompany(countryType){
	var feetype = "B";
	var URI = "${__ctx}/FUND/PURCHASE/getFundCompanyDataAjax";
	var rqData = { FEETYPE:feetype , COUNTRYTYPE:countryType };
	fstop.getServerDataEx(URI,rqData,false,queryFundCompanyFinish);
}
function queryFundCompanyFinish(data){
	if(data.result == true){
		var fundCompanyData = $.parseJSON(data.data);
		
		var COMPANYCODEHTML = "<option value='#'>---<spring:message code= "LB.Select" />---</option>";
		
		var seriesName;
		for(var x=0;x<fundCompanyData.length;x++){
			
			if(fundCompanyData[x].COUNTRYTYPE == "C" || fundCompanyData[x].COUNTRYTYPE == "B"){
				seriesName = fundCompanyData[x].COMPANYCODE+" <spring:message code= "LB.X1505" />";
			}
			else{
				seriesName = fundCompanyData[x].COMPANYCODE+" <spring:message code= "LB.X1506" />";
			}
			
			COMPANYCODEHTML += "<option value='" + fundCompanyData[x].COMPANYCODE + "'>" + seriesName + " " + fundCompanyData[x].COMPANYSNAME + "</option>";
		}
		$("#COMPANYCODE").html(COMPANYCODEHTML);
	}
	else{
		//alert("<spring:message code= "LB.Alert114" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert114' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}
function displayCardList(){
	$("#HTELPHONE").val($("#ACN1").val());
	$("#PAYTYPE").val("3");
	
	var FUNDACNHTML = "<select id='FUNDACN' name='FUNDACN' class='select-input'><option value='#'>---<spring:message code= "LB.W1649" />---</option>";
	
	<c:forEach var="dataMap" items="${N812REC}">
		FUNDACNHTML += "<option value='${dataMap.CARDNUM}'>${dataMap.hiddenCARDNUM}－${dataMap.TYPENAME}</option>";
	</c:forEach>
	
	FUNDACNHTML += "</select>";
	
	$("#ACNTD").html(FUNDACNHTML);
	
	queryFundCompany("C");
}
function getFundDataByCompany(){
	if($("#COMPANYCODE").val() != "#"){
		var invtypeValue = $("input[name=INVTYPE]:checked").val();
		var mip;
		
		//定期定額基金名單查詢
		if(invtypeValue == "1"){
			mip = "";
		}
		//定期不定額基金名單查詢
		else{
			mip = "Y";
		}
		var URI = "${__ctx}/FUND/PURCHASE/getFundDataByCompanyAjax";
		var rqData = {COMPANYCODE:$("#COMPANYCODE").val(), MIP:mip};
		fstop.getServerDataEx(URI,rqData,false,getFundDataByCompanyFinish);
	}
}
function getFundDataByCompanyFinish(data){
	if(data.result == true){
		var fundDatas = $.parseJSON(data.data);
		
		var fundDatasSize = fundDatas.length;
		var TRANSCODEHTML;
		
		if(fundDatasSize > 0){
			TRANSCODEHTML = "<option value='#'>---<spring:message code= "LB.Select" />---</option>";
			
			for(var x=0;x<fundDatas.length;x++){
				
				if(fundDatas[x].FUNDMARK == "Y" || fundDatas[x].FUNDMARK == "A"){
					TRANSCODEHTML += "<option value='" + fundDatas[x].TRANSCODE + "'>（" + fundDatas[x].TRANSCODE + "）" + fundDatas[x].FUNDLNAME + "</option>";
				}
			}
		}
		else{
			TRANSCODEHTML = "<option value='#'>---<spring:message code= "LB.X1507" />---</option>";
		}
		$("#TRANSCODE").html(TRANSCODEHTML);
	}
	else{
		//alert("<spring:message code= "LB.Alert115" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert115' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}
function getSalaryAccountDataAndSubmit() {
	var checkType = $("input[name=TYPE]:checked").val();
	if (checkType == 'C') {
		var URI = "${__ctx}/FUND/REGULAR/getSalaryAccountDataAndSubmitAjax";
		var rqData = $("#formID").serializeArray();
		fstop.getServerDataEx(URI,rqData,false,getSalaryAccountDataAndSubmitFinish);
	} else {
		$("#formID").submit();
	}
}
function getSalaryAccountDataAndSubmitFinish(bsData) {
	var amt3 = $("#AMT3").val();
	
	if (bsData.result == true && bsData.data != null) {
		var fundData = bsData.data;
		
		$("#SAL01").val(parseInt(fundData.SAL01));
		$("#SAL03").val(fundData.SAL03);
		
		if (fundData.SAL03 == "Y") {
			var str1 = "<spring:message code= 'LB.X2608_1' />" + fundData.SAL01Format + "<spring:message code= 'LB.X2608_2' />" 
						+ fundData.SAL02Format + "<spring:message code= 'LB.X2608_3' />";
			var str2 = "<spring:message code= 'LB.X2609' />";
			errorBlock2(
					null, 
					null,
					[str1, str2], 
					'<spring:message code= "LB.Confirm_1" />',
					'<spring:message code= "LB.Cancel" />'
			);
		} else {
			if (fundData.TOPMSG != "0000") {
				$("#TOPMSG").val(fundData.TOPMSG);
				$("#msgCode").val(fundData.msgCode);
				$("#message").val(fundData.msgName);
				$("#SAL01").val("");
				$("#SAL03").val("");
			}
			$("#formID").submit();
		}
	}
}
function getFundData(){
	if($("#TRANSCODE").val() != "#"){
		var URI = "${__ctx}/FUND/TRANSFER/getFundDataAjax";
		var rqData = {INTRANSCODE:$("#TRANSCODE").val()};
		fstop.getServerDataEx(URI,rqData,false,getFundDataFinish);
	}
}
function getFundDataFinish(data){
	if(data.result == true){
		var fundData = $.parseJSON(data.data);
		
		$("#RISKSHOW").html(fundData.RISK);
		
		var checkType = $("input[name=TYPE]:checked").val();
		
// 		if(checkType == "C"){
// 			$("#TRANSCRY").html("<spring:message code= "LB.NTD" />&nbsp;&nbsp;");
// 		}
// 		else{
// 			$("#TRANSCRY").html(fundData.ADCCYNAME+" &nbsp;&nbsp;");
// 		}
		//var COUNTRYTYPE = fundData.COUNTRYTYPE;
		$("#COUNTRYTYPE").val($.trim(fundData.COUNTRYTYPE));
		$("#FUNDLNAME").val(fundData.FUNDLNAME);
		$("#RISK").val(fundData.RISK);
		$("#FUS98E").val(fundData.FUS98E);
	}
	else{
		//alert("<spring:message code= "LB.Alert115" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert115' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}
function showDate(dateType){
	var typeValue = $("input[name=TYPE]:checked").val();
	
	if(dateType == "1"){
	     $("#row1").show();
	     $("#row2").hide();
	     
		$("input[name=PAYDAY]").prop("checked",false);
		
		$("#SelectedRecord").val("");
	}
	else if(dateType == "2"){
		$("#row2").show();
		$("#row1").hide();
		
		$("input[name=PAYDAYC]").prop("checked",false);

		$("#SelectedRecord1").val("");
	}
}

var CheckBoxTimes = 0;
function display(type){
	var SelectedRecordValue = $("#SelectedRecord").val();
	
    var isCheck = $("input[name=PAYDAY][value='" + type + "']").prop("checked");
	var payDayValue = $("input[name=PAYDAY][value='" + type + "']").val();
    
    if(isCheck == true){
		if(CheckBoxTimes >= 9){
			//alert("<spring:message code= "LB.Alert122" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert122' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			$("input[name=PAYDAY][value='" + type + "']").prop("checked",false);
			return;
		}
		CheckBoxTimes = CheckBoxTimes + 1;
		
		if(SelectedRecordValue.length == 0){
			SelectedRecordValue = payDayValue;
		}
		else{
			if(SelectedRecordValue.indexOf(payDayValue) == -1){
				SelectedRecordValue += "|" + payDayValue;
			}
		}
	}
	else{
		CheckBoxTimes = CheckBoxTimes - 1;
    	
		SelectedRecordValue = ("|" + SelectedRecordValue + "|").replace("|" + payDayValue + "|","|");
		if(SelectedRecordValue == "|"){
			SelectedRecordValue = "";
		}
		else{
			SelectedRecordValue = SelectedRecordValue.substr(1,SelectedRecordValue.length - 2);
		}
	}
	$("#SelectedRecord").val(SelectedRecordValue);
}
function display1(type){
	var SelectedRecordOneValue = $("#SelectedRecord1").val();
	
    var isCheck = $("input[name=PAYDAYC][value='" + type + "']").prop("checked");
	var payDayCValue = $("input[name=PAYDAYC][value='" + type + "']").val();
    
    if(isCheck == true){	
		if(SelectedRecordOneValue.length == 0){
			SelectedRecordOneValue = payDayCValue;
		}
		else{
			if(SelectedRecordOneValue.indexOf(payDayCValue) == -1){
				SelectedRecordOneValue += "|" + payDayCValue;
			}
		}
	}
	else{
		SelectedRecordOneValue = ("|" + SelectedRecordOneValue + "|").replace("|" + payDayCValue + "|","|");
		if(SelectedRecordOneValue == "|"){
			SelectedRecordOneValue = "";
		}
		else{
			SelectedRecordOneValue = SelectedRecordOneValue.substr(1,SelectedRecordOneValue.length - 2);
		}
	}
	$("#SelectedRecord1").val(SelectedRecordOneValue);
}

function ReplaceAll(source,stringToFind,stringToReplace){
	var temp = source;
	var index = temp.indexOf(stringToFind);
	while(index != -1){
		temp = temp.replace(stringToFind,stringToReplace);
		index = temp.indexOf(stringToFind);
	}
	return temp;
}
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Funds" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 定期投資申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1083" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1083" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formID" method="post">
						<input type="hidden" name="ADOPID" value="C017"/>
						<input type="hidden" id="FDINVTYPE" name="FDINVTYPE" value="${FDINVTYPE}"/>
						<input type="hidden" id="ACN1" name="ACN1" value="${ACN1}"/>
						<input type="hidden" id="ACN2" name="ACN2" value="${ACN2}"/>
						<input type="hidden" id="PAYTYPE" name="PAYTYPE" value=""/>
						<input type="hidden" id="HTELPHONE" name="HTELPHONE" value="${ACN1}"/>
						<input type="hidden" id="COUNTRYTYPE" name="COUNTRYTYPE" value=""/>
						<input type="hidden" id="COUNTRYTYPE1" name="COUNTRYTYPE1" value=""/>
						<input type="hidden" id="COUNTRY" name="COUNTRY" />
						<input type="hidden" name="BILLSENDMODE" value="2"/>
						<input type="hidden" id="FUNDLNAME" name="FUNDLNAME"/>
						<input type="hidden" name="CUTTYPE" value="${CUTTYPEString}"/>
						<input type="hidden" name="BRHCOD" value="${BRHCOD}"/>
						<input type="hidden" id="RISK" name="RISK"/>
						<input type="hidden" name="SALESNO" value="${EMPNO}"/>
						<input type="hidden" id="MIP" name="MIP"/>
						<input type="hidden" id="FUS98E" name="FUS98E"/>
						<input type="hidden" name="PRO" value="${RISK7}"/>
						<input type="hidden" id="PAYDAY1" name="PAYDAY1"/>
						<input type="hidden" id="PAYDAY2" name="PAYDAY2"/>
						<input type="hidden" id="PAYDAY3" name="PAYDAY3"/>
						<input type="hidden" id="PAYDAY4" name="PAYDAY4"/>
						<input type="hidden" id="PAYDAY5" name="PAYDAY5"/>
						<input type="hidden" id="PAYDAY6" name="PAYDAY6"/>		
						<input type="hidden" id="PAYDAY7" name="PAYDAY7"/>
						<input type="hidden" id="PAYDAY8" name="PAYDAY8"/>
						<input type="hidden" id="PAYDAY9" name="PAYDAY9"/>
						<input type="hidden" id="SelectedRecord" name="SelectedRecord"/>
						<input type="hidden" id="SelectedRecord1" name="SelectedRecord1"/>
						<input type="hidden" id="FEE_TYPE" name="FEE_TYPE" value="B"/>
						<input type="hidden" id="SAL01" name="SAL01"/>
						<input type="hidden" id="SAL03" name="SAL03"/>
						<input type="hidden" id="KYCNO" name="KYCNO" value="${KYCNO}" />
						<input type="hidden" id="TOPMSG" name="TOPMSG" />
						<input type="hidden" id="msgCode" name="msgCode" />
						<input type="hidden" id="message" name="message" />
						
					<div class="main-content-block row">
								<div class="col-12 tab-content">
									<div class="ttb-input-block">
										<!-- 客戶投資屬性 -->
										<div class="ttb-input-item row">
											<span class="input-title"> <label>
													<h4><spring:message code="LB.W1067" /></h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<span class="high-light">${FDINVTYPEChinese}</span>
												</div>
											</span>
										</div>
										<!-- 投資方式 -->
										<div class="ttb-input-item row">
											<span class="input-title"> <label>
													<h4><spring:message code="LB.W0946" /></h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="1" name="INVTYPE" checked onclick="changeAMTDesc()"/><spring:message code="LB.W1086" />
													<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="2" name="INVTYPE" onclick="changeAMTDesc()"/><spring:message code="LB.W1087" />
													<span class="ttb-radio"></span>
													</label>
												</div>
											</span>
										</div>
										<!-- 扣款方式 -->
										<div class="ttb-input-item row">
											<span class="input-title"> <label>
													<h4><spring:message code="LB.D0173" /></h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="C" name="TYPE" checked onclick="changeACNTD('C');queryFundCompany('C');showDate('2')"/><spring:message code= "LB.W1089" />
													<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="Y" name="TYPE" onclick="changeACNTD('Y');queryFundCompany('Y');showDate('2')"/><spring:message code= "LB.W1090" />
													<span class="ttb-radio"></span>
													</label>
													<c:if test="${creditCardFlag == true}">
													<label class="radio-block">
														<input type="radio" value="T" name="TYPE" onclick="displayCardList();showDate('1')"/><spring:message code= "LB.Credit_Card" />
													<span class="ttb-radio"></span>
													</label>
													</c:if>
												</div>
											</span>
										</div>
										<!-- 基金公司名稱 -->
										<div class="ttb-input-item row">
											<span class="input-title"> <label>
													<h4><spring:message code="LB.W1069" /></h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<select class="custom-select select-input half-input" name="COMPANYCODE" id="COMPANYCODE" onchange="getFundDataByCompany()">
														<option value="#">---<spring:message code= "LB.Select" />---</option>
														<c:forEach var="fundCompany" items="${finalFundCompanyList}">
															<option value="${fundCompany.COMPANYCODE}">${fundCompany.seriesName}
																${fundCompany.COMPANYSNAME}</option>
														</c:forEach>
													</select>
												</div>
											</span>
										</div>
										<!-- 扣帳帳號 -->
										<div class="ttb-input-item row">
											<span class="input-title"> <label>
													<h4><spring:message code="LB.W0702" /></h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<span id="ACNTD">${ACN1}</span>
												</div>
											</span>
										</div>
										<!-- 基金名稱 -->
										<div class="ttb-input-item row">
											<span class="input-title"> <label>
													<h4><spring:message code="LB.W0025" /></h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<select class="custom-select select-input half-input" id="TRANSCODE" name="TRANSCODE" onchange="getFundData()">
														<option value="#">---<spring:message code= "LB.Select" />---</option>
													</select>
													<p><spring:message code="LB.W1073" />：<font id="RISKSHOW"></font></p>
												</div>
											</span>
										</div>
										<!-- 轉介人員(6碼) -->
										<div class="ttb-input-item row">
		                                	<span class="input-title">
			                                	<label>
			                                        <h4><spring:message code="LB.X2589" /></h4>
			                                    </label>
		                                    </span>
		                                	<span class="input-block">
		                                		<div class="ttb-input">
													<input type="text" class="text-input" size="6" maxlength="6" id="SLSNO" name="SLSNO"/>
		                                		</div>
		                               		</span>
		                            	</div>
										<!-- 每次定額申購金額  -->
										<div class="ttb-input-item row">
											<span class="input-title"> <label>
													<h4 id="AMTDESC"><spring:message code="LB.W1043" /></h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<font id="TRANSCRY"></font><input type="text" class="text-input" maxLength="9" size="15" id="AMT3" name="AMT3"/>
												</div>
											</span>
										</div>
										<!-- 每月扣款日期1 -->
										<div class="ttb-input-item row" id="row1" style="display:none">
											<span class="input-title"> <label>
													<h4> <spring:message code="LB.W1097" /></h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<label class="check-block">
														<input type="checkbox" name="PAYDAYC" value="03" onclick="display1('03')"/>03<spring:message code="LB.Day" />&nbsp;
													<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" name="PAYDAYC" value="13" onclick="display1('13')"/>13<spring:message code="LB.Day" />&nbsp;
													<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" name="PAYDAYC" value="23" onclick="display1('23')"/>23<spring:message code="LB.Day" />
											        <span class="ttb-check"></span>
													</label> 
													<br/>
													<label class="check-block">
											          	<input type="checkbox" name="PAYDAYC" value="08" onclick="display1('08')"/>08<spring:message code="LB.Day" />&nbsp;
													<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" name="PAYDAYC" value="18" onclick="display1('18')"/>18<spring:message code="LB.Day" />&nbsp;
													<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" name="PAYDAYC" value="28" onclick="display1('28')"/>28<spring:message code="LB.Day" />
													<span class="ttb-check"></span>
													</label>
												</div>
											</span>
										</div>
										<!-- 每月扣款日期2 -->
										<div class="ttb-input-item row" id="row2">
											<span class="input-title"> <label>
													<h4> <spring:message code="LB.W1097" /></h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<label class="check-block">
													<input type="checkbox" name="PAYDAY" value="01" onclick="display('01')"/>01<spring:message code="LB.Day" />&nbsp;
													<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="02" onclick="display('02')"/>02<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="03" onclick="display('03')"/>03<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="04" onclick="display('04')"/>04<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="05" onclick="display('05')"/>05<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="06" onclick="display('06')"/>06<spring:message code="LB.Day" />&nbsp;
													<span class="ttb-check"></span>
													</label>
													<label class="check-block">
													<input type="checkbox" name="PAYDAY" value="07" onclick="display('07')"/>07<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="08" onclick="display('08')"/>08<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="09" onclick="display('09')"/>09<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="10" onclick="display('10')"/>10<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="11" onclick="display('11')"/>11<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="12" onclick="display('12')"/>12<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="13" onclick="display('13')"/>13<spring:message code="LB.Day" />&nbsp;
													<span class="ttb-check"></span>
													</label>
													<label class="check-block">
													<input type="checkbox" name="PAYDAY" value="14" onclick="display('14')"/>14<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="15" onclick="display('15')"/>15<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="16" onclick="display('16')"/>16<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="17" onclick="display('17')"/>17<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="18" onclick="display('18')"/>18<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="19" onclick="display('19')"/>19<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="20" onclick="display('20')"/>20<spring:message code="LB.Day" />&nbsp;
													<span class="ttb-check"></span>
													</label>
													<label class="check-block">
													<input type="checkbox" name="PAYDAY" value="21" onclick="display('21')"/>21<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="22" onclick="display('22')"/>22<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="23" onclick="display('23')"/>23<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="24" onclick="display('24')"/>24<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="25" onclick="display('25')"/>25<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="26" onclick="display('26')"/>26<spring:message code="LB.Day" />&nbsp;
								          			<span class="ttb-check"></span>
													</label>
													<label class="check-block">
								          			<input type="checkbox" name="PAYDAY" value="27" onclick="display('27')"/>27<spring:message code="LB.Day" />&nbsp;
													<span class="ttb-check"></span>
													</label>
													<label class="check-block">
													<input type="checkbox" name="PAYDAY" value="28" onclick="display('28')"/>28<spring:message code="LB.Day" />&nbsp;
													<span class="ttb-check"></span>
													</label>
												</div>
											</span>
										</div>
										<!-- 停利通知設定 -->
										<div class="ttb-input-item row">
											<span class="input-title"> <label>
													<h4><spring:message code="LB.W1075" /></h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<input type="text" class="text-input" maxLength="3" size="4" id="YIELD" name="YIELD" />&nbsp;％
												</div>
											</span>
										</div>
										<!-- 停利通知設定 -->
										<div class="ttb-input-item row">
											<span class="input-title"> <label>
													<h4><spring:message code="LB.W1076" /></h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<input type="text" class="text-input" maxLength="3" size="4" id="STOP" name="STOP" />&nbsp;％
												</div>
											</span>
										</div>
									</div>
								<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange" /> 
							</div>
						</div>
							<ol class="description-list list-decimal">
								<p><spring:message code="LB.Description_of_page"/></p>
								<li><strong><spring:message code="LB.Fund_Regular_Select_P1_D1" /></strong></li>
								<li><strong><spring:message code="LB.Fund_Regular_Select_P1_D2" /></strong></li>
								<li><strong><spring:message code="LB.Fund_Regular_Select_P1_D3" /></strong></li>
							</ol>
						</div>
					</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>