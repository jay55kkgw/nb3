<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="page1" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-11" aria-expanded="true"
				aria-controls="popup1-11">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>Online banking application conditions? How to apply?</span>
					</div>
				</div>
			</a>
			<div id="popup1-11" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li>First, apply at cabinet
										<ol>
											<li>If you are already a deposit customer of the Bank,
												please bring your ID card, deposit seal and passbook, go to
												the bank to fill out the application form, and obtain the
												online banking password to log in to our website
												(https://ebank.tbb.com.tw) to trade. For the Network
												Receipts and Payments individual account (that is, the
												deposit passbook has a deposit seal), you can bring your ID
												card, deposit seal and passbook to all domestic branches to
												apply.</li>
										</ol>
									</li>
									<li>Second, online application
										<ol>
											<li>1、 Bank's chip financial card depositors: Only for
												the individual depositors first time apply. If the depositor
												haven't apply, you can apply online for inquiry service.
												Please apply online at website using the Bank's Chip
												Financial Card + Chip Reader. If the chip financial card
												already has a non-predesignated, then online apply for
												"financial card online application/cancel transaction
												service function", perform NTD non-contract transfer
												transaction, payment tax transaction, online change of
												communication address/phone and user name/sign-in
												password/transaction password online unlocking,etc.</li>
											<li>2、Credit card customers of the Bank (excluding
												business cards, purchasing cards, attached cards, VISA
												financial cards): You can apply online banking by credit
												card at website, enter credit card personal verification
												information, and use online banking credit card related
												service. <br>Note: Online applicants are not able to
												perform functions such as “transfer” and “subscription
												fund”. If you want to add the above functions, please
												contact the branch to apply for upgrade, then you can use
												the full finanacial service.
											</li>
										</ol>
									</li>
								</ol>
							</div>
						</li>
						<li>
							<p>Third, If you have not opened an account with the Bank,
								please bring your ID card and seal to the Bank's domestic
								branches to open an account and apply for online banking at the
								same time.</p>
						</li>
						<li>
							<p>Fourth, the service function:</p>
							<div style="overflow: auto">
								<table class="question-table">
									<thead>
										<tr>
											<th rowspan="2">Function / Application method</th>
											<th rowspan="2">Application for counter</th>
											<th colspan="3">Online Apply</th>
										</tr>
										<tr>
											<th>Chip financial card</th>
											<th>Chip financial card<br>+ Online transaction function<br>(Need to open non-contract transfer transactions)</th>
											<th>Credit card</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">NTD Service</th>
										</tr>
										<tr>
											<th style="text-align: left;">Designated/<br>Non-predesignated transfer <br>(Note 1)</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">Deposit</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">Inward remittance <br>notification setting</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">Foreign Currency Service</th>
										</tr>
										<tr>
											<th style="text-align: left;">Deposit/Loan/<br>Account inquiry</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">Fund Service</th>
										</tr>
										<tr>
											<th style="text-align: left;">Fund inquiry</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">Fund purchases and changes</th>
											<td>O</td>
											<td>X</td>
											<td>X</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">Financial trial</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">Credit Card Service</th>
										</tr>
										<tr>
											<th style="text-align: left;">Credit card account inquiry</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th style="text-align: left;">Electronic bill/<br>Replenish bill/<br>Email payment notice</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th style="text-align: left;">Log in to road rescue</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">Payment Service</th>
										</tr>
										<tr>
											<th style="text-align: left;">Payment tax</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">Apply for withholding fees</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">Personal Service</th>
										</tr>
										<tr>
											<th style="text-align: left;">Change of communication data <br>(Note 2)</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">Online unlocked <br>Username/Login/<br>Transaction password <br>(Note 3)</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">Reporting Lost Service</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p>Note</p>
							<ol class="list-decimal">
								<li>If you have applied for the transaction function online, and the chip financial card already has the non-contracted transfer function, you can use the main account of the chip financial card in your account as the transfer account to perform unconventional transfer transactions and payment tax transactions in NTD.</li>
								<li>The transaction mechanism is "electronic signature" or "chip financial card", and you can change the communication address / phone online.</li>
								<li>The transaction mechanism is "electronic signature" or "chip financial card", which can perform online sign-in / transaction password online unlock.</li>
								<li>O：indicates available functions, X：indicates unavailable functions.</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-12" aria-expanded="true"
				aria-controls="popup1-12">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>If I have an account with each of the Kaohsiung
							branch and the Taipei branch, can I choose an account opening
							branch to have both accounts apply for the transfer function
							instead of go to two branches?</span>
					</div>
				</div>
			</a>
			<div id="popup1-12" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								Yes, if you apply for online banking services at the Taipei
								branch, simply complete the agreement at the Taipei branch and
								fill in the Taipei and Kaohsiung branch account, but please
								bring the passbook of the Kaohsiung branch account, (Limited to
								the Network Receipts and Payments account, the passbook home
								page is covered with a deposit seal) and seals to reduce your
								time.<br>If you are the Network Receipts and Payments
								individual account, you can bring your ID card, deposit seal and
								passbook to all domestic branches .
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-13" aria-expanded="true"
				aria-controls="popup1-13">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>Can I apply for online banking without the transfer
							function? Can I restore it at the future?</span>
					</div>
				</div>
			</a>
			<div id="popup1-13" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Yes, the transfer function can be selected according to
								your needs. Please do not fill in the current account number in
								the “Export Account” field when you apply at the counter. If you
								want to apply for the transfer function, please go to the
								original application branch in the future; the Network Receipts
								and Payments individual account (that is, the passbook home page
								is stamped with a deposit seal), and you can also bring your ID
								card, deposit seal and passbook to all domestic branches.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-14" aria-expanded="true"
				aria-controls="popup1-14">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>What transaction mechanisms does online banking have?</span>
					</div>
				</div>
			</a>
			<div id="popup1-14" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>There are four mechanisms for online banking transaction. The four methods are electronic device such as USB, password (SSL), the ATM card and the mobile notification service.</p>
						</li>
						<li>
							<ol>
								<li>
									<p>1. Electronic device (with the certification of the financial XML format)</p>
									<p>You must go to the bank to sign an agreement, and apply for the certification of the financial XML format. You will acquire the “identification data”, and the electronic device which is I-key.<br>After completion of the applying procedures, please log into the online banking and go to the “registration center” to download the E-voucher. The I-key are $600 each, and the annual fee will be deducted automatically online. The fee would be NT150 for personal account, and NT900 for company account. The period of validity is a year.
</p>
								</li>
								<li>
									<p>2. Trading password (SSL)</p>
									<p>Those who haven't applied for E-voucher can only use SSL password for transactions (such as enquiry, the transaction of registered account, etc.).</p>
								</li>
								<li>
									<p>3. The ATM card ( With Card Reader)</p>
									<p>If the card already has the transfer function, you can use the card and the card reader to do the transaction.</p>
								</li>
								<li>
									<p>4. Mobile Notification Service</p>
									<p>To improve the convenience of the online banking, we provide the notification service. By binding the user’s mobile device to confirm the respectively lower risk transactions. After binding the mobile, the user can do the non-registered transaction, payment service, changing one’s phone number and the home address though the online banking.</p>
								</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-15" aria-expanded="true"
				aria-controls="popup1-15">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>What is the difference between I-key, trading password, the ATM card and the mobile notification service?</span>
					</div>
				</div>
			</a>
			<div id="popup1-15" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>All transactions in online banking were used in SSL 256bit encryption to ensure transmission security. The SSL password could be used in registered account transaction, while non-registered account transaction requires the more secure mechanism such as I-key, the ATM card with card reader, and the mobile notification service.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
