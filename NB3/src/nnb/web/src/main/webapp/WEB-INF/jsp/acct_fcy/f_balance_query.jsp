<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
    <%@ include file="../__import_head_tag.jsp"%>	
    <%@ include file="../__import_js.jsp" %>	
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 帳戶餘額查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Demand_Deposit_Balance" /></li>
		</ol>
	</nav>



		<!-- menu、登出窗格 --> 
 		   <div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單及主頁內容 -->
		 </div><!-- content row END --> 
        <main class="col-12">
		<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.FX_Demand_Deposit_Balance"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action=""> 
		               <div class="main-content-block row">
							<div class="col-12 tab-content">
								<ul class="ttb-result-list">
									<li>
										<!-- 查詢時間 -->
										<h3>
										<spring:message code="LB.Inquiry_time"/>
										</h3>
										<p>
										${fcy_balance_query.data.CMQTIME}
										</p>
									</li>
									<li>
										<!-- 資料總數 -->
										<h3>
										<spring:message code="LB.Total_records"/>									
										</h3>
										<p>
										<c:set var = "list_Size" value="${fcy_balance_query.data.ACNInfo}"></c:set>
										${fn:length(list_Size)} <spring:message code="LB.Rows"/>
										</p>
									</li>
								</ul>
								<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
									<thead>				
										<tr>
											<!-- 帳號欄位-->
											<th data-title='<spring:message code="LB.Account"/>'>
												<spring:message code="LB.Account" />
											</th>
											<!-- 存款種類欄位-->
 											<th data-title='<spring:message code="LB.Deposit_type"/>' data-breakpoints="xs sm">
 												<spring:message code="LB.Deposit_type" />
 											</th>
											<!-- 幣別欄位-->
											<th data-title='<spring:message code="LB.Currency"/>'>
												<spring:message code="LB.Currency" />
											</th>
<!-- 											可用餘額欄位 -->
											<th data-title='<spring:message code="LB.Available_balance"/>' data-breakpoints="xs">
												<spring:message code="LB.Available_balance" />
											</th>
<!-- 											帳戶餘額欄位 -->
											<th data-title='<spring:message code="LB.Account_balance"/>' data-breakpoints="xs">
												<spring:message code="LB.Account_balance" />
											</th>
<!-- 											執行選項欄位 尚未做 -->
											<th data-title='<spring:message code="LB.Quick_Menu"/>'>
												<spring:message code="LB.Quick_Menu" />
											</th>
										</tr>
									</thead>
									
										<c:if test="${fn:length(list_Size) eq '0'}">
												<tbody> 
							        				<tr>
							        					<td></td>
							        					<td></td>
							        					<td></td>
							        					<td></td>
							        					<td></td>
							        					<td></td>
							        				</tr>
												</tbody>
										</c:if>
										<c:if test="${fn:length(list_Size) != '0'}">
										<tbody>
										<c:forEach var="dataList" items="${fcy_balance_query.data.ACNInfo}" varStatus="data">
											<tr>
												<td class="text-center">${dataList.ACN}</td>
      					                		<td class="text-center">      					                		
	      					                		${dataList.KIND }
      					                		</td>
							                	<td class="text-center">${dataList.CUID}</td>
							                	<td class="text-right">${dataList.AVAILABLE}</td>
							                	<td class="text-right">${dataList.BALANCE}</td>
												<td class="text-center">
    											<!-- 下拉式選單-->				
    											<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')" value="..."/>
<%--     											<input type="button" class="d-sm-none d-block" id="click" value="Click" onclick="hd2('actionBar2${data.index}')"/><!-- 快速選單測試 --> --%>
												<select class="d-none d-lg-inline-block custom-select fast-select" id="actionBar" onchange="formReset(this.value,'${dataList.ACN}');"><!-- CLASS -->
													<option value="">-<spring:message code="LB.Select"/>-</option>
													<!-- 外匯活存明細 -->
													<option value="f_demand_deposit_detail"><spring:message code="LB.FX_Demand_Deposit_Detail"/></option>
    												<c:if test="${dataList.OUTACN == 'Y'}">
														<!-- 外匯轉帳 -->
														<option value="f_transfer"><spring:message code="LB.FX_Transfer"/></option>
														<!-- 外匯匯出匯款 -->
														<option value="f_outward_remittances"><spring:message code= "LB.W0286" /></option>
														<!-- 轉入台幣綜存定存 -->
														<option value="f_deposit_transfer"><spring:message code="LB.Open_Foreign_Currency_Time_Deposit"/></option>
													</c:if>
												</select>
												<!-- 快速選單測試新增div -->
 					        					<div id="actionBar2${data.index}" class="fast-div-grey" style="visibility: hidden">
													<div class="fast-div">
														<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${data.index}')">
			                              				<p><spring:message code= "LB.X1592" /></p>
														<ul>
															<a href="${__ctx}/FCY/ACCT/f_demand_deposit_detail"><li><spring:message code="LB.FX_Demand_Deposit_Detail"/><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
															<c:if test="${dataList.OUTACN == 'Y'}">
																<a href="${__ctx}/FCY/TRANSFER/f_transfer"><li><spring:message code="LB.FX_Transfer"/><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
																<a href="${__ctx}/FCY/REMITTANCES/f_outward_remittances"><li><spring:message code= "LB.W0286" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
																<a href="${__ctx}/FCY/ACCT/TDEPOSIT/f_deposit_transfer"><li><spring:message code="LB.Open_Foreign_Currency_Time_Deposit"/><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
															</c:if>
														</ul>
														<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${data.index}')" value="<spring:message code= "LB.X1572" />"/>
													</div>
												</div>
												</td>
											</tr>
										</c:forEach>
										</tbody>
										</c:if>
								</table>
								<br>
								<br>
								<br>
								<div class="main-content-block main-content-left row" style="box-shadow: inherit;">
									<div class="col-12">
<!-- 										可用餘額小計欄位 -->
										<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
<!-- 							        		可用餘額小計資料	 -->
							        		<thead>
							        		<tr>
							        			<th data-title='<spring:message code="LB.Currency"/>'><spring:message code="LB.Currency" /></th>
							        			<th data-title='<spring:message code="LB.Subtotal_of_available_balance"/>'><spring:message code="LB.Subtotal_of_available_balance"/></th>
							        			<th data-title='<spring:message code="LB.Total_records"/>'><spring:message code="LB.Total_records"/></th>
							        		</tr>
							        		</thead>
							        		
							        			<c:if test="${fn:length(list_Size) eq '0'}">
							        			<tbody> 
							        				<tr>
							        					<td></td>
							        					<td></td>
							        					<td></td>
							        				</tr>
												</tbody>
												</c:if>
												<c:if test="${fn:length(list_Size) != '0'}">
												<tbody>
								        		<c:forEach var="dataListA" items="${fcy_balance_query.data.AVASum}">
								            		<tr>
<!-- 								            			可用餘額小計資料 -->	 
								                		<td class="text-center">${dataListA.CUID}</td>
								                		<td class="text-right">${dataListA.AVAILABLE}</td>
								                		<td class="text-center">${dataListA.COUNT} <spring:message code="LB.Rows"/></td>
								            		</tr>
								        		</c:forEach>
								        		</tbody>
								        		</c:if>
						        		</table>
						        		</div>
					        	</div>
					        	<div class="main-content-block main-content-right row" style="box-shadow: inherit;">
					        		<div class="col-12">
<!-- 						        		帳戶餘額小計欄位 -->
						        		<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
						        			<thead>
							        		<tr>
							        			<th data-title='<spring:message code="LB.Currency"/>'><spring:message code="LB.Currency" /></th>
							        			<th data-title='<spring:message code="LB.Subtotal_of_account_balance"/>'><spring:message code="LB.Subtotal_of_account_balance"/></th>
							        			<th data-title='<spring:message code="LB.Total_records"/>'><spring:message code="LB.Total_records"/></th>
							        		</tr>
							        		</thead>
							        	
							        		<c:if test="${fn:length(list_Size) eq '0'}">
							        			<tbody> 
							        				<tr>
							        					<td></td>
							        					<td></td>
							        					<td></td>
							        				</tr>
												</tbody>
											</c:if>
											<c:if test="${fn:length(list_Size) != '0'}">
											<tbody> 
 <!-- 								        		帳戶餘額小計資料 -->	  
								        		<c:forEach var="dataListB" items="${fcy_balance_query.data.BALSum}"> 
							            		<tr> 
							                		<td class="text-center">${dataListB.CUID}</td> 
							                		<td class="text-right">${dataListB.BALANCE}</td> 
								                	<td class="text-center">${dataListB.COUNT} <spring:message code="LB.Rows"/></td> 
								            		</tr> 
								        		</c:forEach> 
								        	</tbody>
								        	</c:if>
						        		</table>
						        		</div>
 					        	</div>
								<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
								</div>
								<!-- 列印鈕-->	
						</div>
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
		<%@ include file="../index/footer.jsp"%>
		
		<script type="text/javascript">
			$(document).ready(function(){
				//initFootable();
				setTimeout("initDataTable()",100);
				$("#printbtn").click(function(){
					var i18n = new Object();
					i18n['jspTitle']='<spring:message code="LB.FX_Demand_Deposit_Balance"/>'
					var params = {
						"jspTemplateName":"fcy_balance_query_print",
						"jspTitle":i18n['jspTitle'],
						"CMQTIME":"${fcy_balance_query.data.CMQTIME}",
						"COUNT":"${fn:length(list_Size)}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});
			});
			
			//快速選單
		 	function formReset(value,ACN){
				var acn = $("#ACN").val();
				switch(value){
					case "f_demand_deposit_detail":
						fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/f_demand_deposit_detail?ACN=' + ACN +'&','', '');
						break;
					case "f_transfer":
						fstop.getPage('${pageContext.request.contextPath}'+'/FCY/TRANSFER/f_transfer?ACN=' + ACN +'&','', '');
						break;
					case "f_deposit_transfer":
						fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/TDEPOSIT/f_deposit_transfer?ACN=' + ACN +'&','', '');
						break;
					case "f_outward_remittances":
						fstop.getPage('${pageContext.request.contextPath}'+'/FCY/REMITTANCES/f_outward_remittances?ACN=' + ACN +'&','', '');
						break;
				}
			}
			
			//快速選單測試新增
		 	//將actionbar select 展開閉合
			$(function(){
		   		$("#click").on('click', function(){
		       		var s = $("#actionBar2").attr('size')==1?8:1
		       		$("#actionBar2").attr('size', s);
		   		});
		   		$("#actionBar2 option").on({
		       		click: function() {
		           		$("#actionBar2").attr('size', 1);
		       		},
		   		});
			});
			function hd2(T){
				var t = document.getElementById(T);
				if(t.style.visibility === 'visible'){
					t.style.visibility = 'hidden';
				}
				else{
					$("div[id^='actionBar2']").each(function() {
						var d = document.getElementById($(this).attr('id'));
						d.style.visibility = 'hidden';
				    });
					t.style.visibility = 'visible';
				}
			}
			
		</script>	
	</body>
</html>