<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="RS" value="${transfer_data_query_history.data.RS}"></c:set>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	
	<link rel="stylesheet" href="../css/reset.css">	
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" href="../css/tbb_common.css">
	
	
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
		}
		
		 function chooseOne1(){
				if($("#check1").prop('checked')){
					//移除 disabled
					$("#CMSUBMIT").addClass("btn-flat-orange")
					$('#CMSUBMIT').removeAttr('disabled');
				} else {
					$('#CMSUBMIT').attr('disabled','disabled');
			        $("#CMSUBMIT").removeClass("btn-flat-orange");
				}
				
			}    
		
		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").submit();
				}
			});
		}
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- SMART FUND自動贖回設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1184" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12">
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1184" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/FUND/ALTER/smart_fund_step1">
				<div class="main-content-block row">
	 				  <div class="col-12">
                        <div class="ttb-message">
                        	<P>【SMART FUND自動贖回特別約定事項】</P>
                            <span>為確保您的權益，請逐一詳讀下列條款或請理財業務人員協助說明後，勾選確認</span>
                        </div>
                        
                        <h4>壹、 名詞說明</h4>
                        
                        <ul class="ttb-result-list terms">
                            <li data-num="一、">
                            	<span class="input-subtitle subtitle-color">報酬率計算式</span>  
                        		<p>[（同一信託憑證號碼之投資標的投資現值）－（同一信託憑證號碼之投資標的投資本金）]／（同一信託憑證號碼之投資標的投資本金）</p>
                            </li>
                            <li data-num="二、">
                            	<span class="input-subtitle subtitle-color">基金投資現值</span>
                            	<ul class="decimal-inside">
                            		<li>新臺幣信託：單位數×本行系統內最近一次淨值×折合新臺幣匯率。</li>
                            		<li>外幣信託：單位數×本行系統內最近一次淨值。</li>
                            	</ul>
                            </li>
                            <li data-num="三、">
                            	<span class="input-subtitle subtitle-color">停利設定門檻</span>
                            	<p>5%至999%（須為整數），委託人同一信託憑證號碼之投資標的，僅能約定一停利報酬率。</p>
                            </li>
                            <li data-num="四、">
                            	<span class="input-subtitle subtitle-color">自動執行停利</span>
                            	<p>係指委託人指示本行，當「報酬率」≧「停利報酬率」，則授權本行代為執行贖回程序。</p>
                            </li>
                            <li data-num="五、">
                            	<span class="input-subtitle subtitle-color">停損設定門檻</span>
                            	<p>-5%至-100%（須為整數），委託人同一信託憑證號碼之投資標的，僅能約定一停損報酬率。</p>
                            </li>
                            <li data-num="六、">
                            	<span class="input-subtitle subtitle-color">自動執行停損</span>	
                            	<p>係指委託人指示本行，當「報酬率」≦「停損報酬率」，則授權本行代為執行贖回程序。</p>
                            </li>
                        </ul>
                        
                        <h4>貳、自動贖回機制說明 </h4>
                            
                        <ul class="ttb-result-list terms">
                            <li data-num="一、">
                            	<span class="input-subtitle subtitle-color">無法自動贖回之基金標的及型態</span>
                            	<p>「手續費後收之基金」無法辦理自動贖回。</p>
                            </li>
                            </li>
                            <li data-num="二、">
                            	<span class="input-subtitle subtitle-color">自動贖回出場點</span>
                            	<p>每一營業日下午２點至２點３０分為本行系統執行自動贖回交易期間，「投資標的之最近一次淨值報價」係以本行於執行自動贖回期間內實際執行自動贖回交易之時點為判斷基準。</p>
                            </li>
                            <li data-num="三、">
                            	<p>於執行自動贖回期間內，本行依據「投資標的之最近一次淨值報價」及「匯率」計算「報酬率」達到委託人約定之停利/停損報酬率而實際執行之自動贖回交易，即無法撤銷或變更。</p>
                            </li>
                            <li data-num="四、">
                            	<p>本行執行自動贖回交易前，若因天災或其他不可抗力因素，致基金公司或發行機構通知本行提前交易截止時間以致本行未能執行自動贖回交易者，本行當日將不執行自動贖回交易。本行將於恢復交易日時，依委託人原約定辦理。</p>
                            </li>
                            <li data-num="五、">
                            	<span class="input-subtitle subtitle-color">自動贖回約定變更時間及方式</span>
                            	<p>委託人如欲取消或變更自動贖回約定，須於每一營業日下午１點３０分前，於網路銀行完成該項變更，每一營業日下午１點３０分至３點３０分暫停自動贖回約定功能，若於每一營業日下午３點３０分後始辦理取消或變更自動贖回約定者，將於次一營業日生效。已達約定之停利/停損報酬率且經本行依約定執行自動贖回交易者，即無法撤銷或更改。</p>
							</li>
                            <li data-num="六、">
                            	<span class="input-subtitle subtitle-color">贖回價格與約定之停利/停損報酬的差異</span>	
                            	<p>委託人瞭解並同意本行依約定自動執行贖回交易者，實際之贖回價格應以基金公司或發行機構或其他交易對手通知確認者為準，非以贖回當天「投資標的之最近一次淨值報價」及「匯率」為準，故投資標的於自動贖回後之實際損益，可能高於或低於原約定之停利/停損報酬率。</p>
							</li>
                            <li data-num="七、">	
                            	<span class="input-subtitle subtitle-color">短線交易之禁止</span>
                            	<p>自動贖回之執行如有違反基金公司當時短線交易期間之規定者，短線交易期間內自動贖回機制將無法執行。</p>
							</li>
                            <li data-num="八、">	
                            	<span class="input-subtitle subtitle-color">贖回次數之限制</span>
                            	<p>同一信託憑證號碼之投資標的，於同一營業日之贖回次數，以一次為限；若委託人於同一營業日已完成贖回交易，則當日自動贖回機制將不被執行。</p>
							</li>
                            <li data-num="九、">
                            	<span class="input-subtitle subtitle-color">自動贖回約定對於轉換投資標的之效力</span>
                            	<p>同一信託憑證號碼之同一投資標的，若「全部」轉換成另一投資標的者，原投資標的之自動贖回約定，對轉換後之投資標的不生效力，需重新約定；若「部分」轉換成另一投資標的者，未轉換之部分繼續適用原投資標的之自動贖回約定，已轉換之部分將需重新約定。</p>
							</li>
                            <li data-num="十、">
                            	<p>單筆及定期(不)定額自動贖回採「全部贖回」設定，惟定期(不)定額原信託憑證號碼下之投資標的全部自動贖回後，仍得繼續扣款。</p>
							</li>
                            <li data-num="十一、">
                            	<p>委託人瞭解與本行所簽立之各項約據關於「強制贖回」、「暫停贖回」之約定於本自動贖回約定仍有適用。如有因暫停贖回或其他不可歸責於本行之原因致無法辦理贖回時，本自動贖回約定即行中斷，俟該原因消滅後，重新計算「報酬率」並續執行原自動贖回約定。 </p>
							</li>
                        </ul>
                        <span class="input-block">
                                    <div class="ttb-input">
                                        <label class="check-block">
                                        <spring:message code="LB.X1494" />
                                            <input type="checkbox" id="check1" name="check1" value="1" onClick="chooseOne1()"/>
                                            <span class="ttb-check"></span>
                                        </label>
                                    </div>
                         </span>
                         
						  <input type="button" class="ttb-button" name="CMSUBMIT" id="CMSUBMIT"  value="<spring:message code="LB.X0080" />" disabled/>
                    </div>
				</div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>

</body>

</html>