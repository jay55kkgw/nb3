<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	var TXID = "${TXID}";
	
	if (TXID == "C016") {
		$("#formID").attr("action","${__ctx}/FUND/PURCHASE/fund_purchase_confirm");
	} else if (TXID == "C031") {
		$("#formID").attr("action","${__ctx}/FUND/RESERVE/PURCHASE/fund_reserve_purchase_confirm");
	}

	$("#okButton").click(function(){
		if($("#riskConfirmCheckBox").prop("checked") == false) {
			errorBlock(
				null, 
				null,
				['<spring:message code="LB.Alert092" />'], 
				'<spring:message code="LB.Quit" />', 
				null
			);
		} else {
			$("#formID").submit();
		}
	});
	$("#cancelButton").click(function(){
		$("#formID").attr("action","${__ctx}/FUND/PURCHASE/fund_purchase_select");
		$("#formID").submit();
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.Funds" /></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.W1064" /></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page">高齡聲明書</li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2>高齡聲明書</h2>
					<form id="formID" method="post">
						<input type="hidden" name="TXID" value="${TXID}"/>
						<input type="hidden" name="TRANSCODE" value="${TRANSCODE}"/>
						<input type="hidden" name="COUNTRYTYPE" value="${COUNTRYTYPE}"/>
						<input type="hidden" name="TRADEDATE" value="${TRADEDATE}"/>
						<input type="hidden" name="AMT3" value="${AMT3}"/>
						<input type="hidden" name="AMT5" value="${AMT5}"/>
						<input type="hidden" name="FCA2" value="${FCA2}"/>
						<input type="hidden" name="OUTACN" value="${OUTACN}"/>
						<input type="hidden" name="INTSACN" value="${INTSACN}"/>
						<input type="hidden" name="HTELPHONE" value="${HTELPHONE}"/>
						<input type="hidden" name="BILLSENDMODE" value="${BILLSENDMODE}"/>
						<input type="hidden" name="FCAFEE" value="${FCAFEE}"/>
						<input type="hidden" name="SSLTXNO" value="${SSLTXNO}"/>
						<input type="hidden" name="BRHCOD" value="${BRHCOD}"/>
						<input type="hidden" name="CUTTYPE" value="${CUTTYPE}"/>
						<input type="hidden" name="DBDATE" value="${DBDATE}"/>
						<input type="hidden" name="SALESNO" value="${SALESNO}"/>
						<input type="hidden" name="STOP" value="${STOP}"/>
						<input type="hidden" name="YIELD" value="${YIELD}"/>
						<input type="hidden" name="MIP" value="${MIP}"/>
						<input type="hidden" name="PRO" value="${PRO}"/>
						<input type="hidden" name="RSKATT" value="${RSKATT}"/>
						<input type="hidden" name="RRSK" value="${RRSK}"/>
					    <input type="hidden" name="XFLAG" value="${XFLAG}"/>
						<input type="hidden" name="NUM" id="NUM" value=""/>
						<input type="hidden" name="SLSNO" value="${SLSNO}"/>
						<input type="hidden" name="KYCNO" value="${KYCNO}"/>
						<input type="hidden" name="FUNDT" value="${FUNDT}"/>
						<input type="hidden" name="OFLAG" value="${OFLAG}"/>
						<input type="hidden" name="FUNDLNAME" value="${FUNDLNAME}"/>
						<input type="hidden" name="FDINVTYPE" value="${FDINVTYPE}"/>
						<input type="hidden" name="TYPE" value="${TYPE}"/>
						<input type="hidden" name="RISK" value="${RISK}"/>
						<input type="hidden" name="RISK7" value="${RISK7}"/>
						<input type="hidden" name="GETLTD" value="${GETLTD}"/>
						<input type="hidden" name="GETLTD7" value="${GETLTD7}"/>
						<input type="hidden" name="CUSNAME" value="${CUSNAME}"/>
						<input type="hidden" name="FDAGREEFLAG" value="${FDAGREEFLAG}"/>
						<input type="hidden" name="FDNOTICETYPE" value="${FDNOTICETYPE}"/>
						<input type="hidden" name="FDPUBLICTYPE" value="${FDPUBLICTYPE}"/>
						<input type="hidden" name="FEE_TYPE" id="FEE_TYPE" value="${FEE_TYPE}"/>
						<input type="hidden" name="CRY1" value="${CRY1}"/>
					    <input type="hidden" name="SHWD" value="${SHWD}"/>
						<input type="hidden" name="PEMAIL" value="${PEMAIL}"/>
						<input type="hidden" name="REPID" value="${REPID}"/>
						<input type="hidden" name="FUNCUR" value="${FUNCUR}"/>
						<input type="hidden" name="PAYDAY1" value="${PAYDAY1}"/>
						<input type="hidden" name="PAYDAY2" value="${PAYDAY2}"/>
						<input type="hidden" name="PAYDAY3" value="${PAYDAY3}"/>
						<input type="hidden" name="PAYDAY4" value="${PAYDAY4}"/>
						<input type="hidden" name="PAYDAY5" value="${PAYDAY5}"/>
						<input type="hidden" name="PAYDAY6" value="${PAYDAY6}"/>
						<input type="hidden" name="PAYDAY7" value="${PAYDAY7}"/>
						<input type="hidden" name="PAYDAY8" value="${PAYDAY8}"/>
						<input type="hidden" name="PAYDAY9" value="${PAYDAY9}"/>
						
						<div class="main-content-block row">
							<div class="col-12">
								<div class="ttb-message">
									<p>聲 明 書(高齡客戶適用)</p>
								</div>
								<ul class="ttb-result-list" style="list-style:none;">
									<li class="full-list">
										<strong style="font-size:15px">
											本人欲透過貴行購買本商品已完全瞭解本金融商品之相關風險，且知悉並同意貴行基於保護投資人之立場針對投資人年齡加上投資產品約定到期最長年限若大於或等於70時，得予以婉拒之做法。但因本人已充分考量自身年齡與本產品天期之關係，並充分了解本產品相關投資風險，特別是流動性風險，即當產品不具備充分之市場流通性時，對提前贖回指示單無法保證成交，且實際交易價格可能會與產品本身之單位資產價值產生顯著之價差，造成投資人於到期前贖回，會發生可能損及原始投資本金之風險。本人特此聲明確實瞭解，並同意承擔承作上開產品所生之一切風險，特請貴行予以受理承作。倘日後該產品發生任何風險或本人有任何損失，將完全由本人自行承擔，與貴行無涉，絕無異議。
										</strong>
										<br/>
	                                </li>
                                </ul>
								<strong>(本人年齡 + 本產品約定到期最長年限≧70)</strong>
								<br><br>
								<label class="check-block">
								<input type="checkbox" id="riskConfirmCheckBox"/>
									<strong style="font-size:15px">本人已詳閱並瞭解本聲明書之內容</strong>
								<span class="ttb-check"></span>
								</label>
								<br>
								 
	                			<input type="button" id="cancelButton" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>	
	                			<input type="button" id="okButton" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
							</div>
					</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>