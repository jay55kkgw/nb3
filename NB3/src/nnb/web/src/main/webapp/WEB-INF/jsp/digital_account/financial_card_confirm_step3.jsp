<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
</head>

<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 數位存款帳戶晶片金融卡確認領用申請及變更密碼     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1535" /></li>
		</ol>
	</nav>

	
	<!-- menu、登出窗格 -->
	<div class="content row">
        <main class="col-12">
            <section id="main-content" class="container">
                <h2><spring:message code="LB.D1535"/></h2>
                <div id="step-bar">
                    <ul>
                       <li class="finished">身分驗證</li>
                        <li class="active">變更密碼</li>
                        <li class="">完成申請</li>
                    </ul>
                </div>
                <form id="formId" method="post" action="${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm_step4">
					<input type="hidden" id="ACN" name="ACN" value="${financial_card_confirm_step3.data.ACN}"> 
					<input type="hidden" id="FGTXWAY" name="FGTXWAY" value="${financial_card_confirm_step3.data.FGTXWAY}"> 
					<input type="hidden" id="CUSIDN" name="CUSIDN" value="${financial_card_confirm_step3.data.CUSIDN}"> 
					<input type="hidden" id="UID" name="UID" value="${financial_card_confirm_step3.data.UID}"> 
					<input type="hidden" id="TYPE" name="TYPE" value="02">
					<input type="hidden" id="CARDNAME" name="CARDNAME" value="">
					<input type="hidden" id="BANKID" name="BANKID" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					
					<div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
	                            <div class="ttb-message">
	                                <p>變更密碼</p>
	                            </div>
	                            <p class="form-description">請您變更本次申請之「數位存款帳戶」晶片金融卡密碼，並請插入該晶片金融卡執行變更密碼</p>
	
	                            <!-- 舊密碼 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4>舊密碼</h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <input type="PASSWORD" name="OLDPIN" id="OLDPIN" value="" placeholder="請輸入舊密碼"
	                                        	class="text-input validate[required]" maxlength="12" size="14"/>
	                                    </div>
	                                </span>
	                            </div>
	                            
								<div class="classification-block"></div>
								
								<!-- 新密碼 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4>新密碼</h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <input type="PASSWORD" name="NEWPIN" id="NEWPIN" value="" maxlength="12" size="14" placeholder="請輸入新密碼"
												class="text-input validate[required, funcCall[validate_CheckNumber['<spring:message code= "LB.X1324" />', NEWPIN, false]]]" />
										</div>
	                                </span>
	                            </div>
	                            
								<!-- 確認新密碼 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4>確認新密碼</h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <input type="PASSWORD" name="CONFIRMNEWPIN" id="CONFIRMNEWPIN" placeholder="請再次輸入新密碼"
	                                        	class="text-input validate[required]" value="" maxlength="12" size="14"/>
										</div>
	                                </span>
	                            </div>
	                        </div>
	                       
	                        <!-- 確認變更晶片金融卡密碼 -->
							<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="確認" onclick="processQuery()" />
	
	                    </div>
                	</div>
                </form>
                
                <ol class="list-decimal description-list">
                </ol>

            </section>
        </main>
	</div>
	
	<%@ include file="../index/footer.jsp"%>
	
</body>

<script type="text/javascript">

	var readerName = "";

	$(document).ready(function() {
		init();
	});
	
    function init(){
    	// 表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
    }
    
	// 確認變更晶片金融卡密碼
	function processQuery() {
		// 去掉舊的錯誤提示訊息
		$(".formError").remove();
		
		
		var main = document.getElementById("formId");
		var oldpin = main.OLDPIN.value;	
		var newpin = main.NEWPIN.value;
		var confirmnewpin = main.CONFIRMNEWPIN.value;
		
		if(oldpin.length<6 || oldpin.length>12) {
			//alert("<spring:message code="LB.Alert055"/>");
			errorBlock(
							null, 
							null,
							["<spring:message code="LB.Alert055"/>"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return false;
		}
		if(newpin.length<6 || newpin.length>12) {
			//alert("<spring:message code="LB.Alert056"/>");
			errorBlock(
							null, 
							null,
							["<spring:message code="LB.Alert056"/>"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return false;
		}
		if(confirmnewpin.length<6 || confirmnewpin.length>12) {
			//alert("<spring:message code="LB.Alert057"/>");
			errorBlock(
							null, 
							null,
							["<spring:message code="LB.Alert057"/>"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return false;
		}
		if(newpin!=confirmnewpin) {
			//alert("<spring:message code="LB.Alert058"/>")
			errorBlock(
							null, 
							null,
							["<spring:message code="LB.Alert058"/>"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return false;
		}
		
		// 表單驗證
		if ( !$('#formId').validationEngine('validate') ) {
			e = e || window.event; // for IE
			e.preventDefault();
			
		} else {
			listReaders();
		}
	
		
		return false;	 				
	}

	//初始化交易
	function initialize(){
		if(window.console){console.log("initialize...");}
		Initialize("initializeFinish");
	}
	
	//取得初始化交易結束
	function initializeFinish(result){
		if(window.console){console.log("initializeFinish.result: " + result);}
		// 成功
		if(result == "E000"){
			readerName = $("#CARDNAME").val();
			verifyPin();
		} // 失敗
		else {
			//alert("<spring:message code="LB.Alert059"/>");
			errorBlock(
							null, 
							null,
							["<spring:message code="LB.Alert059"/>"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return false;
		}
	}
	
// ------------------------------------------------------------------------------------------

	//取得讀卡機
	function listReaders(){
		ConnectCard("listReadersFinish");
	}
	//取得讀卡機結束
	function listReadersFinish(result){
		//成功
		if(result != "false" && result != "E_Send_11_OnError_1006"){
			//找出有插卡的讀卡機
			findOKReader();
		}
	}
	//找出有插卡的讀卡機
	function findOKReader(){
		FindOKReader("findOKReaderFinish");
	}
	var OKReaderName = "";
	//找出有插卡的讀卡機結束(有找到才會到此FUNCTION)
	function findOKReaderFinish(okReaderName){
		//ASSIGN到全域變數
// 		OKReaderName = okReaderName;
// 		readerName = $("#CARDNAME").val();
		$("#CARDNAME").val(okReaderName);
		readerName = okReaderName;
		verifyPin();
	}
	
	//驗證晶片金融卡密碼
	function verifyPin(){
		if(window.console){console.log("verifyPin...");}
		var pinCode = $("#OLDPIN").val();
		VerifyPin(readerName, pinCode, "verifyPinFinish");
	}
	
	//取得驗證晶片金融卡密碼結束
	function verifyPinFinish(result){
		if(window.console){console.log("verifyPinFinish...");}
		//成功
		if(result == "true"){
			getUnitCode();
		} else {
			//alert("<spring:message code="LB.Alert060"/>");
			
			// 驗證錯誤API會跳出錯誤訊息，這邊會蓋掉原有的錯誤訊息，故註解
// 			errorBlock(
// 							null, 
// 							null,
// 							["<spring:message code="LB.Alert060"/>"], 
// 							'<spring:message code= "LB.Quit" />', 
// 							null
// 						);
			return false;		
		}
	}
	
	//取得卡片發卡行資訊
	function getUnitCode(){
		if(window.console){console.log("getUnitCode...");}
		GetUnitCode(readerName, "getUnitCodeFinish");
	}
	
	//取得卡片發卡行資訊結束
	function getUnitCodeFinish(result){
		if(window.console){console.log("getUnitCodeFinish.result: " + result);}

		// 成功
		if(result != "false"){
			$("#BANKID").val(result);
			if(result != "05000000") {
				//alert("<spring:message code="LB.Alert061"/>");
				errorBlock(
							null, 
							null,
							["<spring:message code="LB.Alert061"/>"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				return false;
			} else {
				getMainAccount();
			}						
		} // 失敗
		 else {		
			//alert("<spring:message code="LB.Alert062"/>");
			errorBlock(
							null, 
							null,
							["<spring:message code="LB.Alert062"/>"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return false;
		}
	}
	
	//取得卡片帳號資訊
	function getMainAccount(){
		if(window.console){console.log("getMainAccount...");}
		GetMainAccount(readerName, "getMainAccountFinish");
	}
	
	//取得卡片帳號資訊結束
	function getMainAccountFinish(result){
		if(window.console){console.log("getMainAccountFinish..." + result);}
		//成功
		if(result != "false"){
			$('#ACNNO').val(result);
			var acnno = result;
			if(acnno.length > 11) acnno = acnno.substring(5, 16);
			var acn = "${financial_card_confirm_step3.data.ACN}";

			console.log("getMainAccountFinish.acnno: " + acnno);
			console.log("getMainAccountFinish.acn: " + acn);

			if(acn != acnno) {
				//alert("<spring:message code="LB.Alert063"/>");
				errorBlock(
							null, 
							null,
							["<spring:message code="LB.Alert063"/>"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				return false;
			} else {
				// 通過檢核準備變更密碼
				changePin();
			}			
		} else {
			//alert("<spring:message code="LB.Alert064"/>");
			errorBlock(
					null, 
					null,
					["<spring:message code="LB.Alert064"/>"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			return false;	
		}
	}
	
	//變更晶片金融卡密碼
	function changePin(){
		if(window.console){console.log("changePin...");}
		var oldPin = $("#OLDPIN").val();
		var newPin = $("#NEWPIN").val();
		ChangePin(readerName, oldPin, newPin, "changePinFinish");
	}
	
	//變更晶片金融卡密碼結束
	function changePinFinish(result){
		if(window.console){console.log("changePinFinish.result: " + result);}
		//成功
		if(result){	
			//alert("<spring:message code= "LB.D1551" />"); // 變更晶片金融卡密碼成功，可繼續交易並啟用卡片
			errorBlock(
							null, 
							null,
							["<spring:message code= "LB.D1551" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			Finalize();
			$("CMSUBMIT").prop('disabled', true);
<%-- 				main.action = '<%= RootPath %>/simpleform?trancode=NB32_3&JSPNAME=NB32_3.jsp';	     --%>
			$("#formId").submit();
		} else {
			//alert("<spring:message code="LB.Alert065"/>");
			errorBlock(
					null, 
					null,
					["<spring:message code="LB.Alert065"/>"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			Finalize();	
			return false;		
		}
	}

</script>
	
</html>