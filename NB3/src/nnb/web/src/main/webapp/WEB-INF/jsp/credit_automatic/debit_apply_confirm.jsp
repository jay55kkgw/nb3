<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			goOn();
		});
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
				console.log("submit~~");
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					// 通過表單驗證
					processQuery();
				}
			})
		}
		
		// 通過表單驗證準備送出
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			switch(fgtxway) {
				case '0':
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					initBlockUI();
					$("form").submit();
					break;
				case '1':
					// IKEY
					useIKey();
					break;
				case '7'://IDGATE認證
					showIdgateBlock();
					break;
				default:
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
		function validate(){
			var val = $('input:radio[name="FGTXWAY"]:checked').val();
			if(val=='0'){
				$('#CMPASSWORD').addClass("validate[required]");
			}else if (val == '1'){
				$('#CMPASSWORD').removeClass("validate[required]");
			}else if (val == '7'){
				$('#CMPASSWORD').removeClass("validate[required]");
			}
		}
	</script>
</head>

<body>
<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	 <!--   IDGATE -->
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>	
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 自動扣款申請/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0166" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code="LB.D0166" /><!-- 信用卡款自動扣繳申請/取消 -->
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" action="${__ctx}/CREDIT/AUTOMATIC/debit_apply_result" method="post">
  					 <input type="hidden" name="TXTOKEN" value="${transfer_data.data.TXTOKEN}"> 
  					 <input type="hidden" name="ADOPID" value="NI04">
                     <input type="hidden" name="display_CCACNO" value="">
                     <input type="hidden" name="display_PAYFLAG" value="${transfer_data.data.PAYFLAG}">
                     <input type="hidden" name="APPLYFLAG" value="${transfer_data.data.APPLYFLAG}">
                     <input type="hidden" name="TSFACN" value="${transfer_data.data.TSFACN}">
                     <!-- 交易機制所需欄位 -->
                     <input type="hidden" id="PINNEW" name="PINNEW" value="" >
					 <input type="hidden" id="jsondc" name="jsondc" value="${transfer_data.data.jsondc}">
					 <input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
                     
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<!--交易時間區塊 -->
								<div class="ttb-input-item row">
									<!--交易時間  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Trading_time" />
										</label>
									</span>
									<span class="input-block">
										${transfer_data.data.CMQTIME}
									</span>
								</div>
								<c:if test="${transfer_data.data.APPLYFLAG != '2'}">
								<!-- 扣款帳號區塊 -->
								<div class="ttb-input-item row">
									<!--扣款帳號  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.D0168" /><!-- 扣款帳號 -->
										</label>
									</span>
									<span class="input-block">
										${transfer_data.data.TSFACN}
									</span>
								</div>
								</c:if>
								<!-- 申請項目區塊 -->
								<div class="ttb-input-item row">
									<!--申請項目  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.D0169" /><!-- 申請項目 -->
										</label>
									</span>
									<span class="input-block">
										
									<c:if test="${transfer_data.data.APPLYFLAG == '1'}">
										<spring:message code="LB.Apply" /><!-- 申請 -->
									</c:if>
									<c:if test="${transfer_data.data.APPLYFLAG == '2'}">
										<spring:message code="LB.Cancel" /><!-- 取消 -->
									</c:if>
									<c:if test="${transfer_data.data.APPLYFLAG == '3'}">
										<spring:message code="LB.D0172" /><!-- 變更扣繳方式 -->
									</c:if>
									</span>
								</div>
								<c:if test="${transfer_data.data.APPLYFLAG != '2'}">
								<!-- 扣繳方式區塊 -->
								<div class="ttb-input-item row">
									<!--扣繳方式  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Payment_method" /><!-- 扣繳方式 -->
										</label>
									</span>
									<span class="input-block">
										<c:if test="${transfer_data.data.display_PAYFLAG eq '1' }">
											<spring:message code="LB.D0174" /><!-- 應繳總額 -->
										</c:if>
										<c:if test="${transfer_data.data.display_PAYFLAG eq '2' }">
											<spring:message code="LB.D0175" /><!-- 最低應繳額 -->
										</c:if>
									</span>
								</div>
								</c:if>
								<!-- 交易機制區塊 -->
								<div class="ttb-input-item row">
									<!--交易機制 -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Transaction_security_mechanism" /><!-- 交易機制-->
										</label>
									</span>
									<span class="input-block">
										<!-- 請輸入交易密碼 -->
										<div class="ttb-input">
												<label class="radio-block" onclick="validate()"><spring:message code="LB.SSL_password" />
												<input type="radio" name="FGTXWAY" value="0" id="CMSSL" checked > 
												<span class="ttb-radio"></span></label>
										</div>
										<div class="ttb-input"> 		
											<input type="password" id="CMPASSWORD" name="CMPASSWORD" type="password"
													maxlength="8" value="" class="text-input validate[required]" placeholder="<spring:message code="LB.Please_enter_password" />">
										</div>
										
										<!-- 使用者是否可以使用IKEY -->
										<c:if test = "${sessionScope.isikeyuser}">
											<div class="ttb-input">
													<label class="radio-block" onclick="validate()"> <spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" value="1" id="CMIKEY"> <span class="ttb-radio"></span></label> <!-- 電子簽章(請載入載具i-key) --> 
											</div>
										</c:if>
										<div class="ttb-input" name="idgate_group" style="display:none">
	                                        <label class="radio-block" onclick="validate()" >裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)<input type="radio" id="IDGATE"
	                                            name="FGTXWAY" value="7"> <span class="ttb-radio"></span></label>
                                    	</div>
									</span>
								</div>
							</div>
							<!-- 網頁顯示 button-->
								
								<!-- 重新輸入 -->
						    <input type="reset" name="CMRESET" id="CMRESET" value="<spring:message code="LB.Re_enter" />" class="ttb-button btn-flat-gray"/>
						    <!--網頁顯示 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange" >
							
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
      
</body>

</html>