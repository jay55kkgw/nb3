<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="bs" value="${financial_card_apply_result.data}"></c:set>

<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 轉帳功能申請/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0327" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 快速選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.X0327"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						
						<div class="ttb-message">
							<span>${bs.typeName}<spring:message code="LB.D1099" /></span>
						</div>
						<div class="ttb-input-block">		
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4><spring:message code="LB.D0973" /></h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${bs.typeName}</span>									
									</div>
								</span>
							</div>
						</div>
					</div>	
				</div>
				
				<!-- 說明  -->
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
		           	<li><span><spring:message code="LB.Financial_Card_Apply_P3_D1" /></span></li>
				</ol>
			</form>
		</section>
		</main>
		<!-- main-content END -->
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>
<script type="text/javascript">
	window.onload = setTimeout("logOut()", 3000);
	
	function logOut()
	{
		//  i18n
		//alert("<spring:message code="LB.Alert163" />");
		errorBlock(
			null, 
			null,
			["<spring:message code= 'LB.Alert163' />"], 
			'<spring:message code= "LB.Confirm" />', 
			null
		);
		$("#errorBtn1").click(function() {
			initBlockUI();
			fstop.logout( "${__ctx}" + "/logout_aj" , "${__ctx}" + "/login");
		});
	}
</script>