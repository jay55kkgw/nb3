<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>

 <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<!--交易機制所需JS-->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	$('#CMSSL').prop("checked",true);
	
	$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
	
	$("#CMSUBMIT").click(function(e){
		
		console.log("submit~~");
		
		var FGTXWAY = $("input[name=FGTXWAY]:checked").val();
		
		// 表單驗證
		if ( !$('#formId').validationEngine('validate') ) {
			$('#CMPWD').removeClass("validate[required,custom[onlyLetterNumber]]");
			e.preventDefault();
			
		} else {
			//交易密碼
			if(FGTXWAY == "0"){
				$('#CMPWD').addClass("validate[required,custom[onlyLetterNumber]]");
				var PINNEW = pin_encrypt($("#CMPWD").val());
				$("#PINNEW").val(PINNEW);
				initBlockUI();
				$("#formId").submit();
			}
			//IKEY
			else if(FGTXWAY == "1"){
				$('#CMPWD').removeClass("validate[required,custom[onlyLetterNumber]]");
				var jsondc = $("#jsondc").val();
				//IKEY驗證流程
				uiSignForPKCS7(jsondc);
			}
			else if(FGTXWAY == "7"){
				$('#CMPWD').removeClass("validate[required,custom[onlyLetterNumber]]");
				//IDGATE認證		 
		        idgatesubmit= $("#formId");		 
		        showIdgateBlock();		 
			}
		}
	});
	$("#resetButton").click(function(){
		$("#CMPWD").val("");
	});
	$("#backButton").click(function(){
		location.href = "${__ctx}/GOLD/TRANSACTION/gold_buy";
	});
	$("#CMIKEY").click(function(){
		$('#CMPWD').removeClass("validate[required,custom[onlyLetterNumber]]");
	});
	$("#CMSSL").click(function(){
		$('#CMPWD').addClass("validate[required,custom[onlyLetterNumber]]");
	});
	$("#IDGATE").click(function(){
		$('#CMPWD').removeClass("validate[required,custom[onlyLetterNumber]]");
	});
	
});
	
</script>
</head>
<body>
	
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
    <!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	<!--header-->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 黃金申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1493" /></li>
		</ol>
	</nav>

	<!--左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> 
			<!--主頁內容-->
			<section id="main-content" class="container">
			<!-- 預約黃金買進 -->
				<h2><spring:message code= "LB.X0949" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="${__ctx}/GOLD/TRANSACTION/gold_booking_result">
                	<input type="hidden" name="ADOPID" value="N09101"/>
               		<input type="hidden" name="TXTOKEN" value="${transfer_data.data.TXTOKEN}"><!-- 防止不正常交易 -->
               		<input type="hidden" id="PINNEW" name="PINNEW" value="">
               		<input type="hidden" id="DPMYEMAIL" name="DPMYEMAIL" value="">
					<!-- 交易機制所需欄位 -->
					<input type="hidden" id="jsondc" name="jsondc" value='${transfer_data.data.jsondc}'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
	                        	<div class="ttb-message">
<!-- 請確認預約資料 -->
	                        		<span><spring:message code= "LB.W0251" /></span>
	                        	</div>
								<!--臺幣轉出帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
<!-- 臺幣轉出帳號 -->
	                                        <h4><spring:message code= "LB.W1496" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${transfer_data.data.SVACN}</span>
										</div>
									</span>
								</div>
								<!--黃金轉入帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
		                                <label>
<!-- 黃金轉入帳號 -->
											<h4><spring:message code= "LB.W1497" /></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${transfer_data.data.ACN}</span>
										</div>
									</span>
								</div>
								<!--買進公克數-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 買進公克數 -->
	                                        <h4><spring:message code= "LB.W1498" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${transfer_data.data.TRNGD}</span>
<!-- 公克 -->
											<span class="ttb-unit"><spring:message code= "LB.W1435" /></span>
										</div>
									</span>
								</div>
								<!--交易機制-->
	                            <div class="ttb-input-item row">
									<span class="input-title">
										<label>
	                                        <h4><spring:message code="LB.Transaction_security_mechanism"/></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block" >
	                                    <div class="ttb-input">
											<label class="radio-block"><spring:message code="LB.SSL_password"/> 
	                                            <input type="radio" name="FGTXWAY" id="CMSSL" value="0" />
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                    </div>
	                                    <div class="ttb-input">
	                                    	<label><input type="password" class="text-input" name="CMPWD" id="CMPWD" maxlength="8" placeholder="<spring:message code="LB.Please_enter_password" /> "/></label>
										</div>
										<!--使用者是否可以使用IKEY-->
										<c:if test = "${sessionScope.isikeyuser}">
	                                    	<div class="ttb-input">
	                                       		<label class="radio-block"><spring:message code="LB.Electronic_signature"/>
	                                            	<input type="radio" name="FGTXWAY" id="CMIKEY" value="1"/>
	                                            	<span class="ttb-radio"></span>
	                                        	</label>
	                                    	</div>
	                                    </c:if>
										<div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
	                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
		                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
		                                       <span class="ttb-radio"></span>
	                                       </label>		 
	                                   </div>
	                                </span>
	                            </div>
	                        </div>
	                        
<!-- 回上頁 -->
	                  			<input type="button" id="backButton" value="<spring:message code= "LB.Back_to_previous_page" />" class="ttb-button btn-flat-gray"/>
<!-- 確定 -->
	                			<input type="button" id="CMSUBMIT" value="<spring:message code= "LB.Confirm" />" class="ttb-button btn-flat-orange"/>
	                		
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>