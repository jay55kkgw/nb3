<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:if test="${ not empty BaseResult}">
	<c:set var="bs" value="${BaseResult.data }"></c:set>
	<c:set var="count1" value="${bs.ACN_SSV1.size() }"></c:set>
	<c:set var="count2" value="${bs.ACN_FUD1.size() }"></c:set>
</c:if>

<c:if test="${ not empty sessionScope.n361_4}">
	<c:set var="ACN_SSV" value="${sessionScope.n361_4.ACN_SSV }"></c:set>
	<c:set var="ACN_FUD" value="${sessionScope.n361_4.ACN_FUD }"></c:set>
	<c:set var="AGREETW" value="${sessionScope.n361_4.AGREETW }"></c:set>
	<c:set var="AGREEFX" value="${sessionScope.n361_4.AGREEFX }"></c:set>
	<c:set var="BILLMTH" value="${sessionScope.n361_4.BILLMTH }"></c:set>
	<c:set var="CTDNUM"  value="${sessionScope.n361_4.CTDNUM }"></c:set>
</c:if>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			// 初始化後隱藏span( 表單驗證提示訊息用的 )
			$(".hideblock").hide();
			
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			
			// 確認鍵 click
			goOn();
			
			// 初始化 checkbox
			initCheckbox();
			
			// 回上一頁填入資料
			refillData();
			
			// 檢查同意書
			checkAgree();
			// 檢查指定帳戶
			checkRadio();
			
			// checkbox事件
			$("input[type=checkbox]").on("click", function() {
				checkAgree();
			});
			// radio點擊事件
			$("input[type=radio]").on("click", function() {
				checkRadio();
			});
		}
		
		// 檢查同意書
		function checkAgree(){
			if( $("#ACNMTH").prop("checked") && $("#BILLMTH").prop("checked") ){
				// 可點擊
				$("#CMSUBMIT").attr("class", "ttb-button btn-flat-orange");
				$("#CMSUBMIT").attr("disabled", false);
				
			} else {
				// 不可點擊
				$("#CMSUBMIT").attr("class", "ttb-button btn-flat-gray");
				$("#CMSUBMIT").attr("disabled", true);
			}
		}
		
		// 檢查指定帳戶
		function checkRadio(){
			// 指定臺幣帳戶
			if( $("#AGREETWY").is(':checked') ){
				// 可指定帳戶
				$("#ACN_SSV1").attr("disabled", false);
				$("#ACN_SSV1").addClass("validate[required]");
			} else {
				// 不可指定帳戶
				$("#ACN_SSV1").attr("disabled", true);
				$("#ACN_SSV1").removeClass("validate[required]");
			}
			
			// 指定外幣帳戶
			if( $("#AGREEFXY").is(':checked') ){
				// 可指定帳戶
				$("#ACN_FUD1").attr("disabled", false);
				$("#ACN_FUD1").addClass("validate[required]");
				
			} else {
				// 不可指定帳戶
				$("#ACN_FUD1").attr("disabled", true);
				$("#ACN_FUD1").removeClass("validate[required]");
			}
		}
		
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				console.log("submit~~");
				
				// 顯示驗證隱藏欄位( 為了讓提示訊息對齊 )
				$(".hideblock").show();
				
				// 舊網銀表單驗證 TODO 改成jQuery validate
				if(!validate()) {
					// 隱藏錯誤提示訊息span
					$(".hideblock").hide();
					return false;
				}
				
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
					// 通過alert驗證，沒通過validationEngine驗證，要enable確認按鈕
					$("#CMSUBMIT").prop("disabled", false);
					
				} else {
					// 通過表單驗證準備送出
					processQuery();
				}
			});
		}
		
		
		
		
		// 通過表單驗證準備送出
		function processQuery() {
         	
			//新增EMAIL驗證ajax
			var urihost = "${__ctx}";
			var cusidn = '${sessionScope.cusidn_n361}';
			var email = $('#DPMYEMAIL').val();
			var uri = urihost+"/ONLINE/APPLY/check_mail_aj";
			var rdata = { CUSIDN: cusidn , EMAIL: email };
			result = fstop.getServerDataEx(uri, rdata, false);
			
			if(result !=null && result.result == true ){
				// 遮罩
	         	initBlockUI();
				// 解除表單驗證
				$("#formId").validationEngine('detach');
	         
	            $("#formId").submit();
	            
			}else{
				alert(result.message);
				$("#DPMYEMAIL").focus();
				$('#BILLMTH').prop('checked', false);
				$("#CMSUBMIT").attr("class", "ttb-button btn-flat-gray");
				$("#CMSUBMIT").attr("disabled", true);
			}
			
		}
		
		
	 	// 舊網銀表單驗證
		function validate() {
			// 臺幣活期性存款帳戶為必填，儘管有填寫外幣活期性存款帳戶
			if( !$("#AGREETWY").prop("checked") ) {
				//alert("請指定新臺幣活期性存款帳戶");//<!-- 請勾選新臺幣活期性存款帳戶 -->
				errorBlock(
							null, 
							null,
							["請指定新臺幣活期性存款帳戶"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#AGREETWY").focus();
				return false;
			} else {
				$("#ACN_SSV").val($("#ACN_SSV1").val());
			}
			
			// 外幣活期性存款帳戶
			if( $("#AGREEFXY").prop("checked") ) {
				$("#ACN_FUD").val($("#ACN_FUD1").val());
				$("#ACN_FUD1").addClass("validate[required]");
			} else {
				$("#ACN_FUD").val("");
				$("#ACN_FUD1").removeClass("validate[required]");
			}
			
			// 同意通知書於發送E-mail後視為交付
			if( !$("#BILLMTH").prop("checked") ) {
				//alert("<spring:message code= "LB.Alert142" />"); // <!-- 請勾選通知及報告書之收取方式 -->
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert142' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#BILLMTH").focus();
				return false;		
			}
			else
			{
				$("#BILLMTH").val("2");
			}
			
			// 通過alert驗證，確認按鈕disable防止重送
			$("#CMSUBMIT").prop("disabled", true);
			
			return true;
		}
		
	 	
		// 初始化 checkbox
	 	function initCheckbox()
	 	{
	 		var count1= "${count1}";
	 		var count2= "${count2}";

	 		// 回上頁會選取指定帳戶故先清空
	 		$('#formId').trigger("reset");
	 		
	 		// 無臺幣活期性存款帳戶
			if(count1 == 0) {
				// 只能指定外幣帳戶
				$('input[name="AGREETW"]').prop("disabled", "true");
				$("#AGREETWN").prop("checked", true);
				$("#AGREEFXY").prop("checked", true);
				$("#ACN_FUD1").prop("disabled", false);
				$("#ACN_SSV1").prop("disabled", true);
			}
	 		
	 		// 無外幣活期性存款帳戶
	   	  	if(count2 == 0) {
	   	  		// 只能指定臺幣帳戶
	   	 		$('input[name="AGREEFX"]').prop("disabled", "true");
	   	 		$("#AGREEFXN").prop("checked", true);
	   	 		$("#AGREETWY").prop("checked", true);
	   	 		$("#ACN_SSV1").prop("disabled", false);
	   	 		$("#ACN_FUD1").prop("disabled", true);
	   	  	}
	 		
	 		// 臺、外幣皆有帳戶
			if( count1!=0 && count2!=0 ) {
				// 臺幣未指定帳戶
				if( !$("#AGREETWY").prop("checked") ) {
					$("#ACN_SSV1").prop("disabled", true);
				}
				// 外幣未指定帳戶
				if( !$("#AGREETWY").prop("checked") ) {
					$("#ACN_FUD1").prop("disabled", true);
				}
			}
	 	}
		
		// 回上一頁填入資料
		function refillData() {
			var jsondata = '${previousdata}';
			console.log(jsondata);
			
			if (jsondata != "") {
				JSON.parse(jsondata, function(key, value) {
					if(key) {
						var obj = $("#"+key);
						var type = obj.attr('type');
						
						console.log('--------------------------------');
						console.log('type: ' + type);
						console.log('key: ' + key);
						console.log('value: ' + value);
						console.log('--------------------------------');
						
						if(type == 'text'){
							obj.val(value);
						}
						
						if(type == 'hidden'){
							obj.val(value);
						}
						
						if(type == 'checkbox'){
							obj.prop('checked', true);
						}
						
						// 例外處理--新臺幣活期性存款帳戶
						if(key == 'AGREETW') {
							$("input[name='AGREETW']").filter('[value='+value+']').prop('checked', true);
						}
						// 例外處理--外幣活期性存款帳戶
						if(key == 'AGREEFX') {
							$("input[name='AGREEFX']").filter('[value='+value+']').prop('checked', true);
						}
						// 例外處理--新臺幣活期性存款帳戶--select不是input另外處理
						if(key == 'ACN_SSV1'){
							$("#ACN_SSV1 option").filter(function() {
								return $(this).val() == value;
							}).attr('selected', true);
						}
						// 例外處理--外幣活期性存款帳戶--select不是input另外處理
						if(key == 'ACN_FUD1'){
							$("#ACN_FUD1 option").filter(function() {
								return $(this).val() == value;
							}).attr('selected', true);
						}
					}
				});
			}
		}
		
	</script>
</head>

<body>
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0847" /></li>
		</ol>
	</nav>
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>預約開立基金戶</h2>
				<div id="step-bar">
					<ul>
						<li class="finished">身份驗證</li>
						<c:if test="${FATCA_CONSENT == 'Y'}">
							<li class="finished">FATCA個人客戶身份識別聲明</li>
						</c:if>
						<li class="finished">顧客權益</li>
						<li class="active">申請資料</li>
						<li class="">確認資料</li>
						<li class="">完成申請</li>
					</ul>
				</div>
				
				<form method="post" id="formId" action="${__ctx}${next}">
<!-- 					<input type="hidden" name="ACN_SSV" id="ACN_SSV"> -->
					<input type="hidden" id="ACN_FUD" name="ACN_FUD">
					<input type="hidden" id="ICCOD" name="ICCOD" value="N">
					
					<input type="hidden" name="FDINVTYPE" value="${FDINVTYPE}"/>
    				<!--FATCA所需欄位-->
    				<input type="hidden" name="FATCA_CONSENT" value="${FATCA_CONSENT}"/>
    				<input type="hidden" name="CITY" value="${CITY}"/>
					
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<p>開戶資料</p>
								</div>
								<div class="classification-block">
									<p>帳戶設定</p>
									<p>( <span class="high-light">*</span> 為必填)</p>
								</div>
								<p class="form-description">請指定您用來信託開戶的存款帳戶(請至少指定一個活期性存款帳戶)</p>
								<!-- 新臺幣活期性存款帳戶 * -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>新臺幣活期性存款帳戶 <span class="high-light">*</span></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block" for="AGREETWY">指定帳戶
												<input type="radio" name="AGREETW" id="AGREETWY" value="Y" />
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block" for="AGREETWN">不指定
												<input type="radio" name="AGREETW" id="AGREETWN" value="N" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<c:choose>
												<c:when test="${bs.ACN_SSV1.size() > 0}">
													<select id="ACN_SSV1" name="ACN_SSV1" class="custom-select select-input w-auto validate[required]">
														<option value="">請選擇新台幣活期性存款帳戶</option>
														
														<c:forEach var="acct" items="${bs.ACN_SSV1 }">
															<option value="${acct }">${acct }</option>
														</c:forEach>
														
													</select>
													<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏 -->
													<span class="hideblock">
														<!-- 驗證用的input -->
														<input id="ACN_SSV" name="ACN_SSV" type="text" class="text-input" 
															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
													</span>
												</c:when>
												<c:otherwise><spring:message code= "LB.X0319" /><!-- 無帳號 --></c:otherwise>
											</c:choose>
										</div>
									</span>
								</div>
	
								<!-- 外幣活期性存款帳戶 * -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>外幣活期性存款帳戶 <span class="high-light">*</span></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block" for="AGREEFXY">指定帳戶
												<input type="radio" name="AGREEFX" id="AGREEFXY" value="Y" />
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block" for="AGREEFXN">不指定
												<input type="radio" name="AGREEFX" id="AGREEFXN" value="N" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<c:choose>
												<c:when test="${bs.ACN_FUD1.size() > 0}">
													<select id="ACN_FUD1" name="ACN_FUD1" class="custom-select select-input w-auto">
														<option value="">請選擇外幣活期性存款帳戶</option>
														
														<c:forEach var="fxAcct" items="${bs.ACN_FUD1 }">
															<option value="${fxAcct }">${fxAcct }</option>
														</c:forEach>
														
													</select>
												</c:when>
												<c:otherwise><spring:message code= "LB.X0319" /><!-- 無帳號 --></c:otherwise>
											</c:choose>
										</div>
									</span>
								</div>
	
								<div class="ttb-input">
									<label class="check-block">委託人同意，自立約日起，授權委託人就本約定書規範內交易產生之信託金額、相關費用及信託收益等得逕行轉入委託人開立於受託人之上列帳號，不另開具存款憑條，並同意以受託人電腦系統保存相關記錄為正確。
										<input type="checkbox" id="ACNMTH" name="ACNMTH" />
										<span class="ttb-check"></span>
									</label>
								</div>
	
								<div class="classification-block">
									<p>通知與報告書</p>
									<p>( <span class="high-light">*</span> 為必填)</p>
								</div>
								<p class="form-description">各項通知及報告書之收取方式</p>
	
								<!-- E-mail * -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>E-mail <span class="high-light">*</span></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="DPMYEMAIL" name="DPMYEMAIL" value="${bs.DPMYEMAIL }" maxlength="47"
												class="text-input validate[required, funcCall[validate_EmailCheck[DPMYEMAIL]]]" placeholder="xxx@mail.xxx.com">
											<span class="check-block" style="padding-left: 0px; font-size: inherit"><spring:message code="LB.X2619"></spring:message></span>
										</div>
									</span>
								</div>
	
								<div class="ttb-input">
									<label class="check-block">本人同意上述各項通知及報告書於發送E-mail後視為交付。若有資料需修改，除E-mail信箱可利用一般網路銀行變更外，其餘事項於臨櫃辦理。
										<input type="checkbox" id="BILLMTH" name="BILLMTH" />
										<span class="ttb-check"></span>
									</label>
								</div>
	
								<div class="classification-block">
									<p>推介人員</p>
								</div>
	
								<!--推介人員行員編號-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>推介人員行員編號</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" class="text-input" id="CTDNUM" name="CTDNUM" maxlength="6" placeholder="請輸入推介行員編號"
												class="text-input validate[funcCall[validate_CheckNumber['<spring:message code= "LB.X1298" />', CTDNUM, false]]]" >
										</div>
									</span>
								</div>
							</div>
							
							<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-gray" disabled="disabled" value="<spring:message code= "LB.X0080" />" />
	
						</div>
					</div>
				</form>
				
				<ol class="list-decimal description-list">

				</ol>

			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
</body>

</html>