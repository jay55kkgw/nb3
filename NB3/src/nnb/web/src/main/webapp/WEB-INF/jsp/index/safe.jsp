<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/fullcalendar.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/fullcalendar.print.min.css" rel="stylesheet"
	media="print">
<script type="text/javascript" src="${__ctx}/js/moment.min.js"></script>
<script type="text/javascript" src="${__ctx}/js/fullcalendar.min.js"></script>
<script type="text/javascript" src="${__ctx}/js/fullcalendar.zh-cn.js"></script>
<script type="text/javascript" src="${__ctx}/js/fullcalendar.zh-tw.js"></script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<div>
		<div style="width: 600px; overflow: hidden;">
			<table cellspacing="1" cellpadding="2" class="" id="myScrollTable"
				width="400" style="height: auto; width: 600px;">
				<thead>
				</thead>
				<tbody>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"><img
							alt="" src="./fstop/images/title1.gif" border="0"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td><table border="0" cellpadding="0" cellspacing="0"
								width="100%">
								<tbody>
									<tr>
										<td width="3%" colspan="2" class="msgHead"
											style="padding-right: 16px;">詐騙集團偽造與知名網站一模一樣的假網站，騙取客戶進入這些網站，輸入個人私密資料(如身份證字號、帳號、信用卡號、密碼等)，藉以盜取個人資料，非法使用，損害客戶權益。</td>
									</tr>
									<tr>
										<td width="100%" colspan="2" style="padding-right: 16px;">
										</td>
									</tr>
									<tr>
										<td colspan="2" style="padding-right: 16px;"><span
											class="msgTitle">因應措施</span></td>
									</tr>
									<tr>
										<td width="3%"><img alt=""
											src="./fstop/images/bullet.gif" border="0" width="8"
											height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">臺灣企銀網路銀行網址<font
											color="red"><b> https://portal.tbb.com.tw</b></font>，登入前請逐字核對網址確保進入正確的網站。<a
											onclick="window.open('./fstop/images/sample1.gif','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')"
											href="#">(圖示說明)</a></td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請檢查電腦螢幕右下角顯示安全鎖標誌<a
											onclick="window.open('./fstop/images/sample2.gif','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')"
											href="#">(圖示說明)</a>，點選安全鎖即顯示憑證資訊。<a
											onclick="window.open('./fstop/images/sample3.gif','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')"
											href="#">(圖示說明)</a></td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請勿點選來路不明可疑電子郵件內的超連結。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請定期變更網路銀行密碼，以保護個人私密資料。</td>
									</tr>
								</tbody>
							</table></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"><img
							alt="" src="./fstop/images/title2.gif" border="0"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<td width="3%" colspan="2" class="msgHead"
											style="padding-right: 16px;">詐騙集團假冒銀行或線上服務業者名義通知客戶資料過期、無效需要更新，或者是基於安全理由進行身分驗證，要求客戶輸入個人私密資料（如身分證字號、帳號、信用卡號、密碼等）。客戶如一時不察經由電子郵件指引的網址，連結至偽冒網站輸入個人資料，輕則成為垃圾郵件業者的名單，重則電腦可能被植入木馬程式，破壞系統或個人私密資料遭竊。</td>
									</tr>
									<tr>
										<td width="100%" colspan="2" style="padding-right: 16px;">
										</td>
									</tr>
									<tr>
										<td colspan="2" style="padding-right: 16px;"><span
											class="msgTitle">因應措施</span></td>
									</tr>
									<tr>
										<td width="3%"><img alt=""
											src="./fstop/images/bullet.gif" border="0" width="8"
											height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請勿理會來路不明可疑的電子郵件，且勿點選所提供的超連結，並立即刪除。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請勿回覆要求提供個人機密資料的可疑電子郵件。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;"><font
											color="red">請注意！臺灣企銀絕對不會經由電子郵件，要求客戶輸入個人機密資料。
												如收到此類可疑電子郵件，請立即通知臺灣企銀服務專線：0800-00-7171。</font></td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請安裝及定期更新您的防毒軟體。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請定期變更網路銀行密碼，以保護個人私密資料。</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"><img
							alt="" src="./fstop/images/title3.gif" border="0"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<td width="3%" colspan="2" class="msgHead"
											style="padding-right: 16px;">間諜軟體是指在未經使用者同意的情況下進行廣告、蒐集個人資訊或修改電腦設定等行為的軟體。間諜軟體通常會藉由一些免費網路下載程式，在使用者下載程式時植入到使用者的電腦中，當間諜軟體運作時，可能會干擾客戶的上網活動，或是造成個人資料及儲存於電腦中的機密資料等外洩，嚴重影響資訊安全及個人隱私。</td>
									</tr>
									<tr>
										<td width="100%" colspan="2" style="padding-right: 16px;">
										</td>
									</tr>
									<tr>
										<td colspan="2" style="padding-right: 16px;"><span
											class="msgTitle">因應措施</span></td>
									</tr>
									<tr>
										<td width="3%"><img alt=""
											src="./fstop/images/bullet.gif" border="0" width="8"
											height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">僅從您信任的網站下載程式。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請安裝反間諜軟體的防護軟體。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請安裝及定期更新您的防毒軟體。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請隨時安裝最新的系統安全性更新。<br>
											操作流程：上網 =&gt; 工具 =&gt; Windows Update =&gt; 建議選擇『快速』更新。
										</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請定期變更網路銀行密碼，以保護個人私密資料。</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"><img
							alt="" src="./fstop/images/title4.gif" border="0"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<td width="3%" colspan="2" class="msgHead"
											style="padding-right: 16px;">木馬程式是一種惡意程式，與病毒相似，有很強的隱秘性，並會偽裝成其他的系統程式，隨著作業系統而啟動，但不會自我複製、感染，這一點和一般病毒程式不同。木馬程式會進行一連監聽偵測系統，並蒐集木馬程式指定的資料並且回送給木馬程式發送者。網路駭客常利用偽造電子郵件、偽冒網站引誘客戶點選下載執行或安裝，於客戶未察覺的情形下植入程式碼，破壞系統、竊取個人私密資料或甚至遠端操控被植入者的電腦。</td>
									</tr>
									<tr>
										<td width="100%" colspan="2" style="padding-right: 16px;">
										</td>
									</tr>
									<tr>
										<td colspan="2" style="padding-right: 16px;"><span
											class="msgTitle">因應措施</span></td>
									</tr>
									<tr>
										<td width="3%"><img alt=""
											src="./fstop/images/bullet.gif" border="0" width="8"
											height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請安裝及定期更新您的防毒軟體。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請安裝及定期更新您的清掃木馬軟體。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請隨時安裝最新的系統安全性更新。<br>
											操作流程：上網 =&gt; 工具 =&gt; Windows Update =&gt; 建議選擇『快速』更新。
										</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請定期變更網路銀行密碼，以保護個人私密資料。</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"><img
							alt="" src="./fstop/images/title5.gif" border="0"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<td width="3%" colspan="2" class="msgHead"
											style="padding-right: 16px;">電腦防火牆是為保護一般個人電腦或是公司行號內的內部網路遭到駭客的入侵與破壞，在外部網路與內部網路之間，使用一個過濾器來過濾具有攻擊意圖或不懷好意的資訊或程式，以阻絕未經授權的非法入侵。</td>
									</tr>
									<tr>
										<td width="100%" colspan="2" style="padding-right: 16px;">
										</td>
									</tr>
									<tr>
										<td colspan="2" style="padding-right: 16px;"><span
											class="msgTitle">因應措施</span></td>
									</tr>
									<tr>
										<td width="3%"><img alt=""
											src="./fstop/images/bullet.gif" border="0" width="8"
											height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請安裝及定期更新您的防毒軟體。</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"><img
							alt="" src="./fstop/images/title6.gif" border="0"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<td width="3%" colspan="2" class="msgHead"
											style="padding-right: 16px;">無論多麼安全的系統，仍有賴客戶有正確的使用觀念，才能充分維護本身存款安全，請妥善保管網路銀行密碼。</td>
									</tr>
									<tr>
										<td width="100%" colspan="2" style="padding-right: 16px;">
										</td>
									</tr>
									<tr>
										<td colspan="2" style="padding-right: 16px;"><span
											class="msgTitle">注意事項</span></td>
									</tr>
									<tr>
										<td width="3%"><img alt=""
											src="./fstop/images/bullet.gif" border="0" width="8"
											height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請勿使用個人或公司顯性資料作為網路銀行使用者名稱或密碼，以免遭他人猜中。例如身分證字號、統一編號、生日、聯絡電話，或與基本資料相關之具規則性排列，例如身分證後6碼，電話前/後6碼等。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請勿使用連號或重號作為網路銀行使用者名稱或密碼，例如123456、654321、111111、ABCDEF、FEDCBA、AAAAAA。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td style="padding-right: 16px;">請勿洩漏密碼給任何人，包含本行行員。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請勿將網路銀行登錄之相關資料以電子郵件或其他類似之電子傳送方式進行傳送（包括以通訊軟體，例如MSN、Skype）。</td>
									</tr>
									<tr>
										<td width="3%"><img alt=""
											src="./fstop/images/bullet.gif" border="0" width="8"
											height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請牢記您的網路銀行密碼，請勿用筆寫下，以免遭他人冒用。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請將一般網站的密碼與網路銀行密碼特別區分，以免遭他人盜用。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請定期變更網路銀行密碼，以保護個人私密資料。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請勿在公共電腦、網咖或使用無線上網進行任何網路銀行交易。</td>
									</tr>
									<tr>
										<td width="3%"><img alt=""
											src="./fstop/images/bullet.gif" border="0" width="8"
											height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">離開座位或交易完畢，請立即登出網路銀行。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">客戶接獲網路銀行交易對帳單時，請立即核對是否正確。如有不符，請立即通知銀行查明。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請安裝使用正版軟體。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請勿下載來路不明軟體。</td>
									</tr>
									<tr>
										<td width="3%"><img alt=""
											src="./fstop/images/bullet.gif" border="0" width="8"
											height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請勿開啟來路不明郵件。</td>
									</tr>
									<tr>
										<td><img alt="" src="./fstop/images/bullet.gif"
											border="0" width="8" height="8"></td>
										<td class="msgBody" style="padding-right: 16px;">請勿瀏覽來路不明網站。</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>


					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"><img
							alt="" src="./fstop/images/title7.jpg" border="0"></td>
					</tr>
					<tr>
						<td width="100%" colspan="2" style="padding-right: 16px;"></td>
					</tr>
					<tr>
						<td><table border="0" cellpadding="0" cellspacing="0"
								width="100%">
								<tbody>
									<tr>
										<td width="3%" colspan="2" class="msgHead">
											<table>
												<tbody>
													<tr valign="top">
														<td class="msgHead">一、</td>
														<td class="msgHead" style="padding-right: 16px;">請確認網站與手機應用程式正確性。</td>
													</tr>
													<tr valign="top">
														<td class="msgHead">二、</td>
														<td class="msgHead" style="padding-right: 16px;">請妥善保管使用者代號、密碼並提高警覺勿交付給第三人或其他非授權網站。</td>
													</tr>
													<tr valign="top">
														<td class="msgHead">三、</td>
														<td class="msgHead" style="padding-right: 16px;">請於輸入密碼時，提高警覺避免旁人窺視。</td>
													</tr>
													<tr valign="top">
														<td class="msgHead">四、</td>
														<td class="msgHead" style="padding-right: 16px;">請經常變更密碼且勿與其他應用系統及服務密碼共用。</td>
													</tr>
													<tr valign="top">
														<td class="msgHead">五、</td>
														<td class="msgHead" style="padding-right: 16px;">請勿點選來路不明網址及程式並建議安裝防毒軟體。</td>
													</tr>
													<tr valign="top">
														<td class="msgHead">六、</td>
														<td class="msgHead" style="padding-right: 16px;">請勿書寫密碼於金融卡或其他明顯且他人可取得處。</td>
													</tr>
													<tr valign="top">
														<td class="msgHead">七、</td>
														<td class="msgHead" style="padding-right: 16px;">請勿記錄密碼於電腦或行動裝置內。</td>
													</tr>
													<tr valign="top">
														<td class="msgHead">八、</td>
														<td class="msgHead" style="padding-right: 16px;">請勿透過未加密機制傳送密碼。</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>


									<tr>
										<td width="100%" colspan="2" style="padding-right: 16px;"><img
											alt="" src="./fstop/images/title8.jpg" border="0"></td>
									</tr>
									<tr>
										<td width="100%" colspan="2" style="padding-right: 16px;">
										</td>
									</tr>
									<tr>
										<td><table border="0" cellpadding="0" cellspacing="0"
												width="100%">
												<tbody>
													<tr>
														<td width="3%" colspan="2" class="msgHead">
															<table>
																<tbody>
																	<tr valign="top">
																		<td class="msgHead" colspan="2"
																			style="padding-right: 16px;"><your user=""
																				identification="" code,="" password="" we=""
																				issue="" to="" you="" together="" comprise=""
																				your="" security="" codes.="" in="" connection=""
																				with="" codes:=""
																				本行發給閣下的用戶識別碼、密碼共同構成閣下的保安密碼。關於閣下的保安密碼:=""<=""
																			td=""></your></td>
																	</tr>
																	<tr valign="top">
																		<td class="msgHead">1.</td>
																		<td class="msgHead" style="padding-right: 16px;">Always
																			memory your password and destroy the original printed
																			copy of the secret code. Try not to write it down. If
																			you cannot remember the password, you should always
																			disguise the password and keep it in a safe place
																			separate from where you keep your Login ID, your
																			computer and bank account details.<br>
																			把密碼牢記撕毀密碼函。切勿把密碼用筆記下來。如果您確實記不住密碼，亦請把它小心收藏，切勿與用戶號碼、電腦及銀行戶口的詳細資料放在一起。
																		</td>
																	</tr>
																	<tr valign="top">
																		<td class="msgHead">2.</td>
																		<td class="msgHead" style="padding-right: 16px;">You
																			can change the password to any other 8-digit numbers
																			of your choice by going to our website,
																			https://portal.tbb.com.tw. Please change your
																			password regularly.<br>
																			閣下可透過本行網站https://portal.tbb.com.tw將此密碼更改至任何8位數字組成之自選密碼
																			。並請定期更改閣下的密碼。
																		</td>
																	</tr>
																	<tr valign="top">
																		<td class="msgHead">3.</td>
																		<td class="msgHead" style="padding-right: 16px;">Do
																			not use easy-to-guess numbers as your password, such
																			as your birthday and phone number and avoid using
																			repetitive number as your password.<br>
																			切勿使用容易被猜中的號碼，如出生日期或電話號碼及避免使用重覆之數字組合作為閣下的密碼。
																		</td>
																	</tr>
																	<tr valign="top">
																		<td class="msgHead">4.</td>
																		<td class="msgHead" style="padding-right: 16px;">When
																			keying in your password, cover the keypad and be
																			aware of your surroundings. Report any unusual
																			activity to us.<br>
																			當閣下輸入密碼時，請加以遮掩並留意四周。如有可疑，請聯絡我們。
																		</td>
																	</tr>
																	<tr valign="top">
																		<td class="msgHead">5.</td>
																		<td class="msgHead" style="padding-right: 16px;">Never
																			reveal your password to anyone (No bank staff or
																			police will ask for your password).If you receive any
																			anomalous email or letter or detect any unusual
																			change in website address, asking for sensitive
																			account information, you must not disclose. You
																			should report to us immediately by phone if in doubt.<br>
																			切勿向任何人透露閣下的密碼(銀行的任何人員及警察人員都不會要求閣下說出密碼)。如閣下收到異常的電郵或信件或發現網址有不尋常的改變/轉移，並要求閣下提供戶口資料或重要個人資料，切勿透露。如有疑問，請致電本行。
																		</td>
																	</tr>
																	<tr valign="top">
																		<td class="msgHead">6.</td>
																		<td class="msgHead" style="padding-right: 16px;">You
																			should be advised to refer to the security advice
																			provided by us from time to time.<br>
																			閣下應不時查閱本行提供的保安建議。
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>




													<tr>
														<td width="100%" colspan="2" style="padding-right: 16px;">
														</td>
													</tr>

												</tbody>
											</table> <script type="text/javascript">
												var t = new ScrollableTable(
														document
																.getElementById('myScrollTable'),
														570, 600);
											</script>
										</td>
									</tr>
								</tbody>
							</table></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<%@ include file="../index/footer.jsp"%>

</body>

</html>