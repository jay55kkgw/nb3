<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/naturalCertificate.js"></script>

	<script type="text/JavaScript">
		var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
		var myobj = null; // 自然人憑證用

		
	    $(document).ready(function() {
	    	// HTML載入完成後開始遮罩
	    	setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 銀行代碼
			setTimeout("creatDpBHNO()",500);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 1000);

			datetimepickerEvent();
		});
		
		function init() {
			// 確認鍵 click
			goOn();
			// 上一頁按鈕 click
			goBack();
			// 初始化驗證碼
			initKapImg();
			// 初始化自然人元件
			var acntype = "${financial_card_confirm_step1.data.REC.ACNTYPE}";
			if (acntype == "A1") {
		    	// 初始化自然人元件
				initBlockUI(); 
				setTimeout("initNatural_EXCUTE()", 50);
			}
		}
		
	
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				// 送出進表單驗證前將span顯示
				$("#hideblock").show();
				
				console.log("submit~~");
	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					
					// 通過表單驗證
					processQuery();
				}
			});
		}
		
		// 上一頁按鈕 click
		function goBack() {
			// 上一頁按鈕
			$("#pageback").click(function() {
				// 遮罩
				initBlockUI();
				
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				
				// 讓Controller知道是回上一頁
				$('#back').val("Y");
				// 回上一頁
				var action = '${__ctx}' + '/ONLINE/APPLY/online_apply';
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
		}
		
		// 通過表單驗證準備送出
		function processQuery(){
			$('#UID').val("${financial_card_confirm_step1.data.REC.CUSIDN}");
			
			// 憑證驗證機制
			urihost = "${__ctx}";
			console.log("urihost: " + urihost);
			var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
			docheck(capUri);
		}

		
		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
	
			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}

		function vaInterBankAcc() {
			var uri = '${__ctx}'+"/FISC/InterBank/verify_account_aj";
			var rdata = $("#formId").serializeArray();
			callInterBankVerifyAcc(uri, rdata, "InterBankVerifyOTP","changeCode", "validateMaskInterBankAcc");
		}
		function vaLocalBankAcc(){	
			//本行帳戶驗證
			var uri = '${__ctx}'+"/DIGITAL/ACCOUNT/verify_lo_account_aj";
			var rdata = $("#formId").serializeArray();
			callInterBankVerifyAcc(uri, rdata, "LoBankVerifyOTP","changeCode", "validateMaskLoBankAcc");
		}

		//	建立銀行代號下拉選單
		function creatDpBHNO(){
			//宣告銀行物件 
			var bkdataList = {}
			//2021/05/13 ， 業務單位email通知 
			//非同步名單，若要新增參加銀行，必須請數金部給需求，並依資訊部行程上線，非急緊上線！
			//要排除自行驗證050(台企銀)。		
			//update 2022/03/10	H571110000111	
			var accedingBK = ['004','005','006','007','008','009','011', '012','013','016','017',
										'021','048','052','054','101','102','103','108','114','118','132','146', '147','162','216','600','803',
										'805','806','807','808','809','810','812','815','816','822','824','826','952'];
			//var excludeBk = ['000','001','050'];
			
			var uri = '${__ctx}'+"/FISC/InterBank/getDpBHNO_aj";
			var rdata = {type: 'dpbhno' };
			var options = { keyisval:true ,selectID:'#DPBHNO'}
			var data = fstop.getServerDataEx(uri,rdata,false);
			if(data !=null && data.result == true ) {			
				//建立選單資料 
				$.each(data.data,function(value,text){
					if (accedingBK.indexOf(text.substr(0,3)) > -1) {					
						bkdataList[value] = text;
					}
				});
				fstop.creatSelect(bkdataList,options);			
			}
			
			$( "#DPBHNO" ).change(function() {
				var acno = $('#DPBHNO :selected').val();
				console.log("DPBHNO.acno>>"+acno);
				
				$( "#ATTIBK" ).val(acno.substr(0,3));
			});
		}

		function  InterBankVerifyOTP(data) {
			initBlockUI(); 
			$("#otpCommit").attr("disabled", true);
			$('#otp_error').hide();
			console.log(data);

			var uri = '${__ctx}'+"/FISC/InterBank/otpverify_aj";
			var rdata = {
					OTP : data,
					CUSIDN : $("#CUSIDN").val()
				};
			
			fstop.getServerDataEx(uri, rdata, true,checkOTPFish);
		}

		function checkOTPFish(otpresult) {
			unBlockUI(initBlockId);
			$("#otpCommit").attr("disabled", false);
			if (otpresult.result) {
				closeOTPDialog();
				initBlockUI(); 
				try{
					$("#OTPTOKEN").val(otpresult.data.otptoken);
				}catch(e){
					console.log(e);
				}
				$("#formId").submit();	
			} else {
				$('#otp_error').show();
			}
			
		}
		function LoBankVerifyOTP(data){
			initBlockUI(); 
			$("#otpCommit").attr("disabled", true);
			$('#otp_error').hide();
			console.log(data);

			var uri = '${__ctx}'+"/DIGITAL/ACCOUNT/otpverify_lo_account_aj";
			var rdata = {
					SMSOTP : data,
					CUSIDN : $("#CUSIDN").val()
				};
			
			fstop.getServerDataEx(uri, rdata, true,checkOTPFish);
		}

		//日曆
		function datetimepickerEvent(){

		    $(".BIRTHDATE").click(function(event) {
				$('#BIRTH').datetimepicker('show');
			});
			
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			});
		}
	</script>
</head>

<body>
	<div id="obj"></div>
	<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component_u2.jsp"%>
	<%@ include file="../component/otp_component.jsp"%>
	
	
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 數位存款帳戶晶片金融卡確認領用申請及變更密碼     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1535" /></li>
		</ol>
	</nav>

	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
		<!-- 快速選單內容 -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<!-- 數位存款帳戶晶片金融卡確認領用申請及變更密碼 -->
				<h2><spring:message code="LB.D1535"/></h2>
				<div id="step-bar">
                    <ul>
                        <li class="active">身分驗證</li>
                        <li class="">變更密碼</li>
                        <li class="">完成申請</li>
                    </ul>
                </div>
				<form id="formId" method="post" action="${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm_step2">
					<!-- 驗證機制所需欄位 -->
					<input type="hidden" id="TYPE" name="TYPE" value="13">
					<input type="hidden" id="NOTIEINSTALL" name="NOTIEINSTALL" value="">
					<input type="hidden" id="CUSIDN" name="CUSIDN" value="${financial_card_confirm_step1.data.REC.CUSIDN}">
					<input type="hidden" id="ACNTYPE" name="ACNTYPE" value="${financial_card_confirm_step1.data.REC.ACNTYPE}">
					<input type="hidden" id="BRHCOD" name="BRHCOD" value="${financial_card_confirm_step1.data.REC.BRHCOD}">
					<input type="hidden" id="AUTHCODE" name="AUTHCODE" value="">
					<input type="hidden" id="AUTHCODE1" name="AUTHCODE1" value="">
					<input type="hidden" id="CertFinger" name="CertFinger" value="">
					<input type="hidden" id="CertB64" name="CertB64" value="">
					<input type="hidden" id="CertSerial" name="CertSerial" value="">
					<input type="hidden" id="CertSubject" name="CertSubject" value="">
					<input type="hidden" id="CertIssuer" name="CertIssuer" value="">
					<input type="hidden" id="CertNotBefore" name="CertNotBefore" value="">
					<input type="hidden" id="CertNotAfter" name="CertNotAfter" value="">
					<input type="hidden" id="HiCertType" name="HiCertType" value="">
					<input type="hidden" id="CUSIDN4" name="CUSIDN4" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					<input type="hidden" id="jsondc" name="jsondc" value='${financial_card_confirm_step1.data.REC.jsondc}'>	
					<input type="hidden" id="CHIP_ACN" name="CHIP_ACN" value="">
					<input type="hidden" id="UID" name="UID" value="">
					<input type="hidden" id="ACN" name="ACN" value="${financial_card_confirm_step1.data.REC.ACN}">
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="OUTACN" name="OUTACN" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="ATTIBK" name="ATTIBK" value="004">					
					<input type="hidden" name="OTPTOKEN" id="OTPTOKEN" value="">
					
					
					<div class="main-content-block row">
						<div class="col-12">
							<!-- 身份驗證 -->
							<div class="mt-2 mb-4 text-center"><h3><spring:message code="LB.D0851"/></h3></div>
							<!-- 驗證機制 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
										</label>
									</span>
									<!-- 驗證機制選項 -->
									<span class="input-block">
									
										<c:if test="${financial_card_confirm_step1.data.REC.ACNTYPE=='A1'}">
											<!-- 自然人憑證 -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.D0437"/> (申請數位存款帳戶使用的自然人憑證)
													<input type="radio" name="FGTXWAY" id="CMNPC" value="4" checked/>
													<span class="ttb-radio"></span>
												</label>
											</div>
										</c:if>
										
										<c:if test="${financial_card_confirm_step1.data.REC.ACNTYPE=='A2'}">
											<!-- 晶片金融卡 -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Financial_debit_card" /> (申請數位存款帳戶使用的晶片金融卡)
													<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
													<span class="ttb-radio"></span>
												</label>
											</div>
										</c:if>
										
										<c:if test="${financial_card_confirm_step1.data.REC.ACNTYPE=='A3'}">
											<!-- 跨行金融帳戶認證 -->
											<div class="ttb-input">
												<label class="radio-block" > 
													<spring:message code="LB.Fisc_inter-bank_acc" /> (申請數位存款帳戶使用他行帳戶)
													<input type="radio" name="FGTXWAY" id="CMFISCACC" value="5"  checked="checked"> 
													<span class="ttb-radio"></span>
												</label>
											</div>
										</c:if>	
										<c:if test="${ financial_card_confirm_step1.data.REC.ACNTYPE.equals('A4') }">
											<!-- 本行金融帳戶認證 -->
											<div class="ttb-input">
												<label class="radio-block" for="LOISCACC" onclick=""> 
													自行帳戶+手機簡訊驗證
													<input type="radio" name="FGTXWAY" id="LOISCACC" value="6" checked="checked"> 
													<span class="ttb-radio"></span>
												</label>
											</div>																						
										</c:if>
									</span>									
								</div>						
								
								<c:if test="${ financial_card_confirm_step1.data.REC.ACNTYPE.equals('A4') }">
									<div class="ttb-input-block">
										<!-- 自然人憑證PIN碼 -->
										<div class="ttb-input-item row">
											<span class="input-title">
												<label>
													<h4>存款帳號</h4>
												</label>
											</span>
											<span class="input-block">
												<div class="ttb-input">
													<input type="number"  inputmode="numeric" class="text-input validate[required, maxSize[11]]" 
														onkeyup="maxSizeCode()" name="LOATTIAC" id="LOATTIAC" value="" 
														placeholder="<spring:message code="LB.B0018" />" size="11" maxlength="11"  autocomplete="off"> 
												</div>
											</span>
										</div>
									</div>
								</c:if>		
								<c:if test="${financial_card_confirm_step1.data.REC.ACNTYPE=='A3'}">
									<!-- 跨行金融帳戶認證 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.W1038" /></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<select id="DPBHNO" name="DPBHNO" class="custom-select select-input half-input"></select>
											</div>
											<div class="ttb-input">
												<input type="text"  class="text-input" name="ATTIAC" id="ATTIAC" value="" placeholder="<spring:message code="LB.Enter_Account" />" size="16" maxlength="16"  autocomplete="off"> 
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.D0069" /></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<input type="text" class="text-input validate[funcCallRequired[validate_cellPhone[手機號碼格式錯誤,ATMOBI]]]" maxLength="10" size="11" id="ATMOBI" name="ATMOBI" value="" />
												<span class="input-remarks">請輸入留存於他行的手機號碼</span>
											</div>
										</span>
									</div>
									<!--出生年月日-->
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
		                                    <label>
		                                        <h4><spring:message code= "LB.D0054" /></h4>
		                                    </label>
		                                </span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
												  <input type="text" class="text-input datetimepicker validate[funcCall[validate_CheckDate[<spring:message code= "LB.D0054" />,BIRTH,null,null]]]" id="BIRTH" name="BIRTH" value="" >
		                                          <span class="input-unit BIRTHDATE"><img src="${__ctx}/img/icon-7.svg"/></span>
		                                    </div>
		                                </span>
		                            </div>
								</c:if>								
								
	                            <!-- 驗證碼 -->
	                            <div class="ttb-input-item row" id="chaBlock">
	                                <span class="input-title">
	                                    <label>
	                                        <h4>驗證碼</h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <input id="capCode" name="capCode" type="text" class="text-input input-width-125" 
	                                        	maxlength="6" autocomplete="off" placeholder="請輸入驗證碼" >
                                        	<img name="kaptchaImage" class="verification-img"/>
                                        	<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="changeCode()" value="重新產生" />
	                                        <span class="input-remarks">請注意：英文不分大小寫，限半形字元</span>
	                                    </div>
	                                </span>
	                            </div>
								
							<c:if test="${financial_card_confirm_step1.data.REC.ACNTYPE=='A1'}">

								<!-- 自然人憑證PIN碼 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D1540" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<input type="PASSWORD" class="text-input" maxLength="8" size="10" id="NPCPIN" name="NPCPIN" value=""/>
										</div>
									</span>
								</div>
								
							</c:if>
								
							</div>
							
							<input type="button" class="ttb-button btn-flat-gray" name="pageback" id="pageback" value="<spring:message code="LB.Back_to_previous_page" />" />
							<input type="button" class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.X0080"/>" />
						</div>
					</div>
					
					<ol class="description-list">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li style="margin-left: 0rem">
							一、晶片金融卡開卡依據數位存款帳戶申辦時之身份驗證方式完成開卡程序:
							<ul class="text-left" style="list-style-position: outside;">
								<li style="margin-left: 2rem">(1) 第一類數存帳戶:自然人憑證+讀卡機</li>
								<li style="margin-left: 2rem">(2) 第二類數存帳戶:晶片金融卡+讀卡機或本行臨櫃開立之存款帳戶+手機簡訊驗證</li>
								<li style="margin-left: 2rem">(3) 第三類數存帳戶:他行臨櫃開立之存款帳戶+手機簡訊驗證</li>
							</ul>
						</li>
						<li style="margin-left: 0rem">
							二、晶片金融卡開卡完成後，可直接搭配讀卡機進行密碼變更，若無讀卡機，可至本行實體ATM插入晶片金融卡進行密碼變更。
						</li>
					</ol>
					
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	
</body>

</html>