<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	
	//若前頁有帶acn，預設為此acn
	var getacn = '${reqACN}';
	if(getacn != null && getacn != ''){
		$("#ACN option[value= '"+ getacn +"' ]").prop("selected", true);
		displayGold();
	}
	
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
});
function displayGold(){
	if($("#ACN").val() != "#"){
		var URI = "${__ctx}/GOLD/TRANSACTION/getGoldTradeTWAccountListAjax2";
		var rqData = {ACN:$("#ACN").val()};
		fstop.getServerDataEx(URI,rqData,false,getGoldTradeTWAccountListAjax2Finish);
	}
}
function getGoldTradeTWAccountListAjax2Finish(data){
	if(data.result == true){
		var goldData = $.parseJSON(data.data);
		
		$("#TSFBAL").val(goldData.TSFBAL);
		$("#GDBAL").val(goldData.GDBAL);
		$("#SVACN").val(goldData.SVACN);
		$("#FEEAMT1").val(goldData.FEEAMT1);
		$("#FEEAMT2").val(goldData.FEEAMT2);
		
		$("#formID").submit();
	}
	else{
		//無法取得黃金存摺帳號資料
		//alert("<spring:message code= "LB.X0942" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X0942' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}
</script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!--header-->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 黃金回售     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1512" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> 
			<!--主頁內容-->
			<section id="main-content" class="container">
			<!-- 預約黃金回售 -->
				<h2><spring:message code="LB.X0952"/></h2><i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00"></i>
                <form id="formID" action="${__ctx}/GOLD/TRANSACTION/gold_reserve_resale_select2" method="post">
                	<input type="hidden" name="ADOPID" value="N09102"/>
					<input type="hidden" id="GDBAL" name="GDBAL" value="0"/>
					<input type="hidden" id="TSFBAL" name="TSFBAL" value="0"/>							
					<input type="hidden" id="SVACN" name="SVACN"/>
					<input type="hidden" id="FEEAMT1" name="FEEAMT1"/>
					<input type="hidden" id="FEEAMT2" name="FEEAMT2"/>
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
								<!--黃金轉出帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 黃金轉出帳號 -->
											<h4><spring:message code= "LB.W1515" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input" name="ACN" id="ACN" onchange="displayGold()">
<!-- 請選擇帳號 -->
												<option value="#">---<spring:message code= "LB.W0257" />---</option>
												<c:forEach var="dataMap" items="${goldTradeAccountList}">
													<option value="${dataMap.ACN}">${dataMap.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
							</div>
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>