<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);

	$("#CMSUBMIT").click(function(){
		if(!CheckPuzzle("CMPWD")){
			return false;
		}
		$("#CMPASSWORD").val($("#CMPWD").val());
		
		var PINNEW = pin_encrypt($("#CMPASSWORD").val());
		$("#PINNEW").val(PINNEW);
		
		$("#formID").attr("action","${__ctx}/FUND/REGULAR/fund_regular_result");
		$("#formID").submit();
	});
	$("#resetButton").click(function(){
		$("#CMPWD").val("");
	});
	$("#cancelButton").click(function(){
		$("#formID").attr("action","${__ctx}/FUND/REGULAR/fund_regular_select");
		$("#formID").submit();
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Funds" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 定期投資申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1083" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1083" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
				<form id="formID" method="post">
					<input type="hidden" name="ADOPID" value="C017"/>
					<input type="hidden" id="CMPASSWORD" name="CMPASSWORD"/>
					<input type="hidden" id="PINNEW" name="PINNEW"/>
					<input type="hidden" name="RISK7" value="${RISK7}"/>
					<input type="hidden" name="FDINVTYPE" value="${FDINVTYPE}"/>
					<input type="hidden" name="FCA1" value="${FCA1}"/>
					<input type="hidden" name="FUNCUR" value="${FUNCUR}"/>
					<input type="hidden" name="SHORTTRADE" value="${SHORTTRADE}"/>
					<input type="hidden" name="SHORTTUNIT" value="${SHORTTUNIT}"/>
					<input type="hidden" name="FDAGREEFLAG" value="${FDAGREEFLAG}"/>
					<input type="hidden" name="FDNOTICETYPE" value="${FDNOTICETYPE}"/>
					<input type="hidden" name="FDPUBLICTYPE" value="${FDPUBLICTYPE}"/>
					<input type="hidden" name="TYPE" value="${TYPE}"/>
					<input type="hidden" name="TRANSCODE" value="${TRANSCODE}"/>
					<input type="hidden" name="COUNTRYTYPE" value="${COUNTRYTYPE}"/>
					<input type="hidden" name="COUNTRYTYPE1" value="${COUNTRYTYPE1}"/>
					<input type="hidden" name="COMPANYCODE" value="${COMPANYCODE}"/>
					<input type="hidden" name="PAYTYPE" value="${PAYTYPE}"/>
					<input type="hidden" name="FUNDACN" value="${FUNDACN}"/>
					<input type="hidden" name="INVTYPE" value="${INVTYPE}"/>
					<input type="hidden" name="OTELPHONE" value="${OTELPHONE}"/>
					<input type="hidden" name="TRADEDATE" value="${TRADEDATE}"/>
					<input type="hidden" name="AMT3" value="${AMT3}"/>
					<input type="hidden" name="FCA2" value="${FCA2}"/>
					<input type="hidden" name="AMT5" value="${AMT5}"/>
					<input type="hidden" name="OUTACN" value="${OUTACN}"/>
					<input type="hidden" name="INTSACN" value="${INTSACN}"/>
					<input type="hidden" name="BILLSENDMODE" value="${BILLSENDMODE}"/>
					<input type="hidden" name="FCAFEE" value="${FCAFEE}"/>
					<input type="hidden" name="SSLTXNO" value="${SSLTXNO}"/>
					<input type="hidden" name="PAYDAY1" value="${PAYDAY1}"/>
					<input type="hidden" name="PAYDAY2" value="${PAYDAY2}"/>
					<input type="hidden" name="PAYDAY3" value="${PAYDAY3}"/>
					<input type="hidden" name="BRHCOD" value="${BRHCOD}"/>
					<input type="hidden" name="CUTTYPE" value="${CUTTYPE}"/>
					<input type="hidden" name="CRY1" value="${CRY1}"/>
					<input type="hidden" name="HTELPHONE" value="${HTELPHONE}"/>
					<input type="hidden" name="DBDATE" value="${DBDATE}"/>
					<input type="hidden" name="SALESNO" value="${SALESNO}"/>
					<input type="hidden" name="STOP" value="${STOP}"/>
					<input type="hidden" name="YIELD" value="${YIELD}"/>
					<input type="hidden" name="PAYDAY4" value="${PAYDAY4}"/>
					<input type="hidden" name="PAYDAY5" value="${PAYDAY5}"/>
					<input type="hidden" name="PAYDAY6" value="${PAYDAY6}"/>
					<input type="hidden" name="MIP" value="${MIP}"/>
					<input type="hidden" name="PRO" value="${PRO}"/>
					<input type="hidden" name="RSKATT" value="${RSKATT}"/>
					<input type="hidden" name="RRSK" value="${RRSK}"/>
					<input type="hidden" name="PAYDAY7" value="${PAYDAY7}"/>
					<input type="hidden" name="PAYDAY8" value="${PAYDAY8}"/>
					<input type="hidden" name="PAYDAY9" value="${PAYDAY9}"/>
					<input type="hidden" name="FDINVTYPEChinese" value="${FDINVTYPEChinese}"/>
					<input type="hidden" name="INVTYPEChinese" value="${INVTYPEChinese}"/>
					<input type="hidden" name="TYPEChinese" value="${TYPEChinese}"/>
					<input type="hidden" name="displayTYPE" value="${displayTYPE}"/>
					<input type="hidden" name="FUNDLNAME" value="${FUNDLNAME}"/>
					<input type="hidden" name="RISK" value="${RISK}"/>
					<input type="hidden" name="SHWD" value="${SHWD}"/>
                    <input type="hidden" name="XFLAG" value="${XFLAG}"/>
                    <input type="hidden" name="NUM" value="${NUM}"/>
                    <input type="hidden" name="SAL01" value="${SAL01}"/>
                    <input type="hidden" name="SAL03" value="${SAL03}"/>
                    <input type="hidden" name="SLSNO" value="${SLSNO}"/>
                    <input type="hidden" name="KYCNO" value="${KYCNO}"/>
                    
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 客戶投資屬性 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1067" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${FDINVTYPEChinese}</span>
										</div>
									</span>
								</div>
								<!-- 投資方式 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W0946" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${INVTYPEChinese}</span>
										</div>
									</span>
								</div>
								<!-- 扣款幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.D1622" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${TYPEChinese}</span>
										</div>
									</span>
								</div>
								<!-- Id_no -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.Id_no"/>：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${hiddenCUSIDN}</span>
										</div>
									</span>
								</div>
								<!-- Name -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.Name"/>：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${hiddenNAME}</span>
										</div>
									</span>
								</div>
								<!-- 基金名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W0025" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>（${TRANSCODE}）${FUNDLNAME}</span>
										</div>
									</span>
								</div>
								<!-- 商品風險等級-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1073" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${RISK}</span>
										</div>
									</span>
								</div>
								<!-- 申購金額-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1074" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${ADCCYNAME}&nbsp;${AMT3Format}</span>
										</div>
									</span>
								</div>
								<!-- 手續費率-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1034" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${FCAFEEFormat}％ 
												<c:if test="${SAL01 > 0 && SAL03 == 'N'}">
													<font color="red">（<spring:message code="LB.X2606_1" />${SAL01Format}<spring:message code="LB.X2606_2" />）</font>
												</c:if>
											</span>
										</div>
									</span>
								</div>
								<!-- 手續費-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.D0507" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${ADCCYNAME}&nbsp;${FCA2Format}</span>
											<font color="red">（<spring:message code="LB.X0405" />）</font>
										</div>
									</span>
								</div>
								<!-- 每次扣款金額-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.X0406" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${ADCCYNAME}&nbsp;${AMT5Format}</span>
										</div>
									</span>
								</div>
								<!-- 扣款帳號／卡號-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1046" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<c:if test="${PAYTYPE == '3'}">
													${hiddenFUNDACN}
												</c:if>
												<c:if test="${PAYTYPE == '1' || PAYTYPE == '2'}">
													${HTELPHONE}
												</c:if>
												<c:if test="${SAL01 > 0}">
													<font color="red">（<spring:message code="LB.X2607" />）</font>
												</c:if>
											</span>
										</div>
									</span>
								</div>
								<!-- 申請日期-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.D0127" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${TRADEDATEFormat}</span>
										</div>
									</span>
								</div>
								<!-- 每月扣款日-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1592" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>
											<c:if test="${PAYTYPE == '3'}">
												<c:if test="${PAYDAY4 != '00'}">
													${PAYDAY4}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY1 != '00'}">
													${PAYDAY1}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY5 != '00'}">
													${PAYDAY5}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY2 != '00'}">
													${PAYDAY2}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY6 != '00'}">
													${PAYDAY6}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY3 != '00'}">
													${PAYDAY3}<spring:message code="LB.Day" />&nbsp;
												</c:if>
											</c:if>
											<c:if test="${PAYTYPE == '1' || PAYTYPE == '2'}">
												<c:if test="${PAYDAY1 != '00'}">
													${PAYDAY1}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY2 != '00'}">
													${PAYDAY2}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY3 != '00'}">
													${PAYDAY3}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY4 != '00'}">
													${PAYDAY4}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY5 != '00'}">
													${PAYDAY5}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY6 != '00'}">
													${PAYDAY6}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY7 != '00'}">
													${PAYDAY7}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY8 != '00'}">
													${PAYDAY8}<spring:message code="LB.Day" />&nbsp;
												</c:if>
												<c:if test="${PAYDAY9 != '00'}">
													${PAYDAY9}<spring:message code="LB.Day" />&nbsp;
												</c:if>
											</c:if>
											</span>
										</div>
									</span>
								</div>
								<!-- 首次扣款日-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1108" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${DBDATEFormat}</span>
										</div>
									</span>
								</div>
								<!--停利通知設定-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1075" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${YIELDInteger}％</span>
										</div>
									</span>
								</div>
								<!--停損通知設定-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1076" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>－${STOPInteger}％</span>
										</div>
									</span>
								</div>
								<!--交易機制-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.Transaction_security_mechanism" />：</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
											<spring:message code="LB.SSL_password" />
											<input type="radio" id="CMSSL" name="FGTXWAY" value="0" checked />
											<span class="ttb-radio"></span>
											</label>
											<input type="password" name="CMPWD" id="CMPWD" class="text-input "  maxlength="8">
										</div>
									</span>
								</div>
								<!--本筆符合薪轉戶優惠，交易成立後，不可變更扣款日期及金額。-->
								<c:if test="${SAL01 > 0 && SAL03 == 'N'}">
									<div class="row" style="justify-content: space-around">
										<span> 
											<font color="red"><spring:message code="LB.X2603" /></font>
										</span> 
									</div>
								</c:if>
							</div>
							<input type="button" id="resetButton" value="<spring:message code="LB.Re_enter"/>" class="ttb-button btn-flat-gray"/>
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>
				</form>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><span><spring:message code="LB.Fund_Regular_Comfirm_P2_D1" /></span></li>
					<li><span><spring:message code="LB.Fund_Regular_Comfirm_P2_D2" />：</span></li>
					
					<ul class="description-list">
						<li>(1)&nbsp;<spring:message code="LB.Fund_Regular_Comfirm_P2_D3-1" /></li>
						<ul class="description-list">
							<li>&nbsp;&nbsp;「<spring:message code="LB.Fund_Regular_Comfirm_P2_D3-2" />」<spring:message code="LB.Fund_Regular_Comfirm_P2_D3-3" />→「<spring:message code="LB.Fund_Regular_Comfirm_P2_D3-4" />」→「<spring:message code="LB.Fund_Regular_Comfirm_P2_D3-5" />」<br/></li>
						</ul>
						<li>(2)&nbsp;<spring:message code="LB.Fund_Regular_Comfirm_P2_D4-1" />（https://www.tbb.com.tw/）</li>
						<ul class="description-list">
							<li>&nbsp;&nbsp;「<spring:message code="LB.Fund_Regular_Comfirm_P2_D4-2" />」→「<spring:message code="LB.Fund_Regular_Comfirm_P2_D4-3" />」→「<spring:message code="LB.Fund_Regular_Comfirm_P2_D4-4" />」</li>
						</ul>
					</ul>
				</ol>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>