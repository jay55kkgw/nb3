<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			//上一頁按鈕
			back();

		}
		//上一頁按鈕
		function back(){
			$("#CMBACK").click(function() {
				var action = '${__ctx}/FCY/REMITTANCES/f_outward_remittances_step1';
				$('#back').val("Y");
				$("form").attr("action", action);
				initBlockUI();
				$("form").submit();
			});
		}
		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").submit();
				}
			});
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
	<!-- 匯出匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0286" /></li>
    <!-- 匯出匯款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0286" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--外匯匯出匯款 -->
				<h2>
					<spring:message code="LB.W0286" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId"
					action="${__ctx}/FCY/REMITTANCES/f_outward_remittances_step3">
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<!-- 請確認匯出匯款資料 -->
										<spring:message code="LB.X0039" />
									</span>
								</div>
								<!-- 付款日期 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0289" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${f_outward_remittances_step2.data.PAYDATE}</p>
										</div>
									</span>
								</div>
								<!-- 付款帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0290" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${f_outward_remittances_step2.data.CUSTACC}</p>
										</div>
									</span>
								</div>
								<!-- 付款幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D0448" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${f_outward_remittances_step2.data.display_PAYCCY}</p>
										</div>
									</span>
								</div>
								<!-- 收款帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0292" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${f_outward_remittances_step2.data.BENACC}</p>
										</div>
									</span>
								</div>
								<!-- 收款幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0293" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.display_REMITCY}
											</p>
										</div>
									</span>
								</div>
								<!-- 匯款金額 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0150" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<p class="high-light d-inline-block">
												${f_outward_remittances_step2.data.display_PAYREMIT}
											</p>
										</div>
									</span>
								</div>
								<!-- 收款人身份別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payee_identity" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.display_BENTYPE}
											</p>
										</div>
									</span>
								</div>
								<!-- 匯款分類項目 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0298" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.SRCFUNDDESC}
											</p>
										</div>
									</span>
								</div>
								<!-- 匯款分類說明 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0299" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.FXRMTDESC}
											</p>
										</div>
									</span>
								</div>
								<!-- 匯款附言 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0303" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.MEMO1}
											</p>
										</div>
									</span>
								</div>
								<!-- 手續費負擔別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0155" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.DETCHG}
											</p>
										</div>
									</span>
								</div>
								<!-- 費用扣款帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0305" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.COMMACC}
											</p>
										</div>
									</span>
								</div>
								<!-- 費用扣款幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0306" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.display_COMMCCY}
											</p>
										</div>
									</span>
								</div>
								<!-- 收款銀行資料 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0037" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												${f_outward_remittances_step2.data.SWIFTCODE}
											</span>
										</div>
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.FXRCVBKCODE}
											</p>
										</div>
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.W0295" /></span>
										</div>
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.FXRCVBKADDR}
											</p>
										</div>
									</span>
								</div>
								<!-- 收款人資料 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0294" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.W0295" /></span>
										</div>
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.FXRCVADDR}
											</p>
										</div>
									</span>
								</div>
								<!-- Email通知 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0300" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Email" /></span>
										</div>
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.CMTRMAIL}
											</p>
										</div>
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Summary" /></span>
										</div>
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2.data.CMMAILMEMO}
											</p>
										</div>
									</span>
								</div>
							</div>
							<!--button 區域 -->
							<input class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" type="button" value="<spring:message code="LB.Back_to_previous_page" />" />
							<!--取得匯率/議價 -->
							<input class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" type="button" value="<spring:message code="LB.Obtain_exchange_rate_and_bargaining_number" />" />
							<!--button 區域 -->
						</div>
					</div>
					<font color="#FF0000"><spring:message code="LB.Confirm_exchange_rate" />
					</font>
					<div class="text-left">
						<!-- 						說明： -->
						<!-- <spring:message code="LB.Description_of_page"/>: -->
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>