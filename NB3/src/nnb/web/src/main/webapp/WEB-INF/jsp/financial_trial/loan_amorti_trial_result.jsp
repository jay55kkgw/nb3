<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<meta http-equiv="Content-Language" content="zh-tw">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript">
$(document).ready(function() {
	init();
	setTimeout("initDataTable()",100);
	//initFootable();
});

function init() {
	//重新計算按鈕
	$("#btnBack").click(function() {
		var action = '${__ctx}/FINANCIAL/TRIAL/loan_amorti_trial';
		$("form").attr("action", action);
		initBlockUI();
		$("form").submit();
	});
}
</script>
</head>
 <body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 理財試算服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0087" /></li>
    <!-- 貸款攤還試算     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0102" /></li>
		</ol>
	</nav>


	<!-- 快速選單及主頁內容 -->
	<!-- menu、登出窗格 --> 
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">	
			<!--主頁內容  -->
		<section id="main-content" class="container">
			<!--<h2><spring:message code="LB.Change_User_Name" /></h2>--> 
			<h2><spring:message code="LB.X0102" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form  id="formId" >
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
						        <!-- 攤還方式-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<h4><spring:message code="LB.X0103" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
									<input type="text"  name = "payment"  id = "payment"   value="${payment}"  maxlength="18"  size="20"  value="0"  class="text-input"  disabled>
									</div>
								</span>
							</div>
							<!-- 貸款金額-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.D0660" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "loan"  id = "loan"  value="${loan}"  maxlength="18"  size="20"  value="0"  class="text-input"  disabled>
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
									</div>
								</span>
							</div>
							<!-- 寬限期(月)-->
							<div class="ttb-input-item row"  id="principalGrace_Field"  >
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0107" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "principalGrace"  id = "principalGrace"  value="${principalGrace}"  maxlength="18"  size="20"  value="0"  class="text-input"  disabled>
										<span class="input-unit"><spring:message code="LB.Month" /></span>
									</div>
								</span>
							</div>
							<!-- 貸款總期數-->
							<div class="ttb-input-item row"  >
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0116" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "period"  id = "period"  value="${totalPeriod}"   maxlength="18"  size="20"  value="0"  class="text-input"  disabled>
										<span class="input-unit"><spring:message code="LB.Months" /></span>
									</div>
								</span>
							</div>
							<!-- 貸款利率 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
									 <h4><spring:message code="LB.X0108" /></h4>
								</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span><spring:message code="LB.X0109" /></span>
									</div>
									<div class="ttb-input">
										<input name="yrate1" id="yrate1" type="text" value="${yrate1}" size="6" maxlength="6" class="text-input input-width-85" disabled>
										<span class="input-unit margin-4">%</span>
										<input name="mperiod1" id="mperiod1" type="text" value="${mperiod1}" size="10" maxlength="6" class="text-input input-width-115" disabled>
										<span class="input-unit"><spring:message code="LB.Months" /></span>
									</div>
									<div class="ttb-input">
										<span><spring:message code="LB.X0111" /></span>
									</div>
									<div class="ttb-input">
										<input name="yrate2" id="yrate2" type="text" value="${yrate2}" size="6" maxlength="6" class="text-input input-width-85" disabled>
										<span class="input-unit margin-4">%</span>
										<input name="mperiod2" id="mperiod2" type="text"  value="${mperiod2}" size="10" maxlength="6" class="text-input input-width-115" disabled>
										<span class="input-unit"><spring:message code="LB.Months" /></span>
									</div>
									<div class="ttb-input">
										<span><spring:message code="LB.X0112" /></span>
									</div>
									<div class="ttb-input">
										<input name="yrate3" id="yrate3" type="text" value="${yrate3}" size="6" maxlength="6" class="text-input input-width-85" disabled>
										<span class="input-unit margin-4">%</span>
										<input name="mperiod3" id="mperiod3" type="text" value="${mperiod3}" size="10" maxlength="6" class="text-input input-width-115" disabled>
										<span class="input-unit"><spring:message code="LB.Months" /></span>
									</div>
								</span>	
							</div>
							<!--各項相關費用總金額-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0113" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "otherfee"  id = "otherfee"  value="${otherfee}"   size="20"  class="text-input"  disabled>
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
									</div>
								</span>
							</div>
							<!--總利息-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0117" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "totalInterest"  id = "totalInterest"   value="${totalInterest}"   size="20"  class="text-input"  disabled>
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
									</div>
								</span>
							</div>
							<!--總費用年百分率-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0118" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "totalCostRate"  id = "totalCostRate"   value="${totalCostRate}"    size="20"  class="text-input"  disabled>
										<span class="input-unit">%</span>
									</div>
								</span>
							</div>
							<table class="stripe table-striped ttb-table dtable"  data-toggle-column="first">
								<thead>
									<tr>
										<!--期別-->
										<th  data-title="<spring:message code="LB.X0119" />" data-breakpoints="xs sm" class="text-center"><spring:message code="LB.X0119" /></th>
										<!-- 償還本金-->
										<th class="text-center" data-title="<spring:message code="LB.X0120" />"><spring:message code="LB.X0120" /></th>
										<!--償還利息-->
										<th class="text-center" data-title="<spring:message code="LB.X0121" />"><spring:message code="LB.X0121" /></th>
										<!-- 償還本利和-->
										<th class="text-center" data-title="<spring:message code="LB.X0122" />"><spring:message code="LB.X0122" /></th>
										<!-- 貸款餘額-->
										<th class="text-center" data-title="<spring:message code="LB.X0123" />"><spring:message code="LB.X0123" /></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${periods}"  var="period"  varStatus="vs"  begin="1"  end="${forTotalPeriod}"  disabled>
										<tr>
										  	<td class="text-center"><fmt:parseNumber integerOnly="true"  value="${period}" /></td>
											<td class="text-right"><fmt:parseNumber integerOnly="true"  value="${principal[vs.count]}" /></td>
											<td class="text-right"><fmt:parseNumber integerOnly="true"  value="${interest[vs.count]}" /></td>
											<td class="text-right"><fmt:parseNumber integerOnly="true"  value="${cashFlow[vs.count]}" /></td>
											<td class="text-right"><fmt:parseNumber integerOnly="true"  value="${loanBalance[vs.count]}" /></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<input id="btnBack" name="btnBack"  type="button" class="ttb-button btn-flat-orange"  value="<spring:message code="LB.X0124" />" />
					</div>
				</div>
				</form>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>
	<!-- content row END -->
    <%@ include file="../index/footer.jsp"%>
</body>
</html>
