<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%-- <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script> --%>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
	<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	<label><spring:message code="LB.Inquiry_time" />：</label><label>${CMQTIME}</label>
	<br/><br/>
	<label><spring:message code="LB.Total_records" />：</label><label>${COUNT} <spring:message code="LB.Rows" /></label>
	<br/><br/>
	<table class="print">
		<tr>
			<td style="text-align:center"><spring:message code="LB.Account" /></td>
			<td style="text-align:center"><spring:message code="LB.Deposit_type" /></td>
			<td style="text-align:center"><spring:message code="LB.Available_balance" /></td>
			<td style="text-align:center"><spring:message code="LB.Account_balance" /></td>
			<td style="text-align:center"><spring:message code="LB.Exchange_ticket_today" /></td>
		</tr>
		<c:forEach items="${dataListMap}" var="map">
		<tr>
			<td style="text-align: center">${map.ACN}</td>		
			<td style="text-align: center">${map.KIND }</td>
			<td style="text-align: right">${map.ADPIBAL }</td>
			<td style="text-align: right">${map.BDPIBAL }</td>		
			<td style="text-align: right">${map.CLR}</td>
		</tr>
		</c:forEach>
	</table>
	<br/><br/>
		<div class="text-left">
			<spring:message code="LB.Description_of_page" />
			<ol class="list-decimal text-left">
				<li><spring:message code="LB.Balance_query_P1_D1" /></li>
				<li><spring:message code="LB.Balance_query_P1_D2" /></li>
			</ol>
		</div>
	</body>
</html>