<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
			

			<table class="print">
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1105" /></td>
					<td>${NAME}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.X1589" /></td>
					<td>${CUSNAME}</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D0519" /></td>
					<td>${CUSIDN}</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.X0205" /> </td>
					<td>${COUNTRY}</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D0582" /></td>
					<td><spring:message code= "LB.D0583" />&nbsp;${BIRTHDAY.substring(0,3)}<spring:message code= "LB.Year" />&nbsp;${BIRTHDAY.substring(3,5)}<spring:message code= "LB.Month" />&nbsp;${BIRTHDAY.substring(5,7)}<spring:message code= "LB.Day" />&nbsp;</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1069" /></td>
					<td>
						<c:if test="${ ID_CHGE.equals('1') }">
							<spring:message code= "LB.B0002" />
						</c:if>
						<c:if test="${ ID_CHGE.equals('2') }">
							<spring:message code= "LB.W1664" />
						</c:if>
						<c:if test="${ ID_CHGE.equals('3') }">
							<spring:message code= "LB.W1665" />
						</c:if>
						${CHGE_DT}
					</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1071" /></td>
					<td>${CHGE_CYNAME}</td>
				</tr>
				<tr>
					<c:set var="i18n_D1070_1"><spring:message code= "LB.D1070_1" /></c:set>
					<c:set var="i18n_D1070_2"><spring:message code= "LB.D1070_2" /></c:set>
					<td style="text-align: center">★<spring:message code= "LB.D1115" /></td>
					<td>${HAS_PIC.equals('Y') ? i18n_D1070_1 : i18n_D1070_2}</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D0143" /></td>
					<td>${POSTCOD1} ${PMTADR}</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D0376" /></td>
					<td>${POSTCOD2} ${CTTADR}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.B0020" /></td>
					<td>${HOMEZIP}-${HOMETEL}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0094" /></td>
					<td>${BUSZIP}-${BUSTEL}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0069" /></td>
					<td>${CELPHONE}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D1131" /></td>
					<td>${ARACOD3}-${FAX}</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1132" /></td>
					<td>
						${CAREER1_str}
						&nbsp;&nbsp;★<spring:message code= "LB.D0087" />：
						<c:if test="${ CAREER2.equals('1') }">
							<spring:message code= "LB.X0179" />
						</c:if>
						<c:if test="${ CAREER2.equals('2') }">
							<spring:message code= "LB.X0180" />
						</c:if>
						<c:if test="${ CAREER2.equals('3') }">
							<spring:message code= "LB.X0181" />
						</c:if>
						<c:if test="${ CAREER2.equals('4') }">
							<spring:message code= "LB.X0182" />
						</c:if>
						<c:if test="${ CAREER2.equals('5') }">
							<spring:message code= "LB.X0183" />
						</c:if>
						<c:if test="${ CAREER2.equals('6') }">
							<spring:message code= "LB.X0184" />
						</c:if>
						<c:if test="${ CAREER2.equals('7') }">
							<spring:message code= "LB.X0185" />
						</c:if>
						<c:if test="${ CAREER2.equals('8') }">
							<spring:message code= "LB.X0186" />
						</c:if>
						<c:if test="${ CAREER2.equals('9') }">
							<spring:message code= "LB.X0187" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1074" /></td>
					<td>${EMPLOYER}</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1135" /></td>
					<td>${BRHCOD} ${BRHNAME}</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1075" /></td>
					<td>
						<c:if test="${ PURPOSE.equals('A') }">
							<spring:message code= "LB.D1076" />
						</c:if>
						<c:if test="${ PURPOSE.equals('B') }">
							<spring:message code= "LB.D1077" />
						</c:if>
						<c:if test="${ PURPOSE.equals('C') }">
							<spring:message code= "LB.D1078" />
						</c:if>
						<c:if test="${ PURPOSE.equals('D') }">
							<spring:message code= "LB.D1079" />
						</c:if>
						<c:if test="${ PURPOSE.equals('E') }">
							<spring:message code= "LB.D0572" />：${PREASON}
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1391" /></td>
					<td>${BDEALING_str}</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1397" /></td>
					<td>
						<c:if test="${ MAINFUND.equals('A') }">
							<spring:message code= "LB.D1398" />
						</c:if>
						<c:if test="${ MAINFUND.equals('B') }">
							<spring:message code= "LB.D1076" />
						</c:if>
						<c:if test="${ MAINFUND.equals('C') }">
							<spring:message code= "LB.D1399" />
						</c:if>
						<c:if test="${ MAINFUND.equals('D') }">
							<spring:message code= "LB.D1079" />
						</c:if>
						<c:if test="${ MAINFUND.equals('E') }">
							<spring:message code= "LB.D1400" />
						</c:if>
						<c:if test="${ MAINFUND.equals('F') }">
							<spring:message code= "LB.D1401" />
						</c:if>
						<c:if test="${ MAINFUND.equals('G') }">
							<spring:message code= "LB.D0910" />
						</c:if>
						<c:if test="${ MAINFUND.equals('H') }">
							<spring:message code= "LB.D0572" />：${MREASON}
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center">★E-mail</td>
					<td>${MAILADDR}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D1136" /></td>
					<td>
						<c:if test="${ DEGREE.equals('1') }">
							<spring:message code= "LB.D0588" />
						</c:if>
						<c:if test="${ DEGREE.equals('2') }">
							<spring:message code= "LB.D0589" />
						</c:if>
						<c:if test="${ DEGREE.equals('3') }">
							<spring:message code= "LB.D0590" />
						</c:if>
						<c:if test="${ DEGREE.equals('4') }">
							<spring:message code= "LB.D0591" />
						</c:if>
						<c:if test="${ DEGREE.equals('5') }">
							<spring:message code= "LB.D1141" />
						</c:if>
						<c:if test="${ DEGREE.equals('6') }">
							<spring:message code= "LB.D0866" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.X0170" /><br><spring:message code= "LB.D0625_1" /></td>
					<td>${SALARY}<spring:message code= "LB.D1144" /></td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0057" /></td>
					<td>
						<c:if test="${ MARRY.equals('0') }">
							<spring:message code= "LB.D0596" />
						</c:if>
						<c:if test="${ MARRY.equals('1') }">
							<spring:message code= "LB.D0595" />
						</c:if>
						<c:if test="${ MARRY.equals('2') }">
							<spring:message code= "LB.D0572" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D1149" /></td>
					<td>${CHILD}</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1081_1" /></td>
					<td>${S_MONEY}</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D0111" /></td>
					<td>
						<c:if test="${ !FILE1.equals('') }">
							<img id="msgPic1" src="${__ctx}/upload/${FILE1}">
						</c:if>
						<c:if test="${ FILE1.equals('') }">
							<spring:message code= "LB.X0175" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D0118" /></td>
					<td>
						<c:if test="${ !FILE2.equals('') }">
							<img id="msgPic1" src="${__ctx}/upload/${FILE2}">
						</c:if>
						<c:if test="${ FILE2.equals('') }">
							<spring:message code= "LB.X0175" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1159" /></td>
					<td>
						<c:if test="${ !FILE3.equals('') }">
							<img id="msgPic1" src="${__ctx}/upload/${FILE3}">
						</c:if>
						<c:if test="${ FILE3.equals('') }">
							<spring:message code= "LB.X0175" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center">★<spring:message code= "LB.D1160" /></td>
					<td>
						<c:if test="${ !FILE4.equals('') }">
							<img id="msgPic1" src="${__ctx}/upload/${FILE4}">
						</c:if>
						<c:if test="${ FILE4.equals('') }">
							<spring:message code= "LB.X0175" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center" colspan="2">★<spring:message code= "LB.D1439" /></td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.Loginpage_User_name" /></td>
					<td>${USERNAME}<br>(<spring:message code= "LB.D1184" />)</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.login_password" /></td>
					<td>${LOGINPIN}<br>(<spring:message code= "LB.D1184" />)</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.SSL_password_1" /></td>
					<td>${TRANSPIN}<br>(<spring:message code= "LB.D1184" />)</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D1425" />：</td>
					<td>${TRFLAG.equals('Y') ? i18n_D1070_1 : i18n_D1070_2}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0279" />：</td>
					<td>${EBILLFLAG.equals('Y') ? i18n_D1070_1 : i18n_D1070_2}</td>
				</tr>
				<tr>
					<td style="text-align: center" colspan="2">★<spring:message code= "LB.D1447" /></td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D1431_3" />：</td>
					<td>
						<c:if test="${ CARDAPPLY.equals('Y') }">
							<c:if test="${ SNDFLG.equals('1') }">
								<spring:message code= "LB.B0007" />
							</c:if>
							<c:if test="${ SNDFLG.equals('3') }">
								<spring:message code= "LB.B0006" />
							</c:if>
							<c:if test="${ SNDFLG.equals('2') }">
								<spring:message code= "LB.B0008" />
							</c:if>，<spring:message code= "LB.Card_kind" />：
							<c:if test="${ CRDTYP.equals('1') }">
								<spring:message code= "LB.Financial_debit_card" />
							</c:if>
							<c:if test="${ CRDTYP.equals('2') }">
								<spring:message code= "LB.D1432" />
							</c:if>
						</c:if>
						<c:if test="${ CARDAPPLY.equals('N') }">
							<spring:message code= "LB.D0034_3" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D1437" />：</td>
					<td>${NAATAPPLY.equals('Y') ? i18n_D1070_1 : i18n_D1070_2}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.X0214" />：</td>
					<td>
						<c:if test="${ CChargeApply.equals('Y') }">
							<spring:message code= "LB.D0034_2" />(<spring:message code= "LB.X0215" />：${LMT} <spring:message code= "LB.X0423" />)
						</c:if>
						<c:if test="${ CChargeApply.equals('N') }">
							<spring:message code= "LB.D0034_3" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D1438" />：</td>
					<td>${CROSSAPPLY.equals('Y') ? i18n_D1070_1 : i18n_D1070_2}</td>
				</tr>
				
			</table>
		</div>
		
		<br>
		<br>
<!-- 		<div class="text-left"> -->
<%-- 			<spring:message code="LB.Description_of_page" />: --%>
<!-- 			<ol class="list-decimal text-left"> -->
<!-- 	            <li>本項代扣繳申請，自本行同意接受委託，並將代繳檔案資料送至電力公司審核，經電力公司電腦處理並按繳款日遞送扣繳資料起履行代繳義務；在本行未收到扣繳資料前各月份之電費，仍請自行繳納。</li> -->
<!--                 <li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
<!--                 <li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
<!-- 			</ol> -->
<!-- 		</div> -->
	</div>
	
</body>
</html>