<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript">
		$(document).ready(function () {
			//列印
			$("#printbtn").click(function () {
				var params = {
					"jspTemplateName": "f_renewal_apply_result_print",
					"jspTitle": '<spring:message code="LB.FX_Time_Deposit_Automatic_Rollover_Application_Change"/>',
					"MsgName": '${f_renewal_apply_result.data.MsgName}',
					"CMQTIME": '${f_renewal_apply_result.data.CMQTIME}',
					"FYACN": '${f_renewal_apply_result.data.FYACN}',
					"FDPNUM": '${f_renewal_apply_result.data.FDPNUM}',
					"CRY": '${f_renewal_apply_result.data.CRY}',
					"AMTFDP": '${f_renewal_apply_result.data.AMTFDP}',
					"DEPTYPE": '${f_renewal_apply_result.data.DEPTYPE}',
					"INTMTH": '${f_renewal_apply_result.data.INTMTH}',
					"AUTXFTM": '${f_renewal_apply_result.data.AUTXFTM}',
					"CODE": '${f_renewal_apply_result.data.CODE}',
					"FYTSFAN": '${f_renewal_apply_result.data.FYTSFAN}',
				};
				openWindowWithPost("${__ctx}/print", "height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1", params);
			});
		});
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %>
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存自動轉期申請/變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Time_Deposit_Automatic_Rollover_Application_Change" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--  外匯定存自動轉期申請/變更 -->
					<spring:message code="LB.FX_Time_Deposit_Automatic_Rollover_Application_Change" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="" onSubmit="return processQuery()">
					<c:set var="BaseResultData" value="${f_renewal_apply_result.data}"></c:set>
					<!-- 交易步驟 -->
					<div id="step-bar">
						<ul>
							<!-- 輸入資料 -->
							<li class="finished">
								<spring:message code="LB.Enter_data" />
							</li>
							<!-- 確認資料 -->
							<li class="finished">
								<spring:message code="LB.Confirm_data" />
							</li>
							<!-- 交易完成 -->
							<li class="active">
								<spring:message code="LB.Transaction_complete" />
							</li>
						</ul>
					</div>
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<!-- 變更成功 -->
									<span>${BaseResultData.MsgName}</span>
								</div>
								<!--交易時間 -->

								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Trading_time" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.CMQTIME}
											</span>
										</div>
									</span>
								</div>
								<!-- 存單帳號 -->

								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Account_no" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.FYACN}
											</span>
										</div>
									</span>
								</div>
								<!-- 存單號碼 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Certificate_no" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.FDPNUM}
											</span>
										</div>
									</span>
								</div>
								<!-- 存單金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Certificate_amount" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="high-light">
												<!--幣別 -->
												${BaseResultData.CRY}
												<!--金額 -->
												${BaseResultData.display_AMTFDP}
											</span>
										</div>
									</span>
								</div>
								<!-- 存單種類 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Deposit_certificate_type" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.DEPTYPE}
											</span>
										</div>
									</span>
								</div>
								<!-- 計息方式 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Interest_calculation" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.INTMTH}
											</span>
										</div>
									</span>
								</div>
								<!-- 轉期次數 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Number_of_rotations" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.AUTXFTM}
											</span>
										</div>
									</span>
								</div>
								<!--轉存方式 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Rollover_method" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<!--轉存方式&&&利息轉入帳號-->
											<span>
												<!--轉存方式 -->
												${BaseResultData.CODE}
												<!--利息轉入帳號 -->
												${BaseResultData.FYTSFAN}
											</span>
										</div>
									</span>
								</div>
							</div>
							<!-- 						button -->

								<!-- 列印  -->
								<spring:message code="LB.Print" var="printbtn"></spring:message>
								<input type="button" id="printbtn" value="${printbtn}"
									class="ttb-button btn-flat-orange" />

						</div>
					</div>
					<!-- 				<div class="text-left"> -->
					<!-- 					說明： -->
					<!-- 					<ol class="list-decimal text-left"> -->
					<!-- 						<li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
					<!-- 						<li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
					<!-- 					</ol> -->
					<!-- 				</div> -->
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>