<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            //initFootable(); // 將.table變更為footable 
            init();
            setTimeout("initDataTable()",100);
        });

        function init() {
        	//繼續查詢
        	$("#CMCONTINU").click(function (e) {
				e = e || window.event;
				console.log("submit~~");

				$("#formId").validationEngine('detach');
				initBlockUI(); //遮罩
				$("#formId").attr("action", "${__ctx}/PARKING/FEE/parking_withholding_query_result");
				$("#formId").submit();
			});
        	
            //列印 繳費查詢
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"parking_withholding_query_result_print",
					"jspTitle":'<spring:message code= "LB.D0335" />',
					"QueryTerms":"${input_data.QueryTerms}",
					"CarId":"${input_data.CarId}",
					"Account":"${input_data.Account}",
					"CMSEACHPERDATE":"${result_data.data.CMSEACHPERDATE}",
					"TotalRecord":"${result_data.data.TotalRecord}",
					"WaitPayRecord":"${result_data.data.WaitPayRecord}",
					"PaySuccessRecord":"${result_data.data.PaySuccessRecord}",
					"PayFailRecord":"${result_data.data.PayFailRecord}",
					"CMQTIME":"${result_data.data.CMQTIME}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/PARKING/FEE/parking_withholding_query';
    			$('#back').val("Y");
    			$("#formId").attr("action", action);
    			initBlockUI();
    			$("#formId").submit();
    		});
        }
        
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 停車費     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.D0330" /></li>
    <!-- 繳費查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0335" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 商業信用狀明細(未輸入) -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.D0335"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                
                <form id="formId" method="post">
                	<input type="hidden" id="back" name="back" value="">
                    <input type="hidden" name="StartRecordNo" value="${ result_data.data.StartRecordNo }" />
                    <input type="hidden" name="MaxRecordsPerTime" value="20" />
				   	<input  type="hidden" name="CarId" value="${ input_data.CarId }">
				    <input  type="hidden" name="Account" value="${ input_data.Account }">
<%-- 				    <input  type="hidden" name="TxnCode" value="${ result_data.data.TxnCode }"> --%>
				    <input  type="hidden" name="ZoneCode" value="${ input_data.ZoneCode }">
				    <input  type="hidden" name="STARTSERACHDATE" value="${ input_data.STARTSERACHDATE }">
				    <input  type="hidden" name="ENDSERACHDATE" value="${ input_data.ENDSERACHDATE }">
				    <input  type="hidden" name="QueryTerms" value="${ input_data.QueryTerms }">
				    <input  type="hidden" name="QueryTerms_Str" value="${ input_data.QueryTerms_Str }">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                            <ul class="ttb-result-list">
                                <li>
                                    <!-- 查詢時間 -->
                                    <h3><spring:message code="LB.Inquiry_type"/></h3>
                                    <p>
                                        <c:if test="${ input_data.QueryTerms.equals('0') }">
                                        	<spring:message code="LB.D0336"/>
                                        </c:if>
                                        <c:if test="${ input_data.QueryTerms.equals('1') }">
                                        	<spring:message code="LB.D0337"/> － ${ input_data.CarId }
                                        </c:if>
                                        <c:if test="${ input_data.QueryTerms.equals('2') }">
                                        	<spring:message code="LB.D0338"/> － ${ input_data.Account }
                                        </c:if>
                                    </p>
                                </li>
                                <li>
                                    <!-- 查詢期間 -->
                                    <h3><spring:message code="LB.Inquiry_period" /></h3>
                                    <p>
                                        ${result_data.data.CMSEACHPERDATE }
                                    </p>
                                </li>
                                <li>
                                    <!-- 信用狀號碼 : -->
                                    <h3><spring:message code="LB.D0359" /></h3>
                                    <p>
			                            <spring:message code="LB.D0360_1"/> ${result_data.data.TotalRecord } <spring:message code="LB.Rows" />
			                            <spring:message code="LB.D0360_3"/><br>(<spring:message code="LB.D0361"/> ${result_data.data.WaitPayRecord } <spring:message code="LB.Rows" />
										、<spring:message code="LB.D0362"/> ${result_data.data.PaySuccessRecord } <spring:message code="LB.Rows" />
										、<spring:message code="LB.D0363"/> ${result_data.data.PayFailRecord } <spring:message code="LB.Rows" />)
                                    </p>
                                </li>
                                <li>
                                    <!-- 資料總數 : -->
                                    <h3><spring:message code="LB.Inquiry_time" /></h3>
                                    <p>
                                        ${result_data.data.CMQTIME}
                                    </p>
                                </li>
                            </ul>
                            <!-- 表格區塊 -->
                        
                            <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                                <thead>
                                    <tr>
                                    	<th><spring:message code="LB.D0059"/></th>
										<th><spring:message code="LB.D0342"/><hr><spring:message code="LB.D0343"/></th>
										<th><spring:message code="LB.D0364"/></th>
										<th><spring:message code="LB.D0365"/></th>
										<th><spring:message code="LB.D0366"/></th>
										<th><spring:message code="LB.D0367"/></th>
										<th><spring:message code="LB.D0168"/></th>
										<th><spring:message code="LB.D0368"/></th>
										<th><spring:message code="LB.D0369"/></th>
                                    </tr>
                                </thead>
                                <c:if test="${empty result_data.data.REC}">
                                	<tbody>
	                                        <tr>
	                                            <td></td>
	                                            <td></td>	 	 	
	                                            <td></td>
	                                            <td></td>
	                                            <td></td>
	                                            <td></td>
	                                            <td></td>
	                                            <td></td>
	                                            <td></td>
	                                        </tr>
                                	</tbody>
                                </c:if>
                            	<c:if test="${not empty result_data.data.REC}">
                                	<tbody>
	                                    <c:forEach var="dataList" items="${ result_data.data.REC }">
	                                        <tr>
	                                            <td>${dataList.ZoneCode }</td>
	                                            <td>${dataList.CarId }<hr>${dataList.CarKind }</td>
	                                            <td>${dataList.SlipNo }</td>
	                                            <td>${dataList.OpenDate }</td>
	                                            <td>${dataList.LastDate}</td>
	                                            <td class="text-right">${dataList.OpenAmt }</td>
	                                            <td>${dataList.PayAccount }</td>
	                                            <td>${dataList.PayDate}</td>
	                                            <td>${dataList.SessionCode }</td>
	                                        </tr>
	                                    </c:forEach>
                                	
                                	</tbody>
                                </c:if>
                   
                                
                            </table>
	                        <!--button 區域 -->
	                            <!--回上頁 -->
	                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
	                            <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
	                            <!-- 繼續查詢 -->
	                            <!--<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>-->
	                            <input type="button" name="CMCONTINU" id="CMCONTINU" value="<spring:message code= "LB.X0151" />" class="ttb-button btn-flat-orange">
	                            <!-- 列印  -->
	                            <spring:message code="LB.Print" var="printbtn"></spring:message>
	                            <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
	                        
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                    <div class="text-left">
                        <!-- 		說明： -->
						<ol class="list-decimal description-list">
							<p><spring:message code="LB.Description_of_page"/></p>
                       	     <li><spring:message code="LB.parking_withholding_query_result_P3_D1"/></li>
	
			            	 <li><spring:message code="LB.parking_withholding_query_result_P3_D2"/><a href="#" onClick="window.open('http://www.tcgpmo.nat.gov.tw/index.php?act=query')" ><spring:message code="LB.parking_withholding_query_result_P3_D2-1"/></a><spring:message code="LB.parking_withholding_query_result_P3_D2-2"/>。</li>
			
			            	 <li><spring:message code="LB.parking_withholding_query_result_P3_D3"/><a href="#" onClick="window.open('http://info1.tpc.gov.tw/querycar/parking.htm')" ><spring:message code="LB.parking_withholding_query_result_P3_D3-1"/></a><spring:message code="LB.parking_withholding_query_result_P3_D3-2"/>。</li>
			
			           		 <li><spring:message code="LB.parking_withholding_query_result_P3_D4"/><a href="#" onClick="window.open('http://kpp.tbkc.gov.tw/')" ><spring:message code="LB.parking_withholding_query_result_P3_D4-1"/></a><spring:message code="LB.parking_withholding_query_result_P3_D4-2"/>。</li>
			
			            	 <li><spring:message code="LB.parking_withholding_query_result_P3_D5"/> </li>           

                        </ol>
                    </div>
                </form>

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>