<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<body>
    <!-- header     -->
	<header><%@ include file="../index/header.jsp"%></header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 匯出匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0286" /></li>
    <!-- 匯出匯款查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0143" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.W0143" /></h2>
<!-- 				TODO IS8n -->
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<ul class="ttb-result-list">
							<li>
								<!-- 查詢時間 -->
								<h3>
								<spring:message code="LB.Inquiry_time" />：
								</h3>
								<p>
								${outward_remittances_result.data.CMQTIME}
								</p>
							</li>
							<li>
								<!-- 查詢期間 -->
								<h3>
								<spring:message code="LB.Inquiry_period_1" />：
								</h3>
								<p>
								${outward_remittances_result.data.CMPERIOD}
								</p>
							</li>
							<li>
								<!-- 資料總數 -->
								<h3>
								<spring:message code="LB.Total_records" />：
								</h3>
								<p>
								  ${outward_remittances_result.data.COUNTS} <spring:message code="LB.Rows" />
								</p>								
							</li>
						</ul>
						
					<!-- 全部 -->

						<table class="stripe table-striped ttb-table dtable"  data-show-toggle="first">
						<thead>
							<tr>
								<!--匯款日 -->
								<th><spring:message code="LB.W0147" /></th>
								<!--付款日 -->
								<th><spring:message code="LB.W0148" /></th>
								<!--交易編號 -->
								<th><spring:message code="LB.Transaction_Number" /></th>
								<!--匯款金額 -->
								<th><spring:message code="LB.W0150" /></th>
								<!--收款人名稱/收款人帳號/收款人銀行名稱	 -->
								<th><spring:message code="LB.W0151" /><br><spring:message code="LB.W0152" /><br><spring:message code="LB.W0153" /></th>
								<!--手續費負擔別 -->
								<th><spring:message code="LB.W0155" /></th>
								<!--附言 -->
								<th><spring:message code="LB.W0156" /></th>
								<!--匯率 -->
								<th><spring:message code="LB.Exchange_rate" /></th>						
							</tr>
						</thead>
							<c:if test="${outward_remittances_result.data.COUNTS == '0'}">
							<tbody>
								<tr>
								</tr>
							</tbody>
							</c:if>
							<c:if test="${outward_remittances_result.data.COUNTS ne '0'}">
							<tbody>
							<c:forEach var="dataList" items="${ outward_remittances_result.data.REC }">
								<tr>
					                <td style="text-align:center">${dataList.RREGDATE}</td>
					                <td style="text-align:center">${dataList.RVALDATE}</td>
					                <td style="text-align:center">${dataList.RREFNO}</td>
					                <td style="text-align:center">${dataList.MONEY}</td>
					                <td style="text-align:center">${dataList.RORDCUS1}<br>${dataList.RBENAC}<br>${dataList.RAWB1AD1}</td>
					                <td style="text-align:center">${dataList.DETCHG}</td>
					                <td style="text-align:center">${dataList.MEMO1}</td>
					                <td class="text-right">${dataList.RRATE}</td>				               
								</tr>
							</c:forEach>
							</tbody>
							</c:if>
						</table>
						<c:if test="${outward_remittances_result.data.COUNTS != '0'}">	
							<ul class="ttb-result-list">
								<li>
									<!-- 匯款金額小計 -->
									<p>
<!-- 									TODO is8n -->
									<spring:message code="LB.W0157" />:
									</p>
									<c:forEach var="dataList" items="${ outward_remittances_result.data.CURRANCY_AMOUNT}">
									<p>
										${dataList.RREMITCY} ${dataList.RREMITAM} <spring:message code="LB.W0158" /> ${dataList.temp_mount}		
                           	         <!--筆 -->
                           	         <spring:message code="LB.Rows" /> 														
									</p>
									</c:forEach>
									<p>
										<spring:message code="LB.W0160" />												
									</p>							
								</li>
							</ul>
						</c:if>

						<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />"/>
												<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
				<form id="formId" action="${__ctx}/FCY/ACCT/f_outward_remittances_q_SessionDownload" method="post">
					<!-- 下載用 -->
 					<input type="hidden" name="downloadFileName" value="<spring:message code='LB.W0143' />"/><!--匯出匯款查詢 -->
					<input type="hidden" name="CMQTIME" value="${outward_remittances_result.data.CMQTIME}"/>
					<input type="hidden" name="CMPERIOD" value="${outward_remittances_result.data.CMPERIOD}"/>
					<input type="hidden" name="CURRANCY_PRINT" value="${outward_remittances_result.data.CURRANCY_PRINT}"/>
					<input type="hidden" name="COUNT" value=" ${outward_remittances_result.data.COUNTS}"/>
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/>
					<input type="hidden" name="hasMultiRowData" value="false"/> 
					<input type="hidden" name="hasMultiRowData" value="false"/> 		
					<!-- EXCEL下載用 -->
					<!-- headerRightEnd  資料列以前的右方界線
						 headerBottomEnd 資料列到第幾列 從0開始
						 rowStartIndex 資料列第一列的位置
						 rowRightEnd 資料列用方的界線
					 -->
					<input type="hidden" name="headerRightEnd" value="7"/>
					<input type="hidden" name="headerBottomEnd" value="6"/>
					<input type="hidden" name="rowStartIndex" value="7" />
					<input type="hidden" name="rowRightEnd" value="7" />
					<input type="hidden" name="footerStartIndex" value="9" />
					<input type="hidden" name="footerEndIndex" value="11" />
					<input type="hidden" name="footerRightEnd" value="7" />
					<!-- TXT下載用
						txtHeaderBottomEnd需為資料第一列(從0開始)-->
					<input type="hidden" name="txtHeaderBottomEnd" value="9"/>
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="true"/>
				</form>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
	

	<%@ include file="../index/footer.jsp"%>
      
    	<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);	
				setTimeout("initDataTable()",100);

			});
			function init(){
				//initFootable();
				//console.log('幣別'+'${outward_remittances_result.data.CURRANCY_AMOUNT}');
				//上一頁按鈕
				$("#previous").click(function() {
					initBlockUI();
					fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/f_outward_remittances_q','', '');
				});

				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"f_outward_remittances_q_result_print",
						"jspTitle":"<spring:message code='LB.W0143' />",//TODO匯出匯款查詢
						"CMPERIOD":"${outward_remittances_result.data.CMPERIOD}",
						"CMQTIME":"${outward_remittances_result.data.CMQTIME}",
						"COUNT":"${outward_remittances_result.data.COUNTS}",
						"CURRANCY_PRINT":"${outward_remittances_result.data.CURRANCY_PRINT}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});		
				
			}
				//選項
			 	function formReset() {
		 			if ($('#actionBar').val()=="excel"){
						$("#downloadType").val("OLDEXCEL");
						$("#templatePath").val("/downloadTemplate/f_outward_remittances_q_result.xls");
			 		}else if ($('#actionBar').val()=="txt"){
						$("#downloadType").val("TXT");
						$("#templatePath").val("/downloadTemplate/f_outward_remittances_q_result.txt");
			 		}
					$("#formId").attr("target", "");
					$("#formId").submit();
					$('#actionBar').val("");
				}
		</script>
    
</body>
</html>