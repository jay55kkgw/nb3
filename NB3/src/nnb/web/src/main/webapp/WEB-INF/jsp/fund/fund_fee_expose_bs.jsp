<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!-- 交易機制所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	shwd_prompt_init(true);
	//設定送出路徑
	var TXID = "${result_data.data.TXID}";
	
	if(TXID == "C021"){
		$("#formId").attr("action","${__ctx}/FUND/TRANSFER/fund_transfer_confirm");
	}
	else if(TXID == "C016"){
		$("#formId").attr("action","${__ctx}/FUND/PURCHASE/fund_purchase_confirm");
	}
	else if(TXID == "C017"){
		$("#formId").attr("action","${__ctx}/FUND/REGULAR/fund_regular_confirm_final");
	}
	else if(TXID == "C031"){
		$("#formId").attr("action","${__ctx}/FUND/RESERVE/PURCHASE/fund_reserve_purchase_confirm");
	}
	else if(TXID == "C032"){
		$("#formId").attr("action","${__ctx}/FUND/RESERVE/TRANSFER/fund_reserve_transfer_confirm");
	}
	
	$("#okButton").click(function(){
		if($("#allCheckBox").prop("checked") == false){
			//檢查有無按已閱讀
			errorBlock(
					null, 
					null,
					['<spring:message code= "LB.Alert092" />'], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
		}
		else{
			//檢查是否需要交易機制
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			switch(fgtxway) {
				case '1':
					// IKEY
					FuseIKey();
					break;
				case '2':
					// 晶片金融卡
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
			    	break;
				default:
					//不用交易機制直接送出
					$("#formId").submit();
			}
			
		}
	});
});

function showcontent(){
	$("#main-content").show();//顯示主要手續費
}

//複寫取得卡片主帳號結束拔插卡動作取消
function getMainAccountFinish(result){
	if(window.console){console.log("getMainAccountFinish...");}
	//成功
	if(result != "false"){
		var formId = document.getElementById("formId");
		formId.ACNNO.value = result;
		var cardACN = result;
		if(cardACN.length > 11){
			cardACN = cardACN.substr(cardACN.length - 11);
		}
		//initBlockUI();
		var uri = "${__ctx}/COMPONENT/component_acct_aj";
 		var rdata = { ACN: cardACN };
 		showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",true);
     	fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
	}
	//失敗
	else{
		FinalSendout("MaskArea",false);
	}
}

//卡片押碼結束
function generateTACFinish(result){
	if(window.console){console.log("generateTACFinish...");}
	//成功
	if(result != "false"){
	
		var TACData = result.split(",");
		
		var formId = document.getElementById("formId");
		formId.iSeqNo.value = TACData[1];
		formId.ICSEQ.value = TACData[1];
		formId.TAC.value = TACData[2];
		
		var ACN_Str1 = formId.ACNNO.value;
		if(ACN_Str1.length > 11){
			ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);
		}
	
		formId.ACNNO.value = ACN_Str1;
		
		
		// 遮罩
		initBlockUI();
		
		$("#formId").submit();
	}
	//失敗
	else{
		unBlockUI(initBlockId);
		FinalSendout("MaskArea",false);
	}
}

//驗證碼刷新
function changeCode() {
	$('input[name="capCode"]').val('');
	// 大小版驗證碼用同一個
	console.log("changeCode...");
	$('img[name="kaptchaImage"]').hide().attr(
		'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

	// 登入失敗解遮罩
	unBlockUI(initBlockId);
}

// 初始化驗證碼
function initKapImg() {
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
}

// 生成驗證碼
function newKapImg() {
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').click(function () {
		$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
	});
}
// 使用者選擇晶片金融卡要顯示驗證碼區塊
	function fgtxwayClick() {
		$('input[name="FGTXWAY"]').change(function(event) {
			// 判斷交易機制決定顯不顯示驗證碼區塊
			chaBlock();
	        }
		);
	}
	
// 判斷交易機制決定顯不顯示驗證碼區塊
function chaBlock() {
	var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
	// 交易機制選項
	if(fgtxway == '2'){
		// 若為晶片金融卡才顯示驗證碼欄位
		$("#chaBlock").show();
	}else{
		// 若非晶片金融卡則隱藏驗證碼欄位
		$("#chaBlock").hide();
	}
}

//IKEY 部分 Start
function checkxmlcn(){
	//檢查IKEY是否為本人的AJAX
	var URI = "${__ctx}/FUND/PURCHASE/fundIkeyCheck";
	var jsondc = $("#jsondc").val();
	var pkcs7Sign = $("#pkcs7Sign").val();
	var rqData = {"jsondc":jsondc,"pkcs7Sign":pkcs7Sign};
	fstop.getServerDataEx(URI,rqData,false,IkeyCheckcallback);
}
function IkeyCheckcallback(data){
	console.log(data);
	if(data.data.checkflag=="SUCCESSFUL"){
		$("#formId").submit();
	}else{

		FinalSendout("MaskArea",false);
		errorBlock(
				null, 
				null,
				['<spring:message code= "LB.X2287" />'], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
		changeCode();
		
	}
}

function FuseIKey(){
	console.log("IKey...");
	
	var jsondc = $("#jsondc").val();
	console.log("jsondc: " + jsondc);
	
	// IKEY驗證流程
	FuiSignForPKCS7(jsondc);
}

//IKEY簽章
function FuiSignForPKCS7(data){
	if(window.console){console.log("uiSignForPKCS7...");}
	var formId = document.getElementById("formId");
	SLBForSeconds("MaskArea",60000);
	UISignForPKCS7(data,formId.pkcs7Sign,"FuiSignForPKCS7Finish");
}

//IKEY簽章結束
function FuiSignForPKCS7Finish(result){
	if(window.console){console.log("uiSignForPKCS7Finish...");}
	//成功
	if(result != "false"){
		//繼續做
		checkxmlcn();
	}
	//失敗
	else{
		unBlockUI(initBlockId);
		alert("IKEY簽章失敗");
		ShowLoadingBoard("MaskArea",false);
	}
}
//IKEY 部分 END
//金融卡 部分 Start
function CheckIdResult(data){
	showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",false);
	console.log(data);
	if("0" == data.msgCode) {
		initBlockUI();
		$("#formId").submit();
	}else{
		//unBlockUI(initBlockId);
		errorBlock(
				null, 
				null,
				['<spring:message code= "LB.X1701" />'], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
		changeCode();
	}
}

</script>
</head>
<body>
<c:set var="SHWD" value="${result_data.data.SHWD}"></c:set>
<%@ include file="fund_shwd.jsp"%>
<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.Funds" /></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.W1064" /></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X1783"/></li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12" id="maincontent">
			<!--主頁內容-->
			<section id="main-content" class="container" style="display:none;">
<!-- 				「基金各級別近五年度之費用率及報酬率資訊｣及「基金通路報酬資訊｣ -->
				<h2><spring:message code="LB.X1783"/></h2>
				
					<form id="formId" method="post">
						<input type="hidden" name="TXID" value="${result_data.data.TXID}"/>
						<input type="hidden" name="TRANSCODE" value="${result_data.data.TRANSCODE}"/>
						<input type="hidden" name="CDNO" value="${result_data.data.CDNO}"/>
						<input type="hidden" name="INTRANSCODE" value="${result_data.data.INTRANSCODE}"/>
						<input type="hidden" name="UNIT" value="${result_data.data.UNIT}"/>
						<input type="hidden" name="UNITInteger" value="${result_data.data.UNITInteger}"/>
						<input type="hidden" name="UNITDot" value="${result_data.data.UNITDot}"/>
						<input type="hidden" name="BILLSENDMODE" value="${result_data.data.BILLSENDMODE}"/>
						<input type="hidden" name="FUNDAMT" value="${result_data.data.FUNDAMT}"/>
						<input type="hidden" name="FUNDAMTFormat" value="${result_data.data.FUNDAMTFormat}"/>
						<input type="hidden" name="FUNDAMTNoComma" value="${result_data.data.FUNDAMTNoComma}"/>
						<input type="hidden" name="RRSK" value="${result_data.data.RRSK}"/>
						<input type="hidden" name="RISK7" value="${result_data.data.RISK7}"/>
						<input type="hidden" name="GETLTD" value="${result_data.data.GETLTD}"/>
						<input type="hidden" name="GETLTD7" value="${result_data.data.GETLTD7}"/>
						<input type="hidden" name="FDINVTYPE" value="${result_data.data.FDINVTYPE}"/>
						<input type="hidden" name="TRADEDATE" value="${result_data.data.TRADEDATE}"/>
						<input type="hidden" name="FCA1" value="${result_data.data.FCA1}"/>
						<input type="hidden" name="FCA2" value="${result_data.data.FCA2}"/>
						<input type="hidden" name="AMT3" value="${result_data.data.AMT3}"/>
						<input type="hidden" name="AMT5" value="${result_data.data.AMT5}"/>
						<input type="hidden" name="SSLTXNO" value="${result_data.data.SSLTXNO}"/>
						<input type="hidden" name="FUNCUR" value="${result_data.data.FUNCUR}"/>
						<input type="hidden" name="SHORTTRADE" value="${result_data.data.SHORTTRADE}"/>
						<input type="hidden" name="SHORTTUNIT" value="${result_data.data.SHORTTUNIT}"/>
						<input type="hidden" name="FDAGREEFLAG" value="${result_data.data.FDAGREEFLAG}"/>
						<input type="hidden" name="FDNOTICETYPE" value="${result_data.data.FDNOTICETYPE}"/>
						<input type="hidden" name="FDPUBLICTYPE" value="${result_data.data.FDPUBLICTYPE}"/>
						<input type="hidden" name="COUNTRYTYPE" value="${result_data.data.COUNTRYTYPE}"/>
						<input type="hidden" name="COUNTRYTYPE1" value="${result_data.data.COUNTRYTYPE1}"/>
						<input type="hidden" name="COMPANYCODE" value="${result_data.data.COMPANYCODE}"/>
						<input type="hidden" name="PAYTYPE" value="${result_data.data.PAYTYPE}"/>
						<input type="hidden" name="FUNDACN" value="${result_data.data.FUNDACN}"/>
						<input type="hidden" name="INVTYPE" value="${result_data.data.INVTYPE}"/>
						<input type="hidden" name="HTELPHONE" value="${result_data.data.HTELPHONE}"/>
						<input type="hidden" name="OTELPHONE" value="${result_data.data.OTELPHONE}"/>
						<input type="hidden" name="OUTACN" value="${result_data.data.OUTACN}"/>
						<input type="hidden" name="INTSACN" value="${result_data.data.INTSACN}"/>
						<input type="hidden" name="FCAFEE" value="${result_data.data.FCAFEE}"/>
						<input type="hidden" name="PAYDAY1" value="${result_data.data.PAYDAY1}"/>
						<input type="hidden" name="PAYDAY2" value="${result_data.data.PAYDAY2}"/>
						<input type="hidden" name="PAYDAY3" value="${result_data.data.PAYDAY3}"/>
						<input type="hidden" name="PAYDAY4" value="${result_data.data.PAYDAY4}"/>
						<input type="hidden" name="PAYDAY5" value="${result_data.data.PAYDAY5}"/>
						<input type="hidden" name="PAYDAY6" value="${result_data.data.PAYDAY6}"/>
						<input type="hidden" name="PAYDAY7" value="${result_data.data.PAYDAY7}"/>
						<input type="hidden" name="PAYDAY8" value="${result_data.data.PAYDAY8}"/>
						<input type="hidden" name="PAYDAY9" value="${result_data.data.PAYDAY9}"/>
						<input type="hidden" name="MIP" value="${result_data.data.MIP}"/>
						<input type="hidden" name="BRHCOD" value="${result_data.data.BRHCOD}"/>
						<input type="hidden" name="CUTTYPE" value="${result_data.data.CUTTYPE}"/>
						<input type="hidden" name="CRY1" value="${result_data.data.CRY1}"/>
						<input type="hidden" name="DBDATE" value="${result_data.data.DBDATE}"/>
						<input type="hidden" name="STOP" value="${result_data.data.STOP}"/>
						<input type="hidden" name="YIELD" value="${result_data.data.YIELD}"/>
						<input type="hidden" name="TYPE" value="${result_data.data.TYPE}"/>
						<input type="hidden" name="FUNDLNAME" value="${result_data.data.FUNDLNAME}"/>
						<input type="hidden" name="RISK" value="${result_data.data.RISK}"/>
						<input type="hidden" name="SALESNO" value="${result_data.data.SALESNO}"/>
						<input type="hidden" name="PRO" value="${result_data.data.PRO}"/>
						<input type="hidden" name="RSKATT" value="${result_data.data.RSKATT}"/>
						<input type="hidden" name="CUSNAME" value="${result_data.data.CUSNAME}"/>
					    <input type="hidden" id="ISSUER" name="ISSUER" value="">
					    <input type="hidden" id="ACNNO" name="ACNNO" value="">
					    <input type="hidden" id="TRMID" name="TRMID" value="">
					    <input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					    <input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					    <input type="hidden" id="TAC" name="TAC" value="">
					    <input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					    <input type="hidden" id="jsondc" name="jsondc" value='${jsondc}'>
					    <input type="hidden" name="SHWD" value='${result_data.data.SHWD}'>
					    <input type="hidden" name="XFLAG" value='${result_data.data.XFLAG}'>
						<input type="hidden" name="NUM" id="NUM" value=""/>
						<input type="hidden" name="PEMAIL" value="${result_data.data.PEMAIL}">
						<input type="hidden" name="REPID" value="${result_data.data.REPID}">
						<input type="hidden" name="UPDATEMAIL" id="UPDATEMAIL" value=""/>
						<input type="hidden" name="FEE_TYPE" id="FEE_TYPE" value="${result_data.data.FEE_TYPE}"/>
						<input type="hidden" name="SLSNO" id="SLSNO" value="${result_data.data.SLSNO}"/>
						
					<div class="main-content-block row">
						<div class="col-12">
							<!--內容-->
							<div class="ttb-message">
								<p><spring:message code="LB.X1783"/></p>
                          	</div>
                          	<div style="margin: 0 5% 0 5%">
                            <ul class="ttb-result-list" style="list-style:decimal;">
								<li class="full-list">
									<strong style="font-size:15px">
										台端所申購之基金有不同級別，台端應於申購前充分瞭解該檔基金各級別之不同（如不同計價幣別、配息或不配息、手續費為前收或後收等），以投資合適之級別。
										不同級別之費用率與報酬率或有差異，請詳閱「<a href="https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=${result_data.data.TRANSCODE}&B=21" target="_blank" style="color:#007bff;">基金理財網站</a>」之各級別近五年度之費用率與報酬率資訊。
									</strong>
                               	</li>
                               	<li class="full-list">
									<strong style="font-size:15px">
										台端所申購之基金，請詳閱「<a href="https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=${result_data.data.TRANSCODE}&B=21" target="_blank" style="color:#007bff;">基金理財網站</a>」之該檔基金通路報酬揭露資訊。
									</strong>
                               	</li>
                            </ul>
	                        <label class="check-block">
								<input type="checkbox" class="riskConfirmCheckBox" id="allCheckBox" />
								<strong><p align="left">本人已充分評估且已詳閱並瞭解本次申購貴行上架之「基金各級別近五年度之費用率及報酬率資訊｣及「基金通路報酬資訊｣，且確認本次申購之基金級別符合本人投資需求，並同意貴行留存此評估結果。</p></strong> 
								<span class="ttb-check"></span>
							</label>
							<!-- 							交易機制  Start-->
							<div class="ttb-input-block">
							<div class="ttb-input-item row" id="tradeway"  style="display:none;">
							       <span class="input-title">
										<label>
											<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
										</label>
									</span>
							<!-- 交易機制選項 -->
									<span class="input-block">
                                            <c:if test="${sessionScope.isikeyuser}">
										<!-- IKEY -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
										     </c:if>
										<!-- 晶片金融卡 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Financial_debit_card" />
											 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</span>
									
						    </div>
										<!-- 使用者選擇晶片金融卡後才顯示驗證碼-->
								<div class="ttb-input-item" id="chaBlock" style="display:none;">
									<!-- 驗證碼 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Captcha" /></h4>
										</label>
									</span>
									<span class="input-block">
	                                    <div class="ttb-input">
	                                        <img name="kaptchaImage" class="verification-img" src="" align="top" id="random" />
	                                        <button type="button" class="btn-flat-gray" name="reshow" onclick="changeCode()">
												<spring:message code="LB.Regeneration" /></button>
	                                    </div>
	                                    <div class="ttb-input">
	                                        <input id="capCode" name="capCode" type="text"
												class="text-input" maxlength="6" autocomplete="off">
	                                        <br>
	                                        <span class="input-unit"><spring:message code="LB.Captcha_refence" /></span>
	                                    </div>
	                                </span>
								</div>
								</div>
<!-- 							交易機制  End-->
	                  			<input type="button" id="okButton" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
	                		
						</div>
					</form>
				</div>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>