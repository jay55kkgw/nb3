<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//開始查詢資料並完成畫面
	setTimeout("init()",20);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	errorBlock(
			null, 
			null,
			["<spring:message code= 'LB.X2226' />"], 
			'<spring:message code= "LB.Confirm" />', 
			null
	);
	
	$("#CMSUBMIT").click(function(){
		
		
		//表單驗證
		if(!$("#formID").validationEngine("validate")){
			e = e || window.event;//for IE
			e.preventDefault();
		}
		
// 		if(!CheckSelect("INTRANSCODE","轉入基金名稱","#")){
// 			return false;
// 		}
// 		if(${BILLSENDMODE == "2"}){
// 			if(!CheckAmount("FUNDAMT","轉出信託金額",null,null)){
// 				return false;
// 			}
// 		}
		$("#formID").validationEngine("detach");
		
		var risk7= "${result_data.data.RISK7}";
		var hasRisk = checkRisk($("#RISKSHOW").html(),$("#FDINVTYPE").val());
		
		if(hasRisk == true && risk7.length == 0){
			//基金風險警告投資屬性不合
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_risk_alert?TXID=C032&ALERTTYPE=1");
		}
		else{
			//基金風險確認書
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_risk_confirm?TXID=C032");
		}
		$("#formID").submit();
	});
	$("#cancelButton").click(function(){
		$("#formID").validationEngine("detach");
		//$("#formID").attr("action","${__ctx}/FUND/TRANSFER/query_fund_transfer_data");
		//$("#formID").submit();
		location.href = "${__ctx}/FUND/TRANSFER/query_fund_transfer_data";
	});
	$("#resetButton").click(function(){
		$("#FUNDAMT").val("");
		$("#INTRANSCODE option:first").prop("selected",true);
		$("#RISKSHOW").html("");
	});
});
function init(){
	initFootable();
	//表單驗證
	$("#formID").validationEngine({binded:false,promptPosition:"inline"});
}
function getFundDataAjax(){
	if($("#INTRANSCODE").val() != "#"){
		var URI = "${__ctx}/FUND/TRANSFER/getFundDataAjax";
		var rqData = {INTRANSCODE:$("#INTRANSCODE").val()};
		fstop.getServerDataEx(URI,rqData,false,getFundDataAjaxFinish);
	}
	else{
		$("#RISKSHOW").html("");
	}
}
function getFundDataAjaxFinish(data){
	if(data.result == true){
		var txnFundData = $.parseJSON(data.data);
		$("#RISKSHOW").html(txnFundData.RISK);
		$("#INFUNDSNAME").val(txnFundData.FUNDLNAME);
		$("#FUS98E").val(txnFundData.FUS98E);
		$("#COUNTRY").val(txnFundData.COUNTRYTYPE);
		$("#RRSK").val(txnFundData.RISK);
	}
	else{
		//alert("<spring:message code= "LB.Alert115" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert115' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.Funds" /></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.W1064" /></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code= "LB.W1062" /></li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code= "LB.W1062" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
			
					<form id="formID" method="post">
						<input type="hidden" name="ADOPID" value="C032">
						<input type="hidden" name="TRANSCODE" value="${result_data.data.TRANSCODE}"/>
						<input type="hidden" name="CDNO" value="${result_data.data.CDNO}"/>
						<input type="hidden" name="UNIT" value="${result_data.data.UNIT}"/>
						<input type="hidden" name="BILLSENDMODE" value="${result_data.data.BILLSENDMODE}"/>
						<input type="hidden" name="SHORTTRADE" value="${result_data.data.SHORTTRADE}"/>
						<input type="hidden" name="SHORTTUNIT" value="${result_data.data.SHORTTUNIT}"/>
						<input type="hidden" id="FDINVTYPE" name="FDINVTYPE" value="${result_data.data.FDINVTYPE}"/>
						<input type="hidden" name="RISK7" value="${result_data.data.RISK7}"/>
						<input type="hidden" name="GETLTD7" value="${result_data.data.GETLTD7}"/>
						<input type="hidden" id="INFUNDSNAME" name="INFUNDSNAME"/>
						<input type="hidden" id="FUS98E" name="FUS98E"/>
						<input type="hidden" id="COUNTRY" name="COUNTRY"/>
						<input type="hidden" id="RRSK" name="RRSK"/>
						<input type="hidden" name="BRHCOD" value="${result_data.data.BRHCOD}"/>
						<input type="hidden" name="FUNDLNAME" value="${result_data.data.FUNDLNAME}"/>
						<input type="hidden" id="Step" name="Step" value="1"/>
						<input type="hidden" name="FEE_TYPE" value="${result_data.data.FEE_TYPE}"/>
						<div class="main-content-block row">
							<div class="col-12">
		                        <div class="ttb-input-block">
		                        
		                            <div class="ttb-input-item row">
		                                <span class="input-title"><label>
		                                        <h4><spring:message code="LB.Id_no"/></h4>
		                                    </label></span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>${result_data.data.hiddenCUSIDN}</span>
		                                    </div>
		                                </span>
		                            </div>
		                            <div class="ttb-input-item row">
		                                <span class="input-title"><label>
		                                        <h4><spring:message code="LB.Name"/></h4>
		                                    </label></span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>${result_data.data.hiddenNAME}</span>
		                                    </div>
		                                </span>
		                            </div>
		                            <div class="ttb-input-item row">
		                                <span class="input-title"><label>
		                                        <h4><spring:message code="LB.W1111"/></h4>
		                                    </label></span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>${result_data.data.hiddenCDNO}</span>
		                                    </div>
		                                </span>
		                            </div>
		                            <div class="ttb-input-item row">
		                                <span class="input-title"><label>
		                                        <h4><spring:message code="LB.W0025"/></h4>
		                                    </label></span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>（${result_data.data.TRANSCODE}）${result_data.data.FUNDLNAME}</span>
		                                    </div>
		                                </span>
		                            </div>
		                            <div class="ttb-input-item row">
		                                <span class="input-title"><label>
		                                        <h4><spring:message code="LB.W0026"/></h4>
		                                    </label></span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>
								                	<c:if test="${__i18n_locale eq 'en' }">
						                   				${result_data.data.ADCCYENGNAME}&nbsp;&nbsp;${result_data.data.OFUNDAMTDotTwo}
						                    		</c:if>
						                    		<c:if test="${__i18n_locale eq 'zh_TW' }">
						                   				${result_data.data.ADCCYNAME}&nbsp;&nbsp;${result_data.data.OFUNDAMTDotTwo}
						                    		</c:if>
						                    		<c:if test="${__i18n_locale eq 'zh_CN' }">
						                   				${result_data.data.ADCCYCHSNAME}&nbsp;&nbsp;${result_data.data.OFUNDAMTDotTwo}
						                    		</c:if>
<%-- 		                                        ${result_data.data.ADCCYNAME}&nbsp;&nbsp;${result_data.data.OFUNDAMTDotTwo} --%>
		                                        </span>
		                                    </div>
		                                </span>
		                            </div>
		                            <div class="ttb-input-item row">
		                                <span class="input-title"><label>
		                                        <h4><spring:message code="LB.W0027"/></h4>
		                                    </label></span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>${result_data.data.UNIT}</span>
		                                    </div>
		                                </span>
		                            </div>
		                            <div class="ttb-input-item row">
		                                <span class="input-title"><label>
		                                        <h4><spring:message code="LB.W1115"/></h4>
		                                    </label></span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>${result_data.data.BILLSENDMODEChinese}</span>
		                                    </div>
		                                </span>
		                            </div>
		                            <div class="ttb-input-item row">
		                                <span class="input-title"><label>
		                                        <h4><spring:message code="LB.W1116"/></h4>
		                                    </label></span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>
								                	<c:if test="${__i18n_locale eq 'en' }">
						                   				${result_data.data.ADCCYENGNAME}&nbsp;&nbsp;
						                    		</c:if>
						                    		<c:if test="${__i18n_locale eq 'zh_TW' }">
						                   				${result_data.data.ADCCYNAME}&nbsp;&nbsp;
						                    		</c:if>
						                    		<c:if test="${__i18n_locale eq 'zh_CN' }">
						                   				${result_data.data.ADCCYCHSNAME}&nbsp;&nbsp;
						                    		</c:if>
<%-- 													${result_data.data.ADCCYNAME}&nbsp;&nbsp; --%>
													<c:if test="${result_data.data.BILLSENDMODE == '1'}">
														${result_data.data.FUNDAMT}
														<input type="hidden" name="FUNDAMT" value="${result_data.data.FUNDAMT}"/>
													</c:if>
													<c:if test="${result_data.data.BILLSENDMODE == '2'}">
														<input maxLength="10" size="10" id="FUNDAMT" name="FUNDAMT" class="text-input validate[required,funcCall[validate_CheckAmount[<spring:message code= "LB.W1116" />,FUNDAMT]]]"/>
													</c:if>
												</span>
		                                    </div>
		                                </span>
		                            </div>
		                            <div class="ttb-input-item row">
		                                <span class="input-title"><label>
		                                        <h4><spring:message code="LB.W1067"/></h4>
		                                    </label></span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>${result_data.data.FDINVTYPEChinese}</span>
		                                    </div>
		                                </span>
		                            </div>
		                            <div class="ttb-input-item row">
		                                <span class="input-title"><label>
		                                        <h4><spring:message code="LB.W0967"/></h4>
		                                    </label></span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
	                                        	<select id="INTRANSCODE" name="INTRANSCODE" class="custom-select select-input w-auto validate[funcCall[validate_CheckSelect[<spring:message code= "LB.W0967" />,INTRANSCODE,#]]]" onchange="getFundDataAjax()">
													<option value="#">--- <spring:message code="LB.Select"/> ---</option>					
													<c:forEach var="dataMap" items="${result_data.data.finalFundDataList}">
														<option value="${dataMap.TRANSCODE}">（${dataMap.TRANSCODE}）${dataMap.FUNDLNAME}</option>
													</c:forEach>
												</select>
												<br>
		                                        <span class="input-unit">
													<spring:message code= "LB.W1073" />：<font id="RISKSHOW"></font>&nbsp;&nbsp;
													<c:if test="${result_data.data.shortDays != 0}">
														<spring:message code= "LB.W1120" />：${result_data.data.shortDays}<spring:message code= "LB.W1121" />
													</c:if>
		                                        </span>
		                                    </div>
		                                </span>
		                            </div>
				                            
		                        </div>
<!-- 								<table class="table" data-toggle-column="first"> -->
<!-- 									<tr> -->
<%-- 										<td><spring:message code="LB.Id_no"/></td> --%>
<%-- 									 	<td colspan="5" style="text-align:left">${result_data.data.hiddenCUSIDN}</td> --%>
<!-- 									</tr> -->
<!-- 									<tr> -->
<%-- 										<td><spring:message code="LB.Name"/></td> --%>
<%-- 									 	<td colspan="5" style="text-align:left">${result_data.data.hiddenNAME}</td> --%>
<!-- 									</tr> -->
<!-- 									<tr> -->
<%-- 										<td><spring:message code= "LB.W1111" /></td> --%>
<%-- 										<td colspan="5" style="text-align:left">${result_data.data.hiddenCREDITNO}</td> --%>
<!-- 									</tr> -->
<!-- 									<tr> -->
<%-- 										<td><spring:message code= "LB.W0025" /></td> --%>
<%-- 										<td colspan="5" style="text-align:left">（${result_data.data.TRANSCODE}）${result_data.data.FUNDLNAME}</td> --%>
<!-- 									</tr> -->
<!-- 									<tr> -->
<%-- 										<td><spring:message code= "LB.W0026" /></td> --%>
<%-- 										<td colspan="5" style="text-align:left">${result_data.data.ADCCYNAME}&nbsp;&nbsp;${result_data.data.OFUNDAMTDotTwo}</td> --%>
<!-- 									</tr> -->
<!-- 									<tr> -->
<%-- 										<td><spring:message code= "LB.W0027" /></td> --%>
<%-- 										<td colspan="5" style="text-align:left">${result_data.data.UNIT}</td> --%>
<!-- 									</tr> -->
<!-- 									<tr> -->
<%-- 										<td><spring:message code= "LB.W1115" /></td> --%>
<%-- 										<td colspan="5" style="text-align:left">${result_data.data.BILLSENDMODEChinese}</td> --%>
<!-- 									</tr> -->
<!-- 									<tr>  -->
<%-- 										<td><spring:message code= "LB.W1116" /></td> --%>
<!-- 										<td colspan="5" style="text-align:left"> -->
<%-- 											${result_data.data.ADCCYNAME}&nbsp;&nbsp; --%>
<%-- 											<c:if test="${result_data.data.BILLSENDMODE == '1'}"> --%>
<%-- 												${result_data.data.FUNDAMT} --%>
<%-- 												<input type="hidden" name="FUNDAMT" value="${result_data.data.FUNDAMT}"/> --%>
<%-- 											</c:if> --%>
<%-- 											<c:if test="${result_data.data.BILLSENDMODE == '2'}"> --%>
<%-- 												<input maxLength="10" size="10" id="FUNDAMT" name="FUNDAMT" class="validate[required,funcCall[validate_CheckAmount[<spring:message code= "LB.W1116" />,FUNDAMT]]]"/> --%>
<%-- 											</c:if> --%>
<!-- 										</td> -->
<!-- 									</tr> -->
<!-- 									<tr> -->
<%-- 										<td><spring:message code= "LB.W1067" /></td> --%>
<%-- 										<td colspan="5" style="text-align:left">${result_data.data.FDINVTYPEChinese}</td> --%>
<!-- 									</tr> -->
<!-- 									<tr> -->
<%-- 										<td><spring:message code= "LB.W0967" /></td> --%>
<!-- 										<td colspan="5" style="text-align:left"> -->
<%-- 											<select id="INTRANSCODE" name="INTRANSCODE" class="validate[funcCall[validate_CheckSelect[<spring:message code= "LB.W0967" />,INTRANSCODE,#]]]" onchange="getFundDataAjax()"> --%>
<%-- 												<option value="#">--- <spring:message code="LB.Select"/> ---</option>					 --%>
<%-- 												<c:forEach var="dataMap" items="${result_data.data.finalFundDataList}"> --%>
<%-- 													<option value="${dataMap.TRANSCODE}">（${dataMap.TRANSCODE}）${dataMap.FUNDLNAME}</option> --%>
<%-- 												</c:forEach> --%>
<!-- 											</select> -->
<%-- 											<spring:message code= "LB.W1073" />：<font id="RISKSHOW"></font>&nbsp;&nbsp; --%>
<%-- 											<c:if test="${result_data.data.shortDays != 0}"> --%>
<%-- 												<spring:message code= "LB.W1120" />：${result_data.data.shortDays}<spring:message code= "LB.W1121" /> --%>
<%-- 											</c:if> --%>
<!-- 										</td> -->
<!-- 									</tr> -->
<!-- 								</table> -->
								<input type="button" id="cancelButton" value="<spring:message code="LB.Back_to_function_home_page"/>" class="ttb-button btn-flat-gray"/>	
								<input type="button" id="resetButton" value="<spring:message code="LB.Re_enter"/>" class="ttb-button btn-flat-gray"/>	
								<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
							</div>
						</div>
					</form>
				</div>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>