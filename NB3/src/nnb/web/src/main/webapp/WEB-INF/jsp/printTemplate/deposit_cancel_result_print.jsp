<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
  <br /><br />
  <div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
  <br /><br />
  <div style="text-align:center">
    <font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
  </div>
  <br />
  <div style="text-align:center">
    ${MsgName}
  </div>
  <br />
  <table class="print">
    <!-- 交易時間 -->
    <tr>
      <td style="width:8em">
        <spring:message code="LB.Trading_time" />
      </td>
      <td>${CMQTIME} </td>
    </tr>
    <!-- 帳號 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Account" />
      </td>
      <td>${FDPACN}</td>
    </tr>
    <!-- 存款種類 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Deposit_type" />
      </td>
      <td>${FDPTYPE}</td>
    </tr>
    <!-- 存單號碼 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Certificate_no" />
      </td>
      <td> ${FDPNUM}</td>
    </tr>
    <!-- 存單金額 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Certificate_amount" />
      </td>
      <td>
        <fmt:formatNumber type="number" minFractionDigits="2" value="${AMT}" />
      </td>
    </tr>
    <!-- 計息方式 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Interest_calculation" />
      </td>
      <td>
        ${INTMTH}
      </td>
    </tr>
    <!-- 起存日 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Start_date" />
      </td>
      <td class="DataCell">${SHOWDPISDT}</td>
    </tr>
    <!-- 到期日 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Maturity_date" />
      </td>
      <td class="DataCell">${SHOWDUEDAT}</td>
    </tr>
    <!-- 利息 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Interest_rate" />
      </td>
      <td>
        <fmt:formatNumber type="number" minFractionDigits="2" value="${INTPAY}" />
      </td>
    </tr>
    <!-- 所得稅 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Income_tax" />
      </td>
      <td>
        <fmt:formatNumber type="number" minFractionDigits="2" value="${TAX}" />
      </td>
    </tr>
    <!-- 透支息 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.After-tax_principal_and_interest" />
      </td>
      <td>
        <!--         新台幣 -->
        <spring:message code="LB.NTD" />
        <fmt:formatNumber type="number" minFractionDigits="2" value="${INTRCV}" />
        <!--         元 -->
        <spring:message code="LB.Dollar" />
      </td>
    </tr>
    <!-- 稅後本息 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.After-tax_principal_and_interest" />
      </td>
      <td>
        <!--         新台幣 -->
        <spring:message code="LB.NTD" />
        <fmt:formatNumber type="number" minFractionDigits="2" value="${PAIAFTX}" />
        <!--         元 -->
        <spring:message code="LB.Dollar" />
      </td>
    </tr>
    <!-- 健保費 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.NHI_premium" />
      </td>
      <td>
        <fmt:formatNumber type="number" minFractionDigits="2" value="${NHITAX }" />
      </td>
    </tr>
    <!-- 交易備註 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Transfer_note" />
      </td>
      <td class="DataCell">
        ${CMTRMEMO}
      </td>
    </tr>
  </table>
  <br>
  <br>
  <div class="text-left">
    <!-- 		說明： -->
    <spring:message code="LB.Description_of_page" />
    <ol class="list-decimal text-left">
      <li>
        <spring:message code="LB.Deposit_cancel_P3_D1" />
      </li>
    </ol>
  </div>
</body>

</html>