<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/dataTables.rowsGroup.js"></script>


<script type="text/javascript">
$(document).ready(function() {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 50);
	// 開始跑下拉選單並完成畫面
	setTimeout("init()", 400);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
	// 初始化時隱藏span
	$("#hideblock").hide();
	
	jQuery(function($) {
		$('.dtable').DataTable({
			scrollX: true,
			sScrollX: "99%",
			scrollY: true,
			bPaginate: false,
			bFilter: false,
			bDestroy: true,
			bSort: false,
			info: false,
			rowsGroup: [0, 1, 2],
		});
	});
});

function init(){
	
	$("#formId").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	
	//確定 
	$("#CMSUBMIT").click(function() {
		$("#CUSAD1C").removeClass("validate[required]");
		$("#CUSADD1").removeClass("validate[required]");
		if($("#CUSAD1C").val()== '' && $("#CUSAD2C").val()== ''){
			$("#CUSAD1C").addClass("validate[required]");
		}
		if($("#CUSADD1").val()== '' && $("#CUSADD2").val()== '' && $("#CUSADD3").val()== ''){
			$("#CUSADD1").addClass("validate[required]");
		}
		
	    if (!$('#formId').validationEngine('validate')) {
	        e.preventDefault();
	    } else {
	    	$("#formId").validationEngine('detach');
				initBlockUI();
				$("#formId").attr("action","${__ctx}/CHANGE/DATA/fcy_data_confirm");
	  			$("#formId").submit(); 
	    }
	});
	
}

</script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 外匯進口/出口/匯兌通訊地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0385" /></li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0385" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12">
						<br>
						<!-- 變更通訊地址/電話 -->
						<table class="stripe table ttb-table dtable m-0" data-show-toggle="first">
							<thead>
								<tr>
									<th rowspan="2"><spring:message code="LB.D0375" /></th>
									<th colspan="2"><spring:message code="LB.D0376" /></th>
									<th rowspan="2"><spring:message code="LB.Telephone" /></th>
								</tr>
								<tr style="display:none">
									<th></th>
									<th></th>
								</tr>
							</thead>
								<c:set var="dataList" value="${fcy_data_query.data}" />
	                        <tbody>
                                <tr>
                                    <td>${dataList.BCHNAME}(${dataList.BCHID})</td>
                                    <td><spring:message code="LB.D0392" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0394" />:</span><br>
                                    	${dataList.CUSAD1C}
                                    	${dataList.CUSAD2C}
                                    </td>
                                    <td><span class="sec-title">TEL:</span><br>
                                    	${dataList.CUSTEL1}
                                    </td>
                                </tr>
                                <tr>
                               		<td>${dataList.BCHNAME}(${dataList.BCHID})</td>
                                    <td><spring:message code="LB.D0392" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0396" />:</span><br>
                                    	${dataList.CUSADD1}
                                    	${dataList.CUSADD2}
                                    	${dataList.CUSADD3}
                                    </td>
                                    <td><span class="sec-title">FAX</span><br>
                                    	${dataList.CUSTEL2}
                                    </td>
                                </tr>
                                <tr>
                                	<input type="hidden" name="BCHNAME" value="${dataList.BCHNAME}"/>
                                	<input type="hidden" name="BCHID" value="${dataList.BCHID}"/>
                                    <td></td>
                                    <td><spring:message code="LB.D0393" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0394" />:</span><br>
<%--                                         <input type="text" name="CUSAD1C" id="CUSAD1C" maxlength="19" class="text-input validate[required,funcCall[validate_CheckAddressFormat[<spring:message code= "LB.X1255" />,CUSAD1C,1]]]" value=""/><br> --%>
<%--                                         <input type="text" name="CUSAD2C" id="CUSAD2C" maxlength="19" class="text-input validate[required,funcCall[validate_CheckAddressFormat[<spring:message code= "LB.X1255" />,CUSAD2C,1]]]" value=""/><br> --%>
                                        <input type="text" name="CUSAD1C" id="CUSAD1C" maxlength="19" class="text-input validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.X1255" />,CUSAD1C,1]]]" value="${dataList.CUSAD1C}"/><br>
                                        <input type="text" name="CUSAD2C" id="CUSAD2C" maxlength="19" class="text-input validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.X1255" />,CUSAD2C,1]]]" value="${dataList.CUSAD2C}"/><br>
                                        <span class="text-danger">＊<spring:message code="LB.D0382" /></span>
                                    </td>
                                    <td><span class="sec-title">TEL:</span><br>
										<input type="text" name="CUSTEL1" id="CUSTEL1" maxlength="15" class="text-input validate[funcCallRequired[validate_CheckAddressFormat['TEL',CUSTEL1,3]]]" value="${dataList.CUSTEL1}"/>
									</td>
                                </tr>
                                <tr>
                                	<td></td>
                                    <td><spring:message code="LB.D0393" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0396" />:</span><br>
<%--                                         <input type="text" name="CUSADD1" id="CUSADD1" maxlength="35" class="text-input validate[required,funcCall[validate_CheckAddressFormat[<spring:message code= "LB.X1256" />,CUSADD1,0]]]" value=""/><br> --%>
<%--                                         <input type="text" name="CUSADD2" id="CUSADD2" maxlength="35" class="text-input validate[required,funcCall[validate_CheckAddressFormat[<spring:message code= "LB.X1256" />,CUSADD2,0]]]" value=""/><br> --%>
<%--                                         <input type="text" name="CUSADD3" id="CUSADD3" maxlength="35" class="text-input validate[required,funcCall[validate_CheckAddressFormat[<spring:message code= "LB.X1256" />,CUSADD3,0]]]" value=""/> --%>
                                        <input type="text" name="CUSADD1" id="CUSADD1" maxlength="35" class="text-input validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.X1256" />,CUSADD1,0]]]" value="${dataList.CUSADD1}"/><br>
                                        <input type="text" name="CUSADD2" id="CUSADD2" maxlength="35" class="text-input validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.X1256" />,CUSADD2,0]]]" value="${dataList.CUSADD2}"/><br>
                                        <input type="text" name="CUSADD3" id="CUSADD3" maxlength="35" class="text-input validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.X1256" />,CUSADD3,0]]]" value="${dataList.CUSADD3}"/>
                                    </td>
                                    <td><span class="sec-title">FAX:</span><br>
                                    <input type="text" name="CUSTEL2" id="CUSTEL2" maxlength="15" class="text-input validate[funcCallRequired[validate_CheckAddressFormat['FAX',CUSTEL2,3]]]" value="${dataList.CUSTEL2}"/>
                                    </td>
                                </tr>
                            </tbody>
						</table>						
						
						<!-- 確定 -->
                        <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" >
                        <input type="hidden" id="fgtxway" name="fgtxway" value="${fcy_data_query.data.fgtxway}">
					</div>
				</div>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Note" /></p>
					<li><span><spring:message code="LB.Fcy_Data_P2_D1" /></span></li>
				</ol>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>