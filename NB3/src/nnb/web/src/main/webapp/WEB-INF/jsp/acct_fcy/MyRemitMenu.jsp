<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>


	<script type="text/javascript">
		$(document).ready(function () {
			init();
		});

		function init() {
			setTimeout("initDataTable()",100);
		}
		
	
		
		function fillData(id) 
		{
			if (id == 'NUL') {
				var item = $("#692_ADRMTITEM").val();
				var desc = $("#692_ADRMTDESC").val();
				
			} else {
				var item = $("#" + id + "_ADRMTITEM").val();
				var desc = $("#" + id + "_ADRMTDESC").val();
			}
			
			var obj_srcfund = self.opener.document.getElementById('SRCFUND');
			if (obj_srcfund != null){
				obj_srcfund.value = id;
			}
			////匯款分類項目
			var obj_srcfunddesc = self.opener.document.getElementById('SRCFUNDDESC');
			var show_srcfunddesc = self.opener.document.getElementById('SHOW_SRCFUNDDESC');
			if (obj_srcfunddesc != null) {
				if (id == 'NUL') {
					obj_srcfunddesc.value = '<spring:message code= "LB.X2462" />' + '-' + item;
					show_srcfunddesc.innerHTML = '<spring:message code= "LB.X2462" />' + '-' + item;
					
				} else {
					obj_srcfunddesc.value = id + '-' + item;
					show_srcfunddesc.innerHTML = id + '-' + item;
					
				}
			}
			//匯款分類說明				
			var obj_desc = self.opener.document.getElementById('FXRMTDESC');
			var show_desc = self.opener.document.getElementById('SHOW_FXRMTDESC');
			if (obj_desc != null) {
				obj_desc.value = desc;
				show_desc.innerHTML = desc;
			}
				
			self.close();
		}	
		  
		var index;
	 	//移除常用選單
		  function editMenu(type, rmtid, index1, rmttype) {
			  	var uri = '${__ctx}'+"/FCY/COMMON/fxRemitQueryAj"
				var rdata = {TYPE:type, ADRMTID:rmtid ,index:index,ADRMTTYPE:rmttype};
			  	index = index1;
				var data = fstop.getServerDataEx(uri,rdata,false,callback);
		  }

		function callback(data) {
			if(data !=null && data.result == true ){
				$("#formId").attr("action", "${__ctx}/FCY/COMMON/MyRemitMenu");
				$("#formId").submit();
			}
		}
	</script>
	<title>
		<spring:message code="LB.Title" />
	</title>
</head>

<body>
	<main class="col-12">
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<form method="post" id="formId" action="">
				<h2><spring:message code= "LB.X1691" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
                	<div class="col-12 tab-content">
                		<ul class="ttb-result-list"></ul>
						 <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
								<tr>
									<!-- 分類編號 -->
									<th ><spring:message code= "LB.X1567" /></th>
									<!-- 匯款用途性質 -->
									<th ><spring:message code= "LB.X1692" /></th>
									<!-- 項目 -->
									<th ><spring:message code= "LB.D0271" /></th>
									<!-- 說明 -->
									<th ><spring:message code= "LB.Description_of_page" /></th>
									<!-- 常用選單 -->
									<th ><spring:message code= "LB.Common_menu" /></th>
								</tr>
							</thead>
							<!--         無 資料的處理           --> 
							<c:if test="${empty MyRemitMenu.data.REC}"> 
                            	<tbody> 
                                	<tr> 
                                    	<td></td> 
                                    	<td></td> 
                                    	<td></td> 
                                    	<td></td> 
                                    	<td></td> 
                                	</tr> 
                            	</tbody> 
							</c:if> 
							<!--         有 資料的處理--> 
							<c:if test="${not empty MyRemitMenu.data.REC}"> 
								<tbody>
									<c:forEach  varStatus="loop" var="dataList" items="${MyRemitMenu.data.REC}">
										<c:if test="${dataList.ADRMTID == 'NUL'}">
											<tr>
												<td style="color: #007bff">
													<a href="self" onClick="fillData('${dataList.ADRMTID}');">
														<spring:message code= "LB.X2462" />
													</a>
<% 
    pageContext.setAttribute("dot", "\'");   
%>
	                                 				<c:set var="ADRMTITEM_replace" value="${ fn:replace( dataList.ADRMTITEM, '\"', dot) }" />
	                                 				<c:set var="ADRMTDESC_replace" value="${ fn:replace( dataList.ADRMTDESC, '\"', dot) }" />
													<input type="hidden" id="692_ADRMTITEM" value="${ADRMTITEM_replace}">
													<input type="hidden" id="692_ADRMTDESC" value="${ADRMTDESC_replace}">
												</td>
												<td>${dataList.display_ADRMTTYPE}</td>
												<td>${dataList.ADRMTITEM}</td>
												<td>${dataList.ADRMTDESC}</td>
												<td>  
													<input class="ttb-sm-btn btn-flat-orange" type="button" name="Del_${loop.index}" value="<spring:message code= "LB.X1693" />" 
													onclick="editMenu('D','${dataList.ADRMTID}','${loop.index}','${dataList.ADRMTTYPE}')"></td>
											</tr>
										</c:if>
										<c:if test="${dataList.ADRMTID != 'NUL'}">
											<tr>
												<td style="color: #007bff">
													<a href="self" onClick="fillData('${fn:trim(dataList.ADRMTID)}');">
																${fn:trim(dataList.ADRMTID)}
													</a>
<% 
    pageContext.setAttribute("dot", "\'");   
%>
	                                 				<c:set var="ADRMTITEM_replace" value="${ fn:replace( dataList.ADRMTITEM, '\"', dot) }" />
	                                 				<c:set var="ADRMTDESC_replace" value="${ fn:replace( dataList.ADRMTDESC, '\"', dot) }" />
													<input type="hidden" id="${fn:trim(dataList.ADRMTID)}_ADRMTITEM" value="${ADRMTITEM_replace}">
													<input type="hidden" id="${fn:trim(dataList.ADRMTID)}_ADRMTDESC" value="${ADRMTDESC_replace}">
												</td>
												<td>${dataList.display_ADRMTTYPE}</td>
												<td>${dataList.ADRMTITEM}</td>
												<td>${dataList.ADRMTDESC}</td>
												<td>  
													<input class="ttb-sm-btn btn-flat-orange" type="button" name="Del_${loop.index}" value="<spring:message code= "LB.X1693" />" 
													onclick="editMenu('D','${fn:trim(dataList.ADRMTID)}','${loop.index}','${dataList.ADRMTTYPE}')"></td>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</c:if> 
						</table>
					</div>
				</div>
			</form>
		</section>
	</main> <!-- 		main-content END -->
</body>

</html>