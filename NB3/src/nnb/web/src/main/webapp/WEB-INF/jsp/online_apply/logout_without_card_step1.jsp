<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/jquery.datetimepicker.css">

<script type="text/javascript">
	$(document).ready(function() {
		init();
		initKapImg();
	});
	function init() {
		$("#CMSUBMIT").click( function(e) {
			// 送出進表單驗證前將span顯示
			$("#hideblock").show();
			
			console.log("submit~~");

			// 表單驗證
			if ( !$('#formId').validationEngine('validate') ) {
				e.preventDefault();
			} else {
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				
				//驗證碼驗證
				var capURI = "${__ctx}/CAPCODE/captcha_valided_trans";
				var capData = $("#formId").serializeArray();
				var capResult = fstop.getServerDataEx(capURI,capData,false);
				
				//驗證結果
				if(capResult.result == true){
					// 通過表單驗證
					$("#formId").attr("action","${__ctx}/ONLINE/APPLY/logout_without_card_step2");
					$("#formId").submit();
				}
				else{
					//失敗重新產生驗證碼
					errorBlock(null, null, ["<spring:message code='LB.X1082' />"],
						'<spring:message code= "LB.Quit" />', null);
					changeCode();
				}
			}
		});
	}
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
</script>
</head>

<body>
<!-- 	 header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 無卡提款     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0330" /></li>
    <!-- 線上註銷無卡提款功能     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1584" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.X0331"/></h2>
<!-- 			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i> -->
			<form id="formId" method="post" action="">
				<input type="hidden" name="ACN" value="${logout_without_card_step1.data.ACN}">
				<input type="hidden" name="CUSIDN" value="${logout_without_card_step1.data.CUSIDN}">
				<input type="hidden" name="ISSUER" value="${logout_without_card_step1.data.ISSUER}">
				<input type="hidden" name="UID" value="${logout_without_card_step1.data.UID}">
				<input type="hidden" name="TRFLAG" value="D"/>
				<!-- ---------------------------------------------------------------------------- -->
           
                <div id="step-bar">
                    <ul>
                        <li class="finished"><spring:message code="LB.D1204"/></li>
                        <li class="active"><spring:message code="LB.D1585"/></li>
                        <li class=""><spring:message code="LB.X2060"/></li>
                    </ul>
                </div>
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <p><spring:message code="LB.D1585"/></p>
                            </div>
                            
                            <!-- 註銷帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D1585"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                       <input type="text" name="NAATAPPLY" value="${logout_without_card_step1.data.ACN}" disabled hidden>
                                        <span>${logout_without_card_step1.data.ACN}</span>
                                    </div>
                                </span>
                            </div>
                           
                             <!-- 驗證碼 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Captcha"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <spring:message code="LB.Captcha" var="labelCapCode" />
								<input id="capCode" name="capCode" type="text"
											class="text-input" maxlength="6" autocomplete="off">
								<img name="kaptchaImage" class="verification-img" src="" />
								<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
											onclick="changeCode()" value="<spring:message code="LB.Regeneration_1"/>" />
                                        <span class="input-remarks"><spring:message code="LB.Captcha_refence"/></span>
                                    </div>
                                </span>
                            </div>
                        </div>
                        
                        <button type="button" class="ttb-button btn-flat-orange"name="CMSUBMIT" id="CMSUBMIT"><spring:message code="LB.Confirm"/></button>

                    </div>
                </div>
                <ol class="list-decimal description-list">
                </ol>
				
				
			</form>
		</section>
		</main>
	<!-- 		main-content END -->
	<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>
