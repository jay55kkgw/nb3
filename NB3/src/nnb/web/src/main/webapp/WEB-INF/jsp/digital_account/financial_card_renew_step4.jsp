<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<!-- 變更密碼所需JS -->
<script type="text/javascript"
	src="${__ctx}/component/util/cardReaderChangePin2.js"></script>
<script type="text/javascript"
	src="${__ctx}/component/util/cardReaderChangePin.js"></script>
<script type="text/javascript">
function processQuery()
{
	var main = document.getElementById("formId");
	if(main.CRDTYP.value=="<spring:message code= "LB.D1432" />"){
		main.CRDTYP.value="2";
	} else {
		main.CRDTYP.value="1";
	}
	if(main.SEND.value=="<spring:message code= "LB.D1433" />"){
		main.SEND.value="1";
	} else {
		main.SEND.value="2";
	}
	main.CMSUBMIT.disabled = true;
	main.CRDTYP.disabled=false;
	main.CChargeApply.disabled=false;
	main.NAATAPPLY.disabled=false;
	main.SEND.disabled=false;
	main.CROSSAPPLY.disabled=false;
	main.LMT.disabled=false;
	main.setAttribute("action", '${__ctx}/DIGITAL/ACCOUNT/financial_card_renew_step5'); 
	main.submit();
	
	return false;	 				
} 
//上一頁按鈕
function goBack(){
		// 遮罩
		initBlockUI();
		// 解除表單驗證
		$("#formId").validationEngine('detach');
		// 讓Controller知道是回上一頁
		$('#isBack').val("Y");
		// 回上一頁
		var action = "${__ctx}/DIGITAL/ACCOUNT/financial_card_renew_step3";
		$("#formId").attr("action", action);
		$("#formId").submit();
}
</script>
</head>

<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 數位存款帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0348" /></li>
    <!-- 數位存款帳戶補申請晶片金融卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1553" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 數位存款帳戶補申請晶片金融卡 -->
			<h2>補申請晶片金融卡</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<div id="step-bar">
				<ul>
					<li class="finished">身分驗證</li>
					<li class="finished">顧客權益</li>
					<li class="finished">申請資料</li>
					<li class="active">確認資料</li>
					<li class="">完成申請</li>
				</ul>
			</div>
			<form id="formId" method="post" action="" onSubmit="">
				<input type="hidden" name="isBack" id="isBack" value="">
				<input type="hidden" name="ADOPID" value="NB31"> 
				<input type="hidden" name="CUSIDN" value="${financial_card_renew_step4.data.CUSIDN}"> 
				<input type="hidden" name="ACN" value="${financial_card_renew_step4.data.ACN}"> 
				<input type="hidden" name="TRNTYP" value="02">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<p>確認資料</p>
							</div>
							<div class="classification-block">
								<p>晶片金融卡申請</p>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<!-- 是否申請消費扣款(Smart Pay) -->
									<h4>
										數位存款帳戶
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										${financial_card_renew_step4.data.ACN}
										
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<!-- 金融卡種類 -->
										<h4><spring:message code="LB.D1561"/></h4>
								</label>
								</span> 
								<span class="input-block"> 
									<c:if test="${financial_card_renew_step4.data.CRDTYP=='1'}">
											<!-- 感應式金融卡 -->
											<div class="ttb-input">
												<span><spring:message code="LB.D1432"/></span>
											</div>
											<input type="hidden" name="CRDTYP" value="<spring:message code="LB.D1432"/>"
												disabled>
									</c:if>
									<c:if test="${financial_card_renew_step4.data.CRDTYP=='2'}">
											<!-- 晶片金融卡 -->
											<div class="ttb-input">
												<span><spring:message code="LB.Financial_debit_card"/></span>
											</div>
											<input type="hidden" name="CRDTYP" value="<spring:message code="LB.Financial_debit_card"/>" disabled>
									</c:if>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<!-- 寄送方式 -->
										<h4>領取方式</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<c:if test="${financial_card_renew_step4.data.SEND.equals('1')}">
											<div class="ttb-input">
												<span><spring:message code="LB.D1433"/></span>
											</div>
											
											<input type="hidden" name="SEND" value="<spring:message code="LB.D1433"/>" disabled>
										</c:if>
										<c:if test="${not financial_card_renew_step4.data.SEND.equals('1')}">
											<!-- 親領 -->
											<div class="ttb-input">
												<span><spring:message code="LB.D0072"/></span>
											</div>
											<input type="hidden" name="SEND" value="<spring:message code="LB.D0072"/>" disabled>
										</c:if>
										<span class="input-remarks">選擇郵寄我們將以雙掛號寄至通訊地址；親領則寄至指定之服務分行。</span>
									</div>
								</span>
							</div>
<%-- 							<c:if test="${financial_card_renew_step4.data.SEND.equals('1')}"> --%>
<!-- 							<div class="ttb-input-item row"> -->
<!-- 								<span class="input-title"> <label> -->
<%-- 										<h4><spring:message code="LB.D0061"/></h4> --%>
<!-- 								</label> -->
<!-- 								</span> <span class="input-block"> -->
<!-- 									<div class="ttb-input">  -->
<%-- 										${financial_card_renew_step4.data.POSTCOD} --%>
<%-- 										<input type="hidden" name="POSTCOD" value="${financial_card_renew_step4.data.POSTCOD}" maxlength="10" size="11" disabled> --%>
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
<!-- 							<div class="ttb-input-item row"> -->
<!-- 								<span class="input-title"> <label> -->
<%-- 										<h4><spring:message code="LB.X0353"/></h4> --%>
<!-- 								</label> -->
<!-- 								</span> <span class="input-block"> -->
<!-- 									<div class="ttb-input" > -->
<%-- 										${financial_card_renew_step4.data.MILADR} --%>
<%-- 										<input type="hidden" name="MILADR" value="${financial_card_renew_step4.data.MILADR}" maxlength="10" size="11" disabled> --%>
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
<%-- 							</c:if> --%>
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<!-- 是否申請消費扣款(Smart Pay) -->
										<h4>申請消費扣款限額</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<c:if test="${financial_card_renew_step4.data.CChargeApply.equals('Y')}">
											<div class="ttb-input">
												<span><spring:message code="LB.D0034_2"/></span>
											</div>
											
											<input type="hidden" name="CChargeApply" value="Y" disabled>
										</c:if>
										<c:if
											test="${financial_card_renew_step4.data.CChargeApply.equals('N')}">
											<div class="ttb-input">
												<span><spring:message code="LB.D0034_3"/></span>
											</div>
											<input type="hidden" name="CChargeApply" value="N" disabled>
											<input type="hidden" name="LMT" value="0" disabled>
										</c:if>
									</div>
								</span>
							</div>
							<c:if test="${financial_card_renew_step4.data.CChargeApply.equals('Y')}">
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<!-- 是否申請消費扣款(Smart Pay) -->
										<h4>每日交易限額</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
											
 											<label><spring:message code="LB.X0408"/></label> 
											${financial_card_renew_step4.data.LMT}
											<label><spring:message code="LB.D0088_2"/></label>
											<span class="input-remarks">(每日交易限額以萬元為單位，最高不得超過新台幣10萬元。)</span>
											<input type="hidden" name="LMT" value="${financial_card_renew_step4.data.LMT}" maxlength="10" size="11" disabled>
										
									</div>
								</span>
							</div>
							</c:if>
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<!-- 是否申請非約定帳號轉帳 -->
										<h4>申請非約定帳號轉帳</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
									<c:if test="${financial_card_renew_step4.data.NAATAPPLY=='Y'}">
											<span><spring:message code="LB.D0034_2"/></span>
									</c:if>
									<c:if test="${financial_card_renew_step4.data.NAATAPPLY=='N'}">
											<span><spring:message code="LB.D0034_3"/></span>
									</c:if>
<!-- 									<span class="input-remarks">行動銀行暫不開放約定轉帳功能。</span> -->
									</div>
									<input type="hidden" name="NAATAPPLY" value="${financial_card_renew_step4.data.NAATAPPLY}" disabled>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<!-- 是否跨國交易 -->
										<h4>申請跨國交易</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<c:if test="${financial_card_renew_step4.data.CROSSAPPLY=='Y'}">
											<div class="ttb-input">
												<span><spring:message code="LB.D0034_2"/></span>
											</div>
										</c:if>
										<c:if test="${financial_card_renew_step4.data.CROSSAPPLY=='N'}">
											<div class="ttb-input">
												<span><spring:message code="LB.D0034_3"/></span>
											</div>
										</c:if>
										<input type="hidden" name="CROSSAPPLY" value="${financial_card_renew_step4.data.CROSSAPPLY}" disabled>
									</div>
								</span>
							</div>
						</div>
<%-- 						<input type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.X0194" />" --%>
<!-- 							name="CLOSEPAGE" onClick="open(location, '_self').close();">  -->
						<input type="button" class="ttb-button btn-flat-gray" name="CMBACK"
							id="CMBACK" value="<spring:message code="LB.X0318" />" onclick="goBack()" /> 
						<!-- 確認 -->
						<input type="button"class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT"
							value="確認送出" onclick="processQuery()" />
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>