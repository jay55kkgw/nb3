<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			$('#LOANTYPE').change(function(){
				if($('#LOANTYPE').val()!='#'){
				$('#formId').submit();
				}
			})
		});
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--申請小額信貸 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0543" /></a></li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--申請小額信貸 -->
					<spring:message code= "LB.D0543" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form method="post" id="formId" name="formId" action="${__ctx}/APPLY/CREDIT/apply_creditloan_p2">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 貸款種類 -->
											<h4><spring:message code= "LB.D0544" /></h4>
										</label>
									</span>
									<span class="input-block">
									<div class="ttb-input">
										<select name="LOANTYPE" id="LOANTYPE" class="select-input">
										<!-- 請選擇貸款種類 -->
											<option value="#">----<spring:message code= "LB.D0545" />----</option>
										<!-- 卓越圓夢 -->
											<option value="B"><spring:message code= "LB.D0546" /></option>
										<!-- e網輕鬆貸 -->
											<option value="E"><spring:message code= "LB.D0547" /></option>
										</select>
										</div>
									</span>
								</div>
							</div>
						</div>
					</div>
					</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>