<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<!-- 元件驗證身分JS -->
<%-- 	<script type="text/javascript" src="${__ctx}/component/util/checkIdentity.js"></script> --%>
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
	
	
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	
	<!--   IDGATE -->
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>	
	
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 預約交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X1477" /></li>
    <!-- 預約交易查詢/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Query_Cancel_Scheduled_Transactions" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.Query_Cancel_Scheduled_Transactions" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

<!-- 				資料bar -->
<!-- 				<div id="step-bar"> -->
<!-- 					<ul> -->
<!-- 						<li class="finished">輸入資料</li> -->
<!-- 						<li class="active">確認資料</li> -->
<!-- 						<li class="">交易完成</li> -->
<!-- 					</ul> -->
<!-- 				</div>	 -->
				<form method="post" id="formId" action="${__ctx}/NT/ACCT/RESERVATION/reservation_result"> 
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<span><spring:message code="LB.Cancel_scheduled_transaction" /></span>
							</div>
							<div class="ttb-input-item row">
							<c:set var="dataSet" value="${ reservation_confirm.data.dataSet }" />
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Booking_number" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPSCHNO}</span>		
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Period" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPPERMTDATE}</span>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Effective_date" />
										-
										<spring:message code="LB.Deadline" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
										    ${dataSet.DPFDATE} 
						                	<c:if test="${not empty dataList.DPTDATE}">
						                	-${dataList.DPTDATE}
						                	</c:if>
						                </span>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Next_transfer_date" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPNEXTDATE}</span>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Payers_account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPWDAC}</span>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4>
										<spring:message code="LB.Payees_account_no" />
										/
										<spring:message code="LB.Pay_taxes_fee_code" />
									</h4></label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${dataSet.DPSVBH}
											${dataSet.DPSVAC}
										</span>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Amount" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPTXAMT}</span>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transaction_type" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.TXTYPE}</span>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Note" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPTXMEMO}</span>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${dataSet.DPTXCODES}
											<c:if test="${dataSet.DPTXCODE == '0'}">
						                		<input name="pinnew" id="pinnew" type="password" class="text-input  validate[required] " maxlength="8" value="">
											</c:if>
											<c:if test="${dataSet.DPTXCODE == '7'}">
						                		<font><spring:message code="LB.X2502" /></font>
											</c:if>
										</span>
									</div>
								</span>
							</div>
							<!-- 如果是非約定則不能使用IKEY，只能選晶片金融卡，使用者選擇晶片金融卡後才顯示驗證碼-->
							<div class="ttb-input-item row" id="chaBlock" style="display:none">
								<!-- 驗證碼 -->
								<span class="input-title">
									<label>
										<h4><spring:message code= "LB.Captcha" /></h4>
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text"
										class="text-input" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class = "verification-img" src="" />
									<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
										onclick="changeCode()" value="<spring:message code= "LB.Regeneration" />" />
								</span>
							</div>
						</div>
						<input name="DPSCHID" type="hidden" value="${dataSet.DPSCHID}" />
						<input name="DPSCHNO" type="hidden" value="${dataSet.DPSCHNO}" />
			            <input name="DPPERMTDATE" type="hidden" value="${dataSet.DPPERMTDATE}" />
			            <input name="DPFDATE" type="hidden" value="${dataSet.DPFDATE}" />
			            <input name="DPTDATE" type="hidden" value="${dataSet.DPTDATE}" />
			            <input name="DPNEXTDATE" type="hidden" value="${dataSet.DPNEXTDATE}" />
			            <input name="DPWDAC" type="hidden" value="${dataSet.DPWDAC}" />
			            <input name="DPSVBH" type="hidden" value="${dataSet.DPSVBH}" />
			            <input name="DPSVAC" type="hidden" value="${dataSet.DPSVAC}" />
			            <input name="DPTXAMT" type="hidden" value="${dataSet.DPTXAMT}" />
			            <input name="TXTYPE" type="hidden" value="${dataSet.TXTYPE}" />
			            <input name="DPTXMEMO" type="hidden" value="${dataSet.DPTXMEMO}" />
			            <input name="DPTXCODE" id="DPTXCODE" type="hidden" value="${dataSet.DPTXCODE}" />                           
			            <input name="FGTXWAY" id="FGTXWAY" type="hidden" value="${dataSet.DPTXCODE}" />                           
			            <input name="DPTXCODES" type="hidden" value="${dataSet.DPTXCODES}" />                           
			            <input name="PINNEW" id="PINNEW" type="hidden" value="">
			            <!--  ICcard -->
			            <input name="CMCARD" id="CMCARD" type="hidden" value="2">
						<!--  i-Key -->
			            <input type="hidden" id="jsondc" name="jsondc" value='${reservation_confirm.data.jsondc}'>
			            <input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
			            <input type="hidden" id="ISSUER" name="ISSUER" value="">
						<input type="hidden" id="ACNNO" name="ACNNO" value="">
						<input type="hidden" id="TRMID" name="TRMID" value="">
						<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
						<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
						<input type="hidden" id="TAC" name="TAC" value="">
<%-- 						<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray no-l-disappear-btn" value="<spring:message code="LB.Re_enter" />" />	 --%>
						<input id="previous" name="previous" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						<input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />						
<!-- 				</form> -->
						</div>	
					</div>			
				</form>	
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->

	

	<%@ include file="../index/footer.jsp"%>
	
	<script type="text/javascript">
	var notCheckIKeyUser = false; // 不驗證是否是IKey使用者
	var urihost = "${__ctx}";
	var idgatesubmit= $("#formId");
	$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 100);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			
// 		console.log("${ reservation_confirm.data.dataSet }");		
	});

    function init(){
    	//初始化表單
    	$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});	
    	
		$("#CMSUBMIT").click(function(e){		
			
			e = e || window.event;
			
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
			}
			else{
 				$("#formId").validationEngine('detach');				
 				processQuery(); 
 			}		
  		});
	
		//上一頁按鈕
		$("#previous").click(function() {
			initBlockUI();
			fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/RESERVATION/reservation_detail','', '');
		});

		chaBlock();
   	}
    
		// 交易機制選項
		function processQuery(){
			var fgtxway = $('#DPTXCODE').val();
			console.log("fgtxway: " + fgtxway);
		
			switch(fgtxway) {
				case '0':
					$('#PINNEW').val(pin_encrypt($('#pinnew').val()));
					initBlockUI();
					$("#formId").attr("action","${__ctx}/NT/ACCT/RESERVATION/reservation_result");
					$("#formId").submit(); 
	// 				alert("交易密碼(SSL)...");
					break;
					
				case '1':
	// 				alert("IKey...");
					// IKEY
					useIKey();
					
					break;
					
				case '2':
	// 				alert("晶片金融卡");
					// 晶片金融卡
					console.log("urihost: " + urihost);
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					$("#CMCARD").attr("checked","checked");
					useCardReader(capUri);
			    	break;
			    	
				case '7'://IDGATE認證
					idgatesubmit= $("#formId");
					showIdgateBlock();
					break;
			    	
				default:
					//alert("nothing...");
					errorBlock(
							null, 
							null,
							["nothing..."], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}		
		}

	 	// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('#DPTXCODE').val();
 			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			if(fgtxway == '2'){
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").show();
			}else{
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").hide();
			}
	 	}

		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}
    </script>
</body>
</html>