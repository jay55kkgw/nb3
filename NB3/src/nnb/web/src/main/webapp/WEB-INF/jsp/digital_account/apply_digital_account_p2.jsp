<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp" %> 
<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>
<!-- 交易機制所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/checkIdentity.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<!-- 讀卡機所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
<script type="text/javascript">
	var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
// 	var myobj = null; // 自然人憑證用
	var urihost = "${__ctx}";
	$(document).ready(function() {
		// HTML載入完成後0.1秒開始遮罩
		setTimeout("initBlockUI()", 100);

		// 初始化自然人元件
		hideNpcDiv('N');
		// 初始化跨行金融帳戶認證
		hideFiscDiv('N');
		// 初始化本行金融帳戶認證
		hideLoFiscDiv('Y');
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		// 銀行代碼
		setTimeout("creatDpBHNO()",500);
		// 出生日期
		setTimeout("genDateList()", 500);
		
		setTimeout("init()", 20);
		
		// 過3秒解遮罩
		setTimeout("unBlockUI(initBlockId)", 3000);
		
	});
	
// 	var isconfirm_a = false;
// 	$("#errorBtn1").click(function(){
// 		if(!isconfirm_a)
// 			return false;
// 		isconfirm_a = false;
// 		$("#naturalComponent")[0].click();
// 	});
// 	$("#errorBtn2").click(function(){
// 		isconfirm_a = false;
// 		$('#error-block').hide();
// 	});
// 	// 初始化自然人元件
// 	function initNatural() {
// 		var Brow1 = new ckBrowser1();
// 		if(!Brow1.isIE){	
// 			if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {
// 				myobj = initCapiAgentForChrome();
// 				try {
// 					if(sessionToken == null){
// 						myobj.initChrome();
// 						console.log("myobj.initChrome...");
// 					}
// 				} catch (e) {
// // 					if(confirm("<spring:message code= "LB.X1216" />")){
// // 						$("#naturalComponent")[0].click();
// // 					}
// 					 isconfirm = true;
// 						errorBlock(
// 								null, 
// 								null,
// 								['<spring:message code= "LB.X1216" />'], 
// 								'<spring:message code= "LB.Confirm" />', 
// 								'<spring:message code= "LB.Cancel" />'
// 							);
// 				}			
// 			}
// 		} else {
// 			var strObject = "";
// 	    	// 使用 Script 判斷 win64 或 win32, 執行時間有點久
// 	 		if (navigator.platform.toLowerCase() == "win64"){
// 				// "Windows 64 位元";
// 	 			document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATLx64.cab" width="0px" height="0px"></object>';
// 			} else {
// 				// "Windows 32 位元 或 模擬 64 位元(WOW64)";
// 				document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATL.cab" width="0px" height="0px"></object>';
// 			}
// 		    myobj = document.myobj;
// 		}
// 		console.log("initNatural.finish...");
// 	}
	/**
	 * 初始化BlockUI
	 */
	function initBlockUI() {
		initBlockId = blockUI();
	}

	// 交易機制選項
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
	
		switch(fgtxway) {
			case '0':
//					alert("交易密碼(SSL)...");
    			$("form").submit();
				break;				
			case '1':
//					alert("IKey...");
				var jsondc = $("#jsondc").val();
				
				// 遮罩後不給捲動
//					document.body.style.overflow = "hidden";

				// 呼叫IKEY元件
//					uiSignForPKCS7(jsondc);
				
				// 解遮罩後給捲動
//					document.body.style.overflow = 'auto';
				
				useIKey();
				break;				
			case '2':
//					alert("晶片金融卡");

				// 遮罩後不給捲動
//					document.body.style.overflow = "hidden";
				
				// 呼叫讀卡機元件
				useCardReader();

				// 解遮罩後給捲動
//					document.body.style.overflow = 'auto';
				
		    	break;		    	
			case '4':
//				自然人憑證
				$('#UID').val($('#CUSIDN').val());
				useNatural();
		    	break;
			case '5':
				//跨行帳戶驗證
				var uri = '${__ctx}'+"/FISC/InterBank/verify_account_aj";
				var rdata = $("#formId").serializeArray();
				callInterBankVerifyAcc(uri, rdata, "InterBankVerifyOTP","refreshCapCode","validateMaskInterBankAcc");
				break;	  
			case '6':  	
				//本行帳戶驗證
				var uri = '${__ctx}'+"/DIGITAL/ACCOUNT/verify_lo_account_aj";
				var rdata = $("#formId").serializeArray();
				callInterBankVerifyAcc(uri, rdata, "LoBankVerifyOTP","refreshCapCode", "validateMaskLoBankAcc");
				break;	  
			default:
				alert("nothing...");
		}
		
	}

    function init(){
    	$("#formId").validationEngine({
			validationEventTriggers:'keyup blur', 
			binded:true,
			scroll:true,
			addFailureCssClassToField:"isValid",
			promptPosition: "inline" });
    	$("#CMSUBMIT").click(function(e){			
			console.log("submit~~");
			var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
			var capData = $("#formId").serializeArray();
			var capResult = fstop.getServerDataEx(capUri, capData, false);
			$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
			
    		//出生日期 資料整理 ，填入BIRTHDAY隱藏欄位
    		$("#BIRTH").val($("#BIRTHDATEYY").val() + $("#BIRTHDATEMM").val() + $("#BIRTHDATEDD").val());
    		
			if (capResult.result) {
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();	 				
    				var action = '${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_p3';
	    			$("#formId").attr("action", action);
	    			unBlockUI(initBlockId);
	    			processQuery();
	    			
	 			}
			} else {
// 				alert("<spring:message code= "LB.X1082" />");
				var b = ["<spring:message code= "LB.X1082" />"];
				errorBlock(null, null, b, '確定', null);
				changeCode();
			}
		});
		$("#CMBACK").click( function(e) {
			$("#formId").validationEngine('detach');
			console.log("submit~~");
			$("#formId").attr("action", "${__ctx}/DIGITAL/ACCOUNT/apply_digital_account");
			$("#back").val('Y');

         	initBlockUI();
            $("#formId").submit();
		});
		
		$("#CMNPC").click( function(e) {
			hideNpcDiv('Y');
			hideFiscDiv('N');
			hideLoFiscDiv('N');
		});

		$("#CMCARD").click( function(e) {
			hideNpcDiv('N');
			hideFiscDiv('N');
			hideLoFiscDiv('N');
		});

		$("#CMFISCACC").click( function(e) {
			hideNpcDiv('N');
			hideFiscDiv('Y');
			hideLoFiscDiv('N');
		});

		$("#LOISCACC").click( function(e) {
			hideNpcDiv('N');
			hideFiscDiv('N');
			hideLoFiscDiv('Y');
		});
		
		showNature();
    }	
    
	// 刷新驗證碼
	function refreshCapCode() {
		console.log("refreshCapCode...");

		// 驗證碼
		$('input[name="capCode"]').val('');
		
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 刷新輸入欄位
	function changeCode() {
		console.log("changeCode...");
		
		// 清空輸入欄位
		$('input[name="capCode"]').val('');
		
		// 刷新驗證碼
		refreshCapCode();
	}

	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}

	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			refreshCapCode();
		});
	}
		
	function hideNpcDiv(flag){
		console.log(flag);
		if(flag == 'Y'){
			$("#NPCDIV").show();
		}else{
			$("#NPCDIV").hide();
		}
	}

	function hideFiscDiv(flag){
		console.log(flag);
		if(flag == 'Y'){
			$("#FISCDIV").show();
		}else{
			$("#FISCDIV").hide();
		}
	}
	function hideLoFiscDiv(flag){
		console.log(flag);
		if(flag == 'Y'){
			$("#LOFISCDIV").show();
		}else{
			$("#LOFISCDIV").hide();
		}
	}
	
	//取得卡片主帳號結束
	function getMainAccountFinish(result){
		//成功
		if(result != "false"){
			var cardACN = result;
			if(cardACN.length > 11){
				cardACN = cardACN.substr(cardACN.length - 11);
			}
			var UID = $("#CUSIDN").val();
//			var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//			var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
			
			var uri = urihost+"/COMPONENT/component_without_id_aj";
			var rdata = { ACN: cardACN ,UID: UID };
			fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
		}
		//失敗
		else{
			showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",false);
		}
	}
	
	function showNature() {
		$('input[type=radio][name=FGTXWAY]').change(function() {
			if (this.value == '2') {
				// 晶片金融卡
				component_init();
			}
			if (this.value == '4') {
				// 自然人憑證
				initBlockUI(); 
				setTimeout("initNatural_EXCUTE()", 50);
			}
		});
	}

	//	建立銀行代號下拉選單
	function creatDpBHNO(){
		//宣告銀行物件 
		var bkdataList = {}
		//2021/05/13 ， 業務單位email通知 
		//非同步名單，若要新增參加銀行，必須請數金部給需求，並依資訊部行程上線，非急緊上線！
		//要排除自行驗證050(台企銀)。
		//update 2022/03/10	H571110000111	
		var accedingBK = ['004','005','006','007','008','009','011', '012','013','016','017',
									'021','048','052','054','101','102','103','108','114','118','132','146', '147','162','216','600','803',
									'805','806','807','808','809','810','812','815','816','822','824','826','952'];
		//var excludeBk = ['000','001','050'];
		
		var uri = '${__ctx}'+"/FISC/InterBank/getDpBHNO_aj";
		var rdata = {type: 'dpbhno' };
		var options = { keyisval:true ,selectID:'#DPBHNO'}
		var data = fstop.getServerDataEx(uri,rdata,false);
		if(data !=null && data.result == true ) {			
			//建立選單資料 
			$.each(data.data,function(value,text){
				if (accedingBK.indexOf(text.substr(0,3)) > -1) {					
					bkdataList[value] = text;
				}
			});
			fstop.creatSelect(bkdataList,options);			
		}
		
		$( "#DPBHNO" ).change(function() {
			var acno = $('#DPBHNO :selected').val();
			console.log("DPBHNO.acno>>"+acno);
			
			$( "#ATTIBK" ).val(acno.substr(0,3));
		});
	}

	function  InterBankVerifyOTP(data) {
		initBlockUI(); 
		$("#otpCommit").attr("disabled", true);
		$('#otp_error').hide();
		console.log(data);

		var uri = '${__ctx}'+"/FISC/InterBank/otpverify_aj";
		var rdata = {
				OTP : data,
				CUSIDN : $("#CUSIDN").val()
			};
		
		fstop.getServerDataEx(uri, rdata, true,checkOTPFish);
	}

	function resendOTP() {
		sec = 120;
		countDown();
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		if(fgtxway == "5"){
			var uri = '${__ctx}'+"/FISC/InterBank/otpSend_aj";
			var rdata = {
					ATMOBI : $("#ATMOBI").val(),
					CUSIDN : $("#CUSIDN").val()
				};
			fstop.getServerDataEx(uri,rdata,false);
		}else{
			var uri = '${__ctx}'+"/DIGITAL/ACCOUNT/verify_lo_account_aj";
			var rdata = {
					LOATTIAC : $("#LOATTIAC").val(),
					CUSIDN : $("#CUSIDN").val()
				};
			fstop.getServerDataEx(uri,rdata,false);
		}
	}

	function checkOTPFish(otpresult) {
		unBlockUI(initBlockId);
		$("#otpCommit").attr("disabled", false);
		if (otpresult.result) {
			closeOTPDialog();
			initBlockUI();
			try{
				$("#OTPTOKEN").val(otpresult.data.otptoken);
			}catch(e){
				console.log(e);
			}
			$("#formId").submit();
		} else {
			$('#otp_error').show();
		}
		
	}
	
	function LoBankVerifyOTP(data){
		initBlockUI(); 
		$("#otpCommit").attr("disabled", true);
		$('#otp_error').hide();
		console.log(data);

		var uri = '${__ctx}'+"/DIGITAL/ACCOUNT/otpverify_lo_account_aj";
		var rdata = {
				SMSOTP : data,
				CUSIDN : $("#CUSIDN").val()
			};
		
		fstop.getServerDataEx(uri, rdata, true,checkOTPFish);
	}

	//出生日期
	function genDateList(){
		var today = new Date();
		var y = today.getFullYear();
		var m = today.getMonth() + 1;
		var d = today.getDate();
		var CCBIRTHDATEYY = "${ result_data.data.CCBIRTHDATEYY }";
		var CCBIRTHDATEMM = "${ result_data.data.CCBIRTHDATEMM }";
		var CCBIRTHDATEDD = "${ result_data.data.CCBIRTHDATEDD }";
		for(var i = y;i >= 1;i--){
			j = i.toString();
			for(k = 0;k <= 2 - i.toString().length;k++)j='0' + j;
			if(CCBIRTHDATEYY == i){
				$("#BIRTHDATEYY").append("<option value='" + j + "' selected>" + "<spring:message code= "LB.B0023" />" + i + "<spring:message code= "LB.Year" />" + "</option>");
			}else{
				$("#BIRTHDATEYY").append("<option value='" + j + "'>" + "<spring:message code= "LB.B0023" />" + i + "<spring:message code= "LB.Year" />" + "</option>");
			}
		}
		for(var i = 1;i <= 12;i++){
			j = i.toString();
			for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
			if(CCBIRTHDATEMM == i){
				$("#BIRTHDATEMM").append("<option value='" + j + "' selected>" + i + "<spring:message code= "LB.Month" />" + "</option>");
			}else{
				$("#BIRTHDATEMM").append("<option value='" + j + "' >" + i + "<spring:message code= "LB.Month" />" + "</option>");
			}
		}
		for(var i = 1;i <= 31;i++){
			j = i.toString();
			for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
			if(CCBIRTHDATEDD == i){
				$("#BIRTHDATEDD").append("<option value='" + j + "' selected>" + i + "<spring:message code= "LB.Day" />" + "</option>");
			}else{
				$("#BIRTHDATEDD").append("<option value='" + j + "' >" + i + "<spring:message code= "LB.Day" />" + "</option>");
			}
		}
	}
	//銀行帳號限制字數	
	function maxSizeCode(){
	  
	  $("#ATTIAC").keyup(function () {
            if ($(this).val().length > $(this).attr("maxlength")) $(this).val($(this).val().slice(0, $(this).attr("maxlength")));
        }); 
	}
	

</script>

<style>
	@media screen and (max-width:767px) {
		.ttb-button {
				width: 38%;
				left: 36px;
		}
		#CMBACK.ttb-button{
				border-color: transparent;
				box-shadow: none;
				color: #333;
				background-color: #fff;
		}
	}
</style>
</head>
<body>
	<div id="obj"></div>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component_u2.jsp"%>
	<%@ include file="../component/otp_component.jsp"%>
	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!-- 線上開立數位存款帳戶     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1361" /></li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--基金線上預約開戶作業 -->
<!-- 					線上開立數位存款帳戶 -->
					<spring:message code="LB.D1361"/>
				</h2>
                <div id="step-bar">
                    <ul>
                        <li class="finished">注意事項與權益</li>
                        <li class="active">開戶認證</li>
                        <li class="">開戶資料</li>
                        <li class="">確認資料</li>
                        <li class="">完成申請</li>
                    </ul>
                </div>
				<form method="post" id="formId" name="formId">
					<input type="hidden" name="ADOPID" value="N203">
					<input type="hidden" name="NOTIEINSTALL" id="NOTIEINSTALL" value="">
					<input type="hidden" name="AUTHCODE" id="AUTHCODE" value="">
					<input type="hidden" name="AUTHCODE1" id="AUTHCODE1" value="">
					<input type="hidden" name="CertFinger" id="CertFinger" value="">
					<input type="hidden" name="CertB64" id="CertB64" value="">
					<input type="hidden" name="CertSerial" id="CertSerial" value="">
					<input type="hidden" name="CertSubject" id="CertSubject" value="">
					<input type="hidden" name="CertIssuer" id="CertIssuer" value="">
					<input type="hidden" name="CertNotBefore" id="CertNotBefore" value="">
					<input type="hidden" name="CertNotAfter" id="CertNotAfter" value="">
					<input type="hidden" name="HiCertType" id="HiCertType" value="">
					<input type="hidden" name="CUSIDN4" id="CUSIDN4" value="">
					<input type="hidden" name="pkcs7Sign" id="pkcs7Sign" value="">
					<input type="hidden" name="jsondc" id="jsondc" value='${ input_data.jsondc }'>	
					<input type="hidden" name="CHIP_ACN" id="CHIP_ACN" value="">
					<input type="hidden" name="UID" id="UID" value="">
					<input type="hidden" name="ACN" id="ACN" value="">
					<input type="hidden" name="ISSUER" id="ISSUER" value="">
					<input type="hidden" name="ACNNO" id="ACNNO" value="">
					<input type="hidden" name="OUTACN" id="OUTACN" value="">
					<input type="hidden" name="iSeqNo" id="iSeqNo" value="">
					<input type="hidden" name="ICSEQ" id="ICSEQ" value="">
					<input type="hidden" name="TAC" id="TAC" value="">
					<input type="hidden" name="TRMID" id="TRMID" value="">
					<input type="hidden" name="TYPE" value="10">
					<input type="hidden" name="ATTIBK" id="ATTIBK" value="004">
					<input type="hidden" name="BIRTH" id="BIRTH" value="">
					<input type="hidden" name="OTPTOKEN" id="OTPTOKEN" value="">
					
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<!--請輸入身分證字號-->
							<div class="ttb-input-block">
	                            <div class="ttb-message">
	                                <p>開戶認證</p>
	                            </div>
	                            <p class="form-description">請您輸入您的身分證字號與開戶認證的方式</p>
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.B0013"/></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                          <input class="text-input validate[required, funcCall[validate_checkSYS_IDNO[CUSIDN]]]" type="text" name="CUSIDN" id="CUSIDN" placeholder="<spring:message code="LB.D0025"/>" maxlength="10" size="11">
	                                    </div>
	                                </span>
	                            </div>                  
								<!--交易機制  SSL 密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.B0001" /></h4>
										</label>
									</span>
									<span class="input-block"> 
										<!-- 自然人憑證 -->
										<div class="ttb-input">
											<label class="radio-block tbb-disabled" for="CMNPC" onclick=""> 
												<spring:message code="LB.D0437" /> <span class="d-inline-block d-lg-none">(限桌機)</span>
												<input type="radio" name="FGTXWAY" id="CMNPC" value="4"> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 晶片金融卡 -->
										<div class="ttb-input">
											<label class="radio-block tbb-disabled" for="CMCARD" onclick=""> 
												<spring:message code="LB.Financial_debit_card" /> <span class="d-inline-block d-lg-none">(限桌機)</span>
												<input type="radio" name="FGTXWAY" id="CMCARD" value="2"> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 本行金融帳戶認證 -->
										<div class="ttb-input">
											<label class="radio-block" for="LOISCACC" onclick=""> 
												自行帳戶+手機簡訊驗證
												<input type="radio" name="FGTXWAY" id="LOISCACC" value="6" checked="checked"> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 跨行金融帳戶認證 -->
										<div class="ttb-input">
											<label class="radio-block" for="CMFISCACC" onclick=""> 
												<spring:message code="LB.Fisc_inter-bank_acc" /> 
												<input type="radio" name="FGTXWAY" id="CMFISCACC" value="5" > 
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<div id="NPCDIV">
									<!-- 自然人憑證PIN碼 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.D1540" /></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<input type="PASSWORD" class="text-input" maxLength="8" size="10" id="NPCPIN" name="NPCPIN" value="" />
											</div>
										</span>
									</div>
								</div>
								<div id="LOFISCDIV">
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>存款帳號</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<input type="number"  inputmode="numeric" class="text-input validate[required, maxSize[11]]" 
													onkeyup="maxSizeCode()" name="LOATTIAC" id="LOATTIAC" value="" 
													placeholder="<spring:message code="LB.B0018" />" size="11" maxlength="11"  autocomplete="off"> 
											</div>
										</span>
									</div>
									
								</div>
								<div id="FISCDIV">
									<!-- 跨行金融帳戶認證 -->
									<!-- 存款銀行 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.B0015" /> <span class="high-light">*</span>
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<select id="DPBHNO" name="DPBHNO" class="custom-select select-input half-input validate[required]">
                                                    <option value="">
														<spring:message code="LB.B0017" />
													</option>
                                                </select>
											</div>
										</span>
									</div>
									<!-- 存款帳號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.B0016" /> <span class="high-light">*</span>
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<input type="number"  inputmode="numeric" class="text-input validate[required, maxSize[16]]" onkeyup="maxSizeCode()" name="ATTIAC" id="ATTIAC" value="" placeholder="<spring:message code="LB.B0018" />" size="16" maxlength="16"  autocomplete="off"> 
											</div>
										</span>
									</div>
                                    <!-- 行動電話 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.D0069" /> <span class="high-light">*</span></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<input type="tel" inputmode="numeric" class="text-input validate[funcCallRequired[validate_cellPhone[手機號碼格式錯誤,ATMOBI]]]" placeholder="例如：0981212123" maxLength="10" size="11" id="ATMOBI" name="ATMOBI" value="" />
												<span class="input-remarks">請輸入留存於他行的手機號碼</span>
											</div>
										</span>
									</div>
									<!-- 出生日期 -->
									<div class="ttb-input-item row">
                                        <span class="input-title">
											<label>
												<h4><spring:message code= "LB.D0582" /> <span class="high-light">*</span></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<!-- 年 -->
												<select name="BIRTHDATEYY" id="BIRTHDATEYY" class="custom-select select-input input-width-100 validate[required]">
													<option value="#">
														---
													</option>
												</select>
												<!-- 月 -->
												<select name="BIRTHDATEMM" id="BIRTHDATEMM" class="custom-select select-input input-width-60 validate[required]">
													<option value="#">
														---
													</option>
												</select>
												<!-- 日 -->
												<select name="BIRTHDATEDD" id="BIRTHDATEDD" class="custom-select select-input input-width-60 validate[required]">
													<option value="#">
														---
													</option>
												</select>
 												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_BIRTHDATE" >
 													<!-- 驗證用的input -->
													<input id="validate_BIRTHDATE" name="validate_BIRTHDATE" type="text" class="text-input" style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
												<input id="validate_BIRTHDATEYY" name="validate_BIRTHDATEYY" type="text" class="
													validate[funcCallRequired[validate_CheckSelect[年,BIRTHDATEYY,#]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												<input id="validate_BIRTHDATEMM" name="validate_BIRTHDATEMM" type="text" class="
													validate[funcCallRequired[validate_CheckSelect[月,BIRTHDATEMM,#]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												<input id="validate_BIRTHDATEDD" name="validate_BIRTHDATEDD" type="text" class="
													validate[funcCallRequired[validate_CheckSelect[日,BIRTHDATEDD,#]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</div>
										</span>
									</div>								
								</div>
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Captcha" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.Captcha" var="labelCapCode" />
											<input id="capCode" type="text" class="text-input input-width-125" name="capCode" placeholder="${labelCapCode}" maxlength="6" 
											onkeyup="value=value.replace(/[^\u0000-\u00ff]/g,'')"onpaste="value=value.replace(/[^\u0000-\u00ff]/g,'')"autocomplete="off">
										
											 
											<img name="kaptchaImage" src="" class="verification-img" /> 
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration"/>"/> 
											<!-- <span class="input-remarks">請注意：英文不分大小寫，限半形字元</span> -->
										</div>
									</span>
								</div>
							</div>
							<!-- 確定 -->
							<input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318"/>" onClick="back();" />
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

<script type="text/javascript">
	function checkTC(){
		component_isikeyuser(); // 判斷使用者是否ikey使用者
		component_version(); // 取得各元件的最新版本號
		component_platform(); // 取得裝置作業系統
		component_initKeyBoard(); // 動態鍵盤初始化
		// setTimeout("component_init()", 50);
	}
</script>

</html>