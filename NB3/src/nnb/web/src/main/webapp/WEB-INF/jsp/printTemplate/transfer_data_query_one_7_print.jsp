<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/><br/><br/>
	
	<table class="print">
		<tr>
			<!-- 分割日期 -->
			<th><spring:message code="LB.W0996" /></th>
			<th>${TRADEDATE_F }</th>
		</tr>
		<tr>
			<!-- 信託編號 -->
			<th><spring:message code="LB.W0904" /></th>
			<th>${CDNO_F }</th>
		</tr>
		<tr>
			<!-- 基金名稱 -->
			<th><spring:message code="LB.W0025" /></th>
			<th>${TRANSCODE_FUNDLNAME }</th>
		</tr>
		<tr>
			<!--  原信託單位數 -->
			<th> <spring:message code="LB.W0997" /></th>
			<th>${UNIT_F }</th>
		</tr>
		<tr>
			<!-- 單位數分割率 -->
			<th><spring:message code="LB.W0998" /></th>
			<th>${AMT6_F }</th>
		</tr>
		<tr>
			<!-- 分割後信託單位數 -->
			<th><spring:message code="LB.W0999" /></th>
			<th>${TXUNIT_F }</th>
		</tr>
	</table>
	<br/><br/><br/><br/>
</body>
</html>