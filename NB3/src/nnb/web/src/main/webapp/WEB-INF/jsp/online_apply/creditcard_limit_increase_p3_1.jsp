<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>
<!-- DIALOG會用到 -->
<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
	// 	setTimeout("initBlockUI()", 10);
		initBlockUI();
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
		
	function init(){
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		$("#back").val("");
		
		$("#CMSUBMIT").click(function(e){
			
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
	    		e.preventDefault();
	    	}
			else{
				$("#formId").attr("action","${__ctx}/ONLINE/APPLY/creditcard_limit_increase_confirm");
				$("#formId").submit();
			}
		});
		
		$("#CMBACK").click(function(e){
			$("#formId").validationEngine('detach');
			$("#back").val("Y");
			$("#formId").attr("action","${__ctx}/ONLINE/APPLY/creditcard_limit_increase_p3");
// 			$("#formId").attr("action","${__ctx}/ONLINE/APPLY/creditcard_limit_increase_back");
			$("#formId").submit();
		});
		uploadSetting();
	}
	
	function onUpLoad(fileFilter,imgFilter,type,postFile) {
		var BD = '${limit_increase.data.back}';
		if(document.getElementById(type + "_error")){
			$("#" + type + "_error").remove();
		}
		
	    var fileData = new FormData();

	    var file1 = $(fileFilter).get(0);
	    if (file1.files.length > 0) {
	        fileData.append("picFile", file1.files[0]);
	    }
	    fileData.append("type",type);
	    fileData.append("oldPath",$(postFile).val());
	    fileData.append("CUSIDN",$("#CUSIDN").val());
	    fileData.append("filetime",$("#filetime").val());
	    if('${limit_increase.data.RESULT}' != 'N'){
	    	//預審
	    	fileData.append("PRE","PRE");
	    }
	    if (file1.files.length == 0 ) {
//	         alert("<spring:message code= "LB.Alert052" />");
			$(imgFilter+'er').text("<spring:message code='LB.Alert052' />");
	        return;
	    }

	    $.ajax({
	        url: "${__ctx}/ONLINE/APPLY/apply_creditcard_limit_fileupload",
	        type: "POST",
	        contentType: false,  // Not to set any content header
	        processData: false,  // Not to process data
	        data: fileData,
	        success: function (res) {
	        	console.log(res);
	            if(res.data.validated){
//	                 alert("檔案匯入成功");   
// 	                $(imgFilter).attr('src',"${__ctx}/upload/" + res.data.url + "?timestamp=" + new Date().getTime());
	                $(imgFilter).val(res.data.url);
	                $(imgFilter+'er').text("");
	                $(postFile).val(res.data.url);
	                $(imgFilter+"_close_btn").show();
	            } else {
//	             	alert(res.data.summary);  
// 					$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
					$(imgFilter+'er').text(res.data.summary);
	                $(imgFilter+"_close_btn").hide();
	            }
	        }
	    });
	}
	//拖移用
	function onUpLoadByDrop(file1,imgFilter,type,postFile) {
		var BD = '${limit_increase.data.back}';
		if(document.getElementById(type + "_error")){
			$("#" + type + "_error").remove();
		}
		
	    var fileData = new FormData();
	    if (file1.length > 0) {
	        fileData.append("picFile", file1[0]);
	    }
	    fileData.append("type",type);
	    fileData.append("oldPath",$(postFile).val());
	    fileData.append("CUSIDN",$("#CUSIDN").val());
	    fileData.append("filetime",$("#filetime").val());
	    if('${limit_increase.data.RESULT}' != 'N'){
	    	//預審
	    	fileData.append("PRE","PRE");
	    }
	    if (file1.length == 0 ) {
			$(imgFilter+'er').text("<spring:message code='LB.Alert052' />");
	        return;
	    }

	    $.ajax({
	        url: "${__ctx}/ONLINE/APPLY/apply_creditcard_limit_fileupload",
	        type: "POST",
	        contentType: false,  // Not to set any content header
	        processData: false,  // Not to process data
	        data: fileData,
	        success: function (res) {
	        	console.log(res);
	            if(res.data.validated){
// 	                $(imgFilter).attr('src',"${__ctx}/upload/" + res.data.url + "?timestamp=" + new Date().getTime());
	                $(imgFilter).val(res.data.url);
	                $(imgFilter+'er').text("");
	                $(postFile).val(res.data.url);
	                $(imgFilter+"_close_btn").show();
	            } else {
//	             	alert(res.data.summary);  
// 					$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
					$(imgFilter+'er').text(res.data.summary);
	                $(imgFilter+"_close_btn").hide();
	            }
	        }
	    });
	}

	//移除上傳圖片
	function deleteUpLoad(imgFilter,postFile,orgImg){
	    console.log($(postFile).val());
	    $.ajax({
	        url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_fileupload_delete",
	        type: "POST",
	        data: { 
	        	Path:$(postFile).val()
	        },
	        success: function (res) {
	        	console.log(res);
	            if(res.data.sessulte){
// 	                $(imgFilter).attr('src',"${__ctx}/img/"+orgImg+".svg");
	                $(imgFilter).val("");
	                $(postFile).val("");
	                $(imgFilter+"_close_btn").hide();
	            } else {
//	             	alert(res.data.message);    
					$(imgFilter + 'er').text(res.data.summary);
	            }
	        }
	    });
		
	}

	function uploadSetting(){
		$('#img3').on("dragenter", function(e){
			console.log('dragenter');
			e = e || window.event;
	        e.preventDefault();
	        e.returnValue = false;
	     });
		$('#img3').on('dragover', function(e){
			console.log('dragover');
			e = e || window.event;
		    e.preventDefault();
		    e.returnValue = false;
		  })
		$('#img3').on('drop', function(e){
			e = e || window.event;
			e.stopPropagation();
			e.preventDefault();
	        var file = e.originalEvent.dataTransfer.files;
			onUpLoadByDrop(file,'#img3', '3', '#FILE3');
		})
		$('#img3b').click(function(e){
			$("#file3").click();
		})
		$("#file3").change("change", function(e) {
			onUpLoad('#file3','#img3', '3', '#FILE3');
		});
		if("${limit_increase.data.FILE3}" != ""){
	        $('#img3').val('${limit_increase.data.FILE3}');
	        $("#img3_close_btn").show();
		}else{
	        $("#img3_close_btn").hide();
		}
		
		$('#img4').on("dragenter", function(e){
			console.log('dragenter');
			e = e || window.event;
	        e.preventDefault();
	        e.returnValue = false;
	     });
		$('#img4').on('dragover', function(e){
			console.log('dragover');
			e = e || window.event;
		    e.preventDefault();
		    e.returnValue = false;
		  })
		$('#img4').on('drop', function(e){
			e = e || window.event;
			e.stopPropagation();
			e.preventDefault();
	        var file = e.originalEvent.dataTransfer.files;
			onUpLoadByDrop(file,'#img4', '4', '#FILE4');
		})
		$('#img4b').click(function(e){
			$("#file4").click();
		})
		$("#file4").change("change", function(e) {
			onUpLoad('#file4','#img4', '4', '#FILE4');
		});
		if("${limit_increase.data.FILE4}" != ""){
	        $('#img4').val('${limit_increase.data.FILE4}');
	        $("#img4_close_btn").show();
		}else{
	        $("#img4_close_btn").hide();
		}
		
		$('#img5').on("dragenter", function(e){
			console.log('dragenter');
			e = e || window.event;
	        e.preventDefault();
	        e.returnValue = false;
	     });
		$('#img5').on('dragover', function(e){
			console.log('dragover');
			e = e || window.event;
		    e.preventDefault();
		    e.returnValue = false;
		  })
		$('#img5').on('drop', function(e){
			e = e || window.event;
			e.stopPropagation();
			e.preventDefault();
	        var file = e.originalEvent.dataTransfer.files;
			onUpLoadByDrop(file,'#img5', '5', '#FILE5');
		})
		$('#img5b').click(function(e){
			$("#file5").click();
		})
		$("#file5").change("change", function(e) {
			onUpLoad('#file5','#img5', '5', '#FILE5');
		});
		if("${limit_increase.data.FILE5}" != ""){
	        $('#img5').val('${limit_increase.data.FILE5}');
	        $("#img5_close_btn").show();
		}else{
	        $("#img5_close_btn").hide();
		}
	}
	
</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card_Limit_Increase" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Credit_Card_Limit_Increase" /></h2>
				<div id="step-bar">
                    <ul>
                        <li class="finished"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
                        <li class="finished"><spring:message code="LB.SMS_Verification" /></li>
                        <li class="active"><spring:message code="LB.X1967" /></li><!-- 申請資料 -->
                        <li class=""><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
                        <li class=""><spring:message code="LB.X1968" /></li><!-- 完成申請 -->
                    </ul>
                </div>
				<form method="post" id="formId" action="">
					<input type="hidden" name="CUSIDN" id="CUSIDN" value="${limit_increase.data.CUSIDN}"/>
					<input type="hidden" name="Oday" id="Oday" value="${Oday}"/>
					<input type="hidden" name="FILE3" id="FILE3" value="${limit_increase.data.FILE3}"/>
					<input type="hidden" name="FILE4" id="FILE4" value="${limit_increase.data.FILE4}"/>
					<input type="hidden" name="FILE5" id="FILE5" value="${limit_increase.data.FILE5}"/>
					<input type="hidden" name="filetime" id="filetime" value="${limit_increase.data.filetime}"/>
					
					<input type="hidden" name="REASON" id="REASON" value="${limit_increase.data.REASON}">
					<input type="hidden" name="FGTXSTATUS" id="FGTXSTATUS" value="${limit_increase.data.FGTXSTATUS}">
					<input type="hidden" name="QUOTA" id="QUOTA" value="${limit_increase.data.QUOTA}">
					<input type="hidden" name="RESULT" id="RESULT" value="${limit_increase.data.RESULT}">
					<input type="hidden" name="back" id="back" value="">
					<input type="hidden" id="FROM_NB3" name="FROM_NB3" value="${FROM_NB3}"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block pl-0 pr-0 row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<p><spring:message code="LB.W1747" /></p>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D0049" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.NAMEshow }
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.X1914" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.CUSIDNshow }
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1610" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<!-- 											<input id="QUOTA" type="text" class="ttb-input text-input" name="QUOTA" placeholder="請輸入申請信用額度" maxlength="8" autocomplete="off"> -->
											${limit_increase.data.QUOTA_VIEW}
											<span class="input-unit m-0"><spring:message code='LB.D0509' /></span>
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1604" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.REASON_VIEW}
											<c:if test="${limit_increase.data.REASON == '9'}">
												${limit_increase.data.REASONIN}
											</c:if>
										</div>
										<div class="ttb-input" id="inputREASON" style="display: none">
	                                        <input id="REASONIN" type="text" class="ttb-input text-input" name="REASONIN" placeholder="<spring:message code='LB.D1605_1' />" maxlength="10" autocomplete="off" />
	                                    </div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1606" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.FGTXSTATUS_VIEW}
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<ul style="list-style: disc">
										<p><spring:message code="LB.D1618" /></p>
										<li style="margin-left: 1.25rem;"><spring:message code="LB.D1619" /></li>
										<li style="margin-left: 1.25rem;"><sprimg:message code="LB.D1620" /></li>
									</ul>
								</div>
								
								<p><spring:message code="LB.D1621" /></p>
								<div class="ttb-input-item row">
									<div class="photo-block" style="justify-content: flex-start; margin-left: 1rem;">
										<ol style="list-style-type: decimal;">
											<li>
												<div>
						                          	<input type="text" class="text-input" style="width:250px" readonly id="img3" />
					                                <input type="button" id="img3_close_btn" onClick="deleteUpLoad('#img3', '#FILE3', 'upload_financial_front')" value="<spring:message code='LB.Remove' />"/>
														<!--財力證明文件 -->
					                          		<input id="img3b" type="button" value="<spring:message code='LB.D0642' />" />
						                          	<font id="img3er" style="color:red"></font>
						                          	<p></p>
					                          	</div>
												<input type="file" name="file3" id="file3"
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
					                        </li>
					                        <li>
					                        	<div>
					                                <input type="text" class="text-input" style="width:250px" readonly id="img4" />
					                                <input type="button" id="img4_close_btn" onClick="deleteUpLoad('#img4', '#FILE4', 'upload_financial_front')" value="<spring:message code='LB.Remove' />"/>
														<!--財力證明文件 -->
					                          		<input id="img4b" type="button" value="<spring:message code='LB.D0642' />" />
						                          	<font id="img4er" style="color:red"></font>
						                          	<p></p>
					                          	</div>
												<input type="file" name="file4" id="file4"
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
					                        </li>
					                        <li>
					                        	<div>
					                               	<input type="text" class="text-input" style="width:250px" readonly id="img5" />
					                                <input type="button" id="img5_close_btn" onClick="deleteUpLoad('#img5', '#FILE5', 'upload_financial_front')" value="<spring:message code='LB.Remove' />"/>
														<!--財力證明文件 -->
					                          		<input id="img5b" type="button" value="<spring:message code='LB.D0642' />" />
						                          	<font id="img5er" style="color:red"></font>
						                          	<p></p>
					                            </div>
												<input type="file" name="file5" id="file5"
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</li>
										</ol>
			                    	</div>
								</div>
							</div>
							<!-- 取消 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318" />" class="ttb-button btn-flat-gray"/>
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080" />" class="ttb-button btn-flat-orange"/><!-- 下一步 -->
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>