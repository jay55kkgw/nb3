<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page6" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-61" aria-expanded="true"
				aria-controls="popup1-61">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>After successfully applying for Taiwan Business Bank Internet Banking, how will the passwords given to me by the branch?</span>
					</div>
				</div>
			</a>
			<div id="popup1-61" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>After successfully applying for online banking, the branch will give you a copy of the agreement (customer receipt) and the password list. The usage is as follows: The password list includes the user name, check-in password and transaction password. Please click on “Internet Banking” from the website of the Bank https://ebank.tbb.com.tw within one month. You can log in to Internet Banking with your <font color="red">ID number</font>, <font color="red">username</font> and <font color="red">password</font>, and change your password when you log in for the first time.</p>
							<p>After logging in, perform various inquiry, transfer, payment transaction and login voucher registration center to apply for financial XML certificate, please enter the <font color="red">transaction password</font>.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-62" aria-expanded="true"
				aria-controls="popup1-62">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>Why is there a check-in password and a transaction password? What is the difference?</span>
					</div>
				</div>
			</a>
			<div id="popup1-62" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Check-in password: The password you must type when you log in to online banking. You must change your password online for the first time using.</p>
							<p>Transaction password: The password that you must type when you apply for various transaction passwords (SSL) or login voucher registration center. You must change the password online for the first time using.</p>
							<p>The above passwords will be produced by the Bank when you apply for online banking. To protect your security, you can change your password at any time in the Bank's "Personalized Settings".</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-63" aria-expanded="true"
				aria-controls="popup1-63">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>The password is forgotten, what should I do?</span>
					</div>
				</div>
			</a>
			<div id="popup1-63" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li>1.The Bank provides general online banking customers who keep mobile phone numbers. Using the bank's valid chip financial card with a card reader, the Bank resets the general online banking user name, login and transaction password.</li>
								<li>2.If you need to go back to the cabinet to handle the reset password, please follow the instructions below:</li>
								<ol>
									<li>First, Bank depositors:
										<ol>
											<li>Legal person: Please ask the person in charge to bring the company registration certificate, ID card, deposit seal and passbook to the account bank to apply for “reset password”. After receiving the password list, please log in to the online bank within one month and change the password, otherwise the password will invalid.</li>
											<li>Individuals: Please bring your ID card, deposit seal and passbook to the account bank to apply for “reset password”. The Network Receipts and Payments account (ie, the depositor), can go to any domestic branches. After receiving the password, please log in to the online bank within one month and change the password, otherwise the password will invalid.</li>
										</ol>
									</li>
									<li>Second, the Bank's chip financial card online application households:<br> Please bring the ID card and deposit seal to the account opening bank to apply for "reset password". The Network Receipts and Payments account (ie, the depositor), can go to any domestic branches. After receiving the password, please log in to the online bank within one month and change the password, otherwise the password will invalid.</li>
									<li>Third, the Bank's credit card online application households:<br> Please contact the Bank's customer service 0800-01-7171. Apply online banking again from the next day after processing.</li>
								</ol>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-64" aria-expanded="true"
				aria-controls="popup1-64">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>How to change the "user name"?</span>
					</div>
				</div>
			</a>
			<div id="popup1-64" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The user name is 6-16 alphanumeric characters, English should at least 2 digits and is case sensitive, you cannot set only numbers or set the same alphanumeric or serial number (for example, AAAAAA cannot be set). If you need to change, please check in the "Personalization" change of Internet Banking.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-65" aria-expanded="true"
				aria-controls="popup1-65">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>I have applied for a financial XML certificate and a voucher vehicle (i-Key) to the branch. How do I operate the download voucher?</span>
					</div>
				</div>
			</a>
			<div id="popup1-65" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Please refer to "Novice" → "Voucher Registration Center", install the voucher vehicle (i-Key) driver, and then log in to "Internet Banking" → "Voucher Registration Center" → "Voucher Management" → "User Application Voucher" apply and download voucher. Once you have completed your application, you can use the electronic signature transaction.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-66" aria-expanded="true"
				aria-controls="popup1-66">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>Why is the speed of using Internet banking slow in the early morning?</span>
					</div>
				</div>
			</a>
			<div id="popup1-66" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>During the morning hours (after 03:01), the Bank conducts system maintenance operations from time to time, suspending customer transactions for about 20 to 30 minutes. At this time, the transaction will wait for the completion of the operation, causing the processing to slow down. Please try again later. Forgive me!</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-67" aria-expanded="true"
				aria-controls="popup1-67">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>How to look the unsuccessful message code of the transaction?</span>
					</div>
				</div>
			</a>
			<div id="popup1-67" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Each message code is described in Chinese. For a clearer explanation, please refer to the "error code" of the online banking announcement.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-68" aria-expanded="true"
				aria-controls="popup1-68">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>Why do I log in to Internet Banking and it will display the message "Cannot display webpage". What should I do?</span>
					</div>
				</div>
			</a>
			<div id="popup1-68" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The “Internet Security Certification” of the Bank's online banking has an encryption length of 256 bits, which is in line with the “Financial Institutions' E-Banking Security Control Operation Standard”. To protect your personal or corporate data, if your browser is unable to process 256-bit encryption, you will not be able to use the Bank's online banking services; if you are unable to log in to online banking, please click the hyperlink to the Microsoft Website Update. But please confirm that the version using IE is version 5.5 or higher.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-69" aria-expanded="true"
				aria-controls="popup1-69">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>Why do I log in to Internet Banking and there will be "Unable to display webpage" or "Program is about to close" or no response. Is there any problem with the system settings? What should I do?</span>
					</div>
				</div>
			</a>
			<div id="popup1-69" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>This may be a problem set for the client IE, as follows:</p>
							<p>Please open IE, click "Tools" in the IE toolbar, then "Internet Options" and then select "Advanced". Do not tick the "Settings" → "Security" → "Use TLS 1.0" option, please click " OK, and retry to login.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-610" aria-expanded="true"
				aria-controls="popup1-610">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>Why is it that when I log in to the online banking login screen, I will see "You have not enabled the cookie function and cannot perform online banking transactions! Please re-execute this page after turning on the cookie function". What should I do?</span>
					</div>
				</div>
			</a>
			<div id="popup1-610" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Please select "Tools"→"Privacy" in "Internet Options" to find the cookie option, and check "Always allow session cookies".</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>