<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
	
	<script type="text/JavaScript">
		// 遮罩用
		var blockId;
		
	    $(document).ready(function() {
	    	// HTML載入完成後開始遮罩
			initBlockUI();
			// 開始查詢資料並完成畫面
			setTimeout("init()", 100);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

	 	// 初始化
	    function init(){
	    	// 即時、預約標籤 click
	    	tabEvent();
	    	// 建立約定轉出帳號下拉選單
	    	creatOutAcn();
	    	// 轉出帳號 change
	    	acnoEvent();
	    	// 預約自動輸入明天
	    	getTmrDate();
	    	// 預約日期 change
	    	fgtxdateEvent();
	    	// 西元年日期輸入欄位 click
	    	datetimepickerEvent();
	    	// 通訊錄 click
			addressbookClickEvent();
	    	
			// 回上一頁填入資料
			refillData();

			// 初始化後隱藏span( 表單驗證提示訊息用的 )
			$(".hideblock").hide();
			// 表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// submit觸發事件
			finish();
	    }
	 	
	 	// 確認按鈕點擊觸發submit
		function finish(){
			$("#CMSUBMIT").click(function (e) {
				// 去掉舊的錯誤提示訊息
				$(".formError").remove();
				// 顯示驗證隱藏欄位( 為了讓提示訊息對齊 )
				$(".hideblock").show();
				
				console.log("submit~~");
				
				// 塞值進隱藏span內的input
				$("#CMDATE").val($("#CMDATE_TMP").val());
				$("#AMOUNT").val($("#AMOUNT_TMP").val());

				// 塞值進隱藏span內的input--約定/非約定 之轉出帳號
				if( $('input[name=TransferType]:checked').val() == 'PD'){
					// 轉出帳號為約定
					$("#OUTACN").val($("#OUTACN_PD").val());
				}else if( $('input[name=TransferType]:checked').val() == 'NPD'){
					// 轉出帳號為非約定
					$("#OUTACN").val($("#OUTACN_NPD").val());
				}

			   	// 表單驗證--即時/預約
			   	var fgtxdate = $('input[name="FGTXDATE"]:checked').val();
			   	console.log('fgtxdate: ' + fgtxdate);
			   	
				var sMinDate = '${str_SystemDate}';
				var sMaxDate = '${sNextYearDay}';
				console.log("processQuery.sMinDate: " + sMinDate);
				console.log("processQuery.sMaxDate: " + sMaxDate);
				
				if (fgtxdate == '2'){
					// 驗證是否是可預約單日之日期
					$("#CMDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X0377" />,CMDATE,"+sMinDate+","+sMaxDate+"]]]");
			   	} else {
			   		// 取消驗證是否是可預約單日之日期
			   		$("#CMDATE").removeClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X0377" />,CMDATE,"+sMinDate+","+sMaxDate+"]]]");
				}

				// 表單驗證--轉出帳號
				var transferType = $('input[name="TransferType"]:checked').val();
				console.log('transferType: ' + transferType);
				if(transferType == 'PD') {
					$("#OUTACN_PD").addClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_PD,'']]]");
					$("#OUTACN_NPD").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_NPD,'']]]");
				} else if(transferType == 'NPD') {
					$("#OUTACN_PD").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_PD,'']]]");
					$("#OUTACN_NPD").addClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_NPD,'']]]");
				}
				
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e = e || window.event; // for IE
					e.preventDefault();
					
				} else {
					// 資料檢核，過了則送出表單
					processQuery();
				}
			});
	 	}

		// 即時、預約標籤切換事件
		function tabEvent(){
			// 即時
			$("#nav-trans-now").click(function() {
				$("#transfer-date").hide();
				fstop.setRadioChecked("FGTXDATE" , "1" ,true);
			})
			// 預約
			$("#nav-trans-future").click(function() {
				$("#transfer-date").show();
				fstop.setRadioChecked("FGTXDATE" , "2" ,true);
			})
		}

		// 建立約定轉出帳號下拉選單
		function creatOutAcn(){
			var options = { keyisval:false ,selectID:'#OUTACN_PD'};
			
			uri = '${__ctx}'+"/PAY/EXPENSE/getOutAcn_aj"
			rdata = {type: 'acno' };
			console.log("creatOutAcn.uri: " + uri);
			console.log("creatOutAcn.rdata: " + rdata);
			
			data = fstop.getServerDataEx(uri,rdata,false);

			console.log("creatOutAcn.data: " + JSON.stringify(data));
			
			if(data !=null && data.result == true ){
				fstop.creatSelect( data.data, options);
			}
		}
		
		// 轉出帳號change事件，要秀出選擇的轉出帳號可用餘額
		function acnoEvent(){
			$('input[type=radio][name=TransferType]').change(function() {
				if (this.value == 'PD') { //約定
					$("#OUTACN_NPD").val("");
				} else if (this.value == 'NPD') { //非約
					$("#OUTACN_PD").val("");
					$("#acnoIsShow").hide();
				}
			});
			$("#OUTACN_PD").change(function() {
				var acno = $('#OUTACN_PD :selected').val();
				console.log("acnoEvent.acno: " + acno);
				getACNO_Data(acno);
				$('#TransferType_01').click();
			});
		}
		// 取得轉出帳號餘額資料
		function getACNO_Data(acno){
			// 變更轉出帳號就隱藏重置再重新顯示
			$("#acnoIsShow").hide();
			$("#showText").html('');
			
			var options = { keyisval:true ,selectID:'#OUTACN'};
			
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getACNO_Data_aj"
			rdata = {acno: acno};
			console.log("getACNO_Data.uri: " + uri);
			console.log("getACNO_Data.rdata: " + rdata);
			
			fstop.getServerDataEx(uri,rdata,false,isShowACNO_Data);
		}
		// 顯示轉出帳號餘額
		function isShowACNO_Data(data){
			var i18n= new Object();
			i18n['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
			console.log("isShowACNO_Data.data: " + JSON.stringify(data));
			
			if(data && data.result){
				// 可用餘額
				console.log("data.data.accno_data: " + data.data.accno_data.ADPIBAL);
				
				// 格式化金額欄位
				i18n['available_balance'] += fstop.formatAmt(data.data.accno_data.ADPIBAL);
				$("#showText").html(i18n['available_balance']);
				$("#acnoIsShow").show();
			}else{
				$("#acnoIsShow").hide();
			}
		}

		// 預約自動輸入明天
		function getTmrDate() {
			// 預約日期欄位顯示明天
			$('#CMDATE_TMP').val("${tmrDate}");
		}
		// 預約日期change事件 ，檢核日期邏輯
		function fgtxdateEvent(){
			 $('input[type=radio][name=FGTXDATE]').change(function() {
		        if (this.value == '2') { // 預約
		            console.log("tomorrow");
		            $("#CMDATE").addClass("validate[required]");
		        }else if (this.value == '1') { // 即時
		            $("#CMDATE").removeClass("validate[required]");
		        }
		    });
		}

		// 西元小日曆click
		function datetimepickerEvent(){
		    $(".CMDATE").click(function(event) {
				$('#CMDATE_TMP').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			});
		}

		// 通訊錄click
		function addressbookClickEvent() {
			// 通訊錄btn
			$('#getAddressbook').click(function() {
				openAddressbook();
			});
			// 同交易備註btn
			$('#CMMAILMEMO_btn').click(function() {
				$('#CMMAILMEMO').val( $('#CMTRMEMO').val() );
			});
			//範例
			$('#EXAMPLE').click(function() {
				openexample();
			});
		}
		// 開啟通訊錄email選單
		function openAddressbook() {
			window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
		}
		//開啟範例
		function openexample() {
			window.open('${__ctx}/img/NationalSample.jpg');
		}

		function processQuery(){
			var main = document.getElementById("formId");
			
			main.BARCODE2.value= main.BARCODE2.value.toUpperCase();
						
			if (!CheckNational(main.BARCODE1, main.BARCODE2, main.BARCODE3)) return false;
						
			// 隱藏表單按鈕
			$("#CMTRMAIL").hide();
			$("#CMSUBMIT").hide();
			$("#CMRESET").hide();
			$("#CMMAILMEMO_btn").hide();
			
			// 通過資料檢核解除表單驗證
			$("#formId").validationEngine('detach');
			
			// 畫面遮罩
			initBlockUI();
			// 準備送交的處理
			prepareNextPageData();
			// 送交
			$("#formId").submit();
		}

		function prepareNextPageData() {
			var main = document.getElementById("formId");
			SetPAYDUE();
			AfterBarcode3();
		}

		function SetPAYDUE(){
			var main = document.getElementById("formId");

			var sBarcode1 = document.getElementById("BARCODE1").value;
			var iCYear = parseInt(sBarcode1.substring(0, 2), 10);
			var sMonth = sBarcode1.substring(2, 4);
			var sDate = sBarcode1.substring(4, 6);
			
		   	var oYear = 1911;
		   	
		   	if (iCYear >= 97)
		   	{
		   		oYear += iCYear;
		   	}
		   	else
		   	{//民國百年
		   		oYear += iCYear + 100;
		   	}
		   	
		   	main.PAYDUE.value = oYear.toString() + "/"  + sMonth + "/" + sDate;
		}
		
		function AfterBarcode3() {
			var oBarcode3 = document.getElementById("BARCODE3");
			var sBarcode3 = oBarcode3.value;
			if (sBarcode3.length == 15) {
				trimStartZero(sBarcode3.substring(6));
			}
		}

		function trimStartZero(sBarcode3) {
			var blen = sBarcode3.length;
			var i = 0;

			while (sBarcode3.substr(i, 1) == "0") {
				i++;
			}
			var amount = sBarcode3.substring(i, blen);
			document.getElementById("AMOUNT_TMP").value = amount;
		}


		/*
	 	 * 檢查國民年金保險費條碼一、二、三
	 	 * PARAM 條碼一輸入框
	 	 * PARAM 條碼二輸入框
	 	 * PARAM 條碼三輸入框
	 	 * return
		 */
		function CheckNational(oBarcode1, oBarcode2, oBarcode3) {

			try {
			    var sBarcode1 = oBarcode1.value;
			    var sBarcode2 = oBarcode2.value;
			    var sBarcode3 = oBarcode3.value;
			    
				var yearNation = sBarcode1.substring(0,2)
				var monthNation = sBarcode1.substring(2,4)
				var dayNation = sBarcode1.substring(4,6)

				var inty = Number(yearNation)+2011;
				
				var today = new Date();
				var tmonth = today.getMonth()+1;
				var tday = today.getDate();
				var smonth="";
				var sday="";
				
				if (tmonth<10){
					smonth = "0"+tmonth.toString();
				} else {
					smonth = tmonth.toString();
				}

				if (tday<10){
					sday = "0"+tday.toString();
				} else {
					sday = tday.toString();
				}
					
				var newdate = parseInt((today.getFullYear()).toString()+smonth+sday);
				var nowDate = parseInt(inty.toString()+monthNation+dayNation);
				
			    //檢核條碼一
		        var reg1 = /\d{2}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])(630)/;
		        if (reg1.test(sBarcode1) == false) {
		        	//alert("<spring:message code= "LB.Alert175" />");
		        	errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert175' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			        oBarcode1.focus();
			        return false;
		        }
		        
				if(nowDate < newdate) {
					//alert('<spring:message code= "LB.Alert185" />');
					errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert185' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
					return false;
				}
			    
			    //檢核條碼二
			    if (sBarcode2.length != 16) {
			    	//alert("<spring:message code= "LB.Alert176" />");
			    	errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert176' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			        oBarcode2.focus();
			        return false;
			    }
			    
			    //檢核條碼三(不驗月份)
		        var reg3 = /\d{4}[aAbB0-9][xXyY0-9]\d{9}/;
			    if (reg3.test(sBarcode3) == false) {
			    	//alert("<spring:message code= "LB.Alert177" />");
			    	errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert177' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			        oBarcode3.focus();
			        return false;
			    }
			    
			    //檢查碼一檢核
			    var iBase1 = parseInt(sBarcode1.charAt(0)) + parseInt(sBarcode1.charAt(2)) + parseInt(sBarcode1.charAt(4)) + parseInt(sBarcode1.charAt(6)) + parseInt(sBarcode1.charAt(8)) +
		                     parseInt(ChangeNationalChar(sBarcode2.charAt(0))) + parseInt(ChangeNationalChar(sBarcode2.charAt(2))) + parseInt(ChangeNationalChar(sBarcode2.charAt(4))) + parseInt(ChangeNationalChar(sBarcode2.charAt(6))) + parseInt(ChangeNationalChar(sBarcode2.charAt(8))) + parseInt(ChangeNationalChar(sBarcode2.charAt(10))) + parseInt(ChangeNationalChar(sBarcode2.charAt(12))) + parseInt(ChangeNationalChar(sBarcode2.charAt(14))) + 
		                     parseInt(sBarcode3.charAt(0)) + parseInt(sBarcode3.charAt(2)) + parseInt(sBarcode3.charAt(6)) + parseInt(sBarcode3.charAt(8)) + parseInt(sBarcode3.charAt(10)) + parseInt(sBarcode3.charAt(12)) + parseInt(sBarcode3.charAt(14));
			    var iRemainder1 = iBase1 % 11;
			    var sCheck1 = "";
			    
			    if (iRemainder1 == 0) {
			    	sCheck1 = "A";
			    } else if (iRemainder1 == 10) {
			    	sCheck1 = "B";
			    } else {
			    	sCheck1 = iRemainder1.toString();
			    }
			    
			    if (sBarcode3.charAt(4).toUpperCase() != sCheck1) {
			    	//alert("<spring:message code= "LB.Alert178" />");
			    	errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert178' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			        oBarcode1.focus();
			        return false;
			    }
			    
			    //檢查碼二檢核
			    var iBase2 = parseInt(sBarcode1.charAt(1)) + parseInt(sBarcode1.charAt(3)) + parseInt(sBarcode1.charAt(5)) + parseInt(sBarcode1.charAt(7)) +
		                     parseInt(ChangeNationalChar(sBarcode2.charAt(1))) + parseInt(ChangeNationalChar(sBarcode2.charAt(3))) + parseInt(ChangeNationalChar(sBarcode2.charAt(5))) + parseInt(ChangeNationalChar(sBarcode2.charAt(7))) + parseInt(ChangeNationalChar(sBarcode2.charAt(9))) + parseInt(ChangeNationalChar(sBarcode2.charAt(11))) + parseInt(ChangeNationalChar(sBarcode2.charAt(13))) + parseInt(ChangeNationalChar(sBarcode2.charAt(15))) + 
		                     parseInt(sBarcode3.charAt(1)) + parseInt(sBarcode3.charAt(3)) + parseInt(sBarcode3.charAt(7)) + parseInt(sBarcode3.charAt(9)) + parseInt(sBarcode3.charAt(11)) + parseInt(sBarcode3.charAt(13));
			    var iRemainder2 = iBase2 % 11;
			    var sCheck2 = "";
			    
			    if (iRemainder2 == 0) {
			    	sCheck2 = "X";
			    } else if (iRemainder2 == 10) {
			    	sCheck2 = "Y";
			    } else {
			    	sCheck2 = iRemainder2.toString();
			    }
			    
			    if (sBarcode3.charAt(5).toUpperCase() != sCheck2) {
			    	//alert("<spring:message code= "LB.Alert178" />");
			    	errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert178' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			        oBarcode1.focus();
			        return false;
			    }
			    
			} catch (exception) {
			    //alert("<spring:message code= "LB.Alert186" />:" + exception);
			    errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert186' />:" + exception], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
			}
			
			return true;
		}

		/*
	 	 * 轉換國民年金條碼裡的英文為數字傳回
	 	 * PARAM 轉換前字元
	 	 * PARAM 條碼二輸入框
	 	 * PARAM 條碼三輸入框
	 	 * return
		 */
		function ChangeNationalChar(sChar) {
			var result = sChar;
			switch (sChar.toUpperCase()) {
				case "A": result="1"; break;
				case "B": result="2"; break;
				case "C": result="3"; break;
				case "D": result="4"; break;
				case "E": result="5"; break;
				case "F": result="6"; break;
				case "G": result="7"; break;
				case "H": result="8"; break;
				case "I": result="9"; break;
				case "J": result="1"; break;
				case "K": result="2"; break;
				case "L": result="3"; break;
				case "M": result="4"; break;
				case "N": result="5"; break;
				case "O": result="6"; break;
				case "P": result="7"; break;
				case "Q": result="8"; break;
				case "R": result="9"; break;
				case "S": result="2"; break;
				case "T": result="3"; break;
				case "U": result="4"; break;
				case "V": result="5"; break;
				case "W": result="6"; break;
				case "X": result="7"; break;
				case "Y": result="8"; break;
				case "Z": result="9"; break;
				case "+": result="1"; break;
				case "%": result="2"; break;
				case "-": result="6"; break;
				case ".": result="7"; break;
				case " ": result="8"; break;
				case "$": result="9"; break;
				case "/": result="0"; break;
			}
			
			return result;
		}
		
		// 表單重置
		function formReset() {
			$("#acnoIsShow").hide();
			$(".formError").remove();
			$('form').find('*').filter(':input:visible:first').focus();
			$('#formId').trigger("reset");
		}
		
		// 回上一頁填入資料
		function refillData() {
			var isBack = '${isBack}';
			console.log('isBack: ' + isBack);
			var jsondata = '${previousdata}';
			console.log('jsondata: ' + jsondata);
			
			if ('Y' == isBack && jsondata != null) {
				JSON.parse(jsondata, function(key, value) {
					if(key) {
						var obj = $("input[name='"+key+"']");
						var type = obj.attr('type');
						
						console.log('--------------------------------');
						console.log('type: ' + type);
						console.log('key: ' + key);
						console.log('value: ' + value);
						console.log('--------------------------------');
						
						if(type == 'text'){
							obj.val(value);
						}
						
						if(type == 'hidden'){
							obj.val(value);
						}

						if(type == 'radio'){
							obj.filter('[value='+value+']').prop('checked', true);
						}
						
						if(type == 'checkbox'){
							obj.prop('checked', true);
						}
						
						// SELECT例外處理--約定轉出帳號
						if(key == 'OUTACN_PD'){
							$("#OUTACN_PD option").filter(function() {
								return $(this).val() == value;
							}).attr('selected', true);
						}
						
						// 即時或預約標籤切換
						if(key == 'FGTXDATE' && value == '2'){
							$("#nav-trans-future").click();
						}
					}
				});
				
				$("#BARCODE3").blur();
				
				// TOKEN值要換一個，不可用上一次的
				$("#TOKEN").val('${sessionScope.transfer_confirm_token}');
			}
		}
	</script>
</head>
<body>
	<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 國民年金保險費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0525" /></li>
		</ol>
	</nav>

	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
	
		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<!-- 功能內容 -->
				<form method="post" id="formId" action="${__ctx}/PAY/EXPENSE/national_pension_confirm">
					<input type="hidden" id="PAYDUE" name="PAYDUE">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					<input type="hidden" name="FLAG" value="1"><!-- 約定\非約定 -->
					
					<!-- 功能名稱 -->
					<h2><spring:message code="LB.W0525" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
					<!-- 交易流程階段 -->
					<div id="step-bar">
						<ul>
							<li class="active"><spring:message code="LB.Enter_data" /></li>
							<li class=""><spring:message code="LB.Confirm_data" /></li>
							<li class=""><spring:message code="LB.Transaction_complete" /></li>
						</ul>
					</div>
					
					<!-- 功能內容 -->
					<div class="main-content-block row radius-50">
						<!-- 即時、預約導引標籤 -->
						<nav style="width: 100%;">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-trans-now" data-toggle="tab" href="#nav-trans-now" 
									role="tab" aria-controls="nav-home" aria-selected="false">
									<spring:message code="LB.Immediately" />
								</a> 
								<a class="nav-item nav-link" id="nav-trans-future" data-toggle="tab" href="#nav-trans-future" 
									role="tab" aria-controls="nav-profile" aria-selected="true" >
									<spring:message code="LB.Booking" />
								</a>
							</div>
						</nav>
						
						<!-- 交易流程階段 -->
						<div class="col-12 tab-content" id="nav-tabContent">
							<div class="tab-pane fade " id="nav-trans-future" role="tabpanel"
								aria-labelledby="nav-profile-tab"></div>

							<!-- 預約才顯示轉帳日期 -->
							<div class="ttb-input-block tab-pane fade show active"
								id="nav-trans-now" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="ttb-message">
									<span></span>
								</div>
								
								<!-- 即時的屬性(不須顯示預設隱藏) -->
								<div class="ttb-input" style="display: none;">
									<label class="radio-block">
										<spring:message code="LB.Immediately" />
										<input type="radio" name="FGTXDATE" value="1" checked /> 
										<span class="ttb-radio"></span>
									</label>
								</div>
								
								<!-- 轉帳日期 -->
								<div id="transfer-date" class="ttb-input-item row" style="display: none;">
									<span class="input-title">
										<label><h4><spring:message code="LB.Transfer_date" /></h4></label>
									</span>
									<span class="input-block">
										<!-- 預約的屬性(必須顯示) -->
										<div class="ttb-input ">
											<label class="radio-block">
												<spring:message code="LB.Booking" /> 
												<input type="radio" name="FGTXDATE" value="2" /> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 預約日期 -->
										<div class="ttb-input">
											<input type="text" id="CMDATE_TMP" name="CMDATE_TMP" value="" size="10"
												maxlength="10" class="text-input datetimepicker" />
											<span class="input-unit CMDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏 -->
											<span class="hideblock">
												<!-- 驗證用的input -->
												<input id="CMDATE" name="CMDATE" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
								
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code="LB.Payers_account_no" /></h4></label>
									</span>
									<span class="input-block">
										<!-- 約定 轉出帳號 -->
										<div class="ttb-input">
											<label class="radio-block"> 
												<input type="radio" id="TransferType_01" name="TransferType" value="PD" checked>
													<spring:message code="LB.Designated_account" />
												<span class="ttb-radio"></span>
											</label>
										</div> 
										<div class="ttb-input ">
											<select name="OUTACN_PD" id="OUTACN_PD" class="custom-select select-input half-input">
												<option value="">----<spring:message code="LB.Select_account" />-----</option>
											</select>
											<!-- 約定 轉出帳號 餘額 -->
											<div id="acnoIsShow">
												<span id="showText" class="input-unit"></span>
											</div>
										</div>
										
										<!-- 非約定 轉出帳號 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Out_nondesignated_account" /> 
												<input type="radio" id="TransferType_02" name="TransferType" value="NPD" onclick="listReaders();" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<input type="text" name="OUTACN_NPD" id="OUTACN_NPD" class="text-input" readonly="readonly" value="" />
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.X2361" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
											<span class="hideblock">
												<!-- 轉出帳號 -->
												<input id="OUTACN" name="OUTACN" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
								
								<!-- 條碼一 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4><spring:message code="LB.W0470" /></h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" name="BARCODE1" id="BARCODE1" class="text-input"
												value="" size="9" maxlength="9" />
											<span class="input-unit">(<spring:message code="LB.W0471" />)</span>
											<button type="button" class="btn-flat-orange" name="EXAMPLE" id="EXAMPLE">
												<spring:message code="LB.W0438" />											
											</button>
										</div>
									</span>
								</div>
								
								<!-- 條碼二 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4><spring:message code="LB.W0472" /></h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" name="BARCODE2" id="BARCODE2" class="text-input"
												value="" size="16" maxlength="16" />
											<span class="input-unit">(<spring:message code="LB.W0473" />)</span>
										</div>
									</span>
								</div>
								
								<!-- 條碼三 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4><spring:message code="LB.W0474" /></h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" name="BARCODE3" id="BARCODE3" class="text-input"
												 value="" size="15" maxlength="15" onblur="javascript:AfterBarcode3();"/>
											<span class="input-unit">(<spring:message code="LB.W0475" />)</span>
										</div>
									</span>
								</div>
								
								<!-- 應繳總金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code="LB.W0441" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="AMOUNT_TMP" name="AMOUNT_TMP" class="text-input" size="8" maxlength="8" value="" disabled="disabled" /> 
											<span class="input-unit"><spring:message code="LB.Dollar" /></span>
										</div>
										<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
										<span class="hideblock">
											<!-- 應繳總金額 -->
											<input id="AMOUNT" name="AMOUNT" type="text" readonly="readonly" 
												class="text-input validate[custom[integer,min[1] ,max[99999999]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</span>
								</div>
								
								<!-- 交易備註 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4><spring:message code="LB.Transfer_note" /></h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" size="56" maxlength="20" value="" />
											<span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
										</div>
									</span>
								</div>
	
								<!-- 轉出成功Email通知 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.X0479" /></h4>
										</label>
									</span>
									<span class="input-block">
									
								<!-- 寄信通知 -->
								<c:choose>
								    <c:when test="${sendMe} && !${sessionScope.dpmyemail}.equals('')">
										    
								        <!-- 通知本人 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Notify_me" />：</span>
											<span class="input-subtitle subtitle-color">${sessionScope.dpmyemail}</span>
										</div>
										
										<!-- 另通知 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Another_notice" />：</span>
										</div>
												
								    </c:when>
						    		<c:otherwise>
										    
								        <!-- 通訊錄 -->
										<div class="ttb-input">
											<input type="text" id="CMTRMAIL" name="CMTRMAIL" placeholder="<spring:message code="LB.Email" />"
												class="text-input validate[funcCall[validate_EmailCheck[CMTRMAIL]]]" />
											<span class="input-unit"></span>
											<!-- window open 去查 TxnAddressBook 參數帶入身分證字號 -->
											<button type="button" class="btn-flat-orange" id="getAddressbook"><spring:message code="LB.Address_book" /></button>
										</div>
												
								    </c:otherwise>
								</c:choose>
										
										<!-- 摘要內容 -->
										<div class="ttb-input">
											<input type="text" id="CMMAILMEMO" name="CMMAILMEMO" class="text-input"
												 placeholder="<spring:message code="LB.Summary" />" value="" />
											<span class="input-unit"></span>
											<button type="button" class="btn-flat-orange" id="CMMAILMEMO_btn"><spring:message code="LB.As_transfer_note" /></button>
										</div>
									</span>
								</div>
							</div>
							
							<!-- 重新輸入、確認按鈕 -->
							<input id="CMRESET" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" onclick="formReset();" />
							<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						
						</div>
					</div>
					
					<!-- 說明 -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.National_Pension_P1_D1-1" /><a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank"><spring:message code="LB.National_Pension_P1_D1-2" /></a> </li>
						<li><spring:message code="LB.National_Pension_P1_D2" /></li>
						<li><spring:message code="LB.National_Pension_P1_D3" /></li>
						<li><spring:message code="LB.National_Pension_P1_D4" /></li>
						<li><spring:message code="LB.National_Pension_P1_D5" /></li>
						<li><spring:message code="LB.National_Pension_P1_D6" /></li>
						<li><spring:message code="LB.National_Pension_P1_D7" /></li>
					</ol>
					
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>

</body>
</html>
