<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);

	$("#printButton").click(function(){
		var params = {
			"jspTemplateName":"fund_purchase_result_print",
			"jspTitle":"<spring:message code= "LB.W1065" />",
			"CMQTIME":"${bsData.CMQTIME}",
			"FDINVTYPEChinese":"${FDINVTYPEChinese}",
			"TYPEChinese":"${TYPEChinese}",
			"hiddenCUSIDN":"${hiddenCUSIDN}",
			"hiddenNAME":"${hiddenNAME}",
			"TRANSCODE":"${bsData.TRANSCODE}",
			"FUNDLNAME":"${FUNDLNAME}",
			"RISK":"${RISK}",
			"ADCCYNAME":"${ADCCYNAME}",
			"AMT3Format":"${AMT3Format}",
			"FCAFEEFormat":"${FCAFEEFormat}",
			"FCA2Format":"${FCA2Format}",
			"AMT5Format":"${AMT5Format}",
			"FUNDACN":"${bsData.FUNDACN}",
			"TRADEDATEFormat":"${TRADEDATEFormat}",
			"YIELDInteger":"${YIELDInteger}",
			"STOPInteger":"${STOPInteger}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Funds" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 單筆申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1065" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1065"/></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
				<form id="formID" method="post">
					<input type="hidden" name="ADOPID" value="C016"/>
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
                                	<span><spring:message code="LB.X0398" /></span>
                            	</div>
								<!-- 交易時間 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.Trading_time"/></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bsData.CMQTIME}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 客戶投資屬性 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1067"/></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${FDINVTYPEChinese}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 扣款方式 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.D0173"/></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${TYPEChinese}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 身分證字號/統一編號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.Id_no"/></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${hiddenCUSIDN}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 姓名 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.Name"/></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${hiddenNAME}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 基金名稱 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0025" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>（${bsData.TRANSCODE}）${FUNDLNAME}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 商品風險等級 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1073" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${RISK}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 申購金額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1074" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${ADCCYNAME} ${AMT3Format}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 手續費率 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1034" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${FCAFEEFormat}％</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 手續費 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.D0507" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${ADCCYNAME} ${FCA2Format}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 扣款金額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1079" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${ADCCYNAME} ${AMT5Format}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 扣款帳號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0702" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bsData.FUNDACN}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 生效日期 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1080" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${TRADEDATEFormat}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 停利通知設定 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1075" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${YIELDInteger}％</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 停損通知設定 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1076" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>- ${STOPInteger}％</span>
                                		</div>
                               		</span>
                            	</div>
                            </div>
							<input type="button" id="printButton" value="<spring:message code="LB.Print"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>
				</form>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<p><b><spring:message code="LB.fund_purchase_P3_D1" />：</b></p>
					<li><span><spring:message code="LB.fund_purchase_P3_D2" /></span></li>
					<li><span><spring:message code="LB.fund_purchase_P3_D3" /></span></li>
					<li><span><spring:message code="LB.fund_purchase_P3_D4" /></span></li>
				</ol>
				<ol class="description-list list-decimal">
					<p><b><spring:message code="LB.fund_purchase_P3_D5" />：</b></p>
					<li><span><spring:message code="LB.fund_purchase_P3_D6-1" /><br/>「<spring:message code="LB.fund_purchase_P3_D6-2" />」<spring:message code="LB.fund_purchase_P3_D6-3" />→「<spring:message code="LB.fund_purchase_P3_D6-4" />」→「<spring:message code="LB.fund_purchase_P3_D6-5" />」</span></li>
					<li><span><spring:message code="LB.fund_purchase_P3_D7-1" />（https://www.tbb.com.tw/）<br/>「<spring:message code="LB.fund_purchase_P3_D7-2" />」→「<spring:message code="LB.fund_purchase_P3_D7-3" />」→「<spring:message code="LB.fund_purchase_P3_D7-4" />」</span></li>
				</ol>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
<c:if test="${showDialog == 'true'}">
	<%@ include file="../index/txncssslog.jsp"%>
	</c:if>
</html>