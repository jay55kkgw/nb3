<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>

<body>
<script type="text/javascript">
var shwd= {};
//您有對帳單無法交付或對帳單退件情形，請儘速更新對帳單寄送地址或EMAIL信箱，以保障您的權益。
shwd["01"]='<spring:message code= "LB.X2282" />';
//您所約定之EMAIL為空白，將改寄紙本帳單，如有任何問題，歡迎洽詢開戶之分行。
shwd["02"]='<spring:message code= "LB.X2283" />';
//您所約定之行員信箱非本人所有，請先變更EMAIL信箱，才可進行交易。
shwd["03"]='<spring:message code= "LB.X2284" />';
//您所約定之EMAIL信箱與他人重覆，為確保您的權益，請變更EMAIL信箱，才可進行交易
shwd["04"]='<spring:message code= "LB.X2285" />';
//您所約定之EMAIL信箱係本行公務信箱，為確保您的權益，請先變更EMAIL信箱，才可進行交易。
shwd["05"]='<spring:message code= "LB.X2286" />';
//您所約定之EMAIL信箱與他人重覆，為確保您的權益，請變更EMAIL信箱，才可進行交易。
shwd["06"]='<spring:message code= "LB.X2285" />';
//您所約定之EMAIL信箱係本行公務信箱，為確保您的權益，請先變更EMAIL信箱，才可進行交易。
shwd["07"]='<spring:message code= "LB.X2288" />';
//您所約定之EMAIL信箱與他人信箱重覆，為確保您的權益，請先變更EMAIL信箱，才可進行交易。
shwd["08"]='<spring:message code= "LB.X2289" />';
//行員使用一般EMAIL信箱且與他人重覆，請先變更EMAIL信箱，才可進行交易。
shwd["09"]='<spring:message code= "LB.X2290" />';
//您所約定之行員信箱為無效信箱，請變更EMAIL信箱，才可進行申購。
shwd["16"]='<spring:message code= "LB.X2291" />';
//您所約定之EMAIL信箱與他人重覆，為確保您的權益，是否先變更EMAIL後繼續交易。
shwd["10"]='<spring:message code= "LB.X2292" />';
//您的EMAIL信箱與他人重複，請變更 EMAIL 信箱，才可進行開戶。
shwd["11"]='<spring:message code= "LB.X2293" />';
//行員EMAIL信箱與他人重複，請變更 EMAIL 信箱，才可進行開戶。
shwd["12"]='<spring:message code= "LB.X2294" />';
//您所約定之行員信箱非本人所有，請先變更 EMAIL 信箱，才可進行開戶。
shwd["13"]='<spring:message code= "LB.X2295" />';
//對帳單交付方式使用 E-MAIL 時必須要輸入電子信箱地址。
shwd["14"]='<spring:message code= "LB.X2296" />';
//客戶不可使用TBB信箱開戶，請變更信箱才可進行開戶。
shwd["15"]='<spring:message code= "LB.X2297" />';
var SHWD= "${SHWD}";
function shwd_prompt_init(fee_expose){

	console.log("shwd_prompt_init");
	
	var SHOWDFLAG ="NO";
	if(SHWD){
		if(SHWD.length>=2&&parseInt(SHWD)>0){
			SHOWDFLAG = "YES";
		}
	}
	
	if(SHOWDFLAG=="YES"){
		if(fee_expose)
		    showshwd(SHWD,fee_expose);
		else
			showshwd(SHWD);
	}else{
		if(typeof(showcontent) == "function")
			showcontent();
	}

	
}
var blockstatus;
function showshwd(n,fee_expose){
	if(n){
		//判斷n有無值
	    if(fee_expose && n=="10"){
	    	blockstatus = "2";
		    //10且為報酬揭露頁的情況
	    	errorBlock(
	    			'<spring:message code= "LB.X2298" />', 
					null,
					['<spring:message code= "LB.X0152" />',shwd[n],"&{red}(<spring:message code= 'LB.X2299' />)"], 
					'<spring:message code= "LB.D0034_2" />', 
					'<spring:message code= "LB.D0034_3" />'
			);
	    }else{
	    	blockstatus = "1";
		   //非報酬揭露頁的情況
	    	errorBlock(
	    			'<spring:message code= "LB.X2298" />', 
					null,
					['<spring:message code= "LB.X0152" />',shwd[n]], 
					'<spring:message code= "LB.Confirm" />', 
					null
			);
	    }
	}
}
$(document).ready(function(){
	$("#errorBtn1").click(function() {
		$('#error-block').hide();
		if(blockstatus=="1")
			switchshowdialog();
		if(blockstatus=="2")
			switchshowdialog2();//10選是的情況
		if(blockstatus=="3")
			showtradway();//聲明書簽完
			
	});
	$("#errorBtn2").click(function() {
		$('#error-block').hide();
		//10選否的情況 簽署聲明書
		if(blockstatus=="2")
			setTimeout("switchshowdialog3()", 200);
	});
});


function switchshowdialog(){
	if(SHWD=="01" || SHWD=="02")
	{
      	//nothing  繼續做	
		$('#error-block').hide();
		if(typeof(showcontent) == "function")
			showcontent();
    }
	else
	{
		fstop.getPage('${pageContext.request.contextPath}'+'/INDEX/index','', '');
	}		
}

function switchshowdialog2(){
	//10選是的情況
	fstop.getPage('${pageContext.request.contextPath}'+'/PERSONAL/SERVING/mail_setting','', '');//導頁去改信箱
}

function switchshowdialog3(){
	blockstatus = "3";
	//10選否的情況 簽署聲明書
	errorBlock(
			"聲明書(EMAIL重覆客戶使用)", 
			null,
			[
				"本人/本公司於　貴行辦理特定金錢信託投資國內外有價證券業",
			    "務（含OBU)，本人/本公司所留存　貴行之電子信箱，業經電",
				"子信箱所有人同意使用，以便本人/本公司收取　貴行寄發之",
				"各項通知及報告書，特此聲明，倘日後發生任何糾紛、風險",
				"或損失，皆由本人/本公司自行承擔，與　貴行無涉，絕無",
				"異議。"
			], 
			'<spring:message code= "LB.Confirm" />',  
			null
	);
}

function showtradway(){
	$('#error-block').hide();
	if(typeof(showcontent) == "function")
		showcontent();
	$("#tradeway").show(); //顯示交易機制
	$("#CMCARD").prop("checked",true);
	fgtxwayClick();
	blockstatus = "4";
	setTimeout("checkTC();", 50);
	$("#NUM").val("Y");//同意書註記
    initKapImg();
    newKapImg();
    fgtxwayClick();
    chaBlock();
}
</script>
</body>
</html>