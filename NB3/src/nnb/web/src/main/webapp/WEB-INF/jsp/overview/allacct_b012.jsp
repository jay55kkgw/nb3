<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">

	// column's title i18n

	
	// b012-臺幣活期性存款餘額
	i18n['BO01'] = '<spring:message code="LB.W1009" /><BR><spring:message code="LB.W0023" />'; //申購日期 憑證號碼
	i18n['BO02'] = '<spring:message code="LB.W1011" /><BR><spring:message code="LB.W1012" />'; // 債券代碼 債券名稱
	i18n['BO03'] = '<spring:message code="LB.W0026" /><BR><spring:message code="LB.W1014" />'; // 信託金額 面額
	i18n['BO04'] = '<spring:message code="LB.W1015" />(%)<br><spring:message code="LB.W1016" />'; // 參考賣價(%) 報價日期
	i18n['BO05'] = '<spring:message code="LB.W0915" /><br><spring:message code="LB.W0909" />'; // 參考現值 計價幣別
	i18n['BO06'] = '<spring:message code="LB.W0918" /><br><spring:message code="LB.W1018" />';// 累計配息金額 前手息
	i18n['BO07'] = '<spring:message code="LB.W1019" /><BR><spring:message code="LB.W0919" />'; // 參考投資損益 參考含息報酬率
	i18n['BO08'] = '<spring:message code="LB.W0953" />';// 詳細
	
	var b012_columns = [
		{ "data":"BO01", "title":i18n['BO01'] },
		{ "data":"BO02", "title":i18n['BO02'] },
		{ "data":"BO03", "title":i18n['BO03'] },
		{ "data":"BO04", "title":i18n['BO04'] },
		{ "data":"BO05", "title":i18n['BO05'] },
		{ "data":"BO06", "title":i18n['BO06'] },
		{ "data":"BO07", "title":i18n['BO07'] },
		{ "data":"BO08", "title":i18n['BO08'] }
	];
	
	i18n['BO11'] = '<spring:message code="LB.W0908" />';
	i18n['BO12'] = '<spring:message code="LB.W0933" />'; // 
	i18n['BO13'] = '<spring:message code="LB.W0934" />'; // 
	i18n['BO14'] = '<spring:message code="LB.W0918" />'; // 
	i18n['BO15'] = '<spring:message code="LB.W1018" />'; // 
	i18n['BO16'] = '<spring:message code="LB.W1019" />';//
	i18n['BO17'] = '<spring:message code="LB.W0919" />'; // 
	
	var b012_sum_columns = [
		{ "data":"BO11", "title":i18n['BO11'] },
		{ "data":"BO12", "title":i18n['BO12'] },
		{ "data":"BO13", "title":i18n['BO13'] },
		{ "data":"BO14", "title":i18n['BO14'] },
		{ "data":"BO15", "title":i18n['BO15'] },
		{ "data":"BO16", "title":i18n['BO16'] },
		{ "data":"BO17", "title":i18n['BO17'] },
	];
	
	//0置中 1置右
	var b012_align = [0,0,1,0,0,1,1,0];
	var b012_sum_align = [0,1,1,1,1,1,1];
	
	// Ajax_b012-臺幣活期性存款餘額
	function getMyAssetsb012() {
		$("#b012").show();
		$("#b012_title").show();
		createLoadingBox("b012_BOX",'');
		createLoadingBox("b012_sum_BOX",'<spring:message code="LB.X0393" />');
		uri = '${__ctx}' + "/OVERVIEW/allacct_b012aj";
		console.log("allacct_b012aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax_b012, null, "b012");
	}

	// Ajax_callback
	function countAjax_b012(data){
		// 資料處理後呈現畫面
		if (data.result) {
			b012_rows = [];
			b012_sum_rows = [];
			data.data.REC.forEach(function(d,i){
				if(d.O18 == '1'){
					b012_row = {};
					b012_row['BO01'] = d.O15 + '<br>' + d.HO01;
					b012_row['BO02'] = d.O02 + '<br>' + d.O03;
					b012_row['BO03'] = d.O07 + '<br>' + d.O05;
					b012_row['BO04'] = d.O09 + '<br>' + d.O10;
					b012_row['BO05'] = d.O12 + '<br>' + d.O08;
					b012_row['BO06'] = d.O11 + '<br>' + d.O16;
					b012_row['BO07'] = d.O17 + '<br>' + d.O14;
					b012_row['BO08'] = '<input type="button" class="ttb-sm-btn btn-flat-orange" name="AAA"value="<spring:message code="LB.X2219" />" onclick="processQueryB012(' + d.O01 + ')">';
					b012_rows.push(b012_row);
				}else if(d.O18 == '2'){
					b012_sum_row = {};
					b012_sum_row['BO11'] = d.O08;
					b012_sum_row['BO12'] = d.O07;
					b012_sum_row['BO13'] = d.O12;
					b012_sum_row['BO14'] = d.O11;
					b012_sum_row['BO15'] = d.O16;
					b012_sum_row['BO16'] = d.O17;
					b012_sum_row['BO17'] = d.O14;
					b012_sum_rows.push(b012_sum_row);
					
				}
			});
		} else {
			// 錯誤訊息
			b012_columns = [ {
					"data" : "MSGCODE",
					"title" : '<spring:message code="LB.Transaction_code" />'
				}, {
					"data" : "MESSAGE",
					"title" : '<spring:message code="LB.Transaction_message" />'
				}
			];
			b012_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];
			// 錯誤訊息
			b012_sum_columns = [ {
					"data" : "MSGCODE",
					"title" : '<spring:message code="LB.Transaction_code" />'
				}, {
					"data" : "MESSAGE",
					"title" : '<spring:message code="LB.Transaction_message" />'
			} ];
			b012_sum_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];
			b012_sum_align = [0,0];
		}
		createTable("b012_BOX",b012_rows,b012_columns,b012_align);
		createTable("b012_sum_BOX",b012_sum_rows,b012_sum_columns,b012_sum_align);
	}

 	function processQueryB012(CDNO) {
		//配息查詢
		   var sCDNO = CDNO;
		   if(!(sCDNO.length==11)){
			   sCDNO = "0"+sCDNO;
			}		
		  	$("#b012_fastBarAction").attr("action", "${__ctx}/FUND/QUERY/oversea_data_result_dividend");
  			$("#CDNO_b012").val(sCDNO);
			$("#b012_fastBarAction").submit();	
		return false; 		
 	} 	


</script>
	<p id="b012_title" class="home-title" style="display: none"><spring:message code="LB.W1007" /></p>
	<div id="b012" class="main-content-block" style="display:none">
		<div id="b012_BOX" style="display:none"></div>
		<div id="b012_sum_BOX" style="display:none"></div>
	<form method="post" id="b012_fastBarAction" action="">
		<input type="hidden" id="CDNO_b012" name="CDNO" value="" /> 
		<input type="hidden" id="PREVIOUS_b012" name="PREVIOUS" value="oversea_balance" />
	</form>  
</div>