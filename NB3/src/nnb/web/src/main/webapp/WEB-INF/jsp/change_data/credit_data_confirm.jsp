<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<!-- 交易機制所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 50);
	// 開始查詢資料並完成畫面
	setTimeout("init()", 400);
	// 初始化驗證碼
	setTimeout("initKapImg()", 200);
	// 生成驗證碼
	setTimeout("newKapImg()", 300);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
	
	jQuery(function($) {
		$('.dtable').DataTable({
			scrollX: true,
			sScrollX: "99%",
			scrollY: true,
			bPaginate: false,
			bFilter: false,
			bDestroy: true,
			bSort: false,
			info: false,
		});
	});
});


function init(){
	// 表單驗證初始化
	$("#formId").validationEngine({ binded: false, promptPosition: "inline" });

	// 確認鍵 click
	goOn();
	// 上一頁按鈕 click
	goBack();
	// 判斷顯不顯示驗證碼
	chaBlock();
	// 交易機制 click
	fgtxwayClick();
	// 交易機制 表單驗證
	fgtxwayValidateEvent();
};

// 確認鍵 Click
function goOn() {
	$("#CMSUBMIT").click( function(e) {
		// 送出進表單驗證前將span顯示
		$("#hideblock").show();
		console.log("submit~~");
		// 表單驗證
		if ( !$('#formId').validationEngine('validate') ) {
			e.preventDefault();
		} 
		else {
			// 解除表單驗證
			$("#formId").validationEngine('detach');
			// 通過表單驗證
			processQuery();
		}
	});
}

//上一頁按鈕 click
function goBack() {
	// 上一頁按鈕
	$("#CMBACK").click(function () {
		// 遮罩
		initBlockUI();
		// 解除表單驗證
		$("#formId").validationEngine('detach');
		// 讓Controller知道是回上一頁
		$('#back').val("Y");
		// 回上一頁
		var action = '${__ctx}/CHANGE/DATA/credit_data_detail';
		$("#formId").attr("action", action);
		$("#formId").submit();
	});
}

// 通過表單驗證準備送出
function processQuery(){
	var fgtxway = $('input[name="FGTXWAY"]:checked').val();
	console.log("fgtxway: " + fgtxway);
	// 交易機制選項
	switch(fgtxway) {
		case '1':
			// IKEY
			useIKey();
		
			break;
		case '2':
			// 晶片金融卡
			var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
			useCardReader(capUri);
		
    		break;
        case '7'://IDGATE認證		 
            idgatesubmit= $("#formId");
            showIdgateBlock();		 
            break;   	
		default:
		//alert("<spring:message code= "LB.Alert001" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert001' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	}
}

//使用者選擇晶片金融卡要顯示驗證碼區塊
function fgtxwayClick() {
	$('input[name="FGTXWAY"]').change(function(event) {
		// 判斷交易機制決定顯不顯示驗證碼區塊
		chaBlock();
	});
}

// 判斷交易機制決定顯不顯示驗證碼區塊
function chaBlock() {
	var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
	// 交易機制選項
	if(fgtxway == '2'){
		// 若為晶片金融卡才顯示驗證碼欄位
		$("#chaBlock").show();
	}else{
		// 若非晶片金融卡則隱藏驗證碼欄位
		$("#chaBlock").hide();
	}
}


// 驗證碼刷新
function changeCode() {
	$('input[name="capCode"]').val('');
	// 大小版驗證碼用同一個
	console.log("changeCode...");
	$('img[name="kaptchaImage"]').hide().attr(
		'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

	// 登入失敗解遮罩
	unBlockUI(initBlockId);
}

// 初始化驗證碼
function initKapImg() {
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
}

// 生成驗證碼
function newKapImg() {
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').click(function() {
		$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
	});
}

function fgtxwayValidateEvent(){
	$('input[name="FGTXWAY"]').change(function(event) {
		if($('input[name="FGTXWAY"]:checked').val()=='1'){
			
		}
		if($('input[name="FGTXWAY"]:checked').val()=='2'){
			
		}
	});
}

</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 信用卡帳單地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0404" /></li>
		</ol>
	</nav>


	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D0404" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="${__ctx}/CHANGE/DATA/credit_data_result">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-message">
                        	<span><spring:message code="LB.D0418" /></span>
                        </div>
						<table class="stripe table table-striped ttb-table dtable m-0" data-show-toggle="first">
							<thead>
								<tr>
									<th data-title=""></th>
									<th data-title=""></th>
									<th data-title="<spring:message code="LB.D0409" />"><spring:message code="LB.D0409" /></th>
									<th data-title="<spring:message code="LB.Telephone" />"><spring:message code="LB.Telephone" /></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><spring:message code="LB.D0406" /></td>

									<td>
										<spring:message code="LB.D0149" />：<br>(<spring:message code="${cd_confirm.data.strBILLTITLE}" />)
									</td>
									<td style="text-align:left"><c:out value='${fn:escapeXml(cd_confirm.data.oldZIP)}' /><br><c:out value='${fn:escapeXml(cd_confirm.data.oldADDR)}' /></td>
									<td style="text-align:left">
										<spring:message code="LB.D0206" />：<c:out value='${fn:escapeXml(cd_confirm.data.oldHOMEPHONE)}' /><br>
										<spring:message code="LB.D0094" />：<c:out value='${fn:escapeXml(cd_confirm.data.oldOFFICEPHONE)}' /><br>
										<spring:message code="LB.D0413" />：<c:out value='${fn:escapeXml(cd_confirm.data.oldOFFICEEXT)}' /><br>
										<spring:message code="LB.D0069" />：<c:out value='${fn:escapeXml(cd_confirm.data.oldMOBILEPHONE)}' />
									</td>
								</tr>
								<tr>
									<td style="text-align:center"><spring:message code="LB.D0415" /></td>

									<td>
										<spring:message code="LB.D0149" />：
									</td>
<%-- 									<td style="text-align:left">${cd_confirm.data.newZIP}<br>${cd_confirm.data.newADDR1}${cd_confirm.data.newADDR2}</td> --%>
									<td style="text-align:left"><c:out value='${fn:escapeXml(cd_confirm.data.newZIP)}' /><br><c:out value='${fn:escapeXml(cd_confirm.data.newADDR1)}' /></td>
									<td style="text-align:left">
										<spring:message code="LB.D0206" />：<c:out value='${fn:escapeXml(cd_confirm.data.newHOMEPHONE)}' /><br>
										<spring:message code="LB.D0094" />：<c:out value='${fn:escapeXml(cd_confirm.data.newOFFICEPHONE)}' /><br>
										<spring:message code="LB.D0413" />：<c:out value='${fn:escapeXml(cd_confirm.data.newOFFICEEXT)}' /><br>
										<spring:message code="LB.D0069" />：<c:out value='${fn:escapeXml(cd_confirm.data.newMOBILEPHONE)}' />
									</td>
								</tr>
							</tbody>
						</table>
						
						<div class="ttb-input-block">
							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
											<!-- 交易機制 -->
										</h4>
								</label>
								</span> 
								<span class="input-block">
									<!-- 使用者是否可以使用IKEY -->
									<c:if test = "${sessionScope.isikeyuser}">
										<div class="ttb-input">
											<label class="radio-block"> <spring:message code="LB.Electronic_signature" />
											<input type="radio" name="FGTXWAY" value="1"> <span class="ttb-radio"></span></label> <!-- 電子簽章(請載入載具i-key) --> 
										</div>
									</c:if>
									<div class="ttb-input" name="idgate_group" style="display:none">		 
                                        <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                        <input type="radio" id="IDGATE" name="FGTXWAY" value="7"> 
	                                        <span class="ttb-radio"></span>
                                        </label>
                                    </div>
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
												
											<!-- 只能選晶片金融卡時，預設為非勾選讓使用者點擊後顯示驗證碼-->
											<c:choose>
												<c:when test="${!sessionScope.isikeyuser}">
													<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
												</c:when>
												<c:otherwise>
												 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
												</c:otherwise>
											</c:choose>
												
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
						
							<!-- 如果是非約定則不能使用IKEY，只能選晶片金融卡，使用者選擇晶片金融卡後才顯示驗證碼-->
							<div class="ttb-input-item row" id="chaBlock" style="display:none">
								<!-- 驗證碼 -->
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Captcha" /></h4>
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text"
										class="text-input" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class = "verification-img" src="" />
									<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
										onclick="changeCode()" value="<spring:message code="LB.Regeneration_1" />" />
								</span>
							</div> 
						</div>
						
						<input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page" />"/>
						<input class="ttb-button btn-flat-orange" type="button" id="CMSUBMIT" name="CMSUBMIT" value="<spring:message code="LB.Confirm" />">
						
						<input type="hidden" id="ADOPID" name="ADOPID" value="N900">
						<input type="hidden" id="oldZIP" name="oldZIP" value="<c:out value='${fn:escapeXml(cd_confirm.data.oldZIP)}' />">
						<input type="hidden" id="oldADDR" name="oldADDR" value="<c:out value='${fn:escapeXml(cd_confirm.data.oldADDR)}' />">
						<input type="hidden" id="oldHOMEPHONE" name="oldHOMEPHONE" value="<c:out value='${fn:escapeXml(cd_confirm.data.oldHOMEPHONE)}' />">
						<input type="hidden" id="oldOFFICEPHONE" name="oldOFFICEPHONE" value="<c:out value='${fn:escapeXml(cd_confirm.data.oldOFFICEPHONE)}' />">
						<input type="hidden" id="oldOFFICEEXT" name="oldOFFICEEXT" value="<c:out value='${fn:escapeXml(cd_confirm.data.oldOFFICEEXT)}' />">
						<input type="hidden" id="oldMOBILEPHONE" name="oldMOBILEPHONE" value="<c:out value='${fn:escapeXml(cd_confirm.data.oldMOBILEPHONE)}' />">
						<input type="hidden" id="newZIP" name="newZIP" value="<c:out value='${fn:escapeXml(cd_confirm.data.newZIP)}' />">
<%-- 						<input type="hidden" id="newADDR" name="newADDR" value="${cd_confirm.data.newADDR1}${cd_confirm.data.newADDR2}"> --%>
						<input type="hidden" id="newADDR" name="newADDR" value="<c:out value='${fn:escapeXml(cd_confirm.data.newADDR1)}' />">
						<input type="hidden" id="newHOMEPHONE" name="newHOMEPHONE" value="<c:out value='${fn:escapeXml(cd_confirm.data.newHOMEPHONE)}' />">
						<input type="hidden" id="newOFFICEPHONE" name="newOFFICEPHONE" value="<c:out value='${fn:escapeXml(cd_confirm.data.OFFICETEL)}' />">
						<input type="hidden" id="newMOBILEPHONE" name="newMOBILEPHONE" value="<c:out value='${fn:escapeXml(cd_confirm.data.newMOBILEPHONE)}' />">
						<input type="hidden" id="SMSA" name="SMSA" value="<c:out value='${fn:escapeXml(cd_confirm.data.SMSA)}' />">
						
						<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
						<input type="hidden" id="jsondc" name="jsondc" value="<c:out value='${fn:escapeXml(cd_confirm.data.jsondc)}' />">
						<input type="hidden" id="CHIP_ACN" name="CHIP_ACN" value="">
						<input type="hidden" id="ACN" name="ACN" value="">
						<input type="hidden" id="ISSUER" name="ISSUER" value="">
						<input type="hidden" id="ACNNO" name="ACNNO" value="">
						<input type="hidden" id="OUTACN" name="OUTACN" value="">
						<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
						<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
						<input type="hidden" id="TAC" name="TAC" value="">
						<input type="hidden" id="TRMID" name="TRMID" value="">
					</div>
				</div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>