<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {

	    	//列印
        	$("#PRINT").click(function(){
				var params = {
					"jspTemplateName":"apply_deposit_account_result_print",
					"jspTitle":'<spring:message code= "LB.D1100" />',
					"DUEDT":"${result_data.data.DUEDT}",
					"WEEK":"${result_data.data.WEEK}",
					"BRHNAME":"${result_data.data.BRHNAME}",
					"STAEDYN":"${input_data.STAEDYN}",
					"BRHTEL":"${result_data.data.BRHTEL}"
				};
				openWindowWithPost("${__ctx}/ONLINE/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
			$("#GOBAKE").click(function(e){
	        	var action = '${__ctx}/login';
				$("form").attr("action", action);
    			$("form").submit();
			});
		}
		
		function openMenu() {
			var main = document.getElementById("main");
			window.open('${__ctx}/public/OpenAccount.pdf');		   
		}
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">開戶申請</a></li>
			<li class="ttb-breadcrumb-item"><a href="#">預約開立存款戶</a></li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--基金線上預約開戶作業 -->
					<spring:message code="LB.D1100"/>
				</h2>
				<div id="step-bar">
					<ul>
						<li class="finished">注意事項與權益</li>
						<li class="finished">開戶資料</li>
						<li class="finished">確認資料</li>
						<li class="active">完成預約</li>
					</ul>
				</div>
				<form method="post" id="formId" name="formId" action="${__ctx}/ONLINE/APPLY/apply_deposit_account_p2">
					<input type="hidden" name="TYPE" value="5">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-message">
								<p>完成預約</p>
							</div>
							
							<div class="text-left">
								<p>親愛的客戶您好：
									謝謝您完成『預約開立存款戶』申請作業流程，請親自攜帶
									<span style="color:#FF8000">
										印鑑、身分證、第二證件 (健保卡、駕照等)及供開戶起存的現金 (至少1000元)
									</span>
									，於營業時間至指定分行辦理開戶。為節省您開戶作業時間，建議您可列印本預約成功頁面。
								</p>
								<c:if test="${ input_data.STAEDYN.equals('Y') }">
									<p>提醒您若申辦為起家金帳戶
										<span style="color:#FF8000">『本行備有專屬起家金悠遊Debit卡、請至往來分行服務台申辦。』</span>
									</p>
								</c:if>
								<p>提醒您！在
									<span style="color:#FF8000">
										『${result_data.data.DUEDT}(${result_data.data.WEEK})』
									</span>
									前未來辦理開戶，本行將取消您的預約開戶資料。 若有任何問題，您可撥電話至您所指定的服務分行：
									<span style="color:#FF8000">
										${result_data.data.BRHNAME}
									</span>
									(${result_data.data.BRHTEL}) 詢問，我們將竭誠為您服務。
								</p>
	
								<p>祝您事業順心，萬事如意！</p>
							</div>
							
							<!-- 確定 -->
							<input type="button" id="PRINT" value="<spring:message code="LB.Print"/>" class="ttb-button btn-flat-gray" />
							<input type="button" id="GOBAKE" value="回首頁" class="ttb-button btn-flat-orange"/>
							<input type="button" id="CLOSE" value="繼續其他申請" class="ttb-button btn-flat-orange"  onClick="window.close();"/>
						</div>
					</div>
						
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>