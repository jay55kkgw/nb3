<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<!-- 元件驗證身分JS -->
<script type="text/javascript" src="${__ctx}/component/util/checkIdentity.js"></script>
<script type="text/javascript" src="${__ctx}/js/dataTables.rowsGroup.js"></script>

<script type="text/javascript">
	var notCheckIKeyUser = false; // 不驗證是否是IKey使用者
	var myobj = null; // 自然人憑證用
	var urihost = "${__ctx}";
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 100);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		
		jQuery(function($) {
			$('.dtable').DataTable({
				scrollX: true,
				sScrollX: "99%",
				scrollY: true,
				bPaginate: false,
				bFilter: false,
				bDestroy: true,
				bSort: false,
				info: false,
				rowsGroup: [0, 1, 2],
			});
		});
	});
	
	function init(){
		
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		
// 		if(${fcy_data_confirm.data.fgtxway == '1'}){
// 			$("#CMIKEY").prop('checked', true);
// 		}
// 		else if(${fcy_data_confirm.data.fgtxway == '2'}){
// 			$("#CMCARD").prop('checked', true);
// 		}
		
		//確定 
		$("#CMSUBMIT").click(function() {		
			
		    if (!$('#formId').validationEngine('validate')) {
		        e.preventDefault();
		    } else {
		    	$("#formId").validationEngine('detach');
// 					initBlockUI();
					$("#formId").validationEngine('detach');
					$("#formId").attr("action","${__ctx}/CHANGE/DATA/fcy_data_result");
// 					$("#formId").submit();
					processQuery(); 
		    }
		});
		
		//上一頁按鈕
// 		$("#previous").click(function() {
// 			initBlockUI();
// 			fstop.getPage('${pageContext.request.contextPath}'+'/CHANGE/DATA/communication_data_plural','', '');
// 		});
		
				//上一頁按鈕
		$("#previous").click(function() {

			var action = '${pageContext.request.contextPath}' + '${previous}'
			$('#back').val("Y");
			$("form").attr("action", action);
			initBlockUI();
			$("#formId").validationEngine('detach');
			$("form").submit();

		});
		
		// 判斷顯不顯示驗證碼
		chaBlock();
		// 交易機制 click
		fgtxwayClick();
		
	}

		// 交易機制選項
		function processQuery() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		switch (fgtxway) {
				// IKEY
			case '1':
				useIKey();
				break;
				// 晶片金融卡
			case '2':
// 				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				useCardReader();
				break;
	        case '7'://IDGATE認證		 
	            idgatesubmit= $("#formId");		 
	            showIdgateBlock();		 
                break;
			default:
				//alert("<spring:message code="LB.Alert001" />");
				errorBlock(
						null, 
						null,
						["<spring:message code='LB.Alert001' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
// 				unBlockUI(initBlockId);
			}
		}
		
		// 使用者選擇晶片金融卡要顯示驗證碼區塊
			function fgtxwayClick() {
				$('input[name="FGTXWAY"]').change(function(event) {
					// 判斷交易機制決定顯不顯示驗證碼區塊
					chaBlock();
			});
		}
			// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
				console.log("fgtxway: " + fgtxway);	
				
				switch(fgtxway) {
				case '1':
					$("#chaBlock").hide();
					break;
				case '2':
					$("#chaBlock").show();
			    	break;
				case '7':
					$("#chaBlock").hide();
			    	break; 	
				default:
					$("#chaBlock").hide();
			}	
		}
		// 驗證碼刷新
		function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		
		// 登入失敗解遮罩
// 		unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function () {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100))
			.fadeIn();
		});
		}
	

</script>
</head>
<body>

	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 外匯進口/出口/匯兌通訊地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0385" /></li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0385" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<input type="hidden" id="check" value="0"/>
			<form id="formId" method="post" action="${__ctx}/CHANGE/DATA/communication_data_plural_r">
				<input type="hidden" id="TXID" name="TXID" value="N570_2" />
				<input type="hidden" name="UPDATE" value="N">
				<input type="hidden" name="TYPE" value="01">		
<%-- 				<input type="hidden" id="ROWDATA" name="ROWDATA" value="${fcy_data_confirm.data.sessionRec}">			 --%>
				<input type="hidden" id="ROWDATA" name="ROWDATA" value="${fcy_data_confirm.data.dataSet}">			
<%-- 				<input type="hidden" id="dataRow" name="dataRow" value="${fcy_data_confirm.data.dataSet}">	 --%>
			    <!-- 			晶片金融卡 -->
				<input type="hidden" id="jsondc" name="jsondc" value='${fcy_data_confirm.data.jsondc}'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<div class="main-content-block row">
					<div class="col-12">
						<br>
						<!-- 線上約定轉入帳號註銷表 -->
						<table class="stripe table ttb-table dtable m-0" data-show-toggle="first">
							<thead>
								<tr>
									<th rowspan="2"><spring:message code="LB.D0375" /></th>
									<th colspan="2"><spring:message code="LB.D0376" /></th>
									<th rowspan="2"><spring:message code="LB.Telephone" /></th>
								</tr>
								<tr style="display:none">
									<th></th>
									<th></th>
								</tr>
							</thead>
								<c:set var="dataList" value="${fcy_data_confirm_s.data}" />
	                        <tbody>
                                <tr>
                                    <td>${dataList.BCHNAME}(${dataList.BCHID})</td>
                                    <td><spring:message code="LB.D0392" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0394" />:</span><br>
                                    	${dataList.CUSAD1C}
                                    	${dataList.CUSAD2C}
                                    </td>
                                    <td><span class="sec-title">TEL:</span><br>
                                    	${dataList.CUSTEL1}
                                    </td>
                                </tr>
                                <tr>
                                	<td>${dataList.BCHNAME}(${dataList.BCHID})</td>
                                    <td><spring:message code="LB.D0392" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0396" />:</span><br>
                                    	${dataList.CUSADD1}
                                    	${dataList.CUSADD2}
                                    	${dataList.CUSADD3}
                                    </td>
                                    <td><span class="sec-title">FAX</span><br>
                                    	${dataList.CUSTEL2}
                                    </td>
                                </tr>
	                         <c:set var="dataSet" value="${fcy_data_confirm.data.dataSet}" />	                         
                                <tr>
                                    <td>${dataSet.BCHNAME}(${dataSet.BCHID})</td>
                                    <td><spring:message code="LB.D0393" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0394" />:</span><br>
                                    	${dataSet.CUSAD1C}
                                    	${dataSet.CUSAD2C}
                                    </td>
                                    <td><span class="sec-title">TEL:</span><br>
                                    	${dataSet.CUSTEL1}
                                    </td>
                                </tr>
                                <tr>
                                	<td>${dataSet.BCHNAME}(${dataSet.BCHID})</td>
                                    <td><spring:message code="LB.D0393" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0396" />:</span><br>
                                    	${dataSet.CUSADD1}
                                    	${dataSet.CUSADD2}
                                    	${dataSet.CUSADD3}
                                    </td>
                                    <td><span class="sec-title">FAX</span><br>
                                    	${dataSet.CUSTEL2}
                                    </td>
                                </tr>
	                         </tbody>
						</table>
						
						<!-- 交易機制 -->
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.Transaction_security_mechanism" />
									</label>
								</span>
								<span class="input-block">
									<!-- 使用者是否可以使用IKEY -->
									<c:if test="${fcy_data_confirm.data.fgtxway == '1'}">
										<c:if test="${sessionScope.isikeyuser}">
											<!--電子簽章(請載入載具i-key) -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Electronic_signature" />
													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" checked="checked"/>
													<span class="ttb-radio"></span>
												</label>
											</div>
										</c:if>
									</c:if>
									<div class="ttb-input" name="idgate_group" style="display:none">		 
                                        <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                        <input type="radio" id="IDGATE" name="FGTXWAY" value="7"> 
	                                        <span class="ttb-radio"></span>
                                        </label>		 
                                    </div>
									<!-- 晶片金融卡 -->
									<c:if test="${fcy_data_confirm.data.fgtxway == '2'}">
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Financial_debit_card" />
												<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
									</c:if>
								</span>
							</div>							
								<!-- 驗證碼-->
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
									<span class="input-title">
										<label>
											<!-- 驗證碼 -->
											<spring:message code="LB.Captcha" />
										</label>
									</span>
									<span class="input-block">
										<spring:message code="LB.Captcha" var="labelCapCode" />
										<input id="capCode" name="capCode" type="text" class="text-input"
											placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
										<img name="kaptchaImage" class = "verification-img" src="" />
										<button class="ttb-sm-btn btn-flat-orange" type="button" name="reshow"
											onclick="changeCode()">
											<spring:message code="LB.Regeneration" />
										</button>
									</span>
								</div>
						</div>
						<!-- 重新輸入 -->
						<input type="reset" id="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />"/>
						<!-- 確定 -->
                        <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>