<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
$(document).ready(function() {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	// 開始查詢資料並完成畫面
	setTimeout("init()",20);
	setTimeout("initDataTable()",100);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
});
function init(){			
	//initFootable();
//		fgtxdateEvent();
	$("#formId").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	$("#printbtn").click(function(){
		var params = {
				"jspTemplateName":"passbook_loss_result_print",
				"jspTitle":"<spring:message code= "LB.X0447" />",
				"CMQTIME":"${pbook_loss_result.data.CMQTIME}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});		
}
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 掛失服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0441" /></li>
    <!-- 新臺幣存摺掛失     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0447" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.X0447" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4><spring:message code="LB.System_time" />：</h4>
									</label>
								</span> 
								<span class="input-block"> 
									<div class="ttb-input">
										<span> ${pbook_loss_result.data.CMQTIME} </span>
									</div>
								</span>
							</div>
						</div>
						<!-- 新臺幣存摺掛失 表格 -->
						<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
							<thead>
								<tr>
									<!-- 狀態-->
									<th data-title='<spring:message code="LB.Status" />'><spring:message code="LB.Status" /></th>
									<!-- 帳號 -->
									<th data-title='<spring:message code="LB.Account" />'><spring:message code="LB.Account" /></th>
									<!-- 可用餘額 -->
									<th data-title='<spring:message code="LB.Available_balance" />'><spring:message code="LB.Available_balance" /></th>
									<!-- 帳戶餘額 -->
									<th data-title='<spring:message code="LB.Account_balance" />'><spring:message code="LB.Account_balance" /></th>
									<!-- 本日交換票 -->
									<th data-title='<spring:message code="LB.Exchange_ticket_today" />'><spring:message code="LB.Exchange_ticket_today" /></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="dataList" items="${pbook_loss_result.data.REC }">
									<tr>
										<td class="text-center">
											<c:if test="${dataList.STATUS == '掛失成功'}">
                                        		<spring:message code="LB.Report_successful" />
                                        	</c:if>
                                        	<c:if test="${dataList.STATUS != '掛失成功'}">
                                        		${dataList.STATUS }
                                        	</c:if>
										</td>
										<td class="text-center">${dataList.ACN }</td>
										<td class="text-right">${dataList.BALANCE }</td>
										<td class="text-right">${dataList.BALANCE }</td>
										<td class="text-center">${dataList.CLR }</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<!-- 列印 -->
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><span><spring:message code="LB.Demand_Passbook_loss_P2_D1" /></span></li>
         			<li><span><spring:message code="LB.Demand_Passbook_loss_P2_D2" /></span></li>
          			<li><span><spring:message code="LB.Demand_Passbook_loss_P2_D3" /></span></li>
          			<li><span><spring:message code="LB.Demand_Passbook_loss_P2_D4" /></span></li>
          			<li><span><spring:message code="LB.Demand_Passbook_loss_P2_D5" /></span></li>
				</ol>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>