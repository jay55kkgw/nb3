<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<style>
		.Line-break{
			white-space:normal;
			word-break:break-all;
			width:100px;
			word-wrap:break-word;
		}
	</style>
</head>
<body>
    <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 預約交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X1477" /></li>
    <!-- 預約交易結果查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0053" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		<main class="col-12">	
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.W0053" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
							<li>
								<!-- 查詢時間 -->
								<h3><spring:message code="LB.Inquiry_time" /> </h3>								
								<p>
								${f_reservation_trans_result.data.CMQTIME}
								</p>
							</li>
							<li>
								<!-- 查詢期間 -->
								<h3><spring:message code="LB.Inquiry_period_1" /> </h3>								
								<p>
								${f_reservation_trans_result.data.cmedate}
								</p>
							</li>
							<li>
								<!-- 交易狀態-->
								<h3><spring:message code="LB.W0054" /> </h3>
								<p>
								<c:choose>
									<c:when test="${f_reservation_trans_result.data.FGTXSTATUS == 'All'}">
										<spring:message code="LB.All" />
									</c:when>
									<c:when test="${f_reservation_trans_result.data.FGTXSTATUS == '0'}">
										<spring:message code="LB.D1099" />
									</c:when>
									<c:when test="${f_reservation_trans_result.data.FGTXSTATUS == '1'}">
										<spring:message code="LB.W0056" />
									</c:when>
									<c:otherwise>
										<spring:message code="LB.W0057" />
									</c:otherwise>
								</c:choose>
<%-- 								${f_reservation_trans_result.data.FGTXSTATUS} --%>
								</p>
							</li>
							<li>
								<!-- 資料總數 -->
								<h3><spring:message code="LB.Total_records" /> </h3>								
								<p>
								${f_reservation_trans_result.data.COUNT} <spring:message code="LB.Rows" />
								</p>
							</li>							
							<li>
								<!-- 資料總數 -->
								<h3><spring:message code="LB.Report_name" /> </h3>								
								<p>
								<spring:message code="LB.W0341" />
								</p>
							</li>							
						</ul>
						
						<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
						<thead>
							<tr>
								<!-- 預約編號 -->
								<th><spring:message code="LB.Booking_number" /></th>
								<!-- 轉帳日期 -->		<!-- 轉出帳號 -->						
								<th><spring:message code="LB.Transfer_date" /><hr/><spring:message code="LB.Payers_account_no" /></th>
								<!-- 轉出金額 -->
								<th><spring:message code="LB.Deducted" /></th>
								<!-- 銀行名稱/轉入帳號 -->
								<th><spring:message code="LB.Bank_name" /><br><spring:message code="LB.Payees_account_no" /></th>
								<!-- 轉入金額 -->
								<th><spring:message code="LB.Buy" /></th>
								<!-- 匯率-->
								<th><spring:message code="LB.Exchange_rate" /></th>
								<!-- 手續費/郵電費 -->
								<th><spring:message code="LB.D0507" /><br><spring:message code="LB.W0345" /></th>
								<!-- 國外費用 -->
								<th><spring:message code="LB.W0346" /></th>
								<!-- 交易類別 --><!-- 備註 -->
								<th><spring:message code="LB.Transaction_type" /><hr/><spring:message code="LB.Note" /></th>
								<!-- 轉帳結果 -->
								<th><spring:message code="LB.W0063" /></th>
							</tr>
						</thead>
<!--          列表區 -->
						<c:if test="${f_reservation_trans_result.data.COUNT == '0'}">
							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</c:if>
						<c:if test="${f_reservation_trans_result.data.COUNT > '0'}">
						<tbody>
         					<c:forEach var="dataList" items="${f_reservation_trans_result.data.REC}">
							<tr>            	
								<!-- 預約編號 -->
								<td class="text-center">${dataList.FXSCHNO}</td>
								<!-- 轉帳日期 -->	<!-- 轉出帳號 -->
								<td class="text-center">
									${dataList.FXTXDATE}
									<hr/>
									${dataList.FXWDAC}
								</td>                
								<!-- 轉出金額 -->
								<td class="text-center">${dataList.FXWDCURR}<br>${dataList.FXWDAMT}</td>		          	               
								<!-- 銀行名稱/轉入帳號 -->
								<td class="text-center">
									<c:if test="${not empty dataList.FXSVBH}">
										${dataList.FXSVBH}
										<br>
									</c:if>
									${dataList.FXSVAC}
								</td>								                
								<!-- 轉入金額 -->
								<td class="text-center">${dataList.FXSVCURR}<br>${dataList.FXSVAMT}</td>                
								<!-- 匯率-->
								<td class="text-center">${dataList.FXEXRATE}</td>
								<!-- 手續費/郵電費 -->
								<td class="text-center">${dataList.FXEFEECCY}<br>${dataList.FXEFEE}<br>${dataList.FXTELFEE}</td>
								<!-- 國外費用 -->
								<td class="text-center">${dataList.FXEFEECCY}<br>${dataList.str_OURCHG}</td>
								<!-- 交易類別 --><!-- 備註 -->
								<td class="text-center Line-break">${dataList.ADOPIDH}<hr/>
									<c:if test="${empty dataList.FXTXMEMO}">
									-
									</c:if>
									<c:if test="${not empty dataList.FXTXMEMO}">
										${dataList.FXTXMEMO}
									</c:if>
								</td>
								<!-- 轉帳結果 -->
								<td class="text-center"><spring:message code="${dataList.FXTXSTATUS}" /><br><a href="javascript:return false;" style="color:blue;" title="${dataList.CODEMSG}">${dataList.FXEXCODE}</a></td>
							</tr>	
							</c:forEach>						
							</tbody>
							</c:if>
						</table>
						<input id="previous" name="previous" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />	
						<input type="button" class="ttb-button btn-flat-orange" id = "printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
					<!-- 說明 -->
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li style="font-weight:bold; color:#FF0000;"><span><spring:message code="LB.F_Reservation_Trans_P2_D1" /></span></li>
					<li><span><spring:message code="LB.F_Reservation_Trans_P2_D2" /></span></li>
					<li><span><spring:message code="LB.F_Reservation_Trans_P2_D3" /></span></li>
				</ol>
				<form method="post" id="formId" action="${__ctx}/download">
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="LB.W0053" />
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/>
					<input type="hidden" name="CMQTIME" value="${f_reservation_trans_result.data.CMQTIME}"/>
					<input type="hidden" name="cmedate" value="${f_reservation_trans_result.data.cmedate}"/>
					<input type="hidden" name="COUNT" value="${f_reservation_trans_result.data.COUNT}"/>
					<input type="hidden" name="FGTXSTATUS" value="${f_reservation_trans_result.data.FGTXSTATUS_D}"/>
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="11"/>
					<input type="hidden" name="headerBottomEnd" value="8"/>
					<input type="hidden" name="rowStartIndex" value="9"/>
					<input type="hidden" name="rowRightEnd" value="11"/>
					<!-- 					上到下長度 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="10"/> 					
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="false"/>
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->


	<%@ include file="../index/footer.jsp"%>
		<script type="text/javascript">
		$(document).ready(function(){
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()",10);
			// 開始查詢資料並完成畫面
			setTimeout("init()",20);
			setTimeout("initDataTable()",100);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)",500);	
		});
		function init(){
			//initFootable();

			
			//上一頁按鈕
			$("#previous").click(function() {
				initBlockUI();
				fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/RESERVATION/f_reservation_trans','', '');
			});
			
			$("#printbtn").click(function(){
				//預約交易結果查詢
				var params = {
						"jspTemplateName":"f_reservation_trans_result_print",
						"jspTitle":"<spring:message code= "LB.W0053" />",
						"CMQTIME":"${f_reservation_trans_result.data.CMQTIME}",
						"cmdate":"${f_reservation_trans_result.data.cmedate}",
						"COUNT":"${f_reservation_trans_result.data.COUNT}",
						"FGTXSTATUS":"${f_reservation_trans_result.data.FGTXSTATUS}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});				
		}
		//選項
	 	function formReset() {
// 	 		initBlockUI();
			if ($('#actionBar').val()=="excel"){
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/f_reservation_trans.xls");
	 		}else if ($('#actionBar').val()=="txt"){
				$("#downloadType").val("TXT");
				$("#templatePath").val("/downloadTemplate/f_reservation_trans.txt");
	 		}
			$("#formId").attr("target", "");
            $("#formId").submit();
            $('#actionBar').val("");
        }
		</script>
</body>
</html>