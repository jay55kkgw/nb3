<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		//setTimeout("initDataTable()",200);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		
	});
	
	function init(){
		$('div[id*="page"]').hide();
		$("#page1").show();
		$("#CMBACK").click(function(e){
			$("#formId").attr("action","${__ctx}/login");
			$("#formId").submit();
		});
		
		//footer 點選系統/瀏覽器需求要顯示
		var Tag = '${common_problem.data.tag}';
		if(Tag != null && Tag != ""){
			$("#trans1_4").click();
			$("#popup1-41").addClass("show");
			$("#popup1-Q1").removeClass("collapsed");
		}
	}
	function trans_1(id){
		$("input[name^='trans1_']").removeClass('active');
		$('#'+id).addClass('active');
		var pageshow = id.replace("trans1_","");
		$('div[id*="page"]').hide();
		$("#page"+pageshow).show();
	}
	var flag1 = true;
	var flag2 = true;
	var flag3 = true;
	function trans_5(id){
		$("input[name^='trans5_']").removeClass('active');
		$('#'+id).addClass('active');
		var pageshow = id.replace("trans5_","");
		$('div[id*="err"]').hide();
		$("#err"+pageshow).show();
		if(pageshow == 1 && flag1){
			setTimeout("dodatataable()",200);
			flag1=false;
		}
		if(pageshow == 2 && flag2){
			setTimeout("dodatataable2()",200);
			flag2=false;
		}
		if(pageshow == 3 && flag3){
			setTimeout("dodatataable3()",200);
			flag3=false;
		}
	}
	
	function dt(){
		$("#trans5_1").click();
	}
	function dodatataable(){
		jQuery(function($) {
			$('.dtable1').DataTable({
				scrollX: true,
				sScrollX: "99%",
				lengthChange: false,
				searching: true,
				bPaginate: true,
				bFilter: false,
				bDestroy: true,
				bSort: false,
				info: false,
				language:{
					"paginate":{
						"previous":"上一頁",
						"next":"下一頁"
					},
					"sZeroRecords":"查無此代碼",
					"sSearch":"搜尋:"
				}
			});
		});
	}
	function dodatataable2(){
		jQuery(function($) {
			$('.dtable2').DataTable({
				scrollX: true,
				sScrollX: "99%",
				lengthChange: false,
				searching: true,
				bPaginate: true,
				bFilter: false,
				bDestroy: true,
				bSort: false,
				info: false,
				language:{
					"paginate":{
						"previous":"上一頁",
						"next":"下一頁"
					},
					"sZeroRecords":"查無此代碼",
					"sSearch":"搜尋:"
				}
			});
		});
	}
	function dodatataable3(){
		jQuery(function($) {
			$('.dtable3').DataTable({
				scrollX: true,
				sScrollX: "99%",
				lengthChange: false,
				searching: true,
				bPaginate: true,
				bFilter: false,
				bDestroy: true,
				bSort: false,
				info: false,
				language:{
					"paginate":{
						"previous":"上一頁",
						"next":"下一頁"
					},
					"sZeroRecords":"查無此代碼",
					"sSearch":"搜尋:"
				}
			});
		});
	}
	
	
</script>

<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">常見問題</a></li>
		</ol>
	</nav>
	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					常見問題
				</h2>
				<form id="formId" method="post" action="">
					<div class="main-content-block card-block shadow-box terms-pup-blcok radius-50">
	                   	<nav class="w-100">
	                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
	                            <a class="nav-item nav-link active" id="nav-trans-1-tab" data-toggle="tab" href="#nav-trans-1" role="tab" aria-selected="false">網路銀行</a>
	                            <a class="nav-item nav-link" id="nav-trans-2-tab" data-toggle="tab" href="#nav-trans-2" role="tab" aria-selected="true">外匯申報</a>
	                            <a class="nav-item nav-link" id="nav-trans-3-tab" data-toggle="tab" href="#nav-trans-3" role="tab" aria-selected="true">基金下單</a>
	                            <a class="nav-item nav-link" id="nav-trans-4-tab" data-toggle="tab" href="#nav-trans-4" role="tab" aria-selected="true">憑證註冊</a>
	                            <a class="nav-item nav-link" id="nav-trans-5-tab" data-toggle="tab" href="#nav-trans-5" role="tab" aria-selected="true" onclick='dt();'>錯誤代碼</a>
	                        </div>
	                    </nav>
	                    <div class="col-12 tab-content" id="nav-tabContent">
	                        <div class="ttb-input-block tab-pane fade show active" id="nav-trans-1" role="tabpanel" aria-labelledby="nav-tab-1">
	                            <div class="card-select-block">
	                                <input type="button" class="ttb-sm-btn active" name="trans1_1" id="trans1_1" onclick="trans_1(this.id)" value="申請資格" />
	                                <input type="button" class="ttb-sm-btn" name="trans1_2" id="trans1_2" onclick="trans_1(this.id)" value="服務項目" />
	                                <input type="button" class="ttb-sm-btn" name="trans1_3" id="trans1_3" onclick="trans_1(this.id)" value="預約轉帳" />
	                                <input type="button" class="ttb-sm-btn" name="trans1_4" id="trans1_4" onclick="trans_1(this.id)" value="系統需求" />
	                                <input type="button" class="ttb-sm-btn" name="trans1_5" id="trans1_5" onclick="trans_1(this.id)" value="安全宣言" />
	                                <input type="button" class="ttb-sm-btn" name="trans1_6" id="trans1_6" onclick="trans_1(this.id)" value="操作說明" />
	                                <input type="button" class="ttb-sm-btn" name="trans1_7" id="trans1_7" onclick="trans_1(this.id)" value="費用" />
	                            </div>
	                            <%@ include file="problem_obk_qualifications.jsp"%>
	                            <%@ include file="problem_obk_service.jsp"%>
	                            <%@ include file="problem_obk_appiontment_transfer.jsp"%>
	                            <%@ include file="problem_obk_requirement.jsp"%>
	                            <%@ include file="problem_obk_safe.jsp"%>
	                            <%@ include file="problem_obk_instruction.jsp"%>
	                            <%@ include file="problem_obk_payment.jsp"%>
	                            
	                        </div>
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-2" role="tabpanel" aria-labelledby="nav-profile-tab">
	                            <div class="card-select-block">
	                                <input type="button" class="ttb-sm-btn active" name="trans2_1" id="trans2_1" value="外匯收支或交易申報書填報說明(結售外匯)">
	                            </div>
	                            <%@ include file="problem_obk_f_exchange_declaration.jsp"%>
	                        </div>
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-3" role="tabpanel" aria-labelledby="nav-profile-tab">
	                            <div class="card-select-block">
	                                <input type="button" class="ttb-sm-btn active" name="trans3_1" id="trans3_1" value="基金下單">
	                            </div>
	                            <%@ include file="problem_obk_fund.jsp"%>
	                       	</div>
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-4" role="tabpanel" aria-labelledby="nav-profile-tab">
	                            <div class="card-select-block">
	                                <input type="button" class="ttb-sm-btn active" name="trans4_1" id="trans4_1" value="憑證註冊中心">
	                            </div>
	                            <%@ include file="problem_obk_certificate.jsp"%>
	                       	</div>
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-5" role="tabpanel" aria-labelledby="nav-profile-tab">
	                            <div class="card-select-block">
	                                <input type="button" class="ttb-sm-btn active" name="trans5_1" id="trans5_1" onclick="trans_5(this.id)" value="中心主機" />
	                                <input type="button" class="ttb-sm-btn" name="trans5_2" id="trans5_2" onclick="trans_5(this.id)" value="瀏覽器元件" />
	                                <input type="button" class="ttb-sm-btn" name="trans5_3" id="trans5_3" onclick="trans_5(this.id)" value="電子簽章">
	                            </div>
 	                            <%@ include file="error_code.jsp"%>
 								
	                       	</div>
	                    </div>
                        <input type="BUTTON" class="ttb-button btn-flat-gray" value="返回" name="CMBACK" id="CMBACK">
	                </div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
