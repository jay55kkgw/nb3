<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 簽署信用卡分期付款同意書     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.B0005" /></li>
		</ol>
	</nav>


 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.B0010"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form method="post" id="formId">
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${input_data.TOKEN}" /><!-- 防止重複交易 -->
				<input type="hidden" id="NAME" name="NAME" value="${ result_data.data.NAME }">
                
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
						
							
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.System_time"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.data.CMQTIME }</p>
									</div>
								</span>
							</div>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.D0191"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
											${ result_data.data.NAME } &nbsp;
											<c:if test="${ !result_data.data.SEX.equals('') }">
												<spring:message code="${ result_data.data.SEX }"/> &nbsp;
											</c:if>
											<spring:message code="LB.D0192_3"/><spring:message code="LB.B0011"/>
										</p>
									</div>
								</span>
							</div>
						</div>
                        <input type="button" id="CMSUBMIT" value="<spring:message code="LB.X1169"/>" class="ttb-button btn-flat-orange" />		
						<!--回上頁 -->
<!--                         <input type="button" name="CMBACK" id="CMBACK" value="繼續申請" class="ttb-button btn-flat-gray">					 -->
					</div>
				</div>
			</form>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
		
	    function init(){
	    	
    		//上一頁按鈕
    		$("#CMSUBMIT").click(function() {
    			var action = '${__ctx}/CREDIT/SIGN/card_sign_menu';
    			$("form").attr("action", action);
    			$("form").submit();
    		});
	    }	
	    

 	</script>
</body>
</html>
 
