<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">

	// 交易機制選項
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
	
		switch(fgtxway) {
			case '0':
				//alert('<spring:message code= "LB.SSL_password" />...');
				errorBlock(
							null, 
							null,
							['<spring:message code= 'LB.SSL_password' />...'], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				break;
				
			case '1':
				//alert("IKey...");
				errorBlock(
							null, 
							null,
							["IKey..."], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				var jsondc = $("#jsondc").val();
				
				// 遮罩後不給捲動
				document.body.style.overflow = "hidden";

				// 呼叫IKEY元件
				uiSignForPKCS7(jsondc);
				
				// 解遮罩後給捲動
				document.body.style.overflow = 'auto';
				
				break;
				
			case '2':
				//alert('<spring:message code= "LB.Financial_debit_card" />');
				errorBlock(
							null, 
							null,
							['<spring:message code= 'LB.Financial_debit_card' />'], 
							'<spring:message code= "LB.Quit" />', 
							null
						);

				// 遮罩後不給捲動
				document.body.style.overflow = "hidden";
				
				// 呼叫讀卡機元件
				listReaders();

				// 解遮罩後給捲動
				document.body.style.overflow = 'auto';
				
		    	break;
		    	
			default:
				//alert("nothing...");
				errorBlock(
						null, 
						null,
						["nothing..."], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
		}
		
	}

</script>

<div>
	<table>
		<tr>
			<td><spring:message code= "LB.Transaction_security_mechanism" /></td>
			<td>
				<label>
					<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked /><spring:message code= "LB.SSL_password" />
				</label>
				<input type="password" name="CMPASSWORD" id="CMPASSWORD" value="" size="8" maxlength="8" />
	
				<br />

				<label>
					<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />IKey
				</label>

				<br>
	
				<label>
					<input type="radio" name="FGTXWAY" id="CMCARD" value="2" /><spring:message code= "LB.Financial_debit_card" />
				</label>
	      </td>
	    </tr>
  </table>
</div>
<div>
	<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code= "LB.Confirm" />" onclick="processQuery()" />
</div>

























