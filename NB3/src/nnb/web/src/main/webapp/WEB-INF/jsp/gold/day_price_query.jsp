<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<meta http-equiv="Content-Language" content="zh-tw">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">   
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 查詢服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.use_component_P1_D1-1" /></li>
    <!-- 當日價格查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1464" /></li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  -->

		<section id="main-content" class="container">
			<!-- 當日黃金存摺價格查詢 -->
			<h2>
				<spring:message code="LB.W1464" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block"></div>
			<form id="formId" action="${__ctx}/download" method="post">
				<div class="main-content-block row">
					<div class="col-12">
						<input type="hidden" id="back" name="back" value="">

						<c:if
							test="${(gold_list.data.SIZE != '0')and (holiday_flag == 0)}">

							<div class="ttb-input-block">

								<!-- 系統時間區塊 -->
								<div class="ttb-input-item row">
									<!--系統時間  -->
									<span class="input-title"> <label> <spring:message
												code="LB.System_time" />：
									</label>
									</span> <span class="input-block">
										<div class="ttb-input">
											<span> ${system_time} </span>
										</div>
									</span>
								</div>


								<!-- 掛牌時間區塊 -->
								<div class="ttb-input-item row">
									<!--掛牌日期  -->
									<span class="input-title"> <label> <spring:message
												code="LB.W1465" />：
									</label>
									</span> <span class="input-block">
										<div class="ttb-input">
											<span> ${gold_list.data.REC.data[0].QDATE}

												${gold_list.data.REC.data[0].QTIME} </span>
										</div>
									</span>
								</div>

								<!--  TODO i18n -->
								<!-- 請注意：本表資料僅供參考，實際交易價格以交易確認時顯示之價格為準。 -->
								<div class="ttb-message">
									<span style="color: red"><spring:message
											code="LB.Day_Price_Query_P2_D1" /></span>
								</div>

								<div align="center">
									<!-- 單位 新台幣 元 -->
									<div align="right">
										<spring:message code="LB.W1466" />
										：
										<spring:message code="LB.NTD" />
										<spring:message code="LB.Dollar" />
									</div>
									<table class="stripe table table-striped ttb-table m-0"
										data-show-toggle="first" id="goldtable">
										<thead>
											<tr>
												<th colspan="2"><spring:message code='LB.W1467' /></th>

												<th rowspan="2">1<spring:message code='LB.W1435' /></th>
												<th rowspan="2">100<spring:message code='LB.W1435' /></th>
												<th rowspan="2">250<spring:message code='LB.W1435' /></th>
												<th rowspan="2">500<spring:message code='LB.W1435' /></th>
												<th rowspan="2">1<spring:message code='LB.W1469' /></th>
											</tr>
											<tr style="display:none">
												<th></th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
								<br> <br> <br>


							</div>

							<input type="button" class="ttb-button btn-flat-gray"
								id="printbtn" value="<spring:message code="LB.Print"/>" />
							<!-- 取得最新報價 -->
							<input type="button" class="ttb-button btn-flat-orange"
								id="getnewbtn" value="<spring:message code="LB.W1473"/>" />

						</c:if>
						<c:if
							test="${(holiday_flag == 0) and (gold_list.data.SIZE == '0')}">
							<div class="ttb-message">
								<!-- 查無資料 -->
								<p>
									<spring:message code="LB.Check_no_data" />
								</p>
							</div>
							<input type="button" class="ttb-button btn-flat-orange"
								id="getnewbtn" value="<spring:message code="LB.W1473" />" />

						</c:if>
						<c:if test="${holiday_flag == 1}">
							<p>
								<!-- 本日非營業日，無當日價格牌告，如欲查詢上一營業日黃金價格，請點選歷史黃金存摺價格查詢。 -->
								<spring:message code="LB.X1192" />
							</p>
							<input type="button" class="ttb-button btn-flat-orange"
								id="getnewbtn" value="<spring:message code= "LB.W1473" />" />
						</c:if>
					</div>
				</div>
			</form>
			<!-- 				</form> -->
		</section>
		<!-- 		main-content END --> </main>
	</div>
	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
	<script type="text/JavaScript">

	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		

		createTable();
	});
	
	function init(){
		// 初始化時隱藏span
		
		
		$("formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});	
		
        //列印
    	$("#printbtn").click(function(){
			var params = {
					"jspTemplateName":"day_price_query_print",
					//黃金存摺當日價格查詢
					"jspTitle":"<spring:message code="LB.W1464"/>",
					"SYSTIME":"${system_time}",
					"QDATE":"${gold_list.data.REC.data[0].QDATE}",
					"QTIME":"${gold_list.data.REC.data[0].QTIME}"
				};
				
			
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});
        
        
        
        
		$("#getnewbtn").click(function(e){
		
 				console.log("main submit");
 				$("#formId").validationEngine('detach');
 				initBlockUI();
 				$("#formId").attr("action","${__ctx}/GOLD/PASSBOOK/day_price_query");
 	  			$("#formId").submit(); 
 					
  		});
	}
	
	function createTable(){
		//test="${(gold_list.data.SIZE == '1')and (holiday_flag == 0)}">
		var data = ${gold_listJSON};
		var holiday_flag = "${holiday_flag}"
		if(data.data.SIZE==1 && holiday_flag=="0"){
			var dataSet = [
				["<spring:message code='LB.W1428'/>","<spring:message code='LB.W1470'/>",data.data.REC.data[0].BPRICE,"-","-","-","-"],
				[" ","<spring:message code='LB.W1471'/>",data.data.REC.data[0].SPRICE,"-","-","-","-"],
				["<spring:message code='LB.W1472'/>","X","-",data.data.REC.data[0].PRICE1,data.data.REC.data[0].PRICE2,data.data.REC.data[0].PRICE3,data.data.REC.data[0].PRICE4]
			];
			console.log(dataSet);
        
			
			//
			$('#goldtable').DataTable({
					data: dataSet,
					"bPaginate" : false,
					"bLengthChange": false,
					"scrollX": true,
					"sScrollX": "99%",
			        "scrollY": '80vh',
			        "bFilter": false,
			        "bDestroy": true,
			        "bSort": false,
			        "info": false,
			        "scrollCollapse": true,
					columnDefs: [{
						targets:"_all" ,
						createdCell: function(td, cellData, rowData, row, col) {				
							console.log("row:"+row+"col:"+col,rowData);
							if(row==0 && col==0){
								$(td).attr('rowspan', 2);
								$(td).addClass("text-center");
							}
							else if(row==1 && col==0){
								$(td).remove();
							}
							else if(row==2 && col==0){
								$(td).attr('colspan', 2);
								$(td).addClass("text-center");
							}
							else if(row==2 && col==1){
								$(td).remove();
							}
							else if(row==0 && col==1){
								$(td).addClass("text-center");
							}
							else if(row==1 && col==1){
								$(td).addClass("text-center");
							}
							else{
								$(td).addClass("text-right");
							}
						}
					}]
				});
			//
		}
	}
	
 	</script>
</body>
</html>
