<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

		function init() {		
			
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
			
			$("#CMSUBMIT").click(function (e) {
				
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}
				else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				$("#formId").attr("action","${__ctx}/OTHER/FEE/withholding_cancel_X_step1");
	 	  			$("#formId").submit(); 
	 			}
			});
		}
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 自動扣繳查詢/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0768" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.W0768" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<form id="formId" method="post" action="">

					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<!-- 扣款帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> <label>
											<h4>
												<spring:message code="LB.D0168" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<!--扣帳帳號 -->
											<select name="TSFACN" id="TSFACN" class="custom-select select-input half-input validate[required]">
												<c:forEach var="dataList" items="${withholding_cancel.data.REC}">
													<option>${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								<!-- 查詢代繳類別 -->
								<div class="ttb-input-item row">
									<span class="input-title"> <label>
											<h4>
												<spring:message code="LB.W0771" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="TYPE" id="TYPE" class="custom-select select-input half-input validate[required]">
								        		<option value="999"><spring:message code="LB.All" /></option>
								        		<option value="ELE"><spring:message code="LB.W0425" /></option>
								        		<option value="WAT"><spring:message code="LB.W0451" /></option>
								        		<option value="WAC"><spring:message code="LB.W0468" /></option>
								        		<option value="TLL"><spring:message code="LB.W0683" /></option>
									           	<option value="IHD"><spring:message code="LB.W0699" /></option>
									           	<option value="ILD"><spring:message code="LB.W0484" /></option>
									           	<option value="IKD"><spring:message code="LB.W0736" /></option>
									           	<option value="CKD"><spring:message code="LB.W0752" /></option>		
											</select>
										</div>
									</span>
								</div>								
								
							</div>
							<!-- 確定-->
							<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						</div>
					</div>
				</form>
			</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>