<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",100);
	//開始查詢資料並完成畫面
	setTimeout("init()",200);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#CMSUBMIT").click(function(e){
		e = e || window.event;
		if(!$('#formID').validationEngine('validate')){
    		e.preventDefault();
    	}
		else{
			
			if('${bond_sell_input.data.SELLWAY}'=='2'){

				var min = '${bond_sell_input.data.MINSPRICE}';
				var plus = '${bond_sell_input.data.PROGRESSIVESPRICE}';
				var min_fmt = '${bond_sell_input.data.MINSPRICE_FMT}';
				var plus_fmt = '${bond_sell_input.data.PROGRESSIVESPRICE_FMT}';
				
				min = min.replace(/,/g, "");
				plus = plus.replace(/,/g, "");
				
				if(!CheckAmountBond("I06",'<spring:message code="LB.X2532" />',min,plus,min_fmt,plus_fmt,'2')){
					return false;
				}
			}
			
			
			$("#formID").attr("action","${__ctx}/BOND/SELL/bond_sell_confirm");
			initBlockUI();//遮罩
			$("#formID").submit();	
		}
	
	});
	
	$("#CMCANCEL").click(function(){
		$("#formId").validationEngine('detach');
		initBlockUI();
		$("#formID").attr("action","${__ctx}/BOND/SELL/bond_sell_query");
		$("#formID").submit();
	});
});
function init(){
	$("#formID").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	
	$('#ALERTMSG').html('<spring:message code="LB.X2540" />'+"："+ '${bond_sell_input.data.BONDCRY_SHOW} '+'${bond_sell_input.data.MINSPRICE_FMT}' + "<br>"+'<spring:message code="LB.X2514" />'+"：" +'${bond_sell_input.data.BONDCRY_SHOW} '+ '${bond_sell_input.data.PROGRESSIVESPRICE_FMT}');
	
}
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
    <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
    <!-- 海外債券贖回/部分贖回     -->
    		<c:if test="${bond_sell_confirm.data.SELLWAY == '1'}">
<!--     		海外債券贖回 -->
    			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2520" /></li>
    		</c:if>
    		<c:if test="${bond_sell_confirm.data.SELLWAY == '2'}">
<!--     		海外債券部分贖回 -->
    			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2541" /></li>
    		</c:if>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<c:if test="${bond_sell_input.data.SELLWAY == '1'}">
<!-- 				海外債券贖回交易 -->
    				<h2><spring:message code="LB.X2566" /></h2>
    			</c:if>
    			<c:if test="${bond_sell_input.data.SELLWAY == '2'}">
    				<h2><spring:message code="LB.X2526" /></h2>
    			</c:if>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formID" method="post">
					<input type="hidden" id="TOKEN" name = "TOKEN" value="${sessionScope.transfer_confirm_token}">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 身分證字號/統一編號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.Id_no" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_sell_input.data.CUSIDN}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 客戶姓名 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1066" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_sell_input.data.hiddenNAME}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 信託帳號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0944" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_sell_input.data.O01} </span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 債券名稱 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1012" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_sell_input.data.O02}&nbsp;${bond_sell_input.data.O03}</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 投資幣別 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0908" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_sell_input.data.BONDCRY_SHOW}</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 委託賣價 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2531" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_sell_input.data.O08_FMT}%</span>
                                		</div>
                               		</span>
                            	</div>
                            	
                            	
                            	<!-- 全贖區塊 -->
                            	<c:if test="${bond_sell_input.data.SELLWAY == '1'}">
                            	<!-- 委賣面額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2532" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_sell_input.data.O07_FMT} <spring:message code="LB.Dollar" /></span> 
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 贖回方式 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1140" /></h4>
                                    </label></span>
                                	<span class="input-block">
<!--                                 		全部 -->
                                		<div class="ttb-input">
                                			<spring:message code="LB.All" />
                                		</div>
                               		</span>
                            	</div>
                            	</c:if>
                            	
                            	<!-- 部贖區塊 -->
                            	<c:if test="${bond_sell_input.data.SELLWAY == '2'}">
                            	<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2521" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_sell_input.data.O07_FMT} <spring:message code="LB.Dollar" /></span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 委賣面額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2532" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		
                                		<div class="ttb-input">
                                			<span>${bond_sell_input.data.BONDCRY_SHOW}</span>
                                			<input type="text" id="I06" name="I06" class="text-input" size="11" maxlength="11" value="" placeholder="" autocomplete="off"> 
                                		</div>
                                		<span id="ALERTMSG"></span>
                               		</span>
                            	</div>
                            	<!-- 贖回方式 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1140" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
<!--                                 			部分贖回 -->
											<spring:message code="LB.X2525" />
                                		</div>
                               		</span>
                            	</div>
                            	</c:if>
                            	
<!--                             	信託本金 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2535" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>
                                				${bond_sell_input.data.O06_FMT} <spring:message code="LB.Dollar" />
                                			</span> 
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 入帳帳號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0135" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
<%--                                 			${bond_sell_input.data.O11} --%>
                                			<span>${bond_sell_input.data.ACN4}</span> 
                                		</div>
                               		</span>
                            	</div>
                            </div>
							<input type="button" id="CMCANCEL" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>	
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>	
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>