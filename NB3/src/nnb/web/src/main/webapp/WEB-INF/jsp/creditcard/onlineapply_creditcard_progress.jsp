<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            init();
        });

        function init() {
	    	$("#CMSUBMIT").click(function(e){
	        	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
				$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					$("#CPRIMBIRTHDAY").val($("#YY").val()+""+$("#MM").val()+""+$("#DD").val());
		        	var action = '${__ctx}/CREDIT/APPLY/onlineapply_creditcard_progress_p1';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});
	    	birthday();
        }
        function birthday(){
        	var Today = new Date();
        	var y = Today.getFullYear();
        	var m = (Today.getMonth()+1<10 ? '0' : '')+(Today.getMonth()+1);
        	var d = (Today.getDate()<10 ? '0' : '')+Today.getDate();
        	y = y - 1911;
        	var min_y = y-20;
        	var max_y = y-100;
        	for(var i=min_y;i>=max_y;i--){
        		var YYtext = i;
        		if(i.toString().length < 2){
        			YYtext = "00"+YYtext;
        		}
        		else if(i.toString().length < 3){
        			YYtext = "0"+YYtext;
        		}
        		//民國年
//         		$("#YY").append( $("<option></option>").attr("value", YYtext).text("<spring:message code='LB.D0583' />" + i + "<spring:message code='LB.Year' />"));
        		//西元年
        		if('${transfer}' == 'en'){
        			$("#YY").append( $("<option></option>").attr("value", YYtext).text( i + 1911));
        		}
        		else{
        			$("#YY").append( $("<option></option>").attr("value", YYtext).text((i + 1911) + "年"));
        		}
        	}
        	orderby();
//         	$("#YY").val(y);
        	for(var i=1;i<=12;i++){
        		var j = i;
        		if(j<10){
        			j = "0"+j;
        		}
        		//月
        		if('${transfer}' == 'en'){
        			switch (j) {
        			case '01':
        			　$("#MM").append( $("<option></option>").attr("value", j).text("January"));
        			　break;
        			case '02':
        			　$("#MM").append( $("<option></option>").attr("value", j).text("February"));
        			　break;
        			case '03':
        			　$("#MM").append( $("<option></option>").attr("value", j).text("March"));
        			　break;
        			case '04':
        			　$("#MM").append( $("<option></option>").attr("value", j).text("April"));
        			　break;
        			case '05':
        			　$("#MM").append( $("<option></option>").attr("value", j).text("May"));
        			　break;
        			case '06':
        			　$("#MM").append( $("<option></option>").attr("value", j).text("June"));
        			　break;
        			case '07':
        			　$("#MM").append( $("<option></option>").attr("value", j).text("July"));
        			　break;
        			case '08':
        			　$("#MM").append( $("<option></option>").attr("value", j).text("August"));
        			　break;
        			case '09':
        			　$("#MM").append( $("<option></option>").attr("value", j).text("September"));
        			　break;
        			case 10:
        			　$("#MM").append( $("<option></option>").attr("value", j).text("October"));
        			　break;
        			case 11:
        			　$("#MM").append( $("<option></option>").attr("value", j).text("November"));
        			　break;
        			case 12:
        			　$("#MM").append( $("<option></option>").attr("value", j).text("December"));
        			　break;
        			default:
        			　x="沒有符合的條件";
        			}
        		}
        		else{
        			$("#MM").append( $("<option></option>").attr("value", j).text(j + "<spring:message code='LB.Month' />"));
        		}
        	}
//         	$("#MM").val(m);
        	for(var i=1;i<=31;i++){
        		var j = i;
        		if(j<10){
        			j = "0"+j;
        		}
        		//日
        		if('${transfer}' == 'en'){
        			$("#DD").append( $("<option></option>").attr("value", j).text(j));
        		}
        		else{
        			$("#DD").append( $("<option></option>").attr("value", j).text(j + "<spring:message code='LB.D0586' />"));
        		}
        	}
//         	$("#DD").val(d);
        }
        function orderby(){
        	$('#YY>option').sort(function(a,b){
                //按option中的值排序
                var aText = $(a).val()*1;
                var bText = $(b).val()*1;
                if(aText > bText) return -1;
                if(aText < bText) return 1;
                return 0;
            }).appendTo('#YY');
            $('#YY>option').eq(0).attr("selected","selected");
        }
        function changeday(){
        	var year = ($("#YY").val()+1911);
        	var month = $("#MM").val();
        	console.log(year+"/"+month);
        	//alert(year+"/"+month);
        	errorBlock(
        			null, 
        			null,
        			[year+"/"+month], 
        			'<spring:message code= "LB.Quit" />', 
        			null
        		);
        }        

    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--信用卡 -->
			<li class="ttb-breadcrumb-item"><spring:message code= "LB.Credit_Card" /></a></li>
			<!--信用卡補上傳資料 -->
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.onlineapply_creditcard_progress_TITLE" /></a></li>
		</ol>
	</nav>
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2><spring:message code= "LB.onlineapply_creditcard_progress_TITLE" /></h2>
				<div id="step-bar">
                    <ul>
                        <li class="active"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
                        <li class=""><spring:message code="LB.onlineapply_creditcard_progress_OTP" /></li><!-- OTP驗證 -->
                        <li class=""><spring:message code="LB.D0359" /></li><!-- 查詢結果 -->
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="ADOPID" value="NA03"/>
				  	<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
				  	<input type="hidden" name="HTRANSPIN" id="HTRANSPIN" value="">
					<input type="hidden" name="LMTTYN" id="LMTTYN" value="">	
					<input type="hidden" id="CPRIMBIRTHDAY" name="CPRIMBIRTHDAY"/>
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
							<div class="ttb-message">
                                <p><spring:message code="LB.D0851" /></p>
                            </div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0025" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" name="CUSIDN"  id="CUSIDN"  value="" class="text-input validate[required,funcCall[validate_checkSYS_IDNO[CUSIDN]]]" maxlength="10" size="10" placeholder="<spring:message code="LB.D0025" />">
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
<!-- 											出生日期 -->
											<spring:message code="LB.D0582" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select id="YY" name="YY" class="custom-select select-input input-width-100">
												<option value="#" style="">---</option>
											</select>
											<select id="MM" name="MM" class="custom-select select-input input-width-60">
												<option value="#">---</option>
											</select>
											<select id="DD" name="DD" class="custom-select select-input input-width-60">
												<option value="#">---</option>
											</select>
										</div>
										<input type="text" id="checkDD" class="validate[funcCallRequired[validate_CheckSelects['<spring:message code="LB.D0582" />',YY,MM,DD,#]]]" 
										 style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;"/>
										<input type="hidden" id="CPRIMBIRTHDAY" name="CPRIMBIRTHDAY"/>
										<input type="hidden" id="CPRIMBIRTHDAYshow" name="CPRIMBIRTHDAYshow"/>
									</span>
								</div>
                           	</div>  
	                       	<input type="button" id="CMBACK" value="<spring:message code="LB.D0171"/>" class="ttb-button btn-flat-gray" onClick="window.close();"/>
	                        <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code= "LB.X0080" />" />
                        </div>
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>