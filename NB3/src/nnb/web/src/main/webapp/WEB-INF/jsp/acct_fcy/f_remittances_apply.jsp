<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			// 初始化時隱藏span
			$("#hideblock").hide();
		});

		function init() {
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });

			//判斷是否需Checked
			var str_Flag = '${f_remittances_apply.data.str_Flag }';
			// 		if('Y'==str_Flag){
			// 			fstop.setRadioChecked("TXTYPE", "D", true);
			// 		}else{
			// 			fstop.setRadioChecked("TXTYPE", "A", true);
			// 		}

			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				}
				else {
					$("#formId").validationEngine('detach');
					initBlockUI();
					$("#formId").submit();
				}
			});
		}

	</script>

</head>

<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 匯入匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0125" /></li>
    <!-- 線上解款申請/註銷     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0318" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--  外匯匯入匯款線上解款申請/註銷-->
					<spring:message code="LB.W0318" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form name="formId" id="formId" method="post"
					action="${__ctx}/FCY/ACCT/ONLINE/f_remittances_apply_confirm">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 申請項目 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.D0169" />
										</label>
									</span>
									<span class="input-block">
										<!-- 申請匯入匯款線上解款 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.W0320" />
												<c:if test="${ !f_remittances_apply.data.str_Flag.equals('Y') }">
													<input type="radio" name="TXTYPE" value="A" Checked>
												</c:if>
												<c:if test="${ f_remittances_apply.data.str_Flag.equals('Y') }">
													<input type="radio" name="TXTYPE" value="A" disabled>
												</c:if>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--取消匯入匯款線上解款 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.W0321" />
												<c:if test="${ !f_remittances_apply.data.str_Flag.equals('Y') }">
													<input type="radio" name="TXTYPE" value="D" disabled>
												</c:if>
												<c:if test="${ f_remittances_apply.data.str_Flag.equals('Y') }">
													<input type="radio" name="TXTYPE" value="D" Checked>
												</c:if>
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<div class="ttb-message">
									<span>
										(
										<!--您 -->
										<spring:message code="LB.X0046" />
										<!-- 判斷式 -->
						                 <c:choose>
						                    <c:when test="${f_remittances_apply.data.str_Flag.equals('Y') }">
						                            <!--已申請 -->
						                         <spring:message code="LB.D0277" />
						                    </c:when>
						                    <c:when test="${f_remittances_apply.data.str_Flag.equals('N') }">
						                            <!-- 已取消 -->
						                         <spring:message code="LB.X1833" />
						                    </c:when>
						                    <c:otherwise>
						                            <!-- 尚未申請 -->
						                        <spring:message code="LB.D0276" />
						                    </c:otherwise>
						                 </c:choose>
										<!--匯入匯款線上解款 -->
										<spring:message code="LB.X0045" />
										)
									</span>
								</div>
							</div>
							<!--button 區域 -->
								<!-- 確定 -->
								<spring:message code="LB.Confirm" var="cmSubmit"/>
								<input class="ttb-button btn-flat-orange" type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" >
							<!--button 區域 -->
						</div>
					</div>
					<div class="text-left">
						<!-- 						說明： -->
						<ol class="description-list list-decimal">
							<p>
								<spring:message code="LB.Description_of_page" />
							</p>
							<li>
								<spring:message code="LB.F004_P1_D1-1" />
								<a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank">
									<spring:message code="LB.F004_P1_D1-2" />
								</a>
								<a href="${__ctx}/public/transferFeeRate.htm" target="_blank">
									<spring:message code="LB.F004_P1_D1-3" />
								</a>
								<a href="https://www.tbb.com.tw/exchange_rate" target="_blank">
									<spring:message code="LB.F004_P1_D1-4" />
								</a>
							</li>
							<li>
									<spring:message code="LB.F_Remittances_Apply_P1_D2" />
							</li>
							<li>
								<font color=red>
									<spring:message code="LB.F004_P1_D3-1" />
								</font>
									<spring:message code="LB.F004_P1_D3-2" />
								<font color=red>
									<spring:message code="LB.F004_P1_D3-3" />
								</font>
									<spring:message code="LB.F004_P1_D3-4" />
							</li>
						</ol>
					</div>
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
		<!-- 	content row END -->
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>