<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript">
		$(document).ready(function () {
			
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

		function init() {
			initDataTable();
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		}

		// 點按鈕click
		function relieve(index) {
			// 塞資料
			$("#helperjson").val($("#" + index).val());
			//
			if (!$('#formId').validationEngine('validate')) {
				e = e || window.event; // for IE
				e.preventDefault();
			} else {
				$("#formId").validationEngine('detach');
				initBlockUI();//遮罩
				$("#formId").submit();
			}
		}
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 停損停利通知設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0414" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12">
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0414" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/FUND/ALTER/stop_loss_notice_step1">
					<c:set var="BaseResultData" value="${stop_loss_notice.data}"></c:set>
					<input type="hidden" id="helperjson" name="helperjson" value=''>
					<input type="hidden" id="CUSIDN" name="CUSIDN" value='${BaseResultData.CUSIDN}'>
					<input type="hidden" id="hideid_CUSIDN" name="hideid_CUSIDN" value='${BaseResultData.hideid_CUSIDN}'>
					<input type="hidden" id="hideid_NAME" name="hideid_CUSIDN" value='${BaseResultData.hideid_NAME}'>
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<ul class="ttb-result-list">
								<!-- 資料總數 -->
								<li>
									<h3>
										<spring:message code="LB.Total_records" />：
									</h3>
									<p>
										${BaseResultData.COUNT}
										<spring:message code="LB.Rows" />
									</p>
								</li>
								<!-- 變更種類 -->
								<li>
									<h3>
										<spring:message code="LB.W1167" />
									</h3>
									<p>
										<spring:message code="LB.W1166" />
									</p>
								</li>
								<!-- 身份證字號/統一編號 -->
								<li>
									<h3>
										<spring:message code="LB.Id_no" />
									</h3>
									<p>
										${BaseResultData.hideid_CUSIDN}
									</p>
								</li>
								<!--姓名 -->
								<li>
									<h3>
										<spring:message code="LB.Name" />
									</h3>
									<p>
										${BaseResultData.hideid_NAME}
									</p>
								</li>
							</ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
									<!-- 變更種類 -->
<%-- 									<tr> --%>
<!-- 										變更種類 -->
<%-- 										<th> --%>
<%-- 											<spring:message code="LB.W1167" /> --%>
<%-- 										</th> --%>
<!-- 										變更種類 -->
<%-- 										<th class="text-left" colspan="7"> --%>
<%-- 											<spring:message code="LB.W1166" /> --%>
<%-- 										</th> --%>
<%-- 									</tr> --%>
<!-- 									身份證字號/統一編號 -->
<%-- 									<tr> --%>
<%-- 										<th> --%>
<%-- 											<spring:message code="LB.Id_no" /> --%>
<%-- 										</th> --%>
<%-- 										<th class="text-left" colspan="7"> --%>
<%-- 											${BaseResultData.hideid_CUSIDN} --%>
<%-- 										</th> --%>
<%-- 									<tr> --%>
<!-- 										姓名 -->
<%-- 									<tr> --%>
<%-- 										<th> --%>
<%-- 											<spring:message code="LB.Name" /> --%>
<%-- 										</th> --%>
<%-- 										<th class="text-left" colspan="7"> --%>
<%-- 											${BaseResultData.hideid_NAME} --%>
<%-- 										</th> --%>
<%-- 									<tr> --%>
									<tr>
										<!--信託帳號 -->
										<th nowrap><spring:message code="LB.W0944" /></th>
										<!-- 扣款標的 -->
										<th nowrap><spring:message code="LB.W1041" /></th>
										<!-- 類別 -->
										<th nowrap><spring:message code="LB.D0973" /></th>
										<!-- 信託金額 -->
										<th nowrap><spring:message code="LB.W0026" /></th>
										<!-- 每次定額申購金額＋<br>不定額基準申購金額</th> -->
										<th><spring:message code="LB.W1043" />＋<br><spring:message code="LB.W1044" /></th>
										<!-- 停利點 -->
										<th nowrap><spring:message code="LB.W1174" /></th>
										<!-- 停損點 -->
										<th nowrap><spring:message code="LB.W1175" /></th>
										<!-- 停損停利通知設定/變更 -->
										<th nowrap><spring:message code="LB.W1176" /></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach varStatus="loop" var="dataList" items="${ BaseResultData.REC }">
										<tr>
										<input type="hidden" id="helperjson_${loop.index}"
											value='${dataList.helperjson}'>
											<!--信託帳號 -->
											<td align="center">${dataList.hideid_CDNO }</td>
											<!-- 扣款標的 -->
											<td align="center">${dataList.FUNDLNAME }</td>
											<!-- 類別 -->
											<td align="center">${dataList.str_AC202 }</td>
											<!-- 信託金額 -->
											<td align="center">
												<c:if test="${dataList.FUNDAMT != '0.00'}">
													${dataList.display_FUNDCUR }
													<br>
													${dataList.display_FUNDAMT }
												</c:if>
											</td>
											<!-- 每次定額申購金額＋<br>不定額基準申購金額 -->
											<td align="center">
												<c:if test="${dataList.PAYAMT != '0'}">
													${dataList.display_PAYCUR }
													<br>
													${dataList.display_PAYAMT }
												</c:if>
											</td>
											<!-- 停利點 -->
											<td align="center">${dataList.display_STOPPROF }</td>
											<!-- 停損點 -->
											<td align="center">${dataList.display_STOPLOSS } </td>
											<!-- 停損停利通知設定/變更 -->
											<!--執行選項 -->
											<td align="center">
												<input class="ttb-sm-btn btn-flat-orange" type="button" value="<spring:message code= "LB.Confirm" />"
													name="CMSUBMIT" name="CMSUBMIT"
													onclick="relieve('helperjson_${loop.index}')" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="text-left">
						<ol class="description-list list-decimal">
						<!-- 						說明： -->
						<p>
							<spring:message code="LB.Description_of_page" />
						</p>
							<li>
								<span>
									<strong style="FONT-WEIGHT: 400"><spring:message code="LB.Stop_Loss_Notice_P1_D1" /></strong>
								</span>
							</li>
							<li>
								<span>
									<strong style="FONT-WEIGHT: 400"><spring:message code="LB.Stop_Loss_Notice_P1_D2" /></strong>
								</span>
							</li>
						</ol>
					</div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>

</body>

</html>