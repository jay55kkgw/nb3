<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<label><spring:message code="LB.Inquiry_time" /> ： </label>
	<label>${CMQTIME}</label>
	<br />
	<br />
	<label><spring:message code="LB.Total_records" /> ：</label>
	<label>${COUNT} <spring:message code="LB.Rows" /></label>
	<br />
	<br />
	<br />
	<table class="print">
		<thead>
			<tr >
				<!--帳號 -->
				<th style="text-align:center"><spring:message code="LB.Loan_account" /></th>
				<!--可用餘額(公克) -->
				<th style="text-align:center"><spring:message code="LB.W1433" />(<spring:message code="LB.W1435" />)</th>
				<!--帳戶餘額(公克) -->
				<th style="text-align:center"><spring:message code="LB.W1434" />(<spring:message code="LB.W1435" />)</th>
				<!--參考市值-->
				<th style="text-align:center"><spring:message code="LB.W1436" /></th>
				<!--每公克原始投入參考平均成本-->
				<th style="text-align:center"><spring:message code="LB.W1437" /></th>
			</tr>
		</thead>
		<!--列表區 -->
		<tbody>
			<c:forEach var="dataList" items="${dataListMap}" varStatus="loop">
				<tr>
					<td class="text-center">${dataList.ACN}</td>
					<td class="text-right">${dataList.TSFBAL}</td>
					<td class="text-right">${dataList.GDBAL}</td>
					<td class="text-right">${dataList.MKBAL}</td>
					<td class="text-right">${dataList.AVGCOST}</td>
				</tr>
			</c:forEach>
			<tr>
			<!-- 合計 -->
				<td class="text-center"><spring:message code= "LB.X0923" /></td>
				<td class="text-right">${TOTAL_TSFBAL}</td>
				<td class="text-right">${TOTAL_GDBAL}</td>
				<td class="text-right">${TOTAL_MKBAL}</td>
				<td></td>
			</tr>
		</tbody>
	</table>
	<div class="text-left">
		<!-- 備註:每公克原始投入參考平均成本計算如說明，且不含投資手續費成本，本項目僅供投資人參考。 -->
							<spring:message code="LB.W1440"/><a href="${__ctx}/GOLD/PASSBOOK/gold_balance_query_explan" target=_blank><spring:message code="LB.W1441"/></a></div>
</body>
</html>