<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript">
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item">信用卡</li>
			<li class="ttb-breadcrumb-item">繳費區</li>
			<li class="ttb-breadcrumb-item">臺灣自來水水費</li>
		</ol>
	</nav>
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					信用卡繳臺灣自來水水費結果頁
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											交易時間
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.CMQTIME}
										</div>
									</span>
								</div>
							</div>
							
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											交易狀態
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											成功
										</div>
									</span>
								</div>
							</div>
							
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											代收期限
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.D0583"/>&nbsp;${input_data.CMDATE1.substring(0,3)}<spring:message code="LB.Year"/>&nbsp;${input_data.CMDATE1.substring(3,5)}<spring:message code="LB.Month"/>&nbsp;${input_data.CMDATE1.substring(5,7)}<spring:message code="LB.D0586"/>&nbsp;
										</div>
									</span>
								</div>
							</div>
							
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											銷帳編號
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.WAT_NO}
										</div>
									</span>
								</div>
							</div>
							
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											查核碼
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.CHKCOD}
										</div>
									</span>
								</div>
							</div>
							
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											應繳款金額
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.AMOUNT}
										</div>
									</span>
								</div>
							</div>
							
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											信用卡卡號
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.CARDNUM}
										</div>
									</span>
								</div>
							</div>
							
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											卡片效期
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.EXPDTA.substring(0,2)}<spring:message code="LB.Year"/>&nbsp;${result_data.data.EXPDTA.substring(2)}<spring:message code="LB.Month"/>
										</div>
									</span>
								</div>
							</div>
							
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											信用卡背面末三碼
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.CHECKNO}
										</div>
									</span>
								</div>
							</div>
							
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											手續費
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.FEE}
										</div>
									</span>
								</div>
							</div>
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>