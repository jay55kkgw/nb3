<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/dataTables.rowsGroup.js"></script>
<style type="text/css">
.table-1 {
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
    margin: 0 auto;
    border:
}

.table-1>thead>tr>th,
.table-1>thead>tr>td {
    background-color: #EEEEEE;
    border-top: none;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    font-weight: bold;
}

.table-1>tbody>tr>td,
.table-1>thead>tr>th {
    border: 1px solid #DDD;
    padding: 8px;
    text-align: left;
}

.table-1 tbody tr:first-child td:first-child {
    text-align: center;
}

.table-1 td:hover {
    background-color: #fbf8e9;
}
</style>
</head>

<body>
    <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 定期定額交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1552" /></li>
    <!-- 定期定額查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1586" /></li>
		</ol>
	</nav>

		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		<main class="col-12">	
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<!-- 定期定額查詢 -->
				<h2><spring:message code="LB.W1586"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
							<li>
								<!-- 黃金存摺帳號 -->
								<h3>
								<spring:message code="LB.D1090"/> ：
								</h3>
								<p>								
								${averaging_query_result.data.ACN }
								</p>
							</li>
							<li>
								<!-- 查詢期間 -->
								<h3>
								<spring:message code="LB.Inquiry_period" /> ：			
								</h3>
								<p>	
								<c:choose>
								<c:when test="${averaging_query_result.data.CMPERIOD == '最後一次異動' }">				
								<spring:message code="LB.W1597"/>
								</c:when>
								<c:otherwise>
								${averaging_query_result.data.CMPERIOD}
								  </c:otherwise>
								  </c:choose>
								
								</p>
							</li>
							<li>
							<h3>
								<!-- 資料總數 -->
								<spring:message code="LB.Total_records" /> ：
							</h3>
							<p>								
								${averaging_query_result.data.__OCCURS }
								<!--筆 -->
								<spring:message code="LB.Rows" />
							</p>
							</li>							
						</ul>		
						<c:forEach var="dataList" items="${averaging_query_result.data.REC}">		
					<table class="stripe table table-striped ttb-table dtable m-0" data-show-toggle="first">
						<thead >
							<tr >
								<!--異動時間 -->
								<th  data-title="<spring:message code="LB.W1588"/>" rowspan="2" style="display: table-cell; vertical-align: middle;"><spring:message code="LB.W1588"/></th>
								<!--異動來源 -->							
								<th  data-title="<spring:message code="LB.W1589"/>" rowspan="2" data-breakpoints="xs sm" style="display: table-cell; vertical-align: middle;"><spring:message code="LB.W1589"/></th>
								<!--扣款帳號(限臨櫃變更) -->
								<th data-title="<spring:message code="LB.W1590"/>" data-breakpoints="xs sm" colspan="2"><spring:message code="LB.W1590"/></th>
								<!--扣款手續費-->
								<th data-title="<spring:message code="LB.W1591"/>" data-breakpoints="xs sm"colspan="2" ><spring:message code="LB.W1591"/></th>
								<!--每月扣款日-->
								<th  data-title="<spring:message code="LB.W1592"/>" rowspan="2" data-breakpoints="xs sm" style="display: table-cell; vertical-align: middle;"><spring:message code="LB.W1592"/></th>
								<!--扣款金額 -->
								<th  data-title="<spring:message code="LB.W1079"/>" colspan="2"><spring:message code="LB.W1079"/></th>
								<!--投資狀態 -->
								<th data-title="<spring:message code="LB.W1594"/>" data-breakpoints="xs sm" colspan="2"><spring:message code="LB.W1594"/></th>
							</tr>
							<tr>
								<!--異動前 -->
								<th data-title="<spring:message code="LB.W1595"/>" data-breakpoints="xs sm"><spring:message code="LB.W1595"/></th>
								<!--異動後 -->
								<th data-title="<spring:message code="LB.W1596"/>" data-breakpoints="xs sm"><spring:message code="LB.W1596"/></th>
								<!--異動前 -->
								<th data-title="<spring:message code="LB.W1595"/>" data-breakpoints="xs sm"><spring:message code="LB.W1595"/></th>
								<!--異動後 -->
								<th data-title="<spring:message code="LB.W1596"/>" data-breakpoints="xs sm"><spring:message code="LB.W1596"/></th>
								<!--異動前 -->
								<th data-title="<spring:message code="LB.W1595"/>" ><spring:message code="LB.W1595"/></th>
								<!--異動後 -->
								<th data-title="<spring:message code="LB.W1596"/>"><spring:message code="LB.W1596"/></th>
								<!--異動前 -->
								<th data-title="<spring:message code="LB.W1595"/>" data-breakpoints="xs sm"><spring:message code="LB.W1595"/></th>
								<!--異動後 -->
								<th data-title="<spring:message code="LB.W1596"/>" data-breakpoints="xs sm"><spring:message code="LB.W1596"/></th>
							</tr>
						</thead>
					<!--          列表區 -->
						<tbody>
							<c:if test="${not empty dataList}">
							<tr>            	
								<td class="text-center">${dataList.LTD}<br>${dataList.TIME}</td>
								<td class="text-center">
								<c:if test="${not empty dataList.SOURCE}">
								${dataList.SOURCE}
								</c:if>
								</td> 
								<td class="text-center">${dataList.BTSFACN}</td>  
								<td class="text-center">${dataList.ATSFACN}</td>  
								<td class="text-center">${dataList.BFEEAMT}</td>  
								<td class="text-center">${dataList.AFEEAMT}</td>  
								<td  class="text-center">06</td>
								<td  class="text-right">${dataList.BQTAMT1}</td>
								<td  class="text-right">${dataList.AQTAMT1}</td>
								<td  class="text-center">
								<c:if test="${not empty dataList.BSTATUS1}">
								${dataList.BSTATUS1}
								</c:if>
								</td>
								<td  class="text-center">
								<c:if test="${not empty dataList.ASTATUS1}">
								${dataList.ASTATUS1}
								</c:if>
								</td>            
							</tr>
							<tr>
							<td>${dataList.LTD}<br>${dataList.TIME}</td>
								<td>
								<c:if test="${not empty dataList.SOURCE}">
								${dataList.SOURCE}
								</c:if>
								</td> 
								<td>${dataList.BTSFACN}</td>  
								<td>${dataList.ATSFACN}</td>  
								<td>${dataList.BFEEAMT}</td>  
								<td>${dataList.AFEEAMT}</td>  
								<td class="text-center" style="vertical-align:middle;">16</td>
								<td style="vertical-align:middle;"class="text-right">${dataList.BQTAMT2}</td>
								<td style="vertical-align:middle;"class="text-right">${dataList.AQTAMT2}</td>
								
								<td style="vertical-align:middle;"class="text-center">
								<c:if test="${not empty dataList.BSTATUS2}">
								${dataList.BSTATUS2}
								</c:if>
								</td>   
								<td style="vertical-align:middle;"class="text-center">
								<c:if test="${not empty dataList.ASTATUS2}">
								${dataList.ASTATUS2}
								</c:if>
								</td>   
							</tr>
							<tr>
							<td>${dataList.LTD}<br>${dataList.TIME}</td>
								<td>
								<c:if test="${not empty dataList.SOURCE}">
								${dataList.SOURCE}
								</c:if>
								</td> 
								<td>${dataList.BTSFACN}</td>  
								<td>${dataList.ATSFACN}</td>  
								<td>${dataList.BFEEAMT}</td>  
								<td>${dataList.AFEEAMT}</td>  
								<td class="text-center" >26</td>
								<td class="text-right">${dataList.BQTAMT3}</td>
								<td class="text-right">${dataList.AQTAMT3}</td>
								<td class="text-center">
								<c:if test="${not empty dataList.BSTATUS3}">
								${dataList.BSTATUS3}
								</c:if>
								</td>   
								<td class="text-center">
								<c:if test="${not empty dataList.ASTATUS3}">
								${dataList.ASTATUS3}
								</c:if>
								</td>    
							</tr>
							</c:if>
							<c:if test="${empty dataList}">
							<tr class="footable-group-row">            	
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>              
							</tr>
							</c:if>				
						</tbody>
						</table>
						<br class="bigtabledata">
						</c:forEach>
						<input id="previous" name="previous" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						<input type="button" class="ttb-button btn-flat-orange" id = "printbtn" value="<spring:message code="LB.Print" />"/>	
					</div>
				</div>
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->


	<%@ include file="../index/footer.jsp"%>
	
		<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",10);
				
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);
				
				jQuery(function($) {
					$('.dtable').DataTable({
						scrollX: true,
						sScrollX: "99%",
						scrollY: '80vh',
						bPaginate: false,
						bFilter: false,
						bDestroy: true,
						bSort: false,
						info: false,
						rowsGroup: [0, 1, 2,3,4,5,6],
						scrollCollapse: true,
						fixedColumns: {
							leftColumns: 1
						},
					});
				});
				
			});

			function init(){
				
				//fixtable();
				//上一頁按鈕
				$("#previous").click(function() {
					initBlockUI();
					fstop.getPage('${pageContext.request.contextPath}'+'/GOLD/AVERAGING/averaging_query','', '');
				});

				$("#printbtn").click(function(){
					var params = {
							"jspTemplateName":"averaging_query_result_print",
							//定期定額查詢
							"jspTitle":"<spring:message code="LB.W1586"/>",
							"CMPERIOD":"${averaging_query_result.data.CMPERIOD}",
							"ACN":"${averaging_query_result.data.ACN}",
							"COUNT":"${averaging_query_result.data.__OCCURS }"
						};
						openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});	
			}
			
		</script>
</body>
</html>