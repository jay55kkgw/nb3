<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/>
<c:if test="${RESULT == true}">
<div style="text-align:center">
	<font style="font-weight:bold;font-size:1.2em">
		<spring:message code="LB.W0284" />
	</font>
</div>
</c:if>
<br/>
<table class="print">
	<!-- 資料時間 -->
	<tr>
		<td style="text-align:center"><spring:message code= "LB.Data_time" /></td>
		<td>${CMTXTIME}</td>
	</tr>
	<!-- 轉帳日期 -->
	<tr>
		<td style="text-align:center"><spring:message code= "LB.Transfer_date" /></td>
		<td>${PAYDATE}</td>
	</tr>
	<!-- 轉出帳號 -->
	<tr>
		<td style="text-align:center"><spring:message code= "LB.Payers_account_no" /></td>
		<td>${CUSTACC}</td>
	</tr>
	<!--  轉入帳號 -->
	<tr>
		<td style="text-align:center"><spring:message code= "LB.Payees_account_no" /></td>
		<td>${BENACC}</td>
	</tr>
	<!-- 轉出金額 -->
<c:if test="${PAYREMIT eq '1' }">
	<tr>
		<td style="text-align:center"><spring:message code= "LB.Deducted" /></td>
		<td>${display_PAYCCY }&nbsp;${CURAMT }&nbsp;<spring:message code="LB.Dollar" /></td>
	</tr>
</c:if>
	<!--轉入金額 -->
<c:if test="${PAYREMIT eq '2' }">
	<tr>
		<td style="text-align:center"><spring:message code= "LB.Buy" /></td>
		<td>${display_REMITCY}&nbsp;${CURAMT}&nbsp;<spring:message code="LB.Dollar" /></td>
	</tr>
</c:if>
	<!--收款人附言 -->
<c:if test="${SELFFLAG eq 'Y' }">
	<tr>
		<td style="text-align:center"><spring:message code= "LB.W0280" /></td>
		<td>${MEMO1}</td>
	</tr>
</c:if>
	<!--交易備註 -->
	<tr>
		<td style="text-align:center"><spring:message code= "LB.Transfer_note" /></td>
		<td>${CMTRMEMO}</td>
	</tr>
</table>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D1" /></strong></li>
			<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D2" /></strong></li>
			<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D3" /></strong></li>
			<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D4" /></strong></li>
			<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D5" /></strong></li>
			<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D6" /></strong></li>
			<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D7" /></strong></li>			
	        <li><spring:message code="LB.F_Transfer_P6_D8" /></li>
		</ol>
	</div>
</body>
</html>