<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<div style="text-align:center"><spring:message code="LB.Transaction_successful"/></div>
<br/><br/><br/>
<table class="print">
	<tr>
		<th class="text-center"><spring:message code="LB.Trading_time"/></th>
		<th>${CMQTIME}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W1060" /></th>
		<th>${FDTXTYPE}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W0944" /></th>
		<th>${CDNO}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W0025" /></th>
		<th>(${TRANSCODE})&nbsp;${FUNDLNAME}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.X0377" /></th>
		<th>${TRADEDATE_1}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W1140" /></th>
		<th>
			<c:if test="${BILLSENDMODE_1 == '1'}">
				<spring:message code="LB.All"/>
			</c:if>
			<c:if test="${BILLSENDMODE_1 == '2'}">
				<spring:message code="LB.X1852"/>
			</c:if>
		</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W0979" /></th>
		<th>${UNIT_1}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W0135" /></th>
		<th>${FUNDACN}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W1080" /></th>
		<th>${TRADEDATE_1}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W0978" /></th>
		<th>${ADCCYNAME}&nbsp;${FUNDAMT_1}</th>
	</tr>
</table>
<br/><br/>
</body>
</html>