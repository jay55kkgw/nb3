<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//開始查詢資料並完成畫面
	setTimeout("init()",200);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	var mail = '${sessionScope.dpmyemail}';
// 	 	mail='';
	
	if( ''== mail){
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X2637' />"], 
				'<spring:message code= "LB.Confirm" />', 
				null
			);
		$("#errorBtn1").click(function(e) {
			$('#error-block').hide();
		});
	}
	
	
	if('${bond_purchase_input.data.mailSign}' != 'Y'){
		shwd_prompt_init(true);
	}
	
	$("#CMSUBMIT").click(function(){
		
		if(!CheckSelect("BONDDATA",'<spring:message code="LB.W1012" />',"#")){
			return false;
		} 
		
		$("#formID").attr("action","${__ctx}/BOND/PURCHASE/bond_purchase_input2");
		initBlockUI();//遮罩
		$("#formID").submit();	
	
	});
	
	$("#resetButton").click(function(){
		$("#BONDDATA").val("#")
		showBONDCRY("");
			
	});
	
});
function init(){
	getBondData();
}
function getBondData(){
	var fdinvtype = '${bond_purchase_input.data.FDINVTYPE}';
	var risk7 = '${bond_purchase_input.data.RISK7}' ;
	var URI = "${__ctx}/BOND/PURCHASE/getBondDataAjax";
	var rqData = { FDINVTYPE:fdinvtype , RISK7:risk7 };
	fstop.getServerDataEx(URI,rqData,false,getBondDataFinish);
}
function getBondDataFinish(data){
	if(data.result == true){
		var bondDatas = $.parseJSON(data.data)

		var BONDDATAHTML = "<option value='#'>---<spring:message code= "LB.Select" />---</option>";
		
		for(var x=0;x<bondDatas.length;x++){
			var bondData = JSON.stringify(bondDatas[x]);
			BONDDATAHTML+=  "<option value='" + bondData + "'>（" + bondDatas[x].BONDCODE + "）" + bondDatas[x].BONDNAME + "</option>";
		}
		$("#BONDDATA").html(BONDDATAHTML);
	}else{
		errorBlock(
				null, 
				null,
				['<spring:message code= "LB.X2548" />'], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}

function showBONDCRY(BONDCRY_SHOW){
	$('#BONDCRY').html(BONDCRY_SHOW);
}

</script>
</head>
<body>
<c:set var="SHWD" value="${bond_purchase_input.data.EMAILMSG}"></c:set>
<c:set var="MAILSIGN" value="${bond_purchase_input.data.mailSign}"></c:set>

<c:if test="${MAILSIGN !='Y'}">
	<%@ include file="bond_emailmsg.jsp"%>
</c:if>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
    <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
    <!-- 海外債券申購    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2516" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
<!-- 			海外債券申購 -->
				<h2><spring:message code="LB.X2516" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formID" method="post">
					<input type="hidden" id="TOKEN" name = "TOKEN" value="${sessionScope.transfer_confirm_token}">
					<input type="hidden" id="MAILSIGN" name = "MAILSIGN" value="${bond_purchase_input.data.mailSign}">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 客戶姓名 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1066" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_purchase_input.data.hiddenNAME}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 客戶投資屬性 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1067" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_purchase_input.data.FDINVTYPE_SHOW} </span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 外幣扣帳帳號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2517" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_purchase_input.data.ACN2}</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 債券名稱 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1012" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<select class="custom-select" style="width:auto" name="BONDDATA" id="BONDDATA" onchange="showBONDCRY($.parseJSON($('#BONDDATA').val()).BONDCRY_SHOW)">
                                        	</select>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 投資幣別 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0908" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span id="BONDCRY"></span>
                                		</div>
                               		</span>
                            	</div>
                            </div>
							<input type="button" id="resetButton" value="<spring:message code="LB.Re_enter"/>" class="ttb-button btn-flat-gray"/>	
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>	
				</form>
<!-- 				<ol class="description-list list-decimal"> -->
<!-- 					說明區 -->
<!-- 				</ol> -->
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>