<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="bs" value="${n361_8.data }"></c:set>
<c:set var="RQ" value="${n361_8.data.RQ }"></c:set>
<c:set var="KYC" value="${sessionScope.n361_kyc_confirm }"></c:set>
<c:set var="RESULT">
	<spring:message code="LB.D1214" />
</c:set><!-- 預約開戶申請完成 -->
<c:set var="NOTE">
	<spring:message code="LB.X1318" />
</c:set><!-- 申請後審核約1個工作天，本行會將開立基金戶之審核結果以電子郵件通知，審核通過後即可透過本行網路銀行辦理基金交易服務。 -->

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		
		// 畫面初始化
		function init() {
			//列印
			print();
		}
		
		//列印
		function print() {
			$("#printbtn").click(function () {
				var params = {
						jspTemplateName:	"apply_trust_account_8_print",
						jspTitle:			'<spring:message code= "LB.X0322" />',//<!-- 基金線上預約開戶結果 -->
						CMQTIME:			"${bs.CMQTIME }",
						DPUSERNAME:			"${bs.DPUSERNAME }",
						ACN_SSV_F:			"${RQ.ACN_SSV_F }",
						ACN_FUD_F:			"${RQ.ACN_FUD_F }",
						DPMYEMAIL:			"${RQ.DPMYEMAIL }",
						BRHNAME:			"${bs.BRHNAME }",
						BRHTEL:				"${bs.BRHTEL }",
						BRHADDR:			"${bs.BRHADDR }",
						RESULT:				"${RESULT }",
						NOTE:				"${NOTE }"
					};
				openWindowWithPost("${__ctx}/ONLINE/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
		}
		
		// 右上角下拉式選單
	 	function formReset() {
			if ($('#actionBar').val() == "result")
			{
	            $("#resultForm").submit();
	 		}
			else if ($('#actionBar').val() == "kyc")
			{
	            $("#kycForm").submit();
	 		}
			
            $('#actionBar').val("");
		}
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0262" /></li>
		</ol>
	</nav>

	
	<div class="content row">
		<!-- menu、登出窗格 -->
		<%@ include file="../index/menu.jsp"%>
		
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>預約開立基金戶</h2>
				<div id="step-bar">
					<ul>
						<li class="finished">身份驗證</li>
						<c:if test="${FATCA_CONSENT == 'Y'}">
							<li class="finished">FATCA個人客戶身份識別聲明</li>
						</c:if>
						<li class="finished">顧客權益</li>
						<li class="finished">申請資料</li>
						<li class="finished">確認資料</li>
						<li class="active">完成申請</li>
	              </ul>
				</div>
				
				<!-- 右上角下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Execution_option" /></option>
						<!-- 下載投資屬性問卷結果內容Excel檔 -->
						<option value="kyc"><spring:message code="LB.X0323" /></option><!-- 下載投資屬性問卷結果內容 -->
						<!-- 下載線上信託開戶結果Excel檔 -->
						<option value="result"><spring:message code="LB.X0324" /></option><!-- 下載線上信託開戶結果 -->
					</select>
				</div>
				
				<form id="formId" method="post" >
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<p>完成申請</p>
								</div>
	 								<p class="form-description">申請後，審核約需1個工作天。本行將以E-mail通知開立基金戶之審核結果。審核通過後即可透過本行網路銀行辦理基金交易服務。</p>
	
								<!--申請時間 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>申請時間</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.CMQTIME}</span>
										</div>
									</span>
								</div>
	
								<!-- 戶名 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>戶名</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.DPUSERNAME}</span>
										</div>
									</span>
								</div>
	
								<!--新台幣活期性存款帳戶 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>新台幣活期性存款帳戶</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${RQ.ACN_SSV_F}</span>
											<span class="input-remarks">本帳戶將用於新台幣扣帳/入帳之用。</span>
										</div>
									</span>
								</div>
	
								<!--外幣活期性存款帳戶-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>外幣活期性存款帳戶</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${RQ.ACN_FUD_F}</span>
											<c:if test="${ not empty RQ.ACN_FUD_F}">
												<span class="input-remarks">本帳戶將用於外幣扣帳/入帳之用。</span>
											</c:if>
										</div>
									</span>
								</div>
	
								<!--收取各項通知及報告書E-mail-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>收取各項通知及報告書E-mail</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${RQ.DPMYEMAIL }</span>
										</div>
									</span>
								</div>
	
								<!--服務分行-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>服務分行</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.BRHNAME}</span>
										</div>
										<div class="ttb-input">
											<span>${bs.BRHTEL}</span>
										</div>
										<div class="ttb-input">
											<span>${bs.BRHADDR}</span>
										</div>
									</span>
								</div>
	
							</div>
							
							<input type="button" id="printbtn" name="printbtn" class="ttb-button btn-flat-gray" value="列印結果" >
							<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="回首頁" onclick="fstop.getPage('${__ctx}'+'/INDEX/index','', '')">
							
						</div>
					</div>
					
				</form>
			</section>
		</main>
	</div>
				
	<!-- 下載用 -->
	<form id="resultForm" method="post" action="${__ctx}/download" target="_blank">
		<!-- 線上信託開戶結果 -->
		<input type="hidden" name="downloadFileName" value="<spring:message code="LB.X0325" />"/>
		<input type="hidden" name="downloadType" 	 value="OLDEXCEL"/> 					
		<input type="hidden" name="templatePath" 	 value="/downloadTemplate/apply_trust_account.xls"/>
		<input type="hidden" name="CMQTIME" 	 	 value="${bs.CMQTIME }"/>
		<input type="hidden" name="DPUSERNAME" 	 	 value="${bs.DPUSERNAME }"/>
		<input type="hidden" name="ACN_SSV_F" 	 	 value="${RQ.ACN_SSV_F }"/>
		<input type="hidden" name="ACN_FUD_F" 	 	 value="${RQ.ACN_FUD_F }"/>
		<input type="hidden" name="DPMYEMAIL" 	 	 value="${RQ.DPMYEMAIL }"/>
		<input type="hidden" name="BRHNAME" 	 	 value="${bs.BRHNAME }"/>
		<input type="hidden" name="BRHTEL" 		 	 value="${bs.BRHTEL }"/>
		<input type="hidden" name="BRHADDR" 		 value="${bs.BRHADDR }"/>
		<input type="hidden" name="RESULT" 		 	 value="${RESULT }"/>
		<input type="hidden" name="NOTE" 		 	 value="${NOTE }"/>
		<!-- EXCEL下載用 -->
		<!-- headerRightEnd  資料列以前的右方界線
			 headerBottomEnd 資料列到第幾列 從0開始
			 rowStartIndex 資料列第一列的位置
			 rowRightEnd 資料列用方的界線
		 -->
		<input type="hidden" name="headerRightEnd" 	value="1"/>
		<input type="hidden" name="headerBottomEnd" value="11"/>
	</form>
	
	<!-- 下載用 -->
	<form id="kycForm" method="post" action="${__ctx}/download" target="_blank">
		<!-- 網路銀行基金線上預約開戶作業(KYC問卷結果) -->
		<input type="hidden" name="downloadFileName" value="<spring:message code="LB.X0326" />"/>
		<input type="hidden" name="downloadType" 	 value="OLDEXCEL"/> 					
		<input type="hidden" name="templatePath"	 value="/downloadTemplate/KYC.xls"/>
		<input type="hidden" name="FDQ1" 			 value="${KYC.FDQ1_Desc}"/>
		<input type="hidden" name="FDQ2" 			 value="${KYC.FDQ2_Desc}"/>
		<input type="hidden" name="FDQ3" 			 value="${KYC.FDQ3_Desc}"/>
		<input type="hidden" name="FDQ4" 			 value="${KYC.FDQ4_Desc}"/>
		<input type="hidden" name="FDQ5" 			 value="${KYC.FDQ5_Desc}"/>
		<input type="hidden" name="FDQ6" 			 value="${KYC.FDQ6_Desc}"/>
		<input type="hidden" name="FDQ7" 			 value="${KYC.FDQ7_Desc}"/>
		<input type="hidden" name="FDQ8" 			 value="${KYC.FDQ8_Desc}"/>
		<input type="hidden" name="FDQ9" 			 value="${KYC.FDQ9_Desc}"/>
		<input type="hidden" name="FDQ10" 			 value="${KYC.FDQ10_Desc}"/>
		<input type="hidden" name="FDQ11" 			 value="${KYC.FDQ11_Desc}"/>  
		<input type="hidden" name="FDQ12" 			 value="${KYC.FDQ12_Desc}"/>   
		<input type="hidden" name="FDQ13" 			 value="${KYC.FDQ13_Desc}"/>
		<input type="hidden" name="FDQ14" 			 value="${KYC.FDQ14_Desc}"/>  
		<input type="hidden" name="FDQ15" 			 value="${KYC.FDQ15_Desc}"/>
		<input type="hidden" name="FDMARK1" 		 value="${KYC.FDMARK1_Desc}"/>        
		<input type="hidden" name="FDINVTYPE" 		 value="${KYC.FDINVTYPE_Desc}"/>
		
		<input type="hidden" name="headerRightEnd" 	value="1"/>
		<input type="hidden" name="headerBottomEnd" value="34"/>
	</form>
		
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
	
</body>

</html>