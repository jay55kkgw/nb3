<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			// 確認鍵 click
			goOn();
			//回上一頁
			back();
			//重新整理
			cmrest();

		}
		//重新整理
		function cmrest(){
			$("#CMRESET").click(function () {
				$("#row1").hide();
				$("#row2").hide();
				$("#row3").hide();
				$("#row4").hide();
				$("#formId")[0].reset();
			});
		}
		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					processQuery();
				}
			});
		}
		var CheckBoxTimes = 0;

		function processQuery() {
			var mip = '${regular_investment_step1.data.MIP}';
			//塞值
			var SR_Obj = "";
			var SR_Str = "";
			var SR_Str1 = "";
			var damt = '${regular_investment_step1.data.DAMT}';
			var alterType2checked = $("#ALTERTYPE2").prop("checked");
			if (alterType2checked) {
				if (damt == '1') {
					SR_Str = $("#SelectedRecord").val();
					if (SR_Str.length != 0) {
						SR_Str1 = ReplaceAll(SR_Str, "|", "");
						var splits = SR_Str.split("|");
						for (var i = 1; i <= splits.length; i++) {
							document.getElementById("PAYDAY" + i).value = splits[i - 1];
						}
					} else {
						//<!-- 請至少指定一個扣款日期 -->
						//alert("<spring:message code= "LB.Alert121" />");
						errorBlock(
								null, 
								null,
								["<spring:message code= 'LB.Alert121' />"], 
								'<spring:message code= "LB.Quit" />', 
								null
						);
						return false;
					}
				} else {
					SR_Str = $("#SelectedRecord1").val();
					if (SR_Str.length != 0) {
						SR_Str1 = ReplaceAll(SR_Str, "|", "");
						var splits = SR_Str.split("|");
						for (var i = 1; i <= splits.length; i++) {
							document.getElementById("PAYDAY" + i).value = splits[i - 1];
						}
					} else {
						//<!-- 請至少指定一個扣款日期 -->
						//alert("<spring:message code= "LB.Alert121" />");
						errorBlock(
								null, 
								null,
								["<spring:message code= 'LB.Alert121' />"], 
								'<spring:message code= "LB.Quit" />', 
								null
						);
						return false;
					}
				}
			}
			//送出
			initBlockUI(); //遮罩
			$("#formId").submit();
		}
		//回上一頁
		function back() {
			$("#CMBACK").click(function () {
				var action = '${__ctx}/FUND/ALTER/regular_investment';
				initBlockUI();
				$('#back').val("Y");
				$("form").attr("action", action);
				$("#formId").validationEngine('detach');
				$("form").submit();
			});

		}

		function resetform() {
			main.reset();
			CheckBoxTimes = 0;
			document.getElementById("SelectedRecord").value = "";
			document.getElementById("SelectedRecord1").value = "";
		}
		//顯示欲變更選項
		function display(type) {
			var PAYDAY = document.getElementsByName("PAYDAY");
			var PAYDAYC = document.getElementsByName("PAYDAYC");
			if (type == '1') {
				if ($("#ALTERTYPE1").prop('checked')) {
					$("#row1").show();
					$("#PAYAMT").addClass("validate[required]");
				} else {
					$("#row1").hide();
					$("#PAYAMT").removeClass("validate[required]");
				}
			} else if (type == '2') {
				if ($("#ALTERTYPE2").prop('checked')) {
					$("#row2").show();
					$("#row4").hide();
					for (var i = 0; i < 28; i++) {
						PAYDAY[i].checked = false;
					}
					document.getElementById("SelectedRecord").value = "";
					$("#validate_PAYDAYC").addClass("validate[funcCall[validate_Radio[<spring:message code= "LB.W1159" />,PAYDAYC]]]");
				} else {
					$("#row2").hide();
					$("#validate_PAYDAYC").removeClass("validate[funcCall[validate_Radio[<spring:message code= "LB.W1159" />,PAYDAYC]]]");
				}
			} else if (type == '3') {

				if ($("#ALTERTYPE3").prop('checked')) {
					row3.style.display = "";
					$("#row3").show();
					$("#validate_ALTERTYPE").addClass("validate[funcCall[validate_Radio[<spring:message code= "LB.W1161" />,ALTERTYPE]]]");
				} else {
					$("#row3").hide();
					$("#validate_ALTERTYPE").removeClass("validate[funcCall[validate_Radio[<spring:message code= "LB.W1161" />,ALTERTYPE]]]");
				}
			} else if (type == '4') {

				if ($("#ALTERTYPE2").prop('checked')) {
					$("#row4").show();
					$("#row2").hide();
					for (var i = 0; i < 6; i++) {
						PAYDAYC[i].checked = false;
					}
					document.getElementById("SelectedRecord1").value = "";
					$("#validate_PAYDAY").addClass("validate[funcCall[validate_Radio[<spring:message code= "LB.W1159" />,PAYDAY]]]");
				} else {
					row4.style.display = "none";
					$("#validate_PAYDAY").removeClass("validate[funcCall[validate_Radio[<spring:message code= "LB.W1159" />,PAYDAY]]]");
				}
			}
		}
		//
		function display1(type) {
			var SR_Str = $("#SelectedRecord").val();
			var PAYDAY = document.getElementsByName("PAYDAY");
			if (PAYDAY[type - 1].checked) {
				if (CheckBoxTimes >= 9) {
					//<!-- 已超過設定次數 -->
					//alert("<spring:message code= "LB.Alert122" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert122' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
					);
					PAYDAY[type - 1].checked = false;
					return;
				}
				CheckBoxTimes = CheckBoxTimes + 1;

				if (SR_Str.length == 0) {
					SR_Str += PAYDAY[type - 1].value;
				} else {

					if (SR_Str.indexOf(PAYDAY[type - 1].value) == -1) {

						SR_Str += "|" + PAYDAY[type - 1].value;
					}
				}

			} else {
				CheckBoxTimes = CheckBoxTimes - 1;

				SR_Str = ("|" + SR_Str + "|").replace("|" + PAYDAY[type - 1].value + "|", "|");

				if (SR_Str == "|") {
					SR_Str = "";
				} else {
					SR_Str = SR_Str.substr(1, SR_Str.length - 2);
				}
			}
			$("#SelectedRecord").val(SR_Str);
		}
		//日期2checked
		function display2(type) {
			var main = document.getElementById("formId");
			var SR_Str = $("#SelectedRecord1").val();
			 if(main.PAYDAYC[type-1].checked)
		  	  {
					if(SR_Str.length == 0)
					{
						SR_Str += main.PAYDAYC[type-1].value;
					}
					else
					{
						if(SR_Str.indexOf(main.PAYDAYC[type-1].value) == -1)
						{
							SR_Str += "|" + main.PAYDAYC[type-1].value;
						}
					}
					//alert(SR_Str);  	  			
			  }
			  else
			  {
					SR_Str = ("|" + SR_Str + "|").replace("|" + main.PAYDAYC[type-1].value + "|", "|");
					if(SR_Str == "|")
					{
						SR_Str = "";
					}
					else
					{
						SR_Str = SR_Str.substr(1, SR_Str.length - 2);
					}
					//alert(SR_Str);	
			  }
			$("#SelectedRecord1").val(SR_Str);
		}
		//
		function ReplaceAll(Source, stringToFind, stringToReplace) {
			var temp = Source;
			var index = temp.indexOf(stringToFind);
			while (index != -1) {
				temp = temp.replace(stringToFind, stringToReplace);
				index = temp.indexOf(stringToFind);
			}
			return temp;
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 定期投資約定變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1057" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--定期投資約定變更 -->
				<h2>
					<spring:message code="LB.W1057" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" name="formId"
					action="${__ctx}/FUND/ALTER/regular_investment_confirm">
					<c:set var="BaseResultData" value="${regular_investment_step1.data}"></c:set>
					<input type="hidden" name="ADOPID" value="C111">

					<input type="hidden" value="" name="PAYDAY1" id="PAYDAY1">

					<input type="hidden" value="" name="PAYDAY2" id="PAYDAY2">

					<input type="hidden" value="" name="PAYDAY3" id="PAYDAY3">

					<input type="hidden" value="" name="PAYDAY4" id="PAYDAY4">

					<input type="hidden" value="" name="PAYDAY5" id="PAYDAY5">

					<input type="hidden" value="" name="PAYDAY6" id="PAYDAY6">

					<input type="hidden" value="" name="PAYDAY7" id="PAYDAY7">

					<input type="hidden" value="" name="PAYDAY8" id="PAYDAY8">

					<input type="hidden" value="" name="PAYDAY9" id="PAYDAY9">

					<input type="hidden" name="SelectedRecord" id="SelectedRecord" value="">

					<input type="hidden" name="SelectedRecord1" id="SelectedRecord1" value="">

					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 信託帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0944" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.hide_CDNO}</span>
										</div>
									</span>
								</div>
								<!--扣款標的-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1041" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.FUNDLNAME}</span>
										</div>
									</span>
								</div>
								<!--類別 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.D0973" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span><spring:message code="${BaseResultData.str_MIP}" /></span>
										</div>
									</span>
								</div>
								<!--扣款來源 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.D1623" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span><spring:message code="${BaseResultData.str_DAMT}" /></span>
										</div>
									</span>
								</div>
								<!-- 變更項目 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1153" /></h4>
										</label></span>
									<span class="input-block">
										<c:choose>
											<c:when test="${BaseResultData.SALFLG == 'Y'}">
												<font class="ttb-input" style="color:red; font-weight: bold">(<spring:message code="LB.X2605" />)</font>
											</c:when>
											<c:otherwise>
												<div class="ttb-input">
													<label class="check-block" >
														<input type="checkbox" value="1" name="ALTERTYPE1" id="ALTERTYPE1"
															onclick="display('1')">
														<span class="ttb-unit"><spring:message code="LB.Deposit_amount_1" /></span>
														 <span class="ttb-check"></span>
													</label>
												</div>
												<div class="ttb-input">
													<label class="check-block">
													<c:if test="${BaseResultData.DAMT == '2'}">
														<input type="checkbox" value="2" name="ALTERTYPE2" id="ALTERTYPE2" onclick="display('2')">
													</c:if>
													
													<c:if test="${BaseResultData.DAMT != '2'}">
														<input type="checkbox" value="2" name="ALTERTYPE2" id="ALTERTYPE2" onclick="display('4')">
													</c:if>
														<span class="ttb-unit"><spring:message code="LB.W1155" /></span>
														 <span class="ttb-check"></span>
													</label>
												</div>
											</c:otherwise>
										</c:choose>
										<div class="ttb-input">
											<label class="check-block">
												<input type="checkbox" value="3" name="ALTERTYPE3" id="ALTERTYPE3" onclick="display('3')">
												<span class="ttb-unit"><spring:message code="LB.W1156" /></span>
												<span class="ttb-check"></span>
											</label>
										</div>
										<!-- 檢核欄位 -->
										<span class="hideblock">
											<input id="validate_ALTERTYPE_X" name="validate_ALTERTYPE_X" type="checkbox"  
												class="validate[funcCallRequired[validate_chkClickboxKind[<spring:message code= "LB.W1153" />,3,ALTERTYPE1,ALTERTYPE2,ALTERTYPE3]]]" 
												style="visibility:hidden"/>
										</span>
									</span>
								</div>
								<div id="row1" style="display: none;">
									<!--原每次定期申購金額 -->
									<div class="ttb-input-item row">
										<span class="input-title"><label>
												<h4><spring:message code="${BaseResultData.str_OPAYAMT}" /></h4>
											</label></span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.ADCCYNAME }
													${BaseResultData.display_OPAYAMT }
												</span>
											</div>
										</span>
									</div>
									<!--變更後每次定期申購金額 -->
									<div class="ttb-input-item row">
										<span class="input-title"><label>
												<h4><spring:message code="LB.W1157" /> </h4>
											</label></span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.ADCCYNAME}
													<input type="text" class="text-input" name="PAYAMT" id="PAYAMT">
												</span>
											</div>
										</span>
									</div>
								</div>

								<div id="row2" style="display: none;">
									<!--原日期 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1158" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.OPAYDAY4}
												${BaseResultData.OPAYDAY5}
												${BaseResultData.OPAYDAY6}
												${BaseResultData.OPAYDAY1}
												${BaseResultData.OPAYDAY2}
												${BaseResultData.OPAYDAY3}
											</span>
										</div>
									</span>
								</div>
									<!--變更後日期 -->
									<div class="ttb-input-item row">
										<span class="input-title"><label>
												<h4><spring:message code="LB.W1159" /></h4>
											</label></span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<label class="check-block">
														<input TYPE="checkbox" name="PAYDAYC" VALUE="03"
															onclick="display2(1)">03<spring:message code="LB.Day" />
													 	<span class="ttb-check"></span>											
													</label>
													<label class="check-block">
														<input TYPE="checkbox" name="PAYDAYC" VALUE="13"
															onclick="display2(2)">13<spring:message code="LB.Day" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input TYPE="checkbox" NAME="PAYDAYC" VALUE="23"
															onclick="display2(3)">23<spring:message code="LB.Day" /><BR>
													 	<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input TYPE="checkbox" NAME="PAYDAYC" VALUE="08"
															onclick="display2(4)">08<spring:message code="LB.Day" />
													 	<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input TYPE="checkbox" NAME="PAYDAYC" VALUE="18"
															onclick="display2(5)">18<spring:message code="LB.Day" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input TYPE="checkbox" NAME="PAYDAYC" VALUE="28"
															onclick="display2(6)">28<spring:message code="LB.Day" />
														<span class="ttb-check"></span>
													</label>
												</span>
											</div>
											<span class="hideblock">
												<input id="validate_PAYDAYC" name="validate_PAYDAYC" type="checkbox"  
													class="" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</span>
									</div>
								</div>
								<!--變更後日期2 -->
								<div id="row4" style="display: none;">
									<!--原日期 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1158" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.OPAYDAY4}
												${BaseResultData.OPAYDAY5}
												${BaseResultData.OPAYDAY6}
												${BaseResultData.OPAYDAY1}
												${BaseResultData.OPAYDAY2}
												${BaseResultData.OPAYDAY3}
											</span>
										</div>
									</span>
								</div>
									<!--變更後日期2 -->
									<div class="ttb-input-item row">
										<span class="input-title"><label>
												<h4><spring:message code="LB.W1159" /></h4>
											</label></span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<c:forEach var="i" begin="1" end="28">
														<!--數字 -->
														<c:choose>
															<c:when test="${ i < 10}">
																<c:set var="num" value="0${i}" />
															</c:when>
															<c:otherwise>
																<c:set var="num" value="${i}" />
															</c:otherwise>
														</c:choose>
														<!-- 日期 -->
														<c:choose>
															<c:when test="${i % 4 !=0}">
															<label class="check-block">
																<input type="checkbox" name="PAYDAY" value="<c:out value=" ${num}"/>" onclick="display1(${i})">
																<c:out value="${num}" /><spring:message code="LB.Day" />
																<span class="ttb-check"></span>
															</label>
															</c:when>
															<c:otherwise>
															<label class="check-block">
																<input type="checkbox" name="PAYDAY" value="<c:out value=" ${num}" />" onclick="display1(${i})">
																<c:out value="${num}" /><spring:message code="LB.Day" /><BR>
																<span class="ttb-check"></span>
															</label>	
															</c:otherwise>
														</c:choose>
													</c:forEach>
												</span>
											</div>
											<span class="hideblock">
												<input id="validate_PAYDAY" name="validate_PAYDAY" type="checkbox"  
													class="" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</span>
									</div>
								</div>
								<div id="row3" style="display: none;">
									<!--原扣款狀態 -->
									<div class="ttb-input-item row">
										<span class="input-title"><label>
												<h4><spring:message code="LB.X0371" /> </h4>
											</label></span>
										<span class="input-block">
											<div class="ttb-input">
												<span><spring:message code="${BaseResultData.str_Before}" /></span>
											</div>
										</span>
									</div>
									<!--變更後扣款狀態-->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.W1161" /></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<label class="radio-block">
												<input type="radio" value="B2" name="ALTERTYPE">
														 <spring:message code="LB.W1162" />
													 <span class="ttb-radio">
													 </span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="radio-block">
												<input type="radio" value="B1" name="ALTERTYPE">
													<spring:message code="LB.W1163" />
												<span class="ttb-radio">
												</span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="B0" name="ALTERTYPE">
														<spring:message code="LB.W1164" />
													<span class="ttb-radio">
													</span>
												</label>
											</div>
											<span class="hideblock">
												<input id="validate_ALTERTYPE" name="validate_ALTERTYPE" type="checkbox"  
													class="" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;"/>
											</span>
										</span>
									</div>
								</div>
							</div>
							<!-- button -->
							<input class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" type="button"
								value="<spring:message code="LB.Back_to_function_home_page" />" />
							<input class="ttb-button btn-flat-gray" name="CMRESET" id="CMRESET" type="button"
								value="<spring:message code="LB.Re_enter" />" />
							<input class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" type="button"
								value="<spring:message code="LB.Confirm" />" />
							<!-- button -->
						</div>
					</div>
					<div class="text-left">
						<ol class="description-list list-decimal">
							<!-- 						說明： -->
							<p>
								<spring:message code="LB.Description_of_page" />
							</p>
							
							<li>
								<span>
								<spring:message code="LB.Regular_Investment_P1_D1-1"/><br>
								<spring:message code="LB.Regular_Investment_P1_D1-2"/><br>
								<spring:message code="LB.Regular_Investment_P1_D1-3"/><br>
								<spring:message code="LB.Regular_Investment_P1_D1-4"/>
								</span>
							</li>
							<li>
								<spring:message code="LB.Regular_Investment_P1_D3"/>
							</li>
							<li>
								<b>
								<spring:message code= "LB.regular_investment_P2_D3" />
								</b>
							</li>
						</ol>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>