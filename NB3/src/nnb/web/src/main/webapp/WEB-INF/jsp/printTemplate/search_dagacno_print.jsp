<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
	<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/>
	<table class="print">
		<tr>
			<td colspan="4" style="text-align: right">
			<spring:message code="LB.Total_records" />： ${COUNT} <spring:message code="LB.Rows" />
			</td>
		</tr>
		<tr>
			<!-- 銀行名稱 -->
			<th><spring:message code= "LB.Bank_name" /></th>
			<!-- 約定轉入帳號 -->
			<th><spring:message code= "LB.D0227" /></th>
			<!-- 好記名稱 -->
			<th><spring:message code= "LB.Favorite_name" /></th>
			<!-- 臨櫃/線上約定帳號 -->
			<th><spring:message code= "LB.X1687" /></th>
		</tr>
		<c:if test="${COUNT != 0}">
		<c:forEach items="${dataListMap}" var="map">
			<tr>
				<td>${map.BNKCOD}</td>
				<td>${map.ACN}</td>
				<td>${map.DPGONAME}</td>
<%-- 				<td>${map.TRAFLAG}</td> --%>
				<c:choose>
				    <c:when test="${map.TRAFLAG == '1'}">
				    	<td><spring:message code= "LB.X1688" /></td><!-- 臨櫃約定 -->
				    </c:when>
				    <c:otherwise>
				        <td><spring:message code= "LB.X1689" /></td><!-- 線上約定 -->
				    </c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>
		</c:if>
	</table>
	<br/><br/>
<!-- 		<div class="text-left"> -->
<%-- 			<spring:message code="LB.Description_of_page" />: --%>
<!-- 			<ol class="list-decimal text-left"> -->
<%-- 				<li><spring:message code="LB.Balance_query_P1_D1" /></li> --%>
<%-- 				<li><spring:message code="LB.Balance_query_P1_D2" /></li> --%>
<!-- 			</ol> -->
<!-- 		</div> -->
	</body>
</html>