<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title></title>

<Script language="JavaScript">
//列印不顯示按鈕
function print_the_page(){
	//先回到頂端再列印
	document.body.scrollTop = document.documentElement.scrollTop = 0;
    document.getElementById("CMPRINT").style.visibility = "hidden";    //顯示按鈕
    javascript:print();
    document.getElementById("CMPRINT").style.visibility = "visible";   //不顯示按鈕
}
</Script>

<style>
.watermark.zin:before {
z-index: -1;
}
</style>

</head>

<body class="bodymargin watermark zin" style="-webkit-print-color-adjust: exact">

	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">
		
		
		</font>
	</div>
	<br />

	<table class="print">
		<tr>
			<!-- 列印時間 -->
			<td class="ColorCell" width="13%">
				<spring:message code="LB.X0040" />
			</td>
			<td class="DataCell0" colspan="7">
				${fxcertpreview.data.curentDateTime}
			</td>
		</tr>
		<tr>
			<!-- 統編/身分證號 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.X0041" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.CUSIDN}
			</td>
			<!-- 姓名 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.D0203" />
			</td>
			<td class="DataCell1" colspan="4">
				${fxcertpreview.data.NAME}
			</td>
		</tr>
		<tr>
			<!-- 付款日期 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0289" />
			</td>
			<td class="DataCell0" colspan="2">
				${fxcertpreview.data.PAYDATE}
			</td>
			<!-- 英文姓名 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.D0050" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.ENNAMEbr}
			</td>
			<!-- 收款銀行資料 -->
			<td class="ColorCell" rowspan="2" width="9%">
				<spring:message code="LB.X0037" />
			</td>
			<td class="DataCell0" rowspan="2" width="22%">
				SWIFT CODE：
				${fxcertpreview.data.FXRCVBKCODE}
				<br>
				<!-- 名稱/地址：  -->
				<spring:message code="LB.W0295" />
				<br>
				${fxcertpreview.data.FXRCVBKADDR}
			</td>
		</tr>
		<tr>
			<!-- 交易序號 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.Transaction_Number" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.STAN}
			</td>
			<!-- 銀行交易編號 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.X0042" /><spring:message code="LB.W0286" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.REFNO}
			</td>
		</tr>
		<tr>
			<!-- 付款帳號 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0290" />
			</td>
			<td class="DataCell0" colspan="2">
					${fxcertpreview.data.CUSTACC}
			</td>
			<!-- 付款金額 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.X0031" />
			</td>
			<td class="DataCell0" colspan="2">
					${fxcertpreview.data.ORGCCY}
					${fxcertpreview.data.str_orgAmt}
			</td>
			<!-- 收款人資料 -->
			<td class="ColorCell" rowspan="4" width="9%">
				<spring:message code="LB.W0294" />
			</td>
			<td class="DataCell0" rowspan="4" width="22%">
				<!-- 名稱/地址： -->
				<spring:message code="LB.W0295" />
				<br>
				${fxcertpreview.data.FXRCVADDR}
			</td>
		</tr>
		<tr>
			<!-- 收款帳號 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0292" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.BENACC}
			</td>
			<!-- 收款金額 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.X0032" />
			</td>
			<td class="DataCell1" colspan="2">
				<!-- 幣別 -->
				${fxcertpreview.data.PMTCCY}
				<!-- 金額 -->
				${fxcertpreview.data.str_pmtAmt}
			</td>
		</tr>
		<tr>
			<!-- 匯率 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.Exchange_rate" />
			</td>
			<td class="DataCell0" colspan="2">
				${fxcertpreview.data.str_Rate}
			</td>
			<!-- 議價編號 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.Bargaining_number" />
			</td>
			<td class="DataCell0" colspan="2">
				${fxcertpreview.data.BGROENO}
			</td>
		</tr>
		<tr>
			<!-- 收款人身份別 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.Payee_identity" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.display_BENTYPE}
			</td>
			<!-- 收款地區國別 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.X0043" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.COUNTRY}
			</td>
		</tr>
		<tr>
			<!-- 匯款分類項目 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0298" />
			</td>
			<td class="DataCell0" colspan="5">
				${fxcertpreview.data.SRCFUNDDESC}
			</td>
			<!-- E-MAIL 通知 -->
			<td class="ColorCell" rowspan="5" width="9%">
				<spring:message code="LB.W0300" />
			</td>
			<td class="DataCell0" rowspan="5" width="22%">
				<!-- 電子信箱 : -->
				<spring:message code="LB.Email" />
				${fxcertpreview.data.CMTRMAIL}
				<br>
				<!-- 摘要內容:  -->
				<spring:message code="LB.Summary" />
				<br>
				${fxcertpreview.data.str_MailMemo}
			</td>
		</tr>
		<tr>
			<!-- 匯款分類說明 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0299" />
			</td>
			<td class="DataCell1" colspan="5">
				${fxcertpreview.data.FXRMTDESC}
			</td>
		</tr>
		<tr>
			<!-- 匯款附言 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0303" />
			</td>
			<td class="DataCell0" colspan="5">
				${fxcertpreview.data.str_Memo1}
			</td>
		</tr>
		<tr>
			<!-- 費用扣款帳號 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0305" />
			</td>
			<td class="DataCell1" colspan="2">
			${fxcertpreview.data.COMMACC}
			</td>
			<!-- 手續費負擔別 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.W0155" />
			</td>
			<td class="DataCell1" colspan="2">
			${fxcertpreview.data.DETCHG}
			</td>
		</tr>
		<tr>
			<!-- 手續費 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.D0507" />
			</td>
			<td class="DataCell0" width="10%">
				<!-- 幣別 -->
				${fxcertpreview.data.COMMCCY}
				<!-- 金額 -->
				${fxcertpreview.data.str_commAmt}
			</td>
			<!-- 郵電費 -->
			<td class="ColorCell" width="8%">
				<spring:message code="LB.W0345" />
			</td>
			<td class="DataCell1" width="14%">
				<!-- 幣別 -->
				${fxcertpreview.data.COMMCCY}
				<!-- 金額 -->
				${fxcertpreview.data.str_cabAmt}
			</td>
			<!-- 國外費用 -->
			<td class="ColorCell" width="8%">
				<spring:message code="LB.W0346" />
			</td>
			<td class="DataCell0" width="14%">
				<!-- 幣別 -->
				${fxcertpreview.data.COMMCCY}
				<!-- 金額 -->
				${fxcertpreview.data.str_ourAmt}
			</td>
		</tr>
	</table>

	<br/>

	<div style="text-align:center">
		<spring:message code= "LB.X1465" />
	</div>
	
	<div style="text-align:center">
		<input type="button" class="ttb-button btn-flat-orange" style="z-index:10000" id="CMPRINT" value="<spring:message code= "LB.Print_thepage" />" onclick="print_the_page()" />
	</div>
	<br/><br/><br/><br/>
</body>
</html>