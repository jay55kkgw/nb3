<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">

	
	
</script>
<div class="card-detail-block" id="err1">
	<div class="card-center-block">
<!-- 		<h2>中心主机</h2> -->
		<table class="stripe table table-striped ttb-table dtable1" data-toggle-column="first">
			<thead>
				<tr>
					<th><spring:message code="LB.error_code" /></th>
					<th><spring:message code="LB.ErrMsg" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="dataList" items="${error_code}" >
					<tr>
						<td>${dataList.ADMCODE}</td>
						<td>${dataList.ADMSGIN}</td>
					</tr>

				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
<div class="card-detail-block" id="err2">
	<div class="card-center-block">
<!-- 		<h2>浏览器组件</h2> -->
		<table class="stripe table table-striped ttb-table dtable2" data-toggle-column="first">
			<thead>
				<tr>
					<th><spring:message code="LB.error_code" /></th>
					<th><spring:message code="LB.ErrMsg" /></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						E001
					</td>
					<td>
						卡片阅读机侦测失败。
					</td>
				</tr>
				
				<tr>
					<td>
						E002
					</td>
					<td>
						无法连接卡片阅读机驱动程序。
					</td>
				</tr>
				
				<tr>
					<td>
						E003
					</td>
					<td>
						卡片侦测失败。
					</td>
				</tr>
				
				<tr>
					<td>
						E004
					</td>
					<td>
						系统异常，请将IC卡片拔出后重新插入，再试。
					</td>
				</tr>
				
				<tr>
					<td>
						E005
					</td>
					<td>
						非本行芯片金融卡/密码输入错误，或验证密码失败。
					</td>
				</tr>
				
				<tr>
					<td>
						E005,69F0, 0
					</td>
					<td>
						确认密码操作错误
					</td>
				</tr>
					
				<tr>
					<td>
						E006
					</td>
					<td>
						无法取得发卡行信息。
					</td>
				</tr>
				
				<tr>
					<td>
						E007
					</td>
					<td>
						卡片密码不可为空白。
					</td>
				</tr>
				
				<tr>
					<td>
						E007|69F0
					</td>
					<td>
						密码输入操作错误
					</td>
				</tr>
				
				<tr>
					<td>
						E007|69F0,0
					</td>
					<td>
						确认密码操作错误
					</td>
				</tr>
				
				<tr>
					<td>
						E007|6610
					</td>
					<td>
						密码输入错误
					</td>
				</tr>
				
				<tr>
					<td>
						E008
					</td>
					<td>
						卡片密码错误。
					</td>
				</tr>
				
				<tr>
					<td>
						E009
					</td>
					<td>
						卡片已遭锁定。
					</td>
				</tr>
				
				<tr>
					<td>
						E010
					</td>
					<td>
						取得发卡行代码失败。
					</td>
				</tr>
				
				<tr>
					<td>
						E011
					</td>
					<td>
						取得账号资料失败。
					</td>
				</tr>
					
				<tr>
					<td>
						E012
					</td>
					<td>
						请确认是否正确插入晶片金融卡。
					</td>
				</tr>
				<tr>
					<td>
						E013
					</td>
					<td>
						取得交易序号与TAC码失败。
					</td>
				</tr>
				
				<tr>
					<td>
						E014
					</td>
					<td>
						您于抽插拔芯片金融卡时更换不同IC卡片，该交易取消，若要重试，请重新按确定键。
					</td>
				</tr>
				
				<tr>
					<td>
						E015
					</td>
					<td>
						抽插拔芯片金融卡逾时，该交易取消，若要重试，请重新按确定键。
					</td>
				</tr>
				
				<tr>
					<td>
						E016
					</td>
					<td>
						您于抽插拔芯片金融卡时点了[取消]键，该交易取消，若要重试，请重新按确定键。
					</td>
				</tr>
				
				<tr>
					<td>
						E017
					</td>
					<td>
						系统异常，请关闭浏览器后重做。
					</td>
				</tr>
					
				<tr>
					<td>
						E018
					</td>
					<td>
						系统异常，请关闭浏览器后重做。
					</td>
				</tr>
				<tr>
					<td>
						E019
					</td>
					<td>
						系统异常，请关闭浏览器后重做。
					</td>
				</tr>
					
				<tr>
					<td>
						E020
					</td>
					<td>
						系统异常，请关闭浏览器后重做。
					</td>
				</tr>
				<tr>
					<td>
						E021
					</td>
					<td>
						转入账号验证错误，请重新输入后再试。
					</td>
				</tr>
					
				<tr>
					<td>
						E022
					</td>
					<td>
						系统异常，请关闭浏览器后重做。
					</td>
				</tr>
				<tr>
					<td>
						E023
					</td>
					<td>
						系统异常，请关闭浏览器后重做。
					</td>
				</tr>
					
				<tr>
					<td>
						E024
					</td>
					<td>
						密码变更失败。
					</td>
				</tr>
				<tr>
					<td>
						E025
					</td>
					<td>
						原密码输入错误。
					</td>
				</tr>
				<tr>
					<td>
						E999
					</td>
					<td>
						卡片阅读机控制对象加载失败。
					</td>
				</tr>
				<tr>
					<td>
						E_Send_11_OnError_1006
					</td>
					<td>
						尚未安装组件。
					</td>
				</tr>
				<tr>
					<td>
						0x8010001B
					</td>
					<td>
						The reader driver did not produce a unique reader name.卡片阅读机驱动程序未产生唯一的卡片阅读机名称。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100003
					</td>
					<td>
						The supplied handle was not valid.卡片阅读机装置控制失效。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100027
					</td>
					<td>
						Access is denied to the file.读取IC卡片文件错误。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100006
					</td>
					<td>
						Not enough memory available to complete this command.记忆空间不足。
					</td>
				</tr>
				
				<tr>
					<td>
						0x8010002E
					</td>
					<td>
						No smart card reader is available.未侦测到卡片阅读机。
					</td>
				</tr>
					
				<tr>
					<td>
						0x8010000C
					</td>
					<td>
						The operation requires a smart card, but no smart card is currently in the device.卡片阅读机装置已脱机。
					</td>
				</tr>
				
				<tr>
					<td>
						0x8010000B
					</td>
					<td>
						The smart card cannot be accessed because of other outstanding connections.卡片阅读机正被其他程序独占中。
					</td>
				</tr>
					
				<tr>
					<td>
						0x80100066
					</td>
					<td>
						The smart card is not responding to a reset.IC卡片重置无响应。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100068
					</td>
					<td>
						The smart card was reset.IC卡片重置。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100069
					</td>
					<td>
						The smart card has been removed, so further communication is not possible.IC卡片移出卡片阅读机，无法连接。
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="card-detail-block" id="err3">
	<div class="card-center-block">
<!-- 		<h2>电子签章</h2> -->
		<table class="stripe table table-striped ttb-table dtable3" data-toggle-column="first">
			<thead>
				<tr>
					<th><spring:message code="LB.error_code" /></th>
					<th><spring:message code="LB.ErrMsg" /></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>3202</td>
					<td>找不到可用凭证</td>
				</tr>
				<tr>
					<td>3206</td>
					<td>取得凭证信息失败</td>
				</tr>
				<tr>
					<td>3209</td>
					<td>凭证已过期</td>
				</tr>
				<tr>
					<td>3210</td>
					<td>凭证尚未到达凭证启用时间</td>
				</tr>
				<tr>
					<td>3211</td>
					<td>无可用凭证！凭证不是未达启用时间就是已过期</td>
				</tr>
				<tr>
					<td>5001</td>
					<td>一般错误</td>
				</tr>
				<tr>
					<td>5002</td>
					<td>配置内存发生错误</td>
				</tr>
				<tr>
					<td>5003</td>
					<td>内存缓冲区太小</td>
				</tr>
				<tr>
					<td>5004</td>
					<td>未支援函式</td>
				</tr>
				<tr>
					<td>5005</td>
					<td>错误的参数</td>
				</tr>
				<tr>
					<td>5006</td>
					<td>无效的handle</td>
				</tr>
				<tr>
					<td>5007</td>
					<td>试用版期限已过</td>
				</tr>
				<tr>
					<td>5008</td>
					<td>Base64编码错误</td>
				</tr>
				<tr>
					<td>5010</td>
					<td>无法在MS CryptoAPI Database中找到指定凭证</td>
				</tr>
				<tr>
					<td>5011</td>
					<td>凭证已过期</td>
				</tr>
				<tr>
					<td>5012</td>
					<td>凭证尚未合法，无法使用</td>
				</tr>
				<tr>
					<td>5013</td>
					<td>凭证可能过期或无法使用</td>
				</tr>
				<tr>
					<td>5014</td>
					<td>凭证主旨错误</td>
				</tr>
				<tr>
					<td>5015</td>
					<td>无法找到凭证发行者</td>
				</tr>
				<tr>
					<td>5016</td>
					<td>不合法的凭证签章</td>
				</tr>
				<tr>
					<td>5017</td>
					<td>凭证用途（加解密、签验章）不合适</td>
				</tr>
				<tr>
					<td>5020</td>
					<td>凭证已撤销</td>
				</tr>
				<tr>
					<td>5021</td>
					<td>凭证已撤销（密钥泄露）</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>



