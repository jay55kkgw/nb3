<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
            <div class="ttb-message">
                <p>完成申請</p>
            </div>
							
			<div class="text-left">
					<p>親愛的客戶您好:</p>
					<p>謝謝您完成本行『數位存款帳戶帳戶服務』申請作業，本行將儘速辦理您的開戶申請，並以e-mail方式通知您辦理結果。若有任何問題，您可撥電話至您所指定服務分行：
					<B>${BRHNAME}(聯絡電話：${BRHTEL})</B>
					詢問，我們將竭誠為您服。祝您事業順心，萬事如意!</p>
					<p>提醒您：<c:if test="${chkfile == 'N'}"><font style="color: red">您的證件尚未上傳或有遺漏</font></c:if><br />本行將審核您所上傳的資料，如有模糊不清或資料遺漏，將有專人與您聯繫。如您未在申請日起20日內 (含)補齊，您的數位存款帳戶申請將會失效。</p>
			</div>
<!-- 			<div class="text-left"> -->
<%-- 					<p><spring:message code= "LB.X0152" />:</p> --%>
<%-- 					<p><spring:message code= "LB.X0220" /></p> --%>
<%-- 					<p><spring:message code= "LB.X0221" /><B>${BRHNAME}(<spring:message code= "LB.D0539" />：${BRHTEL})</B><spring:message code= "LB.X0223" /></p> --%>
<%-- 					<p><spring:message code= "LB.X0224" /><BR></p> --%>
<%-- 					<p><spring:message code= "LB.X0225" /></p> --%>
<%-- 					<p><spring:message code= "LB.X0226" /></p> --%>
<!-- 			</div> -->
<!-- 			<div class="text-right"> -->
<%-- 				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code= "LB.W0605" />&nbsp;&nbsp;&nbsp;<spring:message code= "LB.X0193" /> --%>
<!-- 			</div>     -->
		</div>
		
	</div>
	
</body>
</html>