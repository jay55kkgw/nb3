<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
  <br /><br />
  <div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
  <br /><br />
  <div style="text-align:center">
    <font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
  </div>
  <br />
  <!-- 即時交易結果 -->
  <c:if test="${FGTXDATE == '1'}">
    <div style="text-align:center">
      <spring:message code= "LB.Transfer_successful" />
    </div>
    <br />
    <table class="print">
      <!-- 交易時間 -->
      <tr>
        <td style="width:8em">
          <spring:message code="LB.Trading_time" />
        </td>
        <td>${CMTXTIME} </td>
      </tr>
      <!-- 轉帳日期 -->
      <tr>
        <td style="width:8em">
          <spring:message code="LB.Transfer_date" />
        </td>
        <td>${transfer_date} </td>
      </tr>
      <!-- 轉出帳號 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Payers_account_no" />
        </td>
        <td>${OUTACN}</td>
      </tr>
      <!-- 轉入帳號 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Payees_account_no" />
        </td>
        <td> ${DPAGACNO_TEXT}</td>
      </tr>
      <!-- 轉帳金額 -->
      <tr>
        <td class="ColorCell">
          <!--         轉帳金額 -->
          <spring:message code="LB.Certificate_no" />
        </td>
        <td>
          <!--         新台幣 -->
          <spring:message code="LB.NTD" />
          &nbsp;
          <fmt:formatNumber type="number" minFractionDigits="2" value="${AMOUNT }" />
          &nbsp;
          <!--         元 -->
          <spring:message code="LB.Dollar" />
        </td>
      </tr>
      <!-- 存單號碼 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Certificate_no" />
        </td>
        <td class="DataCell">
          ${FDPNUM}
        </td>
      </tr>
      <!-- 存款種類 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Amount" />
        </td>
        <td>
          ${FDPACC}
        </td>
      </tr>
      <!-- 起存日 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Start_date" />
        </td>
        <td>
          ${SDT}
        </td>
      </tr>
      <!-- 到期日 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Maturity_date" />
        </td>
        <td>
          ${DUEDATE}
        </td>
      </tr>
      <!-- 計息方式 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Interest_calculation" />
        </td>
        <td>
          ${INTMTH}
        </td>
      </tr>
      <!-- 利率 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Interest_rate" />
        </td>
        <td>
          ${ITR}%
        </td>
      </tr>
      <!-- 到期轉期 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Rollover_method" />
        </td>
        <td>
          ${CODENAME}
        </td>
      </tr>
      <!-- 轉存方式  //不轉期時不顯示轉存方式-->
      <c:if test="${CODE != '0'}">
        <tr>
          <td class="ColorCell">
            <spring:message code="LB.Roll-over_when_expiration" />
          </td>
          <td>
            ${DPSVTYPENAME}
          </td>
        </tr>
      </c:if>
      <!-- 交易備註 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Transfer_note" />
        </td>
        <td>
          ${CMTRMEMO}
        </td>
      </tr>
      <!-- 轉出帳號帳戶餘額 -->
      <tr>
        <td class="ColorCell">
          <!--         轉出帳號帳戶餘額 -->
          <spring:message code="LB.Payers_account_balance" />
        </td>
        <td>
          <!--           新台幣 -->
          <spring:message code="LB.NTD" />
          &nbsp;
          <fmt:formatNumber type="number" minFractionDigits="2" value="${O_TOTBAL }" />
          <!--           元 -->
          &nbsp;
          <spring:message code="LB.Dollar" />
        </td>
      </tr>
      <!-- 轉出帳號可用餘額 -->
      <tr>
        <td class="ColorCell">
          <!--         轉出帳號可用餘額 -->
          <spring:message code="LB.Payers_available_balance" />
        </td>
        <td>
          <!--           新台幣 -->
          <spring:message code="LB.NTD" />
          &nbsp;
          <fmt:formatNumber type="number" minFractionDigits="2" value="${O_AVLBAL }" />
          &nbsp;
          <!--           元 -->
          <spring:message code="LB.Dollar" />
        </td>
      </tr>
    </table>
    <br>
    <div class="text-left">
      <!-- 		說明： -->
      <spring:message code="LB.Description_of_page" />:
      <ol class="list-decimal text-left">
        <li><spring:message code= "LB.Deposit_transfer_P3_D1" /></li>
        <li><spring:message code= "LB.Deposit_transfer_P3_D2" /></li>
      </ol>
    </div>
  </c:if>
  <!-- 預約交易結果 -->
  <c:if test="${FGTXDATE == '2'}">
    <div style="text-align:center">
     <spring:message code= "LB.W0284" />
    </div>
    <br />
    <table class="print">
      <!-- 交易時間 -->
      <tr>
        <td style="width:8em">
          <spring:message code="LB.Trading_time" />
        </td>
        <td>${CMTXTIME} </td>
      </tr>
      <!-- 轉帳日期 -->
      <tr>
        <td style="width:8em">
          <spring:message code="LB.Transfer_date" />
        </td>
        <td>${transfer_date} </td>
      </tr>
      <!-- 轉出帳號 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Payers_account_no" />
        </td>
        <td>${OUTACN}</td>
      </tr>
      <!-- 轉入帳號 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Payees_account_no" />
        </td>
        <td> ${DPAGACNO_TEXT}</td>
      </tr>
      <!-- 轉帳金額 -->
      <tr>
        <td class="ColorCell">
          <!--         轉帳金額 -->
          <spring:message code="LB.Certificate_no" />
        </td>
        <td>
          <!--         新台幣 -->
          <spring:message code="LB.NTD" />
          &nbsp;
          <fmt:formatNumber type="number" minFractionDigits="2" value="${AMOUNT }" />
          &nbsp;
          <!--         元 -->
          <spring:message code="LB.Dollar" />
        </td>
      </tr>
      <!-- 存款種類 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Amount" />
        </td>
        <td>
          ${FDPACC}
        </td>
      </tr>
      <!-- 	存款期別或<br/>指定到期日 -->
      <tr>
        <td class="ColorCell">
          <spring:message code= "LB.Deposit_period_or_Designated_due_date" />
        </td>
        <td>
          ${TERM_TEXT}
        </td>
      </tr>
      <!-- 計息方式 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Interest_calculation" />
        </td>
        <td>
          ${INTMTH}
        </td>
      </tr>
      <!-- 到期轉期 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Roll-over_when_expiration" />
        </td>
        <td>
          ${CODENAME}
        </td>
      </tr>
      <!-- 轉存方式  //不轉期時不顯示轉存方式-->
      <c:if test="${CODE != '0'}">
        <tr>
          <td class="ColorCell">
            <spring:message code="LB.Rollover_method" />
          </td>
          <td>
            ${DPSVTYPENAME}
          </td>
        </tr>
      </c:if>
      <!-- 交易備註 -->
      <tr>
        <td class="ColorCell">
          <spring:message code="LB.Transfer_note" />
        </td>
        <td>
          ${CMTRMEMO}
        </td>
      </tr>
    </table>
    <br>
    <div class="text-left">
      <!-- 		說明： -->
      <spring:message code="LB.Description_of_page" />
      <ol class="list-decimal text-left">
        <li>
          <spring:message code="LB.Deposit_transfer_PP3_D1" />
        </li>
        <li>
          <spring:message code="LB.Deposit_transfer_PP3_D2" />
        </li>
        <li>
          <spring:message code="LB.Deposit_transfer_PP3_D3" />
        </li>
      </ol>
    </div>
  </c:if>
</body>

</html>