<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			init();
			datetimepickerEvent();
			getTmr();
			// 初始化時隱藏span
			$("#hideblock_CMSDATE").hide();
			$("#hideblock_CMEDATE").hide();
		});

		function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			//btn 
			$("#CMSUBMIT").click(function (e) {
				e = e || window.event;
				if( $('#CMPERIOD').prop('checked') )
				{
					if(checkTimeRange() == false )
					{
						return false;
					}
				}
				//打開驗證隱藏欄位
				$("#hideblock_CMSDATE").show();
				$("#hideblock_CMEDATE").show();
				//塞值進span內的input
				$("#validate_CMSDATE").val($("#CMSDATE").val());
				$("#validate_CMEDATE").val($("#CMEDATE").val());
				console.log("submit~~");
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").removeAttr("target");
					$("#formId").attr("action", "${__ctx}/FCY/ACCT/f_outward_remittances_q_result");
					$("#formId").submit();
				}
			});
			$("#CMRESET").click(function(e){
				console.log("CMRESET submit");
				$("#formId")[0].reset();
				getTmr();
				});
		} // init END
		//選項
	 	function formReset(){
	 		if( $('#CMPERIOD').prop('checked') )
			{
				if(checkTimeRange() == false )
				{
					return false;
				}
			}
			//打開驗證隱藏欄位
			$("#hideblock_CMSDATE").show();
			$("#hideblock_CMEDATE").show();
			//塞值進span內的input
			$("#validate_CMSDATE").val($("#CMSDATE").val());
			$("#validate_CMEDATE").val($("#CMEDATE").val());
			
			if(!$("#formId").validationEngine("validate")){
				e = e || window.event;//forIE
				e.preventDefault();
			}
	 		else{
		 		//initBlockUI();
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/f_outward_remittances_q_result.xls");
		 		}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
				    $("#templatePath").val("/downloadTemplate/f_outward_remittances_q_result.txt");
		 		}
				//ajaxDownload("${__ctx}/FCY/ACCT/f_outward_remittances_q_ajaxDirectDownload","formId","finishAjaxDownload()");
				$("#formId").attr("target", "");
				$("#formId").attr("action", "${__ctx}/FCY/ACCT/f_outward_remittances_q_ajaxDirectDownload");
				$("#formId").submit();
				$('#actionBar').val("");
	 		}
	 	}
		function finishAjaxDownload(){
			$("#actionBar").val("");
			unBlockUI(initBlockId);
		}
		// 日曆欄位參數設定
		function datetimepickerEvent() {
			$(".CMDATE").click(function (event) {
				$('#CMDATE').datetimepicker('show');
			});
			$(".CMSDATE").click(function (event) {
				$('#CMSDATE').datetimepicker('show');
			});
			$(".CMEDATE").click(function (event) {
				$('#CMEDATE').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
		//預約自動輸入今天
		function getTmr() {
			var today = "${outward_remittances.data.TODAY}";
			$('#CMSDATE').val(today);
			$('#CMEDATE').val(today);
			$('#odate').val(today);
		}
		
		function checkTimeRange()
		{
			var now = Date.now();
			var twoYm = 63115200000;
			var twoMm = 5259600000;
			
			var startT = new Date( $('#CMSDATE').val() );
			var endT = new Date( $('#CMEDATE').val() );
			var distance = now - startT;
			var range = endT - startT;
			
			var limitS = new Date(now - twoYm);
			var limitE = new Date(startT.getTime() + twoMm); 
			if(distance > twoYm)
			{
				var m = limitS.getMonth() + 1;
				var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
				// 起始日不能小於
				var msg = '<spring:message code="LB.Start_date_check_note_1" />' + time;
				//alert(msg);
				return false;
			}
			else
			{
				if(range > twoMm)
				{
					var m = limitE.getMonth() + 1;
					var time = limitE.getFullYear() + '/' + m + '/' + limitE.getDate();
					// 終止日不能大於
					var msg = '<spring:message code="LB.End_date_check_note_1" />' + time;
					//alert(msg);
					return false;
				}
			}
			return true;
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 匯出匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0286" /></li>
    <!-- 匯出匯款查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0143" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--匯出匯款查詢 -->
					<spring:message code="LB.W0143" />
<!-- 					 is8n -->
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
							<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>	
				
				<form id="formId" method="post">
					<!-- 下載用 -->
 					<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0143" />"/><!--匯出匯款查詢 -->
					<input type="hidden" name="CMQTIME" value="${outward_remittances_result.data.CMQTIME}"/>
					<input type="hidden" name="CMPERIOD" value="${outward_remittances_result.data.CMPERIOD}"/>
					<input type="hidden" name="COUNT" value="${outward_remittances_result.data.COUNTS}"/>
					<input type="hidden" name="CURRANCY_PRINT" value="${outward_remittances_result.data.CURRANCY_PRINT}"/>
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/>
					<input type="hidden" name="hasMultiRowData" value="false"/> 
					<input type="hidden" name="hasMultiRowData" value="false"/> 
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="7"/>
					<input type="hidden" name="headerBottomEnd" value="6"/>
					<input type="hidden" name="rowStartIndex" value="7" />
					<input type="hidden" name="rowRightEnd" value="7" />
					<input type="hidden" name="footerStartIndex" value="9" />
					<input type="hidden" name="footerEndIndex" value="11" />
					<input type="hidden" name="footerRightEnd" value="7" />
					<!-- TXT下載用 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="9"/>
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="true"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 查詢區間區塊 -->
								<div class="ttb-input-item row">
									<!--查詢區間  -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Inquiry_period" /></h4>
										</label>
									</span>
									<span class="input-block">
											<!--期間起日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_start_date" />
												</span>
												<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" value="" />
												<span class="input-unit CMSDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CMSDATE" >
												<!-- 驗證用的input -->
												<input id="validate_CMSDATE" name="validate_CMSDATE" type="text" class="text-input validate[required, verification_date[validate_CMSDATE]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
											<!--期間迄日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_end_date" />
												</span>
												<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" value="" />
												<span class="input-unit CMEDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CMEDATE" >
												<!-- 驗證用的input -->
												<input id="validate_CMEDATE" name="validate_CMEDATE" type="text" 
													class="text-input validate[required, verification_date[validate_CMEDATE]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CheckDateScope" >
												<!-- 驗證用的input -->
												<input id="odate" name="odate" type="text" 
													class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, CMSDATE, CMEDATE, false, 12,null]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
									</span>
								</div>
							</div>
							<!-- 網頁顯示 button-->
							<!-- 重新輸入 -->
<%-- 								<spring:message code="LB.Re_enter" var="cmRest"></spring:message> --%>
<%-- 								<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray"> --%>
								<!--網頁顯示 -->
								<spring:message code="LB.Display_as_web_page" var="cmSubmit"></spring:message>
								<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
								<!-- button -->
						</div>
					</div>
					<div class="text-left">
						<ol class="description-list list-decimal text-left">
						<p><spring:message code="LB.Description_of_page"/></p>
							<li><span><spring:message code="LB.F_Outward_Remittances_q_P1_D1" /></span></li>
							<li><span><spring:message code="LB.F_Outward_Remittances_q_P1_D2" /></span></li>
						</ol>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>