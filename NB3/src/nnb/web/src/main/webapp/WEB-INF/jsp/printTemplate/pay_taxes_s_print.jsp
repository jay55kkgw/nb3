<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<table class="print">
	<tr>
		<td style="text-align:center"><spring:message code= "LB.Data_time" /></td>
		<td style="text-align:center"><spring:message code= "LB.Transfer_date" /></td>
		<td style="text-align:center"><spring:message code= "LB.Payers_account_no" /></td>
		<td style="text-align:center"><spring:message code= "LB.X0469" /></td>
		<td style="text-align:center"><spring:message code= "LB.W0410" /></td>
		<td style="text-align:center"><spring:message code= "LB.Id_no" /></td>
		<td style="text-align:center"><spring:message code= "LB.W0407" /></td>
		<td style="text-align:center"><spring:message code= "LB.W0409" /></td>
		<td style="text-align:center"><spring:message code= "LB.W0415" /></td>
		<td style="text-align:center"><spring:message code= "LB.Transfer_note" /></td>
	</tr>
	<tr>
		<td>${CMTXTIME }</td>
		<td>${CMDATE }</td>
		<td>${OUTACN}</td>
		<td>${TaxTypeText}</td>
		<td>${INTSACN1}</td>
		<td>${INTSACN2}</td>
		<td>${INTSACN3}</td>
		<td>${PAYDUED}</td>
		<td style="text-align: right"><spring:message code= "LB.NTD" />${AMOUNT}<spring:message code= "LB.Dollar" /></td>
		<td>${CMTRMEMO}</td>
	</tr>
</table>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.Pay_Taxes_P1_D3" /></li>
			<li><spring:message code="LB.Pay_Taxes_P1_D4" /></li>
			<li><spring:message code="LB.Pay_Taxes_P1_D5" /></li>
			<li><spring:message code="LB.Pay_Taxes_P3_D4" /></li>
			<li><spring:message code="LB.Pay_Taxes_P3_D5" /></li>
		</ol>
	</div>
</body>
</html>