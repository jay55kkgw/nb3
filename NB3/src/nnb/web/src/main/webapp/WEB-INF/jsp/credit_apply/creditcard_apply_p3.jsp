<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript">
$(document).ready(function(){
	if(${result_data.data.requestParam.CARDMEMO == "1"}){
		$("#NA034DIV").show();
	}
	if(${result_data.data.requestParam.CARDMEMO == "2"}){
		$("#NA035DIV").show();
	}
	$("#formId").validationEngine({
		binded: false,
		promptPosition: "inline"
	});	
	$("#CMSUBMIT").click(function(e){
		
		e = e || window.event;
		if(!$('#formId').validationEngine('validate')){
    		e.preventDefault();
    	}
		else{
			if(${result_data.data.requestParam.smartcard1 == "true"}){
				$("#smartcard1").val("Y");
			}
			if(${result_data.data.requestParam.smartcard2 == "true"}){
				$("#smartcard2").val("Y");
			}
			$("#formId").submit();
		}
		
		
// 		if($("#allCheckBox").prop("checked") == false){
// 			alert("<spring:message code= "LB.D1064" />");
// 			return false;
// 		}
		
// 		if($("input[type=radio][name=OLAGREEN1][value=1]").prop("checked") == false){
// 			alert("<spring:message code= "LB.Alert033" />");
// 			return false;
// 		}
// 		if($("input[type=radio][name=OLAGREEN2]").prop("checked") == false){
// 			alert("<spring:message code= "LB.Alert034" />");
// 			return false;
// 		}
// 		if($("input[type=radio][name=OLAGREEN3][value=1]").prop("checked") == false){
// 			alert("<spring:message code= "LB.Alert035" />");
// 			return false;
// 		}
// 		if(${result_data.data.requestParam.CARDMEMO == "1"}){
// 			if($("input[type=radio][name=OLAGREEN4][value=1]").prop("checked") == false){
// 				alert("<spring:message code= "LB.Alert036" />");
// 		   		return false;
// 			}
// 		}
// 		if(${result_data.data.requestParam.CARDMEMO == "2"}){
// 			if($("input[type=radio][name=OLAGREEN5][value=1]").prop("checked") == false){
// 				alert("<spring:message code= "LB.Alert037" />");
// 		   		return false;
// 			}
// 		}
	});
	$("#backButton").click(function(){
		$("#formId").validationEngine('detach');
		$("#back").val("Y");
// 		$("#formId").attr("action","${__ctx}/CREDIT/APPLY/apply_creditcard_p3_notagree");
		$("#formId").attr("action","${__ctx}/CREDIT/APPLY/apply_creditcard_p2");
		$("#formId").submit();
	});
});

function checkitemAll(){
	if($("#allCheckBox").prop("checked") == true){
		$('#CMSUBMIT').removeAttr("disabled");
		$('#CMSUBMIT').removeClass('btn-flat-gray');
		$('#CMSUBMIT').addClass('btn-flat-orange');
		
		$("#OLAGREEN1").val("1");
		$("#OLAGREEN2").val("1");
		$("#OLAGREEN3").val("1");
		if(${result_data.data.requestParam.CARDMEMO == "1"}){
			$("#OLAGREEN4").val("1");
		}
// 		else{
// 			$("#OLAGREEN4")..removeAttr('value');
// 		}
		if(${result_data.data.requestParam.CARDMEMO == "2"}){
			$("#OLAGREEN5").val("1");
		}
// 		$("input[type=radio][name=OLAGREEN1][value=1]").prop("checked",true);
// 		$("input[type=radio][name=OLAGREEN2]").prop("checked",true);
// 		$("input[type=radio][name=OLAGREEN3][value=1]").prop("checked",true);
// 		$("input[type=radio][name=OLAGREEN4][value=1]").prop("checked",true);
// 		$("input[type=radio][name=OLAGREEN5][value=1]").prop("checked",true);
		
// 		$("input[type=radio][name=OLAGREEN1][value=2]").prop("checked",false);
// 		$("input[type=radio][name=OLAGREEN3][value=2]").prop("checked",false);
// 		$("input[type=radio][name=OLAGREEN4][value=2]").prop("checked",false);
// 		$("input[type=radio][name=OLAGREEN5][value=2]").prop("checked",false);
	}
	else{
		$('#CMSUBMIT').attr("disabled",true);
		$('#CMSUBMIT').removeClass('btn-flat-orange');
		$('#CMSUBMIT').addClass('btn-flat-gray');
		
		$("#OLAGREEN1").val("2");
		$("#OLAGREEN2").val("2");
		$("#OLAGREEN3").val("2");
		if(${result_data.data.requestParam.CARDMEMO == "1"}){
			$("#OLAGREEN4").val("2");
		}
		if(${result_data.data.requestParam.CARDMEMO == "2"}){
			$("#OLAGREEN5").val("2");
		}
	}
}
</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 申請信用卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0666" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
<%-- 		<%@ include file="../index/menu.jsp"%> --%>
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--線上申請信用卡 -->
				<h2><spring:message code="LB.D0022" /></h2>
				<div id="step-bar">
                    <ul>
                    	<li class="finished"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
                        <li class="active"><spring:message code="LB.X1966" /></li><!-- 信用卡與權益 -->
                        <li class=""><spring:message code="LB.X1967" /></li><!-- 申請資料 -->
                        <li class=""><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
                        <li class=""><spring:message code="LB.X1968" /></li><!-- 完成申請 -->
                    </ul>
                </div>
				<form method="post" id="formId" action="${__ctx}/CREDIT/APPLY/apply_creditcard_p4">
					<input type="hidden" name="ADOPID" value="NA03"/>
					<!-- Avoid Reflected XSS All Clients -->
<%-- 			    <input type="hidden" name="CUSIDN" value="${result_data.data.requestParam.CUSIDN}"/> --%>
                    <input type="hidden" name="CUSIDN" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CUSIDN)}' />"/>
<%--                <input type="hidden" name="CFU2" value="${result_data.data.requestParam.CFU2}"/> --%>
                     <input type="hidden" name="CFU2" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CFU2)}' />"/>
<%-- 				<input type="hidden" name="FGTXWAY" value="${result_data.data.requestParam.FGTXWAY}"/> --%>
				  	<input type="hidden" name="FGTXWAY" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.FGTXWAY)}' />"/>
<%-- 				<input type="hidden" name="CARDNAME" value="${result_data.data.requestParam.CARDNAME}"/> --%>
				  	<input type="hidden" name="CARDNAME" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDNAME)}' />"/>
<%-- 				<input type="hidden" name="VARSTR2" value="${result_data.data.requestParam.VARSTR2}"/> --%>
				  	<input type="hidden" name="VARSTR2" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.VARSTR2)}' />"/>
<%-- 				<input type="hidden" name="oldcardowner" value="${result_data.data.requestParam.oldcardowner}"/> --%>
				  	<input type="hidden" name="oldcardowner" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.oldcardowner)}' />"/>
<%-- 				<input type="hidden" name="CN" value="${result_data.data.requestParam.CN}"/>				 --%>
					<input type="hidden" name="CN" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CN)}' />"/>
<%-- 				<input type="hidden" name="CARDMEMO" value="${result_data.data.requestParam.CARDMEMO}"/> --%>
					<input type="hidden" name="CARDMEMO" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDMEMO)}' />"/>
<%-- 				<input type="hidden" name="DPMYEMAIL" value="${result_data.data.requestParam.DPMYEMAIL}"/> --%>
					<input type="hidden" name="DPMYEMAIL" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.DPMYEMAIL)}' />"/>
					<input type="hidden" id="smartcard1" name="smartcard1"/>
					<input type="hidden" id="smartcard2" name="smartcard2"/>
					<input type="hidden" id="OLAGREEN1" name="OLAGREEN1"/>
					<input type="hidden" id="OLAGREEN2" name="OLAGREEN2"/>
					<input type="hidden" id="OLAGREEN3" name="OLAGREEN3"/>
					<input type="hidden" id="OLAGREEN4" name="OLAGREEN4"/>
					<input type="hidden" id="OLAGREEN5" name="OLAGREEN5"/>
					<input type="hidden" name="back" id="back" />
<%-- 				<input type="hidden" name="CPRIMBIRTHDAY" value="${result_data.data.requestParam.CPRIMBIRTHDAY}"/> --%>
					<input type="hidden" name="CPRIMBIRTHDAY" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CPRIMBIRTHDAY)}' />"/>
<%-- 				<input type="hidden" name="CPRIMBIRTHDAYshow" value="${result_data.data.requestParam.CPRIMBIRTHDAYshow}"/> --%>
					<input type="hidden" name="CPRIMBIRTHDAYshow" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CPRIMBIRTHDAYshow)}' />"/>
					<input type="hidden" name="QRCODE" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.QRCODE)}' />"/>
					<input type="hidden" name="branch" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.branch)}' />"/>
					<input type="hidden" name="memberId" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.memberId)}' />"/>
					<input type="hidden" name="partition" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.partition)}' />"/>
					<input type="hidden" name="BRANCH" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.BRANCH)}' />"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 terms-block">
							<div class="ttb-message">
	                            <p><spring:message code="LB.X1966" /></p>
	                        </div>
	                        <p class="form-description"><spring:message code="LB.X1979" /></p><!-- 請您審閱以下顧客權益與說明 -->
	                        <div class="text-left">
	                            <ul class="ttb-result-list terms">
	                                <li data-num="">
	                                    <span class="input-subtitle subtitle-color"><spring:message code="LB.D0039" /></span><!-- 申請人聲明及同意事項 -->
	                                    <div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
	                                    	<%@ include file="../term/NA03_1.jsp"%>
	                                    </div>
	                                </li>
	                                <li data-num="">
	                                    <span class="input-subtitle subtitle-color"><spring:message code="LB.X1980" /></span><!-- 個人資料保護法告知事項 -->
	                                    <div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
	                                    	<%@ include file="../term/NA03_2.jsp"%>
	                                    </div>
	                                </li>
									<li data-num="">
	                                    <span class="input-subtitle subtitle-color"><spring:message code="LB.D0044" /></span><!-- 臺灣中小企業銀行信用卡約定條款 -->
	                                    <div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
	                                    	<%@ include file="../term/NA03_3.jsp"%>
	                                    </div>
	                                </li>
									<li data-num="" id="NA034DIV" style="display:none">
	                                    <span class="input-subtitle subtitle-color"><spring:message code="LB.D0045" /></span><!-- 臺灣企銀悠遊聯名卡特別約定條款 -->
	                                    <div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
	                                    	<%@ include file="../term/NA03_4.jsp"%>
	                                    </div>
	                                </li>
									<li data-num="" id="NA035DIV" style="display:none">
	                                    <span class="input-subtitle subtitle-color"><spring:message code="LB.X1648" /></span><!-- 臺灣企銀一卡通聯名卡特別約定條款 -->
	                                    <div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
	                                    	<%@ include file="../term/NA03_5.jsp"%>
	                                    </div>
	                                </li>
								</ul>
	                            <span class="input-block">
	                                <div class="ttb-input">
	                                	<!-- 本人已詳細閱讀並清楚以上所有內容。 -->
	                                    <label class="check-block"><spring:message code="LB.D1064" />。
	                                        <input type="checkbox" name="allCheckBox" id="allCheckBox" onclick="checkitemAll()" class="validate[funcCallRequired[validate_chkClickboxKind[<spring:message code= "LB.D1064" />,1,allCheckBox]]]">
	                                        <span class="ttb-check"></span>
	                                    </label>
	                                </div>
	                            </span>
	                        </div>
  							<input type="button" id="backButton" value="<spring:message code="LB.X0318" />" class="ttb-button btn-flat-gray"/><!-- 上一步 -->
  							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.W1554" />" class="ttb-button btn-flat-gray" disabled/><!-- 同意並繼續 -->
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>