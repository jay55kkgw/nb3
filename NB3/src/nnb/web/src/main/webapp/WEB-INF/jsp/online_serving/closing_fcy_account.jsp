<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		// 確認鍵 click
		processQuery();
	}
	
		// 確認鍵 click
	function processQuery()
	{
			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").submit();
				}
			});
	}
	function checkReadFlag()
    {
    	console.log($("#ReadFlag").prop('checked'));
        if ($("#ReadFlag").prop('checked'))    		
      	{
       		$("#CMSUBMIT").prop('disabled',false);
       		$("#CMSUBMIT").addClass("btn-flat-orange");
      	}
      	else
      	{
       		$("#CMSUBMIT").prop('disabled',true);
       		$("#CMSUBMIT").removeClass("btn-flat-orange");
   	  	}	
    }
	function BoradSwitch1()
	{
		$("#prodshow1").show();
		$("#prodshow2").hide();
	}

	 function BoradSwitch2()
	{
		$("#prodshow1").hide();
		$("#prodshow2").show();
		
	}  

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	<!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 外匯存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0299" /></li>
		</ol>

	</nav>
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D0446" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form autocomplete="off" method="post" id="formId" action="${__ctx}/ACCOUNT/CLOSING/closing_fcy_account_step1">
               
                <div class="main-content-block row">
                    <div class="col-12 tab-content"  id="prodshow1">
                        <div class="ttb-message">
							<p style='line-height:22.0pt'><b><span style='font-size:13.0pt;font-family:新細明體'><spring:message code= "LB.X0152" />：</span></b></p>
							<p style='line-height:22.0pt'><b><span style='font-size:13.0pt;font-family:新細明體'><spring:message code= "LB.X1277" /></span></b></p><!-- 歡迎您使用臺灣企銀線上結清銷戶服務，請您在辦理本行線上結清銷戶服務前詳讀下列告知事項。 -->
                        </div>
                        <ul class="ttb-result-list" style="list-style: decimal; list-style-position: inside;">
							<li class="full-list"><spring:message code= "LB.X1258" /></li><!-- 使用本服務前請先備妥「臺灣企銀晶片金融卡+讀卡機」或電子簽章(憑證載具) -->
							<li class="full-list"><spring:message code= "LB.X1722" /></li><!-- 本結清銷戶服務提供對象為開立於本行之外匯活期性存款帳戶(輕鬆理財不適用本服務)。 -->
							<li class="full-list"><spring:message code= "LB.X1721" /></li><!-- 帳戶有定期存款未解約者不適用本服務。 -->
							<li class="full-list"><spring:message code= "LB.X1280" /></li><!-- 結清之餘額以不超過新臺幣伍萬元為限，限匯入申請人開立於本行之其他新臺幣存款或外匯存款帳戶。 -->
							<li class="full-list"><spring:message code= "LB.X1281" /></li><!-- 本項結清銷戶服務交易成功後，將自動註銷結清帳戶之網路銀行功能。 -->
							<li class="full-list"><spring:message code= "LB.X1263" /></li><!-- 使用本服務前請確認代收票據是否已全部入帳。 -->
							<li class="full-list"><spring:message code= "LB.X1264" /></li><!-- 結清銷戶基準日係以銀行電腦系統完成銷戶當日為準。 -->
							<li class="full-list"><spring:message code= "LB.X1284" /></li><!-- 結清銷戶交易(含所有幣別)成功後，該帳戶之存摺即視為作廢失效。 -->
                        </ul>
                      		<input class="ttb-button btn-flat-orange" type="button" name="CMOK" id="CMOK" value="<spring:message code="LB.Confirm_1"/>" onClick='BoradSwitch2(true);'/>
                   		</div>
					<!-- -->
					<div  class="col-12 tab-content" id="prodshow2" style="display:none" >
                        <div class="ttb-message">
                        	<p><spring:message code= "LB.X1285" /></p>
						</div>
							<ul class="ttb-result-list terms">
<%-- 								<li class="full-list"><spring:message code= "LB.X1285" /></li><!-- 外匯存款帳戶線上結清銷戶之相關約定條款 --> --%>
								<li data-num="一、"><span><spring:message code= "LB.X1286" /></span></li><!-- 立約人茲向貴行申請線上結清存款帳戶，並同意一併註銷/終止該帳戶結清前各項往來服務項目，且遵守貴行有關之業務規定。 -->
								<li data-num="二、"><span><spring:message code= "LB.X1268" /></span></li><!-- 立約人同意存摺上存提交易明細或結存餘額或立約人查詢所得之餘額如與貴行帳載資料不符時，以貴行帳載之金額為準。但經核對貴行提出之交易紀錄，確為貴行記載錯誤，並經貴行查證屬實者，貴行應即更正之。 -->
								<li data-num="三、"><span><spring:message code= "LB.X1288" /></span></li><!-- 立約人同意貴行依指示將結存餘額匯入立約人開立於本行之其他新臺幣存款或外匯存款帳戶成功後，該帳戶始能結清。 -->
								<li data-num="四、"><span><spring:message code= "LB.X1270" /></span></li><!-- 結清銷戶基準日係以貴行電腦系統完成銷戶日為準。 -->
								<li data-num="五、"><span><spring:message code= "LB.X1271" /></span></li><!-- 立約人同意結清銷戶交易成功後，該帳戶之存摺即視為已作廢失效。 -->
							</ul>
						<p align=center style='text-align:center;line-height:22.0pt'><span style='font-size:13.0pt;font-family:新細明體'>
							<label class="check-block">
								<input type="checkbox" name="ReadFlag" id="ReadFlag" onclick="checkReadFlag()"> 
								<spring:message code= "LB.X1272" />
								<span class="ttb-check"></span>
							</label>
						</p><!-- 立約人已詳細審閱並充分了解上述約款內容，並上述約定。 -->
						<input class="ttb-button btn-flat-gray	" type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.D0041" />" onClick='BoradSwitch1(true);'/>
						<input type="button" id="CMSUBMIT" value="<spring:message code="LB.D0097"/>" class="ttb-button" disabled/>
                    
					</div>
			
                </div>
                </form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>