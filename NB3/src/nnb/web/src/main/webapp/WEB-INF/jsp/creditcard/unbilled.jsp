<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    
</head>
<script type="text/javascript">
	$(document).ready(function () {
		
		if('${card_unbilled.data.n810MsgCode}'=='E091'||'${card_unbilled.data.n810MsgCode}'!=''){
			$('#RAD2').trigger('click');
		}
		//表單驗證
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});
		$("#CMSUBMIT").click(function(e){
			console.log("submit~~");
			
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
			} else {
				initBlockUI();
				$("#formId").validationEngine('detach');
				if($('#QueryType1').prop("checked")==true){
					$('#formId').attr("action","${__ctx}/CREDIT/INQUIRY/card_unbilled_email")
				}
				$("form").submit();
			}
		});
		disableFunction();
		autoclickRAD2();
	});
	
	
	function showVISACardNo(id){
		if (id=="RAD1"){
			$("#hid").hide();
		}
		if (id=='RAD2'){
			$("#hid").show();
		}
	}
	
	function autoclickRAD2(){
		if($('#RAD2').is('checked')){
			$('#RAD2').trigger("click");
		}
	}
	
	function disableFunction(){
		//有一般卡但有錯誤訊息
		if('${card_unbilled.data.n810MsgCode}'!='E091' && '${card_unbilled.data.n810MsgCode}'!=''){
			$('#RAD1').removeAttr("checked");
			$('#RAD1').attr("disabled",true);
			//只有一般卡的情況,把確認鈕鎖起來
			if($('#RAD2').length==0){
				$('#CMSUBMIT').removeClass("ttb-button btn-flat-orange");
				$('#CMSUBMIT').addClass("ttb-button btn-flat-gray");
				$('#CMSUBMIT').attr("disabled",true);
			}
		}
		//有VISA卡但有錯誤訊息
		if('${card_unbilled.data.n813MsgCode}'!='E091' && '${card_unbilled.data.n813MsgCode}'!=''){
			$('#RAD2').removeAttr("checked");
			$('#RAD2').attr("disabled",true);
			//只有VISA的情況,把確認鈕鎖起來
			if($('#RAD1').length==0){
				$('#CMSUBMIT').removeClass("ttb-button btn-flat-orange");
				$('#CMSUBMIT').addClass("ttb-button btn-flat-gray");
				$('#CMSUBMIT').attr("disabled",true);
			}
		}
		//有一般卡及VISA卡且兩個都有錯誤訊息  把確認鈕鎖起來
		if(('${card_unbilled.data.n810MsgCode}'!='E091' && '${card_unbilled.data.n810MsgCode}'!='') 
				&& ('${card_unbilled.data.n813MsgCode}'!='E091' && '${card_unbilled.data.n813MsgCode}'!='')){
			$('#CMSUBMIT').removeClass("ttb-button btn-flat-orange");
			$('#CMSUBMIT').addClass("ttb-button btn-flat-gray");
			$('#CMSUBMIT').attr("disabled",true);
		}
		
	}
	
	// 驗證帳號下拉選單
	function validate_CARDNUM(field, rules, i, options) {
		var inputAttr = rules[i + 2];
		console.log("inputAttr>>" + inputAttr);
		var dpagacno_val = $("#" + inputAttr).find(":selected").val()
		console.log("funccall test");
		console.log("funccall test>>" + options.allrules.required.alertText);
		if (fstop.isEmptyString(dpagacno_val) || dpagacno_val.indexOf('#') > -1) {
			return options.allrules.required.alertText
		}
	}
	
</script>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
	<!-- 未出帳單明細查詢 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card_Un-billed_Transactions" /></li>
		</ol>
	</nav>

	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	
	
	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<!-- 繳款紀錄查詢 -->
			<h2><spring:message code= "LB.X1418" /></h2>
			
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/CREDIT/INQUIRY/card_unbilled_result">
			<input type="hidden" name="display_CARDNUM" value="">
   			<input type="hidden" name="H_CARDTYPE" value="">
				<div class="main-content-block row">
					<!-- 主頁內容  -->
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
							<p><spring:message code= "LB.X1418" /></p><!-- 信用卡未出帳單交易明細查詢 -->
						</div>
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Inquiry_time" /></h4></label><!-- 查詢時間 -->
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<p>${card_unbilled.data.CMQTIME}</p>
								</div>
							</span>
						</div>
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Card_type" /></h4></label><!-- 卡別 -->
							</span> 
							<c:if test="${card_unbilled.data.n810MsgCode != 'E091'}">
							<span class="input-block">
								<div class="ttb-input">
								<!-- 顯示一般卡  -->
								
										<label class="radio-block"><spring:message code="LB.Bank_card" />
											<input type="radio" name="CARDTYPE1" value="0" id="RAD1" onclick="showVISACardNo(this.id)" checked/>
											<span class="ttb-radio"></span>
										</label>
										<c:if test="${card_unbilled.data.n810MsgCode != 'E091' && card_unbilled.data.n810MsgCode !=''}">
											<span class="input-unit">
											<font color=red>
											(<spring:message code="LB.ErrMsg" />：&nbsp;${card_unbilled.data.n810MsgCode}&nbsp;${card_unbilled.data.N810Message})
											</font>
											</span>
										</c:if>
									
							</span>
							</c:if>
							<c:if test="${card_unbilled.data.n813MsgCode != 'E091'}">
							<br><br>
							<span class="input-block">
								<div class="ttb-input">
								<!-- 顯示VISA金融卡  -->
									
										<c:choose>
										<c:when test="${card_unbilled.data.n810MsgCode=='E091'||card_unbilled.data.n810MsgCode !=''}">
										<label class="radio-block"><spring:message code= "LB.VISA_debit_card" />
											<input type="radio" name="CARDTYPE1" value="1" id="RAD2" onclick="showVISACardNo(this.id)" checked/>
											<span class="ttb-radio"></span>
										</label>
										</c:when>
										<c:when test="${card_unbilled.data.n810MsgCode==''}">
										<label class="radio-block"><spring:message code= "LB.VISA_debit_card" />
											<input type="radio" name="CARDTYPE1" value="1" id="RAD2" onclick="showVISACardNo(this.id)" />
											<span class="ttb-radio"></span>
										</label>
										</c:when>
										</c:choose>
										<c:if test="${card_unbilled.data.n813MsgCode != 'E091' && card_unbilled.data.n813MsgCode != ''}">
											<span class="input-unit">
											<font color="red">
											(<spring:message code="LB.ErrMsg" />：&nbsp;${card_unbilled.data.n813MsgCode}&nbsp;${card_unbilled.data.N813Message})
											</font>
											</span>
										</c:if>
									
									<br>
									<span id="hid" class="ttb-input" style="display:none">
									<br>
									<spring:message code="LB.Card_number" />：
									
								    <select name="CARDNUM" id="CARDNUM" class="select-input validate[required,funcCall[validate_CARDNUM[CARDNUM]]">
					                		<!-- 預設值  -->
						    				<option value="#">------<spring:message code="LB.Select_account" />------</option> <!-- ------ 請選擇(信用卡)帳號 ------ -->
						    			
						    				<!-- 卡號_List迴圈  -->
						    				<c:set var="count" value="0" scope="page" />
					                		<c:forEach var="cardnumList" items="${card_unbilled.data.N813REC}">
					                				<option value="${cardnumList.VALUE}"> ${cardnumList.CARDNUM} </option>
					                				<c:set var="count" value="${count + 1}" scope="page"/>
					                		</c:forEach>
					                		<!-- 若卡號不只一個則新增 [全部]選項  -->
					                		<c:if test="${count > 1}">
					                			<option value="ALL">------ <spring:message code="LB.All" /> ------</option><!-- 全部＊ -->
					                		</c:if>
						    			</select>
									</span>
							</span>
							</c:if>
						</div>
						</div>
						
						<c:if test="${card_unbilled.data.n810MsgCode != 'E091' && card_unbilled.data.n813MsgCode != 'E091'}">
						</div>
						<br>
						</c:if>
					<div class="ttb-input-item row">
						<span class="input-title"> <label><h4><spring:message code= "LB.Inquiry_type" /></h4></label></span> <span
							class="input-block">
							<div class="ttb-input">
								<label class="radio-block"><spring:message code= "LB.Display_as_web_page" /> <input type="radio"
									name="QueryType" value="0" id="QueryType0" checked /> <span
									class="ttb-radio"></span>
								</label>
							</div>
							<div class="ttb-input">
								 <label class="radio-block"><spring:message code= "LB.e-mail" />： <input type="radio" name="QueryType" value="1" id="QueryType1" />
									<input class="text-input" type="text" size="25" name="CMMAIL" value="${sessionScope.dpmyemail}">
								<span class="ttb-radio" style="top:10px;"></span>
								</label>
							</div>
					</div>
				</div>
						<input id="CMRESET" name="CMRESET" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" /> 
						<input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
					</div>
				</div>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
					<!-- 正卡持卡人歸戶名下所有信用卡(含附卡)之交易明細。  -->
					<li><spring:message code= "LB.Unbilled_P1_D1" /></li>
				</ol>
			</div>
			</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
