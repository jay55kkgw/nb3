<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 回上一頁填入資料
			refillData();
			// 檢查 checkbox 是否全勾選，決定是否可按下一步
			checkReadFlag();
		}
		
		// 回上一頁填入資料
		function refillData() {
			var jsondata = '${previousdata}';
			console.log(jsondata);
			
			if (jsondata != "") {
				JSON.parse(jsondata, function(key, value) {
					if(key!='' && value == 'Y') {
						var obj = $("#"+key);
						$("#"+key).prop('checked', true);
					}
				});
			}
		}
		
		// 檢查 checkbox 是否全勾選，決定是否可按下一步
		function checkReadFlag() {
			var result = true;
			$("input[type='checkbox'][name='check_agree']").each( function () {
				if(!$(this).prop('checked')) {
					// 未勾選
					result = false;
				}
			});
			if ($("input[type='checkbox'][id='Q1']").prop('checked') == false) {
				result = false;
			}
			
			if(result) {
				// 可點擊
				$("#NEXT").attr("class", "ttb-button btn-flat-orange");
				$("#NEXT").attr("disabled", false);
				
			} else {
				// 不可點擊
				$("#NEXT").attr("class", "ttb-button btn-flat-gray");
				$("#NEXT").attr("disabled", true);
			}
		}
		
		// 通過表單驗證準備送出
		function processQuery() {
			if($("#CITY").val() == "#"){
				console.log("CITY NOT SELECTED");
				errorBlock(
						null, 
						null,
						["請選擇出生地"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
			}
			
			// 表單驗證
			if ( !$('#formId').validationEngine('validate') ) {
				e.preventDefault();
			} else {
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				
				// 遮罩
	         	initBlockUI();
				
				// 檢查KYC可填寫次數
	         	var cusidn = '${CUSIDN}';
	         	var kycCanDo = getKycCanDo(cusidn);
	         	if(kycCanDo == false)
				{
					//alert("<spring:message code= "LB.Alert093" />");
					errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert093' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
					unBlockUI(initBlockId)
					return false;
				}
				
	            $("#formId").submit();
			}
		}
		
		// 判斷可否填寫KYC
		function getKycCanDo(cusidn) {
			var uri = '${__ctx}' + "/ONLINE/APPLY/getKycCanDo_aj"
			console.log("getKycCanDo >>" + uri);
			
			rdata = { CUSIDN: cusidn };
			console.log("rdata >> " + rdata);
			
			bs = fstop.getServerDataEx(uri, rdata, false);
			console.log("data >> ", bs);
			
			if(bs != null && bs.result == true) {
				return bs.data.KycCanDo;
			}
		}
		
		function check(input) {
			for(var i = 0; i < document.form1.yesorno.length; i++) {
		      document.form1.yesorno[i].checked = false;
		    }
		    
		    input.checked = true;
		    return true;
		}
		
		function citizen(input) {
			if(input == 'Y') {
				document.getElementById('WARN').style.display = 'none';
				document.getElementById('DATA').style.display = 'block';
			} else {
				document.getElementById('WARN').style.display = 'block';
				document.getElementById('DATA').style.display = 'none';
				
				$("input[type='checkbox'][name='check_agree']").each( function () {
					$(this).prop('checked',false);
				});
			}
			checkReadFlag();
		}
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	
	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item active" aria-current="page">FATCA及CRS個人客戶自我聲明書暨個人資料同意書</li>
		</ol>
	</nav>
	
	<div class="content row">
		<c:if test="${isLogin == 'Y'}">
			<!-- menu、登出窗格 -->
			<%@ include file="../index/menu.jsp"%>
		</c:if>
		
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>FATCA及CRS個人客戶自我聲明書暨個人資料同意書</h2>
				<div id="step-bar">
					<ul>
						<li class="finished">注意事項與權益</li>
						<li class="active">FATCA個人客戶身份識別聲明</li>
						<li class="">投資屬性調查</li>
						<li class="">開戶資料</li>
						<li class="">確認資料</li>
						<li class="">完成申請</li>
					</ul>
				</div>
				
				<form method="post" id="formId" name="form1" action="${__ctx}${next}">
					<input type="hidden" id="FATCA_CONSENT" name="FATCA_CONSENT" value="Y">
				
					<div class="main-content-block row">
						<div class="col-12 terms-block">
							<div class="ttb-message">
								<p>FATCA及CRS個人客戶自我聲明書暨個人資料同意書
									<br>FATCA and CRS Individual Self-Certification & Personal information Consent Form
								</p>
							</div>
							<p class="form-description">
									本人僅為台灣之稅務居民
									<br>
									I am only a Taiwan tax resident
								</p>
							<div class="text-left">
								<ul class="questionnaire-list">
									<li>
											<div class="ttb-input">
												<label class="check-block">
													<input type="checkbox" value="Y" id="Q1" name="yesorno" onclick="check(this); citizen('Y');">是
													<span class="ttb-check"></span>
												</label>

												<label class="check-block">
													<input type="checkbox" value="N" id="Q2" name="yesorno" onclick="check(this); citizen('N');">否
													<span class="ttb-check"></span>
												</label>
											</div>
									</li>

								</ul>
								<strong id="WARN" name="WARN" style="color: red; font-size: x-large; display: none">親愛的貴賓您好，因您的身分是非屬本國單一國籍自然人，故無法受理開戶，造成不便敬請見諒。</strong>
								<div id="DATA" name="DATA">
								<p class="form-description">
									帳戶持有人基本資料 
									<br>
									Account Holder Information
								</p>

								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>A. 身分證號碼/統一證號/護照號碼 ID / Uniform ID Number/ Passport No.:</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<label class="radio-block">
													<input type="radio" value="A" name="ACCOUNT_INFO" checked disabled>身分證號碼 ID
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" name="ACCOUNT_INFO" disabled>統一證號 Uniform ID Number
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" name="ACCOUNT_INFO" disabled>護照號碼 Passport No.
													<span class="ttb-radio"></span>
												</label>
											</p>
											</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<input name="A" class="text-input" type="text" size="7" maxlength="10" value="${CUSIDN}" disabled="disabled">
											</p>
										</div>
									</li>
								</ul>
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>B. 出生地 Place of Birth:</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												城市 City:&nbsp;
												<select class="custom-select select-input half-input " style="width:auto; text-align:center" name="CITY" id="CITY">
	                                            	<option value="#">---<spring:message code= "LB.Select" />---</option>
	                                            	<c:forEach var="city" items="${cityList}">
														<option value="${city.CITYCODE}">${city.CITYNAME}</option>
													</c:forEach>
                                        		</select>
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
												<spring:message code="LB.W0198" /> Country:&nbsp; 
												<input id="COUNTRY" name="COUNTRY" type="text" style="width: 90px; text-align: center;"class="text-input" size="7" maxlength="5" value="臺灣" disabled="disabled"> 
											</div>											
										</div>
									</li>
								</ul>
								<p class="form-description">聲明
									<br>Declarations and Signature
								</p>
								<p class="form-description"></p>
								<div class="text-left">
									<label class="check-block">我同意簽署此份文件
										<input type="checkbox" id="check1" name="check_agree" value="Y" onclick="checkReadFlag()">
										<span class="ttb-check"></span>
									</label>
									<ul class="terms">
										<li data-num="">
											<textarea id="Textarea1" rows="4" style="width:90%;margin-top: 1rem;margin-left: 1.7rem;font-size: 0.875rem;color: #666;border-color: #ccc;">本人聲明本自我聲明書暨個人資料同意書之內容均屬真實正確及完整。倘爾後有情事變更致使本自我聲明書暨個人資料 同意書之內容已不正確，本人承諾於變更之日起 30 天內通知 貴公司，並承諾提供更新後之自我聲明書暨個人資料同意 書予 貴公司。本自我聲明書暨個人資料同意書除美國海外帳戶稅收遵從法案(ForeignAccountTaxComplianceAct，以 下簡稱「FATCA 法案」)等相關法令外，應以臺灣之法令為準據法。倘開戶申請書之內容與本自我聲明書暨個人資料同 意書有衝突時，以本自我聲明書暨個人資料同意書為準。I hereby certify that all statements made in this self-certification and personal information consent form are true, correct and complete. I undertake to notify Taiwan Business Bank, (the “Bank”) promptly of any change in circumstances which causes the information contained herein to become incorrect and to provide the Bank with an updated self-certification and personal information consent form within 30 days of such change in circumstances. In addition to the U.S. Foreign Account Tax Compliance Act (hereinafter referred to as “FATCA”) and its related laws and regulations, the governing law of this self-certification and personal information consent form shall be the laws of Taiwan. In the event of any discrepancy between the account opening form and this self-certification and personal information consent form, this self-certification and personal information consent form shall prevail.另本人於 貴公司開立帳戶並進行交易，為符合個人資料保護法下個人資料之合理使用，並配合 貴公司遵循 FATCA 法案、駐美國台北經濟文化代表處與美國在台協會合作促進外國帳戶稅收遵從法執行協定等相關規定，本人確認已收受 並充分瞭解 貴公司所提供之遵循 FATCA 法案蒐集、處理及利用個人資料告知事項(下簡稱「告知事項」)之全部內容， 並同意 貴公司依據告知事項所載內容，對本人相關個人資料為蒐集、處理及利用:Whereas I intend to establish an account and to proceed transactions with Taiwan Business Bank, (the “Bank”), and whereas it is regulated to comply with the proper use of personal information stipulated by the Personal Information Protection Act, and it is necessary to cooperate with the Bank to comply with FATCA, Agreement between the American institute in Taiwan and Taipei Economic And Cultural Representative Office In The United States and the related regulations. In witness thereof, I hereby confirm that I have duly received and understood all the content of the Notice for the Collection, Processing and Use of Personal Information for FATCA Compliance (the ”Notice”) as provided by the Bank, and I agree that the Bank may collect, process and use the personal information of the Customer in accordance with the Notice.
				              </textarea>
										</li>
									</ul>
									<p class="form-description"></p>
									<label class="check-block">我瞭解已充分詳讀 FATCA 法案蒐集、處理及利用個人資料告知事項
										<input type="checkbox" id="check2" name="check_agree" value="Y" onclick="checkReadFlag()">
										<span class="ttb-check"></span>
									</label>
									<ul class="terms">
										<li data-num="">
											<textarea id="Textarea1" rows="4" style="width:90%;margin-top: 1rem;margin-left: 1.7rem;font-size: 0.875rem;color: #666;border-color: #ccc;">緣臺灣中小企業銀行股份有限公司(下簡稱「本公司」)因參與遵循美國海外帳戶稅收遵從法案(Foreign Account Tax Compliance Act，下簡稱「FATCA 法案」)，及駐美國台北經濟文化代表處與美國在台協會合作促進外國帳戶稅收遵從法 執行協定(下稱「IGA 協議」)，而負辨識美國帳戶之義務，現因 台端於本公司開立帳戶及進行交易，為符合個人資料 保護法下個人資料之合理使用，本公司茲請求 台端協力遵循 FATCA 法案及 IGA 協議之相關規定，特告知下列事項: In compliance with the U.S. Foreign Account Tax Compliance Act (hereinafter referred to as “FATCA”) and Agreement between the American institute in Taiwan and Taipei Economic And Cultural Representative Office In The United States (hereinafter referred to as the “IGA”), Taiwan Business Bank Co., Ltd. (hereinafter referred to as the “TBB”) has the obligation to identify U.S. accounts. In order to comply with the proper use of personal information in accordance with the Personal Information Protection Act for accounts you establish and transactions proceed with TBB, TBB hereby requests your cooperation with the compliance of FATCA and the relevant provisions under the Agreement, with notice as follows:
一、個人資料蒐集、處理及利用之目的及類別 Purpose and Type of Collection, Processing and Use of Personal Information 為辨識本公司內所有帳戶持有者之身分，並於必要時申報具有美國帳戶之持有者資訊予美國國稅局及臺灣權責主管機 關，經 台端提供之相關個人資料及留存於本公司之一切交易資訊，包括但不限於姓名、出生地及出生日期、國籍、戶 籍地址、住址及工作地址、電話號碼、美國稅籍編號、帳戶帳號及帳戶餘額、帳戶總收益金額與交易明細等，將因本公 司遵循 FATCA 法案及 IGA 協議之需要，由本公司蒐集、處理及利用。
In order to identify the account holders of TBB and to report accounts held by U.S. persons to the Internal Revenue Service (hereinafter referred to as “IRS”) and the competent authority in Taiwan R.O.C., all personal information provided by you and all transaction information kept by TBB, including but not limited to name, place of birth, date of birth, nationality, domicile address, residence address and work location, telephone number, U.S. tax identifying number (TIN), account number and account balance, the gross proceeds and statement of the account shall be collected, processed and used by TBB for the purpose of FATCA compliance and as required by the IGA.
二、個人資料利用之期間及方式 The Period and Method of Using Personal information
為遵循 FATCA 法案及 IGA 協議之必要年限內，本公司所蒐集之 台端個人資料將由本公司為保存及利用，並於特定目 的之範圍內，以書面、電子文件、電磁紀錄、簡訊、電話、傳真、電子或人工檢索等方式為處理、利用與國際傳輸。 In compliance with the period required by FATCA and the IGA, the personal information collected by TBB will be kept and used by TBB and processed, used and transmitted internationally in writing, via email, electromagnetic record, text message, telephone, fax, electronic or manual search within the scope of the said specified purpose.
三、個人資料利用之地區 Geographical Limitation for Use of Personal Information
為履行 FATCA 法案及 IGA 協議下之相關義務， 台端個人資料將於臺灣及美國地區受利用。
In order to fulfill the obligations under FATCA and the IGA, your personal information will be used in both Taiwan R.O.C. and United States.
四、個人資料利用之對象 Parties Using the Personal Information
為履行 FATCA 法案及 IGA 協議下之相關義務， 台端個人資料將由本公司、臺灣權責主管機關及美國國稅局所利用。 In order to fulfill the obligations under FATCA and the IGA, your personal information will be used by TBB, the competent authority in
Taiwan R.O.C. and the IRS.
五、個人資料之權利行使及其方式 Exercise of the Rights Regarding Personal Information 台端就本公司所蒐集、處理及利用之個人資料，得隨時向本公司請求查詢、閱覽、製給複製本、補充或更正、停止蒐集 處理及利用或刪除。 台端如欲行使前述權利，有關如何行使之方式，得向本公司分行臨櫃查詢。
With regard to the personal information collected, processed and used by TBB, you may request to search, review, make duplications, supplement or correct the personal information or to discontinue the collection, processing, and use of the personal information, or request to delete the personal information. If you would use abovementioned rights, please dial or find a counter-service in every branch for understanding how to use your rights.
六、不提供個人資料之影響 The Effect of Refusal to Provide Personal Information
台端若拒絕提供本公司為遵循 FATCA 法案及 IGA 協議所需之個人資料、或嗣後撤回、撤銷同意，本公司仍可能須將關 於 台端之帳戶資訊申報予美國國稅局及臺灣權責主管機關。
In the event that you refuse to provide the personal information as required for the compliance of FATCA and the IGA, or withdraw or revoke your consent thereof, TBB may still report your account information to the competent authority in Taiwan R.O.C. and the IRS.
				              </textarea>
										</li>
									</ul>
								</div>
							</div>
							</div>
							
							<input type="button" id="CANCEL" value="取消" class="ttb-button btn-flat-gray" onclick="window.close()"/>
							<input type="button" id="NEXT" value="下一步" class="ttb-button btn-flat-gray" onclick="processQuery();" disabled />
						</div>
					</div>
					
				</form>
				
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
</body>
<div class="modal fade" id="InstructionC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="min-width:850px">
    <div class="modal-content">
      <div class="modal-header" style="padding: 20px;margin: 20px 0;">
        <h5 class="modal-title" id="exampleModalLabel">說明</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="max-width:800px;">
        <p>根據美國海外帳戶稅收遵從法(「FATCA」)及臺灣金融機構執行共同申報及盡職審查作業辦法(「CRS」)之規定，本公司 應收集及申報有關帳戶持有人稅籍與特定相關資料。每個稅籍國家均按其本身的規則釐定稅籍的定義。一般來說，個人稅 籍係為個人居住的國家。若干特別情況可能會導致個人成為其他國家的居民，或同時成為超過一個國家的居民(多重居住 地)。若個人為美國公民或具有美國稅務居民身分，亦需將美國稅籍身分於此聲明書中列示。
		<p>若您的稅籍非屬臺灣，本公司在法律上有責任把此聲明書內的資料及有關金融帳戶之其他金融資訊，申報予美國國稅局或 臺灣稅務機關，除具有美國公民或美國稅籍居民身分外，臺灣稅務機關會將該資訊交換予與本國簽訂跨國協定之其他稅籍國家。
		<p>相關名詞解釋請詳名詞解釋。
		<p>除依據FATCA及CRS之規定或帳戶持有人之稅籍出現變動外，此聲明書屬永久有效。
		<p>若帳戶持有人為未達法定年齡之未成年人，需由法定代理人完成此聲明書。
		<p>本公司作為一家金融機構，依法不得提供稅務或法律意見。
		<p>若您對此聲明書內容或所屬稅籍定義具有疑問，請聯絡您的稅務顧問或參照當地稅務機關發布之相關資訊。
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</html>