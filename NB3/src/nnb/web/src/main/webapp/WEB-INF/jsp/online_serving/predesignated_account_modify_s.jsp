<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
	<head>
		<%@ include file="../__import_head_tag.jsp"%>
		<%@ include file="../__import_js.jsp"%>
		<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
		<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
		<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
		<!-- 交易機制所需JS -->
		<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
		
		<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			// 將.table變更為footable
			//initFootable();
			setTimeout("initDataTable()",100);
		});
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
			
			$("#CMSUBMIT").click(function(e){
								
				e = e || window.event;
				
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				processQuery();
	 			}		
	  		});
			
			//上一頁按鈕
			$("#previous").click(function() {
				initBlockUI();
				fstop.getPage('${pageContext.request.contextPath}'+'/ONLINE/SERVING/predesignated_account_modify','', '');
			});
			
			// 判斷顯不顯示驗證碼
			chaBlock();
			// 交易機制 click
			fgtxwayClick();
		}

		// 交易機制選項
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			switch (fgtxway) {
					// IKEY
				case '1':
					useIKey();
					unBlockUI(initBlockId);
					break;
					// 晶片金融卡
				case '2':
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
					unBlockUI(initBlockId);
					break;
				default:
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					unBlockUI(initBlockId);
			}
		}
		
		// 使用者選擇晶片金融卡要顯示驗證碼區塊
	 	function fgtxwayClick() {
	 		$('input[name="FGTXWAY"]').change(function(event) {
	 			// 判斷交易機制決定顯不顯示驗證碼區塊
	 			chaBlock();
			});
		}
	 	// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
				console.log("fgtxway: " + fgtxway);	
				
				switch(fgtxway) {
				case '1':
					$("#chaBlock").hide();
					break;
				case '2':
					$("#chaBlock").show();
			    	break;
			    	
				default:
					$("#chaBlock").hide();
			}	
	 	}
		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function () {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100))
				.fadeIn();
			});
		}

		</script>
	</head>
	<body>
		<!-- 交易機制所需畫面    -->
		<%@ include file="../component/trading_component.jsp"%>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 修改好記名稱     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0226" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.D0226"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="${__ctx}/ONLINE/SERVING/predesignated_account_modify_r">
				<c:set var="dataSet" value="${predesignated_account_modify.data.dataSet}" />		
				<input type="hidden" NAME="DPBHNO1" value="${dataSet.BNKCOD}">
				<input type="hidden" NAME="DPACNO1" value="${dataSet.ACN}">
				<input type="hidden" NAME="DPACCSETID1" value="${dataSet.DPACCSETID}">
				<input type="hidden" NAME="DPACGONAME1" value="<c:out value='${dataSet.DPGONAME}' />">
				
				<input type="hidden" id="ACNINFO" name="ACNINFO" value="${dataSet.ACNINFO}">
				<input type="hidden" id="SelectedRecord" name="SelectedRecord" value="${dataSet.ACNINFO}">
				<input type="hidden" id="COUNT" name="COUNT" value="${predesignated_account_modify.data.COUNT}">
				<%-- 驗證相關 --%>
				<input type="hidden" id="jsondc" name="jsondc" value='${predesignated_account_modify.data.jsondc}'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" id="BNKCOD" name="BNKCOD" value="${dataSet.BNKCOD}">
				<input type="hidden" id="ACN" name="ACN" value="${dataSet.ACN}">
				
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<ul class="ttb-result-list"></ul>
							<!-- 我的約定轉入帳號註銷表-->
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
									<tr>
										<th></th>
										<!-- 銀行名稱-->
										<th><spring:message code="LB.Bank_name" /></th>
										<!-- 約定轉入帳號-->
										<th>	
											<spring:message code="LB.D0227"/>
										</th>
										<!-- 好記名稱-->
										<th><spring:message code="LB.Favorite_name" /></th>
									</tr>
								</thead>
								<tbody>
										<tr>
											<td><spring:message code="LB.D0241" /></td>
											<td>${dataSet.BNKCOD}</td>
											<td>${dataSet.ACN}</td>
											<td><c:out value='${dataSet.DPGONAME}' /></td>
										</tr>
										<tr>
											<td><spring:message code="LB.D0242" /></td>
											<td>${dataSet.BNKCOD}</td>
											<td>${dataSet.ACN}</td>
											<td><input type="text" name="DPACGONAME2" id="DPACGONAME2" class="text-input" value=""></td>
										</tr>
								</tbody>
								</table>
								<div class="ttb-input-block">
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Transaction_security_mechanism" />
										</label>
									</span>
									<span class="input-block">
										<!-- 使用者是否可以使用IKEY -->
										<c:if test="${sessionScope.isikeyuser}">
											<!--電子簽章(請載入載具i-key) -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Electronic_signature" />
													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
													<span class="ttb-radio"></span>
												</label>
											</div>
										</c:if>
										<!-- 晶片金融卡 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Financial_debit_card" />
												<input type="radio" name="FGTXWAY" id="CMCARD" value="2" />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 驗證碼-->
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
									<span class="input-title">
										<label>
											<!-- 驗證碼 -->
											<spring:message code="LB.Captcha" />
										</label>
									</span>
									<span class="input-block">
	                                    <div class="ttb-input">
											<img name="kaptchaImage"  class="verification-img" src="" />
											<button class="ttb-sm-btn btn-flat-orange" type="button" name="reshow"
												onclick="changeCode()">
												<spring:message code="LB.Regeneration" />
											</button>
	                                    </div>
	                                    <div class="ttb-input">
											<spring:message code="LB.Captcha" var="labelCapCode" />
											<input id="capCode" name="capCode" type="text" class="text-input"
												placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
										</div>
									</span>
								</div>
							</div>
							<!--回上頁 -->
							<input type="button" name="previous" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page"/>"/>
							<!-- 確定 -->
	                        <input type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange">
						</div>
					</div>
				</form>
			</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>