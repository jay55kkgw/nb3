<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
  <br /><br />
  <div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
  <br /><br />
  <div style="text-align:center">
    <font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
  </div>
  <br />
  <div style="text-align:center">
    ${MsgName}
  </div>
  <br />
  <table class="print">
    <tr>
      <td class="ColorCell" width="21%">
        <spring:message code="LB.W0944" />
      </td>
      <td class="DataCell0" colspan="3">${hide_CDNO }</td>
    </tr>
    <tr>
      <td class="ColorCell" width="21%">
        <spring:message code="LB.W1041" />
      </td>
      <td class="DataCell1" colspan="3">${FUNDLNAME }</td>
    </tr>
    <tr>
      <td class="ColorCell" width="21%">
        <spring:message code="LB.D0973" />
      </td>
      <td class="DataCell1" colspan="3">
        <spring:message code="${str_MIP}" />
      </td>
    </tr>
    <tr>
      <td class="ColorCell" width="21%">
        <spring:message code="LB.W1153" />
      </td>
      <td class="DataCell0" colspan="3">
        <c:if test="${str_ALTERTYPE1 != ''}">
          <spring:message code="LB.Deposit_amount_1" />
        </c:if>
        <c:if test="${str_ALTERTYPE2 != ''}">
          <spring:message code="LB.W1155" />
        </c:if>
        <c:if test="${str_ALTERTYPE3 != ''}">
          <spring:message code="LB.W1156" />
        </c:if>
      </td>
    </tr>
    <c:if test="${str_ALTERTYPE1 != ''}">
      <tr id="row1">
        <td class="ColorCell" width="23%">
          <spring:message code="${str_OPAYAMT}" />
        </td>
        <td class="DataCell1" width="16%">
          ${ADCCYNAME}
          ${display_OPAYAMT}
        </td>
        <td class="ColorCell" width="26%">
          <spring:message code="${str_NPAYAMT}" />
        </td>
        <td class="DataCell1" width="31%">
          ${ADCCYNAME }
          ${display_PAYAMT }
      </tr>
    </c:if>
    <c:if test="${str_ALTERTYPE2 != ''}">
      <c:if test="${DAMT == '2'}">
        <tr id="row2">
          <td class="ColorCell" width="23%">
            <spring:message code="LB.W1158" />
          </td>
          <td class="DataCell0" width="16%">
            ${OPAYDAY4}
            ${OPAYDAY5}
            ${OPAYDAY6}
            ${OPAYDAY1}
            ${OPAYDAY2}
            ${OPAYDAY3}
          </td>
          <td class="ColorCell" width="26%">
            <spring:message code="LB.W1159" />
          </td>
          <td class="DataCell0" width="31%">
            ${PAYDAY4}
            ${PAYDAY5}
            ${PAYDAY6}
            ${PAYDAY1}
            ${PAYDAY2}
            ${PAYDAY3}
          </td>
        </tr>
      </c:if>
      <c:if test="${DAMT != '2'}">
        <tr id="row2">
          <td class="ColorCell" width="23%">
            <spring:message code="LB.W1158" />
          </td>
          <td class="DataCell0" width="16%">
            ${OPAYDAY1}
            ${OPAYDAY2}
            ${OPAYDAY3}
            ${OPAYDAY4}
            ${OPAYDAY5}
            ${OPAYDAY6}
            ${OPAYDAY7}
            ${OPAYDAY8}
            ${OPAYDAY9}
          </td>
          <td class="ColorCell" width="26%">
            <spring:message code="LB.W1159" />
          </td>
          <td class="DataCell0" width="31%">
            ${PAYDAY1}
            ${PAYDAY2}
            ${PAYDAY3}
            ${PAYDAY4}
            ${PAYDAY5}
            ${PAYDAY6}
            ${PAYDAY7}
            ${PAYDAY8}
            ${PAYDAY9}
          </td>
        </tr>
      </c:if>
    </c:if>
    <c:if test="${str_ALTERTYPE3 != ''}">
      <tr id="row3">
        <td class="ColorCell" width="23%">
          <spring:message code="LB.X0371" />
        </td>
        <td class="DataCell1" width="16%">
          <spring:message code="${str_Before}" />
        </td>
        <td class="ColorCell" width="26%">
          <spring:message code="LB.W1161" />
        </td>
        <td class="DataCell1" width="31%">
          <spring:message code="${str_After}" />
        </td>
      </tr>
    </c:if>
  </table>
  <br>
  <br>
  <!--   說明 -->
  <div class="text-left">
  </div>
</body>

</html>