<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust:exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<div style="text-align: center">
		<spring:message code= "LB.X1419" />
	</div>
	<br />
	<table class="print">
		<!-- 查詢時間-->
		<tr>
			<td style="width: 8em">
				<!--         查詢時間 --> <spring:message code= "LB.Inquiry_time" />
			</td>
			<td>${CMQTIME}</td>
		</tr>
		<!-- 信用卡當月交易明細傳送位址 -->
			<tr>
				<td class="ColorCell"><spring:message code= "LB.X1420" /></td>
				<td>${CMMAIL}</td>
			</tr>
		
	</table>
	<br>
	<br>
</body>

</html>