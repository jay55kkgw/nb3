<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
		
	});
	function readcheck(){
		if($('#READ').is(':checked')){
			$('#CMSUBMIT').removeAttr("disabled");
			$('#CMSUBMIT').removeClass('btn-flat-gray');
			$('#CMSUBMIT').addClass('btn-flat-orange');
		}
		if(!$('#READ').is(':checked')){
			$('#CMSUBMIT').attr("disabled","disabled");
			$('#CMSUBMIT').removeClass('btn-flat-orange');
			$('#CMSUBMIT').addClass('btn-flat-gray');
		}
	}
	function buttonAction(clicked_id){
		if(clicked_id=="BACKTO"){
			action = '${__ctx}/ONLINE/APPLY/online_apply'
			$("form").attr("action", action);
			$("form").submit();
		}
	}


</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 隨護神盾申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1573" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			
			<h2><spring:message code="LB.D1573"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<div id="step-bar">
				<ul>
				    <li class="active"><spring:message code="LB.D0176"/></li>
				    <li class=""><spring:message code="LB.X2066"/></li>
				    <li class=""><spring:message code="LB.X2067"/></li>
			    </ul>
			</div>
			
			<form id="formId" method="post" action="${__ctx}/ONLINE/APPLY/shield_apply_p3">
				<input type="hidden" id="TYPE" name="TYPE" value="14"> 
				<div class="main-content-block row">
					<div class="col-12 terms-block">
<!-- 						<div class="CN19-1-header"> -->
<!-- 							<div class="logo"> -->
<%-- 								<img src="${__ctx}/img/logo-1.svg"> --%>
<!-- 							</div> -->
<!-- 							常用網址超連結 -->
<!-- 							<div class="text-right hyperlink"> -->
<!-- 							臺灣企銀首頁 -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw');" style="text-decoration: none"> <spring:message code="LB.TAIWAN_BUSINESS_BANK"/> </a> <strong><font color="#e65827">|</font></strong> --%>
<!-- 								網路ATM  -->
<%-- 								<a href="#" onClick="window.open('https://eatm.tbb.com.tw/');" style="text-decoration: none"> <spring:message code="LB.Web_ATM"/> </a><strong><font color="#e65827">|</font></strong> --%>
<!-- 								意見信箱 	 -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw/q3.html?');" style="text-decoration: none"> <spring:message code="LB.Customer_service"/> </a> --%>
<!-- 							</div> -->
<!-- 						</div> -->
						
						<!-- 隨護神盾約定條款-->
						<div>
							
			                <br>
							<div class="CN19-clause">
								<p  style="font-size:16.0pt; text-align:center;">
									<b>隨護神盾約定條款</b>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<b>隨護10810</b>
								</p>
								立約人茲向貴行申請「隨護神盾」，同意且遵守下列條款：<br>
								第一條　名詞定義<br>
								<div>
									一、隨護神盾：係指立約人透過行動裝置（如智慧型手機、平板電腦等）下載商務&nbsp;XML&nbsp;憑證並以該憑證進行交易驗證之安控機制。<br>
									二、下載密碼：係指立約人於臨櫃申請隨護神盾時由貴行所核發之密碼或於線上申請時自行設定之密碼，用於下載商務&nbsp;XML&nbsp;憑證時使用。<br>
									三、隨護密碼：係指立約人透過行動裝置下載商務&nbsp;XML&nbsp;憑證時自行設定之密碼，用於行動裝置透過隨護神盾進行交易驗證時使用<br>
								</div>
								第二條　使用範圍
								<div>
									隨護神盾可使用於特定金額範圍內之非約定轉入帳號交易，及主管機關與中華民國銀行商業同業公會聯合會訂定之金融機構辦理電子銀行業務安全控管作業基準明訂之應用或業務，使用範圍及有關作業規定事項變更時，得逕於貴行網站公告。<br>
								</div>
								第三條　隨護神盾之申請、使用與保管
								<div>
									一、立約人申請使用隨護神盾服務應臨櫃或線上(限自然人)申請辦理，並經貴行確認身分及驗證申請資格。<br>
									二、立約人應妥善保管下載密碼，不可洩漏予第三人知悉，須自取得下載密碼起於貴行所定期間內完成隨護神盾開通使用之程序，逾時須重新辦理申請手續。<br>
									三、立約人應妥善保管「隨護密碼」，不得洩漏或交付予他人使用，如因故意或過失，致遭受損害時，立約人自負一切損害賠償責任。<br>
									四、立約人應妥善保管已下載憑證之行動裝置，如有遺失、滅失、被竊或其他喪失占有等情形時，應立即向貴行辦理憑證廢止手續，日後如擬恢復使用，應重新辦理申請手續。未辦理憑證廢止手續前而遭冒用所生之損害，立約人應自行負責。<br>
								</div>
								第四條　隨護神盾之異動作業
								<div>
									一、立約人之憑證有效期限，至少為一年，有效期限屆滿前一個月起至到期日止，應申請憑證更新。憑證若已過期則無法執行更新，應重新辦理申請手續。<br>
									二、立約人於行動裝置輸入「隨護密碼」連續錯誤達五次時，即自動停止立約人使用本項機制服務。立約人如擬恢復使用，應重新辦理申請手續。<br>
								</div>
								第五條　隨護神盾憑證之廢止作業
								<div>
									立約人擬終止使用隨護神盾，得臨櫃或線上(限自然人)辦理憑證廢止手續，並經貴行完成電腦登錄後始生效。如欲恢復使用時，應重新辦理申請手續。<br>
								</div>
								第六條　爭議之處理
								<div>
									一、立約人與用戶憑證機構或註冊中心因使用憑證所引發之任何爭議，如係可歸責於用戶憑證機構或註冊中心者，應由立約人分別與用戶憑證機構或註冊中心協議解決。<br>
									二、前項爭議如責任歸屬不明時，應由用戶憑證機構及註冊中心共同與立約人協議解決。<br>
									三、於爭議協商、訴訟處理過程所發生之費用分擔，依據協商或相關之法律規範處理。<br>
									四、如為跨國或跨區域之爭議處理，無法以上述之處理方式解決時，依照相關之跨國或跨區域糾紛仲裁規範處理。<br>
								</div>
							</div>
							<!-- 本人於事前詳閱全部條款，充分瞭解且同意其內容。 -->
							<br>
							<label class="check-block">
								<spring:message code="LB.D1574" />
								<input type="checkbox" id="READ" name="READ" onclick="readcheck()"/>
								<span class="ttb-check"></span>
							</label>
							<span class="input-unit"></span>
						</div>
						<!-- 不同意 -->
						<input type="button" id="BACKTO" class="ttb-button btn-flat-gray" value="<spring:message code="LB.D0041"/>" onclick="buttonAction(this.id)"/>
<!-- 						同意ttb-button btn-flat-orange -->
						<input type="submit" id="CMSUBMIT" class="ttb-button btn-flat-gray" value="<spring:message code="LB.D0097"/> " disabled/>
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>