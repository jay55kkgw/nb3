<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 回上一頁填入資料
			refillData();
			// 檢查 checkbox 是否全勾選，決定是否可按下一步
			checkReadFlag();
		}
		
		// 通過表單驗證準備送出
		function processQuery() {
			// 表單驗證
			if ( !$('#formId').validationEngine('validate') ) {
				e.preventDefault();
			} else {
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				
				// 遮罩
	         	initBlockUI();
				
				// 檢查KYC可填寫次數
	         	var cusidn = '${sessionScope.CUSIDN_N361}';
	         	var kycCanDo = getKycCanDo(cusidn);
	         	if(kycCanDo == false)
				{
					//alert("<spring:message code= "LB.Alert093" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert093' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					unBlockUI(initBlockId)
					return false;
				}
				
	            $("#formId").submit();
			}
		}
		
		// 判斷可否填寫KYC
		function getKycCanDo(cusidn) {
			var uri = '${__ctx}' + "/ONLINE/APPLY/getKycCanDo_notlogin_aj"
			console.log("getKycCanDo >>" + uri);
			
			rdata = { CUSIDN: cusidn };
			console.log("rdata >> " + rdata);
			
			bs = fstop.getServerDataEx(uri, rdata, false);
			console.log("data >> ", bs);
			
			if(bs != null && bs.result == true) {
				return bs.data.KycCanDo;
			}
		}
		
		// 檢查 checkbox 是否全勾選，決定是否可按下一步
		function checkReadFlag() {
			var result = true;
			$("input[type='checkbox']").each( function () {
				if(!$(this).prop('checked')) {
					// 未勾選
					result = false;
				}
			});
			
			if(result) {
				// 可點擊
				$("#CMSUBMIT").attr("class", "ttb-button btn-flat-orange");
				$("#CMSUBMIT").attr("disabled", false);
				
			} else {
				// 不可點擊
				$("#CMSUBMIT").attr("class", "ttb-button btn-flat-gray");
				$("#CMSUBMIT").attr("disabled", true);
			}
		}
		
		// 回上一頁填入資料
		function refillData() {
			var jsondata = '${previousdata}';
			console.log(jsondata);
			
			if (jsondata != "") {
				JSON.parse(jsondata, function(key, value) {
					if(key) {
						var obj = $("#"+key);
						var type = obj.attr('type');
						
						if(type == 'checkbox'){
							obj.prop('checked', true);
						}
					}
				});
			}
		}
		
	</script>
</head>

<body>
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0847" /></li>
		</ol>
	</nav>
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>預約開立基金戶</h2>
				<div id="step-bar">
					<ul>
						<li class="active">注意事項與權益</li>
						<c:if test="${N361_1.data.FATCA == 'N'}">
							<li class="">FATCA個人客戶身份識別聲明</li>
						</c:if>
						<li class="">投資屬性調查</li>
						<li class="">開戶資料</li>
						<li class="">確認資料</li>
						<li class="">完成申請</li>
					</ul>
				</div>
				
				<form method="post" id="formId" action="${__ctx}${next}">
					<div class="main-content-block row">
						<div class="col-12 terms-block">
							<div class="ttb-message">
								<p>顧客權益</p>
							</div>
							<p class="form-description">請先下載並審閱臺灣中小企業銀行「特定金錢信託投資國內外有價證券信託約定書」(以下稱約定書)，並留存本契約。
								<button type="button" class="btn-flat-orange terms-btn" onclick="window.open('${__ctx}/term/特定金錢信託投資國內外有價證券信託契約書暨約定書.pdf');">下載</button>
							</p>
							<div class="text-left">
								<div class="text-left">
									<span class="input-block">
										<div class="ttb-input">
											<label class="check-block">本人已詳閱約定書內容。
												<input type="checkbox" id="check1" name="check1" value="Y" onClick="checkReadFlag()">
												<input type="hidden" id="N3611flag1" name="N3611flag1" value="Y" >
												<span class="ttb-check"></span>
											</label>
										</div>
									</span>
								</div>
								<p class="form-description subtitle-color" style="font-weight:normal; color:#696a6c;">本人 (以下稱委託人，限成年之自然人) 同意下列事項 (請逐項勾選)：</p>
								<div class="text-left">
									<span class="input-block">
										<div class="ttb-input">
											<label class="check-block">
												委託人為臺灣中小企業銀行(以下稱受託人)之既有網路銀行客戶，爰利用受託人一般網路銀行申辦特定金錢信託投資國內外有價證券業務線上開戶(以下稱本服務)。
												<input type="checkbox" id="check2" name="check2" value="Y" onClick="checkReadFlag()">
												<span class="ttb-check"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="check-block">
												委託人申辦本服務之身分識別，須採電子憑證簽章、晶片金融卡或其他受託人認可之安全機制介面(合稱識別機制)辦理。
												<input type="checkbox" id="check3" name="check3" value="Y" onClick="checkReadFlag()">
												<span class="ttb-check"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="check-block">
												委託人辦理本服務已於合理審閱期間(至少5日)閱訖並完全瞭解及同意約定書、
												本注意事項所載各約定條款(包括但不限於蒐集個人資料應告知事項)。委託人同意以符合電子簽章法之簽章，
												或以「金融機構辦理電子銀行業務安全控管作業基準」所訂之安全規範，作為委託人身分識別與同意本服務契據條款之依據，無須另行簽名或蓋章。
												<input type="checkbox" id="check4" name="check4" value="Y" onClick="checkReadFlag()">
												<span class="ttb-check"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="check-block">
												委託人申辦本服務者，所有相關訊息之送達方式皆以電子郵件地址為主，如委託人之電子郵件地址變更，應利用一般網路銀行變
											   	 更並確認資料更新成功，受託人皆以委託人輸入之電子郵件地址作為送達地址之依據。
												<input type="checkbox" id="check5" name="check5" value="Y" onClick="checkReadFlag()">
												<span class="ttb-check"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="check-block">
												委託人以一般網路銀行辦理本服務之申請者，與約定書具有相同效力。委託人同意以電子文件作為表示方法，依本服務契據條款所交換之電子文件，其效力與書面文件相同。
												<input type="checkbox" id="check6" name="check6" value="Y" onClick="checkReadFlag()">
												<span class="ttb-check"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="check-block">
												委託人同意由受託人提供本服務全部電子文件及其資訊網頁供委託人確認後下載，以代交付，視同以實體文件交付。事後委託人可隨時線上查閱、下載與列印本服務契據條款內容。
												<input type="checkbox" id="check7" name="check7" value="Y" onClick="checkReadFlag()">
												<span class="ttb-check"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="check-block">
												為維護委託人權益，委託人對本服務有所疑義，除書面外，亦得透過下列方式向受託人提出申訴或反映意見：<br />
												(一)	申訴及客服專線：0800-00-7171#5(申訴)，#1(客服)<br />
												(二)	網址：https://www.tbb.com.tw<br />
												(三)	地址：臺北市塔城街30號<br />
												(四)	傳真號碼：02-2550-8338<br />
												(五)	電子信箱：tbb@mail.tbb.com.tw<br />
												(六)	營業時間中可逕洽營業單位<br />
												受託人受理申訴後，將由專人與委託人溝通說明釐清原因，並將處理結果回覆委託人。
												<input type="checkbox" id="check8" name="check8" value="Y" onClick="checkReadFlag()">
												<span class="ttb-check"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="check-block">
												本契約條款未盡事宜事項，悉依受託人相關業務規定及一般金融機構慣例辦理。
												<input type="checkbox" id="check9" name="check9" value="Y" onClick="checkReadFlag()">
												<span class="ttb-check"></span>
											</label>
										</div>
									</span>
								</div>
								<p class="form-description"></p>
								<div class="text-left">
	
									<ul class="terms">
										<li data-num="">
											<span class="input-subtitle subtitle-color">FATCA個人客戶身份別識別聲明</span>
											<div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
												<div class="ttb-terms">
													<div class="ttb-result-list terms">
														<ol>
															<li data-num="一、">本人不具有美國公民或稅務居民身分</li>
															<li data-num="二、">本人同意，若爾後有任何FATCA身分別變更之情事，將於變更後30日內通知 貴行。</li>
															<li data-num="三、">本聲明如有不實，本人願自負法律責任。</li>
														</ol>
														<p>註：美國公民或稅務居民係指具有美國國籍者(持有美國護照)、持有綠卡者，或當年度入境美國並停留超過183天，或者當年度入境並在美國待超過31天，同時滿足所謂的「前3年審核期」的計算超過183天。</p>
													</div>
												</div>
											</div>
										</li>
									</ul>
									<label class="check-block">本人確認並同意以上聲明
										<input type="checkbox" id="N3611flag2" name="N3611flag2" value="Y" onclick="checkReadFlag()">
										<span class="ttb-check"></span>
									</label>
								</div>
								<p class="form-description"></p>
								<div class="text-left">
	
									<ul class="terms">
										<li data-num="">
											<span class="input-subtitle subtitle-color">臺灣中小企業銀行股份有限公司履行個人資料保護法義務告知</span>
											<div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
												<%@ include file="../term/N201_1.jsp"%>
											</div>
										</li>
									</ul>
									<label class="check-block">本人確認並同意以上聲明
										<input type="checkbox" id="N3613flag1" name="N3613flag1" value="Y" onclick="checkReadFlag()">
										<span class="ttb-check"></span>
									</label>
								</div>
							</div>
							
							<input type="button" id="CLOSEPAGE" value="不同意並離開" class="ttb-button btn-flat-gray" onclick="window.close();"/>
							<input type="button" id="CMSUBMIT" value="同意並繼續" class="ttb-button btn-flat-gray" onclick="processQuery();" disabled />
						</div>
					</div>
					
				</form>
				
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	
</body>

</html>