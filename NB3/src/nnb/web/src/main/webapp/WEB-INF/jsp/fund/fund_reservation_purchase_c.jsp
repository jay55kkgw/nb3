<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
	        }
			else{
				$("#formId").validationEngine('detach');
				// 遮罩
				initBlockUI();
				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
				$("#formId").attr("action","${__ctx}/FUND/QUERY/fund_reservation_purchase_r");
	  			$("#formId").submit();
			}
		});
	}
	function processQuery(){
		$("#back").attr("action","${__ctx}/FUND/QUERY/fund_reservation_purchase");
		$("#back").submit();
}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Funds" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 取消預約申購交易     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0381" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0376" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                        <c:set var="dataSet" value="${ fund_reservation_purchase_c.data.dataSet }" />
							<!--交易種類-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
									<label>
                                        <h4><spring:message code="LB.W1060" /></h4>
                                    </label>
								</span>
                                <span class="input-block">
									<div class="ttb-input">
                                       	<span>${fund_reservation_purchase_c.data.FDTXTYPE}</span>
                                  	</div>
								</span>
							</div>
							
							 <!--基金名稱-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W0025" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
										<span>(${dataSet.TRANSCODE})&nbsp;${dataSet.FUNDLNAME}</span>
									</div>
								</span>
							</div>
							
							 <!--預約日期-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.X0377" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                       	<span>${dataSet.TRADEDATE_1}</span>
									</div>
								</span>
							</div>
							
							<!--申購金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
									<label>
                                        <h4><spring:message code="LB.W1074" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                       	<span>${dataSet.ADCCYNAME} ${dataSet.AMT3_1}</span>
									</div>
								</span>
							</div>
							
							<!--手續費-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
									<label>
                                        <h4><spring:message code="LB.D0507" /></h4>
                                    </label>
								</span>
                                <span class="input-block">
									<div class="ttb-input">
                                       	<span>${dataSet.ADCCYNAME} ${dataSet.FCA2_1}</span>
									</div>
								</span>
							</div>
							
							<!--轉出帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Payers_account_no"/></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                       	<span>${dataSet.OUTACN_1}</span>
									</div>
								</span>
							</div>
							
							<!--扣款金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
									<label>
                                        <h4><spring:message code="LB.W1079" /></h4>
                                    </label>
								</span>
                                <span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.ADCCYNAME} ${dataSet.AMT5_1}</span>
									</div>
								</span>
							</div>
							
							<!--手續費率-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W1034" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                       	<span>${dataSet.FCAFEE_1}%</span>
									</div>
								</span>
							</div>

							<!--交易機制-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Transaction_security_mechanism"/></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                       <label class="radio-block"><spring:message code="LB.SSL_password"/>
                                            <input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked >
                                            <span class="ttb-radio"></span>
                                        </label>
                                    </div>   
                                    <div class="ttb-input">      
										<input class="text-input validate[required,funcCall[validate_CheckTxnNewPassword[CMPASSWORD]]]" autocomplete="off" type="password" name="CMPASSWORD" id="CMPASSWORD" size="8" maxlength="8" value="">
									</div>
                                </span>
                            </div> 
							
                        </div>
						<input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code= "LB.Back_to_previous_page" />" onClick="processQuery()"/> 
                        <input class="ttb-button btn-flat-gray" name="CMRESET" type="reset" value="<spring:message code="LB.Re_enter"/>" />
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm"/>" />
						<input type="hidden" name="ADOPID" value="N381">  
						<input type="hidden" name="CMPASSWORD" id="CMPASSWORD" value="">
						<input type="hidden" name="PINNEW" id="PINNEW" value="">
						<input type="hidden" id="FDTXTYPE" name="FDTXTYPE" value="${fund_reservation_purchase_c.data.FDTXTYPE}">
						<input type="hidden" id="TRANSCODE" name="TRANSCODE" value="${dataSet.TRANSCODE}">
	  					<input type="hidden" id="FUNDLNAME" name="FUNDLNAME" value="${dataSet.FUNDLNAME}">	  	
						<input type="hidden" id="TRADEDATE" name="TRADEDATE" value="${dataSet.TRADEDATE}">
						<input type="hidden" id="ADCCYNAME" name="ADCCYNAME" value="${dataSet.ADCCYNAME}">
						<input type="hidden" id="CRY" name="CRY" value="${dataSet.CRY}">
						<input type="hidden" id="AMT3" name="AMT3" value="${dataSet.AMT3}">	
						<input type="hidden" id="FCA2" name="FCA2" value="${dataSet.FCA2}">
						<input type="hidden" id="OUTACN" name="OUTACN" value="${dataSet.OUTACN}">	
						<input type="hidden" id="AMT5" name="AMT5" value="${dataSet.AMT5}">
						<input type="hidden" id="FCAFEE" name="FCAFEE" value="${dataSet.FCAFEE}">		
						<input type="hidden" id="SSLTXNO" name="SSLTXNO" value="${dataSet.SSLTXNO}">				
						<input type="hidden" id="" name="action" value="forward">
						<input type="hidden" id="" name="urlPath" value="">
                    </div>
                </div>
				</form>
				<form method="post" id="back">
					<input type="hidden" id="ACN1" name="ACN1" value="${fund_reservation_purchase_c.data.ACN1}">
					<input type="hidden" id="ACN2" name="ACN2" value="${fund_reservation_purchase_c.data.ACN2}">
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>