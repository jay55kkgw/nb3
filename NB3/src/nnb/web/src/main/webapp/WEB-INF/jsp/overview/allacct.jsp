<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp" %>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/fullcalendar.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/fullcalendar.print.min.css" rel="stylesheet" media="print">
<script type="text/javascript" src="${__ctx}/js/moment.min.js"></script>
<script type="text/javascript" src="${__ctx}/js/fullcalendar.min.js"></script>
<script type="text/javascript" src="${__ctx}/js/fullcalendar.zh-cn.js"></script>
<script type="text/javascript" src="${__ctx}/js/fullcalendar.zh-tw.js"></script>

<style type="text/css">
	.fast-btn-blcok {
		position: absolute;
		top: auto;
		bottom: auto;
		left: auto;
		right: 20%;
		min-width: 40%;
		width:55%;
		margin: 0 auto;
		background-color: #fbf8e9;
		z-index: 999;
	}
	/* 箭頭上-邊框 */
	.fast-btn-blcok .arrow_t_out {
		width: 0px;
		height: 0px;
		border-width: 15px;
		border-style: solid;
		border-color: transparent transparent #fddd9a transparent;
		position: absolute;
		top: -29px;
		right: 20px;
	}

	.fast-btn-blcok ul {
		text-align: left;
		margin: 0 auto;
		padding: 6%;
		background-color: #fddd9a;
		border-radius: 5px;
	}

	.fast-btn-blcok ul li a {
		color: #374140;
	}	
</style>
<script type="text/javascript">
	// 畫面呈現
	$(document).ready(function () {
		// 開始查詢資料並完成畫面
		init();
	});

	// 帳戶總覽查詢
	function init() {
		var dpoverview = '${dpoverview}';
		console.log("dpoverview: " + dpoverview);
		
		// 使用者帳戶總覽設定的功能，未設定或設定不合理，則只顯示預設的臺幣查詢
		if(dpoverview != null || dpoverview != '') {
			var checkResult = settingCheck(dpoverview);
			console.log("checkResult: " + checkResult);
			
			// 設定合理，顯示設定的查詢功能
			if(checkResult) {
				// 依據使用者定義的查詢功能發Ajax
				var dpArray = dpoverview.split(',');
				for(let o in dpArray){
					console.log("dpoverview.dpArray[o]: " + dpArray[o]);
					switch (dpArray[o]) {
						case "1":
							getMyAssets110(); // n110-臺幣活期性存款餘額
							getMyAssets420(); // n420-臺幣定期性存款明細
							break;
						case "2":
							getMyAssets510(); // n510-外匯活存餘額
							getMyAssets530(); // n530-外幣定存明細
							break;
						case "3":
							getMyAssets320(); // n320-借款明細查詢
							break;
						case "4":
							getMyAssets012(); // c012-基金餘額及損益查詢
							getMyAssetsb012(); // b012-臺幣活期性存款餘額
							break;
						case "5":
							getMyAssets810(); // n810-信用卡總覽
							break;
						case "6":
							getMyAssets870(); // n870-中央登錄債券餘額
							break;
						case "7":
							getMyAssets190(); // n190-黃金存摺餘額查詢
							break;
						default:
							console.log("dpoverview.default !");
					}
				}
				
			} else {
				// 設定不合法，顯示預設臺幣查詢的功能
				getMyAssets110(); // n110-臺幣活期性存款餘額
				getMyAssets420(); // n420-臺幣定期性存款明細
			}
			
		} else {
			// 帳戶總覽未設定，顯示預設臺幣查詢的功能
			getMyAssets110(); // n110-臺幣活期性存款餘額
			getMyAssets420(); // n420-臺幣定期性存款明細
		}
	}
	
	// 是否有合法的帳戶總覽設定
	function settingCheck(dpoverview) {
		// 回傳結果
		var checkResult = false;
		
		// 合法的帳戶總覽設定
		var checkArray = ["1","2","3","4","5","6","7"];
		
		// TXNUSER.DPOVERVIEW使用者帳戶總覽設定
		var dpArray = dpoverview.split(',');
		dpArray.forEach(function(k){
			if(checkArray.hasOwnProperty(k)){
				checkResult=true;
			}
		});
	

		return checkResult;
	}

</script>

</head>
<c:set var="SHWD" value=""></c:set>
<%@ include file="../fund/fund_shwd.jsp"%>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 帳戶總覽     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Account_Overview" /></li>
		</ol>
	</nav>

	
	<!-- 主畫面 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 帳戶總覽 -->
	           	<h2>
	           		<spring:message code="LB.Account_Overview"/>
	           	</h2>
	           	<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
	           	
				<!-- 查詢時間 -->
	           	<jsp:useBean id="now" class="java.util.Date" />
				<fmt:formatDate value="${now}" type="both" dateStyle="long" pattern="yyyy/MM/dd HH:mm:ss" var="today"/>
	           	<p><spring:message code="LB.Inquiry_time"/>：${today}</p>

 	            	<%@ include file="./allacct_template.jsp"%> 
			     	<%@ include file="./allacct_n110.jsp"%>
      				<%@ include file="./allacct_n420.jsp"%>  
     				<%@ include file="./allacct_n510.jsp"%>
	     		    <%@ include file="./allacct_n530.jsp"%>
				    <%@ include file="./allacct_n320.jsp"%>
			    	<%@ include file="./allacct_c012.jsp"%>
			    	<%@ include file="./allacct_b012.jsp"%>
	       			<%@ include file="./allacct_n810.jsp"%>
				    <%@ include file="./allacct_n870.jsp"%>
			    	<%@ include file="./allacct_n190.jsp"%>
				
				<!-- 說明 -->
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<!-- 本功能自動提供台幣存款歸戶查詢，欲增加查詢項目，請按 -->
					<li><spring:message code= "LB.Allact_P1_D1-1" />
						<!-- 帳戶總覽設定 -->
						<input type="button" name="DPSET" id="DPSET" value="<spring:message code= "LB.Allact_P1_D1-2" />" onClick="fstop.getPage('${pageContext.request.contextPath}'+'/PERSONAL/SERVING/account_settings','', '')">。
					</li>
					<!-- 帳戶總覽設定項目愈多，查詢時等待系統回應時間愈長。 -->
					<li><spring:message code= "LB.Allact_P1_D2" /></li>
		        </ol>
					
			</section>
		</main>
	</div><!-- content row END -->
	
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>