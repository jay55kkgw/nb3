<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			// 初始化時隱藏span
			$("#hideblock").hide();
		});

		function init() {		
			
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
			
			
			$("#CMSUBMIT").click(function (e) {
				
				//打開驗證欄位
				$("#hideblock").show();
				//選取的欄位
				var type = $('input[name="FGSVACNO"]:checked').val();
				$("#ErrorMsg").val(type);
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}
				else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				if(type == 0){
		 				$("#formId").attr("action","${__ctx}/FINANCIAL/TRIAL/NT/deposit_interest");
	 				}else if(type == 1){
		 				$("#formId").attr("action","${__ctx}/FINANCIAL/TRIAL/NT/whole_deposit");
	 				}else if(type == 2){
		 				$("#formId").attr("action","${__ctx}/FINANCIAL/TRIAL/NT/scatter_deposit");
	 				}
		 	  		$("#formId").submit(); 
	 			}
			});
		}
		
</script>

</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 理財試算服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0087" /></li>
    <!-- 臺幣定期儲蓄存款試算     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0095" /></li>
		</ol>
	</nav>


	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.X0095" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">							
							<!-- 臺幣定期儲蓄存款試算 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label><h4><spring:message code="LB.Option_item" /></h4></label>
								</span> 
								<span class="input-block">
								<!--存本取息 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.NTD_deposit_type_2" />
											<input type="radio" name="FGSVACNO" value="0" checked> 
											<span class="ttb-radio"></span>
										</label>
									</diV>									
								<!--整存整付 -->
									<div class="ttb-input">
										<label class="radio-block"> 
											<spring:message code="LB.NTD_deposit_type_3" />
											<input type="radio" name="FGSVACNO" value="1"> 
											<span class="ttb-radio"></span>
										</label>
									</div>
								<!--零存整付 -->									
									<div class="ttb-input">
										<label class="radio-block"> 
											<spring:message code="LB.NTD_deposit_type_5" />
											<input type="radio" name="FGSVACNO" value="2"> 
											<span class="ttb-radio"></span>
										</label>
									</div>
								<!-- 不在畫面上顯示的span -->
								<span id="hideblock" class="ttb-input">
								<!-- 驗證用的input -->
								<input id="ErrorMsg" type="text" class="text-input validate[groupRequired[FGSVACNO]]" 
									style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" value="" />
								</span>
								</span>
							</div>	


						</div>
						<input id="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
						<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
					</div>	
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>