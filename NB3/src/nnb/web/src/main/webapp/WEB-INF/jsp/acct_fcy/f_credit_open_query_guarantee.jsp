<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			init();
			datetimepickerEvent();
			getTmr();
			// 初始化時隱藏span
// 			$("#hideblock_CMSDATE").hide();
// 			$("#hideblock_CMEDATE").hide();
		});

		function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			//btn 
			$("#CMSUBMIT").click(function (e) {
				e = e || window.event;
// 				if(checkTimeRange() == false )
// 				{
// 					return false;
// 				}
				//打開驗證隱藏欄位
// 				$("#hideblock_CMSDATE").show();
// 				$("#hideblock_CMEDATE").show();
				//塞值進span內的input
// 				$("#validate_CMSDATE").val($("#CMSDATE").val());
// 				$("#validate_CMEDATE").val($("#CMEDATE").val());
				console.log("submit~~");
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").attr("action", "${__ctx}/FCY/ACCT/f_credit_open_query_guarantee_result");
					$("#formId").submit();
				}
			});
			//重新輸入
			$("#CMRESET").click(function(e){
				$("#formId")[0].reset();
				getTmr();
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/FCY/ACCT/f_credit_open_query';
    			$('#back').val("Y");
    			$("#formId").attr("action", action);
    			initBlockUI();
    			$("#formId").submit();
    		});
		} // init END
		//選項
	 	function formReset(){
// 	 		if(checkTimeRange() == false )
// 			{
// 				return false;
// 			}
			//打開驗證隱藏欄位
// 			$("#hideblock_CMSDATE").show();
// 			$("#hideblock_CMEDATE").show();
			//塞值進span內的input
// 			$("#validate_CMSDATE").val($("#CMSDATE").val());
// 			$("#validate_CMEDATE").val($("#CMEDATE").val());
			
			if(!$("#formId").validationEngine("validate")){
				e = e || window.event;//forIE
				e.preventDefault();
			}
	 		else{
		 		//下載文件(未設定)
// 				initBlockUI();
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/f_letter_of_credit_opening_inquiry_guarantee_result.xls");
		 		}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
				    $("#templatePath").val("/downloadTemplate/f_letter_of_credit_opening_inquiry_guarantee_result.txt");
		 		}
	    		$("form").attr("action", "${__ctx}/FCY/ACCT/f_credit_open_query_guarantee_ajaxDirectDownload");
	    		$("#formId").attr("target", "");
	            $("#formId").submit();
	            $('#actionBar').val("");
// 				ajaxDownload("${__ctx}/FCY/ACCT/f_credit_open_query_guarantee_ajaxDirectDownload","formId","finishAjaxDownload()");
	 		}
		}
		function finishAjaxDownload(){
			$("#actionBar").val("");
			unBlockUI(initBlockId);
		}
		// 日曆欄位參數設定
		function datetimepickerEvent() {
			$(".CMDATE").click(function (event) {
				$('#CMDATE').datetimepicker('show');
			});
			$(".CMSDATE").click(function (event) {
				$('#CMSDATE').datetimepicker('show');
			});
			$(".CMEDATE").click(function (event) {
				$('#CMEDATE').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
		//預約自動輸入今天
		function getTmr() {
			var today = new Date("${time_now}");
			today.setDate(today.getDate());
			var y = today.getFullYear();
			var m = today.getMonth() + 1;
			var d = today.getDate();
			if(m<10){
				m = "0"+m
			}
			if(d<10){
				d="0"+d
			}
			var tmr = y + "/" + m + "/" + d
			$('#CMSDATE').val(tmr);
			$('#CMEDATE').val(tmr);
			$("#baseDate").val(tmr);
		}
		
		function checkTimeRange()
		{
			var now = Date.now("${time_now}");
			var twoYm = 63115200000;
			var twoMm = 5259600000;
			var oneYm = 31622400000;
			
			
			var startT = new Date( $('#CMSDATE').val() );
			var endT = new Date( $('#CMEDATE').val() );
			var NSDistance = now - startT;
			var range = endT - startT;
			var NEDistance = now - endT;
			
			
			var limitS = new Date(now - oneYm + 86400000);
			if(NSDistance >= oneYm){
				var m = limitS.getMonth() + 1;
				var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
				// 起始日不能小於
				var msg = '<spring:message code="LB.Start_date_check_note_1" />' + time;
				//alert(msg);
				errorBlock(
						null, 
						null,
						[msg], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
			}
			else if(NSDistance < 0){
				var m = now.getMonth() + 1;
				var time = now.getFullYear() + '/' + m + '/' + now.getDate();
				// 起始日不能大於
				var msg = '<spring:message code= "LB.X1472" />' + time;
				//alert(msg);
				errorBlock(
						null, 
						null,
						[msg], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
			}
			else{
				if(range < 0){
					var msg = '<spring:message code= "LB.X1193" />';
					//alert(msg);
					errorBlock(
						null, 
						null,
						[msg], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
					return false;
				}else if(NEDistance < 0){
					var m = now.getMonth() + 1;
					var time = now.getFullYear() + '/' + m + '/' + now.getDate();
					// 終止日不能大於
					var msg = '<spring:message code="LB.End_date_check_note_1" />' + time;
					//alert(msg);
					errorBlock(
						null, 
						null,
						[msg], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
					return false;
				}
			}
			
			return true;
		}
		
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 開狀查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0098" /></li>
    <!-- 擔保信用狀/保證函明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0006" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--擔保信用狀/保證函明細(未輸入) -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.X0006" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
							<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>	
				
				<form id="formId" method="post">
                	<input type="hidden" id="back" name="back" value="">
                    <input type="hidden" name="USERDATA_X50" value="" />
					<!-- 下載用 -->
                    <input type="hidden" name="downloadFileName" value="<spring:message code="LB.X0006" />" />
                    <input type="hidden" name="downloadType" id="downloadType" />
                    <input type="hidden" name="templatePath" id="templatePath" />
                    <input type="hidden" name="hasMultiRowData" value="false"/>
                    <!-- EXCEL下載用 -->
                    <input type="hidden" name="headerRightEnd" value="9" />
                    <input type="hidden" name="headerBottomEnd" value="7" />
                    <input type="hidden" name="rowStartIndex" value="8" />
                    <input type="hidden" name="multiRowDataListMapKey" value="rowListMap" />
                    <input type="hidden" name="rowRightEnd" value="9" />
                    <input type="hidden" name="footerStartIndex" value="10" />
                    <input type="hidden" name="footerEndIndex" value="13" />
                    <input type="hidden" name="footerRightEnd" value="2" />
                    <!-- TXT下載用 -->
                    <input type="hidden" name="txtHeaderBottomEnd" value="11"/>
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="true"/>
					<input type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap"/>
					
                	<input type="hidden" id="baseDate" name="baseDate" value="">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 查詢區間區塊 -->
								<div class="ttb-input-item row">
									<!--查詢區間  -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Inquiry_period" /></h4>
										</label>
									</span>
									<span class="input-block">
										<!--  指定日期區塊 -->
										<div class="ttb-input">
											<!--期間起日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.D0013" />
												</span>
												<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" value="" />
												<span class="input-unit CMSDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CMSDATE" >
												<!-- 驗證用的input -->
<!-- 												<input id="validate_CMSDATE" name="validate_CMSDATE" type="text" class="text-input validate[required, verification_date[validate_CMSDATE]]"  -->
<!-- 													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
												</span>
											</div>
											<!--期間迄日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_end_date" />
												</span>
												<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" value="" />
												<span class="input-unit CMEDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CMEDATE" >
												<!-- 驗證用的input -->
<!-- 												<input id="validate_CMEDATE" name="validate_CMEDATE" type="text" class="text-input validate[required, verification_date[validate_CMEDATE]]"  -->
<!-- 													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
												<input id="validate_CMSDATE" name="validate_CMSDATE" type="text" class="text-input
													validate[funcCallRequired[validate_CheckDateScope['sFieldName', baseDate, CMSDATE, CMEDATE, false, 12, null]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
										</div>
									</span>
								</div>
								<!-- 信用狀號碼區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 信用狀號碼  -->
											<h4><spring:message code="LB.W0104" /></h4>
										</label>
									</span>
									<div class="ttb-input">
										<input type="text" id="LCNO" name="LCNO" class="text-input" maxlength="20" value="" />
										<br>
										<span class="input-unit"><spring:message code="LB.Blank_For_ALL" /></span>
									</div>
								</div>
							</div>
							<!-- 網頁顯示 button-->
							
	                            <!--回上頁 -->
	                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
	                            <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
								<!-- 重新輸入 -->
<%-- 								<spring:message code="LB.Re_enter" var="cmRest"></spring:message> --%>
<%-- 								<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray"> --%>
								<!--網頁顯示 -->
								<spring:message code="LB.Display_as_web_page" var="cmSubmit"></spring:message>
								<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
							
						</div>
					</div>
					<!-- 說明： -->
				    <ol class="description-list list-decimal">
				    	<p><spring:message code="LB.Description_of_page"/></p>
				        <li><span><spring:message code="LB.F_Credit_Open_Query_P1_D1"/></span></li>
				        <li><span><spring:message code="LB.F_Credit_Open_Query_P1_D2"/></span></li>
				        <li><span><spring:message code="LB.F_Credit_Open_Query_P1_D3"/></span></li>
				    </ol>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>