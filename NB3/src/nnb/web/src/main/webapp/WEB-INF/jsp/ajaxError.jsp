<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="./__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="./__import_head_tag.jsp"%>
<%@ include file="./__import_js.jsp"%>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="./index/header.jsp"%>
		</header>
		<!-- 麵包屑 -->
		<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
			<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
				<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			</ol>
		</nav>
		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="./index/menu.jsp"%>
		</div>
		<!-- content row END -->
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Transaction_result" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-result-block">
							</div>
							<ul class="ttb-result-list">
								<li>
									<h3><spring:message code="LB.Transaction_code"/></h3>
									<p>${errorCode}</p>
								</li>
								<li>
									<h3><spring:message code="LB.Transaction_message"/></h3>
									<p>${errorMessage}</p>
								</li>
							</ul>
						</div>
					</div>
			</section>
		</main>
		<!-- 		main-content END --> 
	<!-- 	content row END -->
		<%@ include file="./index/footer.jsp"%>
	</body>
</html>