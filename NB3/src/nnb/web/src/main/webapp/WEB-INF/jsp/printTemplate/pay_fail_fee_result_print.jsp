<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
			
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						交易時間 -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<%-- 						${CMTXTIME} --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						黃金轉出帳號 -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<%-- 						${ACN} --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						扣款失敗手續費<br />(繳款金額) -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<%-- 						${TRNAMT} --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						轉出帳號 -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<%-- 						${SVACN} --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						轉出帳號帳戶餘額 -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<!-- 新臺幣 -->
<!-- 元 -->
<%-- 						<spring:message code= "LB.D0508" />${O_TOTBAL}<spring:message code= "LB.Dollar" /> --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
<!-- 			<div class="ttb-input-item row"> -->
<!-- 				<span class="input-title"> -->
<!-- 					<label> -->
<!-- 						轉出帳號可用餘額 -->
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 				<span class="input-block"> -->
<!-- 					<label> -->
<!-- 新臺幣 -->
<!-- 元 -->
<%-- 						<spring:message code= "LB.D0508" />${ O_AVLBAL }<spring:message code= "LB.Dollar" /> --%>
<!-- 					</label> -->
<!-- 				</span> -->
<!-- 			</div> -->
			<table class="print">
				<tr>
<!-- 交易時間 -->
					<td style="text-align: center"><spring:message code= "LB.D0019" /></td>
					<td>${CMTXTIME}</td>
				</tr>
				<tr>
<!-- 黃金存摺帳號 -->
					<td style="text-align: center"><spring:message code= "LB.D1090" /></td>
					<td>${ACN}</td>
				</tr>
				<tr>
<!-- 扣款失敗手續費 -->
<!-- 繳款金額 -->
					<td style="text-align: center"><spring:message code= "LB.W1542" /><br />(<spring:message code= "LB.D0021" />)</td>
<!-- 元 -->
					<td>${TRNAMT}<spring:message code= "LB.Dollar" /></td>
				</tr>
				<tr>
<!-- 轉出帳號 -->
					<td style="text-align: center"><spring:message code= "LB.D0465" /></td>
					<td>${SVACN}</td>
				</tr>
				<tr>
<!-- 轉出帳號帳戶餘額 -->
					<td style="text-align: center"><spring:message code= "LB.W1550" /></td>
<!-- 新臺幣 -->
<!-- 元 -->
					<td><spring:message code= "LB.NTD" />${O_TOTBAL}<spring:message code= "LB.Dollar" /></td>
				</tr>
				<tr>
<!-- 轉出帳號可用餘額 -->
					<td style="text-align: center"><spring:message code= "LB.W0283" /></td>
<!-- 新臺幣 -->
<!-- 元 -->
					<td><spring:message code= "LB.NTD" />${ O_AVLBAL }<spring:message code= "LB.Dollar" /></td>
				</tr>
			</table>
		</div>
		
		<br>
		<br>
		<div class="text-left">
			<spring:message code="LB.Description_of_page" />
			<ol class="list-decimal text-left">
			 <!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
	             <li><spring:message code="LB.Pay_Fail_Fee_P3_D1"/></li>
			</ol>
		</div>
	</div>
	
</body>
</html>