<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>
<body calss="watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
			<table class="print">
				<tr>
					<td style="text-align: center">
						<spring:message code="LB.Inquiry_time" />
					</td>
					<td>
						${CMQTIME }
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.L/C_no" />
					</td>
					<td>
						${RLCNO }
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.W0089" />
					</td>
					<td>
						${ROPENDAT }
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.X0021" />
					</td>
					<td>
						${RAMDNO } <spring:message code="LB.Time_s" />
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.W0094" />
					</td>
					<td>
						${RLCCCY } ${RORIGAMT }
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.X0011" />
					</td>
					<td>
						${RALLOW } %
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.X0012" />
					</td>
					<td>
						${RMARGOS }
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.X0013" />
					</td>
					<td>
						${RLCCCY } ${RMAXDRAW }
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.X0014" />
					</td>
					<td>
						${RLCCCY } ${RLCOS }
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.W0118" />
					</td>
					<td>
						${RADVBK } ${RADVNM }
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.X0015" />
					</td>
					<td>
						${RBENNAME }
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.X0016" />
					</td>
					<td>
						<spring:message code= "LB.X0018" />${RTXTENOR }<spring:message code= "LB.W1121" /><br><spring:message code= "LB.X0017" />${RTENOR }<spring:message code= "LB.W1121" />
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.X0020" />
					</td>
					<td>
						${REXPDATE }
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.X0019" />
					</td>
					<td>
						${RSHPDATE }
					</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code="LB.Note" />
					</td>
					<td>
						${RMARK }
					</td>
				</tr>
			</table>
		</div>
	</div>
	
	<br>
	<br>
</body>
</html>