<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<style>
	.Line-break{
		white-space:normal;
		word-break:break-all;
		width:100px;
		word-wrap:break-word;
	}
</style>
<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				
				setTimeout("initDataTable()",100);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);
				shwd_prompt_init(false);
							
			});
			
			function init(){
				$("#printbtn").click(function(){
					var params = {
							"jspTemplateName":"fund_regular_invest_query_result_print",
							"jspTitle":"<spring:message code= "LB.W1039" />",
							"CMQTIME":"${regular_invest_query_result.data.CMQTIME}",
							"TOTRECNO":"${regular_invest_query_result.data.TOTRECNO}",
							"NAME":"${regular_invest_query_result.data.NAME}"
						};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});
				
			}
			
			function changeIcon(id) {
			 	var status = $('#'+id)[0].src
				if (status.indexOf('asc') == '-1'){
					 $('#'+id).attr("src","${__ctx}/img/order_asc.gif")
				}
				else{
					$('#'+id).attr("src","${__ctx}/img/order_desc.gif")
				}
			    if (id == 'img1') {
			    	$('#img2,#img3,#img4').attr("src","${__ctx}/img/order_desc.gif");	    	
			    }
			    else if (id == 'img2') {       	  
			    	$('#img1,#img3,#img4').attr("src","${__ctx}/img/order_desc.gif");
			    }
			    else if (id == 'img3') {
			    	$('#img2,#img1,#img4').attr("src","${__ctx}/img/order_desc.gif");
			    }
			    else if (id == 'img4') {
			    	$('#img2,#img3,#img1').attr("src","${__ctx}/img/order_desc.gif");
			    }
			 }
			var lastcol, lastseq;
			function sortTable( ai_sortcol, ab_header, isNumber )
			{
				
				ao_table = document.getElementById("RESULTTABLE");
			    var ir, ic, is, ii, id;
				
			    ir = ao_table.rows.length;
			    
			    if( ir < 1 ) return;

			    ic = ao_table.rows[1].cells.length;
			    
			    // if we have a header row, ignore the first row
			    //if( ab_header == true ) is=1; else is=0;
			    
			    is = 2;
			    	
			    // take a copy of the data to shuffle in memory
			    var row_data = new Array( ir );
			    ii=0;
			    for( i=is; i < ir; i++ )
			    {
			        var col_data = new Array( ic );
			        for( j=0; j < ic; j++ )
			        {
			            col_data[j] = ao_table.rows[i].cells[j].innerHTML;
			        }
			        row_data[ ii++ ] = col_data;
			    }

			    // sort the data
			    var bswap = false;
			    var row1, row2;
			    
			    if( ai_sortcol != lastcol )
			        lastseq = 'A';
			    else
			    {
			        if( lastseq == 'A' ) lastseq = 'D'; else lastseq = 'A';
			    }

			    // if we have a header row we have one less row to sort
			    //if( ab_header == true ) id=ir-1; else id=ir;
			    
			    id = ir-2;
			    
			    for( i=0; i < id; i++ )
			    {
			        bswap = false;
			        for( j=0; j < id - 1; j++ )
			        {
			            // test the current value + the next and
			            // swap if required.
			            row1 = row_data[j];
			            row2 = row_data[j+1];
			            
			            /*
			            if( lastseq == "A" )
			            {
			                if( row1[ ai_sortcol ] > row2[ ai_sortcol ] )
			                {
			                    row_data[j+1] = row1;
			                    row_data[j] = row2;
			                    bswap=true;
			                }
			            }
			            else
			            {
			                if( row1[ ai_sortcol ] < row2[ ai_sortcol ] )
			                {
			                    row_data[j+1] = row1;
			                    row_data[j] = row2;
			                    bswap=true;
			                }
			            }
			            */


			            if( lastseq == "A" )
			            {                        
			            	if (isNumber == true) //表示金額排序
			            	{

			                if( sortAmount(row1[ ai_sortcol ], row2[ ai_sortcol ]) == '>' )
			                {
			                    row_data[j+1] = row1;
			                    row_data[j] = row2;
			                    bswap=true;
			                }
			                
			            	}
			            	else 
			              {                      
			                if( row1[ ai_sortcol ] > row2[ ai_sortcol ] )
			                {
			                    row_data[j+1] = row1;
			                    row_data[j] = row2;
			                    bswap=true;
			                }
			              }  
			            }
			            else
			            {
			            	if (isNumber == true) //表示金額排序
			            	{

			                if( sortAmount(row1[ ai_sortcol ], row2[ ai_sortcol ]) == '<' )
			                {
			                    row_data[j+1] = row1;
			                    row_data[j] = row2;
			                    bswap=true;
			                }                            	            
			                
			            	}
			            	else 
			              { 
			                if( row1[ ai_sortcol ] < row2[ ai_sortcol ] )
			                {
			                    row_data[j+1] = row1;
			                    row_data[j] = row2;
			                    bswap=true;
			                }                            
			              }                
			            }            
			            
			        }
			        if( bswap == false ) break;
			    }

			    // load the data back into the table
			    ii = is;
			    for( i=0; i < id; i++ )
			    {	
			        row1 = row_data[ i ];
			        for( j=0; j < ic; j++ )
			        { 	
			            ao_table.rows[ii].cells[j].innerHTML = row1[j];
			        }
			        ii++;
			    }
			    lastcol = ai_sortcol;
			}
			
		function barfunction(value,CDNO){
			switch (value){
			case "C111":
				$('#CDNO').val(CDNO);
				$('#formId').submit();
			break;
			}
		}
		
		//快速選單測試新增
	 	//將actionbar select 展開閉合
		$(function(){
	   		$("#click").on('click', function(){
	       		var s = $("#actionBar2").attr('size')==1?8:1
	       		$("#actionBar2").attr('size', s);
	   		});
	   		$("#actionBar2 option").on({
	       		click: function() {
	           		$("#actionBar2").attr('size', 1);
	       		},
	   		});
		});
		function hd2(T){
			var t = document.getElementById(T);
			if(t.style.visibility === 'visible'){
				t.style.visibility = 'hidden';
			}
			else{
				$("div[id^='actionBar2']").each(function() {
					var d = document.getElementById($(this).attr('id'));
					d.style.visibility = 'hidden';
			    });
				t.style.visibility = 'visible';
			}
		}
		</script>
</head>
<body>
    <c:set var="SHWD" value="${regular_invest_query_result.data.SHWD}"></c:set>
    <%@ include file="fund_shwd.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 定期投資約定資料查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1039" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<!-- 	快速選單及主頁內容 -->
	<main class="col-12"> 
		<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.W1039" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form method="post" id="formId" action="${__ctx}/FUND/ALTER/regular_investment">
				<input type="hidden" name="CDNO" id="CDNO" value>
			</form>
			<div class="main-content-block row">
				<div class="col-12 tab-content">
					<ul class="ttb-result-list">
						<li>
							<!-- 查詢時間 -->
							<h3>
								<spring:message code="LB.Inquiry_time" />
							</h3>
							<p>
								${regular_invest_query_result.data.CMQTIME}
							</p>
						</li>
						<li>
							<!-- 查詢筆數 -->
							<h3>
								<spring:message code="LB.Total_records" />
							</h3>
							<p>
								${regular_invest_query_result.data.TOTRECNO} <spring:message code="LB.Rows" />
							</p>							
						</li>
					</ul>
					<ul class="ttb-result-list">
						<li>
							<h3>
								<spring:message code="LB.Name"/>
							</h3>
							<p>
								${regular_invest_query_result.data.NAME}
							</p>
						</li>
					</ul>
					<table id="RESULTTABLE" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
					<thead style="">
<%-- 						<tr> --%>
<%-- 							<th><spring:message code="LB.Name"/></th> --%>
<%-- 							<th colspan="12" style="text-align:left">${regular_invest_query_result.data.NAME}</th> --%>
<%-- 						</tr> --%>
						<tr>
							<!-- 申請日期 --><!-- 扣款標的 -->
							<th><spring:message code="LB.D0127"/><hr/><spring:message code="LB.W1041"/></th>
							<!-- 類別 --><!-- 扣款方式 -->
							<th><spring:message code="LB.D0973"/><hr><spring:message code="LB.D0173"/></th>
							<!-- 每次定額申購金額+不定額基準申購金額 -->
							<th><spring:message code="LB.W1043"/>+<br><spring:message code="LB.W1044"/></th>
							<!-- 扣款帳號/卡號 -->
							<th><spring:message code="LB.W1046"/></th>
							<!-- 扣款日 --><!-- 首次申購日 -->
							<th><spring:message code="LB.W1047"/><hr><spring:message code="LB.W0906"/></th>
							<!-- 暫停扣款起日 暫停扣款迄日 -->
							<th><spring:message code="LB.W1049"/><hr><spring:message code="LB.W1050"/></th>
							<!-- 終止扣款日 --><!-- 扣款約定 -->
							<th><spring:message code="LB.W1051"/><hr><spring:message code="LB.W1052"/></th>
							<!-- 憑證 -->
							<th><spring:message code="LB.W1053"/></th>
							<!-- 信託帳號/快速選單 -->
							<th><spring:message code="LB.W0944"/><hr><spring:message code="LB.W1055"/></th>
						</tr>
					</thead>
					<tbody style="">
						<c:forEach var="dataList" items="${regular_invest_query_result.data.REC}" varStatus="data">
							<tr>
								<!-- 申請日期 --><!-- 扣款標的 -->
								<td class="text-center Line-break">${dataList.CONDAY}<hr/>${dataList.PAYTAG}</td>
								<!-- 類別 --><!-- 扣款來源 -->
								<td class="text-center"><spring:message code="${dataList.MIP}"/><hr><spring:message code="${dataList.DBTTYPE}"/></td>
								<!-- 每次定額申購金額+不定額基準申購金額 -->
								<td class="text-center" >${dataList.PAYCUR}<br/>${dataList.PAYAMT}</td>
								<!-- 扣款帳號/卡號 -->
								<td class="text-center">${dataList.PAYACNO}<br/>
									<c:if test="${dataList.SALFLG == 'Y'}">
										<font style="color:red; font-weight: bold"><spring:message code="LB.X2604" /></font>
									</c:if>
								</td>
								<!-- 扣款日 --><!-- 首次申購日 -->
								<td class="text-center Line-break">${dataList.PAYDAY1} ${dataList.PAYDAY2} ${dataList.PAYDAY3}
								 ${dataList.PAYDAY4} ${dataList.PAYDAY5} ${dataList.PAYDAY6} 
								 ${dataList.PAYDAY7} ${dataList.PAYDAY8} ${dataList.PAYDAY9}<hr>${dataList.FSTDAY}</td>
								<!-- 暫停扣款起日 暫停扣款迄日 -->
								<td class="text-center">${dataList.STOPBEGDAY}<hr>${dataList.STOPENDDAY}</td>
								<!-- 終止扣款日 --><!-- 扣款約定 -->
								<td class="text-center">
									${dataList.STPDAY}
									<hr>
									<c:if test="${dataList.REMARK1 !=''}">
										<spring:message code="${dataList.REMARK1}"/>
									</c:if>
									<c:if test="${dataList.REMARK1 ==''}">
										${dataList.REMARK1}
									</c:if>
								</td>
								<!-- 憑證 -->
								<td class="text-center">${dataList.REMARK2}</td>
								<!-- 信託帳號/快速選單 -->
								<td class="text-center">
									${dataList.CDNOCover}<br>
<!-- 									下拉式選單	 -->
									<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')" value="..." />
<%-- 									<button class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')">...</button>			 --%>
<%--     								<input type="button" class="d-sm-none d-block" id="click" value="Click" onclick="hd2('actionBar2${data.index}')"/><!-- 快速選單測試 --> --%>
									<select id="actionBar" class="d-none d-lg-inline-block custom-select fast-select" onchange="barfunction(this.value,'${dataList.CDNO}', '${dataList.SAL03}');" style="width:100px">
											<option>-<spring:message code="LB.Select"/>-</option>
											<option value="C111"><spring:message code="LB.W1057"/></option>
									</select>
<!-- 									快速選單測試新增div -->
 					        		<div id="actionBar2${data.index}" class="fast-div-grey" style="visibility: hidden">
 					        			<div class="fast-div">
											<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${data.index}')">
<!-- 											快捷操作                              				 -->
											<p><spring:message code= "LB.X1592" /></p>
											<ul>
												<a href="${__ctx}/FUND/ALTER/regular_investment?CDNO=${dataList.CDNO}"><li><spring:message code="LB.W1057" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
											</ul>
											<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${data.index}')" value='<spring:message code= "LB.X1572"/>' />
										</div>
									</div>
								</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<!-- 關閉 -->
					<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
				</div>
			</div>
			<div style="text-align: center;">
				<font style="color:red;font-size: 15px;"><spring:message code="LB.Profitloss_Balance_P1_D2" /> </font>
			</div>
<!-- 			<div style="text-align:center"> -->
<!-- 				<font style="color:red;font-size: 15px">如需列印，請將印表機設定為「橫印列印方向」，以列印全部內容。</font> -->
<!-- 				</div> -->
 			<ol class="description-list list-decimal">
				<p><spring:message code="LB.Description_of_page" /></p>
					<li><span><spring:message code="LB.Regular_Invest_P1_D1" /></span></li>
					<li><span><spring:message code="LB.Regular_Invest_P1_D2" /></span></li>
			</ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>