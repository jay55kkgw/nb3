<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {

	    	//列印
        	$("#PRINT").click(function(){
				var params = {
					"jspTemplateName":"upload_digital_identity_result_print",
					"jspTitle":'<spring:message code= "LB.X0491" />',
					"BRHNAME":"${result_data.data.BRHNAME}",
					"BRHTEL":"${result_data.data.BRHTEL}"
				};
				openWindowWithPost("${__ctx}/ONLINE/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
			$("#GOBAKE").click(function(e){
	        	var action = '${__ctx}/login';
				$("form").attr("action", action);
    			$("form").submit();
			});
	    	
		}
		
	</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--信用卡 -->
			<li class="ttb-breadcrumb-item"><spring:message code= "LB.Credit_Card" /></a></li>
			<!--信用卡補上傳資料(身分證件/財力證明) -->
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.onlineapply_creditcard_progress_TITLE" /></a></li>
		</ol>
	</nav>
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code= "LB.onlineapply_creditcard_progress_TITLE" />
				</h2>
				<div id="step-bar">
                    <ul>
						<li class="finished"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
						<li class="finished"><spring:message code="LB.onlineapply_creditcard_progress_OTP" /></li><!-- OTP驗證 -->
						<li class="finished"><spring:message code="LB.D0359" /></li><!-- 查詢結果 -->
						<li class="finished"><spring:message code="LB.onlineapply_creditcard_progress_UploadDocuments" /></li><!-- 上傳證件 -->
						<li class="finished"><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
						<li class="active"><spring:message code="LB.onlineapply_creditcard_progress_COMPLETE" /></li><!-- 完成補件 -->
                    </ul>
                </div>
				<form method="post" id="formId" name="formId">
					<input type="hidden" name="TYPE" value="5">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
		                            <p><spring:message code="LB.onlineapply_creditcard_progress_COMPLETE" /><!-- 完成補件 --></p>
		                        </div>
							</div>
						<!-- <input type="button" id="PRINT" value="<spring:message code="LB.Print" />" class="ttb-button btn-flat-gray" /> -->
						<input type="button" id="GOBAKE" value="<spring:message code="LB.X0956" />" class="ttb-button btn-flat-orange"/>
						<input type="button" id="CLOSE" value="<spring:message code="LB.X0282" />" class="ttb-button btn-flat-orange"  onClick="window.close();"/>
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>