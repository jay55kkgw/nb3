<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
  <br /><br />
  <div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
  <br /><br />
  <div style="text-align:center">
    <font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
  </div>
  <br />
  <div style="text-align:center">
    ${MsgName}
  </div>
  <br />
  <table class="print">
  <!-- 交易時間-->
    <tr>
      <td style="width:10em"><spring:message code="LB.Trading_time" /></td>
      <td>
        ${CMQTIME}
      </td>
    </tr>
    <!-- 身分證字號/統一編號-->
    <tr>
      <td><spring:message code="LB.Id_no" /></td>
      <td>
        ${hideid_CUSIDN}
      </td>
    </tr>
    <!--信託帳號-->
    <tr>
      <td><spring:message code="LB.W0944" /></td>
      <td>
        ${hideid_CDNO}
      </td>
    </tr>
    <!--基金名稱-->
    <tr>
      <td><spring:message code="LB.W0025" /></td>
      <td>
        ${FUNDLNAME}
      </td>
    </tr>
    	<!--類別-->
    <tr>
      <td><spring:message code="LB.D0973" /></td>
      <td>
        ${str_AC202}
      </td>
    </tr>
    <!--信託金額-->
    <tr>
      <td><spring:message code="LB.W0026" /></td>
      <td>
        <c:if test="${FUNDAMT != '0.00'}">
          ${display_FUNDCUR }
          ${display_FUNDAMT }
        </c:if>
      </td>
    </tr>
    <!-- 每次定額申購金額＋<br>不定額基準申購金額  -->
    <tr>
      <!-- 每次定額申購金額＋<br>不定額基準申購金額  -->
      <td>${show_PAYAMT}</td>
      <td>
        <c:if test="${PAYAMT != '0'}">
          ${display_PAYCUR }
          ${PAYAMT1 }
        </c:if>
      </td>
    </tr>
    <!--停利點-->
    <tr>
      <td><spring:message code="LB.W1174" /></td>
      <td> ${NSTOPPROF} %</td>
    </tr>
   <!--停損點-->
    <tr>
      <td><spring:message code="LB.W1175" /></td>
      <td>-${NSTOPLOSS} %</td>
    </tr>
  </table>
  <br>
  <br>
</body>

</html>