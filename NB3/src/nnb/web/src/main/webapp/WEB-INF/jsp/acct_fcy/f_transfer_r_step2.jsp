<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
	<script type="text/JavaScript">
		
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 100);
		});

		
		function init() {
			// 確認鍵 click
			goOn();
			// 上一頁按鈕 click
// 			goBack();
		}

		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				// 送出表單
				$("#formId").submit();
			});
		}

		// 上一頁按鈕 click
// 		function goBack() {
// 			// 上一頁按鈕
// 			$("#pageback").click(function() {
// 				// 回上一頁到輸入頁ESAPI驗證jsondc會壞掉故不送jsondc
// 				$("#jsondc").prop("disabled",true);
				
// 				// 遮罩
// 				initBlockUI();
				
// 				// 解除表單驗證
// 				$("#formId").validationEngine('detach');
				
// 				// 讓Controller知道是回上一頁
// 				$('#back').val("Y");
// 				// 回上一頁
// 				var action = '${pageContext.request.contextPath}' + '${previous}';
// 				$("#formId").attr("action", action);
// 				$("#formId").submit();
// 			});
// 		}

	</script>
</head>
<body>
	<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 買賣外幣/約定轉帳     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Exchange_Transfer" /></li>
		</ol>
	</nav>



	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
	
		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<!-- 功能內容 -->
				<form method="post" id="formId" action="${__ctx}/FCY/TRANSFER/f_transfer_r_step3">
					<input type="hidden" name="ADAGREEF" value="${transfer_data.data.adagreef}">
					<input type="hidden" name="ADCURRENCY" value="${transfer_data.data.PAYCCY}">
					<input type="hidden" name="CMTRANPAGE" value="1">
				    				    
				    <input type="hidden" name="TXTOKEN" value="${transfer_data.data.TXTOKEN}"><!-- 防止不正常交易 -->
					<input type="hidden" id="back" name="back" value=""><!-- 回上一頁資料 -->
					<input type="hidden" name="CMTRMAIL" value="${transfer_data.data.CMTRMAIL}"><!--轉出成功Email通知-->
				    
					<!-- 功能名稱 -->
					<h2><spring:message code="LB.FX_Exchange_Transfer" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
					<!-- 交易流程階段 -->
<!-- 					<div id="step-bar"> -->
<!-- 						<ul> -->
<%-- 							<li class="active"><spring:message code="LB.Enter_data" /></li> --%>
<%-- 							<li class=""><spring:message code="LB.Confirm_data" /></li> --%>
<%-- 							<li class=""><spring:message code="LB.Transaction_complete" /></li> --%>
<!-- 						</ul> -->
<!-- 					</div> -->
					
					<!-- 功能內容 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<spring:message code="LB.Confirm_transfer_data" />
									</span>
								</div>
								
								<!-- 轉帳日期 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transfer_date" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.PAYDATE }
										</div>
									</span>
								</div>
	
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payers_account_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.CUSTACC }
										</div>
									</span>
								</div>
							
								<!-- 轉帳金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Amount" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.str_Curr }
											
											<c:choose>
												<c:when test="${transfer_data.data.str_OutAmt != ''}">
													${transfer_data.data.str_OutAmt }
												</c:when>
												<c:when test="${transfer_data.data.str_InAmt != ''}">
													${transfer_data.data.str_InAmt }
												</c:when>
											</c:choose>
											
											&nbsp;<spring:message code="LB.Dollar" />
										</div>
									</span>
								</div>
							
								<!-- 轉入帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payees_account_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.BENACC }
										</div>
									</span>
								</div>
							
							</div>
							
							<input class="ttb-button btn-flat-gray" name="CMBACK" type="button" value="<spring:message code="LB.Back_to_previous_page" />" onclick="history.go(-1)" />
<%-- 							<input class="ttb-button btn-flat-gray" id="pageback" type="button" value="<spring:message code="LB.Back_to_previous_page" />" /> --%>
							<input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Obtain_exchange_rate_and_bargaining_number" />" />
							
						</div>
					</div>
					
					<div class="text-left">
						<font color="#FF0000"><spring:message code="LB.Confirm_exchange_rate" /></font>
					</div>
					
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>

</body>
</html>
