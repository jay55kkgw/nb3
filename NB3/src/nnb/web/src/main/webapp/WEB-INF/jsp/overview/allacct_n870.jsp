<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<script type="text/javascript">
	// n870-中央登錄債券餘額
	i18n['BONCOD'] = '<spring:message code="LB.W0036" />'; // 債券代號
	i18n['DPIBAL'] = '<spring:message code="LB.W0017" />'; // 餘額
	i18n['TSFBAL_N870'] = '<spring:message code="LB.W0038" />'; // 可動支餘額
	i18n['LMTSFO'] = '<spring:message code="LB.W0039" />'; // 限制性轉出餘額
	i18n['LMTSFI'] = '<spring:message code="LB.W0040" />'; // 限制性轉入餘額
	i18n['RPBAL'] = '<spring:message code="LB.W0041" />'; // 附條件簽發餘額

	// DataTable Ajax tables
	var n870_columns = [ {
		"data" : "BONCOD",
		"title" : i18n['BONCOD']
	}, {
		"data" : "DPIBAL",
		"title" : i18n['DPIBAL']
	}, {
		"data" : "TSFBAL_N870",
		"title" : i18n['TSFBAL_N870']
	}, {
		"data" : "LMTSFO",
		"title" : i18n['LMTSFO']
	}, {
		"data" : "LMTSFI",
		"title" : i18n['LMTSFI']
	}, {
		"data" : "RPBAL",
		"title" : i18n['RPBAL']
	}, ];

	var n870_error_columns = [ {
		"data" : "msgCode",
		"title" : '<spring:message code="LB.Transaction_code" />'
	}, {
		"data" : "msgName",
		"title" : '<spring:message code="LB.Transaction_message" />'
	} ];

	//0置中 1置右
	var n870_align = [ 0, 1, 1, 1, 1, 1 ];

	// Ajax_n870-臺幣活期性存款餘額
	function getMyAssets870() {
		$("#n870").show();
		$("#n870_title").show();
		createLoadingBox("n870_BOX", '');
		uri = '${__ctx}' + "/OVERVIEW/allacct_n870aj";
		console.log("allacct_n870aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax_n870, null, "n870");
	}

	// Ajax_callback
	function countAjax_n870(data) {
		$("#n870_BOX_table_loadingBox").hide();
		// 資料處理後呈現畫面
		if (data.result) {
			var n870_rows = data.data.REC;
			//console.log(rec);
			n870_rows
					.forEach(function(d, i) {
						console.log(d);

						var box = $("#n870_TEMP_BOX").clone();
						var lb = $("#table_temp").clone();
						lb[0].id = "n870_" + i + "_table";
						box.append(lb[0]);
						box[0].children[0].children[0].children[1].innerHTML = d.ACN;
						box.show();
						$("#n870_BOX").append(box);
						if (d.msgCode != '0') {
							createTable("n870_" + i, [ d ], n870_error_columns,
									[ 0, 0 ]);
						} else {
							createTable("n870_" + i, d.Table, n870_columns,
									n870_align);
						}

					});
		} else {
			// 錯誤訊息
			n870_columns = [ {
				"data" : "MSGCODE",
				"title" : '<spring:message code="LB.Transaction_code" />'
			}, {
				"data" : "MESSAGE",
				"title" : '<spring:message code="LB.Transaction_message" />'
			} ];
			n870_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];

		}
		//createTable("n870_BOX",n870_rows,n870_columns,n870_align);
	}
</script>
<p id="n870_title" class="home-title" style="display: none">
	<spring:message code="LB.X2359" />
</p>
<div id="n870" class="main-content-block" style="display: none">
	<div id="n870_BOX">

		<div class="col-12" id='n870_TEMP_BOX' style="display: none">
			<ul class="ttb-result-list">
				<li>
					<h3>
						<spring:message code="LB.Account" />
					</h3>
					<p>ASDF123</p>
				</li>
			</ul>
		</div>
	</div>
</div>