<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

		</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金存摺帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2273" /></li>
    <!-- 黃金存摺帳戶申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1655" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<!-- 線上申請黃金存摺帳戶 -->
			<h2><spring:message code= "LB.W1655" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>	
                <div id="step-bar">
                    <ul>
                        <li class="finished">注意事項與權益</li>
                        <li class="finished">開戶資料</li>
                        <li class="finished">確認資料</li>
                        <li class="active">申請結果</li>
                    </ul>
                </div>
			<!-- 申請完成 -->					
<%-- 			<p style="text-align: center;color: red;"><spring:message code= "LB.X0932" /></p> --%>
			<form method="post" id="formId">
				<input type="hidden" id="back" name="back" value="">
                
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<!-- 申請完成 -->					
							<div class="ttb-message">
								<p>申請結果</p>
								<i class="fa fa-check-circle fa-5x success-color"></i>
								<span>申請成功</span>
                            </div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										申請時間
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.data.CMTIME }</p>
									</div>
								</span>
							</div>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
									<!-- 新開黃金存摺帳號 -->
										<spring:message code= "LB.X0933" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.data.GDACN }</p>
									</div>
								</span>
							</div>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										網路交易指定新台幣帳戶
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ input_data.SVACN }</p>
									</div>
								</span>
							</div>
									<!-- 身分證統一編號 -->
<!-- 							<div class="ttb-input-item row"> -->
<!-- 								<span class="input-title">  -->
<!-- 								<label> -->
<!-- 									<h4> -->
<%-- 										<spring:message code= "LB.D0519" /> --%>
<!-- 									</h4> -->
<!-- 								</label> -->
<!-- 								</span> <span class="input-block"> -->
<!-- 									<div class="ttb-input"> -->
<%-- 										<p>${ input_data.CUSIDN2 }</p> --%>
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
										<!-- 出生年月日 -->
<!-- 							<div class="ttb-input-item row"> -->
<!-- 								<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<%-- 											<spring:message code= "LB.D0054" /> --%>
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 								</span>  -->
<!-- 								<span class="input-block"> -->
<!-- 									<div class="ttb-input"> -->
<%-- 										<p>${ input_data.CMDATE1 }</p> --%>
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
										<!-- 身分證為補發/換發/新發日期 -->
<!-- 							<div class="ttb-input-item row"> -->
<!-- 								<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<%-- 										<spring:message code= "LB.D1112" /> --%>
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 								</span>  -->
<!-- 								<span class="input-block"> -->
<!-- 									<div class="ttb-input"> -->
<!-- 										<p> -->
<%-- 											<c:if test="${ input_data.TYPECHA.equals('1') }"> --%>
<!-- 											初領 -->
<%-- 											<spring:message code= "LB.D1113" /> --%>
<%-- 											</c:if> --%>
<%-- 											<c:if test="${ input_data.TYPECHA.equals('2') }"> --%>
<!-- 											換發 -->
<%-- 											<spring:message code= "LB.W1664" /> --%>
<%-- 											</c:if> --%>
<%-- 											<c:if test="${ input_data.TYPECHA.equals('3') }"> --%>
<!-- 											補發 -->
<%-- 											<spring:message code= "LB.W1665" /> --%>
<%-- 											</c:if> --%>
<!-- 											日 -->
<%-- 											<spring:message code= "LB.Day" />  ${ input_data.CMDATE2 } --%>
<!-- 										</p> -->
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
										<!-- 補換發縣市-->
<!-- 							<div class="ttb-input-item row"> -->
<!-- 								<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<%-- 										<spring:message code= "LB.D1071" /> --%>
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 								</span>  -->
<!-- 								<span class="input-block"> -->
<!-- 									<div class="ttb-input"> -->
<%-- 										<p>${ input_data.CITYCHA }</p> --%>
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
										<!-- 身份證有無相片 -->
<!-- 							<div class="ttb-input-item row"> -->
<!-- 								<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<%-- 											<spring:message code= "LB.D1072" /> --%>
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 								</span>  -->
<!-- 								<span class="input-block"> -->
<!-- 									<div class="ttb-input"> -->
<!-- 										<p> -->
<%-- 											<c:if test="${ input_data.PIC_FLAG.equals('Y') }"> --%>
<%-- 												<spring:message code= "LB.D1070_1" /> --%>
<%-- 											</c:if> --%>
<%-- 											<c:if test="${ input_data.PIC_FLAG.equals('N') }"> --%>
<%-- 												<spring:message code= "LB.D1070_2" /> --%>
<%-- 											</c:if> --%>
<!-- 										</p> -->
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
										<!-- 連絡電話 -->
<!-- 							<div class="ttb-input-item row"> -->
<!-- 								<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<%-- 											<spring:message code= "LB.D0205" /> --%>
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 								</span>  -->
<!-- 								<span class="input-block"> -->
<!-- 									<div class="ttb-input"> -->
<%-- 										<p>(${ input_data.ARACOD2 }) ${ input_data.TELNUM2 }</p> --%>
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
									<!-- 領取黃金存摺支開戶行 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code= "LB.D1088" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.data.BRHNUM }</p>
									</div>
								</span>
							</div>
							
										<!-- 開戶手續費 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code= "LB.D1082" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
									<!-- 新臺幣 XX 元-->
										<p><spring:message code= "LB.NTD" /> ${ result_data.data.AMT_FEE }<spring:message code= "LB.Dollar" /></p>
									</div>
								</span>
								
							</div>
							
						</div>
                        <!-- 列印  -->
                        <spring:message code="LB.Print" var="printbtn"></spring:message>
                        <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-gray" />			
						<input type="button" class="ttb-button btn-flat-orange" id="cmback" value="回首頁"/>			
					</div>
				</div>
			</form>
			<div class="text-left">
			    <!-- 		說明： -->
			    <ol class="list-decimal description-list">
			    	<p><spring:message code="LB.Description_of_page"/></p>
			    <!-- 您所提供的身分證資料，經本行查詢後，如有疑義，本行得暫停您的黃金存摺網路銀行功能。 -->
			        <li><spring:message code= "LB.Gold_Account_Apply_P6_D1" /></li>
			        <!-- 臨櫃領摺須知 -->
			         <li><spring:message code= "LB.Gold_Account_Apply_P6_D2" /><BR>
			         	<table>
			         	<!-- (1) 客戶於營業日下午3:30  以前 線上申請黃金存摺帳戶，如有領摺需求者，請於 次營業日起至黃金存摺帳戶行領取。-->
			         		<tr><td>（1）</td><td><spring:message code= "LB.Gold_Account_Apply_P6_D22" /><font color="red"><u><b><spring:message code= "LB.Gold_Account_Apply_P6_D22-1" /></b></u></font><spring:message code= "LB.Gold_Account_Apply_P6_D22-2" /><font color="red"><u><b><spring:message code= "LB.Gold_Account_Apply_P6_D22-3" /></b></u></font></td></tr>
			         		<!-- (2) 客戶於營業日下午3:30 以後 或 非營業日期間 線上申請黃金存摺帳戶，如有領摺需求者，請於 次次營業日起 至黃金存摺帳戶行領取。-->
			         		<tr><td>（2）</td><td><spring:message code= "LB.Gold_Account_Apply_P6_D23" /><font color="red"><u><b><spring:message code= "LB.Gold_Account_Apply_P6_D23-1" /></b></u></font><spring:message code= "LB.Gold_Account_Apply_P6_D23-2" /><font color="red"><u><b><spring:message code= "LB.Gold_Account_Apply_P6_D23-3" /></b></u></font><spring:message code= "LB.Gold_Account_Apply_P6_D23-4" /><font color="red"><u><b><spring:message code= "LB.Gold_Account_Apply_P6_D23-5" /></b></u></font><spring:message code= "LB.Gold_Account_Apply_P6_D23-6" /></td></tr>
			         		<!-- (3) 請親攜身分證件、新台幣存款存摺及原留存款印鑑至黃金存摺開戶行辦理領取黃金存摺手續。-->
			         		<tr><td>（3）</td><td><spring:message code= "LB.Gold_Account_Apply_P6_D24" /></td></tr>
		         	        <tr><td>（4）</td><td>為保護您的安全，交易結束或離開電腦時，請務必將晶片金融卡抽離讀卡機或是將電子簽章 (載具i-key)拔除，並登出系統。</td></tr>
			         	</table>
			         </li>
			         <!-- 查詢分行地址 -->
			         <li><spring:message code= "LB.Gold_Account_Apply_P6_D3" /><a href="https://www.tbb.com.tw/web/guest/-422" target="_blank">https://www.tbb.com.tw/web/guest/-422</a></li>
			    </ol>
			</div>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
		
	    function init(){
	    	//列印
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"gold_account_apply_result_print",
					//線上申請黃金存摺帳戶
					"jspTitle":'<spring:message code="LB.W1655"/>',
					"CMTIME":"${result_data.data.CMTIME}",
					"GDACN":"${result_data.data.GDACN}",
					"SVACN":"${input_data.SVACN}",
					"CUSIDN2":"${input_data.CUSIDN2}",
					"CMDATE1":"${input_data.CMDATE1}",
					"TYPECHA":"${input_data.TYPECHA}",
					"CMDATE2":"${input_data.CMDATE2}",
					"CITYCHA":"${input_data.CITYCHA}",
					"PIC_FLAG":"${input_data.PIC_FLAG}",
					"AMT_FEE":"${result_data.data.AMT_FEE}",
					"BRHNUM":"${result_data.data.BRHNUM}",
					"ARACOD2":"${input_data.ARACOD2}",
					"TELNUM2":"${input_data.TELNUM2}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
    		//上一頁按鈕
    		$("#cmback").click(function() {
    			var action = '${__ctx}/INDEX/index';
    			$("form").attr("action", action);
    			$("form").submit();
    		});
	    }	
	    

 	</script>
</body>
</html>
 