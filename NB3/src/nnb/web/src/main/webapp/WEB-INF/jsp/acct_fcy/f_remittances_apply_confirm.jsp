	<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	<script type="text/javascript">
		var idgatesubmit= $("#formId");
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
			// 確認鍵 click
			goOn();
			// 上一頁按鈕 click
			goBack();
			//交易類別change 事件
			changeFgtxway();
		}

		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				// 表單驗證
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					// 通過表單驗證
					$("#formId").validationEngine('detach');
					processQuery();
				}
			});
		}
		// 交易機制選項
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			switch (fgtxway) {
				case '0':
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					$('#CMPASSWORD').val("");
					initBlockUI(); //遮罩
					$("#formId").submit();
					break;
				// IKEY
				case '1':
					useIKey();
					break;
		        case '7'://IDGATE認證		 
	               idgatesubmit= $("#formId");		 
	               showIdgateBlock();
	               		 
	               break;
				default:
					//請選擇交易機制
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				
			}
		}
		//交易類別change 事件
		function changeFgtxway() {
			$('input[type=radio][name=FGTXWAY]').change(function () {
				console.log(this.value);
				if (this.value == '0') {
					$("#CMPASSWORD").addClass("validate[required]")
				} else if (this.value == '1' || this.value == '7') {
					$("#CMPASSWORD").removeClass("validate[required]");
				}
			});
		}
		// 上一頁按鈕 click
		function goBack() {
			// 上一頁按鈕
			$("#CMBACK").click(function () {
				// 遮罩
				initBlockUI();
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				// 讓Controller知道是回上一頁
				$('#back').val("Y");
				// 回上一頁
				var action = '${__ctx}/FCY/ACCT/ONLINE/f_remittances_apply';
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
		}

		//重新輸入
		function formReset() {
			if ($('#actionBar').val() == "reEnter") {
				$('#actionBar').val("");
				document.getElementById("formId").reset();
			}
		}
	</script>

</head>

<body>
    <!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>
	<!-- 交易機制所需畫面    -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 匯入匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0125" /></li>
    <!-- 線上解款申請/註銷     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0318" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--  外匯匯入匯款線上解款申請/註銷-->
					<spring:message code="LB.W0318" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form name="formId" id="formId" method="post"
					action="${__ctx}/FCY/ACCT/ONLINE/f_remittances_apply_result">
					<input type="hidden" name="ADOPID" value="F004">
					<input type="hidden" name="TXTYPE" value="${f_remittances_apply_confirm.data.TXTYPE}">
					<%--  TXTOKEN  防止重送代碼--%>
					<input type="hidden" name="TXTOKEN" value="${f_remittances_apply_confirm.data.TXTOKEN}" />
					<%-- 驗證相關 --%>
					<input type="hidden" id="PINNEW" name="PINNEW" value="">
					<input type="hidden" id="jsondc" name="jsondc" value='${f_remittances_apply_confirm.data.jsondc}'>
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<!-- 申請項目 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 申請項目 -->
											<spring:message code="LB.D0169" />
										</label>
									</span>
									<span class="input-block">
										<!-- 判斷式 -->
										<c:choose>
											<c:when test="${f_remittances_apply_confirm.data.TXTYPE == 'A'}">
												<!-- 申請 -->
												<spring:message code="LB.D0170" />
											</c:when>
											<c:otherwise>
												<!-- 取消 -->
												<spring:message code="LB.Cancel" />
											</c:otherwise>
										</c:choose>
										&nbsp;
										<!--匯入匯款線上解款 -->
										<spring:message code="LB.X0045" />
									</span>
								</div>
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 交易機制 -->
											<spring:message code="LB.Transaction_security_mechanism" />
										</label>
									</span>
									<span class="input-block">
										<!-- 交易密碼SSL -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.SSL_password" />
												<input type="radio" name="FGTXWAY" checked="checked" value="0">
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--請輸入密碼 -->
										<div class="ttb-input">
											<spring:message code="LB.Please_enter_password" var="pleaseEnterPin" />
											<input type="password" id="CMPASSWORD" name="CMPASSWORD"
												class="text-input validate[required]" maxlength="8"
												placeholder="${plassEntpin}">
										</div>
									<div class="ttb-input" name="idgate_group" style="display:none" >		 
                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
	                                       <span class="ttb-radio"></span>
                                       </label>		 
                                   	</div>
										<!-- 使用者是否可以使用IKEY -->
										<c:if test="${sessionScope.isikeyuser}">
											<!--電子簽章(請載入載具i-key) -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Electronic_signature" />
													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
													<span class="ttb-radio"></span>
												</label>
											</div>
										</c:if>
									</span>
								</div>
							</div>
							<!--button 區域 -->
								<!-- 確定 -->
								<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
								<input class="ttb-button btn-flat-orange" type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" >
							<!--button 區域 -->
						</div>
					</div>
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
		<!-- 	content row END -->
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>