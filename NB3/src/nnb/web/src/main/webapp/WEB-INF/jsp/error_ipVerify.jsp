<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="./__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>

<%@ include file="./__import_head_tag.jsp"%>
<%@ include file="./__import_js_u2.jsp"%>

</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="./index/header_logout.jsp"%>
		</header>
		<!-- 麵包屑     -->
		<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
			<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
				<li class="ttb-breadcrumb-item">　</li><!-- 避免跑版加全形空白 -->
			</ol>
		</nav>
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  --> 
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Transaction_result" /></h2>
<!-- 				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i> -->
				
<%-- 				<form id="formId" method="post" action="${__ctx}/login"> --%>
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-result-block">
								
							</div>
							<ul class="ttb-result-list">
								<li>
									<h3><spring:message code="LB.Transaction_code" /></h3>
									<p>E523</p>
								</li>
								<li>
									<h3><spring:message code="LB.Transaction_message" /></h3>
<!-- 									<p>於行內IP位址，僅開放行員身份之身分證字號使用</p> -->
									<p><spring:message code="LB.X2395" /></p>
								</li>
							</ul>
<!-- 							<button type="button" onClick="$('#formId').submit();" -->
							<button type="button" onClick="window.close();"
									class="ttb-button btn-flat-orange"><spring:message code="LB.X0194" /></button>
						</div>
					</div>
<!-- 				</form> -->
			</section>
		</main>
		<!-- 		main-content END --> 
	<!-- 	content row END -->
		<%@ include file="./index/footer.jsp"%>
	</body>
</html>