<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<!-- 快速選單 -->
		<div id="actionBar" hidden>
			<select  class="custom-select fast-select" onchange="formReset(this);">
			<option value="">
			-<spring:message code="LB.Select" />-
			</option>
			</select>
		</div>


<!-- LOADINGFRAME -->
<div class="" id="LOADINGBOX" style="display:none">
	<div class="col-12 block-title">
		<h3>TEMPLATE</h3>
	</div>
	<div class="col-12" style="margin: 10px 0px auto;">
		<!-- LOADING遮罩區塊 -->
		<div id="table_loadingBox" class="table_loadingBox">
		    <div class="sk-fading-circle">
		      <div class="sk-circle1 sk-circle"></div>
		      <div class="sk-circle2 sk-circle"></div>
		      <div class="sk-circle3 sk-circle"></div>
		      <div class="sk-circle4 sk-circle"></div>
		      <div class="sk-circle5 sk-circle"></div>
		      <div class="sk-circle6 sk-circle"></div>
		      <div class="sk-circle7 sk-circle"></div>
		      <div class="sk-circle8 sk-circle"></div>
		      <div class="sk-circle9 sk-circle"></div>
		      <div class="sk-circle10 sk-circle"></div>
		      <div class="sk-circle11 sk-circle"></div>
		      <div class="sk-circle12 sk-circle"></div>
		    </div>
		</div>
		<!-- TABLE -->
		<table id="table_temp" class="stripe table-striped ttb-table dtable" data-show-toggle="first">
	
		</table>
	</div>
</div>

<script>
var i18n = new Object();
function createLoadingBox(code,title){
	var lb = $("#LOADINGBOX").clone();
	lb[0].id=code;
	lb.children()[0].innerHTML="<h3>"+title+"</h3>";
	lb.children()[1].children[1].id = code+"_table";
	lb.children()[1].children[0].id = code+"_table_loadingBox";
	lb.show();
	$("#"+code).show();
	$("#"+code).append(lb);
	$("#"+code+"_table_loadingBox").show();
}

function createTable(code,rows,columns,align,tb){
	jQuery(function($) {
		//
		if(rows.length==0){
			rows.push([]);
		}
		rows.forEach(function(d){
			console.log(d);
			fillData(columns,d);
		});
		var pre="上一頁";
		var next="下一頁";
		if(dt_locale == "zh_CN")
			{
			pre = "上一页";
			next = "下一页";
			}
		if(dt_locale == "en")
			{
			pre="Previous";
			next="Next";
			}

		// callback變更css置中table的表頭
		$('#'+code+'_table').DataTable({
			"bPaginate" : true,
			"bLengthChange": false,
			"autoWidth": false,
			"data": rows,
			"columns" : columns,
			"scrollX": true,
			"sScrollX": "99%",
	        "bFilter": false,
	        "bDestroy": true,
	        "bSort": false,
	        "info": false,
	        "scrollCollapse": true,
	        "language" :{
	                	"paginate":{
	                		"previous":pre,
	                		"next":next
	                	},
	                	"emptyTable":" "
	                },
	        //fixedColumns為固定欄位
// 	        "fixedColumns": {
// 	            leftColumns: 1
// 			},
			columnDefs: [{
				targets:"_all" ,
				createdCell: function(td, cellData, rowData, row, col) {				
// 						console.log("row:"+row+"col:"+col);
					if(align[col]==0){
						$(td).css('text-align','center');
					}
					if(align[col]==1){
						$(td).css('text-align','right');
					}
					if(tb != null){
						if(tb[col]==1){
							$(td).css('white-space','normal');
							$(td).css('word-break','break-all');
							$(td).css('width','100px');
							$(td).css('word-wrap','break-word');
						}
					}
					
				}
			}]
		});
	});
	$("#"+code+"_table_loadingBox").hide();
}

//快速選單
function formReset(d) {
    console.log(d.id);
	switch (d.id){
	//n110
	case "n110":
    console.log(d.options[d.options.selectedIndex].value);
    var data = n110_rows[d.name];
	switch (d.options[d.options.selectedIndex].value) {
	case "demand_deposit_detail":
		$('#n110_ACN').val(data.ACN);
		$('#n110_FGSELECT').attr("disabled",true);
		$('#n110_fastBarAction').attr("action","${__ctx}/NT/ACCT/demand_deposit_detail?Acn=data.ACN");
		$('#n110_fastBarAction').submit();
		break;
	case "checking_insufficient":
		fstop.getPage('${pageContext.request.contextPath}'
				+ '/NT/ACCT/checking_insufficient?Acn='+data.ACN+'&', '', '');
		break;
	case "checking_cashed":
		fstop.getPage('${pageContext.request.contextPath}'
				+ '/NT/ACCT/checking_cashed?Acn='+data.ACN+'&', '', '');
		break;
	case "transfer":
		fstop.getPage('${pageContext.request.contextPath}'
				+ '/NT/ACCT/TRANSFER/transfer?Acn='+data.ACN+'&', '', '');
		break;
	case "deposit_transfer":
		fstop.getPage('${pageContext.request.contextPath}'
				+ '/NT/ACCT/TDEPOSIT/deposit_transfer?Acn='+data.ACN+'&', '', '');
		break;
	case "payment":
		fstop.getPage('${pageContext.request.contextPath}'
				+ '/CREDIT/PAY/payment?Acn='+data.ACN+'&', '', '');
		break;
	case "n614":
		fstop.getPage('${pageContext.request.contextPath}'
				+ '/PARKING/FEE/parking_withholding?Acn='+data.ACN+'&', '', '');
		break;
	}
	//N420
	case "n420":
		//alert(n420_rows[d.name]);
		var data = n420_rows[d.name];
		switch (d.options[d.options.selectedIndex].value) {
		case "deposit_cancel":
			$('#FDPNUM').val(data.FDPNUM);
			$('#ACN').val(data.ACN);
			$('#FGSELECT').attr("disabled",true);
			$('#fastBarAction').attr("action","${__ctx}/NT/ACCT/TDEPOSIT/deposit_cancel");
			$('#fastBarAction').submit();
			break;
		case "renewal_apply":
			$('#FDPNUM').attr("disabled",true);
			$('#ACN').attr("disabled",true);
			$('#FGSELECT').val(htmldecode(data.MapValue));
			$('#fastBarAction').attr("action","${__ctx}/NT/ACCT/TDEPOSIT/renewal_apply_step1");
			$('#fastBarAction').submit();
			break;
		case "installment_saving":
			$('#FDPNUM').val(data.FDPNUM);
			$('#ACN').val(data.ACN);
			$('#FGSELECT').attr("disabled",true);
			$('#fastBarAction').attr("action","${__ctx}/NT/ACCT/TDEPOSIT/installment_saving");
			$('#fastBarAction').submit();
			break;
			
		}
	case "n510":
		console.log(d);
		var data = n510_rows[d.name];
		switch (d.options[d.options.selectedIndex].value) {
		case "f_demand_deposit_detail":
			fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/f_demand_deposit_detail?ACN='+data.ACN+'&','', '');
		    break;
		case "f_transfer":
			fstop.getPage('${pageContext.request.contextPath}'+'/FCY/TRANSFER/f_transfer?ACN='+data.ACN+'&','', '');
		    break;
		case "f_deposit_transfer":
			fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/TDEPOSIT/f_deposit_transfer?ACN='+data.ACN+'&','', '');
		    break;
		case "f_outward_remittances":
			fstop.getPage('${pageContext.request.contextPath}'+'/FCY/REMITTANCES/f_outward_remittances?ACN='+data.ACN+'&','', '');
			break;
		}
	case "n530":
		var data = n530_rows[d.name];
		switch(d.options[d.options.selectedIndex].value){
		case "f_deposit_cancel":
			$('#FDPNO').val(data.FDPNO);
			$('#ACN530').val(data.ACN);
			$('#fastBarAction_fcy').attr("action","${__ctx}/FCY/ACCT/TDEPOSIT/f_deposit_cancel");
			$('#fastBarAction_fcy').submit();
// 			fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/TDEPOSIT/f_deposit_cancel','', '');
			break;
		case "f_renewal_apply":
			$('#FDPNO').val(data.FDPNO);
			$('#ACN530').val(data.ACN);
			$('#FGSELECT530').val(htmldecode(data.MapValue));
			$('#fastBarAction_fcy').attr("action","${__ctx}/FCY/ACCT/TDEPOSIT/f_renewal_apply_step1");
			$('#fastBarAction_fcy').submit();
// 			fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/TDEPOSIT/f_deposit_cancel','', '');
			break;
			}
	case "n320":
		var data = n320tw_rows[d.name];
		switch(d.options[d.options.selectedIndex].value){
		case "repay_advance_p1":
				$("#loanACN").val(data.ACN);
				$("#loanSEQ").val(data.SEQ);
				$("#loanAMT").val(data.AMTAPY);
				$("#formN320").attr("action","${__ctx}/LOAN/ADVANCE/repay_advance_p1");
	  			$("#formN320").submit(); 
				break;
		case "settlement_advance_p1":
			$("#loanACN").val(data.ACN);
				$("#loanSEQ").val(data.SEQ);
				$("#loanAMT").val(data.AMTAPY);
				$("#formN320").attr("action","${__ctx}/LOAN/ADVANCE/settlement_advance_p1");
	  			$("#formN320").submit(); 
				break;
		case "interest_list":
			fstop.getPage('${pageContext.request.contextPath}'+'/HOUSE/GUARANTEE/interest_list','', '');
			break;
		case "paid_query":
			fstop.getPage('${pageContext.request.contextPath}'+'/LOAN/QUERY/paid_query','', '');
			break;
	}
	case "c012":
		var data = c012_rows[d.name];
		switch (d.options[d.options.selectedIndex].value) {
		case "1":
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + data.TRANSCODE + "&B=02");
			break;
		case "2":
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + data.TRANSCODE + "&B=02");
			break;
		case "3":
			if(data.REFMARKTransfer !="N"){
				//alert("<spring:message code= "LB.X0970" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X0970' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
			else{
				$("#formC012").attr("action","${__ctx}/FUND/REDEEM/fund_redeem_data?CDNO=" + data.CDNO + "&TRANSCODE=" + data.TRANSCODE);
				$("#formC012").submit();
			}
			break;
		case "4":
			if(data.REFMARKTransfer !="N"){
				//alert("<spring:message code= "LB.X0970" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X0970' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
			else{
				$("#formC012").attr("action","${__ctx}/FUND/TRANSFER/query_fund_transfer_data?CDNO=" + data.CDNO + "&TRANSCODE=" + data.TRANSCODE);
				$("#formC012").submit();
			}
			break;
		case "5":
			$("#formC012").attr("action","${__ctx}/FUND/QUERY/transfer_data_query_history?CDNO=" + data.CDNO);
			$("#formC012").submit();
			break;
		case "6":
			$("#formC012").attr("action","${__ctx}/FUND/ALTER/regular_investment?CDNO=" + data.CDNO);
			$("#formC012").submit();
			break;
		case "7":
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + data.TRANSCODE + "&DownFile=99");
			break;
		case "8":
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + data.TRANSCODE + "&DownFile=1");
			break;
		case "9":
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + data.TRANSCODE + "&B=08");
			break;
		case "10":
			$("#formC012").attr("action","${__ctx}/FUND/QUERY/transfer_data_query");
			$("#formC012").submit();
			break;
			
		}
	case "n190":
		var data = n190_rows[d.name];
		console.log(d.options[d.options.selectedIndex].value);
		switch (d.options[d.options.selectedIndex].value) {
		case "N09001":
			$("#n190_ACN").val(data.ACN);
			target="${__ctx}/GOLD/TRANSACTION/gold_buy";
			$("#formn190").attr("action", target);
			$("#formn190").submit();
			break;
		case "N09002":
			$("#n190_ACN").val(data.ACN);
			target="${__ctx}/GOLD/TRANSACTION/gold_resale";
			$("#formn190").attr("action", target);
			$("#formn190").submit();
			break;
		case "N09301":
			$("#n190_ACN").val(data.ACN);
			target="${__ctx}/GOLD/AVERAGING/averaging_purchase";
			$("#formn190").attr("action", target);
			$("#formn190").submit();
			break;
		case "N09302":
			$("#n190_ACN").val(data.ACN);
			target="${__ctx}/GOLD/AVERAGING/averaging_alter";
			$("#formn190").attr("action", target);
			$("#formn190").submit();
			break;
		case "N191":
			$("#n190_ACN").val(data.ACN);
			$("#n190_FASTMENU").val(data.ACN);
			target="${__ctx}/GOLD/PASSBOOK/gold_detail_query";
			$("#formn190").attr("action", target);
			$("#formn190").submit();
			break;
		
		}
		
	}
}

function htmldecode(s){
	var div = document.createElement('div');
	div.innerHTML = s;
	return div.innerText || div.textContent;
	}

function fillData(a,b){
	a.forEach(function(d){	
		if(b.hasOwnProperty(d.data)){
			b[d.data]=b[d.data];
			if(!b[d.data])
				b[d.data]=" ";
		}
		else{
			b[d.data]=" ";
		}
	});
}


</script>