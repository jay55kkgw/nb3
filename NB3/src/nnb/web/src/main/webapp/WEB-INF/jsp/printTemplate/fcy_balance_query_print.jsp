<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
	});
</script>
<style>
@page {
 margin: 7%; /*print邊界*/
 }
</style>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact;">
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/>
	<br/>
	<br/>
	<!-- 外匯活存餘額   -->
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/>
	<br/>
	<br/>
	<!-- 查詢時間： -->
	<label><spring:message code="LB.Inquiry_time"/>：</label>
	<label>${CMQTIME}</label>
	<br/>
	<br/>
	<!-- 資料總數： -->
	<label><spring:message code="LB.Total_records"/>：</label>
	<label>${COUNT} <spring:message code="LB.Rows"/></label>
<br/>
<br/>
	<table class="print" style="margin-top: 5%">
		<tr>
			<!-- 帳號欄位-->
			<td><spring:message code="LB.Account"/></td>
			<%-- <!-- 存款種類欄位-->
			<td><spring:message code="LB.Deposit_type"/></td> --%>
			<!-- 幣別欄位-->
			<td><spring:message code="LB.Currency"/></td>
			<!-- 可用餘額欄位-->
			<td><spring:message code="LB.Available_balance"/></td>
			<!-- 帳戶餘額欄位-->
			<td><spring:message code="LB.Account_balance"/></td>
			<!-- 執行選項欄位 尚未做-->
		</tr>
		<c:forEach items="${dataListMap[0]}" var="map">
			<tr>
				<td class="text-center">${map.ACN}</td>
				<%-- <td>${map._FXKind}</td> --%>
				<td class="text-center">${map.CUID}</td>
				<td class="text-right">${map.AVAILABLE}</td>
				<td class="text-right">${map.BALANCE}</td>
			</tr>
		</c:forEach>
	</table>
		<br />
		<br />
		<div class="fcy-balance"  style="margin-top: 5%">
			<div class="available-balance" width="49%"	margin-right="1%">	
				<!-- 可用餘額小計欄位-->
				
				<table class="print">
					<tr>
					<td><spring:message code="LB.Currency"/></td>
					<td><spring:message code="LB.Subtotal_of_available_balance"/></td>
					<td><spring:message code="LB.Total_records"/></td>
					</tr>
					<c:forEach items="${dataListMap[1]}" var="map">
						<tr>
							<td class="text-center">${map.CUID}</td>
							<td class="text-right">${map.AVAILABLE}</td>
							<td class="text-center"> ${map.COUNT} <spring:message code="LB.Rows"/> </td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="account-balance" width="49%" margin-left="1%" margin-top="5%">
				<!-- 帳戶餘額小計欄位-->
				
				<table class="print">
					<tr>
					<td><spring:message code="LB.Currency"/></td>
					<td><spring:message code="LB.Subtotal_of_account_balance"/></td>
					<td><spring:message code="LB.Total_records"/></td>
					</tr>
					<c:forEach items="${dataListMap[2]}" var="map">
						<tr>
							<td class="text-center">${map.CUID}</td>
							<td class="text-right">${map.BALANCE}</td>
							<td class="text-center"> ${map.COUNT} <spring:message code="LB.Rows"/></td>
						</tr>
					</c:forEach>
				</table>
			</div>
	</div>
	</body>
</html>