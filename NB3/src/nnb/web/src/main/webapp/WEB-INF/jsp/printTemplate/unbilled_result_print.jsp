<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body  class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<!-- 查詢時間： -->
	<label><spring:message code="LB.Inquiry_time" />：</label>
	<label>${CMQTIME}</label>
	<br />
	<br />
	<!-- 資料總數： -->
	<label><spring:message code="LB.Total_records" />：</label>
	<label>${COUNT} &nbsp;<spring:message code="LB.Rows" /></label>
	<br />
	<br />
	
	<table class="print">
		<tr>
			<td style="text-align: center">
				<!-- 姓名  --> <spring:message code="LB.Name" />
			</td>
			<td style="text-align: center">
				<!-- 每月結帳日  --> <spring:message code="LB.Statement_date_TD01" />
			</td>
		</tr>
		<tr>
			<td style="text-align: center">
				<!-- 姓名  --> ${HOLDERNAME}
			</td>
			<td style="text-align: center">
				<!-- 每月結帳日  --> ${CYCLEDATE}
			</td>
		</tr>
	</table>
	
	<br/>
	<br/>
	<c:forEach items="${dataListMap}" var="map">
	<table class="print">
		<tr>
			<td ><spring:message code="LB.Card_kind" /></td>
			<!-- 卡片種類  -->
			<td><spring:message code="LB.Consumption_date" /></td>
			<!-- 消費日期  -->
			<td><spring:message code="LB.The_account_credited_date" /></td>
			<!-- 入帳日期  -->
			<td><spring:message code="LB.Description_of_transaction" /></td>
			<!-- 交易說明  -->
			<td><spring:message code="LB.Card_holder" /></td>
			<!-- 持卡人 -->
			<td><spring:message code="LB.Currency" /></td>
			<!-- 幣別  -->
			<td><spring:message code="LB.Foreign_currency_amount" /></td>
			<!-- 外幣金額  -->
			<td><spring:message code="LB.NTD_amount" /></td>
			<!-- 台幣金額  -->
		</tr>
		<c:forEach items="${map.TABLE}" var="mapList">
			<tr>
				<td>${mapList.CARDTYPE}</td>
				<td>${mapList.PURDATE}</td>
				<td>${mapList.POSTDATE}</td>
				<td>${mapList.DESCTXT}</td>
				<td>${mapList.CRDNAME}</td>
				<td>${mapList.CURRENCY}</td>
				<td style="text-align: right">${mapList.SRCAMNT}</td> 
				<td style="text-align: right">${mapList.CURAMNT}</td>
			</tr>
		</c:forEach>
	</table>
	<br>
	</c:forEach>
	<br />
</body>
</html>