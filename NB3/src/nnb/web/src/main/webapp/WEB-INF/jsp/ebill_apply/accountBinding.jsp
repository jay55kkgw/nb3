<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta content="" name="description">
    <meta content="臺灣中小企業銀行" name="author">
    <title>臺灣中小企業銀行 - 全國性繳費平台線上約定作業</title>
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/bootstrap.min.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/full-width-pics.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/e-style.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/all.css" rel="stylesheet">

</head>
<body class="page">
    <header>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg fixed-top">
            <div class="container">
                <div class="col-12 text-center">
                   <a class="navbar-brand" href=""><img src="${__ctx}/ebillApply/img/logo_tbb.svg"></a>
                </div>
            </div>
        </nav>
        <!-- Header section -->
    </header>
    <main class="px-3 px-sm-0">
        <form id="validationForm" method="post" action="${__ctx}/EBILL/APPLY/accountBinding_step2">
            <input type="hidden" name="IBPD_Param" value='${accountBinding.data.IBPD_Param}'>
            <div class="container">
                <div class="row">
                    <p class="main-content-desc text-left mt-4">親愛的客戶您好：</p>
                    <p class="main-content-desc mt-sm-0 adj-p">請詳閱以下<span style="color: #FF8029;">帳戶授權扣款線上約定作業條款</span>
                    </p>
                </div>
            </div>
            <div class="container mt-2 mt-sm-3">
                <div class="row">
                    <div class="col-md-12 main-content pt-2 py-sm-4 px-sm-5">
                        <div class="agreement embed-responsive overflow-auto my-2 my-sm-3" id="agreement">
                            <li style="text-indent: 0px;list-style: none;font-weight: 500;">全國性繳費（稅）業務授權轉帳繳款約定書</li>
                            <ul class="agreement-outer-ul">
                                <li>立約人為便於利用金融機構帳戶支付應付予委託單位款項，同意以立約人約定之活期性存款帳戶（以下稱約定扣款帳戶）逕行轉帳扣繳立約人之應付款項，並同意遵守下列約定事項：</li>
                                <li>一、立約人同意貴行依財金資訊股份有限公司（以下稱財金）「全國性繳費（稅）系統」所傳送之訊息，自下列活期性存款帳戶轉帳扣繳應付款項，當立約人存款金額不足、帳戶遭法院、行政執行署或其他機關扣押或存款帳戶結清時，貴行得不予扣款。因上開事由所致之損失或責任，概由立約人自行負擔。</li>
                                <li>二、為辦理本件轉帳扣款業務，委託單位得將立約人轉帳扣繳資料交付予帳務代理行，經由財金轉交貴行辦理；貴行亦得將扣繳結果（包括扣繳不成功之原因）經由財金回覆帳務代理行，由帳務代理行回覆委託單位。</li>
                                <li>三、立約人同意由貴行逕依委託單位提供經由「全國性繳費（稅）系統」傳送之資料（含扣款日期、金額等），辦理轉帳扣繳作業，如因此所生之錯誤或疏漏，由立約人逕洽委託單位處理。</li>
                                <li>四、立約人瞭解使用本服務每筆轉帳扣繳可能需繳納手續費，立約人將自行向委託單位確認，如需由立約人負擔手續費者，立約人並授權貴行自約定扣款帳戶逕行扣繳。</li>
                                <li>五、「全國性繳費（稅）系統」如發生故障或電信中斷或其他不可抗力之事由致無法交易者，貴行得順延至系統恢復正常，始予扣款。</li>
                                <li>六、立約人同意本作業轉帳扣繳限額單筆及每日最高轉帳扣繳限額皆為新臺幣伍佰萬元，但關稅費及基金證券費每日最高轉帳扣繳限額各為新臺幣參仟萬元。</li>
                                <li>七、貴行於同一日需自約定扣款帳戶執行多筆轉帳扣繳作業而立約人存款不足時，立約人同意貴行得依貴行實際作業之順序扣款。</li>
                                <br><br><br>
                            </ul>   
                        </div>    
                    </div>
                </div>
            </div>
            <br>
            <div class="container">
                <div class="row">
                    <div class="form-check pl-0 pt-0 pb-4 pb-sm-0">
                        <input class="form-check-input with-font" type="checkbox" name="form_check_agreement"
                            id="form_check_agreement" value="true" required>
                        <label class="form-check-label" for="form_check_agreement">
                            <strong>我已閱讀並同意以上服務約定條款</strong><br>
                            <font color="red">(本服務透過財金資訊股份有限公司全國性繳費(稅)平台提供服務)</font>
                        
                        </label>
                    </div>
                </div>
                
                <br>
                
                <div class="row action-container">
                    <div class="col-sm-12 px-0">
                        <div class="float-right">
                            <button id="goback" class="back">取消</button> 
                            <button type="submit" class="btn btn-primary submit">下一步<i class="fas fa-chevron-right float-right mt-1"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </main>
    <footer class="py-sm-0 d-flex align-items-center align-middle">
		<div class="container">
			<div class="row d-flex align-items-center align-middle">
				<div class="col-12 mt-3 mt-sm-0 pr-sm-0 text-center order-last order-sm-first">
					<h6 class="copyright mb-0">©臺灣中小企業銀行</h6>
					<span class="small"><a href="https://www.tbb.com.tw/web/guest/-173" target="_blank">隱私權聲明</a> | <a href="https://www.tbb.com.tw/web/guest/-551" target="_blank">安全政策</a></span>
				</div>
				<div class="col-12 mt-3 mt-sm-0 p-0 text-center d-sm-none">
					<p class="mt-sm-4">24H服務專線 <a href="tel:0800-01-7171">0800-01-7171</a>(限市話), <a href="tel:02-2357-7171">02-2357-7171</a></p>
				</div>
				<div class="col-12 pt-0 footer-sns-link">
					<ul class="list-inline ml-auto mb-sm-0 text-center">
						<li class="list-inline-item d-none d-sm-inline-block mr-2">
							<p>24H服務專線 <a href="tel:0800-01-7171">0800-01-7171</a>(限市話), <a href="tel:02-2357-7171">02-2357-7171</a></p>
						</li>
						<li class="list-inline-item mr-2">
							<a href="https://www.facebook.com/tbbdreamplus/" target="_blank"><i class="fab fa-facebook"></i></a>
						</li>
						<li class="list-inline-item">
							<a href="https://www.youtube.com/channel/UCyRmUHjcJV3ROmXrF7q3xOA" target="_blank"><i class="fab fa-youtube"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
    <!-- Bootstrap core JavaScript -->
    <script src="${__ctx}/ebillApply/js/jquery.min.js"></script>
    <script src="${__ctx}/ebillApply/js/bootstrap.bundle.min.js"></script>
    <script src="${__ctx}/ebillApply/js/jquery.validate.min.js"></script>
    <script src="${__ctx}/ebillApply/js/localization/messages_zh_TW.js"></script>
    <script src="${__ctx}/ebillApply/js/e-bill-1.js"></script>
    <script>
  	//註冊enter
  	$(window).keydown(function(e){
		var curKey = window.event ? e.keyCode : e.which;
		if(curKey == 13){//enter键位:13
			e.preventDefault();
			$("#validationForm").submit();
		}
	})
	 $(document).ready(function () {
            //註冊按鈕
            $("#goback").click(function () {
            	$("#form_check_agreement").addClass("ignore");
                $("#validationForm").attr("action", "${accountBinding.data.BackUrl}");
                $("#validationForm").submit();
            });
      });
  	
    </script>
</body>
</html>