<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
	<div class="ttb-message">
		<p>客戶辦理網路銀行外匯結購及轉帳、匯出匯款及匯入匯款解款應注意事項</p>
	</div>
	<ul class="ttb-result-list terms">
		<li data-num="">
			<ul class="">
				<li>
					<strong style="font-size:15px">
						1.本網路銀行
						<font color="red">
							不提供需檢附核准函或交易文件之各項外匯服務
						</font>
						，如申報義務人備有核准函或欲執行須檢附交易文件之外匯服務請臨櫃辦理。
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						2.本網路銀行
						<font color="red">
							涉及新台幣之外匯業務
						</font>
							僅提供
						<font color="red">
							未達等值新台幣50萬元以下之轉帳、匯出匯款及匯入匯款解款交易
						</font>
						。如超過交易限額請臨櫃辦理。
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						3.<font color="red">個人涉及人民幣之結購(含幣轉)及結售(含幣轉)亦應個別累計不得逾2萬元人民幣。</font>
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						4.<font color="red">如單筆交易或累計已達等值新台幣50萬元者，則該筆交易取消。</font>
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						5.本網路銀行
						<font color="red">未涉及新台幣之外匯業務</font>
						，其每筆或每日交易限額
						<font color="red">依本行網路銀行服務系統作業程序有關交易限額之規定辦理</font>
						。（參看本行網站重要公告內容）
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						6.申報義務人之外匯收支或交易未辦理新台幣結匯者，以本行掣發之其它交易憑證視同申報書。
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						7.申報義務人利用網際網路辦理新台幣結匯申報，經查獲有申報不實情形者，其日後辦理新台幣結匯申報事宜，應臨櫃辦理。
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						8.本網路銀行提供之匯入匯款解款交易，倘因貴行有錯付或產生任何糾紛，對於匯入款項及可能衍生之利息，本人（公司）願負返還之責任。
					</strong>
				</li>
			</ul>
		</li>
	</ul>
</div>