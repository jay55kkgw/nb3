<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="col-lg-6 col-12">
	<p class="home-title"><spring:message code="LB.Notifications" /></p>
	<div class="main-content-block">
		<ul id="bulletinUL" class="ttb-todolist-checkbox">
			<!-- 動態生成 -->
		</ul>

	</div>
</div>

<script type="text/JavaScript">
// 取得公告訊息
function getBulletin(){
	console.log("getBulletin.now: " + new Date());
	
	uri = '${__ctx}'+"/INDEX/bulletin_aj";
// 	console.log("bulletin_uri: " + uri);
	fstop.getServerDataEx(uri, null, true, showBulletin);
}

// 顯示公告訊息
function showBulletin(data){
	console.log("showBulletin.now: " + new Date());
	
// 	console.log("showBulletin.data...");
	if (data) {
		for (i = 0; i < data.length; i++) {
			var id = data[i].id; // 訊息類型
			var type = data[i].type; // 訊息類型
			var title = data[i].title; // 訊息標題
			var contenttype = data[i].contenttype; // 訊息內容類型
			
			var urlLink = data[i].url; // 訊息內容類型--超連結
			var content = data[i].content; // 訊息內容類型--文字
			var srccontent = data[i].srccontent; // 訊息內容類型--pdf
			
			// 訊息類型--1:重要、0:一般、其他:已讀
// 			console.log("data["+i+"].type: " + data[i].type);
			
			// 變更換行符號
			if(content != null){
				content=content.replace(/\n\r/g,"<br/>");
				content=content.replace(/\r\n/g,"<br/>");
				content=content.replace(/\n/g,"<br/>");
				content=content.replace(/\r/g,"<br/>");
			}
			
			// 訊息類型--1:重要
			if (type == '1') {
				// 訊息公告內容為超連結
				if (contenttype == '1') {
					var url = ' onclick="window.open(' + "'" + urlLink + "'" + ');"';
					
					$("#bulletinUL").append(
						  '<li>'
						+ '<label class="ttb-todolist-priority">'
// 						+ '<input type="checkbox" />'
// 						+ '<span class="ttb-todolist-mark"></span>'
						+ '<span class="ttb-todolist-label"' + url + '>' + title + '</span>'
						+ '</label>'
						+ '</li>'
					);
					
				} // 訊息公告內容為pdf
				else if (contenttype == '2') {
					var uri = "${__ctx}/getAnn/"+id;
					var url = ' onclick="window.open(' + "'" + uri + "'" + ');"';
					
					$("#bulletinUL").append(
						  '<li>'
						+ '<label class="ttb-todolist-priority">'
// 						+ '<input type="checkbox" />'
// 						+ '<span class="ttb-todolist-mark"></span>'
						+ '<span class="ttb-todolist-label"' + url + '>' + title + '</span>'
						+ '</label>'
						+ '</li>'
					);
					
				} // 訊息公告內容為文字訊息
				else if (contenttype == '3') {
	 				var url = ' onclick="' + "$('#text-info').html(" + "'" + content + "'" + '); ' + "$('#text-block').show(); " + '"';

					$("#bulletinUL").append(
						  '<li>'
						+ '<label class="ttb-todolist-priority">'
// 						+ '<input type="checkbox" />'
// 						+ '<span class="ttb-todolist-mark"></span>'
						+ '<span class="ttb-todolist-label"' + url + '>' + title + '</span>'
						+ '</label>'
						+ '</li>'
					);
				}
				
			} // 訊息類型--0:一般
			else if(type == '0'){
				// 訊息公告內容為超連結
				if (contenttype == '1') {
					var url = ' onclick="window.open(' + "'" + urlLink + "'" + ');"';
					
					$("#bulletinUL").append(
						  '<li>'
						+ '<label>'
// 						+ '<input type="checkbox" />'
// 						+ '<span class="ttb-todolist-mark"></span>'
						+ '<span class="ttb-todolist-label"' + url + '>' + title + '</span>'
						+ '</label>'
						+ '</li>'
					);
					
				} // 訊息公告內容為pdf
				else if (contenttype == '2') {
					var uri = "${__ctx}/getAnn/"+id;
					var url = ' onclick="window.open(' + "'" + uri + "'" + ');"';
					
					$("#bulletinUL").append(
						  '<li>'
						+ '<label>'
// 						+ '<input type="checkbox" />'
// 						+ '<span class="ttb-todolist-mark"></span>'
						+ '<span class="ttb-todolist-label"' + url + '>' + title + '</span>'
						+ '</label>'
						+ '</li>'
					);
					
				} // 訊息公告內容為文字訊息
				else if (contenttype == '3') {
	 				var url = ' onclick="' + "$('#text-info').html(" + "'" + content + "'" + '); ' + "$('#text-block').show(); " + '"';

					$("#bulletinUL").append(
						  '<li>'
						+ '<label>'
// 						+ '<input type="checkbox" />'
// 						+ '<span class="ttb-todolist-mark"></span>'
						+ '<span class="ttb-todolist-label"' + url + '>' + title + '</span>'
						+ '</label>'
						+ '</li>'
					);
				}
				
			} // 訊息類型--其他:已讀
			else {
				// 訊息公告內容為超連結
				if (contenttype == '1') {
					var url = ' onclick="window.open(' + "'" + urlLink + "'" + ');"';
					
					$("#bulletinUL").append(
						  '<li>'
						+ '<label class="ttb-todolist">>'
// 						+ '<input type="checkbox" checked />'
// 						+ '<span class="ttb-todolist-mark"></span>'
						+ '<span class="ttb-todolist-label"' + url + '>' + title + '</span>'
						+ '</label>'
						+ '</li>'
					);
					
				} // 訊息公告內容為pdf
				else if (contenttype == '2') {
					var uri = "${__ctx}/getAnn/"+id;
					var url = ' onclick="window.open(' + "'" + uri + "'" + ');"';
					
					$("#bulletinUL").append(
						  '<li>'
						+ '<label class="ttb-todolist">>'
// 						+ '<input type="checkbox" checked />'
// 						+ '<span class="ttb-todolist-mark"></span>'
						+ '<span class="ttb-todolist-label"' + url + '>' + title + '</span>'
						+ '</label>'
						+ '</li>'
					);
					
				} // 訊息公告內容為文字訊息
				else if (contenttype == '3') {
	 				var url = ' onclick="' + "$('#text-info').html(" + "'" + content + "'" + '); ' + "$('#text-block').show(); " + '"';

					$("#bulletinUL").append(
						  '<li>'
						+ '<label class="ttb-todolist">>'
// 						+ '<input type="checkbox" checked />'
// 						+ '<span class="ttb-todolist-mark"></span>'
						+ '<span class="ttb-todolist-label"' + url + '>' + title + '</span>'
						+ '</label>'
						+ '</li>'
					);
				}
			}
		}
// 		console.log("showBulletin...Finish!!!");
		console.log("showBulletin.end: " + new Date());
		
	} else {
		console.log("Oops");
	}
	
}
</script>