<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/><br/><br/>
	
	<table class="print">
		<tr>
			<!-- 分配日期 -->
			<th><spring:message code="LB.W0988" /></th>
			<th>${TRADEDATE_F }</th>
		</tr>
		<tr>
			<!-- 信託編號 -->
			<th><spring:message code="LB.W0904" /></th>
			<th>${CDNO_F }</th>
		</tr>
		<tr>
			<!-- 基金名稱 -->
			<th><spring:message code="LB.W0025" /></th>
			<th>${TRANSCODE_FUNDLNAME }</th>
		</tr>
		<tr>
			<!-- 基準日 -->
			<th><spring:message code="LB.W0028" /></th>
			<th>${DATE_F }</th>
		</tr>
		<tr>
			<!-- 除息金額 -->
			<th><spring:message code="LB.W0990" /></th>
			<th>${CRY_CRYNAME } ${EXAMT_F }</th>
		</tr>
		<tr>
			<!-- 基準日信託單位數 -->
			<th><spring:message code="LB.W0991" /></th>
			<th>${UNIT_F }</th>
		</tr>
		<tr>
			<!-- 每單位分配值 -->
			<th><spring:message code="LB.W0992" /></th>
			<th>${TRANSCRY_CRYNAME } ${AMT4_F }</th>
		</tr>
		<tr>
			<!-- 匯率 -->
			<th><spring:message code="LB.Exchange_rate" /></th>
			<th>${EXRATE_F }</th>
		</tr>
		
		<tr>
			<!-- 代扣稅款 -->
			<th><spring:message code="LB.W0993" /></th>
			<th>${CRY_CRYNAME } ${AMT5_F }</th>
		</tr>
		<tr>
			<!-- 代扣健保費 -->
			<th><spring:message code="LB.W0994" /></th>
			<th>${CRY_CRYNAME } ${NHITAX_F }</th>
		</tr>
		<tr>
			<!-- 給付淨額 -->
			<th><spring:message code="LB.W0985" /></th>
			<th>${CRY_CRYNAME } ${AMT7_F }</th>
		</tr>
	</table>
	<br/><br/><br/><br/>
</body>
</html>