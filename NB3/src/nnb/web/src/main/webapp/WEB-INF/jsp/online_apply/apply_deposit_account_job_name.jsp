<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
		<%@ include file="../__import_head_tag.jsp"%>
		<%@ include file="../__import_js.jsp" %>
		<style>
			.Line-break{
				white-space:normal;
				word-break:break-all;
				width:90%;
				word-wrap:break-word;
			}
		</style>
		<script type="text/javascript">
			$(document).ready(function() {
				setTimeout("initDataTable()",100);
	
			});
		
			function fillData(id) 
			{
				var oDpbhno = self.opener.document.getElementById("SRCFUNDDESC");
				var CAREER2 = self.opener.document.getElementById("CAREER2");
				var validate_CAREER2 = self.opener.document.getElementById("validate_CAREER2");
					
				if (oDpbhno != undefined){
					oDpbhno.value = id;
					CAREER2.value = id;
					validate_CAREER2.value = id;
				}
				self.close();
			}
		</script>
	</head>
	<body>
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<form method="post" id="formId" action="">
					<!-- 我的通訊錄 -->
					<h2><spring:message code="LB.X0195"/></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<div class="main-content-block row">
						<div class="col-12 printClass">
							<br>
						    <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
	                            <thead>
								    <tr>
					                    <th><spring:message code="LB.X0196"/></th>
					                    <th><center><spring:message code="LB.D0087"/></center></th>
					      			</tr>
	                            </thead>
	                            <tbody>
					      			<tr  onclick="fillData('1');">
										<td><center><a href="#">1</a></center></td>
										<td class="text-left Line-break"><spring:message code="LB.X0179"/></td>
									</tr>	
									<tr onclick="fillData('2');">
										<td><center><a href="#" >2</a></center></td>
										<td class="text-left"><spring:message code="LB.X0180"/></td>
									</tr>
									<tr onclick="fillData('3');">
										<td><center><a href="#">3</a></center></td>
										<td class="text-left"><spring:message code="LB.X0181"/></td>
									</tr>		  
									<tr onclick="fillData('4');">
										<td><center><a href="#">4</a></center></td>
										<td class="text-left"><spring:message code="LB.X0182"/></td>
									</tr>
									<tr onclick="fillData('5');">
										<td><center><a href="#">5</a></center></td>
										<td class="text-left"><spring:message code="LB.X0183"/></td>
									</tr>
									<tr onclick="fillData('6');">
										<td><center><a href="#">6</a></center></td>
										<td class="text-left"><spring:message code="LB.X0184"/></td>
									</tr>
									<tr onclick="fillData('7');">
										<td><center><a href="#">7</a></center></td>
										<td class="text-left"><spring:message code="LB.X0185"/></td>
									</tr>
									<tr onclick="fillData('8');">
										<td><center><a href="#">8</a></center></td>
										<td class="text-left"><spring:message code="LB.X0186"/></td>
									</tr>
									<tr onclick="fillData('9');">
										<td><center><a href="#">9</a></center></td>
										<td class="text-left"><spring:message code="LB.X0187"/></td>
									</tr>
								</tbody>
							</table>
							<br>
						</div>
					</div>
				</form>	
			</section>
		</main> <!-- 		main-content END -->
	</body>
</html>