<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/><br/>
	<div style="text-align:center">
		<!-- 交易成功 -->
		<font style="font-weight:bold;font-size:1.2em"><spring:message code="LB.Transaction_successful" /></font>
	</div>
	<br/>
	<table class="print">
		<tr>
			<!-- 交易時間 -->
			<th><spring:message code="LB.Trading_time" /></th>
			<th>${CMQTIME }</th>
		</tr>
		<tr>
			<!-- 欲結清銷號 -->
			<th><spring:message code="LB.D0424" /></th>
			<th>${ACN }</th>
		</tr>
		<tr>
			<!-- 結清轉入本行帳號 -->
			<th><spring:message code="LB.D0441" /></th>
			<th>${INACN }</th>
		</tr>
		<tr>
			<!-- 結清帳號帳戶餘額 -->
			<th><spring:message code="LB.D0442" /></th>
			<th>${O_TOTBAL }</th>
		</tr>
		<tr>
			<!-- 結清帳號可用餘額 -->
			<th><spring:message code="LB.D0443" /></th>
			<th>${O_AVLBAL }</th>
		</tr>
		<tr>
			<!-- 轉入帳號帳戶餘額 -->
			<th><spring:message code="LB.Payees_account_balance" /></th>
			<th>${I_TOTBAL }</th>
		</tr>
		<tr>
			<!-- 轉入帳號可用餘額 -->
			<th><spring:message code="LB.Payees_available_balance" /></th>
			<th>${I_AVLBAL }</th>
		</tr>
		
	</table>
	<br/><br/><br/><br/>
</body>
</html>