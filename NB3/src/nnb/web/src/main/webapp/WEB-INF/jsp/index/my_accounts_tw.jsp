<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 臺幣資產總額 -->
<div class="account-left tab-pane fade show active" id="NTD" role="tabpanel" aria-labelledby="tab-NTD">
    <div class="main-account">
        <span class="title"><spring:message code="LB.X2304"/><span class="description"></span></span>
        <span class="content"><span class='content' onclick='showEyeClick()'><img src='${__ctx}/img/eye-solid-orange.svg' /></span></span>
<!--         <span id="assets_tw" class="content"></span> -->
    </div>
    <div class="account-item">
        <div class="account-item-show">
            <span><spring:message code="LB.X2305"/></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/NT/ACCT/balance_query','', '')" id="balance_account_tw"></a></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/NT/ACCT/TRANSFER/transfer','', '')"><spring:message code="LB.NTD_transfer_1"/></a></span>
        </div>
        <div class="account-item-hide">
            <span id="balance_money_tw"></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="account-item">
    	<div class="account-item-show">
            <span><spring:message code="LB.X2306"/></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/NT/ACCT/time_deposit_details','', '')" id="deposit_account_tw"></a></span>
            <span><a href="#" onclick="fstop.getPage('/nb3'+'/NT/ACCT/TDEPOSIT/deposit_transfer','', '')"><spring:message code="LB.X2307"/></a></span>
        </div>
        <div class="account-item-hide">
            <span id="deposit_money_tw"></span>
            <span></span>
            <span></span>
        </div>
    </div>
    
   	<div class="main-account"></div>
	<div class="account-item">
    	<div style="font-size: 1rem; color: #6c757d; text-align: left;">
    		<spring:message code="LB.Note"/>：<spring:message code="LB.X2387"/>
    	</div>
    </div>
</div>

 
<script type="text/JavaScript">

function getTW_aj() {
	// 先清空資料初始化
	$("#balance_money_tw").empty();
	$("#balance_account_tw").empty();
	$("#deposit_money_tw").empty();
	$("#deposit_account_tw").empty();
// 	$("#assets_tw").empty();
// 	$("#assets_tw").prepend("<span class='content' onclick='showEyeClick()'><img src='${__ctx}/img/eye-solid-orange.svg' /></span>");
	
	console.log("getTW_aj.now: " + new Date());
	
	uri = '${__ctx}' + "/INDEX/summary_TW";

	fstop.getServerDataEx(uri, null, true, showTW);
}

function showTW(data) {
	// 避免打很多次回來同時顯示多次的資料，故再次清空資料
	$("#balance_money_tw").empty();
	$("#balance_account_tw").empty();
	$("#deposit_money_tw").empty();
	$("#deposit_account_tw").empty();
// 	$("#assets_tw").empty();
	
	console.log("showTW.now: " + new Date());
	
	// --------------------------------------------------- 臺幣活存 --------------------------------------------------------
// 	console.log("showTW.summary_TW_balance: " + data.data.summary_TW_balance);
	if (data.data.summary_TW_balance != 'error' && data.data.summary_TW_balance != 'null' && data.data.summary_TW_balance != null) {
		var balance_money_tw, balance_account_tw, balance_cmrecnum_tw;
		// 臺幣活存
		JSON.parse(data.data.summary_TW_balance, function(k, v) {
	// 		console.log("k= " + k + ", v=" + v);
			// 
			if (k == 'TOTAL_ADPIBAL') {
				balance_money_tw = v;
			}
			if (k == 'CMRECNUM') {
				balance_cmrecnum_tw = v;
			}
			if (k == 'ACN') {
				balance_account_tw = v;
			}
			if (k == 'msgCode') {
				msgcode = v;
			}
		});
		
		console.log("showTW.balance_money_tw: " + balance_money_tw);
		console.log("showTW.balance_account_tw: " + balance_account_tw);
		console.log("showTW.balance_cmrecnum_tw: " + balance_cmrecnum_tw);
		console.log("showTW.msgcode: " + msgcode);
		
		if (msgcode != 'ENRD') {
			$("#balance_money_tw").prepend("<span class='accountST' account='" + fstop.fmoney(balance_money_tw, 2) + "'>" + coverSoA(fstop.fmoney(balance_money_tw, 2)) + "</span> <spring:message code='LB.D0509'/>");
			balance_cmrecnum_tw == 1 ? $("#balance_account_tw").prepend(balance_account_tw) : $("#balance_account_tw").prepend("<spring:message code='LB.X2308'/>&nbsp;<spring:message code='LB.D0360_1'/> " + balance_cmrecnum_tw + " <spring:message code='LB.X2310'/>");
		
		} else {
			console.log("showTW_balance.ENRD...");
			$("#balance_account_tw").prepend("<spring:message code='LB.D0017'/>");
			balance_money_tw = 0;
		}
		
	} else {
		console.log("showTW_balance.error...");
		$("#balance_account_tw").prepend("<spring:message code='LB.D0017'/>");
	}
	
// --------------------------------------------------- 臺幣定存 --------------------------------------------------------
// 	console.log("showTW.summary_TW_deposit: " + data.data.summary_TW_deposit);
	if (data.data.summary_TW_deposit != 'error' && data.data.summary_TW_deposit != 'null' && data.data.summary_TW_deposit != null) {
		var deposit_money_tw, deposit_account_tw, deposit_cmrecnum_tw, msgcode;
		// 臺幣定存
		JSON.parse(data.data.summary_TW_deposit, function(k, v) {
	// 		console.log("k= " + k + ", v=" + v);
			// 
			if (k == 'DPTAMT') {
				deposit_money_tw = v;
			}
			if (k == 'CMRECNUM') {
				deposit_cmrecnum_tw = v;
			}
			if (k == 'ACN') {
				deposit_account_tw = v;
			}
			if (k == 'msgCode') {
				msgcode = v;
			}
		});
		
		console.log("showTW.deposit_money_tw: " + deposit_money_tw);
		console.log("showTW.deposit_account_tw: " + deposit_account_tw);
		console.log("showTW.deposit_cmrecnum_tw: " + deposit_cmrecnum_tw);
		console.log("showTW.msgcode: " + msgcode);
		
		if (msgcode != 'ENRD') {
			$("#deposit_money_tw").prepend("<span class='accountST' account='" + fstop.fmoney(deposit_money_tw, 2) + "'>" + coverSoA(fstop.fmoney(deposit_money_tw, 2)) + "</span> <spring:message code='LB.D0509'/>");
			deposit_cmrecnum_tw == 1 ? $("#deposit_account_tw").prepend(deposit_account_tw) : $("#deposit_account_tw").prepend("<spring:message code='LB.X2309'/>&nbsp;<spring:message code='LB.D0360_1'/> " + deposit_cmrecnum_tw + " <spring:message code='LB.X2310'/>");
		
		} else {
			console.log("showTW_deposit.ENRD...");
			$("#deposit_account_tw").prepend("<spring:message code='LB.D0017'/>");
// 			$("#assets_tw").prepend("<span class='content' onclick='showEyeClick()'><img src='${__ctx}/img/eye-solid-orange.svg' /></span>");
			deposit_money_tw = 0;
		}
		// total_money_tw
// 		var	total_money_tw = parseFloat(balance_money_tw) + parseFloat(deposit_money_tw);
// 		console.log("showTW.total_money_tw: " + total_money_tw);
// 		$("#assets_tw").prepend("<span class='accountST' account='" + fstop.fmoney(String(total_money_tw), 2) + "'>" + coverSoA(fstop.fmoney(String(total_money_tw), 2)) + "</span> <spring:message code='LB.D0509'/><span class='content' onclick='showEyeClick()'><img src='${__ctx}/img/eye-solid-orange.svg' /></span>");


	} else {
		console.log("showTW_deposit.error...");
		$("#deposit_account_tw").prepend("<spring:message code='LB.D0017'/>");
// 		$("#assets_tw").prepend("<span class='content' onclick='showEyeClick()'><img src='${__ctx}/img/eye-solid-orange.svg' /></span>");
	}
	
	// 解遮罩
	unAcctsBlock('NTD');
}

</script>