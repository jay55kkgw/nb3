<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
//             initFootable(); // 將.table變更為footable 
			   initDataTable(); //datatable 初始化
            init();
        });

        
       
        
        function init() {
            //列印
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"gold_reservation_query_print",
					//預約黃金交易查詢/取消
					"jspTitle":'<spring:message code= "LB.X0948" />',
					"CMQTIME":"${result_data.data.CMQTIME}",
					"CMRECNUM":"${result_data.data.CMRECNUM_TOTAL}",
					"COUNT_APPTYPE_BUY":"${result_data.data.COUNT_APPTYPE_BUY}",
					"COUNT_APPTYPE_SELL":"${result_data.data.COUNT_APPTYPE_SELL}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
        }
        
    	//選項
	 	function formReset() {
	 		initBlockUI();
		}
	 	//到單查詢按鈕
	 	function process(APPTYPE, APPDATE, APPTIME, ACN, ACN2, TRNGD, TRNGD_str){
			$("#APPTYPE").val(APPTYPE);
			$("#APPDATE").val(APPDATE);
			$("#APPTIME").val(APPTIME);
			$("#ACN").val(ACN);
			$("#SVACN").val(ACN2);
			$("#TRNGD").val(TRNGD);
			$("#TRNGD_str").val(TRNGD_str);
			var action = '${__ctx}/GOLD/PASSBOOK/gold_reservation_query_detail';
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").submit();
	 	}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
	<!-- 查詢服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.use_component_P1_D1-1" /></li>
    <!-- 預約取消/查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1491" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 預約黃金交易查詢/取消 -->
					<spring:message code= "LB.X0948" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post">
                    <input type="hidden" id="APPTYPE" name="APPTYPE" value="" />
                    <input type="hidden" id="APPDATE" name="APPDATE" value="" />
                    <input type="hidden" id="APPTIME" name="APPTIME" value="" />
                    <input type="hidden" id="ACN" name="ACN" value="" />
                    <input type="hidden" id="SVACN" name="SVACN" value="" />
                    <input type="hidden" id="TRNGD" name="TRNGD" value="" />
                    <input type="hidden" id="TRNGD_str" name="TRNGD_str" value="" />
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                            <ul class="ttb-result-list">
                                <li>
                                    <!-- 查詢時間 -->
                                    <h3><spring:message code="LB.Inquiry_time" /></h3>
                                    <p>
                                        
                                        ${result_data.data.CMQTIME }
                                    </p>
                                </li>
                                <li>
                                    <!-- 資料總數 : -->
                                    <h3><spring:message code="LB.Total_records" /></h3>
                                    <p>
                                        
                                        ${result_data.data.CMRECNUM_TOTAL}
                                        <!--筆 -->
                                        <spring:message code="LB.Rows" />
                                    </p>
                                </li>
                            </ul>
                            <!-- 表格區塊 -->
                        
                            <ul class="ttb-result-list">
                                <li>
                                    <!-- 資料總數 : -->
                                    <h3><spring:message code= "LB.W1060" /></h3>
                                    <p>
                                        <spring:message code= "LB.X0949" />
                                    </p>
                                </li>
                            </ul>
                            <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                                <thead>
<%-- 	                            	<tr> --%>
<!-- <!-- 交易種類 -->
<%-- 										<th><spring:message code= "LB.W1060" /></th> --%>
<!-- 										預約黃金買進 -->
<%-- 										<th class="text-left" colspan="5"><spring:message code= "LB.X0949" /></th> --%>
<%-- 	                            	</tr> --%>
                                    <tr>
                                    <!-- 預約日期 -->
								      	<th><spring:message code= "LB.X0377" /></th>      
<!-- 臺幣轉出帳號 -->
								      	<th><spring:message code= "LB.W1496" /></th>
<!-- 黃金轉入帳號 -->
								      	<th><spring:message code= "LB.W1497" /></th>
<!-- 買進公克數 -->
								      	<th ><spring:message code= "LB.W1498" /></th>
								      	<th ><spring:message code="LB.Status" /></th>
                                        <!-- 執行選項 -->
                                        <th>
                                            <spring:message code="LB.Execution_option" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<c:if test="${ result_data.data.COUNT_APPTYPE_BUY > 0 }">
	                                    <c:forEach var="dataList" items="${ result_data.data.REC }">
	                                    	<c:if test="${ dataList.APPTYPE.equals('01') }">
	                                    		<tr>
		                                            <!--預約日期-->
		                                            <td class="text-center">${dataList.APPDATE_str }</td>
		                                            <!--台幣轉出帳號-->
		                                            <td class="text-center">${dataList.ACN2 }</td>
		                                       		<!-- 黃金轉入帳號 -->
		                                            <td class="text-center">${dataList.ACN }</td>
		                                        	<!-- 買進公克數 -->
		                                            <td  class="text-right">${dataList.TRNGD_str }</td>
		                                            <!-- 狀態 -->
		                                            <td class="text-center">${dataList.STATUS}</td>
		                                        	<!-- 執行選項 -->
		                                            <td class="text-center">
							                            <!-- 到單查詢  -->
							                            <!-- 預約取消 -->
							                            <input type="button" class="ttb-sm-btn btn-flat-orange" value="<spring:message code= "LB.X0951" />" onclick=
							                            				"process(	'${dataList.APPTYPE}',
							                            							'${dataList.APPDATE }',
							                            							'${dataList.APPTIME }',
							                            							'${dataList.ACN }',
							                            							'${dataList.ACN2 }',
							                            							'${dataList.TRNGD }',
							                            							'${dataList.TRNGD_str}')" />
													</td>
		                                        </tr>
	                                    	</c:if>
	                                    </c:forEach>
                                    </c:if>
                                	<c:if test="${ result_data.data.COUNT_APPTYPE_BUY <= 0 }">
	                                    <tr>
	                                    	<td><spring:message code="LB.Check_no_data" /></td>
	                                    	<td></td>
	                                    	<td></td>
	                                    	<td></td>
	                                    	<td></td>
	                                    	<td></td>
	                                    </tr>
                                	</c:if>
                                </tbody>
                            </table>
                            
                            
                            <ul class="ttb-result-list">
                                <li>
                                    <!-- 資料總數 : -->
                                    <h3><spring:message code= "LB.W1060" /></h3>
                                    <p>
                                        <spring:message code= "LB.X0952" />
                                    </p>
                                </li>
                            </ul>
                            <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                                <thead>
<%-- 	                            	<tr> --%>
<!-- 交易種類 -->
<%-- 										<th><spring:message code= "LB.W1060" /></th> --%>
<!-- 										預約黃金回售 -->
<%-- 										<th class="text-left" colspan="5"><spring:message code= "LB.X0952" /></th> --%>
<%-- 	                            	</tr> --%>
                                    <tr>
                                    <!-- 預約日期 -->
								      	<th><spring:message code= "LB.X0377" /></th>      
<!-- 黃金轉出帳號 -->
								      	<th><spring:message code= "LB.W1515" /></th>
<!-- 臺幣轉入帳號 -->
								      	<th><spring:message code= "LB.W1518" /></th>
<!-- 賣出公克數 -->
								      	<th ><spring:message code= "LB.W1519" /></th>
								      	<th ><spring:message code="LB.Status" /></th>
                                        <!-- 執行選項 -->
                                        <th>
                                            <spring:message code="LB.Execution_option" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<c:if test="${ result_data.data.COUNT_APPTYPE_SELL > 0 }">
	                                    <c:forEach var="dataList" items="${ result_data.data.REC }">
	                                    	<c:if test="${ dataList.APPTYPE.equals('02') }">
	                                    		<tr>
		                                            <!--預約日期-->
		                                            <td>${dataList.APPDATE_str }</td>
		                                            <!--黃金轉出帳號-->
		                                            <td>${dataList.ACN }</td>
		                                       		<!-- 台幣轉入帳號 -->
		                                            <td>${dataList.ACN2 }</td>
		                                        	<!-- 賣出公克數 -->
		                                            <td  class="text-right">${dataList.TRNGD_str }</td>
		                                            <!-- 狀態 -->
		                                            <td>${dataList.STATUS}</td>
		                                        	<!-- 執行選項 -->
		                                            <td>
							                            <!-- 到單查詢  -->
							                            <!-- 預約取消 -->
							                            <input type="button" class="ttb-sm-btn btn-flat-orange" value="<spring:message code= "LB.X0951" />" onclick=
							                            				"process(	'${dataList.APPTYPE}',
							                            							'${dataList.APPDATE }',
							                            							'${dataList.APPTIME }',
							                            							'${dataList.ACN }',
							                            							'${dataList.ACN2 }',
							                            							'${dataList.TRNGD }',
							                            							'${dataList.TRNGD_str}')" />
													</td>
		                                        </tr>
	                                    	</c:if>
	                                    </c:forEach>
                                    </c:if>
                                	<c:if test="${ result_data.data.COUNT_APPTYPE_SELL <= 0 }">
	                                    <tr>
	                                    	<td><spring:message code="LB.Check_no_data" /></td>
	                                    	<td></td>
	                                    	<td></td>
	                                    	<td></td>
	                                    	<td></td>
	                                    	<td></td>
	                                    </tr>
                                	</c:if>
                                </tbody>
                            </table>
	                        <!--button 區域 -->
	                        
	                            <!-- 列印  -->
	                            <spring:message code="LB.Print" var="printbtn"></spring:message>
	                            <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
	                        
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                    <div class="text-left">
                        <!-- 		說明： -->
						
                        <ol class="list-decimal description-list">
                        	<p><spring:message code="LB.Description_of_page"/></p>
                        <!-- 預約黃金買進將以預約後的第一個營業日的第一次牌告賣出價格交易，請於扣款前一日存足款項於轉出帳戶備扣。預約黃金回售將以預約後的第一個營業日的第一次牌告買進價格交易。 -->
                            <li><spring:message code="LB.Gold_Reservation_Query_P1_D1"/></li>
						<!-- 預約取消時間為預約後的第一個營業日（即買進扣款日、回售入帳日）08：59前。 -->
							<li><font color=red><spring:message code="LB.Gold_Reservation_Query_P1_D2"/></font></li>
                        </ol>
                    </div>
                </form>	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>