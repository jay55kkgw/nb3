<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>


	<script type="text/javascript">
		//Client Cross Frame Scripting Attack
		$(document).ready(function () {
			initDataTable(); // 將.table變更為DataTable
			setTimeout("dodatataable()",200);
		});
		
		function dodatataable(){
			var pre="上一頁";
			var next="下一頁";
			if(dt_locale == "zh_CN")
			{
				pre = "上一页";
				next = "下一页";
			}
			else if(dt_locale == "en")
			{
				pre="Previous";
				next="Next";
			}
			jQuery(function($) {
				$('.FxRemitQuery-table').DataTable({
					"columnDefs": [
		            	{ width: "60px", targets: [0]},
						{ width: "110px", targets: [1]}, 
		            ],
		            lengthChange: false,
		            scrollX: true, //水平滾動
		            sScrollX: "99%",
					bPaginate: true, //啟用或禁用分頁
					bFilter: false, //啟用或禁用數據過濾
					bDestroy: true, //如果沒有表與選擇器匹配，則將按照常規構造新的DataTable
					bSort: false, //啟用或禁用列排序
					info: false, //功能控製表信息顯示字段。
					scrollCollapse: true, 
					language:{
						"paginate":{
							"previous":pre,
							"next":next
						}
					}
				});
			});
		}

		
	</script>
	<title>
		<spring:message code="LB.Title" />
	</title>
</head>

<body>
	<main class="col-12">
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<form method="post" id="formId" action="">
				<h2><spring:message code= "LB.X1467" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
					<ul class="ttb-result-list"></ul>
					  <table class="stripe table-striped ttb-table FxRemitQuery-table" data-toggle-column="first">
						  	<thead>
								<tr>
									<!-- 分類編號 -->
									<th ><spring:message code= "LB.X1567" /></th>
									<!-- 項目 -->
									<th ><spring:message code= "LB.D0271" /></th>
									<!-- 說明 -->
									<th ><spring:message code= "LB.Description_of_page" /></th>
								</tr>
							<thead>
							<tbody>
								<c:forEach var="dataList" items="${FxRemitQuery_1.data.REC}">
										<tr>
											<td style="color: #007bff">
												<a href="FxRemitQuery_2?ADMKINDID=${dataList.ADMKINDID}&ADLKINDID=${dataList.ADLKINDID}&ADRMTTYPE=${dataList.ADRMTTYPE}&CUTTYPE=${FxRemitQuery_1.data.str_CUTTYPE}&CNTY=${FxRemitQuery_1.data.CNTY}&REMTYPE=${FxRemitQuery_1.data.REMTYPE}">
												${dataList.ADMKINDID}
												</a>
											</td>
											<td>${dataList.ADRMTITEM}</td>
											<td>${dataList.ADRMTDESC}</td>
										</tr>
								</c:forEach>
							</tbody>
						</table>
						<!-- 					回上一頁 -->
						<input class="ttb-button btn-flat-orange" type="button" name="TransactionSubmit"
						value="<spring:message code="LB.Back_to_previous_page" />" onclick="history.back()"/>
					</div>
				</div>
			</form>
		</section>
	</main> <!-- 		main-content END -->
</body>

</html>