<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<style>
   /* footable */
.table {
    border-collapse: separate;
    border-spacing: 0;
    width: 100%;
    border: solid #DDD 1px;
    -moz-border-radius: 6px;
    -webkit-border-radius: 6px;
    border-radius: 6px;
    margin: 20px auto;
}

.table th {
    cursor: n-resize;
}

.table td a {
    text-decoration: none;
    color: #000;
}

.table td a.origin {
    text-decoration: underline;
    color: blue;
}

.table td a:hover {}

.table.breakpoint>tbody>tr>td.expand {
    background: none;
    /*background: url('../img/plus.png') no-repeat 5px center;*/
    padding-left: 40px;
}

.table.breakpoint>tbody>tr>td.expand:before {
    content: "";
}

.table.breakpoint>tbody>tr.table-detail-show>td.expand {
    background: none;
    /*background: url('minus.png') no-repeat 5px center;*/
}

.table.breakpoint>tbody>tr.table-detail-show>td.expand:before {
    content: "";
}

.table.breakpoint>tbody>tr.table-row-detail {
    background: #FAFAFA;
}

.table>tbody>tr:hover {
    background: #fbf8e9;
}

.table.breakpoint>tbody>tr:hover:not (.table-row-detail) {
    cursor: pointer;
}

.table>tbody>tr>td, .table>thead>tr>th {
    border-left: 1px solid #DDD;
    border-top: 1px solid #DDD;
    padding: 8px;
    text-align: center;
}

.table>tbody>tr>td.table-cell-detail {
    border-left: none;
}

table>tbody>tr>td>span.table-toggle {
    display: inline;
}

.table>tbody>tr td:first-child {
    padding-left: 20px;
}

.table>thead>tr>th, .table>thead>tr>td {
    background-color: #EEEEEE;
    border-top: none;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    font-weight: bold;
}

.table>thead>tr:first-child>th.table-first-column, .table>thead>tr:first-child>td.table-first-column {
    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;
    border-radius: 6px 0 0 0;
}

.table>thead>tr:first-child>th.table-last-column, .table>thead>tr:first-child>td.table-last-column {
    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;
    border-radius: 0 6px 0 0;
}

.table>thead>tr:first-child>th.table-first-column.table-last-column,
.table>thead>tr:first-child>td.table-first-column.table-last-column {
    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;
    border-radius: 6px 6px 0 0;
}

.table>tbody>tr:last-child>td.table-first-column {
    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;
    border-radius: 0 0 0 6px;
}

.table>tbody>tr:last-child>td.table-last-column {
    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;
    border-radius: 0 0 6px 0;
}

.table>tbody>tr:last-child>td.table-first-column.table-last-column {
    -moz-border-radius: 0 0 6px 6px;
    -webkit-border-radius: 0 0 6px 6px;
    border-radius: 0 0 6px 6px;
}

.table>thead>tr>th.table-first-column, .table>thead>tr>td.table-first-column,
.table>tbody>tr>td.table-first-column {
    border-left: none;
}

.table>tbody img {
    vertical-align: middle;
}

.table>tfoot>tr>th, .table>tfoot>tr>td {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc),
        to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: linear-gradient(to bottom, #ebf3fc, #dce9f9);
    -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, .8) inset;
    -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, .8) inset;
    box-shadow: 0 1px 0 rgba(255, 255, 255, .8) inset;
    border-top: 1px solid #ccc;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    padding: 10px;
}

.table tbody>tr:nth-child(odd)>td, .table tbody>tr:nth-child(odd)>th {
    background-color: transparent;
}

.table-nav {
    list-style: none;
    flex: 1 0 70%;
    margin: 0;
}

.table-nav li {
    display: inline-block;
}

.table-nav li a {
    display: inline-block;
    padding: 5px 10px;
    text-decoration: none;
    font-weight: bold;
    color: #000;
}

.table-nav .table-page-current {
    border-radius: 50%;
    background-color: #FF6600;
}

.table-nav .table-page-current a {
    color: #fff;
}

table.table-details {
    text-align: left;
}

table.table-details th, table.table-details td {
    padding: 0px;
}

table.table-details>tbody>tr>th:nth-child(1), table.table-details>tbody>tr>td:nth-child(2) {
    display: inline-block !important;
    text-align: left !important;
    border-top: none;
}

table.table-details>tbody>tr>th:nth-child(1) {
    width: max-content;
    padding-right: 10px;
    width: -moz-max-content;
    width: -webkit-max-content;
    width: -o-max-content;
    width: -ms-max-content;
}
h3{
	margin-bottom:20px; !important
}
</style>
<script type="text/javascript">
$(document).ready(function() {
	$('#CMCANCEL1').click(function(){
		$('#formId').attr("action","${__ctx}/CREDIT/APPLY/instalment_repayment");
		$('#formId').submit();
	})
	$('#CMCANCEL2').click(function(){
		$('#formId').attr("action","${__ctx}/CREDIT/APPLY/instalment_repayment");
		$('#formId').submit();
	})
	$("#CMSUBMIT").click( function(e) {
		// 送出進表單驗證前將span顯示
		
		console.log("submit~~");
		
		// 表單驗證
		if ( !$('#formId').validationEngine('validate') ) {
			e.preventDefault();
		} else {
			// 解除表單驗證
			$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
			initBlockUI();
			$("#formId").validationEngine('detach');
			$("#formId").submit();
		}
	});
	
})
function processQuery() {
	var formId = document.getElementById("formId");
	
	if (!CheckNumber("<spring:message code= "LB.D0211" />", formId.AMOUNT_TEXT, false)) {
      return false;
    }
	
	if (getSelectedPeriod() == -1) {
	  //alert('<spring:message code= "LB.Alert047" />');
	  errorBlock(
							null, 
							null,
							['<spring:message code= 'LB.Alert047' />'], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
	  return false;
	}
	
	processInputData();
	
	//檢核每期還款金額不得低於1000
	if (parseInt(formId.PERIOD_AMOUNT.value) < 1000) {
	  //alert('<spring:message code= "LB.Alert048" />');
	  errorBlock(
							null, 
							null,
							['<spring:message code= 'LB.Alert048' />'], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
	  return false;
	}
	
	$('#SSL').show();
	$('#Step1button').hide();
	$('#Step2button').show();
	var peroidAmount = $('#AMOUNT_TEXT').val();
	
	$('#AMOUNT').val($('#AMOUNT_TEXT').val());
	$('#AMOUNT_IUPUT').html(FormatNumber(peroidAmount));
	
	$("input[name='PERIOD_RADIO']:checked").each(function(){
		$('#PERIOD_TEXT').html($(this).val()+"<spring:message code= "LB.W0854" />")
		$('#PERIOD').val($(this).val());
	})
	$('#CARDNUM').val($('#CARDNUMSELECT').val());
	$('#CARDNUM_TEXT').html($('#CARDNUMSELECT').val());
	
	return false;
  } 
	
	

	function FloatAdd(arg1, arg2) {
		var r1, r2, m;
		try {
			r1 = arg1.toString().split(".")[1].length
		} catch (e) {
			r1 = 0
		}
		try {
			r2 = arg2.toString().split(".")[1].length
		} catch (e) {
			r2 = 0
		}
		m = Math.pow(10, Math.max(r1, r2));
		return (arg1 * m + arg2 * m) / m;
	}
	
	
	function FormatNumber(n) {
		n += "";
		var arr = n.split(".");
		var re = /(\d{1,3})(?=(\d{3})+$)/g;
		return arr[0].replace(re, "$1,")
				+ (arr.length == 2 ? "." + arr[1] : "");
	}
	
	  
	function getRateAdjust(level, peroid) {
		var peroidArr = [ "6", "12", "18", "24", "30", "36", "48", "60" ];
		var lowLevelRateArr = [ -1.25, -1, -0.75, -0.5, -0.25, -0.2, -0.15,
				-0.05 ];
		var heighLevelRateArr = [ -1.25, -1, -0.75, -0.5, -0.25, 0, 0.2, 0.4 ];
		switch (level) {
		case 1:
		case 2:
		case 3:
			return lowLevelRateArr[peroidArr.indexOf(peroid)];
			break;
		case 4:
		case 5:
			return heighLevelRateArr[peroidArr.indexOf(peroid)];
			break;
		}
	}
	
	function getSelectedPeriod() {
		var formId = document.getElementById("formId");
		var period = -1;
		var radios = formId.elements["PERIOD_RADIO"];
		for (var i = 0; i < radios.length; i++) {
			var radio = radios[i];
			if (radio.checked) {
				period = radio.value;
			}
		}
		return period;
	}
	
	function checkAcceptRate(period) {
	    var formId = document.getElementById("formId");
	    formId.APPLY_RATE.value = '';
	    var pot = formId.POT.value;
	    var orgRate = parseFloat(formId.RATE.value);
	    if(pot=="15" || pot=="14")
	    	orgRate = '${instalment_repayment_input.data.RATE1}';
		var newRate = FloatAdd(orgRate, getRateAdjust(parseInt(pot) - 10,
				period));
		if (newRate < 0)
			newRate = 0;
		formId.APPLY_RATE.value = newRate;
		return true;
	}
	
	function checkApplyAmount() {
	    var formId = document.getElementById("formId");
	    formId.FIRST_AMOUNT.value = '';
	    formId.FIRST_INTEREST.value = '';
	    if (!CheckNumber("<spring:message code= "LB.D0211" />", formId.AMOUNT_TEXT, true)) {
	    	formId.AMOUNT.value = '';
	      return false;
	    } else {
	      return true;
	    }
	  }
	
	function calculateFirstAmountAndInterest(period) {
	    var formId = document.getElementById("formId");
	    var amount = parseInt(formId.AMOUNT_TEXT.value);
	    var applyRate = parseFloat(formId.APPLY_RATE.value);
	    formId.PERIOD_AMOUNT.value = Math.floor(amount / period);
	    formId.FIRST_AMOUNT.value = Math.floor(amount / period) + (amount % period);
	    //利息計算 = 分期總金額 X ((核定利率 / 365) X 30)
	    formId.FIRST_INTEREST.value = Math.round(amount * (((applyRate/100) / 365) * 30));
	    return true;
	  }
	
	function processInputData() {
	    var rateDone = false;
	    var amountDone = false;
	    var period = getSelectedPeriod();
	    
	    $('#applyRate').innerHTML = '';
	    $('#firstAmount').innerHTML = '';
	    $('#firstInterest').innerHTML = '';
	      
	    if (period != -1) {
		  if (checkAcceptRate(period)) {
	        rateDone = true;
	      }
	    }
	    if (formId.AMOUNT_TEXT.value != '' && checkApplyAmount()) {
	      //當「分期總金額」大於「可申請參加金額」時，以後者代入
	      if (parseInt(formId.AMOUNT_TEXT.value) > parseInt(formId.CURRBAL.value))
	        formId.AMOUNT_TEXT.value = formId.CURRBAL.value;
	      amountDone = true;
	    }
	    if (amountDone == true && rateDone == true) {
	      
	      //當「申請金額」超過「原信用卡額度」比率50%(含)以上，酌減利率0.25%
	      var applyAmount = parseInt(formId.AMOUNT_TEXT.value);
	      var creditLimit = parseInt(formId.CRLIMIT.value);
	      if (applyAmount >= Math.round(creditLimit / 2)) {
	        //alert('酌減利率0.25%');
	        //alert(FloatAdd(formId.APPLY_RATE.value, -0.25));
	        formId.APPLY_RATE.value = FloatAdd(formId.APPLY_RATE.value, -0.25);
	      }
	      if (calculateFirstAmountAndInterest(period)) {
	        $('#firstAmount').text(FormatNumber(formId.FIRST_AMOUNT.value));
	        $('#firstInterest').text(FormatNumber(formId.FIRST_INTEREST.value));
	      }
	    }
	    if (formId.APPLY_RATE.value != '')
	      $('#applyRate').text(formId.APPLY_RATE.value + "%") ;
	  }
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 長期使用循環信用申請分期還款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0194" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 	快速選單及主頁內容 -->
		 <main class="col-12">
            <section id="main-content" class="container">
				<form id="formId" method="post" action="${__ctx}/CREDIT/APPLY/instalment_repayment_result" >
				  <input type="hidden" name="CUSNAME" value='${instalment_repayment_input.data.CUSNAME}'>
				  <input type="hidden" name="CUSTID" value='${instalment_repayment_input.data.CUSTID}'>
				  <input type="hidden" name="HPHONE" value='${instalment_repayment_input.data.HPHONE}'>
				  <input type="hidden" name="OPHONE" value='${instalment_repayment_input.data.OPHONE}'>
				  <input type="hidden" name="MPFONE" value='${instalment_repayment_input.data.MPFONE}'>
				  <input type="hidden" name="CURRBAL" value='${instalment_repayment_input.data.CURRBAL}'>
				  <input type="hidden" name="POT" value='${instalment_repayment_input.data.POT}'>
				  <input type="hidden" name="RATE" value='${instalment_repayment_input.data.RATE}'>
				  <input type="hidden" name="POTDESC" value='${instalment_repayment_input.data.POTDESC}'>
				  <input type="hidden" name="CRLIMIT" value='${instalment_repayment_input.data.CRLIMIT}'>
				  <input type="hidden" id="AMOUNT" name="AMOUNT" value=''>
				  <input type="hidden" id="CARDNUM" name="CARDNUM" value=''>
				   <input type="hidden" id="PERIOD" name="PERIOD" value=''>
				  <input type="hidden" name="APPLY_RATE" value=''>
				  <input type="hidden" name="PERIOD_AMOUNT" value=''>
				  <input type="hidden" name="FIRST_AMOUNT" value=''>
				  <input type="hidden" name="FIRST_INTEREST" value=''>
				  <input type="hidden" id="PINNEW" name="PINNEW" value="">
				  <input type="hidden" id="USERIP" name="USERIP" value="${sessionScope.userip}">
				  
				  
                <h2><spring:message code="LB.D0194" /><!-- 長期使用循環信用申請分期還款 --></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

                <div class="main-content-block row">
                    <div class="col-12">
                    	<div class="ttb-input-block">
                    		<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.Name" /><!-- 姓名 --></h4></label>
								</span>
								<span class="input-block">
									${instalment_repayment_input.data.CUSNAME}
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0204" /><!-- 身分證字號 --></h4></label>
								</span>
								<span class="input-block">
									${instalment_repayment_input.data.CUSTID}
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0205" /><!-- 連絡電話 --></h4></label>
								</span>
								<span class="input-block">
									<c:if test="${instalment_repayment_input.data.HPHONE !=''}">
									<spring:message code="LB.D0206" /><!-- 住家電話 -->：${instalment_repayment_input.data.HPHONE} <br>
									</c:if>
									<c:if test="${instalment_repayment_input.data.MPFONE !=''}">
									<spring:message code= "LB.D0207" />：${instalment_repayment_input.data.MPFONE}<br>
									</c:if>
									<c:if test="${instalment_repayment_input.data.OPHONE !=''}">
									<spring:message code="LB.D0094" /><!-- 公司電話 -->：${instalment_repayment_input.data.OPHONE} <br>
									</c:if>
								</span><br>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.Credit_card_number" /><!-- 信用卡卡號 --></h4></label>
								</span>
								<span class="input-block" id="CARDNUM_TEXT">
									
									<select class="custom-select select-input half-input" id="CARDNUMSELECT">
									<c:forEach var="CARDNUMLIST" items="${instalment_repayment_input.data.CARDNUMS}">
										<option value="${CARDNUMLIST}">${CARDNUMLIST}</option>
									</c:forEach>
									</select>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0209" /><!-- 可申請分期金額 --></h4></label>
								</span>
								<span class="input-block">
									${instalment_repayment_input.data.CURRBAL_SHOW}
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0210" /><!-- 目前適用利率 --></h4></label>
								</span>
								<span class="input-block">
									${instalment_repayment_input.data.RATE_SHOW}%&nbsp;${instalment_repayment_input.data.POTDESC}
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0211" /><!-- 分期總金額 --></h4></label>
								</span>
								<span class="input-block" id="AMOUNT_IUPUT">
									<input class="text-input" id="AMOUNT_TEXT" name="AMOUNT_TEXT"  onkeyup="processInputData()"/>&nbsp;&nbsp;&nbsp;<spring:message code="LB.Dollar" /><!-- 元 -->
								</span>
							</div>
							
                    		<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0212" /><!-- 分期期數 --></h4></label>
								</span>
								<span class="input-block" id="PERIOD_TEXT">
								<label class="radio-block"> 
								<input  id="per_06" type="radio" name="PERIOD_RADIO" value="6" onclick="processInputData()" >
								6<spring:message code="LB.W0854" /><!-- 期 -->
								<span class="ttb-radio"></span>
								</label>
								<label class="radio-block"> 
								<input  id="per_12" type="radio" name="PERIOD_RADIO" value="12" onclick="processInputData()">
								12<spring:message code="LB.W0854" /><!-- 期 -->
								<span class="ttb-radio"></span>
								</label>
								<label class="radio-block"> 
								<input  id="per_18" type="radio" name="PERIOD_RADIO" value="18" onclick="processInputData()">
								18<spring:message code="LB.W0854" /><!-- 期 -->
								<span class="ttb-radio"></span>
								</label>
								<label class="radio-block"> 
								<input  id="per_24" type="radio" name="PERIOD_RADIO" value="24" onclick="processInputData()">
								24<spring:message code="LB.W0854" /><!-- 期 -->
								<span class="ttb-radio"></span>
								</label>
								<label class="radio-block"> 
								<input  id="per_30" type="radio" name="PERIOD_RADIO" value="30" onclick="processInputData()">
								30<spring:message code="LB.W0854" /><!-- 期 -->
								<span class="ttb-radio"></span>
								</label>
								<label class="radio-block"> 
								<input  id="per_36" type="radio" name="PERIOD_RADIO" value="36" onclick="processInputData()">
								36<spring:message code="LB.W0854" /><!-- 期 -->
								<span class="ttb-radio"></span>
								</label>
								<label class="radio-block"> 
								<input  id="per_48" type="radio" name="PERIOD_RADIO" value="48" onclick="processInputData()">
								48<spring:message code="LB.W0854" /><!-- 期 -->
								<span class="ttb-radio"></span>
								</label>
								<label class="radio-block"> 
								<input id="per_60" type="radio" name="PERIOD_RADIO" value="60" onclick="processInputData()">
								60<spring:message code="LB.W0854" /><!-- 期 -->
								<span class="ttb-radio"></span>
								</label>
								</span>
                        </div>
                        <div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0213" /><!-- 核定利率 --></h4></label>
								</span>
								<span class="input-block">
									<span id="applyRate"/></span>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0214" /><!-- 本金一期 --></h4></label>
								</span>
								<span class="input-block">
									<span id="firstAmount"/></span>&nbsp;<spring:message code="LB.Dollar_1" /><!-- 元 --><br>
									(<spring:message code="LB.D0215" /><!-- 不含利息，分期總金額除以分期期數後之剩餘款項將併入第一期分期金額 -->)
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0216" /><!-- 第一期利息 --></h4></label>
								</span>
								<span class="input-block">
									<span id="firstInterest"/></span>&nbsp;<spring:message code="LB.Dollar_1" /><!-- 元 --><br>
									(<spring:message code="LB.D0217" /><!-- 此金額為試算，以帳單為主 -->)
								</span>
							</div>
							<br><br>
							<div class="ttb-input-item row" id="SSL" style="display:none;">
								<span class="input-title">
									<label><h4><spring:message code="LB.Transaction_security_mechanism" /><!-- 交易機制 --></h4></label>
								</span>
								<span class="input-block">
										<label class="radio-block"> <spring:message
												code="LB.SSL_password" />
											<!-- 交易密碼(SSL) --> <input type="radio" name="FGTXWAY"
											checked="checked" value="0" checked>
											<span class="ttb-radio"></span>
										</label>
										<input type="password" id="CMPASSWORD" name="CMPASSWORD" type="password"
											maxlength="8" value=""
											class="text-input  validate[required,custom[onlyLetterNumber]]"
											placeholder="<spring:message code="LB.Please_enter_password" />">
										<!-- 請輸入交易密碼 -->
								</span>
							</div>
							
							<div id="Step1button" class="text-center">
							<br>
							<input class="ttb-button btn-flat-gray"  type="button" value="<spring:message code="LB.Cancel" />" id="CMCANCEL1" name="CMCANCEL1" >
							<input class="ttb-button btn-flat-orange"  type="button" value="<spring:message code="LB.Confirm_1" />" id="STEP1" onclick="return processQuery()">
							</div>
							<div id="Step2button" class="text-center" style="display: none;">
							<input class="ttb-button btn-flat-gray"  type="button" value="<spring:message code="LB.Cancel" />" id="CMCANCEL2" name="CMCANCEL2" >
							<input class="ttb-button btn-flat-orange"  type="button" value="<spring:message code="LB.Confirm_1" />" id="CMSUBMIT" name="CMSUBMIT" >
							</div>
							
						 <div class="ttb-message text-left" style="width: 85%;margin-top: 100px">
                            <span><font style="color:black"><spring:message code="LB.X0880" /><!-- 註 --> ：1.<spring:message code="LB.X0881" /></font></span><br>
      	  					<span><font style="color:black">　　2.<spring:message code="LB.X0874" /></font></span><br>
      	  					<span><font style="color:black">　　3.<spring:message code="LB.X0883" /></font></span><br>      	  
      	  					<span><font style="color:black">　　4.<spring:message code="LB.X0884" /></font></span> 
                        </div>
						<div class="CN19-clause" style="width: 85%;margin:auto;overflow:auto;">
                        <table class="table" data-show-toggle="false">
                            <thead>
                                <tr>
                                    <th data-title="分期期數" rowspan="2"><spring:message code="LB.D0212" /><!-- 分期期數 --></th>
                                    <th data-title="差別利率" colspan="5"><spring:message code="LB.D0199" /><!-- 差別利率 --></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th></th>
									<td nowrap><spring:message code="LB.X0875" /></td><!-- 第一級 -->
                                    <td nowrap><spring:message code="LB.X0876" /></td><!-- 第二級 -->
                                    <td nowrap><spring:message code="LB.X0877" /></td><!-- 第三級 -->
                                    <td nowrap><spring:message code="LB.X0878" /></td><!-- 第四級 -->
                                    <td nowrap><spring:message code="LB.X0879" /></td><!-- 第五級 -->
                                </tr>
								 <tr data-expanded="false">
                                    <td class="expand">6<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />1.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />1.25%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">12<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />1%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />1%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">18<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.75%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.75%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.75%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.75%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">24<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.5%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.5%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.5%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.5%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">30<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.25%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">36<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.2%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.2%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.2%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.2%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">48<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.15%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.15%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.15%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.2%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">60<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.05%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.05%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.05%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.4%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
						</div>
					
                    </div>
                </div>
                </form>
            </section>
        </main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>