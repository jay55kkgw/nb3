<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/><br/><br/>
	
	<table class="print">
		<tr>
			<!-- 交易日期 -->
			<th><spring:message code="LB.Transaction_date" /></th>
			<th>${TRADEDATE_F }</th>
		</tr>
		<tr>
			<!-- 信託編號 -->
			<th><spring:message code="LB.W0904" /></th>
			<th>${CDNO_F }</th>
		</tr>
		<tr>
			<!-- 轉出基金名稱 -->
			<th><spring:message code="LB.W0963" /></th>
			<th>${TRANSCODE_FUNDLNAME }</th>
		</tr>
		<tr>
			<!-- 轉出基金信託金額 -->
			<th><spring:message code="LB.W0964" /></th>
			<th>${TRANSCRY_CRYNAME } ${FUNDAMT_F }</th>
		</tr>
		<tr>
			<!-- 轉出基金單位數 -->
			<th><spring:message code="LB.W0965" /></th>
			<th>${UNIT_F }</th>
		</tr>
		<tr>
			<!-- 轉出基金單位淨值 -->
			<th><spring:message code="LB.W0966" /></th>
			<th>${PRICE1_F }</th>
		</tr>
		<tr>
			<!-- 轉入基金名稱 -->
			<th><spring:message code="LB.W0967" /></th>
			<th>${INTRANSCODE_FUNDLNAME }</th>
		</tr>
		<tr>
			<!-- 轉入基金信託金額 -->
			<th><spring:message code="LB.W0968" /></th>
			<th>${INTRANSCRY_CRYNAME } ${EXAMT_F }</th>
		</tr>
		<tr>
			<!-- 轉入基金單位數 -->
			<th><spring:message code="LB.W0969" /></th>
			<th>${TXUNIT_F }</th>
		</tr>
		<tr>
			<!-- 轉入基金單位淨值 -->
			<th><spring:message code="LB.W0970" /></th>
			<th>${PRICE2_F }</th>
		</tr>
		<tr>
			<!-- 交易匯率 -->
			<th><spring:message code="LB.W0971" /></th>
			<th>${EXRATE_F }</th>
		</tr>
		<tr>
			<!-- 轉換手續費-本行 -->
			<th><spring:message code="LB.W0972" /></th>
			<th>${AMT3_F }</th>
		</tr>
		<tr>
			<!-- 轉換手續費-基金機構 -->
			<th><spring:message code="LB.W0973" /></th>
			<th>${FCA2_F }</th>
		</tr>
		<tr>
			<!-- 補收手續費 -->
			<th><spring:message code="LB.W0974" /></th>
			<th>${AMT7_F }</th>
		</tr>
		<tr>
			<!-- 短線交易費用 -->
			<th><spring:message code="LB.W0975" /></th>
			<th>${CRY_CRYNAME } ${SHORTTRADEFEE_F }</th>
		</tr>
		<tr>
			<!-- 是否已分配 -->
			<th><spring:message code="LB.W0960" /></th>
			<th>${FUNDFLAG_F }</th>
		</tr>
	</table>
	<br/><br/><br/><br/>
</body>
</html>