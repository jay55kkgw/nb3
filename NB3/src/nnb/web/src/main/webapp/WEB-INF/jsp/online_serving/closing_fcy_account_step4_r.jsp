<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		// 確認鍵 click
		 submit();
		//網頁倒數計時(30秒)   
		 countDown();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click(function (e) {
			if (!$('#formId').validationEngine('validate')) {
				e = e || window.event; // for IE
				e.preventDefault();
			} else {
				$("#formId").validationEngine('detach');
				initBlockUI(); //遮罩
				$("#formId").submit();
			}
		});
	}
	
	//上一頁按鈕
	function back(){
		//此處ID可能不一樣
		$("#CMCANCEL").click(function() {
			var action = '${__ctx}/ACCOUNT/CLOSING/closing_fcy_account_step2';
			$('#back').val("Y");
			$("form").attr("action", action);
			initBlockUI();
			$("form").submit();
		});
	}

	//網頁倒數計時(30秒)    
	var sec = 30;
	var the_Timeout;
	function countDown() {
		var counter = document.getElementById("CountDown");		   		
	   	  counter.innerHTML = sec;
		sec--;
		if (sec == -1) {
			$("#CMSUBMIT").val("<spring:message code= "LB.X1291" />");
			$("#formId").removeAttr("target");
			$("#formId").attr("action", "${__ctx}/ACCOUNT/CLOSING/closing_fcy_account_step1");
			return false;
		}
		//網頁倒數計時(30秒)      	  	
		the_Timeout = setTimeout("countDown()", 1000);
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 外匯存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0299" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0299" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/ACCOUNT/CLOSING/closing_fcy_account_confirm_r">
                <c:set var="BaseResultData" value="${closing_fcy_account_step4_r.data}"></c:set>
                <input type="hidden" name="previousPageJson" value='${BaseResultData.previousPageJson}'>
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                             <div class="ttb-message">
                                <span><spring:message code="LB.D0467" /><font id="CountDown" color="red"></font><spring:message code="LB.D0467_1" /></span>
                            </div>
                            <!-- 轉出金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Deducted" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span class="ttb-unit">
										<!--幣別 -->
										${BaseResultData.OUT_CRY}
										</span>
										${BaseResultData.str_OutAmt}
										<span class="ttb-unit"><spring:message code="LB.Dollar_1" /></span>
									</div>
                                </span>		
                            </div>
							<!-- 轉入金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Buy" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span class="ttb-unit">
										<!-- 幣別 -->
										${BaseResultData.IN_CRY}
										</span>
										${BaseResultData.str_InAmt}
										<span class="ttb-unit"><spring:message code="LB.Dollar_1" /></span>
									</div>
                                </span>		
                            </div>
							<!-- 匯率-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Exchange_rate" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                   <span>
                                   ${BaseResultData.RATE}
                                   </span>
                                    </div>
                                </span>
                            </div>
							<!--議價編號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Bargaining_number" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
	                                   <span>
	                                   ${BaseResultData.BGROENO}
	                                   </span>
                                    </div>
                                </span>
                            </div>
						</div>
                        <input class="ttb-button btn-flat-gray" id="CMCANCEL" name="CMCANCEL" type="button" value="<spring:message code="LB.Cancel_transaction" />" onClick="goback()"/>
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm_1" />"/>
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>