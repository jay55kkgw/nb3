<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<label><spring:message code="LB.Inquiry_time" /> <!-- 查詢時間 -->：</label>
	<label>${CMQTIME}</label>
	<br />
	<label><spring:message code="LB.Inquiry_period_1" /> <!-- 查詢期間 -->：</label>
	<label>${CMPERIOD}</label>
	<br />
	<label><spring:message code="LB.Total_records" /> <!-- 資料總數 -->：</label>
	<label>${COUNT} <spring:message code="LB.Rows" /></label>
	<br />
	<br />
	<table class="print" width="90%">
		<thead>
			<tr>
			<!-- 表名 -->
				<th><spring:message code= "LB.Report_name" /></th>
				<!-- 外匯轉出紀錄 -->
				<th colspan="11"><spring:message code= "LB.W0351" /></th>
			</tr>
			<tr>
		<!-- 轉帳日期 -->
				<th ><spring:message code= "LB.Transfer_date" /></th>
<!-- 轉出帳號 -->
				<th ><spring:message code= "LB.Payers_account_no" /></th>
<!-- 轉出幣別 -->
				<th ><spring:message code= "LB.Currency_o" /></th>
<!-- 轉出金額 -->
				<th ><spring:message code= "LB.Deducted" /></th>
<!-- 銀行名稱 -->
<!-- 轉入帳號 -->
				<th ><spring:message code= "LB.Payees_account_no" />
				
		<!-- 		<br><spring:message code= "LB.Payees_account_no" /> -->
				</th>

<!-- 轉入幣別 -->
				<th ><spring:message code= "LB.Currency_i" /></th>
<!-- 轉入金額 -->
				<th ><spring:message code= "LB.Buy" /></th>
<!-- 匯率 -->
				<th ><spring:message code= "LB.Exchange_rate" /></th>
<!-- 手續費 -->
<!-- 郵電費 -->
				<th ><spring:message code= "LB.D0507" /><br><spring:message code= "LB.W0345" />
				</th>
<!-- 國外費用 -->
				<th ><spring:message code= "LB.W0346" /></th>

<!-- 交易類別 -->
				<th ><spring:message code= "LB.Transaction_type" /></th>
<!-- 備註 -->
				<th ><spring:message code= "LB.Note" /></th>
			</tr>
		</thead>
		<tbody>
		<c:if test="${not empty print_datalistmap_data}">
			<c:forEach var="dataList" items="${ print_datalistmap_data.REC }">
				<tr>
					<td style="text-align:center">${dataList.FXTXDATE}</td>
					<td style="text-align:center">${dataList.FXWDAC}</td>
					<td style="text-align:center">${dataList.FXWDCURR}</td>
					<td style="text-align:right">${dataList.FXWDAMT}</td>
					<td style="text-align:center">${dataList.FXSVAC}</td>
					<!--<td>${dataList.FXTITAINFO}</td>-->
					<td style="text-align:center">${dataList.FXSVCURR}</td>
					<td style="text-align:right">${dataList.FXSVAMT}</td>
					<td style="text-align:right">${dataList.FXEXRATE}</td>
					<td style="text-align:center">${dataList.FXEFEECCY}<br>${dataList.FXEFEE}<br>${dataList.FXTELFEE}</td>				
					<td style="text-align:right">${dataList.FXOURCHG}</td>
					<td style="text-align:center">${dataList.FXTRANFUNC}</td>
					<td style="text-align:center">${dataList.FXTXMEMO}</td>
				</tr>
			</c:forEach>
		</c:if>
		</tbody>
	</table>
	<br />
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		：
		<ol class="list-decimal text-left">
			<li><spring:message code= "LB.F_Transfer_Record_Query_P2_D1" /></li>
			<li><spring:message code= "LB.F_Transfer_Record_Query_P2_D2" /></li>
			<li><spring:message code= "LB.F_Transfer_Record_Query_P2_D3" /></li>
			<li><spring:message code= "LB.F_Transfer_Record_Query_P2_D4" /></li>
			<li><spring:message code= "LB.F_Transfer_Record_Query_P2_D5" /></li>
			<li style="color: red;"><spring:message code= "LB.F_Transfer_Record_Query_P2_D6" /></li>
		</ol>
	</div>
	<br />
</body>
</html>