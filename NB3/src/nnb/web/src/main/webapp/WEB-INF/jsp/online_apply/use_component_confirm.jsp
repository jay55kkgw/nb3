<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"src="${__ctx}/js/jquery.datetimepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	function init() {
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		// 確認鍵 click
		$("#CMSUBMIT").click( function(e) {
			// 送出進表單驗證前將span顯示
			$("#hideblock").show();
			
			console.log("submit~~");

			// 表單驗證
			if ( !$('#formId').validationEngine('validate') ) {
				e.preventDefault();
			} else {
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				console.log("validatdone~~");
				// 通過表單驗證準備送出
				processQuery();
			}
		});
	}
	
	function mainQuery() {
		var main = document.getElementById("main");	
		if(main.chk.value == "Y"){
			main.setAttribute("action", 'simpleform?trancode=NA30_3');
			main.submit();
		} else {
			EnableButtons();
		}
		return false;
	}

	function processQuery(){
		var main = document.getElementById("formId");
		console.log("processQuery()");	

		var LOGINPIN = $('#LOGINPIN').val();
		var TRANSPIN = $('#TRANSPIN').val();
		main.HLOGINPIN.value = pin_encrypt(LOGINPIN);
	    main.HTRANSPIN.value = pin_encrypt(TRANSPIN);
		processQuery1();

	  	return false;
	}

	function processQuery1()
	{
		console.log("processQuery1()");
		var main = document.getElementById("formId");
		listReaders();
		return false;
	}
	//取得讀卡機
	function listReaders(){
		console.log("cardReader GO GO");
		var CardReInsert = true;
		var GoQueryPass = true;
		ConnectCard("listReadersFinish");
	}
	//取得讀卡機結束
	function listReadersFinish(result){
		//成功
		if(result != "false" && result != "E_Send_11_OnError_1006"){
			//找出有插卡的讀卡機
			findOKReader();
		}
	}
	//找出有插卡的讀卡機
	function findOKReader(){
		FindOKReader("findOKReaderFinish");
	}
	var OKReaderName = "";
	//找出有插卡的讀卡機結束(有找到才會到此FUNCTION)
	function findOKReaderFinish(okReaderName){
		//ASSIGN到全域變數
		OKReaderName = okReaderName;
		//取得卡片銀行代碼
		getCardBankID();
// 		//拔插卡
// 		removeThenInsertCard();
	}
	//取得卡片銀行代碼
	function getCardBankID(){
		GetUnitCode(OKReaderName,"getCardBankIDFinish");
	}
	//取得卡片銀行代碼結束
	function getCardBankIDFinish(result){
		//成功
		if(result != "false"){
			//還要另外判斷是否為本行卡
			//是
			if(result == "05000000"){
				var formId = document.getElementById("formId");
				formId.ISSUER.value = result;
				
				showDialog("verifyPin",OKReaderName,"verifyPinFinish");
			}
			//不是
			else{
				//alert(GetErrorMessage("E005"));
				errorBlock(
						null, 
						null,
						[GetErrorMessage("E005")], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
	}
	var pazzword = "";
	//驗證卡片密碼
	function verifyPin(readerName,password,outerCallBackFunction){
		//ASSIGN到全域變數
		pazzword = password;
		
		VerifyPin(readerName,password,outerCallBackFunction);
	}
	//驗證卡片密碼結束
	function verifyPinFinish(result){
		//成功
		if(result == "true"){
			//繼續做
			SubmitForm();
		}
	}
	function SubmitForm(){
		console.log(" === SubmitForm === ");
		FinalSendout("MaskArea",true);
		//取得卡片主帳號
		getMainAccount();
	}
	//取得卡片主帳號
	function getMainAccount(){
		
		GetMainAccount(OKReaderName,"getMainAccountFinish");
	}
	//取得卡片主帳號結束
	function getMainAccountFinish(result){
		//成功
		if(result != "false"){
			var formId = document.getElementById("formId");
			formId.ACNNO.value = result;
			
			//拔插卡
			removeThenInsertCard();
		}
		//失敗
		else{
			FinalSendout("MaskArea",false);
		}
	}
	//拔插卡
	function removeThenInsertCard(){
		RemoveThenInsertCard("MaskArea",60,OKReaderName,"removeThenInsertCardFinish");
	}
	//拔插卡結束(成功才會到此FUNCTION)
	function removeThenInsertCardFinish(){
		initBlockUI();
		//拔插後繼續
		generateTAC();
	}
	//卡片押碼
	function generateTAC(){
		var TRMID = MakeTRMID();
		
		var formId = document.getElementById("formId");
		formId.TRMID.value = TRMID;
		
		var transData = "2500" + TRMID + formId.ACNNO.value;
		
		GenerateTAC(OKReaderName,transData,pazzword,"generateTACFinish");
	}

	//卡片押碼結束
	function generateTACFinish(result){
		//成功
		if(result != "false"){
			var TACData = result.split(",");
			
			var formId = document.getElementById("formId");
			formId.iSeqNo.value = TACData[1];
			formId.ICSEQ.value = TACData[1];
			formId.TAC.value = TACData[2];
			
			var ACN_Str1 = formId.ACNNO.value;
			if(ACN_Str1.length > 11){
				ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);
			}
			formId.ACNNO.value = ACN_Str1;
			unBlockUI(initBlockId);
			formId.submit();
		}
		//失敗
		else{
			FinalSendout("MaskArea",false);
		}
	}

	   	function goback(){

			main.setAttribute("action", 'simpleform?trancode=NA30');

			main.submit();

		  	return false;

		}
</script>
</head>

<body>
<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 交易機制所需畫面 -->
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Register" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 晶片金融卡申請網路銀行 -->
			<h2><spring:message code= "LB.X1077" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/ONLINE/APPLY/use_component_result">
				<input type="hidden" id="isBack" name="isBack" value="">
				<!-- 交易機制所需欄位 -->
				<input type="hidden" id="jsondc" name="jsondc" value='${use_component_confirm.data.jsondc}'>
  				<input type="hidden" name="ADOPID" value="NA30">
  				<input type="hidden" name="USERIP" value="${use_component_confirm.data.USERIP}">  
  				<input type="hidden" name="readername" value="">
  				<input type="hidden" name="braCode" value="">
  				<input type="hidden" name="accNo" value="">
  				<input type="hidden" name="icSeq" value="${use_component_confirm.data.icSeq}">
  				<input type="hidden" name="chk" value="">
  				<input type="hidden" name="_braCode" value="${use_component_confirm.data._braCode}">
  				<input type="hidden" name="_accNo" value="${use_component_confirm.data._accNo}">

  				<input type="hidden" name="CARDNUM" value="${use_component_confirm.data.CARDNUM}">
  				<input type="hidden" name="_CUSIDN" value="${use_component_confirm.data._CUSIDN}">
  				<input type="hidden" name="CCBIRTHDATEYY" value="${use_component_confirm.data.CCBIRTHDATEYY}">
  				<input type="hidden" name="CCBIRTHDATEMM" value="${use_component_confirm.data.CCBIRTHDATEMM}">
  				<input type="hidden" name="CCBIRTHDATEDD" value="${use_component_confirm.data.CCBIRTHDATEDD}">
  				<input type="hidden" name="MOBILE" value="${use_component_confirm.data.MOBILE}">
  				<input type="hidden" name="MAIL" value="${use_component_confirm.data.MAIL}">
  				<input type="hidden" name="cflg" value="${use_component_confirm.data.cflg}">
  				<input type="hidden" name="NOTIFY_ACTIVE" value="${use_component_confirm.data.NOTIFY_ACTIVE}">
  				<input type="hidden" name="HLOGINPIN" value="">
  				<input type="hidden" name="HTRANSPIN" value="">
  				<input type="hidden" name="ATMTRAN" value="${use_component_confirm.data.ATMTRAN1}">
  				<input type="hidden" name="BRHCOD" value="${use_component_confirm.data.BRHCOD}">
  				<input type="hidden" name="BRHNAME" value="${use_component_confirm.data.BRHNAME}">
  				
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<!-- 下一步驟需要的 -->
				<input type="hidden" name="CUSIDN" value="${use_component_confirm.data.CUSIDN}">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                         
                          <!--存款帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code= "LB.D0988" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                      	<span>${use_component_confirm.data.CARDNUM}</span>
                                    </div>
								</span>
							</div>
							
							<!--設定使用者名稱-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1002" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                      	<input type="text" class="text-input validate[required,funcCall[validate_CheckLenEqual2['<spring:message code= "LB.Loginpage_User_name" />',USERNAME,false,6,16]],funcCall[validate_CheckUserName['<spring:message code= "LB.Loginpage_User_name" />',USERNAME]]]" name="USERNAME" id="USERNAME" value=""><br>
									<!-- 6-16位英數字，英文至少2位且區分大小寫 -->
										<span class="tbb-unit"><font color="#FF0000"><spring:message code= "LB.D1003" /></font></span>
                                    </div>
								</span>
							</div>
							
							<!--確認使用者名稱-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1004" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="text" class="text-input validate[required,funcCall[validate_DoubleCheck['<spring:message code= "LB.Loginpage_User_name" />',USERNAME,USERNAME2]]]" name="USERNAME2" id="USERNAME2" value=""><br>
										<span class="tbb-unit"> <spring:message code= "LB.D1005" /></span>
                                    </div>
                                </span>
                            </div>
							
								<!--設定簽入密碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1006" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="password" class="text-input validate[required,funcCall[validate_CheckNumWithDigit2['<spring:message code= "LB.D1442" />',LOGINPIN,false]],funcCall[validate_ChkSerialNum['<spring:message code= "LB.D1442" />',LOGINPIN]],funcCall[validate_ChkSameEngOrNum['<spring:message code= "LB.D1442" />',LOGINPIN]],funcCall[validate_CheckLenEqual2['<spring:message code= "LB.D1442" />',LOGINPIN,false,6,8]]]" name="LOGINPIN" id="LOGINPIN" value=""><br>
										<!-- 6-8位數字 -->
										<span class="tbb-unit"><font color="#FF0000"> <spring:message code= "LB.D1007" /></font></span>
                                    </div>
                                </span>
                            </div>
							
								<!--確認簽入密碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1008" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="password" class="text-input validate[required,funcCall[validate_CheckNumWithDigit2['<spring:message code= "LB.D1008" />',LOGINPIN2,false]],funcCall[validate_DoubleCheck['<spring:message code= "LB.D1442" />',LOGINPIN,LOGINPIN2]]]" name="LOGINPIN2" id="LOGINPIN2" value=""><br>
										<!-- 請再次輸入簽入密碼 -->
										<span class="tbb-unit"> <spring:message code= "LB.D1009" /></span>
                                    </div>
                                </span>
                            </div>
							
							
								<!--設定交易密碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1010" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="password" class="text-input validate[required,funcCall[validate_CheckNumWithDigit2['<spring:message code= "LB.SSL_password_1" />',TRANSPIN,false]],funcCall[validate_ChkSerialNum['<spring:message code= "LB.SSL_password_1" />',TRANSPIN]],funcCall[validate_ChkSameEngOrNum['<spring:message code= "LB.SSL_password_1" />',TRANSPIN]],funcCall[validate_CheckLenEqual2['<spring:message code= "LB.SSL_password_1" />',TRANSPIN,false,6,8]]]" name="TRANSPIN" id="TRANSPIN" value=""><br>
										<!-- 6-8位數字 -->
										<span class="tbb-unit"><font color="#FF0000"><spring:message code= "LB.D1007" /></font></span>
                                    </div>
                                </span>
                            </div>
							
							
								<!--確認交易密碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1011" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="password" class="text-input validate[required,funcCall[validate_CheckNumWithDigit2['<spring:message code= "LB.D1011" />',TRANSPIN2,false]],funcCall[validate_DoubleCheck['<spring:message code= "LB.SSL_password_1" />',TRANSPIN,TRANSPIN2]]]" name="TRANSPIN2" id="TRANSPIN2" value=""><br>
										<!-- 請再次輸入交易密碼 -->
										<span class="tbb-unit"><spring:message code= "LB.D1012" /></span>
                                    </div>
                                </span>
                            </div>
                                
                        </div>
  <!-- 重新輸入 -->
                        <input class="ttb-button btn-flat-gray" name="CMRESET" type="reset" value=" <spring:message code= "LB.Re_enter" />" />
                      <!-- 確定 -->
                        <input class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" type="button" value=" <spring:message code= "LB.Confirm" />" />
                    </div>
                </div>
                <!-- 說明 -->
				<ol class="description-list list-decimal">
				
					<p> <spring:message code= "LB.Description_of_page" /></p>
				<!-- 使用者名稱、密碼，請勿設定連號、重號，如：123456、111111、AAAAAA。 -->
					<li><spring:message code= "LB.Use_Component_P6_D1" /></li>
					<!-- 使用者名稱、密碼，請勿設定與身分證字號/統一編號、生日、電話號碼、帳號等相同。 -->
					<li><spring:message code= "LB.Use_Component_P6_D2" /></li>
					<!-- 使用者名稱不可與簽入密碼、交易密碼相同。 -->
					<li><spring:message code= "LB.Use_Component_P6_D3" /></li>
					<!-- 使用者名稱為6-16位英數字且區分大小寫；簽入、交易密碼為6-8位數字，每次輸入時請特別注意。 -->
		          	<li><spring:message code= "LB.Use_Component_P6_D4" /></li>
                </ol>
				
			</form>
		</section>
		</main>
	<!-- 		main-content END -->
	<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>