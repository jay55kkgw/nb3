<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- Email設定     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Email_Setting" /></li>
    <!-- 我的Email設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.My_Email_setting" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	
	<main class="col-12"> 
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.My_Email_setting" /><!-- 我的 Email 設定 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
						<!-- 主頁內容  -->
						<div class="ttb-input-block">
							<!-- 系統時間 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.System_time" /></h4></label></span> <!-- 系統時間 -->
								<span class="input-block">
									<div class="ttb-input">
										<p>${chg_result.data.CMQTIME}</p>
									</div>
								</span>
							</div>					
						</div>
						<!-- 設定結果 -->
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.Setting_result" /></h4></label></span> <!-- 設定結果 -->
								<span class="input-block">
									<div class="ttb-input">
										<font color=red>
											<c:if test="${chg_result.data.TOPMSG eq '0' && DOUBLEMAIL != 'Y'}">
											<spring:message code="LB.Change_successful" />
											</c:if>
											<c:if test="${chg_result.data.TOPMSG eq '0' && DOUBLEMAIL == 'Y'}">
												<spring:message code="LB.X2620" />
											</c:if>
										</font>
									</div>
								</span>
							</div>					
						</div>
						<!-- 新的電子郵件 -->
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4>
								<c:if test="${chg_result.data.TOPMSG eq '0' && DOUBLEMAIL != 'Y'}">
									<spring:message code="LB.New_Email" />
								</c:if>
								<c:if test="${chg_result.data.TOPMSG eq '0' && DOUBLEMAIL == 'Y'}">
									<spring:message code="LB.X2621" />
								</c:if>
								</h4>
								</label></span> <!-- 新的電子郵件 -->
								<span class="input-block">
									<div class="ttb-input">
										<p>${ chg_result.data.DPMYEMAIL}</p>
									</div>
								</span>
							</div>					
						</div>
					</div>
				</div>
				<!-- 說明 -->
						<ol class="list-decimal description-list">
							<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
								<li>
								<spring:message code="LB.Mail_set_P2_D4" />
									<!-- 若您變更『電子郵件信箱』，將同時異動留存於本行之其他電子郵件信箱(但企業網路銀行、證券業務及電話方式逕向信用卡部客服通知變更e-mail信箱除外)。 -->
								</li>
							</ol>
						</div>
		</section>
	</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
</body>
</html>