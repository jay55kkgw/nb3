<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
   	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
	<script type="text/JavaScript">
	
		var wait = false;
	
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			initBlockUI();
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });

			// 確認鍵 click
			goOn();
			// 判斷顯不顯示驗證碼
			chaBlock();
			// 交易機制 click
			fgtxwayClick();
			// CLICK ENTER KEY
			enterClick();
			
			$("#capCode").focus();
		}
		
		// CLICK ENTER KEY
		function enterClick() {
			//避免頁面只有單一輸入框時，按enter會觸發submit兩次
			$(document).keydown(function(event){
	 	 	    if(event.keyCode == 13) {
	 	 	       	event.preventDefault();
	 	 	       	return false;
	 	 	    }
	 	 	});
			
			//輸入框內按enter可觸發click事件
			$("form input[type=text]").keydown(function(event){
		    	if(event.keyCode == 13 && $('#error-block').is(":hidden") && !wait) {
		    		wait = true;
		    		$("#CMSUBMIT").click();
		    		setTimeout("wait=false;", 500);
		       	}
			});
		}
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				// 送出進表單驗證前將span顯示
				$("#hideblock").show();
				
				console.log("submit~~");
	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					
					// 通過表單驗證
					processQuery();
				}
			});
		}
		
		// 通過表單驗證準備送出
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			switch(fgtxway) {
				case '1':
					// IKEY
					useIKey();
					
					break;
				case '2':
					// 晶片金融卡
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
					
			    	break;
				default:
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert001' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
		
		
		// 使用者選擇晶片金融卡要顯示驗證碼區塊
	 	function fgtxwayClick() {
	 		$('input[name="FGTXWAY"]').change(function(event) {
	 			// 判斷交易機制決定顯不顯示驗證碼區塊
	 			chaBlock();
			});
		}
	 	// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
 			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			if(fgtxway == '2'){
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").show();
			}else{
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").hide();
			}
	 	}
	 	
		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}
		
		// 卡片押碼結束
		function generateTACFinish(result){
			if(window.console){console.log("generateTACFinish...");}
			//成功
			if(result != "false"){
				// e.x. E000,00000551,6BF84A4B9319B145A64F3866506D3313594B10D08CDEA863BFA8F9D6
				var TACData = result.split(",");
				
				var formId = document.getElementById("formId");
				formId.iSeqNo.value = TACData[1];
				formId.ICSEQ.value = TACData[1];
				formId.TAC.value = TACData[2];
				
				var ACN_Str1 = formId.ACNNO.value;
				if(ACN_Str1.length > 11){
					ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);
				}
			
				formId.CHIP_ACN.value = ACN_Str1;
				formId.OUTACN.value = ACN_Str1;
				
				// 遮罩
				initBlockUI();
				
				formId.submit();
			}
			//失敗
			else{
				unBlockUI(initBlockId);
				FinalSendout("MaskArea",false);
			}
		}
	</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	
	<div class="content row">
		<main class="col-12"> 
			<section id="main-content" class="container">
				<h2>
					<spring:message code='LB.X2242' />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
									
				<form method="post" id="formId" action="${__ctx}/lock_reset_result">
					<!-- 交易機制所需欄位 -->
					<input type="hidden" id="jsondc" name="jsondc" value='${jsondc}'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					<input type="hidden" id="CHIP_ACN" name="CHIP_ACN" value="">
					<input type="hidden" id="OUTACN" name="OUTACN" value="">
					
					<!-- 交易流程階段 -->
					<div id="step-bar">
						<ul>
							<li class="finished"><spring:message code="LB.Enter_data" /></li>
							<li class="active"><spring:message code="LB.Confirm_data" /></li>
							<li class=""><spring:message code="LB.Transaction_complete" /></li>
						</ul>
					</div>
					
					<!-- 功能內容 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								
								<!-- 身分證字號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code='LB.D0581' /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="CUSIDN" name="CUSIDN" value="${cusidn}">
											<p>${cusidn}</p>
										</div>
									</span>
								</div>
								
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
										</label>
									</span>
									<!-- 交易機制選項 -->
									<span class="input-block">

										<!-- IKEY -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
										
										<!-- 晶片金融卡 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Financial_debit_card" />
											 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</span>
								</div>
								
								<!-- 使用者選擇晶片金融卡後才顯示驗證碼-->
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
									<!-- 驗證碼 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Captcha" /></h4>
										</label>
									</span>
									<span class="input-block">
	                                    <div class="ttb-input">
	                                        <img name="kaptchaImage" class="verification-img" src="" align="top" id="random" />
	                                        <button type="button" class="btn-flat-gray" name="reshow" onclick="changeCode()">
												<spring:message code="LB.Regeneration" /></button>
	                                    </div>
	                                    <div class="ttb-input">
	                                        <input id="capCode" name="capCode" type="text"
												class="text-input" maxlength="6" autocomplete="off">
	                                        <br>
	                                        <span class="input-unit"><spring:message code="LB.Captcha_refence" /></span>
	                                    </div>
	                                </span>
								</div>
								
							</div>
							
							<!-- 確定 -->
							<input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />						
						
						</div>
					</div>
				</form>
				
			</section>
		</main>
	</div>
</body>
</html>
 