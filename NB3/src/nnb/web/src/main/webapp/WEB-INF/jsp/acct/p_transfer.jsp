<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
  <script type="text/javascript">
    var isikeyuser = '${sessionScope.isikeyuser}';
	var banklist = [];
    console.log("isikeyuser>>"+isikeyuser);
	    $(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);

		});
	    
	    
	    function init(){
			console.log("init>>");
			
			// 初始化時隱藏span
			$("#hideblock").hide();
			
			var rdata = null;
			var val = null;
			getSelectData();
			creatCMDD_Select();
			fgtxdateEvent();
			tabEvent();
			selectChangeEvent();
			
			datetimepickerEvent();
			clickEvent();
			keyUpEvent();
// 			refillData();
			getTmr();
			searchDagacno();
			$("#CMRESET").click(function (e) {
				$("#acnoIsShow").hide();
				$(".formError").remove();
				$('form').find('*').filter(':input:visible:first').focus();
			})
			//銀行代碼變更，寫入text
			$("#DPBHNO").change(function() {
				$("#DPBHNO_TEXT").val($("#DPBHNO").val());
			})
			$("#DPAGACNO").change(function() {
				changeDisplay();
			})
			$("input[name='TransferType']:first").prop("checked", true).trigger("click");
			$("input[name='FGSVACNO']:first").prop("checked", true).trigger("change");
			
			refillData();
			
			//若前頁有帶acn，轉出預設為此acn
			var getacn = '${requestScope.Acn}';
			if(getacn != null && getacn != ''){
				$("#ACNO option[value= '"+ getacn +"' ]").prop("selected", true);
				$("#ACNO").change();
			}
			
// 			$("input[name='FGTXDATE']:second").prop("checked", true).trigger("change");
		  	//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// form Submit 
			$("#CMSUBMIT").click(function (e) {
				$(".formError").remove();
				chekckAmount_card();
				//打開驗證隱藏欄位
				$("input[name='hideblock']").show();
				console.log("submit~~");
// 				radioCheck();
				//塞值進span內的input
				$("#H_AMOUNT").val($("#AMOUNT").val());
				$("#H_CMTRMAIL").val($("#CMTRMAIL").val());
				$("#H_DPAGACNO").val($("#DPAGACNO").val());
				$("#H_DPACNO").val($("#DPACNO").val());
				$("#H_CMDD").val($("#CMDD").val());
				$("#H_CMDATE").val($("#CMDATE").val());
				$("#H_CMSDATE").val($("#CMSDATE").val());
				$("#H_CMEDATE").val($("#CMEDATE").val());

				if($("#FGTXDATE3").prop('checked')){
					$("#validate_CMEDATE2").show();
				}else{
					$("#validate_CMEDATE2").hide();
				}
				
				e = e || window.event;
				
				
				
				transferConfirm();
// 				if(!transferConfirm()){
// 					e.preventDefault();
// 					return false;
// 				}
				
			});
			
			$('select').on('change', function() {
				console.log("change");
				$("#"+this.id).attr("onmousewheel", 'return false');
			});
			$('select').on('mousedown', function() {
				console.log("click");
				$("#"+this.id).removeAttr("onmousewheel", 'return false');
			});
			
		} // init END
		var isconfirm_a = false;
		$("#errorBtn1").click(function(){
			if(!isconfirm_a)
				return false;
			isconfirm_a = false;
			
			if(!checkINPCST('INPCST', 10)){
				
				e.preventDefault();
				return false;
			}
			if (!$('#formId').validationEngine('validate')) {
				return false;
			} else {
				var retStr = "";
				retStr = validate_amount();
				if( fstop.isNotEmptyString(retStr)){
					//alert(retStr);
					errorBlock(
						null, 
						null,
						[retStr], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
					setTimeout("$('#error-block').show()", 200);
					return false ;
				}

				//取收款人戶名 
				if(getACCOUNTNAME()){
					e.preventDefault();
					return false;
				}

				
				//特殊標記帳號判別
				var RISKFLAG = "${RISKFLAG}";
				if(RISKFLAG=='N'){
					//false就誤會是bad guy
					if(riskAndA106Check()){
						return false;
					}
				}
				$("#formId").validationEngine('detach');
				initBlockUI();//遮罩
				$("#formId").submit();
			}
		});
		$("#errorBtn2").click(function(){
			isconfirm_a = false;
			$('#error-block').hide();
		});
// 		轉帳訊息確認
		function transferConfirm(){
			var fgtxdate = $("input[name='FGTXDATE']:checked").val();
			console.log("fgtxdate>>"+fgtxdate);
			isconfirm_a = true;
			if(fgtxdate == '1'){
				errorBlock(
						null, 
						null,
						['<spring:message code='LB.Confirm003' />'], 
						'<spring:message code= "LB.Confirm" />', 
						'<spring:message code= "LB.Cancel" />'
					);
//  				if(!confirm("<spring:message code='LB.Confirm003' />"))
// 		    		return false;    	
		   	}
			if(fgtxdate == '2'){
				errorBlock(
						null, 
						null,
						["<spring:message code='LB.Confirm004' />"+$('#CMDATE').val(),"<spring:message code='LB.Confirm005' />"], 
						'<spring:message code= "LB.Confirm" />', 
						'<spring:message code= "LB.Cancel" />'
					);
// 				if(!confirm("<spring:message code='LB.Confirm004' />"+$('#CMDATE').val()+"\n<spring:message code='LB.Confirm005' />"))
// 		    		return false;
			}
			if(fgtxdate == '3'){
				errorBlock(
						null, 
						null,
						["<spring:message code='LB.Confirm006' />",$('#CMSDATE').val()+"~"+$('#CMEDATE').val(),"<spring:message code='LB.Confirm007' />"+$('#CMDD').val()+"<spring:message code= "LB.Day" />","<spring:message code= "LB.X1446" />"+"!"], 
						'<spring:message code= "LB.Confirm" />', 
						'<spring:message code= "LB.Cancel" />'
					);
// 				if(!confirm("<spring:message code='LB.Confirm006' />"+$('#CMSDATE').val()+"~"+$('#CMEDATE').val()+"\n<spring:message code='LB.Confirm007' />"+$('#CMDD').val()+"<spring:message code= "LB.Day" />\n<spring:message code= "LB.X1446" />!"))
// 		    		return false;
			}
			
			return true;
		}
		
		
		
// 		約定或非約定 有不同的金額檢查邏輯\
		function validate_amount()
		{
			console.log("validate_amount>>");
// 			先判斷目前選的是哪種轉出 轉入帳號
			var amount ="";
			var retStr = "";
			var transferType_val =$("input[name='TransferType']:checked").val();
			var fgsvacno_val =$("input[name='FGSVACNO']:checked").val();
			
				amount  = $("#AMOUNT").val();
// 			轉出 約定 轉入非約定
			if('PD'==transferType_val && '2' ==fgsvacno_val  ){
				if(amount > 3000000){
					retStr = "<spring:message code= "LB.X1447" /> NTD300 <spring:message code= "LB.D0088_2" />";
					return retStr;
				}
			}
// 			轉出 非約定  檢查晶片卡主帳是否為臨櫃辦理帳號 (就是有出現在約定轉出帳號清單內)
			if('NPD'==transferType_val   ){
				console.log("NPD...")
// 				取得約定轉出帳號清單
				var isAgree = false;
				var outacn = $("#OUTACN_NPD").val();
				console.log("outacn>>"+outacn)
				var cnt = 0;
				$("#ACNO option").each(function() {
					console.log("a>>"+this.value)
					if( this.value === outacn ){
// 						表示此卡號 有在約定帳號清單內
						isAgree = true;
// 						break;
					}
// 				    alert(this.text);    //  文字
// 				    alert(this.value);   //  值
				});
				
				console.log("isAgree>>"+isAgree)
				if(!isAgree && amount > 50000){
					retStr = "<spring:message code= "LB.X1447" /> NTD5 <spring:message code= "LB.D0088_2" />";
					return retStr ;
				}
			}
			//如果是非IKEY使用者轉入帳號 選到約定帳號選單中的常用非約定帳號 及非約定帳號 限額5萬
			if("true" != isikeyuser){
				
				if(('2' ==fgsvacno_val || 'NPD'==transferType_val) && amount > 50000){
					retStr = "<spring:message code= "LB.X1448" />";
					return retStr ;
				}
				
				if('1' ==fgsvacno_val){
					var inAcno = $.parseJSON( $("#DPAGACNO").val() );
					var agree = inAcno.AGREE;
					if('0' == agree && amount > 50000 ){
						retStr = "<spring:message code= "LB.X1447" />";
						return retStr ;
					}
				}
				
			}
			
			
			
// 			預設1500萬，因為第2關卡 最高轉帳金額為1500萬
			if(amount > 15000000){
				retStr = "<spring:message code= "LB.Alert171" />";
				return retStr ;
			}
			
			return retStr;
		}
		
// 		上一頁回填區
		function refillData(){
			if('${bk_key}'=='Y'){
				
				//預約頁籤判斷切換 預設即時 
				if('${requestScope.back_data.FGTXDATE}'!='1'){
					$("#nav-trans-future").click();
					//預約時間回填
					
					if('${requestScope.back_data.FGTXDATE}'=='2'){
					//預約單日
					fstop.setRadioChecked("FGTXDATE" , "2" ,true);
					$('#CMDATE').val('${requestScope.back_data.transfer_date}');
					}else if('${requestScope.back_data.FGTXDATE}'=='3'){
					//預約定期每月的第幾日
						$("input:radio[name='FGTXDATE'][value='3']").trigger('click')
						fstop.setRadioChecked("FGTXDATE" , "3" ,true);
						$('#CMDD').val('${requestScope.back_data.CMDD}')
						$('#CMSDATE').val('${requestScope.back_data.CMSDATE}')
						$('#CMEDATE').val('${requestScope.back_data.CMEDATE}') 
					}
					
				}
				
				//轉出帳號回填	
				$('#ACNO').val('${requestScope.back_data.ACNO}')
				getACNO_Data('${requestScope.back_data.ACNO}');
				console.log("FGSVACNO>>"+'${requestScope.back_data.FGSVACNO}')
				console.log("DPBHNO>>"+'${requestScope.back_data.DPBHNO}')
				console.log("DPACNO>>"+'${requestScope.back_data.DPACNO}')
				//轉入帳號回填
				if('${requestScope.back_data.FGSVACNO}' =='2'){
					var dpbhno = '${requestScope.back_data.DPBHNO}';
					var tmp_val ="";
// 					$("#FGSVACNO_02").click();
					$("#DPBHNO").val('${requestScope.back_data.DPBHNO}')
					$("#DPBHNO option").each(function() {
// 						console.log("text>>"+this.text)
// 						console.log("value>>"+this.value)
						if(this.value.indexOf(dpbhno) != -1){
							tmp_val = this.value;
						}
					});
					$("#DPBHNO").val(tmp_val);
					$("#DPBHNO").trigger('change');
					$("#DPACNO").val('${requestScope.back_data.DPACNO}')
				}
				
				$("#DPAGACNO").val('${requestScope.back_data.DPAGACNO}')
				//信用卡繳費radio回填
// 			    var radio1 = $('#radioAmount1').val();
// 				var radio2 =  $('#radioAmount2').val();
// 				if('${requestScope.back_data.AMOUNT}'== radio1){
// 					$('#radioAmount1').attr('checked','checked');
// 				}else if ('${requestScope.back_data.AMOUNT}'== radio2){
// 					$('#radioAmount2').attr('checked','checked');
// 				}
				//交易金額回填 交易備註回填 通知回填 摘要內容回填 
				var tmpAmount = '${requestScope.back_data.AMOUNT}';
				tmpAmount =  fstop.unFormatAmtToInt(tmpAmount);
				$('#AMOUNT').val(tmpAmount)
// 				$('#AMOUNT').val('${requestScope.back_data.AMOUNT}')
				$('#INPCST').val('${requestScope.back_data.INPCST}')
				$('#CMTRMEMO').val('${requestScope.back_data.CMTRMEMO}')
			 	$('#CMTRMAIL').val('${requestScope.back_data.CMTRMAIL}');
			 	$('#CMMAILMEMO').val('${requestScope.back_data.CMMAILMEMO}');
			}
		}
		
		
// 		日曆欄位參數設定
		function datetimepickerEvent(){

		    $(".CMDATE").click(function(event) {
				$('#CMDATE').datetimepicker('show');
			});
		    $(".CMSDATE").click(function(event) {
				$('#CMSDATE').datetimepicker('show');
			});
		    $(".CMEDATE").click(function(event) {
				$('#CMEDATE').datetimepicker('show');
			});
			
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			});
		}
		
		
		
		
// 		預約日期change事件 ，檢核日期邏輯
		function fgtxdateEvent(){
			var sMinDate = '${requestScope.sMinDate}'
			var sMaxDate = '${requestScope.sMaxDate}'
				$('input[type=radio][name=FGTXDATE]').change(function() {
			        if (this.value == '2') {
			            console.log("tomorrow");
// 			            $("#H_CMDATE").val($("#CMDATE").val())
// 			            id="CMDATE" type="text" name="CMDATE" class="text-input validate[required]"
// 			            $("#CMDATE").addClass("validate[required ,funcCall[validate_CheckDate[預約日,CMDATE,"+sMinDate+","+sMaxDate+"]]]")
// 			            $("#CMDD").removeClass("validate[required]");
			            $("#H_CMDATE").addClass("validate[required,funcCall[validate_CheckDate[<spring:message code= "LB.Period_end_date" />,H_CMDATE,"+sMinDate+","+sMaxDate+"]]]")
			            $("#H_CMDD").removeClass("validate[required]");
			            $("#H_CMSDATE").removeClass("validate[required, past[#H_CMEDATE] ,funcCall[validate_CheckDate[<spring:message code= "LB.Period_end_date" />,H_CMSDATE,"+sMinDate+","+sMaxDate+"]]]");
			            $("#H_CMEDATE").removeClass("validate[required, future[#H_CMSDATE],funcCall[validate_CheckDate[<spring:message code= "LB.Period_end_date" />,H_CMEDATE,"+sMinDate+","+sMaxDate+"]]]");
			        }
			        else if (this.value == '3') {
// 			        	$("#H_CMDD").val($("#CMDD").val())
// 			        	$("#H_CMSDATE").val($("#CMSDATE").val())
// 			        	$("#H_CMEDATE").val($("#CMEDATE").val())
			            console.log("Transfer Thai Gayo");
// 			            $("#CMDATE").removeClass("validate[required]");
// 			            $("#CMDD").addClass("validate[required]")
// 			            $("#CMSDATE").addClass("validate[required , past[#CMEDATE] ,future[now]]")
// 			            $("#CMEDATE").addClass("validate[required , future[#CMSDATE]]")
			            $("#H_CMDATE").removeClass("validate[required,funcCall[validate_CheckDate[<spring:message code= "LB.Period_end_date" />,H_CMDATE,"+sMinDate+","+sMaxDate+"]]]");
			            $("#H_CMDD").addClass("validate[required]")
			            $("#H_CMSDATE").addClass("validate[required, past[#H_CMEDATE] ,funcCall[validate_CheckDate[<spring:message code= "LB.Period_end_date" />,H_CMSDATE,"+sMinDate+","+sMaxDate+"]]]")
			            $("#H_CMEDATE").addClass("validate[required, future[#H_CMSDATE],funcCall[validate_CheckDate[<spring:message code= "LB.Period_end_date" />,H_CMEDATE,"+sMinDate+","+sMaxDate+"]]]")
			            
			        }
			    });
			 $('input[type=radio][name=FGTXDATE]').trigger('change');
		}
		
		

		
//			取得轉出帳號餘額資料
		function getACNO_Data(acno){
			var options = { keyisval:true ,selectID:'#ACNO'};
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getACNO_Data_aj"
			console.log("getACNO_Data>>" + uri);
			rdata = {acno: acno };
			console.log("rdata>>" + rdata);
			fstop.getServerDataEx(uri,rdata,false,isShowACNO_Data);
		}
		
//			顯示轉出帳號餘額
		function isShowACNO_Data(data){
			
// 			可用餘額
			var i18n= new Object();
			i18n['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
			console.log("isShowACNO_Data.data"+data);
			if(data && data.result){
				$("#acnoIsShow").show();
				$("#showText").html("");
				console.log("data.data.accno_data"+data.data.accno_data.ADPIBAL);
				// 格式化金額欄位
				i18n['available_balance'] += fstop.formatAmt(data.data.accno_data.ADPIBAL);
				$("#showText").html(i18n['available_balance']);
			}else{
				$("#acnoIsShow").hide();
				
			}
		}
		
		
		
//			標籤切換事件
		function tabEvent(){
			
			$("#nav-trans-now").click(function() {
				console.log("hi>>");
				$("#now-date").show();
				$("#transfer-date").hide();
// 				fstop.setRadioChecked("FGTXDATE" , "1" ,true);
				$("input[name='FGTXDATE']:eq(0)").prop("checked", true).trigger("change");
			})
			
			$("#nav-trans-future").click(function() {
				$("#now-date").hide();
				$("#transfer-date").show();
//					$("input [name='FGTXDATE']").val("2");
//					$("input [name='FGTXDATE']").trigger("change");
// 				fstop.setRadioChecked("FGTXDATE" , "2" ,true);
				$("input[name='FGTXDATE']:eq(1)").prop("checked", true).trigger("change");
			})
		}
		
		//預約自動輸入明天
		function getTmr() {
			// 預約日期欄位顯示明天
			$('#CMDATE').val("${tmr}");
			$('#CMSDATE').val("${tmr}");
			$('#CMEDATE').val("${tmr}");				
		}
		
// 		把下拉選單選中的文字塞到隱藏欄位
		function changeDisplay(){
			console.log("val"+$("#DPAGACNO").find(":selected").val())
			console.log("val1>>"+$("#DPAGACNO").val())
			$("#DPAGACNO_TEXT").val($("#DPAGACNO").find(":selected").text());
		}
		
						
		//取得下拉選單資料
		function getSelectData(){
// 			var options = {selected:'01001400319' , keyisval:true ,selectID:'#acno'}
			
			creatOutACNO();
			creatInACNO();
			creatDpBHNO();
			
//				等轉入帳號確定有要做非約定再打開
// 			createBankSelect();
		}
		
//			建立約定轉出帳號下拉選單
		function creatOutACNO(){
			var options = { keyisval:false ,selectID:'#ACNO'};
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getOutAcno_aj"
			console.log("getSelectData>>" + uri);
			rdata = {type: 'acno' };
			console.log("rdata>>" + rdata);
			data = fstop.getServerDataEx(uri,rdata,false);
			console.log("data>>", data);
			if(data !=null && data.result == true ){
				fstop.creatSelect(data.data,options);
			}else{
				errorBlock(
						null, 
						null,
						['<spring:message code= 'LB.Alert004' />'], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
//			建立約定及常用轉入帳號下拉選單
		function creatInACNO(){
			data = null;
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getInAcno_aj"
			rdata = {type: 'in_acno' };
			options = { keyisval:false ,selectID:'#DPAGACNO'}
			data = fstop.getServerDataEx(uri,rdata,false);
			
			console.log("creatInACNO.data: " + JSON.stringify(data));
			if(data !=null && data.data !=null && data.result == true && data.data.REC !=null){
				fstop.creatSelect(data.data.REC,options);
				$("#DPAGACNO").children().each(function(){
					if($(this).val()=="#1" || $(this).val()=="#2"){
						$(this).css("background-color","orange");
						$(this).css("font-weight","bold");
					}
				})
			}else{
				//alert('<spring:message code= "LB.Alert005" />');
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert005' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
		
//		建立非約定銀行代號下拉選單
		function creatDpBHNO(){
			var tmp_val = "";
			var dpbhno = "-1";//預設值
			data = null;
			
			uri = '${__ctx}'+"/PNONE/CONFIG/getBankList"
			rdata = {};
			options = { keyisval:true ,selectID:'#DPBHNO'}
			data = fstop.getServerDataEx(uri,rdata,false);
			if(data !=null && data.result == true ){
				fstop.creatSelect(data.data,options);
				
				$("#DPBHNO option").each(function() {
					if(this.value.indexOf(dpbhno) != -1){
						tmp_val = this.value;
					}else{
						banklist.push(this.value);
					}
					
				});
				$("#DPBHNO").val(tmp_val);
			}
		}
		
// 		建立月天數的下拉選單
		function creatCMDD_Select(){
			var day = new Object();
			var options = { keyisval:false ,selectID:'#CMDD'}
			for(var i = 1 ;i<=31 ; i++){
				day[i] = i;
			}
// 			console.log("day>>"+day);
			fstop.creatSelect(day,options);
		}
		
		
// 		驗證轉出帳號下拉選單
	    function validate_DPAGACNO(field, rules, i, options){
			 var inputAttr=rules[i+2];
			 console.log("inputAttr>>"+inputAttr );
			 var dpagacno_val = $("#"+inputAttr).find(":selected").val()
			 
			 console.log("funccall test" );
			 console.log("funccall test>>"+options.allrules.required.alertText );
			 if(fstop.isEmptyString(dpagacno_val) || dpagacno_val.indexOf('#') > -1){
				 return options.allrules.required.alertText
			 }
// 			 var inAcno =  $.parseJSON( dpagacno_val );
// 			 branch = inAcno.BNKCOD;
// 			 inAcno = inAcno.ACN;
// 			 inAcno = inAcno.substr(0,2);
// 			 console.log("validate_DPAGACNO.inAcno>>"+inAcno );
// 			 if("98" == inAcno || "99" == inAcno){
// 				 return "* 帳號為期貨帳號，請使用繳納期貨保證金功能";
// 			 }
		}
		
// 		開啟通訊錄email選單
		function openAddressbook()
		{
			window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
		}
	    
// 	    驗證非約定轉入帳號是否為期貨帳號
		function validate_DPACNO(field, rules, i, options){
			var dpbhno = $("#DPBHNO").val();
			var dpacno = $("#DPACNO").val();
			if(dpbhno != "#"){
				dpbhno=dpbhno.split("-");
				dpbhno=dpbhno[0]
			}
			
			console.log("dpbhno>>" + dpbhno );
			dpacno = dpacno.substr(0, 2);
			console.log("dpacno>>" + dpacno );
			
			if(dpbhno == "050" && (dpacno == "98" || dpacno == "99")){
				return "* <spring:message code= "LB.X1450" />";
			}
			
		}
		
		
		
		function keyUpEvent()
		{
			$('#INPCST').focusout(function() {
// 			$('#INPCST').keyup(function() {
				checkINPCST('INPCST', 10);
			});
			
// 			$('#INPCST').keyup(function(e) {
// 				code = (e.keyCode ? e.keyCode : e.which);
// 				console.log("keyup.code>>" + code );
// 				console.log("+this.value>>" +this.value );
// 				console.log("+this.length>>" +this.value.length );
// // 				表示中文字
// 				if(code == 229 && this.value.length >7){
// 					checkINPCST('INPCST', 10);
// 				}
// 			});
			
// 			$('#INPCST').change(function(e) {
// 				console.log("change>>");
// 					checkINPCST('INPCST', 10);
// 			});
			$("#INPCST").bind("input", function(e) {
				code = (e.keyCode ? e.keyCode : e.which);
				console.log("code2>>" + code );
				console.log("value>>"+this.value);
			    this.value; //可取得目前的文字內容
			});
		}
		
		
// 		限制欄位長度不可超過指定byte
		function checkINPCST(id, byteLength)
		{
			var oINPCST = document.getElementById(id);
			var sValue = oINPCST.value;
			var rePattern = /[^\x00-\xff]/ig;
			var oArr = sValue.match(rePattern); 
			var valueLength = oArr == null ? sValue.length : sValue.length + oArr.length; 

			while (valueLength > byteLength)
			{
			    sValue = sValue.substring(0, sValue.length - 1);
			    oArr = sValue.match(rePattern); 
			    valueLength = oArr == null ? sValue.length : sValue.length + oArr.length;
			}
			
			if (sValue != oINPCST.value)
		    {
// 				oINPCST.value = sValue;
				errorBlock(null,null,["收款人附言，不可超過5個中文字"],"確定",null);
				$("#INPCST").focus();
				return false;
		    }
			return true;
		}
		
		

		function clickEvent()
		{
			// 通訊錄btn
			$('#getAddressbook').click(function() {
				openAddressbook();
			});
			// 非約定轉入帳號
			$('#DPACNO').click(function() {
				$("input[name='FGSVACNO']:eq(1)").prop("checked", true).trigger("change");
			});
			
			$('#DPACNO').keyup(function(e) {
			    var code = e.keyCode || e.which;
			    console.log("code>>"+code);
			    if (code === 9) {  
			    $("input[name='FGSVACNO']:eq(1)").prop("checked", true).trigger("change");
// 			        e.preventDefault();
// 			        myFunction();
// 			        alert('it works!');
			    }
			});
			
			
			
			// 同交易備註btn
			$('#CMMAILMEMO_btn').click(function() {
				$('#CMMAILMEMO').val( $('#CMTRMEMO').val() );
			});
			//  轉出帳號 radio click事件 
			$("input[name='TransferType']").click(function(e) {
				var checkedval =$("input[name='TransferType']:checked").val();
				 console.log("TransferType checkedval>>"+checkedval);
				 
				 if(checkedval == 'PD'){
					 $("#ACNO").addClass("validate[required]");
					 $("#OUTACN_NPD").removeClass("validate[required]");
					 $("#OUTACN").val("");
					 $("#OUTACN_NPD").val("");
				 }
				 if(checkedval == 'NPD'){
					 $("#OUTACN").val("");
					 $("#OUTACN_NPD").val("");
					 $("#OUTACN_NPD").addClass("validate[required]");
					 $("#ACNO").removeClass("validate[required]");
// 					 $('#ACNO').prop('disabled', false);
// 					 $('#ACNO').prop('disabled', 'disabled');
					 $('#ACNO').val('').trigger('change')
					 $("#acnoIsShow").hide();
					 listReaders();
				 }
				 
// 				listReaders();
			});
			
		//  轉入帳號 radio click事件
			$("input[name='FGSVACNO'] ").change(function() {
				console.log("FGSVACNO>>");
		        val = $(this).val(); // retrieve the value
				
				console.log("val>>"+val);
// 		                      約定
		        if(val == '1'){
// 		        	隱藏收款人附言
// 		        	$("#optionChg").hide();
		        	$("#DPACNO").val("");
// 		        	$("#DPACNO").removeClass("validate[required ,custom[integer],funcCall[validate_DPACNO[DPACNO]]]");
// 		        	$("#DPAGACNO").addClass("validate[required,funcCall[validate_DPAGACNO[DPAGACNO]]]");
		        	$("#H_DPACNO").removeClass("validate[required ,custom[integer],funcCall[validate_DPACNO[DPACNO]]]");
		        	$("#H_DPAGACNO").addClass("validate[funcCallRequired[validate_DPAGACNO[DPAGACNO]]]");
		        	$("#optionChg").hide();
		        }
// 		                     非 約定
		        if(val == '2'){
// 		        	$("#optionChg").show();
		        	$("#DPAGACNO").val("").trigger('change');
// 		        	$("#DPAGACNO").removeClass("validate[required,funcCall[validate_DPAGACNO[DPAGACNO]]]");
// 		        	$("#DPACNO").addClass("validate[required ,custom[integer],funcCall[validate_DPACNO[DPACNO]]]");
		        	$("#H_DPAGACNO").removeClass("validate[funcCallRequired[validate_DPAGACNO[DPAGACNO]]]");
		        	$("#H_DPACNO").addClass("validate[required ,custom[integer],funcCall[validate_DPACNO[DPACNO]]]");
					var tmp=$("#DPBHNO").val();
		        	if(fstop.isNotEmptyString(tmp) && "undefined" != tmp){
						if(tmp.substr(0,3)=="050"){
							$("#optionChg").show();
						}else{
							$("#optionChg").hide();
						}
					}else{
						$("#optionChg").hide();
					}
		        	console.log("清除約定的下拉選單值");
		        }
			})
			
		}
		
		
		function radioCheck(){
// 			 var checkedval =$("input[name='TransferType']:checked").val();
// 			 console.log("TransferType checkedval>>"+checkedval);
// 			 if(checkedval == 'PD'){
// 				 $("#ACNO").addClass("validate[required]");
// 				 $("#OUTACN_NPD").removeClass("validate[required]");
// 			 }
// 			 if(checkedval == 'NPD'){
// 				 $("#OUTACN_NPD").addClass("validate[required]");
// 				 $("#ACNO").removeClass("validate[required]");
// 			 }
		}
		
		
		
// 		下拉選單異動事件
		function selectChangeEvent()
		{
// 			約定轉出帳號
			$( "#ACNO" ).change(function() {
				var acno = $('#ACNO :selected').val();
				console.log("acno>>"+acno);
// 				如果此下拉選單有異動 radio 就改成checked
				if(fstop.isNotEmptyString(acno) && 'undefined' != acno){
					$("#OUTACN").val("");
					$("#OUTACN_NPD").val("");
					fstop.setRadioChecked("TransferType" , "PD" ,true);
					getACNO_Data(acno);
				}
			});
			
// 			約定轉入帳號
			$( "#DPAGACNO" ).change(function() {
				var acno = $('#DPAGACNO :selected').val();
				console.log("acno>>"+acno);
				displayPostscript();
// 				如果此下拉選單有異動 radio 就改成checked
				if(fstop.isNotEmptyString(acno) && 'undefined' != acno){
					$("#FGSVACNO_01").trigger('click')
					fstop.setRadioChecked("FGSVACNO" , "1" ,true);
				}
			});
// 			非約定轉入帳號
			$( "#DPBHNO" ).change(function() {
				var acno = $('#DPBHNO :selected').val();
				console.log("DPBHNO.acno>>"+acno);
// 				如果此下拉選單有異動 radio 就改成checked
				if(fstop.isNotEmptyString(acno)){
					$("#FGSVACNO_02").trigger('click')
					fstop.setRadioChecked("FGSVACNO" , "2" ,true);
					acno = acno.split("-")[0];
				console.log("acno2>>"+acno);
					$('#optionChg').hide();
					if(acno === '050')
					{
						$('#optionChg').show();
					}else{
						$('#optionChg').hide();
					}
				}else{
					$('#optionChg').hide();
				}
				
			});
			
			
			
			
		}
		
		
		function changeDisplayEvent()
		{
			displayPostscript();
		}
		
		
// 		收款人附言顯示相關邏輯，預設關閉，轉入自行時顯示
		function displayPostscript()
		{
// 			$('#DPAGACNO').change(function() {
				$('#optionChg').hide();
				var DPAGACNO = $('#DPAGACNO').val();

				if( fstop.isNotEmptyString(DPAGACNO) && DPAGACNO.indexOf('BNKCOD') !== -1)
				{
					var jDPAGACNO = jQuery.parseJSON( DPAGACNO );
					if(jDPAGACNO.BNKCOD === '050')
					{
						$('#optionChg').show();
					}
				}
// 			});
		}
		
		//查詢約定轉入帳號
		function searchDagacno(){
			$("#SearchDagacno").click(function(){
				window.open('${__ctx}/NT/ACCT/TRANSFER/searchDagacno','<spring:message code="LB.Designated_account_Inquiry" />','height=400,width=700,scrollbars=yes,resizable=yes');				
			})
		}
		
		function chekckAmount_card(){
			var amount ="";
			var retStr = "";
			var transferType_val =$("input[name='TransferType']:checked").val();
			var fgsvacno_val =$("input[name='FGSVACNO']:checked").val();
			amount  = $("#AMOUNT").val();
			//檢核一般晶片金融卡非約定轉帳限額50000元
			if("true" != isikeyuser){
			if('2' ==fgsvacno_val || 'NPD'==transferType_val){
				if(amount > 50000 ){
					retStr = "<spring:message code= "LB.X1448" />";
				}
			}
			if( fstop.isNotEmptyString(retStr)){
				//alert(retStr);
				$("#AMOUNT").val("");				
				errorBlock(
					null, 
					null,
					[retStr], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
				setTimeout("$('#error-block').show()", 200);
				return false ;
			}
			}
		}
		
		function getACCOUNTNAME(){
				bankcode = $("#DPBHNO_TEXT").val();
				dpacno = $("#DPACNO").val();
				//Step1取得A106資料
				uri = '${__ctx}' + "/PNONE/CONFIG/getACCOUNTNAME";
				ACCOUNT_data = fstop.getServerDataEx(uri,{"BANKCODE":bankcode,"DPACNO":dpacno}, false);
				//Step2設值
				console.log("ACCOUNT_data = ");
				console.log(ACCOUNT_data);
				var AccountName = ACCOUNT_data.data.AccountName;
				var BankCode = ACCOUNT_data.data.BankCode;
				var msg = ACCOUNT_data.data.TOPMSG;
				if(AccountName == null || AccountName.length == 0 ){
					if(bankcode == null || bankcode.length == 0){
						errorBlock(null,null,["手機號碼，未註冊"],"確定",null);
					}else{
						errorBlock(null,null,["手機號碼，未註冊或收款銀行有誤"],"確定",null);
					}
					return true;
				}else{
					$("#ACCOUNTNAME").val(AccountName);
					console.log("banklist >>");
					
					banklist.forEach(
							element => {
								console.log(element); 
								console.log(element.indexOf(BankCode));
								if(element.indexOf(BankCode) != -1){
									$("#DPBHNO_TEXT").val(element);
								}
							});
					
				}
				return false;
		}


		function riskAndA106Check(){
			try{
				//Step1取得A106資料
				uri = '${__ctx}' + "/NT/ACCT/TRANSFER/getA106_Data_aj";
				A106data = fstop.getServerDataEx(uri,"{}", false);
				//Step2設值
				var auth= "${sessionScope.authority}";
				var career1 =A106data.data.CAREER1;
				var career2 =A106data.data.CAREER2;
				var conpnam =A106data.data.CONPNAM;
				var usid =A106data.data.CUSIDN;			
				if((career1 == "" ||career2 == "" ||conpnam == ""   ) &&
					(usid.length == 10) && (auth !="CRD")){
					//依交易機制作動
					var transferType_val =$("input[name='TransferType']:checked").val();//轉出
					var fgsvacno_val =$("input[name='FGSVACNO']:checked").val();//轉入
					//非約轉出
					if('NPD'==transferType_val ){
						console.log("NPD...")
						//取得約定轉出帳號清單
						var isAgree = false;
						var outacn = $("#OUTACN_NPD").val();
						console.log("outacn>>"+outacn)
						var cnt = 0;
						$("#ACNO option").each(function() {
							console.log("a>>"+this.value)
							if( this.value === outacn ){
								//表示此卡號 有在約定帳號清單內
								isAgree = true;
							}
						});
						console.log("isAgree>>"+isAgree);
						if(!isAgree && outacn.length!=0){						
							popA106form(A106data);
							return true;
						}else return false;
					}
					//非約轉入
					if('2' ==fgsvacno_val){
						if(usid.length == 10){
							popA106form(A106data);
							return true;
						}else return false;
					}
					//常用非約轉入
					if('1' ==fgsvacno_val){
						if($("#DPAGACNO").val()!="" && $("#DPAGACNO").val()!="#1" && $("#DPAGACNO").val()!="#2")//避免欄位煤田導致出錯
						{
							var inAcno = $.parseJSON( $("#DPAGACNO").val() );
							var agree = inAcno.AGREE;
							if('0' == agree){
								if(usid.length == 10){
									popA106form(A106data);
									return true;
								}else return false;
							}
						}else return false;
					}
				}else return false;
			}catch(e){
				return false;
			}
		}
		function popA106form(A106data){
			var career1 =A106data.data.CAREER1;
			var career2 =A106data.data.CAREER2;
			var conpnam =A106data.data.CONPNAM;
			if(career1!=""){
				$("#CAREER1_DIV").hide();
				$("#PARAM4").val(career1);
			}
			if(career2!=""){
				$("#CAREER2_DIV").hide();
				$("#PARAM5").val(career2);
			}
			if(conpnam!=""){
				$("#CONPNAM_DIV").hide();
				$("#PARAM3").val(conpnam);
			}			
			retStr='<spring:message code= "LB.X2404" />';
			errorBlock(
					null, 
					null,
					[retStr], 
					'<spring:message code= "LB.Confirm" />', 
					null
				);
			setTimeout("$('#error-block').show()", 200);
			$('#update-content').modal('show');
		}

		 /*
	     * 驗證手機是否綁定
	     */
	    function validate_accountPhone(field, rules, i, options) {
	    	//錯誤訊息(此收款行查無此門號綁定----有選擇銀行)
	    	var sFieldName_bk = rules[i + 2];
	    	//錯誤訊息(手機門號未註冊---無選擇銀行)
	    	var sFieldName_nobk = rules[i + 3];
	    	//手機號碼物件
	    	var mobilephoneField = rules[i + 4];
	    	//銀行代碼物件
	    	var bankCodeField = rules[i + 5];
	    	var pnumber = $("#"+mobilephoneField).val();
	    	var bkcode = $("#"+bankCodeField).val();
	    	uri = '${__ctx}'+"/PNONE/CONFIG/check_phone";
	    	console.log("pnumber = " + pnumber);
	    	console.log("bkcode = " + bkcode);
	    	rdata = {
	    				PNUMBER: pnumber,
	    				BKCODE: bkcode
	    			};
	    	data = fstop.getServerDataEx(uri,rdata,false);
	    	console.log("datadata = ");
	    	console.log(data);
	    	if(data != true){
	    		if(bkcode == null || bkcode.trim().length == 0 || bkcode == '-1')
	    			return sFieldName_nobk;
	    		else
	    			return sFieldName_bk;
	    	}
	    }

		
	</script>
</head>
<body>
	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>
	
    <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 	<%@ include file="../acct/transfer_A106.jsp"%>
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 轉帳交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Transfer" /></li>
    <!-- 手機號碼轉帳交易     -->
			<li class="ttb-breadcrumb-item active" aria-current="page">手機號碼轉帳交易</li>
		</ol>
	</nav>



	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			
		
		<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<form  method="post" id="formId" action="${__ctx}/NT/ACCT/PHONE_TRANSFER/p_transfer_confirm" autocomplete="off">
			<input type="hidden" id="DPAGACNO_TEXT" name = "DPAGACNO_TEXT" value="${requestScope.back_data.DPAGACNO_TEXT }">
			<input type="hidden" id="TOKEN" name = "TOKEN" value="${sessionScope.transfer_confirm_token}">
			<input type="hidden" name="transferdate" id="transferdate" value="${requestScope.today}">
			<input type="hidden" id="OUTACN" name = "OUTACN" value="">
			<input type="hidden"  id="FGSVACNO_02" name="FGSVACNO" value="2" >
			<input type="hidden"  id="ACCOUNTNAME" name="ACCOUNTNAME" value="" >
				<!-- I18N-->
				<h2>手機號碼轉帳交易</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

				<div id="step-bar">
					<ul>
						<li class="active"><spring:message code="LB.Enter_data" /></li>
						<li class=""><spring:message code="LB.Confirm_data" /></li>
						<li class=""><spring:message code="LB.Transaction_complete" /></li>
					</ul>
				</div>

				<div class="main-content-block row ">

					<div class="col-12">

						<div class="tab-pane fade " id="nav-trans-future" role="tabpanel"
							aria-labelledby="nav-profile-tab"></div>

						<div class="ttb-input-block tab-pane fade show active"
							id="nav-trans-now" role="tabpanel" aria-labelledby="nav-home-tab">

							<div class="ttb-message">
								<span></span>
							</div>
							<!-- 							這是即時的屬性  預設就是隱藏 -->
							<div class="ttb-input" style="display: none;">
								<label class="radio-block"> <spring:message code="LB.Immediately" />
								 <input type="radio" name="FGTXDATE" value="1" checked> 
								 <span class="ttb-radio"></span>
								</label>
							</div>
							
							<!-- 交易類型 -->
							<div id="now-date" class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											交易類型
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input ">
										<span>即時轉帳</span>
									</div>
								</span>
							</div>
							
							<!-- 轉帳日期 -->
							<div id="now-date" class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transfer_date" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input ">
										<span>${requestScope.today}</span>
									</div>
								</span>
							</div>
							<!-- 轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Payers_account_no" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block" for="TransferType_01"> 
										<input type="radio"  id="TransferType_01" name="TransferType" value="PD" checked>
											<spring:message code="LB.Designated_account" />
										<span class="ttb-radio"></span>
										</label>
									</div> 
									<!--  <span class="input-block"> -->
									<div class="ttb-input ">
										<select name="ACNO" id="ACNO" class="custom-select select-input half-input">
											<option value="">----<spring:message code="LB.Select_account" />-----</option>
										</select>
										<div id="acnoIsShow">
											<span id = "showText" class="input-unit "></span>
										</div>
									</div>
									
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Out_nondesignated_account" /> 
										<input type="radio" id = "TransferType_02" name="TransferType" value="NPD" >
										<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<input type="text" id=OUTACN_NPD name="OUTACN_NPD" class="text-input" value="" readonly="readonly" autocomplete="off" /> 
										<span class="input-remarks subtitle-color"><spring:message code="LB.X2361" /></span>
										</div>
										</span>
									</div>
							
							<!-- 收款行 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4>收款行</h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input"> 
										<select id="DPBHNO" name="DPBHNO" 
												class = "custom-select select-input half-input"
												placeholder="<spring:message code="LB.Not_required" />">
											<option value="-1" >請選擇收款銀行</option>
										</select>
											<input type="hidden" id="DPBHNO_TEXT" name = "DPBHNO_TEXT" value="">
									</div>
								</span>
							</div>
							
							<!-- 手機號碼 (轉出帳號)-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label><h4>手機號碼</h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" name="DPACNO" id="DPACNO" maxlength="10"  value="" autocomplete="off"  class="text-input validate[required,funcCallRequired[validate_accountPhone[* 此收款行查無此門號綁定,* 手機門號未註冊,DPACNO,DPBHNO]]] validate[funcCallRequired[validate_cellPhone[* 手機電話格式有誤,DPACNO]]]"/>
									</div>
								</span>
							</div>
							
							<!-- 轉帳金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.Amount" /></h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<!--  不可超過1500萬  -->
<%-- 										<span class="input-unit"><spring:message code="LB.NTD" /></span> --%>
<!-- 										<input type="text" id="AMOUNT" name="AMOUNT" class="text-input  validate[required ,custom[integer,min[1] ,max[15000000]]] " size="8" maxlength="8" value="">  -->
										<input type="text" id="AMOUNT" name="AMOUNT" class="text-input" size="8" maxlength="8" value="" placeholder="請輸入轉帳金額(新台幣)" onChange="chekckAmount_card()" autocomplete="off"> 
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
										<br>
										<span class="input-remarks" style="color:red"><spring:message code="LB.For_Debit_card_transfer_note" /></span>
										<!-- 不在畫面上顯示的span -->
										<span name="hideblock" >
											<!-- 驗證用的input -->
											<input id="H_AMOUNT" name="H_AMOUNT" type="text" class="text-input validate[required ,custom[integer,min[1] ,max[15000000]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" autocomplete="off"/>
										</span>
									</div>
								</span>
							</div>
							
							<!-- 收款人附言 -->
							<div id ="optionChg" class="ttb-input-item row" style="display: none;" >
								<span class="input-title"><label><h4><spring:message code="LB.Payees_postscript" /></h4></label></span> <span
									class="input-block">
									<div class="ttb-input">
										<input type="text" id="INPCST" name="INPCST" class="text-input" placeholder="<spring:message code="LB.Not_required" />" maxlength="10" autocomplete="off" > 
										<span class="input-unit"><img src="${__ctx}/img/icon-13.svg"></span>
										<br>
										<span class="input-remarks"><spring:message code="LB.Payees_postscript_note" /></span>
									</div>
								</span>
							</div>
							<!-- 交易備註 -->
							<div class="ttb-input-item row">
								<span class="input-title"><label><h4><spring:message code="LB.Transfer_note" /></h4></label></span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" size="56" value="" maxlength='20' autocomplete="off">
										<span class="input-unit"></span>
										<span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
									</div>
								</span>
							</div>
						<!-- 轉出成功 Email通知 -->
						<div class="ttb-input-item row">
							<span class="input-title"> 
							<label>
								<h4><spring:message code="LB.X0479" /></h4>
							</label>
							</span> <span class="input-block">
								<div class="ttb-input">
									<span class="input-subtitle subtitle-color"><spring:message code="LB.Notify_me" />：</span>
									<span class="input-subtitle subtitle-color">${sessionScope.dpmyemail}</span>
								</div>
								<div class="ttb-input">
									<span class="input-subtitle subtitle-color"><spring:message code="LB.Another_notice" />：</span>
								</div>
								<!-- 通訊錄 -->
								<div class="ttb-input">
									<input type="text" id="CMTRMAIL" name="CMTRMAIL" class="text-input" placeholder="<spring:message code="LB.Not_required" />" autocomplete="off"> 
									<span class="input-unit"></span>
<!-- 										window open 去查 TxnAddressBook  參數帶入身分證字號 -->
									<button type="button" id="getAddressbook" class="btn-flat-orange"><spring:message code="LB.Address_book" /></button>
									<!-- 不在畫面上顯示的span -->
									<span name="hideblock" >
										<!-- 驗證用的input -->
										<input id="H_CMTRMAIL" name="H_CMTRMAIL" type="text" class="text-input validate[funcCall[validate_EmailCheck[CMTRMAIL]]]" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" autocomplete="off"/>
									</span>
								</div>
								<div class="ttb-input">
									<input type="text" id="CMMAILMEMO" name="CMMAILMEMO"
										class="text-input" placeholder="<spring:message code="LB.Summary" />"  value="" maxlength='50' autocomplete="off">
									<span class="input-unit"></span>
									<button type="button" id="CMMAILMEMO_btn" class="btn-flat-orange"><spring:message code="LB.As_transfer_note"  /></button>
								</div>
							</span>
						</div>
						</div>
						<input id="CMRESET" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" /> 
						<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						
					
					</div>
				</div>
			</form>
		</section>
		<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->

	

	<%@ include file="../index/footer.jsp"%>
	  
</body>
</html>