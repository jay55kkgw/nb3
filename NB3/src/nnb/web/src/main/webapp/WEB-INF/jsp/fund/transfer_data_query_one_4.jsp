<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="RS" value="${transfer_data_query_one.data.RS}"></c:set>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript">
		$(document).ready(function () {
			//列印
			$("#printbtn").click(function () {
				var params = {
						jspTemplateName:		"transfer_data_query_one_4_print",
						jspTitle:				"<spring:message code= "LB.W0987" />",
						TRADEDATE_F: 			"${RS.TRADEDATE_F }",
						CDNO_F: 			"${RS.CDNO_F }",
						TRANSCODE_FUNDLNAME: 	"${RS.TRANSCODE_FUNDLNAME }",
						DATE_F:					"${RS.DATE_F }",
						CRY_CRYNAME: 			"${RS.CRY_CRYNAME }",
						EXAMT_F: 				"${RS.EXAMT_F }",
						UNIT_F: 				"${RS.UNIT_F }",
						TRANSCRY_CRYNAME: 		"${RS.TRANSCRY_CRYNAME }",
						AMT4_F: 				"${RS.AMT4_F }",
						EXRATE_F: 				"${RS.EXRATE_F }",
						AMT5_F: 				"${RS.AMT5_F }",
						NHITAX_F: 				"${RS.NHITAX_F }",
						AMT7_F: 				"${RS.AMT7_F }"
					};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
		});
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 基金交易資料查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0943" /></li>
    <!-- 基金除息分配交易資料查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0987" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- 快速選單及主頁內容 -->
	<main class="col-12">
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.W0987" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form method="post" action="" >
			
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<!-- 分配日期 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0988" />
									</label>
								</span>
								<span class="input-block">
									${RS.TRADEDATE_F }
								</span>
							</div>
							<!-- 信託編號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0904" />
									</label>
								</span>
								<span class="input-block">
									${RS.CDNO_F }
								</span>
							</div>
							<!-- 基金名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0025" />
									</label>
								</span>
								<span class="input-block">
									${RS.TRANSCODE_FUNDLNAME }
								</span>
							</div>
							<!-- 基準日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0028" />
									</label>
								</span>
								<span class="input-block">
									${RS.DATE_F }
								</span>
							</div>
							<!-- 除息金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0990" />
									</label>
								</span>
								<span class="input-block">
									${RS.CRY_CRYNAME } ${RS.EXAMT_F }
								</span>
							</div>
							<!-- 基準日信託單位數 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0991" />
									</label>
								</span>
								<span class="input-block">
									${RS.UNIT_F }
								</span>
							</div>
							<!-- 每單位分配值 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0992" />
									</label>
								</span>
								<span class="input-block">
									${RS.TRANSCRY_CRYNAME } ${RS.AMT4_F }
								</span>
							</div>
							<!-- 匯率 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.Exchange_rate" />
									</label>
								</span>
								<span class="input-block">
									${RS.EXRATE_F }
								</span>
							</div>
							<!-- 代扣稅款 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0993" />
									</label>
								</span>
								<span class="input-block">
									${RS.CRY_CRYNAME } ${RS.AMT5_F }
								</span>
							</div>
							<!-- 代扣健保費 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0994" />
									</label>
								</span>
								<span class="input-block">
									${RS.CRY_CRYNAME } ${RS.NHITAX_F }
								</span>
							</div>
							<!-- 給付淨額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0985" />
									</label>
								</span>
								<span class="input-block">
									${RS.CRY_CRYNAME } ${RS.AMT7_F }
								</span>
							</div>
						</div>
						
<!-- 						<ul class="ttb-result-list result-shift"> -->
<!-- 							<li> -->
<!-- 								分配日期 -->
<%-- 								<h3><spring:message code="LB.W0988" /></h3> --%>
<%-- 								<p>${RS.TRADEDATE_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								信託編號 -->
<%-- 								<h3><spring:message code="LB.W0904" /></h3> --%>
<%-- 								<p>${RS.CREDITNO_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								基金名稱 -->
<%-- 								<h3><spring:message code="LB.W0025" /></h3> --%>
<%-- 								<p>${RS.TRANSCODE_FUNDLNAME }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								基準日 -->
<%-- 								<h3><spring:message code="LB.W0028" /></h3> --%>
<%-- 								<p>${RS.DATE_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								除息金額 -->
<%-- 								<h3><spring:message code="LB.W0990" /></h3> --%>
<%-- 								<p>${RS.CRY_CRYNAME } ${RS.EXAMT_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								基準日信託單位數 -->
<%-- 								<h3><spring:message code="LB.W0991" /></h3> --%>
<%-- 								<p>${RS.UNIT_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								每單位分配值 -->
<%-- 								<h3><spring:message code="LB.W0992" /></h3> --%>
<%-- 								<p>${RS.TRANSCRY_CRYNAME } ${RS.AMT4_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								匯率 -->
<%-- 								<h3><spring:message code="LB.Exchange_rate" /></h3> --%>
<%-- 								<p>${RS.EXRATE_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								代扣稅款 -->
<%-- 								<h3><spring:message code="LB.W0993" /></h3> --%>
<%-- 								<p>${RS.CRY_CRYNAME } ${RS.AMT5_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								代扣健保費 -->
<%-- 								<h3><spring:message code="LB.W0994" /></h3> --%>
<%-- 								<p>${RS.CRY_CRYNAME } ${RS.NHITAX_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								給付淨額 -->
<%-- 								<h3><spring:message code="LB.W0985" /></h3> --%>
<%-- 								<p>${RS.CRY_CRYNAME } ${RS.AMT7_F }</p> --%>
<!-- 							</li> -->
<!-- 						</ul> -->
						
						<div>
							<!-- 列印  -->
							<spring:message code="LB.Print" var="printbtn"></spring:message>
							<input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
						</div>
					</div>
				</div>
			</form>
		</section>
		<!-- main-content END -->
	</main>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>