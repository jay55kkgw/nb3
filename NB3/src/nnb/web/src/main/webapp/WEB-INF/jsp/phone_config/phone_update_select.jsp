<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>

	<script type="text/javascript">
	var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
	var myobj = null; // 自然人憑證用
	var urihost = "${__ctx}";
	$(document).ready(function() {
		// HTML載入完成後0.1秒開始遮罩
		setTimeout("initBlockUI()", 100);

		// 將.table變更為footable ??
		initFootable(); 
		
		init();

		init2();
		
		// 過0.5秒解遮罩
		setTimeout("unBlockUI(initBlockId)", 800);
	});
	
	/**
	 * 初始化BlockUI
	 */
	function initBlockUI() {
		initBlockId = blockUI();
	}

	// 畫面初始化
	function init2() {
		$("input[name^='ReadFlag']").on('click', function() {
			var clickName = this.id;
			if($("#"+clickName).prop('checked')){
				if(clickName == "ReadFlag1"){
					$("#terms1").show();
				}
				return false;
			}
		});
	}

	function agreeRead(data){
		var chk = "ReadFlag" + data.replace("read","");
		$("#"+chk).prop('checked',true);
	}
	
	function chkclick(){
		if($('#ReadFlag1').prop('checked')){
			return true;
		}
		else{
			return false;
		}
	}
	
    function init(){
        
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
    	$("#CNSUBMIT").click(function(e){	
    		shareinit('${__ctx}/PNONE/CONFIG/update_cannel_confirm');
		});

    	$("#UDSUBMIT").click(function(e){	
    			shareinit('${__ctx}/PNONE/CONFIG/update_change');
		});
    }
		function shareinit(url){
			if(!chkclick()){
				errorBlock(null, null, ["請審閱條款及勾選"],
						'<spring:message code= "LB.Confirm" />', null);
			}else{
				console.log("submit~~");
				$("#formId").validationEngine('detach');
				initBlockUI();
				var action = url;
				$('#formId').attr("action", action);
				unBlockUI(initBlockId);
				$("#formId").validationEngine({binded: false,promptPosition: "inline"});
				$('#formId').submit();
			}
		}
    	

	</script>
</head>
<body>

		<section id="terms1" class="error-block" style="display:none">
		<div class="error-for-message" style="max-width:80%;">
			<p class="error-title" style="margin-bottom: 0px">手機門號轉帳服務約定條款</p>
<!-- 			<p class="error-content"> -->
				<ul class="ttb-result-list terms">
					<li data-num="" style="overflow-x: auto; padding-bottom: 0px;">
						<span class="input-subtitle subtitle-color">
						<div class="CN19-clause" style="width: 100%; border: 1px solid #D7D7D7; border-radius: 6px; overflow-x: auto;">
							<%@ include file="../term/N750C.jsp"%>
						</div>
					</li>
				</ul>
<!-- 			</p> -->
			<input type="button" id="read1" value="已審閱並同意" class="ttb-button btn-flat-orange " onclick="agreeRead(this.id);$('#terms1').hide();"/>
		</div>
	</section>

	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	<!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 手機門號收款帳號設定    -->
			<li class="ttb-breadcrumb-item active" aria-current="page">手機門號收款帳號設定</li>
		</ol>
	</nav>
	
	<!-- menu、登出窗格 -->
	<div class="content row">
		
		<c:if test="${sessionScope.cusidn != null}">
			<%@ include file="../index/menu.jsp"%>
		</c:if>
		
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					變更轉入帳號
				</h2>
				<form method="post" id="formId">
					<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
					<input type="hidden" name="CHIP_ACN" id="CHIP_ACN" value="">
					<input type="hidden" name="UID" id="UID" value="">
					<input type="hidden" name="ACN" id="ACN" value="">
					<input type="hidden" name="ISSUER" id="ISSUER" value="">
					<input type="hidden" name="ACNNO" id="ACNNO" value="">
					<input type="hidden" name="OUTACN" id="OUTACN" value="">
					<input type="hidden" name="iSeqNo" id="iSeqNo" value="">
					<input type="hidden" name="ICSEQ" id="ICSEQ" value="">
					<input type="hidden" name="TAC" id="TAC" value="">
					<input type="hidden" name="TRMID" id="TRMID" value="">
					<input type="hidden" name="TRANSEQ" id="TRANSEQ" value="2500">
					<input type="hidden" name="oldtxacn" id="oldtxacn" value="${result_data.data.oldtxacn}">
					<input type="hidden" name="binddefault" id="binddefault" value="${result_data.data.binddefault}">
					<input type="hidden" name="mobilephone" id="mobilephone" value="${result_data.data.mobilephone}">
					<input type="hidden" name="oldbind" id="oldbind" value="${result_data.data.binddefault}">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<!-- 手機號碼 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											手機號碼
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.mobilephone}
										</div>
									</span>
								</div>
							</div>
	                        <!-- 綁定收款帳號 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											綁定收款帳號
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.oldtxacn}
										</div>
									</span>
								</div>
							</div>
							
						<!-- 同意作為預設收款帳號 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>同意作為預設收款帳號</h4>
										</label>
									</span>
									<span class="input-block">
										<c:if test="${result_data.data.binddefault == 'Y'}">
											<div class="ttb-input">
											<!-- 是 -->
												是
											</div>
										</c:if>
										<c:if test="${result_data.data.binddefault == 'N'}">
											<div class="ttb-input">
											<!-- 否-->
												否
											</div>
										</c:if>
									</span>
								</div>
							</div>

						<div class="ttb-input-block">
		                    	<div class="ttb-input-item row">
		                             <span class="input-title">
		                                 <label>
		                                 </label>
		                              </span>
		                              <span class="input-block">
		                                         <div class="ttb-input">
												<label class="check-block" for="ReadFlag1">
													<input type="checkbox" name="ReadFlag" id="ReadFlag1">
													<b>本人同意<font class="high-light">手機門號轉帳服務約定條款</font></b>
					                                <span class="ttb-check"></span>
												</label>
											</div>
		                              </span>
			                	</div>
							</div>
						<!-- 確定 -->
						<input type="button" id="CNSUBMIT" value="註銷綁定" class="ttb-button btn-flat-orange"/>
						<input type="button" id="UDSUBMIT" value="變更綁定" class="ttb-button btn-flat-orange"/>
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>