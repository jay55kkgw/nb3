<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			// 將.table變更為DataTable
			initDataTable();
			// 初始化時隱藏span
			$("#hideblock").hide();
		});

		function init() {

			$("#formId").validationEngine({binded: false,promptPosition: "inline",});

			$("#CMSUBMIT").click(function (e) {
				//打開驗證欄位
				$("#hideblock").show();
				var datalist = $("#ROWDATA").val();
				$("#ErrorMsg").val(datalist);

				e = e || window.event;

				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				}
				else {
					$("#formId").validationEngine('detach');
					initBlockUI();
					$("#formId").attr("action", "${__ctx}/HOUSE/GUARANTEE/interest_list_result");
					$("#formId").submit();
				}
			});
		}

		function addData(dataCount) {
			// 將頁面資料塞進hidden欄位
			console.log("dataCount: " + dataCount);
			var data = $("#" + dataCount).val();
			console.log("data: " + data);
			$("#ROWDATA").val(data);
		};

		function clearData() {
			$("#ROWDATA").val("");
		};

	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 房屋擔保借款繳息清單     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0876" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					<spring:message code="LB.W0876" /><!-- 房屋擔保借款繳息清單 -->
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post">
					<input type="hidden" id="ROWDATA" name="ROWDATA" value="" />
					<input type="hidden" name="TXTOKEN" value="${transfer_data.data.TXTOKEN}"><!-- 防止不正常交易 -->
					<input type="hidden" name="CUSIDN" value="${interest_list_confirm.data.CUSIDN}">
					<input type="hidden" name="TYPE" value="01">
					<input type="hidden" name="CURDATE" value="N">
					<div class="main-content-block row">
						<div class="col-12">
							<ul>
								<!-- 請選擇欲掛失的存單帳號-->
								<p class="text-center" style="color: red; margin-top:30px;">
									<spring:message code="LB.W0878" /><!-- 請勾選欲列印之放款序號，並確認印表機已準備就緒 -->
								</p>
							</ul>
							<!-- 房屋擔保借款繳息清單  表格 -->
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
									<tr>
										<!-- 請點選 -->
										<th class="text-center">
											<spring:message code="LB.W0879" />
										</th>
										<!-- 序號-->
										<th class="text-center">
											<spring:message code="LB.Serial_number" />
										</th>
										<!-- 貸款起日-->
										<th class="text-center">
											<spring:message code="LB.W0880" />
										</th>
										<!-- 貸款迄日-->
										<th class="text-center">
											<spring:message code="LB.W0881" />
										</th>
										<!-- 初貸金額(元)-->
										<th class="text-center">
											<spring:message code="LB.W0882" />
										</th>
									</tr>
								</thead>
								<!--空白資料 -->
								<c:if test="${empty interest_list_confirm.data.REC}">
									<tbody style="display:none;">
								</c:if>
								<!--有資料 -->
								<c:if test="${not empty interest_list_confirm.data.REC}">
									<tbody>
										<c:forEach var="row" items="${interest_list_confirm.data.REC}" varStatus="data">
											<tr>
												<td class="text-center">
													<label class="radio-block">
						                    			&nbsp; 
														<input type="radio" name="datalist" value="${data.count}" onclick="addData(${data.count})">
						                                <span class="ttb-radio"></span> 
						                             </label> 
                                   				</td>
												<td class="text-center">${row.SEQ}</td>
												<td class="text-center">${row.DATFSLN}</td>
												<td class="text-center">${row.DDT}</td>
												<td class="text-right">${row.AMTORLN}</td>
												<input type="hidden" id="${data.count}" name="${data.count}" value="${row}">
											</tr>
										</c:forEach>
									</tbody>
								</c:if>
							</table>
							<!-- 不在畫面上顯示的span -->
							<span id="hideblock">
								<!-- 驗證用的input -->
								<input id="ErrorMsg" name="ErrorMsg" type="text"
									class="text-input validate[groupRequired[datalist]]"
									style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" value="" />
							</span>
							<!-- 重新輸入 -->
							<input id="reset" name="reset" type="reset" onclick="clearData()"
								class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />"/>
							<!-- 確定 -->
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" class="ttb-button btn-flat-orange"
								value="<spring:message code="LB.Confirm" />"/>
						</div>
					</div>
					<div class="text-left">
						 <ol class="description-list list-decimal">
						 <P>
						<spring:message code="LB.Description_of_page" />
						 </P>
							<li>
								<spring:message code="LB.Interest_List_P2_D1" />
							</li>
							<li>
								<spring:message code="LB.Interest_List_P2_D2" />
							</li>
						</ol>
					</div>
				</form>
			</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>