<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

    <%@ include file="../__import_head_tag.jsp"%>
    <%@ include file="../__import_js.jsp" %>
   
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>


<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});

	function init() {
		console.log("init>>");
		var rdata = null;
		var val = null;
		// 取得下拉選單資料
		getSelectData();
		// 及時預約切換事件
		tabEvent();
		//轉出帳號餘額顯示
		acnoEvent();
		datetimepickerEvent();
		InAccountEvent();
		OutAccountEvent();
		InAmountEvent();
		refillData();
		
		//若前頁有帶acn，轉出預設為此acn
		var getacn = '${pay_information.data.Acn}';
		if(getacn != null && getacn != ''){
			$("#OUTACN option[value= '"+ getacn +"' ]").prop("selected", true);
			$("#OUTACN").change();
		}
		
// 		$('#DPNAGACNO').change(function() {
// 			$('#CARDNUM2').val("");
// 		});
		// 確定非約定可以做時在打開
		// 	agreeACNOEvent();

		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});
		$("#CMSUBMIT").click(function(e) {
			console.log("submit~~");
			$("#hideblock").show(); //非約定轉入TEXT驗證防跑版區塊
			$("#hideblock2").show(); //轉帳金額自訂TEXT驗證防跑版區塊
			$("#hideblock3").show();
			$("#hideblock4").show();
			
			
			$("#validate").val($('#CARDNUM2').val());
			$("#validate2").val($('#AmountText').val());
			$("#validate4").val($('#CMTRDATE').val());
			//金額判斷區
			//1. 約轉出>約轉入>1500W  2. 約轉出>非約轉入>300W 3.非約轉出>約/非約轉入>10W
			if($('input[name=TransferType]:checked').val()=='PD' 
				&& $('input[name=AMOUNT]:checked').val()=='self' 
					&& $('input[name=FLAG]:checked').val()=='1'){ //約出PD 約入self 
				$('#validate2').removeClass("validate[required ,custom[integer,min[1] ,max[3000000]]]");
				$('#validate2').removeClass("validate[required ,custom[integer,min[1] ,max[100000]]]");
				$('#validate2').addClass("validate[required ,custom[integer,min[1] ,max[15000000]]]");
				
			}
			else if($('input[name=TransferType]:checked').val()=='PD' 
				&& $('input[name=AMOUNT]:checked').val()=='self' 
					&& ($('input[name=FLAG]:checked').val()=='0'||$('input[name=FLAG]:checked').val()=='2')){
				$('#validate2').removeClass("validate[required ,custom[integer,min[1] ,max[15000000]]]");
				$('#validate2').removeClass("validate[required ,custom[integer,min[1] ,max[100000]]]");
				$('#validate2').addClass("validate[required ,custom[integer,min[1] ,max[3000000]]]");
				
			}
			else if($('input[name=TransferType]:checked').val()=='NPD' 
			&& $('input[name=AMOUNT]:checked').val()=='self' 
			){
				$('#validate2').removeClass("validate[required ,custom[integer,min[1] ,max[3000000]]]");
				$('#validate2').removeClass("validate[required ,custom[integer,min[1] ,max[15000000]]]");
				$('#validate2').addClass("validate[required ,custom[integer,min[1] ,max[100000]]]");
				
			}else{
				$('#validate2').removeClass("validate[required ,custom[integer,min[1] ,max[3000000]]]");
				$('#validate2').removeClass("validate[required ,custom[integer,min[1] ,max[15000000]]]");
				$('#validate2').removeClass("validate[required ,custom[integer,min[1] ,max[100000]]]");
			}
			
			
			
			//轉入信用卡帳號區    id:validate => 非約定轉入帳號TEXT  id :DPNAGACNO 非約定
			if($('input[type=radio][name=FLAG]:checked').val()=='0'){
				//主要驗證  三種狀況 1.自己輸入身分證 2.選擇下拉式選單 3.都沒輸入
				if($("#CARDNUM2").val()!=""){ //自行輸入身分證字號  舊網銀目前只用簡單的驗證A-Z加九位數 並無驗證輸入身分證之正確性
					$("#validate").addClass("validate[required ,CCtwIdCheck[validate]]");
					//額外 將輸入的值放到隱藏顯示TEXT欄位中
					$('#DPAGACNO_TEXT').val($("#CARDNUM2").val());
					//移除其他欄位驗證(約定信用卡帳號下拉式選單,非約定信用卡下拉式選單)
					$("#DPAGACNO").removeClass("validate[required ,funcCall[validate_DPAGACNO[DPAGACNO]]]");
					$("#validate3").removeClass("validate[required ,CCtwIdCheck[validate3]]");
				}else if ($("#CARDNUM2").val()=="" && !$("#DPNAGACNO").val().indexOf("#")==0){ 
					//舊網銀驗證頗怪  有選擇若中間是身分證 過 選擇不是身分證的 不過 顯示:請輸入正卡持卡人身分證字號/統一編號 然後打開被disabled的text區塊並帶入下拉式選單內的ACN
					var acn = JSON.parse($('#DPNAGACNO').val()).ACN
					console.log("ACN >>>>> "+acn);
					$("#validate3").val(acn);
					$("#validate3").addClass("validate[required ,CCtwIdCheck[validate3]]");
					
					$("#DPAGACNO_TEXT").val($("#DPNAGACNO").find(":selected").text());
					//移除其他欄位驗證(約定信用卡帳號下拉式選單,非約定信用卡TEXT)
					$("#DPAGACNO").removeClass("validate[required ,funcCall[validate_DPAGACNO[DPAGACNO]]]");
					$("#validate").removeClass("validate[required ,CCtwIdCheck[validate]]");
													 //$("#DPNAGACNO").val().startsWith("#")
				}else if($("#CARDNUM2").val()=="" && $("#DPNAGACNO").val().indexOf("#")==0){ //如果兩個都空  舊網銀是跳請輸入正卡持卡人身分證字號/統一編號
					$("#validate").addClass("validate[required ,CCtwIdCheck[validate]]");
					
					$("#DPAGACNO_TEXT").val($("#CARDNUM2").val());
					//移除其他欄位驗證(約定信用卡帳號下拉式選單)
					$("#DPAGACNO").removeClass("validate[required ,funcCall[validate_DPAGACNO[DPAGACNO]]]");
					$("#validate3").removeClass("validate[required ,CCtwIdCheck[validate3]]");
				}
				
			}else if($('input[type=radio][name=FLAG]:checked').val()=='1'){ //已約定信用卡帳號(下拉式選單)
				//主要欄位驗證機制
				$("#DPAGACNO").addClass("validate[required ,funcCall[validate_DPAGACNO[DPAGACNO]]]");
			
				$("#DPAGACNO_TEXT").val($("#DPAGACNO").find(":selected").text());
				//移除其他欄位驗證機制class (非約定信用卡帳號text , 非約定信用卡帳號下拉式選單)
				$("#validate").removeClass("validate[required ,CCtwIdCheck[validate]]");
				$("#DPNAGACNO").removeClass("validate[required ,funcCall[validate_DPAGACNO[DPNAGACNO]]]");
				$("#validate3").removeClass("validate[required ,CCtwIdCheck[validate3]]");
				
			}else if($('input[type=radio][name=FLAG]:checked').val()=='2'){ //繳納本人信用卡款radio 
				//主要驗證 no need 但要把值送到CARDNUM2給後端用
				$('#CARDNUM2').val($("#CUSIDN").val());
			
				$("#DPAGACNO_TEXT").val($("#CUSIDN").val());
				//移除其他欄位驗證機制class (約定轉入信用卡帳號 ,非約定信用卡帳號text , 非約定信用卡帳號radio)
				$("#DPAGACNO").removeClass("validate[required ,funcCall[validate_DPAGACNO[DPAGACNO]]]");
				$("#validate3").removeClass("validate[required ,CCtwIdCheck[validate3]]");
				$("#validate").removeClass("validate[required ,CCtwIdCheck[validate]]");
				
			}
			
			//金額欄位判斷
// 			if($("input[name='FGTXDATE']:checked").val()==2)//預約
// 			{
			if($("input[name='AMOUNT']:checked").val()!='self')
				if($("input[name='AMOUNT']:checked").val()<=0){
		 			errorBlock(
 						null, 
 						null,
 						['<spring:message code= "LB.X2424" />'], 
 						'<spring:message code= "LB.Confirm" />', 
 						null
 					);
		 			return false;
				}
// 			}
			
			
			if (!$('#formId').validationEngine('validate')) {
				var acn = JSON.parse($('#DPNAGACNO').val()).ACN
				console.log("ACN >>>>> "+acn);
				$("#CARDNUM2").attr("disabled",false);
				$("#CARDNUM2").val(acn);
				e.preventDefault();
			} else {
				if ($('#AmountText').val() != "") {
					$('#PAYTYPE3').val($('#AmountText').val());
				}
				initBlockUI();
				$("#formId").validationEngine('detach');
				$("form").submit();
			}
		});
		
		

	}// init END
	function refillData() {
		//上一頁回填區
		if ('${bk_key}' == 'Y') {

			//預約頁籤判斷切換 預設即時 FGTXDATE=2時為預約（有的有3）
			if ('${requestScope.back_data.FGTXDATE}' != '1') {
				$("#nav-trans-future").click();
				//預約時間回填
				$('#CMDATE').val('${requestScope.back_data.transfer_date}');
			}

			//轉出帳號回填	
			$('#OUTACN').val('${requestScope.back_data.OUTACN}');
			getACNO_Data('${requestScope.back_data.OUTACN}');
			//轉入帳號回填
			if ('${requestScope.back_data.FLAG}' == "0") {
				if('${requestScope.back_data.CARDNUM3}'.indexOf('#')==0){
					$("input:radio[name='FLAG'][value='0']").trigger("click");
					$('#CARDNUM2').val('${requestScope.back_data.CARDNUM2}');
				}else{
					$("input:radio[name='FLAG'][value='0']").trigger("click");
					var tmp = '${requestScope.back_data.CARDNUM3}';
					$("#DPNAGACNO option[value='"+ tmp +"']").prop("selected", true);
	 				$("#DPNAGACNO").change();
				}
			} else if ('${requestScope.back_data.FLAG}' == "1") {
				$('#DPAGACNO').val('${requestScope.back_data.CARDNUM1}');
				$("input:radio[name='FLAG'][value='1']").prop('checked', true);
			}else if ('${requestScope.back_data.FLAG}'== "2"){
				$("input:radio[name='FLAG'][value='2']").prop('checked', true);
			}
			
			//金額回填
			if('${requestScope.back_data.AMOUNT}'== $('#PAYTYPE1').val()){
				fstop.setRadioChecked("AMOUNT", "${requestScope.back_data.AMOUNT}",true);
			}else if('${requestScope.back_data.AMOUNT}'== $('#PAYTYPE2').val()){
				fstop.setRadioChecked("AMOUNT", "${requestScope.back_data.AMOUNT}",true);
			}else{
				fstop.setRadioChecked("AMOUNT", "self",true);
				$('#AmountText').val('${requestScope.back_data.AMOUNT}');
			}
			
			//交易備註回填 通知回填 摘要內容回填 
			$('#CMTRMAIL').val('${requestScope.back_data.CMTRMAIL}');
			$('#CMTRMEMO').val('${requestScope.back_data.CMTRMEMO}');
			$('#CMMAILMEMO').val('${requestScope.back_data.CMMAILMEMO}');
		}
	}

	// 日曆欄位參數設定
	function datetimepickerEvent() {
		$(".CMDATE").click(function(event) {
			$('#CMTRDATE').datetimepicker('show');
			$('#CMTRDATE').datetimepicker({
			minDate:'${pay_information.data.tmrDate}'
			});
		});

		jQuery('.datetimepicker').datetimepicker({
			timepicker : false,
			closeOnDateSelect : true,
			scrollMonth : false,
			scrollInput : false,
			format : 'Y/m/d',
			lang: '${transfer}'

		});
	}


	// 轉出帳號change事件，要秀出可用餘額
	function acnoEvent() {
		$("#OUTACN").change(function() {
			var acno = $('#OUTACN :selected').val();
			console.log("acno>>" + acno);
			getACNO_Data(acno);
		});
	}

	// 取得轉出帳號餘額資料
	function getACNO_Data(acno) {
		var options = {
			keyisval : true,
			selectID : '#OUTACN'
		};
		uri = '${__ctx}' + "/CREDIT/PAY/getACNO_Data_aj"
		console.log("getACNO_Data>>" + uri);
		rdata = {
			acno : acno
		};
		console.log("rdata>>" + rdata);
		fstop.getServerDataEx(uri, rdata, false, isShowACNO_Data);
	}

	// 顯示轉出帳號餘額
	function isShowACNO_Data(data) {
		//可用餘額
		var i18n= new Object();
		i18n['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
		console.log("isShowACNO_Data.data"+data);
		if(data && data.result){
			$("#acnoIsShow").show();
			$("#showText").html("");
			console.log("data.data.accno_data"+data.data.accno_data.ADPIBAL);
			// 格式化金額欄位
			i18n['available_balance'] += fstop.formatAmt(data.data.accno_data.ADPIBAL);
			$("#showText").html(i18n['available_balance']);
		}else{
			$("#acnoIsShow").hide();
			
		}
	}

	// 即時預約切換事件
	function tabEvent() {
		var sMinDate = '${pay_information.data.tmrDate}';
		var sMaxDate = '${pay_information.data.maxDate}';
		$("#nav-trans-now").click(function() {
			console.log("now");
			$("#transfer-date").hide();
			fstop.setRadioChecked("FGTXDATE", "1", true);
			$('#validate4').removeClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />,CMTRDATE,"+sMinDate+","+sMaxDate+"]]]");
		})

		$("#nav-trans-future").click(function() {
			console.log("future");
			$("#transfer-date").show();
			// 				$("input [name='FGTXDATE']").val("2");
			// 				$("input [name='FGTXDATE']").trigger("change");
			fstop.setRadioChecked("FGTXDATE", "2", true);
			$('#validate4').addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />,CMTRDATE,"+sMinDate+","+sMaxDate+"]]]");
		})
	}

	
	function getSelectData() {
		//轉出帳號
		creatOutACNO();
		//約定轉入及常用轉入帳號
		creatInACNO();
		// 等轉入帳號確定有要做非約定再打開
		// createBankSelect();
	}
	
	function creatOutACNO() {
		var options = {
			keyisval : true,
			selectID : '#OUTACN'
		};
		uri = '${__ctx}' + "/CREDIT/PAY/getOutAcno_aj"
		console.log("getSelectData>>" + uri);
		rdata = {
			type : 'out_acno'
		};
		console.log("rdata>>" + rdata);
		data = fstop.getServerDataEx(uri, rdata, false);
		if (data != null && data.result == true) {
			fstop.creatSelect(data.data, options);
		}
	}


	function creatInACNO() {
		try{
		data = null;
		uri = '${__ctx}' + "/CREDIT/PAY/getInAcno_aj"
		rdata = {
			type : 'creditcard_pay_inacno'
		};
		options = { keyisval:false ,selectID:'#DPAGACNO'}
		data = fstop.getServerDataEx(uri, rdata, false);
		console.log("data >>" + data);
		if(data.msgCode !='ENRD' && data.result == true){
			fstop.creatSelect(data.data.REC,options);
		}
		else{
			$("input:radio[name='FLAG'][value='2']").trigger("click");
			//20200507 信用卡繳費不須顯示錯誤訊息
// 			//alert('<spring:message code= "LB.Alert005" />');
// 			errorBlock(
// 					null, 
// 					null,
// 					['<spring:message code= 'LB.Alert005' />'], 
// 					'<spring:message code= "LB.Quit" />', 
// 					null
// 				);
		}
		
		options = { keyisval:false ,selectID:'#DPNAGACNO'}
		if(data.msgCode !='ENRD' && data.result == true){
			fstop.creatSelect(data.data.REC2,options);
		}else{
			$("input:radio[name='FLAG'][value='2']").trigger("click");
			//alert('<spring:message code= "LB.Alert005" />');
			//20200507 信用卡繳費不須顯示錯誤訊息
// 			errorBlock(
// 					null, 
// 					null,
// 					['<spring:message code= 'LB.Alert005' />'], 
// 					'<spring:message code= "LB.Quit" />', 
// 					null
// 				);
		}
		}catch(e){
			$("input:radio[name='FLAG'][value='2']").trigger("click");
		}
		
	}

	// 驗證帳號下拉選單
	function validate_DPAGACNO(field, rules, i, options) {
		var inputAttr = rules[i + 2];
		console.log("inputAttr>>" + inputAttr);
		var dpagacno_val = $("#" + inputAttr).find(":selected").val()
		console.log("funccall test");
		console.log("funccall test>>" + options.allrules.required.alertText);
		if (fstop.isEmptyString(dpagacno_val) || dpagacno_val.indexOf('#') > -1) {
			return options.allrules.required.alertText
		}
	}
	//copyTN
	function copyTN() {
		$("#CMMAILMEMO").val($("#CMTRMEMO").val());
	}
	//開啟通訊錄email選單
	function openAddressbook() {
		window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
	}
	//轉入帳號表單驗證轉變
	function InAccountEvent() {
		$('input[type=radio][name=FLAG]').change(function() {
			if (this.value == '1') { //約定
				$("#DPNAGACNOSELECT").hide();
				$("#validate").val("");
				$("#DPNAGACNO option[value='#0']").prop("selected", true);
// 				$("#DPNAGACNO").change();
				$("#CARDNUM2").attr("disabled", false);
				$("#DPAGACNOSELECT").show();
			} else if (this.value == '0') { //非約
				$("#DPAGACNOSELECT").hide();
				$("#DPAGACNO option[value='#0']").prop("selected", true);
				$("#DPNAGACNOSELECT").show();
				$("#DPNAGACNO").change();
				$('#PAYTYPE3').trigger('click');				
			} else if (this.value == '2'){//繳納本行信用卡
				$("#DPAGACNOSELECT").hide();
				$("#DPNAGACNOSELECT").hide();
				$("#CARDNUM2").attr("disabled", false);
			} 
		});
		$('#DPAGACNO').change(function() {
			$('input:radio[name=FLAG][value=1]').attr("checked", "checked");
			$("input:radio[name='FLAG'][value='1']").trigger('click');
			$("#CARDNUM2").attr("disabled", true);
		});
		$('#DPNAGACNO').change(function() {
			if($('#DPNAGACNO').val()!='#0' && $('#DPNAGACNO').val()!='#1' && $('#DPNAGACNO').val()!=null)$("#CARDNUM2").attr("disabled", true);
			else $("#CARDNUM2").attr("disabled", false);
		});
	}

	//轉出帳號表單驗證轉變
	function OutAccountEvent() {
		$('input[type=radio][name=TransferType]').change(function() {
			if (this.value == 'PD') { //約定
				$("#OUTACN").addClass("validate[required ,funcCall[validate_DPAGACNO[OUTACN]]]");
				$("#OUTACN_NPD").val("");
				$("#OUTACN_NPD").removeClass("validate[required]");
			} else if (this.value == 'NPD') { //非約
				$("#OUTACN").val("#");
				$("#OUTACN").removeClass("validate[required ,funcCall[validate_DPAGACNO[OUTACN]]]");
				$("#OUTACN_NPD").addClass("validate[required]");
				$("#acnoIsShow").hide();
			}
		});
		$('#OUTACN').change(
				function() {
					$('input:radio[name=TransferType][value=PD]').prop(
							"checked", "checked");
					$("#OUTACN").trigger('click');
					$("#OUTACN_NPD").val("");
					$("#OUTACN_NPD").removeClass("validate[required]");
					$("#OUTACN").removeClass("validate[required ,funcCall[validate_DPAGACNO[OUTACN]]]");
				});
		$('#OUTACN_NPD').click(function(){
			$('#TransferType_02').trigger('click');
		})
	}
	//轉入金額區塊表單連動
	function InAmountEvent() {
		//點TEXT區自動綁定點選radio
		$("#AmountText").click(function() {
			$('input:radio[name=AMOUNT][value=self]').trigger('click')
		});
		//點Radio3區自動綁定focus Text區
		$("#PAYTYPE3").click(function() {
			//$('#AmountText').trigger('focus');
		});
		//點Radio1,2清空3的text 金額
		$("#PAYTYPE1").click(function() {
			$('#AmountText').val("");
		});
		$("#PAYTYPE2").click(function() {
			$('#AmountText').val("");
		});
	}

	//驗證轉出成功Email通知欄位
	function validate_CMTRMAIL(field, rules, i, options) {
		var inputAttr = rules[i + 2];
		console.log("inputAttr>>" + inputAttr);

		var cmtrmail_val = $("#" + inputAttr).val();
		var emails = cmtrmail_val.split(';');

		var valid = true;

		for ( var i in emails) {
			value = $.trim(emails[i]);
			valid = valid && options.allrules.email.regex.test(value);
		}

		if (!valid) {
			return options.allrules.email.alertText;
		}
	}
	
	function CheckIdResult(data,result){
		showTempMessage(500,'晶片金融卡身份查驗',"","MaskArea",false);
		console.log(data);
		if("0" == data.msgCode) {
			console.log("GOODPEOPLE");
// 			document.getElementById("OUTACN").value = result;
			document.getElementById("OUTACN_NPD").value = result.substr(5);
		}else{
			//unBlockUI(initBlockId);
			try{
				if(data.data.msgCode == "0"){
			        errorBlock(
						null,
						null,
						["非本人帳戶之晶片金融卡，無法進行此功能"], 
						'確定', 
						null
					);
				}else{
			        errorBlock(
							null,
							null,
							[ data.data.msgCode + " " + data.data.msgName ], 
							'確定', 
							null
						);
				}
			}catch (e) {
		        errorBlock(
					null,
					null,
					["非本人帳戶之晶片金融卡，無法進行此功能"], 
					'確定', 
					null
				);
			}
		}
	}
</script>

</head>
<body>
	<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 繳本行信用卡費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Payment_The_Banks_Credit_Card" /></li>
		</ol>
	</nav>
	
	<!-- 左邊menu及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	
	<main class="col-12"> 
		<!--  主頁內容  -->
		<section id="main-content" class="container">
			<!-- 繳納本行信用卡 -->
			<h2><spring:message code="LB.Payment_The_Banks_Credit_Card" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form method="post" id="formId" action="${__ctx}/CREDIT/PAY/payment_confirm">
				<input type="hidden" id="DPAGACNO_TEXT" name = "DPAGACNO_TEXT" value="">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}">
				<input type="hidden" id="CUSIDN" value="${pay_information.data.CUSIDN}">
	
				<div id="step-bar">
					<ul>
						<!-- 輸入資料 -->
						<li class="active"><spring:message code="LB.Enter_data" /></li>
						<!-- 確認資料 -->
						<li class=""><spring:message code="LB.Confirm_data" /></li>
						<!-- 交易完成 -->
						<li class=""><spring:message code="LB.Transaction_complete" /></li>
					</ul>
				</div>
	
				<div class="main-content-block row radius-50">
					<nav style="width: 100%;">
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="nav-trans-now" data-toggle="tab" href="#nav-trans-now" 
								role="tab" aria-controls="nav-home" aria-selected="true">
								<spring:message code="LB.Immediately" /><!-- 即時 -->
							</a> 
							<a class="nav-item nav-link" id="nav-trans-future" data-toggle="tab" href="#nav-trans-future" 
								role="tab" aria-controls="nav-profile" aria-selected="false" >
								<spring:message code="LB.Booking" /><!-- 預約 -->
							</a>
						</div>
					</nav>
	
					<div class="col-12 tab-content" id="nav-tabContent">
						<div class="tab-pane fade " id="nav-trans-future" role="tabpanel"
							aria-labelledby="nav-profile-tab">
						</div>
	
						<div class="ttb-input-block tab-pane fade show active"
							id="nav-trans-now" role="tabpanel" aria-labelledby="nav-home-tab">
	
							<div class="ttb-message">
								<span></span>
							</div>
	<!-- 							這是即時的屬性  預設就是隱藏 -->
							<div class="ttb-input" style="display: none;">
								<label class="radio-block"> <spring:message code="LB.Immediately" /><!-- 即時 -->
								<input type="radio" name="FGTXDATE" value="1" checked> 
								<span class="ttb-radio"></span>
								</label>
							</div>
							
							<!-- 轉帳日期 -->
							<div id="transfer-date" class="ttb-input-item row" style="display: none;">
								<span class="input-title">
									<!-- 轉帳日期 -->
									<label><h4><spring:message code="LB.Transfer_date" /></h4></label>
								</span>
								<span class="input-block" >
									<div class="ttb-input">
										<!-- 預約 --> 
										<label class="radio-block"><spring:message code="LB.Booking" />
											<input type="radio" name="FGTXDATE" value="2"> 
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<input id="CMTRDATE" name="CMTRDATE" type="text" class="text-input datetimepicker" value="${pay_information.data.tmrDate}"  maxlength="10">
										<span class="input-unit CMDATE">
											<img src="${__ctx}/img/icon-7.svg" />
										</span>
										<span id="hideblock4">
												<!-- 驗證用的input -->
												<input id="validate4" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</div>
								</span>
							</div>
							<!-- 轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<!-- 轉出帳號 -->
									<label><h4><spring:message code="LB.Payers_account_no" /></h4></label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<!-- 約定帳號 -->
										<label class="radio-block"> <spring:message code="LB.Designated_account" />
											<input type="radio"  id="TransferType_01" name="TransferType" value="PD" checked>
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<select name="OUTACN" id="OUTACN" class="custom-select select-input half-input validate[required ,funcCall[validate_DPAGACNO[OUTACN]]]">
										<option value="#">---<spring:message code="LB.Select_account" />---</option>
										</select>
										<div id="acnoIsShow">
											<span id = "showText" class="input-unit "></span>
											<span id= "showMoney" class="input-unit"></span>
										</div>
									</div>
									<div class="ttb-input">
										<!-- 非約定  -->
										<label class="radio-block"><spring:message code="LB.Non-designated_account" />
											<input type="radio" id = "TransferType_02" name="TransferType" value="NPD"  onclick="listReaders();" >
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<!-- 晶片金融卡帳號 -->
										<input type="text" name="OUTACN_NPD" id="OUTACN_NPD" class="text-input" readonly="readonly" value="" /> 
										<span class="input-subtitle subtitle-color"><spring:message code="LB.X2361" /></span>
									</div>
								</span>
							</div>
							<!-- 轉入信用卡帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.Transfer_to_credit_card_account" /></h4></label><!-- 轉入信用卡帳號 -->
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<!-- 繳納本人信用卡款  -->
										<label class="radio-block"> <spring:message code="LB.X0852" />
										<input type="radio" name="FLAG" value="2" checked>
										<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<!-- 已約定  -->
										<label class="radio-block"><spring:message code="LB.X1824" />
										<input type="radio" name="FLAG" value="1"> 
										<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input"  id="DPAGACNOSELECT">
										<select id="DPAGACNO" name="CARDNUM1" class="custom-select select-input half-input " >
										
										</select>
									</div>
									<div class="ttb-input">
										<!-- 非約定信用卡帳號 -->
										<label class="radio-block"><spring:message code="LB.Non-designated_credit_card_account" />
										<input type="radio" name="FLAG" value="0"> 
										<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<input type="text" name="CARDNUM2" id="CARDNUM2"  size="32" maxlength="16"  value="" class="text-input" placeholder="<spring:message code="LB.Transfer_to_credit_card_account_note2" />"/>
										<label class="check-block">
											<!-- 加入常用帳號 -->
											<spring:message code="LB.Join_common_account" />
											<input type="checkbox" name="ADDACN" id="ADDACN" value="checked" >
											<span class="ttb-check"></span>
										</label>
										<span id="hideblock" >
										<!-- 驗證用的input -->
										<input id="validate" type="text" size="32" maxlength="16"  class="text-input" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
										<span id="hideblock3" >
										<!-- 驗證用的input -->
										<input id="validate3" type="text" size="32" maxlength="16"  class="text-input" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</div>
									<div class="ttb-input" id="DPNAGACNOSELECT" style="display: none;">
										<select id="DPNAGACNO" name="CARDNUM3" class="custom-select select-input half-input ">
										</select>
									</div>

								</span>
							</div>
							<br>
							<!-- 轉帳金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0021" /></h4></label><!-- 轉帳金額 -->
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
										<spring:message code="LB.The_total_amount_payable" />（ <spring:message code="LB.NTD" /> ）：${pay_information.data.CURR_BAL}  <spring:message code="LB.Dollar"/><!-- 本期應繳金額 -->
										<input type="radio"  id="PAYTYPE1" name="AMOUNT" value="${pay_information.data.CURR_BAL_SEND}" checked="checked" />
										<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block">
										<spring:message code="LB.Minimum_payment"/> （ <spring:message code="LB.NTD" /> ）： ${pay_information.data.TOTL_DUE}  <spring:message code="LB.Dollar"/> <!-- 本期最低金額 -->
										<input type="radio"  id="PAYTYPE2" name="AMOUNT" value="${pay_information.data.TOTL_DUE_SEND}"/>
										<span class="ttb-radio"></span>
										</label> 
									</div>
									<div class="ttb-input">
										<label class="radio-block">
										<spring:message code="LB.Payment_amount_myself" /><!-- 自訂 -->
										<input type="radio" id="PAYTYPE3" name="AMOUNT" value="self"/>
										<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<input type="text" name="AMOUNT" id="AmountText" class="text-input "  maxlength="11"> &nbsp; <spring:message code="LB.Dollar"/><br>
										<span id="hideblock2" >
										<!-- 驗證用的input -->
										<input id="validate2" type="text" size="32" maxlength="16"  class="text-input" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
										<span class="input-remarks" style="color:red"><spring:message code="LB.W0622" /></span>
<!-- 										(以晶片金融卡執行非約定轉帳，交易限額NTD5萬元/筆，NTD10萬元/日，NTD20萬元/月；轉入帳號為線上約定者，每日限額NTD10萬元) -->
										<span class="input-unit"></span><!-- 新台幣 -->
									</div>
								</span>
							</div>
							<br>
							<!-- 交易備註 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.Transfer_note" /></h4></label><!-- 交易備註 -->
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" size="56" maxlength="20" value="" > <!-- 非必填 -->
										<span class="input-unit"></span>
										<span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
									</div>
								</span>
							</div>
							<br>
							<!-- 轉出成功 Email通知 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.X0479" /></h4><!-- Email通知 -->
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<span class="input-subtitle subtitle-color"><spring:message code="LB.Notify_me" /></span><!-- 通知本人 -->
										<span class="input-subtitle subtitle-color">${sessionScope.dpmyemail}</span>
									</div>
									<div class="ttb-input">
										<span class="input-subtitle subtitle-color"><spring:message code="LB.Another_notice" /></span><!-- 另通知 -->
									</div>
									<div class="ttb-input">
										<input type="text" id="CMTRMAIL" name="CMTRMAIL" class="text-input validate[funcCall[validate_EmailCheck[CMTRMAIL]]]" maxlength="500" placeholder="<spring:message code="LB.Not_required" />" value=""> <!-- 非必填 --> 
										<span class="input-unit"></span>
	<!-- 									TODO	window open 去查 TxnAddressBook  參數帶入身分證字號 -->
										<button  type="button" class="btn-flat-orange" onclick="openAddressbook()"><spring:message code="LB.Address_book" /></button><!-- 通訊錄 -->
									</div>
									<div class="ttb-input">
										<input type="text" id="CMMAILMEMO" name="CMMAILMEMO"
											class="text-input" placeholder="<spring:message code="LB.Summary" />" value="" maxlength="20"/> <!-- 摘要內容 -->
										<span class="input-unit"></span>
										<button type="button" class="btn-flat-orange"  onclick="copyTN()" ><spring:message code="LB.As_transfer_note"/></button><!-- 同交易備註 -->
									</div>
								</span>
							</div>
							
						</div><!-- ttb-input-block tab-pane fade show active -->
						<input type="reset" class="ttb-button btn-flat-gray"  value="<spring:message code="LB.Re_enter" />" />
							<!-- 重新輸入 -->
						<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange"  value="<spring:message code="LB.Confirm" />">
							<!-- 確定 -->
					</div><!-- col-12 tab-content END -->
				</div><!-- main-content-block END -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
							<li>
							<spring:message code="LB.Payment_P1_D1-1" /><a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank"><spring:message code="LB.Payment_P1_D1-2" /></a>
 							<%-- 查詢「<a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank">交易最高限額表</a>」。 --%>
							</li>
							
							<li><spring:message code="LB.Payment_P1_D2" />
							<!-- 逾15：30之繳款，列入次日繳款記錄。 -->
							</li>
        					<li><spring:message code="LB.Payment_P1_D3" />
        					<!-- 如預約之轉帳日期為曆法所無之日期，以該月之末日為轉帳日，例如：預約6月31日轉帳，因6月無31日，故會在6月30日轉帳。 -->
        					</li>
         					<li><spring:message code="LB.Payment_P1_D4" />
         					<!-- 預約交易可預約次日起1年內之交易。預約之轉帳日為曆法所無之日期，以該月之末日為轉帳日，例如：預約轉帳日為6月31日，但6月無31日，故轉帳日為6月30日。 -->
         					</li>
         					<li><spring:message code="LB.Payment_P1_D5" />
         					<!-- 預約交易請於轉帳日之前1日，存足款項於轉出帳號備扣。預約交易將於轉帳日與立即交易併計最高轉出限額。 -->
         					</li>
         					<li><spring:message code="LB.Payment_P1_D6" />
         					<!-- 預約成功不代表交易已完成，請於轉帳日利用『預約交易結果查詢』，以確認交易結果。 -->
         					</li>
         					<li><spring:message code="LB.Payment_P1_D7" />
         					<!-- 如欲取消預約，請於轉帳日之前1日辦理。交易密碼(SSL)預約交易，請以交易密碼(SSL)取消交易；晶片金融卡預約交易，請以晶片金融卡取消交易；電子簽章(i-key)預約交易，請以電子簽章(i-key)取消預約。 -->
         					</li>
         					<li><spring:message code="LB.Payment_P1_D8" />
         					<!-- 為提升服務品質，本行自100年5月起信用卡帳單將由各卡帳單改為「依正卡持卡人身份證號碼歸戶方式之整合式帳單」，故若您原約定之轉入帳號為本行信用卡卡號者，將於100年5月1日失效，並盡速至全省各營業單位重新約定，造成不便，敬請見諒！ -->
         					</li>
         					<c:if test="${pay_information.data.isProd eq 3}"><li><span><font color="red"><spring:message code="LB.X2405" /></font></span></li></c:if>
							<!--併行作業期間:倘您已曾經登入新版網路銀行後，前於舊版網銀已預約之轉帳交易，統一將移轉至新版網路銀行執行及查詢;另新、舊版網路銀行執行預約交易，需回原執行之網路銀行查詢，無法於兩系統間相互查詢。 -->
      					</ol>
			</form>
		</section>
	</main>
	</div><!-- content row END -->

	<%@ include file="../index/footer.jsp"%>
	  
</body>
</html>

