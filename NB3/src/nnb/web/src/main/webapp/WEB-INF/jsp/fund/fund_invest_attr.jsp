<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

		// 畫面初始化
		function init() {
			//表單驗證
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			// 確認鍵 click
			processQuery();
		}

		// 確認鍵 click
		function processQuery() {
			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					var uid ='${fund_invest_attr.data.CUSIDN}'
					var action = '';
					if (uid.length == 10){
						 action = '${__ctx}/FUND/TRANSFER/fund_invest_attr1?TXID=S';
					}
					else{
						action = '${__ctx}/FUND/TRANSFER/fund_invest_attr3?TXID=S';
					}
					$("form").attr("action", action);
					$("#formId").submit();
				}
			});
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 投資屬性評估調查表－（自然人版）     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1218" /></li>
		</ol>

	</nav>
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 	快速選單及主頁內容 -->
		<main class="col-12">
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2>
				<spring:message code="LB.W1217" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId">
					<div class="main-content-block row">
						<div class="col-12" id="prodshow1">
							<div class="ttb-message">
								<spring:message code= "LB.Remind_you" />，<font color=red><b><spring:message code= "LB.X1511" /></b></font>，<spring:message code= "LB.X1512" /><br>
								<spring:message code= "LB.X1513" /><font color=red><b><spring:message code= "LB.X1514" /><BR>
										<spring:message code= "LB.X1515" /><BR>
										<spring:message code= "LB.X1516" /></b></font><spring:message code= "LB.X1517" />
							</div>
							<input class="ttb-button btn-flat-orange" type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code= "LB.Confirm_1" />"/>
						</div>
					</div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>

</html>