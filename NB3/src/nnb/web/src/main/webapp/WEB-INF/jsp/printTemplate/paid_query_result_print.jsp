<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/>
	<br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/>
	<br/>
	<br/>
	<!-- 已繳款本息查詢   -->
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/>
	<br/>
	<br/>
	<!-- 查詢時間： -->
	<label><spring:message code="LB.Inquiry_time"/>：</label>
	<label>${CMQTIME}</label>
	<br/>
	<br/>
	<!-- 查詢期間 -->
	<label><spring:message code="LB.Inquiry_period_1" />：</label>
	<label>${CMPERIOD}</label>
	<br/>
	<br/>
	<!-- 資料總數： -->
	<label><spring:message code="LB.Total_records"/>：</label>
	<label>${COUNT} <spring:message code="LB.Rows"/></label>
<br/>
<br/>

	<div  style="margin-top: 5%">
	<table class="print">
		<thead>
		<tr>
			<!-- 表名-->
			<td><spring:message code="LB.Report_name" /></td>
			<td class="text-left" colspan="6">
				<!-- 臺幣已繳款本息明細表--> 
				<spring:message code="LB.Paid_principal_and_interest_TWD" />
			</td>
		</tr>
		<tr>
			<td><spring:message code="LB.Account" /></td>
			<td><spring:message code="LB.Seq_of_account" /></td><!-- 分號 -->
			<td><spring:message code="LB.Paid_principal_and_interest_TWD" /></td><!-- 繳款日期 -->
			<td><spring:message code="LB.Principal" /></td><!-- 本金 -->
			<td><spring:message code="LB.Interest" /></td><!-- 利息 -->
			<td><spring:message code="LB.Interest_payment_after_the_change" /></td><!-- 異動後繳息迄日 -->
			<td><spring:message code="LB.Current_loan_balance" /></td><!-- 當期借款餘額 -->
		</tr>
		</thead>
		<tbody>
		<c:if test="${TOPMSG_016 eq 'OKLR'}">
			<c:forEach var="dataList" items="${dataListMap[0]}">
				<tr>
					<td class="text-center">${dataList.ACN }</td>
					<td class="text-center">${dataList.SEQ }</td>
					<td class="text-center">${dataList.DATTRN}</td>
					<td class="text-right">${dataList.TRNPAL}</td>
					<td class="text-right">${dataList.TRNINT}</td>
					<td class="text-center">${dataList.CURIPD}</td>
					<td class="text-right">${dataList.BAL}</td>
				</tr>
			</c:forEach>
		</c:if>
		<c:if test="${TOPMSG_016 != 'OKLR'}">
			<tr>
				<td>${TOPMSG_016}</td>
				<td colspan="6">${errorMsg016}</td>
			</tr>
		</c:if>
		</tbody>
	</table>
	</div>
		<br />
		<br />
	<div  style="margin-top: 5%">
	<table class="print">
		<thead>
		<tr>
			<!-- 表名-->
			<td><spring:message code="LB.Report_name" /></td>
			<td class="text-left" colspan="8">
				<!-- 外幣已繳款本息明細表-->
				<spring:message code="LB.Paid_principal_and_interest_FX" />
			</td>
		</tr>
		<tr>
			<td><spring:message code="LB.Account" /></td>
			<td><spring:message code="LB.Transaction_Number" /></td><!-- 交易編號 -->
			<td><spring:message code="LB.Principal_Currency" /></td><!-- 本金幣別 -->
			<td><spring:message code="LB.Principal" /></td><!-- 本金 -->
			<td><spring:message code="LB.Interest_Currency" /></td><!-- 利息幣別 -->
			<td><spring:message code="LB.Interest" /></td><!-- 利息 -->
			<td><spring:message code="LB.Interest_payment_after_the_change" /></td><!-- 異動後繳息迄日 -->
			<td><spring:message code="LB.Data_date" /></td><!-- 資料日期 -->
		</tr>
		</thead>
		<tbody>
		<c:if test="${TOPMSG_554 eq 'OKLR'}">
			<c:forEach var="dataListB" items="${dataListMap[1]}">
				<tr>
					<td class="text-center">${dataListB.LNACCNO}</td>
					<td class="text-center">${dataListB.LNREFNO}</td>
					<td class="text-center">${dataListB.CAPCCY}</td>
					<td class="text-right">${dataListB.CAPCOS}</td>
					<td class="text-center">${dataListB.INTCCY}</td>
					<td class="text-right">${dataListB.INTCOS}</td>
					<td class="text-center">${dataListB.CUSPYDT}</td>
					<td class="text-center">${dataListB.LNUPDATE}</td>
				</tr>
			</c:forEach>
		</c:if>
		<c:if test="${TOPMSG_554 != 'OKLR'}">
			<tr>
				<td>${TOPMSG_554}</td>
				<td  colspan="7">${errorMsg554}</td>
			</tr>
		</c:if>
		</tbody>
	</table>
	</div>
	<br>
		<div class="text-left">
						<spring:message code="LB.Description_of_page" />
						<ol class="list-decimal text-left">
							<%-- <li><spring:message code="LB.Balance_query_P1_D1" /></li>
							<li><spring:message code="LB.Balance_query_P1_D2" /></li> --%>
							<li><spring:message code="LB.Paid_Query_P2_D1" /></li>
							<li><spring:message code="LB.Paid_Query_P2_D2" /></li>
							<li><spring:message code="LB.Paid_Query_P2_D3" /></li>
						</ol>
					</div>
	</body>
</html>