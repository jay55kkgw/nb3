<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            //initFootable(); // 將.table變更為footable 
            init();
            setTimeout("initDataTable()",100);
        });

        function init() {
            //列印
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"f_letter_of_credit_opening_inquiry_import_detail_print",
					"jspTitle":'<spring:message code= "LB.X0008" />',
					"CMQTIME":"${base_result.data.CMQTIME}",
					"LCNO":"${base_result.data.LCNO}",
					"RORIGAMT":"${base_result.data.RORIGAMT}",
					"RBENNAME":"${base_result.data.RBENNAME}",
					"CMRECNUM":"${base_result.data.CMRECNUM}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/FCY/ACCT/f_credit_open_query_date_result';
    			$('#back').val("Y");
    			$("#formId").attr("action", action);
    			initBlockUI();
    			$("#formId").submit();
    		});
        }
       
	 	function finishAjaxDownload(){
			$("#actionBar").val("");
			unBlockUI(initBlockId);
		}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 帳戶明細查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Demand_Deposit_Detail" /></li>
    <!-- 商業信用狀明細     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0004" /></li>
    <!-- 帳戶明細查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Demand_Deposit_Detail" /></li>
    <!-- 進口到單明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0008" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 進口到單明細(未輸入) -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.X0008" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                
                <form id="formId" method="post">
                	<input type="hidden" id="back" name="back" value="">
					<input type="hidden" id="LCNO" name="LCNO" value="${base_result.data.LCNO}" />
                    <input type="hidden" name="USERDATA_X50_import_detail" value="${base_result.data.USERDATA_X50}" />
                    
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                            <ul class="ttb-result-list">
                                <li>
                                	<h3><spring:message code="LB.Inquiry_time" /></h3>
                                    <!-- 查詢時間 -->
                                    <p>
                                        ${base_result.data.CMQTIME }
                                    </p>
								</li>
								<li>
                                	<h3><spring:message code="LB.L/C_no" /></h3>
                                    <!-- 信用狀號碼 : -->
                                    <p>
                                        ${base_result.data.LCNO}
                                    </p>
								</li>
								<li>
                                	<h3><spring:message code="LB.W0094" /></h3>
                                    <!-- 開狀金額 : -->
                                    <p>
                                        ${base_result.data.RORIGAMT}
                                    </p>
                                    
								</li>
								<li>
                                	<h3><spring:message code="LB.W0174" /></h3>
                                    <!-- 受益人 : -->
                                    <p>
                                        ${base_result.data.RBENNAME}
                                    </p>
								</li>
								<li>
                                	<h3><spring:message code="LB.Total_records" /></h3>
                                    <!-- 資料總數 : -->
                                    <p>
                                        ${base_result.data.CMRECNUM}
                                        <!--筆 -->
                                        <spring:message code="LB.Rows" />
                                    </p>
                                </li>
                            </ul>
                            <!-- 表格區塊 -->
                        
                            <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                                <thead>
                                    <tr>
                                    	<!--到單日期-->
                                        <th>
                                            <!--<spring:message code="LB.Change_date" />-->
											<spring:message code="LB.W0166" />
                                        </th>
                                        <!--到單號碼-->
                                        <th>
                                            <!--<spring:message code="LB.Change_date" />-->
											<spring:message code="LB.X0007" />
                                        </th>
                                        <!-- 幣別 -->
                                        <th data-breakpoints="xs">
                                            <spring:message code="LB.Currency" />
                                        </th>
                                        <!--到單金額-->
                                        <th data-breakpoints="xs">
                                            <!-- <spring:message code="LB.Deposit_amount" /> -->
											<spring:message code="LB.W0169" />
                                        </th>
                                        <!-- 融資利率 -->
                                        <th data-breakpoints="xs sm">
                                            <!-- <spring:message code="LB.Account_balance_2" /> -->
											<spring:message code="LB.X0010" />
                                        </th>
                                        <!-- 融資起日 -->
                                        <th data-breakpoints="xs sm">
                                            <!-- <spring:message code="LB.Account_balance_2" /> -->
											<spring:message code="LB.W0170" />
                                        </th>
                                        <!-- 融資迄日 -->
                                        <th data-breakpoints="xs sm">
                                            <!-- <spring:message code="LB.Account_balance_2" /> -->
											<spring:message code="LB.W0171" />
                                        </th>
                                        <!-- 承諾到期日 -->
                                        <th data-breakpoints="xs sm">
                                            <!--<spring:message code="LB.Data_content" />-->
											<spring:message code="LB.X0009" />
                                        </th>
                                        <!-- 備註 -->
                                        <th>
                                            <spring:message code="LB.Note" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="dataList" items="${ base_result.data.REC }">
                                        <tr>
                                            <!-- 到單日期-->
                                            <td style="text-align:center">${dataList.RIBDATE }</td>
                                            <!--到單號碼-->
                                            <td style="text-align:center">${dataList.RIBNO }</td>
                                       		<!-- 幣別 -->
                                            <td style="text-align:center">${dataList.RBILLCCY }</td>
                                        	<!-- 到單金額 -->
                                            <td  style="text-align: right;">${dataList.RBILLAMT }</td>
	                                        <!--融資利率  -->
                                            <td  style="text-align: right;">${dataList.RADVFIXR }</td>
                                            <!-- 融資起日 -->
                                            <td style="text-align:center">${dataList.RINTSTDT }</td>
                                        	<!-- 融資迄日 -->
                                            <td style="text-align:center">${dataList.RINTDUDT }</td>
                                        	<!-- 承諾到期日 -->
                                            <td style="text-align:center">${dataList.RCFMDATE }</td>
                                        	<!-- 備註 -->
                                            <td style="text-align:center">${dataList.RMARK }</td>
                                        	
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
	                        <!--button 區域 -->
	                        
	                            <!--回上頁 -->
	                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
	                            <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
	                            <!-- 繼續查詢 -->
	                            <c:if test="${ base_result.data.TOPMSG != 'OKLR' }">
		                            <!--<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>-->
		                            <input type="button" name="CMCONTINU" id="CMCONTINU" value="<spring:message code="LB.X0151" />" class="ttb-button btn-flat-orange">
	                            </c:if>
	                            <!-- 列印  -->
	                            <spring:message code="LB.Print" var="printbtn"></spring:message>
	                            <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
	                        
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>