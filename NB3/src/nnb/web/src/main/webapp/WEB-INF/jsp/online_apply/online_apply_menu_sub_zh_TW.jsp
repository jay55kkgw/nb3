<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<link rel="stylesheet" type="text/css"
		href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript"
		src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript"
		src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript"
		src="${__ctx}/js/jquery.datetimepicker.js"></script>
		<script type="text/JavaScript">
			$(document).ready(function () {
				
				$('#applyTable').DataTable({
					"bPaginate" : false,
					"bLengthChange": false,
					"scrollX": true,
					"sScrollX": "99%",
					"scrollY": "80vh",
			        "bFilter": false,
			        "bDestroy": true,
			        "bSort": false,
			        "info": false,
			        "scrollCollapse": true,
			    });
				
				$(window).bind('resize', function (){
					$('#applyTable').DataTable({
						"bPaginate" : false,
						"bLengthChange": false,
						"scrollX": true,
						"sScrollX": "99%",
						"scrollY": "80vh",
				        "bFilter": false,
				        "bDestroy": true,
				        "bSort": false,
				        "info": false,
				        "scrollCollapse": true,
				    });
				});

				
			});
			
		</script>
	</head>
	<body >
		<!-- 功能清單及登入資訊 -->
		<div class="content row">
			<!-- 快速選單及主頁內容 -->
			<main class="col-12">
				<!-- 主頁內容  -->
				<section id="main-content" class="container" >
					<!-- 功能名稱 -->
					<h2>線上申請</h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<!-- 功能內容 -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="CN19-1-header">
								<div class="logo">
									<img src="${__ctx}/img/logo-1.svg">
								</div>
							</div>
						</div>
						<!-- 如何申請 頁面 -->
						<div class="col-12 tab-content" id="CN19-how-apply">
						<div id="MESSAGE" style="margin: auto">
							<font color="red" size="5">
					      	<B>申請方式</B><br>
				      		</font>
							</div>
							<div class="ttb-input-block">
								<div class="CN19-caption1">
									壹、網路銀行申請方式及服務功能
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>申請方式</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>線上申請：限個人戶</li>
									</ul>
									<div>
										本行晶片金融卡存戶：<br>
 										未曾臨櫃申請之存戶，可線上申請<font color="#FF6600">查詢</font>服務，請使用<b>本行晶片金融卡 + 晶片讀卡機</b> ，直接線上申請網路銀行，如該晶片金融卡已具備非約定轉帳功能者，得線上申請「金融卡線上申請/取消交易服務功能」，執行新臺幣非約定轉帳交易、繳費稅交易、線上更改通訊地址/電話及使用者名稱/簽入密碼/交易密碼線上解鎖等功能。<br>
										<br>
										本行信用卡正卡客戶（不含商務卡、採購卡、附卡及VISA金融卡）：<br>
 										請您直接線上申請網路銀行，只要輸入個人驗證資料，即可立刻使用網路銀行信用卡相關服務。<br>
 										<br>
									</div>
									<ul>
										<li>臨櫃申請：</li>
									</ul>
									<div>
										法人存戶：<br>
 										請負責人攜帶<b>公司登記證件、身分證、存款印鑑及存摺</b>至開戶行申請網路銀行。<br>
										<br>
										個人存戶：<br>
 										請本人攜帶<b>身分證、存款印鑑及存摺</b>至開戶行申請網路銀行，全行收付戶（即通儲戶），亦可至各營業單位申請。<br>
 										<br>
									</div>
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>服務功能</span>
								</div>
								<div>
									<table id="applyTable" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
										<thead>				
											<tr>
												<!-- 功能比較-->
												<th>功能比較</th>
												<!-- 臨櫃申請-->
	 											<th>臨櫃申請</th>
												<!-- 晶片金融卡線上申請-->
												<th>晶片金融卡線上申請</th>
												<!-- 晶片金融卡線上申請且線上申請交易服務功能 -->
												<th>晶片金融卡線上申請且線上申請交易服務功能</th>
												<!-- 信用卡線上申請 -->
												<th>信用卡線上申請</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													信用卡帳務查詢
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													信用卡電子帳單
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													補寄信用卡帳單
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													信用卡繳款Email通知
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													存、放款、外匯帳務查詢
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													基金查詢
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													黃金存摺查詢
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													黃金申購/回售及變更
												</td>
												<td>O(註1)</td>
												<td>X</td>
												<td>X</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													轉帳、繳費稅、定存
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													新台幣非約定轉帳(註2)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													基金申購及變更
												</td>
												<td>O</td>
												<td>X</td>
												<td>X</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													申請代扣繳費用
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													掛失服務
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													變更通訊資料(註3)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													使用者名稱/簽入/交易密碼線上解鎖(註4)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													理財試算
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													國內臺幣匯入匯款通知設定
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
										</tbody>
									</table>
									</div>
									<div>
										<b>備註：</b><br>
										<ol class="list-decimal">
											<li>臨櫃申請黃金存摺帳戶客戶，需於臨櫃或逕自本行網路銀行申請黃金存摺網路交易功能，始得執行黃金申購/回售及變更等功能。</li>
											<li>倘已線上申請交易功能者，且該晶片金融卡已具備非約定轉帳功能者，並得以本人帳戶之晶片金融卡之主帳號為轉出帳戶，執行新臺幣非約定轉帳交易、繳費稅交易。</li>
											<li>交易機制為「電子簽章」或「晶片金融卡」，即可執行線上更改通訊地址/電話。</li>
											<li>交易機制為「電子簽章」或「晶片金融卡」，即可執行線上簽入/交易密碼線上解鎖。</li>
											<li>O：表示可使用功能，X：表示不可使用功能。(原註2)</li>
										</ol>
									</div>
								<div class="CN19-caption1">
									貳、黃金存摺線上申請資格、服務功能及費用收取
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>線上申請黃金存摺帳戶</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>申請資格</li>
									</ul>
									<div>
										年滿20歲之本國人。<br>
 										已申請使用本行網路銀行且已約定新台幣活期性存款（不含支票存款）帳戶為轉出帳號者。<br>
										已申請使用本行晶片金融卡＋晶片讀卡機或電子簽章（憑證載具）。<br>
									</div>
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>線上申請黃金存摺網路交易功能</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>申請資格</li>
									</ul>
									<div>
										年滿20歲之本國人。<br>
 										已申請使用本行網路銀行且已約定新台幣活期性存款（不含支票存款）帳戶為轉出帳號者。<br>
										已申請使用本行晶片金融卡＋晶片讀卡機或電子簽章（憑證載具）。<br>
									</div>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>服務功能</li>
									</ul>
									<div>
										查詢服務：黃金存摺餘額、明細、當日、歷史價格查詢功能。<br> 
 										交易服務：黃金買進、回售、繳納定期扣款失敗手續費功能。<br>
										預約服務：預約買進、回售、取消、查詢功能。<br>
									 	定期定額服務：定期定額申購、定期定額變更、定期定額查詢功能。<br>
									 	線上申請：線上申請黃金存摺帳戶、黃金存摺網路交易功能。<br>
									</div>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>費用收取</li>
									</ul>
									<div>
										線上「申請黃金存摺帳戶」及「定期定額投資」扣帳成功，收取新台幣50元手續費，其餘交易免收。<br>
									</div>
								</div>
							</div>
						</div>
					</div>
					</form>
					</div>
				</section>
			</main>
		</div>
	</body>

</html>