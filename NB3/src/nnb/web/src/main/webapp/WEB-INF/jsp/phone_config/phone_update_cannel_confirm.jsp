<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<script type="text/javascript">
	var urihost = "${__ctx}";
	$(document).ready(function() {
		// HTML載入完成後0.1秒開始遮罩
		setTimeout("initBlockUI()", 100);
		initFootable(); // 將.table變更為footable 
		init();
		
		// 過0.5秒解遮罩
		setTimeout("unBlockUI(initBlockId)", 800);
	});
	
	/**
	 * 初始化BlockUI
	 */
	function initBlockUI() {
		initBlockId = blockUI();
	}
	
    function init(){

    	$('input[type=radio][name=txselect]').change(function() {
        	$("#validate_txselect").val(this.value);
    	});
    	
    	//上一頁按鈕
		$("#pageback").click(function() {

			var action = '${pageContext.request.contextPath}' + '${previous}'
			$('#back').val("Y");
			$('#jsondc').attr('disabled', true);
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").validationEngine('detach');
			$("#formId").submit();

		});
        
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
    	$("#CMSUBMIT").click(function(e){			
			console.log("submit~~");
			if (!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
 				console.log("submit~~");
 				$("#formId").validationEngine('detach');
 				initBlockUI();
   				var action = '${__ctx}/PNONE/CONFIG/update_cannel_result';
   				$('#formId').attr("action", action);
    			unBlockUI(initBlockId);
    			$("#formId").submit();
 			}
		});
    }	
    

	</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	<!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 手機門號收款帳號設定    -->
			<li class="ttb-breadcrumb-item active" aria-current="page">手機門號收款帳號設定</li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		
		<c:if test="${sessionScope.cusidn != null}">
			<%@ include file="../index/menu.jsp"%>
		</c:if>
		
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					註銷轉入帳號
				</h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
<!-- 				<div id="step-bar"> -->
<!-- 					<ul> -->
<%-- 						<li class="finished"><spring:message code="LB.Enter_data" /></li> --%>
<%-- 						<li class="active"><spring:message code="LB.Confirm_data" /></li> --%>
<%-- 						<li class="">  <spring:message	code="LB.Transaction_complete" /></li> --%>
<!-- 					</ul> -->
<!-- 				</div> -->
				<form method="post" id="formId">
					<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
					<input type="hidden" name="CHIP_ACN" id="CHIP_ACN" value="">
					<input type="hidden" name="UID" id="UID" value="">
					<input type="hidden" name="ACN" id="ACN" value="">
					<input type="hidden" name="ISSUER" id="ISSUER" value="">
					<input type="hidden" name="OUTACN" id="OUTACN" value="">
					<input type="hidden" name="TRANSEQ" id="TRANSEQ" value="2500">
					<input type="hidden" name="IP" value="${transfer_data.data.IP}">
					<input type="hidden" name="TXTOKEN" id="TXTOKEN" value="${transfer_data.data.TXTOKEN}">
					<input type="hidden" name="mobilephone" id="mobilephone" value="${transfer_data.data.mobilephone}">
					<input type="hidden" name="binddefault" id="binddefault" value="${transfer_data.data.binddefault}">
					<input type="hidden" name="txacn" id="txacn" value="${transfer_data.data.txacn}">
					
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
						
						<div class="ttb-message">
								<span>
								<!-- 請確認註銷資料 -->
								請確認註銷資料
								</span>
							</div>
						
							<!-- 手機號碼 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											手機號碼
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.mobilephone}
										</div>
									</span>
								</div>
							</div>
							
							<!-- 預設綁定轉入帳號 -->
<!-- 							<div class="ttb-input-block"> -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title"> -->
<!-- 										<label> -->
<!-- 											預設綁定轉入帳號 -->
<!-- 										</label> -->
<!-- 									</span> -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<!-- 											001 - 00232435346 -->
<!-- 										</div> -->
<!-- 										<div class="ttb-input"> -->
<!-- 											<input type="checkbox" id="cbox1" value="first_checkbox"> -->
<!-- 	  										<label for="cbox1">同意註銷</label> -->
<!--   										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
<!-- 							</div> -->
							
							<!-- 綁定轉入帳號 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											註銷綁定帳號	
										</label>
									</span>
									<span class="input-block"> 
										<div class="ttb-input">
											${transfer_data.data.txacn}
										</div>
									</span>
								</div>
							</div>
							
							<!-- 預設綁定轉入帳號 -->
<!-- 							<div class="ttb-input-block"> -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title"> -->
<!-- 										<label> -->
<!-- 											取消範圍 -->
<!-- 										</label> -->
<!-- 									</span> -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<!-- <!-- 											<input type="checkbox" id="cbox1" value="first_checkbox"> -->
<!-- 	  										<input type="radio" id="CANNEL_A" name="txselect" value="A" > -->
<!-- 	  										<label for="cbox1">註銷本行綁定帳戶以及預設收款帳戶</label> -->
<!--   										</div> -->
<!-- 										<div class="ttb-input"> -->
<!-- <!-- 											<input type="checkbox" id="cbox1" value="first_checkbox"> --> 
<!-- 	  										<input type="radio" id="CANNEL_B" name="txselect" value="B" > -->
<!-- 	  										<label for="cbox1">註銷本行綁定帳戶</label> -->
<!--   										</div> -->
<!-- 										<input id="validate_txselect" name="validate_txselect" type="text" class="text-input validate[required]"  -->
<!-- 										style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" autocomplete="off"/> -->
<!-- 			                        </span> -->
<!-- 								</div> -->
<!-- 							</div> -->
							<input type="button" id="pageback" class="ttb-button btn-flat-gray" value="回上頁"/>
							<!-- 確定 -->
							<input type="button" id="CMSUBMIT" value="確認" class="ttb-button btn-flat-orange"/>
					</div>
					</div>
												<!-- 說明 -->
<!-- 					<ol class="description-list list-decimal"> -->
<!-- 						<li> -->
 						<span><font color="red">重要提醒：註銷手機門號設定後，您的朋友將無法再由此手機門號轉帳給您綁定之帳號。</font></span>
<!-- 						</li> -->
<!-- 			        </ol> -->
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>