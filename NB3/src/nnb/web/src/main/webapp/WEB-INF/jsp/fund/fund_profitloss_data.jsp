<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/dataTables.rowsGroup.js"></script>
<%-- <script type="text/javascript" src="${__ctx}/js/dataTables.fixedColumns.min.js"></script> --%>
<style>
	.Line-break{
		white-space:normal;
		word-break:break-all;
		width:100px;
		word-wrap:break-word;
	}
</style>
<script type="text/javascript">

$(document).ready(function(){
	$('.btn-filter').on('click', function() {
		$('.btn-filter.current').removeClass('current btn btn-primary');
		$(this).addClass('current btn btn-primary');
		var searchdata = $(this).text();
		if (searchdata == "<spring:message code= 'LB.All' />") {
			searchdata = "";
		}
		$('#fundtable1').dataTable().api().columns(1).search(searchdata).draw();

	});
	
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	setTimeout("initDataTable()",100);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	init();
	shwd_prompt_init(false);
	//initFootable();
	
	
	$("#printButton").click(function(){
		var params = {
			"jspTemplateName":"fund_profitloss_data_print",
			"jspTitle":"<spring:message code= "LB.W0903" />",
			"CMQTIME":"${bs.data.bsData.CMQTIME}",
			"dataCount":"${bs.data.dataCount}",
			"hiddenName":"${bs.data.hiddenName}",
			"secondDataCount":"${bs.data.secondDataCount}",
			"TOTTWDAMTFormat":"${bs.data.bsData.TOTTWDAMTFormat}",
			"TOTREFVALUEFormat":"${bs.data.bsData.TOTREFVALUEFormat}",
			"TOTDIFAMTFormat":"${bs.data.bsData.TOTDIFAMTFormat}",
			"TOTLCYRATFormat":"${bs.data.bsData.TOTLCYRATFormat}",
			"TOTDIFAMT2Format":"${bs.data.bsData.TOTDIFAMT2Format}",
			"TOTFCYRATFormat":"${bs.data.bsData.TOTFCYRATFormat}",
			"thirdDataCount":"${bs.data.thirdDataCount}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
});
function processURL(TRANSCODE,COUNTRYTYPE,CDNO,selectID,REFMARKTransfer){
	var selectValue = $("#" + selectID).val();

	if(selectValue == "#"){
		return false;
	}

	if(selectValue == "1"){
		window.open("https://ebank.tbb.com.tw/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=1&TRANSCODE="+TRANSCODE);
	}
	else if(selectValue == "2"){
		window.open("https://ebank.tbb.com.tw/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=2&TRANSCODE="+TRANSCODE);
	}
	else if(selectValue == "3"){
		if(REFMARKTransfer != "N"){
			//alert("<spring:message code= "LB.X0970" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X0970' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;
        }
		$("#formID").attr("action","${__ctx}/FUND/REDEEM/fund_redeem_data?CDNO=" + CDNO + "&TRANSCODE=" + TRANSCODE);
		$("#formID").submit();
	}
	else if(selectValue == "4"){
		if(REFMARKTransfer != "N"){
			//alert("<spring:message code= "LB.X0970" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X0970' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;
        }
		$("#formID").attr("action","${__ctx}/FUND/TRANSFER/query_fund_transfer_data?CDNO=" + CDNO + "&TRANSCODE=" + TRANSCODE);
		$("#formID").submit();
	}
	else if(selectValue == "5"){
		//基金歷史交易明細資料查詢
		$("#formID").attr("action","${__ctx}/FUND/QUERY/transfer_data_query_history?CDNO=" + CDNO);
		$("#formID").submit();
	}
	else if(selectValue == "6"){
		//定期投資約定變更
		$("#formID").attr("action","${__ctx}/FUND/ALTER/regular_investment?CDNO=" + CDNO);
		$("#formID").submit();
	}
	else if(selectValue == "7"){
		window.open("https://ebank.tbb.com.tw/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=7&TRANSCODE="+TRANSCODE);
	}
	else if(selectValue == "8"){
		window.open("https://ebank.tbb.com.tw/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=8&TRANSCODE="+TRANSCODE);
	}
	else if(selectValue == "9"){
		window.open("https://ebank.tbb.com.tw/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=9&TRANSCODE="+TRANSCODE);
	}
	else if(selectValue == "10"){
		//基金交易資料查詢
		$("#formID").attr("action","${__ctx}/FUND/QUERY/transfer_data_query");
		$("#formID").submit();
	}
}
function processURL_small(TRANSCODE,COUNTRYTYPE,CDNO,selectval,REFMARKTransfer){
	var selectValue = selectval;

	if(selectValue == "#"){
		return false;
	}

	if(selectValue == "1"){
		window.open("https://ebank.tbb.com.tw/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=1&TRANSCODE="+TRANSCODE);
	}
	else if(selectValue == "2"){
		window.open("https://ebank.tbb.com.tw/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=2&TRANSCODE="+TRANSCODE);
	}
	else if(selectValue == "3"){
		if(REFMARKTransfer != "N"){
			//alert("<spring:message code= "LB.X0970" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X0970' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;
        }
		$("#formID").attr("action","${__ctx}/FUND/REDEEM/fund_redeem_data?CDNO=" + CDNO + "&TRANSCODE=" + TRANSCODE);
		$("#formID").submit();
	}
	else if(selectValue == "4"){
		if(REFMARKTransfer != "N"){
			//alert("<spring:message code= "LB.X0970" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X0970' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;
        }
		$("#formID").attr("action","${__ctx}/FUND/TRANSFER/query_fund_transfer_data?CDNO=" + CDNO + "&TRANSCODE=" + TRANSCODE);
		$("#formID").submit();
	}
	else if(selectValue == "5"){
		//基金歷史交易明細資料查詢
		$("#formID").attr("action","${__ctx}/FUND/QUERY/transfer_data_query_history?CDNO=" + CDNO);
		$("#formID").submit();
	}
	else if(selectValue == "6"){
		//定期投資約定變更
		$("#formID").attr("action","${__ctx}/FUND/ALTER/regular_investment?CDNO=" + CDNO);
		$("#formID").submit();
	}
	else if(selectValue == "7"){
		window.open("https://ebank.tbb.com.tw/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=7&TRANSCODE="+TRANSCODE);
	}
	else if(selectValue == "8"){
		window.open("https://ebank.tbb.com.tw/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=8&TRANSCODE="+TRANSCODE);
	}
	else if(selectValue == "9"){
		window.open("https://ebank.tbb.com.tw/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=9&TRANSCODE="+TRANSCODE);
	}
	else if(selectValue == "10"){
		//基金交易資料查詢
		$("#formID").attr("action","${__ctx}/FUND/QUERY/transfer_data_query");
		$("#formID").submit();
	}
}
//快速選單測試新增
//將actionbar select 展開閉合
$(function(){
		$("#click").on('click', function(){
		   	var s = $("#actionBar2").attr('size')==1?8:1
		   	$("#actionBar2").attr('size', s);
		});
		$("#actionBar2 option").on({
   			click: function() {
       			$("#actionBar2").attr('size', 1);
   			},
		});
});
function init(){
	console.log("RR");
	var pre="上一頁";
	var next="下一頁";
	var search="搜尋";
	if(dt_locale == "zh_CN")
		{
		pre = "上一页";
		next = "下一页";
		search="搜寻";
		}
	if(dt_locale == "en")
		{
		pre="Previous";
		next="Next";
		search="Search";
		}
		table = $('#fundtable1').dataTable({
			"columnDefs": [
			{
				searchable: true,
				visible: true,
				targets: [2]
			},
			{
				orderable: false,
				targets: [2, 3, 5, 8]
			}
		],
		"scrollX": false,
		"sScrollX": "99%",
		"autoWidth": false,
		"ordering": false,
		"info": false,
		"lengthChange": false,
		"colReorder": false,
		"fixedColumns":   {
	        leftColumns: 1 //左側要固定的欄目數，如果右側需要固定可以用 rightColunms
	    },
		"language": {
			"search": "搜尋",
			"oPaginate": {
				"sFirst": "第一頁",
				"sPrevious": "上一頁",
				"sNext": "下一頁",
				"sLast": "最後一頁",
			},
		},
	});

		if ($(window).width() < 576) {
			$('#example thead th:eq(0)').css('width', '200px');
		}

		
		
		
	
}
function hd2(T){
	var t = document.getElementById(T);
	if(t.style.visibility === 'visible'){
		t.style.visibility = 'hidden';
	}
	else{
		$("div[id^='"+T+"']").each(function() {
			var d = document.getElementById($(this).attr('id'));
			d.style.visibility = 'hidden';
	    });
		t.style.visibility = 'visible';
	}
}
</script>
</head>
<body>
<c:set var="SHWD" value="${bs.data.SHWD}"></c:set>
<%@ include file="fund_shwd.jsp"%>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
		
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 基金餘額/損益查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0903" /></li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W0903" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
				<div class="main-content-block row">
				<div class="col-12 tab-content">
					<form id="formID" method="post">
						<input type="hidden" name="ADOPID" value="C012"/>
					
						
	                        <ul class="ttb-result-list">
	                            <li>
	                                <h3><spring:message code="LB.Inquiry_time"/>：</h3>
	                                <p>${bs.data.bsData.CMQTIME}</p>
	                            </li>
	                            <li>
	                                <h3><spring:message code="LB.Total_records"/>：</h3>
	                                <p>${bs.data.dataCount} <spring:message code="LB.Rows"/></p>
	                            </li>
	                            <li>
	                                <h3><spring:message code="LB.Name"/>：</h3>
	                                <p>${bs.data.hiddenName}</p>
	                            </li>
	                        </ul>
	                        <div class="my-3 table-select-btn">
							<button type="button" class="btn btn-primary btn-filter current"><spring:message code= 'LB.All' /></button>
							<button type="button" class="btn btn-filter"><spring:message code= 'LB.X1847' /></button>
							<button type="button" class="btn-filter"><spring:message code= 'LB.W1086' /></button>
						    </div>
	                        <table id="fundtable1" class="table table-striped ttb-table" data-show-toggle="first">
                            <thead>
                            <th>
	                            <!--信託編號<br>基金名稱 -->
	                            <spring:message code="LB.W0904"/><br><spring:message code="LB.W0025"/>
							</th>
							<th>
	                            <!--首次申購<hr>申購方式 -->
	                            <spring:message code="LB.X2247"/><hr><spring:message code= 'LB.X2248' />
							</th>
							<th>
								<!--投資幣別<hr>計價幣別 -->
	                            <spring:message code="LB.W0908"/><hr><spring:message code="LB.W0909"/>
							</th>
							<th>
								<!-- 信託金額<hr>參考現值 -->
								<spring:message code="LB.W0026"/><hr><spring:message code="LB.W0915"/>
							</th>
							<th>
								<!-- 單位數/淨值<hr>淨值日 -->
								<spring:message code="LB.W0027"/>/<spring:message code="LB.W1198"/><hr><spring:message code="LB.W0912"/>
							</th>
							<th>
								<!-- 匯率 -->
								<spring:message code="LB.X2388"/><br><spring:message code="LB.Exchange_rate"/>
							</th>
							<th>
								<!-- 參考損益<hr>累積配息 -->
								<spring:message code="LB.W0916"/><hr><spring:message code="LB.X2249"/>
							</th>
							<th>
								<!-- 損益報酬率<hr>含息報酬率 -->
								<spring:message code="LB.X2388"/><spring:message code="LB.W0936"/><hr><spring:message code="LB.X2388"/><spring:message code="LB.W0938"/>
							</th>
							<th>
								<!-- 快速選單 -->
								<spring:message code="LB.W1055"/>
							</th>
							</tr>
							</thead>
	                           <tbody>

	                            <c:forEach var="dataMap" items="${bs.data.rows}" varStatus="status">
	                                <tr class="text-center">
	                                    <!--信託編號<br>基金名稱 -->
	                                    <td class="Line-break">${dataMap.hiddenCDNO}<br/>${dataMap.TRANSCODE}/${dataMap.FUNDLNAME}</td>
	                                    <!--首次申購<hr>申購方式 -->
	                                    <td>${dataMap.AC225Format}<hr/>${dataMap.AC202Chinese}</td>
	                                    <!--投資幣別<hr>計價幣別 -->
	                                    <td>${dataMap.CRY}<hr/>${dataMap.TRANSCRY}</td>
										<!-- 信託金額<hr>參考現值 -->
	                                    <td style="text-align:right">
											${dataMap.TOTAMTFormat}
	                                        <hr/>
	                                        ${dataMap.REFVALUEFormat}
										</td>
										<!-- 單位數/淨值<hr>淨值日 -->
										<td>
											<c:if test="${dataMap.ACUCOUNTFormat == '0.0000'}">
	                                            <spring:message code="LB.W0921"/>
	                                        </c:if>
	                                        <c:if test="${dataMap.ACUCOUNTFormat != '0.0000'}">
	                                            ${dataMap.ACUCOUNTFormat}
	                                        </c:if>
	                                        /
	                                        ${dataMap.REFUNDAMTFormat}
											<hr>
											${dataMap.NET01Format}
										</td>
										<!-- 匯率 -->
										<td style="text-align:right">
											${dataMap.FXRATEFormat}
										</td>
										<!-- 參考損益<hr>累積配息 -->
										<td style="text-align:right">
											<c:if test="${dataMap.DIFAMTDouble >= 0}">
	                                            <font color="red">${dataMap.DIFAMTFormat}</font>
	                                        </c:if>
	                                        <c:if test="${dataMap.DIFAMTDouble < 0}">
	                                            <font color="green" >${dataMap.DIFAMTFormat}</font>
	                                        </c:if>
											<hr>
											<c:if test="${dataMap.AMTDouble >= 0}">
	                                            <font color="red">${dataMap.AMTFormat}</font>
	                                        </c:if>
	                                        <c:if test="${dataMap.AMTDouble < 0}">
	                                            <font color="green">${dataMap.AMTFormat}</font>
	                                        </c:if>
										</td>
										<!-- 損益報酬率<hr>含息報酬率 -->
										<td style="text-align:right">
											<c:if test="${dataMap.LCYRATDouble >= 0}">
	                                            <font color="red">${dataMap.LCYRATFormat}％</font>
	                                        </c:if>
	                                        <c:if test="${dataMap.LCYRATDouble < 0}">
	                                            <font color="green">${dataMap.LCYRATFormat}％</font>
	                                        </c:if>
	                                        <hr>
	                                        <c:if test="${dataMap.FCYRATDouble >= 0}">
	                                            <font color="red">${dataMap.FCYRATFormat}％</font>
	                                        </c:if>
	                                        <c:if test="${dataMap.FCYRATDouble < 0}">
	                                            <font color="green">${dataMap.FCYRATFormat}％</font>
	                                        </c:if>
										</td>
	                                    <!-- 快速選單 -->
	                                    <td>
	                                    	<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${status.index}')" value="..." />                              
	                                        <select class="custom-select d-none d-lg-inline-block fast-select" id="typeSelect${status.index}" onchange="processURL('${dataMap.TRANSCODE}','${dataMap.COUNTRYTYPE}','${dataMap.CDNO}','typeSelect${status.index}','${dataMap.REFMARKTransfer}')">
	                                            <option value="#"><spring:message code="LB.Select"/></option>
	                                            <option value="1"><spring:message code="LB.W0923"/></option>
	                                            <option value="2"><spring:message code="LB.W0924"/></option>
	                                            <option value="7"><spring:message code="LB.W0925"/></option>
	                                            <option value="8"><spring:message code="LB.W0926"/></option>
	                                            <option value="9"><spring:message code="LB.W0927"/></option>
	                                            <option value="10"><spring:message code="LB.W0928"/></option>
	                                            <c:if test="${dataMap.ACUCOUNTFormat != '0.0000' && bs.data.txnFlag == true}">
	                                                <option value="3"><spring:message code="LB.W0929"/></option>
	                                                <option value="4"><spring:message code="LB.W0930"/></option>
	                                            </c:if>
	                                            <option value="5"><spring:message code="LB.W0931"/></option>
	                                            <c:if test="${dataMap.AC202 == '2'}">
	                                                <option value="6"><spring:message code="LB.W0932"/></option>
	                                            </c:if>
	                                        </select>
<!-- 	                                       快速選單測試新增div -->
	                                    </td>
	                                </tr>
	                                       <div id="actionBar2${status.index}" class="fast-div-grey" style="visibility: hidden;">
												<div class="fast-div">
													<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${status.index}')">
		                              				<!-- 快捷操作 -->
		                              				<p><spring:message code= "LB.X1592" /></p>
													<ul>
														<a href="javascript: processURL_small('${dataMap.TRANSCODE}','${dataMap.COUNTRYTYPE}','${dataMap.CDNO}','1','${dataMap.REFMARKTransfer}')"><li><spring:message code= "LB.W0923" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
														<a href="javascript: processURL_small('${dataMap.TRANSCODE}','${dataMap.COUNTRYTYPE}','${dataMap.CDNO}','2','${dataMap.REFMARKTransfer}')"><li><spring:message code= "LB.W0924" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
														<c:if test="${dataMap.ACUCOUNTFormat != '0.0000' && bs.data.txnFlag == true}">
														<a href="javascript: processURL_small('${dataMap.TRANSCODE}','${dataMap.COUNTRYTYPE}','${dataMap.CDNO}','3','${dataMap.REFMARKTransfer}')"><li><spring:message code= "LB.W0929" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
		                                            	<a href="javascript: processURL_small('${dataMap.TRANSCODE}','${dataMap.COUNTRYTYPE}','${dataMap.CDNO}','4','${dataMap.REFMARKTransfer}')"><li><spring:message code= "LB.W0930" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
		                                            	</c:if>
														<a href="javascript: processURL_small('${dataMap.TRANSCODE}','${dataMap.COUNTRYTYPE}','${dataMap.CDNO}','5','${dataMap.REFMARKTransfer}')"><li><spring:message code= "LB.W0931" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
														<c:if test="${dataMap.AC202 == '2'}">
		                                                <a href="javascript: processURL_small('${dataMap.TRANSCODE}','${dataMap.COUNTRYTYPE}','${dataMap.CDNO}','6','${dataMap.REFMARKTransfer}')"><li><spring:message code= "LB.W0932" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
		                                            	</c:if>
														<a href="javascript: processURL_small('${dataMap.TRANSCODE}','${dataMap.COUNTRYTYPE}','${dataMap.CDNO}','7','${dataMap.REFMARKTransfer}')"><li><spring:message code= "LB.W0925" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
														<a href="javascript: processURL_small('${dataMap.TRANSCODE}','${dataMap.COUNTRYTYPE}','${dataMap.CDNO}','8','${dataMap.REFMARKTransfer}')"><li><spring:message code= "LB.W0926" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
														<a href="javascript: processURL_small('${dataMap.TRANSCODE}','${dataMap.COUNTRYTYPE}','${dataMap.CDNO}','9','${dataMap.REFMARKTransfer}')"><li><spring:message code= "LB.W0927" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
														<a href="javascript: processURL_small('${dataMap.TRANSCODE}','${dataMap.COUNTRYTYPE}','${dataMap.CDNO}','10','${dataMap.REFMARKTransfer}')"><li><spring:message code= "LB.W0928" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
													</ul>
													<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${status.index}')" value="<spring:message code= "LB.X1572" />"/>
												</div>
											</div>
	                            </c:forEach>
	                           	</tbody>
	                   
	                        </table>
	                        <ul class="ttb-result-list">
	                        	<li class="full-list"><p><spring:message code="LB.Note"/>：<spring:message code="LB.W0915"/>=<spring:message code="LB.W0027"/>*<spring:message code="LB.W1198"/>*<spring:message code="LB.X2388"/><spring:message code="LB.Exchange_rate"/></p></li>
	                        </ul>
	                        <div class="ttb-message">
	                            <p><spring:message code="LB.X0393" /></p>
	                        </div>
	                        <table class="table table-striped ttb-table dtable" data-show-toggle="first">
	                            <thead>
									<tr>
										<th data-title="<spring:message code="LB.W0908"/>"><spring:message code="LB.W0908"/></th>
										<th data-title="<spring:message code="LB.W0933"/>"><spring:message code="LB.W0933"/></th>
										<th data-title="<spring:message code="LB.W0934"/>"><spring:message code="LB.W0934"/></th>
										<th data-title="<spring:message code="LB.W0935"/>" ><spring:message code="LB.W0935"/></th>
										<th data-title="<spring:message code="LB.W0936"/>" ><spring:message code="LB.W0936"/></th>
										<th data-title="<spring:message code="LB.W0937"/>" ><spring:message code="LB.W0937"/></th>
										<th data-title="<spring:message code="LB.W0938"/>" ><spring:message code="LB.W0938"/></th>
									</tr>
								</thead>
	                            <tbody>
	                            <c:if test="${empty bs.data.rows}">
	                                <tr>
	                                    <td><spring:message code="LB.W0939"/></td>
	                                    <td><spring:message code="LB.W0939"/></td>
	                                    <td><spring:message code="LB.W0939"/></td>
	                                    <td><spring:message code="LB.W0939"/></td>
	                                    <td><spring:message code="LB.W0939"/></td>
	                                    <td><spring:message code="LB.W0939"/></td>
	                                    <td><spring:message code="LB.W0939"/></td>
	                                </tr>
	                            </c:if>
	                            <c:forEach var="secondDataMap" items="${bs.data.secondRows}">
	                                <tr>
	                                    <td class="text-center">${secondDataMap.ADCCYNAME}</td>
	                                    <td style="text-align:right">${secondDataMap.SUBTOTAMTFormat}</td>
	                                    <td style="text-align:right">${secondDataMap.SUBREFVALUEFormat}</td>
	                                    <td style="text-align:right">${secondDataMap.SUBDIFAMTFormat}</td>
	                                    <td style="text-align:right">${secondDataMap.SUBLCYRATFormat}％</td>
	                                    <td style="text-align:right">${secondDataMap.SUBDIFAMT2Format}</td>
	                                    <td style="text-align:right">${secondDataMap.SUBFCYRATFormat}％</td>
	                                </tr>
	                            </c:forEach>
	                            <tr>
	                                <td class="text-center"><spring:message code="LB.W0940"/></td>
	                                <td style="text-align:right">${bs.data.bsData.TOTTWDAMTFormat}</td>
	                                <td style="text-align:right">${bs.data.bsData.TOTREFVALUEFormat}</td>
	                                <td style="text-align:right">${bs.data.bsData.TOTDIFAMTFormat}</td>
	                                <td style="text-align:right">${bs.data.bsData.TOTLCYRATFormat}％</td>
	                                <td style="text-align:right">${bs.data.bsData.TOTDIFAMT2Format}</td>
	                                <td style="text-align:right">${bs.data.bsData.TOTFCYRATFormat}％</td>
	                            </tr>
	                        </tbody>
	                        </table>
	                       
	                        <ul class="ttb-result-list">
	                        	<li class="full-list"><p><spring:message code="LB.W0941"/></p></li>
	                        	<br><br>
	                            <c:if test="${bs.data.thirdDataCount > 0}">
	                            <li class="full-list"><p><spring:message code="LB.W0942"/></p></li>
	                            </c:if>
	                            <table>
	                            <c:forEach var="thirdDataMap" items="${bs.data.thirdRows}">
	                            <tr>
	                            	<td class=" text-left">${thirdDataMap.UNALLOTAMTCRYADCCYNAME}&nbsp;</td>
	                            	<td class=" text-right">${thirdDataMap.UNALLOTAMTFormat}</td>
	                            	<td  class=" text-right">&nbsp;&nbsp;&nbsp;<spring:message code="LB.W0921"/></td>
	                            </tr>
	                            </c:forEach>
	                            </table>
<%-- 	                             <c:forEach var="thirdDataMap" items="${bs.data.thirdRows}"> --%>
<%-- 	                            <li class="full-list"><p>${thirdDataMap.UNALLOTAMTCRYADCCYNAME}&nbsp;&nbsp;&nbsp;${thirdDataMap.UNALLOTAMTFormat}&nbsp;&nbsp;&nbsp;未分配</p></li> --%>
<%-- 	                            </c:forEach> --%>
	                        </ul>
								<input type="button" id="printButton" value="<spring:message code="LB.Print"/>" class="ttb-button btn-flat-orange"/>
	                    </div>
					</form>
				</div>

				
				<!-- 說明 -->
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><font color="red"><b><spring:message code="LB.Profitloss_Balance_P1_D1" /></b></font></li>
					<li><font color="red"><spring:message code="LB.Profitloss_Balance_P1_D2" /></font></li>
				</ol>

			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
<c:if test="${showDialog == 'true'}">
			<%@ include file="../index/txncssslog.jsp"%>
			</c:if>
</body>
</html>
