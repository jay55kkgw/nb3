<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<section id="password-miss" class="modal fade more-info-block" role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header mb-0">
					<p class="ttb-pup-header">忘記密碼</p>
				</div>
				<div class="modal-body w-auto m-0 pl-5 pr-5">
					<p class="text-warning">重設密碼</p>
					<p>提供客戶得以晶片金融卡重新設定使用者名稱、簽入密碼或交易密碼，並透過簡訊發送授權碼至客戶已留存於本行之行動電話確認身份完成後，即可使用重設後之使用者名稱、簽入密碼或交易密碼登入網路銀行或執行交易。</p>
					<a href="#" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/RESET/user_reset','', '')">重設使用者名稱/簽入密碼</a>
					<br />
					<a href="#" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/RESET/ssl_reset','', '')">重設交易密碼</a>
					<hr />
					<p class="text-warning">忘記密碼說明</p>
					<p>1.提供客戶簽入密碼/使用者名稱及交易密碼連續輸入錯誤5次，得以電子簽章或本行晶片金融卡解除鎖定，解鎖成功後，即可以原交易密碼執行交易，倘再連續錯誤超過5次，即需回臨櫃辦理重置密碼作業。</p>
					<p>2.如需回臨櫃辦理重置密碼，請依下列辦理:</p>
					<table class="password-miss-table">
						<tbody>
							<tr>
								<td>本行存戶</td>
								<td></td>
							</tr>
							<tr>
								<td>法人戶</td>
								<td>請負責人親攜<span class="text-danger">公司登記證件</span>、<span class="text-danger">身分證</span>、<span class="text-danger">存款印鑑</span>
									及<span class="text-danger">存摺</sapn>至開戶行辦理『重置密碼』，
									領取密碼單後請於１個月內登入網路銀行並變更密碼，逾期密碼失效。</td>
							</tr>
							<tr>
								<td>個人戶</td>
								<td>請本人親攜<span class="text-danger">身分證件</span>、<span class="text-danger">存款印鑑</span>及<span class="text-danger">存摺</span>至開戶行辦理『重置密碼』，
									全行收付戶（即通儲戶），亦可至<a href="#">至國內各分行</a>辦理。領取密碼單後請於１個月內登入網路銀行並變更密碼，逾期密碼失效。</td>
							</tr>
							<tr>
								<td>本行晶片金融卡線 上申請</td>
								<td>請本人親攜身分證件、存款印鑑及存摺至開戶行辦理『重置密碼』，全行收付戶(即通儲戶)，亦可至<a href="#">至國內各分行</a>辦理。領取密碼單後請於１個月內登入網路銀行並變更密碼，逾期密碼失效。</td>
							</tr>
							<tr>
								<td>本行信用卡線上申請</td>
								<td>本行信用卡線上申請：請電洽<span class="text-danger">本行客服0800-01-7171</span>處理後之次日起重新線上申請網路銀行。</td>
							</tr>
						</tbody>
					</table>
					<p>3.如您在國外忘記網路銀行密碼或網路銀行密碼遭鎖定時，您可委託家人處理，
						並請您洽駐外單位辦理授權書認證，將此認證授權書寄回台灣給您的代理人，代理人收到認證授權書後，
						再請代理人持此認證授權書、授權人之開戶印鑑、身分證明文件以及代理人的印鑑、身分證明文件於營業時間至分行辦理密碼重置。</p>
					<hr />
					<p class="text-warning">密碼設定規則</p>
					<p>使用者名稱<span class="text-danger">6－16位英數字，英文至少2位且區分大小寫</sapn>。 簽入密碼、交易密碼：<span class="text-danger">6－8位英、數字或英數字混合，如變更密碼為英文字請注意大小寫</span>。</p>
					<p>請勿使用個人或公司顯性資料作為網路銀行使用者名稱或密碼，以免遭他人猜中。例如身分證字號、統一編號、生日、聯絡電話，或與基本資料相關之具規則性排列，例如身分證後6碼，電話前/後6碼等。</p>
					<p>請勿使用連號或重號作為網路銀行使用者名稱，例如123456、654321、111111、ABCDEF、FEDCBA、AAAAAA。</p>
					<p>使用者名稱、簽入密碼、交易密碼連續輸入錯誤5次將會被系統鎖住，一般網路銀行用戶、企業網路銀行系統管理者，請至往來營業單位辦理『重置密碼』；企業網路銀行一般使用者，請系統管理者登入網路銀行，於『使用者管理』，重新設定您的密碼。</p>
					<hr />
					<p class="text-warning">其他使用問題</p>
					<p>請直接至<a href="#">客服信箱</a>留言。</p>
					<p>或洽詢：臺灣0800-01-7171、02-23577171按3、基金(02)2559-7171#5461～5463、香港 29710111(營業日9:00~18:00)。</p>
				</div>
				<div class="modal-footer ttb-pup-footer">
					<input type="submit" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="關閉" />
				</div>
			</div>
		</div>
	</section>