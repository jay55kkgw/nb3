<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body  class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<c:if test="${TRANSERTYPE =='1'}">
		<p><spring:message code="LB.Payment_successful" /></p>
	</c:if>
	<!-- 預約成功 -->
	<c:if test="${TRANSERTYPE =='2'}">
		<p><spring:message code= "LB.W0284" /></p>
	</c:if>
	</div>
	<br />
		<table class="print">
			<!-- 								資料時間 -->
			<tr>
				<td style="text-align: center"><spring:message
						code="LB.Trading_time" /></td>
				<td>${CMQTIME}</td>
			</tr>
			<c:if test="${TRANSERTYPE == '2'}">
				<tr>
					<td style="text-align: center"><spring:message
							code="LB.Transfer_date" /></td>
					<td>${BOOKDATE}</td>
				</tr>
			</c:if>
			<tr>
				<td style="text-align: center"><spring:message
						code="LB.Payers_account_no" /></td>
				<td>${OUTACN}</td>
			</tr>
			<tr>
				<td style="text-align: center"><spring:message
						code="LB.Payees_account_no" /></td>
				<td>${INACN}</td>
			</tr>
			<tr>
				<td style="text-align: center"><spring:message code="LB.Amount" /></td>
				<td><spring:message code="LB.NTD" />&nbsp;
					${AMOUNT}&nbsp; 
				<spring:message code="LB.Dollar" />
			</td>
			</tr>
			<!-- 					            備註                    -->
			<tr>
				<td style="text-align: center"><spring:message
						code="LB.Transfer_note" /></td>
				<td>${CMTRMEMO}</td>
			</tr>
			<c:if test="${TRANSERTYPE == '1'}">
				
					<tr>
						<td style="text-align: center"><spring:message
								code="LB.Payers_account_balance" /></td>
						<td><spring:message code="LB.NTD" />&nbsp;${O_TOTBAL}&nbsp;<spring:message code="LB.Dollar" /></td>
						
					</tr>
				
					<tr>
						<td style="text-align: center"><spring:message
								code="LB.Payers_available_balance" /></td>
						<td><spring:message code="LB.NTD" />&nbsp;${O_AVLBAL}&nbsp;<spring:message code="LB.Dollar" /></td>
					</tr>
				
			</c:if>
		</table>
	<br />
	<br />
	<div style="margin: auto;">
	<c:if test="${TRANSERTYPE == '1'}">
		<div class="text-left">
			<p><spring:message code= "LB.Description_of_page" />：</p>
			<ol class="list-decimal text-left">
				<li><spring:message code= "LB.Payment_P3_D1" /></li>
				<li><spring:message code= "LB.Payment_P3_D2" /></li>
				<li><spring:message code= "LB.Payment_P3_D3" /></li>
			</ol>
		</div>
	</c:if>
	<c:if test="${TRANSERTYPE == '2'}">
		<div class="text-left">
			<spring:message code= "LB.Description_of_page" />
			<ol class="list-decimal text-left">
				<li><spring:message code= "LB.Payment_P1_D3" /></li>
				<li><spring:message code= "LB.Payment_P1_D5" /></li>
				<li><spring:message code= "LB.Payment_P1_D6" /></li>
				<li><spring:message code= "LB.Payment_P1_D7" /></li>
				<li><spring:message code= "LB.Payment_P3_D2" /></li> 
				<li><spring:message code= "LB.Payment_P3_D3" /></li> 
			</ol>
		</div>
	</c:if>
</div>
</body>
</html>