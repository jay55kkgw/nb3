<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 元件驗證身分JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdProcess.js"></script>
	<script type="text/javascript">
		var urihost = "${__ctx}";
	    $(document).ready(function () {
	    	// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 1000);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
	    	
	    	$("#CMSUBMIT").click(function(){
	    		var allCheck = true;
	    		
	    		$(".riskConfirmCheckBox").each(function(){
	    			if($(this).prop("checked") == false){
	    				allCheck = false;
	    			}
	    		});
	    		if(allCheck == true){
	    			processQuery(); 
	    		}
	    		else{
	    			errorBlock(
	    					null, 
	    					null,
	    					['<spring:message code="LB.Alert123" />'], 
	    					'<spring:message code="LB.Quit" />', 
	    					null
	    			);
	    		}
	    	
	    		
        });
	    	
	    $("#CMCANCEL").click(function(){
	    	fstop.getPage('${pageContext.request.contextPath}'+'/INDEX/index','', '');
	    });
	    
	 });
	    
	 function init(){
		// 判斷顯不顯示驗證碼
			chaBlock();
			// 交易機制 click
			fgtxwayClick();	
	 }
	 
	 function fgtxwayClick() {
	 	$('input[name="FGTXWAY"]').change(function(event) {
	 			// 判斷交易機制決定顯不顯示驗證碼區塊
	 		chaBlock();
		});
	}
	 	// 判斷交易機制決定顯不顯示驗證碼區塊
	function chaBlock() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			switch(fgtxway) {
			case '0':
				$('#CMPASSWORD').addClass("validate[required]")
				$("#chaBlock").hide();
				break;
			case '2':
				$('#CMPASSWORD').removeClass("validate[required]")
				$("#chaBlock").show();
			    break;
			    	
			default:
				$('#CMPASSWORD').removeClass("validate[required]")
				$("#chaBlock").hide();
			}
	 	}
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	
	// 交易機制選項
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		switch (fgtxway) {
				// IKEY
			case '1':
				useIKey();
				break;
				// 晶片金融卡
			case '2':
				useCardReader();
				break;
			default:
				//alert("<spring:message code= "LB.Alert001" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert001' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				unBlockUI(initBlockId);
			}
	}
	
	//IKEY簽章結束
	function uiSignForPKCS7Finish(result){
		var main = document.getElementById("main");
		
		//成功
		if(result != "false"){
			//SubmitForm();
			checkxmlcn();
		}
		//失敗
		else{
			alert("IKEY簽章失敗");
			ShowLoadingBoard("MaskArea",false);
		}
	}
	
	function checkxmlcn(){
		var cusidn = '${sessionScope.cusidn}';
		var jsondc = $('#jsondc').val();
		var pkcs7Sign = $('#pkcs7Sign').val();
		var bs64 = $('#bs64').val();
		var uri = "${__ctx}"+"/COMPONENT/ikey_without_login_aj";
		var rdata = { UID:cusidn, jsondc: jsondc, pkcs7Sign:pkcs7Sign ,bs64:bs64 };
		
		fstop.getServerDataEx(uri, rdata, true, IkeyCheckcallback);
	}
	
	function IkeyCheckcallback(response) {
		
		var checkflag = response.data.checkflag;
		if(!checkflag){
			alert(response.data.msgCode +" : "+ response.data.msgName);
			ShowLoadingBoard("MaskArea",false);
			return ; 
		}
		if(checkflag.indexOf("SUCCESSFUL")>-1){
			initBlockUI();
            $("#formId").submit();
		}else{
			alert("<spring:message code= 'LB.X2287' />");
			ShowLoadingBoard("MaskArea",false);
		}
	}
	
	//取得卡片主帳號結束
	function getMainAccountFinish(result){
		//成功
		if(result != "false"){
			var main = document.getElementById("formId");
			main.ACNNO.value = result;
			var cardACN = result;
			if(cardACN.length > 11){
				cardACN = cardACN.substr(cardACN.length - 11);
			}
			
//			var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//			var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
			var cusidn = '${sessionScope.cusidn}';
			var uri =  "${__ctx}"+"/COMPONENT/component_checkid_aj";
			var rdata = { CUSIDN: cusidn, ACN: cardACN };
			
			fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
		}
		//失敗
		else{
			showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
		}
	}
</script>
</head>
<body>
	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
			 <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2567" /></li>
		</ol>
	</nav>


	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
                <form id="formId" method="post" action="${__ctx}/BOND/DEAL/bond_sign_result">
                <input type="hidden" name="TXTOKEN" value="${transfer_data.data.TXTOKEN}">
                <!-- 			晶片金融卡及IKEY  -->
				<input type="hidden" id="jsondc" name="jsondc" value='${transfer_data.data.jsondc}'>
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" id="ISSUER" 	name="ISSUER" />
				<input type="hidden" id="ACNNO" 	name="ACNNO" />
				<input type="hidden" id="TRMID" 	name="TRMID" />
				<input type="hidden" id="iSeqNo" 	name="iSeqNo" />
				<input type="hidden" id="ICSEQ" 	name="ICSEQ" />
				<input type="hidden" id="ICSEQ1" 	name="ICSEQ1" />
				<input type="hidden" id="TAC" 		name="TAC" />
				<input type="hidden" id="CHIP_ACN"  name="CHIP_ACN" />
				<input type="hidden" id="OUTACN"	name="OUTACN">
				<input type="hidden" id="bs64"	name="bs64" value="Y">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        	<div style="margin:0% 5% 0% 5%">
								<div class="NA01_3">
								<div class="head-line">臺灣中小企業銀行受託投資海外債券特別約定事項暨同意書</div>
								<br>
								<p align="left"><font size="3">立約人(以下稱委託人)業已簽訂「臺灣中小企業銀行特定金錢信託投資國內外有價證券信託約定書」 
								(以下稱信託約定書），今委託人為另指定投資標的，委託臺灣中小企業銀行（以下稱受託人）辦理特定金錢信託投資海外債券，特簽訂本特別約定事項暨同意書(以下稱同意書），並願遵守下列約定：
								</font></p>
								<table width="100%" class="DataBorder"  style='font-family:標楷體'>
									<tr>
									<td width="10%">
									  <p>第一條</p>
									</td>
									<td> 
										<p>下單指示與信託金額圈存
										<br>委託人同意受託人於約定之服務時間內依其下單指示運用信託資金於海外債券，並以圈存申購方式辦理存款帳戶扣款相關事宜。下單日為實際交易日(T日)，<b>受託人經委託人下單指示申購後當日辦理圈存，圈存後之申購款項無法動用但仍照常計息。</b>除海外債券商品說明書另有約定外，申購款項解圈扣款日為下單日之次一受託人營業日 (T+1日，遇例假日順延)。
										<br>因海外債券之交易市場休市或其他因素致不能交易時，委託人同意受託人依第五條及第六條約定方式處理已圈存之申購金額、成交及贖回單位之分配。委託人並同意受託人得因實際市場交易作業需要，彈性規定下單指示的日期與時間。</p>
									</td>
									</tr>
									<tr>
										<td>
											<p>第二條</p>
										</td>
										<td>
											<p>成交價格
											<br>委託人同意申購/贖回海外債券之成交價格如下：
											<br>一、	委託人同意於下單日以受託人當日選定之每單位交易價格為申購/贖回海外債券之每單位預約價格，如受託人所選定之交易價格非為單一價格，委託人應於其中擇一為每單位預約價格，惟受託人不確保委託人所選擇之預約價格必然成交，亦不確保成交價格為實際交易日之最低價或最高價。
											<br>二、	<b>委託人瞭解並同意實際成交價格可能因市場變動而與受託人提供之參考報價不同，實際成交價格及金額須以海外債券之交易對手提供交易確認單為準。</b>
											<br>三、 委託人同意申購/贖回海外債券之成交價格如下：
											<br>1、申購之成交價格須低於或等於委託人每單位預約價格。
											<br>2、贖回之成交價格須高於或等於委託人每單位預約價格。
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p><b>第三條</b></p>
										</td>
										<td>
											<p><b>信託費用
											<br>信託費用之計收標準如下：
											<br>一、申購手續費：最高費率1.5%，於申購時依信託投資金額乘以申購手續費率計收。
											<br>二、信託管理費：年化費率0.2％，於贖回時依贖回信託金額X 0.2% X債券持有期間計收(未滿一年部分依實際天數計算)，並於信託存續期間屆滿或提前終止或贖回時，就應返還之信託本益中扣收。
											<br>三、通路服務費：費率最高0.5％(年化費率)，依申購面額x通路服務費率x債券剩餘年期計收(未滿一年部分依實際天數計算)。
											</b></p>
										</td>
									</tr>
									<tr>
										<td>
											<p>第四條</p>
										</td>
										<td>
											<p>最低信託金額
											<br>每筆最低申購面額及最低累加單位依各海外債券之商品說明暨風險預告書而定。
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>第五條</p>
										</td>
										<td>
											<p>成交單位分配
											<br><b>受託人不確保委託人申購交易一定成交，若全部交易無法順利成交，委託人同意受託人逕予辦理解除圈存且不予以扣款，委託人應再提出申購申請。</b>
											<br>
											申購成交單位數之確認，將依海外債券交易對象所訂之交易確認書日數加計合理作業時間為準，如遇國內、外市場休市或其他因素則將順延。
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>第六條</p>
										</td>
										<td>
											<p>贖回單位分配
											<br><b>受託人不確保委託人申請贖回交易一定成交，若該贖回交易無法順利成交，視為委託人撤回贖回申請；委託人應再提出贖回申請。</b>
											<br>贖回撥款將依海外債券之交易對象所訂給付贖回價金日數加計合理作業時間為準，如遇國內、外市場休市或其他因素則將順延。
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>第七條</p>
										</td>
										<td>
											<p>贖回限制
											<br>委託人同意申購海外債券後，須待受託人完成申購單位數分配後，始能提出贖回申請，且贖回交易依投資標的規定有最低贖回面額及餘額之限制者，應符合其規定。
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>第八條</p>
										</td>
										<td>
											<p>交易取消限制
											<br>委託人同意於向受託人申購/ 贖回海外債券後，<b>一經受託人下單確認成交即不得要求取消交易。</b>
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p><b>第九條</b></p>
										</td>
										<td>
											<p><b>轉換標的限制
											<br>受託人不受理委託人申請海外債券之轉換交易。</b>
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>第十條</p>
										</td>
										<td>
											<p>委託人身分
											<br>美國公民、美國居民或具有美國永久居留權者不得為本項業務之委託人。委託人承諾若嗣後持有美國籍身分，應立即指示受託人贖回全部已投資標的。委託人如未盡上述義務，應自負一切責任，且受託人得逕行終止信託關係，如因而導致受託人遭受任何損害或有損害之虞，一經受託人請求，委託人應立即賠償或處理。
											<br>如投資標的之發行機構有限制委託人不得具有特定國籍別之身分；或債券之銷售條件有限制專業投資人始得投資者，亦應符合。
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p><b>第十一條</b></p>
										</td>
										<td>
											<p><b>商品適合度
											<br>受託人受託辦理海外債券交易，已依委託人簽立本同意書時之風險承受等級與商品風險等級適合度進行評估，委託人應留意投資標的之最新訊息，自行研判是否繼續投資。
											</b></p>
										</td>
									</tr>
									<tr>
										<td>
											<p>第十二條</p>
										</td>
										<td>
											<p>受託投資遵循事項
											<br>受託人受託投資海外債券，除委託人為專業投資人且相關法規另有規定外，受託人應辦理下列事項：
											<br>一、<b>提供商品說明暨風險預告書並由專人對委託人解說依法應揭露事項。</b>
											<br>二、依「金融消費者保護法」之金融消費爭議處理機制處理金融交易爭議事件。
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>第十三條</p>
										</td>
										<td>
											<p>稅捐
											<br>委託人辦理海外債券之信託投資所生之稅捐，悉依中華民國稅法及相關法令規定辦理；如投資標的所在國家或地區另有規定者，從其規定。
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>第十四條</p>
										</td>
										<td>
											<p>同意書效力
											<br>本同意書為信託約定書之ㄧ部分。除本同意書另有約定外，信託約定書及其他附件其他條款不變，且繼續有效。本同意書與信託約定書之約定其相互間如有牴觸或矛盾者，應優先適用本同意書之約定。
											<br>本同意書未盡事宜，悉依中華民國有關法令規定、同業公會所訂相關自律規範、投資標的公開說明書、投資人須知、商品說明書或其他委託人與受託人間之書面協議辦理。
											</p>
										</td>
									</tr>
								</table>
								<br>
								<label class="check-block">
								<input type="checkbox" class="riskConfirmCheckBox" id="allCheckBox"/>
<!-- 								本人已閱讀並充分了解「特別約定事項暨同意書」所載之內容 -->
									<spring:message code="LB.X2568" />
								<span class="ttb-check"></span>
								</label>
								<br>
								</div>
							 </div>
							 <div class="ttb-input-block">
							 <div class="ttb-input-item row">
							 	<span class="input-title"> 
									<h4><spring:message code="LB.Transaction_security_mechanism" />：</h4>
								</span>
								<span class="input-block">
								<c:if test = "${sessionScope.isikeyuser}">
								<div class="ttb-input">
									<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> <spring:message code="LB.Electronic_signature" />
										<input type="radio" id="CMIKEY" name="FGTXWAY" value="1"> 
										<span class="ttb-radio"></span>
										</label> <!-- 電子簽章(請載入載具i-key) --> 
									</div>
									</span>
								</div>
								</c:if>
								<div class="ttb-input">
									<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
								        <input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked>
								        <spring:message code="LB.Financial_debit_card" />
										<span class="ttb-radio"></span>
										</label>
									</div>						           	
									</span>
								</div>
								</span>
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
								<!-- 驗證碼 -->
								<span class="input-title">
									<label>
										<h4><spring:message code= "LB.Captcha" /></h4>
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text"
										class="text-input" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class = "verification-img" src="" />
									<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
										onclick="changeCode()" value="<spring:message code= "LB.Regeneration" />" />
								</span>
							</div> 
							 </div>
							 </div>
							 
							 
							<input type="button" id="CMCANCEL" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>
							<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
			               
                        </div>
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>