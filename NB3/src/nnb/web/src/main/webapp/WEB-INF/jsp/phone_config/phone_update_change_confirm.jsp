<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
	<script type="text/javascript">
	var urihost = "${__ctx}";
	$(document).ready(function() {
		// HTML載入完成後0.1秒開始遮罩
		setTimeout("initBlockUI()", 100);
		initFootable(); // 將.table變更為footable 
		init();
		
		// 過0.5秒解遮罩
		setTimeout("unBlockUI(initBlockId)", 800);
	});
	
	/**
	 * 初始化BlockUI
	 */
	function initBlockUI() {
		initBlockId = blockUI();
	}
	
    function init(){
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});

		changeCode();
		// 交易類別change事件
		changeFgtxway();
		// 判斷顯不顯示驗證碼
		chaBlock();

		//上一頁按鈕
		$("#pageback").click(function() {

			var action = '${pageContext.request.contextPath}' + '${previous}'
			$('#back').val("Y");
			$('#jsondc').attr('disabled', true);
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").validationEngine('detach');
			$("#formId").submit();

		});
		
    	$("#CMSUBMIT").click(function(e){			
			console.log("submit~~");
	
			if (!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
// 				$('#HLOGINPIN').val(pin_encrypt($('#LOGINPIN').val()));
 				console.log("submit~~");
 				$("#formId").validationEngine('detach');
 				initBlockUI();
   				var action = '${__ctx}/PNONE/CONFIG/update_change_result';
   				$('#formId').attr("action", action);
    			unBlockUI(initBlockId);
    			processQuery();
    			$('#LOGINPIN').val("");
    			$('#CKLOGINPIN').val("");
    			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
 			}
		});
    }	
    
    
	/**************************************/
	/*          	驗證碼驗證   		          */
	/**************************************/
	// 刷新驗證碼
	function refreshCapCode() {
		console.log("refreshCapCode...");

		// 驗證碼
		$('input[name="capCode"]').val('');
		
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 刷新輸入欄位
	function changeCode() {
		console.log("changeCode...");
		
		// 清空輸入欄位
		$('input[name="capCode"]').val('');
		
		// 刷新驗證碼
		refreshCapCode();
	}

	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}

	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			refreshCapCode();
		});
	}
		
	function hideNpcDiv(flag){
		console.log(flag);
		if(flag == 'Y'){
			$("#NPCDIV").show();
		}else{
			$("#NPCDIV").hide();
		}
	}


		// 交易機制選項
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			switch (fgtxway) {
				case '1':
					// IKEY
					useIKey();
					break;
				case '2':
					// 晶片金融卡
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
			    	break;
				case '7':
					// IDGATE認證
				    idgatesubmit= $("#formId");
				    showIdgateBlock();
				    break;
				default:
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				
			}
		}
		//交易類別change 事件
		function changeFgtxway() {
			$('input[type=radio][name=FGTXWAY]').change(function () {
				console.log(this.value);
				if (this.value == '0') {
					$("#CMPASSWORD").addClass("validate[required]");
				} else if (this.value == '1') {
					$("#CMPASSWORD").removeClass("validate[required]");
				}
				chaBlock();
			});
		}
	 	// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
				console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			if(fgtxway == '2'){
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").show();
			}else{
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").hide();
			}
	 	}
		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}
	</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	<!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 手機門號收款帳號設定    -->
			<li class="ttb-breadcrumb-item active" aria-current="page">手機門號收款帳號設定</li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		
		<c:if test="${sessionScope.cusidn != null}">
			<%@ include file="../index/menu.jsp"%>
		</c:if>
		
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					變更轉入帳號
				</h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
<!-- 				<div id="step-bar"> -->
<!-- 					<ul> -->
<%-- 						<li class="finished"><spring:message code="LB.Enter_data" /></li> --%>
<%-- 						<li class="active"><spring:message code="LB.Confirm_data" /></li> --%>
<%-- 						<li class="">  <spring:message	code="LB.Transaction_complete" /></li> --%>
<!-- 					</ul> -->
<!-- 				</div> -->
				<form method="post" id="formId">
					<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
					<input type="hidden" name="CHIP_ACN" id="CHIP_ACN" value="">
					<input type="hidden" name="UID" id="UID" value="">
					<input type="hidden" name="ACN" id="ACN" value="">
					<input type="hidden" name="ISSUER" id="ISSUER" value="">
					<input type="hidden" name="OUTACN" id="OUTACN" value="">
					<input type="hidden" name="TRANSEQ" id="TRANSEQ" value="2500">
					<input type="hidden" name="IP" value="${transfer_data.data.IP}">
					<input type="hidden" name="TXTOKEN" id="TXTOKEN" value="${transfer_data.data.TXTOKEN}">
					<input type="hidden" name="txacn" id="txacn" value="${transfer_data.data.txacn}">
					<input type="hidden" name="oldbind" id="oldbind" value="${result_data.data.oldbind}">
					<input type="hidden" name="oldtxacn" id="oldtxacn" value="${transfer_data.data.oldtxacn}">
					<input type="hidden" name="mobilephone" id="mobilephone" value="${transfer_data.data.mobilephone}">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					<input type="hidden" name="binddefault" id="binddefault" value="${result_data.data.binddefault}">
					
					<!-- 交易機制所需欄位 -->
					<input type="hidden" id="jsondc" name="jsondc" value='${transfer_data.data.jsondc}'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				    <input type="hidden" id="PINNEW" name="PINNEW" value="">
					
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
							
							<div class="ttb-message">
								<span>
								<!-- 請確認變更資料 -->
								<spring:message code="LB.Confirm_update_data"/>
								</span>
							</div>
							<!-- 手機號碼 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
<%-- 											手機號碼 --%>
手機號碼
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.mobilephone}
										</div>
									</span>
								</div>
							</div>
							
							<!-- 綁定轉入帳號 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											綁定收款帳號
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.txacn}
										</div>
									</span>
								</div>
							</div>
							
							<!-- 同意作為預設收款帳戶 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>同意作為預設收款帳號</h4>
										</label>
									</span>
									<span class="input-block">
										<c:if test="${transfer_data.data.binddefault == 'Y'}">
											<div class="ttb-input">
											<!-- 是 -->
												是
											</div>
										</c:if>
										<c:if test="${transfer_data.data.binddefault == 'N'}">
											<div class="ttb-input">
											<!-- 否-->
												否
											</div>
										</c:if>
									</span>
								</div>
							</div>
							
							
							<div class="ttb-input-block">
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
										</label>
									</span>
									<span class="input-block">
								
										
									<c:if test = "${sessionScope.isikeyuser}">
									
										<!-- 電子簽章(請載入載具i-key) -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</c:if>	
										
										<!-- 裝置推播認證 -->
										<div class="ttb-input" name="idgate_group" style="display:none">		  
											<label class="radio-block">
												裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
												<input type="radio" id="IDGATE" name="FGTXWAY" value="7">
												<span class="ttb-radio"></span>
											</label>		 
										</div>
										
										<!-- 晶片金融卡 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Financial_debit_card" />
												<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</span>
								</div>
								
								<!-- 如果是非約定則不能使用IKEY，只能選晶片金融卡，使用者選擇晶片金融卡後才顯示驗證碼-->
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
									<!-- 驗證碼 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Captcha" /></h4>
										</label>
									</span>
									<span class="input-block">
	                                    <div class="ttb-input">
	                                        <img name="kaptchaImage" class="verification-img" src="" align="top" id="random" />
	                                        <button type="button" class="btn-flat-gray" name="reshow" onclick="changeCode()">
												<spring:message code="LB.Regeneration" /></button>
	                                    </div>
	                                    <div class="ttb-input">
	                                        <input id="capCode" name="capCode" type="text"
												class="text-input" maxlength="6" autocomplete="off">
	                                        <br>
	                                        <span class="input-unit"><spring:message code="LB.Captcha_refence" /></span>
	                                    </div>
	                                </span>
								</div>
							</div>
							<input type="button" id="pageback" class="ttb-button btn-flat-gray" value="回上頁"/>
							<!-- 確定 -->
							<input type="button" id="CMSUBMIT" value="確認" class="ttb-button btn-flat-orange"/>
						
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>