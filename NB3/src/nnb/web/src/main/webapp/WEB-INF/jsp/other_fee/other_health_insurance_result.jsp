<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 健保費代扣繳申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0700" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.W0700" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form method="post" id="formId">
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${input_data.TOKEN}" /><!-- 防止重複交易 -->
                
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
                                <span><spring:message code="LB.D0181" /></span>
                            </div>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.D1097" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${ result_data.data.CMQTIME }</span>
									</div>
								</span>
							</div>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.W0702" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${ result_data.data.TSFACN }</span>
									</div>
								</span>
							</div>
							
							<!-- 台幣轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.W0716" />
									</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<span>
											<c:if test="${ !result_data.data.TYPNUM_str.equals('') }">
												<spring:message code="${ result_data.data.TYPNUM_str }" />
											</c:if>
										</span>
									</div>
								</span>
							</div>
							
							<!-- 台幣轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.W0706" />
									</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<span>${ result_data.data.UNTNUM1 }</span>
									</div>
								</span>
							</div>
							
							<!-- 台幣轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.W0708" />
									</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<span>${ result_data.data.CUSIDN1 }</span>
									</div>
								</span>
							</div>
							
							<!-- 台幣轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.W0709" />
									</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<span>${ result_data.data.CUSIDN6 }</span>
									</div>
								</span>
							</div>
							
						</div>
                        <!-- 列印  -->
                        <spring:message code="LB.Print" var="printbtn"></spring:message>
                        <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />					
					</div>
				</div>
			</form>
			    <!-- 		說明： -->
				<ol class="list-decimal description-list">
			    	<p><spring:message code="LB.Description_of_page"/></p>
	            	<li><spring:message code="LB.Other_Health_Insurance_P4_D1" /></li>
	            	<li><spring:message code="LB.Other_Health_Insurance_P4_D2" /></li>
	                <li><spring:message code="LB.Other_Health_Insurance_P4_D3" /></li>
			    </ol>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
		
	    function init(){
	    	//列印
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"other_health_insurance_result_print",
					"jspTitle":'<spring:message code= "LB.W0700" />',
					"CMQTIME":"${result_data.data.CMQTIME}",
					"TSFACN":"${input_data.TSFACN}",
					"TYPNUM":"${result_data.data.TYPNUM_str}",
					"UNTNUM1":"${result_data.data.UNTNUM1}",
					"CUSIDN1":"${input_data.CUSIDN1}",
					"CUSIDN6":"${result_data.data.CUSIDN6}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
	    }	
	    

 	</script>
</body>
</html>
 