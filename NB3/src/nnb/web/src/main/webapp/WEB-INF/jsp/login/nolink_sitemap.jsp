<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>

	<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
	<title>Insert title here</title>

</head>

<body>
	<div>
		<!-- ${ menuFilterList.data.sort_menu_only_name[0].SEED_LAYER } -->
		<fmt:setLocale value="${__i18n_locale}" />
		<!-- 直接抓table，如menu.jsp功能選單 -->
		<tr>
			<td>
				<ul>
					<li>
						<div>
							<spring:message code="LB.Account_Overview" />
						</div>
						<!-- 帳戶總覽 -->
						<div>
							<spring:message code="LB.Account_Overview" />
						</div>
						<!-- 我的首頁 -->
						<div>
							<spring:message code="LB.My_home" />
						</div>
					</li>
				</ul>
			</td>

			<td>
				<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu_only_name[0].SEED_LAYER }">
					<div>
						<ul>
							<li>
								<c:if test="${ dataListLayer1.ADOPALIVE.equals('1') && !dataListLayer1.DPACCSETID.equals('1100') }">
									<c:if test="${__i18n_locale eq 'en' }">
										${dataListLayer1.ADOPENGNAME}
									</c:if>
									<c:if test="${__i18n_locale eq 'zh_TW' }">
										${dataListLayer1.ADOPNAME}
									</c:if>
									<c:if test="${__i18n_locale eq 'zh_CN' }">
										${dataListLayer1.ADOPCHSNAME}
									</c:if>
								</c:if>
							</li>
							<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }">
								<ul>
									<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }">
										<c:if test="${ dataListLayer3.ADOPALIVE.equals('1') && dataListLayer3.ISANONYMOUS.equals('0') }">
											<li>
												<c:if test="${__i18n_locale eq 'en' }">
													${dataListLayer3.ADOPENGNAME}
												</c:if>
												<c:if test="${__i18n_locale eq 'zh_TW' }">
													${dataListLayer3.ADOPNAME}
												</c:if>
												<c:if test="${__i18n_locale eq 'zh_CN' }">
													${dataListLayer3.ADOPCHSNAME}
												</c:if>														
											</li>
										</c:if>
									</c:forEach>
								</ul>
							</c:forEach>
						</ul>
					</div>
				</c:forEach>
			</td>

			<td>
				<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu_only_name[0].SEED_LAYER }">
					<div>
						<ul>
							<li>
								<c:if test="${ dataListLayer1.ADOPALIVE.equals('0') && dataListLayer1.ISANONYMOUS.equals('1') }">
									<c:if test="${__i18n_locale eq 'en' }">
										${dataListLayer1.ADOPENGNAME}
									</c:if>
									<c:if test="${__i18n_locale eq 'zh_TW' }">
										${dataListLayer1.ADOPNAME}
									</c:if>
									<c:if test="${__i18n_locale eq 'zh_CN' }">
										${dataListLayer1.ADOPCHSNAME}
									</c:if>
								</c:if>
							</li>
							<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }">
								<ul>
									<li>
										<c:if test="${ dataListLayer2.ADOPALIVE.equals('1') && dataListLayer2.ISANONYMOUS.equals('1') }">
											<c:if test="${__i18n_locale eq 'en' }">
												${dataListLayer2.ADOPENGNAME}
											</c:if>
											<c:if test="${__i18n_locale eq 'zh_TW' }">
												${dataListLayer2.ADOPNAME}
											</c:if>
											<c:if test="${__i18n_locale eq 'zh_CN' }">
												${dataListLayer2.ADOPCHSNAME}
											</c:if>
										</c:if>
									</li>
									<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }">
										<c:if test="${ dataListLayer3.ADOPALIVE.equals('1') && dataListLayer3.ISANONYMOUS.equals('1') }">
											<li>
												<c:if test="${__i18n_locale eq 'en' }">
													${dataListLayer3.ADOPENGNAME}
												</c:if>
												<c:if test="${__i18n_locale eq 'zh_TW' }">
													${dataListLayer3.ADOPNAME}
												</c:if>
												<c:if test="${__i18n_locale eq 'zh_CN' }">
													${dataListLayer3.ADOPCHSNAME}
												</c:if>														
											</li>
										</c:if>
									</c:forEach>
								</ul>
							</c:forEach>
						</ul>
					</div>
				</c:forEach>
			</td>
		</tr>
	</div>

</body>

</html>