<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			init();
			datetimepickerEvent();
			getTmr();
			// 初始化時隱藏span
			$("#hideblock_CMSDATE").hide();
			$("#hideblock_CMEDATE").hide();
		});

		function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			//btn 
			$("#CMSUBMIT").click(function (e) {
				e = e || window.event;

// 				if(checkTimeRange() == false )
// 				{
// 					return false;
// 				}
				//打開驗證隱藏欄位
				$("#hideblock_STARTSERACHDATE").show();
				$("#hideblock_ENDSERACHDATE").show();
				//塞值進span內的input
				$("#validate_STARTSERACHDATE").val($("#STARTSERACHDATE").val());
				$("#validate_ENDSERACHDATE").val($("#ENDSERACHDATE").val());
				console.log("submit~~");
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").attr("action", "${__ctx}/PARKING/FEE/parking_withholding_query_result");
					$("#formId").submit();
				}
			});

    		//上一頁按鈕
    		$("#CMBACK").click(function() {
				$("#formId").validationEngine('detach');
    			var action = '${__ctx}/PARKING/FEE/parking_withholding';
    			$('#back').val("Y");
    			$("#formId").attr("action", action);
//     			initBlockUI();
    			$("#formId").submit();
    		});
		} // init END
		// 日曆欄位參數設定
		function datetimepickerEvent() {
			$(".STARTSERACHDATE").click(function (event) {
				$('#STARTSERACHDATE').datetimepicker('show');
			});
			$(".ENDSERACHDATE").click(function (event) {
				$('#ENDSERACHDATE').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
		//預約自動輸入今天
		function getTmr() {
			var today = new Date("${result_data_p018.data.CMQTIME}");
			today.setDate(today.getDate());
			var y = today.getFullYear();
			var m = today.getMonth() + 1;
			var d = today.getDate();
			if(m<10){
				m = "0"+m
			}
			if(d<10){
				d="0"+d
			}
			var tmr = y + "/" + m + "/" + d
			$('#STARTSERACHDATE').val(tmr);
			$('#ENDSERACHDATE').val(tmr);
			$("#baseDate").val(tmr);
		}
		
		function checkTimeRange()
		{
			var now =new Date("${result_data_p018.data.CMQTIME}");
			var twoYm = 63115200000;
			var twoMm = 5259600000;
			var oneYm = 31622400000;
			
			
			var startT = new Date( $('#STARTSERACHDATE').val() );
			var endT = new Date( $('#ENDSERACHDATE').val() );
			var NSDistance = now - startT;
			var range = endT - startT;
			var NEDistance = now - endT;
			
			
			var limitS = new Date(now - oneYm + 86400000);
			if(NSDistance >= oneYm){
				var m = limitS.getMonth() + 1;
				var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
				// 起始日不能小於
				var msg = '<spring:message code="LB.Start_date_check_note_1" />' + time;
				//alert(msg);
				errorBlock(
						null, 
						null,
						[msg], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
			}
			else if(NSDistance < 0){
				var m = now.getMonth() + 1;
				var time = now.getFullYear() + '/' + m + '/' + now.getDate();
				// 起始日不能大於
				var msg = '<spring:message code= "LB.X1472" />' + time;
				//alert(msg);
				errorBlock(
						null, 
						null,
						[msg], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
			}
			else{
				if(range < 0){
					var msg = '<spring:message code= "LB.X1193" />';
					//alert(msg);
					errorBlock(
						null, 
						null,
						[msg], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
					return false;
				}else if(NEDistance < 0){
					var m = now.getMonth() + 1;
					var time = now.getFullYear() + '/' + m + '/' + now.getDate();
					// 終止日不能大於
					var msg = '<spring:message code="LB.End_date_check_note_1" />' + time;
					//alert(msg);
					errorBlock(
						null, 
						null,
						[msg], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
					return false;
				}
			}
			
			return true;
		}
		
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 停車費     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.D0330" /></li>
    <!-- 繳費查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0335" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--商業信用狀明細(未輸入) -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.D0335"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<form id="formId" method="post">
                	<input type="hidden" id="back" name="back" value="">
                    <input type="hidden" name="StartRecordNo" value="1" />
                    <input type="hidden" name="MaxRecordsPerTime" value="20" />
                    <input type="hidden" name="QueryTerms" value="0" />
				   	<input  type="hidden" name="CarId" value=" ">
				    <input  type="hidden" name="Account" value=" ">
				    <input  type="hidden" name="QueryTerms_Str" value="All">
					
                	<input type="hidden" id="baseDate" name="baseDate" value="">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								
	                             <!--扣款帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title"><label>
	                                        <h4><spring:message code="LB.Inquiry_time"/></h4>
	                                    </label></span>
	                                <span class="input-block">
	                                        <div class="ttb-input">
												${result_data_p018.data.CMQTIME}
	                                       </div>
									</span>
								</div>
								
	                             <!--扣款帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title"><label>
	                                        <h4><spring:message code="LB.Inquiry_type"/></h4>
	                                    </label></span>
	                                <span class="input-block">
	                                        <div class="ttb-input">
												<spring:message code="LB.D0336"/>
	                                       </div>
									</span>
								</div>
								
	                             <!--扣款帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title"><label>
	                                        <h4><spring:message code="LB.D0059"/></h4>
	                                    </label></span>
	                                <span class="input-block">
	                                        <div class="ttb-input">
											<select name="ZoneCode" id="ZoneCode" class="custom-select select-input half-input">
												<option value="   " selected><spring:message code="LB.All"/></option>
												<c:forEach var="dataList" items="${result_data_p018.data.REC_sort}">
													<option value="${dataList.ZoneCode}"> ${dataList.ZoneName}</option>
												</c:forEach>
											</select>
	                                       </div>
									</span>
								</div>
								<!-- 查詢區間區塊 -->
								<div class="ttb-input-item row">
									<!--查詢區間  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Inquiry_period" />
										</label>
									</span>
									<span class="input-block">
										<!--  指定日期區塊 -->
										<div class="ttb-input">
											<!--期間起日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.D0013" />
												</span>
												<input type="text" id="STARTSERACHDATE" name="STARTSERACHDATE" class="text-input datetimepicker" value="" />
												<span class="input-unit STARTSERACHDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_STARTSERACHDATE" >
												<!-- 驗證用的input -->
												<input id="validate_STARTSERACHDATE" name="validate_STARTSERACHDATE" type="text" class="text-input validate[required, verification_date[validate_STARTSERACHDATE]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
											<!--期間迄日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_end_date" />
												</span>
												<input type="text" id="ENDSERACHDATE" name="ENDSERACHDATE" class="text-input datetimepicker" value="" />
												<span class="input-unit ENDSERACHDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_ENDSERACHDATE" >
												<!-- 驗證用的input -->
												<input id="validate_ENDSERACHDATE" name="validate_ENDSERACHDATE" type="text" class="text-input validate[required, verification_date[validate_ENDSERACHDATE]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_DATE" >
												<input id="validate_DATE" name="validate_DATE" type="text" class="text-input
													validate[funcCallRequired[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', baseDate, STARTSERACHDATE, ENDSERACHDATE, false, 6, null]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
										</div>
									</span>
								</div>
								<!-- 信用狀號碼區塊-->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title"> -->
<!-- 										<label> -->
<!-- 											信用狀號碼  -->
<!-- 											信用狀號碼 -->
<!-- 										</label> -->
<!-- 									</span> -->
<!-- 									<input type="text" id="LCNO" name="LCNO" class="text-input" maxlength="20" value="" /> -->
<!-- 										&nbsp;&nbsp;&nbsp;&nbsp; -->
<!-- 									<label> -->
<!-- 										空白表示查詢全部 -->
<!-- 									</label> -->
<!-- 								</div> -->
							</div>
							<!-- 網頁顯示 button-->
							
	                            <!--回上頁 -->
	                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
	                            <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
								<!--網頁顯示 -->
								<spring:message code="LB.Display_as_web_page" var="cmSubmit"></spring:message>
								<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
								<!-- 重新輸入 -->
<%-- 								<spring:message code="LB.Re_enter" var="cmRest"></spring:message> --%>
<%-- 								<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-orange"> --%>
							
						</div>
					</div>
					<div class="text-left">
						<!-- 		說明： -->
						<ol class="list-decimal description-list">
							<p><spring:message code="LB.Description_of_page"/></p>
							<li><spring:message code= "LB.parking_withholding_query_P2_D1" /></li>
						</ol>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>