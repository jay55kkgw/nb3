<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title></title>

	<Script language="JavaScript">
		//列印不顯示按鈕
		function print_the_page() {
			//先回到頂端再列印
			document.body.scrollTop = document.documentElement.scrollTop = 0;
			document.getElementById("CMPRINT").style.visibility = "hidden";    //顯示按鈕
			javascript: print();
			document.getElementById("CMPRINT").style.visibility = "visible";   //不顯示按鈕
		}
	</Script>

	<style>
		.watermark.zin:before {
			z-index: -1;
		}
	</style>

</head>

<body class="bodymargin watermark zin" style="-webkit-print-color-adjust: exact">

	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">


		</font>
	</div>
	<br />
	<table class="print">
		<tr>
			<!-- 列印時間 -->
			<td class="ColorCell" width="13%">
				<spring:message code="LB.X0040" />
			</td>
			<td class="DataCell0" colspan="7">
				${fxcertpreview.data.curentDateTime}
			</td>
		</tr>
		<tr>
			<!-- 統編/身分證號 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.X0041" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.CUSIDN}
			</td>
			<!-- 姓名 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.D0203" />
			</td>
			<td class="DataCell1" colspan="4">
				${fxcertpreview.data.NAME}
			</td>
		</tr>
		<tr>
			<!--解款日期 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.X2174"/>
			</td>
			<td class="DataCell0" colspan="5">
				${fxcertpreview.data.currentDate}
			</td>
			<!--國外匯款銀行資料 -->
			<td class="ColorCell" rowspan="2" width="9%">
				<spring:message code="LB.X2175" />
			</td>
			<td class="DataCell0" rowspan="2" width="22%">
				SWIFT CODE：
				${fxcertpreview.data.M1SBBIC}
				<br>
				<!-- 名稱/地址：  -->
				<spring:message code="LB.X0037" />
				：
				<br>
				${fxcertpreview.data.RCVBANK}
				<br>
				${fxcertpreview.data.RCVAD1}
				<br>
				${fxcertpreview.data.RCVAD2}
				<br>
				${fxcertpreview.data.RCVAD3}
			</td>
		</tr>
		<tr>
			<!-- 交易序號 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.Transaction_Number" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.STAN}
			</td>
			<!-- 銀行交易編號 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.X0042" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.REFNO}
			</td>
		</tr>
		<tr>
			<!--解款帳號 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0329" />
			</td>
			<td class="DataCell0" colspan="2">
				${fxcertpreview.data.BENACC}
			</td>
			<!--解款金額-->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.W0330" />
				<br>
				<font color="red">(<spring:message code="LB.X2180"/>)</font>
			</td>
			<td class="DataCell0" colspan="2">
				${fxcertpreview.data.PMTCCY}
				${fxcertpreview.data.str_PmtAmt}
			</td>
			<td class="ColorCell" rowspan="4" width="9%">
				<spring:message code="LB.X2176" />
			</td>
			<td class="DataCell0" rowspan="4" width="22%">
			<!-- 名稱/地址 -->
				<spring:message code="LB.W0295" />
				：<br>
				${fxcertpreview.data.ORDNAME}
				<br>
				${fxcertpreview.data.ORDAD1}
				<br>
				${fxcertpreview.data.ORDAD2}
				<br>
				${fxcertpreview.data.ORDAD3}
			</td>
		</tr>
		<tr>
			<!-- 國外匯款帳號 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.X2177" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.ORDACC}
			</td>
			<!--匯款金額-->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.W0150" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.ORGCCY}
				${fxcertpreview.data.str_OrgAmt}
			</td>
		</tr>
		<tr>
			<!-- 匯率-->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.Exchange_rate" />
			</td>
			<td class="DataCell0" colspan="2">
				${fxcertpreview.data.str_Rate}
			</td>
			<!--議價編號-->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.D0471" />
			</td>
			<td class="DataCell0" colspan="2">
				${fxcertpreview.data.BGROENO}
			</td>
		</tr>
		<tr>
			<!--  匯款人身份別-->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.X0058" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.display_BENTYPE}
			</td>
			<!--匯款地區國別 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.X2178" />
			 </td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.CNTRY}
			</td>
		</tr>
		<tr>
			<!-- 匯款分類項目 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0298" />
			</td>
			<td class="DataCell0" colspan="5">
				${fxcertpreview.data.SRCFUNDDESC}
			</td>
			<td class="ColorCell" rowspan="5" width="9%"></td>
			<td class="DataCell0" rowspan="5" width="22%"></td>
		</tr>
		<tr>
			<!-- 匯款分類說明 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0299" />
			</td>
			<td class="DataCell1" colspan="5">
				${fxcertpreview.data.FXRMTDESC}
			</td>
		</tr>
		<tr>
			<!-- 匯款附言 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0303" />
			</td>
			<td class="DataCell0" colspan="5">
				${fxcertpreview.data.str_ORDMEMO1}
			</td>
		</tr>
		<tr>
			<!-- 費用扣款帳號 -->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.W0305" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.BENACC}
			</td>
			<!-- 手續費負擔別 -->
			<td class="ColorCell" width="14%">
				<spring:message code="LB.W0155" />
			</td>
			<td class="DataCell1" colspan="2">
				${fxcertpreview.data.DETCHG}
			</td>
		</tr>
		<tr>
			<!-- 手續費-->
			<td class="ColorCell" width="11%">
				<spring:message code="LB.D0507" />
			</td>
			<td class="DataCell0" width="10%" colspan="5">
				${fxcertpreview.data.COMMCCY2}
				${fxcertpreview.data.str_CommAmt}
			</td>
		</tr>
	</table>

	<br />

	<div style="text-align:center">
		<spring:message code="LB.X1465" />
	</div>

	<div style="text-align:center">
		<input type="button" class="ttb-button btn-flat-orange" style="z-index:10000" id="CMPRINT"
			value="<spring:message code="LB.Print_thepage" />" onclick="print_the_page()" />
	</div>
	<br /><br /><br /><br />
</body>

</html>