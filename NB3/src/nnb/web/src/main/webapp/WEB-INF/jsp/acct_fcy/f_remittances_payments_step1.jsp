<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			setTimeout("initDataTable()",100);
			init();
		});
		function init() {
			//initFootable();	// 將.table變更為footable 
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			
			   //列印
	    	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"f_remittances_payments_step1_print",
					"jspTitle":'<spring:message code= "LB.W0317" />',
					"CMQTIME":"${f_remittances_payments_step1.data.CMQTIME}",
					"COUNT":"${f_remittances_payments_step1.data.COUNT}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
				
		}
		
			// 確認鍵 click
		function relieve(index, dt){
			var upDate = new Date(dt.substring(0,4) + '/' + dt.substring(4,6) + '/' + dt.substring(6,8));
			console.log(upDate);
			var nowDate = new Date();
			if(nowDate > upDate){
				// 塞資料
				$("#helperjson").val($("#"+index).val());
				if(!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				}else{
					$("#formId").validationEngine('detach');
					initBlockUI();//遮罩
					$("#formId").submit();
				}
			}else{
				//<!-- 未達生效日，無法進行解款 -->
				//alert('<spring:message code= "LB.Alert015" />');
				errorBlock(
						null, 
						null,
						['<spring:message code= 'LB.Alert015' />'], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
	 
			
			
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>			
	<!-- 匯入匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0125" /></li>
    <!-- 線上解款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0317" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--外匯匯入匯款線上解款 -->
				<h2>
					<spring:message code="LB.W0317" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/FCY/ACCT/ONLINE/f_remittances_payments_step2">
				<c:set var="BaseResultData" value="${f_remittances_payments_step1.data}"></c:set>
				<input type="hidden" id="helperjson" name="helperjson" value=''>
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
                    		<div class="col-12 tab-content">
									<ul class="ttb-result-list">
											<li>
												<h3><spring:message code="LB.Inquiry_time" /></h3>
												<p>${BaseResultData.CMQTIME}</p>
											</li>
											<li>
												<h3><spring:message code="LB.X0049" /></h3>
												<p>${BaseResultData.COUNT} <spring:message code="LB.Rows" /></p>
											</li>
										</ul>
										<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
											<thead>
												<tr>
													<!--匯入編號 -->
													<th data-title="<spring:message code="LB.X0050" />"><spring:message code="LB.X0050" /></th>
													<!--建檔日期 -->
													<th data-title="<spring:message code="LB.X0051" />"><spring:message code="LB.X0051" /></th>
													<!--幣別 -->
													<th data-title="<spring:message code="LB.Currency" />"><spring:message code="LB.Currency" /></th>
													<!--金額 -->
													<th data-title="<spring:message code= "LB.Deposit_amount_1" />"><spring:message code="LB.Deposit_amount_1" /></th>
													<!--匯款人 -->
													<th data-title="<spring:message code= "LB.W0136" />"><spring:message code="LB.W0136" /></th>
													<!--狀態 -->
													<th data-title="<spring:message code= "LB.Status" />"><spring:message code="LB.Status" /></th>
													<!--營業單位-->
													<th data-title="<spring:message code= "LB.X0052" />"><spring:message code="LB.X0052" /></th>
													<!--帳號 -->
													<th data-title="<spring:message code= "LB.Account" />"><spring:message code="LB.Account" /></th>
													<!--生效日 -->
													<th data-title="<spring:message code= "LB.Effective_date" />"><spring:message code="LB.Effective_date" /></th>
													<!--解款日 -->
													<th data-title="<spring:message code= "LB.W0140" />"><spring:message code="LB.W0140" /></th>
													<!--執行選項 -->
													<th data-title="<spring:message code= "LB.Execution_option" />"><spring:message code="LB.Execution_option" /></th>
												</tr>
											</thead>
											<tbody>
											<c:forEach varStatus="loop" var="dataList" items="${ BaseResultData.REC }">
												<tr>
												<input type="hidden" id="helperjson_${loop.index}" value='${dataList.helperjson}'>
<!-- 													匯入編號 -->
													<td class="expand text-center">${dataList.REFNO}</td>
<!-- 													建檔日期 -->
													<td class="text-center">${dataList.TXDATE}</td>
<!-- 													幣別 -->
													<td class="text-center">${dataList.TXCCY}</td>
<!-- 													金額 -->
													<td class="text-right">${dataList.showTXAMT}</td>
<!-- 													匯款人 -->
													<td class="text-center">${dataList.ORDNAME}</td>
<!-- 													狀態 -->
													<td class="text-center"><spring:message code="LB.X0053" /></td>
													<!-- 營業單位 -->
													<td class="text-center">${dataList.TXBRH}</td>
<!-- 													帳號 -->
													<td class="text-center">${dataList.BENACC}</td>
<!-- 													生效日 -->
													<td class="text-center">${dataList.VALDATE}</td>
<!-- 													解款日 -->
													<td class="text-center"></td>
<!-- 													執行選項 -->
													<td class="text-center"><input class="ttb-sm-btn btn-flat-orange" type="button" value="<spring:message code="LB.X1148" />" name="CMSUBMIT" onclick="relieve('helperjson_${loop.index}','${dataList.VALDATE}')" /></td>
												</tr>
												</c:forEach>
											</tbody>
										</table>
							<!-- button -->
								<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />">
							<!-- buttonEND -->
						</div>
					</div>
					<!--說明： -->
					<div class="text-left">
						<ol class="description-list list-decimal">
							<p>
								<spring:message code="LB.Description_of_page" />
							</p>
							<li>
								<strong style="font-weight: 400">
										<spring:message code="LB.F_Remittances_Payments_P1_D1-1" /><a href="${__ctx}/CUSTOMER/SERVICE/rate_table"
										target="_blank"><spring:message code="LB.F_Remittances_Payments_P1_D1-2" /></a><a href="${__ctx}/public/transferFeeRate.htm"
										target="_blank"><spring:message code="LB.F_Remittances_Payments_P1_D1-3" /></a><a href="https://www.tbb.com.tw/exchange_rate"
										target="_blank"><spring:message code="LB.F_Remittances_Payments_P1_D1-4" /></a>
								</strong>
							</li>
						</ol>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>