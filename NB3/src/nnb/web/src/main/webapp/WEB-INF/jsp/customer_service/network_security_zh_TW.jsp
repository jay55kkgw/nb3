<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		$("#CMBACK").click(function(e){
			$("#formId").attr("action","${__ctx}/login");
			$("#formId").submit();
		});
	}
</script>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">網路安全</a></li>
		</ol>
	</nav>
	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					網路安全
				</h2>
				<form id="formId" method="post" action="">
					<div class="card-block shadow-box terms-pup-blcok">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block tab-pane fade show active" id="nav-trans-1" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <!-- Q1 -->
	                                    <div class="ttb-pup-block" role="tab">
	                                        <a role="button" class="ttb-pup-title d-block" data-toggle="collapse" href="#popup-1" aria-expanded="true" aria-controls="popup-1">
	                                            <div class="row">
	                                                <span class="col-1">Q1</span>
	                                                <div class="col-10 row">
	                                                    <span class="col-12">什麼是偽冒網站？</span>
	                                                </div>
	                                            </div>
	                                        </a>
	                                        <div id="popup-1" class="ttb-pup-collapse collapse show" role="tabpanel">
	                                            <div class="ttb-pup-body">
	                                                <ul class="ttb-pup-list">
	                                                    <li>
	                                                        <p>詐騙集團偽造與知名網站一模一樣的假網站，騙取客戶進入這些網站，輸入個人私密資料(如身份證字號、帳號、信用卡號、密碼等)，藉以盜取個人資料，非法使用，損害客戶權益。</p>
	                                                    </li>
	                                                    <li>
	                                                        <p>因應措施</p>
	                                                        <ol style="list-style-type: circle; margin-left: 1rem;">
	                                                            <li><p>臺灣企銀網路銀行網址 <a href="https://ebank.tbb.com.tw">https://ebank.tbb.com.tw</a> ，登入前請逐字核對網址確保進入正確的網站。
	                                                                <a onclick="window.open('${__ctx}/img/newsample1.png','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')" href="#">(圖示說明)</a></p>
	                                                            </li>
	                                                            <li>
	                                                                <p>請檢查電腦螢幕右下角顯示安全鎖標誌
	                                                                <a onclick="window.open('https://portal.tbb.com.tw/tbbportal/fstop/images/sample2.gif','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')" href="#">(圖示說明)</a>
	                                                               		 點選安全鎖即顯示憑證資訊。
	                                                                <a onclick="window.open('https://portal.tbb.com.tw/tbbportal/fstop/images/sample3.gif','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')" href="#">(圖示說明)</a></p>
	                                                            </li>
	                                                            <li><p>請勿點選來路不明可疑電子郵件內的超連結。</p></li>
	                                                            <li><p>請定期變更網路銀行密碼，以保護個人私密資料。</p></li>
	                                                        </ol>
	                                                    </li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <!-- Q2 -->
	                                    <div class="ttb-pup-block" role="tab">
	                                        <a role="button" class="ttb-pup-title d-block" data-toggle="collapse" href="#popup-2" aria-expanded="true" aria-controls="popup-2">
	                                            <div class="row">
	                                                <span class="col-1">Q2.</span>
	                                                <div class="col-10 row">
	                                                    <span class="col-12">什麼是偽冒電郵？</span>
	                                                </div>
	                                            </div>
	                                        </a>
	                                        <div id="popup-2" class="ttb-pup-collapse collapse show" role="tabpanel">
	                                            <div class="ttb-pup-body">
	                                                <ul class="ttb-pup-list">
	                                                    <li>
	                                                        <p>詐騙集團假冒銀行或線上服務業者名義通知客戶資料過期、無效需要更新，或者是基於安全理由進行身分驗證，
										                                                            要求客戶輸入個人私密資料（如身分證字號、帳號、信用卡號、密碼等）。客戶如一時不察經由電子郵件指引的網址，
										                                                            連結至偽冒網站輸入個人資料，輕則成為垃圾郵件業者的名單，重則電腦可能被植入木馬程式，破壞系統或個人私密資料遭竊。</p>
	                                                    </li>
	                                                    <li>
	                                                        <p>因應措施</p>
	                                                        <ol style="list-style-type: circle; margin-left: 1rem;">
	                                                            <li><p>請勿理會來路不明可疑的電子郵件，且勿點選所提供的超連結，並立即刪除。</p>
	                                                            </li>
	                                                            <li>
	                                                                <p>請勿回覆要求提供個人機密資料的可疑電子郵件。</p>
	                                                            </li>
	                                                            <li><p class="text-danger">請注意！臺灣企銀絕對不會經由電子郵件，要求客戶輸入個人機密資料。 如收到此類可疑電子郵件，請立即通知臺灣企銀服務專線：0800-00-7171。請安裝及定期更新您的防毒軟體。</p></li>
	                                                        </ol>
	                                                    </li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                    </div>
	
	                                </div>
	                            </div>
	                        </div>
	                        <input type="BUTTON" class="ttb-button btn-flat-gray" value="返回" name="CMBACK" id="CMBACK">
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
