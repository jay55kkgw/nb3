<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
  <br /><br />
  <div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
  <br /><br />
  <div style="text-align:center">
    <font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
  </div>
  <br/>
  <div style="text-align:center">
  ${MsgName}
  </div>
  <br />
     <table class="print">
    <!-- 交易時間 -->
    <tr>
      <td style="width:8em">
<!--         交易時間 -->
       <spring:message code="LB.Trading_time" />
      </td>
      <td>${CMTXTIME} </td>
    </tr>
    <!-- 存單帳號 -->
    <tr>
      <td class="ColorCell">
<!--         存單帳號 -->
                <spring:message code="LB.Account_no" />
      </td>
      <td>${FDPACN}</td>
    </tr>
    <!-- 存單號碼 -->
    <tr>
      <td class="ColorCell">
<!--         存單號碼 -->
                      <spring:message code="LB.Certificate_no" />
      </td>
      <td> ${FDPNUM}</td>
    </tr>
    <!-- 存單金額 -->
    <tr>
      <td class="ColorCell">
<!--         存單金額 -->
                       <spring:message code="LB.Certificate_amount" />
      </td>
      <td>
      <!-- 								新台幣 : -->
		<spring:message code="LB.NTD" />
        <fmt:formatNumber type="number" minFractionDigits="2" value="${AMOUNT}" />
<!--         元 -->
        									<spring:message code="LB.Dollar" />       
      </td>
    </tr>
    <!-- 存單種類 -->
    <tr>
      <td class="ColorCell">
<!--         存單種類 -->
        <spring:message code="LB.Deposit_certificate_type" />
      </td>
      <td>
        <p>${FDPACC}</p>
      </td>
    </tr>
    <!-- 存款期別 -->
    <tr>
      <td class="ColorCell">
<!--         存款期別 -->
                      	<spring:message code="LB.Fixed_duration" />
      </td>
      <td class="DataCell">${TERM}</td>
    </tr>
    <!-- 起存日 -->
    <tr>
      <td class="ColorCell">
<!--         起存日 -->
                      <spring:message code="LB.Start_date" />
      </td>
      <td class="DataCell">${SDT}</td>
    </tr>
    <!-- 到期日 -->
    <tr>
      <td class="ColorCell">
<!--         到期日 -->
                      <spring:message code="LB.Maturity_date" />
      </td>
      <td class="DataCell">${DUEDAT}</td>
    </tr>
    <!-- 計息方式 -->
    <tr>
      <td class="ColorCell">
<!--         計息方式 -->
                      <spring:message code="LB.Interest_calculation" />
      </td>
      <td class="DataCell">${INTMTH}</td>
    </tr>
    <!-- 利率 -->
    <tr>
      <td class="ColorCell">
<!--         利率 -->
                      <spring:message code="LB.Interest_rate" />
      </td>
      <td class="DataCell">${ITR}%</td>
    </tr>
    <!-- 續存方式 -->
    <tr>
      <td class="ColorCell">
<!--         續存方式 -->
                      <spring:message code="LB.Interest_transfer_to_account" />
      </td>
      <td class="DataCell">
        ${FGSVNAME}
<!--         帳號 -->
        ${TSFACN}
      </td>
    </tr>
    <!-- 原存單金額 -->
    <tr>
      <td class="ColorCell">
<!--         原存單金額 -->
                      <spring:message code="LB.Original_amount" />
      </td>
      <td>
<!--         新台幣 : -->
        <spring:message code="LB.NTD" />
        	<fmt:formatNumber type="number" minFractionDigits="2" value="${FDPAMT }" />
<!--         元 -->
        <spring:message code="LB.Dollar" />       
      </td>
    </tr>
    <!-- 原存單利息 -->
    <tr>
      <td class="ColorCell">
<!--         原存單利息 -->
        <spring:message code="LB.Original_interest" />
      </td>
      <td>
      <!--         新台幣 : -->
        <spring:message code="LB.NTD" />
        <fmt:formatNumber type="number" minFractionDigits="2" value="${PYDINT}" />
<!--         元 -->
        <spring:message code="LB.Dollar" />
      </td>
    </tr>
    <!-- 代扣利息所得稅 -->
    <tr>
      <td class="ColorCell">
<!--         代扣利息所得稅 -->
        <spring:message code="LB.Withholding_interest_income_tax" />
      </td>
      <td>
<!--         新台幣 : -->
        				<spring:message code="LB.NTD" />
        <fmt:formatNumber type="number" minFractionDigits="2" value="${GRSTAX }" />
<!--         元 -->
        <spring:message code="LB.Dollar" />
      </td>
    </tr>
    <!-- 健保費 -->
    <tr>
      <td class="ColorCell">
<!--         健保費 -->
        <spring:message code="LB.NHI_premium" />
      </td>
      <td>
<!--         新台幣 : -->
                <spring:message code="LB.NTD" />
                		 <c:choose>
							<c:when test="${empty NHITAX}">
									<fmt:formatNumber type="number" minFractionDigits="2" value="0" /> 
							</c:when>
							<c:otherwise>
								     <fmt:formatNumber type="number" minFractionDigits="2" value="${NHITAX }" />
							</c:otherwise>
        				</c:choose>
<!--         元 -->
        <spring:message code="LB.Dollar" />
      </td>
    </tr>
  </table>
  <br>
  <br>
<!--   <div class="text-left"> -->
<!--     說明： -->
<!--     <ol class="list-decimal text-left"> -->
<!--       <li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
<!--       <li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
<!--     </ol> -->
<!--   </div> -->
</body>

</html>