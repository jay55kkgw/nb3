<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<meta http-equiv="Content-Language" content="zh-tw">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 出口押匯查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Export_Bills_Negotiation_Inquiry" /></li>
		</ol>
	</nav>



	<!-- 	快速選單及主頁內容 -->
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<!-- 		主頁內容  -->
		<main class="col-12"> 
		<section id="main-content" class="container">
			<h2><spring:message code="LB.Export_Bills_Negotiation_Inquiry" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Downloads" /></option>
					<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
					<!-- 						下載為excel檔 -->
					<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					<!-- 						下載為txt檔 -->
				</select>
			</div>
			<form id="formId" action="${__ctx}/download" method="post">
						<input type="hidden" id="back" name="back" value="">
						<!-- 下載用 -->
						<input type="hidden" name="downloadFileName" value="<spring:message code="LB.Export_Bills_Negotiation_Inquiry" />" /> 
						<input type="hidden" name="CMQTIME"  value="${export_bill_query_result.data.CMQTIME}" />
						<input type="hidden" name="CMPERIOD" value="${export_bill_query_result.data.CMPERIOD}" /> 
						<input type="hidden" name="CMRECNUM" value="${export_bill_query_result.data.CMRECNUM}" />
						<input type="hidden" name="LCNO" value="${export_bill_query_result.data.LCNO_DOWNLOAD}" /> 
						<input type="hidden" name="REFNO" value="${export_bill_query_result.data.REFNO_DOWNLOAD}" />
						<input type="hidden" name="CURRENCY_TOTAL_TXT" value="${export_bill_query_result.data.DownloadStringTXT}"/>
						<input type="hidden" name="CURRENCY_TOTAL_EXCEL" value="${export_bill_query_result.data.DownloadStringEXCEL}"/>
						<input type="hidden" name="downloadType" id="downloadType" /> 
						<input type="hidden" name="templatePath" id="templatePath" />
						<!-- EXCEL下載用 -->
						<input type="hidden" name="headerRightEnd" value="9" /> 
						<input type="hidden" name="headerBottomEnd" value="8"/>
						<input type="hidden" name="rowStartIndex" value="9"/>
						<input type="hidden" name="rowRightEnd" value="9"/>
						<input type="hidden" name="footerStartIndex" value="11" />
						<input type="hidden" name="footerEndIndex" value="13" />
						<input type="hidden" name="footerRightEnd" value="10" />
						<!-- TXT下載用 -->
						<input type="hidden" name="txtHeaderBottomEnd" value="9" /> 
						<input type="hidden" name="txtHasRowData" value="true" /> 
						<input type="hidden" name="txtHasFooter" value="true" />
			<div class="main-content-block row">
				<div class="col-12 tab-content">
								<ul class="ttb-result-list">
									<li>
										<!-- 查詢時間 -->
										<h3>
											<spring:message code="LB.Inquiry_time" />
										</h3>
										<p>	
											${export_bill_query_result.data.CMQTIME }
										</p>
									</li>
									<li>
										<!-- 查詢期間 -->
										<h3>
											<spring:message code="LB.Inquiry_period_1" />
										</h3>
										<p>	
											${export_bill_query_result.data.CMPERIOD}
										</p>
									</li>
									<li>
										<h3><spring:message code="LB.L/C_no" /></h3>
										<p>
										<c:if test="${export_bill_query_result.data.LCNO =='' }">
											 <spring:message code="${export_bill_query_result.data.LCNO_SHOW}" />
										</c:if>
										<c:if test="${export_bill_query_result.data.LCNO !='' }">
											 ${export_bill_query_result.data.LCNO_SHOW}
										</c:if>
										</p>
									</li>
									<li>
										<h3><spring:message code="LB.Ref_no" /></h3>
										<p>
										<c:if test="${export_bill_query_result.data.REFNO =='' }">
											 <spring:message code="${export_bill_query_result.data.REFNO_SHOW}" />
										</c:if>
										<c:if test="${export_bill_query_result.data.REFNO !='' }">
											 ${export_bill_query_result.data.REFNO_SHOW}
										</c:if>
										</p>
									</li>
									<li>
											<!-- 資料總數 -->
										<h3>
											<spring:message code="LB.Total_records" />
										</h3>
										<p>
											 ${export_bill_query_result.data.CMRECNUM}
											<spring:message code="LB.Rows" />
										</p>
									</li>
								</ul>

								<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
									<thead>
										<tr>
											<th><spring:message code="LB.W0218" /></th>
											<th><spring:message code="LB.Ref_no" /></th>
											<th><spring:message code="LB.L/C_no" /></th>
											<th><spring:message code="LB.Currency" /></th>
											<th><spring:message code="LB.W0220" /></th>
											<th><spring:message code="LB.W0195" /></th>
											<th><spring:message code="LB.W0196" /></th>
											<th><spring:message code="LB.W0198" /></th>
											<th><spring:message code="LB.W0203" /></th>
											<th><spring:message code="LB.W0199" /></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="dataList"
											items="${export_bill_query_result.data.REC }">
											<tr>
												<td class="text-center">${dataList.RNEGDATE }</td>
												<td class="text-center">${dataList.RREFNO }</td>
												<td class="text-center">${dataList.RLCNO }</td>
												<td class="text-center">${dataList.RBILLCCY }</td>
												<td class="text-right">${dataList.RBILLAMT }</td>
												<td class="text-center">${dataList.RVALDATE }</td>
												<td class="text-center"><spring:message code="${dataList.RUPSTAT }"/></td>
												<td class="text-center">${dataList.RCOUNTRY }</td>
												<td class="text-center">${dataList.RBILLTYP }</td>
												<td class="text-center">${dataList.RCMDATE1 }</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<br>
								<div class="text-left">
									<p><spring:message code="LB.W0227" />：</p>
									<c:forEach var="dataTable2" items="${export_bill_query_result.data.CRY}">
							 		<p>&nbsp;${dataTable2.AMTRBILLCCY}&nbsp;${dataTable2.FXTOTAMT}&nbsp;<spring:message code="LB.W0158" />&nbsp;${dataTable2.FXTOTAMTRECNUM}&nbsp;<spring:message code="LB.Rows" /></p>
									</c:forEach>
									<c:if test="${N560_RESULT.data.TOPMSG eq 'OKOV'}">
									<div>
										<spring:message code="LB.F_Export_Bill_Query_P2_D1" />
										<input type="button" class="ttb-sm-btn btn-flat-orange"  id="Query" value="<spring:message code="LB.X0151" />" onclick="QueryNext();"/>
									</div>
									</c:if>
								</div>
								<br>	
								<input type="button" class="ttb-button btn-flat-gray"id="previous" value="<spring:message code="LB.Back_to_previous_page"/>" />					
								<input type="button" class="ttb-button btn-flat-orange"id="printbtn" value="<spring:message code="LB.Print"/>" /> 
							</div>
						</div>
					</form>
					<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><spring:message code= "LB.F_Export_Bill_Query_P2_D1" /></li>
					<li><spring:message code= "LB.F_Export_Bill_Query_P2_D2" /></li>
				</ol>
				</div>
			</div>
			</div>
			<!-- 				</form> -->
		</section>
		<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
	<script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()",10);
			// 開始查詢資料並完成畫面
			setTimeout("init()",20);
			
			setTimeout("initDataTable()",100);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)",500);
		});

		function init() {
			//列印
			$("#printbtn").click(function() {
				var i18n = new Object();
				i18n['jspTitle']='<spring:message code= "LB.Export_Bills_Negotiation_Inquiry" />'
				var params = {
					"jspTemplateName" : "export_bill_query_result_print",
					"jspTitle" : i18n['jspTitle'],
					"CMQTIME" : "${export_bill_query_result.data.CMQTIME}",
					"CMPERIOD" : "${export_bill_query_result.data.CMPERIOD}",
					"CMRECNUM" : "${export_bill_query_result.data.CMRECNUM }",
					"LCNO":"${export_bill_query_result.data.LCNO}",
					"LCNO_SHOW":"${export_bill_query_result.data.LCNO_SHOW}",
					"REFNO":"${export_bill_query_result.data.REFNO}",
					"REFNO_SHOW":"${export_bill_query_result.data.REFNO_SHOW}"
				};
				openWindowWithPost(
					"${__ctx}/print",
					"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
			//上一頁按鈕
			$("#previous").click(function() {
				var action = '${__ctx}/FCY/ACCT/f_export_bill_query';
				$('#back').val("Y");
				$("form").attr("action", action);
				initBlockUI();
				$("form").submit();
			});

		}

		//選項
		function formReset() {
			//initBlockUI();
			if ($('#actionBar').val() == "excel") {
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/f_export_bill_query_result.xls");
			} else if ($('#actionBar').val() == "txt") {
				$("#downloadType").val("TXT");
				$("#templatePath").val("/downloadTemplate/f_export_bill_query_result.txt");
			}
			//ajaxDownload("${__ctx}/ajaxDownload", "formId","finishAjaxDownload()");
			$("#formId").attr("target", "");
			$("#formId").submit();
			$('#actionBar').val("");
		}

	</script>
</body>
</html>
