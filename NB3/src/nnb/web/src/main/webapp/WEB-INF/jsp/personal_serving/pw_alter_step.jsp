<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    
	<script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
    
	    function init(){
		    // 初始化表單驗證
	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });

	    	// submit前
	    	$("#pageshow").click(function(e){			
				console.log("before submit...");

				if(!$('#formId').validationEngine('validate')){
			      	e.preventDefault();
				}else{
					$("#formId").validationEngine('detach');
					
					// 送出前先把變更的密碼做加密塞入
					var DPSIPD = $('#DPSIPD').val();
					var DPNSIPD = $('#DPNSIPD').val();
					$('#PINOLD').val(pin_encrypt(DPSIPD));
					$('#PINNEW').val(pin_encrypt(DPNSIPD));
					
					initBlockUI();
					
					$("#formId").submit();
				}
			});
	    }
	    
	    function chkp(data){
			if(data.length >= 8){
				errorBlock(
						null, 
						null,
						['<spring:message code= "LB.X2362" />'], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}

 	</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 密碼變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Change_Password" /></li>
		</ol>
	</nav>

	<!-- 快速選單及主頁內容 -->
	<!-- menu、登出窗格 --> 
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">	
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Change_Login_Password" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<form method="post" id="formId" action="${__ctx}/PERSONAL/SERVING/pw_alter_result">
					<!-- 在前端比對新簽入密碼不得與身分證/營利事業統一編號相同-->
					<input type="hidden" id="UID" name="UID" value="${UID}" />
					<!-- 在前端比對新密碼不可與使用者名稱相同 -->
					<input type="hidden" id="USERNAME" name="USERNAME" value="${USERNAME}" />
					
<!-- 				<div id="step-bar"> -->
<!-- 					<ul> -->
<!-- 						<li class="finished">Step1</li> -->
<!-- 						<li class="active">Step2</li> -->
<!-- 						<li class="">Step3</li> -->
<!-- 					</ul> -->
<!-- 				</div> -->

					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 舊簽入密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.login_password" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="PINOLD" name="PINOLD" />
											<input type="password" id="DPSIPD" name="DPSIPD" class="text-input validate[required, custom[onlyLetterNumber]] " size="10" maxlength="8" value="" onkeypress="chkp(this.value)"/>
										</div>
									</span>
								</div>
								
								<!--新簽入密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.New_login_password" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="PINNEW" name="PINNEW" />
											<input type="password" id="DPNSIPD" name="DPNSIPD" size="10" maxlength="8" value="" onkeypress="chkp(this.value)"
												class="text-input validate[required,newcolumn[PW, DPSIPD, DPNSIPD, USERNAME, UID],funcCall[validate_chkSerialNum['<spring:message code= "LB.New_login_password" />',DPNSIPD]],funcCall[validate_chkSameEngOrNum['<spring:message code= "LB.New_login_password" />',DPNSIPD]]]" />
											<p><spring:message code="LB.User_Change_password_note" /></p>
										</div>
									</span>
								</div>
								
								<!-- 確認新簽入密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
					                 			<spring:message code="LB.Confirm_Login_Password" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="password" id="DPNSSIPD" name="DPNSSIPD" size="10" maxlength="8" value="" onkeypress="chkp(this.value)"
												class="text-input validate[required ,recolumn[PW, DPSIPD, DPNSIPD, DPNSSIPD]]" />
											<p><spring:message code="LB.Please_Enter_New_Login_Password_again" /></p>
										</div>
									</span>
								</div>
							</div>
							<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
							<input type="button" id="pageshow" name="pageshow" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />						
						</div>
					</div>	
				</form>

				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><spring:message code="LB.Pw_alter_step_P2_D1" /></li>
					<li style="color:#FF0000;"><spring:message code="LB.Pw_alter_step_P2_D2" /></li>
					<li style="color:#FF0000;"><spring:message code="LB.Pw_alter_step_P2_D3" /></li>
				</ol>
				
			</section>
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
    
</body>
</html>
