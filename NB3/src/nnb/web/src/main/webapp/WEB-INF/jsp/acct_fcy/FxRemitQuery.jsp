<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>


	<script type="text/javascript">

		$(document).ready(function () {
			init();
			
			var cnty ="${FxRemitQuery.data.CNTY}";
			var remtype = "${FxRemitQuery.data.REMTYPE}";
			
			console.log("CNTY >> {}" + cnty);
			console.log("REMTYPE >> {}" + remtype);
			
			$('a').each(function() {
			    if ($(this).is('[href*="ADLKINDID=07 "') && cnty == 'TW' && remtype == '4') {
			        $(this).attr("onclick", "return false");
			        $(this).click(function(){
			        	errorBlock(
								null, 
								null,
								['<spring:message code= "LB.X2635" />'], 
								'<spring:message code= "LB.Quit" />', 
								null
						)
			        });
			    }
			});
		});

		function init() {
			initDataTable(); // 將.table變更為DataTable
			setTimeout("dodatataable()",200);
		}
		
		function dodatataable(){
			var pre="上一頁";
			var next="下一頁";
			if(dt_locale == "zh_CN")
			{
				pre = "上一页";
				next = "下一页";
			}
			else if(dt_locale == "en")
			{
				pre="Previous";
				next="Next";
			}
			jQuery(function($) {
				$('.FxRemitQuery-table').DataTable({
					"columnDefs": [
		            	{ width: "60px", targets: [0]},
						{ width: "110px", targets: [1]}, 
		            ],
		            lengthChange: false,
		            scrollX: true, //水平滾動
		            sScrollX: "99%",
					bPaginate: true, //啟用或禁用分頁
					bFilter: false, //啟用或禁用數據過濾
					bDestroy: true, //如果沒有表與選擇器匹配，則將按照常規構造新的DataTable
					bSort: false, //啟用或禁用列排序
					info: false, //功能控製表信息顯示字段。
					scrollCollapse: true, 
					language:{
						"paginate":{
							"previous":pre,
							"next":next
						}
					}
				});
			});
		}
		
		function fillData(id, item, desc) 
		{					
			var obj_srcfund = self.opener.document.getElementById('SRCFUND');
			if (obj_srcfund != null){
				obj_srcfund.value = id;
			}
			////匯款分類項目
			var obj_srcfunddesc = self.opener.document.getElementById('SRCFUNDDESC');
			var show_srcfunddesc = self.opener.document.getElementById('SHOW_SRCFUNDDESC');
			if (obj_srcfunddesc != null) {
				if (id == 'NUL') {
					obj_srcfunddesc.value = '<spring:message code="LB.X2462" />' + '-' + item;
					
				} else {
					obj_srcfunddesc.value = id + '-' + item;
				}
			}
			if(show_srcfunddesc != null) {
				if (id == 'NUL') {
					show_srcfunddesc.innerHTML = '<spring:message code="LB.X2462" />' + '-' + item;
					
				} else {
					show_srcfunddesc.innerHTML = id + '-' + item;
				}
			}
			//匯款分類說明				
			var obj_desc = self.opener.document.getElementById('FXRMTDESC');
			var show_desc = self.opener.document.getElementById('SHOW_FXRMTDESC');
			if (obj_desc != null) {
				obj_desc.value = desc;
			}
			if (show_desc != null) {
				show_desc.innerHTML = desc;
			}
				
			self.close();
		}
		
	
	</script>
	<title>
		<spring:message code="LB.Title" />
	</title>
</head>

<body>
	<main class="col-12">
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<form method="post" id="formId" action="">
				<h2><spring:message code= "LB.X1467" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
                	<div class="col-12 tab-content">
                		<ul class="ttb-result-list"></ul>
                		<table class="stripe table-striped ttb-table FxRemitQuery-table" data-toggle-column="first">
						 	<thead>
								<tr>
									<!-- 分類編號 -->
									<th ><spring:message code= "LB.X1567" /></th>
									<!-- 項目 -->
									<th ><spring:message code= "LB.D0271" /></th>
									<!-- 說明 -->
									<th ><spring:message code= "LB.Description_of_page" /></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="dataList" items="${FxRemitQuery.data.REC}">
									<!-- 如果是從外幣匯出匯款頁面進入此頁面，則不顯示09國內交易 -->
									<!-- 110.12.22更新，國際部需求041_110_289，匯出匯款&匯入解款，類別為政府、團體、民間(非TW)，不顯示09國內交易 -->
									<c:if test="${FROM_FCY_REMITTANCES == null || FxRemitQuery.data.CNTY == 'TW' || (FxRemitQuery.data.CNTY != 'TW' && dataList.ADLKINDID != '09 ')}"> <!-- 09後面的空白不要刪掉，因為回傳的值就是有空白... -->
										<tr>
											<td style="color: #007bff">
												<a href="FxRemitQuery_1?ADLKINDID=${dataList.ADLKINDID}&ADRMTTYPE=${FxRemitQuery.data.str_ADRMTTYPE}&CUTTYPE=${FxRemitQuery.data.str_CUTTYPE}&CNTY=${FxRemitQuery.data.CNTY}&REMTYPE=${FxRemitQuery.data.REMTYPE}">
													${dataList.ADLKINDID}
												</a>										
											</td>
											<td>${dataList.ADRMTITEM}</td>
											<td class="white-spacing">${dataList.ADRMTDESC}</td>
										</tr>
									</c:if>
								</c:forEach>
								<c:if test="${FxRemitQuery.data.NULFLAG == 'Y' && FxRemitQuery.data.str_ADRMTTYPE=='1'}">
										<tr >
											<td style="color: #007bff"><a href="self" onclick="fillData('NUL','<spring:message code= "LB.X2463" />','<spring:message code= "LB.X2464" />');"><spring:message code= "LB.X2462" /></a></td>
											<td><spring:message code= "LB.X2463" /></td>
											<td><spring:message code= "LB.X2464" /></td>
										</tr>
								</c:if>
								<c:if test="${FxRemitQuery.data.NULFLAG == 'Y' && FxRemitQuery.data.str_ADRMTTYPE=='2'}">
										<tr>
											<td style="color: #007bff"><a href="self" onclick="fillData('NUL','<spring:message code= "LB.X2465" />','<spring:message code= "LB.X2466" />');"><spring:message code= "LB.X2462" /></a></td>
											<td><spring:message code= "LB.X2465" /></td>
											<td><spring:message code= "LB.X2466" /></td>
										</tr>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		</section>
	</main> <!-- 		main-content END -->
</body>

</html>