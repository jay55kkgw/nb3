<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
$(document).ready(function(){
	initFootable();
	
	$("#printbtn").click(function(){
		var i18n = new Object();
		i18n['jspTitle']='<spring:message code="LB.Credit_Card_Un-billed_Transactions" />'
		var params = {
				"jspTemplateName":"unbilled_result_print",
				"jspTitle":i18n['jspTitle'],
				"HOLDERNAME":'${card_unbilled_result.data._CUSNAME}',
				"CYCLEDATE":'${card_unbilled_result.data._CYCLE}',
				"CMQTIME":"${card_unbilled_result_SYSTIME}",
				"COUNT":"${card_unbilled_result_TOTCNT}",
		}
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

	});
	
});
function selectBlockUI() {
	//change後遮罩啟動
	setTimeout("initBlockUI()",10);
	// 開始執行動作
	setTimeout("selectAction()",20);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
		
}
function selectAction(){
	if($('#actionBar').val()=="excel"){
		$("#downloadType").val("OLDEXCEL");
		$("#templatePath").val("/downloadTemplate/unbilled.xls");
		$('#actionBar').val("");
		$("#formId").submit();
	}else if ($('#actionBar').val()=="txt"){
		$("#downloadType").val("TXT");
		$("#templatePath").val("/downloadTemplate/unbilled.txt");
		$('#actionBar').val("");
		$("#formId").submit();
	}
}


</script>
</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 未出帳單明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card_Un-billed_Transactions" /></li>
		</ol>
	</nav>

	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	

	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<!-- 信用卡未出帳單明細查詢 -->
			<h2><spring:message code="LB.Credit_Card_Un-billed_Transactions" /></h2>
			
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			
			<div class="print-block">
					<select class="minimal" id="actionBar" onchange="selectBlockUI()">
						<!-- 執行選項-->
						<option value=""><spring:message
								code="LB.Downloads" /></option>
						<!-- 下載Excel檔-->
						<option value="excel"><spring:message
								code="LB.Download_excel_file" /></option>
						<!-- 下載為txt檔-->
						<option value="txt"><spring:message
								code="LB.Download_txt_file" /></option>
					</select>
				</div>
			<br/>
			<br/>
				<div class="main-content-block row">
					<div class="col-12">
						<div>
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間  -->
									
									<p>
										<spring:message code="LB.Inquiry_time" />：
										${card_unbilled_result.data.SYSTIME}<input type="hidden" name="SYSTIME" value="${card_unbilled_result_SYSTIME}" readonly="readonly"/>
									</p>
									<p>
										<!-- 資料總數  -->
										<spring:message code="LB.Total_records" />：
										${card_unbilled_result.data.TOTCNT}<input type="hidden" name="TOTCNT" value="${card_unbilled_result_TOTCNT}" readonly="readonly"/>
										<spring:message code="LB.Rows" />
									</p>
								</li>
								
							</ul>
							<table class="table" data-toggle-column="first">
								<thead>
					            <tr>
					                <th>
										<!-- 姓名  -->
					               		<spring:message code="LB.Name" />
					                </th>
					                <th>
					                	<!-- 每月結帳日  -->
					               		<spring:message code="LB.Statement_date_TD01" />
					                </th>
					            </tr>
					            </thead>
					            <tbody>
					            <tr>
					                <td>
										<!-- 姓名  -->
					               		${card_unbilled_result.data._CUSNAME}
					                </td>
					                <td>
					                	<!-- 每月結帳日  -->
					               		${card_unbilled_result.data._CYCLE}
					                </td>
					            </tr>
					            </tbody>
							</table>
							
							<table class="table" data-toggle-column="first">
								<thead>
					            <tr>
					                <th>
										<!-- 卡片種類  -->
					               		<spring:message code="LB.Card_kind" />
					                </th>
					                <th data-breakpoints="xs">
					                	<!-- 消費日期  -->
					               		<spring:message code="LB.Consumption_date" />
					                </th>
					                <th data-breakpoints="xs">
					                	<!-- 入帳日期  -->
					               		<spring:message code="LB.The_account_credited_date" />
					                </th>
					                <th data-breakpoints="xs">
					                	<!-- 交易說明  -->
					               		<spring:message code="LB.Description_of_transaction" />
					                </th>
					                <th data-breakpoints="xs">
					                	<!-- 持卡人 -->
					               		<spring:message code="LB.Card_holder" />
					                </th>
					                <th data-breakpoints="xs sm">
					                	<!-- 幣別  -->
					               		<spring:message code="LB.Currency" />
					                </th >
					                <th data-breakpoints="xs sm">
					                	<!-- 外幣金額  -->
					               		<spring:message code="LB.Foreign_currency_amount" />
					                </th>
					                <th>
					                	<!-- 台幣金額  -->
					               		<spring:message code="LB.NTD_amount" />
					                </th>
					            </tr>
					            </thead>
					            <tbody>
							<c:forEach var="dataTable" items="${card_unbilled_result.data._Table}">
					            <tr>
					                <td>
										<!-- 卡片種類  -->
					               		${dataTable._CARDTYPE}
					                </td>
					                <td>
					                	<!-- 消費日期  -->
					               		${dataTable._PURDATE}
					                </td>
					                <td>
					                	<!-- 入帳日期  -->
					               		${dataTable._POSTDATE}
					                </td>
					                <td>
					                	<!-- 交易說明  -->
					               		${dataTable._DESCTXT}
					                </td>
					                <td>
					                	<!-- 持卡人 -->
					               		${dataTable._CRDNAME}
					                </td>
					                <td>
					                	<!-- 幣別  -->
					               		${dataTable._CURRENCY}
					                </td>
					                <td class="text-right">
					                	<!-- 外幣金額  -->
					               		${dataTable._SRCAMNTFMT}
					                </td>
					                <td class="text-right">
					                	<!-- 台幣金額  -->
					               		${dataTable._CURAMNTFMT}
					                </td>
					            </tr>
							</c:forEach>
							</tbody>
					        </table>
						</div>
						
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
					<form id="formId" action="${__ctx}/download" method="post">
<!-- 						下載用 -->
						<input type="hidden" name="downloadFileName" value="LB.Credit_Card_Un-billed_Transactions"/>
						<input type="hidden" name="CMQTIME" value="${card_unbilled_result.data.SYSTIME}"/>
						<input type="hidden" name="COUNT" value="${card_unbilled_result.data.TOTCNT}"/>
						<input type="hidden" name="CUSNAME" value="${card_unbilled_result.data._CUSNAME}"/>
						<input type="hidden" name="CYCLE" value="${card_unbilled_result.data._CYCLE}"/>
						<input type="hidden" name="downloadType" id="downloadType"/> 					
						<input type="hidden" name="templatePath" id="templatePath"/> 	
<!-- 						EXCEL下載用 -->
						<input type="hidden" name="headerRightEnd" value="7"/>
						<input type="hidden" name="headerBottomEnd" value="6"/>
						<input type="hidden" name="rowStartIndex" value="7"/>
						<input type="hidden" name="rowRightEnd" value="8"/>
						
<!-- 						TXT下載用 -->
						<input type="hidden" name="txtHeaderBottomEnd" value="11"/>
						<input type="hidden" name="txtHasRowData" value="true"/>
						<input type="hidden" name="txtHasFooter" value="false"/>
					</form>
				</div>
		</section>
	</main><!-- main-content END --> 
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
