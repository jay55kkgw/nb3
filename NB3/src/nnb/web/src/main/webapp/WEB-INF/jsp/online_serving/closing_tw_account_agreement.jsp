<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		// 確認鍵 click
		goOn();
		// 回上一頁
		goBack();
	}
	
	// 確認鍵 Click
	function goOn() {
 	  	$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
			var next = "${__ctx}" + "${next}";
            $("#formId").attr("action", next);
	 	  	$("#formId").submit();
		});
	}
	function checkReadFlag()
    {
    	console.log($("#ReadFlag").prop('checked'));
        if ($("#ReadFlag").prop('checked'))    		
      	{
       		$("#CMSUBMIT").prop('disabled',false);
      		$("#CMSUBMIT").removeClass("btn-flat-gray");
       		$("#CMSUBMIT").addClass("btn-flat-orange");
      	}
      	else
      	{
       		$("#CMSUBMIT").prop('disabled',true);
       		$("#CMSUBMIT").removeClass("btn-flat-orange");
       		$("#CMSUBMIT").addClass("btn-flat-gray");
   	  	}	
    }
	function goBack() {
		// 回上一頁
		$("#CMBACK").click( function(e) {
			var previous = "${__ctx}" + "${previous}";
			$("#formId").attr("action", previous);
			// 讓Controller知道是回上一頁
			$("#formId").append('<input type="hidden" name="back" value="Y" />');	
			$("#formId").submit();
		});
	}

</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 臺幣存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0422" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D0422" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<form id="formId" method="post" action="">
				<div class="main-content-block row">
                    <div class="col-12">
	                    <div class="ttb-input-block">
	                        <div class="ttb-message">
								<p><spring:message code= "LB.X1266" /></p><!-- 新臺幣存款帳戶線上結清銷戶之相關約定條款 -->
	                        </div>
	                        <ol class="ttb-result-list terms">
								<li data-num="一、"><span><spring:message code= "LB.X1267" /></span></li><!-- 立約人茲向貴行申請線上結清存款帳戶，並同意一併註銷/終止該帳戶結清前各項往來服務項目及委託代繳之各項費用，且遵守貴行有關之業務規定。 -->
								<li data-num="二、"><span><spring:message code= "LB.X1268" /></span></li><!-- 立約人同意存摺上存提交易明細或結存餘額或立約人查詢所得之餘額如與貴行帳載資料不符時，以貴行帳載之金額為準。但經核對貴行提出之交易紀錄，確為貴行記載錯誤，並經貴行查證屬實者，貴行應即更正之。 -->
								<li data-num="三、"><span><spring:message code= "LB.X1269" /></span></li><!-- 立約人同意貴行依指示將結存餘額匯入立約人開立於本行之其他新臺幣存款帳戶成功後，該帳戶始能結清。 -->
								<li data-num="四、"><span><spring:message code= "LB.X1270" /></span></li><!-- 結清銷戶基準日係以貴行電腦系統完成銷戶日為準。 -->
								<li data-num="五、"><span><spring:message code= "LB.X1271" /></span></li><!-- 立約人同意結清銷戶交易成功後，該帳戶之存摺即視為已作廢失效。 -->
	                        </ol>
	                    </div>
	  	   				<div class="ttb-input"><!-- 立約人已詳細審閱並充分了解上述約款內容，並上述約定。 -->
		  	   				<label class="check-block" for="ReadFlag"><input type="checkbox" name="ReadFlag" id="ReadFlag" onclick="checkReadFlag()"><spring:message code= "LB.X1272" />
		  	   				<span class="ttb-check"></span></label>
	  	   				</div>
						<input class="ttb-button btn-flat-gray" type="button" id="CMBACK" value="<spring:message code="LB.D0041" />"/>
						<input type="button" class="ttb-button btn-flat-gray" id="CMSUBMIT" value="<spring:message code="LB.D0097"/>" disabled/>
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>