<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<link rel="stylesheet" type="text/css"
		href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript"
		src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript"
		src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript"
		src="${__ctx}/js/jquery.datetimepicker.js"></script>
		<script type="text/JavaScript">
			$(document).ready(function () {
				tabEvent();
				$("#cmback").click(function() {
					var action = '${__ctx}/nb3/login';
					$("form").attr("action", action);
					$("form").submit();
				});
				$("#CMSUBMIT").click( function(e) {
					var TYPE = $("input[name='TYPE']:checked").val();
						switch (TYPE){
						case "3":
							$('#CN19-now-apply').hide();
							$('#MESSAGE').hide();
							$('#TYPE3REQ').show();
							$('#SUBSUBMIT').show();
							break;
						case "4":
							$('#CN19-now-apply').hide();
							$('#MESSAGE').hide();
							$('#TYPE4REQ').show();
							$('#SUBSUBMIT').show();
							break;
						case "8":
							$('#CN19-now-apply').hide();
							$('#MESSAGE').hide();
							$('#TYPE8REQ').show();
							$('#SUBSUBMIT').show();
							break;
						case "9":
							$('#CN19-now-apply').hide();
							$('#MESSAGE').hide();
							$('#TYPE9REQ').show();
							$('#SUBSUBMIT').show();
							break;
						case "18":
							window.open('https://nnb.tbb.com.tw/TBBNBAppsWeb/eeloanweb/NA05.jsp', '_blank');
							break;
						default:
							$('#formId').submit();
						}
				
				});
				
				$("#CMCANCEL").click( function(e) {
					$("#CN19-now-apply, #CN19-description, #CN19-button").show();
					$("#CN19-how-apply").hide();
					$('#TYPE3REQ').hide();
					$('#TYPE4REQ').hide();
					$('#TYPE8REQ').hide();
					$('#TYPE9REQ').hide();
					$('#SUBSUBMIT').hide();
				})
				
				$("#CMSUBMIT2").click( function(e) {
					var TYPE = $("input[name='TYPE']:checked").val();
					switch (TYPE){
					case "3":
						$('#formId').attr("action","${__ctx}/login?ADOPID=G210");
						break;
					case "4":
						$('#formId').attr("action","${__ctx}/login?ADOPID=G200");
						break;
					case "8":
						$('#formId').attr("action","${__ctx}/login?ADOPID=N814");
						break;
					case "9":
						$('#formId').attr("action","${__ctx}/login?ADOPID=CK01");
						break;
					}
					$('#formId').submit();
				});
				
				$("#CMSUBMIT2").click( function(e) {
					$('#formId').attr("action","${__ctx}/ONLINE/APPLY/online_apply");
				});
			});
			// 立刻、如何申請標籤切換事件
			function tabEvent() {
				// 立刻申請
				$("#now-apply").click(function() {
					$("#CN19-now-apply, #CN19-description, #CN19-button").show();
					$("#CN19-how-apply").hide();
					$('#TYPE3REQ').hide();
					$('#TYPE4REQ').hide();
					$('#TYPE8REQ').hide();
					$('#TYPE9REQ').hide();
					$('#SUBSUBMIT').hide();
				})
				// 如何申請
				$("#how-apply").click(function() {
					$("#CN19-how-apply").show();
					$('#applyTable').DataTable({
						"bPaginate" : false,
						"bLengthChange": false,
						"scrollX": true,
						"sScrollX": "99%",
						"scrollY": "80vh",
				        "bFilter": false,
				        "bDestroy": true,
				        "bSort": false,
				        "info": false,
				        "scrollCollapse": true,
				    });
					$("#CN19-now-apply, #CN19-description, #CN19-button").hide();
					$('#TYPE3REQ').hide();
					$('#TYPE4REQ').hide();
					$('#TYPE8REQ').hide();
					$('#TYPE9REQ').hide();
					$('#SUBSUBMIT').hide();
				})
			}
			
			
			$(window).bind('resize', function (){
				$('#applyTable').DataTable({
					"bPaginate" : false,
					"bLengthChange": false,
					"scrollX": true,
					"sScrollX": "99%",
					"scrollY": "80vh",
			        "bFilter": false,
			        "bDestroy": true,
			        "bSort": false,
			        "info": false,
			        "scrollCollapse": true,
			    });
			});
			
			function goPage(url){
				window.open(url, '_blank');
			}

		</script>
	</head>
	<body >
		<header>
			<%@ include file="../index/header_logout.jsp"%>
		</header>
		<!-- 功能清單及登入資訊 -->
		<div class="content row">
			<!-- 快速選單及主頁內容 -->
			<main class="col-12">
				<!-- 主頁內容  -->
				<section id="main-content" class="container" >
				<form id="formId" method="post"
				action="${__ctx}/ONLINE/APPLY/online_apply" target="_blank">
					<input type="hidden" id="FROM_NB3" name="FROM_NB3" value="Y">
					<!-- 功能名稱 -->
					<h2>線上申請</h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<!-- 功能內容 -->
					<div class="main-content-block row radius-50">
						<!-- 即時、預約導引標籤 -->
						<nav style="width: 100%;">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="now-apply"
									data-toggle="tab" href="#now-apply" role="tab"
									aria-controls="nav-home" aria-selected="false">立即申請
								</a>
								<a class="nav-item nav-link" id="how-apply"
									data-toggle="tab" href="#how-apply" role="tab"
									aria-controls="nav-profile" aria-selected="true">如何申請
								</a>
<!-- 								<a class="triple-nav-item nav-item nav-link" id="return-login" -->
<%-- 									href="#" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/login','', '')">回登入頁 --%>
<!-- 								</a> -->
							</div>
						</nav>
<!-- 						<div class="col-12"> -->
<!-- 							<div class="CN19-1-header"> -->
<!-- 								<div class="logo"> -->
<%-- 									<img src="${__ctx}/img/logo-1.svg"> --%>
<!-- 								</div> -->
<!-- 								常用網址超連結 -->
<!-- 								<div class="text-right hyperlink"> -->
<!-- 									<a href="#" onClick="window.open('https://www.tbb.com.tw');" style="text-decoration: none"> 臺灣企銀首頁 </a> <strong><font color="#e65827">|</font></strong>  -->
<!-- 									<a href="#" onClick="window.open('https://eatm.tbb.com.tw/');" style="text-decoration: none"> 網路ATM </a><strong><font color="#e65827">|</font></strong> 	 -->
<!-- 									<a href="#" onClick="window.open('https://www.tbb.com.tw/q3.html?');" style="text-decoration: none"> 意見信箱 </a> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
						<!-- 如何申請 頁面 -->
						<div class="col-12 tab-content" id="CN19-how-apply" style="display:none;">
							<div class="ttb-input-block">
								<div class="CN19-caption1">
									壹、網路銀行申請方式及服務功能
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>申請方式</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>線上申請：限個人戶</li>
									</ul>
									<div>
										本行晶片金融卡存戶：<br>
 										未曾臨櫃申請之存戶，可線上申請<font color="#FF6600">查詢</font>服務，請使用<b>本行晶片金融卡 + 晶片讀卡機</b> ，直接線上申請網路銀行，如該晶片金融卡已具備非約定轉帳功能者，得線上申請「金融卡線上申請/取消交易服務功能」，執行新臺幣非約定轉帳交易、繳費稅交易、線上更改通訊地址/電話及使用者名稱/簽入密碼/交易密碼線上解鎖等功能。<br>
										<br>
										本行信用卡正卡客戶（不含商務卡、採購卡、附卡及VISA金融卡）：<br>
 										請您直接線上申請網路銀行，只要輸入個人驗證資料，即可立刻使用網路銀行信用卡相關服務。<br>
 										<br>
									</div>
									<ul>
										<li>臨櫃申請：</li>
									</ul>
									<div>
										法人存戶：<br>
 										請負責人攜帶<b>公司登記證件、身分證、存款印鑑及存摺</b>至開戶行申請網路銀行。<br>
										<br>
										個人存戶：<br>
 										請本人攜帶<b>身分證、存款印鑑及存摺</b>至開戶行申請網路銀行，全行收付戶（即通儲戶），亦可至各營業單位申請。<br>
 										<br>
									</div>
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>服務功能</span>
								</div>
								<div>
									<table id="applyTable" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
										<thead>				
											<tr>
												<!-- 功能比較-->
												<th>功能比較</th>
												<!-- 臨櫃申請-->
	 											<th>臨櫃申請</th>
												<!-- 晶片金融卡線上申請-->
												<th>晶片金融卡線上申請</th>
												<!-- 晶片金融卡線上申請且線上申請交易服務功能 -->
												<th>晶片金融卡線上申請且線上申請交易服務功能</th>
												<!-- 信用卡線上申請 -->
												<th>信用卡線上申請</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													信用卡帳務查詢
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													信用卡電子帳單
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													補寄信用卡帳單
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													信用卡繳款Email通知
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													存、放款、外匯帳務查詢
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													基金查詢
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													黃金存摺查詢
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													黃金申購/回售及變更
												</td>
												<td>O(註1)</td>
												<td>X</td>
												<td>X</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													轉帳、繳費稅、定存
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													新台幣非約定轉帳(註2)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													基金申購及變更
												</td>
												<td>O</td>
												<td>X</td>
												<td>X</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													申請代扣繳費用
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													掛失服務
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													變更通訊資料(註3)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													使用者名稱/簽入/交易密碼線上解鎖(註4)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													理財試算
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													國內臺幣匯入匯款通知設定
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
										</tbody>
									</table>
									</div>
									<div>
										<b>備註：</b><br>
										<ol>
											<li>臨櫃申請黃金存摺帳戶客戶，需於臨櫃或逕自本行網路銀行申請黃金存摺網路交易功能，始得執行黃金申購/回售及變更等功能。</li>
											<li>倘已線上申請交易功能者，且該晶片金融卡已具備非約定轉帳功能者，並得以本人帳戶之晶片金融卡之主帳號為轉出帳戶，執行新臺幣非約定轉帳交易、繳費稅交易。</li>
											<li>交易機制為「電子簽章」或「晶片金融卡」，即可執行線上更改通訊地址/電話。</li>
											<li>易機制為「電子簽章」或「晶片金融卡」，即可執行線上簽入/交易密碼線上解鎖。</li>
											<li>O：表示可使用功能，X：表示不可使用功能。(原註2)</li>
										</ol>
									</div>
								<div class="CN19-caption1">
									貳、黃金存摺線上申請資格、服務功能及費用收取
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>線上申請黃金存摺帳戶</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>申請資格</li>
									</ul>
									<div>
										年滿20歲之本國人。<br>
 										已申請使用本行網路銀行且已約定新台幣活期性存款（不含支票存款）帳戶為轉出帳號者。<br>
										已申請使用本行晶片金融卡＋晶片讀卡機或電子簽章（憑證載具）。<br>
									</div>
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>線上申請黃金存摺網路交易功能</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>申請資格</li>
									</ul>
									<div>
										年滿20歲之本國人。<br>
 										已申請使用本行網路銀行且已約定新台幣活期性存款（不含支票存款）帳戶為轉出帳號者。<br>
										已申請使用本行晶片金融卡＋晶片讀卡機或電子簽章（憑證載具）。<br>
									</div>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>服務功能</li>
									</ul>
									<div>
										查詢服務：黃金存摺餘額、明細、當日、歷史價格查詢功能。<br> 
 										交易服務：黃金買進、回售、繳納定期扣款失敗手續費功能。<br>
										預約服務：預約買進、回售、取消、查詢功能。<br>
									 	定期定額服務：定期定額申購、定期定額變更、定期定額查詢功能。<br>
									 	線上申請：線上申請黃金存摺帳戶、黃金存摺網路交易功能。<br>
									</div>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>費用收取</li>
									</ul>
									<div>
										線上「申請黃金存摺帳戶」及「定期定額投資」扣帳成功，收取新台幣50元手續費，其餘交易免收。<br>
									</div>
								</div>
							</div>
						</div>
						<!-- 立即申請 頁面 -->
						<div class="col-12 tab-content" id="CN19-now-apply" >
							<div class="ttb-input-block tab-pane fade show active"
								 role="tabpanel" aria-labelledby="nav-home-tab">
								<!-- 帳號區塊-->
								<br>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 網路銀行-->
											網路銀行
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												使用臺灣企銀晶片金融卡 + 讀卡機 
												<input type="radio" name="TYPE" id="" value="0" checked />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												使用臺灣企銀信用卡 
												<input type="radio" name="TYPE" id="" value="1"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 黃金存摺 -->
											黃金存摺
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												申請黃金存摺帳戶(含黃金網路交易功能)
												<input type="radio" name="TYPE" id="" value="3"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												申請黃金網路交易功能—已有臺灣企銀實體黃金存摺者
												<input type="radio" name="TYPE" id="" value="4"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 開戶申請 -->
											開戶申請
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												線上預約開立存款戶
												<input type="radio" name="TYPE" id="" value="5"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												預約開立基金戶
												<input type="radio" name="TYPE" id="" value="6"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 信用卡 -->
											信用卡
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												線上申請信用卡
												<input type="radio" name="TYPE" id="" value="7"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												申請信用卡進度查詢
												<input type="radio" name="TYPE" id="" value="20"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												簽署信用卡分期付款同意書
												<input type="radio" name="TYPE" id="" value="8"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												長期使用循環信用客戶線上申請分期還款
												<input type="radio" name="TYPE" id="" value="9"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												信用卡額度調升
												<input type="radio" name="TYPE" id="" value="19"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 數位存款帳戶 -->
											數位存款帳戶
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												線上開立數位存款帳戶
												<input type="radio" name="TYPE" id="" value="10"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												線上數位存款帳戶補上傳身分證件
												<input type="radio" name="TYPE" id="" value="11"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												修改數位存款帳戶資料
												<input type="radio" name="TYPE" id="" value="12"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												數位存款帳戶晶片金融卡開卡及變更密碼
												<input type="radio" name="TYPE" id="" value="13"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												數位存款帳戶補申請晶片金融卡
												<input type="radio" name="TYPE" id="" value="16"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 隨護神盾 -->
											隨護神盾
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												申請隨護神盾
												<input type="radio" name="TYPE" id="" value="14"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 無卡提款 -->
											無卡提款
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												線上註銷無卡提款功能
												<input type="radio" name="TYPE" id="" value="15"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 掃碼提款 -->
											掃碼提款
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												線上註銷台灣Pay掃碼提款功能
												<input type="radio" name="TYPE" id="" value="17"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 掃碼提款 -->
											<spring:message code= "LB.X2384" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code= "LB.X2385" />
												<input type="radio" name="TYPE" id="" value="18"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
							</div>
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" class="ttb-button btn-flat-gray" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/login','', '')" value="回首頁"/>	
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
							
						</div>
						<div class="CN19-1-main text-left" id="TYPE3REQ" style="display: none;margin: 10px 200px;">
					    <font color="#FF6600" size="5">
					      	<B><span style="FONT-FAMILY: Wingdings;">u</span>本服務需具備下列申請要件：</B><br>
				      	</font><BR>
					    <font size="4">
						      	 一、	年滿20歲之本國人。<br>
  								 二、	已申請使用本行網路銀行且已約定新臺幣活期性存款（不含支票存款）帳戶為轉出帳號者。<br>
   								 三、	已申請使用本行晶片金融卡或電子簽章(憑證载具)。<br>
								如符合要件並已完成本行客戶基本資料及投資屬性測驗問卷(KYC)者，請登入網路銀行繼續操作。<br>
					    </font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE4REQ" style="display: none;margin: 10px 200px;">
					    <font color="#FF6600" size="5">
					      	<B><span style="FONT-FAMILY: Wingdings;">u</span>本服務需具備下列申請要件：</B><br>
				      	</font><BR>
					    <font size="4">
						      	一、		年滿20歲之本國人。<br>
   								二、		已申請使用本行網路銀行且已約定新臺幣活期性存款（不含支票存款）帳戶為轉出帳號者。<br>
								三、		已臨櫃開立黃金存摺帳戶，但尚未申請黃金存摺網路交易。<br>
								四、		已申請使用本行晶片金融卡或電子簽章(憑證载具)。<br>
								符合上述要件並已完成本行客戶基本資料及投資屬性測驗問卷(KYC)者， 請備妥本人之「臺灣企銀晶片金融卡＋讀卡機」，或電子簽章（憑證載具）。<br>
					    </font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE7REQ" style="display: none;margin: 10px 200px;">
					    <font color="#FF6600" size="5">
					      <B><span style="FONT-FAMILY: Wingdings;">u</span>本服務需具備下列申請要件：</B><br>
				      	</font><BR>
					    <font size="4">
						      	  一、  年滿20歲之自然人，且持有本行信用卡正卡或為本行存款戶。<br>
   								  二、  已申請使用本行網路銀行。<br>
   								  三、  已申請使用本行晶片金融卡或電子簽章(憑證載具)。<br>
   								  
					    </font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE8REQ" style="display: none;margin: 10px 200px;">
					    <font color="#FF6600" size="5">
					      <B><span style="FONT-FAMILY: Wingdings;">u</span>本服務需具備下列申請要件：</B><br>
				      	</font><BR>
					    <font size="4">
						      	 一、  持有本行信用卡正卡者。<br>
   								 二、  已申請使用本行網路銀行。<br>
					    </font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE9REQ" style="display: none;margin: 10px 200px;">
					    <font color="#FF6600" size="5">
					      	<B><span style="FONT-FAMILY: Wingdings;">u</span>本服務需具備下列申請要件：</B><br>
				      	</font><BR>
					    <font size="4">
						      	 一、  持有本行信用卡正卡者。<br>
  					 			 二、  已申請使用本行網路銀行。<br>
   								 三、  連續使用循環信用達一年以上且最近一年內於財團法人金融聯合徵信中心無帳款遲繳或信用不良紀錄。<br>
					    </font>
					</div>
					<div id="SUBSUBMIT" style="display: none;margin: auto">
							<input type="button" name="CMCANCEL" id="CMCANCEL" value="取消" class="ttb-button btn-flat-gray">
							<input type="button" name="CMSUBMIT2" id="CMSUBMIT2" value="登入網路銀行" class="ttb-button btn-flat-orange">
					</div>
					</div>
					</form>
						<!-- 說明 -->
						<div id="CN19-description" class="text-left">
							<spring:message code="LB.Description_of_page" />:
							<ol class="list-decimal text-left">
								<li>網路銀行：使用晶片金融卡申請者，限個人存戶申請；信用卡申請者，請使用一般信用卡正卡申請（不含商務卡、採購卡、附卡及VISA金融卡）。查詢<a href="#"  onclick="goPage('${pageContext.request.contextPath}'+'/ONLINE/APPLY/online_apply_menu_sub')">網路銀行申請方式及服務功能</a></li>            
				                <li>黃金存摺：已申請使用<font color=red>本行網路銀行、年滿20歲</font>之本國人,並<font color=red>已約定新台幣活期性存款帳戶為轉出帳號</font>（不含支票存款）及已完成本行客戶基本資料及投資屬性測驗問卷(KYC)者並具臺灣企銀晶片金融卡或電子簽章（憑證載具）者，每家分行限開乙戶，<a href="#"  onclick="goPage('${pageContext.request.contextPath}'+'/ONLINE/APPLY/online_apply_menu_sub','', '')">黃金存摺線上申請資格、服務功能及費用收取</a>。</li>
				                <li>如果您為本行存戶但不符合線上申請條件，請您攜帶身分證、存款印鑑及存摺至往來分行申請。</li>
				                <li>線上申請信用卡：申請者須已申請使用本行網路銀行、年滿20歲且持有本行信用卡正卡或為本行存款戶，並具臺灣企銀晶片金融卡或電子簽章（憑證載具）者。</li>
				                <li>簽署信用卡分期付款同意書：申請者須已申請使用本行網路銀行且持有本行信用卡正卡者。</li>
				                <li>長期使用循環信用客戶線上申請分期還款：申請者須已申請使用本行網路銀行且持有本行信用卡正卡者。</li>
							</ol>
						</div>
					</div>
				</section>
			</main>
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>

</html>