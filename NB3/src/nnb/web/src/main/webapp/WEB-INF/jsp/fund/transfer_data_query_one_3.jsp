<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="RS" value="${transfer_data_query_one.data.RS}"></c:set>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript">
		$(document).ready(function () {
			//列印
			$("#printbtn").click(function () {
				var params = {
						jspTemplateName:		"transfer_data_query_one_3_print",
						jspTitle:				"<spring:message code= "LB.W0976" />",
						TRADEDATE_F: 			"${RS.TRADEDATE_F }",
						DATE_F:					"${RS.DATE_F }",
						CDNO_F: 			"${RS.CDNO_F }",
						TRANSCODE_FUNDLNAME: 	"${RS.TRANSCODE_FUNDLNAME }",
						CRY_CRYNAME: 			"${RS.CRY_CRYNAME }",
						FUNDAMT_F: 				"${RS.FUNDAMT_F }",
						UNIT_F: 				"${RS.UNIT_F }",
						TRANSCRY_CRYNAME: 		"${RS.TRANSCRY_CRYNAME }",
						PRICE1_F: 				"${RS.PRICE1_F }",
						EXRATE_F: 				"${RS.EXRATE_F }",
						redemptionIncome: 		"${RS.redemptionIncome }",
						FCA2_F: 				"${RS.FCA2_F }",
						SHORTTRADEFEE_F: 		"${RS.SHORTTRADEFEE_F }",
						AMT1_F: 				"${RS.AMT1_F }",
						AMT7_F: 				"${RS.AMT7_F }",
						AUTOR_F:				"${RS.AUTOR_F }"
					};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
		});
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 基金交易資料查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0943" /></li>
    <!-- 基金贖回交易資料查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0976" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- 快速選單及主頁內容 -->
	<main class="col-12">
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.W0976" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form method="post" action="" >
			
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<!-- 交易日期 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.Transaction_date" />
									</label>
								</span>
								<span class="input-block">
									${RS.TRADEDATE_F }
								</span>
							</div>
							<!-- 入帳日期 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.The_account_credited_date" />
									</label>
								</span>
								<span class="input-block">
									${RS.DATE_F }
								</span>
							</div>
							<!-- 信託編號-->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0904" />
									</label>
								</span>
								<span class="input-block">
									${RS.CDNO_F }
								</span>
							</div>
							<!-- 基金名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0025" />
									</label>
								</span>
								<span class="input-block">
									${RS.TRANSCODE_FUNDLNAME }
								</span>
							</div>
							<!-- 贖回信託金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0978" />
									</label>
								</span>
								<span class="input-block">
									${RS.CRY_CRYNAME } ${RS.FUNDAMT_F }
								</span>
							</div>
							<!-- 贖回單位數 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0979" />
									</label>
								</span>
								<span class="input-block">
									${RS.UNIT_F }
								</span>
							</div>
							<!-- 贖回單位淨值 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0980" />
									</label>
								</span>
								<span class="input-block">
									${RS.TRANSCRY_CRYNAME } ${RS.PRICE1_F }
								</span>
							</div>
							<!-- 匯率 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.Exchange_rate" />
									</label>
								</span>
								<span class="input-block">
									${RS.EXRATE_F }
								</span>
							</div>
							<!-- 贖回收益 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0981" />
									</label>
								</span>
								<span class="input-block">
									${RS.CRY_CRYNAME } ${RS.redemptionIncome }
								</span>
							</div>
							<!-- 信託管理費 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0982" />
									</label>
								</span>
								<span class="input-block">
									${RS.CRY_CRYNAME } ${RS.FCA2_F }
								</span>
							</div>
							<!-- 短線交易費用 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0975" />
									</label>
								</span>
								<span class="input-block">
									${RS.CRY_CRYNAME } ${RS.SHORTTRADEFEE_F }
								</span>
							</div>
							<!-- 遞延申購手續費 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0984" />
									</label>
								</span>
								<span class="input-block">
									${RS.AMT1_F }
								</span>
							</div>
							<!-- 給付淨額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0985" />
									</label>
								</span>
								<span class="input-block">
									${RS.CRY_CRYNAME } ${RS.AMT7_F }
								</span>
							</div>
							<!-- 自動贖回註記 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0986" />
									</label>
								</span>
								<span class="input-block">
									${RS.AUTOR_F }
								</span>
							</div>
						</div>
<!-- 						<ul class="ttb-result-list result-shift"> -->
<!-- 							<li> -->
<!-- 								交易日期 -->
<%-- 								<h3><spring:message code="LB.Transaction_date" /></h3> --%>
<%-- 								<p>${RS.TRADEDATE_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								入帳日期 -->
<%-- 								<h3><spring:message code="LB.The_account_credited_date" /></h3> --%>
<%-- 								<p>${RS.DATE_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								信託編號 -->
<%-- 								<h3><spring:message code="LB.W0904" /></h3> --%>
<%-- 								<p>${RS.CREDITNO_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								基金名稱 -->
<%-- 								<h3><spring:message code="LB.W0025" /></h3> --%>
<%-- 								<p>${RS.TRANSCODE_FUNDLNAME }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								贖回信託金額 -->
<%-- 								<h3><spring:message code="LB.W0978" /></h3> --%>
<%-- 								<p>${RS.CRY_CRYNAME } ${RS.FUNDAMT_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								贖回單位數 -->
<%-- 								<h3><spring:message code="LB.W0979" /></h3> --%>
<%-- 								<p>${RS.UNIT_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								贖回單位淨值 -->
<%-- 								<h3><spring:message code="LB.W0980" /></h3> --%>
<%-- 								<p>${RS.TRANSCRY_CRYNAME } ${RS.PRICE1_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								匯率 -->
<%-- 								<h3><spring:message code="LB.Exchange_rate" /></h3> --%>
<%-- 								<p>${RS.EXRATE_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								贖回收益 -->
<%-- 								<h3><spring:message code="LB.W0981" /></h3> --%>
<%-- 								<p>${RS.CRY_CRYNAME } ${RS.redemptionIncome }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								信託管理費 -->
<%-- 								<h3><spring:message code="LB.W0982" /></h3> --%>
<%-- 								<p>${RS.CRY_CRYNAME } ${RS.FCA2_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								短線交易費用 -->
<%-- 								<h3><spring:message code="LB.W0975" /></h3> --%>
<%-- 								<p>${RS.CRY_CRYNAME } ${RS.SHORTTRADEFEE_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								遞延申購手續費 -->
<%-- 								<h3><spring:message code="LB.W0984" /></h3> --%>
<%-- 								<p>${RS.AMT1_F }</p> --%>
<!-- 							</li> -->
							
<!-- 							<li> -->
<!-- 								給付淨額 -->
<%-- 								<h3><spring:message code="LB.W0985" /></h3> --%>
<%-- 								<p>${RS.CRY_CRYNAME } ${RS.AMT7_F }</p> --%>
<!-- 							</li> -->
<!-- 							<li> -->
<!-- 								自動贖回註記 -->
<%-- 								<h3><spring:message code="LB.W0986" /></h3> --%>
<%-- 								<p>${RS.AUTOR_F }</p> --%>
<!-- 							</li> -->
<!-- 						</ul> -->
						
						<div>
							<!-- 列印  -->
							<spring:message code="LB.Print" var="printbtn"></spring:message>
							<input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
						</div>
					</div>
				</div>
			</form>
		</section>
		<!-- main-content END -->
	</main>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>