<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
    
<script>
	$(document).ready(function () {
	initFootable();
	$("#printbtn").click(function(){
		var i18n = new Object();
		i18n['jspTitle']='<spring:message code="LB.Historical_Statement_Detail_Inquiry" />'
		var params = {
				"jspTemplateName":"history_onlinePay_print",
				"jspTitle":i18n['jspTitle'],
				"IMGB64":"${history_onlinePay.data.IMGB64}"
		}
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

	});
	//上一頁按鈕
	$("#previous").click(function() {
		initBlockUI();
		$("form").attr("action","${__ctx}/CREDIT/INQUIRY/card_history");
		$("form").submit();
	});
	
	$('#emailbtn').click(function(){
		$('#QUERYTYPE').val("BHWSReissue");
		$('#formId').attr("action","${__ctx}/CREDIT/INQUIRY/card_history_email");
		$("form").submit();
	});
	});
	
</script>
</head>
<body style="background-color: #f2f2f2 !important;">
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 歷史帳單明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Historical_Statement_Detail_Inquiry" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	</div><!-- content row END -->
		
	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.Historical_Statement_Detail_Inquiry" /><!-- 帳單明細查詢 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<input type="hidden" name="FGPERIOD" value="${history_onlinePay.data.FGPERIOD}">
				<input type="hidden" name="CARDTYPE" value="${history_onlinePay.data.CARDTYPE}">
				<input type="hidden" name="BILL_NO" value="${history_onlinePay.data.BILL_NO}">
				<input type="hidden" id="QUERYTYPE" name="QUERYTYPE" value="${history_onlinePay.data.QUERYTYPE}">
				<input type="hidden" id="FUNCTION" name="FUNCTION" value="${history_onlinePay.data.FUNCTION}"> 
				<div class="main-content-block row">
					<div class="col-12">
						<div>
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間  -->
									<p>
										<spring:message code="LB.Inquiry_time" /><!-- 查詢時間  -->：${history_onlinePay.data.CMQTIME}
									</p>
									<p>
										<!-- 查詢期間  -->
										<spring:message code="LB.Inquiry_period" />：<!-- 查詢期間  -->
										<c:if test="${history_onlinePay.data.FGPERIOD == 'CMCURMON'}">
					                    	<spring:message code="LB.This_period" /><!-- 本期 -->
					                    </c:if>
					                    <c:if test="${history_onlinePay.data.FGPERIOD == 'CMLASTMON'}">
					                    	<spring:message code="LB.Previous_period" /><!-- 上期 -->
					                    </c:if>
					                    <c:if test="${history_onlinePay.data.FGPERIOD == 'CMLAST2MON'}">
					                    	<spring:message code="LB.Last_period" /><!-- 上上期 -->
					                    </c:if>
									</p>
								</li>
							</ul>
						</div>

						<table class="table" data-toggle-column="first">
							<thead>
								<tr>
									<th>
										<spring:message code="LB.Content_of_payment" /><!-- 繳款單內容  -->
									</th>
								</tr>
						</thead>
						<tbody style="display:none">
						</tbody>
					</table>
					<div >
						<img src="data:image/png;base64,${history_onlinePay.data.IMGB64}" style="width: 100%; height: auto"/>
					</div>
					<ul class="ttb-result-list">
							<li style="margin-bottom: 10px;">
							<input type="button" class="ttb-sm-btn btn-flat-orange" id="emailbtn" value="<spring:message code="LB.Online_to_email_of_statement" />" onclick=""><!-- EMAIL帳單 -->
							</li>
					</ul >
						
						<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />" /><!-- 列印帳單 -->
					</div>
				</div>
			</form>
		</section>
	</main><!-- main-content END -->

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>