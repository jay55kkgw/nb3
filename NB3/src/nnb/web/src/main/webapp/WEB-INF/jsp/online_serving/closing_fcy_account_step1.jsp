<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		// 確認鍵 click
		processQuery();
	}
	
		// 確認鍵 click
	function processQuery()
	{
			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").submit();
				}
			});
	}
		
	// 轉出帳號onChange
	function onChangeCustacc() {
		uri = '${__ctx}' + "/FCY/TRANSFER/currency_ajax";
		// 不是請選擇才發Ajax問幣別
		if ($("#FYACN").val() != "#") {
			rdata = { ACN: $("#FYACN").val() };
			console.log("creatOutAcn.uri: " + uri);
			console.log("creatOutAcn.rdata: " + rdata);
			var data = fstop.getServerDataEx(uri, rdata, false, onChangeCustaccCallback);
			$("#CRY").addClass("select-input validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D0448" />',CRY,#]]]");
		}
	}
	// 轉出帳號onChange後打Ajax的callback
	function onChangeCustaccCallback(data) {
		console.log("onChangeCustaccCallback data: " + data);
		if (data) {
			// currency_ajax回傳資料型態: List<Map<String, String>>
			console.log("data.json: " + JSON.stringify(data));
			// 先清空原有之"OPTION"內容,並加上一個 "---請選擇---" 欄位
			$('#CRY').empty();
			$("#CRY").append($("<option></option>").attr("value", "#").text("---<spring:message code= "LB.Select"/>---"));
			// 迴圈塞轉出幣別
			data.forEach(function (map) {
				console.log(map);
				$("#CRY").append($("<option></option>").attr("value", map.ADCURRENCY).text(map.ADCURRENCY + " " + map.ADCCYNAME));
			});
		}
	}
	
	//取得轉出帳號幣別餘額資料
	function getACNO_Data(){
		uri = '${__ctx}' + "/FCY/COMMON/getACNO_Currency_Data_aj"
		console.log("getACNO_Data URL >>" + uri);
		//轉出帳號
		var acno = $("#FYACN").find(":selected").val();
		//轉出幣別			
		var cry = $("#CRY").find(":selected").val();
		rdata = {acno: acno,cry: cry};
		console.log("rdata>>" + rdata);
		fstop.getServerDataEx(uri,rdata,true,isShowACNO_Data);
	}
	//顯示轉出帳號幣別餘額資料
	function isShowACNO_Data(data){
//			可用餘額
		console.log("isShowACNO_Data.data"+data);
		if(data && data.result){
			$("#acnoIsShow").show();
			$("#showText").html("");
			var bal = "0.00";
			if(data.data.accno_data) {
				console.log("data.data.accno_data"+data.data.accno_data.BDPIBAL);
				bal = fstop.formatAmt(data.data.accno_data.AVAILABLE); //格式化金額
			}
			console.log("bal :" + bal);
			// 格式化金額欄位
			var showBal = i18nValue['available_balance'] +bal;
			$("#showText").html(showBal);
		}else{
			$("#acnoIsShow").hide();
		}
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 外匯存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0299" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0299" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form autocomplete="off" method="post" id="formId" action="${__ctx}/ACCOUNT/CLOSING/closing_fcy_account_step2">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                             <div class="ttb-message">
                                <span></span>
                            </div>
                              <!-- 請選擇欲結清：<br>外匯活期存款或外匯綜合存款-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.D0447"/></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input validate[funcCall[validate_CheckSelect['<spring:message code= "LB.Account" />',FYACN,#]]]" id="FYACN" name="FYACN" onchange="onChangeCustacc();getACNO_Data()">
												<!-- 請選擇帳號 -->
													<option value="#">----<spring:message code="LB.Select_account" />-----</option>
												<c:forEach var="dataList" items="${ closing_fcy_account_step1.data.REC}">
													<option value='${dataList.ACN}'> ${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								<!-- 請選擇幣別-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.D0448"/></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input validate[funcCall[validate_CheckSelect['<spring:message code= "LB.Currency" />',CRY,#]]]"  name="CRY" id="CRY" size="1" onchange="getACNO_Data()">
												<option value="#">---- <spring:message code="LB.Select"/> -----</option>
											</select>
										</div>
										<div id="acnoIsShow">
											<span id = "showText" class="input-unit "></span>
										</div>
									</span>
								</div>
						</div>
                        <input class="ttb-button btn-flat-orange" type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.Confirm_1"/>" />
                    </div>
                </div>
                <ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page"/></p>
                  	<li><strong style="FONT-WEIGHT: 400"><spring:message code="LB.Closing_Fcy_Account_P1_D1"/></strong> </li>
					<li><strong style="FONT-WEIGHT: 400"><spring:message code="LB.Closing_Fcy_Account_P1_D2"/></strong></li>
					<li><strong style="FONT-WEIGHT: 400"><spring:message code="LB.Closing_Fcy_Account_P1_D3"/></strong></li>    
                </ol>
                </form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>