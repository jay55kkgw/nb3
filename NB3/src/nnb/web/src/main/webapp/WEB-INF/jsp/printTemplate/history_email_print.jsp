<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body  class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	</div>
	<br />
		<table class="print">
			<!-- 								查詢時間 -->
			<tr>
				<td style="text-align: center"><spring:message code= "LB.Inquiry_time" /></td>
				<td>${CMQTIME}</td>
			</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.Id_no" /></td>
					<td>${CUSIDN}</td>
				</tr>
			<tr>
				<td style="text-align: center"><spring:message code= "LB.Mail_address" />
				</td>
				<td>${DPMYEMAIL}</td>
			</tr>
			<tr>
				<td style="text-align: center"><spring:message code= "LB.E-statement_password" /></td>
				<c:if test="${CUSIDNLength =='10'}">
				<td><spring:message code= "LB.Id_or_16" /></td>
				</c:if>
				<c:if test="${CUSIDNLength !='10'}">
				<td><spring:message code= "LB.ENI_or_16" /></td>
				</c:if>
			</tr>
			
		</table>
	<br />
	<br />
	<div style="margin: auto;">
		<div class="text-left">
			<p><spring:message code= "LB.Description_of_page" /></p>
			<ol class="list-decimal text-left">
				<li><spring:message code= "LB.History_P23_D1" /></li>
			</ol>
		</div>
</div>
</body>
</html>