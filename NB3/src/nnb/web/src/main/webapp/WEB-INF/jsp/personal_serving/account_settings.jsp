<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		'${inward_notice_setting.data.DPOVERVIEW}'.split(',').forEach(function(i){
			$("#cho" + i).attr("checked",true);
		});
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			
			addParam();
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
            $("#formId").attr("action","${__ctx}/PERSONAL/SERVING/account_settings_result");
	 	  	$("#formId").submit();
		});
	}

   function addParam()
   {
   	  var strVal = "";
   	  var blnF  = true;
   	  $("input[name='r']:checked").each(function(){
   		 if(blnF){
   			strVal = $(this).val();
   			blnF = false;
   		 }else{
   			strVal += ',' + $(this).val();
   		 }
   	  });
   	  console.log(strVal);
   	  $("#DPOVERVIEW").val(strVal);
   }     
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
	<!-- 帳戶總覽設定 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0959" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
			<!-- 帳戶總覽設定 -->
				<h2><spring:message code= "LB.X0959" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/PERSONAL/SERVING/account_settings_result">
				<input type="hidden" name="DPOVERVIEW"  id="DPOVERVIEW" >
			    <input type="hidden" name="EXECUTEFUNCTION" value="UPDATE">
			    <input type="hidden" name="DPUSERID"  value="${inward_notice_setting.data.CUSIDN}">
			    <input type="hidden" name="COLUMN" value="DPOVERVIEW">
			    <input type="hidden" name="ADOPID" value="N999">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                            <!-- 請選擇欲設定的項目 -->
                                <span><spring:message code="LB.Select_Item_to_be_Set" /></span>
                            </div>
                            
                            <!-- 項目 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                    <!-- 項目 -->
                                        <h4><spring:message code= "LB.D0271" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	
                                        
                                        <label  class="check-block" for="cho1">
                                        <!-- 臺幣存款 -->
                                        	<spring:message code= "LB.X0960" />
											<input id="cho1" checked type="checkbox"  name="r" value="1" disabled>
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    
                                    <div class="ttb-input">
                                        <label class="check-block" for="cho2">
                                        <!-- 外匯存款 -->
                                        	<spring:message code= "LB.X0961" />
											<input id="cho2" type="checkbox" name="r" value="2">
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input">
                                        <label class="check-block" for="cho3">
                                        <!-- 借款 -->
                                        	<spring:message code= "LB.X0962" />
											<input id="cho3"  type="checkbox"   name="r" value="3">
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input">
                                        <label class="check-block" for="cho4">
                                        <!-- 基金 -->
                                        	<spring:message code= "LB.Funds" />
											<input id="cho4" type="checkbox"  name="r" value="4">
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input">
                                        <label class="check-block" for="cho5">
                                        <!-- 信用卡 -->
                                        	<spring:message code= "LB.D0001" />
											<input id="cho5"  type="checkbox"  name="r" value="5">
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input">
                                        <label class="check-block" for="cho6">
                                        <!-- 債票劵 -->
                                        	<spring:message code= "LB.X1211" />
											<input id="cho6"  type="checkbox"  name="r" value="6">
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input">
										<label class="check-block" for="cho7">
										<!-- 黃金存摺 -->
											<spring:message code= "LB.W1428" />
											<input id="cho7"  type="checkbox"  name="r" value="7">
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <!-- 重新輸入 -->
                        <input class="ttb-button btn-flat-gray" name="CMRESET" type="reset" value="<spring:message code= "LB.Re_enter" />" />
                        <!-- 確定 -->
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="submit" value="<spring:message code= "LB.Confirm" />" />
                    </div>
                </div>
				</form>
				<!-- 說明 -->
                <ol class="list-decimal">
                	<p><spring:message code= "LB.Description_of_page" /></p>
                <!-- 『帳戶總覽』功能自動提供臺幣存款歸戶查詢，欲增加查詢項目，請設訂增加項目，惟設定項目愈多，查詢時等待系統回應時間愈長。 -->
                  	<li><spring:message code= "LB.Account_Settings_P1_D1" /></li>
                </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>