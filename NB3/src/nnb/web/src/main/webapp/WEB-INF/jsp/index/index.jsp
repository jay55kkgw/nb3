<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp" %>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

<!-- import 圓餅圖 js -->
<script language="javascript" type="text/javascript" src="${__ctx}/js/jquery.flot.min.js"></script>
<script language="javascript" type="text/javascript" src="${__ctx}/js/jquery.flot.pie.js"></script>


<!-- import 行事曆 js -->
<script language="javascript" type="text/javascript" src="${__ctx}/js/packages/core/main.js"></script>
<script language="javascript" type="text/javascript" src="${__ctx}/js/packages/core/locales-all.js"></script>
<script language="javascript" type="text/javascript" src="${__ctx}/js/packages/interaction/main.js"></script>
<script language="javascript" type="text/javascript" src="${__ctx}/js/packages/daygrid/main.js"></script>
<script language="javascript" type="text/javascript" src="${__ctx}/js/packages/timegrid/main.js"></script>
<script language="javascript" type="text/javascript" src="${__ctx}/js/packages/list/main.js"></script>

<script>
if(typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, '');
	}
} 
</script>
<script language="javascript" type="text/javascript" src="${__ctx}/js/d3.min.js"></script>

<link href='${__ctx}/js/packages/core/main.css' rel='stylesheet' />
<link href='${__ctx}/js/packages/daygrid/main.css' rel='stylesheet' />
<link href='${__ctx}/js/packages/timegrid/main.css' rel='stylesheet' />
<link href='${__ctx}/js/packages/list/main.css' rel='stylesheet' />

<script type="text/javascript">
	var locale = "${pageContext.response.locale}";
	var i18n = new Object();
	
	var eyeFlag = false;
	

	// 畫面呈現
	$(document).ready(function () {
		console.log('ready....................');
		
		var firstlogin = '${sessionScope.firstlogin}';
		console.log('index.firstlogin: ' + firstlogin);
		console.log(firstlogin == 'true');
		
		if (firstlogin == 'true') {
			initData();
			
		} else {
			index_init();
		}
	});
	
	
	function index_init() {
		console.log('index_init....................');
		
		i18n(); // header的切換語系選單
		init(); // 開始查詢資料並完成畫面
	}
	

	function coverSoA(input){
		if(eyeFlag){
			return account2star(input);
		}else{
			return input;
		}
	}
	
	function showEyeClick(){
		eyeFlag = !eyeFlag;

		$( ".accountST" ).each(function() {
			$(this).html(coverSoA($(this).attr("account")));
		});
	}
	
	// 開始查詢資料並完成畫面
	function init(){
		// 我的資產
		setTimeout("getAccounts()", 100);
		
		// 公告訊息--my_bulletin.jsp
		setTimeout("getBulletin()", 200);
		
		// 取得使用者名稱
		setTimeout("getUserName()", 300);
		
		// 取得變更Email提示
		setTimeout("getEmailAlert()", 400);
	}
	
	// 依據 active tab 決定查詢帳戶的類別
	function getAccounts() {
		console.log("getAccounts.now: " + new Date());
		
		showAcctsBlock('NTD');
		getTW_aj(); // 預設顯示臺幣總覽
		
		tabSwitch();
	}
	
	// 總覽頁籤切換事件
	function tabSwitch() {
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		    // here is the new selected tab id
		    var selectedTabId = e.target.id != null ? String(e.target.id).substr(4) : '';
			console.log('tabSwitch: ' + selectedTabId);
			
			showAcctsBlock(selectedTabId);
			
			switch(selectedTabId) {
				case 'NTD':
					getTW_aj();
			  		break;
				case 'FCY':
					getFX_aj();
					break;
				case 'CRD':
					getCRD_aj();
					break;
				case 'FUND':
					getFUND_aj();
					break;
				case 'LOAN':
					getLOAN_aj();
					break;
				default:
			}
			
		});
	}

	// 取得使用者姓名
	function getUserName(){
		console.log("getUserName.now: " + new Date());
		
		uri = '${__ctx}'+"/INDEX/username_aj";
// 		console.log("username_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, showUserName);
	}
	
	// 顯示使用者姓名
	function showUserName(data){
		console.log("showUserName.now: " + new Date());
		
// 		console.log("showUserName.data: " + JSON.stringify(data));
		if(data && data.result){
			$("span[name='username_show']").empty().html(data.data.DPUSERNAME);
		}
		console.log("showUserName.end: " + new Date());
	}
	
	function getEmailAlert() {
		console.log("getEmailAlert.now: " + new Date());
		
		uri = '${__ctx}'+"/INDEX/emailalert_aj";
// 		console.log("username_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, showEmailAlert);
		
	}
	
	// 顯示EMAIL提示
	function showEmailAlert(data){
		console.log("showEmailAlert.now: " + new Date());
		if(data && data.result){
			errorBlock(
					["<spring:message code= 'LB.X0152' />"], 
					["您於  <font color='#FF5733'>"+ data.data.TXDATE+"</font> 申請或變更電子郵件信箱，因尚未執行驗證電子郵件信箱，<br>請於 <font color='#FF5733'>"+ data.data.EXPIREDATE +" </font>前至您的電子郵件信箱  <font color='#3370FF'><u>（"+ data.data.EMAIL +"） </u></font>確認，<br>若需重新寄送確認信件請點擊重新寄送。</li>"],
					null, 
					'關閉', 
					'重新寄送'
			);
			
			// 複寫errorBtn1 事件
			$("#errorBtn1").click(function(e) {
				$('#error-block').hide();
			});
			// 複寫errorBtn2 事件
			$("#errorBtn2").click(function(e) {
				reSendEmail();
				$('#error-block').hide();
			});
		}
		console.log("showEmailAlert.end: " + new Date());
		
	}
	
	function reSendEmail() {
		console.log("reSendEmail.now: " + new Date());
		
		uri = '${__ctx}'+"/INDEX/resendEmail_aj";
// 		console.log("username_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, reSendEmailResult);
		
		console.log("reSendEmail.end: " + new Date());
	}
	
	function reSendEmailResult(data){
		if(data && data.result){
			console.log("resendEmail.end: " + new Date());
		}
	}
	
</script>

</head>
<body>

   	<!-- 使用者基本資料 -->
	<c:if test="${sessionScope.userdata}">
		<%@ include file="../index/index_userdata.jsp"%>
	</c:if>

	<!-- 第一階段首次登入轉檔視窗 -->
	<c:if test="${sessionScope.firstlogin}">
		<%@ include file="../index/first_login.jsp"%>
	</c:if>
	
	<!-- 電子對帳單退件處理 -->
	<c:if test="${showRemailId}">
		<section id="remailid2-block" class="error-block">
			<div class="error-for-message">
				<p class="error-content">
					<!-- 親愛的用戶您好，您上期電子化銀行轉帳交易對帳單遭郵局退件，請您確認通訊地址是否正確，或更改寄送方式為E-MAIL寄送。 -->
					${remailIdText}
				<p>
				<button id="textBtn" class="btn-flat-orange ttb-pup-btn" onclick="$('#remailid2-block').hide();"><spring:message code= "LB.X1572" /></button>
			</div>
		</section>
	</c:if>
	
	<!-- 第二階段首次登入轉檔視窗 -->
	<c:if test="${firstaday}">
		<section id="remind-block" class="error-block" <c:if test="${sessionScope.firstlogin}"> style="display:none"</c:if>  >
			<div class="error-for-message">
				<!-- 親愛的客戶您好： -->
				<p class="error-title" style="text-align: left;"><spring:message code= "LB.X2331" /></p>
				<p class="error-content">
					<!-- 歡迎使用本行之新版網路銀行，本行已將您舊版網路銀行之資料同步移轉至新版網路銀行。為提供更優質服務，相關注意事項說明如下： -->
					<spring:message code= "LB.X2332" />
					
					<ul class="error-content ttb-pup-list">
						<div class="ttb-result-list terms">
							<li>
								<!-- 一、 平行作業期間... -->
								<spring:message code= "LB.X2333" />
							</li>
							<li>
								<!-- 二、舊版網路銀行未來將視新版網路銀行營運狀況，最晚於併行期開始四個月後下架，感謝您的支持與愛護。 -->
								<spring:message code= "LB.X2406" />
							</li>
							<li>
								<!-- 三、新/舊網路銀行雙平台併行期間，所有服務功能皆正常提供，惟「預約交易查詢」及「轉出紀錄查詢」二項功能服務說明如下，不便之處敬請見諒: -->
								<spring:message code= "LB.X2334" />
								<ol>
									<li>
										<!-- (一)預約交易查詢: -->
										<text style="color: red;"><spring:message code= "LB.X2335" /></text>
										<ol  style="color: red;">
											<!-- 1.首次登入新版網路銀行後，預約交易將會從舊版網路銀行移轉到新版網路銀行，請您於新版網路銀行中查詢。 -->
											<li><spring:message code= "LB.X2336" /></li>
											
											<!-- 2.併行期間，若您分別於新、舊版網路銀行執行預約交易，須於原執行的網路銀行中查詢，無法於新/舊網路銀行間相互查詢。 -->
											<li><spring:message code= "LB.X2337" /></li>
											
											<!-- 3.行動銀行的預約交易，請於行動銀行或新版網路銀行中查詢。 -->
											<li><spring:message code= "LB.X2403" /></li>
										</ol>
									</li>
									<li>
										<!-- (二)轉出紀錄查詢:... -->
										<text style="color: red;"><spring:message code= "LB.X2338" /></text>
										<ol  style="color: red;">
											<!-- 併行期間，若您分別於新、舊版網路銀行執行轉出交易，須於原執行的網路銀行中查詢轉出紀錄，無法於新/舊網路銀行間相互查詢。 -->
											<li><spring:message code= "LB.X2339" /></li>
										</ol>
									</li>
								</ol>
							</li>
							<li>
								<!-- 四、全面統一使用新版網路銀行日期時間，本行將另行公告。 -->
								<spring:message code= "LB.X2341" />
							</li>
							<li style="display: inline-flex">
								<!-- 再次感謝您的支持與愛護，新/舊網路銀行併行營運期間，若有造成不便敬請見諒，
      亦歡迎提供寶貴建議或有任何問題請撥本行客服中心專線0800-01-7171撥9，我們將竭誠為您服務。 -->
								<spring:message code= "LB.X2407" />
							</li>
							<li>
<!-- 								臺灣中小企銀行 敬上 -->
								<spring:message code= "LB.X2408" />
							</li>
						</div>
					</ul>
				</p>
				<button id="textBtn" class="btn-flat-orange ttb-pup-btn" onclick="$('#remind-block').hide();"><spring:message code= "LB.X1572" /></button>
			</div>
		</section>
	</c:if>
	
	<section id="text-block" class="error-block" style="display:none">
		<div class="error-for-message">
			<div id="text-info" class="error-content"></div>
			<button id="textBtn" class="btn-flat-orange ttb-pup-btn" onclick="$('#text-block').hide();"><spring:message code= "LB.X1572" /></button>
		</div>
	</section>
	
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
	<!-- 交易機制元件所需畫面 20190919開會討論 移除首頁元件判斷-->
<%-- 	<%@ include file="../component/component_new.jsp"%> --%>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 我的首頁     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.My_home" /></li>
		</ol>
	</nav>

	
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 我的資產 -->
<%-- 				<%@ include file="../index/my_assets.jsp"%> --%>
				<!-- 帳戶總覽 -->
				<!-- 我的首頁 -->
				<h2>
					<spring:message code="LB.My_home" />
				</h2>
				
				<!-- 我的資產 -->
				<ul class="nav nav-tabs account-nav" id="accountTab" role="tablist">
				    <li class="nav-item">
				        <a class="nav-link active" id="tab-NTD" data-toggle="tab" href="#NTD" role="tab" aria-controls="NTD" aria-selected="true"><spring:message code="LB.X2328" /></a>
				    </li>
				    <li class="nav-item">
				        <a class="nav-link" id="tab-FCY" data-toggle="tab" href="#FCY" role="tab" aria-controls="FCY" aria-selected="false"><spring:message code="LB.W1090" /></a>
				    </li>
				    <li class="nav-item">
				        <a class="nav-link" id="tab-CRD" data-toggle="tab" href="#CRD" role="tab" aria-controls="CRD" aria-selected="false"><spring:message code="LB.Credit_Card" /></a>
				    </li>
				    <li class="nav-item">
				        <a class="nav-link" id="tab-FUND" data-toggle="tab" href="#FUND" role="tab" aria-controls="FUND" aria-selected="false"><spring:message code="LB.X2392" /></a>
				    </li>
				    <li class="nav-item">
				        <a class="nav-link" id="tab-LOAN" data-toggle="tab" href="#LOAN" role="tab" aria-controls="LOAN" aria-selected="false"><spring:message code="LB.X2329" /></a>
				    </li>
				</ul>
				<div class="main-content-block container">
					<div class="row" id="myTabContent">
						<!-- 我的資產 -->
						<%@ include file="../index/my_accounts_bg.jsp"%>
						<%@ include file="../index/my_accounts_tw.jsp"%>
						<%@ include file="../index/my_accounts_fx.jsp"%>
						<%@ include file="../index/my_accounts_crd.jsp"%>
						<%@ include file="../index/my_accounts_fund.jsp"%>
						<%@ include file="../index/my_accounts_loan.jsp"%>
				
						<!-- 公告訊息 -->
						<%@ include file="../index/my_accounts_bulletin.jsp"%>

				    </div>
				</div><!-- 我的資產 End -->
				
				
<!-- 				<div class="row"> -->
					<!-- 訊息公告 -->
<%-- 					<%@ include file="../index/my_bulletin.jsp"%> --%>
	
					<!-- 我的行事曆 -->
<%-- 					<%@ include file="../index/my_calendar.jsp"%> --%>
<!-- 				</div> -->

			</section>
		</main>
	</div><!-- content row End -->
	
	<%@ include file="../index/footer.jsp"%>
	
</body>

</html>