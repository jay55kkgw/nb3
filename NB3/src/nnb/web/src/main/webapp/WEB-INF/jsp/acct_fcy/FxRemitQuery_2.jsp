<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>

	<script type="text/javascript">
		$(document).ready(function () {
			init();
		});

		function init() {
			initDataTable(); // 將.table變更為DataTable
			setTimeout("dodatataable()",200);
		}
		
		function dodatataable(){
			var pre="上一頁";
			var next="下一頁";
			if(dt_locale == "zh_CN")
			{
				pre = "上一页";
				next = "下一页";
			}
			else if(dt_locale == "en")
			{
				pre="Previous";
				next="Next";
			}
			jQuery(function($) {
				$('.FxRemitQuery-table').DataTable({
					"columnDefs": [
		            	{ width: "60px", targets: [0]},
						{ width: "110px", targets: [1]}, 
		            ],
		            lengthChange: false,
		            scrollX: true, //水平滾動
		            sScrollX: "99%",
					bPaginate: true, //啟用或禁用分頁
					bFilter: false, //啟用或禁用數據過濾
					bDestroy: true, //如果沒有表與選擇器匹配，則將按照常規構造新的DataTable
					bSort: false, //啟用或禁用列排序
					info: false, //功能控製表信息顯示字段。
					scrollCollapse: true, 
					language:{
						"paginate":{
							"previous":pre,
							"next":next
						}
					}
				});
			});
		}

		function fillData(id) {
			if(id=='NUL'){
				var item = $("#692_ADRMTITEM").val();
				var desc = $("#692_ADRMTDESC").val();
			} else {
				var item = $("#" + id + "_ADRMTITEM").val();
				var desc = $("#" + id + "_ADRMTDESC").val();
			}
			
			
			var cnty;
			var obj_country = self.opener.document.getElementById('COUNTRY');
			if (obj_country == null)
				cnty = '';
			else
				cnty = obj_country.value;
			var obj_custacc = self.opener.document.getElementById('CUSTACC');
			if (obj_custacc != null) {
				var outacc = obj_custacc.value
				if (('${FxRemitQuery_2.data.str_CUSTYPE}' == '1') && (cnty == 'CN') && (id == '510') && (outacc.substring(0, 3) != '893')) {
					if (!confirm('<spring:message code="LB.Confirm009" />!\n\n\n<spring:message code= "LB.Confirm010" />\n\n(<spring:message code= "LB.Confirm011" />)')) {
						//alert('<spring:message code="LB.Alert022-1" />\n<spring:message code= "LB.Alert022-2" />');
						errorBlock(
							null, 
							null,
							['<spring:message code='LB.Alert022-1' />\n<spring:message code= 'LB.Alert022-2' />'], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
						return false;
					}
				}
			}

			var obj_srcfund = self.opener.document.getElementById('SRCFUND');
			if (obj_srcfund != null) {
				obj_srcfund.value = id;
			}
			////匯款分類項目
			var obj_srcfunddesc = self.opener.document.getElementById('SRCFUNDDESC');
			var show_srcfunddesc = self.opener.document.getElementById('SHOW_SRCFUNDDESC');
			if (obj_srcfunddesc != null) {
				if(id=='NUL'){
					obj_srcfunddesc.value = '<spring:message code="LB.X2462" />' + '-' + item;
				} else {
					obj_srcfunddesc.value = id + '-' + item;
				}
			}
			if(show_srcfunddesc !=null){
				if(id=='NUL'){
					show_srcfunddesc.innerHTML = '<spring:message code="LB.X2462" />' + '-' + item;
				} else {
					show_srcfunddesc.innerHTML = id + '-' + item;
				}
			}
			
			//匯款分類說明				
			var obj_desc = self.opener.document.getElementById('FXRMTDESC');
			var show_desc = self.opener.document.getElementById('SHOW_FXRMTDESC');
			if (obj_desc != null) {
				obj_desc.value = desc;
			}
			if(show_desc!=null){
				show_desc.innerHTML = desc;
			}
			self.close();
		}
		
		var index;
		
		//加入常用選單
		function editMenu(type, rmtid, index1, rmttype) {
			var uri = '${__ctx}' + "/FCY/COMMON/fxRemitQueryAj"
			index = index1;
			var rdata = { TYPE: type, ADRMTID: rmtid, index: index, ADRMTTYPE: rmttype };
			var data = fstop.getServerDataEx(uri, rdata, false, callback);
		}

		function callback(data) {
			if (data != null && data.result == true) {
				if (data.data.TYPE == 'A') {
					console.log('callback1 >>>>' + data.data);
					var addIndex = "Add_" + index;
					console.log('callback2 addIndex >>>>' + addIndex);
					$("#" + addIndex).prop("disabled", true);
					$("#" + addIndex).removeClass("btn-flat-orange");
					$("#" + addIndex).addClass("btn-flat-gray");
				}
			}

		}
	</script>

</head>

<body>
	<main class="col-12">
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<form method="post" id="formId" action="">
				<h2>
					<spring:message code="LB.X1467" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<ul class="ttb-result-list"></ul>
						<table class="stripe table-striped ttb-table FxRemitQuery-table" data-toggle-column="first">
							<thead>
								<tr>
									<!-- 分類編號 -->
									<th>
										<spring:message code="LB.X1567" />
									</th>
									<!-- 項目 -->
									<th>
										<spring:message code="LB.D0271" />
									</th>
									<!-- 說明 -->
									<th>
										<spring:message code="LB.Description_of_page" />
									</th>
									<!-- 常用選單 -->
									<th>
										<spring:message code="LB.Common_menu" />
									</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach varStatus="loop" var="dataList" items="${FxRemitQuery_2.data.REC}">
								<c:if test="${dataList.ADRMTID =='692'}">
									<tr>
										<td style="color: #007bff">
<% 
    pageContext.setAttribute("dot", "\'");   
%>
                                				<c:set var="ADRMTITEM_replace" value="${ fn:replace( dataList.ADRMTITEM, '\"', dot) }" />
                                				<c:set var="ADRMTDESC_replace" value="${ fn:replace( dataList.ADRMTDESC, '\"', dot) }" />
											<a href="#"
												onClick='fillData("NUL");'>
												<spring:message code="LB.X2462" />
											</a>
											<input type="hidden" id="${fn:trim(dataList.ADRMTID)}_ADRMTITEM" value="${ADRMTITEM_replace}">
											<input type="hidden" id="${fn:trim(dataList.ADRMTID)}_ADRMTDESC" value="${ADRMTDESC_replace}">
										</td>
										<td>${dataList.ADRMTITEM}</td>
										<td>${dataList.ADRMTDESC}</td>
										<td class="text-center">
											<input class="ttb-sm-btn btn-flat-orange" type="button"
												id="Add_${loop.index}" name="Add_${loop.index}"
												value="<spring:message code="LB.X1469" />"
											onclick="editMenu('A','NUL','${loop.index}','${dataList.ADRMTTYPE}')"/>
										</td>
									</tr>
								</c:if>
								<c:if test="${dataList.ADRMTID !='692'}">
										<c:if test="${dataList.ADRMTID !='693'}">
											<tr>
												<td style="color: #007bff">
<% 
    pageContext.setAttribute("dot", "\'");   
%>
	                                 				<c:set var="ADRMTITEM_replace" value="${ fn:replace( dataList.ADRMTITEM, '\"', dot) }" />
	                                 				<c:set var="ADRMTDESC_replace" value="${ fn:replace( dataList.ADRMTDESC, '\"', dot) }" />
													<a href="#"
														onClick='fillData("${fn:trim(dataList.ADRMTID)}");'>
														${fn:trim(dataList.ADRMTID)}
													</a>
													<input type="hidden" id="${fn:trim(dataList.ADRMTID)}_ADRMTITEM" value="${ADRMTITEM_replace}">
													<input type="hidden" id="${fn:trim(dataList.ADRMTID)}_ADRMTDESC" value="${ADRMTDESC_replace}">
												</td>
												<td>${dataList.ADRMTITEM}</td>
												<td>${dataList.ADRMTDESC}</td>
												<td class="text-center">
													<input class="ttb-sm-btn btn-flat-orange" type="button"
														id="Add_${loop.index}" name="Add_${loop.index}"
														value="<spring:message code="LB.X1469" />"
													onclick="editMenu('A','${fn:trim(dataList.ADRMTID)}','${loop.index}','${dataList.ADRMTTYPE}')"/>
												</td>
											</tr>
										</c:if>
										<!--國內匯出他行 -->
										<c:if test="${dataList.ADRMTID =='693' && FxRemitQuery_2.data.CNTY=='TW' && (FxRemitQuery_2.data.str_ADRMTTYPE=='1'||FxRemitQuery_2.data.str_ADRMTTYPE == '2') }">
											<tr>
												<td style="color: #007bff">
													<a href="#" onclick="fillData('R');">
														693(R)</a>
													<input type="hidden" id="R_ADRMTITEM" value="國內貨款之支付">
													<input type="hidden" id="R_ADRMTDESC" value="國內貨款之支付">
												</td>
												<td>國內貨款之支付</td>
												<td>國內貨款之支付</td>
												<td>
													<input class="ttb-sm-btn btn-flat-orange" type="button"
														id="Add_${loop.index+1}" name="Add_${loop.index+1}"
														value="<spring:message code="LB.X1469" />"
													onclick="editMenu('A','R','${loop.index+1}','${FxRemitQuery_2.data.str_ADRMTTYPE}')"/>
												</td>
											</tr>
											<tr>
												<td style="color: #007bff">
													<a href="#" onclick="fillData('S');">
														693(S)</a>
													<input type="hidden" id="S_ADRMTITEM" value="國內外幣保單、基金、債券等投資款項之支付">
													<input type="hidden" id="S_ADRMTDESC" value="國內外幣保單、基金、債券等投資款項之支付">
												</td>
												<td>國內外幣保單、基金、債券等投資款項之支付</td>
												<td>國內外幣保單、基金、債券等投資款項之支付</td>
												<td>
													<input class="ttb-sm-btn btn-flat-orange" type="button"
														id="Add_${loop.index+2}" name="Add_${loop.index+2}"
														value="<spring:message code="LB.X1469" />"
													onclick="editMenu('A','S','${loop.index+2}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
												</td>
											</tr>
											<tr>
												<td style="color: #007bff"><a href="#" onclick="fillData('T');">
														693(T)</a>
													<input type="hidden" id="T_ADRMTITEM" value="國內贍家、捐贈、繼承等移轉支出">
													<input type="hidden" id="T_ADRMTDESC" value="國內贍家、捐贈、繼承等移轉支出">
												</td>
												<td>國內贍家、捐贈、繼承等移轉支出</td>
												<td>國內贍家、捐贈、繼承等移轉支出</td>
												<td>
													<input class="ttb-sm-btn btn-flat-orange" type="button"
														id="Add_${loop.index+3}" name="Add_${loop.index+3}"
														value="<spring:message code="LB.X1469" />"
													onclick="editMenu('A','T','${loop.index+3}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
												</td>
											</tr>
											<tr>
												<td style="color: #007bff"><a href="#" onclick="fillData('L');">
														693(L)</a>
													<input type="hidden" id="L_ADRMTITEM" value="國內外幣借貸之支出">
													<input type="hidden" id="L_ADRMTDESC" value="國內外幣借貸之支出">
												</td>
												<td>國內外幣借貸之支出</td>
												<td>國內外幣借貸之支出</td>
												<td>
													<input class="ttb-sm-btn btn-flat-orange" type="button"
														id="Add_${loop.index+4}" name="Add_${loop.index+4}"
														value="<spring:message code="LB.X1469" />"
													onclick="editMenu('A','L','${loop.index+4}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
												</td>
											</tr>
											<tr>
												<td style="color: #007bff"><a href="#" onclick="fillData('P');">
														693(P)</a>
													<input type="hidden" id="P_ADRMTITEM" value="國內服務之支出">
													<input type="hidden" id="P_ADRMTDESC" value="國內服務之支出">
												</td>
												<td>國內服務之支出</td>
												<td>國內服務之支出</td>
												<td>
													<input class="ttb-sm-btn btn-flat-orange" type="button"
														id="Add_${loop.index+5}" name="Add_${loop.index+5}"
														value="<spring:message code="LB.X1469" />"
													onclick="editMenu('A','P','${loop.index+5}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
												</td>
											</tr>
											<c:if test="${FxRemitQuery_2.data.REMTYPE =='5'}">
												<tr>
													<td style="color: #007bff"><a href="#" onclick="fillData('SPA');">
															693(空白)</a>
														<input type="hidden" id="SPA_ADRMTITEM" value="結購外匯時僅作外匯存款">
														<input type="hidden" id="SPA_ADRMTDESC" value="結購外匯時僅作外匯存款">
													</td>
													<td>結購外匯時僅作外匯存款</td>
													<td>結購外匯時僅作外匯存款</td>
													<td>
	
														<input class="ttb-sm-btn btn-flat-orange" type="button"
															id="Add_${loop.index+6}" name="Add_${loop.index+6}"
															value="<spring:message code="LB.X1469" />"
														onclick="editMenu('A','SPA','${loop.index+6}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
													</td>
												</tr>
											</c:if>
	<%-- 										<tr> --%>
	<%-- 											<td colspan="4">備註：<BR> --%>
	<!-- 												1.匯款人與受款人不同：填報資金用途<BR> -->
	<!-- 												2.匯款人與受款人相同：以資金原始性質填報<BR> -->
	<!-- 												3.匯款人與受款人相同，細分類方可選擇空白 -->
	<%-- 											</td> --%>
	<%-- 										</tr> --%>
											<!-- //國內他行匯入款 -->
											<c:if test="${FxRemitQuery_2.data.CNTY != 'TW'&& FxRemitQuery_2.data.str_ADMKINDID == '02'}">
												<tr>
													<td style="color: #007bff"><a href="#" onclick="fillData('R');">
															693(R)</a>
														<input type="hidden" id="R_ADRMTITEM" value="國內貨款之收入">
														<input type="hidden" id="R_ADRMTDESC" value="國內貨款之收入">
													</td>
													<td>國內貨款之收入</td>
													<td>國內貨款之收入</td>
													<td>
														<input class="ttb-sm-btn btn-flat-orange" type="button"
															id="Add_${loop.index+7}" name="Add_${loop.index+7}"
															value="<spring:message code="LB.X1469" />"
														onclick="editMenu('A','R','${loop.index+7}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
													</td>
												</tr>
												<tr>
													<td style="color: #007bff"><a href="#" onclick="fillData('S');">
															693(S)</a>
														<input type="hidden" id="S_ADRMTITEM" value="國內外幣保單、基金、債券等投資款項(含孳息等收益)之收入">
														<input type="hidden" id="S_ADRMTDESC" value="國內外幣保單、基金、債券等投資款項(含孳息等收益)之收入">
													</td>
													<td>國內外幣保單、基金、債券等投資款項(含孳息等收益)之收入</td>
													<td>國內外幣保單、基金、債券等投資款項(含孳息等收益)之收入</td>
													<td>
														<input class="ttb-sm-btn btn-flat-orange" type="button"
															id="Add_${loop.index+8}" name="Add_${loop.index+8}"
															value="<spring:message code="LB.X1469" />"
														onclick="editMenu('A','S','${loop.index+8}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
													</td>
												</tr>
												<tr >
													<td style="color: #007bff"><a href="#" onclick="fillData('T');">
															693(T)</a>
														<input type="hidden" id="T_ADRMTITEM" value="國內贍家、捐贈、繼承等移轉之收入">
														<input type="hidden" id="T_ADRMTDESC" value="國內贍家、捐贈、繼承等移轉之收入">
													</td>
													<td>國內贍家、捐贈、繼承等移轉之收入</td>
													<td>國內贍家、捐贈、繼承等移轉之收入</td>
													<td>
														<input class="ttb-sm-btn btn-flat-orange" type="button"
															id="Add_${loop.index+9}" name="Add_${loop.index+9}"
															value="<spring:message code="LB.X1469" />"
														onclick="editMenu('A','T','${loop.index+9}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
													</td>
												</tr>
												<tr >
													<td style="color: #007bff"><a href="#" onclick="fillData('L');">
															693(L)</a>
														<input type="hidden" id="L_ADRMTITEM" value="國內外幣借貸之收入">
														<input type="hidden" id="L_ADRMTDESC" value="國內外幣借貸之收入">
													</td>
													<td>國內外幣借貸之收入</td>
													<td>國內外幣借貸之收入</td>
													<td>
														<input class="ttb-sm-btn btn-flat-orange" type="button"
															id="Add_${loop.index+10}" name="Add_${loop.index+10}"
															value="<spring:message code="LB.X1469" />"
														onclick="editMenu('A','L','${loop.index+10}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
													</td>
												</tr>
												<tr >
													<td style="color: #007bff"><a href="#" onclick="fillData('P');">
															693(P)</a>
														<input type="hidden" id="P_ADRMTITEM" value="國內服務之收入">
														<input type="hidden" id="P_ADRMTDESC" value="國內服務之收入">
													</td>
													<td>國內服務之收入</td>
													<td>國內服務之收入</td>
													<td>
														<input class="ttb-sm-btn btn-flat-orange" type="button"
															id="Add_${loop.index+11}" name="Add_${loop.index+11}"
															value="<spring:message code="LB.X1469" />"
														onclick="editMenu('A','P','${loop.index+11}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
													</td>
												</tr>
												<c:if test="${FxRemitQuery_2.data.REMTYPE =='5'}">
													<tr>
														<td style="color: #007bff"><a href="#" onclick="fillData('SPA');">
																693(空白)</a>
															<input type="hidden" id="SPA_ADRMTITEM" value="結購外匯時僅作外匯存款">
															<input type="hidden" id="SPA_ADRMTDESC" value="結購外匯時僅作外匯存款">
														</td>
														<td>結購外匯時僅作外匯存款</td>
														<td>結購外匯時僅作外匯存款</td>
														<td>
															<input class="ttb-sm-btn btn-flat-orange" type="button"
																id="Add_${loop.index+12}" name="Add_${loop.index+12}"
																value="<spring:message code="LB.X1469" />"
															onclick="editMenu('A','SPA','${loop.index+12}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
														</td>
													</tr>
												</c:if>
	<%-- 											<tr> --%>
	<%-- 												<td colspan="4">備註：<BR> --%>
	<!-- 													1.匯款人與受款人不同：填報資金用途<BR> -->
	<!-- 													2.匯款人與受款人相同：以外匯資金原始來源性質填報<BR> -->
	<!-- 													3.匯款人與受款人相同，細分類方可選擇空白 -->
	<%-- 												</td> --%>
	<%-- 											</tr> --%>
											</c:if>
										</c:if>
									</c:if>
								</c:forEach>
									
								<c:if test="${FxRemitQuery_2.data.SHOWOFF == '1'}">
										<tr >
											<td style="color: #007bff"><a href="self" onclick="fillData('NUL');"><spring:message code="LB.X2462" /></a>
												<input type="hidden" id="NUL_ADRMTITEM" value="<spring:message code="LB.X2463" />">
												<input type="hidden" id="NUL_ADRMTDESC" value="<spring:message code="LB.X2464" />">
											</td>
											<td><spring:message code="LB.X2463" /></td>
											<td><spring:message code="LB.X2464" /></td>
											<td>
												<input class="ttb-sm-btn btn-flat-orange" type="button"
													id="Add_${fn:length(FxRemitQuery_2.data.REC)}" name="Add_${fn:length(FxRemitQuery_2.data.REC)}"
													value="<spring:message code="LB.X1469" />"
													onclick="editMenu('A','NUL','${fn:length(FxRemitQuery_2.data.REC)}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
											</td>
										</tr>
								</c:if>
								<c:if test="${FxRemitQuery_2.data.SHOWOFF == '2'}">
										<tr>
											<td style="color: #007bff"><a href="self" onclick="fillData('NUL','<spring:message code="LB.X2465" />','<spring:message code="LB.X2466" />');"><spring:message code="LB.X2462" /></a></td>
											<td><spring:message code="LB.X2465" /></td>
											<td><spring:message code="LB.X2466" /></td>
											<td>
												<input class="ttb-sm-btn btn-flat-orange" type="button"
													id="Add_${${fn:length(FxRemitQuery_2.data.REC)}}" name="Add_${fn:length(FxRemitQuery_2.data.REC)}"
													value="<spring:message code="LB.X1469" />"
												onclick="editMenu('A','NUL','${fn:length(FxRemitQuery_2.data.REC)}','${FxRemitQuery_2.data.str_ADRMTTYPE}')">
											</td>
										</tr>
								</c:if>
								
							<tbody>
						</table>
						
						<!-- 					回上一頁 -->
						<input class="ttb-button btn-flat-orange" type="button" name="TransactionSubmit"
							value="<spring:message code="LB.Back_to_previous_page" />" onclick="history.back()"/>
					</div>
				</div>
				<div class="text-left">
                            <!-- 		說明： -->
                            <ol class="description-list list-decimal">
                            	<p><spring:message code="LB.Description_of_page" /></p>
<!--                                 <li>匯款人與受款人不同：填報資金用途</li> -->
                                <li><spring:message code="LB.FxRemitQuery_P1_D1" /></li>
<!--                                 <li>匯款人與受款人相同：以外匯資金原始來源性質填報</li> -->
                                <li><spring:message code="LB.FxRemitQuery_P1_D2" /></li>
<!--                                 <li>匯款人與受款人相同，細分類方可選擇空白</li> -->
                                <li><spring:message code="LB.FxRemitQuery_P1_D3" /></li>
                            </ol>
                        </div>
			</form>
		</section>
	</main> <!-- 		main-content END -->
</body>

</html>