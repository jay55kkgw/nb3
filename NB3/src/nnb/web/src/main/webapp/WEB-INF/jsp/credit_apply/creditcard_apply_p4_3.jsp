<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../__import_head_tag.jsp" %>
    <%@ include file="../__import_js.jsp" %>
    <!-- 交易機制所需JS -->
    <script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <!--舊版驗證-->
    <script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
    <style type="text/css">
        input[type="file"] {
            overflow: hidden;
        }
    </style>
    <script type="text/JavaScript">
        var isTimeout = "${not empty sessionScope.timeout}";
        var countdownObjheader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
        var countdownSecHeader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
        $(document).ready(function () {
            // HTML載入完成後開始遮罩
            setTimeout("initBlockUI()", 10);
            // 開始查詢資料並完成畫面
            setTimeout("init()", 20);
            // 解遮罩
            setTimeout("unBlockUI(initBlockId)", 500);
            //time out
            timeLogout();
        });

        // 畫面初始化
        function init() {
            console.log("${result_data.data.CPRIMBIRTHDAYshow}");
            uploadSetting();
            $("#CMSUBMIT").click(function (e) {
                console.log("submit~~");
                //20220223 更新  新戶也不檢查是否有上傳證明 , 故註解
                //20220302 再更新 檢核加回來
                var CFU = '${result_data.data.CFU2}';
                if (CFU == '1') {
                    if ($("#FILE1").val() == "") {
                        errorBlock(null, null, ["<spring:message code='LB.X1074' />"],
                            '<spring:message code= "LB.Quit" />', null);
// 				alert("<spring:message code='LB.X1074' />");
                        return false;
                    }
                    if ($("#FILE2").val() == "") {
                        errorBlock(null, null, ["<spring:message code='LB.X1075' />"],
                            '<spring:message code= "LB.Quit" />', null);
// 				alert("<spring:message code='LB.X1075' />");
                        return false;
                    }
                }

                initBlockUI();
                $("#formId").attr("action", "${__ctx}/CREDIT/APPLY/apply_creditcard_p5");
                $("#formId").submit();
            });
            $("#CMBACK").click(function (e) {
                $("#formId").validationEngine('detach');
                console.log("submit~~");
                $("#formId").attr("action", "${__ctx}/CREDIT/APPLY/apply_creditcard_p4_2");
                $("#back").val('Y');

                initBlockUI();
                $("#formId").submit();
            });
        }

        function openMenu() {
            var main = document.getElementById("main");
            window.open('${__ctx}/public/OpenAccount.pdf');
        }

        function onUpLoad(fileFilter, imgFilter, type, postFile) {
            var BD = '${result_data.data.back}';
            if (document.getElementById(type + "_error")) {
                $("#" + type + "_error").remove();
            }

            var fileData = new FormData();

            var file1 = $(fileFilter).get(0);
            if (file1.files.length > 0) {
                fileData.append("picFile", file1.files[0]);
            }
            fileData.append("type", type);
            fileData.append("oldPath", $(postFile).val());
            fileData.append("filetime", '${result_data.data.filetime}');
            if (file1.files.length == 0) {
//         alert("<spring:message code= "LB.Alert052" />");
                $(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'><spring:message code='LB.Alert052' /></p>");
                return;
            }

            $.ajax({
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_zip_fileupload",
                type: "POST",
                contentType: false,  // Not to set any content header
                processData: false,  // Not to process data
                data: fileData,
                success: function (res) {
                    console.log(res);
                    if (res.data.validated) {
//                 alert("檔案匯入成功");   
                        $(imgFilter).attr('src', "${__ctx}/upload/" + res.data.url + "?timestamp=" + new Date().getTime());
                        $(postFile).val(res.data.url);
                        $(imgFilter + "_close_btn").show();
                    } else {
//             	alert(res.data.summary);  
                        $(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
                        $(imgFilter + "_close_btn").hide();
                    }
                }
            });
        }

        //拖移用
        function onUpLoadByDrop(file1, imgFilter, type, postFile) {
            var BD = '${result_data.data.back}';
            if (document.getElementById(type + "_error")) {
                $("#" + type + "_error").remove();
            }

            var fileData = new FormData();
            if (file1.length > 0) {
                fileData.append("picFile", file1[0]);
            }
            fileData.append("type", type);
            fileData.append("oldPath", $(postFile).val());
            fileData.append("filetime", '${result_data.data.filetime}');
            if (file1.length == 0) {
                $(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'><spring:message code='LB.Alert052' /></p>");
                return;
            }

            $.ajax({
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_zip_fileupload",
                type: "POST",
                contentType: false,  // Not to set any content header
                processData: false,  // Not to process data
                data: fileData,
                success: function (res) {
                    console.log(res);
                    if (res.data.validated) {
                        $(imgFilter).attr('src', "${__ctx}/upload/" + res.data.url + "?timestamp=" + new Date().getTime());
                        $(postFile).val(res.data.url);
                        $(imgFilter + "_close_btn").show();
                    } else {
//             	alert(res.data.summary);  
                        $(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
                        $(imgFilter + "_close_btn").hide();
                    }
                }
            });
        }

        //移除上傳圖片
        function deleteUpLoad(imgFilter, postFile, orgImg) {
            console.log($(postFile).val());
            $.ajax({
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_fileupload_delete",
                type: "POST",
                data: {
                    Path: $(postFile).val()
                },
                success: function (res) {
                    console.log(res);
                    if (res.data.sessulte) {
                        $(imgFilter).attr('src', "${__ctx}/img/" + orgImg + ".svg");
                        $(postFile).val("");
                        $(imgFilter + "_close_btn").hide();
                    } else {
//             	alert(res.data.message);    
                        $(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
                    }
                }
            });

        }

        function uploadSetting() {

            $('#img1').on("dragenter", function (e) {
                console.log('dragenter');
                e = e || window.event;
                e.preventDefault();
                e.returnValue = false;
            });
            $('#img1').on('dragover', function (e) {
                console.log('dragover');
                e = e || window.event;
                e.preventDefault();
                e.returnValue = false;
            })
            $('#img1').on('drop', function (e) {
                e = e || window.event;
                e.stopPropagation();
                e.preventDefault();
                var file = e.originalEvent.dataTransfer.files;
                onUpLoadByDrop(file, '#img1', '1', '#FILE1');
            })
            $('#img1').click(function (e) {
                $("#file1").click();
            })
            $("#file1").change("change", function (e) {
                onUpLoad('#file1', '#img1', '1', '#FILE1');
            });
            if ("${result_data.data.FILE1}" != "") {
                $('#img1').attr('src', "${__ctx}/upload/${result_data.data.FILE1}?timestamp=" + new Date().getTime());
                $("#img1_close_btn").show();
            } else {
                $("#img1_close_btn").hide();
            }

            $('#img2').on("dragenter", function (e) {
                console.log('dragenter');
                e = e || window.event;
                e.preventDefault();
                e.returnValue = false;
            });
            $('#img2').on('dragover', function (e) {
                console.log('dragover');
                e = e || window.event;
                e.preventDefault();
                e.returnValue = false;
            })
            $('#img2').on('drop', function (e) {
                e = e || window.event;
                e.stopPropagation();
                e.preventDefault();
                var file = e.originalEvent.dataTransfer.files;
                onUpLoadByDrop(file, '#img2', '2', '#FILE2');
            })
            $('#img2').click(function (e) {
                $("#file2").click();
            })
            $("#file2").change("change", function (e) {
                onUpLoad('#file2', '#img2', '2', '#FILE2');
            });
            if ("${result_data.data.FILE2}" != "") {
                $('#img2').attr('src', "${__ctx}/upload/${result_data.data.FILE2}?timestamp=" + new Date().getTime());
                $("#img2_close_btn").show();
            } else {
                $("#img2_close_btn").hide();
            }

            $('#img3').on("dragenter", function (e) {
                console.log('dragenter');
                e = e || window.event;
                e.preventDefault();
                e.returnValue = false;
            });
            $('#img3').on('dragover', function (e) {
                console.log('dragover');
                e = e || window.event;
                e.preventDefault();
                e.returnValue = false;
            })
            $('#img3').on('drop', function (e) {
                e = e || window.event;
                e.stopPropagation();
                e.preventDefault();
                var file = e.originalEvent.dataTransfer.files;
                onUpLoadByDrop(file, '#img3', '3', '#FILE3');
            })
            $('#img3').click(function (e) {
                $("#file3").click();
            })
            $("#file3").change("change", function (e) {
                onUpLoad('#file3', '#img3', '3', '#FILE3');
            });
            if ("${result_data.data.FILE3}" != "") {
                $('#img3').attr('src', "${__ctx}/upload/${result_data.data.FILE3}?timestamp=" + new Date().getTime());
                $("#img3_close_btn").show();
            } else {
                $("#img3_close_btn").hide();
            }

            $('#img4').on("dragenter", function (e) {
                console.log('dragenter');
                e = e || window.event;
                e.preventDefault();
                e.returnValue = false;
            });
            $('#img4').on('dragover', function (e) {
                console.log('dragover');
                e = e || window.event;
                e.preventDefault();
                e.returnValue = false;
            })
            $('#img4').on('drop', function (e) {
                e = e || window.event;
                e.stopPropagation();
                e.preventDefault();
                var file = e.originalEvent.dataTransfer.files;
                onUpLoadByDrop(file, '#img4', '4', '#FILE4');
            })
            $('#img4').click(function (e) {
                $("#file4").click();
            })
            $("#file4").change("change", function (e) {
                onUpLoad('#file4', '#img4', '4', '#FILE4');
            });
            if ("${result_data.data.FILE4}" != "") {
                $('#img4').attr('src', "${__ctx}/upload/${result_data.data.FILE4}?timestamp=" + new Date().getTime());
                $("#img4_close_btn").show();
            } else {
                $("#img4_close_btn").hide();
            }

            $('#img5').on("dragenter", function (e) {
                console.log('dragenter');
                e = e || window.event;
                e.preventDefault();
                e.returnValue = false;
            });
            $('#img5').on('dragover', function (e) {
                console.log('dragover');
                e = e || window.event;
                e.preventDefault();
                e.returnValue = false;
            })
            $('#img5').on('drop', function (e) {
                e = e || window.event;
                e.stopPropagation();
                e.preventDefault();
                var file = e.originalEvent.dataTransfer.files;
                onUpLoadByDrop(file, '#img5', '5', '#FILE5');
            })
            $('#img5').click(function (e) {
                $("#file5").click();
            })
            $("#file5").change("change", function (e) {
                onUpLoad('#file5', '#img5', '5', '#FILE5');
            });
            if ("${result_data.data.FILE5}" != "") {
                $('#img5').attr('src', "${__ctx}/upload/${result_data.data.FILE5}?timestamp=" + new Date().getTime());
                $("#img5_close_btn").show();
            } else {
                $("#img5_close_btn").hide();
            }
        }

        function showwhat() {
            $("#financial").show();
        }

        // function backData(){
        // 	var BD = '${result_data.data.back}';
        // 	if(BD == 'Y'){
        // 		$("#img1").src();
        // 	}
        // }

        function timeLogout() {
            // 刷新session
            var uri = '${__ctx}/login_refresh';
            console.log('refresh.uri: ' + uri);
            var result = fstop.getServerDataEx(uri, null, false, null);
            console.log('refresh.result: ' + JSON.stringify(result));
            // 初始化登出時間
            $("#countdownheader").html(parseInt(countdownSecHeader) + 1);
            $("#countdownMin").html("");
            $("#mobile-countdownMin").html("");
            $("#countdownSec").html("");
            $("#mobile-countdownSec").html("");
            // 倒數
            startIntervalHeader(1, refreshCountdownHeader, []);
        }

        function refreshCountdownHeader() {
            // timeout剩餘時間
            var nextSec = parseInt($("#countdownheader").html()) - 1;
            $("#countdownheader").html(nextSec);

            // 提示訊息--即將登出，是否繼續使用
            if (nextSec == 120) {
                initLogoutBlockUI();
            }
            // timeout
            if (nextSec == 0) {
                // logout
                fstop.logout('${__ctx}' + '/logout_aj', '${__ctx}' + '/timeout_logout');
            }
            if (nextSec >= 0) {
                // 倒數時間以分秒顯示
                var minutes = Math.floor(nextSec / 60);
                $("#countdownMin").html(('0' + minutes).slice(-2));
                $("#mobile-countdownMin").html(('0' + minutes).slice(-2));

                var seconds = nextSec - minutes * 60;
                $("#countdownSec").html(('0' + seconds).slice(-2));
                $("#mobile-countdownSec").html(('0' + seconds).slice(-2));
            }
        }

        function startIntervalHeader(interval, func, values) {
            clearInterval(countdownObjheader);
            countdownObjheader = setRepeater(func, values, interval);
        }

        function setRepeater(func, values, interval) {
            return setInterval(function () {
                func.apply(this, values);
            }, interval * 1000);
        }

        /**
         * 初始化logoutBlockUI
         */
        function initLogoutBlockUI() {
            logoutblockUI();
        }

        /**
         * 畫面BLOCK
         */
        function logoutblockUI(timeout) {
            $("#logout-block").show();

            // 遮罩後不給捲動
            document.body.style.overflow = "hidden";

            var defaultTimeout = 60000;
            if (timeout) {
                defaultTimeout = timeout;
            }
        }

        /**
         * 畫面UNBLOCK
         */
        function unLogoutBlockUI(timeoutID) {
            if (timeoutID) {
                clearTimeout(timeoutID);
            }
            $("#logout-block").hide();

            // 解遮罩後給捲動
            document.body.style.overflow = 'auto';
        }

        /**
         *繼續使用
         */
        function keepLogin() {
            unLogoutBlockUI(); // 解遮罩
            timeLogout(); // 刷新倒數計時
        }

    </script>
</head>
<body>
<section id="financial" class="error-block" style="display:none">
    <div class="error-for-message">
        <!-- 財力證明包含哪些？ -->
        <p class="error-title"><spring:message code="LB.X2015"/></p>
        <div class="error-content ttb-result-list terms">
            <ul class="">
                <!-- 近一年各類扣繳憑單 -->
                <li data-num="１."><spring:message code="LB.X2016"/></li>
                <!-- 近6個月薪轉存褶明細含封面 -->
                <li data-num="２."><spring:message code="LB.X2017"/></li>
                <!-- 近一年綜合所得稅結算申報書(或附回執聯之二維條碼申報或網路申報書)加附繳款書。 -->
                <li data-num="３."><spring:message code="LB.X2018"/></li>
            </ul>
        </div>
        <input type="button" id="" value="<spring:message code='LB.X2019' />" class="ttb-button btn-flat-orange "
               onclick=" $('#financial').hide();"/>
    </div>
</section>
<!-- header -->
<header>
    <%@ include file="../index/header_logout.jsp" %>
</header>

<!-- 麵包屑     -->
<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
    <ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
        <li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
        <!-- 申請信用卡     -->
        <li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0666"/></li>
    </ol>
</nav>

<!--左邊menu及登入資訊-->
<div class="content row">
    <%-- 	<%@ include file="../index/menu.jsp"%> --%>
    <!--快速選單及主頁內容-->
    <main class="col-12">
        <!--主頁內容-->
        <section id="main-content" class="container">
            <!--線上申請信用卡 -->
            <h2><spring:message code="LB.D0022"/></h2>
            <div id="step-bar">
                <ul>
                    <li class="finished">信用卡選擇</li><!-- 身份驗證 -->
                    <li class="finished">身份驗證與權益</li><!-- 信用卡與權益 -->
                    <li class="active"><spring:message code="LB.X1967"/></li><!-- 申請資料 -->
                    <li class=""><spring:message code="LB.Confirm_data"/></li><!-- 確認資料 -->
                    <li class=""><spring:message code="LB.X1968"/></li><!-- 完成申請 -->
                </ul>
            </div>
            <!-- timeout -->
            <section id="id-and-fast">
						<span class="id-name">
							<spring:message code="LB.Welcome_1"/>&nbsp;
							<c:if test="${empty result_data.data.CUSNAME}">
                                <span id="username" name="username_show"><spring:message code="LB.X2190"/></span>
                            </c:if>
							<c:if test="${not empty result_data.data.CUSNAME}">
                                <span id="username" name="username_show">${result_data.data.CUSNAME}</span>
                            </c:if>
							&nbsp;<spring:message code="LB.Welcome_2"/>
						</span>
                <div id="id-block">
                    <div>
							<span class="id-time">
								<fmt:setLocale value="${pageContext.response.locale}"/>
								<c:if test="${!empty sessionScope.logindt && !empty sessionScope.logintm}">
                                    <fmt:parseDate var="parseDate"
                                                   value="${sessionScope.logindt} ${sessionScope.logintm}"
                                                   pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <fmt:formatDate value="${parseDate}" dateStyle="full"
                                                    type="both"/>&nbsp;<spring:message code="LB.X2250"/>
                                    <br/>
                                </c:if>

								<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full"/>
								&nbsp;<spring:message code="LB.Login_successful"/>
							</span>
                        <!-- 自動登出剩餘時間 -->
                        <span class="id-time"><spring:message code="LB.X1912"/>
								<span class="high-light ml-1">
									<!-- 分 -->
									<font id="countdownMin"></font>
									:
                                    <!-- 秒 -->
									<font id="countdownSec"></font>
								</span>
							</span>
                    </div>
                    <button type="button" class="btn-flat-orange" onclick="timeLogout()"><spring:message
                            code="LB.X1913"/></button>
                    <button type="button" class="btn-flat-darkgray"
                            onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')"><spring:message
                            code="LB.Logout"/></button>
                </div>
            </section>
            <form method="post" id="formId" enctype="multipart/form-data">
                <input type="hidden" name="FILE1" id="FILE1" value="${result_data.data.FILE1}"/>
                <input type="hidden" name="FILE2" id="FILE2" value="${result_data.data.FILE2}"/>
                <input type="hidden" name="FILE3" id="FILE3" value="${result_data.data.FILE3}"/>
                <input type="hidden" name="FILE4" id="FILE4" value="${result_data.data.FILE4}"/>
                <input type="hidden" name="FILE5" id="FILE5" value="${result_data.data.FILE5}"/>
                <input type="hidden" name="filetime" id="filetime" value="${result_data.data.filetime}"/>
                <input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}"/>
                <input type="hidden" name="back" id="back"/>
                <%-- 				<input type="hidden" name="alldata" id="alldata" value="${result_data.data}"/> --%>
                <!-- 				<input type="hidden" name="ADOPID" value="NA03"/> -->
                <%-- 				<input type="hidden" name="CFU2" value="${result_data.data.requestParam.CFU2}"/><!-- 是否持有本行信用卡1否2是 --> --%>
                <%-- 				<input type="hidden" name="FGTXWAY" value="${result_data.data.requestParam.FGTXWAY}"/> --%>
                <%-- 				<input type="hidden" name="CARDNAME" value="${result_data.data.requestParam.CARDNAME}"/> --%>
                <%-- 				<input type="hidden" name="CARDMEMO" value="${result_data.data.requestParam.CARDMEMO}"/> --%>
                <%-- 				<input type="hidden" name="CN" value="${result_data.data.requestParam.CN}"/> --%>
                <%-- 				<input type="hidden" name="CARDDESC" value="${result_data.data.requestParam.CARDDESC}"/> --%>
                <%-- 				<input type="hidden" name="VARSTR2" value="${result_data.data.requestParam.VARSTR2}"/><!-- 悠遊卡是否同意預設開啟flag --> --%>
                <%-- 				<input type="hidden" name="OLAGREEN1" value="${result_data.data.requestParam.OLAGREEN1}"/> --%>
                <%-- 				<input type="hidden" name="OLAGREEN2" value="${result_data.data.requestParam.OLAGREEN2}"/> --%>
                <%-- 				<input type="hidden" name="OLAGREEN3" value="${result_data.data.requestParam.OLAGREEN3}"/> --%>
                <%-- 				<input type="hidden" name="OLAGREEN4" value="${result_data.data.requestParam.OLAGREEN4}"/> --%>
                <!-- 			    <input type="hidden" id="ECERT" name="ECERT"/>交易機制 -->
                <!-- 			    <input type="hidden" name="STATUS" value="0"/> -->
                <!-- 			    <input type="hidden" name="VERSION" value="10707"/> -->
                <%-- 			    <input type="hidden" name="IP" value="${result_data.data.requestParam.IP}"/> --%>
                <%-- 			    <input type="hidden" name="BRANCHNAME" value="${result_data.data.requestParam.BRHNAME}"/> --%>
                <%-- 			    <input type="hidden" name="CPRIMCHNAME" value="${result_data.data.requestParam.CPRIMCHNAME}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMENGNAME" value="${result_data.data.requestParam.CPRIMENGNAME}"/> --%>
                <%-- 				<input type="hidden" name="CHENAME" value="${result_data.data.requestParam.CHENAME}"/> --%>
                <%-- 				<input type="hidden" name="CHENAMEChinese" value="${result_data.data.requestParam.CHENAMEChinese}"/> --%>
                <%-- 				<input type="hidden" name="CTRYDESC" value="${result_data.data.requestParam.CTRYDESC}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMBIRTHDAY" value="${result_data.data.requestParam.CPRIMBIRTHDAY}"/> --%>
                <%-- 				<input type="hidden" name="MPRIMEDUCATION" value="${result_data.data.requestParam.MPRIMEDUCATION}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMID" value="${result_data.data.requestParam.CUSIDN}"/> --%>
                <%-- 				<input type="hidden" name="CUSIDN" value="${result_data.data.requestParam.CUSIDN}"/> --%>
                <%-- 				<input type="hidden" name="MPRIMMARRIAGE" value="${result_data.data.requestParam.MPRIMMARRIAGE}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMADDR2" value="${result_data.data.requestParam.CPRIMADDR2}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMHOMETELNO2A" value="${result_data.data.requestParam.CPRIMHOMETELNO2A}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMHOMETELNO2B" value="${result_data.data.requestParam.CPRIMHOMETELNO2B}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMADDR" value="${result_data.data.requestParam.CPRIMADDR}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMHOMETELNO1A" value="${result_data.data.requestParam.CPRIMHOMETELNO1A}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMHOMETELNO1B" value="${result_data.data.requestParam.CPRIMHOMETELNO1B}"/> --%>
                <%-- 				<input type="hidden" name="MPRIMADDR1COND" value="${result_data.data.requestParam.MPRIMADDR1COND}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMCELLULANO1" value="${result_data.data.requestParam.CPRIMCELLULANO1}"/> --%>
                <%-- 				<input type="hidden" name="MBILLTO" value="${result_data.data.requestParam.MBILLTO}"/> --%>
                <%-- 				<input type="hidden" name="MCARDTO" value="${result_data.data.requestParam.MCARDTO}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMEMAIL" value="${result_data.data.requestParam.CPRIMEMAIL}"/> --%>
                <%-- 				<input type="hidden" name="VARSTR3" value="${result_data.data.requestParam.VARSTR3}"/> --%>
                <%-- 				<input type="hidden" name="FDRSGSTAFF" value="${result_data.data.requestParam.FDRSGSTAFF}"/> --%>
                <%-- 				<input type="hidden" name="BANKERNO" value="${result_data.data.requestParam.BANKERNO}"/> --%>
                <%-- 				<input type="hidden" name="MEMO" value="${result_data.data.requestParam.MEMO}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMJOBTYPE" value="${result_data.data.requestParam.CPRIMJOBTYPE}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMCOMPANY" value="${result_data.data.requestParam.CPRIMCOMPANY}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMJOBTITLE" value="${result_data.data.requestParam.CPRIMJOBTITLE}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMSALARY" value="${result_data.data.requestParam.CPRIMSALARY}"/> --%>
                <%-- 				<input type="hidden" name="WORKYEARS" value="${result_data.data.requestParam.WORKYEARS}"/> --%>
                <%-- 				<input type="hidden" name="WORKMONTHS" value="${result_data.data.requestParam.WORKMONTHS}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMADDR3" value="${result_data.data.requestParam.CPRIMADDR3}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMOFFICETELNO1A" value="${result_data.data.requestParam.CPRIMOFFICETELNO1A}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMOFFICETELNO1B" value="${result_data.data.requestParam.CPRIMOFFICETELNO1B}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMOFFICETELNO1C" value="${result_data.data.requestParam.CPRIMOFFICETELNO1C}"/> --%>
                <%-- 				<input type="hidden" name="MCASH" value="${result_data.data.requestParam.MCASH}"/> --%>
                <%-- 				<input type="hidden" name="CNOTE4" value="${result_data.data.requestParam.VARSTR2}"/> --%>
                <%-- 				<input type="hidden" name="CNOTE1" value="${result_data.data.requestParam.CNOTE1}"/> --%>
                <%-- 				<input type="hidden" name="CNOTE3" value="${result_data.data.requestParam.CNOTE3}"/> --%>
                <%-- 				<input type="hidden" name="oldcardowner" value="${result_data.data.requestParam.oldcardowner}"/> --%>
                <%-- 				<input type="hidden" name="BIRTHY" value="${result_data.data.requestParam.BIRTHY}"/> --%>
                <%-- 			  	<input type="hidden" name="BIRTHM" value="${result_data.data.requestParam.BIRTHM}"/> --%>
                <%-- 			  	<input type="hidden" name="BIRTHD" value="${result_data.data.requestParam.BIRTHD}"/> --%>
                <%-- 			  	<input type="hidden" name="CTTADR" value="${result_data.data.requestParam.CTTADR}"/> --%>
                <%-- 			  	<input type="hidden" name="PMTADR" value="${result_data.data.requestParam.PMTADR}"/> --%>
                <%-- 			  	<input type="hidden" name="MPRIMEDUCATIONChinese" value="${result_data.data.requestParam.MPRIMEDUCATIONChinese}"/> --%>
                <%-- 				<input type="hidden" name="MPRIMMARRIAGEChinese" value="${result_data.data.requestParam.MPRIMMARRIAGEChinese}"/> --%>
                <%-- 				<input type="hidden" name="MPRIMADDR1CONDChinese" value="${result_data.data.requestParam.MPRIMADDR1CONDChinese}"/> --%>
                <%-- 				<input type="hidden" name="MBILLTOChinese" value="${result_data.data.requestParam.MBILLTOChinese}"/> --%>
                <%-- 				<input type="hidden" name="MCARDTOChinese" value="${result_data.data.requestParam.MCARDTOChinese}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMJOBTITLEChinese" value="${result_data.data.requestParam.CPRIMJOBTITLEChinese}"/> --%>
                <!-- 				<input type="hidden" name="ISSUER" value=""> -->
                <!-- 				<input type="hidden" name="ACNNO" value=""> -->
                <!-- 				<input type="hidden" name="iSeqNo" value=""> -->
                <!-- 				<input type="hidden" name="ICSEQ" value=""> -->
                <!-- 				<input type="hidden" name="TAC" value=""> -->
                <!-- 				<input type="hidden" name="TRMID" value=""> -->
                <!--信用卡PDF會用到-->
                <!-- 				<input type="hidden" name="templatePath" value="/pdfTemplate/Creditcard_Apply2.html"/> -->
                <div class="main-content-block row">
                    <div class="col-12">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <!-- 申請資料 -->
                                <p><spring:message code="LB.X1967"/> (3/3)</p>
                            </div>
                            <!-- 請上傳您的身份證件與財力證明 -->
                            <p class="form-description"><spring:message code="LB.X2020"/></p>
                            <div class="classification-block">
                                <!-- 身分證資料 -->
                                <p><spring:message code="LB.X2021"/></p>
                            </div>
                            <!-- 請上傳您的身分證正背面。 -->
                            <!-- 圖片格式僅允許 -->
                            <p class="form-description"><spring:message code="LB.X2022"/><br/><spring:message
                                    code="LB.D1101"/>：<br/>- <spring:message code="LB.X2023"/>：JPG (JPEG), PNG <br/>-
                                <spring:message code="LB.X2636"/></p>
                            <div class="photo-block">
                                <div>
                                    <img src="${__ctx}/img/upload_id_front.svg" id="img1"/>
                                    <a id="img1_close_btn" class="photo-close-btn"
                                       onClick="deleteUpLoad('#img1', '#FILE1', 'upload_id_front')"></a>
                                    <!-- 身分證正面圖片 -->
                                    <p><spring:message code="LB.D0111"/></p>
                                </div>
                                <div>
                                    <!-- 身分證背面圖片 -->
                                    <img src="${__ctx}/img/upload_id_back.svg" id="img2"/>
                                    <a id="img2_close_btn" class="photo-close-btn"
                                       onClick="deleteUpLoad('#img2', '#FILE2', 'upload_id_back')"></a>
                                    <p><spring:message code="LB.D0118"/></p>
                                </div>
                                <input type="file" name="file1" id="file1"
                                       style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
                                <input type="file" name="file2" id="file2"
                                       style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
                            </div>
                            <div class="classification-block">
                                <!-- 財力證明文件 -->
                                <p><spring:message code="LB.X2024"/></p>
                            </div>
                            <p class="form-description">
                                <!-- 請上傳財力證明文件。 -->
                                <spring:message code="LB.X2025"/><br/>
                                <spring:message code="LB.D1101"/>：<br/>
                                - <spring:message code="LB.X2015"/> <img src="${__ctx}/img/icon-13.svg"
                                                                         onclick="showwhat()"/><br>
                                - <spring:message code="LB.X2023"/>：JPG (JPEG), PNG
                                <br/>- <spring:message code="LB.X2636"/>
                            </p>
                            <div class="photo-block" style="justify-content: flex-start; margin-left: 1rem;">
                                <ol style="list-style-type: decimal;">
                                    <li>
                                        <div>
                                            <img src="${__ctx}/img/upload_financial_front.svg" id="img3"/>
                                            <a id="img3_close_btn" class="photo-close-btn"
                                               onClick="deleteUpLoad('#img3', '#FILE3', 'upload_financial_front')"></a>
                                            <!--財力證明文件 -->
                                            <p></p>
                                        </div>
                                        <input type="file" name="file3" id="file3"
                                               style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
                                    </li>
                                    <li>
                                        <div>
                                            <img src="${__ctx}/img/upload_financial_front.svg" id="img4"/>
                                            <a id="img4_close_btn" class="photo-close-btn"
                                               onClick="deleteUpLoad('#img4', '#FILE4', 'upload_financial_front')"></a>
                                            <!--財力證明文件 -->
                                            <p></p>
                                        </div>
                                        <input type="file" name="file4" id="file4"
                                               style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
                                    </li>
                                    <li>
                                        <div>
                                            <img src="${__ctx}/img/upload_financial_front.svg" id="img5"/>
                                            <a id="img5_close_btn" class="photo-close-btn"
                                               onClick="deleteUpLoad('#img5', '#FILE5', 'upload_financial_front')"></a>
                                            <!--財力證明文件 -->
                                            <p></p>
                                        </div>
                                        <input type="file" name="file5" id="file5"
                                               style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <input type="button" id="CMBACK" value="<spring:message code="LB.X0318" />"
                               class="ttb-button btn-flat-gray"/><!-- 上一步 -->
                        <input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080" />"
                               class="ttb-button btn-flat-orange"/><!-- 下一步 -->
                    </div>
                </div>
            </form>
        </section>
    </main>
</div>
<%@ include file="../index/footer.jsp" %>
</body>
</html>