<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<script type="text/javascript">
	
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);	
			});	
			//
			function init(){
				//改用DataTable
				initDataTable();
				printClick();
			}
			//列印
			function printClick(){
				
				$("#printbtn").click(function(){
					var i18n = new Object();
					i18n['jspTitle']='<spring:message code="LB.FX_Time_Deposit_Detail" />'
					var params = {
						"jspTemplateName":"f_time_deposit_d_print",
						"jspTitle":i18n['jspTitle'],
						"CMQTIME":"${f_time_deposit_details.data.CMQTIME }",
						"COUNT":"${f_time_deposit_details.data.CMRECNUM }",
						"TOTAMT":"${f_time_deposit_details.data.FCYTOTMAT }"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});	
				
			}
			
			//選項
			function formReset() {
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/f_time_deposit.xls");
			 	}else if ($('#actionBar').val()=="txt"){
			 		$("#downloadType").val("TXT");
			 		$("#templatePath").val("/downloadTemplate/f_time_deposit.txt");
			 	}
				$("#formId").attr("target", "${__ctx}/download");
				$("#formId").submit();
	            $('#actionBar').val("");
			}
			function finishAjaxDownload(){
				$("#actionBar").val("");
				unBlockUI(initBlockId);
			}
			 	
			//快速選單
		 	function selectionBar(value,FDPNO,ACN,FGSELECT){
				switch(value){
					case "f_deposit_cancel":
						$('#FDPNO').val(FDPNO);
						$('#ACN530').val(ACN);
						//$('#FGSELECT').attr("disabled",true);
						$('#fastBarAction').attr("action","${__ctx}/FCY/ACCT/TDEPOSIT/f_deposit_cancel");
						$('#fastBarAction').submit();
						//fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/TDEPOSIT/f_deposit_cancela?ACN=' + ACN + '&FDPNO=' + FDPNO,'', '');
						break;
					case "f_renewal_apply":
						$('#FDPNO').val(FDPNO);
						$('#ACN530').val(ACN);
						$('#FGSELECT').val(FGSELECT);
						$('#fastBarAction').attr("action","${__ctx}/FCY/ACCT/TDEPOSIT/f_renewal_apply_step1");
						$('#fastBarAction').submit();
// 						fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/TDEPOSIT/f_renewal_apply','', '');
						break;
				}
			}
			
		 	//快速選單測試新增
		 	//將actionbar select 展開閉合
			$(function(){
		   		$("#click").on('click', function(){
		       	var s = $("#actionBar2").attr('size')==1?8:1
		       	$("#actionBar2").attr('size', s);
		   		});
		   		$("#actionBar2 option").on({
		       		click: function() {
		           	$("#actionBar2").attr('size', 1);
		       		},
		   		});
			});
			function hd2(T){
				var t = document.getElementById(T);
				if(t.style.visibility === 'visible'){
					t.style.visibility = 'hidden';
				}
				else{
					$("div[id^='actionBar2']").each(function() {
						var d = document.getElementById($(this).attr('id'));
						d.style.visibility = 'hidden';
				    });
					t.style.visibility = 'visible';
				}
			}
		</script>
	
</head>
<body>
        <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
	<!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Time_Deposit_Detail" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	
		
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">

		
				<h2><spring:message code="LB.FX_Time_Deposit_Detail" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
					<div class="main-content-block row">
						<div class="col-12"> 
						<ul class="ttb-result-list">
							<!-- 查詢時間 -->
                            <li>
                                <h3><spring:message code="LB.Inquiry_time" />：</h3>
                                <p>${f_time_deposit_details.data.CMQTIME }</p>
                            </li>
                            <!-- 資料總數 -->
                            <li>
                                <h3><spring:message code="LB.Total_records" />：</h3>
                                <p> 
                                	${f_time_deposit_details.data.CMRECNUM }
									<spring:message code="LB.Rows" />
								</p>
                            </li>
                            <!-- 總計金額 -->
                            <li>
                                <h3><spring:message code="LB.Total_amount" />：</h3>
                                <p> 
                                	<c:forEach var="datatotmat" items="${f_time_deposit_details.data.FCYTOTMAT}">
										${datatotmat}<br>
										</c:forEach>
								</p>
                            </li>
                        </ul>
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
						<thead>
							<tr>
								<!-- 帳號 -->
								<th><spring:message code="LB.Account" /></th>
								<!-- 存單號碼 -->
								<th><spring:message code="LB.Certificate_no" /></th>
								<!-- 幣別 -->
								<th><spring:message code="LB.Currency" /></th>
								<!-- 存單金額 -->
								<th><spring:message code="LB.Certificate_amount" /></th>
								<!-- 計息方式/利率(%) -->
								<th><spring:message code="LB.Interest_calculation" /><hr/><spring:message code="LB.Interest_rate1" /></th>
								<!-- 起存日/到期日 -->
								<th><spring:message code="LB.Start_date" /><hr/><spring:message code="LB.Maturity_date" /></th>
								<!-- 利息轉入帳號 -->
								<th><spring:message code="LB.Interest_transfer_to_account" /></th>
								<!-- 自動轉期已轉次數/自動轉期未轉次數 -->
								<th><spring:message code="LB.Automatic_number_of_rotations" /><hr/><spring:message code="LB.Automatic_number_of_unrotated" /></th>
								<!-- 選單 -->
								<th><spring:message code="LB.Quick_Menu" /></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="dataList" items="${f_time_deposit_details.data.REC }" varStatus="data">
								<tr>
									<!-- 帳號 -->
									<td class="text-center">${dataList.ACN }</td>
					                <!-- 存單號碼 -->
					                <td class="text-center">${dataList.FDPNO }</td>
					                <!-- 幣別 -->
					                <td class="text-center">${dataList.CUID }</td>
					                <!-- 存單金額 -->
					                <td class="text-right">${dataList.BALANCE}</td>
					                <!-- 計息方式/利率(%) -->
					                <td class="text-center">${dataList.INTMTH }<hr/>${dataList.ITR }</td>
					                <!-- 起存日/到期日 -->
					                <td class="text-center">${dataList.DPISDT }<hr/>${dataList.DUEDAT }</td>
					                <!-- 利息轉入帳號 -->
					                <td class="text-center">${dataList.TSFACN }</td>
					                <!-- 自動轉期已轉次數/自動轉期未轉次數 -->
					                <td class="text-center">${dataList.ILAZLFTM }<hr/>${dataList.AUTXFTM }</td>
					                <!-- 選單 -->
					                <td class="text-center">
    								<!-- 下拉式選單-->
    								<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')" value="..." />
<%--     								<input type="button" class="d-sm-none d-block" id="click" value="Click" onclick="hd2('actionBar2${data.index}')"/><!-- 快速選單測試 -->				 --%>
									<select class="custom-select d-none d-lg-inline-block fast-select" id="selectionBar" onchange="selectionBar(this.value,'${dataList.FDPNO}','${dataList.ACN}','${dataList.MapValue}');">
										<option value="">-<spring:message code="LB.Select"/>-</option>
										<c:if test="${dataList.OPTIONTYPE =='1' }">
											<!-- 外匯綜存定存解約 -->
											<option value="f_deposit_cancel"><spring:message code="LB.FX_Time_Deposit_Termination" /></option>
    									</c:if>
    									<!-- 外匯定存自動轉期申請/變更 -->
										<option value="f_renewal_apply"><spring:message code="LB.FX_Time_Deposit_Automatic_Rollover_Application_Change" /></option>
									</select>
									<!-- 快速選單測試新增div -->
									<div id="actionBar2${data.index}" class="fast-div-grey" style="visibility: hidden">
		 					        	<div class="fast-div">
											<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${data.index}')">
                              				<p><spring:message code= "LB.X1592" /></p>
											<ul>
												<c:if test="${dataList.OPTIONTYPE =='1' }">
<%--  													<a href="${__ctx}/FCY/ACCT/TDEPOSIT/f_deposit_cancel"><li><spring:message code="LB.FX_Time_Deposit_Termination" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a> --%>
 													<a onclick="selectionBar('f_deposit_cancel','${dataList.FDPNO}','${dataList.ACN}','${dataList.MapValue}');"><li><spring:message code="LB.FX_Time_Deposit_Termination" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
 												</c:if>
<%-- 												<a href="${__ctx}/FCY/ACCT/TDEPOSIT/f_renewal_apply_step1?FDPNO=${dataList.FDPNO}&ACN530=${dataList.ACN}&FGSELECT=${dataList.MapValue}"><li><spring:message code="LB.FX_Time_Deposit_Automatic_Rollover_Application_Change" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a> --%>
												<a onclick="selectionBar('f_renewal_apply','${dataList.FDPNO}','${dataList.ACN}','${dataList.MapValue}');"><li><spring:message code="LB.FX_Time_Deposit_Automatic_Rollover_Application_Change" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a>
											</ul>
											<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${data.index}')" value="<spring:message code= "LB.X1572" />"/>
										</div>
									</div>
									</td>
								</tr>
							</c:forEach>
						</tbody>
						</table>
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
				<form id="formId" action="${__ctx}/download" method="post">
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="LB.FX_Time_Deposit_Detail"/>
					<input type="hidden" name="CMQTIME" value="${f_time_deposit_details.data.CMQTIME }"/>
					<input type="hidden" name="COUNT" value="${f_time_deposit_details.data.CMRECNUM }"/>
					<input type="hidden" name="DATATOTMAT" value="${f_time_deposit_details.data.FCYTOTMAT }"/>
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/> 	
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="11"/>
					<input type="hidden" name="headerBottomEnd" value="12"/>
					<input type="hidden" name="rowStartIndex" value="13"/>
					<input type="hidden" name="rowRightEnd" value="11"/>
					
					<!-- TXT下載用 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="9"/>
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="false"/>
				</form>
				<!-- for selectionBar -->
				<form method="post" id="fastBarAction" action="">
					<input type="hidden" id="FDPNO" name="FDPNO" value="">
					<input type="hidden" id="ACN530" name="ACN530" value="">
					<input type="hidden" id="FGSELECT" name="FGSELECT" value="">
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
	

<%@ include file="../index/footer.jsp"%>
</body>
</html>