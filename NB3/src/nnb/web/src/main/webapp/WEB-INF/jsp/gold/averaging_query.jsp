
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

	<script type="text/javascript">
		$(document).ready(function () {
			init();
			getToday();
			datetimepickerEvent();
		});
		function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			$("#hideblock_CheckDateScope").hide();
			$("#CMSUBMIT").click(function(e) {
				//TODO 驗證
				e = e || window.event;
// 				if (checkTimeRange() == false) {return false;}
				//打開驗證隱藏欄位
				if ($("#CMPERIOD").prop("checked")) {
					$("#hideblock_CheckDateScope").show();
				}
				else{
					$("#hideblock_CheckDateScope").hide();
				}
				console.log("CMPERIOD"+ $("#CMPERIOD").prop("checked"));
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					console.log("submit~~");
					$("#formId").validationEngine('detach');
					console.log("submit~~");
					initBlockUI(); //遮罩
					$("#formId").attr("action","${__ctx}/GOLD/AVERAGING/averaging_query_result");
					$("#formId").submit();
				}
			});
		}
		//預約自動輸入今天
		function getToday() {
			var today = "${averaging_query.data.TODAY}";
			$('#CMSDATE').val(today);
			$('#CMEDATE').val(today);
			$('#odate').val(today);
		}
		//	日曆欄位參數設定
		function datetimepickerEvent() {

			$(".CMSDATE").click(function(event) {
				$('#CMSDATE').datetimepicker('show');
			});
			$(".CMEDATE").click(function(event) {
				$('#CMEDATE').datetimepicker('show');
			});

			jQuery('.datetimepicker').datetimepicker({
				timepicker : false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
				format : 'Y/m/d',
				lang: '${transfer}'
			});
		}
		// 		//日期驗證
		// 		function checkTimeRange() {
		// 			console.log($('#CMEDATE').val());
		// 			console.log($('#CMSDATE').val());
		// 			var now = new Date(Date.now());
		// 			var twoYm = 63115200000;
		// 			var twoMm = 5259600000;
		// 			var startT = new Date($('#CMSDATE').val());
		// 			var endT = new Date($('#CMEDATE').val());
		// 			var distance = now - startT;
		// 			var range = endT - startT;
		// 			var distanceE = now - endT;

		// 			var limitS = new Date(now - twoYm);
		// 			var limitE = new Date(startT.getTime() + twoMm);
		// 			if (distance >= limitS) {
		// 				var m = limitS.getMonth() + 1;
		// 				var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
		// 				// 起始日不能小於
		// 				var msg = '<spring:message code="LB.Start_date_check_note_1" />' +
		// 					time;
		// 				alert(msg);
		// 				return false;
		// 			} else if (distance < 0) {
		// 				var m = (now.getMonth()) + 1;
		// 				var time = now.getFullYear() + '/' + m + '/' + now.getDate();
		// 				// 起始日不能大於
		// 				var msg = '起始日不能大於' + time;
		// 				alert(msg);
		// 				return false;
		// 			} else {
		// 				if (range < 0) {
		// 					var msg = '終止日不能小於起始日';
		// 					alert(msg);
		// 					return false;
		// 				} else if (distanceE < 0) {
		// 					var m = now.getMonth() + 1;
		// 					var time = now.getFullYear() + '/' + m + '/' + now.getDate();
		// 					// 終止日不能大於
		// 					var msg = '<spring:message code="LB.End_date_check_note_1" />' +
		// 						time;
		// 					alert(msg);
		// 					return false;
		// 				}
		// 			}
		// 			return true;
		// 		}
		//下拉式選單
		function formReset() {
			initBlockUI();
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 定期定額交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1552" /></li>
    <!-- 定期定額查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1586" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<!-- 定期定額查詢 -->
				<h2><spring:message code="LB.W1586"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<!-- 帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> <label>
											<h4>
												<spring:message code="LB.Account" />
											</h4>
										</label>
									</span> <span class="input-block">
										<div class="ttb-input">
											<!--帳號 -->
											<select class="custom-select select-input half-input validate[funcCall[validate_CheckSelect['<spring:message code= "LB.Account" />',ACN,#]]]" name="ACN" id="ACN">
												<!--請選擇-->
												<option value="#"><spring:message code="LB.Select_account" /></option>
												<c:forEach var="dataList" items="${averaging_query.data.REC}">
													<option>${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<!--查詢區間  -->
									<span class="input-title"> <label>
											<spring:message code="LB.Inquiry_period" />
										</label>
									</span> <span class="input-block">
										<!--  指定日期區塊 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name=FGPERIOD id="CMLASTDATE" value="CMLASTDATE" checked/>
												<!-- 最後一次異動 -->
												<spring:message code="LB.W1597"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
											<!--期間起日 -->
										<div class="ttb-input">
											<label class="radio-block">
											<!--  指定日期 -->
												<spring:message code="LB.Specified_period" />
												<input type="radio" name="FGPERIOD" id="CMPERIOD" value="CMPERIOD" /> 
												<span class="ttb-radio"></span>
											</label>
											<div class="ttb-input mt-3">
<!-- 											TODO validate[required,funcCall[verification_date[CMSDATE]]] -->
<!-- 起日 -->
												<spring:message code="LB.D0013"/>:&nbsp;<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker validate[required]" maxlength="10" value="" /> 
												<span class="input-unit CMSDATE"> <img src="${__ctx}/img/icon-7.svg" /></span> 
												<br><br>
												<!-- 迄日 -->
												<spring:message code="LB.D0014"/>:&nbsp;<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker validate[required]" maxlength="10" value="" /> 
												<span class="input-unit CMEDATE"> <img src="${__ctx}/img/icon-7.svg" /></span>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CheckDateScope" >
												<!-- 驗證用的input 查詢期間-->
												<input id="odate" name="odate" type="text" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, CMSDATE, CMEDATE, false, 24,null]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
												<!-- (查詢最近兩年內資料) -->
												<br><br><spring:message code="LB.W1600"/>
											</div>
										</div>

									</span>
								</div>
							</div>
							<!-- 確定-->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" />
<!-- 							重新輸入 -->
<%-- 							<spring:message code="LB.Re_enter" var="cmRest"></spring:message> --%>
<%-- 							<input type="reset" class="ttb-button btn-flat-orange" name="CMRESET" id="CMRESET" value="${cmRest}" /> --%>
						</div>
					</div>
				</form>
			</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>