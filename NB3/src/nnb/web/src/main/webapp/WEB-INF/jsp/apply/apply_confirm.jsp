<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    
	<script type="text/JavaScript">
		$(document).ready(function() {
			
			// 線上立即申請
			$("#CMAPPLY").click( function(e) {
				$("#formId").attr("action", "${pageContext.request.contextPath}" + "/ELECTRONIC/CHECKSHEET/apply_bill");
				initBlockUI();
				$("#formId").submit();
			});
			
			// 取消
			$("#CMCANCEL").click( function(e) {
				$("#formId").attr("action", "${pageContext.request.contextPath}" + "/INDEX/index");
				initBlockUI();
				$("#formId").submit();
			});
		});
 	</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 密碼變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Change_Password" /></li>
		</ol>
	</nav>

	
	<!-- 快速選單及主頁內容 -->
	<!-- menu、登出窗格 --> 
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">	
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code= "LB.D0305" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<form method="post" id="formId" action="">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 申請項目 -->
								
										<div class="text-center">
											<font style="color:red"><spring:message code= "LB.X1690" /></font>
										</div>
								
							</div>
							
							<input type="button" id="CMCANCEL" name="CMCANCEL" class="ttb-button btn-flat-gray" value="<spring:message code= "LB.Cancel" />" />
							<input type="button" id="CMAPPLY" name="CMAPPLY" class="ttb-button btn-flat-orange" value="<spring:message code= "LB.X0954" />" />
						</div>
					</div>	
				</form>
			</section>
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
    
</body>
</html>
