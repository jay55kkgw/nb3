<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">

	$(document).ready(function() {
		// 將.table變更為footable
		//initFootable();
		setTimeout("initDataTable()",100);
// 		alert('${elec_bill_apply.data}');
// 		alert('${sessionScope.authority}');
		apply_status();
		
		var a1status = ('${elec_bill_apply.data.b_DPBILL}'=='true')?true:false;
		var a2status = ('${elec_bill_apply.data.dpcardbill}'=='Y')?true:false;
		var a3status = ('${elec_bill_apply.data.dpfundbill}'=='Y')?true:false;
		
		var c1status = (('${elec_bill_apply.data.b_DPBILL}'=='false'))?true:false;
		var c2status = (!('${elec_bill_apply.data.dpcardbill}'=='Y'))?true:false;
		var c3status = (!('${elec_bill_apply.data.dpfundbill}'=='Y'))?true:false;
		
		 
		$("#a1").attr('checked',a1status);
		$("#a2").attr('checked',a2status);
		$("#a3").attr('checked',a3status);
		
		$("#cancel").hide(); 
		$("#remail").hide();
		
		if(!(a1status && a2status && a3status)){
			$('#ch0').click();
		}
		
		$("#ch0").click(function(){ 
			$("#apply").show("slow");
			$("#a1").attr('checked',a1status);
			$("#a2").attr('checked',a2status);
			$("#a3").attr('checked',a3status);
			$("#cancel").hide("slow");  
			$("#remail").hide("slow");		
		});

		$("#ch1").click(function(){ 
			$("#apply").hide("slow"); 
			$("#remail").hide("slow");
			$("#cancel").show("slow");
			$("#c1").attr('checked',c1status);
			$("#c2").attr('checked',c2status);
			$("#c3").attr('checked',c3status);  
		});
			
		$("#ch2").click(function(){ 
			$("#apply").hide();
			$("#cancel").hide();  
			$("#remail").show("slow");
		});
		
		
		
	});
	
	function apply_status() {
		('${elec_bill_apply.data.b_DPBILL}' == 'true') ? $('#EBILLSTATUS')
				.text("<spring:message code= "LB.D0277" />") : $('#EBILLSTATUS').text("<spring:message code= "LB.D0276" />");
		('${elec_bill_apply.data.dpfundbill}' == 'Y' || '${elec_bill_apply.data.b_C015Applied}' == 'true') ? $(
				'#FUNDBILLSTATUS').text("<spring:message code= "LB.D0277" />")
				: $('#FUNDBILLSTATUS').text("<spring:message code= "LB.D0276" />");
		('${elec_bill_apply.data.dpcardbill}' == 'Y') ? $('#CREDITBILLTATUS')
				.text("<spring:message code= "LB.D0277" />") : $('#CREDITBILLTATUS').text("<spring:message code= "LB.D0276" />");
	}
	
	function processQuery()
	{
		var main = document.getElementById("formId");
		

		var alertFlag = 'Y';
		
		if ($('input:radio[name="APPLYFLAG"]:checked').val()=='A'){
		
		    var a1 = document.getElementById("a1");	
		    var a2 = document.getElementById("a2");	
		    var a3 = document.getElementById("a3");		    	    
			var obj_Array = new Array(a1, a2, a3);
							
			for (var i=0; i < obj_Array.length; i++) {
								
				if ((obj_Array[i] != null) &&(! obj_Array[i].disabled) && (obj_Array[i].checked)) {
					alertFlag = 'N';	
					break;	
				}
			}//end for
			
			if (alertFlag == 'Y') 
			{
				//<!-- 請選擇申請之帳單種類 -->
				//alert("<spring:message code= "LB.Alert073" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert073' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
		
				return false;
			}
			
			$('input[type=checkbox]').each(function(){
				var name = this.name;
				var id = name.substring(0,5);
				if(this.disabled||!(this.checked)){
					$("#"+id).val("");
				}else{
					$("#"+id).val(this.value);
				}
				
			})
			
			$('#formId').attr("action","${__ctx}/ELECTRONIC/CHECKSHEET/apply_statement");
			$('#formId').submit();
// 		 	main.urlPath.value = 'apply/N1010A.jsp';
// 		 	main.submit();
		 	
		}else if ($('input:radio[name="APPLYFLAG"]:checked').val()=='B'){

		    var c1 = document.getElementById("c1");	
		    var c2 = document.getElementById("c2");	
		    var c3 = document.getElementById("c3");	
			var obj_Array = new Array(c1, c2, c3);
							
			for (var i=0; i < obj_Array.length; i++) {
								
				if ((obj_Array[i] != null) &&(! obj_Array[i].disabled) && (obj_Array[i].checked)) {
					alertFlag = 'N';	
					break;	
				}
			}//end for
			
			if (alertFlag == 'Y') 
			{
				//<!-- 請選擇取消之帳單種類 -->
				//alert("<spring:message code= "LB.Alert074" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert074' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
		
				return false;
			}
			
			$('input[type=checkbox]').each(function(){
				var name = this.name;
				var id = name.substring(0,5);
				if(this.disabled||!(this.checked)){
					$("#"+id).val("");
				}else{
					$("#"+id).val(this.value);
				}
				
			})
		
			$('#formId').attr("action","${__ctx}/ELECTRONIC/CHECKSHEET/cancel_confirm");
			$('#formId').submit();
// 		 	main.urlPath.value = 'apply/N1010C2.jsp';
// 		 	main.submit();	
		 	
		}else if ($('input:radio[name="APPLYFLAG"]:checked').val()=='C'){
				if ($('input:radio[name="TYPE7_SHOW"]:checked').val()=='1') {
					$('#formId').attr("action","${__ctx}/ELECTRONIC/CHECKSHEET/elec_bill_resent");
					$('#formId').submit();
// 			 		main.urlPath.value = 'apply/N1010_R1.jsp';
// 			 		main.submit();
			 	}
			 	else if ($('input:radio[name="TYPE7_SHOW"]:checked').val()=='3') {
					$('#formId').attr("action","${__ctx}/ELECTRONIC/CHECKSHEET/aio_bill_query");
					$('#formId').submit();
			 	}
			 	else {
			 		$('#formId').attr("action","${__ctx}/CREDIT/INQUIRY/card_history");
					$('#formId').submit();
			 	}	
		} 

		return false;
	}
	
</script>

</script>

</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 電子對帳單     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1396" /></li>
    <!-- 電子對帳單申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0270" /></li>
		</ol>
	</nav>


	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->


		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->

			<h2><spring:message code="LB.D0270" /></h2><!-- 電子帳單申請 -->

			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
			<input type="hidden" id="TYPE1" name="TYPE1" value="">
			<input type="hidden" id="TYPE2" name="TYPE2" value="">
			<input type="hidden" id="TYPE3" name="TYPE3" value="">
			<input type="hidden" id="TYPE4" name="TYPE4" value="">
			<input type="hidden" id="TYPE5" name="TYPE5" value="">
			<input type="hidden" id="TYPE6" name="TYPE6" value="">
			<input type="hidden" id="TYPE7" name="TYPE7" value="">
				<div class="main-content-block row">
					<div class="col-12">
					<ul class="ttb-result-list"></ul>
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
								<tr>
									<th  data-title='<spring:message code="LB.D0271"/>'><spring:message code="LB.D0271" /><!-- 項目 --></th>
									<th  data-title='<spring:message code="LB.Status"/>' nowrap><spring:message code="LB.Status" /><!-- 狀態 --></th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${sessionScope.authority eq 'ALL'||sessionScope.authority eq 'ATMT'||sessionScope.authority eq 'ATM'||sessionScope.authority eq 'CRD'}">
								<tr>
									<td class="text-center"><spring:message code="LB.D0273" /><!-- 電子交易對帳單（即電子化銀行轉帳交易對帳單) --></td>
									<td class="text-center" nowrap id="EBILLSTATUS"></td>
								</tr>
								</c:if>
								<c:if test="${(sessionScope.authority eq 'ALL'||sessionScope.authority eq 'ATMT') && elec_bill_apply.data.b_ApplyNBFund}">
								<tr>
									<td class="text-center"><spring:message code="LB.D0274" /><!-- 基金電子對帳單 --></td>
									<td class="text-center" nowrap id="FUNDBILLSTATUS"></td>
								</tr>
								</c:if>
								<c:if test="${elec_bill_apply.data.b_DisplayCard}">
								<tr>
									<td class="text-center"><spring:message code="LB.D0275" /><!-- 信用卡電子帳單 --></td>
									<td class="text-center" nowrap id="CREDITBILLTATUS"></td>
								</tr>
								</c:if>
							</tbody>
						</table>

						<p style="color: red"><spring:message code="LB.D0278" /><!-- 請選擇申請項目 --></p>

						<div class="ttb-input-block">
							<!--申請項目區塊 -->
							<div class="ttb-input-item row">
								<!--申請項目  -->
								<span class="input-title"> <label> <spring:message code="LB.D0169" /><!-- 申請項目 -->： </label>
								</span> <span class="input-block">
								
								<label class="radio-block" id="ch0_1">
										<input type="radio" id="ch0" name="APPLYFLAG" value="A" ${elec_bill_apply.data.ApplyFlag}>
										<span class="ttb-radio-input"><spring:message code="LB.D0279" /><!-- 申請電子帳單 --></span> <span
										class="ttb-radio"></span><br><br>
										<div id="apply">
										<div class="ttb-input">
											<c:if test="${sessionScope.authority eq 'ALL'||sessionScope.authority eq 'ATMT'||sessionScope.authority eq 'ATM'||sessionScope.authority eq 'CRD'}">
											<label class="check-block"> 
												<span class="ttb-check-input"><spring:message code="LB.D0280" /><!-- 電子交易對帳單 --> </span>
												<input type="checkbox" id="a1" name="TYPE1_SHOW" value="1" ${elec_bill_apply.data.Type1Flag}/> 
												<span class="ttb-check"></span>
											</label><br><br>
											</c:if>
											<c:if test="${sessionScope.authority eq 'ALL' && elec_bill_apply.data.b_ApplyNBFund}">
											<label class="check-block"> 
												<spring:message code="LB.D0274" /><!-- 基金電子對帳單 -->
												<input type="checkbox" id="a3" name="TYPE3_SHOW" value="3" ${elec_bill_apply.data.Type3Flag}/> 
												<span class="ttb-check"></span>
											</label><br><br>
											</c:if>
											<c:if test="${elec_bill_apply.data.b_DisplayCard}">
											<label class="check-block"> 
												<spring:message code="LB.D0275" /><!-- 信用卡電子帳單 -->
												<input type="checkbox" id="a2" name="TYPE2_SHOW" value="2" ${elec_bill_apply.data.Type2Flag}/> 
												<span class="ttb-check"></span>
											</label>
											<br><br>
											</c:if>
										</div>
										</div>
								</label>
								<br>
								
								  <label class="radio-block" >
										<input type="radio" id="ch1" name="APPLYFLAG" value="B" ${elec_bill_apply.data.CancelFlag}>
										<span class="ttb-radio-input"><spring:message code="LB.D0284" /><!-- 取消電子帳單 --></span> <span
										class="ttb-radio"></span><br><br>
										<div id="cancel">
										<div class="ttb-input">
											<c:if test="${sessionScope.authority eq 'ALL'||sessionScope.authority eq 'ATMT'||sessionScope.authority eq 'ATM'||sessionScope.authority eq 'CRD'}">
											<label class="check-block"> 
												<spring:message code="LB.D0280" /><!-- 電子交易對帳單--> 
												<input type="checkbox" id="c1" name="TYPE4_SHOW" value="4" ${elec_bill_apply.data.Type4Flag}/>
												<span class="ttb-check"></span>
											</label>
											<br> <br>
											</c:if>
											<c:if test="${sessionScope.authority eq 'ALL' && elec_bill_apply.data.b_ApplyNBFund}">
											<label class="check-block"> 
												<spring:message code="LB.D0274" /><!-- 基金電子對帳單 -->
												 <input type="checkbox" id="c3" name="TYPE6_SHOW" value="6" ${elec_bill_apply.data.Type6Flag}/>
												<span class="ttb-check"></span>
											</label>
											<br><br>
											</c:if>
											<c:if test="${elec_bill_apply.data.b_DisplayCard}">
											<label class="check-block">
												<spring:message code="LB.D0275" /><!-- 信用卡電子帳單 --> 
												<input type="checkbox" id="c2" name="TYPE5_SHOW" value="5" ${elec_bill_apply.data.Type5Flag}/>
											<span class="ttb-check"></span>
											</label>
											<br><br>
											</c:if>
										</div>
										</div>
								</label><br>
								
								<label class="radio-block">
										<input type="radio" id="ch2" name="APPLYFLAG" value="C">
										<span class="ttb-radio-input"><spring:message code="LB.D0285" /><!-- 補發電子帳單 --></span>
										<span class="ttb-radio"></span><br><br>
										<div id="remail">
										<div class="ttb-input">
											<c:if test="${sessionScope.authority eq 'ALL'||sessionScope.authority eq 'ATMT'||sessionScope.authority eq 'ATM'||sessionScope.authority eq 'CRD'}">
											<label class="radio-block"> 
												<spring:message code="LB.D0286" /><!-- 電子交易對帳單：上月交易明細 -->
												<input type="radio" id="b1" name="TYPE7_SHOW" value="1" checked/>
												<span class="ttb-radio"></span>
											</label>
											<br> <br>
											</c:if>
											<c:if test="${elec_bill_apply.data.b_DisplayCard}">
											<label class="radio-block">
												<spring:message code="LB.D0275" /><!-- 信用卡電子帳單 -->
												<input type="radio" id="b2" name="TYPE7_SHOW" value="2"/>
												<span class="ttb-radio"></span>
											</label>
											<br> <br>
											</c:if>
											
											<label class="radio-block">
												綜合電子帳單<!-- 信用卡電子帳單 -->
												<input type="radio" id="b3" name="TYPE7_SHOW" value="3"/>
												<span class="ttb-radio"></span>
											</label>
											<br> <br>
											
											<c:if test="${sessionScope.authority eq 'ALL' && elec_bill_apply.data.b_ApplyNBFund}">
											<font style="color:red"><spring:message code="LB.D0287" /></font> <!-- 電子交易對帳單：上月交易明細基金電子對帳單請電洽(02)2559-7171分機5462補發-->
											</c:if>
										</div>
										</div>
								</label><br>
							</div>
						</div>


						<input type="reset" name="CMRESET" class="ttb-button btn-flat-gray no-l-disappear-btn" value="<spring:message code="LB.Re_enter" />"/>
						<!-- 重新輸入 -->
						<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" onclick="processQuery()">
						<!-- 確定 -->
					</div>
				</div>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /><!-- 說明 --></p>
						<li><spring:message code="LB.Apply_Bill_P1_D1" /></li>
						<li><spring:message code="LB.Apply_Bill_P1_D2" /></li>
						<li><spring:message code="LB.Apply_Bill_P1_D3" /></li>
						<li><spring:message code="LB.Apply_Bill_P1_D4" /></li>
				</ol>
			</form>
		</section>
		</main>
		<!-- main-content END -->
	</div>
	<!-- content row END -->

	<%@ include file="../index/footer.jsp"%>

</body>
</html>