<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$('#but_Agree').prop('checked', false);
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
            $("#formId").attr("action","${__ctx}/OTHER/FEE/other_labor_insurance_confirm");
	 	  	$("#formId").submit();
		});
		//上一頁按鈕
		$("#CMBACK").click(function() {
// 			initBlockUI();
// 			fstop.getPage('${pageContext.request.contextPath}'+'/OTHER/FEE/other_labor_insurance','', '');
			var action = '${__ctx}/OTHER/FEE/other_labor_insurance';
			$("#back").val("Y");
			$("form").attr("action", action);
			$("form").submit();
		});
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 委託轉帳代繳勞工保險保險費暨工資墊償基金提繳費約定條款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0227" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2>
					<!-- 預約黃金交易查詢/取消 -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.X0227"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
				
                <form id="formId" method="post">
					<input type="hidden" name="action" value="forward">
					<input type="hidden" id="back" name="back" value="">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
							<div class="ttb-message">
								<p><spring:message code="LB.X0227"/></p>
								<span><font color="royalblue" size="3"><b>下列為申請委託轉帳代繳勞工保險保險費暨工資墊償基金提繳費應遵守之約定條款內容，您如接受本約定條款則請按<font color=red>我同意約定條款</font>鍵，以完成申請作業，您如不同意條款內容，則請按<font color=red>回上頁</font>鍵，本行將不受理您的代扣繳申請。</b></font></span>
                            </div>
							<table style="text-align: left">
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>一、</td>
	                            	<td>本人【公司】(以下簡稱立約人)委託臺灣中小企業銀行(以下簡稱貴行)自指定之存款帳戶(即指前頁之扣帳帳號，以下簡稱轉帳代繳帳戶)轉帳代繳勞工保險保險費暨工資墊償基金提繳費(以下簡稱勞保費)，並自行依據最近月份保險費繳款單或收據內容輸入代扣繳資料，如因代扣繳申請書內容輸入不全、錯誤或其他原因，致貴行無法辦理轉帳，則本約定書不生效力，所受損失由立約人自行負責。</td>
	                            </tr>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>二、</td>
	                            	<td>立約人申請轉帳代繳本人【公司】或指定第三人勞保費，自貴行同意接受委託，將轉帳代繳檔案輸入磁帶送勞工保險局審核，並自勞工保險局繳款單通知之日起開始轉帳代繳，在未通知前各月份之勞保費，仍由投保單位自行繳納。</td>
	                            </tr>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>三、</td>
	                            	<td>立約人以同一轉帳代繳帳戶委託貴行轉帳代繳勞工保險費及勞工退休金提繳費者，貴行應依先扣繳勞工保險費，再扣繳勞工退休金提繳費之順序執行扣繳作業。</td>
	                            </tr>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>四、</td>
	                            	<td>貴行代繳義務，以立約人轉帳代繳帳戶可用餘額足敷當月份(每月底為轉帳日)委託代繳之勞保費為限(即每月月底帳戶須保持足夠之餘額以供備付)。轉帳代繳帳戶餘額不敷繳付時，貴行得於次月十五日零時(如遇假日為其次一營業日)再行轉帳乙次(即十四日轉帳代繳帳戶須足夠餘額以供備付)，倘仍存款不足，則由投保單位自行持勞保費繳款單至指定之金融機構繳納，如投保單位因此而須負擔滯納金，概由立約人負責。</td>
	                            </tr>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>五、</td>
	                            	<td>立約人委託代繳勞保費，如轉帳代繳帳戶因遭法院強制執行或其他事故致無法代繳時，貴行得終止代繳之約定，其因此而致投保單位須負擔之滯納金，概由立約人負責。</td>
	                            </tr>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>六、</td>
	                            	<td>立約人在貴行另行指定轉帳代繳帳戶時，應註銷原委託約定再重新辦理代扣繳申請；並同意自貴行受理變更，將轉帳代繳檔案磁帶送勞工保險局審核，完成變更通知之月份起，由新帳戶轉帳代繳勞保費。</td>
	                            </tr>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>七、</td>
	                            	<td>立約人委託代繳勞保費，在未終止委託前，不得藉故拒絕繳納勞保費，其因此而致投保單位須負擔滯納金，概由立約人負責。</td>
	                            </tr>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>八、</td>
	                            	<td>立約人委託代繳勞保費，在未終止委託前，自行結清轉帳代繳帳戶時，視同當然終止代繳之約定，應繳納之勞保費需由投保單位持勞工保險局繳款單至指定金融機構繳納，因此而致投保單位須負擔滯納金，概由立約人負責。</td>
	                            </tr>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>九、</td>
	                            	<td>貴行或立約人皆得隨時以書面通知對方終止代繳契約。立約人終止代繳時應填具註銷約定書，並自貴行接受註銷委託，將轉帳代繳檔案磁帶送勞工保險局審核，並自完成變更通知之月份起，終止以該轉帳代繳帳戶轉帳代繳勞保費，因註銷委託而致投保單位須負擔滯納金時，概由立約人負責。</td>
	                            </tr>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>十、</td>
	                            	<td>立約人指定之轉帳代繳帳戶為支票存款帳戶者，倘因扣繳勞保費而致存款不足，發生退票情事，概由立約人自行負責。</td>
	                            </tr>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>十一、</td>
	                            	<td>倘貴行之電腦系統發生故障或電信中斷等因素致無法執行轉帳代繳時，貴行得順延至系統恢復正常，始予扣款，其因上開事由所致之損失及責任，由立約人自行負擔。</td>
	                            </li>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>十二、</td>
	                            	<td>貴行於同一日需自轉帳代繳帳戶執行多筆轉帳扣繳作業而立約人存款不足時，立約人同意由貴行自行選定扣款順序。</td>
	                            </tr>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>十三、</td>
	                            	<td>立約人委託代繳勞保費之收據由勞工保險局寄發。</td>
	                            </li>
	                            <tr>
								<td style="width: 5em; text-align: center" rowspan=1>十四、</td>
	                            	<td>立約人同意貴行得將立約人個人根據特定目的填列之相關基本資料提供貴行電腦處理及利用。</td>
	                            </tr>
							</table>
                        	<div>
	                            <input id="CMBACK" name="CMBACK" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page"/>" />
	                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.W1554"/>"/>
	                        </div>
	            		</div>  
                    </div>
                </form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>