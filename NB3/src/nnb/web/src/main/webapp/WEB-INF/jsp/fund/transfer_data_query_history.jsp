<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="RS" value="${transfer_data_query_history.data.RS}"></c:set>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			setTimeout("initDataTable()",100);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		function init() {
// 			initFootable();
			$("#printbtn").click(function(){
				var params = {
						jspTemplateName: 	"transfer_data_query_history_print",
						jspTitle: 			"<spring:message code= "LB.X0389" />",
						CMQTIME: 			"${RS.CMQTIME }",
						CMRECNUM: 			"${RS.CMRECNUM }",
						CUSIDN_F: 			"${RS.CUSIDN_F }",
						CUSNAME_F: 			"${RS.CUSNAME_F }",
						CDNO_F:			"${RS.REC[0].CDNO_F }"
					};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
			
			$("#backbtn").click(function() {
				// 遮罩
				initBlockUI();
				// 回基金損益餘額查詢
				var url = "${__ctx}/FUND/QUERY/profitloss_balance";
				window.open(url, '_self');
			});
		}
		
		
		// 右上角下拉式選單
	 	function formReset() {
			if(!$("#formId").validationEngine("validate")){
				e = e || window.event; // forIE
				e.preventDefault();
			}
	 		else{
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/transfer_data_query_history.xls");
		 		}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/transfer_data_query_history.txt");
		 		}
				$("#formId").attr("target", "");
	            $("#formId").submit();
	            $('#actionBar').val("");
	 		}
		}
		  
	</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 基金交易資料查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0943" /></li>
    <!-- 基金歷史交易明細資料查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0389" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<!-- 快速選單及主頁內容 -->
	<main class="col-12"> 
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.X0389" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

			<!-- 右上角下拉式選單-->
			<div class="print-block">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Execution_option" /></option>
					<!-- 下載Excel檔 -->
					<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
					<!-- 下載為txt檔 -->
					<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
				</select>
			</div>
				
			<div class="main-content-block row">
				<div class="col-12">
					<ul class="ttb-result-list">
						<!-- 查詢時間 -->
					    <li>
					        <h3><spring:message code="LB.Inquiry_time" /></h3>
					        <p>${RS.CMQTIME }</p>
					    </li>
					    <!-- 資料總數-->
					    <li>
					        <h3><spring:message code="LB.Total_records" /></h3>
					        <p>${RS.CMRECNUM }</p>
					    </li>
					    <!-- 身份證字號/統一編號-->
					    <li>
					        <h3><spring:message code="LB.Id_no" /></h3>
					        <p>${RS.CUSIDN_F }</p>
					    </li>
					    <!-- 姓名 -->
					    <li>
					        <h3><spring:message code="LB.Name" /></h3>
					        <p>${RS.CUSNAME_F }</p>
					    </li>
					    <!-- 信託帳號 -->
					    <li>
					        <h3><spring:message code="LB.W0944" /></h3>
					        <p>${RS.REC[0].CDNO_F }</p>
					    </li>
					</ul>
<!-- 					<ul class="ttb-result-list"> -->
<!-- 						<li> -->
<!-- 							查詢時間 -->
<%-- 							<p><spring:message code="LB.Inquiry_time" />：${RS.CMQTIME }</p> --%>
<!-- 							資料總數 -->
<!-- 							<p> -->
<%-- 								<spring:message code="LB.Total_records" />：${RS.CMRECNUM }  --%>
<%-- 								<spring:message code="LB.Rows" /> --%>
<!-- 							</p>							 -->
<!-- 						</li> -->
<!-- 					</ul> -->
					<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
					<thead>
<%-- 						<tr> --%>
<!-- 							身份證字號/統一編號 -->
<%-- 							<th><spring:message code="LB.Id_no" /></th> --%>
<%-- 							<th class="text-left" colspan="13">${RS.CUSIDN_F }</th> --%>
<%-- 						<tr> --%>
<%-- 						<tr> --%>
<!-- 							姓名 -->
<%-- 							<th><spring:message code="LB.Name" /></th> --%>
<%-- 							<th class="text-left" colspan="13">${RS.CUSNAME_F }</th> --%>
<%-- 						<tr> --%>
<%-- 						<tr> --%>
<!-- 							信託帳號 -->
<%-- 							<th><spring:message code="LB.W0944" /></th> --%>
<%-- 							<th class="text-left" colspan="13">${RS.REC[0].CREDITNO_F }</th> --%>
<%-- 						<tr> --%>
						<tr>
							<!-- 交易日期 -->
							<th nowrap><spring:message code="LB.Transaction_date" /></th>
							<th nowrap><spring:message code="LB.W0025" /></th>
							<th nowrap><spring:message code="LB.W0946" /></th>
							<!-- 交易類別 -->
							<th nowrap><spring:message code="LB.Transaction_type" /></th>
							<th><spring:message code="LB.W0947" /></th>
							<th nowrap><spring:message code="LB.W0027" /></th>
							<th nowrap><spring:message code="LB.W0949" /></th>
							<!-- 匯率 -->
							<th nowrap><spring:message code="LB.Exchange_rate" /></th>
							<th nowrap data-breakpoints="xs sm"><spring:message code="LB.X0390" /></th>
							<th nowrap data-breakpoints="xs sm"><spring:message code="LB.X0391" /></th>
							<th nowrap data-breakpoints="xs sm"><spring:message code="LB.W0950" /></th>
							<th nowrap data-breakpoints="xs sm"><spring:message code="LB.W0951" /></th>
							<th nowrap data-breakpoints="xs sm"><spring:message code="LB.W0952" /></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="row" items="${RS.REC }">
							<tr>
								<td class="text-center">${row.TRADEDATE_F }</td>					<!-- 交易日期 -->
								<td class="text-center">${row.TRANSCODE_FUNDLNAME }</td>			<!-- 基金名稱 -->
								<td class="text-center">${row.TR106_F }</td>						<!-- 投資方式 -->
								<td class="text-center">${row.FUNDTYPE_F }</td>						<!-- 交易類別 -->
								<td class="text-center">
									${row.CRY_CRYNAME }							<!-- 信託/除息金額幣別 -->
									<br/>
									${row.FUNDAMT_F }							<!-- 信託/除息金額 -->
								</td>
								<td class="text-right">${row.UNIT_F }</td>		<!-- 單位數 -->
								<td class="text-right">${row.PRICE1_F }</td>	<!-- 淨值 -->
								<td class="text-right">${row.EXRATE_F }</td>	<!-- 匯率 -->
								<td class="text-center">											<!-- 相關費用合計 -->
									${row.FEECRY_CRYNAME } 						<!-- 手續費幣別 -->
									${row.FEEAMT_F }							<!-- 手續費金額 -->
								</td>			
								<td class="text-center">${row.PAYACN_F }</td>						<!-- 存款帳號/信用卡卡號 -->
								<td class="text-center">${row.INTRANSCODE_FUNDLNAME }</td>			<!-- 轉入基金名稱 -->
								<td class="text-right">${row.TXUNIT_F }</td>	<!-- 轉入單位數 -->
								<td class="text-center">							
									${row.INTRANSCRY_CRYNAME } 					<!-- 轉入價格幣別 -->
									${row.PRICE2_F }							<!-- 轉入價格 -->
								</td>
							</tr>
						</c:forEach>
					</tbody>
					</table>
					<!-- 列印 -->
					<input type="button" class="ttb-button btn-flat-gray" id="printbtn" value="<spring:message code="LB.Print"/>" />
					<!-- 基金損益餘額查詢 -->
					<input type="button" class="ttb-button btn-flat-orange" id="backbtn" value="<spring:message code="LB.X0413" />" />
				</div>
			</div>
			
			
			<form id="formId" action="${__ctx}/download" method="post">
				<!-- 下載用 -->
				<!-- 基金歷史交易明細資料查詢 -->
				<input type="hidden" name="downloadFileName" value="<spring:message code="LB.X0389" />"/>
				<input type="hidden" name="CMQTIME" value="${RS.CMQTIME}"/>
				<input type="hidden" name="CMRECNUM" value="${RS.CMRECNUM}"/>
				<input type="hidden" name="CUSIDN_F" value="${RS.CUSIDN_F}"/>
				<input type="hidden" name="CUSNAME_F" value="${RS.CUSNAME_F }"/>
				<input type="hidden" name="CDNO_F" value="${RS.REC[0].CDNO_F }"/>
				<input type="hidden" name="downloadType" id="downloadType"/> 					
				<input type="hidden" name="templatePath" id="templatePath"/>		
				<!-- EXCEL下載用 -->
				<!-- headerRightEnd  資料列以前的右方界線
					 headerBottomEnd 資料列到第幾列 從0開始
					 rowStartIndex 資料列第一列的位置
					 rowRightEnd 資料列用方的界線
				 -->
				<input type="hidden" name="headerRightEnd" 	value="12"/>
				<input type="hidden" name="headerBottomEnd" value="8"/>
				<input type="hidden" name="rowStartIndex" 	value="9"/>
				<input type="hidden" name="rowRightEnd" 	value="12"/>
				<!-- TXT下載用
					txtHeaderBottomEnd需為資料第一列(從0開始)-->
				<input type="hidden" name="txtHeaderBottomEnd" 	value="11"/> 					
				<input type="hidden" name="txtHasRowData" 		value="true"/>
				<input type="hidden" name="txtHasFooter" 		value="false"/>
			</form>
				
				
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>