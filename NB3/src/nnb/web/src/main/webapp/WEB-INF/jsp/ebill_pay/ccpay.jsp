<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.twzipcode-1.6.1.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<style>
		.zipcode {
			display: none;
		}
	</style>
	    <script type="text/javascript">
        $(document).ready(function () {
        });

        function changeItemStyle(){
            var item = $('#SelectMenu').val();
            if (item == "1"){
	        	var action = '${__ctx}/EBILL/PAY/taiwaterpayment';
				$("form").attr("action", action);
    			$("form").submit();
            }
        }
        function init() {
        }
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item">信用卡</li>
			<li class="ttb-breadcrumb-item">繳費區</li>
		</ol>
	</nav>
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					信用卡繳費
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post">
					<input type="hidden" name="back" id="back" value=>	
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											繳費項目
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="PayItem" id="SelectMenu" onChange="changeItemStyle()" class="custom-select select-input">
												  <option value="#">---請選擇繳費項目---</option>
										          <option value="1">台灣自來水水費</option>
											</select>
										</div>
									</span>
								</div>
							</div>
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>