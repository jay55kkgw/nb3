<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<body>
    <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 密碼變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Change_Password" /></li>
		</ol>
	</nav>

		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.Change_SSL_Password" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

				<div class="main-content-block row">
					<div class="col-12">
					<!--新交易密碼變更成功  -->
					<h5 style="color:red"><spring:message code= "LB.New_SSL_Password" /><spring:message code= "LB.Change_successful" /></h5>
						<div class="ttb-input-block">							
							<!-- 系統時間 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
								<h4><spring:message code="LB.System_time" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										${ pw_alter_ssl_result.data.TIME }
									</div>
								</span>
							</div>
						</div>
					</div>
				</div>
				
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><spring:message code="LB.Pw_alter_ssl_step_P3_D1" /></li>
				</ol>
				
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	
		<script type="text/javascript">
			$(document).ready(function(){
				initFootable();
			});
		
		</script>
	
</body>
</html>