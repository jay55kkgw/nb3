<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<table class="print">
	<tr>
		<td style="text-align:center"><spring:message code="LB.Trading_time" /></td>
		<td >${CMTXTIME}</td>
	</tr>
	<tr>
		<td style="text-align:center"><spring:message code="LB.Omnibus_Account_No" /></td>
		<td>${FDPACN}</td>
	</tr>
</table>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" /> 
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.collateral_P3_D1" /></li>
<!-- 							<li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
		</ol>
	</div>
</body>
</html>