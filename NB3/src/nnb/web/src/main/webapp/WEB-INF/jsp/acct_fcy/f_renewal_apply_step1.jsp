<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript">
		
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			// 上一頁按鈕 click
			goBack();
			// 產生下拉選單 - 自動轉期
			generateAUTXFTM();
			// 回填上一頁資料
			refillData();
		}
		
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				// 送出進表單驗證前將span顯示
				$("#hideblock").show();
				
				console.log("submit~~");
	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					initBlockUI();	//遮罩
	                $("#formId").submit();
				}
			});
		}
		
		
		// 上一頁按鈕 click
		function goBack() {
			// 上一頁按鈕
			$("#CMBACK").click(function() {
				// 遮罩
				initBlockUI();
				
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				
				// 讓Controller知道是回上一頁
				$('#back').val("Y");
				// 回上一頁
				var action = '${__ctx}/FCY/ACCT/TDEPOSIT/f_renewal_apply';
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
		}
		
		// 產生下拉選單 - 自動轉期
	    function generateAUTXFTM() {
	        // 宣告變數，資料類型為物件 
	        var data1 = {}
	        var data2 = {}
	        var options = {
	            selectID: '#AUTXFTM'
	        }
	        // 01~09
	        for (i = 1; i <= 9; i++) {
	            data1['0' + i] = i;
	        }
	        // 10~98
	        for (i = 10; i <= 98; i++) {
	            data2[i] = i;
	        }
	        fstop.creatSelect(data1, options);
	        fstop.creatSelect(data2, options);
	    }
		
		//重新輸入
	 	function formReset() {
	 		if ($('#actionBar').val()=="reEnter"){
		 		$('#actionBar').val("");
		 		document.getElementById("formId").reset();
	 		}
		}
	    
		// 控制自動轉期下拉選單
		function change(obj) 
		{
			console.log('name=CODE value >> ' + obj.value);
			if (obj.value == 'C') 
			{
				$('#AUTXFTM').attr('disabled', true);
			}
		 	else if (obj.value == 'D') 
		 	{
				$('#AUTXFTM').attr('disabled', true);
		 	}
		 	else if (obj.value == 'A' || obj.value == 'B' || obj.value == 'E') 
		 	{ 	  
				$('#AUTXFTM').attr('disabled', false);
		 	}
	 	}
		
		
		// 回填上一頁資料
		function refillData()
		{
			var bk = '${bk_key}';
			if (bk == 'Y')
			{
				var	back = '${sessionScope.back_data}';
				back = JSON.parse(back);
				// 自動轉存期數 加selected
				var value = back.AUTXFTM;
				$('#AUTXFTM option[value='+ value +']').attr('selected', true);
				// 轉存方式 加Checked
				fstop.setRadioChecked("CODE", back.CODE, true);
				// 利息轉入帳號 加selected
				refillFYTSFAN(back);
			}
		}

		
		// 利息轉入帳號 加selected
		function refillFYTSFAN(back)
		{
			// 將後端送來的FYTSFAN放入array
			var arr = [back.FYTSFAN1, back.FYTSFAN2, back.FYTSFAN3, back.FYTSFAN4];
			// 檢查value是否有值
			for(var i=0; i <= arr.length; i++)
			{
				var value = arr[i];
				if(value != '' && value != null)
				{
					// 不為空，則將對應的select元素option設為selected
					var num = i + 1;	// select元素由1開始
					var name = 'FYTSFAN' + num;
					console.log('FYTSFAN name= ' + name);
					console.log('FYTSFAN value= ' + value);
					$("select[name='" + name + "'] option[value='" + value + "']").attr('selected', true);
				}
			}
		}
	</script>
</head>

<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存自動轉期申請/變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Time_Deposit_Automatic_Rollover_Application_Change" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- content row END -->
	<!-- 		主頁內容  -->
	<main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!--  外匯定存自動轉期申請/變更 -->
				<spring:message code="LB.FX_Time_Deposit_Automatic_Rollover_Application_Change" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block no-l-display-btn">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Execution_option" /></option>
						<!-- 重新輸入-->
					<option value="reEnter"><spring:message code="LB.Re_enter"/></option>
				</select>
			</div>
			<form id="formId" method="post" action="${__ctx}/FCY/ACCT/TDEPOSIT/f_renewal_apply_confirm">
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="display_CODE" name="display_CODE" value="">
				<input type="hidden" name="REQPARAMJSON" value='${f_renewal_apply_step1.data.fgselectJson}' />
				<c:set var="BaseResultData" value="${f_renewal_apply_step1.data }"></c:set>
        <!--交易步驟 -->
				<div id="step-bar">
					<ul>
				<!--輸入資料 -->
						<li class="active"><spring:message code="LB.Enter_data"/></li>
				<!-- 確認資料 -->
						<li class=""><spring:message code="LB.Confirm_data"/></li>
				<!-- 交易完成 -->
						<li class=""><spring:message code="LB.Transaction_complete"/></li>
					</ul>
        		</div>
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<!--   存單帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<p>
											${BaseResultData.fgselectMap.ACN}
										</p>
									</div>
								</span>
							</div>
							<!--   存單號碼  -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<p>
											${BaseResultData.fgselectMap.FDPNO}
										</p>
									</div>
								</span>
							</div>
							<!--   存單金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_amount" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span class="high-light">
											${BaseResultData.fgselectMap.CUID}
											${BaseResultData.display_BALANCE}
											</span>
											<!--元 -->
											<spring:message code="LB.Dollar"/>
										</p>
									</div>
								</span>
							</div>
							<!-- 轉存方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<!--  轉存方式 -->
										<h4><spring:message code="LB.Rollover_method" /></h4>
									</label>
								</span>
								<span class="input-block">
									<!-- 第一區 -->
									<div class="ttb-input">
										<label>
											<!-- 自動轉期 -->
											<spring:message code="LB.Automatic_renewal" />
											<select class="custom-select select-input half-input w-auto" name="AUTXFTM" id="AUTXFTM">
												<option value="99">
													<!--  無限次數 -->
													<spring:message code="LB.Unlimited_number_of_times" />
												</option>
											</select>
										</label>
									</div>
									<div class="ttb-input sen-input">
										<label class="radio-block">
											<!--本金利息一併轉期 -->
											<spring:message code="LB.Principal_and_Interest_rollover1"/>
											<input type="radio" value="A" name="CODE" checked onclick="change(this)">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input sen-input">
										<label class="radio-block">
											<!-- 本金轉期－利息按月轉入活存，轉入帳號 -->
											<spring:message code="LB.Principal_rollover_interest_monthly_to_demand_deposit" />
											，
											<!-- 轉入帳號 -->
											<spring:message code="LB.Payees_account_no"/>
											<input type="radio" value="B" name="CODE" onclick="change(this)">
											<span class="ttb-radio"></span>
											<br>
											<select class="custom-select select-input half-input" name="FYTSFAN1">
												<option value="">
													<!--  請選擇帳號 -->
													---<spring:message code="LB.Select_account" />---
												</option>
												<c:choose>
													<c:when test="${BaseResultData.isSubject51 == false}">
														<c:set var="oneRow" value="${BaseResultData}"></c:set>
														<option value="${oneRow.ACN}" selected> ${oneRow.TEXT}</option>
													</c:when>
													<c:when test="${BaseResultData.isSubject51 == true}">
														<c:forEach var="row" items="${ BaseResultData.REC }">
															<option value="${row.ACN}" > ${row.TEXT}</option>
														</c:forEach>
													</c:when>
												</c:choose>
											</select>
										</label>
									</div>
									<div class="ttb-input sen-input">
										<label class="radio-block">
											<!--本金轉期－利息於定存到期或解約轉入活存，轉入帳號 -->
											<spring:message code="LB.Principal_rollover-interest_to_demand_deposit_at_maturity_or_termination_input"/>
											<input type="radio" value="E" name="CODE" onclick="change(this)">
											<span class="ttb-radio"></span>
											<select class="custom-select select-input half-input"  name="FYTSFAN2">
												<option value="">
													<!--  請選擇帳號 -->
													---<spring:message code="LB.Select_account" />---
												</option>
												<c:choose>
													<c:when test="${BaseResultData.isSubject51 == false}">
														<c:set var="oneRow" value="${BaseResultData}"></c:set>
														<option value="${oneRow.ACN}" selected> ${oneRow.TEXT}</option>
													</c:when>
													<c:when test="${BaseResultData.isSubject51 == true}">
														<c:forEach var="row" items="${ BaseResultData.REC }">
															<option value="${row.ACN}" > ${row.TEXT}</option>
														</c:forEach>
													</c:when>
												</c:choose>
											</select>
										</label>
									</div>
									<!-- 第二區 -->
									<div class="ttb-input">
										<label>
											<!-- 不轉期 -->
											<spring:message code="LB.Not_automatic_renewal" />
										</label>
									</div>
									<div class="ttb-input sen-input">
										<label class="radio-block">
											<!--利息按月轉入活存，轉入帳號 -->
											<spring:message code="LB.Upon_maturity_or_terminating_credit_Interest_to_Account"/>
											<input type="radio" value="C" name="CODE" onclick="change(this)">
											<span class="ttb-radio"></span>
											<br>
											<select class="custom-select select-input half-input" name="FYTSFAN3">
												<option value="">
													<!--  請選擇帳號 -->
													---<spring:message code="LB.Select_account" />---
												</option>
												<c:choose>
													<c:when test="${BaseResultData.isSubject51 == false}">
														<c:set var="oneRow" value="${BaseResultData}"></c:set>
														<option value="${oneRow.ACN}" selected> ${oneRow.TEXT}</option>
													</c:when>
													<c:when test="${BaseResultData.isSubject51 == true}">
														<c:forEach var="row" items="${ BaseResultData.REC }">
															<option value="${row.ACN}" > ${row.TEXT}</option>
														</c:forEach>
													</c:when>
												</c:choose>
											</select>
										</label>
									</div>
									<div class="ttb-input sen-input">
										<label class="radio-block">
											<!--利息於定存到期或解約轉入活存，轉入帳號-->
											<spring:message code="LB.Interest_to_demand_deposit_at_maturity_or_termination_input"/>
											<input type="radio" value="D" name="CODE" onclick="change(this)">
											<span class="ttb-radio"></span>
											<br>
											<select class="custom-select select-input half-input" name="FYTSFAN4">
												<option value="">
													<!--  請選擇帳號 -->
													---<spring:message code="LB.Select_account" />---
												</option>
												<c:choose>
													<c:when test="${BaseResultData.isSubject51 == false}">
														<c:set var="oneRow" value="${BaseResultData}"></c:set>
														<option value="${oneRow.ACN}" selected> ${oneRow.TEXT}</option>
													</c:when>
													<c:when test="${BaseResultData.isSubject51 == true}">
														<c:forEach var="row" items="${ BaseResultData.REC }">
															<option value="${row.ACN}" > ${row.TEXT}</option>
														</c:forEach>
													</c:when>
												</c:choose>
											</select>
										</label>
									</div>
								</span>
							</div>
						</div>

							<!--回上頁 -->
							<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
							<input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray" >
							<!--重新輸入 -->
<%-- 							<spring:message code="LB.Re_enter" var="cmRest"></spring:message> --%>
<%-- 							<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray no-l-disappear-btn"> --%>
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">

					</div>
				</div>
				<div class="text-left">
							<!-- 		說明： -->
					<ol class="list-decimal description-list">
						<p><spring:message code="LB.Description_of_page"/></p>					
						<li><spring:message code="LB.F_renewal_apply_P2_D1"/> </li>
						<li><spring:message code="LB.F_renewal_apply_P2_D2"/> </li>
						<li><spring:message code="LB.F_renewal_apply_P2_D3"/></li>
						<li><spring:message code="LB.F_renewal_apply_P2_D4"/></li>
					</ol>
				</div>
			</form>
		</section>
		<!-- main-content END -->
	</main>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>