<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 	<!--   IDGATE --> 		 
	<%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 質借功能取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Pledged_Time_Deposit_Function_Application_Cancellation" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">		
				<h2><spring:message code="LB.Pledged_Time_Deposit_Function_Application_Cancellation" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div id="step-bar">
				    <ul>
				       <ul>
						<li class="finished">
							<spring:message code="LB.Enter_data" /><!-- 輸入資料 -->
						</li>
						<li class="finished">
							<spring:message code="LB.Confirm_data" /><!-- 確認資料 -->
						</li>
						<li class="active">
							<spring:message code="LB.Transaction_complete" /><!-- 交易完成 -->
						</li>
					</ul>
				    </ul>
				</div>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
					<h5 style="color:red"><spring:message code="LB.Transaction_successful" /></h5>
						<div class="ttb-input-block">							
<!-- 					交易時間-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
								<h4><spring:message code="LB.Trading_time" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										${transfer_result_data.data.CMTXTIME}
									</div>
								</span>
							</div>
<!-- 					綜合存款帳號-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
								<h4><spring:message code="LB.Omnibus_Account_No" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										${transfer_result_data.data.FDPACN}
									</div>
								</span>
							</div>							
						</div>
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
					<div class="text-left">
						<spring:message code="LB.Description_of_page" /> 
						<ol class="list-decimal text-left">
							<li><spring:message code="LB.collateral_P3_D1" /></li>
<!-- 							<li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
						</ol>
					</div>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
    <%@ include file="../index/footer.jsp"%>

		<script type="text/javascript">
			$(document).ready(function(){

				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"collateral_print",
						"jspTitle":"<spring:message code='LB.Pledged_Time_Deposit_Function_Application_Cancellation' />",
						"CMTXTIME":"${transfer_result_data.data.CMTXTIME }",
						"FDPACN":"${transfer_result_data.data.FDPACN}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});
			
			});
		
		</script>

</body>
</html>
