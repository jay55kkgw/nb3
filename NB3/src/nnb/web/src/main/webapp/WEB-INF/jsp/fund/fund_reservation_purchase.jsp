<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		setTimeout("initDataTable()",100);
	});
	
	// 畫面初始化
	function init() {
		//initFootable();
		$("#printbtn").click(function(){
			var params = {
					"jspTemplateName":"fund_reservation_purchase_print",
					"jspTitle":"<spring:message code= "LB.X1248" />",
					"CMQTIME":"${fund_reservation_purchase.data.CMQTIME}",
					"NAME":"${fund_reservation_purchase.data.NAME}",
					"REC_NO":"${fund_reservation_purchase.data.COUNT}"
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});		
	}
	
	// 確認鍵 Click
	function cancel(dataCount){
		initBlockUI();
    	// 將頁面資料塞進hidden欄位
    	console.log("dataCount: " + dataCount);
    	var data = $("#"+dataCount).val();
    	console.log("data: " + data);
		$("#ROWDATA").val(data);
    	
		$("#formId").submit();
    };

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Funds" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 預約申購交易查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X1230" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<!-- 預約交易查詢(申購)  -->
				<h2><spring:message code= "LB.X1248" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="${__ctx}/FUND/QUERY/fund_reservation_purchase_c">
					<input id="ROWDATA" name="ROWDATA" type="hidden" value="" />
					<input id="ACN1" name="ACN1" type="hidden" value="${fund_reservation_purchase.data.ACN1}" />
					<input id="ACN2" name="ACN2" type="hidden" value="${fund_reservation_purchase.data.ACN2}" />
					<!-- 顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間 -->
									<h3><spring:message code="LB.Inquiry_time" /></h3>
									<p>
										${fund_reservation_purchase.data.CMQTIME}
									</p>
								</li>
								<li>
									<!-- 交易種類 -->
									<h3><spring:message code="LB.W1060" /></h3>
									<p>
										<spring:message code="LB.W1061" />
									</p>
								</li>
								<li>	
									<!-- 總計筆數 -->
									<h3><spring:message code="LB.X0380" /></h3>
									<p>
										${fund_reservation_purchase.data.COUNT}&nbsp;<spring:message code="LB.Rows"/>
									</p>
								</li>
								<li>	
									<!-- 姓名 -->
									<h3><spring:message code="LB.Name" /></h3>
									<p>
										${fund_reservation_purchase.data.NAME}
									</p>
								</li>
							</ul>
							<!-- 預約交易查詢 -->
							<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
								<thead>
<%-- 									<tr> --%>
<!-- 										姓名 -->
<%-- 										<th><spring:message code="LB.Name"/></th> --%>
<%-- 										<th class="text-left" colspan="8">${fund_reservation_purchase.data.NAME}</th> --%>
<%-- 									</tr> --%>
									<tr>
										<!-- 預約日期 -->
										<th data-title='<spring:message code="LB.X0377" />'><spring:message code="LB.X0377" /></th>
										<!-- 基金名稱-->
										<th data-title='<spring:message code="LB.W0025" />'><spring:message code="LB.W0025"/></th>
										<!-- 申購金額 -->
										<th data-title='<spring:message code="LB.W1074" />'><spring:message code="LB.W1074"/></th>
										<!-- 手續費-->
										<th data-breakpoints="xs sm" data-title='<spring:message code="LB.D0507" />'><spring:message code="LB.D0507" /></th>
										<!-- 轉出帳號 -->
										<th data-breakpoints="xs sm" data-title='<spring:message code="LB.Payers_account_no" />'><spring:message code="LB.Payers_account_no"/></th>
										<!-- 扣款金額-->
										<th data-breakpoints="xs sm" data-title='<spring:message code="LB.W1079" />'><spring:message code="LB.W1079" /></th>
										<!-- 手續費率 -->
										<th data-breakpoints="xs sm" data-title='<spring:message code="LB.W1034" />'><spring:message code="LB.W1034" /></th>
										<!-- 選項 -->
										<th data-title='<spring:message code="LB.Option_item" />'><spring:message code="LB.Option_item"/></th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${empty fund_reservation_purchase.data.REC}">
										<tr style="display:none;">
											<td></td>
										</tr>
									</c:if>
									<c:if test="${not empty fund_reservation_purchase.data.REC}">
										<c:forEach var="dataList" items="${fund_reservation_purchase.data.REC}" varStatus="data">
											<tr>
												<!-- 預約日期 -->
												<td class="text-center">${dataList.TRADEDATE_1}</td>
												<!-- 基金名稱-->
												<td class="text-center">(${dataList.TRANSCODE})&nbsp;${dataList.FUNDLNAME}</td><!-- 代號+中文 -->
												<!-- 申購金額 -->
												<td class="text-center">
													${dataList.ADCCYNAME}<br><!-- 中文幣別 -->
													${dataList.AMT3_1}
												</td>
												<!-- 手續費-->
												<td class="text-center">
													${dataList.ADCCYNAME}<br><!-- 中文幣別 -->
													${dataList.FCA2_1}
												</td>
												<!-- 轉出帳號 -->
												<td class="text-center">${dataList.OUTACN_1}</td>
												<!-- 扣款金額-->
												<td class="text-center">
													${dataList.ADCCYNAME}<br><!-- 中文幣別 -->
													${dataList.AMT5_1}
												</td>
												<!-- 手續費率 -->
												<td class="text-right">${dataList.FCAFEE_1}%</td>
												<!-- 選項 -->
												<td>
													<input type="button" name="CMSUBMIT" id="CMSUBMIT" 
													onclick="cancel(${data.count});" 
													class="ttb-sm-btn btn-flat-orange" value="<spring:message code="LB.Cancel"/>"/>
												</td>
												<input type="hidden" id="FDTXTYPE" name="FDTXTYPE" value="<spring:message code="LB.W1061"/>">				
												<input type="hidden" id="action" name="action" value="forward">
												<input type="hidden" id="urlPath" name="urlPath" value="" value="">
												<input id="${data.count}" name="${data.count}" type="hidden" value="${dataList}" />
											</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
							<!-- 列印鈕-->					
							<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
						</div>
					</div>
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><span><spring:message code="LB.fund_reservation_purchase_P1_D1" /></span></li>
          				<li><span><spring:message code="LB.fund_reservation_purchase_P1_D2" /></span></li>
					</ol>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>