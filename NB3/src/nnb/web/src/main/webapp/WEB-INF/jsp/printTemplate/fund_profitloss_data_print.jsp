<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust:exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<div class="col-12">
		<ul class="ttb-result-list">
			<li>
				<p>
					<spring:message code="LB.Inquiry_time"/>：
					${CMQTIME}
				</p>
				<p>
					<spring:message code="LB.Total_records"/>：
					${dataCount}
					<spring:message code="LB.Rows"/>
				</p>
			</li>
		</ul>
		<table class="print">
			<tr>
				<td><spring:message code="LB.Name"/></td>
			 	<td colspan="9">${hiddenName}</td>
			</tr>
			<tr style="text-align: center">
				<td><spring:message code="LB.W0904"/><br/><spring:message code="LB.W0905"/></td>
			 	<td><spring:message code="LB.W0906"/><br/><spring:message code="LB.W0907"/></td>
				<td><spring:message code="LB.W0908"/><br/><spring:message code="LB.W0909"/></td>
				<td><spring:message code="LB.W0026"/><br/><spring:message code="LB.W0027"/>（A）</td>
				<td><spring:message code="LB.W0912"/><br/><spring:message code="LB.W0913"/>（B）</td>
				<td><spring:message code="LB.W0030"/>（C）</td>
				<td><spring:message code="LB.W0908"/><br/><spring:message code="LB.W0915"/>（A）＊（B）＊（C）</td>
				<td><spring:message code="LB.W0916"/><br/><spring:message code="LB.W0917"/></td>
				<td><spring:message code="LB.W0918"/><br/><spring:message code="LB.W0919"/></td>
			</tr>
			<c:forEach var="dataMap" items="${dataListMap[0].rows}" varStatus="status">
				<tr style="text-align: center">
					<td style="text-align: center">${dataMap.hiddenCDNO}<br/>${dataMap.TRANSCODE}／${dataMap.FUNDLNAME}</td>
				 	<td style="text-align: center">${dataMap.AC225Format}<br/>${dataMap.AC202Chinese}</td>
					<td style="text-align: center">${dataMap.CRY}<br/>${dataMap.TRANSCRY}</td>
					<td style="text-align: center">${dataMap.TOTAMTFormat}
						<br/>
						<c:if test="${dataMap.ACUCOUNTFormat == '0.0000'}">
							<spring:message code= "LB.W0921" />
						</c:if>
						<c:if test="${dataMap.ACUCOUNTFormat != '0.0000'}">
							${dataMap.ACUCOUNTFormat}
						</c:if>
					</td>
					<td style="text-align: center">${dataMap.NET01Format}<br/>${dataMap.REFUNDAMTFormat}</td>
					<td style="text-align: right">${dataMap.FXRATEFormat}</td>
					<td style="text-align: center">${dataMap.CRY}<br/>${dataMap.REFVALUEFormat}</td>
					<td style="text-align: center">
						<c:if test="${dataMap.DIFAMTDouble >= 0}">
							<font color="red">${dataMap.DIFAMTFormat}</font>
						</c:if>
						<c:if test="${dataMap.DIFAMTDouble < 0}">
							<font color="green" size="4">${dataMap.DIFAMTFormat}</font>
						</c:if>
						<br/>
						<c:if test="${dataMap.LCYRATDouble >= 0}">
							<font color="red">${dataMap.LCYRATFormat}％</font>
						</c:if>
						<c:if test="${dataMap.LCYRATDouble < 0}">
							<font color="green" size="4">${dataMap.LCYRATFormat}％</font>
						</c:if>
					</td>
					<td style="text-align: center">
						<c:if test="${dataMap.AMTDouble >= 0}">
							<font color="red">${dataMap.AMTFormat}</font>
						</c:if>
						<c:if test="${dataMap.AMTDouble < 0}">
							<font color="green" size="4">${dataMap.AMTFormat}</font>
						</c:if>
						<br/>
						<c:if test="${dataMap.FCYRATDouble >= 0}">
							<font color="red">${dataMap.FCYRATFormat}％</font>
						</c:if>
						<c:if test="${dataMap.FCYRATDouble < 0}">
							<font color="green" size="4">${dataMap.FCYRATFormat}％</font>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<br><br><br>
	<div class="col-12">
		<table class="print">
			<tr>
				<td colspan="7"><spring:message code="LB.X0393" /></td>        
			</tr>
			<tr style="text-align: center">
				<td><spring:message code="LB.W0908"/></td>
		        <td><spring:message code="LB.W0933"/></td>
		        <td><spring:message code="LB.W0934"/></td>
		        <td><spring:message code="LB.W0935"/></td>
		        <td><spring:message code="LB.W0936"/></td>
		        <td><spring:message code="LB.W0937"/></td>
		        <td><spring:message code="LB.W0938"/></td>
			</tr>
			<c:if test="${secondDataCount == 0}">
				<tr style="text-align: center">
					<td colspan="7"><spring:message code="LB.W0939"/></td>
				</tr>
			</c:if>
			<c:forEach var="secondDataMap" items="${dataListMap[0].secondRows}">
				<tr style="text-align: center">
					<td style="text-align: center">${secondDataMap.ADCCYNAME}</td>
					<td style="text-align: right">${secondDataMap.SUBTOTAMTFormat}</td>
					<td style="text-align: right">${secondDataMap.SUBREFVALUEFormat}</td>
					<td style="text-align: right">${secondDataMap.SUBDIFAMTFormat}</td>
					<td style="text-align: right">${secondDataMap.SUBLCYRATFormat}％</td>
					<td style="text-align: right">${secondDataMap.SUBDIFAMT2Format}</td>
					<td style="text-align: right">${secondDataMap.SUBFCYRATFormat}％</td>
				</tr>
			</c:forEach>
				<tr style="text-align: center">
					<td style="text-align: center"><spring:message code= "LB.W0940" /></td>
					<td style="text-align: right">${TOTTWDAMTFormat}</td>
					<td style="text-align: right">${TOTREFVALUEFormat}</td>
					<td style="text-align: right">${TOTDIFAMTFormat}</td>
					<td style="text-align: right">${TOTLCYRATFormat}％</td>
					<td style="text-align: right">${TOTDIFAMT2Format}</td>
					<td style="text-align: right">${TOTFCYRATFormat}％</td>
				</tr>
		</table>
		<ul class="ttb-result-list">
			<li class="full-list"><p><spring:message code="LB.W0941"/></p></li>
			<br><br>
			<c:if test="${dataListMap[0].thirdDataCount > 0}">
				<li class="full-list"><p><spring:message code="LB.W0942"/></p></li>
			</c:if>
			<table>
				<c:forEach var="thirdDataMap" items="${dataListMap[0].thirdRows}">
					<tr>
						<td class=" text-left">${thirdDataMap.UNALLOTAMTCRYADCCYNAME}&nbsp;</td>
						<td class=" text-right">${thirdDataMap.UNALLOTAMTFormat}</td>
						<td class=" text-right">&nbsp;&nbsp;&nbsp;<spring:message code="LB.W0921"/></td>
					</tr>
				</c:forEach>
			</table>
        </ul>
	</div>
	<div class="text-left">
		<spring:message code="LB.Description_of_page"/>
		<ol class="list-decimal text-left">
			<li><font color="red"><b><spring:message code="LB.Profitloss_Balance_P1_D1" /></b></font></li>
			<li><font color="red"><spring:message code="LB.Profitloss_Balance_P1_D2" /></font></li>
		</ol>
	</div>
</body>
</html>