<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"src="${__ctx}/js/jquery.datetimepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	function init() {
		//列印
		$("#CMPRINT").click(function() {
			var params = {
				"jspTemplateName" : "use_component_print",
				//電子銀行業務總契約書
				"jspTitle" : "<spring:message code= "LB.X1078" />",
				};
			openWindowWithPost(
				"${__ctx}/ONLINE/print",
				"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
				params);
		});
		// 確認鍵 click
		//goOn();
	}
	function processQuery(){
		// 遮罩
		
		listReaders();
	 	// 開發用繞過去
		//$("#formId").submit();
	}
	//取得讀卡機
	function listReaders(){
		if(window.console){console.log("listReaders...");}
		var CardReInsert = true;
		var GoQueryPass = true;
		ConnectCard("listReadersFinish");
	}
	//取得讀卡機結束
	function listReadersFinish(result){
		if(window.console){console.log("listReadersFinish...");}
		//成功
		if(result != "false" && result != "E_Send_11_OnError_1006"){
			//找出有插卡的讀卡機
			findOKReader();
		}
	}
	//找出有插卡的讀卡機
	function findOKReader(){
		FindOKReader("findOKReaderFinish");
	}
	var OKReaderName = "";
	//找出有插卡的讀卡機結束(有找到才會到此FUNCTION)
	function findOKReaderFinish(okReaderName){
		//ASSIGN到全域變數
		OKReaderName = okReaderName;
		//取得卡片銀行代碼
		getCardBankID();
		//拔插卡
		//removeThenInsertCard();
	}
	//取得卡片銀行代碼
	function getCardBankID(){
		if(window.console){console.log("getCardBankID...");}
		GetUnitCode(OKReaderName,"getCardBankIDFinish");
	}
	//取得卡片銀行代碼結束
	function getCardBankIDFinish(result){
		if(window.console){console.log("getCardBankIDFinish...");}
		if(window.console){console.log("result: " + result);}
		//成功
		if(result != "false"){
			//還要另外判斷是否為本行卡
			//是
			if(result == "05000000"){
				var formId = document.getElementById("formId");
				formId.ISSUER.value = result;
				
				showDialog("verifyPin",OKReaderName,"verifyPinFinish");
			}
			//不是
			else{
				//alert(GetErrorMessage("E005"));
				errorBlock(
						null, 
						null,
						[GetErrorMessage("E005")], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
	}
	var pazzword = "";
	//驗證卡片密碼
	function verifyPin(readerName,password,outerCallBackFunction){
		//ASSIGN到全域變數
		pazzword = password;
		
		VerifyPin(readerName,password,outerCallBackFunction);
	}
	//驗證卡片密碼結束
	function verifyPinFinish(result){
		//成功
		if(result == "true"){
			//繼續做
			SubmitForm();
		}
	}
	// 送交前押碼
	function SubmitForm(){
		if(window.console){console.log("SubmitForm...");}
		FinalSendout("MaskArea",true);
		getMainAccount();
	}
	//取得卡片主帳號
	function getMainAccount(){
// 		initBlockUI();
		GetMainAccount(OKReaderName,"getMainAccountFinish");
	}
	//取得卡片主帳號結束
	function getMainAccountFinish(result){
		//成功
		if(result != "false"){
			var formId = document.getElementById("formId");
			formId.ACNNO.value = result;
			
			//拔插卡
			removeThenInsertCard();
		}
		//失敗
		else{
			FinalSendout("MaskArea",false);
		}
	}
	//拔插卡
	function removeThenInsertCard(){
		console.log("removeThenInsertCard.OKReaderName: " + OKReaderName);
		RemoveThenInsertCard("MaskArea",60,OKReaderName,"removeThenInsertCardFinish");
	}
	//拔插卡結束(成功才會到此FUNCTION)
	function removeThenInsertCardFinish(){
		initBlockUI();
		//拔插後繼續
		generateTAC();
	}

	//卡片押碼
	function generateTAC(){
		if(window.console){console.log("preGenerateTAC...");}
		var TRMID = MakeTRMID();
		
		var formId = document.getElementById("formId");
		formId.TRMID.value = TRMID;
		
		var transData = "2500" + TRMID + formId.ACNNO.value;
		
		// 小鍵盤輸入密碼
		GenerateTAC(OKReaderName,transData,pazzword,"generateTACFinish");
	}

	//卡片押碼結束
	function generateTACFinish(result){
		if(window.console){console.log("generateTACFinish...");}
		//成功
		if(result != "false"){
			var TACData = result.split(",");
			
			var formId = document.getElementById("formId");
			formId.iSeqNo.value = TACData[1];
			formId.ICSEQ.value = TACData[1];
			formId.TAC.value = TACData[2];
			
			var ACN_Str1 = formId.ACNNO.value;
			if(ACN_Str1.length > 11){
				ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);
			}
			formId.ACNNO.value = ACN_Str1;
			$("#formId").attr("action","${__ctx}/ONLINE/APPLY/use_component_step1");
			//unBlockUI(initBlockId);
			formId.submit();
		}
		//失敗
		else{
			FinalSendout("MaskArea",false);
		}
	}
</script>
</head>

<body>
<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 交易機制所需畫面 -->
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Register" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 晶片金融卡申請網路銀行 -->
			<h2><spring:message code= "LB.X1077" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<input type="hidden" id="isBack" name="isBack" value="">
				<!-- 交易機制所需欄位 -->
				<input type="hidden" id="jsondc" name="jsondc" value="">
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<!--開發用欄位 -->
<!-- 				<input type="hidden" id="ISSUER" name="ISSUER" value="05000000"> -->
<!-- 				<input type="hidden" id="ACNNO" name="ACNNO" value="01062604936"> -->
<!-- 				<input type="hidden" id="TRMID" name="TRMID" value="00034808"> -->
<!-- 				<input type="hidden" id="iSeqNo" name="iSeqNo" value="00002532"> -->
<!-- 				<input type="hidden" id="ICSEQ" name="ICSEQ" value="00002532"> -->
<!-- 				<input type="hidden" id="TAC" name="TAC" value="1E506BBFA9ACC4D8"> -->
<!-- 				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value=""> -->
				<!-- 下一步驟需要的 -->
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<%@ include file="./use_compponent_contract.jsp"%>
						<!-- 取消 -->
						<input onclick="window.close()" class="ttb-button btn-flat-gray" type="button" value=" <spring:message code= "LB.Cancel" />" name="CMBACK">
						<!-- 列印 -->
						<input type="button" class="ttb-button btn-flat-gray" name="CMPRINT" id="CMPRINT" value=" <spring:message code= "LB.Print" />" />
						<!-- 我同意約定條款 -->
						<input type="BUTTON" class="ttb-button btn-flat-orange" value=" <spring:message code= "LB.W1554" />" name="CMSUBMIT" onclick="return processQuery();">
					</div>
				</div>
			</form>
		</section>
		</main>
	<!-- 		main-content END -->
	<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>