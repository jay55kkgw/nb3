<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
  <br /><br />
  <div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
  <br /><br />
  <div style="text-align:center">
    <font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
  </div>
  <br />
  <div style="text-align:center">
    ${MsgName}
  </div>
  <br />
  <!-- 查詢時間 -->
		<p>
			<spring:message code="LB.Inquiry_time" /> : ${CMQTIME}
		</p>
		<!-- 資料總數 : -->
		<p>
			<spring:message code="LB.Total_records" /> : ${COUNT}
			<!--筆 -->
			<spring:message code="LB.Rows" />
		</p>

  <table class="print">
    <tr>
      <th><spring:message code= "LB.X0050" /></th>
      <th style="width:6em"><spring:message code= "LB.X0051" /></th>
      <th><spring:message code="LB.Currency"/></th>
      <th><spring:message code="LB.Deposit_amount_1"/></th>
      <th><spring:message code="LB.W0136"/></th>
      <th><spring:message code="LB.Status"/></th>
      <th><spring:message code= "LB.X0052" /></th>
      <th><spring:message code="LB.Account"/></th>
      <th style="width:6em"><spring:message code="LB.Effective_date"/></th>
      <th style="width:6em"><spring:message code="LB.W0140"/></th>
    </tr>
    <c:forEach items="${print_datalistmap_data}" var="map" varStatus="index">
      <tr>
        <td class="text-center">${map.REFNO} </td>
        <td class="text-center">${map.TXDATE} </td>
        <td class="text-center">${map.TXCCY}</td>
        <td align="right">${map.showTXAMT}</td>
        <td class="text-center">${map.ORDNAME}</td>
        <td class="text-center"><spring:message code= "LB.X0053" /></td>
        <td class="text-center">${map.TXBRH}</td>
        <td class="text-center">${map.BENACC}</td>
        <td class="text-center">${map.VALDATE}</td>
        <td></td>
      </tr>
    </c:forEach>

  </table>
  <br>
  <br>
  <div class="text-left">
    <!-- 		說明： -->
    <spring:message code="LB.Description_of_page" />:
    <ol class="list-decimal text-left">
      <li><strong style="font-weight: 400"><spring:message code= "LB.F003_P1_D1-1" /><a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank"><spring:message code= "LB.F003_P1_D1-2" /></a><a
            href="${__ctx}/public/transferFeeRate.htm" target="_blank"><spring:message code= "LB.F003_P1_D1-3" /></a><a
            href="https://www.tbb.com.tw/exchange_rate" target="_blank"><spring:message code= "LB.F003_P1_D1-4" /></a></strong></li>

    </ol>
  </div>
</body>

</html>