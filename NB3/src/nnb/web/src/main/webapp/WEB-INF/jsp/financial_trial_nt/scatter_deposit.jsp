<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	
	<script type="text/JavaScript">
	
	$(document).ready(function() { 
		
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 完成畫面
		setTimeout("init()", 100);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		
	});
	
	function init(){
		
	 	for (i = 1; i < 21; i++) { 
	     	 $("#period").append($("<option value='" + i + "'>" + i + "</option>"));
		}
		
		//上一頁按鈕
		$("#previous").click(function() {
			initBlockUI();
			fstop.getPage('${pageContext.request.contextPath}'+'/FINANCIAL/TRIAL/NT/deposit_trial','', '');
		});
	}
	
	function chkdata(){
	    
	    if($("#principal").val() == ""){
	        //alert("<spring:message code= "LB.Alert082" />");
	        errorBlock(
					null, 
					null,
					["<spring:message code= "LB.Alert082" />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	        $("#principal").focus();
	        return false;
	    }
	    
	    if(isNaN($("#principal").val())){
	        //alert("<spring:message code= "LB.Alert083" />");
	        errorBlock(
					null, 
					null,
					["<spring:message code= "LB.Alert083" />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	        $("#principal").focus();
	        return false;
	    }
	    
	    if($("select#period")[0].selectedIndex == 0){
	        //alert("<spring:message code= "LB.Alert088" />");
	        errorBlock(
					null, 
					null,
					["<spring:message code= "LB.Alert088" />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	        $("select#period").focus();
	        return false;
	    }
	    
	    if($("#yrate").val() == ""){
	        //alert("<spring:message code= "LB.Alert086" />");
	        errorBlock(
					null, 
					null,
					["<spring:message code= "LB.Alert086" />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	        $("#yrate").focus();
	        return false;
	    }
	    
	    if(isNaN($("#yrate").val())){
	        //alert("<spring:message code= "LB.Alert087" />");
	        errorBlock(
					null, 
					null,
					["<spring:message code= "LB.Alert087" />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	        $("#yrate").focus();
	        return false;
	    }
	    
	    
	    return true;
	}

	function sum()
	{

	//本金
	var principal=validate_IntString($("#principal").val(),1)*1000;

	//年利率
	var yrate=validate_IntString($("#yrate").val(),1);

	//期間
	var period=validate_IntString($("#period").val(),0)*12;

	//月利率
	var mrate=yrate/1200;

	//利息
	var interest=0;


	var bcheck = chkdata();
	  if(bcheck == true){
	   /*
	    if(document.form1.type.selectedIndex == 0)//定期存款
	    {
	      interest=Math.round(principal*mrate*period)
	      total=principal+interest
	    }

	    else if(document.form1.type.selectedIndex == 1)//零存整付
	    {
	      for(var i=1;i<=period;i++)
	      {
	        interest=interest+(principal*Math.pow(1+mrate,i)-principal)
	      }
	      interest=Math.round(interest)
	      principal=principal*period
	      total=principal+interest
	    }
	    else if(document.form1.type.selectedIndex == 2) //整存整付儲蓄存款
	    {
	      var mratetemp=Math.pow(1+mrate,period)
	      interest=Math.round(principal*(mratetemp-1))
	      total=principal+interest
	    }
	    else if(document.form1.type.selectedIndex == 3) //整存零付儲蓄存款
	    {
	var YearRate = yrate;
	var MonthRate = YearRate/1200;
	var Money = principal;
	var N = period
	var MonthRate1 = MonthRate+1;
	var total = Math.round(MonthRate*Math.pow(MonthRate1, N)/(Math.pow(MonthRate1, N)-1)*Money);
	var interest = total*period-principal;

	    }
	    
	   
	    
	    if(document.form1.type.selectedIndex == 3){
//	      document.form1.result.value="本金:" + principal + "\n"
//	      + "利息:" + interest + "\n"
//	      +"每月領取:" + total;
	    }else{
//	      document.form1.result.value="本金:" + principal + "\n"
//	      + "利息:" + interest + "\n"
//	      + "本利和:" + total
	    }
	     */



	      for(var i=1;i<=period;i++)
	      {
	        interest=interest+(principal*Math.pow(1+mrate,i)-principal);
	      }
	      interest=Math.round(interest);
	      principal=principal*period;
	      total=principal+interest;
	      $("#result").val(fstop.formatAmt(String(total)));
	  }
	}

	</script>
</head>
 <body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 理財試算服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0087" /></li>
    <!-- 臺幣定期儲蓄存款試算     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0095" /></li>
    <!-- 零存整付     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_deposit_type_5" /></li>
		</ol>
	</nav>


	<!-- 快速選單及主頁內容 -->
	<!-- menu、登出窗格 --> 
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">	
			<!--主頁內容  -->
		<section id="main-content" class="container">
			<!--<h2><spring:message code="LB.Change_User_Name" /></h2>--> 
			<h2><spring:message code="LB.NTD_deposit_type_5" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
			<form  id="formId">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							
							<!-- 您現在存入新臺幣-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0089" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "principal"  id = "principal"  maxlength="100"  class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
										<span class="input-unit"><spring:message code="LB.X2218" /></span>
									</div>
								</span>
							</div>
							<!-- 期間 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
									 <h4><spring:message code="LB.X0090" /></h4>
								</label>
								</span>								
								<span class="input-block">
									<div class="ttb-input">
										<select id="period" name="period"  class="custom-select select-input half-input">
										<option value="#">---<spring:message code="LB.Select" />---</option>
										</select>
										<span class="input-unit"><spring:message code="LB.Year" /></span>
									</div>
								</span>
							</div>
							<!-- 年利率 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0091" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "yrate"  id = "yrate"  maxlength="10"  class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
											<span class="input-unit">%</span>
										 <br> 
										 <span class="input-unit">
										 	「<a href="https://www.tbb.com.tw/web/guest/-82"  target="_blank"><spring:message code="LB.X0092" /></a>」
										 </span>
									</div>
								</span>
							</div>
							<!--到期後本利和新臺幣-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X1154" />&nbsp;&nbsp;<spring:message code="LB.NTD" /> </h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "result"  id = "result"  maxlength="100"   readonly="readonly" class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
									</div>
								</span>
							</div>
						</div>
						<!-- <input id="pageshow" name="pageshow" type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" /> -->
						<input id="btnReset" name="btnReset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />						
						<input id="previous" name="previous" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						<input id="btnCount" name="btnCount" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0094" />"  onclick="sum()" />
					</div>
				</div>
				</form>
					
						<ol class="list-decimal description-list">
							<!--<spring:message code="LB.Username_alter_P1_D1" />-->
							<p><spring:message code="LB.Description_of_page" /></p>
							<li><spring:message code="LB.Scatter_Deposit_P1_D1" /></li>
						</ol>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>
	<!-- content row END -->
    <%@ include file="../index/footer.jsp"%>

</body>
</html>
