<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<section id="main-content" class="container position-absolute" style="display:none">
	<div class="pupup-block">

		<div class="card-block shadow-box terms-pup-blcok pipa">
			<!-- n_e_e_d t_o f_i_x p_o_s_i_t_i_o_n -->
			<button type="button" class="popup-close-btn d-none"
				data-dismiss="modal" aria-label="Close">×</button>
			<h2 class="ttb-pup-h2">個人資料保謢法告知事項</h2>

			<div class="col-12 tab-content" id="nav-tabContent">
				<div class="ttb-input-block tab-pane fade show active"
					id="nav-trans-1" role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!-- 個人資料保護法告知事項 -->
							<%@ include file="../term/NA03_2.jsp"%>
						</div>
					</div>
				</div>
				<input type="BUTTON" class="ttb-button btn-flat-gray" value="返回" name="" onclick="$('#main-content').hide();">
			</div>
		</div>
	</div>
</section>
