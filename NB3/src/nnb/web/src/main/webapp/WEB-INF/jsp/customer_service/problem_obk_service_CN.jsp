<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="page2" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-21" aria-expanded="true"
				aria-controls="popup1-21">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>网路银行提供那些功能？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-21" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>网络银行提供十类功能，介绍如下：</p>
							<div style="overflow: auto">
								<table class="question-table">
									<thead>
										<tr>
											<th>项目</th>
											<th>内容</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>服务总览</th>
											<td style="text-align: left;">账户总览</td>
										</tr>
										<tr>
											<th>台币服务</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">账户查询：账户余额查询、账户明细查询、虚拟账号入账明细</li>
													<li data-num="2.">转账交易：转账交易</li>
													<li data-num="3.">转出记录：转出记录查询</li>
													<li data-num="4.">预约交易：预约交易查询/取消、预约交易结果查询</li>
													<li data-num="5.">定存服务：转入综存定存、定存明细查询、定存自动转期申请/变更、定存单到期续存、综存定存解约、零存整付按月缴存</li>
													<li data-num="6.">轻松理财户：存折明细、证券交割明细、自动申购基金明细</li>
													<li data-num="7.">票据查询：托收票据明细、支存当日不足扣票据明细、已兑现票据明细</li>
													<li data-num="8.">中央登录债券：余额查询、明细查询</li>
													<li data-num="9.">质借功能：质借功能取消 </li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>外币服务</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">账户查询：账户余额查询、活存明细查询</li>
													<li data-num="2.">换汇/转账交易：买卖外币/约定转账 </li>
													<li data-num="3.">转出记录：转出记录查询 </li>
													<li data-num="4.">汇出汇款：汇出汇款 、汇出汇款查询</li>
													<li data-num="5.">汇入汇款：汇入汇款查询、在线解款、在线解款申请/注销</li>
													<li data-num="6.">预约交易：预约交易查询/取消、预约交易结果查询</li>
													<li data-num="7.">定存服务：转入外币综存定存、定存明细查询、综存定存解约、定存自动转期申请/变更、存单到期续存</li>
													<li data-num="8.">进出口服务：进口到单查询、出口押汇查询</li>
													<li data-num="9.">信用状服务：通知查询、开状查询</li>
													<li data-num="10.">托收查询：进口/出口托收查询、光票托收查询</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>缴费缴税</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">缴税：缴税</li>
													<li data-num="2.">公用事业费：台湾电力公司、台湾自来水公司、台北自来水事业处、欣欣瓦斯 </li>
													<li data-num="3.">健保/劳保/国保/劳退金：健保费/补充保险费、劳保费、国民年金保险费、新制劳工退休金 </li>
													<li data-num="4.">学杂费：学杂费</li>
													<li data-num="5.">其他费用：期货保证金、缴费</li>
													<li data-num="6.">自动扣缴服务：自动扣缴查询/取消、台湾电力公司、台湾自来水公司、台北自来水事业处、中华电信费、健保费、劳保费、新制劳工退休金、旧制劳工退休金、停车费</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>贷款服务</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">小额信贷：申请小额信贷、申请数据查询</li>
													<li data-num="2.">借款查询：借款明细查询、下期借款本息查询、已缴款本息查询 </li>
													<li data-num="3.">房屋担保借款：缴息清单 </li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>基金</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">基金查询：基金余额/损益查询、基金交易明细查询、定期投资约定数据查询、国内基金净值查询、国外基金净值查询、信托契约书查询、预约查询/取消</li>
													<li data-num="2.">海外债券查询：海外债券余额/损益查询、海外债券交易明细查询</li>
													<li data-num="3.">基金交易：单笔申购、定期投资申购、转换交易、赎回交易、定期投资约定变更、停损/停利通知设定、SMART FUND自动赎回设定</li>
													<li data-num="4.">投资属性评估 (KYC评估)：投资属性评估调查表</li>
													<li data-num="5.">电子对账单：账单申请、密码重置、密码变更</li>
													<li data-num="6.">境外信托商品：商品推介申请、商品推介终止</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>黄金存折</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">查询服务：黄金存折余额查询、黄金存折明细查询、当日价格查询、历史价格查询、预约取消/查询</li>
													<li data-num="2.">黄金交易：黄金申购、黄金回售、缴纳定期扣款失败手续费</li>
													<li data-num="3.">定期定额交易：定期定额申购、定期定额变更、定期定额查询</li>
													<li data-num="4.">黄金存折账户：黄金存折帐户申请、黄金存折网络交易申请</li>
													<li data-num="5.">投资属性评估 (KYC评估)：投资属性评估调查表</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>信用卡</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">申请信用卡：申请信用卡、申请进度查询</li>
													<li data-num="2.">帐务查询：信用卡持卡总览、未出账单明细查询、历史账单明细查询、缴款纪录查询</li>
													<li data-num="3.">缴信用卡费：缴本行信用卡费、自动扣款申请/取消</li>
													<li data-num="4.">电子对账单：电子对账单申请、密码重置、密码变更</li>
													<li data-num="5.">实体对账单：实体账单补寄</li>
													<li data-num="6.">信用卡服务：信用卡开卡、信用卡挂失</li>
													<li data-num="7.">预借现金：密码函补寄</li>
													<li data-num="8.">信用卡分期服务：签署信用卡分期付款同意书、长期使用循环信用申请分期还款</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>在线服务专区</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">行动银行服务：启用行动银行服务</li>
													<li data-num="2.">约定转入账号服务：约定转入账号设定、约定转入账号取消</li>
													<li data-num="3.">电子对账单：电子对账单申请、密码重置、密码变更</li>
													<li data-num="4.">转账功能：转账功能申请/取消</li>
													<li data-num="5.">存款余额证明：存款余额证明申请</li>
													<li data-num="6.">票据服务：支票存款开户申请、空白票据</li>
													<li data-num="7.">信用卡服务：申请信用卡、申请信用卡进度查询、签署信用卡分期付款同意书、长期使用循环信用申请分期还款</li>
													<li data-num="8.">小额信贷：申请小额信贷、申请数据查询</li>
													<li data-num="9.">基金服务：预约开立基金户</li>
													<li data-num="10.">投资属性评估 (KYC评估)：投资属性评估调查表</li>
													<li data-num="11.">境外信托商品：商品推介申请、商品推介终止</li>
													<li data-num="12.">随护神盾：申请随护神盾</li>
													<li data-num="13.">理财试算服务：台币定期存款试算、台币定存储蓄存款试算、外汇定期存款利息试算、贷款摊还试算、年金计划试算、定期定额投资试算</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>个人服务</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">账户总览设定：账户总览设定</li>
													<li data-num="2.">账户设定：常用账号设定、用户名称变更、密码变更、网络银行交易密码在线解锁、Email设定</li>
													<li data-num="3.">通讯数据变更：往来帐户及信托业务通讯地址/电话变更、外汇进口/出口/汇兑通讯地址/电话变更、信用卡账单地址/电话变更</li>
													<li data-num="4.">通知服务：通知服务设定、国内台币汇入汇款通知设定</li>
													<li data-num="5.">挂失/结清销户：挂失服务、台币存款帐户结清销户申请、外汇存款帐户结清销户申请</li>
													<li data-num="6.">扣缴凭单：各类所得扣缴凭单打印</li>
												</ol>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-22" aria-expanded="true"
				aria-controls="popup1-22">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>网路银行交易的最高限额？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-22" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>一、依本行现行安控机制，以统一编号归户，订定已约定及非约定转帐帐号每日交易限额如下：</p>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th rowspan="2">安控机制</th>
											<th colspan="2">约定转账</th>
											<th colspan="3">非约定转账</th>
											<th colspan="3">在线约定转入账号</th>
										</tr>
										<tr>
											<th>自行</th>
											<th>跨行</th>
											<th>自行</th>
											<th>跨行</th>
											<th>合并</th>
											<th>自行</th>
											<th>跨行</th>
											<th>合并</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>交易密码</th>
											<td rowspan="5">500 万</td>
											<td rowspan="5">200 万</td>
											<td>-</td>
											<td>-</td>
											<td>-</td>
											<td rowspan="5" colspan="3">单笔5 万<br>每日10 万<br>每月20 万元整。</td>
										</tr>
										<tr>
											<th>芯片金融卡</th>
											<td rowspan="4" colspan="3">单笔5 万<br>每日10 万<br>每月20 万元整。</td>
										</tr>
										<tr>
											<th>动态密码卡</th>
										</tr>
										<tr>
											<th>随护神盾</th>
										</tr>
										<tr>
											<th>台湾Pay<br>(快速交易)</th>
										</tr>
										<tr>
											<th>电子签章</th>
											<td>1,500 万</td>
											<td>500 万(每笔200 万元)</td>
											<td>300 万</td>
											<td>200 万(不含外汇汇出汇款)</td>
											<td>-</td>
											<td>300 万</td>
											<td>200 万</td>
											<td>-</td>
										</tr>
										<tr>
											<th>备注</th>
											<td colspan="8" style="text-align: left;">
												<ol class="list-decimal">
													<li data-num="1.">上列密码/电子签章/芯片金融卡/动态密码卡/随护神盾之每日转账限额合并计算以2000万元为上限。</li>
													<li data-num="2.">台外币交易限额合并计算。</li>
													<li data-num="3.">行动银行交易限额与一般网银合并计算。</li>
												</ol>
											</td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="ttb-result-list terms">
								<p>二、其他交易限额及相关规定：</p>
								<ol>
									<li>（一）数位存款帐户转帐限额：同一客户之「数位存款帐户」合计非约定转帐金额，每笔限新台币5万元整，每日限新台币10万元整，每月限新台币20万元；客户亲临服务分行申请「约定转帐」功能后，则比照贵行一般存款帐户临柜约定转帐之交易限额，依不同安控机制订定之每日交易限额数字存款帐户转账限额: 同一客户合计非约定转账金额，数字存款帐户第一类及第二类每笔限新台币5万元整，每日限新台币10万元整，每月限新台币20万元整;客户亲临服务分行申请「约定转账」功能后，则比照本行一般存款帐户临柜约定转账之交易限额，依不同安控机制订定之每日交易限额。数字存款帐户第三类其转入非本人账户之约定、非约定转账及本人账户之跨行转账限额每笔限新台币1万元整，每日限新台币3万元整，每月限新台币5万元整；转入本人本行账户之非约定转账限额每笔限新台币5万元整，每日限新台币10万元整，每月限新台币20万元整；转入本人本行账户之临柜及在线约定转账之交易限额比照本行一般存款帐户。</li>
									<li>（二）转入综存定期约定与非约定帐户两者合计每一转入帐户，当月累计限额100万元。 (自104年9月1日适用)</li>
									<li>（三）「缴费/税」类：
										<ol>
											<li data-num="1.">以「晶片金融卡」、「动态密码卡」及「随护神盾」方式交易者与「密码方式（SSL机制）」约定转帐之跨行转帐交易合并计算限额。
											</li>
											<li data-num="2.">以「电子签章」方式交易者与电子签章非约定转帐之跨行转帐交易合并计算限额。</li>
										</ol>
									</li>
									<li>（四）基金申购：每人每营业日以电子化交易方式（含网路/语音）进行基金交易转出之金额，合计不得逾等值新台币伍佰万元整。
									</li>
									<li>（五）黄金申购：每笔最低交易量为1公克，每日累计买进最高交易数量每一帐户为50,000公克。</li>
									<li>（六）外汇交易：
										<ol>
											<li data-num="1.">外汇转帐/外汇汇出汇款：
												<ol>
													<li data-num="(1)">同一户名外币与台币互转，外币结购或结售应个别累积，每日累积最高限额应低于新台币50万元，并与电话银行转帐、金融卡提领外币合并计算限额。
													</li>
													<li data-num="(2)">未满20岁自然人，同一外汇存款帐户不同币别间户转、不同帐户间汇转帐（含汇出汇款）分别计算，每日累积最高限额应低于新台币50万元。
													</li>
													<li data-num="(3)">个人户结购或结售人民币应个别累积，每人每日透过帐户买卖之金额，不得逾人民币二万元。
													</li>
													<li data-num="(4)">领有中华民国国民身分证之个人始能申办人民币汇款至大陆地区，且每人每日之限额为人民币八万元。
													</li>
												</ol> ＊上述(3)(4)项若涉及人民币买卖者，个人户仍应受人民币2万元之限制。
											</li>
											<li data-num="2.">外汇汇入汇款线上解款：
												<ol>
													<li data-num="(1)">）存入台币帐户应低于新台币50万元。</li>
													<li data-num="(2)">存入外存帐户无金额限制，惟未满20岁自然人每日应低于等值新台币50万元。
													</li>
												</ol>
											</li>
										</ol>
									</li>
									<li>（七）转出帐户无交易金额限制项目：
										<ol>
											<li data-num="1.">同一外汇存款帐户不同币别之转帐交易。</li>
											<li data-num="2.">同一外汇综合存款帐户之活期存款与定期存款间转帐交易。</li>
										</ol>
									</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-23" aria-expanded="true"
				aria-controls="popup1-23">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>网路银行的服务时间</span>
					</div>
				</div>
			</a>
			<div id="popup1-23" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>外汇转帐服务时间为周一至周五银行营业日上午9：10至下午3：30。其他项目为24小时服务，惟跨行转帐转入时间依照各转入行库作业规定。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-24" aria-expanded="true"
				aria-controls="popup1-24">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>申请网路银行，是不是可以查到本人〈本公司〉的所有帐户？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-24" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>是的，您可查询到开立于本行您本人（本公司）全部帐户交易明细资料。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-25" aria-expanded="true"
				aria-controls="popup1-25">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>请问网路银行提供非约定转帐功能？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-25" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<p>是的，客户得使用「电子签章」或「本行晶片金融卡」+「读卡机」执行非约定转帐交易。</p>
								<ol>
									<li data-num="1.">电子签章: 需申请金融XML凭证，请依一:【申请资格】A04办理申请及下载凭证。
									</li>
									<li data-num="2.">本行晶片金融卡:持本行晶片金融卡+读卡机，且该晶片金融卡已具备非约定转帐功能者:
										<ol>
											<li data-num="(1)">临柜申请一般网路银行客户，得以该晶片金融卡之主帐号执行新台币非约定转帐交易及其他交易类服务。
											</li>
											<li data-num="(2)">以「本行晶片金融卡」线上申请之客户，仅具有查询类功能，如您持有的「本行晶片金融卡」具备非约定转帐功能者，可登入一般网路​​银行线上申请交易服务功能，即得以该晶片金融卡之主帐号执行新台币非约定转帐交易及其他交易类服务。
											</li>
										</ol>
									</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-26" aria-expanded="true"
				aria-controls="popup1-26">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>请问网路银行提供哪些外汇功能？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-26" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>是。本行网路银行提供外汇存款帐务查询、外汇综存定存、外汇综存定存解约、外汇转帐及外汇汇出汇款等功能。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-27" aria-expanded="true"
				aria-controls="popup1-27">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>请问网路银行外汇转帐除了最高交易金额限制外，是否有最低交易金额及次数的限制？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-27" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>是。单一客户外汇存款币转及结购、结售交易每笔在等值新台币1,000元以下者，同一日交易累计次数(含一般网路银行、企业网路银行、行动银行及电话银行)合计以20次为限。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-28" aria-expanded="true"
				aria-controls="popup1-28">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>我可以使用「一般网路银行」扣缴停车费？如何申请?需支付手续费？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-28" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>可以，但限台湾企银存户专用。请登入「一般网路银行」约定帐户代扣缴本人或他人之停车费，目前提供代缴台北市、新北市、高雄市路边停车费，操作流程如下：</p>
							<p>※尚未申请网路银行者，请先至营业单位申请「一般网路银行」，并约定转帐功能。</p>
							<p>※申请生效后，您的停车单将由系统自动扣款代缴，并E-MAIL通知您缴款结果，亦可登入「一般网路银行」查询。</p>
							<p>※扣缴停车费免支付手续费，欢迎多加利用。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-29" aria-expanded="true"
				aria-controls="popup1-29">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>申请路边停车费代扣缴服务何时生效扣款？假设开单日为99年4月2日，贵行何时代扣缴？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-29" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">当天申请次日生效，举例说明：
										<ol>
											<li data-num="(1)">客户4月2日上本行网路银行申请路边停车费代扣缴，则客户开单日为4月3日(含)以后之路边停车费，本行将自约定之扣款帐号扣款。
											</li>
										</ol>
									</li>
									<li data-num="2.">依不同县市停管处与本行约定之扣缴日不同，于不同日期扣款，就目前本行代扣台北市、新北市及高雄市路边停车费时间分析如下：
										<ol>
											<li data-num="(1)">台北市：开单日起算之第5天扣款，不分例假日，以4月2日(周五)开单日为例，则扣款日为4月6日，若开单日为4月7日，则扣款日为4月11日(周日)。
											</li>
											<li data-num="(2)">新北市：开单日起算起之第6天扣款，不分例假日，以4月2日(周五)开单日为例，则扣款日为4月7日，若开单日为4月7日，则扣款日为4月12日(周一)。
											</li>
											<li data-num="(3)">高雄市：开单日算起之第5天停管处传档，第6天扣款，惟传档日适逢例假日停管处不传档，则顺延次日扣款，以4月2日(周五)开单日为例，则扣款日为4月7日，若开单日为4月7日，则扣款日为4月13日(周二，因4月11日为例假日，顺延至4月12日)(周一收到代扣档，隔日扣款)。
											</li>
										</ol>
									</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-210" aria-expanded="true"
				aria-controls="popup1-210">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>重覆缴款如何处理？已注销网路银行路边停车费代扣服务，注销日前之重覆缴款，贵行是否自动退费？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-210" class="ttb-pup-collapse collapse"
				role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">客户已申请本行网路银行路边停车费代扣缴服务，又持帐单至超商缴费，处理情形如下：
										<ol>
											<li data-num="(1)">如申请代扣缴台北市及新北市路边停车费：由本行系统于接获停车管理处退费通00过后径行退入客户与本行约定扣款帐户。
											</li>
											<li data-num="(2)">如申请代扣缴高雄市路边停车费：于99年6月30日(含)前，请客户径向高雄市政府交通局要求退费，自99年7月1日起，将由本行系统于接获停车管理处退费通知，于当日24:00过后径行退入客户与本行约定扣款帐户。
											</li>
										</ol>
									</li>
									<li data-num="2.">如于注销日前收到停车费退款资料，系统于隔日退款至约定帐户，惟如于注销日后(含当日)收到停车费退款资料，请客户径行至停车单所属县市政府停车管理处申请退费。
									</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q11 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-211" aria-expanded="true"
				aria-controls="popup1-211">
				<div class="row">
					<span class="col-1">Q11</span>
					<div class="col-11">
						<span>假设客户于99年4月2日已注销网路银行路边停车费代扣服务，则贵行代扣至开单日为何日？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-211" class="ttb-pup-collapse collapse"
				role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">台北市于4月2日注销，本行只处理至前一日收到之停车单资料，前一日收到之停车单资料为收档日期减三天，因此只处理至开单日为3月30日为止之停车单。
									</li>
									<li data-num="2.">新北市于4月2日注销，本行只处理至前一日收到之停车单资料，前一日收到之停车单资料为收档日期减五天，因此只处理至开单日为3月28日为止之停车单。
									</li>
									<li data-num="3.">高雄市于4月2日注销，本行只处理至前一日收到之停车单资料，前一日收到之停车单资料为收档日期减五天，因此只处理至开单日为3月28日为止之停车单。
									</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q12 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-212" aria-expanded="true"
				aria-controls="popup1-212">
				<div class="row">
					<span class="col-1">Q12</span>
					<div class="col-11">
						<span>如何申请网路银行电子帐单？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-212" class="ttb-pup-collapse collapse"
				role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>网路银行提供电子交易对帐单、基金电子对帐单及信用卡电子帐单，请登入一般网银「在线服务专区」、「基金」或「信用卡」类选项，申请电子帐单；如电子邮箱位址变更，务请登录变更，以利寄发电子帐单。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q13 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-213" aria-expanded="true"
				aria-controls="popup1-213">
				<div class="row">
					<span class="col-1">Q13</span>
					<div class="col-11">
						<span>如何开启电子交易对帐单？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-213" class="ttb-pup-collapse collapse"
				role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">初次使用前，请依电子对帐单邮件指示安装浏览程式(同一台电脑安装一次即可)。</li>
									<li data-num="2.">点选电子邮件之附加档案并输入密码：依您的统一编号/身分证字号，如为10码者请输入后8码；如小于10码者，请输入全部证号。
									</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q14 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-214" aria-expanded="true"
				aria-controls="popup1-214">
				<div class="row">
					<span class="col-1">Q14</span>
					<div class="col-11">
						<span>如何使用通知服务功能？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-214" class="ttb-pup-collapse collapse"
				role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>请登入网路银行「个人服务」－「Email设定」的「我的Email」，首次设定完成后，即启用Email通知服务功能，提供转帐成功、预约即将扣款、预约交易结果、定存即将到期通知、基金停利/停损点及相关服务及优​​惠讯息等多项通知服务。提醒您，为了能顺利通知您，请确认您的Email位址的正确性，谢谢！
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q15 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-215" aria-expanded="true"
				aria-controls="popup1-215">
				<div class="row">
					<span class="col-1">Q15</span>
					<div class="col-11">
						<span>如何线上申请金融卡交易服务功能？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-215" class="ttb-pup-collapse collapse"
				role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>提供以「本行晶片金融卡」线上申请一般网路银行客户，登入一般网银「在线服务专区」服务点选「金融卡线上申请/取消交易服务功能」申请即可。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q16 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-216" aria-expanded="true"
				aria-controls="popup1-216">
				<div class="row">
					<span class="col-1">Q16</span>
					<div class="col-11">
						<span>人在国外忘记网路银行密码或者网路银行密码遭锁定？ </span>
					</div>
				</div>
			</a>
			<div id="popup1-216" class="ttb-pup-collapse collapse"
				role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>为了网路交易安全的控管，网路银行交易密码锁住，须本人亲临至柜台办理密码重置。若身处国外，您可委托家人处理，但需请您洽驻外单位办理授权书认证，并将此认证授权书寄回台湾给您的代理人。代理人收到认证授权书后，再请代理人持此认证授权书、授权人之开户印鉴、身分证明文件以及代理人的印鉴、身分证明文件于营业时间至分行办理密码重置。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>