﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><spring:message code="LB.X1724"/></title>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/css-reset.css" />
<link rel="stylesheet" type="text/css" href="${__ctx}/css/layout.css" />
<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>

<script type="text/javascript">
	function fillData(id) {
		var o = self.opener.document.getElementById("INTSACN1");

		if (o != undefined){
			o.value = id;
		}
		self.close();
	}
</script>
</head>
<body>
	<form name="main" id="main" method="post" action="" onSubmit="return processQuery()">
		<input type="hidden" name="ADOPID" value="N171">
		<div id="PrintLogo" class="NoScreen">
			<img src="${__ctx}/img/logo1.gif" alt="Logo" />
		</div>
		<div id="PrintArea" class="WidthS">
			<div id="TitleBar">
				<h4><spring:message code="LB.X1724"/></h4>
			</div>

			<table width="100%" class="DataBorder">
				<tr>
					<td class="ColorCell" style="width: 8em"><spring:message code= "LB.D0059" /></td><!-- 縣市別 -->
					<td class="DataCell1"><script>
						ShowTaxDataTitle();
					</script></td>
				</tr>
			</table>

			<script>
				CreateAreaTables();
				ShowTable(1);
			</script>
		</div>
	</form>
</body>
</html>
