<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- DIALOG會用到 -->
	<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">
	<style>
		.ui-dialog-titlebar-close {
		display: none;
	}
	</style>
	<script type="text/javascript">
	
		$(document).ready(function () {
			var url = '';
			$("#redirectDialog").dialog({
				autoOpen:true,
				modal:true,
				closeOnEscape:false,
				width:850,
				height:200,
				position:{
					my:"center",of:window
				}
			});
			
			init();
			
			setTimeout(function(){ $("#GO").click(); },5000);
		});
		
		function init(){
			
		var type = "${TYPE}";
		var transcode = "${TRANSCODE}";
		
			switch (type) {
			case '1':
				url = "https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + transcode + "&B=02";
				console.log("TYPE 1")
				break;
			case '2':
				url = "https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + transcode + "&B=02";
				console.log("TYPE 2")
				break;
			case '7':
				url = "https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + transcode + "&DownFile=99";
				console.log("TYPE 7")
				break;
			case '8':
				url = "https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + transcode + "&DownFile=1";
				console.log("TYPE 8")
				break;
			case '9':
				url = "https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + transcode + "&B=08";
				console.log("TYPE 9")
				break;
			case 'D':
				console.log("TYPE D")
				url = "https://tbb.moneydj.com/W/WW/WW01.DJHTM"
				break;
			case 'F':
				console.log("TYPE F")
				url = "https://tbb.moneydj.com/W/WC/WC02.DJHTM"
				break;
			default:
				url = "https://tbb.moneydj.com"
				console.log("TYPE DEFAULT")
				break;
			}
			console.log("url :" + url);
			
			//立即前往
			$("#GO").click(function() {
				window.location.href = url;
			});
			
			//關閉
			$("#CLOSE").click(function() {
				window.close();
			});
		}
	</script>
</head>
<body>
	<div id="redirectDialog" title=" 即將跳轉至 臺灣企銀 基金理財網   ">
		<div style="text-align:center">
			<table id="countDownTable" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
					<tr>
						即將於五秒後跳轉至 臺灣企銀 基金理財網
					</tr>
			</table>
			<input type="button" id="GO" value="立即前往"  class="ttb-button btn-flat-orange"/>
			<input type="button" id="CLOSE" value="關閉視窗" class="ttb-button btn-flat-orange"/>
		</div>
	</div>
</body>
</html>