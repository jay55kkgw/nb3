<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
    <div id="step-bar">
        <ul>
			<li class="finished">注意事項與權益</li>
            <li class="finished">設定帳戶</li>
            <li class="finished">確認資料</li>
            <li class="active">申請結果</li>
        </ul>
    </div>
	
	<div>
		<div class="ttb-message">
			<p>申請結果</p>
		</div>
		<table class="print">
			<tr>
				<!-- 申請時間 -->
				<td style="text-align:center"><spring:message code= "LB.D1097" /></td>
				<td>${CMQTIME }</td>
			</tr>
			<tr>
				<!-- 帳號 -->
				<td style="text-align:center"><spring:message code= "LB.D1090" /></td>
				<td>
					<c:forEach var="dataList" items="${print_datalistmap_data}">
						<div class="ttb-input">
							<span>${dataList.GDACN}</span>
							<c:if test="${dataList.msgName == null}">
								<!-- 成功 -->
								<span class="application-correct-prompt"><i class="fa fa-check-circle"></i>申請成功</span>
							</c:if>
							<c:if test="${dataList.msgName != null}">
								<span class="application-error-prompt"><i class="fa fa-times-circle"></i>申請失敗<span class="application-error-code">${dataList.msgName}(代碼：${dataList.msgCode})</span></span>
							</c:if>
						</div>
					</c:forEach>
				</td>
			</tr>
			<tr>
				<!-- 申請時間 -->
				<td style="text-align:center">網路交易指定新台幣帳戶</td>
				<td>${SVACN }</td>
			</tr>
		</table>
<!-- 		<p> -->
<!-- 黃金存摺帳號 -->
<%-- 		<spring:message code= "LB.D1090" />： --%>
<%-- 		<c:if test="${ACN.length() > 11}"> --%>
<%-- 			<spring:message code="LB.All"/> --%>
<%-- 		</c:if> --%>
<%-- 		<c:if test="${ACN.length() <= 11}"> --%>
<%-- 			${ ACN } --%>
<%-- 		</c:if> --%>

<%-- 		<table class="print" data-toggle-column="first"> --%>
<%-- 		<thead> --%>
<%-- 			<tr> --%>
<!-- 黃金存摺帳號 -->
<%-- 				<th><spring:message code= "LB.D1090" /></th> --%>
<!-- 指定網路銀行交易 申購/回售新臺幣帳戶 -->
<%-- 				<th><spring:message code= "LB.D1091" /></th> --%>
<!-- 申請狀態 -->
<%-- 				<th><spring:message code= "LB.D1098" /></th>						 --%>
<%-- 			</tr> --%>
<%-- 			</thead> --%>
<%-- 			<tbody> --%>
<%-- 				<c:forEach var="dataList" items="${print_datalistmap_data}"> --%>
<%-- 					<tr> --%>
<%-- 		                <td>${dataList.GDACN }</td> --%>
<%-- 		                <td>${dataList.SVACN }</td> --%>
<%-- 		                <td> --%>
<%-- 		                	<c:if test="${dataList.msgName == null}"> --%>
<!-- 成功 -->
<%-- 								<spring:message code= "LB.D1099" /> --%>
<%-- 							</c:if> --%>
<%-- 							<c:if test="${dataList.msgName != null}"> --%>
<!-- 失敗 -->
<%-- 								<spring:message code= "LB.W0056" /><br><a herf="#" title='${dataList.msgName}'>${dataList.msgCode}</a> --%>
<%-- 							</c:if> --%>
<%-- 						</td>				                --%>
<%-- 					</tr> --%>
<%-- 				</c:forEach> --%>
<%-- 			</tbody> --%>
<%-- 		</table> --%>
		<br /><br />
		<div class="text-left">
			<!-- 		說明： -->
			<spring:message code="LB.Description_of_page" />
			<ol class="list-decimal text-left">
			<!-- 晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。 -->
		        <li><spring:message code="LB.Gold_Trading_Apply_P1_D1" /></li>
		        <!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
			 	<li><spring:message code="LB.Gold_Trading_Apply_P1_D2"/></li>
			</ol>
		</div>
	</div>
</body>

</html>