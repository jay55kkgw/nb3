<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<style>
    
		@media screen and (max-width:767px) {
			.ttb-button {
					width: 32%;
					left: 0px;
			}
			#CMBACK.ttb-button{
					border-color: transparent;
					box-shadow: none;
					color: #333;
					background-color: #fff;
			}


	}
    </style>
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {

	    	//列印
        	$("#PRINT").click(function(){
				var params = {
					"jspTemplateName":"apply_digital_account_result_print",
					"jspTitle":'<spring:message code= "LB.X0216" />',
					"BRHNAME":"${result_data.data.BRHNAME}",
					"BRHTEL":"${result_data.data.BRHTEL}",
					"chkfile":"${result_data.data.chkfile}"
				};
				openWindowWithPost("${__ctx}/ONLINE/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
			$("#GOBAKE").click(function(e){
	        	var action = '${__ctx}/login';
	        	$("#formId").attr("action", action);
	        	$("#formId").submit();
			});
	    	
		}
		
		function openMenu() {
			var main = document.getElementById("main");
			window.open('${__ctx}/public/OpenAccount1.pdf');		   
		}
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上開立數位存款帳戶     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1361" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
<!-- 					線上開立數位存款帳戶 -->
					<spring:message code="LB.D1361"/>
				</h2>
                <div id="step-bar">
                    <ul>
                        <li class="finished">注意事項與權益</li>
                        <li class="finished">開戶認證</li>
                        <li class="finished">開戶資料</li>
                        <li class="finished">確認資料</li>
                        <li class="active">完成申請</li>
                    </ul>
                </div>
				<form method="post" id="formId" name="formId" action="${__ctx}/ONLINE/APPLY/apply_deposit_account_p2">
					<input type="hidden" name="TYPE" value="5">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
		                        <div class="ttb-message">
		                            <p>完成申請</p>
		                        </div>
							
								<div class="text-left">
										<p>親愛的客戶您好:</p>
										<p>謝謝您完成本行『數位存款帳戶帳戶服務』申請作業，本行將儘速辦理您的開戶申請，並以e-mail方式通知您辦理結果。若有任何問題，您可撥電話至您所指定服務分行：
										<B>${result_data.data.BRHNAME}(聯絡電話：${result_data.data.BRHTEL})</B>
										詢問，我們將竭誠為您服務。祝您事業順心，萬事如意!</p>
										<p>提醒您：<c:if test="${result_data.data.chkfile == 'N'}"><font style="color: red">您的證件尚未上傳或有遺漏</font></c:if><br />本行將審核您所上傳的資料，如有模糊不清或資料遺漏，將有專人與您聯繫。如您未在申請日起20日內 (含)補齊，您的數位存款帳戶申請將會失效。</p>
								</div>
<!-- 								<div class="text-right"> -->
<%-- 									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="LB.W0605"/>&nbsp;&nbsp;&nbsp;<spring:message code="LB.X0193"/> --%>
<!-- 								</div> -->
							</div>
							
							<!-- 確定 -->
							<input type="button" id="PRINT" value="<spring:message code="LB.Print" />" class="ttb-button btn-flat-gray" />
							<input type="button" id="GOBAKE" value="回首頁" class="ttb-button btn-flat-orange"/>
							<input type="button" id="CLOSE" value="繼續其他申請" class="ttb-button btn-flat-orange"  onClick="window.close();"/>
<%-- 							<input type="button" id="CMPRINT" value="<spring:message code="LB.X0217"/>" class="ttb-button btn-flat-orange" style="width: 200px;" onclick="openMenu();" /> --%>
						</div>
					</div>
						
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>