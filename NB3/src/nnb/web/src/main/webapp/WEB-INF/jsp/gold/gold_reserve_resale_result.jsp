<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#printButton").click(function(){
		var params = {
			"jspTemplateName":"gold_reserve_resale_result_print",
			//預約黃金回售
			"jspTitle":"<spring:message code="LB.X0952"/>",
			"CMQTIME":"${bsData.CMQTIME}",
			"ACN":"${bsData.ACN}",
			"SVACN":"${bsData.SVACN}",
			"TRNGDFormat":"${bsData.TRNGDFormat}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
});
</script>
</head>
<body>
	<!--header-->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 黃金回售     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1512" /></li>
		</ol>
	</nav>

	<!--左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> 
			<!--主頁內容-->
			<section id="main-content" class="container">
			<!-- 預約黃金回售 -->
				<h2><spring:message code="LB.X0952"/></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formID" method="post">
                	<input type="hidden" name="ADOPID" value="N09102"/>
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
	                        	<div class="ttb-message">
<!-- 預約成功 -->
	                        		<span><spring:message code= "LB.W0284" /></span>
	                        	</div>
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 資料時間 -->
	                                        <h4><spring:message code= "LB.D0339" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.CMQTIME}</span>
										</div>
									</span>
								</div>
								<!--黃金轉出帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
<!-- 黃金轉出帳號 -->
	                                        <h4><spring:message code= "LB.W1515" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.ACN}</span>
										</div>
									</span>
								</div>
								<!--台幣轉入帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
		                                <label>
<!-- 臺幣轉入帳號 -->
											<h4><spring:message code= "LB.W1518" /></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.SVACN}</span>
										</div>
									</span>
								</div>
								<!--賣出公克數-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 賣出公克數 -->
	                                        <h4><spring:message code= "LB.W1519" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.TRNGDFormat}</span>
<!-- 公克 -->
											<span class="ttb-unit"><spring:message code= "LB.W1435" /></span>
										</div>
									</span>
								</div>
		                    </div>
		                    <!-- 列印 -->
	                  	<input type="button" id="printButton" value="<spring:message code= "LB.Print" />" class="ttb-button btn-flat-orange"/>
		                </div>
					</div>
						<ol class="description-list list-decimal">
								<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
									<!-- 預約黃金回售將以 預約後的第一個營業日的第一次牌告買進價格交易。 -->
									<li><spring:message code= "LB.Gold_Reserve_Resale_P4_D1-1" />&nbsp;<font color="red"><spring:message code= "LB.Gold_Reserve_Resale_P4_D1-2" /></font></li>
									<!-- 電子簽章：為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章（載具i－key）拔除並登出系統。 -->
									<li><spring:message code= "LB.Gold_Reserve_Resale_P4_D2" /></li>
									<!-- 預約成功不代表交易已完成，請至黃金存摺-查詢服務-黃金存摺明細查詢，以確認交易結果。 -->
									<li><spring:message code= "LB.Gold_Reserve_Resale_P4_D3" /></li>
								</ol>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>