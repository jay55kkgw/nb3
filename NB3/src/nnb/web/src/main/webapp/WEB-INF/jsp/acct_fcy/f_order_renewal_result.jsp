<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="bs" value="${f_order_renewal_result.data}"></c:set>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript">
		$(document).ready(function () {
			//列印
			$("#printbtn").click(function () {
				var params = {
						jspTemplateName:	"f_order_renewal_result_print",
						jspTitle:			'<spring:message code="LB.Renewal_At_Maturity_Of_FX_Time_Deposit"/>',
						MsgName:			'${bs.MsgName}',
						CMQTIME:			'${bs.CMQTIME}',
						FYACN:				'${bs.FYACN}',
						FDPNUM:				'${bs.FDPNUM}',
						CRYNAME:			'${bs.CRYNAME}',
						AMTTSF:				'${bs.AMTTSF}',
						DEPTYPE:			'<spring:message code="LB.NTD_deposit_type_1"/>',
						TERM:				'${bs.SHOW_TERM}',
						SHOWDPISDT:			'${bs.SHOWDPISDT}',
						SHOWDUEDAT:			'${bs.SHOWDUEDAT}',
						INTMTH:				'${bs.SHOW_INTMTH}',
						ITR:				'${bs.ITR}',
						AMTFDP:				'${bs.AMTFDP}',
						INT:				'${bs.INT}',
						FYTAX:				'${bs.FYTAX}',
						NHITAX:				'${bs.NHITAX}',
						FYTSFAN:			'${bs.FYTSFAN}'
					};
				
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
		});
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %>  
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存單到期續存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Renewal_At_Maturity_Of_FX_Time_Deposit" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- 快速選單及主頁內容 -->
	<main class="col-12">
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<!-- 外匯定存單到期續存 -->
			<h2>
				<spring:message code="LB.Renewal_At_Maturity_Of_FX_Time_Deposit" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form name="main" id="main" method="post" action="" onSubmit="return processQuery()">
	
				<!-- 交易步驟 -->
				<div id="step-bar">
					<ul>
						<!-- 輸入資料 -->
						<li class="finished">
							<spring:message code="LB.Enter_data" />
						</li>
						<!-- 確認資料 -->
						<li class="finished">
							<spring:message code="LB.Confirm_data" />
						</li>
						<!-- 交易完成 -->
						<li class="active">
							<spring:message code="LB.Transaction_complete" />
						</li>
					</ul>
				</div>
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<!-- 續存成功 -->
								<span>${bs.MsgName}</span>
							</div>
							<!-- 交易時間 -->
	
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Trading_time" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${bs.CMQTIME}
										</span>
									</div>
								</span>
							</div>
							<!-- 存單帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Account_no" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${bs.FYACN}
										</span>
									</div>
								</span>
							</div>
							<!-- 存單號碼 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Certificate_no" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${bs.FDPNUM}
										</span>
									</div>
								</span>
							</div>
							<!-- 存單金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Certificate_amount" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${bs.CRYNAME}
											<fmt:formatNumber type="number" minFractionDigits="2" value="${bs.AMTTSF }" />
											<!--元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!-- 存單種類 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Deposit_certificate_type" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<spring:message code="LB.NTD_deposit_type_1" />
										</span>
									</div>
								</span>
							</div>
							<!-- 存單期別 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Fixed_duration" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${bs.SHOW_TERM}
										</span>
									</div>
								</span>
							</div>
							<!-- 起存日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Start_date" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${bs.SHOWDPISDT}
										</span>
									</div>
								</span>
							</div>
							<!-- 到期日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Maturity_date" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${bs.SHOWDUEDAT}
										</span>
									</div>
								</span>
							</div>
							<!-- 計息方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Interest_calculation" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${bs.SHOW_INTMTH}
										</span>
									</div>
								</span>
							</div>
							<!-- 利率 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Interest_rate" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${bs.ITR}%
										</span>
									</div>
								</span>
							</div>
							<!-- 利息轉入帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
	
											<spring:message code="LB.Interest_transfer_to_account" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${bs.FYTSFAN}
										</span>
									</div>
								</span>
							</div>
							<!-- 原存單金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Original_amount" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<fmt:formatNumber type="number" minFractionDigits="2" value="${bs.AMTFDP }" />
											<!--元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!-- 原存單利息 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Original_interest" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<fmt:formatNumber type="number" minFractionDigits="2" value="${bs.INT }" />
											<!--元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!-- 代扣利息所得稅 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Withholding_interest_income_tax" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<fmt:formatNumber type="number" minFractionDigits="2" value="${bs.FYTAX }" />
											<!--元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!--健保費 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.NHI_premium" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<fmt:formatNumber type="number" minFractionDigits="2" value="${bs.NHITAX }" />
											<!--元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
						</div>
						<!-- button -->
							<!-- 列印  -->
							<spring:message code="LB.Print" var="printbtn"></spring:message>
							<input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
						<!-- button -->
					</div>
				</div>
				<div class="text-left">
					<!-- 說明  -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>					
						<li>
							<spring:message code="LB.F_order_renewal_P4_D1" />
						</li>
					</ol>
				</div>
			</form>
		</section>
		<!-- main-content END -->
	</main>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>