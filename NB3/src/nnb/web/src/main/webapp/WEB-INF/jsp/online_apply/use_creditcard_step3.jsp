<% response.setHeader("X-Frame-Options", "DENY"); %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$('#but_Agree').prop('checked', false);
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
            $("#formId").attr("action","${__ctx}/ONLINE/APPLY/use_creditcard_result");
	 	  	$("#formId").submit();
		});
		//上一頁按鈕
		$("#CMBACK").click(function() {
			$("#BACKTYPE").val("Y");
			initBlockUI();
			$("#formId").attr("action","${__ctx}/ONLINE/APPLY/use_creditcard_step2");
			$("#formId").submit();
		});
		//列印
		$("#CMPRINT").click(function(){
			var params = {
					"jspTemplateName":"use_creditcard_3_print",
					//臺灣企銀信用卡申請網路銀行
					"jspTitle":"<spring:message code= "LB.X1085" />"
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});	
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Register" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
			<!-- 信用卡申請網路銀行 -->
				<h2><spring:message code= "LB.X1086" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12">
                        <div class="ttb-message">
                        <!-- 臺灣企銀電子帳單約定條款 -->
                        <!-- 下列為申請電子帳單應遵守之約定條款內容，您如接受本約定條款則請按 我同意約定條款 -->
                        <!-- 鍵，以完成申請作業，您如不同意條款內容款則請按 取消 鍵 本行將不受理您的申請。 -->
                            <p><spring:message code= "LB.X1214" /></p>
                            <span><font color="blue" size="3"><b><spring:message code= "LB.X1215" /><span style="border: 1px solid #000000;background-color: #FF0000;color: #FFFFFF;"><spring:message code= "LB.W1554" /></span><spring:message code= "LB.X1209" /><span style="border: 1px solid #000000;background-color: #FF0000;color: #FFFFFF;"><spring:message code= "LB.Cancel" /></span><spring:message code= "LB.X1210" />
							</b></font></span>
                        </div>
                        <ul class="ttb-result-list">
                            <li class="full-list"><span><b>第一條　</b></span><span class="input-subtitle subtitle-color">適用範圍</span><br>本約定條款係電子帳單服務之一般性共通約定，除個別契約另有規定外，悉依本約定辦理。</li>
                            <li class="full-list"><span><b>第二條　</b></span><span class="input-subtitle subtitle-color">服務內容</span><br>本服務包含「電子交易對帳單」、「基金電子對帳單」及「信用卡電子帳單」三項服務。本人（以下簡稱立約人）申請臺灣中小企業銀行（以下簡稱貴行）以網際網路傳輸方式，傳送下列資料至立約人設定之電子郵箱位址。
							
									<ul>
										<li>
											一、電子交易對帳單：貴行每月五日（含）以前將立約人上月之電子交易對帳單（即網路銀行、行動銀行、電話銀行、加值型電子銀行及全國性繳費活期性帳戶ID＋ACCOUNT非約定繳費等電子銀行之各轉帳成功交易明細），傳送至立約人設定之電子郵箱（該月無交易時不寄），<font color="#FF0000">不另寄送書面對帳單</font>
										</li>
									
										<li>
											二、基金電子對帳單：貴行按月將立約人之基金電子對帳單，傳送至立約人設定之電子郵箱，<font color="#FF0000">不另寄送書面對帳單</font>。
										</li>
										
										<li>
											三、信用卡電子帳單：貴行按月於立約人之貴行所有信用卡結帳日之後，傳送至立約人各信用卡之帳單內容至立約人設定之電子郵箱，<font color="#FF0000">並仍會寄送書面帳單</font>。
										</li>
										
										
									</ul>
							</li>
							
							
                            <li class="full-list"><span><b>第三條　</b></span><span class="input-subtitle subtitle-color">服務啟用時間</span><br>
							
									<ul>
										<li>
											一、電子交易對帳單、基金電子對帳單：立約人申請完成後，將自最近一期帳單開始生效，惟當期對帳單已進行製作處理中，或已完成製作時，此申請將順延至下一期對帳單方能生效。
										</li>
									
										<li>
											二、信用卡電子帳單：若立約人係於當月結帳日前(不含結帳日當日)申請成功者，則本服務自立約人申請成功日當月之月結單結帳日起提供本服務；若立約人係於當月結帳日後(含結帳日當日)申請成功者，則本服務自客戶申請成功日之次一月結單結帳日起提供本服務。立約人選擇信用卡電子帳單服務後，應每月查閱電子帳單內容。若立約人於當期繳款截止日起七日前仍未收到電子帳單，其應立即電洽貴行客服人員<font color="#FF0000"><b>0800-01-7171處理</b></font>。        
										</li>
									
									</ul>
							</li>
							
						
                            <li class="full-list"><span><b>第四條　</b></span><span class="input-subtitle subtitle-color">電子郵箱位址變更設定</span><br>倘立約人需變更其於申請本服務時所設定之電子郵箱位址，其應依據貴行所指示之程序及方式重新設定電子郵箱新位址。<br>倘立約人之電子郵箱位址有變更而未依前項規定辦理變更者，貴行仍以立約人最後依前項程序及方式設定之電子郵箱位址為立約人應受送達之位址。</li>
							
                            <li class="full-list"><span><b>第五條　</b></span><span class="input-subtitle subtitle-color">電子帳單無法送達之處理</span><br>如因立約人之電子郵箱或線路傳輸等因素致無法接收電子帳單，則立約人同意辦理如下：
							
							<ul>
										<li>
											一、電子交易對帳單：立約人同意於該月底以前，自行於貴行網路銀行申請補發，貴行隨即再次傳送上月份電子交易對帳單；倘仍無法接收，同意以網路銀行或臨櫃查詢或補登存摺方式取得帳戶明細資料，貴行不另寄送書面對帳單。
										</li>
									
										<li>
											二、基金電子對帳單：立約人同意以網路銀行查詢或電洽貴行信託部<font color="#FF0000"><b>(02)2559-7171分機5462補發</b></font>。        
										</li>
										<li>
											三、信用卡電子帳單：立約人同意自行於貴行網路銀行申請補發，貴行隨即傳送信用卡電子帳單；或電洽貴行客服人員<font color="#FF0000"><b>0800-01-7171處理</b></font>。
										</li>
									</ul>
									<ul>
										<li>
											立約人因電子郵箱或線路傳輸等因素致無法接收電子帳單，為保障己身權益，同意自行於貴行網站申請取消電子帳單，由貴行按月寄送書面帳單。
										</li>
										<li>
											倘立約人未依上開方式辦理，如有任何損失，應自負其責任。
										</li>
									</ul>
							
							</li>
                            <li class="full-list"><span><b>第六條　</b></span><span class="input-subtitle subtitle-color">電子帳單之效力</span><br>電子帳單之效力與書面帳單相同。因本服務所生之任何糾紛，於審判、仲裁、調解或其他法定爭議處理程序中，立約人均不得主張電子帳單不具書面要件而無效，或主張貴行未履行寄發帳單之義務。<br>貴行依立約人指定之電子郵箱位址傳送電子帳單時，以電子帳單進入電子郵箱所在系統時之收文時間視為送達，但因立約人本身之原因而造成傳送失敗者（包括但不限於立約人輸入錯誤之電子郵箱、立約人變更電子郵箱位址而未辦理更新、立約人取消電子郵箱位址、立約人端網路設備故障或運作不當等），則以貴行對外發送之時間視為送達。</li>
							
							<li class="full-list"><span><b>第七條　</b></span><span class="input-subtitle subtitle-color">電子帳單錯誤之處理</span><br>立約人使用本服務時，如其電子帳單因不可歸責於貴行之事由而發生錯誤時，貴行不負更正及賠償之責任。</li>
							
							<li class="full-list"><span><b>第八條　</b></span><span class="input-subtitle subtitle-color">修訂</span><br>除另有約定外，貴行得隨時修訂本約定條款並以電子郵件傳送方式通知立約人，且於貴行網站公告。倘立約人不同意貴行新修訂之條款，得隨時依第九條規定取消電子帳單服務，惟立約人於貴行修訂本約定條款後仍繼續行使用電子帳單服務，即視為接受本約定條款之修改。</li>
							
							<li class="full-list"><span><b>第九條　</b></span><span class="input-subtitle subtitle-color">終止</span><br>立約人得隨時以貴行網路銀行取消電子帳單服務。</li>
							
                        </ul>
                      <!-- 取消 -->
						<input type="button" class="ttb-button btn-flat-gray" id="CMBACK" name="CMBACK" value="<spring:message code="LB.Cancel" />" />
						<!-- 列印 -->
						<input type="button" class="ttb-button btn-flat-gray" name="CMPRINT" id="CMPRINT" value="<spring:message code="LB.Print" />" />
                        <!-- 我同意約定條款 -->
                        <input type="button" class="ttb-button btn-flat-orange" value="<spring:message code= "LB.W1554" />" id="CMSUBMIT" name="CMSUBMIT" />
                		<input type="hidden" name="ADOPID" value="NA40">
						<input type="hidden" id="_CUSIDN" name="_CUSIDN" value="${use_creditcard_step3.data._CUSIDN}">
						<input type="hidden" id="CARDNUM" name="CARDNUM" value="${use_creditcard_step3.data.CARDNUM}">
						<input type="hidden" id="EXPDTA" name="EXPDTA" value="${use_creditcard_step3.data.EXPDTA}">
						<input type="hidden" id="EXPDTA" name="CHECKNO" value="${use_creditcard_step3.data.CHECKNO}">
						<input type="hidden" id="BIRTHDAY" name="BIRTHDAY" value="${use_creditcard_step3.data.BIRTHDAY}">
						<input type="hidden" id="MOBILE" name="MOBILE" value="${use_creditcard_step3.data.MOBILE}">
						<input type="hidden" id="PHONE_H" name="PHONE_H" value="${use_creditcard_step3.data.PHONE_H}">
						<input type="hidden" id="USERIP" name="USERIP" value="${use_creditcard_step3.data.USERIP}">
						<input type="hidden" id="MAIL" name="MAIL" value="${use_creditcard_step3.data.MAIL}">
						<input type="hidden" id="NOTIFY_CCARDPAY" name="NOTIFY_CCARDPAY" value="${use_creditcard_step3.data.NOTIFY_CCARDPAY}">
						<input type="hidden" id="NOTIFY_ACTIVE" name="NOTIFY_ACTIVE" value="${use_creditcard_step3.data.NOTIFY_ACTIVE}">
						<input type="hidden" id="NOTIFY_CCARDBILL" name="NOTIFY_CCARDBILL" value="${use_creditcard_step3.data.NOTIFY_CCARDBILL}">
						<input type="hidden" id="USERNAME" name="USERNAME" value="${use_creditcard_step3.data.USERNAME}">
						<input type="hidden" id="LOGINPIN" name="LOGINPIN" value="${use_creditcard_step3.data.LOGINPIN}">
						<input type="hidden" id="TRANSPIN" name="TRANSPIN" value="${use_creditcard_step3.data.TRANSPIN}">
						<input type="hidden" id="HLOGINPIN" name="HLOGINPIN" value="${use_creditcard_step3.data.HLOGINPIN}">
						<input type="hidden" id="HTRANSPIN" name="HTRANSPIN" value="${use_creditcard_step3.data.HTRANSPIN}">
						<input type="hidden" id="CCBIRTHDATEYY" name="CCBIRTHDATEYY" value="${use_creditcard_step3.data.CCBIRTHDATEYY}">
					  	<input type="hidden" id="CCBIRTHDATEMM" name="CCBIRTHDATEMM" value="${use_creditcard_step3.data.CCBIRTHDATEMM}">
					  	<input type="hidden" id="CCBIRTHDATEDD" name="CCBIRTHDATEDD" value="${use_creditcard_step3.data.CCBIRTHDATEDD}">
					  	<input type="hidden" id="BACKTYPE" name="BACKTYPE" value="">
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>