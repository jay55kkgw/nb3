<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		setTimeout("initDataTable()",100);
	});
	
	// 畫面初始化
	function init() {
		//initFootable();
		$('#but_Agree').prop('checked', false);
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
            $("#formId").submit();
		});
	}
	function buttonEvent(){
		if($('input[type=checkbox][name=MB_Agree]').is(':checked')){
			$('#CMSUBMIT').removeAttr("disabled");
			$('#CMSUBMIT').addClass('btn-flat-orange');
			$('#CMSUBMIT').removeClass('btn-flat-gray');
		}
		else{
			$('#CMSUBMIT').attr("disabled",true);
			$('#CMSUBMIT').removeClass('btn-flat-orange');
			$('#CMSUBMIT').addClass('btn-flat-gray');
		}
	}
	
</script>
</head>
	<body>
		<!-- header -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 行動銀行服務     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0318" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<!-- 快速選單內容 -->
			<%@ include file="../index/menu.jsp"%>
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.D0318"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="${__ctx}/ONLINE/SERVING/enable_mobile_confirm">
					<input id="Ori_Stat" type="hidden" name="OriStatus" value="${enable_mobile.currentStatus }">
				
					<div class="main-content-block row">
						<div class="col-12">
							<!-- 線上啟用行動銀行服務 -->
							<ul class="ttb-result-list"></ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
									<tr>
										<!-- 目前設定狀態 -->
										<th data-title="<spring:message code="LB.D0319"/>"><spring:message code="LB.D0319"/></th>
										<!-- 啟用/停用日期及時間 -->
										<th data-title="<spring:message code="LB.D0320"/>"><spring:message code="LB.D0320"/></th>
										<!-- 變更設定 -->
										<th data-title="<spring:message code="LB.D0321"/>"><spring:message code="LB.D0321"/></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<c:choose>
											<c:when test="${enable_mobile.currentStatus eq 'A' }">
												<!-- 已啟用 -->
												<td class="text-center"><font style="color:green; font-weight:bold;"><spring:message code="LB.D0329"/></font></td>
											</c:when>
											<c:when test="${enable_mobile.currentStatus eq 'D' }">
												<!-- 已停用 -->
												<td class="text-center"><font style="color:red; font-weight:bold;"><spring:message code="LB.D0328"/></font></td>
											</c:when>
											<c:otherwise>
												<!-- 未啟用 -->
												<td class="text-center"><font style="color:gray; font-weight:bold;"><spring:message code="LB.D0322"/></font></td>
											</c:otherwise>
										</c:choose>
										<!-- 行動銀行上次啟用/關閉日期時間 -->
										<td class="text-center">
											<c:if test="${transfer == 'en'}">
												${enable_mobile.modifyMonth }/${enable_mobile.modifyDay }/${enable_mobile.modifyYearEn }
												--
												${enable_mobile.modifyHH }:${enable_mobile.modifyMM }:${enable_mobile.modifySS }
											</c:if>
											<c:if test="${transfer != 'en'}">
												<spring:message code="LB.D0583"/>
												${enable_mobile.modifyYear }
												<spring:message code="LB.Year"/>
												${enable_mobile.modifyMonth }
												<spring:message code="LB.Month"/>
												${enable_mobile.modifyDay }
												<spring:message code="LB.Day"/>
												--
												${enable_mobile.modifyHH }
												<spring:message code= "LB.X1723" /><!-- 時 -->
												${enable_mobile.modifyMM }
												<spring:message code="LB.Minute"/>
												${enable_mobile.modifySS }
												<spring:message code="LB.Second"/>
											</c:if>
										</td>
										<td class="text-center">
											<label class="radio-block"><spring:message code="LB.D0324"/>
												<input type="radio" id="MB_Switch_On"  name="MB_Switch" value="A" ${enable_mobile.enableStatus }>
												<span class="ttb-radio"></span>
											</label>
      										<label class="radio-block"><spring:message code="LB.D0325"/>
      											<input type="radio" id="MB_Switch_Off" name="MB_Switch" value="D" ${enable_mobile.disableStatus }>
      											<span class="ttb-radio"></span>
      										</label>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
						<ol class="description-list list-decimal">
							<p><spring:message code="LB.D1101"/>:</p>
							<b><spring:message code="LB.enable_mobile_P2_D1"/></b>
							<li><spring:message code="LB.enable_mobile_P2_D2"/></li>
				   			<li><spring:message code="LB.enable_mobile_P2_D3"/></li>
				   			<li><spring:message code="LB.enable_mobile_P2_D4"/></li>
				   			<li><spring:message code="LB.enable_mobile_P2_D5"/></li>
				   			<li><spring:message code="LB.enable_mobile_P1_D6"/></li>
				   			<li><spring:message code="LB.enable_mobile_P1_D7"/></li>
				   			<li><spring:message code="LB.enable_mobile_P1_D8"/></li>
						</ol>
						<ol class="description-list list-decimal">
							<b><spring:message code="LB.enable_mobile_P1_D9"/></b>
				   			<li><spring:message code="LB.enable_mobile_P1_D10"/></li>
				   			<li><spring:message code="LB.enable_mobile_P1_D11"/></li>
						</ol>
						<div class="text-center">
							<label class="check-block">
					   			<input onclick="buttonEvent()" id="but_Agree" type="checkbox" name="MB_Agree" value="1">
					   			<span class="ttb-check"></span>
					   			<b><spring:message code="LB.enable_mobile_P2_D6"/></b>
					   		</label>
					   	</div>
			   			<div class="text-center">
			   				<!-- 確認-->
	                        <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-gray" disabled value="<spring:message code="LB.Confirm_1"/>"/>
	                    </div>
				</form>
				</section>
			</main>
			<!-- main-content END -->
			<!-- content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>