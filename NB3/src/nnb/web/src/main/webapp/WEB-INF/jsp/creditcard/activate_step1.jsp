<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
	$(document).ready(function(){
		$("#printbtn").click(function(){
			var i18n = new Object();
			i18n['jspTitle']='<spring:message code="LB.Card_Activation" />'
			var params = {
					"jspTemplateName":"activate_step1_print",
					"jspTitle":i18n['jspTitle'],
					"STATUS":'${activate_result.data.STATUS}',
					"CMQTIME":'${activate_result.data.CMQTIME}',
					"CARDNUM":"${activate_result.data.CARDNUM}",
					"EXP_DATE":"${activate_result.data.EXP_DATE}"
			}
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

		});
		initFootable();
	});
</script>
</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 信用卡開卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Card_Activation" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	

	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
		
			<h2><spring:message code="LB.Card_Activation" /></h2><!-- 信用卡開卡 -->
			
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/CREDIT/INQUIRY/card_unbilled_result">
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
						<div class="ttb-message">
								<c:if test="${activate_result.data.STATUS !='0'}">
									<p style="color:red">${activate_result.data.STATUS}</p>
								</c:if>
								<c:if test="${activate_result.data.STATUS =='0'}">
									<p style="color:red"><spring:message code="LB.Active_successful" /></p>
								</c:if>
						</div>
						<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.Trading_time" />：
											<!-- 交易時間 -->
										</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<p>
				      						${activate_result.data.CMQTIME}
										</p>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.Credit_card_number" />：
											<!-- 信用卡卡號 -->
										</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<p>
				      						${activate_result.data.CARDNUM}
										</p>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.Card_validity" />：
											<!-- 卡片效期 -->
										</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<p>
				      						${activate_result.data.EXP_DATE}
										</p>
									</div>
								</span>
							</div>
						</div>
						<input type="button" id="printbtn" class="ttb-button btn-flat-orange" name="CMSUBMIT" value="<spring:message code="LB.Print" />" ><!-- 列印 -->
					</div>
				</div>
			</form>
			<c:if test="${activate_result.data.STATUS !='0'}">
			<ol class="description-list list-decimal">
				<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
				<li><spring:message code="LB.Activate_P3_D1" /></li>
			</ol>
			</c:if>
		</section>
	</main><!-- main-content END --> 
	</div><!-- content row END -->

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>