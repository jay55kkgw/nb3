<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.twzipcode-1.6.1.js"></script>
<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<!-- DIALOG會用到 -->
<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">
<style type="text/css">
#countryTable{
	border:solid 2px;
}
#countryTable tr{
	border:solid 2px;
}
#countryTable td{
	border:solid 2px;
}
</style>
<script type="text/JavaScript">
    var isTimeout = "${not empty sessionScope.timeout}";
    var countdownObjheader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
    var countdownSecHeader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
	$(document).ready(function() {
		//HTML載入完成後開始遮罩
		setTimeout("initBlockUI()",10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		//解遮罩
		setTimeout("unBlockUI(initBlockId)",500);
        //timeout
        timeLogout();
	});
	
	function init(){
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});

		countrydata();
		GO();
		goBack();
		putData();
	}
	
	function putData(){
		//職業類別
		$("#CPRIMJOBTYPE").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X2437' />,CPRIMJOBTYPE,#]]]");
		//職位名稱
		$("#CPRIMJOBTITLE").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X2438' />,CPRIMJOBTITLE,#]]]");
		//聯名/認同卡之個資使用
		$("#CNOTE1_1").addClass("validate[funcCall[validate_CheckBox['聯名/認同卡之個資使用',CNOTE1_1,#]]]");
		//是否更換過中文姓名
		$("#CHENAMECHK").addClass("validate[funcCallRequired[validate_Radio[<spring:message code='LB.D0052' />,CHENAME]]]");
		var getback = '${result_data.data.back}';
		var oldcardowner = "<c:out value='${fn:escapeXml(sessionScope.oldcardownerchk)}' />" ;
		if(getback == 'Y'){
			//資料回填
			var oldcardownerB = '${result_data.data.oldcardowner}';
			$("#CPRIMCHname").text("<c:out value='${fn:escapeXml(result_data.data.CPRIMCHNAME)}' />");
			$("#CPRIMCHNAME").val('${result_data.data.CPRIMCHNAME}');
			$("#CPRIMENGNAME").val('${result_data.data.CPRIMENGNAME}');
			$("#IDnum").text('${result_data.data.CUSIDN}');
			$("#CPRIMID").val('${result_data.data.CUSIDN}');
			$("#CTRYDESC1").val('${result_data.data.CTRYDESC1}');
			$("#CPRIMCELLULANO1").val("${result_data.data.CPRIMCELLULANO1}");
			$("#CPRIMcellulanO1").text($("#CPRIMCELLULANO1").val().substring(0,4)+"***"+$("#CPRIMCELLULANO1").val().substring(7));
			
			$("#CPRIMJOBTYPE").val("${result_data.data.CPRIMJOBTYPE}");
			$("#CPRIMJOBTITLE").val("${result_data.data.CPRIMJOBTITLE}");
			
			var chename = '${result_data.data.CHENAME}';
			if(chename == '1'){
				$("#CHENAMEN").prop("checked",false);
				$("#CHENAMEY").prop("checked",true);
			}
			else{
				$("#CHENAMEY").prop("checked",false);
				$("#CHENAMEN").prop("checked",true);
			}
			
			if('1'=="${result_data.data.MCASH}"){
				$("#MCASH_1").prop("checked",true);
			}else{
				$("#MCASH_1").prop("checked",false);
			}
			
			if('1'=="${result_data.data.CNOTE4}"){
				$("#CNOTE4_1").prop("checked",false);
				$("#VARSTR2").val("1");
			}else{
				$("#CNOTE4_1").prop("checked",true);
				$("#VARSTR2").val("2");
			}
			
			if('1'=="${result_data.data.CNOTE1}"){
				$("#CNOTE1_1").prop("checked",true);
			}else{
				$("#CNOTE1_1").prop("checked",false);
			}
			
			if('1'=="${result_data.data.CNOTE3}"){
				$("#CNOTE3_1").prop("checked",true);
			}else{
				$("#CNOTE3_1").prop("checked",false);
			}
			
			$("#CUSIDN").val("${result_data.data.CUSIDN}");
			$("#CFU2").val("${result_data.data.CFU2}");
			$("#CN").val("${result_data.data.CN}");
			$("#CARDNAME").val("${result_data.data.CARDNAME}");
			$("#CARDMEMO").val("${result_data.data.CARDMEMO}");
			$("#FGTXWAY").val("${result_data.data.FGTXWAY}");
			$("#OLAGREEN1").val("${result_data.data.OLAGREEN1}");
			$("#OLAGREEN2").val("${result_data.data.OLAGREEN2}");
			$("#OLAGREEN3").val("${result_data.data.OLAGREEN3}");
			$("#OLAGREEN4").val("${result_data.data.OLAGREEN4}");
			$("#OLAGREEN5").val("${result_data.data.OLAGREEN5}");
			$("#oldcardowner").val("${result_data.data.oldcardowner}");
			$("#CPRIMBIRTHDAY").val("${result_data.data.CPRIMBIRTHDAY}");
			$("#CPRIMBIRTHDAYshow").val("${result_data.data.CPRIMBIRTHDAYshow}");
			$("#BIRTHY").val("${result_data.data.BIRTHY}");
			$("#BIRTHM").val("${result_data.data.BIRTHM}");
			$("#BIRTHD").val("${result_data.data.BIRTHD}");
			$("#CTTADR").val("${result_data.data.CTTADR}");
			<!-- Avoid Reflected XSS All Clients -->
			//$("#PMTADR").val("${result_data.data.PMTADR}");
			$("#PMTADR").val("<c:out value='${fn:escapeXml(result_data.data.PMTADR)}' />");
			$("#VARSTR2").val("${result_data.data.VARSTR2}");
			$("#QRCODE").val("${result_data.data.QRCODE}");
			var Lcard = "${result_data.data.QRCODE}";
			//獅友會公益卡
			if(Lcard == "L"){
				$("#partition").val("${result_data.data.partition}");
				$("#partition").change();
				$("#branch").val("${result_data.data.branch}");
				if('${result_data.data.CARDNAME}' == '85'){
					$("#memberId").val("${result_data.data.memberId}");
				}
			}
		}
		else{
            var oldcardowner = "<c:out value='${fn:escapeXml(sessionScope.oldcardownerchk)}' />" ;
			if(oldcardowner == "Y"){
                $("#CPRIMCHname").text("<c:out value='${fn:escapeXml(result_data.data.CUSTNM)}' />");
                $("#CPRIMCHNAME").val("<c:out value='${fn:escapeXml(result_data.data.CUSTNM)}' />");
                $("#CPRIMCELLULANO1").val("<c:out value='${fn:escapeXml(result_data.data.MOBILEPHONE)}' />");
                $("#CPRIMcellulanO1").text($("#CPRIMCELLULANO1").val().substring(0,4)+"***"+$("#CPRIMCELLULANO1").val().substring(7));
                $("#CTTADRtext").text("<c:out value='${fn:escapeXml(result_data.data.requestParam.strBILLADDR1)}' />");
			}
			$("#IDnum").text("<c:out value='${fn:escapeXml(result_data.data.requestParam.CUSIDN)}' />");
			$("#CPRIMID").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CUSIDN)}' />");
			$("#CUSIDN").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CUSIDN)}' />");
            $("#CFU2").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CFU2)}' />");
            $("#CN").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CN)}' />");
			$("#CARDNAME").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDNAME)}' />");
			$("#CARDMEMO").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDMEMO)}' />");
			$("#FGTXWAY").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.FGTXWAY)}' />");
			$("#OLAGREEN1").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.OLAGREEN1)}' />");
			$("#OLAGREEN2").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.OLAGREEN2)}' />");
			$("#OLAGREEN3").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.OLAGREEN3)}' />");
			$("#OLAGREEN4").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.OLAGREEN4)}' />");
			$("#OLAGREEN5").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.OLAGREEN5)}' />");
			$("#oldcardowner").val("<c:out value='${fn:escapeXml(sessionScope.oldcardownerchk)}' />");
			$("#CPRIMBIRTHDAY").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CPRIMBIRTHDAY)}' />");
			$("#BIRTHY").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.BIRTHY)}' />");
			$("#BIRTHM").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.BIRTHM)}' />");
			$("#BIRTHD").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.BIRTHD)}' />");
			$("#VARSTR2").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.VARSTR2)}' />");
		}
	}
	
	function goBack(){
		$("#CMBACK").click( function(e) {
			$("#formId").validationEngine('detach');
			console.log("submit~~");
			$("#formId").attr("action", "${__ctx}/CREDIT/APPLY/apply_creditcard_terms");
			$("#back").val('Y');

	     	initBlockUI();
	        $("#formId").submit();
		});
	}
	function GO(){
		$("#CMSUBMIT").click(function(e){
			var oldcardowner = "<c:out value='${fn:escapeXml(sessionScope.oldcardownerchk)}' />";
			//聯名/認同卡之個資使用
			if($("#CNOTE1_1").prop("checked") == true){
				$("#CNOTE1").attr("value","1");
			}
			else{
				$("#CNOTE1").attr("value","2");
			}
			//個人資料之行銷運用
			if($("#CNOTE3_1").prop("checked") == true){
				$("#CNOTE3").attr("value","1");
			}
			else{
				$("#CNOTE3").attr("value","2");
			}
			//悠遊卡自動
			if($("#CNOTE4_1").prop("checked") == true){
				$("#CNOTE4").attr("value","2");
				$("#VARSTR2").val("2");
			}
			else{
				$("#CNOTE4").attr("value","1");
			}
			//申請預借現金密碼函
			if($("#MCASH_1").prop("checked") == true){
				$("#MCASH").attr("value","1");
			}
			else{
				$("#MCASH").attr("value","2");
			}
			var CARDMEMO = "<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDMEMO)}' />";
	
			if(CARDMEMO == "1" || CARDMEMO == "2"){
				$("input[type=radio][name=CNOTE1][value=1]").prop("checked",true);
			}
	
			if(!ckValue()){//行員編號檢核
				return false;	
			}		  
			var job = $("#CPRIMJOBTYPE").val();
			var getback = '${result_data.data.back}';
			var oldcardowner = "${sessionScope.oldcardownerchk}";
			if(getback == 'Y'){
				oldcardowner = '${result_data.data.oldcardowner}';
			}
			
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
        		e.preventDefault();
        	}
			else{
// 				var oldcardowner = "${result_data.data.requestParam.oldcardowner}";
				var CPRIMCHNAME = $("#CPRIMCHNAME").val();
				var CPRIMENGNAME = $("#CPRIMENGNAME").val();
		
				var CARDMEMO = "<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDMEMO)}' />";
	       	
				if(CARDMEMO == "1" || CARDMEMO == "2"){
					$("input[type=radio][name=CNOTE1][value=1]").prop("checked",true);
				}
		
				var CTRYDESC = $("#CTRYDESC1").val();
				if(CTRYDESC.length == 0){
					errorBlock(null, null, ["<spring:message code='LB.Alert045' />"],
							'<spring:message code= "LB.Quit" />', null);
// 					alert("<spring:message code= "LB.Alert045" />");
					return false;
				}
				$("#CTRYDESC").val($("#CTRYDESC1").find(":selected").text());
				$("#CPRIMJOBTYPEval").val($("select[name='CPRIMJOBTYPE']").val());
				$("#CPRIMJOBTITLEval").val($("select[name='CPRIMJOBTITLE']").val());
				var CHENAMEval = $(".CHENAME").val();
				if(CHENAMEval == "1"){
					$("#CHENAME").val(1);
				}else{
					$("#CHENAME").val(2);
				}

				if(oldcardowner == "Y"){
					if($('input[name=CHENAME]:checked', '#formId').val()=="2"){
						$("#CHENAME").val(2);
					}else{
						$("#CHENAME").val(1);
					}
				}
				
				$("#formId").attr("action","${__ctx}/CREDIT/APPLY/apply_creditcard_p4_2");
				$("#formId").submit();
			}
		});
	}

//居住地址:縣市
function doquery(){
	if($("#CITY2").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getAreaListAjax";
		var rqData = {CITY:$("#CITY2").val()};
		fstop.getServerDataEx(URI,rqData,false,doqueryFinish);
	}
}
function doqueryFinish(data){
	if(data.result == true){
		var areaList = $.parseJSON(data.data);
		
		$("#ZONE2").html("");
// 		var ZONE2HTML = "<option value='#'>---<spring:message code= "LB.X0546" />---</option>";
		var ZONE2HTML = "<option value='#'><spring:message code='LB.D0060' /></option>";
		for(var x=0;x<areaList.length;x++){
			ZONE2HTML += "<option value='" + areaList[x].AREA + "'>" + areaList[x].AREA + "</option>";
		}
		$("#ZONE2").html(ZONE2HTML);
	}
	else{
		//alert("<spring:message code= "LB.X1069" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= "LB.X1069" />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
//居住地址:市／區鄉鎮
function doquery1(){
	if($("#ZONE2").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getZipCodeByCityAreaAjax";
		var rqData = {CITY:$("#CITY2").val(),AREA:$("#ZONE2").val()};
		fstop.getServerDataEx(URI,rqData,false,doquery1Finish);
	}
}
function doquery1Finish(data){
	if(data.result == true){
		var zipCode = data.data;
		
		$("#ZIP2").val(zipCode);
	}
	else{
		//alert("<spring:message code= "LB.X1070" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= "LB.X1070" />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
//戶籍地址:縣市
function doquery2(){
	if($("#CITY1").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getAreaListAjax";
		var rqData = {CITY:$("#CITY1").val()};
		fstop.getServerDataEx(URI,rqData,false,doquery2Finish);
	}
}
function doquery2Finish(data){
	if(data.result == true){
		var areaList = $.parseJSON(data.data);
		
		$("#ZONE1").html("");
// 		var ZONE1HTML = "<option value='#'>---<spring:message code= "LB.X0546" />---</option>";
		var ZONE1HTML = "<option value='#'><spring:message code='LB.D0060' /></option>";
		for(var x=0;x<areaList.length;x++){
			ZONE1HTML += "<option value='" + areaList[x].AREA + "'>" + areaList[x].AREA + "</option>";
		}
		$("#ZONE1").html(ZONE1HTML);
	}
	else{
		//alert("<spring:message code= "LB.X1069" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= "LB.X1069" />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
//戶籍地址:市／區鄉鎮
function doquery3(){
	if($("#ZONE1").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getZipCodeByCityAreaAjax";
		var rqData = {CITY:$("#CITY1").val(),AREA:$("#ZONE1").val()};
		fstop.getServerDataEx(URI,rqData,false,doquery3Finish);
	}
}
function doquery3Finish(data){
	if(data.result == true){
		var zipCode = data.data;
		
		$("#ZIP1").val(zipCode);
	}
	else{
		//alert("<spring:message code= "LB.X1070" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= "LB.X1070" />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
//公司地址:縣市
function doquery4(){
	if($("#CITY4").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getAreaListAjax";
		var rqData = {CITY:$("#CITY4").val()};
		fstop.getServerDataEx(URI,rqData,false,doquery4Finish);
	}
}
function doquery4Finish(data){
	if(data.result == true){
		var areaList = $.parseJSON(data.data);
		
		$("#ZONE4").html("");
// 		var ZONE4HTML = "<option value='#'>---<spring:message code= "LB.X0546" />---</option>";
		var ZONE4HTML = "<option value='#'><spring:message code='LB.D0060' /></option>";
		for(var x=0;x<areaList.length;x++){
			ZONE4HTML += "<option value='" + areaList[x].AREA + "'>" + areaList[x].AREA + "</option>";
		}
		$("#ZONE4").html(ZONE4HTML);
	}
	else{
		//alert("<spring:message code= "LB.X1069" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= "LB.X1069" />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
//公司地址:市／區鄉鎮
function doquery5(){
	if($("#ZONE4").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getZipCodeByCityAreaAjax";
		var rqData = {CITY:$("#CITY4").val(),AREA:$("#ZONE4").val()};
		fstop.getServerDataEx(URI,rqData,false,doquery5Finish);
	}
}
function doquery5Finish(data){
	if(data.result == true){
		var zipCode = data.data;
		
		$("#ZIP4").val(zipCode);
	}
	else{
		//alert("<spring:message code= "LB.X1070" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= "LB.X1070" />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
//國籍選擇
function fillData(id,desc){
	$("#CTRYDESC").val(desc);
	$("#countryDialog").dialog("close");
}
//英文姓名檢核
function checkinput(name){
	var CPRIMENGNAME = $("#"+name).val();
	
	var pattern = new RegExp("[0-9|\u4E00-\u9FA5]+");

	if(pattern.test(CPRIMENGNAME)){
		$("#"+name).val("");
	} 
}
//勾選同行動電話
function mobtohome(){
	if($("#samemobtel").prop("checked") == true){
		$("#samemobtel").attr("value","Y");
		//居住電話=行動電話
		$("#CPRIMHOMETELNO").val($("#CPRIMCELLULANO1").val());
	}
	else{
		$("#samemobtel").attr("value","N");
		$("#CPRIMHOMETELNO").val("");
	}
}
//勾選同居住電話
function samehome(){
	if($("#samehometel").prop("checked") == true){
		$("#samehometel").attr("value","Y");
		//戶籍電話=居住電話
		$("#CPRIMHOMETELNO2").val($("#CPRIMHOMETELNO").val());
	}
	else{
		$("#samehometel").attr("value","N");
		$("#CPRIMHOMETELNO2").val("");
	}
}

//國籍欄位帶初始值
function setCTRYDESC(){
	<!-- Avoid Reflected XSS All Clients -->
	//var uid = "${result_data.data.requestParam.CUSIDN}";
	var uid  = "<c:out value='${fn:escapeXml(result_data.data.requestParam.CUSIDN)}' />";
	var intVal = ['0','1','2','3','4','5','6','7','8','9'];
	for(var i=0;i<intVal.length;i++){
		//判斷身分證第二位是否為數字，不為數字為外國人
		if(uid.substring(1,2) == intVal[i]){
			$("#CTRYDESC").val("<spring:message code= "LB.X0828" />");				
			break;
		}
	}

	var CARDMEMO = "<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDMEMO)}' />";

	if(CARDMEMO == "1" || CARDMEMO == "2"){
		$("input[type=radio][name=CNOTE1][value=1]").prop("checked",true);
	}
}

function countrydata() {
	uri = '${__ctx}' + "/CREDIT/APPLY/country_ajax";
		
	console.log("creatOutAcn.uri: " + uri);
		
	data = fstop.getServerDataEx(uri, null, false,
			backdata);
}

function backdata(data) {
	  console.log("data: " + data);
	  if (data) {
		//ajax回傳資料型態: List<Map<String, String>>
		console.log("data.json: " + JSON.stringify(data));
		  
		//先清空原有之"OPTION"內容
		$("#CTRYDESC1").empty();
// 		$("#CTRYDESC").append( $("<option></option>").attr("value", "#").text("---<spring:message code="LB.Select_account"/>---"));
		
		//迴圈帶出"國籍"之下拉式選單	
		if('${transfer}' == 'en'){
			data.forEach(function(map) {
				console.log(map);
				$("#CTRYDESC1").append( $("<option></option>").attr("value", map.COUNTRYABB).text(map.COUNTRYEN));
			});
		}
		else if('${transfer}' == 'zh'){
			data.forEach(function(map) {
				console.log(map);
				$("#CTRYDESC1").append( $("<option></option>").attr("value", map.COUNTRYABB).text(map.COUNTRYCN));
			});
		}
		else{
			data.forEach(function(map) {
				console.log(map);
				$("#CTRYDESC1").append( $("<option></option>").attr("value", map.COUNTRYABB).text(map.COUNTRY));
			});
		}
		$("#CTRYDESC1").val('TW');
// 		data.forEach(function(map) {
// 			console.log(map);
// 			$("#CTRYDESC").append( $("<option></option>").attr("value", map.ACN).text(map.ACN));
// 		});
	 }  
 }
 
//行員編號邏輯性檢核
function ckValue(){
	var BANKERNO = $("#BANKERNO").val();
	
	var arr = ['7','9','7','3','1'];//加權
	
	var istrue = BANKERNO.substring(0,1) * arr[0] +
				 BANKERNO.substring(1,2) * arr[1] +
				 BANKERNO.substring(2,3) * arr[2] +
				 BANKERNO.substring(3,4) * arr[3] +
				 BANKERNO.substring(4,5) * arr[4];
	
	var finalValue = BANKERNO.substring(5,6);
	var modOk = istrue % 10;
	//輸入值最後一位是否等於餘數
	if(finalValue != modOk){
		errorBlock(null, null, ["<spring:message code='LB.Alert046' />"],
				'<spring:message code= "LB.Quit" />', null);
// 		alert("<spring:message code= "LB.Alert046" />");
		return false;
	}
	return true;
}
    function timeLogout() {
        // 刷新session
        var uri = '${__ctx}/login_refresh';
        console.log('refresh.uri: ' + uri);
        var result = fstop.getServerDataEx(uri, null, false, null);
        console.log('refresh.result: ' + JSON.stringify(result));
        // 初始化登出時間
        $("#countdownheader").html(parseInt(countdownSecHeader) + 1);
        $("#countdownMin").html("");
        $("#mobile-countdownMin").html("");
        $("#countdownSec").html("");
        $("#mobile-countdownSec").html("");
        // 倒數
        startIntervalHeader(1, refreshCountdownHeader, []);
    }

    function refreshCountdownHeader() {
        // timeout剩餘時間
        var nextSec = parseInt($("#countdownheader").html()) - 1;
        $("#countdownheader").html(nextSec);

        // 提示訊息--即將登出，是否繼續使用
        if (nextSec == 120) {
            initLogoutBlockUI();
        }
        // timeout
        if (nextSec == 0) {
            // logout
            fstop.logout('${__ctx}' + '/logout_aj', '${__ctx}' + '/timeout_logout');
        }
        if (nextSec >= 0) {
            // 倒數時間以分秒顯示
            var minutes = Math.floor(nextSec / 60);
            $("#countdownMin").html(('0' + minutes).slice(-2));
            $("#mobile-countdownMin").html(('0' + minutes).slice(-2));

            var seconds = nextSec - minutes * 60;
            $("#countdownSec").html(('0' + seconds).slice(-2));
            $("#mobile-countdownSec").html(('0' + seconds).slice(-2));
        }
    }

    function startIntervalHeader(interval, func, values) {
        clearInterval(countdownObjheader);
        countdownObjheader = setRepeater(func, values, interval);
    }

    function setRepeater(func, values, interval) {
        return setInterval(function () {
            func.apply(this, values);
        }, interval * 1000);
    }

    /**
     * 初始化logoutBlockUI
     */
    function initLogoutBlockUI() {
        logoutblockUI();
    }

    /**
     * 畫面BLOCK
     */
    function logoutblockUI(timeout) {
        $("#logout-block").show();

        // 遮罩後不給捲動
        document.body.style.overflow = "hidden";

        var defaultTimeout = 60000;
        if (timeout) {
            defaultTimeout = timeout;
        }
    }

    /**
     * 畫面UNBLOCK
     */
    function unLogoutBlockUI(timeoutID) {
        if (timeoutID) {
            clearTimeout(timeoutID);
        }
        $("#logout-block").hide();

        // 解遮罩後給捲動
        document.body.style.overflow = 'auto';
    }

    /**
     *繼續使用
     */
    function keepLogin() {
        unLogoutBlockUI(); // 解遮罩
        timeLogout(); // 刷新倒數計時
    }
</script>
</head>
<body>
<!-- header -->
<header>
	<%@ include file="../index/header_logout.jsp"%>
</header>	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 申請信用卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0666" /></li>
		</ol>
	</nav>

<!--左邊menu及登入資訊-->
<div class="content row">
<%-- 	<%@ include file="../index/menu.jsp"%> --%>
<!--快速選單及主頁內容-->
	<main class="col-12">
		<!--主頁內容-->
		<section id="main-content" class="container">
			<!--線上申請信用卡 -->
			<h2><spring:message code="LB.D0022" /></h2>
			<div id="step-bar">
				<ul>
					<li class="finished">信用卡選擇</li><!-- 信用卡 -->
					<li class="finished">身份驗證與權益</li><!-- 身份驗證與權益 -->
					<li class="active"><spring:message code="LB.X1967" /></li><!-- 申請資料 -->
					<li class=""><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
					<li class=""><spring:message code="LB.X1968" /></li><!-- 完成申請 -->
				</ul>
			</div>
            <!-- timeout -->
            <section id="id-and-fast">
						<span class="id-name">
							<spring:message code="LB.Welcome_1"/>&nbsp;
							<c:if test="${empty result_data.data.CPRIMCHNAME}">
                                <span id="username" name="username_show"><spring:message code="LB.X2190"/></span>
                            </c:if>
							<c:if test="${not empty result_data.data.CPRIMCHNAME}">
                                <span id="username" name="username_show">${result_data.data.CPRIMCHNAME}</span>
                            </c:if>
							&nbsp;<spring:message code="LB.Welcome_2"/>
						</span>
                <div id="id-block">
                    <div>
							<span class="id-time">
								<fmt:setLocale value="${pageContext.response.locale}"/>
								<c:if test="${!empty sessionScope.logindt && !empty sessionScope.logintm}">
                                    <fmt:parseDate var="parseDate"
                                                   value="${sessionScope.logindt} ${sessionScope.logintm}"
                                                   pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <fmt:formatDate value="${parseDate}" dateStyle="full"
                                                    type="both"/>&nbsp;<spring:message code="LB.X2250"/>
                                    <br/>
                                </c:if>

								<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full"/>
								&nbsp;<spring:message code="LB.Login_successful"/>
							</span>
                        <!-- 自動登出剩餘時間 -->
                        <span class="id-time"><spring:message code="LB.X1912"/>
								<span class="high-light ml-1">
									<!-- 分 -->
									<font id="countdownMin"></font>
									:
                                    <!-- 秒 -->
									<font id="countdownSec"></font>
								</span>
							</span>
                    </div>
                    <button type="button" class="btn-flat-orange" onclick="timeLogout()"><spring:message
                            code="LB.X1913"/></button>
                    <button type="button" class="btn-flat-darkgray"
                            onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')"><spring:message
                            code="LB.Logout"/></button>
                </div>
            </section>
			<form method="post" id="formId">
			<!-- Avoid Reflected XSS All Clients -->
				<input type="hidden" name="ADOPID" value="NA03"/>
				<input type="hidden" name="back" id="back" />
				<input type="hidden" name="CUSIDN" id="CUSIDN" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CUSIDN)}' />"/>
				<input type="hidden" name="CFU2" id="CFU2" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CFU2)}' />"/>
                <input type="hidden" name="CN" id="CN" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CN)}' />"/>
                <input type="hidden" name="CARDNAME" id="CARDNAME" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDNAME)}' />"/>
				<input type="hidden" name="CARDMEMO" id="CARDMEMO" value="${result_data.data.requestParam.CARDMEMO}"/>
				<input type="hidden" name="FGTXWAY" id="FGTXWAY" value="${result_data.data.requestParam.FGTXWAY}"/>
				<input type="hidden" name="OLAGREEN1" id="OLAGREEN1" value="${result_data.data.requestParam.OLAGREEN1}"/>
				<input type="hidden" name="OLAGREEN2" id="OLAGREEN2" value="${result_data.data.requestParam.OLAGREEN2}"/>
				<input type="hidden" name="OLAGREEN3" id="OLAGREEN3" value="${result_data.data.requestParam.OLAGREEN3}"/>
				<input type="hidden" name="OLAGREEN4" id="OLAGREEN4" value="${result_data.data.requestParam.OLAGREEN4}"/>
				<input type="hidden" name="OLAGREEN5" id="OLAGREEN5" value="${result_data.data.requestParam.OLAGREEN5}"/>
			  	<input type="hidden" id="RCVNO" name="RCVNO"/>
			  	<input type="hidden" id="CPRIMADDR" name="CPRIMADDR"/>
			  	<input type="hidden" id="CPRIMADDR2" name="CPRIMADDR2"/>
			  	<input type="hidden" id="CPRIMADDR3" name="CPRIMADDR3"/>
			  	<input type="hidden" name="STATUS" value="0"/>
			  	<input type="hidden" id="VARSTR2" name="VARSTR2" value="${result_data.data.requestParam.VARSTR2}"/>
<!-- 			  	<input type="hidden" id="BRANCHNAME" name="BRANCHNAME"/> -->
			  	<input type="hidden" name="oldcardowner" id="oldcardowner" value="${sessionScope.oldcardownerchk}"/>
			  	<input type="hidden" name="CPRIMBIRTHDAY" id="CPRIMBIRTHDAY" value="${result_data.data.requestParam.CPRIMBIRTHDAY}"/>
			  	<input type="hidden" name="CPRIMBIRTHDAYshow" id="CPRIMBIRTHDAYshow" value="${result_data.data.requestParam.CPRIMBIRTHDAYshow}"/>
			  	<input type="hidden" name="BIRTHY" id="BIRTHY" value="${result_data.data.requestParam.BIRTHY}"/>
			  	<input type="hidden" name="BIRTHM" id="BIRTHM" value="${result_data.data.requestParam.BIRTHM}"/>
			  	<input type="hidden" name="BIRTHD" id="BIRTHD" value="${result_data.data.requestParam.BIRTHD}"/>
			  	
			  	<input type="hidden" name="QRCODE" id="QRCODE" value="${result_data.data.requestParam.QRCODE}"/>
			  	<input type="hidden" name="branch" id="branch" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.branch)}' />"/>
				<input type="hidden" name="memberId" id="memberId" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.memberId)}' />"/>
				<input type="hidden" name="partition" id="partition" value="<c:out value='${fn:escapeXml(result_data.data.requestParam.partition)}' />"/>
				
				<input type="hidden" name="CHENAME" id="CHENAME" value=""/>
				<input type="hidden" name="CPRIMJOBTYPEval" id="CPRIMJOBTYPEval" value=""/>
				<input type="hidden" name="CPRIMJOBTITLEval" id="CPRIMJOBTITLEval" value=""/>
				

			  	
                <!-- 顯示區  -->
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<p><spring:message code="LB.X1967" /></p>
							</div>
							<div class="classification-block">
								<!-- 基本資料 -->
								<p><spring:message code='LB.D0109' /></p>
							</div>
							<!-- 中文姓名 -->
	                        <div id="nameN" class="ttb-input-item row">
	                        	<span class="input-title">
	                        		<label>
	                        			<h4><spring:message code='LB.D0049' /></h4>
	                        		</label>
	                        	</span>
	                        	<span class="input-block">
	                        		<div class="ttb-input">
	                        			<span><font id="CPRIMCHname"></font></span>
	                        			<input type="hidden" name="CPRIMCHNAME" id="CPRIMCHNAME" />
	                        		</div>
	                        	</span>
	                        </div>
                            <!-- 更換過中文姓名 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                    	<!-- 更換過中文姓名 -->
                                        <h4><spring:message code='LB.X1984' /><span class="high-light">*</span></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <label class="radio-block" for="CHENAMEN"><spring:message code='LB.D0034_3' /><!-- 否 -->
                                            <input type="radio" name="CHENAME" id="CHENAMEN" value="2" />
                                            <span class="ttb-radio"></span>
                                        </label>
                                        <label class="radio-block" for="CHENAMEY"><spring:message code='LB.D0034_2' /><!-- 是 -->
                                            <input type="radio" name="CHENAME" id="CHENAMEY" value="1" />
                                            <span class="ttb-radio"></span>
                                        </label>
                                        <input type="text" id="CHENAMECHK" class="text-input"
											style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
                                    </div>
                                </span>
                            </div>
                            <!--身分證字號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0581' /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                       <span><font id="IDnum"></font></span>
                                       <input type="hidden" name="CPRIMID" id="CPRIMID" />
                                    </div>
                                </span>
                            </div>
                            <!-- 行動電話 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0069' /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    		<span><font id="CPRIMcellulanO1"></font></span>
	                        				<input type="hidden" name="CPRIMCELLULANO1" id="CPRIMCELLULANO1" />
                                    </div>
                                </span>
                            </div>
                            <!--國籍-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.X0205' /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<select class="custom-select select-input" name="CTRYDESC1" id="CTRYDESC1">
                                    	</select>
                                    	<input type="hidden" id="CTRYDESC" name="CTRYDESC">
<%--                                     	<input id="CTRYDESC" name="CTRYDESC" type="text"  class="text-input" size="15" maxlength="15" value="（<spring:message code="LB.X1647" />）"/> --%>
<%-- 										<input type="button" value="<spring:message code="LB.Menu" />" id="COUNTRYMENU"  class="ttb-sm-btn btn-flat-orange" /> --%>
                                    </div>
                                </span>
                            </div>
                            <!-- 職業類別 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.X2437' /><span class="high-light">*</span></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <select class="custom-select select-input" name="CPRIMJOBTYPE" id="CPRIMJOBTYPE">
                                            <option value="#"><spring:message code='LB.X2447' /></option><!-- 請選擇職業類別 -->
                                            <option value="061100"><spring:message code='LB.X2439' /></option><!-- 軍官、軍人 -->
                                            <option value="061200"><spring:message code='LB.X2440' /></option><!-- 警官、員警 -->
                                            <option value="061300"><spring:message code='LB.D0873' /></option><!-- 其它公共行政業 -->
                                            <option value="061400"><spring:message code='LB.D0874' /></option><!-- 教育業 -->
                                            <option value="061410"><spring:message code='LB.D0081' /></option><!-- 學生 -->
                                            <option value="061500"><spring:message code='LB.D0876' /></option><!-- 工、商及服務業-->
                                            <option value="0615A0"><spring:message code='LB.D0877' /></option><!-- 農林漁牧業 -->
                                            <option value="0615B0"><spring:message code='LB.D0878' /></option><!-- 礦石及土石採取業 -->
                                            <option value="0615C0"><spring:message code='LB.D0879' /></option><!-- 製造業 -->
                                            <option value="0615D0"><spring:message code='LB.D0880' /></option><!-- 水電燃氣業 -->
                                            <option value="0615E0"><spring:message code='LB.D0881' /></option><!-- 營造業 -->
                                            <option value="0615F0"><spring:message code='LB.D0882' /></option><!-- 批發及零售業 -->
                                            <option value="0615G0"><spring:message code='LB.D0883' /></option><!-- 住宿及餐飲業 -->
                                            <option value="0615H0"><spring:message code='LB.D0884' /></option><!-- 運輸、倉儲及通信業 -->
                                            <option value="0615I0"><spring:message code='LB.D0885' /></option><!-- 金融及保險業 -->
                                            <option value="0615J0"><spring:message code='LB.D0886' /></option><!-- 不動產及租賃業 -->
                                            <option value="061610"><spring:message code='LB.X2441' /></option><!-- 其他專業服務業 (建築、電腦資訊、設計、顧問、研發、醫護、社服等服務業) -->
                                            <option value="061620"><spring:message code='LB.X2442' /></option><!-- 技術服務業 (出版、廣告、影視、休閒、保全、環保、維修、宗教、團體、美髮、殯葬、停車場…等) -->
                                            <option value="069999"><spring:message code='LB.D0899' /></option><!-- 非法人組織授信戶負責人 -->
                                            <option value="061630"><spring:message code='LB.D0889' /></option><!-- 特定專業服務業(律師、會計師、地政士、公證人、記帳士、記帳及報稅代理人) -->
                                            <option value="061640"><spring:message code='LB.D0890' /></option><!-- 特定專業服務業(受聘於專業服務業之行政事務職員) -->
                                            <option value="061650"><spring:message code='LB.D0891' /></option><!-- 銀樓業 (包含珠寶、鐘錶及貴金屬之製造、批發及零售) -->
                                            <option value="061660"><spring:message code='LB.D0892' /></option><!-- 虛擬貨幣交易服務業 -->
                                            <option value="061670"><spring:message code='LB.D0893' /></option><!-- 博弈業 -->
                                            <option value="061680"><spring:message code='LB.D0894' /></option><!-- 國防武器或戰爭設備相關行業(軍火) -->
                                            <option value="061690"><spring:message code='LB.D0082' /></option><!-- 家管 -->
                                            <option value="061691"><spring:message code='LB.D0083' /></option><!-- 自由業 -->
                                            <option value="061692"><spring:message code='LB.X0571' /></option><!-- 無業 -->
                                            <option value="061700"><spring:message code='LB.D0572' /></option><!-- 其他 -->
                                        </select>
                                    </div>
                                </span>
                            </div>
                            <!-- 職位名稱 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.X2438' /><span class="high-light">*</span></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <select class="custom-select select-input" name="CPRIMJOBTITLE" id="CPRIMJOBTITLE">
                                            <option value="#"><spring:message code="LB.X2448" /></option><!-- 請選擇職位名稱 -->
											<option value="01"><spring:message code='LB.X2443' /></option><!-- 董事長/負責人 -->  	     	
											<option value="02"><spring:message code='LB.X2444' /></option> <!--總經理 -->   	     	
											<option value="03"><spring:message code="LB.X0561" /></option><!-- 主管 -->
											<option value="04"><spring:message code="LB.X0588" /></option><!-- 專業人員 -->     	
											<option value="05"><spring:message code='LB.X2445' /></option><!-- 職員 -->    	
											<option value="06"><spring:message code='LB.X2446' /></option><!-- 業務/服務人員 -->    	     	
											<option value="07"><spring:message code='LB.D0572' /></option><!-- 其他 -->
                                        </select>
                                    </div>
                                </span>
                            </div>
                            <!-- 勸募人員  -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0076' /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<!-- 請輸入勸募人員名字 -->
                                    	<input type="text" class="text-input" name="FDRSGSTAFF" id="FDRSGSTAFF" placeholder="<spring:message code='LB.X2012' />" size="10" maxlength="10"/>
                                    </div>
                                </span>
                            </div>
                            <!-- 行員編號  -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0077' /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<!-- 請輸入行員編號 -->
                                    	<c:choose>
	                                    	<c:when test="${'040' == result_data.data.requestParam.requestParam.BRANCH}">
	                                    		<input type="text" class="text-input" name="BANKERNO" id="BANKERNO" placeholder="<spring:message code='LB.X2013' />" size="6" maxlength="6" value='087171' disabled/>
	                                    	</c:when>
	                                    	<c:otherwise>
	                                    		<input type="text" class="text-input" name="BANKERNO" id="BANKERNO" placeholder="<spring:message code='LB.X2013' />" size="6" maxlength="6" />
											</c:otherwise>
                                    	</c:choose>
                                    </div>
                                </span>
                            </div>
                            <!-- 申請預借現金密碼函 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X1999" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <label class="check-block" for="MCASH_1"><spring:message code="LB.X2000" /><!-- 本人同意申請 -->
                                            <input type="checkbox" name="MCASH_1" id="MCASH_1" value="2"/><!-- 同意1不同意2 -->
                                            <span class="ttb-check"></span>
                                        </label>
                                        <!-- 申請預借現金密碼後，本行將會在核卡後主動寄送給您。（未勾選視為不同意） -->
                                        <span class="input-remarks"><spring:message code="LB.X2001" />（<spring:message code="LB.X0591" />）</span>
                                        <input type="hidden" id="MCASH" name="MCASH" value="">
                                    </div>
                                </span>
                            </div>
                            <!-- 申請悠遊卡自動加值功能 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <!-- 申請悠遊卡/一卡通 -->
                                        <!-- 自動加值功能 -->
                                        <h4><spring:message code="LB.X2002" /></h4>
                                        <h4><spring:message code="LB.X2003" /></h4>
                                    </label>
                                </span>
                                <span class="input-block mt-auto">
                                    <div class="ttb-input">
                                    	<!-- 悠遊卡/一卡通預設已開啟自動加值功能 -->
                                    	<p><spring:message code="LB.X2004" /></p>
	                                    <c:if test="${result_data.data.requestParam.CARDMEMO != '2' }">
	                                        <label class="check-block" for="CNOTE4_1"><spring:message code="LB.X2005" /><!-- 不同意悠遊卡預設開啟 -->
	                                            <input type="checkbox" name="CNOTE4_1" id="CNOTE4_1"/><!-- 不同意2 -->
	                                            <span class="ttb-check"></span>
	                                        </label>
	                                        <!-- 一旦開啟自動加值後，將無法關閉。 -->
	                                        <span class="input-remarks"><spring:message code="LB.X2006" /></span>
	                                        <input type="hidden" id="CNOTE4" name="CNOTE4" value="">
	                                    </c:if>
                                    </div>
                                </span>
                            </div>                            
                            <!-- 聯名/認同卡之個資使用 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X2007" /></h4>
                                    </label>
                                </span>
                                <!-- 右邊文件太多，需要加 mt-auto -->
                                <span class="input-block mt-auto">
                                    <div class="ttb-input">
                                        <label class="check-block" for="CNOTE1_1"><spring:message code="LB.X2008" /><br>
                                        	<span class="text-danger">(未勾選將無法核發申辦之聯名/認同卡)</span>
                                            <input type="checkbox" name="CNOTE1_1" id="CNOTE1_1" value="2"/><!-- 同意1不同意2 -->
                                            <span class="ttb-check"></span>
                                            <input type="hidden" id="CNOTE1" name="CNOTE1" value="">
                                        </label>
                                    </div>
                                </span>
                            </div>
                            <!-- 個人資料之行銷運用 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X2010" /></h4>
                                    </label>
                                </span>
                                <!-- 右邊文件太多，需要加 mt-auto -->
                                <span class="input-block mt-auto">
                                    <div class="ttb-input">
                                        <label class="check-block" for="CNOTE3_1"><spring:message code="LB.X2011" />
                                            <input type="checkbox" name="CNOTE3_1" id="CNOTE3_1" value="2"/><!-- 同意1不同意2 -->
                                            <span class="ttb-check"></span>
                                            <input type="hidden" id="CNOTE3" name="CNOTE3" value="">
                                        </label>
                                    </div>
                                </span>
                            </div>                            
						</div>
						<input type="button" id="CMBACK" value="<spring:message code="LB.X0318" />" class="ttb-button btn-flat-gray"/><!-- 上一步 -->
						<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080" />" class="ttb-button btn-flat-orange"/><!-- 下一步 -->
					</div>
				</div>
			</form>
		</section>
	</main>
</div>
<%@ include file="../index/footer.jsp"%>
</body>
</html>