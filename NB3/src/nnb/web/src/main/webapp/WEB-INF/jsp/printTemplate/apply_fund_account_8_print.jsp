<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/><br/><br/>
	
	<table class="print">
		<tr>
			<!-- 申請時間 -->
			<th><spring:message code= "LB.D1097" /></th>
			<th>${CMQTIME }</th>
		</tr>
		<tr>
			<!-- 戶名 -->
			<th><spring:message code= "LB.D1215" /></th>
			<th>${DPUSERNAME }</th>
		</tr>
		<tr>
			<!-- 新臺幣扣款/入帳存款帳戶 -->
			<th><spring:message code= "LB.D1210" /></th>
			<th>${ACN_SSV_F }</th>
		</tr>
		<tr>
			<!-- 外幣扣款/入帳存款帳戶 -->
			<th><spring:message code= "LB.D1216" /></th>
			<th>${ACN_FUD_F }</th>
		</tr>
		<tr>
			<!-- 收取各項通知及報告書之E-MAIL -->
			<th><spring:message code= "LB.D1211" /></th>
			<th>${DPMYEMAIL }</th>
		</tr>
		<tr>
			<!-- 服務分行名稱 -->
			<th><spring:message code= "LB.D1217" /></th>
			<th>${BRHNAME }</th>
		</tr>
		<tr>
			<!-- 服務分行電話 -->
			<th><spring:message code= "LB.D1212" /></th>
			<th>${BRHTEL }</th>
		</tr>
		<tr>
			<!-- 服務分行地址 -->
			<th><spring:message code= "LB.D1218" /></th>
			<th>${BRHADDR }</th>
		</tr>
		<tr>
			<!-- 交易結果 -->
			<th><spring:message code= "LB.Transaction_result" /></th>
			<th>${RESULT }</th>
		</tr>
		<tr>
			<!-- 備註 -->
			<th><spring:message code="LB.Note" /></th>
			<th>${NOTE }</th>
		</tr>
		
	</table>
	<br/><br/><br/><br/>
</body>
</html>