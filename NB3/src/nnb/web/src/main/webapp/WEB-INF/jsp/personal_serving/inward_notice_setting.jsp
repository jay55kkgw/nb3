<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			init();
			setTimeout("initDataTable()",100);
		});
		
	    function init(){
			//initDataTable();//使用DataTable
			//表單驗證初始化
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
		    //送出事件
		    $("#CMSUBMIT").click(function(e) {
		        console.log("submit~~");
		        if (!$('#formId').validationEngine('validate')) {
		            e.preventDefault();
		        } else {
		            $("#formId").validationEngine('detach');
		           		initBlockUI();
		         	if(processQuery()){
		    			$("#formId").submit();
		         	}else{
		         		unBlockUI(initBlockId);
		         	}
		        }
		    });
	    }


	    function processQuery() {
	        var SR_Obj = document.getElementById("SelectedRecord");
	        var SR_Str = SR_Obj.value;
	        var paddcounter = "";
	        if (SR_Str.length == 0) {
	            //alert("<spring:message code="LB.Alert190" />");
	            errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert190' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	            return false;
	        }
	        SR_Str = ReplaceAll(SR_Str, "|", "");
	        var REC_NO = eval((SR_Str.length) / 21);
	        if (REC_NO > 20) {
	            //alert("<spring:message code="LB.Alert191" />");
	            errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert191' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	            return false;
	        }
	        else{
		        $('#REC_NO').val(REC_NO);
		        $('#ACNINFO').val(SR_Str);
		        return true;
	        }
	    }

	    function CheckObj(ChkObj, index, realstatus, oldamt) {
	        var SR_Obj = document.getElementById("SelectedRecord");
	        var AMOUNT = document.getElementById("AMT_" + index).value;
	        var rightamt = "";
	        if (!CheckNumber("AMT_" + index, "<spring:message code= "LB.Deposit_amount_1" />", false)) {
	            $('#AMT_'+index).val('');
	            document.getElementById("AMT_" + index).focus();
	            return false;
	        }
	        if (eval(AMOUNT) < 1000) {
	        	$('#AMT_'+index).val('1000');
	            AMOUNT =$('#AMT_'+index).val();
	        }
	        if (eval(AMOUNT) > 999999999 || isNaN(AMOUNT) || AMOUNT.replace(/(^s*)|(s*$)/g, "").length ==0) {
	            //alert("<spring:message code= "LB.Alert192" />");
	            errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert192' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	            setTimeout(function() {document.getElementById("AMT_" + index).focus();}, 100);
	            //document.getElementById("AMT_"+index).focus();
	            //document.getElementById("ACN_"+index).checked=false;
	            return false;
	        }
	        for (var i = 0; i < 9 - AMOUNT.length; i++) {
	            rightamt += "0";
	        }
	        rightamt += AMOUNT;

	        var SR_Str = SR_Obj.value;
	        var ChkStatus = ChkObj.checked;
	        var allvalue = "";
	        if (realstatus != "Y") {
	            if (ChkStatus) {
	                allvalue = ChkObj.value + '1' + rightamt;
	                if (SR_Str.length == 0) {
	                    SR_Str += allvalue;
	                } else {
	                    if (SR_Str.indexOf(allvalue) == -1) {
	                        SR_Str += "|" + allvalue;
	                    }
	                }
	            } else {
	                allvalue = ChkObj.value + '1' + rightamt;
	                SR_Str = ("|" + SR_Str + "|").replace("|" + allvalue + "|", "|");
	                if (SR_Str == "|") {
	                    SR_Str = "";
	                } else {
	                    SR_Str = SR_Str.substr(1, SR_Str.length - 2);
	                }
	            }
	        } else {
	            if (!ChkStatus) {
	                allvalue = ChkObj.value + '2' + rightamt;
	                if (SR_Str.length == 0) {
	                    SR_Str += allvalue;
	                } else {
	                    if (SR_Str.indexOf(allvalue) == -1) {
	                        SR_Str += "|" + allvalue;
	                    }
	                }
	            } else {
// 	                if (eval(oldamt) != eval(AMOUNT)) {
	                	allvalue = ChkObj.value + '2' + rightamt;
		                SR_Str = ("|" + SR_Str + "|").replace("|" + allvalue + "|", "|");
		                if (SR_Str == "|") {
		                    SR_Str = "";
		                } else {
		                    SR_Str = SR_Str.substr(1, SR_Str.length - 2);
		                }
// 	                }
	            }
	        }
	        document.getElementById("SelectedRecord").value = SR_Str;
	    }

	    function CheckObj1(index, oldamt) {
	        var index = index;
	        var SR_Obj = $('#SelectedRecord').val();
	        var AMOUNT = $('#AMT_'+index).val();
	        var rightamt = "";
	        var indexcheck = document.getElementById("ACN_" + index).checked;
	        if (oldamt != AMOUNT && indexcheck) {
	            if (eval(AMOUNT) < 1000 || eval(AMOUNT) > 999999999 || AMOUNT.replace(/(^s*)|(s*$)/g, "").length ==0) 
	            {
	                //alert("<spring:message code= "LB.Alert192" />!!!");
	                errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert192' />" + "!!!"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	                setTimeout(function() {document.getElementById("AMT_" + index).focus();}, 100);
	                //document.getElementById("AMT_"+index).focus();
	                //document.getElementById("ACN_"+index).checked=false;
	                return false;
	            }
	            for (var i = 0; i < 9 - AMOUNT.length; i++) {
	                rightamt += "0";
	            }
	            rightamt += AMOUNT;

	            var SR_Str = SR_Obj;
	            var allvalue = "";
	            if (indexcheck) {
	                allvalue = document.getElementById("ACN_" + index).value + '1' + rightamt;
	                var oldvalue = document.getElementById("ACN_" + index).value + '1';

	                if (SR_Str.length == 0) {
	                    SR_Str += allvalue;
	                } else {
	                    if (SR_Str.indexOf(oldvalue) >= 0) {
	                        SR_Str = SR_Str.substring(0, SR_Str.indexOf(oldvalue)) + allvalue + SR_Str.substring(SR_Str.indexOf(oldvalue) + 21);
	                    }
	                    if (SR_Str.indexOf(allvalue) == -1) {
	                        SR_Str += "|" + allvalue;
	                    }
	                }
	            }
	            document.getElementById("SelectedRecord").value = SR_Str;
	        }
	    }

	    function ReplaceAll(Source, stringToFind, stringToReplace) {
	        var temp = Source;
	        var index = temp.indexOf(stringToFind);
	        while (index != -1) {
	            temp = temp.replace(stringToFind, stringToReplace);
	            index = temp.indexOf(stringToFind);
	        }
	        return temp;
	    }
	    
	    function CheckNumber(ID, name, canEmpty) {
	    	var value = $("#" + ID).val();
	    	if (window.console) {
	    		console.log("value=" + value);
	    	}

	    	if (canEmpty == false && value == "") {
	    		//alert("<spring:message code= "LB.Alert193" />" + name);
	    		errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert193' />" + name], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	    		return false;
	    	}
	    	var regex = /[^0-9]/;
	    	if (regex.test(value)) {
	    		//alert(name + "<spring:message code="LB.Alert194" />");
	    		errorBlock(
					null, 
					null,
					[name + "<spring:message code= 'LB.Alert194' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	    		return false;
	    	}
	    	return true;
	    }

	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 國內臺幣匯入匯款通知設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0437" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%>
		<!-- 功能選單內容 -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					<spring:message code="LB.X0437" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" name="formId" method="post" action="${__ctx}/PERSONAL/SERVING/inward_notice_setting_result">
					<input type="hidden" name="ADOPID" value="N935">
					<input type="hidden" name="SelectedRecord" id="SelectedRecord" value="">
					<input type="hidden" name="REC_NO" id="REC_NO" value="">
					<input type="hidden" name="ACNINFO" id="ACNINFO" value="">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<ul class="ttb-result-list"></ul>
							<!-- 表格區塊 -->
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
									<tr>
										<!-- 設定 -->
										<th>
											<spring:message code="LB.X0438" />
										</th>
										<!-- 匯入匯款通知帳號 -->
										<th>
											<spring:message code="LB.X0439" />
										</th>
										<!-- 金額 -->
										<th>
											<spring:message code="LB.Deposit_amount_1"/>
										</th>
									</tr>
								</thead>
								<c:if test="${empty inward_notice_setting.data.REC}"> 
								<tbody>
								<tr>
								<td></td>
								<td></td>
								<td></td>
								</tr>
								</tbody>
								</c:if>
								<c:if test="${not empty inward_notice_setting.data.REC}"> 
								<tbody>
									<c:forEach varStatus="loop" var="dataList" items="${ inward_notice_setting.data.REC }">
										<tr>
											<!-- 設定 -->
											<c:choose>
												<c:when test="${dataList.FLAG =='Y'}">
													<td style="text-align:center">&nbsp;&nbsp;&nbsp;&nbsp;
														<label class="check-block">
															<input type="checkbox" name="SelACN" id="ACN_${loop.index}" value="${dataList.ACN}" 
																checked onclick="CheckObj(this,'${loop.index}','${dataList.FLAG}','${dataList.AMOUNT}')" />
															<span class="ttb-check"></span>&nbsp;
				                                        </label>
													</td>
												</c:when>
												<c:otherwise>
													<td style="text-align:center">&nbsp;&nbsp;&nbsp;&nbsp;
														<label class="check-block">
															<input type="checkbox" name="SelACN" id="ACN_${loop.index}" value="${dataList.ACN}"
																onclick="CheckObj(this,'${loop.index}','${dataList.FLAG}','${dataList.AMOUNT}')" />
															<span class="ttb-check"></span>&nbsp;
				                                        </label>
													</td>
												</c:otherwise>
											</c:choose>
											<!-- 匯入匯款通知帳號 -->
											<td style="text-align:center">${dataList.ACN}</td>
											<!-- 金額-->
											<c:choose>
												<c:when test="${dataList.AMOUNT==0}">
													<td style="text-align:right">
														<input type="text"name="AMT_${loop.index}" id="AMT_${loop.index}" value="0" maxlength="9" size="9" onblur="CheckObj1(${loop.index},${dataList.AMOUNT})" />
													</td>
												</c:when>
												<c:otherwise>
													<td style="text-align:right">
														<input type="text" name="AMT_${loop.index}" id="AMT_${loop.index}" value="${dataList.AMOUNT}" maxlength="9" size="9" onblur="CheckObj1(${loop.index},${dataList.AMOUNT})" />
													</td>
												</c:otherwise>
											</c:choose>
										</tr>
									</c:forEach>
								</tbody>
								</c:if>
								
							</table>
							<!-- Button -->
								<!-- 確定 -->
								<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
								<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
						<!-- Button -->
						</div>
					</div>
						<!-- 		說明： -->
						<ol class="description-list list-decimal">
							<p><spring:message code="LB.Description_of_page"/></p>
							<li><font face="細明體"><strong style="font-weight: 400">
							<spring:message code="LB.Inward_Notice_Setting_P1_D1"/></strong></font>
							</li>
							<li>
							<font face="細明體"><strong style="font-weight: 400">
							<spring:message code="LB.Inward_Notice_Setting_P1_D2"/></strong></font>
							</li>
							<li><font face="細明體"><strong style="font-weight: 400">
							<spring:message code="LB.Inward_Notice_Setting_P1_D3"/></strong></font>
							</li>				
							<li><font face="細明體"><strong style="font-weight: 400">
							<spring:message code="LB.Inward_Notice_Setting_P1_D4"/></strong></font>
							</li>				
							<li><font face="細明體"><strong style="font-weight: 400">
							<spring:message code="LB.Inward_Notice_Setting_P1_D5"/></strong></font>
							</li>	
							<li><font face="細明體"><strong style="font-weight: 400">
							<spring:message code="LB.Inward_Notice_Setting_P1_D6"/></strong></font>
							</li>				
						</ol>
					<!-- main-content-block END -->
				</form>
			</section>
		</main>
	</div>
</body>
</html>