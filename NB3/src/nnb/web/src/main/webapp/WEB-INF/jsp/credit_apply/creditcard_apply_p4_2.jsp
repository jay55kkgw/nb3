<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../__import_head_tag.jsp" %>
    <%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.twzipcode-1.6.1.js"></script>
    <!--舊版驗證-->
    <script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
    <!-- DIALOG會用到 -->
    <script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">
    <script type="text/JavaScript">
        var isTimeout = "${not empty sessionScope.timeout}";
        var countdownObjheader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
        var countdownSecHeader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
        $(document).ready(function () {
            //HTML載入完成後開始遮罩
            setTimeout("initBlockUI()", 10);
            // 開始查詢資料並完成畫面
            setTimeout("init()", 20);
            //解遮罩
            setTimeout("unBlockUI(initBlockId)", 500);
            //time out
            timeLogout();
        });

        function init() {
            $("#formId").validationEngine({
                binded: false,
                promptPosition: "inline"
            });
            $("#bank").hide();
// 	getbank();
            setCITY();
// 	$("#BRANCHNAMEC").val($("#BHID").val());
// 	$("#BRANCHNAME").val($("#BHID").find(":selected").text());
            //悠遊卡自動加值勾選
            if (${result_data.data.requestParam.VARSTR2 == "2"}) {
                $("#CNOTE4").prop("checked", true);
            }

            backData();
            $("#CMSUBMIT").click(function (e) {
                var oldcardowner = "${result_data.data.oldcardowner}";
                //聯名/認同卡之個資使用
                $("#CNOTE1_1").addClass("validate[funcCall[validate_CheckBox['聯名/認同卡之個資使用',CNOTE1_1,#]]]");
                if ($("#MCARDTO").val() == '1') {
                    $("#BCITY").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.D0149' />,BCITY,#]]]");
                    $("#BZONE").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.D0149' />,BZONE,#]]]");
// 			$("#MBILLTO").removeClass("validate[funcCallRequired[validate_CheckSelect[<spring:message code='LB.D0149' />,MBILLTO,#]]]");
                } else {
                    $("#BCITY").removeClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.D0149' />,BCITY,#]]]");
                    $("#BZONE").removeClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.D0149' />,BZONE,#]]]");
// 			$("#MBILLTO").addClass("validate[funcCallRequired[validate_CheckSelect[<spring:message code='LB.D0149' />,MBILLTO,#]]]");
                }
                if (oldcardowner == "Y") {
                    $("#MCARDTO").removeClass("validate[funcCallRequired[validate_CheckSelect[<spring:message code='LB.D0071' />,MCARDTO,#]]]");
                    $("#MBILLTO").removeClass("validate[funcCallRequired[validate_CheckSelect[<spring:message code='LB.D0149' />,MBILLTO,#]]]");
                }
                e = e || window.event;
                if (!$('#formId').validationEngine('validate')) {
                    e.preventDefault();
                } else {

                    // 		if(oldcardowner == "N"){

                    // 	   	  	if($("input[type=radio][name=CNOTE1]").is(":checked") != true){
                    // 	   	  		alert("<spring:message code= "LB.Alert043" />");
                    // 	   	  		return false;
                    // 	   	  	}
                    // 	   	  	if($("input[type=radio][name=CNOTE3]").is(":checked") != true){
                    // 	   	  		alert("<spring:message code= "LB.Alert044" />");
                    // 	   	  		return false;
                    // 			}
                    // 		}

                    // 		if(!CheckSelect("MBILLTO","<spring:message code= "LB.D0149" />","#")){
                    // 			return false;
                    // 		}
                    // 		if(!CheckSelect("MCARDTO","<spring:message code= "LB.D0071" />","#")){
                    // 			return false;
                    // 		}
                    //電子帳單
                    if ($("#VARSTR3_1").prop("checked") == true) {
                        $("#VARSTR3").val("11");
                    } else {
                        $("#VARSTR3").val("00");
                    }
                    //聯名/認同卡之個資使用
                    if ($("#CNOTE1_1").prop("checked") == true) {
                        $("#CNOTE1").attr("value", "1");
                    } else {
                        $("#CNOTE1").attr("value", "2");
                    }
                    //個人資料之行銷運用
                    if ($("#CNOTE3_1").prop("checked") == true) {
                        $("#CNOTE3").attr("value", "1");
                    } else {
                        $("#CNOTE3").attr("value", "2");
                    }
                    //悠遊卡自動
                    if ($("#CNOTE4_1").prop("checked") == true) {
                        $("#CNOTE4").attr("value", "2");
                        $("#VARSTR2").val("2");
                    } else {
                        $("#CNOTE4").attr("value", "1");
                    }
                    //申請預借現金密碼函
                    if ($("#MCASH_1").prop("checked") == true) {
                        $("#MCASH").attr("value", "1");
                    } else {
                        $("#MCASH").attr("value", "2");
                    }
                    <!-- Avoid Reflected XSS All Clients -->
                    var CARDMEMO = "<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDMEMO)}' />";

                    if (CARDMEMO == "1" || CARDMEMO == "2") {
                        $("input[type=radio][name=CNOTE1][value=1]").prop("checked", true);
                    }

                    if (!ckValue()) {//行員編號檢核
                        return false;
                    }
                    //自動加值功能是否不同意開啟
                    // 		if($("#CNOTE4_1").prop("checked") == true){
                    // 			$("#VARSTR2").val("2");
                    // 		}
                    if ($("#MCARDTO").val() == '1') {
                        $("#BRANCHNAME").val($("#BHID").val());
                        $("#BRANCHNAMEC").val($("#BHID").find(":selected").text());
                    } else {
                        $("#BRANCHNAME").val("");
                        $("#BRANCHNAMEC").val("");
                    }

                    initBlockUI();
                    $("#BANKERNO").prop("disabled", false);
// 			var CPRIMBIRTHDAYshow = '${result_data.data.CPRIMBIRTHDAYshow}'
// 			var reg = new RegExp(" ","g");
// 			CPRIMBIRTHDAYshow = CPRIMBIRTHDAYshow.replace(reg,"_");
// 			$("#CPRIMBIRTHDAYshow").val(CPRIMBIRTHDAYshow);
                    $("#formId").attr("action", "${__ctx}/CREDIT/APPLY/apply_creditcard_p4_3");
                    $("#formId").submit();
                }
            });
            $("#CMBACK").click(function (e) {
                $("#formId").validationEngine('detach');
                console.log("submit~~");
                $("#formId").attr("action", "${__ctx}/CREDIT/APPLY/apply_creditcard_p4");
                $("#back").val('Y');

                initBlockUI();
                $("#formId").submit();
            });
        }

        //行員編號邏輯性檢核
        function ckValue() {
            var BANKERNO = $("#BANKERNO").val();

            var arr = ['7', '9', '7', '3', '1'];//加權

            var istrue = BANKERNO.substring(0, 1) * arr[0] +
                BANKERNO.substring(1, 2) * arr[1] +
                BANKERNO.substring(2, 3) * arr[2] +
                BANKERNO.substring(3, 4) * arr[3] +
                BANKERNO.substring(4, 5) * arr[4];

            var finalValue = BANKERNO.substring(5, 6);
            var modOk = istrue % 10;
            //輸入值最後一位是否等於餘數
            if (finalValue != modOk) {
                errorBlock(null, null, ["<spring:message code='LB.Alert046' />"],
                    '<spring:message code= "LB.Quit" />', null);
// 		alert("<spring:message code= "LB.Alert046" />");
                return false;
            }
            return true;
        }

        //帳單地址
        function getAddr() {
            var adv = $("#MBILLTO").val();
            if (adv == "1") {
                //戶籍地址
                $("#AD").text("");
                $("#AD").text("<spring:message code= 'LB.D0143' />：" + '${result_data.data.CPRIMADDR2}');
            } else if (adv == "2") {
                //居住地址
                $("#AD").text("");
                $("#AD").text("<spring:message code= 'LB.D0063' />：" + '${result_data.data.CPRIMADDR}');
            } else if (adv == "3") {
                //公司地址
                $("#AD").text("");
                $("#AD").text("<spring:message code= 'LB.D0090' />：" + '${result_data.data.CPRIMADDR3}');
            } else {
                $("#AD").text("");
            }
        }

        //初始化 指定服務分行
        function setCITY() {
            llo = "${__ctx}/js/";
            $('#twzipcode').twzipcode({
                countyName: 'BCITY',
                districtName: 'BZONE',
                zipcodeName: 'BZIP',
                countySel: '${result_data.data.BCITY}',
                districtSel: '${result_data.data.BZONE}',
                language: 'zip_${pageContext.response.locale}',
                onCountySelect: null,
                onDistrictSelect: function () {
                    doquery8();
                }
            });
            $("#BHID").children().each(function () {
                if ($(this).val() == '${result_data.data.BHID}') {
                    //jQuery給法
                    console.log("TRUE");
                    $(this).attr("selected", true); //或是給"selected"也可
                }
            });
        }

        //顯示該Zip下之分行
        function doquery8() {
            $.ajax({
                type: "POST",
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_zip_bh",
                data: {
                    zip: $("input[name='BZIP']").val()
                },
                async: false,
                dataType: "json",
                success: function (msg) {
                    $('#BHID').html("<option value=''>--- <spring:message code= "LB.X1003" /> ---</option>  ");
                    if ('${transfer}' == 'en') {
                        msg.data.bh.forEach(function (bh) {
                            console.log(bh);
                            $("#BHID").append("<option value='" + bh.adbranchid + "'> " + bh.adbranengname + "</option>");
                        });
                    } else if ('${transfer}' == 'zh') {
                        msg.data.bh.forEach(function (bh) {
                            console.log(bh);
                            $("#BHID").append("<option value='" + bh.adbranchid + "'> " + bh.adbranchsname + "</option>");
                        });
                    } else {
                        msg.data.bh.forEach(function (bh) {
                            console.log(bh);
                            $("#BHID").append("<option value='" + bh.adbranchid + "'> " + bh.adbranchname + "</option>");
                        });
                    }
                    var bd = '${result_data.data.back}';
                    if (bd == 'Y') {
                        $('#BHID').val('${result_data.data.BHID}');
                    }
//         	$("#BHID option:eq(1)").prop("selected",true);

                }
            })
        }

        //取得分行
        function getbank() {
            uri = '${__ctx}' + "/CREDIT/APPLY/get_bank_ajax";
            console.log("creatOutAcn.uri: " + uri);

            data = fstop.getServerDataEx(uri, null, false,
                callback);
        }

        function callback(data) {
            console.log("data: " + data);
            if (data) {
                //ajax回傳資料型態: List<Map<String, String>>
                console.log("data.json: " + JSON.stringify(data));
                //先清空原有之"OPTION"內容,並加上一個 "---請選擇分行---" 欄位
                $("#bankName").empty();
                $("#bankName").append($("<option></option>").attr("value", "#").text("---<spring:message code="LB.X1003"/>---"));
                data.forEach(function (map) {
                    console.log(map);
                    $("#bankName").append($("<option></option>").attr("value", map.ADBRANCHID).text(map.ADBRANCHNAME));
                });
            }
        }

        //選擇卡片領取方式
        function disBCH() {
            var mcardto = $("#MCARDTO").val();//1親領 2掛號郵寄

            if (mcardto == "1") {
                $("#bank").show();
            } else {
                $("#bank").hide();
                $("#BHID").val("");
// 		$("#bankName").val("#");
                $("#BRANCHNAME").val("");
// 		$("#BRANCHNAMEC").val("");
            }
        }

        function setBranchName() {
            var BRANCHNO = $("#bankName").val();

            if (BRANCHNO != "#") {
                $("#BRANCHNAME").val(BRANCHNO);
            } else {
                $("#BRANCHNAME").val("");
            }
        }

        function setvalue() {
            if ($("#VARSTR3").prop("checked") == true) {
                $("#VARSTR3").attr("value", "11");
            } else {
                $('#VARSTR3').prop("checked", false);
                $("#VARSTR3").attr("value", "00");
            }
        }

        function backData() {
            var BD = '${result_data.data.back}';
            if (BD == "Y") {
                $("#MCARDTO").val('${result_data.data.MCARDTO}');
                disBCH();
                $("#MBILLTO").val('${result_data.data.MBILLTO}');
                getAddr();
                $("#FDRSGSTAFF").val('${result_data.data.FDRSGSTAFF}');
                $("#BANKERNO").val('${result_data.data.BANKERNO}');
                $("#MEMO").val('${result_data.data.MEMO}');

                $("#VARSTR3").val('${result_data.data.VARSTR3}');
                $("#MCASH").val('${result_data.data.MCASH}');
                $("#CNOTE4").val('${result_data.data.CNOTE4}');
                $("#CNOTE1").val('${result_data.data.CNOTE1}');
                $("#CNOTE3").val('${result_data.data.CNOTE3}');
                var V3 = $("#VARSTR3").val();
                if (V3 == "11") {
                    $('#VARSTR3_1').prop("checked", true);
                }
                var mca = $("#MCASH").val();
                if (mca == "1") {
                    $('#MCASH_1').prop("checked", true);
                }
                var C4 = $("#CNOTE4").val();
                if (C4 == "2") {
                    $('#CNOTE4_1').prop("checked", true);
                }
                var C1 = $("#CNOTE1").val();
                if (C1 == "1") {
                    $('#CNOTE1_1').prop("checked", true);
                }
                var C3 = $("#CNOTE3").val();
                if (C3 == "1") {
                    $('#CNOTE3_1').prop("checked", true);
                }
            }
        }

        function timeLogout() {
            // 刷新session
            var uri = '${__ctx}/login_refresh';
            console.log('refresh.uri: ' + uri);
            var result = fstop.getServerDataEx(uri, null, false, null);
            console.log('refresh.result: ' + JSON.stringify(result));
            // 初始化登出時間
            $("#countdownheader").html(parseInt(countdownSecHeader) + 1);
            $("#countdownMin").html("");
            $("#mobile-countdownMin").html("");
            $("#countdownSec").html("");
            $("#mobile-countdownSec").html("");
            // 倒數
            startIntervalHeader(1, refreshCountdownHeader, []);
        }

        function refreshCountdownHeader() {
            // timeout剩餘時間
            var nextSec = parseInt($("#countdownheader").html()) - 1;
            $("#countdownheader").html(nextSec);

            // 提示訊息--即將登出，是否繼續使用
            if (nextSec == 120) {
                initLogoutBlockUI();
            }
            // timeout
            if (nextSec == 0) {
                // logout
                fstop.logout('${__ctx}' + '/logout_aj', '${__ctx}' + '/timeout_logout');
            }
            if (nextSec >= 0) {
                // 倒數時間以分秒顯示
                var minutes = Math.floor(nextSec / 60);
                $("#countdownMin").html(('0' + minutes).slice(-2));
                $("#mobile-countdownMin").html(('0' + minutes).slice(-2));

                var seconds = nextSec - minutes * 60;
                $("#countdownSec").html(('0' + seconds).slice(-2));
                $("#mobile-countdownSec").html(('0' + seconds).slice(-2));
            }
        }

        function startIntervalHeader(interval, func, values) {
            clearInterval(countdownObjheader);
            countdownObjheader = setRepeater(func, values, interval);
        }

        function setRepeater(func, values, interval) {
            return setInterval(function () {
                func.apply(this, values);
            }, interval * 1000);
        }

        /**
         * 初始化logoutBlockUI
         */
        function initLogoutBlockUI() {
            logoutblockUI();
        }

        /**
         * 畫面BLOCK
         */
        function logoutblockUI(timeout) {
            $("#logout-block").show();

            // 遮罩後不給捲動
            document.body.style.overflow = "hidden";

            var defaultTimeout = 60000;
            if (timeout) {
                defaultTimeout = timeout;
            }
        }

        /**
         * 畫面UNBLOCK
         */
        function unLogoutBlockUI(timeoutID) {
            if (timeoutID) {
                clearTimeout(timeoutID);
            }
            $("#logout-block").hide();

            // 解遮罩後給捲動
            document.body.style.overflow = 'auto';
        }

        /**
         *繼續使用
         */
        function keepLogin() {
            unLogoutBlockUI(); // 解遮罩
            timeLogout(); // 刷新倒數計時
        }

    </script>
</head>
<body>
<!-- header -->
<header>
    <%@ include file="../index/header_logout.jsp" %>
</header>

<!-- 麵包屑     -->
<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
    <ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
        <li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
        <!-- 申請信用卡     -->
        <li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0666"/></li>
    </ol>
</nav>

<!--左邊menu及登入資訊-->
<div class="content row">
    <%-- 	<%@ include file="../index/menu.jsp"%> --%>
    <!--快速選單及主頁內容-->
    <main class="col-12">
        <!--主頁內容-->
        <section id="main-content" class="container">
            <!--線上申請信用卡 -->
            <h2><spring:message code="LB.D0022"/></h2>
            <div id="step-bar">
                <ul>
                    <li class="finished">信用卡選擇</li><!-- 身份驗證 -->
                    <li class="finished">身份驗證與權益</li><!-- 信用卡與權益 -->
                    <li class="active"><spring:message code="LB.X1967"/></li><!-- 申請資料 -->
                    <li class=""><spring:message code="LB.Confirm_data"/></li><!-- 確認資料 -->
                    <li class=""><spring:message code="LB.X1968"/></li><!-- 完成申請 -->
                </ul>
            </div>
            <!-- timeout -->
            <section id="id-and-fast">
						<span class="id-name">
							<spring:message code="LB.Welcome_1"/>&nbsp;
							<c:if test="${empty result_data.data.CUSNAME}">
                                <span id="username" name="username_show"><spring:message code="LB.X2190"/></span>
                            </c:if>
							<c:if test="${not empty result_data.data.CUSNAME}">
                                <span id="username" name="username_show">${result_data.data.CUSNAME}</span>
                            </c:if>
							&nbsp;<spring:message code="LB.Welcome_2"/>
						</span>
                <div id="id-block">
                    <div>
							<span class="id-time">
								<fmt:setLocale value="${pageContext.response.locale}"/>
								<c:if test="${!empty sessionScope.logindt && !empty sessionScope.logintm}">
                                    <fmt:parseDate var="parseDate"
                                                   value="${sessionScope.logindt} ${sessionScope.logintm}"
                                                   pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <fmt:formatDate value="${parseDate}" dateStyle="full"
                                                    type="both"/>&nbsp;<spring:message code="LB.X2250"/>
                                    <br/>
                                </c:if>

								<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full"/>
								&nbsp;<spring:message code="LB.Login_successful"/>
							</span>
                        <!-- 自動登出剩餘時間 -->
                        <span class="id-time"><spring:message code="LB.X1912"/>
								<span class="high-light ml-1">
									<!-- 分 -->
									<font id="countdownMin"></font>
									:
                                    <!-- 秒 -->
									<font id="countdownSec"></font>
								</span>
							</span>
                    </div>
                    <button type="button" class="btn-flat-orange" onclick="timeLogout()"><spring:message
                            code="LB.X1913"/></button>
                    <button type="button" class="btn-flat-darkgray"
                            onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')"><spring:message
                            code="LB.Logout"/></button>
                </div>
            </section>
            <form method="post" id="formId">
                <!-- 				<input type="hidden" name="ADOPID" value="NA03"/> -->
                <%-- 				<input type="hidden" name="CUSIDN" value="${result_data.data.requestParam.CUSIDN}"/> --%>
                <%-- 				<input type="hidden" name="CFU2" value="${result_data.data.requestParam.CFU2}"/> --%>
                <%-- 				<input type="hidden" name="CN" value="${result_data.data.requestParam.CN}"/> --%>
                <%-- 				<input type="hidden" name="CARDNAME" value="${result_data.data.requestParam.CARDNAME}"/> --%>
                <%-- 				<input type="hidden" name="CARDMEMO" value="${result_data.data.requestParam.CARDMEMO}"/> --%>
                <%-- 				<input type="hidden" name="FGTXWAY" value="${result_data.data.requestParam.FGTXWAY}"/> --%>
                <%-- 				<input type="hidden" name="OLAGREEN1" value="${result_data.data.requestParam.OLAGREEN1}"/> --%>
                <%-- 				<input type="hidden" name="OLAGREEN2" value="${result_data.data.requestParam.OLAGREEN2}"/> --%>
                <%-- 				<input type="hidden" name="OLAGREEN3" value="${result_data.data.requestParam.OLAGREEN3}"/> --%>
                <%-- 				<input type="hidden" name="OLAGREEN4" value="${result_data.data.requestParam.OLAGREEN4}"/> --%>
                <%-- 				<input type="hidden" name="OLAGREEN5" value="${result_data.data.requestParam.OLAGREEN5}"/> --%>
                <!-- 			  	<input type="hidden" id="RCVNO" name="RCVNO"/> -->
                <!-- 			  	<input type="hidden" id="CPRIMADDR" name="CPRIMADDR"/> -->
                <!-- 			  	<input type="hidden" id="CPRIMADDR2" name="CPRIMADDR2"/> -->
                <!-- 			  	<input type="hidden" id="CPRIMADDR3" name="CPRIMADDR3"/> -->
                <!-- 			  	<input type="hidden" name="STATUS" value="0"/> -->
                <input type="hidden" id="VARSTR2" name="VARSTR2" value="1"/>
                <input type="hidden" id="BRANCHNAMEC" name="BRANCHNAMEC"/>
                <input type="hidden" id="BRANCHNAME" name="BRANCHNAME"/>
                <input type="hidden" name="back" id="back"/>
                <%-- 			  	<input type="hidden" name="oldcardowner" value="${result_data.data.requestParam.oldcardowner}"/> --%>
                <%-- 			  	<input type="hidden" name="BIRTHY" value="${result_data.data.requestParam.BIRTHY}"/> --%>
                <%-- 			  	<input type="hidden" name="BIRTHM" value="${result_data.data.requestParam.BIRTHM}"/> --%>
                <%-- 			  	<input type="hidden" name="BIRTHD" value="${result_data.data.requestParam.BIRTHD}"/> --%>
                <%-- 			  	<input type="hidden" name="CTTADR" value="${result_data.data.requestParam.CTTADR}"/> --%>
                <%-- 			  	<input type="hidden" name="PMTADR" value="${result_data.data.requestParam.PMTADR}"/> --%>
                <%-- 			  	<input type="hidden" name="ALLdata" value="${result_data.data}"/> --%>
                <%-- 			  	<input type="hidden" name="CPRIMBIRTHDAYshow" id="CPRIMBIRTHDAYshow" value="${result_data.data.CPRIMBIRTHDAYshow}"/> --%>


                <!-- 顯示區  -->
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <!-- 申請資料 -->
                                <p><spring:message code="LB.X1967"/> (2/3)</p>
                            </div>
                            <div class="classification-block">
                                <!-- 信用卡申請資料 -->
                                <p><spring:message code="LB.X1996"/></p>
                                <!-- 								<p> -->
                                <%-- 									( <span class="high-light">*</span> <spring:message code="LB.D0557" />)<!-- 為必填 --> --%>
                                <!-- 								</p> -->
                                <p>
                                    <!-- 									新卡戶請填寫完整基本資料，已持有本行信用卡正卡僅需填寫「<span class="high-light">*</span>」欄位 -->
                                    <spring:message code='LB.X2409'/>「<span class="high-light"><spring:message
                                        code='LB.X2410'/></span>」<spring:message code='LB.X2411'/>
                                </p>
                            </div>
                            <!-- 卡片領取方式 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0071"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <select name="MCARDTO" id="MCARDTO" onchange="disBCH()"
                                                class="custom-select select-input validate[funcCallRequired[validate_CheckSelect[<spring:message code='LB.D0071' />,MCARDTO,#]]]">
                                            <option value="#"><spring:message code="LB.Select"/></option>
                                            <!-- 請選擇卡片領取方式 -->
								 			<option value="1"><spring:message code="LB.D0072" /><!-- 親領 --></option>
								 			<option value="2"><spring:message code="LB.D0073" /><!-- 掛號郵寄 --></option>
                                        </select>
                                    </div>
                                    <div class="ttb-input" id="bank">
	                                    <div class="ttb-input" id="twzipcode">
											<span data-role="county"
                                                  data-style="custom-select select-input input-width-125 validate[required]"></span>
											<span data-role="district"
                                                  data-style="custom-select select-input input-width-100 validate[required]"></span>
											<span data-role="zipcode" data-name="BZIP" data-style="zipcode"
                                                  style="display: none;"></span>
										</div>
										<div class="ttb-input">
											<select name="BHID" id="BHID"
                                                    class="custom-select select-input validate[required]"
                                                    value="${result_data.data.BHID}">
									        	<option value=""><spring:message code="LB.X1003"/></option>  
									        	<c:forEach var="dataList" items="${bh_data}">
                                                    <option value="${dataList.ADBRANCHID}"> ${dataList.ADBRANCHNAME}</option>
                                                </c:forEach>
											</select>
										</div>
									</div>
                                    <!-- 									<div class="ttb-input" id="bank"> -->
                                    <!-- 										<select id="bankName" name="bankName" class="custom-select select-input" onchange="setBranchName()"> -->
                                    <!-- 										</select> -->
                                    <!-- 									</div> -->
                                </span>
                            </div>
                            <!-- 帳單地址 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0149"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <select class="custom-select select-input validate[funcCallRequired[validate_CheckSelect[<spring:message code='LB.D0149' />,MBILLTO,#]]]"
                                                name="MBILLTO" id="MBILLTO" onchange="getAddr()">
                                            <option value="#"><spring:message code="LB.Select"/></option>
                                            <!-- 請選擇帳單地址 -->
								 			<option value="1"><spring:message code="LB.D1125"/></option><!-- 同戶籍地址 -->
								 			<option value="2"><spring:message code="LB.X0552"/></option><!-- 同居住地址 -->
								 			<option value="3"><spring:message code="LB.X0553"/></option><!-- 同公司地址-->
                                        </select>
                                        <span class="input-remarks" id="AD">
                                        </span>
                                    </div>
                                </span>
                            </div>
                            <!-- 申請電子帳單 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0279"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<!-- 本人同意使用信用卡電子帳單，並以詳閱 -->
                                        <label class="check-block" for="VARSTR3_1"><spring:message code="LB.X1997"/><a
                                                href="${__ctx}/term/NA03_6.pdf" target="_blank"><spring:message
                                                code="LB.X0556"/></a>。
                                            <input type="checkbox" name="VARSTR3_1" id="VARSTR3_1"/>
                                            <span class="ttb-check"></span>
                                        </label>
                                        <!-- 如未勾選，視為不同意申請。申請電子帳單後，本行將不再寄送紙本帳單。 -->
                                        <span class="input-remarks"><spring:message code="LB.X1998"/></span>
                                        <input type="hidden" id="VARSTR3" name="VARSTR3" value="">
                                    </div>
                                </span>
                            </div>
                            <!-- 申請預借現金密碼函 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X1999"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <label class="check-block"
                                               for="MCASH_1"><spring:message code="LB.X2000" /><!-- 本人同意申請 -->
                                            <input type="checkbox" name="MCASH_1" id="MCASH_1" value="2"/>
                                            <!-- 同意1不同意2 -->
                                            <span class="ttb-check"></span>
                                        </label>
                                        <!-- 申請預借現金密碼後，本行將會在核卡後主動寄送給您。（未勾選視為不同意） -->
                                        <span class="input-remarks"><spring:message code="LB.X2001"/>（<spring:message
                                                code="LB.X0591"/>）</span>
                                        <input type="hidden" id="MCASH" name="MCASH" value="">
                                    </div>
                                </span>
                            </div>
                            <!-- 申請悠遊卡自動加值功能 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <!-- 申請悠遊卡/一卡通 -->
                                        <!-- 自動加值功能 -->
                                        <h4><spring:message code="LB.X2002"/></h4>
                                        <h4><spring:message code="LB.X2003"/></h4>
                                    </label>
                                </span>
                                <span class="input-block mt-auto">
                                    <div class="ttb-input">
                                    	<!-- 悠遊卡/一卡通預設已開啟自動加值功能 -->
                                    	<p><spring:message code="LB.X2004"/></p>
	                                    <c:if test="${result_data.data.CARDMEMO != '2' }">
	                                        <label class="check-block"
                                                   for="CNOTE4_1"><spring:message code="LB.X2005" /><!-- 不同意悠遊卡預設開啟 -->
	                                            <input type="checkbox" name="CNOTE4_1" id="CNOTE4_1"/><!-- 不同意2 -->
	                                            <span class="ttb-check"></span>
	                                        </label>
                                            <!-- 一旦開啟自動加值後，將無法關閉。 -->
                                            <span class="input-remarks"><spring:message code="LB.X2006"/></span>
                                            <input type="hidden" id="CNOTE4" name="CNOTE4" value="">
                                        </c:if>
                                    </div>
                                </span>
                            </div>
                            <!-- 聯名/認同卡之個資使用 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X2007"/></h4>
                                    </label>
                                </span>
                                <!-- 右邊文件太多，需要加 mt-auto -->
                                <span class="input-block mt-auto">
                                    <div class="ttb-input">
                                        <label class="check-block" for="CNOTE1_1"><spring:message code="LB.X2008"/><br>
                                        	<span class="text-danger">(未勾選將無法核發申辦之聯名/認同卡)</span>
                                            <input type="checkbox" name="CNOTE1_1" id="CNOTE1_1" value="2"/>
                                            <!-- 同意1不同意2 -->
                                            <span class="ttb-check"></span>
                                            <input type="hidden" id="CNOTE1" name="CNOTE1" value="">
                                        </label>
                                    </div>
                                </span>
                            </div>
                            <!-- 個人資料之行銷運用 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X2010"/></h4>
                                    </label>
                                </span>
                                <!-- 右邊文件太多，需要加 mt-auto -->
                                <span class="input-block mt-auto">
                                    <div class="ttb-input">
                                        <label class="check-block" for="CNOTE3_1"><spring:message code="LB.X2011"/>
                                            <input type="checkbox" name="CNOTE3_1" id="CNOTE3_1" value="2"/>
                                            <!-- 同意1不同意2 -->
                                            <span class="ttb-check"></span>
                                            <input type="hidden" id="CNOTE3" name="CNOTE3" value="">
                                        </label>
                                    </div>
                                </span>
                            </div>
                            <!-- 勸募人員  -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0076'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<!-- 請輸入勸募人員名字 -->
                                    	<input type="text" class="text-input" name="FDRSGSTAFF" id="FDRSGSTAFF"
                                               placeholder="<spring:message code='LB.X2012' />" size="10"
                                               maxlength="10"/>
                                    </div>
                                </span>
                            </div>
                            <!-- 行員編號  -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0077'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<!-- 請輸入行員編號 -->
                                    	<c:choose>
                                            <c:when test="${'040' == result_data.data.BRANCH}">
                                                <input type="text" class="text-input" name="BANKERNO" id="BANKERNO"
                                                       placeholder="<spring:message code='LB.X2013' />" size="6"
                                                       maxlength="6" value='087171' disabled/>
                                            </c:when>
                                            <c:otherwise>
                                                <input type="text" class="text-input" name="BANKERNO" id="BANKERNO"
                                                       placeholder="<spring:message code='LB.X2013' />" size="6"
                                                       maxlength="6"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </span>
                            </div>
                            <!-- 備註 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0078'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<!-- 請輸入備註 -->
                                    	<input type="text" class="text-input" name="MEMO" id="MEMO"
                                               placeholder="<spring:message code='LB.X2014' />" size="35"
                                               maxlength="35"/>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <input type="button" id="CMBACK" value="<spring:message code="LB.X0318" />"
                               class="ttb-button btn-flat-gray"/><!-- 上一步 -->
                        <input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080" />"
                               class="ttb-button btn-flat-orange"/><!-- 下一步 -->
                    </div>
                </div>
            </form>
        </section>
    </main>
</div>
<%@ include file="../index/footer.jsp" %>
</body>
</html>