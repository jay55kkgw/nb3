<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<style>
   /* footable */
.table {
    border-collapse: separate;
    border-spacing: 0;
    width: 100%;
    border: solid #DDD 1px;
    -moz-border-radius: 6px;
    -webkit-border-radius: 6px;
    border-radius: 6px;
    margin: 20px auto;
}

.table th {
    cursor: n-resize;
}

.table td a {
    text-decoration: none;
    color: #000;
}

.table td a.origin {
    text-decoration: underline;
    color: blue;
}

.table td a:hover {}

.table.breakpoint>tbody>tr>td.expand {
    background: none;
    /*background: url('../img/plus.png') no-repeat 5px center;*/
    padding-left: 40px;
}

.table.breakpoint>tbody>tr>td.expand:before {
    content: "";
}

.table.breakpoint>tbody>tr.table-detail-show>td.expand {
    background: none;
    /*background: url('minus.png') no-repeat 5px center;*/
}

.table.breakpoint>tbody>tr.table-detail-show>td.expand:before {
    content: "";
}

.table.breakpoint>tbody>tr.table-row-detail {
    background: #FAFAFA;
}

.table>tbody>tr:hover {
    background: #fbf8e9;
}

.table.breakpoint>tbody>tr:hover:not (.table-row-detail) {
    cursor: pointer;
}

.table>tbody>tr>td, .table>thead>tr>th {
    border-left: 1px solid #DDD;
    border-top: 1px solid #DDD;
    padding: 8px;
    text-align: center;
}

.table>tbody>tr>td.table-cell-detail {
    border-left: none;
}

table>tbody>tr>td>span.table-toggle {
    display: inline;
}

.table>tbody>tr td:first-child {
    padding-left: 20px;
}

.table>thead>tr>th, .table>thead>tr>td {
    background-color: #EEEEEE;
    border-top: none;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    font-weight: bold;
}

.table>thead>tr:first-child>th.table-first-column, .table>thead>tr:first-child>td.table-first-column {
    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;
    border-radius: 6px 0 0 0;
}

.table>thead>tr:first-child>th.table-last-column, .table>thead>tr:first-child>td.table-last-column {
    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;
    border-radius: 0 6px 0 0;
}

.table>thead>tr:first-child>th.table-first-column.table-last-column,
.table>thead>tr:first-child>td.table-first-column.table-last-column {
    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;
    border-radius: 6px 6px 0 0;
}

.table>tbody>tr:last-child>td.table-first-column {
    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;
    border-radius: 0 0 0 6px;
}

.table>tbody>tr:last-child>td.table-last-column {
    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;
    border-radius: 0 0 6px 0;
}

.table>tbody>tr:last-child>td.table-first-column.table-last-column {
    -moz-border-radius: 0 0 6px 6px;
    -webkit-border-radius: 0 0 6px 6px;
    border-radius: 0 0 6px 6px;
}

.table>thead>tr>th.table-first-column, .table>thead>tr>td.table-first-column,
.table>tbody>tr>td.table-first-column {
    border-left: none;
}

.table>tbody img {
    vertical-align: middle;
}

.table>tfoot>tr>th, .table>tfoot>tr>td {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc),
        to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: linear-gradient(to bottom, #ebf3fc, #dce9f9);
    -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, .8) inset;
    -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, .8) inset;
    box-shadow: 0 1px 0 rgba(255, 255, 255, .8) inset;
    border-top: 1px solid #ccc;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    padding: 10px;
}

.table tbody>tr:nth-child(odd)>td, .table tbody>tr:nth-child(odd)>th {
    background-color: transparent;
}

.table-nav {
    list-style: none;
    flex: 1 0 70%;
    margin: 0;
}

.table-nav li {
    display: inline-block;
}

.table-nav li a {
    display: inline-block;
    padding: 5px 10px;
    text-decoration: none;
    font-weight: bold;
    color: #000;
}

.table-nav .table-page-current {
    border-radius: 50%;
    background-color: #FF6600;
}

.table-nav .table-page-current a {
    color: #fff;
}

table.table-details {
    text-align: left;
}

table.table-details th, table.table-details td {
    padding: 0px;
}

table.table-details>tbody>tr>th:nth-child(1), table.table-details>tbody>tr>td:nth-child(2) {
    display: inline-block !important;
    text-align: left !important;
    border-top: none;
}

table.table-details>tbody>tr>th:nth-child(1) {
    width: max-content;
    padding-right: 10px;
    width: -moz-max-content;
    width: -webkit-max-content;
    width: -o-max-content;
    width: -ms-max-content;
}
</style>
<script type="text/javascript">
	
	function processQuery() {
		var main = document.getElementById("formId");
		if(main.check2[0].checked)
	    {	
			$('#formId').attr("action","${__ctx}/CREDIT/APPLY/instalment_repayment_input");
			main.submit();
		}
		if(main.check2[1].checked)
		{
			$('#formId').attr("action","${__ctx}/INDEX/index")
			main.submit();
		}
		return false;
	} 

	function chooseOne(cb)
	{
		//先取得同name的chekcBox的集合物件
		var obj = document.getElementsByName("check2");
		for (i=0; i<obj.length; i++){
			//判斷obj集合中的i元素是否為cb，若否則表示未被點選
			if (obj[i]!=cb)	obj[i].checked = false;
			//若是 但原先未被勾選 則變成勾選；反之 則變為未勾選
			//else	obj[i].checked = cb.checked;
			//若要至少勾選一個的話，則把上面那行else拿掉，換用下面那行
			else obj[i].checked = true;
		}
		if(obj[0].checked || obj[1].checked)
		{
			var main = document.getElementById("formId");		
			if(main.check1.checked){
				main.CMSUBMIT.disabled=false;
				$('#CMSUBMIT').addClass("btn-flat-orange");
				$('#CMSUBMIT').removeClass("btn-flat-gray");
			}
			else{
			main.CMSUBMIT.disabled=true;
			$('#CMSUBMIT').addClass("btn-flat-gray");
			$('#CMSUBMIT').removeClass("btn-flat-orange");
			}
		}				
	}
	function chooseOne1(cb)
	{
		var obj = document.getElementsByName("check1");
		for (i=0; i<obj.length; i++){
		//判斷obj集合中的i元素是否為cb，若否則表示未被點選
		if (obj[i]!=cb)	obj[i].checked = false;
		//若是 但原先未被勾選 則變成勾選；反之 則變為未勾選
		//else	obj[i].checked = cb.checked;
		//若要至少勾選一個的話，則把上面那行else拿掉，換用下面那行
		else obj[i].checked = true;
		}	
		if($('#cho1').prop("checked"))
		{
			var main = document.getElementById("formId");		
			if(main.check2[0].checked||main.check2[1].checked){
				main.CMSUBMIT.disabled=false;
				$('#CMSUBMIT').addClass("btn-flat-orange");
				$('#CMSUBMIT').removeClass("btn-flat-gray");
			}
			else{
				main.CMSUBMIT.disabled=true;
				$('#CMSUBMIT').addClass("btn-flat-gray");
				$('#CMSUBMIT').removeClass("btn-flat-orange");
			}
		}
	}
	
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 長期使用循環信用申請分期還款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0194" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 	快速選單及主頁內容 -->
		 <main class="col-12">
            <section id="main-content" class="container">
				<form id="formId" method="post" action="" onSubmit="return processQuery()">
                <h2><spring:message code="LB.D0194" /><!-- 長期使用循環信用申請分期還款 --></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				
                <div class="main-content-block row">
                    <div class="col-12">
                        <div class="ttb-message">
                            <p><spring:message code="LB.D0194" /><!-- 長期使用循環信用申請分期還款約定事項 --></p>
                        </div>
                        <div id="STATPART" style="width:90%;margin: auto">
                        <ul class="ttb-result-list" style="list-style: decimal; list-style-position:outside;"><strong style="font-size:15px">
                            <li class="full-list"><spring:message code="LB.X0856" />
<!--                             	申請分期還款客戶限連續使用本行信用卡循環信用達一年以上且最近一年內於財團法人金融聯合徵信中心無帳款遲繳或信用不良紀錄。 -->
                            </li>
                            <li class="full-list"><spring:message code="LB.X0857" />
<!--                             	新利率依申請時適用之原差別利率調降。 -->
                            </li>
                            <li class="full-list"><font style="color:red"><spring:message code="LB.X0863" />
<!--                             	每期還款金額不得低於新臺幣(以下同)1,000元。 -->
                            </font></li>
                            <li class="full-list"><font style="color:red"><spring:message code="LB.X0864" />
<!--                             	分期方案一經申請恕不可取消，且不得中途更改還款期限。分期方案經核准後，本行將主動調降申請人信用卡可用額度，申請人須連續正常履行還款方案，始得向本行申請調高信用額度，並依主管機關規定辦理。 -->
                            </font></li>
                            <li class="full-list"><spring:message code="LB.X0865" />
<!--                             	分期總金額除以分期期數後之剩餘款項將併入第一期分期金額。 -->
                            </li>
                            <li class="full-list"><font style="color:red"><spring:message code="LB.X0866" />
<!--                           	  	利息計算方式：以本金按期平均攤還，各期利息按「剩餘本金」計算之方式收取。 -->
                            </font></li>
							<li class="full-list"><font style="color:red"><spring:message code="LB.X0867" />
<!-- 								「已轉換額度」與「原信用卡額度」之比率達50%以上者，新適用利率再酌減利率0.25%。 -->
							</font></li>
							<li class="full-list">
							<font style="color:red"><spring:message code="LB.X0868" /><br>
<!-- 								分期期間內如未於每月繳款截止日前付清當期最低應繳金額或遲誤繳款期限者，本行將依約收取違約金，計收方式如下： -->
								<ul style="list-style-position:outside;">
										<li>
											&emsp;(1)<spring:message code="LB.X0869" /><!-- 每月帳單未繳清金額在1,000元（含）以下者無須繳納違約金。 -->
										</li>
										<li>
											&emsp;(2)<spring:message code="LB.X0870" /><!-- 延滯第一個月，當月計付逾期手續費100元。 -->
										</li>
										<li>
											&emsp;(3)<spring:message code="LB.X0871" /><!-- 延滯第二個月，當月計付逾期手續費300元。 -->
										</li>
										<li>
											&emsp;(4)<spring:message code="LB.X0872" /><!-- 延滯第三個月，當月計付逾期手續費500元。 -->
										</li>
								</ul>
							</font>
							</li>
							<li class="full-list"><font style="color:red"><spring:message code="LB.X0873" />
<!-- 							倘分期期間內發生停卡或違反「臺灣企銀信用卡約定條款」，則須一次付清所有剩餘款項，不得再享有分期付款之優惠。 -->
							</font></li>
							<li class="full-list"><font style="color:red"><spring:message code="LB.X0874" />
<!-- 							本行得視情況決定是否查詢您的聯徵資料；並保留隨時變更/終止本服務及各項規定之權利。 -->
							</font></li>
                        </strong></ul>
                        </div>
                        <div class="ttb-message">
                            <p><spring:message code="LB.D0197" /><!-- 分期期數與利率說明如下 -->：</p>
                        </div>
						<div class="CN19-clause" style="width: 85%;margin:auto;overflow:auto;">
                        <table class="table" data-show-toggle="false">
                            <thead>
                                <tr>
                                    <th data-title="分期期數" rowspan="2"><spring:message code="LB.D0212" /><!-- 分期期數 --></th>
                                    <th data-title="差別利率" colspan="5"><spring:message code="LB.D0199" /><!-- 差別利率 --></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th></th>
									<td nowrap><spring:message code="LB.X0875" /></td><!-- 第一級 -->
                                    <td nowrap><spring:message code="LB.X0876" /></td><!-- 第二級 -->
                                    <td nowrap><spring:message code="LB.X0877" /></td><!-- 第三級 -->
                                    <td nowrap><spring:message code="LB.X0878" /></td><!-- 第四級 -->
                                    <td nowrap><spring:message code="LB.X0879" /></td><!-- 第五級 -->
                                </tr>
								 <tr data-expanded="false">
                                    <td class="expand">6<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />1.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />1.25%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">12<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />1%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />1%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">18<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.75%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.75%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.75%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.75%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">24<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.5%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.5%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.5%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.5%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">30<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.25%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">36<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.2%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.2%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.2%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.2%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">48<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.15%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.15%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.15%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.2%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">60<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.05%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.05%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.05%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.4%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
						</div>
					<div class="ttb-input">
						<span><spring:message code="LB.D0200" /><!-- 上述約定事項本人已於合理期間(至少五日)審閱並充分了解，且願意遵守。-->
							<label class="check-block">
										<spring:message code="LB.Confirm" /><!-- 確認 -->
								<input type="checkbox"  class="authcheck1" id="cho1" name="check1" value="1" onClick="chooseOne1(this)"/>
								<span class="ttb-check"></span>
							</label>
							<span class="input-unit"></span>
						</span>
						<br><br>				
										
					 <span><spring:message code="LB.D0096" /><!-- 本人-->&nbsp;&nbsp;&nbsp;
					 	<label class="check-block">
										<spring:message code="LB.D0097" /><!-- 同意 -->
								<input type="checkbox"  class="authcheck1" id="cho1" name="check2" value="1" onClick="chooseOne(this)"/>
								<span class="ttb-check"></span>
							</label>
							<span class="input-unit"></span>
						<label class="check-block">
								<span><spring:message code="LB.D0041" /><!-- 不同意 --></span>
								<input type="checkbox"  class="authcheck1" id="cho1" name="check2" value="2" onClick="chooseOne(this)"/>
								<span class="ttb-check"></span>
							</label>
							<span class="input-unit"></span>
						<spring:message code="LB.D0202_4" />(<spring:message code="LB.D0202_5" />)<!-- 貴行得視情況調閱聯徵中心資料及調整信用額度(如勾選不同意將無法申請本項服務) --></span>
					</div>


					<input class="ttb-button btn-flat-gray"  type="submit" value="<spring:message code="LB.X0080" />" id="CMSUBMIT" name="CMSUBMIT" disabled><!-- 下一步 -->
                    </div>
                </div>
                </form>
            </section>
        </main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>