<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	<label><spring:message code="LB.Inquiry_time" />：</label><label>${CMQTIME}</label>
	<br/><br/>
	<label><spring:message code="LB.Total_records" />：</label><label>${CMRECNUM} <spring:message code="LB.Rows" /></label>
	<br/><br/>
	<table class="print">
      	<tr>
<!-- 交易種類 -->
			<th><spring:message code= "LB.W1060" /></th>
			<!-- 預約黃金買進 -->
			<th class="text-left" colspan="4"><spring:message code= "LB.X0949" /></th>
        </tr>
		<tr>
		<!-- 預約日期 -->
			<th><spring:message code= "LB.X0377" /></th>      
<!-- 臺幣轉出帳號 -->
	      	<th><spring:message code= "LB.W1496" /></th>
<!-- 黃金轉入帳號 -->
	      	<th><spring:message code= "LB.W1497" /></th>
<!-- 買進公克數 -->
	      	<th><spring:message code= "LB.W1498" /></th>
<!-- 狀態 -->
	      	<th><spring:message code= "LB.D0272" /></th>
		</tr>
       	<c:if test="${ COUNT_APPTYPE_BUY > 0 }">
			<c:forEach items="${print_datalistmap_data}" var="labelListMap" varStatus="index">
	           	<c:if test="${ labelListMap.APPTYPE.equals('01') }">
					<tr>
						<td>${labelListMap.APPDATE_str}</td>
						<td>${labelListMap.ACN2}</td>
						<td>${labelListMap.ACN}</td>
						<td style="text-align: right">${labelListMap.TRNGD_str}</td>
						<td>${labelListMap.STATUS}</td>
					</tr>
				</c:if>
			</c:forEach>
		</c:if>
       	<c:if test="${ COUNT_APPTYPE_BUY <= 0 }">
       		<tr>
<!-- 查無資料 -->
       			<td colspan="5"><spring:message code= "LB.D0017" /></td>
       		</tr>
		</c:if>
	</table>
	<br/><br/>
		<table class="print">
      	<tr>
<!-- 交易種類 -->
			<th><spring:message code= "LB.W1060" /></th>
<!-- 預約黃金回售 -->
			<th class="text-left" colspan="4"><spring:message code= "LB.X0952" /></th>
        </tr>
		<tr>
<!-- 預約日期 -->
	      	<th><spring:message code= "LB.X0377" /></th>      
<!-- 黃金轉出帳號 -->
	      	<th><spring:message code= "LB.W1515" /></th>
<!-- 臺幣轉入帳號 -->
	      	<th><spring:message code= "LB.W1518" /></th>
<!-- 賣出公克數 -->
	      	<th><spring:message code= "LB.W1519" /></th>
<!-- 狀態 -->
	      	<th><spring:message code= "LB.D0272" /></th>
		</tr>
       	<c:if test="${ COUNT_APPTYPE_SELL > 0 }">
			<c:forEach items="${print_datalistmap_data}" var="labelListMap" varStatus="index">
	           	<c:if test="${ labelListMap.APPTYPE.equals('02') }">
					<tr>
						<td>${labelListMap.APPDATE_str}</td>
						<td>${labelListMap.ACN}</td>
						<td>${labelListMap.ACN2}</td>
						<td style="text-align: right">${labelListMap.TRNGD_str}</td>
						<td>${labelListMap.STATUS}</td>
					</tr>
				</c:if>
			</c:forEach>
		</c:if>
       	<c:if test="${ COUNT_APPTYPE_SELL <= 0 }">
       		<tr>
<!-- 查無資料 -->
       			<td colspan="5"><spring:message code= "LB.D0017" /></td>
       		</tr>
		</c:if>
	</table>
	<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
             <li><spring:message code="LB.Gold_Reservation_Query_P1_D1"/></li>
			<li><font color=red><spring:message code="LB.Gold_Reservation_Query_P1_D2"/></font></li>
		</ol>
	</div>
</body>
</html>