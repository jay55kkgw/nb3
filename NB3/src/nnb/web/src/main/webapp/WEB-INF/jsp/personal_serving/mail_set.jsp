<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<!-- 元件驗證身分JS -->
		 <script type="text/javascript">
	    $(document).ready(function() {
	    	
	    	//HTML載入完成後開始遮罩
	    	setTimeout("initBlockUI()",10);
	    	//解遮罩
	    	setTimeout("unBlockUI(initBlockId)",500);
	    	$("#CMSUBMIT").click(function(e){
	    		
	    		//email是否為行員ID之驗證
				if($("#ISCUSIDN").val()=="N"){  //是否為行員ID
					$('#NEW_EMAIL').addClass("validate[funcCallRequired[validate_TmailNotEmp[NEW_EMAIL]]]");
				}else{
					$('#NEW_EMAIL').removeClass("validate[funcCallRequired[TmailNotEmp[NEW_EMAIL]]]");
				}
	    		
	    		console.log("submit~~");
	    		
	    		$("#formId").validationEngine({
	    			binded:false,
	    			promptPosition: "inline"
	    		});
				
				e = e || window.event;
				
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
					}
				else{
					$("#formId").validationEngine('detach');
					var new_email = $('#NEW_EMAIL').val();
					var dpuserid = $('#DPUSERID').val();
					//ajax 打N930 查詢有沒有重覆EMAIL
					var uri = "${__ctx}/VERIFYMAIL/N930_check_double_aj";
					var rdata = { NEW_EMAIL : new_email , DPUSERID : dpuserid};
					data = fstop.getServerDataEx(uri,rdata,false);
					if(data !=null && data.result != true ){
						showError();
					}else{
						var action = '${__ctx}/PERSONAL/SERVING/mailChg_comfirm';
						$("#formId").attr("action", action);
						$('#formId').submit();
					}
					
	 			}
		    });
		});
	    
	    $(document).keydown(function(event){
	    	if(event.keyCode == 27) {
	    		if(!$('#errorBtn2').is(":hidden") && !wait1){
	    			wait1 = true;
	    			event.preventDefault();
	    			$('#error-block').hide();
	    		}
	    		if($('#errorBtn2').is(":hidden")){
	    			$('#error-block').hide();
	    		}
	       	}
	    	
		});
	    
		function goNotificationService(){
			console.log("go Notification Service")
			$('#formId').attr("action","${__ctx}"+"/PERSONAL/SERVING/notification_service");
			$('#formId').submit();
		}
		
		function showError(){
			//這邊代表有重覆
			errorBlock(
					["<spring:message code= 'LB.X2477' />"], 
					["<spring:message code= 'LB.X2614' /><br><br><spring:message code= 'LB.X2615' /><br><br><spring:message code= 'LB.X2616' />"],
					null, 
					'<spring:message code= "LB.X2617" />', 
					'<spring:message code= "LB.X2618" />'
			);
			// 複寫errorBtn1 事件
			$("#errorBtn1").click(function(e) {
				$('#error-block').hide();
			});
// 			// 複寫errorBtn2 事件
			$("#errorBtn2").click(function(e) {
				var action = '${__ctx}/VERIFYMAIL/statement_page';
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
		}
	 </script>
</head>
<body>
	
	<!--   IDGATE --> 		 
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>

	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- Email設定     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Email_Setting" /></li>
    <!-- 我的Email設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.My_Email_setting" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	

		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.My_Email_setting" /><!-- 我的 Email 設定 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" action="${__ctx}/PERSONAL/SERVING/mailChg" method="post" autocomplete="off">
			<input type="hidden" name="notifyData"  value='${orgInfo.data.notify}'>
			<input type="hidden" id="DPUSERID" name="DPUSERID"  value='${orgInfo.data.UID}'>
			<input type="hidden" name="ISCUSIDN" id="ISCUSIDN" value="${orgInfo.data.ISCUSIDN}">
			
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
						<!-- 主頁內容  -->
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.System_time" />：</h4></label></span> <!-- 系統時間 -->
								<span class="input-block">
									<div class="ttb-input">
										<p>${orgInfo.data.CMQTIME}</p>
										<input type="hidden" name="SYSTIME" value="${orgInfo.data.CMQTIME}" readonly="readonly"/>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.Current_Email" />：</h4></label></span> <!-- 目前電子郵箱 -->
								<span class="input-block">
									<div class="ttb-input">
										<c:if test="${orgInfo.data.DPMYEMAIL eq ''}">
											<p><spring:message code="LB.None" /></p>
										</c:if>
										<p>${orgInfo.data.DPMYEMAIL}</p>
										<input id="OLD_EMAIL" name="OLDEMAIL" type="hidden" value="${orgInfo.data.DPMYEMAIL}">
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.New_Email" />：</h4></label></span> <!-- 新的電子郵箱 -->
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" id="NEW_EMAIL" name="NEW_EMAIL" class="text-input validate[required,newcolumn[MAIL,OLD_EMAIL,NEW_EMAIL],funcCall[validate_EmailCheck[NEW_EMAIL]]]" autocomplete="off" >
									</div>
									<p><spring:message code= "LB.X2619" /></p>
								</span>
							</div>
					</div>
					<input type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />"/>
								<!-- 重新輸入 -->
					<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
								<!-- 確定 -->
				</div>
				</div>
    <ol class="description-list list-decimal">
		<p><spring:message code="LB.Description_of_page" /></p>
		<li><span><spring:message code="LB.Mail_set_P2_D1" /></span><u><input onClick="goNotificationService();" class="ttb-sm-btn btn-flat-orange" type="button" value='<spring:message code="LB.Notification_Service" />' name="CMSUBMIT1"></u></li>
		<li><span><spring:message code="LB.Mail_set_P2_D2" /></span></li>
		<li><span><spring:message code="LB.Mail_set_P2_D3" /></span></li>
		<li><span><spring:message code="LB.Mail_set_P2_D4" /></span></li>
		<li><span><font color="red"><spring:message code="LB.Mail_set_P2_D5" /></font></span></li>
	</ol>
  </div>
			</form>
		</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>