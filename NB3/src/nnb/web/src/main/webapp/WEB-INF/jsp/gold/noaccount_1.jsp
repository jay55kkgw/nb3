<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

	<script type="text/javascript">
		$(document).ready(function () {
			init();
		});
		function init() {
			$("#CMSUBMIT").click(function (e) {
				$("#formId").attr("action","${__ctx}/${bsdata.data.NCdata.NC_forward}");
				$("#formId").submit();
					});
			$("#CANCEL").click(function(){
				$("#formId").attr("action","${__ctx}/INDEX/index");
				$("#formId").submit();
			});
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 定期定額交易     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1552" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="${bsdata.data.NCdata.NC_title}" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<!-- 帳號 -->
								<div class="ttb-input-item row">
										<div style="color:red;" class="ttb-input mx-auto">
											<spring:message code="${bsdata.data.NCdata.NC_msg}"/>
										</div>
								</div>
							</div>
							<!-- 取消-->
							<spring:message code="LB.Cancel" var="cancel"></spring:message>
							<input type="button" class="ttb-button btn-flat-gray" name="CANCEL" id="CANCEL" value="${cancel}"/>
							<!-- 確定-->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="${bsdata.data.NCdata.NC_forwardmsg}"/>" />
							
						</div>
					</div>
				</form>
			</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>