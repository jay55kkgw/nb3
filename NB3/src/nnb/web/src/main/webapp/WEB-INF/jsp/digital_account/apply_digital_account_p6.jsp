<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdentityData.js"></script>
	
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
	<script type="text/javascript">
		var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
		//var myobj = null; // 自然人憑證用
		var urihost = "${__ctx}";
		
		var width = $(window).width();
		$(window).resize(function() {
			width = $(this).width();
			changeDisplay();
		});

		var isTimeout = "${not empty sessionScope.timeout}";
		var countdownObjheader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
		var countdownSecHeader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
		
        $(document).ready(function () {
			// HTML載入完成後0.1秒開始遮罩
			setTimeout("initBlockUI()", 100);

			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			
			setTimeout("init()", 20);
			
			// 過0.5秒解遮罩
			setTimeout("unBlockUI(initBlockId)", 800);

			timeLogout();
        });
//         var isconfirm_a = false;
//     	$("#errorBtn1").click(function(){
//     		if(!isconfirm_a)
//     			return false;
//     		isconfirm_a = false;
//     		$("#naturalComponent")[0].click();
//     	});
//     	$("#errorBtn2").click(function(){
//     		isconfirm_a = false;
//     		$('#error-block').hide();
//     	});
    	// 初始化自然人元件
//     	function initNatural() {
//     		var Brow1 = new ckBrowser1();
//     		if(!Brow1.isIE){	
//     			if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {
//     				myobj = initCapiAgentForChrome();
//     				try {
//     					if(sessionToken == null){
//     						myobj.initChrome();
//     						console.log("myobj.initChrome...");
//     					}
//     				} catch (e) {
// //     					if(confirm("<spring:message code= "LB.X1216" />")){
// //     						$("#naturalComponent")[0].click();
// //     					}
//     					isconfirm = true;
//  						errorBlock(
//  								null, 
//  								null,
//  								['<spring:message code= "LB.X1216" />'], 
//  								'<spring:message code= "LB.Confirm" />', 
//  								'<spring:message code= "LB.Cancel" />'
//  							);
//     				}			
//     			}
//     		} else {
//     			var strObject = "";
//     	    	// 使用 Script 判斷 win64 或 win32, 執行時間有點久
//     	 		if (navigator.platform.toLowerCase() == "win64"){
//     				// "Windows 64 位元";
//     	 			document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATLx64.cab" width="0px" height="0px"></object>';
//     			} else {
//     				// "Windows 32 位元 或 模擬 64 位元(WOW64)";
//     				document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATL.cab" width="0px" height="0px"></object>';
//     			}
//     		    myobj = document.myobj;
//     		}
//     		console.log("initNatural.finish...");
//     	}
    	
		/**
		 * 初始化BlockUI
		 */
		function initBlockUI() {
			initBlockId = blockUI();
		}

		// 交易機制選項
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
		
			switch(fgtxway) {
				case '0':
//						alert("交易密碼(SSL)...");
	    			$("#formId").submit();
					break;
					
				case '1':
//						alert("IKey...");
					var jsondc = $("#jsondc").val();
					
					// 遮罩後不給捲動
//						document.body.style.overflow = "hidden";

					// 呼叫IKEY元件
//						uiSignForPKCS7(jsondc);
					
					// 解遮罩後給捲動
//						document.body.style.overflow = 'auto';
					
					useIKey();
					break;
					
				case '2':
//						alert("晶片金融卡");

					// 遮罩後不給捲動
//						document.body.style.overflow = "hidden";
					
					// 呼叫讀卡機元件
					listReaders();

					// 解遮罩後給捲動
//						document.body.style.overflow = 'auto';
					
			    	break;

				case '4':
//					自然人憑證
					processQuery2();
			    	break;
				case '5':
					//跨行帳戶驗證
					initBlockUI();
					$("#formId").submit();
					break;
				case '6':
					//跨行帳戶驗證
					initBlockUI();
					$("#CHIP_ACN").val($("#LOACN").val());
					$("#formId").submit();
					break;
			    	
				default:
					alert("nothing...");
			}
			
		}
		
		function changeDisplay(){
			<%-- 避免小版跑版 --%>
			if(width < 900){
				$("#bigBlock1").css("display","inline-block");
				$("#bigBlock2").css("display","inline-block");
// 				$("#hideBlock").hide();
			}
			else{
				$("#bigBlock1").css("display","flex");
				$("#bigBlock2").css("display","flex");
// 				$("#hideBlock").show();
			}
		}
		
        function init() {
	    	$("#CMSUBMIT").click(function(e){
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				var capData = $("#formId").serializeArray();
				var capResult = fstop.getServerDataEx(capUri, capData, false);
				if (capResult.result) {
					if (!$('#formId').validationEngine('validate')) {
						e.preventDefault();
					} else {
						$("#formId").validationEngine('detach');
	// 		        	initBlockUI();
			        	var action = '${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_result';
			        	$("#formId").attr("action", action);
		    			unBlockUI(initBlockId);
		    			processQuery();
					}
				} else {
					//alert("<spring:message code= "LB.X1082" />");
					
						errorBlock(
								null, 
								null,
								["<spring:message code= "LB.X1082" />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
					changeCode();
				}
			});
			$("#CMBACK").click(function(e){
	        	var action = '${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_p4';
	        	$("#formId").attr("action", action);
				$("#back").val('Y');
				$("#formId").submit();
			});
			
			changeDisplay();

	    	//列印
        	$("#CMPRINT").click(function(){
				var params = {
					"jspTemplateName":"apply_digital_account_p6_print",
					"jspTitle":'<spring:message code= "LB.D1100" />',
					"NAME":"${input_data.data.NAME}",
					"CUSNAME":"${input_data.data.CUSNAME}",
					"CUSIDN":"${input_data.data.CUSIDN}",
					"COUNTRY":"${input_data.data.COUNTRY}",
					"CCBIRTHDATEYY":"${input_data.data.CCBIRTHDATEYY}",
					"CCBIRTHDATEMM":"${input_data.data.CCBIRTHDATEMM}",
					"CCBIRTHDATEDD":"${input_data.data.CCBIRTHDATEDD}",
					"ID_CHGE":"${input_data.data.ID_CHGE}",
					"CHGE_DT":"${input_data.data.CHGE_DT}",
					"CHGE_CYNAME":"${input_data.data.CHGE_CYNAME}",
					"HAS_PIC":"${input_data.data.HAS_PIC}",
					"POSTCOD1":"${input_data.data.POSTCOD1} ",
					"PMTADR":"${input_data.data.PMTADR}",
					"POSTCOD2":"${input_data.data.POSTCOD2} ",
					"CTTADR":"${input_data.data.CTTADR}",
					"HOMEZIP":"${input_data.data.HOMEZIP}",
					"HOMETEL":"${input_data.data.HOMETEL}",
					"BUSZIP":"${input_data.data.BUSZIP}",
					"BUSTEL":"${input_data.data.BUSTEL}",
					"CELPHONE":"${input_data.data.CELPHONE}",
					"ARACOD3":"${input_data.data.ARACOD3}",
					"FAX":"${input_data.data.FAX}",
					"CAREER1_str":"${input_data.data.CAREER1_str}",
					"CAREER2":"${input_data.data.CAREER2}",
					"EMPLOYER":"${input_data.data.EMPLOYER}",
					"BRHCOD":"${input_data.data.BRHCOD} ",
					"BRHNAME":"${input_data.data.BRHNAME}",
					"PURPOSE":"${input_data.data.PURPOSE}",
					"PREASON":"${input_data.data.PREASON}",
					"BDEALING_str":"${input_data.data.BDEALING_str}",
					"MAINFUND":"${input_data.data.MAINFUND}",
					"MREASON":"${input_data.data.MREASON}",
					"MAILADDR":"${input_data.data.MAILADDR}",
					"DEGREE":"${input_data.data.DEGREE}",
					"SALARY":"${input_data.data.SALARY}",
					"MARRY":"${input_data.data.MARRY}",
					"CHILD":"${input_data.data.CHILD}",
					"S_MONEY":"${input_data.data.S_MONEY}",
					"FILE1":"${input_data.data.FILE1}",
					"FILE2":"${input_data.data.FILE2}",
					"FILE3":"${input_data.data.FILE3}",
					"FILE4":"${input_data.data.FILE4}",
					"USERNAME":"${input_data.data.USERNAME}",
					"LOGINPIN":"${input_data.data.LOGINPIN}",
					"TRANSPIN":"${input_data.data.TRANSPIN}",
					"TRFLAG":"${input_data.data.TRFLAG}",
					"EBILLFLAG":"${input_data.data.EBILLFLAG}",
					"CARDAPPLY":"${input_data.data.CARDAPPLY}",
					"SNDFLG":"${input_data.data.SNDFLG}",
					"CRDTYP":"${input_data.data.CRDTYP}",
					"NAATAPPLY":"${input_data.data.NAATAPPLY}",
					"CChargeApply":"${input_data.data.CChargeApply}",
					"LMT":"${input_data.data.LMT}",
					"CROSSAPPLY":"${input_data.data.CROSSAPPLY}",
					"BIRTHDAY":"${input_data.data.BIRTHDAY}"
				};
				openWindowWithPost("${__ctx}/ONLINE/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
    		// 初始化自然人元件
//     		initNatural();
    		showNature();
    		
			//img值初始化
			if("${input_data.data.FILE1}" != ""){
            	initImage("${input_data.data.FILE1}", '#img1');
			}
			if("${input_data.data.FILE2}" != ""){
            	initImage("${input_data.data.FILE2}", '#img2');
			}
			if("${input_data.data.FILE3}" != ""){
            	initImage("${input_data.data.FILE3}", '#img3');
			}
			if("${input_data.data.FILE4}" != ""){
            	initImage("${input_data.data.FILE4}", '#img4');
			}
        }

		function ValidateValue(textbox) {
			var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
			var textboxvalue = textbox.value;
			var index = textboxvalue.length - 1;
			var s = textbox.value.charAt(index);
			if (IllegalString.indexOf(s) >= 0) {
				s = textboxvalue.substring(0, index);
				textbox.value = s;
			}
		}
		
		// 刷新驗證碼
		function refreshCapCode() {
			console.log("refreshCapCode...");

			// 驗證碼
			$('input[name="capCode"]').val('');
			
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 刷新輸入欄位
		function changeCode() {
			console.log("changeCode...");
			
			// 清空輸入欄位
			$('input[name="capCode"]').val('');
			
			// 刷新驗證碼
			refreshCapCode();
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				refreshCapCode();
			});
		}
		//取得卡片主帳號結束
// 		function getMainAccountFinish(result){
// 			//成功
// 			if(result != "false"){
// 				var cardACN = result;
// 				if(cardACN.length > 11){
// 					cardACN = cardACN.substr(cardACN.length - 11);
// 				}
// 				$("#OUTACN").val(cardACN);
// 				$("#CHIP_ACN").val(cardACN);
// 				$("#ACNNO").val(cardACN);
// 				var UID = $("#CUSIDN").val();
// //				var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
// //				var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
				
// 				var uri = urihost+"/COMPONENT/component_without_id_aj";
// 				var rdata = { ACN: cardACN ,UID: UID };
// 				fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
// 			}
// 			//失敗
// 			else{
// 				showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",false);
// 			}
// 		}
		function CheckIdProcess()
		{
			showTempMessage(500,"晶片金融卡身份查驗","<br><p>晶片金融卡身份查驗中，請稍候...</p><br><p>查驗期間請勿移除您的晶片金融卡，以免造成驗證錯誤。</p><br><br>","MaskArea",true);
			var formId = document.getElementById("formId");
			var cardACN = formId.ACNNO.value;
			if(cardACN.length > 11){
				cardACN = cardACN.substr(cardACN.length - 11);
			}
			$("#OUTACN").val(cardACN);
			$("#CHIP_ACN").val(cardACN);
			$("#ACNNO").val(cardACN);
			var UID = $("#CUSIDN").val();
			console.log("testetstestetsetstetsetsetes>>>>"+cardACN);
			var uri = urihost+"/COMPONENT/component_without_id_aj";
			var rdata = { ACN: cardACN ,UID: UID };
			fstop.getServerDataEx(uri, rdata, true, CheckResult);
		}
		

		function CheckResult(data) {
			console.log("data: " + data);
			if (data) {
				// login_aj回傳資料
				console.log("data.json: " + JSON.stringify(data));
				// 成功
				if("0" == data.msgCode) {
					var formId = document.getElementById("formId");
					formId.submit();
					return false;
				} else {
					showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
					try{
						if(data.data.msgCode == "0"){
					        errorBlock(
								null,
								null,
								["非本人帳戶之晶片金融卡，無法進行此功能"], 
								'確定', 
								null
							);
						}else{
					        errorBlock(
									null,
									null,
									[ data.data.msgCode + " " + data.data.msgName ], 
									'確定', 
									null
								);
						}
					}catch (e) {
				        errorBlock(
							null,
							null,
							["非本人帳戶之晶片金融卡，無法進行此功能"], 
							'確定', 
							null
						);
					}
				}
			}
		}
        function initImage(fileName, imgFilter){
            $.ajax({
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_image",
                type: "POST",
                data: {
                	"filename": fileName
                },
		        dataType: "json",
                success: function (res) {
                	console.log(res);
                 if(res.data.validated){
 					 $(imgFilter).attr('src',res.data.picFile);
                     $(imgFilter+"_close_btn").show();
                 } else {
					$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
                     $(imgFilter+"_close_btn").hide();
                 }
                }
            });
        	
        }
        function showNature() {
        	var fgtxway = $('input[name="FGTXWAY"]:checked').val();
   			if (fgtxway == '2') {
   				// 晶片金融卡
   				component_init();
   			}
   			if (fgtxway == '4') {
   				// 自然人憑證
   				initBlockUI(); 
   				setTimeout("initNatural_EXCUTE()", 50);
   			}
    	}
		function timeLogout(){
			// 刷新session
			var uri = '${__ctx}/login_refresh';
			console.log('refresh.uri: '+uri);
			var result = fstop.getServerDataEx( uri, null, false, null);
			console.log('refresh.result: '+JSON.stringify(result));
			// 初始化登出時間
			$("#countdownheader").html(parseInt(countdownSecHeader)+1);
			$("#countdownMin").html("");
			$("#mobile-countdownMin").html("");
			$("#countdownSec").html("");
			$("#mobile-countdownSec").html("");
			// 倒數
			startIntervalHeader(1, refreshCountdownHeader, []);
		}

		function refreshCountdownHeader(){
			// timeout剩餘時間
			var nextSec = parseInt($("#countdownheader").html()) - 1;
			$("#countdownheader").html(nextSec);

			// 提示訊息--即將登出，是否繼續使用
			if(nextSec == 120){
				initLogoutBlockUI();
			}
			// timeout
			if(nextSec == 0){
				// logout
				fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/timeout_logout');
			}
			if (nextSec >= 0){
				// 倒數時間以分秒顯示
				var minutes = Math.floor(nextSec / 60);
				$("#countdownMin").html(('0' + minutes).slice(-2));
				$("#mobile-countdownMin").html(('0' + minutes).slice(-2));

				var seconds = nextSec - minutes * 60;
				$("#countdownSec").html(('0' + seconds).slice(-2));
				$("#mobile-countdownSec").html(('0' + seconds).slice(-2));
			}
		}
		function startIntervalHeader(interval, func, values){
			clearInterval(countdownObjheader);
			countdownObjheader = setRepeater(func, values, interval);
		}
		function setRepeater(func, values, interval){
			return setInterval(function(){
				func.apply(this, values);
			}, interval * 1000);
		}
		/**
		 * 初始化logoutBlockUI
		 */
		function initLogoutBlockUI() {
			logoutblockUI();
		}

		/**
		 * 畫面BLOCK
		 */
		function logoutblockUI(timeout){
			$("#logout-block").show();

			// 遮罩後不給捲動
			document.body.style.overflow = "hidden";

			var defaultTimeout = 60000;
			if(timeout){
				defaultTimeout = timeout;
			}
		}

		/**
		 * 畫面UNBLOCK
		 */
		function unLogoutBlockUI(timeoutID){
			if(timeoutID){
				clearTimeout(timeoutID);
			}
			$("#logout-block").hide();

			// 解遮罩後給捲動
			document.body.style.overflow = 'auto';
		}
		/**
		 *繼續使用
		 */
		function keepLogin(){
			unLogoutBlockUI(); // 解遮罩
			timeLogout(); // 刷新倒數計時
		}
    </script>
		<style>
			@media screen and (max-width:767px) {
				.ttb-button {
						width: 38%;
						left: 36px;
				}
				#CMBACK.ttb-button{
						border-color: transparent;
						box-shadow: none;
						color: #333;
						background-color: #fff;
				}
	 
	
			}
		</style>
</head>
<body>
	<div id="obj"></div>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component_u2.jsp"%>
	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上開立數位存款帳戶     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1361" /></li>
		</ol>
	</nav>

	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
<!-- 					線上開立數位存款帳戶 -->
					<spring:message code="LB.D1361"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <div id="step-bar">
                    <ul>
                        <li class="finished">注意事項與權益</li>
                        <li class="finished">開戶認證</li>
                        <li class="finished">開戶資料</li>
                        <li class="active">確認資料</li>
                        <li class="">完成申請</li>
                    </ul>
                </div>
                <form id="formId" method="post">
                    <!-- ikey -->
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					<input type="hidden" id="jsondc" name="jsondc" value='${input_data.data.jsondc }'>
					<input type="hidden" name="CMTRANPAGE" value="1">
					<input type="hidden" id="CUSIDN" name="CUSIDN" value="${input_data.data.CUSIDN}">
					<input type="hidden" id="HASRISK" name="HASRISK" value="${input_data.data.HASRISK}">
					<input type="hidden" id="IP" name="IP" value="${input_data.data.IP}">
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="OUTACN" name="OUTACN" value="">
					<input type="hidden" id="AUTHCODE" name="AUTHCODE" value="">
					<input type="hidden" id="AUTHCODE1" name="AUTHCODE1" value="">
					
					<input type="hidden" id="ACN" name="ACN" value="">		
					<input type="hidden" id="CHIP_ACN" name="CHIP_ACN" value="">
					<input type="hidden" id="CertFinger" name="CertFinger" value="">
					<input type="hidden" id="CertB64" name="CertB64" value="">
					<input type="hidden" id="CertSerial" name="CertSerial" value="">
					<input type="hidden" id="CertSubject" name="CertSubject" value="">
					<input type="hidden" id="CertIssuer" name="CertIssuer" value="">
					<input type="hidden" id="CertNotBefore" name="CertNotBefore" value="">
					<input type="hidden" id="CertNotAfter" name="CertNotAfter" value="">
					<input type="hidden" id="HiCertType" name="HiCertType" value="">
					<input type="hidden" id="CUSIDN4" name="CUSIDN4" value="">
					<input type="hidden" id="NOTIEINSTALL" name="NOTIEINSTALL" value="">
					<input type="hidden" name="back" id="back" value=>	
					
					<input type="hidden" id="CITYCHA" name="CITYCHA" value="${input_data.data.CITYCHA}">
					<input type="hidden" id="CMDATE2" name="CMDATE2" value="${input_data.data.CMDATE2}">
					<input type="hidden" id="DIGVERSION" name="DIGVERSION" value="${input_data.data.DIGVERSION}">
					<input type="hidden" id="ACNVERSION" name="ACNVERSION" value="${input_data.data.ACNVERSION}">
					<input type="hidden" name="outside_source" id="outside_source" value="${ outside_source }">	
					<input type="hidden" name="LOACN" id="LOACN" value="${ input_data.data.LOACN }">	
						
					<input type="hidden" id="TOKEN" name="TOKEN" value="${input_data.data.TOKEN}" /><!-- 防止重複交易 -->
					<!-- timeout -->
					<section id="id-and-fast">
						<span class="id-name">
							<spring:message code="LB.Welcome_1"/>&nbsp;
							<c:if test="${empty input_data.data.CUSNAME}">
								<span id="username" name="username_show"><spring:message code="LB.X2190"/></span>
							</c:if>
							<c:if test="${not empty input_data.data.CUSNAME}">
								<span id="username" name="username_show">${input_data.data.CUSNAME}</span>
							</c:if>
							&nbsp;<spring:message code="LB.Welcome_2"/>
						</span>
						<div id="id-block">
							<div>
							<span class="id-time">
								<fmt:setLocale value="${pageContext.response.locale}"/>
								<c:if test="${!empty sessionScope.logindt && !empty sessionScope.logintm}">
									<fmt:parseDate var="parseDate"
												   value="${sessionScope.logindt} ${sessionScope.logintm}"
												   pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:formatDate value="${parseDate}" dateStyle="full"
													type="both"/>&nbsp;<spring:message code="LB.X2250"/>
									<br/>
								</c:if>

								<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full"/>
								&nbsp;<spring:message code="LB.Login_successful"/>
							</span>
								<!-- 自動登出剩餘時間 -->
								<span class="id-time"><spring:message code="LB.X1912"/>
								<span class="high-light ml-1">
									<!-- 分 -->
									<font id="countdownMin"></font>
									:
									<!-- 秒 -->
									<font id="countdownSec"></font>
								</span>
							</span>
							</div>
							<button type="button" class="btn-flat-orange" onclick="timeLogout()"><spring:message
									code="LB.X1913"/></button>
							<button type="button" class="btn-flat-darkgray"
									onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')"><spring:message
									code="LB.Logout"/></button>
						</div>
					</section>
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
	                            <div class="ttb-message">
                                	<p>確認資料</p>
	                            </div>
								<p class="form-description">請再次確認您填寫的資料</p>
								<!-- ***************************************** -->
	                            <div class="classification-block">
	                                <p>基本資料</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- ***************************************** -->
								<!-- 中文戶名 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1105"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.CUSNAME}
											<input type="hidden" name="CUSNAME" value="${input_data.data.CUSNAME}">
											<input type="hidden" name="RENAME" value="${input_data.data.RENAME}">
										</div>
									</span>
								</div>
								<!-- 身分證統一編號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0519"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.CUSIDN}
											<input type="hidden" name="UID" id="UID" value="${input_data.data.CUSIDN}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 國籍 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.X0205"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.COUNTRY}
											<input type="hidden" name="COUNTRY" value="${input_data.data.COUNTRY}">
										</div>
									</span>
								</div>
								<!-- 出生日期 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0582"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<!-- 											民國 -->
											<spring:message code="LB.D0583"/>&nbsp;${input_data.data.BIRTHDAY.substring(0,3)}<spring:message code="LB.Year"/>&nbsp;${input_data.data.BIRTHDAY.substring(3,5)}<spring:message code="LB.Month"/>&nbsp;${input_data.data.BIRTHDAY.substring(5,7)}<spring:message code="LB.D0586"/>&nbsp;
											<input type="hidden" name="BIRTHDAY" value="${input_data.data.BIRTHDAY}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 身分證發證資訊 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											身分證發證資訊
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.D0583"/>&nbsp;${input_data.data.CHGE_DT.substring(0,3)}<spring:message code="LB.Year"/>&nbsp;${input_data.data.CHGE_DT.substring(3,5)}<spring:message code="LB.Month"/>&nbsp;${input_data.data.CHGE_DT.substring(5,7)}<spring:message code="LB.D0586"/>&nbsp;
											(${input_data.data.CHGE_CYNAME})
											<c:if test="${ input_data.data.ID_CHGE.equals('1') }">
<!-- 												初發 -->
												<spring:message code="LB.B0002"/>
											</c:if>
											<c:if test="${ input_data.data.ID_CHGE.equals('3') }">
<!-- 												換發 -->
												<spring:message code="LB.W1664"/>
											</c:if>
											<c:if test="${ input_data.data.ID_CHGE.equals('2') }">
<!-- 												補發 -->
												<spring:message code="LB.W1665"/>
											</c:if>
											<input type="hidden" name="ID_CHGE" value="${input_data.data.ID_CHGE}">
											<input type="hidden" name="CHGE_DT" value="${input_data.data.CHGE_DT}">
											<input type="hidden" name="CHGE_CY" value="${input_data.data.CHGE_CY}">
											<input type="hidden" name="HAS_PIC" value="${input_data.data.HAS_PIC}">
										</div>
									</span>
								</div>
								<!-- ********** -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<!-- 										身分證有無相片 -->
<%-- 											<spring:message code="LB.D1115"/> --%>
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<%-- 											<c:if test="${ input_data.data.HAS_PIC.equals('Y') }"> --%>
<%-- 												<spring:message code="LB.D1070_1"/> --%>
<%-- 											</c:if> --%>
<%-- 											<c:if test="${ input_data.data.HAS_PIC.equals('N') }"> --%>
<%-- 												<spring:message code="LB.D1070_2"/> --%>
<%-- 											</c:if> --%>
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
								<!--學歷 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1136"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.DEGREE.equals('1') }">
<!-- 												博士 -->
												<spring:message code="LB.D0588"/>
											</c:if>
											<c:if test="${ input_data.data.DEGREE.equals('2') }">
<!-- 												碩士 -->
												<spring:message code="LB.D0589"/>
											</c:if>
											<c:if test="${ input_data.data.DEGREE.equals('3') }">
<!-- 												大學 -->
												<spring:message code="LB.D0590"/>
											</c:if>
											<c:if test="${ input_data.data.DEGREE.equals('4') }">
<!-- 												專科 -->
												<spring:message code="LB.D0591"/>
											</c:if>
											<c:if test="${ input_data.data.DEGREE.equals('5') }">
<!-- 												高中高職 -->
												<spring:message code="LB.D1141"/>
											</c:if>
											<c:if test="${ input_data.data.DEGREE.equals('6') }">
<!-- 												國中以下 -->
												<spring:message code="LB.D0866"/>
											</c:if>
											<input type="hidden" name="DEGREE" value="${input_data.data.DEGREE}">
										</div>
									</span>
								</div>
								<!--婚姻狀況 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0057"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.MARRY.equals('0') }">
<!-- 												未婚 -->
												<spring:message code="LB.D0596"/>
											</c:if>
											<c:if test="${ input_data.data.MARRY.equals('1') }">
<!-- 												已婚 -->
												<spring:message code="LB.D0595"/>
											</c:if>
											<c:if test="${ input_data.data.MARRY.equals('2') }">
<!-- 												其他 -->
												<spring:message code="LB.D0572"/>
											</c:if>
											<input type="hidden" name="MARRY" value="${input_data.data.MARRY}">
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 											子女人數 -->
											<spring:message code="LB.D1149"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${!input_data.data.CHILD.equals('')}">
												${input_data.data.CHILD}人
											</c:if>
											<input type="hidden" name="CHILD" value="${input_data.data.CHILD}">
										</div>
									</span>
								</div>
								<!-- ***************************************** -->
	                            <div class="classification-block">
	                                <p>聯絡資訊</p>
<!--                                 	<a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- ***************************************** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											E-mail
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.MAILADDR}
											<input type="hidden" name="MAILADDR" value="${input_data.data.MAILADDR}">
										</div>
									</span>
								</div>
								<!-- 行動電話 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0069"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.CELPHONE}
											<input type="hidden" name="CELPHONE" value="${input_data.data.CELPHONE}">
										</div>
									</span>
								</div>
								<!--通訊地電話 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.B0020"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.HOMEZIP}-${input_data.data.HOMETEL}
											<input type="hidden" name="ARACOD1" value="${input_data.data.HOMEZIP}">
											<input type="hidden" name="TELNUM1" value="${input_data.data.HOMETEL}">
											<input type="hidden" name="HOMEZIP" value="${input_data.data.HOMEZIP}">
											<input type="hidden" name="HOMETEL" value="${input_data.data.HOMETEL}">
										</div>
									</span>
								</div>
								<!--公司電話 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0094"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.BUSZIP}-${input_data.data.BUSTEL}
											<input type="hidden" name="ARACOD2" value="${input_data.data.BUSZIP}">
											<input type="hidden" name="TELNUM2" value="${input_data.data.BUSTEL}">
											<input type="hidden" name="BUSZIP" value="${input_data.data.BUSZIP}">
											<input type="hidden" name="BUSTEL" value="${input_data.data.BUSTEL}">
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<!--傳真 -->
											<spring:message code="LB.D1131"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.ARACOD3}-${input_data.data.FAX}
											<input type="hidden" name="ARACOD3" value="${input_data.data.ARACOD3}">
											<input type="hidden" name="FAX" value="${input_data.data.FAX}">
										</div>
									</span>
								</div>
								<!-- 戶籍地址 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0143"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.POSTCOD1} ${input_data.data.PMTADR}
											<input type="hidden" name="POSTCOD1" value="${input_data.data.POSTCOD1}">
											<input type="hidden" name="PMTADR" value="${input_data.data.PMTADR}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										通訊地址 -->
                                            <spring:message code="LB.D0376"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.POSTCOD2} ${input_data.data.CTTADR}
											<input type="hidden" name="POSTCOD2" value="${input_data.data.POSTCOD2}">
											<input type="hidden" name="CTTADR" value="${input_data.data.CTTADR}">
										</div>
									</span>
								</div>
								<!-- ***************************************** -->
	                            <div class="classification-block">
	                                <p>職業資訊</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- ***************************************** -->
								<!--職業 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1132"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.CAREER1_str}
											<input type="hidden" name="CAREER1" value="${input_data.data.CAREER1}">
											
										</div>
									</span>
								</div>
								<!--職稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0087"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<%-- 											${input_data.data.CAREER2_str} --%>
											<c:if test="${ input_data.data.CAREER2.equals('1') }">
												<spring:message code="LB.X0179"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('2') }">
												<spring:message code="LB.X0180"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('3') }">
												<spring:message code="LB.X0181"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('4') }">
												<spring:message code="LB.X0182"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('5') }">
												<spring:message code="LB.X0183"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('6') }">
												<spring:message code="LB.X0184"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('7') }">
												<spring:message code="LB.X0185"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('8') }">
												<spring:message code="LB.X0186"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('9') }">
												<spring:message code="LB.X0187"/>
											</c:if>
											<input type="hidden" name="CAREER2" value="${input_data.data.CAREER2}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										任職機構 -->
											<spring:message code="LB.D1074"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.EMPLOYER}
											<input type="hidden" name="EMPLOYER" value="${input_data.data.EMPLOYER}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											您所任職機構是否涉及高風險項目
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.HIRISK0.equals('N') }">
												否
											</c:if>
											<c:if test="${ input_data.data.HIRISK0.equals('Y') }">
												是
											</c:if>
										</div>
									</span>
								</div>
								<c:if test="${ input_data.data.HIRISK0.equals('Y') }">
									<div class="ttb-input-item row">
										<span class="input-title"> 
										<label>
											<h4>
												項目內容
											</h4>
										</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<c:if test="${ input_data.data.HIRISK.equals('E') }">
													第三方支付服務業
												</c:if>
												<c:if test="${ input_data.data.HIRISK.equals('F') }">
													油品海運貿易業
												</c:if>
												<input type="hidden" name="HIRISK" value="${input_data.data.HIRISK}">
											</div>
										</span>
									</div>
								</c:if>
								<br>
								
	                            <!-- ********************** -->
	                            <div class="classification-block">
	                                <p>開戶資訊</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
	                            
	                            <!-- ********************** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
<!-- 									本次開戶目的 -->
											<h4><spring:message code="LB.D1075"/></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.PURPOSE.equals('A') }">
<!-- 												儲蓄 -->
												<spring:message code="LB.D1076"/>
											</c:if>
											<c:if test="${ input_data.data.PURPOSE.equals('B') }">
<!-- 												薪轉 -->
												<spring:message code="LB.D1077"/>
											</c:if>
											<c:if test="${ input_data.data.PURPOSE.equals('C') }">
<!-- 												資金調撥 -->
												<spring:message code="LB.D1078"/>
											</c:if>
											<c:if test="${ input_data.data.PURPOSE.equals('D') }">
<!-- 												投資 -->
												<spring:message code="LB.D1079"/>
											</c:if>
											<c:if test="${ input_data.data.PURPOSE.equals('E') }">
<!-- 												其他： -->
												<spring:message code="LB.D0572"/>: ${input_data.data.PREASON}
											</c:if>
											<input type="hidden" name="PURPOSE" value="${input_data.data.PURPOSE}">
											<input type="hidden" name="PREASON" value="${input_data.data.PREASON}">
										</div>
									</span>
								</div>
								<!-- 與本行已往來業務 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1391"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.BDEALING_str}
											<input type="hidden" name="BDEALING" value="${input_data.data.BDEALING}">
											<input type="hidden" name="BREASON" value="${input_data.data.BREASON}">
										</div>
									</span>
								</div>
								<!--主要資金/財產來源 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1397"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.MAINFUND.equals('A') }">
<!-- 												薪資 -->
												<spring:message code="LB.D1398"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('B') }">
<!-- 												儲蓄 -->
												<spring:message code="LB.D1076"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('C') }">
<!-- 												營運 -->
												<spring:message code="LB.D1399"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('D') }">
<!-- 												投資 -->
												<spring:message code="LB.D1079"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('E') }">
<!-- 												銷售 -->
												<spring:message code="LB.D1400"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('F') }">
<!-- 												家業繼承 -->
												<spring:message code="LB.D1401"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('G') }">
<!-- 												退休金 -->
												<spring:message code="LB.D0910"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('H') }">
<!-- 												其他： -->
												<spring:message code="LB.D0572"/>: ${input_data.data.MREASON}
											</c:if>
											<input type="hidden" name="MAINFUND" value="${input_data.data.MAINFUND}">
											<input type="hidden" name="MREASON" value="${input_data.data.MREASON}">
										</div>
									</span>
								</div>
								<!-- 個人/家庭 年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.X0170"/><spring:message code="LB.D0625_1"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.SALARY}<spring:message code="LB.D1144"/>
											<input type="hidden" name="SALARY" value="${input_data.data.SALARY}">
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										預期往來金額(近一年) -->
											<spring:message code="LB.D1081_1"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.S_MONEY} <spring:message code="LB.X0423"/>
											<input type="hidden" name="S_MONEY" value="${input_data.data.S_MONEY}">
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										指定開戶分行 -->
											<spring:message code="LB.D1135"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.BRHCOD} ${input_data.data.BRHNAME}
											<input type="hidden" name="BRHCOD" value="${input_data.data.BRHCOD}">
											<input type="hidden" name="BANKCODE" value="${input_data.data.BRHCOD}">
											<input type="hidden" name="BRHNAME" value="${input_data.data.BRHNAME}">
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										推薦碼 -->
											推薦碼
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.EMPNO}
											<input type="hidden" name="EMPNO" value="${input_data.data.EMPNO}">
										</div>
									</span>
								</div>
								<!-- *********************************** -->
	                            <div class="classification-block">
	                                <p><spring:message code="LB.Financial_debit_card"/></p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- *********************************** -->
								
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 										<label> -->
<!-- 											<h4> -->
<%-- 												<spring:message code="LB.D1431_3"/> --%>
<!-- 											</h4> -->
<!-- 										</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<%-- 											<c:if test="${ input_data.data.CARDAPPLY.equals('Y') }"> --%>
<!--  													是 -->
<%-- 												<spring:message code="LB.D0034_2"/> --%>
<%-- 											</c:if> --%>
<%-- 											<c:if test="${ input_data.data.CARDAPPLY.equals('N') }"> --%>
<!-- 													否 -->
<%-- 												<spring:message code="LB.D0034_3"/> --%>
<%-- 											</c:if> --%>
<!-- 										</div> -->
<!-- 									</span>  -->
<!-- 								</div> -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Card_kind"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.CARDAPPLY.equals('Y') }">
												<c:if test="${ input_data.data.CRDTYP.equals('1') }">
<!-- 														晶片金融卡 -->
													<spring:message code="LB.Financial_debit_card"/>
												</c:if>
												<c:if test="${ input_data.data.CRDTYP.equals('2') }">
<!-- 														感應式金融卡 -->
													<spring:message code="LB.D1432"/>
												</c:if>
											</c:if>
											<c:if test="${ input_data.data.CARDAPPLY.equals('N') }">
<!--													否  -->
												<spring:message code="LB.D0034_3"/>
											</c:if>
											<input type="hidden" name="CARDAPPLY" value="${input_data.data.CARDAPPLY}">
											<input type="hidden" name="SNDFLG" value="${input_data.data.SNDFLG}">
											<input type="hidden" name="CRDTYP" value="${input_data.data.CRDTYP}">
										</div>
									</span> 
								</div>
								<c:if test="${ input_data.data.CARDAPPLY.equals('Y') }">
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
	<!-- 													領取方式 -->
													<spring:message code="LB.D0503"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<c:if test="${ input_data.data.SNDFLG.equals('1') }">
													<!-- 郵寄至通訊地址 -->
													<spring:message code="LB.B0007"/>
												</c:if>
												<c:if test="${ input_data.data.SNDFLG.equals('3') }">
													<!-- 郵寄至戶籍地址 -->
													<spring:message code="LB.B0006"/>
												</c:if>
												<c:if test="${ input_data.data.SNDFLG.equals('2') }">
													<!-- 至指定服務分行親領 -->
													<spring:message code="LB.B0008"/>
												</c:if>
											</div>
										</span> 
									</div>
								</c:if>
								<c:if test="${ input_data.data.CARDAPPLY.equals('Y') }">
									<div class="ttb-input-item row">
										<span class="input-title"> 
										<label>
											<h4>
												<!-- 申請消費扣款(Smart Pay) -->
												<spring:message code="LB.X0214"/>
											</h4>
										</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<c:if test="${ input_data.data.CChargeApply.equals('Y') }">
													<spring:message code="LB.D0034_2"/>
												</c:if>
												<c:if test="${ input_data.data.CChargeApply.equals('N') }">
	<!-- 													否 -->
													<spring:message code="LB.D0034_3"/>
												</c:if>
											</div>
										</span> 
									</div>
								</c:if>
								<input type="hidden" name="CChargeApply" value="${input_data.data.CChargeApply}">
								<input type="hidden" name="LMT" value="${input_data.data.LMT}">
								<c:if test="${ input_data.data.CChargeApply.equals('Y') }">
									<c:if test="${ input_data.data.CARDAPPLY.equals('Y') }">
										<div class="ttb-input-item row">
											<span class="input-title"> 
											<label>
												<h4>
													<!-- 每日限額 -->
													<spring:message code="LB.X0215"/>
												</h4>
											</label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<fmt:parseNumber var="formatLMT" type="number" integerOnly="true" value="${input_data.data.LMT}" />
													${formatLMT} <spring:message code="LB.D0088_2"/>
													<span class="input-remarks">(<spring:message code="LB.D1436"/>)</span>
												</div>
											</span> 
										</div>	
									</c:if>
								</c:if>
								<c:if test="${ input_data.data.CARDAPPLY.equals('Y') }">
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<!-- 申請非約定帳號轉帳(每日限額三萬元整) -->
													<spring:message code="LB.D1437"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<c:if test="${ input_data.data.NAATAPPLY.equals('Y') }">
	<!-- 													是 -->
													<spring:message code="LB.D0034_2"/>
												</c:if>
												<c:if test="${ input_data.data.NAATAPPLY.equals('N') }">
	<!-- 													否 -->
													<spring:message code="LB.D0034_3"/>
												</c:if>
<!--                                         	<span class="input-remarks">行動銀行暫不開放約定轉帳功能。</span> -->
											</div>
										</span> 
									</div>
								</c:if>
								<input type="hidden" name="NAATAPPLY" value="${input_data.data.NAATAPPLY}">
								<c:if test="${ input_data.data.CARDAPPLY.equals('Y') }">
									<div class="ttb-input-item row">
										<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1438"/>
											</h4>
										</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<c:if test="${ input_data.data.CROSSAPPLY.equals('Y') }">
	<!-- 													是 -->
													<spring:message code="LB.D0034_2"/>
												</c:if>
												<c:if test="${ input_data.data.CROSSAPPLY.equals('N') }">
	<!-- 													否 -->
													<spring:message code="LB.D0034_3"/>
												</c:if>
											</div>
										</span>
									</div>    
								</c:if>
								<input type="hidden" name="CROSSAPPLY" value="${input_data.data.CROSSAPPLY}">
								<!-- *********************************** -->
	                            <div class="classification-block">
	                                <p>網路銀行服務申請</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- *********************************** -->
								
								<c:if test="${ input_data.data.checknb.equals('Y') }">
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
		                                    <label>
		                                        <h4>網路銀行帳號密碼設定</h4>
		                                    </label>
		                                </span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>您已經開通網路銀行服務，您可透過現有的使用者名稱、簽入密碼、交易密碼登入使用。</span>
		                                    </div>
		                                </span>
		                            </div>
	                            </c:if>
								<c:if test="${ input_data.data.checknb.equals('N') }">
									<div class="ttb-input-item row">
										<span class="input-title"> 
										<label>
											<h4>
												網路銀行帳號密碼設定
											</h4>
										</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color"><spring:message code="LB.Loginpage_User_name"/></span>
												<span>${input_data.data.USERNAME}</span>
												<span class="input-remarks">(<spring:message code="LB.D1184"/>)</span>
											</div>
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color"><spring:message code="LB.login_password"/></span>
												<span>${input_data.data.LOGINPIN}</span>
												<span class="input-remarks">(<spring:message code="LB.D1184"/>)</span>
											</div>
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color"><spring:message code="LB.SSL_password_1"/></span>
												<span>${input_data.data.TRANSPIN}</span>
												<span class="input-remarks">(<spring:message code="LB.D1184"/>)</span>
											</div>
		                                </span>
		                            </div>
	                            </c:if>
								<input type="hidden" id="CHECKNB" name="CHECKNB" value="${ input_data.data.checknb }">
								<input type="hidden" name="USERNAME" value="${input_data.data.USERNAME}">
								<input type="hidden" name="LOGINPIN" value="${input_data.data.LOGINPIN}">
								<input type="hidden" name="TRANSPIN" value="${input_data.data.TRANSPIN}">
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										啟用「數位存款帳戶」轉出服務功能 -->
											<spring:message code="LB.D1425"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.TRFLAG.equals('Y') }">
<!-- 													是 -->
												<spring:message code="LB.D0034_2"/>
											</c:if>
											<c:if test="${ input_data.data.TRFLAG.equals('N') }">
<!-- 													否 -->
												<spring:message code="LB.D0034_3"/>
											</c:if>
										</div>
	                                </span>
	                            </div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 											申請電子帳單 -->
											<spring:message code="LB.D0279"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.EBILLFLAG.equals('Y') }">
<!-- 													是 -->
												<spring:message code="LB.D0034_2"/>
											</c:if>
											<c:if test="${ input_data.data.EBILLFLAG.equals('N') }">
<!-- 													否 -->
												<spring:message code="LB.D0034_3"/>
											</c:if>
											<input type="hidden" name="TRFLAG" value="${input_data.data.TRFLAG}">
											<input type="hidden" name="EBILLFLAG" value="${input_data.data.EBILLFLAG}">
										</div>
									</span>
								</div>
								
								<!-- ***************************************** -->
	                            <div class="classification-block">
	                                <p>身份資料</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- ***************************************** -->
	                            <div class="photo-block" id="bigBlock1">
	                                <div>
										<img id="img1" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE1" value="${input_data.data.FILE1}">
	                                    <p><spring:message code="LB.D0111"/></p>
	                                </div>
	                                <div>
										<img id="img2" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE2" value="${input_data.data.FILE2}">
	                                    <p>身分證背面圖片</p>
	                                </div>
	                            </div>
								<br>
								<!-- ***************************************** -->
	                            <div class="classification-block">
	                                <p>第二證件資料</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- ***************************************** -->
	                            <div class="photo-block" id="bigBlock2">
	                                <div>
										<img id="img3" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE3" value="${input_data.data.FILE3}">
	                                    <p><spring:message code="LB.D1159"/></p>
	                                </div>
									<div>
										<img id="img4" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE4" value="${input_data.data.FILE4}">
	                                    <p>第二證件背面圖片</p>
	                                </div>
	                            </div>
								<!-- *********************************** -->
	                            <div class="classification-block">
	                                <p>身分驗證</p>
	                            </div>
								<!-- *********************************** -->
								
							<!--交易機制  SSL 密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.B0001" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<c:if test="${input_data.data.FGTXWAY.equals('4')}">
											<!-- 自然人憑證 -->
											<div class="ttb-input">
												<label class="radio-block">
<!-- 													自然人憑證 -->
													<spring:message code="LB.D0437"/>
												 	<input type="radio" name="FGTXWAY" id="CMNPC" value="4" checked="checked">
														
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A1">
												</label>
											</div>
										</c:if>
										<c:if test="${input_data.data.FGTXWAY.equals('2')}">
											<!-- 晶片金融卡 -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Financial_debit_card" />
													
												 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
														
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A2">
												</label>
											</div>
										</c:if>
										<c:if test="${input_data.data.FGTXWAY.equals('5')}">
											<!-- 跨行金融帳戶認證 -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Fisc_inter-bank_acc" /> 													
												 	<input type="radio" name="FGTXWAY" id="CMFISCACC" value="5" checked="checked">														
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A3">
												</label>
											</div>
										</c:if>										
										
										<c:if test="${input_data.data.FGTXWAY.equals('6')}">
											<!-- 跨行金融帳戶認證 -->
											<div class="ttb-input">
												<label class="radio-block">
													自行帳戶認證 
												 	<input type="radio" name="FGTXWAY" id="LOISCACC" value="6" checked="checked">														
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A4">
												</label>
											</div>
										</c:if>										
										
									</span>
								</div>
								<!-- 確認新使用者名稱 -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 										<label> -->
<!-- 											<h4> -->
<!--  												驗證碼 -->
<%-- 												<spring:message code="LB.Captcha" /> --%>
<!-- 											</h4> -->
<!-- 										</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
											
<%-- 											<spring:message code="LB.Captcha" var="labelCapCode" /> --%>
<!-- 											<input id="capCode" type="text" class="ttb-input" -->
<%-- 												name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off"> --%>
<!-- 											<img name="kaptchaImage" src="" /> -->
<%-- 											&nbsp;<font color="#FF0000"><spring:message code="LB.Captcha_refence" /> </font> --%>
<!-- 											<div class="login-input-block"> -->
<%-- 												<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration"/>" /> --%>
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
								
							
								<c:if test="${input_data.data.FGTXWAY.equals('4')}">
									<!-- 自然人憑證PIN碼 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.D1540" /></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<input type="PASSWORD" class="text-input" maxLength="8" size="10" id="NPCPIN" name="NPCPIN" value=""/>
											</div>
										</span>
									</div>
								</c:if>
								
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Captcha" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.Captcha" var="labelCapCode" />
											<input id="capCode" type="text" class="text-input input-width-125"
											name="capCode" placeholder="${labelCapCode}" maxlength="6" 
											onkeyup="value=value.replace(/[^\u0000-\u00ff]/g,'')"onpaste="value=value.replace(/[^\u0000-\u00ff]/g,'')"autocomplete="off">
												
											<img name="kaptchaImage" src=""  class="verification-img"/>
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration"/>" />
											<!-- <span class="input-remarks">請注意：英文不分大小寫，限半形字元</span> -->
										</div>
									</span>
								</div>
                           	</div>
	                        <!--button 區域 -->
							<!-- 重新輸入 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318"/>" class="ttb-button btn-flat-gray">
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="送出申請" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
<script type="text/javascript">
	function checkTC(){
		component_isikeyuser(); // 判斷使用者是否ikey使用者
		component_version(); // 取得各元件的最新版本號
		component_platform(); // 取得裝置作業系統
		component_initKeyBoard(); // 動態鍵盤初始化
	}
</script>
</html>