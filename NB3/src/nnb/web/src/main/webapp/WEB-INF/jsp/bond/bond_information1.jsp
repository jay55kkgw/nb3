<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
	<script type="text/javascript">
		$(document).ready(function(){
			//HTML載入完成後開始遮罩
			setTimeout("initBlockUI()",100);
			//解遮罩
			setTimeout("unBlockUI(initBlockId)",200);
			
			$("#CMSUBMIT").click(function(){
				if(chkclick()){
					initBlockUI();
					$("#formID").attr("action","${__ctx}/BOND/PURCHASE/bond_information2");
					initBlockUI();//遮罩
					$("#formID").submit();
				}
				else{
					errorBlock(null, null, ['<spring:message code= "LB.X2549" />'],
							'<spring:message code= "LB.Confirm" />', null);
				}
			});
		
			$("#CMCANCEL").click(function(){
				initBlockUI();
				$("#formID").attr("action","${__ctx}/BOND/PURCHASE/bond_purchase_input");
				initBlockUI();//遮罩
				$("#formID").submit();
			});
			
			$("input[name^='ReadFlag']").on('click', function() {
				var clickName = this.id;
				var bondNUM = "${bond_information1.data.PRDNO}";
				if($("#"+clickName).prop('checked')){
					$("#"+clickName).prop('checked', true)
					if(clickName == "ReadFlag1"){
						window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + bondNUM + "&DownFile=102", "_blank");
					}
					else if(clickName == "ReadFlag2"){
						window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + bondNUM + "&DownFile=101", "_blank");
					}
				}
				else{
					$("#"+clickName).prop('checked', false)
				}
			});
		});
		
		function chkclick(){
			if($('#ReadFlag1').prop('checked') && $('#ReadFlag2').prop('checked') && $('#AgreeFlag').prop('checked')){
				return true;
			}
			else{
				return false;
			}
		}
	</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
    <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
    <!-- 海外債券產品說明暨風險預告書     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2550" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
<!-- 			海外債券產品說明暨風險預告書 -->
				<h2><spring:message code="LB.X2550" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
				<form id="formID" method="post">
					<input type="hidden" id="TOKEN" name = "TOKEN" value="${sessionScope.transfer_confirm_token}">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
	                                <h4><spring:message code="LB.X2551" /></h4>
	                            </div>
								<span class="input-block">
	                                <div class="ttb-input">
										<label class="check-block" for="ReadFlag1">
											<input type="checkbox" name="ReadFlag1" id="ReadFlag1" onclick="checkReadFlag()">
											<spring:message code="LB.X2552" />
			                                <span class="ttb-check"></span>
										</label>
	                                </div>
	                                <div class="ttb-input">
										<label class="check-block" for="ReadFlag2">
											<input type="checkbox" name="ReadFlag2" id="ReadFlag2" onclick="checkReadFlag()">
<!-- 											商品說明書(英文版) -->
			                                	<spring:message code="LB.X2553" />
			                                <span class="ttb-check"></span>
										</label>
	                                </div>
	                                <div class="ttb-input">
										<label class="check-block" for="AgreeFlag">
											<input type="checkbox" name="AgreeFlag" id="AgreeFlag">
<!-- 											本人已自行判斷本投資並承擔風險，投資實際損益之計算方式以商品說明暨風險預告書所載條款為準 -->
			                                	<spring:message code="LB.X2554" />
			                                <span class="ttb-check"></span>
										</label>
	                                </div>
	                            </span>
                            </div>
							<input type="button" id="CMCANCEL" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>	
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>	
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>