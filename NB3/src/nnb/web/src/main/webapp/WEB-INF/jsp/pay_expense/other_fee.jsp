<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
	
	<script type="text/JavaScript">
		// 遮罩用
		var blockId;
		
	    $(document).ready(function() {
	    	// HTML載入完成後開始遮罩
	    	initBlockUI();
			// 開始查詢資料並完成畫面
			setTimeout("init()", 100);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

	 	// 初始化
	    function init(){
	    	// 即時、預約標籤 click
	    	tabEvent();
	    	// 建立約定轉出帳號下拉選單
	    	creatOUTACN();
	    	// 建立轉入帳號下拉選單
	    	creatDPAGACNO();
	    	// 建立非約定銀行代號下拉選單
	    	creatDpBHNO()
	    	// 轉出帳號 change
	    	acnoEvent();
	    	// 預約自動輸入明天
	    	getTmrDate();
	    	// 西元年日期輸入欄位 click
	    	datetimepickerEvent();
	    	// 通訊錄 click
			addressbookClickEvent();

			// 回上一頁填入資料
			refillData();
			
			// 初始化後隱藏span( 表單驗證提示訊息用的 )
			$(".hideblock").hide();
			// 表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// submit觸發事件
			finish();
	    }
	 	
	 	// 確認按鈕點擊觸發submit
		function finish(){
			$("#CMSUBMIT").click(function (e) {
				// 去掉舊的錯誤提示訊息
				$(".formError").remove();
				// 顯示驗證隱藏欄位( 為了讓提示訊息對齊 )
				$(".hideblock").show();
				
				console.log("submit~~");
				
				// 塞值進隱藏span內的input
				$("#CMDATE").val($("#CMDATE_TMP").val());
				$("#CMSDATE").val($("#CMSDATE_TMP").val());
				$("#CMEDATE").val($("#CMEDATE_TMP").val());
				$("#CMDD").val($("#CMDD_TMP").val());
				$("#AMOUNT").val($("#AMOUNT_TMP").val());
				
				// 塞值進隱藏span內的input--約定/非約定 之轉出帳號
				if( $('input[name=TransferType]:checked').val() == 'PD'){
					// 轉出帳號為約定
					$("#OUTACN").val($("#OUTACN_PD").val());
				}else if( $('input[name=TransferType]:checked').val() == 'NPD'){
					// 轉出帳號為非約定
					$("#OUTACN").val($("#OUTACN_NPD").val());
				}

			   	// 表單驗證--即時/預約單日/預約區間
			   	var fgtxdate = $('input[name="FGTXDATE"]:checked').val();
			   	console.log('fgtxdate: ' + fgtxdate);
			   	
				var sMinDate = '${str_SystemDate}';
				var sMaxDate = '${sNextYearDay}';
				console.log("processQuery.sMinDate: " + sMinDate);
				console.log("processQuery.sMaxDate: " + sMaxDate);
// 				$("#MINDATE").val(sMinDate);
// 				$("#MAXDATE").val(sMaxDate);
				
				if (fgtxdate == '2'){
					// 驗證預約單日
		            $("#CMDATE").addClass("validate[required,funcCall[validate_CheckDate['<spring:message code= "LB.X0377" />',CMDATE,"+sMinDate+","+sMaxDate+"]]]");
		         	// 取消驗證預約區間欄位
		            $("#CMSDATE").removeClass("validate[required,funcCall[validate_CheckDate['<spring:message code= "LB.X0377" />',CMSDATE,"+sMinDate+","+sMaxDate+"]]]");
		            $("#CMEDATE").removeClass("validate[required,funcCall[validate_CheckDate['<spring:message code= "LB.X0377" />',CMEDATE,"+sMinDate+","+sMaxDate+"]]]");
		            $("#odate").removeClass("validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />CheckDateScope', odate, CMSDATE , CMEDATE, true, null, 12]]]");
// 		            $("#CMDD").removeClass("validate[required,funcCall[validate_CheckPerMonthDay['查詢期間', CMDD,MINDATE,MAXDATE]]]");
		            $("#CMDD").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.X1433" />',CMDD_TMP,'']]]");
		            
			   	} else if(fgtxdate == '1') {
			   		// 即時交易取消驗證預約欄位
		            $("#CMDATE").removeClass("validate[required,funcCall[validate_CheckDate['<spring:message code= "LB.X0377" />',CMDATE,"+sMinDate+","+sMaxDate+"]]]");
		            $("#CMSDATE").removeClass("validate[required,funcCall[validate_CheckDate['<spring:message code= "LB.X0377" />',CMSDATE,"+sMinDate+","+sMaxDate+"]]]");
		            $("#CMEDATE").removeClass("validate[required,funcCall[validate_CheckDate['<spring:message code= "LB.X0377" />',CMEDATE,"+sMinDate+","+sMaxDate+"]]]");
		            $("#odate").removeClass("validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />CheckDateScope', odate, CMSDATE , CMEDATE, true, null, 12]]]");
// 		            $("#CMDD").removeClass("validate[required,funcCall[validate_CheckPerMonthDay['查詢期間', CMDD,MINDATE,MAXDATE]]]");
		            $("#CMDD").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.X1433" />',CMDD_TMP,'']]]");
		            
			   	} else if(fgtxdate == '3') {
			   		// 驗證預約區間欄位
		            $("#CMSDATE").addClass("validate[required,funcCall[validate_CheckDate['<spring:message code= "LB.X0377" />',CMSDATE,"+sMinDate+","+sMaxDate+"]]]");
		            $("#CMEDATE").addClass("validate[required,funcCall[validate_CheckDate['<spring:message code= "LB.X0377" />',CMEDATE,"+sMinDate+","+sMaxDate+"]]]");
		            $("#odate").addClass("validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />CheckDateScope', odate, CMSDATE , CMEDATE, true, null, 12]]]");
// 		            $("#CMDD").addClass("validate[required,funcCall[validate_CheckPerMonthDay['查詢期間', CMDD,MINDATE,MAXDATE]]]");
					$("#CMDD").addClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.X1433" />',CMDD_TMP,'']]]");
			   		// 取消驗證預約單日
			   		$("#CMDATE").removeClass("validate[required,funcCall[validate_CheckDate['<spring:message code= "LB.X0377" />',CMDATE,"+sMinDate+","+sMaxDate+"]]]");
				}

				// 表單驗證--轉出帳號
				var transferType = $('input[name="TransferType"]:checked').val();
				console.log('transferType: ' + transferType);
				if(transferType == 'PD') {
					$("#OUTACN_PD").addClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_PD,'']]]");
					$("#OUTACN_NPD").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_NPD,'']]]");
				} else if(transferType == 'NPD') {
					$("#OUTACN_PD").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_PD,'']]]");
					$("#OUTACN_NPD").addClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_NPD,'']]]");
				}

				// 表單驗證--約定/常用帳號
				if (document.getElementById("DPSVACNO1").checked) {
			   		$("#DPAGACNO").addClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.X1432" />',DPAGACNO,'']]]");
			   	} else {
			   		$("#DPAGACNO").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.X1432" />',DPAGACNO,'']]]");
				}

				// 表單驗證--行庫別、非約定帳號
				var oDPSVACNO2 = document.getElementById("DPSVACNO2");
				if (oDPSVACNO2 != null && oDPSVACNO2.checked) {
					$("#DPBHNO").addClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.X0269" />',DPBHNO,'']]]");
					$("#DPACNO").addClass("validate[required, funcCall[validate_CheckNumber['<spring:message code= "LB.Non-designated_account" />',DPACNO]]]");
			   	} else {
			   		$("#DPBHNO").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.X0269" />',DPBHNO,'']]]");
					$("#DPACNO").removeClass("validate[required, funcCall[validate_CheckNumber['<spring:message code= "LB.Non-designated_account" />',DPACNO]]]");
				}
			   	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e = e || window.event; // for IE
					e.preventDefault();
					
				} else {
					// 資料檢核，過了則送出表單
					processQuery();
				}
					
			});
	 	}


		// 即時、預約標籤切換事件
		function tabEvent(){
			// 即時
			$("#nav-trans-now").click(function() {
				$("#transfer-date").hide();
				fstop.setRadioChecked("FGTXDATE" , "1" ,true);
			})
			// 預約
			$("#nav-trans-future").click(function() {
				$("#transfer-date").show();
				fstop.setRadioChecked("FGTXDATE" , "2" ,true);
			})
		}

		// 預約自動輸入明天
		function getTmrDate() {
			// '預約日'期欄位顯示明天
			$('#CMDATE_TMP').val("${tmrDate}");
			$('#CMSDATE_TMP').val("${tmrDate}");
			$('#CMEDATE_TMP').val("${tmrDate}");
		}
		
		// 西元小日曆click
		function datetimepickerEvent(){
		    $(".CMDATE").click(function(event) {
				$('#CMDATE_TMP').datetimepicker('show');
			});
		    $(".CMSDATE").click(function(event) {
				$('#CMSDATE_TMP').datetimepicker('show');
			});
		    $(".CMEDATE").click(function(event) {
				$('#CMEDATE_TMP').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			});
		}

	 	
		// 建立約定轉出帳號下拉選單
		function creatOUTACN(){
			var options = { keyisval:false ,selectID:'#OUTACN_PD'};
			
			uri = '${__ctx}'+"/PAY/EXPENSE/getOutAcn_aj"
			rdata = {type: 'acno' };
			console.log("creatACN.uri: " + uri);
			console.log("creatACN.rdata: " + rdata);
			
			data = fstop.getServerDataEx(uri,rdata,false);

			console.log("creatACN.data: " + JSON.stringify(data));
			
			if(data !=null && data.result == true ){
				fstop.creatSelect( data.data, options);
			}
		}

		// 轉出帳號change事件，要秀出選擇的轉出帳號可用餘額
		function acnoEvent(){
			$('input[type=radio][name=TransferType]').change(function() {
				if (this.value == 'PD') { //約定
					$("#OUTACN_NPD").val("");
				} else if (this.value == 'NPD') { //非約
					$("#OUTACN_PD").val("");
					$("#acnoIsShow").hide();
				}
			});
			$("#OUTACN_PD").change(function() {
				var acno = $('#OUTACN_PD :selected').val();
				console.log("acnoEvent.acno: " + acno);
				getACNO_Data(acno);
				$('#TransferType_01').click();
			});
		}
		// 取得轉出帳號餘額資料
		function getACNO_Data(acno){
			// 變更轉出帳號就隱藏重置再重新顯示
			$("#acnoIsShow").hide();
			$("#showText").html('');
			
			var options = { keyisval:true ,selectID:'#OUTACN'};
			
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getACNO_Data_aj"
			rdata = {acno: acno};
			console.log("getACNO_Data.uri: " + uri);
			console.log("getACNO_Data.rdata: " + rdata);
			
			fstop.getServerDataEx(uri,rdata,false,isShowACNO_Data);
		}
		// 顯示轉出帳號餘額
		function isShowACNO_Data(data){
			var i18n= new Object();
			i18n['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
			console.log("isShowACNO_Data.data: " + JSON.stringify(data));
			
			if(data && data.result){
				// 可用餘額
				console.log("data.data.accno_data: " + data.data.accno_data.ADPIBAL);
				
				// 格式化金額欄位
				i18n['available_balance'] += fstop.formatAmt(data.data.accno_data.ADPIBAL);
				$("#showText").html(i18n['available_balance']);
				$("#acnoIsShow").show();
			}else{
				$("#acnoIsShow").hide();
			}
		}


		// 建立約定及常用轉入帳號下拉選單
		function creatDPAGACNO(){
			data = null;
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getInAcno_aj"
			rdata = {type: 'other_fee_acno' };
			var isikeyuser = '${sessionScope.isikeyuser}';
			options = { keyisval:isikeyuser ,selectID:'#DPAGACNO'}
			data = fstop.getServerDataEx(uri,rdata,false);

			console.log("creatDPAGACNO.data: " + JSON.stringify(data));
			
			if(data !=null && data.result == true ){
				fstop.creatSelect(data.data.REC,options);
			}
		}

		// 建立非約定銀行代號下拉選單
		function creatDpBHNO(){
			data = null;
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getDpBHNO_aj"
			rdata = {type: 'dpbhno' };
			options = { keyisval:true ,selectID:'#DPBHNO'}
			data = fstop.getServerDataEx(uri,rdata,false);
			if(data !=null && data.result == true ){
				fstop.creatSelect(data.data,options);
			}
		}
		
		// 通訊錄click
		function addressbookClickEvent() {
			// 通訊錄btn
			$('#getAddressbook').click(function() {
				openAddressbook();
			});
			// 同交易備註btn
			$('#CMMAILMEMO_btn').click(function() {
				$('#CMMAILMEMO').val( $('#CMTRMEMO').val() );
			});
		}
		// 開啟通訊錄email選單
		function openAddressbook() {
			window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
		}

		function processQuery(){
			var main = document.getElementById("formId");

			var TransferType_01 = document.getElementById("TransferType_01");
			var DPSVACNO1 = document.getElementById("DPSVACNO1");	
				
			// 若為約定轉出	
			if (TransferType_01.checked) {	
			    //若為約定轉入 
				if (DPSVACNO1.checked) {
					if( $("#AMOUNT").val() > 15000000) {
						//alert("<spring:message code= "LB.Alert171" />");
						errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert171' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
						return false;
					}
				} //若為非約定轉入 
				else {
					if( $("#AMOUNT").val() > 3000000) {
						//alert("<spring:message code= "LB.Alert172" />");
						errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert172' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
						return false;
					}				
				}
			}
				
			var UnBondingTransoutBut = document.getElementById("TransferType_02");
			var UnBondingTransinBut = document.getElementById("DPSVACNO2");
			
			if(UnBondingTransoutBut.checked == true) {
				var TransoutAccNo = $("#OUTACN").val();
				console.log("TransoutAccNo: " + TransoutAccNo);
				
				if(TransoutAccNo.length == 0) {
					//alert("<spring:message code= "LB.Alert173" />");
					errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert173' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
					return false;
				} else {
					// 檢核晶片卡主帳是否為臨櫃辦理帳號
					var BondingMatchCount = 0;
					
					if(TransoutAccNo.length > 11)TransoutAccNo = TransoutAccNo.substr(TransoutAccNo.length - 11);

					console.log('OUTACN_PD: ' + $('#OUTACN_PD option').length );
					$("#OUTACN_PD option").each(function(){
						console.log('OUTACN_PD.val: ' + $(this).val() );
						if($(this).val() == TransoutAccNo) { 
							BondingMatchCount++;
						}
					});

				}
				
				if(BondingMatchCount == 0 && $("#AMOUNT").val() > 100000) {
					//alert("<spring:message code= "LB.Alert174" />");
					errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert174' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
					return false;
				}
			}

			console.log('OUTACN: ' + $("#OUTACN").val() );
			if($("#OUTACN").val().length > 0 && $("#OUTACN").val().length > 11){
				var tmp=$("#OUTACN").val().substr($("#OUTACN").val().length - 11);
				$("#OUTACN").val(tmp);
			}
			
			prepareNextPageData();
			
			var acnNo = $("#OUTACN").val();
			while(acnNo.length < 16)acnNo = "0" + acnNo;
			
			var OutAccNo = document.getElementById("TransferType_01").checked ? acnNo : $("#OUTACN").val();
			
			// 隱藏表單按鈕
			$("#CMTRMAIL").hide();
			$("#CMSUBMIT").hide();
			$("#CMRESET").hide();
			$("#CMMAILMEMO_btn").hide();

			// 通過資料檢核解除表單驗證
			$("#formId").validationEngine('detach');
			
			// 畫面遮罩
			initBlockUI();
			// 送交
			$("#formId").submit();
		}

		function prepareNextPageData() {
			var main = document.getElementById("formId");

		  	var obj;
		  	 	  	
		    // 約定轉入帳號文字
		   	var oDPSVACNO1 = document.getElementById("DPSVACNO1");
		    var oDPSVACNO2 = document.getElementById("DPSVACNO2");
		    
		   	if (oDPSVACNO1 != null && oDPSVACNO1.checked) {
				obj = document.getElementById("DPAGACNO");   	  
				$("#agreeAcno").val(obj.options[obj.selectedIndex].text);
		   	} // 非約定轉入帳號文字   	
		   	else if (oDPSVACNO2 != null && oDPSVACNO2.checked) {
				obj = document.getElementById("DPBHNO");
				$("#customBhno").val(obj.options[obj.selectedIndex].text); 
		   	} 	
		   	
		}
		
		// 表單重置
		function formReset() {
			$("#acnoIsShow").hide();
			$(".formError").remove();
			$('form').find('*').filter(':input:visible:first').focus();
			$('#formId').trigger("reset");
		}
		
		// 回上一頁填入資料
		function refillData() {
			var isBack = '${isBack}';
			console.log('isBack: ' + isBack);
			var jsondata = '${previousdata}';
			console.log('jsondata: ' + jsondata);
			
			if ('Y' == isBack && jsondata != null) {
				JSON.parse(jsondata, function(key, value) {
					if(key) {
						var obj = $("input[name='"+key+"']");
						var type = obj.attr('type');
						
						console.log('--------------------------------');
						console.log('type: ' + type);
						console.log('key: ' + key);
						console.log('value: ' + value);
						console.log('--------------------------------');
						
						if(type == 'text'){
							obj.val(value);
						}
						
						if(type == 'hidden'){
							obj.val(value);
						}

						if(type == 'radio'){
							obj.filter('[value='+value+']').prop('checked', true);
						}
						
						if(type == 'checkbox'){
							obj.prop('checked', true);
						}
						
						// SELECT例外處理--約定轉出帳號
						if(key == 'OUTACN_PD'){
							$("#OUTACN_PD option").filter(function() {
								return $(this).val() == value;
							}).attr('selected', true);
						}
						
						// 即時或預約標籤切換
						if(key == 'FGTXDATE' && value == '2'){
							$("#nav-trans-future").click();
						}
					}
				});
				
				// DPAGACNO 會有雙引號所以JSON.parse會有問題，後端remove，讓前端另外處理
				$("#DPAGACNO option").filter(function() {
					return $(this).val() == '${DPAGACNO_BACK}';
				}).attr('selected', true);
				
				// TOKEN值要換一個，不可用上一次的
				$("#TOKEN").val('${sessionScope.transfer_confirm_token}');
			}
		}
		
	</script>
</head>
<body>
	<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 繳費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0616" /></li>
		</ol>
	</nav>

	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
	
		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<!-- 功能內容 -->
				<form method="post" id="formId" action="${__ctx}/PAY/EXPENSE/other_fee_confirm">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					<input type="hidden" id="FLAG" name="FLAG" value="1"><!-- 約定\非約定 -->
					<input type="hidden" id="TYPE" name="TYPE" value="02">
					<input type="hidden" id="agreeAcno" name="agreeAcno" value=""><!-- 約定轉入帳號欄位text內容 -->
					<input type="hidden" id="customBhno" name="customBhno" value=""><!-- 非約定轉入帳號欄位text內容 -->
					<input type="hidden" id="MINDATE" value="">
					<input type="hidden" id="MAXDATE" value="">
					
					<!-- 功能名稱 -->
					<h2><spring:message code="LB.W0616" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
					<!-- 交易流程階段 -->
					<div id="step-bar">
						<ul>
							<li class="active"><spring:message code="LB.Enter_data" /></li>
							<li class=""><spring:message code="LB.Confirm_data" /></li>
							<li class=""><spring:message code="LB.Transaction_complete" /></li>
						</ul>
					</div>
					
					<!-- 功能內容 -->
					<div class="main-content-block row radius-50">
						<!-- 即時、預約導引標籤 -->
						<nav style="width: 100%;">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-trans-now" data-toggle="tab" href="#nav-trans-now" 
									role="tab" aria-controls="nav-home" aria-selected="false">
									<spring:message code="LB.Immediately" />
								</a> 
								<a class="nav-item nav-link" id="nav-trans-future" data-toggle="tab" href="#nav-trans-future" 
									role="tab" aria-controls="nav-profile" aria-selected="true" >
									<spring:message code="LB.Booking" />
								</a>
							</div>
						</nav>
						
						<!-- 交易流程階段 -->
						<div class="col-12 tab-content" id="nav-tabContent">
							<div class="tab-pane fade " id="nav-trans-future" role="tabpanel"
								aria-labelledby="nav-profile-tab"></div>

							<!-- 預約才顯示轉帳日期 -->
							<div class="ttb-input-block tab-pane fade show active"
								id="nav-trans-now" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="ttb-message">
									<span></span>
								</div>
								
								<!-- 即時的屬性(不須顯示預設隱藏) -->
								<div class="ttb-input" style="display: none;">
									<label class="radio-block">
										<spring:message code="LB.Immediately" />
										<input type="radio" name="FGTXDATE" value="1" checked /> 
										<span class="ttb-radio"></span>
									</label>
								</div>
								
								<!-- 轉帳日期 -->
								<div id="transfer-date" class="ttb-input-item row" style="display: none;">
									<span class="input-title">
										<label><h4><spring:message code="LB.Transfer_date" /></h4></label>
									</span>
									<span class="input-block">
										<!-- 預約的屬性(必須顯示) -->
										<div class="ttb-input ">
											<label class="radio-block">
												<spring:message code="LB.Booking" /> 
												<input type="radio" name="FGTXDATE" value="2" /> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- '預約日'期 -->
										<div class="ttb-input">
											<input type="text" id="CMDATE_TMP" value="" size="10"
												maxlength="10" class="text-input datetimepicker" />
											<span class="input-unit CMDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏 -->
											<span class="hideblock">
												<!-- 驗證用的input -->
												<input id="CMDATE" name="CMDATE" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										<!-- 預約日期--區間 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Booking" />
												</br>
												<input type="radio" name="FGTXDATE" value="3"> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<span class="input-unit">
												<spring:message code="LB.Monthly" />
											</span>
											<select id="CMDD_TMP" name="CMDD_TMP" class="custom-select select-input half-input">
												<option value=""><spring:message code="LB.Select" /></option>
												<c:forEach var="day" begin="1" end="31"> 
											         <option value="${day}">${day}</option> 	
												</c:forEach>
											</select> 
											<span class="input-unit"><spring:message code="LB.Day" /></span>
											<!-- 不在畫面上顯示的span -->
											<span id="hideblockcmdd" name="hideblock">
												<!-- 驗證用的input -->
												<input id="CMDD" name="CMDD" type="text" class="text-input " 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Period_start_date" /></span> 
												<input type="text" id="CMSDATE_TMP" name="CMSDATE_TMP" class="text-input datetimepicker" value="" /> 
											<span class="input-unit CMSDATE">
											<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span id="hideblockb" name="hideblock" >
												<!-- 驗證用的input -->
												<input id="CMSDATE" name="CMSDATE" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Period_end_date" /></span> 
											<input type="text" id="CMEDATE_TMP" name="CMEDATE_TMP" class="text-input datetimepicker" value="" /> 
											<span class="input-unit CMEDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span id="hideblockc"  name="hideblock">
												<!-- 驗證用的input -->
												<input id="CMEDATE" name="CMEDATE" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										<!-- 驗證用的span預設隱藏 -->
										<span id="hideblock_CheckDateScope" >
											<!-- 驗證用的input -->
											<input id="odate" name="odate" type="text" value="${str_SystemDate}" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</span>
								</div>
								
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code="LB.Payers_account_no" /></h4></label>
									</span>
									<span class="input-block">
										<!-- 約定 轉出帳號 -->
										<div class="ttb-input">
											<label class="radio-block"> 
												<input type="radio" id="TransferType_01" name="TransferType" value="PD" checked>
													<spring:message code="LB.Designated_account" />
												<span class="ttb-radio"></span>
											</label>
										</div> 
										<div class="ttb-input ">
											<select name="OUTACN_PD" id="OUTACN_PD" class="custom-select select-input half-input">
												<option value="">----<spring:message code="LB.Select_account" />-----</option>
											</select>
											<!-- 約定 轉出帳號 餘額 -->
											<div id="acnoIsShow">
												<span id="showText" class="input-unit"></span>
											</div>
										</div>
										
										<!-- 非約定 轉出帳號 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Out_nondesignated_account" /> 
												<input type="radio" id="TransferType_02" name="TransferType" value="NPD" onclick="listReaders();" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<input type="text" name="OUTACN_NPD" id="OUTACN_NPD" class="text-input" readonly="readonly" value="" />
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.X2361" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
											<span class="hideblock">
												<!-- 轉出帳號 -->
												<input id="OUTACN" name="OUTACN" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
								
								<!-- 轉入帳號 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
	                                        <h4><spring:message code="LB.Payees_account_no" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <label class="radio-block">
	                                        	<spring:message code="LB.Designated_account" />/<spring:message code="LB.Common_non-designated_account" />
	                                            <input type="radio" name="FGSVACNO" id="DPSVACNO1" value="1" checked />
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                    </div>
                                        <div class="ttb-input">
	                                        <select class="custom-select select-input half-input" name="DPAGACNO" id="DPAGACNO" >
	                                            <option value="">--<spring:message code="LB.Select_designated_or_common_non-designated_account" />--</option>
	                                        </select>
                                        </div>
	                                    <div class="ttb-input">
	                                        <label class="radio-block"><spring:message code="LB.Non-designated_account" />
	                                            <input type="radio" name="FGSVACNO" id="DPSVACNO2" value="2" >
                                                <span class="ttb-radio"></span>
	                                        </label>
										</div>
										<div class="ttb-input">
											<select id="DPBHNO" name="DPBHNO" class="custom-select select-input half-input"></select>
										</div>
										<div class="ttb-input">
											<input type="text" class="text-input" name="DPACNO" id="DPACNO" size="16" maxlength="16"
												placeholder="<spring:message code="LB.Enter_Account" />" value="">
											<label class="check-block"><spring:message code="LB.Join_common_account" />
												<input type="checkbox" name="ADDACN" id="ADDACN" value="1" />
												<span class="ttb-check"></span>
											</label>
	                                    </div>
	                                </span>
	                            </div>
								
	                            <!-- 轉帳金額 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.Amount" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
											<input type="text" id="AMOUNT_TMP" name="AMOUNT_TMP" class="text-input" size="10" maxlength="10" value="" /> 
											<span class="input-unit"><spring:message code="LB.Dollar" /></span>
											<br/>
											<font style="color:red">
												<spring:message code="LB.W0622" />
											</font>
										</div>
										<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
										<span class="hideblock">
											<!-- 應繳總金額 -->
											<input id="AMOUNT" name="AMOUNT" type="text" readonly="readonly" 
												class="text-input validate[required,min[1],max[9999999999],funcCall[validate_CheckAmount['<spring:message code= "LB.Amount" />',AMOUNT]]]"												
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
	                                </span>
	                            </div>
								
								<!-- 交易備註 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4><spring:message code="LB.Transfer_note" /></h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" size="56" maxlength="20" value="" />
											<span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
										</div>
									</span>
								</div>
	
								<!-- 轉出成功Email通知 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.X0479" /></h4>
										</label>
									</span>
									<span class="input-block">
									
								<!-- 寄信通知 -->
								<c:choose>
								    <c:when test="${sendMe} && !${sessionScope.dpmyemail}.equals('')">
										    
								        <!-- 通知本人 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Notify_me" />：</span>
											<span class="input-subtitle subtitle-color">${sessionScope.dpmyemail}</span>
										</div>
										
										<!-- 另通知 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Another_notice" />：</span>
										</div>
												
								    </c:when>
						    		<c:otherwise>
										    
								        <!-- 通訊錄 -->
										<div class="ttb-input">
											<input type="text" id="CMTRMAIL" name="CMTRMAIL" placeholder="<spring:message code="LB.Email" />"
												class="text-input validate[funcCall[validate_EmailCheck[CMTRMAIL]]]" />
											<span class="input-unit"></span>
											<!-- window open 去查 TxnAddressBook 參數帶入身分證字號 -->
											<button type="button" class="btn-flat-orange" id="getAddressbook"><spring:message code="LB.Address_book" /></button>
										</div>
												
								    </c:otherwise>
								</c:choose>
										
										<!-- 摘要內容 -->
										<div class="ttb-input">
											<input type="text" id="CMMAILMEMO" name="CMMAILMEMO" class="text-input"
												 placeholder="<spring:message code="LB.Summary" />" value="" />
											<span class="input-unit"></span>
											<button type="button" class="btn-flat-orange" name="CMMAILMEMO_btn" id="CMMAILMEMO_btn">
												<spring:message code="LB.As_transfer_note" />
											</button>
										</div>
									</span>
								</div>
							</div>
						
							<!-- 重新輸入、確認按鈕 -->
							<input id="CMRESET" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" onclick="formReset();" />
							<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						
						</div>
					</div>
					
					<!-- 說明 -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.Other_Fee_P1_D1-1" /><a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank"><spring:message code="LB.Other_Fee_P1_D1-2" /></a><a href="${__ctx}/public/transferFeeRate.htm" target="_blank"><spring:message code="LB.Other_Fee_P1_D1-3" /></a></li>
						<li><spring:message code="LB.Other_Fee_P1_D2" /></li>
						<li><spring:message code="LB.Other_Fee_P1_D3" /></li>
						<li><spring:message code="LB.Other_Fee_P1_D4" /></li>
						<li><spring:message code="LB.Other_Fee_P1_D5" /></li>
						<li><spring:message code="LB.Other_Fee_P1_D6" /></li>
						<li><spring:message code="LB.Other_Fee_P1_D7" /></li>
			        </ol>
					
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>

</body>
</html>
