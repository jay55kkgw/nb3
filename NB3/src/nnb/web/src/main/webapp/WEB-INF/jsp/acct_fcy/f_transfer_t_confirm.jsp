<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	<style>
		.noZhCn{
			-ms-ime-mode: disabled;
		}
	</style>
	
	<script type="text/JavaScript">
		var sec = 180;
		var the_Timeout;
		
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 初始化驗證碼
			setTimeout("initKapImg()", 30);
			// 生成驗證碼
			setTimeout("newKapImg()", 40);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 100);
		});

		
		function init() {
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline"});
			//一開始focusBHO
			focusBHO();
			// 確認鍵 click
			goOn();
			// 交易時限倒數計時
			countDown();
			// 交易類別change事件
			changeFgtxway();
			// 判斷顯不顯示驗證碼
			chaBlock();
		}

		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				//送出進表單驗證前將span顯示
				$("#hideblock").show();
				// BHO塞值進span內的input
				$("#CARDNUM_TOTAL").val(""+$("#transInAccountText1").val()+ $("#transInAccountText2").val()+ $("#transInAccountText3").val());

				
				// 表單驗證
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					var uri = "${__ctx}/checkBHO"

					rdata = {
						keyInBHO : $("#CARDNUM_TOTAL").val(),
						functionName : "transfer",
					};
					fstop.getServerDataEx(uri, rdata, false, checkBHOFinish);
				}
			});
		}
		function checkBHOFinish(data) {
			if (data.result == true) {
				$("#formId").validationEngine('detach');
				processQuery();
			} else {
				//alert(data.message);
				errorBlock(
						null, 
						null,
						[data.message], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
		
		// 畫面初始化後先FOCUS到BHO
		function focusBHO(){
			$("#transInAccountText1").focus();

			$("#transInAccountText1").keyup(
					function(event) {
						if (event.which != 8 && event.which != 46
								&& event.which != 9 && event.which != 16
								&& event.which != 20) {
							$("#transInAccountText2").focus();
						}
					});
			$("#transInAccountText2").keyup(
					function(event) {
						if (event.which != 8 && event.which != 46
								&& event.which != 9 && event.which != 16
								&& event.which != 20) {
							$("#transInAccountText3").focus();
						}
					});
		}
		
		// 交易機制選項
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			switch (fgtxway) {
				case '0':
					// 交易密碼
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					$('#CMPASSWORD').val("");
					initBlockUI(); //遮罩
					$("#formId").submit();
					break;
				case '1':
					// IKEY
					useIKey();
					break;
				case '2':
					// 晶片金融卡
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
			    	break;
				case '7':
					// IDGATE認證
				    idgatesubmit= $("#formId");
				    showIdgateBlock();
				    break;
				default:
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
		// 交易類別change 事件
		function changeFgtxway() {
			$('input[type=radio][name=FGTXWAY]').change(function () {
				console.log(this.value);
				if (this.value == '0') {
					$("#CMPASSWORD").addClass("validate[required]");
				} else if (this.value == '1') {
					$("#CMPASSWORD").removeClass("validate[required]");
				}
				chaBlock();
			});
		}
	 	// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
 			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			if(fgtxway == '2'){
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").show();
			}else{
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").hide();
			}
	 	}

		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}
		
		// 外匯轉帳交易限時
		function countDown() {
			var main = document.getElementById("formId");

			var counter = document.getElementById("CountDown");
			counter.innerHTML = sec;
			sec--;

			if (sec == -1) {
				main.CMSUBMIT.value = "<spring:message code="LB.X1291" />";
				$("#formId").attr("action", "${__ctx}/FCY/TRANSFER/f_transfer_step1");
				// 重設onclick事件
				$('#CMSUBMIT').off("click");
				$("#CMSUBMIT").click(function (e) {
					$("#formId").validationEngine('detach'); // 解除表單驗證
					initBlockUI(); //遮罩
					$("#formId").submit();
					return false;
				});
				return;
			}

			//網頁倒數計時(180秒)      	  	
			the_Timeout = setTimeout("countDown()", 1000);
		}
	</script>
</head>
<body>
	<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	
	<!--   IDGATE --> 		 
	<%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %> 
	
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 買賣外幣/約定轉帳     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Exchange_Transfer" /></li>
		</ol>
	</nav>



	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
	
		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<!-- 功能內容 -->
				<form method="post" id="formId" action="${__ctx}/FCY/TRANSFER/f_transfer_t_result">
					<input type="hidden" name="ADAGREEF" value="${transfer_data.data.adagreef}">
					<input type="hidden" name="ADCURRENCY" value="${transfer_data.data.PAYCCY}">
					<input type="hidden" name="CMTRANPAGE" value="1">
				    				    
				    <input type="hidden" name="TXTOKEN" value="${transfer_data.data.TXTOKEN}"><!-- 防止不正常交易 -->
					<input type="hidden" id="back" name="back" value=""><!-- 回上一頁資料 -->
					<input type="hidden" name="CMTRMAIL" value="${transfer_data.data.CMTRMAIL}"><!--轉出成功Email通知-->
					
   					<!-- 交易機制所需欄位 -->
					<input type="hidden" id="jsondc" name="jsondc" value='${transfer_data.data.jsondc}'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				    <input type="hidden" id="PINNEW" name="PINNEW" value="">

				    
					<!-- 功能名稱 -->
					<h2><spring:message code="LB.FX_Exchange_Transfer" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
					<!-- 交易流程階段 -->
<!-- 					<div id="step-bar"> -->
<!-- 						<ul> -->
<%-- 							<li class="active"><spring:message code="LB.Enter_data" /></li> --%>
<%-- 							<li class=""><spring:message code="LB.Confirm_data" /></li> --%>
<%-- 							<li class=""><spring:message code="LB.Transaction_complete" /></li> --%>
<!-- 						</ul> -->
<!-- 					</div> -->
					
					<!-- 功能內容 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<spring:message code="LB.Time_limit_for_transaction_data" /> -  <spring:message code="LB.Time_limit_time_passed" />：<font id="CountDown" color="red"></font> <spring:message code= "LB.Second" />
									</span>
								</div>
								
								<!-- 轉帳日期 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transfer_date" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.PAYDATE }
										</div>
									</span>
								</div>
	
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payers_account_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.CUSTACC }
										</div>
									</span>
								</div>
								
							<c:if test="${transfer_data.data.str_OutAmt != ''}">
							
								<!-- 轉出金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Deducted" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.str_Curr }
											${transfer_data.data.str_OutAmt }
											&nbsp;<spring:message code="LB.Dollar" />
										</div>
									</span>
								</div>
								
							</c:if>
							
								<!-- 轉入帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payees_account_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.BENACC }
										</div>
									</span>
								</div>
	
								<!-- 轉入帳號確認 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Confirm_payers_account_no" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<img
												src="${__ctx}/getBHO?transferInAccount=${transfer_data.data.BENACC }&functionName=transfer" />
											<br />
											<br />
										</div>
										<div class="BHOInput">
											<input type="text" id="transInAccountText1" maxlength="1" class="text-input noZhCn" /> 
											- 
											<input type="text" id="transInAccountText2" maxlength="1" class="text-input noZhCn" />
											- 
											<input type="text" id="transInAccountText3" maxlength="1" class="text-input noZhCn" /> 
											<span class="input-unit">
											<spring:message	code="LB.Anti-blocking_BHO_attack" />
											</span>
										</div>
										<!-- 不在畫面上顯示的span -->
										<span id="hideblock">
											<input id="CARDNUM_TOTAL" name="CARDNUM_TOTAL" type="text"
											class="text-input validate[required,minSize[3],custom[onlyNumberSp]]"
											style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
									</span>
								</div>
	
							<c:if test="${transfer_data.data.str_InAmt != ''}">
							
								<!-- 轉入金額-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Buy" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.str_Curr }
											${transfer_data.data.str_InAmt }
											&nbsp;<spring:message code="LB.Dollar" />
										</div>
									</span>
								</div>
								
							</c:if>
	
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
										</label>
									</span>
									<span class="input-block">
								
									<!-- 非約定不能用SSL  -->
									<c:if test = "${transfer_data.data.hiddenCMSSL != 'true'}">
										<!-- 交易密碼SSL -->
										<div class="ttb-input">
											<label class="radio-block"> 
												<spring:message	code="LB.SSL_password" /> 
												<input type="radio" name="FGTXWAY" checked="checked" value="0"> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<input id="CMPASSWORD" name="CMPASSWORD" type="password" size="8" maxlength="8"
												class="text-input"
												placeholder="<spring:message code="LB.Please_enter_password"/>">
										</div>
									</c:if>
										
									<c:if test = "${sessionScope.isikeyuser}">
									
										<!-- 電子簽章(請載入載具i-key) -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</c:if>	
										
										<!-- 裝置推播認證 -->
										<div class="ttb-input" name="idgate_group" style="display:none">		 
											<label class="radio-block">
												裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
												<input type="radio" id="IDGATE" name="FGTXWAY" value="7">
												<span class="ttb-radio"></span>
											</label>		 
										</div>
										
										<!-- 晶片金融卡 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Financial_debit_card" />
												<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</span>
								</div>
								
								<!-- 如果是非約定則不能使用IKEY，只能選晶片金融卡，使用者選擇晶片金融卡後才顯示驗證碼-->
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
									<!-- 驗證碼 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Captcha" /></h4>
										</label>
									</span>
									<span class="input-block">
	                                    <div class="ttb-input">
	                                        <img name="kaptchaImage" class="verification-img" src="" align="top" id="random" />
	                                        <button type="button" class="btn-flat-gray" name="reshow" onclick="changeCode()">
												<spring:message code="LB.Regeneration" /></button>
	                                    </div>
	                                    <div class="ttb-input">
	                                        <input id="capCode" name="capCode" type="text"
												class="text-input" maxlength="6" autocomplete="off">
	                                        <br>
	                                        <span class="input-unit"><spring:message code="LB.Captcha_refence" /></span>
	                                    </div>
	                                </span>
								</div>
								
							</div>
								
							<!-- 確定 -->
							<input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
							
						</div>
					</div>
					
					<!-- 說明 -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P4_D1" /></strong></li>
			        </ol>
			        
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>

</body>
</html>
