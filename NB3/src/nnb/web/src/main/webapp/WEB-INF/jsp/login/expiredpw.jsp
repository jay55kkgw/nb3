<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
$(document).ready(function () {
	// 是，立即變更
	$("#yes").click(function() {
		initBlockUI();
		$("#formId").attr("action", "${__ctx}" + "/doublepw");
    	$("#formId").submit();
	});
	
	// 稍後變更
	$("#later").click(function() {
		initBlockUI();
		
		setTimeout("lateralter()", 100);
	});
	
	// 否，沿用原密碼
	$("#no").click(function() {
		initBlockUI();
		
		setTimeout("useoldpw()", 100);
	});
	
});


// 稍後變更
function lateralter() {
	var laterAlterUri = '${__ctx}' + "/lateralter";
	var laterAlterData = fstop.getServerDataEx(laterAlterUri, null, false);
	// 告知結果
	if (laterAlterData != null && laterAlterData.result==true) {
		// 稍後變更成功直接進首頁
		$("#formId").attr("action", "${__ctx}" + "/INDEX/index");
    	$("#formId").submit();
    	
	} else {
		unBlockUI(initBlockId);
		errorBlock(
			null, 
			null,
			["<spring:message code= 'LB.Transaction_code' />: " + laterAlterData.msgCode , "\n" , "<spring:message code='LB.Transaction_message' />: " + laterAlterData.message], 
			'<spring:message code= "LB.Quit" />', 
			null
		);
// 		errorBlock(
// 			null, 
// 			null,
// 			["<spring:message code= 'LB.X1799' />"], 
// 			'<spring:message code= "LB.Quit" />', 
// 			null
// 		);
	}
}

// 稍後變更
function useoldpw() {
	// ajax用原簽入密碼去變更(密碼有一個變更，就會重置)，成功則導頁到成功頁
	var useOldUri = '${__ctx}' + "/useoldpw";
	var useOldData = fstop.getServerDataEx(useOldUri, null, false);
	// 告知結果
	if (useOldData!= null && useOldData.result==true) {
		// 成功導頁到首頁
		$("#formId").attr("action", "${__ctx}" + "/INDEX/index");
    	$("#formId").submit();
    	
	} else {
		unBlockUI(initBlockId);
		errorBlock(
			null, 
			null,
			["<spring:message code= 'LB.Transaction_code' />: " + useOldData.msgCode , "\n" , "<spring:message code='LB.Transaction_message' />: " + useOldData.message], 
			'<spring:message code= "LB.Quit" />', 
			null
		);
// 		errorBlock(
// 			null, 
// 			null,
// 			["<spring:message code= 'LB.Alert137' />"], 
// 			'<spring:message code= "LB.Quit" />', 
// 			null
// 		);
	}
}
</script>

<style>
.content-block {
    margin-top: 30px;
    margin-bottom: 50px;
    border-radius: 8px;
    background-color: #ffffff;
    box-shadow: 0 4px 10px 0 rgba(0, 0, 0, 0.06);
/*     text-align: center; */
}
</style>

</head>
<body>
	<div class="content row">
		<section id="main-content" class="container">
			<form id="formId" method="post" action="">
				<div class="content-block">
					<p>
						<font color="red" size="4">
							<b>※&nbsp;&nbsp;<spring:message code="LB.Password_note_2" /></b></font>
					</p>
					<p>
						<input type="button" class="ttb-button btn-flat-orange" id="yes" value="<spring:message code="LB.Use_the_new_pwd" />" />
						<input type="button" class="ttb-button btn-flat-orange" id="later" value="<spring:message code="LB.Change_later" />" />
						<input type="button" class="ttb-button btn-flat-orange" id="no" value="<spring:message code="LB.Use_the_org_pwd" />" />
					</p>
					<br>
					<p>
						<font size="4">
							<b>⊙&nbsp;&nbsp;<spring:message code="LB.Password_note_3" /></b></font>
					</p>
					<p>
						<font size="4">
							<b>⊙&nbsp;&nbsp;<spring:message code="LB.Password_note_4" />
							<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="LB.Password_note_5" /></b></font>
					</p>
				</div>
			</form>
		</section>
	</div>
</body>
</html>