<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		//initFootable();
		init();
		setTimeout("initDataTable()",100);
	});
	
	function init() {
        //列印
    	$("#printbtn").click(function(){
			var params = {
				"jspTemplateName":"bond_balance_result_print",
				"jspTitle":'<spring:message code= "LB.W0034" />',
				"CMQTIME":"${loan_detail.data.CMQTIME}",
				"CMRECNUM":"${loan_detail.data.CMRECNUM}"
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});
		//上一頁按鈕
		$("#CMBACK").click(function() {
			var action = '${__ctx}/NT/ACCT/BOND/bond_balance';
			$('#back').val("Y");
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").submit();
		});
		
		//繼續查詢
    	$("#CMCONTINU").click(function (e) {
			e = e || window.event;
			console.log("submit~~");

			$("#formId").validationEngine('detach');
			initBlockUI(); //遮罩
			$("#formId").attr("action", "${__ctx}/NT/ACCT/BOND/bond_balance_result");
			$("#formId").submit();
		});
    }
	
	//下拉式選單
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
	 		$('#actionBar').val("");
 		}
	}

</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 中央登錄債券查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0033" /></li>
    <!-- 餘額查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0034" /></li>
		</ol>
	</nav>



		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
				<section id="main-content" class="container">
					<!-- 主頁內容  -->
					<h2><spring:message code="LB.W0034" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<form id="formId" method="post" action="">
                		<input type="hidden" id="back" name="back" value="">
                    	<input type="hidden" name="USERDATA_X50" value="${loan_detail.data.USERDATA_X50}" />
                    	<input type="hidden" name="ACN1" value="${loan_detail.data.ACN1}" />
						<div class="main-content-block row">
							<div class="col-12 tab-content">
								<ul class="ttb-result-list">
									<li><!-- 查詢時間 -->
                                		<h3><spring:message code="LB.Inquiry_time" /></h3>
                                		<p>${loan_detail.data.CMQTIME}</p>
                            		</li>
									<li><!-- 單位 -->
                                		<h3><spring:message code="LB.W1466" /></h3>
                                		<p><spring:message code="LB.X0423" /></p>
                            		</li>
									<li><!-- 資料總數 -->
                                		<h3><spring:message code="LB.Total_records" /></h3>
                                		<p>${loan_detail.data.CMRECNUM}&nbsp;<spring:message code="LB.Rows"/></p>
                            		</li>
								</ul>
								<!-- 中央登錄債券餘額 -->
								
								<c:forEach var="tableList" items="${loan_detail.data.REC}">
									<ul class="ttb-result-list">
                            			<li>
                                			<h3><spring:message code="LB.Account" /></h3>
                                			<p>${tableList.ACN}</p>
                           				</li>
                        			</ul>
									<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
										<thead>
											<tr>
												<!-- 債券代號 -->
												<th><spring:message code="LB.W0036" /> </th>
												<!-- 餘額-->
												<th><spring:message code="LB.W0017" /></th>
												<!-- 可動支餘額 -->
												<th><spring:message code="LB.W0038" /></th>
												<!-- 限制性轉出餘額 -->
												<th><spring:message code="LB.W0039" /></th>
												<!-- 限制性轉入餘額 -->
												<th><spring:message code="LB.W0040" /></th>
												<!-- 附條件簽發餘額-->
												<th><spring:message code="LB.W0041" /></th>
											</tr>
										</thead>
										<tbody>
											<c:if test="${tableList.Table.size() > 0}">
												<c:forEach var="dataList" items="${tableList.Table}">
													<tr>
														<td class="text-center">${dataList.BONCOD}</td>
														<td class="text-right">${dataList.DPIBAL}</td>
														<td class="text-right">${dataList.TSFBAL_N870}</td>
														<td class="text-right">${dataList.LMTSFO}</td>
														<td class="text-right">${dataList.LMTSFI}</td>
														<td class="text-right">${dataList.RPBAL}</td>
													</tr>
												</c:forEach>
											</c:if>
											<c:if test="${tableList.Table.size() <= 0}">
												<tr>
													<td>${tableList.msgCode}</td>
													<td>${tableList.msgName}</td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
											</c:if>
										</tbody>
									</table>
								
								</c:forEach>
	                            <!-- 回上頁-->
	                            <input type="button" class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page"/>"/>
								<!-- 列印鈕-->					
								<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>	
								
	                            <!-- 繼續查詢 -->
<%-- 	                            <c:if test="${ loan_detail.data.TOPMSG != 'OKLR' && loan_detail.data.MSGCOD != 'OKLR'  }"> --%>
<!-- 		                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message> -->
<!-- 		                            <input type="button" name="CMCONTINU" id="CMCONTINU" value="繼續查詢" class="ttb-button btn-flat-orange"> -->
<%-- 	                            </c:if> --%>
	                            
							</div>
						</div>
						
					</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>