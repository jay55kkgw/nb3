<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 支存當日不足扣票據明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Insufficient_Balance_Of_Check_Deposits_Detail" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.Insufficient_Balance_Of_Check_Deposits_Detail" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 					下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 					下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 					下載為舊txt檔 -->
						<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
					</select>
				</div>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<form method="post" id="formId">
						<!-- 下載用 -->
						<input type="hidden" name="downloadFileName" value="LB.Insufficient_Balance_Of_Check_Deposits_Detail"/>
						<input type="hidden" name="downloadType" id="downloadType"/> 					
						<input type="hidden" name="templatePath" id="templatePath"/> 	
						<!-- EXCEL下載用 -->
						<input type="hidden" name="headerRightEnd" value="4"/>
						<input type="hidden" name="headerBottomEnd" value="9"/>
						<input type="hidden" name="rowStartIndex" value="10"/>
						<input type="hidden" name="rowRightEnd" value="2"/>

						<!-- TXT下載用 -->
						<input type="hidden" name="txtHeaderBottomEnd" value="12"/>
						<input type="hidden" name="txtHasRowData" value="true"/>
						<input type="hidden" name="txtHasFooter" value="false"/>
						
						<div class="ttb-input-block">
								<!-- 帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.Account" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
							           	<select name="ACN" id="ACN" class="custom-select select-input half-input">
							           		<option value=""><spring:message code="LB.Select_account" /></option>
								           	<c:forEach var="dataList" items="${checking_insufficient.data.REC}">
								           		<option value="${dataList.ACN}">${dataList.ACN}</option>
								           	</c:forEach>
							           	</select>
									</div>
								</span>
							</div>
						</div>	
					</form>
					<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />					
					<input type="button" class="ttb-button btn-flat-orange" id="pageshow" value="<spring:message code="LB.Display_as_web_page" />"/>								
					</div>
				</div>
					<div class="text-left">
						<ol class="description-list list-decimal">
							<p>
								<spring:message code="LB.Description_of_page" />
							</p>
							<li><spring:message code="LB.Checking_insufficient_P1_D1" /></li>
							<li>
								<spring:message code= "LB.Checking_insufficient_P1_D2-1" />
								<a target="_blank" href='${pageContext.request.contextPath}/public/N015_TXT.htm'>
									<spring:message code= "LB.Checking_insufficient_P1_D2-2" />
								</a> 
								<spring:message code= "LB.Checking_insufficient_P1_D2-3" />
							</li>
							<li>
								<spring:message code= "LB.Checking_insufficient_P1_D3-1" />
								<a target="_blank" href='${pageContext.request.contextPath}/public/N015_OLDTXT.htm'>
									<spring:message code= "LB.Checking_insufficient_P1_D3-2" />
								</a>
							</li>
						</ol>
					</div>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->	

	<%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
    
	    $(document).ready(function(){
	    	
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			
		});
	    	function init(){	 
	    		//acn
				var getacn = '${checking_insufficient.data.Acn}';
				if(getacn != null && getacn != ''){
					$("#ACN option[value= '"+ getacn +"' ]").prop("selected", true);
				}
				
				$("#pageshow").click(function(e){					
					e = e || window.event;
					
					if($('#ACN').val() == ""){
			        	e.preventDefault();
		 			}else{
		 				initBlockUI();
		 				$("#formId").attr("action","${__ctx}/NT/ACCT/checking_insufficient_details_result");
		 				$("#formId").submit(); 
		 			}		
		  		});
	    	}
	    
	    
			//選項
		 	function formReset() {
// 		 		if($('#ACN').val() == ""){
// 		 			return false;
// 		 		}
		 		if(!$("#formId").validationEngine("validate")){
					e = e || window.event;//forIE
					e.preventDefault();
				}
		 		else{
// 			 		initBlockUI();
					if ($('#actionBar').val()=="excel"){
						$("#downloadType").val("OLDEXCEL");
						$("#templatePath").val("/downloadTemplate/checking_insufficient.xls");
			 		}else if ($('#actionBar').val()=="txt"){
						$("#downloadType").val("TXT");
						$("#templatePath").val("/downloadTemplate/checking_insufficient.txt");
			 		}else if ($('#actionBar').val()=="oldtxt"){
						$("#downloadType").val("OLDTXT");
						$("#templatePath").val("/downloadTemplate/checking_insufficientOLD.txt");
			 		}
// 					ajaxDownload("${__ctx}/NT/ACCT/checking_insufficient_ajaxDirectDownload","formId","finishAjaxDownload()");
					$("#formId").attr("target", "");
	                $("#formId").attr("action", "${__ctx}/NT/ACCT/checking_insufficient_directDownload");
		            $("#formId").submit();
		            $('#actionBar').val("");
		 		}
			}

 	</script>
</body>
</html>
