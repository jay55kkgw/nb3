<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TBB-${pageContext.response.locale}.js"></script>

</head>
<script type="text/JavaScript">
$(document).ready(function() {
	init();
});
function init(){
	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	$("#CMSUBMIT").click(function(e){			
		console.log("submit~~");
		
	// 表單驗證
		if (!$('#formId').validationEngine('validate')) {
			e.preventDefault();
		} else {
			$("#formId").validationEngine('detach');
			initBlockUI();
			$("#formId").submit();
		}
	});
}
</script>
<body>
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- Email設定     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Email_Setting" /></li>>
	<!-- 電子信箱變更驗證     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2624" /></li>
		</ol>
	</nav>

	<div class="content row">
		<main class="col-12">
		<section id="main-content" class="container">
			<h2><spring:message code="LB.X2625" /></h2>
			<div id="step-bar">
					<ul>
						<li class="active"><spring:message code="LB.X2626" /></li>
						<li class=""><spring:message code="LB.X2627" /></li>
					</ul>
				</div>
			<form id="formId" action="${__ctx}${next}" method="post">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<!-- 主頁內容  -->
						<div class="ttb-input-block">
							<div class="ttb-message">
									<p><spring:message code="LB.X2628" /></p>
								</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4>
											<spring:message code="LB.X2629" />
										</h4></label></span>
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" id="VERIFYCODE" name="VERIFYCODE"
											class="text-input validate[required,funcCall[validate_CheckNumber['checknumber',VERIFYCODE]]]"
											maxlength="10" size="10"
											autocomplete="off">
									</div>
								</span>
							</div>
						</div>
						<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X2630" />" name="CMSUBMIT">
					</div>
				</div>
			</form>
		</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>