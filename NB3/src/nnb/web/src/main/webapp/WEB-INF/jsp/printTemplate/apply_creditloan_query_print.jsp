<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<label><spring:message code="LB.Inquiry_time" /> ：</label><label>${CMQTIME}</label>
<br/><br/>
<label><spring:message code="LB.Total_records" /> ：</label><label>${CMRECNUM}&nbsp;<spring:message code="LB.Rows"/></label>
<br/><br/>
<table class="print">
	<tr>
		<td style="text-align:center"><spring:message code="LB.Report_name" /></td>
		<td colspan="5"><spring:message code= "LB.D0654" /></td>
	</tr>
	<tr>
		<td style="text-align:center"><spring:message code="LB.Loan_CaseNo"/></td>
		<td style="text-align:center"><spring:message code= "LB.D0544" /></td>
		<td style="text-align:center"><spring:message code= "LB.D0660" /></td>
		<td style="text-align:center"><spring:message code= "LB.D0661" /></td>
		<td style="text-align:center"><spring:message code= "LB.D0662" /></td>
		<td style="text-align:center"><spring:message code= "LB.D0128" /></td>
	</tr>
	<c:forEach items="${dataListMap}" var="map">
	<tr>
		<td class="text-center">${map.RCVNO}</td>
		<td class="text-center"><spring:message code= "${map.APPKIND}" /></td>
		<td class="text-right">${map.APPAMT} <spring:message code= "LB.D0088_2" /></td>
		<td class="text-center">${map.PERIODS} <spring:message code= "LB.W0854" /></td>
		<td class="text-center">${map.BANKNA}</td>
		<td class="text-center"><spring:message code= "${map.STATUS}" /></td>
	</tr>
	</c:forEach>
</table>
<br/><br/><br/><br/>
</body>
</html>