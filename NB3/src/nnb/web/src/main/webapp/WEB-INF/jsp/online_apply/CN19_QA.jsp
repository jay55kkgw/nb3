<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

	<script type="text/javascript">
		$(function(){
			// 幫 #qaContent 的 ul 子元素加上 .accordionPart
			// 接著再找出 li 中的第一個 div 子元素加上 .qa_title
			// 並幫其加上 hover 及 click 事件
			// 同時把兄弟元素加上 .qa_content 並隱藏起來
			$('#qaContent ul').addClass('accordionPart').find('li div:nth-child(1)').addClass('qa_title').hover(function(){
				$(this).addClass('qa_title_on');
			}, function(){
				$(this).removeClass('qa_title_on');
			}).click(function(){
				// 當點到標題時，若答案是隱藏時則顯示它，同時隱藏其它已經展開的項目
				// 反之則隱藏
				var $qa_content = $(this).next('div.qa_content');
				if(!$qa_content.is(':visible')){
					$('#qaContent ul li div.qa_content:visible').slideUp();
				}
				$qa_content.slideToggle();
			}).siblings().addClass('qa_content').hide();
		});
	</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 隨護神盾     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0343" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 申請隨護神盾 -->
			<h2><spring:message code="LB.D1573"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12">
<!-- 						<div class="CN19-1-header"> -->
<!-- 							<div class="logo"> -->
<%-- 								<img src="${__ctx}/img/logo-1.svg"> --%>
<!-- 							</div> -->
<!-- 							常用網址超連結 -->
<!-- 							<div class="text-right hyperlink"> -->
<!-- 							臺灣企銀首頁 -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw');" style="text-decoration: none"> <spring:message code="LB.TAIWAN_BUSINESS_BANK"/> </a> <strong><font color="#e65827">|</font></strong>  --%>
<!-- 								網路ATM -->
<%-- 								<a href="#" onClick="window.open('https://eatm.tbb.com.tw/');" style="text-decoration: none"> <spring:message code="LB.Web_ATM"/> </a><strong><font color="#e65827">|</font></strong> 	 --%>
<!-- 								意見信箱  -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw/q3.html?');" style="text-decoration: none"> <spring:message code="LB.Customer_service"/> </a> --%>
<!-- 							</div> -->
<!-- 						</div> -->
						<!-- 數位存款帳戶補申請晶片金融卡 -->
						<br><br>
						<div id="qaContent">
							<ul>
								<li>
									<div>Q1. 什麼是隨護神盾？</div>
									<div>隨護神盾是結合智慧型裝置進行身分認證，客戶透過輸入密碼確認交易，隨時隨地帶著行動裝置都能輕鬆完成交易。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q2. 隨護神盾有哪些功能？</div>
									<div>
										提供客戶於行動銀行使用下列服務：<br> 
										一、非約定轉帳<br> 
										二、台灣Pay行動支付<br>
										三、無卡提款功能<br> 
										四、繳納學雜費及企業代收費用
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q3. 下載密碼是做什麼用的？</div>
									<div>綁定行動裝置時，用於行動銀行下載憑證時使用。</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q4. 隨護密碼是做什麼用的？</div>
									<div>下載憑證時所設定的密碼，用於行動銀行透過隨護神盾進行交易驗證時使用。</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q5. 申請資格？</div>
									<div>已申請本行一般網路銀行並開通行動銀行，且有設定轉出帳號之客戶。</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q6. 如何申請？</div>
									<div>
										一、 本行線上申請專區：持本人本行晶片金融卡至申請頁面申請，並於48小時內至行動銀行進行啟用。(限自然人)<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(一) 選擇申請「隨護神盾」<BR> 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(二) 自行設定6~8位數「下載密碼」<BR> 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(三) 插入本行晶片金融卡並輸入金融卡密碼進行驗證<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(四)登入行動銀行選擇「隨護神盾」、輸入「下載密碼」並設定「隨護密碼」，下載憑證成功後即完成啟用程序<BR> 
										二、一般網路銀行：登入一般網路銀行，選擇「申請/掛失服務」，點選「申請隨護神盾」，搭配本人本行晶片金融卡驗證申請，並於48小時內至行動銀行進行啟用。(限自然人)<BR>
										三、 臨櫃：親攜身分證明文件與原留印鑑，洽原網銀開戶行辦理(註)，並於30天內至行動銀行進行啟用。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q7. 如何終止隨護神盾服務？</div>
									<div>
										一、 臨櫃：親攜身分證明文件與原留印鑑洽原網銀開戶行辦理(註)。<BR> 
										二、客服人員：撥打客服專線0800-01-7171、02-2357-7171按3(手機請撥付費電話)，洽客服人員終止(限自然人)。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q8. 更換或遺失手機時，應如何處理？</div>
									<div>
										更換手機時，透過舊手機設定更換設備下載密碼，再以新的手機依程序下載即可。<BR>
										若手機遺失欲終止隨護神盾服務時，可於原網銀開戶行(註)或洽客服人員(限自然人)進行終止。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q9. 隨護神盾連續錯誤之限制？</div>
									<div>
										為防止有心人士隨意測試，「下載密碼」連續錯誤5次時密碼將失效，需重新申請隨護神盾。「隨護密碼」連續錯誤5次時將自動終止服務。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q10. 隨護神盾下載密碼超過時效後該如何處理？</div>
									<div>重新於本行線上申請專區(限自然人)、一般網路銀行(限自然人)或原網銀開戶行辦理(註)申請即可。</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q11. 終止隨護神盾後是否可以重新申請？</div>
									<div>
										終止隨護神盾服務後，可於本行線上申請專區(限自然人)、一般網路銀行(限自然人)或原網銀開戶行重新申請(註)。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q12. 忘記下載密碼、隨護密碼或密碼連續錯誤5次時該怎麼辦？</div>
									<div>
										一、	忘記下載密碼時:可直接於本行線上申請專區(限自然人)、一般網路銀行(限自然人)或原網銀開戶行重新申請(註)。<BR>
										二、	忘記隨護密碼或密碼連續錯誤5次時:<BR> 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(一)	如憑證效期尚未逾有效期限，需終止隨護神盾服務後再重新申請(詳見Q7.如何終止隨護神盾服務?)。<BR> 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(二)	如憑證已超過有效期限，可直接於本行線上申請專區(限自然人)、一般網路銀行(限自然人)或網銀開戶行重新申請(註)<BR> 
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q13. 每個人可以申請幾個隨護神盾？</div>
									<div>隨護神盾以身分證字號或統一編號歸戶，每個人只能綁定一個行動裝置。</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q14. 申請/更新隨護神盾需要費用嗎？</div>
									<div>
										免收憑證年費，歡迎多加利用。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q15. 隨護神盾進行非約定帳戶轉帳之交易有金額限制嗎？</div>
									<div>
										一、 預設限額：每筆新臺幣5萬元、每日新臺幣10萬元、每月新臺幣20萬元。<BR> 
										二、此限額與晶片金融卡及動態密碼卡之轉帳交易合併計算。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q16. 隨護神盾有使用時間的限制嗎？</div>
									<div>
										申請隨護神盾，使用期限自下載之日起算一年。隨護神盾到期前一個月內，系統自動以eMail提示展期訊息。客戶須於期限內辦理更新，逾有效期限導致更新不成功時，請重新申請隨護神盾服務。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q17. 我收到隨護神盾憑證即將到期通知信，要如何更新隨護神盾？</div>
									<div>
										請於憑證到期前一個月內登入本行行動銀行，點選「隨護神盾」並依指示辦理憑證更新。
									</div>
								</li>
							</ul>
							註：個人戶已申請全行收付者可至臺灣企銀任一分行辦理，法人戶限於開戶行辦理。
						</div>
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>