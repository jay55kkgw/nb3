<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 登出前的遮罩，詢問是否繼續使用 -->
<section id="logout-block" class="error-block" style="display:none">
    <div class="error-for-message">
    	<!-- 自動登出提醒 -->
        <p class="error-title"><spring:message code= "LB.X1552" /></p>
        <p class="error-content"><spring:message code= "LB.X1553" /><br /><spring:message code= "LB.X1554" /></p>
        <!-- 登出倒數 -->
        <p class="error-info"><img class="error-icon" src="${__ctx}/img/clock-circular-outline.svg?a=${jscssDate}" /><spring:message code= "LB.X1912" />
        	<span class="high-light pl-2">
        		<font id="countdownheader"></font><spring:message code= "LB.Second" />
			</span>
        </p>
        <!-- 繼續使用 -->
        <button class="btn-flat-orange ttb-pup-btn" name="CMCONTINUE" id="CMCONTINUE" onClick="keepLogin();">
        	<spring:message code= "LB.X1555" />
        </button>
        <!-- 登出 -->
        <button class="btn-flat-orange ttb-pup-btn" name="CMLOGOUT" id="CMLOGOUT" 
        	onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login');">
        	<spring:message code= "LB.Logout" />
        </button>
    </div>
</section>

<div id="desk-header" class="row" onmouseenter="meun_close()">
	<div class="header-logo col-4">
		<a href="#" onclick="loginhome();"><img src="${__ctx}/img/tbb-logo-onlinebanking.svg?a=${jscssDate}"></a>
<%-- 		<span><spring:message code="LB.Internet_banking"/></span> --%>
	</div>
	<div class="col-8 text-right">
	
		<!-- <a href="#">臺灣企銀首頁</a> -->
		<c:choose>
			<c:when test="${__i18n_locale eq 'en' }">
					<a href="https://www.tbb.com.tw/web/guest/english2" target="_blank" role="button">
                  		<spring:message code="LB.TAIWAN_BUSINESS_BANK"/>
                  	</a>
			</c:when>
			<c:when test="${__i18n_locale eq 'zh_TW'}">
	  			<a href="https://www.tbb.com.tw/" target="_blank" role="button">
                   	<spring:message code="LB.TAIWAN_BUSINESS_BANK"/>
                   </a>
			</c:when>
			<c:otherwise>
		   		<a href="https://www.tbb.com.tw/" target="_blank" role="button">
                   	<spring:message code="LB.TAIWAN_BUSINESS_BANK"/>
                   </a>
			</c:otherwise>
		</c:choose>
		
		<!-- 憑證管理 -->
		<div class="dropdown">
			<span><spring:message code="LB.X2148"/></span>
			<div class="dropdown-content">
				<ul>
					<c:if test="${sessionScope.isCertificationRegistery eq 'Y'}"><li class="noClass" ><a href="#"><spring:message code="LB.X2149"/></a></li></c:if>
					<li><a href="${__ctx}/TRUST/changepw"><spring:message code="LB.X2150"/></a></li>
				</ul>
			</div>
		</div>
		
		<!-- 網站導覽 -->
		<a href="${__ctx}/CUSTOMER/SERVICE/site_login" target="_self"><spring:message code="LB.Service_overview"/></a>
		
		<!-- 多語系選項 -->
		<select class="login-custom-select select-input half-input" name="selectLg" id="selectLg" onchange="location = this.value" style="display:none;">
<!-- 			<option value="0">Language</option> -->
<!-- 			<option value="1">繁體</option> -->
<!-- 			<option value="2">英文</option> -->
<!-- 			<option value="3">殘體</option> -->
		</select>
		
	</div>
	
</div>
<div id="mobile-header">
<!-- 	<a href="#fast-menu-block" data-toggle="collapse" data-parent="#fast-menu" aria-expanded="true" aria-controls="fast-menu-block" role="button"> -->
<%-- 		<img src="${__ctx}/img/group-2.svg?a=${jscssDate}"> --%>
<!-- 	</a> -->
	<img class="mobile-logo" src="${__ctx}/img/logo-white.png?a=${jscssDate}">
	<!-- 小版多語系 --> 	
<!-- 	<select id="selectLg2"  onchange="location = this.value">			   -->
<!-- 	</select> -->
<!-- 	<a href="#" role="button"> -->
<%-- 		<img src="${__ctx}/img/menu.svg?a=${jscssDate}"> --%>
<!-- 	</a> -->
	<!-- for mobile menu -->
	<button class="hamburger hamburger--spring" type="button" href="#header-content" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="header-content">
    	<span class="hamburger-box">
        	<span class="hamburger-inner"></span>
        </span>
   	</button>
<%-- 	<a data-toggle="collapse" href="#header-content" role="button" aria-expanded="false" aria-controls="header-content"><img src="${__ctx}/img/menu.svg?a=${jscssDate}"></a> --%>


</div>
<c:if test="${sessionScope.cusidn != null}">
<div id="big_menu">
	<%@ include file="../index/big-menu.jsp"%>
</div>
</c:if>

<!-- LOADING遮罩區塊改放到__import.jsp -->
<!-- <div id="loadingBox" class="Loading_box"> -->
<!--     <div class="sk-fading-circle"> -->
<!--       <div class="sk-circle1 sk-circle"></div> -->
<!--       <div class="sk-circle2 sk-circle"></div> -->
<!--       <div class="sk-circle3 sk-circle"></div> -->
<!--       <div class="sk-circle4 sk-circle"></div> -->
<!--       <div class="sk-circle5 sk-circle"></div> -->
<!--       <div class="sk-circle6 sk-circle"></div> -->
<!--       <div class="sk-circle7 sk-circle"></div> -->
<!--       <div class="sk-circle8 sk-circle"></div> -->
<!--       <div class="sk-circle9 sk-circle"></div> -->
<!--       <div class="sk-circle10 sk-circle"></div> -->
<!--       <div class="sk-circle11 sk-circle"></div> -->
<!--       <div class="sk-circle12 sk-circle"></div> -->
<!--     </div> -->
<!-- </div> -->
<script type="text/JavaScript">
 		function i18n(){
 			// 動態加入多語系下拉選單
  			var locale = "${__i18n_locale}";
  			if(locale == "zh_TW"){
   				$("[name='selectLg']").append($("<option></option>").attr("value", "${__i18n_en}").text("English"));
   				$("[name='selectLg']").append($("<option></option>").attr("value", "${__i18n_zh_TW}").text("繁體"));
   				$("[name='selectLg']").append($("<option></option>").attr("value", "${__i18n_zh_CN}").text("简体"));
   				$("[name='selectLg']").val('${__i18n_zh_TW}');
   				
  			}else if(locale == "zh_CN"){
   				$("[name='selectLg']").append($("<option></option>").attr("value", "${__i18n_en}").text("English"));
   				$("[name='selectLg']").append($("<option></option>").attr("value", "${__i18n_zh_TW}").text("繁體"));
   				$("[name='selectLg']").append($("<option></option>").attr("value", "${__i18n_zh_CN}").text("简体"));
  			 	$("[name='selectLg']").val('${__i18n_zh_CN}');
  			 	
  			}else if(locale == "en"){
   				$("[name='selectLg']").append($("<option></option>").attr("value", "${__i18n_en}").text("English"));
   				$("[name='selectLg']").append($("<option></option>").attr("value", "${__i18n_zh_TW}").text("繁體"));
   				$("[name='selectLg']").append($("<option></option>").attr("value", "${__i18n_zh_CN}").text("简体"));
   				$("[name='selectLg']").val('${__i18n_en}');
  			}
  			
  			$("[name='selectLg']").show();
 		}
 		
 		// Look for .hamburger
 		var hamburger = document.querySelector(".hamburger");
 		// On click
 		hamburger.addEventListener("click", function() {
	 		// Toggle class "is-active"
	 		hamburger.classList.toggle("is-active");
	 		// Do something else, like open/close menu
 		});
 		
    	var isCertificationRegistery = '${sessionScope.isCertificationRegistery}';
    	if("Y" == isCertificationRegistery){
    		
		    var noClass = document.querySelector(".noClass");
		    
		    noClass.addEventListener("click", function() {
		        uri = '${__ctx}'+"/INDEX/certificate_url_aj"
		        rdata = {}
		        bs = fstop.getServerDataEx(uri,rdata,false);
		        if(bs.result){
		        	var a=bs.data;
		        	var b=a.substr(a.split('?',1)[0].length+1);
		        	var cerUrl=a.split('?',1)[0];
		        	var param=b.substr(b.split('=',1)[0].length+1);//param
		        	// 建立Form  
		            var form = $('<form id="certCenter"></form>');  
		            // 設定屬性  
		            form.attr('action',cerUrl);  
		            form.attr('method', 'post');  
		            // form的target屬性決定form在哪個頁面提交 
		            // _self -> 當前頁面 _blank -> 新頁面   
		            form.attr('target', '_blank');  
		            // 建立Input  
		            var my_input = $('<input type="text" name="authcode" />');  
		            my_input.attr('value', param);  
		            // 附加到Form  
		            form.append(my_input);
		            $(document.body).append(form);
		            // 提交表單  
		            form.submit();
		            $("#certCenter").remove();
		        }else{
		            console.log("產生authCode異常...."+bs.message)
		            window.open(bs.data);
		        }
		    });
    	}
 		
 		
 		
 		//判斷LOGO是否觸發，依登入狀況導回首頁
 		function loginhome(){
 			var loginid = '${sessionScope.cusidn}';
 			if(loginid != null && loginid != ""){
 				window.location.href='${__ctx}/INDEX/index';
 			}
 			else{
 				window.location.href='${__ctx}/login';
 			}
 		}
</script>