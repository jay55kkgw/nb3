<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<meta http-equiv="Content-Language" content="zh-tw">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 通知查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0081" /></li>
		</ol>
	</nav>



	<!-- 	快速選單及主頁內容 -->
	<!-- menu、登出窗格 --> 
 	<div class="content row">
 		 <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->	
		<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W0081" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
						<!-- 下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
						<!--下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				<div class="main-content-block row">
					<div class="col-12"> 						
						<form id="formId" method="post" action="${__ctx}/FCY/ACCT/f_credit_letter_query_result">
						<!-- 下載用 -->
 						<!-- 	TODO downloadFileName	要改為信用狀名稱 -->
						<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0081" />"/>
						<input type="hidden" name="CMQTIME" value="${credit_letter_query_result.data.CMQTIME}"/>
						<input type="hidden" name="CMPERIOD" value="${credit_letter_query_result.data.CMPERIOD}"/>
						<input type="hidden" name="sBAL"value="${credit_letter_query_result.data.sBAL}" />
						<input type="hidden" name="downloadType" id="downloadType"/> 					
						<input type="hidden" name="CMRECNUM" value="${credit_letter_query_result.data.CMRECNUM}"/>
						<input type="hidden" name="templatePath" id="templatePath"/> 
						<!-- EXCEL下載用 -->
						<input type="hidden" name="headerRightEnd" value="11" /> 
						<input type="hidden" name="headerBottomEnd" value="8" /> 
						<input type="hidden" name="rowStartIndex" value="9" /> 
						<input type="hidden" name="rowRightEnd" value="11" />
						
						<input type="hidden" name="footerStartIndex" value="11" />
   	   		            <input type="hidden" name="footerEndIndex" value="12" />
    	               	<input type="hidden" name="footerRightEnd" value="2" />
						<!-- TXT下載用 -->
						<input type="hidden" name="txtHeaderBottomEnd" value="9"/>
						<input type="hidden" name="txtHasRowData" value="true"/>
						<input type="hidden" name="txtHasFooter" value="true"/>
							<div class="ttb-input-block">
								<!-- 查詢區間區塊 -->
								<div class="ttb-input-item row">
									<!--查詢區間  -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Inquiry_period" /></h4>
										</label>
									</span>
									<span class="input-block">
										<!--  開狀日修狀日 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="QKIND" value="1" checked/>
												<label><spring:message code="LB.W0083" /></label>
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block">
												<input type="radio" name="QKIND" value="2" />
												<label><spring:message code="LB.W0084" /></label>
												<span class="ttb-radio"></span>
											</label>
										</div>							
										<!--期間起日 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.D0013" />
											</span>
											<input type="text" id="FDATE" name="FDATE" class="text-input datetimepicker" maxlength="10" value="${cd_letter_query.data.TODAY}" /> 
				 							<span class="input-unit FDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
										</div>
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_end_date" />
											</span>
											<input type="text" id="TDATE" name="TDATE" class="text-input datetimepicker" maxlength="10" value="${cd_letter_query.data.TODAY}" /> 
											<span class="input-unit TDATE ">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
										</div>
										<!--驗證用的span預設隱藏 -->
										<span id="hideblock_CheckDateScope" >
											<!-- 驗證用的input -->
											<input id="odate" name="odate" type="text" value="${cd_letter_query.data.TODAY}" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, FDATE ,TDATE, false,12,null]]]" style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</span>
								</div>
								<!-- 信用狀號碼區塊 -->
								<div class="ttb-input-item row">
									<!--信用狀號碼  -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0085" /></h4>
										</label>								
									</span>
									<!--信用狀號碼  -->
									<span class="input-block"> 
										<input type="text" id="LCNO" name="LCNO" class="text-input" maxlength="20" value="" /> 
										<br>
										<!--空白表示查詢全部--> 
										<span class="input-unit"><spring:message code="LB.W0087" /></span>
									</span>
								</div>
								<!-- 通知編號區塊 -->
								<div class="ttb-input-item row">
									<!--通知編號  -->
									<span class="input-title">
									<label>
										<h4><spring:message code="LB.W0086" /></h4>
									</label>
									</span>
									<span class="input-block">
										<input type="text" id="REFNO" name="REFNO" class="text-input" maxlength="20" value="" />
										<br>
										<!--空白表示查詢全部-->
               		                 	<span class="input-unit"><spring:message code="LB.W0087" /></span>
									</span>
								</div>						
							</div>
						</form>
						<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
							<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
							<spring:message code="LB.Confirm" var="cmConfirm"></spring:message>
							<input type="button"  name="CMSUBMIT" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="${cmConfirm}"/>
					</div>
				</div>
				<div class="text-left">				
					<ol class="description-list list-decimal text-left">
					    <p><spring:message code="LB.Description_of_page" /> </p>
						<li><span><spring:message code="LB.F_Credit_Letter_Query_P1_D1" /></span></li>
						<li><span><spring:message code="LB.F_Credit_Letter_Query_P1_D2" /></span></li>
						<li><span><spring:message code="LB.F_Credit_Letter_Query_P1_D3" /></span></li>
					</ol>
				</div>
			</section>
			<!-- main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		// 初始化時隱藏span
		$("#hideblocka").hide();
		$("#hideblockb").hide();
		
		datetimepickerEvent();
		
// 		getTmr();
		
		initFootable();
		
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});	
		$("#CMSUBMIT").click(function(e){
			if($("#LCNO").val()!=null){
				if(($("#FDATE").val()==$("#TDATE").val()) && ($("#FDATE").val()==$("#odate").val())){
					$("#odate").removeClass("validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, FDATE ,TDATE, false,12,null]]]");
	 				initBlockUI();
	 				$("#formId").attr("target", "");
	 				$("#formId").attr("action","${__ctx}/FCY/ACCT/f_credit_letter_query_result");
	 	  			$("#formId").submit();
				}
			}
			e = e || window.event;
			if( $('#CMPERIOD').prop('checked') )
			{
				if(checkTimeRange() == false )
				{
					return false;
				}
			}
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
 				console.log("main submit");
 				$("#formId").validationEngine('detach');
 				//initBlockUI();
 				$("#formId").attr("target", "");
 				$("#formId").attr("action","${__ctx}/FCY/ACCT/f_credit_letter_query_result");
 	  			$("#formId").submit(); 
 			}		
  		});
		$("#CMRESET").click(function(e){
			console.log("CMRESET submit");
			$("#formId")[0].reset();
			getTmr();
			});
	}
	
	function getTmr() {
		var today = "${cd_letter_query.data.TODAY}";
		$('#FDATE').val(today);
		$('#TDATE').val(today);
		$('#odate').val(today);
	}
    
	// 日曆欄位參數設定
	function datetimepickerEvent() {
		$(".FDATE").click(function (event) {
			$('#FDATE').datetimepicker('show');
		});
		$(".TDATE").click(function (event) {
			$('#TDATE').datetimepicker('show');
		});
		jQuery('.datetimepicker').datetimepicker({
			timepicker: false,
			closeOnDateSelect: true,
			scrollMonth: false,
			scrollInput: false,
			format: 'Y/m/d',
			lang: '${transfer}'
		});
	}	
	
	function checkTimeRange()
	{
		var now = Date.now();
		var twoYm = 63115200000;
		var twoMm = 5259600000;
		
		var startT = new Date( $('#FDATE').val() );
		var endT = new Date( $('#TDATE').val() );
		var distance = now - startT;
		var range = endT - startT;
		
		var limitS = new Date(now - twoYm);
		var limitE = new Date(startT.getTime() + twoMm); 
		if(distance > twoYm)
		{
			var m = limitS.getMonth() + 1;
			var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
			// 起始日不能小於
			var msg = '<spring:message code="LB.Start_date_check_note_1" />' + time;
			//alert(msg);
			return false;
		}
		else
		{
			if(range > twoMm)
			{
				var m = limitE.getMonth() + 1;
				var time = limitE.getFullYear() + '/' + m + '/' + limitE.getDate();
				// 終止日不能大於
				var msg = '<spring:message code="LB.End_date_check_note_1" />' + time;
				//alert(msg);
				return false;
			}
		}
		return true;
	}
	//選項，之後來做
	 	function formReset() {
	 		//打開驗證隱藏欄位
			$("#hideblocka").show();
			$("#hideblockb").show();
			//塞值進span內的input
			$("#Monthly_DateA").val($("#FDATE").val());
			$("#Monthly_DateB").val($("#TDATE").val());
			
			if( $('#CMPERIOD').prop('checked') )
			{
				if(checkTimeRange() == false )
				{
					return false;
				}
			}
			if(!$("#formId").validationEngine("validate")){
				e = e || window.event;//forIE
				e.preventDefault();
			}
	 		else{
		 		//initBlockUI();
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/f_credit_letter.xls");
		 		}else if ($('#actionBar').val()=="txt"){
		 			$("#downloadType").val("TXT");
		 			$("#templatePath").val("/downloadTemplate/f_credit_letter.txt");
		 		}
				//ajaxDownload("${__ctx}/FCY/ACCT/f_credit_letter_query_ajaxDirectDownload","formId","finishAjaxDownload()");
				$("#formId").attr("target", "");
				$("#formId").attr("action", "${__ctx}/FCY/ACCT/f_credit_letter_query_ajaxDirectDownload");
				$("#formId").submit();
				$('#actionBar').val("");
	 		}
		}
 	</script>
</body>
</html>
