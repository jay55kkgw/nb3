<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta content="" name="description">
    <meta content="臺灣中小企業銀行" name="author">
    <title>臺灣中小企業銀行 - 全國性繳費平台線上約定作業</title>
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/bootstrap.min.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/full-width-pics.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/e-style.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/all.css" rel="stylesheet">
</head>
<body class="page">
    <header>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg fixed-top">
            <div class="container">
                <div class="col-12 text-center">
                   <a class="navbar-brand" href=""><img src="${__ctx}/ebillApply/img/logo_tbb.svg"></a>
                </div>
            </div>
        </nav>
        <!-- Header section -->
    </header>
    <main class="px-3 px-sm-0 py-2">
        <div class="container mt-3 mt-sm-3">
            <form id="validationForm" method="post" action="${accountBinding_result.data.BackUrl}">
            <input type="hidden" name="IBPD_Param" value='${accountBinding_result.data.IBPD_Param}'>
            <div class="row">
                <div class="col-md-12 py-sm-4">
                    <div class="progressbar text-center">
                        <ul class="list-inline">
                            <li class="list-inline-item done"><span>01</span></li>
                            <li class="list-inline-item done"><span>02</span></li>
                            <li class="list-inline-item current"><span>03</span><step>約定結果</step></li>   
                        </ul>
                    </div>
                </div>  
            </div>
        </div>
        <!--約定成功 start-->
        <div class="container form-table mt-3 mt-sm-3" id="bind-success">
            <div class="row">
                <div class="col-md-12 main-content pt-4 py-sm-4 px-sm-5 mx-auto text-center">
                    <img src="${__ctx}/ebillApply/img/icon-success.svg" alt="" class="img-fluid mt-3">
                    <p class="txt-green mt-2">約定成功</p>
                    <p class="error-text mt-4">您的帳號<span class="user-information">${accountBinding_result.data.BindingAccount}</span>已經成功約定。</p>
                </div>
            </div>
            <div class="row action-container">
                <div class="col-sm-12 mt-5">
                    <div class="float-right">
                        <a href="#" id="submitbtn" class="btn btn-primary submit">返回約定平台</a>
                    </div>
                </div>
             </div>
        </div> 
        <!--約定成功 over-->
    </main>
    <footer class="py-sm-0 d-flex align-items-center align-middle">
		<div class="container">
			<div class="row d-flex align-items-center align-middle">
				<div class="col-12 mt-3 mt-sm-0 pr-sm-0 text-center order-last order-sm-first">
					<h6 class="copyright mb-0">©臺灣中小企業銀行</h6>
					<span class="small"><a href="https://www.tbb.com.tw/web/guest/-173" target="_blank">隱私權聲明</a> | <a href="https://www.tbb.com.tw/web/guest/-551" target="_blank">安全政策</a></span>
				</div>
				<div class="col-12 mt-3 mt-sm-0 p-0 text-center d-sm-none">
					<p class="mt-sm-4">24H服務專線 <a href="tel:0800-01-7171">0800-01-7171</a>(限市話), <a href="tel:02-2357-7171">02-2357-7171</a></p>
				</div>
				<div class="col-12 pt-0 footer-sns-link">
					<ul class="list-inline ml-auto mb-sm-0 text-center">
						<li class="list-inline-item d-none d-sm-inline-block mr-2">
							<p>24H服務專線 <a href="tel:0800-01-7171">0800-01-7171</a>(限市話), <a href="tel:02-2357-7171">02-2357-7171</a></p>
						</li>
						<li class="list-inline-item mr-2">
							<a href="https://www.facebook.com/tbbdreamplus/" target="_blank"><i class="fab fa-facebook"></i></a>
						</li>
						<li class="list-inline-item">
							<a href="https://www.youtube.com/channel/UCyRmUHjcJV3ROmXrF7q3xOA" target="_blank"><i class="fab fa-youtube"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
    <!-- Bootstrap core JavaScript -->
    <script src="${__ctx}/ebillApply/js/jquery.min.js"></script>
    <script src="${__ctx}/ebillApply/js/bootstrap.bundle.min.js"></script> 
    <script type="text/javascript">
	$(document).ready(function () {
    	//註冊按鈕
        $("#submitbtn").click(function () {
      		$("#validationForm").submit();
        });
	});
	</script>
</body>
</html>

