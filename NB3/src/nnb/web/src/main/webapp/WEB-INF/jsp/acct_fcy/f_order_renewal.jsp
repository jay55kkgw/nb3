<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript">
	$(document).ready(
	    function() {
	    	initDataTable(); // 將.table變更為DataTable
	        // 確定按鈕事件
	        $("#CMSUBMIT").click(
	            function() {
	                var radioflag = $('input[type=radio][name=FGSELECT]').is(':checked');
	                console.log('radioflag :' + radioflag);
	                if (!radioflag) {
	                    //錯誤!!尚未選取資料
	                    //alert('<spring:message code="LB.Field_check_note"/>');
	                	errorBlock(
								null, 
								null,
								['<spring:message code='LB.Field_check_note'/>'], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
	                } else {
	                    initBlockUI(); //遮罩
	                    $("#formId").submit();
	                }
	            });
	    });
	</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存單到期續存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Renewal_At_Maturity_Of_FX_Time_Deposit" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- content row END -->
	<!-- 主頁內容  -->
	<main class="col-12">
	<section id="main-content" class="container">
		<h2>
			<!--  外匯定存單到期續存 -->
			<spring:message code="LB.Renewal_At_Maturity_Of_FX_Time_Deposit" />
		</h2>
		<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
		<form id="formId" method="post"
			action="${__ctx}/FCY/ACCT/TDEPOSIT/f_order_renewal_step1">
			<c:set var="BaseResultData" value="${f_order_renewal.data}"></c:set>
			<!-- 表單顯示區  -->
			<div class="main-content-block row">
				<div class="col-12">
					<ul class="ttb-result-list">
						<!-- 查詢時間 -->
						<li>
							<h3><spring:message code="LB.Inquiry_time" />：</h3><p>${BaseResultData.CMQTIME }</p>
						</li>
						<li>
								<!-- 資料總數 : -->
							<h3><spring:message code="LB.Total_records" />：</h3><p>${BaseResultData.CMRECNUM }
								<!-- 筆 -->
								<spring:message code="LB.Rows" />
							</p>
						</li>
					</ul>
					<!-- 表格區塊 -->
					<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
						<thead>
							<tr>
								<!-- 點選 -->
								<th data-title="<spring:message code="LB.Option" />"><spring:message code="LB.Option" /></th>
								<!-- 帳號 -->
								<th data-title="<spring:message code="LB.Account" />"><spring:message code="LB.Account" /></th>
								<!--存單號碼 --><!-- 存款種類 -->
								<th data-title="<spring:message code="LB.Certificate_no" /><hr><spring:message code="LB.Deposit_type" />"><spring:message code="LB.Certificate_no" /><hr><spring:message code="LB.Deposit_type" /></th>
								<!--存單幣別 --><!--存單金額 -->
								<th data-title="<spring:message code="LB.Certificate_currency" /><hr><spring:message code="LB.Certificate_amount" />"><spring:message code="LB.Certificate_currency" /><hr><spring:message code="LB.Certificate_amount" /></th>
								<!--利率(%) -->
								<th data-title="<spring:message code="LB.Interest_rate1" />"><spring:message code="LB.Interest_rate1" /></th>
								<!--計息方式 -->
								<th data-title="<spring:message code="LB.Interest_calculation" />"><spring:message code="LB.Interest_calculation" /></th>
								<!--起存日 --><!--到期日 -->
								<th data-title="<spring:message code="LB.Start_date" /><hr><spring:message code="LB.Maturity_date" />"><spring:message code="LB.Start_date" /><hr><spring:message code="LB.Maturity_date" /></th>
								<!--利息轉入帳號 -->
								<th data-title="<spring:message code="LB.Interest_transfer_to_account" />"><spring:message code="LB.Interest_transfer_to_account" /></th>
								<!--自動轉期已轉次數 --><!--自動轉期未轉次數 -->
								<th data-title="<spring:message code="LB.Automatic_number_of_rotations" /><hr><spring:message code="LB.Automatic_number_of_unrotated" />"><spring:message code="LB.Automatic_number_of_rotations" /><hr><spring:message code="LB.Automatic_number_of_unrotated" /></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="dataList" items="${ BaseResultData.REC }">
								<tr>
									<td class="text-center">
										<label class="radio-block">
			                    			&nbsp; 
											<input type="radio" name="FGSELECT" value='${dataList.JSONSTR}' />
			                                <span class="ttb-radio"></span> 
			                             </label> 
                                   	</td>
									<!-- 帳號 -->
									<td class="text-center">${dataList.ACN }</td>
									<!-- 存單號碼--><!-- 存款種類 -->
									<td class="text-center">${dataList.FDPNO }<hr><spring:message code= "LB.NTD_deposit_type_1" /></td>
									<!-- 存單幣別 --><!-- 存單金額 -->
									<td class="text-center">${dataList.CUID }<hr>${dataList.BALANCE }</td>
									<!-- 利率(%) -->
									<td class="text-right">${dataList.ITR }</td>
									<!-- 計息方式 -->
									<td class="text-center">${dataList.INTMTH }</td>
									<!-- 起存日 --><!-- 到期日-->
									<td class="text-center">${dataList.SHOWDPISDT }<hr>${dataList.SHOWDUEDAT }</td>
									<!-- 利息轉入帳號 -->
									<td class="text-center">${dataList.TSFACN }</td>
									<!-- 自動轉期已轉次數< --><!-- 自動轉期未轉次數 -->
									<td class="text-center">${dataList.ILAZLFTM }<hr>${dataList.AUTXFTM }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<!-- button -->
<!-- 						重新輸入 -->
<%-- 						<spring:message code="LB.Re_enter" var="cmRest"></spring:message> --%>
<%-- 						<input class="ttb-button btn-flat-gray" type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" > --%>
						<!-- 確定 -->
						<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
						<input class="ttb-button btn-flat-orange" type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" >
					<!-- button -->
				</div>
			</div>
			<div class="text-left">
				<!-- 說明： -->
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>				
					<li>
						<strong style="font-weight: 400">
							<spring:message code="LB.F_order_renewal_P1_D1-1"/>
							<a href="https://www.tbb.com.tw/web/guest/-84" target="_blank">
								<!-- 外幣存款利率查詢 -->
								<spring:message code="LB.F_order_renewal_P1_D1-2"/>
							</a>
						</strong>
					</li>
					<li><strong style="font-weight: 400"><spring:message code="LB.F_order_renewal_P1_D2" /></strong></li>
					<li><strong style="font-weight: 400"><spring:message code="LB.F_order_renewal_P1_D3" /></strong></li>
					<li><strong style="font-weight: 400"><spring:message code="LB.F_order_renewal_P1_D4" /></strong></li>
				</ol>
			</div>
		</form>
	</section>
	</main>
	<!-- main-content END --> 
	<%@ include file="../index/footer.jsp"%>
</body>

</html>