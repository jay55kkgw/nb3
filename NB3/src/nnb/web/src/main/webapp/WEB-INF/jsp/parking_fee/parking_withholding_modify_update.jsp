<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 停車費     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.D0330" /></li>
    <!-- 資料維護     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0285" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.X0285"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
<!-- 			<p style="text-align: center;color: red;">請確認申請資料</p> -->
			<form method="post" id="formId">
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${result_data.TOKEN}" /><!-- 防止重複交易 -->
				
                <!-- ikey -->
				<input type="hidden" id="PINNEW" name="PINNEW" value="">
			    <input type="hidden" name="CardType" value="B">
			    <input type="hidden" name="NeedConfirm" value="0">
			    <input type="hidden" name="MailFlag" value="1">
			    <input type="hidden" name="SMSFlag" value="0">
			    <input type="hidden" name="CardValidDate" value=" ">
			    <input type="hidden" name="CardCVC2" value=" ">
			    <input type="hidden" name="TxnCode" value="005">
			    <input type="hidden" name="Name" value=" ">
    			<input  type="hidden" name="CardType" value="B">
    			<input  type="hidden" name="Count" value="0001">
     			<input type="hidden" name="NeedSHA1">
    			<input  type="hidden" name="Source" value="NB   ">
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
						
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0702"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p> ${ result_data.Account }</p>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.X2223"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
											<c:if test="${ result_data.ZoneCode.equals('TPC') || result_data.ZoneCode.equals('TPY') || result_data.ZoneCode.equals('KHC') }">
		                                        <select class="custom-select select-input validate[required]" name="Account" id="Account">
		                                        	<option value=""><spring:message code="LB.X0855"/></option>
													<c:forEach var="dataList" items="${result_data_n920.data.REC}">
														<option value="${dataList.ACN}"> ${dataList.ACN}</option>
													</c:forEach>
		                                        </select>
		                                    </c:if>
											<c:if test="${ !result_data.ZoneCode.equals('TPC') && !result_data.ZoneCode.equals('TPY') && !result_data.ZoneCode.equals('KHC') }">
												<b><font color="red"><spring:message code="LB.X2224"/></font></b>
												<br>
          										<input type="button" class="ttb-sm-btn btn-flat-orange"  value="<spring:message code="LB.X2225"/>" id="CMBACK">
											</c:if>
                                        </p>
									</div>
								</span>
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0059"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p> ${ result_data.ZoneCode_str }</p>
									</div>
								</span>
								<input type="hidden" id="ZoneCode" name="ZoneCode" value="${ result_data.ZoneCode }">
								<input type="hidden" id="ZoneCode_str" name="ZoneCode_str" value="${ result_data.ZoneCode_str }">
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0342"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p> ${ result_data.CarId }</p>
									</div>
								</span>
								<input type="hidden" id="CarId" name="CarId" value="${ result_data.CarId }">
								<input type="hidden" id="cardid" name="cardid" value="${ result_data.CarId }">
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0343"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										${ result_data.CarKind_str }
									</div>
								</span>
								<input type="hidden" id="CarKind" name="CarKind" value="${ result_data.CarKind }">
								<input type="hidden" id="CarKind_str" name="CarKind_str" value="${ result_data.CarKind_str }">
							</div>
							
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Mail_address"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input class="text-input  validate[required,funcCall[validate_EmailCheck[Email]]]" maxLength="50" size="50" type="text" name="Email" id="Email" value="${ result_data.Email }">
									</div>
								</span>
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0069"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input class="text-input  validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.D0069" />',Phone]]]" maxLength="10" size="10" type="text" name="Phone" id="Phone" value="${ result_data.Phone }">
									</div>
								</span>
							</div>
							
							
							
							
							<!--交易機制  SSL 密碼 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block" for="CMSSL" >
											<spring:message code="LB.SSL_password" />
								       		<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked />
                                            <span class="ttb-radio"></span>
								       	</label>
										<input type="password" name = "CMPASSWORD" id="CMPASSWORD"  maxlength="8" class="text-input validate[required, pwvd[PW, CMPASSWORD], custom[onlyLetterNumber]]">
										<input type="hidden" name = "PINNEW" id="PINNEW" value="">
									</div>
<!-- 									使用者是否可以使用IKEY -->
									<!-- 使用者是否可以使用IKEY -->
<%-- 									<c:if test = "${sessionScope.isikeyuser}"> --%>
<!-- 										即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森) -->
<%-- 										<c:if test = "${!result_data.TransferType.equals('NPD')}"> --%>
								
<!-- 											IKEY -->
<!-- 											<div class="ttb-input"> -->
<!-- 												<label class="radio-block"> -->
<%-- 													<spring:message code="LB.Electronic_signature" /> --%>
<!-- 													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1"> -->
<!-- 													<span class="ttb-radio"></span> -->
<!-- 												</label> -->
<!-- 											</div> -->
											
<%-- 										</c:if> --%>
<%-- 									</c:if> --%>
									<!-- 晶片金融卡 -->
<!-- 									<div class="ttb-input"> -->
<!-- 										<label class="radio-block"> -->
<%-- 											<spring:message code="LB.Financial_debit_card" /> --%>
											
<!-- 											只能選晶片金融卡時，預設為非勾選讓使用者點擊後顯示驗證碼 -->
<%-- 											<c:choose> --%>
<%-- 												<c:when test="${result_data.TransferType.equals('NPD')}"> --%>
<!-- 													<input type="radio" name="FGTXWAY" id="CMCARD" value="2"> -->
<%-- 												</c:when> --%>
<%-- 												<c:when test="${!sessionScope.isikeyuser}"> --%>
<!-- 													<input type="radio" name="FGTXWAY" id="CMCARD" value="2"> -->
<%-- 												</c:when> --%>
<%-- 												<c:otherwise> --%>
<!-- 												 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked"> -->
<%-- 												</c:otherwise> --%>
<%-- 											</c:choose> --%>
											
<!-- 											<span class="ttb-radio"></span> -->
<!-- 										</label> -->
<!-- 									</div> -->
									
								</span>
							</div>
							
<!-- 							確認新使用者名稱 -->
<!-- 							<div class="ttb-input-item row"> -->
<!-- 								<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<!-- 											驗證碼 -->
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 								</span>  -->
<!-- 								<span class="input-block"> -->
<!-- 									<div class="ttb-input"> -->
										
<%-- 										<spring:message code="LB.Captcha" var="labelCapCode" /> --%>
<!-- 										<input id="capCode" type="text" class="ttb-input" -->
<%-- 											name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off"> --%>
<!-- 										<img name="kaptchaImage" src="" /> -->
<!-- 										&nbsp;<font color="#FF0000">※英文不分大小寫，限半型字 </font> -->
<!-- 										<div class="login-input-block"> -->
<!-- 											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" onclick="refreshCapCode()" value="重新產生驗證碼" /> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
							
						</div>
						<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
						<input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						<!--回上頁 -->
<%--                         <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message> --%>
<%--                         <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">						 --%>
					</div>
				</div>
			</form>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後0.1秒開始遮罩
			setTimeout("initBlockUI()", 100);

			// 初始化驗證碼
// 			setTimeout("initKapImg()", 200);
			// 生成驗證碼
// 			setTimeout("newKapImg()", 300);
			
			init();
			
			// 過0.5秒解遮罩
			setTimeout("unBlockUI(initBlockId)", 800);
		});
		
		/**
		 * 初始化BlockUI
		 */
		function initBlockUI() {
			initBlockId = blockUI();
		}

		// 交易機制選項
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
		
			switch(fgtxway) {
				case '0':
// 					alert("交易密碼(SSL)...");
	    			$('#formId').submit();
					break;
					
				case '1':
// 					alert("IKey...");
					var jsondc = $("#jsondc").val();
					
					// 遮罩後不給捲動
// 					document.body.style.overflow = "hidden";

					// 呼叫IKEY元件
// 					uiSignForPKCS7(jsondc);
					
					// 解遮罩後給捲動
// 					document.body.style.overflow = 'auto';
					
					useIKey();
					break;
					
				case '2':
// 					alert("晶片金融卡");

					// 遮罩後不給捲動
// 					document.body.style.overflow = "hidden";
					
					// 呼叫讀卡機元件
					listReaders();

					// 解遮罩後給捲動
// 					document.body.style.overflow = 'auto';
					
			    	break;
			    	
				default:
					//alert("nothing...");
					errorBlock(
							null, 
							null,
							["nothing..."], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
			
		}

	    function init(){
	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	    	$("#pageshow").click(function(e){			
				console.log("submit~~");
// 				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
// 				var capData = $("#formId").serializeArray();
// 				var capResult = fstop.getServerDataEx(capUri, capData, false);
// 				if (capResult.result) {
				if(!$('#formId').validationEngine('validate') && $('input[name="FGTXWAY"]:checked').val() == 0){
		        	e.preventDefault();
	 			}else{
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
    				var action = '${__ctx}/PARKING/FEE/parking_withholding_modify_update_result';
    				$('#formId').attr("action", action);
	    			unBlockUI(initBlockId);
	    			processQuery();
	 			}
// 				} else {
// 					alert("驗證碼有誤");
// 					changeCode();
// 				}
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
 				$("#formId").validationEngine('detach');
    			var action = '${__ctx}/PARKING/FEE/parking_withholding_modify';
    			$('#back').val("Y");
    			$('#formId').attr("action", action);
    			$('#formId').submit();
    		});
    		//若前頁有帶acn，預設為此acn
			var getacn = '${result_data.getAcn}';
			if(getacn != null && getacn != ''){
				$("#Account option[value= '"+ getacn +"' ]").prop("selected", true);
			}
	    }	
	    
		// 刷新驗證碼
		function refreshCapCode() {
			console.log("refreshCapCode...");

			// 驗證碼
			$('input[name="capCode"]').val('');
			
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 刷新輸入欄位
		function changeCode() {
			console.log("changeCode...");
			
			// 清空輸入欄位
			$('input[name="capCode"]').val('');
			
			// 刷新驗證碼
			refreshCapCode();
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				refreshCapCode();
			});
		}

 	</script>
</body>
</html>
 