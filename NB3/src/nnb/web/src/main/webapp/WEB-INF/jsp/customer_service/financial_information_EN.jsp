<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<section id="main-content" class="container position-absolute" style="display:none">
	<div class="pupup-block">

		<div class="card-block shadow-box terms-pup-blcok">
			<!-- n_e_e_d t_o f_i_x p_o_s_i_t_i_o_n -->
			<button type="button" class="popup-close-btn d-none"
				data-dismiss="modal" aria-label="Close">×</button>
			<h2 class="ttb-pup-h2">Financial Information</h2>

			<nav class="nav card-select-block text-center d-block" id="nav-tab"
				role="tablist">
				<input type="button" class="nav-item ttb-sm-btn active"
					id="nav-trans-1-tab" data-toggle="tab" href="#nav-trans-1"
					role="tab" aria-selected="false" value="Exchange rate" /> 
				<input type="button" class="nav-item ttb-sm-btn" id="nav-trans-2-tab"
					data-toggle="tab" href="#nav-trans-2" role="tab"
					aria-selected="true" value="Interest rate" /> 
				<input type="button" class="nav-item ttb-sm-btn" id="nav-trans-3-tab" data-toggle="tab"
					href="#nav-trans-3" role="tab" aria-selected="true" value="Net value of fund" />
				<input type="button" class="nav-item ttb-sm-btn"
					id="nav-trans-4-tab" data-toggle="tab" href="#nav-trans-4"
					role="tab" aria-selected="true" value="Gold passbook" /> 
			</nav>
			<div class="col-12 tab-content" id="nav-tabContent">
				<div class="ttb-input-block tab-pane fade show active"
					id="nav-trans-1" role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!-- 當日匯率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/exchange_rate" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank" rel="noopener noreferrer"> <span>Current<br />Exchange Rate
								</span>
								</a>
							</div>
							<!-- 歷史匯率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-1" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank" rel="noopener noreferrer"> <span>Historical<br />Exchange Rate
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="ttb-input-block tab-pane fade" id="nav-trans-2"
					role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!-- 新台幣存款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-82" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>NTD Deposit<br />Interest Rate
								</span>
								</a>
							</div>
							<!-- 新台幣放款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-83" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>NTD Loan<br />Interest Rate
								</span>
								</a>
							</div>
							<!-- 外幣存款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-84" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>Foreign currency<br />Deposit<br />Interest Rate
								</span>
								</a>
							</div>
							<!-- 外幣放款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-85" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>Foreign currency<br />Lending<br />Interest Rate
								</span>
								</a>
							</div>
							<!-- 債券及票券利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-86" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>Bonds & Tickets<br />Interest Rate
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="ttb-input-block tab-pane fade" id="nav-trans-3"
					role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!-- 國內基金淨值查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=D" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>Domestic Fund<br />Net Asset Value
								</span>
								</a>
							</div>
							<!-- 國外基金淨值查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=F" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>Foreign Fund<br />Net Asset Value
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="ttb-input-block tab-pane fade" id="nav-trans-4"
					role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!-- 當日黃金牌價查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="${__ctx}/GOLD/OUT/PASSBOOK/day_price_query" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>Current<br />Gold Price
								</span>
								</a>
							</div>
							<!-- 歷史黃金牌價查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="${__ctx}/GOLD/OUT/PASSBOOK/history_price_query" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>Historical<br />Gold Price
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<input type="BUTTON" class="ttb-button btn-flat-gray" value="Back" name="" onclick="$('#main-content').hide();">
			</div>
		</div>
	</div>
</section>
