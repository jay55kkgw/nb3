<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>
<body class="watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	
	<div> 
		<!-- 查詢時間 -->
		<p>
			<spring:message code="LB.Inquiry_time" /> : ${CMQTIME}
		</p>
		<!-- 查詢期間 -->
		<p>
			<spring:message code="LB.Inquiry_period" /> : ${CMPERIOD}
		</p>
		<!-- 查詢帳號 : -->
        <p>
			<spring:message code= "LB.L/C_no" /> :
        	${LCNO}
		</p>
		<!-- 資料總數 : -->
		<p>
			<spring:message code="LB.Total_records" /> : ${CMRECNUM}
			<!--筆 -->
			<spring:message code="LB.Rows" />
		</p>
		<!-- 匯入金額總金額 : -->
		
		
		<!-- 資料Row -->
		
		<table class="print">
			<tr>
				<!-- 擔保信用狀/保證函號碼 -->
	            <th>
	                <!--<spring:message code="LB.Change_date" />-->
					<spring:message code= "LB.W0104" />
	            </th>
	            <!-- 簽發日 -->
	            <th>
	                <!--<spring:message code="LB.Change_date" />-->
					<spring:message code= "LB.W0113" />
	           </th>
	            <!-- 幣別 -->
	            <th>
	                <spring:message code="LB.Currency" />
	            </th>
	            <!-- 擔保信用狀/保證函金額 -->
	            <th>
	                <!-- <spring:message code="LB.Withdrawal_amount" /> -->
					<spring:message code= "LB.W0115" />
	            </th>
	            <!-- 擔保信用狀/保證函餘額 -->
	            <th>
	                <!-- <spring:message code="LB.Withdrawal_amount" /> -->
					<spring:message code= "LB.W0116" />
	            </th>
	            <!--有效期限 -->
	            <th>
	                <!-- <spring:message code="LB.Deposit_amount" /> -->
					<spring:message code= "LB.W0120" />
	            </th>
	            <!-- 受益人名稱/地址 -->
	            <th >
	                <!-- <spring:message code="LB.Account_balance_2" /> -->
					<spring:message code= "LB.W0117" />
	            </th>
	            <!-- 通知銀行 -->
	            <th >
	                <!--<spring:message code="LB.Data_content" />-->
					<spring:message code= "LB.W0118" />
	            </th>
	            <!-- 備註 -->
	            <th >
	                <spring:message code="LB.Note" />
	            </th>
			</tr>
			<c:forEach var="dataList" items="${print_datalistmap_data}">
				<tr>
	                <!--擔保信用狀/保證函號碼-->
	                <td style="text-align:center">${dataList.RLGNO }</td>
	                <!--簽發日-->
	                <td style="text-align:center">${dataList.ROPENDAT }</td>
	           		<!-- 幣別 -->
	                <td style="text-align:center">${dataList.RLGCCY }</td>
	            	<!--擔保信用狀/保證函金額 -->
	                <td style="text-align:right">${dataList.RLGAMT }</td>
	            	<!--擔保信用狀/保證函餘額 -->
	                <td style="text-align:right">${dataList.RLGOS }</td>
	             	<!--有效期限 -->
	                <td style="text-align:center">${dataList.REXPDATE }</td>
	                <!-- 受益人名稱/地址 -->
	                <td style="text-align:center">
	                	${dataList.RBENNAME}<br>
	                	${dataList.RBENADD1}<br>${dataList.RBENADD2}<br>${dataList.RBENADD3}
	                </td>
	            	<!-- 通知銀行 -->
	                <td style="text-align:center">${dataList.RADVNM }</td>
	            	<!-- 備註 -->
	                <td style="text-align:center">${dataList.RMARK }</td>
	            	
	            </tr>
			</c:forEach>
		</table>
   		<c:set var="total_i18n">
   			<spring:message code='LB.W0123' />
   		</c:set>
   		<c:set var="row_i18n">
   			<spring:message code='LB.Rows' />
   		</c:set>
		<p>
       		<c:set var="SATM_replace" value="${ fn:replace( SATM, 'i18n{LB.W0123}', total_i18n) }" />
       		<c:set var="SATM_replace2" value="${ fn:replace( SATM_replace, 'i18n{LB.Rows}', row_i18n) }" />
			<spring:message code= "LB.W0121" /> : <br> ${SATM_replace2}
		</p>
		<p>
       		<c:set var="SBAL_replace" value="${ fn:replace( SBAL, 'i18n{LB.W0123}', total_i18n) }" />
       		<c:set var="SBAL_replace2" value="${ fn:replace( SBAL_replace, 'i18n{LB.Rows}', row_i18n) }" />
			<spring:message code= "LB.W0122" /> : <br> ${SBAL_replace2}
		</p>
	</div>
	
	<br>
	<br>
	<div class="text-left">
		<!-- 		說明： -->
	
		<ol class="list-decimal text-left description-list">
		<p>	<spring:message code="LB.Description_of_page"/></p>
			<li>
				<!-- <spring:message code="LB.Demand_deposit_detail_P2_D1"/> -->
                           <spring:message code="LB.F_Credit_Open_Query_P2_D1"/>
			</li>
		</ol>
	</div>
</body>
</html>