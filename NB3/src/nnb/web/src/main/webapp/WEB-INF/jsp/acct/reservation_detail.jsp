<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<style>
		.Line-break{
			white-space:normal;
			word-break:break-all;
			width:95px;
			word-wrap:break-word;
		}
	</style>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 預約交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X1477" /></li>
    <!-- 預約交易查詢/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Query_Cancel_Scheduled_Transactions" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
				<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 			<main class="col-12">	
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Query_Cancel_Scheduled_Transactions" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form method="post" id="formId" action="${__ctx}/NT/ACCT/RESERVATION/reservation_confirm">
					<input id="ROWDATA" name="ROWDATA" type="hidden" value="" />
					<div class="main-content-block row">
						<div class="col-12">
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間 -->
	               		  			<h3><spring:message code="LB.Inquiry_time" /></h3>
									<p>
										${reservation_detail.data.CMQTIME}
									</p>
								</li>
								<li>
									<!-- 資料總數 -->
									<h3><spring:message code="LB.Total_records" /></h3>								
									<p>
									${reservation_detail.data.COUNT}
									<spring:message code="LB.Rows" />
	               					</p>
								</li>
							</ul>
							<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
								<thead>
								<tr>
	<!-- 							預約編號 -->
									<th>
										<spring:message code="LB.Booking_number" />
									</th>
	<!-- 								週期 -->
									<th>
										<spring:message code="LB.Period" />
									</th>
	<!-- 								生效日/截止日 -->
									<th>
										<spring:message code="LB.Effective_date" />
										</br>
										<spring:message code="LB.Deadline" />
									</th>
	<!-- 								下次轉帳日 -->
									<th>
									<spring:message code="LB.Next_transfer_date" />
									</th>
	<!-- 								轉出帳號 -->
									<th>
									<spring:message code="LB.Payers_account_no" />
									</th>
	<!-- 								轉入帳號/繳費稅代號 -->
									<th>
										<spring:message code="LB.Payees_account_no" />/
										</br>
										<spring:message code="LB.Pay_taxes_fee_code" />
									</th>
	<!-- 								轉帳金額 -->
									<th>
										<spring:message code="LB.Amount" />
									</th>
	<!-- 								交易類別/交易機制 -->
									<th>
										<spring:message code="LB.Transaction_type" /><hr/><spring:message code="LB.Transaction_security_mechanism" />
									</th>
	<!-- 								備註 -->
									<th>
										<spring:message code="LB.Note" />
									</th>
	<!-- 								狀態 -->
									<th>
										<spring:message code="LB.Status" />
									</th>
								</tr>
								</thead>
								<c:if test="${reservation_detail.data.COUNT == '0'}">
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</c:if>																	
								<c:if test="${reservation_detail.data.COUNT > '0'}">
									<tbody>
										<c:forEach var="dataList" items="${reservation_detail.data.REC}" varStatus="data" >
											<tr>            	
								                <!-- 預約編號 -->
								                <td class="text-center">${dataList.DPSCHNO}</td>
								                <!-- 週期 -->
								                <td class="text-center">${dataList.DPPERMTDATE}</td>                
								                <!-- 生效日/截止日 -->
								                <td class="text-center">
								                	${dataList.DPFDATE} 
								                	<c:if test="${dataList.DPTDATE != ''}">
								                	<br>
								                	${dataList.DPTDATE}
								                	</c:if>
								                </td>				          	               
								                <!-- 下次轉帳日 -->
								                <td class="text-center">${dataList.DPNEXTDATE}</td>                
								                <!-- 轉出帳號 -->
								                <td class="text-center">${dataList.DPWDAC}</td>                
								                <!-- 轉入帳號/繳費稅代號 -->
								                <td class="text-center">
								                	<c:if test="${not empty dataList.DPSVBH}">
									                	${dataList.DPSVBH}
									                	<br>
								                	</c:if>
													${dataList.DPSVAC}						                
								                </td>
								                <!-- 轉帳金額 -->
								                <td class="text-right">${dataList.DPTXAMTS}</td>
								                <!-- 交易類別 --> <!-- 交易機制 -->
								                <td class="text-center Line-break">${dataList.TXTYPE}<hr/>${dataList.DPTXCODES}</td>
								                <!-- 備註 -->
								                <td class="text-center Line-break">${dataList.DPTXMEMO}</td>
								                <!-- 狀態 -->
								                <td class="text-center">${dataList.DPTXSTATUS}<br/>
										            <!-- 點選 -->
									                <c:if test="${dataList.STATUS != '0' || dataList.DPNEXTDATEchk <= reservation_detail.data.TODAY || (dataList.DPTXCODE != '0' && dataList.DPTXCODE != '1' && dataList.DPTXCODE != '2' && (dataList.DPTXCODE != '7' || idgateUserFlag != 'Y'))}">
									                	<input type="button" disabled="disabled" class="ttb-sm-btn btn-flat-gray" value = '<spring:message code="LB.Cancel" />'>
													</c:if>
									                <c:if test="${dataList.STATUS == '0' && dataList.DPNEXTDATEchk > reservation_detail.data.TODAY && (dataList.DPTXCODE == '0' || dataList.DPTXCODE == '1' || dataList.DPTXCODE == '2' || (dataList.DPTXCODE == '7' && idgateUserFlag == 'Y'))}">
										                <input type="button" class="ttb-sm-btn btn-flat-orange" value = '<spring:message code="LB.Cancel" />' onclick="cancel(${data.count});" >
										                	<input id="${data.count}" name="${data.count}" type="hidden" value='${dataList}' />
													</c:if>
												</td>					           
							            	</tr>        
										</c:forEach>
									</tbody>
								</c:if>
							</table>
							<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
						</div>
					</div>
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /> </p>
						<li><span><spring:message code="LB.Reservation_P1_D1" /></span></li>
						<li><span><spring:message code="LB.Reservation_P1_D2" /></span></li>
						<li><span><spring:message code="LB.Reservation_P1_D3" /></span></li>
						<li><span><spring:message code="LB.Reservation_P1_D4" /></span></li>
						<c:if test="${reservation_detail.data.isProd eq 3}"><li><span><font color="red"><spring:message code="LB.X2405" /></font></span></li></c:if>
						<!--併行作業期間:倘您已曾經登入新版網路銀行後，前於舊版網銀已預約之轉帳交易，統一將移轉至新版網路銀行執行及查詢;另新、舊版網路銀行執行預約交易，需回原執行之網路銀行查詢，無法於兩系統間相互查詢。 -->					
					</ol>
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
	
	<%@ include file="../index/footer.jsp"%>
    <script type="text/javascript">
    	
		$(document).ready(function(){
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()",10);
			// 開始查詢資料並完成畫面
			setTimeout("init()",20);
			
			setTimeout("initDataTable()",100);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)",500);	
			
		});
		function init(){			
// 			initFootable();
			
			$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"reservation_detail_print",
					"jspTitle":"<spring:message code='LB.Query_Cancel_Scheduled_Transactions' />",
					"CMQTIME":"${reservation_detail.data.CMQTIME }",
					"COUNT":"${reservation_detail.data.COUNT}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
		}
    
	    function cancel(dataCount){
	    	initBlockUI();
	    	// 將頁面資料塞進hidden欄位
	    	console.log("dataCount: " + dataCount);
	    	var data = $("#"+dataCount).val();
	    	console.log("data: " + data);
			$("#ROWDATA").val(data);
	    	
			$("#formId").submit();
	    };
    </script>
</body>
</html>