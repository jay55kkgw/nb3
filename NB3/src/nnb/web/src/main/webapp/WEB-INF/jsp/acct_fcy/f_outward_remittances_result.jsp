<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
		}


		//選項
		function formReset() {
			//initBlockUI();
			if ($('#actionBar').val() == "excel") {
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/f_outward_remittances.xls");
			} else if ($('#actionBar').val() == "txt") {
				$("#downloadType").val("TXT");
				$("#templatePath").val("/downloadTemplate/f_outward_remittances.txt");
			}
			$("#formId").attr("target", "");
			$("#formId").submit();
			$('#actionBar').val("");
		}
		//交易單據
		function openFxRemitQuery() {
			window.open('${__ctx}/FCY/COMMON/fxcertpreview?TXID=F002&ADTXNO=${f_outward_remittances_result.data.ADTXNO}');
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
	<!-- 匯出匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0286" /></li>
    <!-- 匯出匯款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0286" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--外匯匯出匯款 -->
				<h2>
					<spring:message code="LB.W0286" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value="">
							<spring:message code="LB.Downloads" />
						</option>
						<!-- 						下載Excel檔 -->
						<option value="excel">
							<spring:message code="LB.Download_excel_file" />
						</option>
						<!-- 						下載為txt檔 -->
						<option value="txt">
							<spring:message code="LB.Download_txt_file" />
						</option>
					</select>
				</div>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/download">
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0286" />" />
					<input type="hidden" name="nowDate" value="${f_outward_remittances_result.data.nowDate}" />
					<input type="hidden" name="CUSIDN" value="${f_outward_remittances_result.data.CUSIDN}" />
					<input type="hidden" name="ENNAME" value="${f_outward_remittances_result.data.ENNAME}" />
					<input type="hidden" name="STAN" value="${f_outward_remittances_result.data.STAN}" />
					<input type="hidden" name="REFNO" value="${f_outward_remittances_result.data.REFNO}" />
					<input type="hidden" name="ORGCCY" value="${f_outward_remittances_result.data.ORGCCY}" />
					<input type="hidden" name="PMTCCY" value="${f_outward_remittances_result.data.PMTCCY}" />
					<input type="hidden" name="str_pmtAmt" value="${f_outward_remittances_result.data.str_pmtAmt}" />
					<input type="hidden" name="str_Rate" value="${f_outward_remittances_result.data.str_Rate}" />
					<input type="hidden" name="str_commAmt" value="${f_outward_remittances_result.data.str_commAmt}" />
					<input type="hidden" name="ORGCCY" value="${f_outward_remittances_result.data.ORGCCY}" />
					<input type="hidden" name="str_commAmt" value="${f_outward_remittances_result.data.str_commAmt}" />
					<input type="hidden" name="str_cabAmt" value="${f_outward_remittances_result.data.str_cabAmt}" />
					<input type="hidden" name="str_orgAmt" value="${f_outward_remittances_result.data.str_orgAmt}" />
					<input type="hidden" name="CMQTIME" value="${f_outward_remittances_result.data.nowDate}" />
					<input type="hidden" name="NAME" value="${f_outward_remittances_result.data.NAME}" />
					<input type="hidden" name="PAYDATE" value="${f_outward_remittances_result.data.PAYDATE}" />
					<input type="hidden" name="FXRCVBKCODE" value="${f_outward_remittances_result.data.FXRCVBKCODE}" />
					<input type="hidden" name="CUSTACC" value="${f_outward_remittances_result.data.CUSTACC}" />
					<input type="hidden" name="BENACC" value="${f_outward_remittances_result.data.BENACC}" />
					<input type="hidden" name="BGROENO" value="${f_outward_remittances_result.data.BGROENO}" />
					<input type="hidden" name="display_BENTYPE"
						value="${f_outward_remittances_result.data.display_BENTYPE}" />
					<input type="hidden" name="COUNTRY" value="${f_outward_remittances_result.data.COUNTRY}" />
					<input type="hidden" name="SRCFUNDDESC" value="${f_outward_remittances_result.data.SRCFUNDDESC}" />
					<input type="hidden" name="CMTRMAIL" value="${f_outward_remittances_result.data.CMTRMAIL}" />
					<!--摘要內容 -->
					<input type="hidden" name="CMMAILMEMO" value="${f_outward_remittances_result.data.CMMAILMEMO}" />
					<!--摘要內容 -->
					<input type="hidden" name="str_MailMemo" value="${f_outward_remittances_result.data.str_MailMemo}" />
					<input type="hidden" name="FXRMTDESC" value="${f_outward_remittances_result.data.FXRMTDESC}" />
					<input type="hidden" name="MEMO1" value="${f_outward_remittances_result.data.MEMO1}" />
					<input type="hidden" name="COMMACC" value="${f_outward_remittances_result.data.COMMACC}" />
					<input type="hidden" name="DETCHG" value="${f_outward_remittances_result.data.DETCHG}" />
					<input type="hidden" name="COMMCCY" value="${f_outward_remittances_result.data.COMMCCY}" />
					<input type="hidden" name="fxRcvBkaddr_download"
						value="${f_outward_remittances_result.data.fxRcvBkaddr_download}" />
					<input type="hidden" name="fxRcvAdd_download"
						value="${f_outward_remittances_result.data.fxRcvAdd_download}" />
					<!-- 下載用 -->
					<input type="hidden" name="downloadType" id="downloadType" />
					<input type="hidden" name="templatePath" id="templatePath" />
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="8" /><!-- headerRightEnd 資料列以前的右方界線 -->
					<input type="hidden" name="headerBottomEnd" value="16" /><!-- headerBottomEnd 資料列到第幾列 從0開始 -->
					<!-- TXT下載用-總行數 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="17" />
					<input type="hidden" name="txtHasRowData" value="false" />
					<input type="hidden" name="txtHasFooter" value="false" />
					<!--電文回應 -->
					<c:set var="BaseResultData" value="${f_outward_remittances_result.data}"></c:set>
					<!--輸入頁的值 -->
					
					<c:if test="${BaseResultData.FGTRDATE == '0' && showDialog == 'true'}">
				 		<%@ include file="../index/txncssslog.jsp"%>
					</c:if>
					<!-- 即時交易結果 -->
					<c:if test="${BaseResultData.FGTRDATE == '0'}">
						<!-- 表單顯示區 -->
						<div class="main-content-block row">
							<div class="col-12 tab-content">
								<div class="ttb-input-block">
									<div class="ttb-message">
										<span>
											<spring:message code="LB.Transfer_successful" />
										</span>
									</div>
									<!-- 列印時間 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.X0040" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>${BaseResultData.nowDate }</span>
											</div>
										</span>
									</div>
									<!-- 統編/身分證號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.X0041" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>${BaseResultData.CUSIDN}</span>
											</div>
										</span>
									</div>
									<!-- 姓名 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.D0203" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>${BaseResultData.NAME}</span>
											</div>
										</span>
									</div>
									<!-- 付款日期 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0289" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>${BaseResultData.PAYDATE}</span>
											</div>
										</span>
									</div>
									<!-- 英文姓名 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.D0050" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>${BaseResultData.ENNAME}</span>
											</div>
										</span>
									</div>
									<!-- 收款銀行資料 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.X0037" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													SWIFT CODE
													${BaseResultData.FXRCVBKCODE}
													<br>
													<spring:message code="LB.W0295" />
													${BaseResultData.FXRCVBKADDR}
												</span>
											</div>
										</span>
									</div>
									<!-- 交易序號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Transaction_Number" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.STAN}
												</span>
											</div>
										</span>
									</div>
									<!-- 銀行交易編號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.X0042" /><spring:message code="LB.W0286" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.REFNO}
												</span>
											</div>
										</span>
									</div>
									<!-- 付款帳號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0290" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.CUSTACC}
												</span>
											</div>
										</span>
									</div>
									<!-- 付款金額 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.X0031" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.ORGCCY}
													${BaseResultData.str_orgAmt}
												</span>
											</div>
										</span>
									</div>
									<!--收款人資料-->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0294" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<spring:message code="LB.W0295" />
													<br>
													${BaseResultData.FXRCVADDR}
												</span>
											</div>
										</span>
									</div>
									<!-- 收款帳號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0292" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.BENACC}
												</span>
											</div>
										</span>
									</div>
									<!-- 收款金額-->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.X0032" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<!-- 幣別 -->
													${BaseResultData.PMTCCY}
													<!-- 金額 -->
													${BaseResultData.str_pmtAmt}
												</span>
											</div>
										</span>
									</div>
									<!-- 匯率 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Exchange_rate" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.str_Rate}
												</span>
											</div>
										</span>
									</div>
									<!--議價編號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Bargaining_number" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.BGROENO}
												</span>
											</div>
										</span>
									</div>
									<!-- 收款人身份別 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Payee_identity" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.display_BENTYPE}
												</span>
											</div>
										</span>
									</div>
									<!-- 收款地區國別 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.X0043" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.COUNTRY}
												</span>
											</div>
										</span>
									</div>
									<!-- 匯款分類項目 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0298" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.SRCFUNDDESC}
												</span>
											</div>
										</span>
									</div>
									<!-- Email通知 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0300" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<p>
														<spring:message code="LB.Email" />
														${BaseResultData.CMTRMAIL}
													</p>
													<p>
														<spring:message code="LB.Summary" />
														${BaseResultData.str_MailMemo}
													</p>
												</span>
											</div>
										</span>
									</div>
									<!-- 匯款分類說明 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0299" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.FXRMTDESC}
												</span>
											</div>
										</span>
									</div>
									<!-- 匯款附言 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0303" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.str_Memo1}
												</span>
											</div>
										</span>
									</div>
									<!-- 費用扣款帳號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0305" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.COMMACC}
												</span>
											</div>
										</span>
									</div>
									<!-- 手續費負擔別 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0155" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.DETCHG}
												</span>
											</div>
										</span>
									</div>
									<!-- 手續費 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.D0507" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<!-- 幣別 -->
													${BaseResultData.COMMCCY}
													<!-- 金額 -->
													${BaseResultData.str_commAmt}
												</span>
											</div>
										</span>
									</div>
									<!-- 郵電費 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0345" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<!-- 幣別 -->
													${BaseResultData.COMMCCY}
													<!-- 金額 -->
													${BaseResultData.str_cabAmt}
												</span>
											</div>
										</span>
									</div>
									<!-- 國外費用 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0346" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<!-- 幣別 -->
													${BaseResultData.COMMCCY}
													<!-- 金額 -->
													${BaseResultData.str_ourAmt}
												</span>
											</div>
										</span>
									</div>
								</div>
								<!-- button -->
									<!-- 列印 -->
									<input class="ttb-button btn-flat-orange" type="button"  id="printbtn"
										name="printbtn" onclick="openFxRemitQuery()" value="<spring:message code="LB.Transaction_document" />" />
								<!-- button -->
							</div>
						</div>
						<div class="text-left">
							<!-- 		說明： -->
							<spring:message code="LB.Description_of_page" />
							<ol class="list-decimal text-left">
								<li><spring:message code="LB.F_Outward_Remittances_P6_D1" /></li>
							</ol>
						</div>
					</c:if>
					<!-- 預約交易結果 -->
					<c:if test="${BaseResultData.FGTRDATE != '0'}">
						<!-- 表單顯示區 -->
						<div class="main-content-block row">
							<div class="col-12 tab-content">
								<div class="ttb-input-block">
									<div class="ttb-message">
										<span>
											<spring:message code="LB.W0284" />
										</span>
									</div>
									<!-- 付款日期 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0289" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.PAYDATE}
												</span>
											</div>
										</span>
									</div>
									<!-- 收款銀行資料 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.X0037" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<span class="input-subtitle subtitle-color">SWIFT CODE</span>
													<p>
														${BaseResultData.FXRCVBKCODE}
													</p>
													<span class="input-subtitle subtitle-color"><spring:message code="LB.W0295" /></span>
													<p>
														${BaseResultData.FXRCVBKADDR}
													</p>
												</span>
											</div>
										</span>
									</div>
									<!-- 付款帳號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0290" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.CUSTACC}
												</span>
											</div>
										</span>
									</div>
									<!-- 付款幣別 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.D0448" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.display_PAYCCY}
												</span>
											</div>
										</span>
									</div>
									<!-- 收款帳號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0292" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.BENACC}
												</span>
											</div>
										</span>
									</div>
									<!-- 收款幣別 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0293" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.display_REMITCY}
												</span>
											</div>
										</span>
									</div>
									<!--收款人資料-->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0294" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<span class="input-subtitle subtitle-color"><spring:message code="LB.W0295" /></span>
													<p>
														${BaseResultData.FXRCVADDR}
													</p>
												</span>
											</div>
										</span>
									</div>
									<!-- 匯款金額 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0150" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.display_PAYREMIT}
												</span>
											</div>
										</span>
									</div>

									<!-- 收款人身份別 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0266" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.display_BENTYPE}
												</span>
											</div>
										</span>
									</div>
									<!-- 匯款分類項目 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0298" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.SRCFUNDDESC}
												</span>
											</div>
										</span>
									</div>
									<!-- 匯款分類說明 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0299" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.FXRMTDESC}
												</span>
											</div>
										</span>
									</div>
									<!-- Email通知 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0300" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<span class="input-subtitle subtitle-color"><spring:message code="LB.Email" /></span>
													<p>
														${BaseResultData.CMTRMAI}
													</p>
													<span class="input-subtitle subtitle-color"><spring:message code="LB.Summary" /></span>
													<p>
														${BaseResultData.str_MailMemo}
													</p>
												</span>
											</div>
										</span>
									</div>
									<!-- 匯款附言 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0303" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.str_Memo1}
												</span>
											</div>
										</span>
									</div>
									<!-- 手續費負擔別 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0155" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.DETCHG}
												</span>
											</div>
										</span>
									</div>
									<!-- 費用扣款帳號 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0305" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.COMMACC}
												</span>
											</div>
										</span>
									</div>
									<!-- 費用扣款幣別 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W0306" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.display_COMMCCY}
												</span>
											</div>
										</span>
									</div>
								</div>
							</div>
						</div>
					</c:if>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>