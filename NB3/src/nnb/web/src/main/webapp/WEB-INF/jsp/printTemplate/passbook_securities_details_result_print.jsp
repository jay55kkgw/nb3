<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- <script type="text/javascript"	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script> -->
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<label><spring:message code="LB.Inquiry_time" /> <!-- 查詢時間 -->：</label>
	<label>${CMQTIME}</label>
	<br />
	<label><spring:message code="LB.Inquiry_period_1" /> <!-- 查詢期間 -->：</label>
	<label>${CMPERIOD}</label>
	<br />
	<label><spring:message code="LB.Total_records" /> <!-- 資料總數 -->：</label>
	<label>${COUNT} <spring:message code="LB.Rows" /></label>
	<br />
	<br />
	<c:forEach var="tableList" items="${dataListMap}">
		<table class="print">
			<tr>
				<!-- 帳號 -->
				<td><spring:message code="LB.Account" /></td>
				<td class="text-left" colspan="7">${tableList.ACN}</td>
			</tr>
			<tr>
				<!-- 異動日 -->
				<th><spring:message code="LB.Change_date" /></th>
				<!-- 摘要-->
				<th><spring:message code="LB.Summary_1" /></th>
				<!-- 借貸別 -->
				<th><spring:message code="LB.W0015" /></th>
				<!-- 交易金額-->
				<th><spring:message code="LB.W0016" /></th>
				<!-- 餘額-->
				<th><spring:message code="LB.W0017" /></th>
				<!-- 資料內容 -->
				<th><spring:message code="LB.Data_content" /></th>
				<!-- 收付行-->
				<th><spring:message code="LB.Receiving_Bank" /></th>
				<!-- 備註 -->
				<th><spring:message code="LB.Note" /></th>
			</tr>
			<c:forEach var="dataList" items="${tableList.TABLE}">
				<tr>
					<td class="text-center">${dataList.LSTLTD}</td>
					<td class="text-center">${dataList.MEMO}</td>
					<td class="text-center">${dataList.CODDBCR}</td>
					<td class="text-right">${dataList.AMTTRN}</td>
					<td class="text-right">${dataList.BAL}</td>
					<td class="text-center">${dataList.DATA16}</td>
					<td class="text-center">${dataList.TRNBRH}</td>
					<td class="text-center">${dataList.FILLER_X4}</td>
				</tr>
			</c:forEach>
		</table>
	</c:forEach>
	<br />
	<br />
</body>
</html>