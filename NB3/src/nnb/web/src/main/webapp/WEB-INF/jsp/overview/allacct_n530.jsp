<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">

	// column's title i18n


	// n530-外幣定存明細
	i18n['ACN'] = '<spring:message code="LB.Account" />'; // 帳號
	i18n['FDPNO'] = '<spring:message code="LB.Certificate_no" />'; // 存單號碼
	i18n['CUID'] = '<spring:message code="LB.Currency" />'; // 幣別
	i18n['BALANCE'] = '<spring:message code="LB.Certificate_amount" />'; // 存單金額
// 	i18n['ITR'] = '<spring:message code="LB.Interest_rate1" />'; // 利率(%)
	i18n['INTMTH'] = '<spring:message code="LB.Interest_calculation" /><hr><spring:message code="LB.Interest_rate1" />'; // 計息方式
	i18n['DPISDT'] = '<spring:message code="LB.Start_date" /><hr><spring:message code="LB.Maturity_date" />'; // 起存日
// 	i18n['DUEDAT'] = '<spring:message code="LB.Maturity_date" />'; // 到期日
	i18n['TSFACN'] = '<spring:message code="LB.Interest_transfer_to_account" />'; // 利息轉入帳號
	i18n['ILAZLFTM'] = '<spring:message code="LB.Automatic_number_of_rotations" /><hr><spring:message code="LB.Automatic_number_of_unrotated" />'; // 自動轉期已轉次數
// 	i18n['AUTXFTM'] = '<spring:message code="LB.Automatic_number_of_unrotated" />'; // 	自動轉期未轉次數
	i18n['QM'] = '<spring:message code="LB.Quick_Menu" />';//快速選單
	//
	i18n['Total'] = '<spring:message code="LB.Total_amount" />';//總計金額
	i18n['TotalRow'] = '<spring:message code="LB.X0380" />';
	i18n['ROWS']='<spring:message code="LB.Rows"/>';//筆
	// DataTable Ajax tables
	var n530_columns = [
		{ "data":"ACN", "title":i18n['ACN'] },
		{ "data":"FDPNO", "title":i18n['FDPNO'] },
		{ "data":"CUID", "title":i18n['CUID'] },
		{ "data":"BALANCE", "title":i18n['BALANCE'] },
// 		{ "data":"ITR", "title":i18n['ITR'] },
		{ "data":"INTMTH", "title":i18n['INTMTH'] },
		{ "data":"DPISDT", "title":i18n['DPISDT'] },
// 		{ "data":"DUEDAT", "title":i18n['DUEDAT'] },
		{ "data":"TSFACN", "title":i18n['TSFACN'] },
		{ "data":"ILAZLFTM", "title":i18n['ILAZLFTM'] },
// 		{ "data":"AUTXFTM", "title":i18n['AUTXFTM'] },
		{ "data":"QM","title":i18n['QM'] }
	];
	
	var n530_total_columns = [
		{ "data":"A1", "title":i18n['CUID'] },
		{ "data":"A2", "title":i18n['Total'] },
	];
	var n530_total_rows = [];

	//0置中 1置右
	var n530_align = [0,0,0,1,1,0,0,0,0,0,0,0,0,0,0];
	var n530_total_align = [0,1];
	// Ajax_n530-外幣定存明細
	function getMyAssets530() {
		$("#n530").show();
		$("#n530_title").show();
		createLoadingBox("n530_BOX",'');
	
		uri = '${__ctx}' + "/OVERVIEW/allacct_n530aj";
		console.log("allacct_n530aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax_n530, null, "n530");
	}

	// Ajax_callback
	function countAjax_n530(data){
		var error = false;
		// 資料處理後呈現畫面
		if (data.result) {
			
			n530_rows = data.data.REC;
			n530_rows.forEach(function(d,i){
				d["INTMTH"]=d["INTMTH"]+"<hr>"+d["ITR"];
				d["DPISDT"]=d["DPISDT"]+"<hr>"+d["DUEDAT"];
				d["ILAZLFTM"]=d["ILAZLFTM"]+"<hr>"+d["AUTXFTM"];
				
				var options = [];
				if(d.OPTIONTYPE =='1'){
					//<!-- 外匯綜存定存解約 -->
					options.push(new Option("<spring:message code='LB.FX_Time_Deposit_Termination' />", "f_deposit_cancel"));
				}
				//<!-- 外匯定存自動轉期申請/變更 -->
				options.push(new Option("<spring:message code='LB.FX_Time_Deposit_Automatic_Rollover_Application_Change' />", "f_renewal_apply"));
				
				var qm = $("#actionBar").clone();
				qm.children()[0].id = "n530";
				qm.children()[0].name=i;
				qm.show();
				options.forEach(function(x){
					qm[0].children[0].options.add(x);
				});
				
				d["QM"]=qm.html();
			});
			//總計
			FCYTOTMAT = data.data.FCYTOTMAT;
			
			FCYTOTMAT.forEach(function(d,i){
				var da = new Object();
				da["A1"]=d.split(" ")[0];
				da["A2"]=d.split(" ")[1];
				n530_total_rows.push(da);
			});
			//
			createLoadingBox("n530_InnerBOX1",i18n['TotalRow']+':'+data.data.CMRECNUM+i18n['ROWS']);
			
		} else {
			error= true;
			// 錯誤訊息
			n530_columns = [ {
					"data" : "MSGCODE",
					"title" : '<spring:message code="LB.Transaction_code" />'
				}, {
					"data" : "MESSAGE",
					"title" : '<spring:message code="LB.Transaction_message" />'
				}
			];
			n530_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];
		}
		createTable("n530_BOX",n530_rows,n530_columns,n530_align);
		if(!error)
		{
		createTable("n530_InnerBOX1",n530_total_rows,n530_total_columns,n530_total_align);
		}
	}

</script>
<p id="n530_title" class="home-title" style="display: none"><spring:message code="LB.X2358" /></p>
<div id="n530" class="main-content-block" style="display:none"> 
<div id="n530_BOX" style="display:none">
	<form method="post" id="fastBarAction_fcy" action="">
		<input type="hidden" id="FDPNO" name="FDPNO" value="">
		<input type="hidden" id="ACN530" name="ACN530" value="">
		<input type="hidden" id="FGSELECT530" name="FGSELECT530" value="">
	</form>  
</div>
<div id="n530_InnerBOX1" style="display:none"></div>
</div>


