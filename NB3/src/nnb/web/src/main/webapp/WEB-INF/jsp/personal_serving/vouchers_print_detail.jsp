<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function() {
		initFootable();
		$("#printbtn").click(function(){
			var params = {
				"jspTemplateName":"vouchers_print_detail_print",
				"jspTitle":"<spring:message code= "LB.X0141" />",
				"YEARLY":"${vouchers_print_detail.data.YEARLY}",
				"TAXPYCD":"${vouchers_print_detail.data.TAXPYCD}",
				"LIVE183":"${vouchers_print_detail.data.LIVE183}",
				"BUSIDN":"${vouchers_print_detail.data.BUSIDN}",
				"TAMPAY":"${vouchers_print_detail.data.TAMPAY}",
				"TAXBAID":"${vouchers_print_detail.data.TAXBAID}",
				"TAX":"${vouchers_print_detail.data.TAX}",
				"ACN1":"${vouchers_print_detail.data.ACN1}",
				"ACN2":"${vouchers_print_detail.data.ACN2}",
				"ACN3":"${vouchers_print_detail.data.ACN3}",
				"ACN4":"${vouchers_print_detail.data.ACN4}",
				"ACN5":"${vouchers_print_detail.data.ACN5}",
				"ACN6":"${vouchers_print_detail.data.ACN6}",
				"ACN7":"${vouchers_print_detail.data.ACN7}",
				"ACN8":"${vouchers_print_detail.data.ACN8}",
				"ACN9":"${vouchers_print_detail.data.ACN9}",
				"ACN10":"${vouchers_print_detail.data.ACN10}",
				"ACN11":"${vouchers_print_detail.data.ACN11}",
				"ACN12":"${vouchers_print_detail.data.ACN12}",
				"C_TAX":"${vouchers_print_detail.data.C_TAX}",
				"TBB_OWNER":"${vouchers_print_detail.data.TBB_OWNER}",
				"COUNTRY":"${vouchers_print_detail.data.COUNTRY}",
				"W_TAX":"${vouchers_print_detail.data.W_TAX}",
				"CTYIDN":"${vouchers_print_detail.data.CTYIDN}",
				"FORM":"${vouchers_print_detail.data.FORM}",
				"TRANS_FORM":"${vouchers_print_detail.data.TRANS_FORM}",
				"NETPAY":"${vouchers_print_detail.data.NETPAY}",
				"AMTTAX":"${vouchers_print_detail.data.AMTTAX}",
				"NAME":"${vouchers_print_detail.data.NAME}",
				"SEQCLM":"${vouchers_print_detail.data.SEQCLM}",
				"ADDRESS":"${vouchers_print_detail.data.ADDRESS}",
				"BACKAMT":"${vouchers_print_detail.data.BACKAMT}",
				"CUSIDN":"${vouchers_print_detail.data.CUSIDN}",
				"N106RATE":"${vouchers_print_detail.data.N106RATE}",
				"CURDATE":"${vouchers_print_detail.data.CURDATE}"
				
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});

		$('#previous').click(function(e){
			$("#formId").attr("action","${__ctx}/PERSONAL/SERVING/vouchers_print_list");
	   		$("#formId").submit();

			 
		});
	});
	</script>
	
</head>
<body>
	

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 各類所得扣繳憑單列印     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0141" /></li>
		</ol>
	</nav>

		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
            <section id="main-content" class="container">
                <h2><spring:message code="LB.X0141" /></h2>
                <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="${__ctx}/PERSONAL/SERVING/vouchers_print_detail">
			    <input type="hidden" id="ADOPID" name="ADOPID" value="N106_List">
                <div class="main-content-block row">
                <div style="width:90%;margin: auto;">
                <c:choose>
                	<c:when test="${vouchers_print_detail.data.FORM != '5C' && vouchers_print_detail.data.FORM != '60' && vouchers_print_detail.data.FORM != '61' && vouchers_print_detail.data.FORM != '90' && vouchers_print_detail.data.FORM != '96' }">
                	   <table border="0" width="100%" id="table3" style="border-collapse: collapse" cellpadding="0">
					<tr>
						<td rowspan="2" width="480">
						<p align="right"><b><font face="標楷體" style="font-size: 16pt">
						各類所得扣繳暨免扣繳憑單</font><font face="標楷體" size="4">&nbsp;&nbsp;&nbsp; <br>
						</font>
						</b><font face="標楷體">Withholding &amp; Non-Withholding&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br>
						Tax Statement(金融機構電子申報專用)&nbsp;&nbsp;&nbsp; </font></td>
						<td  width="487">　</td>
					</tr>
					<tr>
						<td height="57">
						<table border="0" width="100%" id="table11" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 3px; padding: 0" width="487" align="left"><font face="標楷體" style="font-size: 9pt">租賃房屋之房屋稅籍編號:</font></td>
							</tr>
							<tr>
								<td height="52" style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" width="487" align="left"><font face="標楷體" style="font-size: 9pt">租賃房屋地址:</font></td>
							</tr>
						</table>
						</td>
					</tr>
					</table>
				<table border="0" width="100%" id="table2" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table border="0" width="100%" id="table4" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td align="center" width="255" style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">扣繳單位統一編號</font></td>
								<td align="center"  width="74" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0"><font face="標楷體" style="font-size: 9pt">稽徵機關</font></td>
								<td align="center" width="295"  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">製 單 編 號</font></td>
								<td align="center" width="341"  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">格式代號及所得類別Category of Income</font></td>
							</tr>
							<tr>
								<td align="center" width="255" style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0">${vouchers_print_detail.data.BUSIDN }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="74">${vouchers_print_detail.data.CTYIDN }${vouchers_print_detail.data.TAXBAID }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="295">${vouchers_print_detail.data.BUSIDN }${vouchers_print_detail.data.SEQCLM }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="341">${vouchers_print_detail.data.FORM }</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table5" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="253">
								<font face="標楷體" style="font-size: 9pt">所得人統一編(證)號<br>
								Taxpayer's ID No.</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="199">
								<font face="標楷體" style="font-size: 9pt">國內有無住所<br>
								<u>Residency</u></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="320">
								<font face="標楷體" style="font-size: 9pt">所得人、執業別代號、其他所得給付項目或外僑護照號碼&nbsp;&nbsp;&nbsp; Passport No.</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="80" colspan="2">
								<font face="標楷體" style="font-size: 9pt"><u>國家代碼<br>
								Country<br>
								Code</u></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="111" colspan="2">
								<font face="標楷體" style="font-size: 9pt"><u>適用租稅協定<br>
								Tax<br>
								Agreement</u></font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="253">${vouchers_print_detail.data.CUSIDN }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="199">
								<font face="標楷體" style="font-size: 9pt">有( ｖ )&nbsp; 
								無(&nbsp; )</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="320">${vouchers_print_detail.data.TAXPYCD }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="37">${vouchers_print_detail.data.COUNTRY }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="34"></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="53">${vouchers_print_detail.data.TAX }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="55"></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table6" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="16%"><font face="標楷體">
								<span style="font-size: 9pt">所得人姓名<br>
								</span>
								<font style="font-size: 9pt">Name of Taxpayer</font></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="left" width="37%">${vouchers_print_detail.data.NAME }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="47%">
								<p align="left"><font face="標楷體" size="1"><u>本給付年度內按所得人護照入出境章戳日期累計在華是否已滿183天? </u></font><input type="checkbox" name="C1" <c:if test="${vouchers_print_detail.data.LIVE183 =='Y' }">checked</c:if> disabled>是<input type="checkbox" name="C2" <c:if test="${vouchers_print_detail.data.LIVE183 =='N' }">checked</c:if> disabled>否</td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="16%"><font face="標楷體">
								<span style="font-size: 9pt">所得人地址<br>
												Present Address</span></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="left" colspan="2">${vouchers_print_detail.data.ADDRESS}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table7" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="429"><font face="標楷體" style="font-size: 9pt">所得所屬年月 Period of Income</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="167"><font face="標楷體">
								<span style="font-size: 9pt">所得給付年度<br>
								</span>
								<font style="font-size: 9pt">Year of Payment</font></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="371">
								<font face="標楷體" style="font-size: 9pt">
								依勞退條例自願提繳之退休金額(E)</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="429"><font face="標楷體" style="font-size: 9pt">自${vouchers_print_detail.data.YEARLY } 
								年&nbsp;&nbsp;&nbsp;&nbsp;01&nbsp;&nbsp; 月至${vouchers_print_detail.data.YEARLY } 年&nbsp;&nbsp;&nbsp;12&nbsp;&nbsp;&nbsp;月<br>
								From&nbsp; &nbsp; Year&nbsp; &nbsp;&nbsp; 
								Month To&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Year&nbsp;&nbsp;&nbsp;&nbsp; 
								Month</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="167">${vouchers_print_detail.data.YEARLY }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="371">${vouchers_print_detail.data.BACKAMT }</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table8" height="134" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="279" height="64"><font face="標楷體">
								<font style="font-size: 9pt">給付總額(A)Total Amount Paid</font><br>
								<font size="1">(結算申報時應按本欄數額填報)<br>
								The above amount should be reported in the Individual Income Tax Return</font></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="150" height="64">
								<font face="標楷體" style="font-size: 9pt">扣 繳 
								率(<u>B</u>)<br>
								Withholding Rate</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="290" height="64" colspan="2">
								<font face="標楷體" style="font-size: 9pt">
								扣繳稅額(<u>C=C1-C2</u>)<u><br>
								</u>Net Withholding Tax</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" height="64" width="248">
								<font face="標楷體" style="font-size: 9pt">給付淨額<u>(D=A-C)<br>
								</u>Net Payment</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="279" rowspan="3">${vouchers_print_detail.data.TAMPAY }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="150" rowspan="3">
								<p align="center"><font face="標楷體">${vouchers_print_detail.data.N106RATE } %</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="290" colspan="2" height="27">${vouchers_print_detail.data.AMTTAX }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" rowspan="3" width="248">${vouchers_print_detail.data.NETPAY }</td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="132" height="33">
								<font face="標楷體" style="text-decoration: underline" size="1">
								應扣繳稅額(C1=AXB)Withholding Tax</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="157" height="33">
								<font face="標楷體" style="text-decoration: underline" size="1">
								股利或盈餘抵繳稅額(C2)Creditable Surtax</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="132">${vouchers_print_detail.data.W_TAX }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="157">${vouchers_print_detail.data.C_TAX }</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table9" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="44" rowspan="3">
								<font face="標楷體" style="font-size: 9pt">
								存號款明帳細</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN1 != '' }">${vouchers_print_detail.data.ACN1}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN2 != '' }">${vouchers_print_detail.data.ACN2}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN3 != '' }">${vouchers_print_detail.data.ACN3}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN4 != '' }">${vouchers_print_detail.data.ACN4}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN5 != '' }">${vouchers_print_detail.data.ACN5}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN6 != '' }">${vouchers_print_detail.data.ACN6}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN7 != '' }">${vouchers_print_detail.data.ACN7}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN8 != '' }">${vouchers_print_detail.data.ACN8}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN9 != '' }">${vouchers_print_detail.data.ACN9}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN10 != '' }">${vouchers_print_detail.data.ACN10}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN11 != '' }">${vouchers_print_detail.data.ACN11}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${vouchers_print_detail.data.ACN12 != '' }">${vouchers_print_detail.data.ACN12}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table10" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" colspan="2"  width="523">
								<font face="標楷體" style="font-size: 9pt">扣&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								款&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 單&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								位</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="440">
								<font face="標楷體" style="font-size: 9pt">格式代號說明 Category 
								of Income</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								稱</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="378">
								<font face="標楷體" style="font-size: 9pt">臺 灣 
								中 小 企 業 銀 行</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="left" width="440" rowspan="3">
								<font face="標楷體" size="1">
								${vouchers_print_detail.data.FORM}:${vouchers_print_detail.data.TRANS_FORM}&nbsp;&nbsp;&nbsp;&nbsp;<br><br><br><br>
								</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								址</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="378">
								<font face="標楷體" style="font-size: 9pt">台 北 
								市 塔 城 街 30 號</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">扣繳義務人</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="center" width="378">${vouchers_print_detail.data.TBB_OWNER}</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				
				<div style="font-family: 標楷體" align="left"><font style="font-size: 8.5pt">※<u>依勞工退休金條例規定自願提繳之退休金或年金保險費，合計在每月工資6%範圍內，免計入薪資給付總額，其金額應另行填寫於(E)欄。</u><br>
				第２聯：備查聯　交所得人保存備查<br>
				Copy II For the taxpayer's reference.</font></div>
                	</c:when>
                	<c:otherwise>
                		<table border="0" width="100%" id="table3" style="border-collapse: collapse" cellpadding="0">
					<tr>
						<td width="887">
						<p align="center"><b><font face="標楷體" style="font-size: 16pt">
						各類所得扣繳暨免扣繳憑單</font><font face="標楷體" size="4">&nbsp;&nbsp;&nbsp; <br>
						</font>
						</b><font face="標楷體">Withholding &amp; Non-Withholding Tax Statement&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br>
						(For the Income of Individuals to be taxed separately and of Profit - Seeking Enterprise)<br>
						(個人分離課稅所得及營利事業同類所得電子申報專用)</font></td>
					</tr>
					</table>
				<table border="0" width="100%" id="table2" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table border="0" width="100%" id="table4" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td align="center" width="253" style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">扣繳單位統一編號</font></td>
								<td align="center"  width="74" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0"><font face="標楷體" style="font-size: 9pt">稽徵機關</font></td>
								<td align="center" width="295"  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">製 單 編 號</font></td>
								<td align="center" width="341"  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">格式代號及所得類別<br>Category of Income</font></td>
							</tr>
							<tr>
								<td align="center" width="253" style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0">${vouchers_print_detail.data.BUSIDN}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="74">${vouchers_print_detail.data.CTYIDN}${vouchers_print_detail.data.TAXBAID}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="295">${vouchers_print_detail.data.BUSIDN}${vouchers_print_detail.data.SEQCLM}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="341">${vouchers_print_detail.data.FORM}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table5" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="253">
								<font face="標楷體" style="font-size: 9pt">所得人統一編(證)號<br>
								Taxpayer's ID No.</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="199">
								<font face="標楷體" style="font-size: 9pt">國內有無住所<br>
								<u>Residency</u></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="300">
								<font face="標楷體" style="font-size: 9pt">所得人代號或外僑護照號碼<br>Passport No.</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="90" colspan="2">
								<font face="標楷體" style="font-size: 9pt"><u>國家代碼<br>
								Country	Code</u></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="121" colspan="2">
								<font face="標楷體" style="font-size: 9pt"><u>適用租稅協定<br>
								Tax	Agreement</u></font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="253">${vouchers_print_detail.data.CUSIDN}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="199">
								<font face="標楷體" style="font-size: 9pt">有( ｖ )&nbsp; 
								無(&nbsp; )</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="320">${vouchers_print_detail.data.TAXPYCD}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="37">${vouchers_print_detail.data.COUNTRY}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="34"></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="53">${vouchers_print_detail.data.TAX}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="55"></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table6" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="26%"><font face="標楷體">
								<span style="font-size: 9pt">所得人姓名 Name of Taxpayer </span></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="left" width="27%">${vouchers_print_detail.data.NAME}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="47%">
								<p align="left"><font face="標楷體" size="1"><u>本給付年度內按所得人護照入出境章戳日期累計在華是否已滿183天? </u></font><input type="checkbox" name="C1" <c:if test="${vouchers_print_detail.data.LIVE183 =='Y' }">checked</c:if> disabled>是<input type="checkbox" name="C2" <c:if test="${vouchers_print_detail.data.LIVE183 =='N' }">checked</c:if> disabled>否</td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="16%"><font face="標楷體">
								<span style="font-size: 9pt">所得人地址 Present Address</span></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="left" colspan="2">${vouchers_print_detail.data.ADDRESS}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table7" style="border-collapse: collapse" cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="479"><font face="標楷體" style="font-size: 9pt">所得所屬年月 Period of Income</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="217"><font face="標楷體">
								<span style="font-size: 9pt">所得給付年度<br>
								</span>
								<font style="font-size: 9pt">Year of Payment</font></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="271">
								<font face="標楷體" style="font-size: 9pt">
								扣 繳 率 <br>Withholding Rate</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="479"><font face="標楷體" style="font-size: 9pt">自${vouchers_print_detail.data.YEARLY} 
								年&nbsp;&nbsp;&nbsp;&nbsp;01&nbsp;&nbsp; 月至${vouchers_print_detail.data.YEARLY} 年&nbsp;&nbsp;&nbsp;12&nbsp;&nbsp;&nbsp;月<br>
								From&nbsp; &nbsp; Year&nbsp; &nbsp;&nbsp; 
								Month To&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Year&nbsp;&nbsp;&nbsp;&nbsp; 
								Month</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="217">${vouchers_print_detail.data.YEARLY} </td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="271">${vouchers_print_detail.data.N106RATE} %</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table8" height="134" style="border-collapse: collapse" cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="329" height="64"><font face="標楷體">
								<font style="font-size: 9pt">給付總額(A)<br>Total Amount Paid</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="340" height="64">
								<font face="標楷體" style="font-size: 9pt">扣繳稅額(B)<br>Net Withholding Tax</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" height="64" width="298">
								<font face="標楷體" style="font-size: 9pt">給付淨額(A)-(B)<br>	Net Payment</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="329">${vouchers_print_detail.data.TAMPAY}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="340" height="27">${vouchers_print_detail.data.AMTTAX}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="298">${vouchers_print_detail.data.NETPAY}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table9" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="5%">
								<font face="標楷體" style="font-size: 9pt">說<br>明</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="left" width="95%">
								本憑單之各項所得，所得人為個人時，給付總額經依規定扣繳稅款後，免併計綜合所得總額課稅，其已扣繳稅款亦不得抵繳應納稅額；所得人非屬個人時，除發票日在98年12月31日以前之短期票券利息及依法免辦理結算申報者外，仍應併入營利事業所得額課稅，其已扣繳稅款得抵繳應納稅額。
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table10" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" colspan="2"  width="523">
								<font face="標楷體" style="font-size: 9pt">扣&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								繳&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 單&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								位</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="440">
								<font face="標楷體" style="font-size: 9pt">格式代號說明 Category 
								of Income</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								稱</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="378">
								<font face="標楷體" style="font-size: 9pt">臺 灣 
								中 小 企 業 銀 行</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="left" width="440" rowspan="3">
								<font face="標楷體" size="1">
								5C：公債、公司債或金融債券利息<br>
								60：資產基礎證券分配之利息<br>
								61：附條件交易之利息<br>
								90：告發或檢舉獎金<br>
								96：結構型商品交易之所得
								</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								址</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="378">
								<font face="標楷體" style="font-size: 9pt">台 北 
								市 塔 城 街 30 號</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">扣繳義務人</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="center" width="378">${vouchers_print_detail.data.TBB_OWNER}</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				
				<div style="font-family: 標楷體" align="left"><font style="font-size: 8.5pt">※本扣繳憑單適用於99年1月1日起給付之所得。<br>
				第２聯：備查聯　交所得人保存備查。Copy II For the taxpayer's reference.</font></div>
                	</c:otherwise>
                </c:choose>
				 </div> 
				
	
                </div>
              </form>
              <div style="text-align:center">
              	<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" >
				<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
			</div>
            </section>
        </main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>