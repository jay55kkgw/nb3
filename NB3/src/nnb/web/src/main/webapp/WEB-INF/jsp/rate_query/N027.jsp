<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<style type="text/css">
.content12-gy {
	FONT-SIZE: 12px; COLOR: #575757; LINE-HEIGHT: 16px; FONT-FAMILY: "Arial", "新細明體", "taipei"
}
.content12-gy A:link {
	COLOR: #575757; TEXT-DECORATION: none
}
.content12-gy A:visited {
	COLOR: #575757; TEXT-DECORATION: none
}
.content12-gy A:hover {
	COLOR: #ff6600; TEXT-DECORATION: underline
}
.content12-bk {
	FONT-SIZE: 12px; COLOR: #000000; LINE-HEIGHT: 16px; FONT-FAMILY: "Arial", "新細明體", "taipei"
}
</style>
</HEAD>
<body>
<c:choose>
<c:when test="${N027data.data.RECSIZE=='0'}">
查無資料!!!
</c:when>
<c:when test="${N027data.data.RECSIZE!='0'}">
<H3 ALIGN=CENTER></H3>
	<H3 ALIGN=CENTER>
		${N027data.data.JSPTITLE}
	</H3>
	<P>
	<center>
		<table border=0 width='65%'>
			<tr>
				<TH align="left"><FONT SIZE=-1>掛牌日期： ${N027data.data.UPDATEDATE}
			</tr>
		</table>
	</center>
	<c:forEach var="dataList" items="${N027data.data.REC}" varStatus="data">
		<center>
		<table width='65%' cellpadding='1' cellspacing='1' bgcolor='FFBD57'
			ALIGN='CENTER'>
			<fmt:parseNumber var = "TITLE1num" type = "number" value = "${dataList.TITLE1}" />
			<fmt:parseNumber var = "TITLE2num" type = "number" value = "${dataList.TITLE2}" />
			<fmt:parseNumber var = "TITLE3num" type = "number" value = "${dataList.TITLE3}" />
			<TR>
				<TH bgcolor='FC9A05' class='content12-bk' rowspan="2"><FONT
					SIZE='-1'>期 別</FONT></TH>
				<c:if test="${TITLE1num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>大額${dataList.amt1}億元(不含)以上&nbsp;
					</FONT></TH>
				</c:if>
				<c:if test="${TITLE2num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>大額${dataList.amt2}億元(不含)以上&nbsp;
					</FONT></TH>
				</c:if>
				<c:if test="${TITLE3num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>大額${dataList.amt3}億元(不含)以上&nbsp;
					</FONT></TH>
				</c:if>
			</tr>
			<TR>
				<c:if test="${TITLE1num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>固 定</FONT></TH>
				</c:if>
				<c:if test="${TITLE2num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>固 定</FONT></TH>
				</c:if>
				<c:if test="${TITLE3num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>固 定</FONT></TH>
				</c:if>
			</TR>
			<TR>
				<td bgcolor='white' class='content12-gy' Align='center'>${dataList.TERM}&nbsp;</td>
				<c:if test="${TITLE1num > 0}">
					<td bgcolor='white' class='content12-gy'
					Align='center'>${dataList.ITR1}&nbsp;</td>
				</c:if>
				<c:if test="${TITLE2num > 0}">
					<td bgcolor='white' class='content12-gy'
					Align='center'>${dataList.ITR2}&nbsp;</td>
				</c:if>
				<c:if test="${TITLE3num > 0}">
					<td bgcolor='white' class='content12-gy'
					Align='center'>${dataList.ITR3}&nbsp;</td>
				</c:if>
			</TR>
		</table>
		</center>
		<BR>
		<BR>
	</c:forEach>
</c:when>
</c:choose>
</BODY>
</HTML>