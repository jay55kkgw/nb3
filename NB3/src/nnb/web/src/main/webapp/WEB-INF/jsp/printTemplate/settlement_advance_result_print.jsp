<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
		
			<c:if test="${ FGTXDATE.equals('1') }">						
				<p style="text-align: center;color: red;"><spring:message code= "LB.W1718" /></p>
			</c:if>
			<c:if test="${ !FGTXDATE.equals('1') }">						
				<p style="text-align: center;color: red;"><spring:message code= "LB.W1719" /></p>
			</c:if>
			

			<table class="print">
				<tr>
					<td style="text-align: center"><spring:message code= "LB.Trading_time" /></td>
					<td>${CMQTIME}</td>
				</tr>
				<c:if test="${ FGTXDATE.equals('1') }">
					<tr>
						<td style="text-align: center"><spring:message code= "LB.Transfer_date" /></td>
						<td>${PAYDATE}</td>
					</tr>
				</c:if>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.Payers_account_no" />
					</td>
					<td>${ACN_OUT}</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.W0891" />
					</td>
					<td>${ACN}</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.W1707" />
					</td>
					<td>${SEQ}</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.Amount" />
					</td>
					<td>${PALPAY}</td>
				</tr>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.Transfer_note" />
					</td>
					<td>${CMTRMEMO}</td>
				</tr>
			</table>
		</div>
		
		<br>
		<br>
	</div>
	
</body>
</html>