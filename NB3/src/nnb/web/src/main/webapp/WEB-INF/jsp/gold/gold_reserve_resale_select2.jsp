<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
 	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
  	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	if(${(requestParam.GDBAL != requestParam.TSFBAL) || requestParam.TSFBAL == "0.00"}){
		$("input[name=SELLFLAG][value=A]").prop("disabled",true);
	}
	else{
		$("input[name=SELLFLAG][value=A]").prop("disabled",false);
	}
	
	$("#payButton").click(function(){
		$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/pay_fail_fee");
		$("#formID").submit();
	});
	$("#CMSUBMIT").click(function(){
		//若為部分賣出須檢核賣出公克數
		if($("input[name=SELLFLAG]:checked").val() == "P"){
			if($("#TRNGD_P").val().indexOf(".") != -1){
				//請輸入整數的賣出公克數
				//alert("<spring:message code= "LB.X0940" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X0940' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
				return false;
			}
//賣出公克數
		   	if(!CheckNumber("TRNGD_P","<spring:message code= "LB.W1519" />",false)){
		   	    return false;
		   	}
			if(parseInt($("#TRNGD_P").val()) < 1 || parseFloat($("#TRNGD_P").val()) > parseFloat($("#TSFBAL").val())){
				//每筆最低交易數量為 1 公克，最高交易數量不可超過黃金存摺可用餘額
				//alert("<spring:message code= "LB.X0941" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X0941' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
				return false;
			}
			$("#TRNGD").val($("#TRNGD_P").val()); 
		}
		else{
			$("#TRNGD").val($("#TSFBAL").val());
		}
		
		$('#formID').attr("action","${__ctx}/GOLD/TRANSACTION/gold_reserve_resale_confirm");
		$('#formID').removeAttr("target");
		$("#formID").submit();
	});
	$("#resetButton").click(function(){
		$("#TRNGD_P").val("");
	});
});
function priceQuery(){
	$('#formID').attr("action","${__ctx}/GOLD/PASSBOOK/history_price_query_result");
	$('#formID').attr("target","_balnk");
	$('#formID').submit();
}
</script>
</head>
<body>
	<!--header-->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 黃金回售     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1512" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> 
			<!--主頁內容-->
			<section id="main-content" class="container">
			<!-- 預約黃金回售 -->
				<h2><spring:message code="LB.X0952"/></h2><i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00"></i>
                <form id="formID" action="${__ctx}/GOLD/TRANSACTION/gold_reserve_resale_confirm" method="post">
                	<input type="hidden" name="ADOPID" value="N09102"/>
					<input type="hidden" name="GDBAL" value="<c:out value='${fn:escapeXml(requestParam.GDBAL)}' />"/>
					<input type="hidden" id="TSFBAL" name="TSFBAL" value="<c:out value='${fn:escapeXml(requestParam.TSFBAL)}' />"/>
					<input type="hidden" name="ACN" value="<c:out value='${fn:escapeXml(requestParam.ACN)}' />"/>
					<input type="hidden" name="SVACN" value="<c:out value='${fn:escapeXml(requestParam.SVACN)}' />"/>
					<input type="hidden" name="FEEAMT1" value="<c:out value='${fn:escapeXml(requestParam.FEEAMT1)}' />"/>
					<input type="hidden" name="FEEAMT2" value="<c:out value='${fn:escapeXml(requestParam.FEEAMT2)}' />"/>
					<input type="hidden" id="TRNGD" name="TRNGD"/>
					<input type="hidden" name="QUERYTYPE" value="LASTMON">
	                <div class="main-content-block row">
	                	<c:if test="${requestParam.FEEAMT1Format != '0'}">
	                		<div class="col-12">
	                		<!-- 貴用戶尚有黃金存摺定期定額扣款失敗手續費未繳納，請先繳納後再執行預約黃金回售功能。 -->
								<h4 style="text-align:center;margin:50px 0 50px 0"><b><font color="red"><spring:message code= "LB.X1196" /></font></b></h4>
	                			
	                			<!-- 立即繳費 -->
		                  			<input type="button" id="payButton" value="<spring:message code= "LB.X1197" />" class="ttb-button btn-flat-orange"/>
		                		
	                		</div>
	                	</c:if>
	                	<c:if test="${requestParam.FEEAMT1Format == '0'}">
		                    <div class="col-12 tab-content">
		                        <div class="ttb-input-block">
									<!--黃金轉出帳號-->
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
											<label>
<!-- 黃金轉出帳號 -->
												<h4><spring:message code= "LB.W1515" /></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<c:out value='${fn:escapeXml(requestParam.ACN)}' />
											</div>
										</span>
									</div>
									<!--黃金存摺帳戶餘額-->
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
											<label>
<!-- 黃金存摺帳戶餘額 -->
												<h4><spring:message code= "LB.W1516" /></h4>
											</label>
										</span>
		                                <span class="input-block">
											<div class="ttb-input">
<!-- 公克 -->
		                                       	<c:out value='${fn:escapeXml(requestParam.GDBAL)}' /><spring:message code= "LB.W1435" />
											</div>
										</span>
									</div>
									<!--黃金存摺可用餘額-->
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
											<label>
<!-- 黃金存摺可用餘額 -->
												<h4><spring:message code= "LB.W1517" /></h4>
											</label>
										</span>
		                                <span class="input-block">
											<div class="ttb-input">
<!-- 公克 -->
		                                       	<c:out value='${fn:escapeXml(requestParam.TSFBAL)}' /><spring:message code= "LB.W1435" />
											</div>
										</span>
									</div>
									<!--台幣轉入帳號-->
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
											<label>
<!-- 臺幣轉入帳號 -->
												<h4><spring:message code= "LB.W1518" /></h4>
											</label>
										</span>
		                                <span class="input-block">
											<div class="ttb-input">
		                                       	<c:out value='${fn:escapeXml(requestParam.SVACN)}' />
											</div>
										</span>
									</div>
									<!--賣出公克數-->
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
											<label>
<!-- 賣出公克數 -->
												<h4><spring:message code= "LB.W1519" /></h4>
											</label>
										</span>
		                                <span class="input-block">
											<div class="ttb-input">
<!-- 部分賣出 -->
												<label class="radio-block"><spring:message code= "LB.W1520" />
                                            		<input type="radio" name="SELLFLAG" value="P" checked/>
                                            		<span class="ttb-radio"></span>
                                        		</label>
                                        	</div>
                                        	<div class="ttb-input">
<!-- 公克 -->
		                                       	<input type="text" class="text-input" style="width:100px;" id="TRNGD_P" name="TRNGD_P" maxLength="5" />&nbsp;&nbsp;<spring:message code= "LB.W1435" />&nbsp;&nbsp;
<!-- 走勢 -->
												<a href="javascript:void(0)" onclick="priceQuery()"><spring:message code= "LB.W1501" /></a><br/>
											</div>
<!-- 全部賣出 -->
											<div class="ttb-input">
												<label class="radio-block"><spring:message code= "LB.W1522" />
                                            		<input type="radio" name="SELLFLAG" value="A"/>
                                            		<span class="ttb-radio"></span>
                                        		</label>
											</div>
										</span>
									</div>
								</div>
								<!-- 確定 -->
		                		<input type="button" id="CMSUBMIT" value="<spring:message code= "LB.Confirm" />" class="ttb-button btn-flat-orange"/>		                			
		                    </div>
						</c:if>
	                </div>
	                <c:if test="${requestParam.FEEAMT1Format == '0'}">
									<ol class="description-list list-decimal">
										<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
											<!-- 「預約黃金回售」免手續費。 -->
											<li><spring:message code= "LB.Gold_Reserve_Resale_P2_D1" /></li>
											<!-- 本服務限約定帳戶，如尚未約定，請洽往來分行辦理。 -->
											<li><spring:message code= "LB.Gold_Reserve_Resale_P2_D2" /></li>
											<!-- 預約黃金買進／回售交易時間為非營業日及營業日8：59以前或15：30以後。 -->
											<li><font style="color:red;"><spring:message code= "LB.Gold_Reserve_Resale_P2_D3" /></font></li>
											<!--每筆交易數量最低為1公克，庫存黃金帳戶餘額如不足1公克須全部一次賣出。-->
											<li><spring:message code= "LB.Gold_Reserve_Resale_P2_D4" /></li>
											<!-- 預約黃金回售將以預約後的第一個營業日的第一次牌告買進價格交易。 -->
											<li><font color="red"><spring:message code= "LB.Gold_Reserve_Resale_P2_D5" /></font></li>
											<!-- 黃金存摺不支付利息，黃金價格有漲有跌，投資時可能產生收益或損失，請慎選買賣時機，並承擔風險。 -->
											<li><spring:message code= "LB.Gold_Reserve_Resale_P2_D6" /></li>
										</ol>
					</c:if>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>