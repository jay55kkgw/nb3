<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>

<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>

	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdProcess.js"></script>

<script type="text/javascript">
var urihost = "${__ctx}";
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 1000);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		
	});

	function init() {
		
		//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });

		$("#CMSUBMIT").click(function(e) {
			if (!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
//  				initBlockUI();
 				processQuery();
// 				$("#formId").submit();
 			}
		});
		
		$("#information").click(function(e) {
			window.open("${__ctx}/ONLINE/SERVING/idgate_more","_black");
		});

	}//init END
	
	// 交易機制選項
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
	
		switch(fgtxway) {
			case '0':
//					alert("交易密碼(SSL)...");
    			$('#formId').submit();
				break;
			case '2':
				
				// 呼叫讀卡機元件
				listReaders();
				
		    	break;
		    	
		    	
			default:
				//alert("nothing...");
				errorBlock(
						null, 
						null,
						["nothing..."], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
		}
		
	}
	


	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	
	
	
	/**************************************/
	/*          複寫checkIdProcess         */
	/**************************************/
	//取得卡片主帳號結束
// 	function getMainAccountFinish(result){
// 		//成功
// 		if(result != "false"){
// 			var cardACN = result;
// 			$("#ACNNO").val(cardACN);
// 			if(cardACN.length > 11){
// 				cardACN = cardACN.substr(cardACN.length - 11);
// 			}
// 			var UID = $("#CUSIDN").val();
// //			var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
// //			var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
			
// 			var uri = urihost+"/COMPONENT/component_without_id_aj";
// 			var rdata = { ACN: cardACN ,UID: UID };
// 			fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
// 		}
// 		//失敗
// 		else{
// 			showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",false);
// 		}
// 	}
	//卡片押碼結束
// 	function generateTACFinish(result){
// 		//成功
// 		if(result != "false"){
// 			var TACData = result.split(",");
			
// 			var main = document.getElementById("formId");
// 			main.iSeqNo.value = TACData[1];
// 			main.ICSEQ.value = TACData[1];
// 			main.TAC.value = TACData[2];

// 			var ACN_Str1 = main.ACNNO.value;
// 			main.CHIP_ACN.value = ACN_Str1;
// 			main.OUTACN.value = ACN_Str1;
// 			main.ACNNO.value = ACN_Str1;
// 			main.submit();
// 		}
// 		//失敗
// 		else{
// 			FinalSendout("MaskArea",false);
// 		}
// 	}
	
	
	
	

	/**************************************/
	/*     			 發送簡訊   			      */
	/**************************************/
	var sec = 120;
	var the_Timeout;

	function countDown()
	{
	     var main = document.getElementById("formId");
	                
	      var counter = document.getElementById("CountDown");                      
	       counter.innerHTML = sec;
	       sec--;
	       
	       if (sec == -1) 
	       {              		
				$("#getotp").prop("disabled",false);
				$("#getotp").removeClass("btn-flat-gray");
				$("#getotp").addClass("btn-flat-orange");
			   sec =120;

	           return;                
	       }                                     
	           
	       //網頁倒數計時(120秒)              
	       the_Timeout = setTimeout("countDown()", 1000);    
	} 
	    //取得otp並發送簡訊
	    function getsmsotp() {
	    	var cusidn = '${sessionScope.cusidn}';
	    	console.log("cusidn>>"+cusidn);
	    	cusidn = atob(cusidn);
			var main = document.getElementById("formId");
			$("#getotp").prop("disabled",true);
			$("#getotp").removeClass("btn-flat-orange");
			$("#getotp").addClass("btn-flat-gray");
			sec = 120;
			countDown();
			$.ajax({
		        type :"POST",
		        url  : "${__ctx}/RESET/smsotp_ajax",
		        data : { 
		        	CUSIDN: cusidn,
		        	ADOPID: 'IDGATE',
		        	MSGTYPE: 'IDGATE'
		        },
		        async: false ,
		        dataType: "json",
		        success : function(msg) {
		        	callbackgetsmsotp(msg)
		        }
		    })
		}
	    
	    
		function callbackgetsmsotp(response) {
			//alert(response.responseText);
		    var main = document.getElementById("formId");
			//eval("var result = " + response.responseText);
			console.log(response);
			var msgCode = response[0].MSGCOD;
			var msgName = response[0].MSGSTR;
			var phone = response[0].phone;
			if(msgCode=="0000")
			{	
				phone = phone = phone.substring(0,6)+"***"+phone.substring(9);
			    //alert("<spring:message code= "LB.Alert198" />");
			    errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X2280' />"+phone+"<br/><spring:message code= 'LB.X2281' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			    main.OTP.focus();			
			}
			else
			{
				sec = 0;
	      		var counter = document.getElementById("CountDown");                      
	       		counter.innerHTML = sec;		
				$("#getotp").prop("disabled",false);
				$("#getotp").removeClass("btn-flat-gray");
				$("#getotp").addClass("btn-flat-orange");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.error_code' />" + ":" + msgCode + " " + msgName], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}				
		}
	
	
</script>

</head>
<body>
	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>
	
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- IDGATE 申請   確認頁 (交易機制)   N960 拿手機號碼-->
			<li class="ttb-breadcrumb-item active" aria-current="page">申請裝置認證服務</li>
		</ol>
	</nav>




	<!--     左邊menu 及登入資訊-->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<form method="post" name="formId" id="formId"
				action="${__ctx}/ONLINE/SERVING/idgate_result">
				<!-- 			晶片金融卡 -->
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="ICSEQ1" name="ICSEQ1" value="">
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="CHIP_ACN" name="CHIP_ACN" value="">
				<input type="hidden" id="OUTACN" name="OUTACN" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="jsondc" name="jsondc" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<h2>
					申請裝置認證服務
				</h2>

				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							
									<!--輸入簡訊驗證碼-->
<!--                             <div class="ttb-input-item row"> -->
<!--                                 <span class="input-title"> -->
<!--                                     <label> -->
<%--                                         <h4><spring:message code= "LB.X1574" /></h4> --%>
<!--                                     </label> -->
<!--                                 </span> -->
<!--                                 <span class="input-block"> -->
<!--                                     <div class="ttb-input"> -->
<%--                                       <input type="text" maxLength="8" size="10" class="ttb-input text-input input-width-125 validate[required,funcCall[validate_CheckLenEqual[<spring:message code="LB.Captcha"/>,OTP,false,8,8]],funcCall[validate_CheckNumber['LB.Captcha',OTP]]]" id="OTP" name="OTP" value=""/> --%>
<%--                                       <input class="ttb-sm-btn btn-flat-orange" type="button" id="getotp" name="getotp" value="<spring:message code= "LB.X1576" />" onclick="javascript:getsmsotp()" /> --%>
<!--                                     </div> -->
<%-- 									<span class="ttb-unit"><spring:message code= "LB.X1575" />：<font id="CountDown" color="red"></font><spring:message code= "LB.Second" /></span> --%>
<!--                                 </span> -->
<!--                             </div> -->
							
							
							<div class="ttb-input-item row">
		                    	<span class="input-title">
		                       		<label>
		                           		<h4>交易項目</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>產生裝置認證啟用碼</span>
									</div>
								</span>
							</div>
							
							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4>
											<spring:message code="LB.Transaction_security_mechanism" />
										</h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input" name="card_group">
										<label class="radio-block"><spring:message
												code="LB.Financial_debit_card" /> <input type="radio" id="CMCARD"
											name="FGTXWAY" value="2" checked="checked"> <span class="ttb-radio"></span></label>
									</div>
								</span>
							</div>
							
							<!-- 晶片金融卡-->
<!-- 							<div class="ttb-input-item row" id="chaBlock" > -->
<!-- 								驗證碼 -->
<!-- 								<span class="input-title"> -->
<!-- 									<label> -->
<%-- 										<h4><spring:message code= "LB.Captcha" /></h4> --%>
<!-- 									</label> -->
<!-- 								</span> -->
<!-- 								<span class="input-block"> -->
<%-- 									<spring:message code="LB.Captcha" var="labelCapCode" /> --%>
<!-- 									<input id="capCode" name="capCode" type="text" -->
<!-- 										class="text-input" maxlength="6" autocomplete="off"> -->
<!-- 									<img name="kaptchaImage" class = "verification-img" src="" /> -->
<!-- 									<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" -->
<%-- 										onclick="changeCode()" value="<spring:message code= "LB.Regeneration" />" /> --%>
<!-- 								</span> -->
<!-- 							</div> -->
							
							<div class="ttb-input-item row">
		                    	<span class="input-title">
		                       		<label>
		                           		<h4>說明</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input" style="max-width: 512px;">
										<span>
											<p>
												使用<font style="color: red;">裝置認證</font>除了能提高交易安全外，還可以讓您進行特定交易（例如：非約定轉帳、繳費、繳稅、變更或查詢個人資料...等），免敲密碼，更加方便......
												<input type="button" id="information" class="ttb-sm-btn btn-flat-orange" value="了解更多" />
											</p>
										</span>
									</div>
								</span>
							</div>
							
						</div>
<%-- 						<input type="button" id="pageback" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" /> --%>
						<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
					</div>
				</div>
			</form>

		</section>
		<!-- 		main-content END --> </main>
	</div>
	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>