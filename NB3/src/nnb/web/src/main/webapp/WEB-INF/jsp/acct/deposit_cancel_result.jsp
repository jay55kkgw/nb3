<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<script type="text/javascript">
		$(document).ready(function () {
			//列印
			$("#printbtn").click(function () {
				var params = {
					"jspTemplateName": "deposit_cancel_result_print",
					"jspTitle": '<spring:message code="LB.NTD_Time_Deposit_Termination"/>',
					"MsgName": '${deposit_cancel_result.data.MsgName}',
					"CMQTIME": '${deposit_cancel_result.data.CMQTIME}',
					"FDPACN": '${deposit_cancel_result.data.FDPACN}',
					"FDPTYPE": '${deposit_cancel_result.data.FDPTYPE}',
					"FDPNUM": '${deposit_cancel_result.data.FDPNUM}',
					"AMT": '${deposit_cancel_result.data.AMT}',
					"INTMTH": '${deposit_cancel_result.data.INTMTH}',
					"SHOWDPISDT": '${deposit_cancel_result.data.SHOWDPISDT}',
					"SHOWDUEDAT": '${deposit_cancel_result.data.SHOWDUEDAT}',
					"INTPAY": '${deposit_cancel_result.data.INTPAY}',
					"TAX": '${deposit_cancel_result.data.TAX}',
					"INTRCV": '${deposit_cancel_result.data.INTRCV}',
					"PAIAFTX": '${deposit_cancel_result.data.PAIAFTX}',
					"NHITAX": '${deposit_cancel_result.data.NHITAX}',
					"CMTRMEMO": '${deposit_cancel_result.data.CMTRMEMO}',
				};

				openWindowWithPost("${__ctx}/print",
					"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
					params);
			});
		});
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 綜存定存解約     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_Time_Deposit_Termination" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- content row END -->
	<!-- 		主頁內容  -->
	<main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!--  臺幣綜存定存解約 -->
				<spring:message code="LB.NTD_Time_Deposit_Termination" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<c:set var="BaseResultData" value="${deposit_cancel_result.data}"></c:set>
				<!-- 交易步驟 -->
				<div id="step-bar">
					<ul>
						<!-- 確認資料 -->
						<li class="finished">
							<spring:message code="LB.Confirm_data" />
						</li>
						<!-- 交易完成 -->
						<li class="active">
							<spring:message code="LB.Transaction_complete" />
						</li>
					</ul>
				</div>
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<span>
									<spring:message code= "LB.Terminate_successful" />
								</span>
							</div>
							<!-- 交易時間 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Trading_time" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${BaseResultData.CMQTIME}</span>
									</div>
								</span>
							</div>
							<!-- 帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Account" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${BaseResultData.FDPACN}</span>
									</div>
								</span>
							</div>
							<!-- 存款種類 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Deposit_type" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${BaseResultData.FDPTYPE}</span>
									</div>
								</span>
							</div>
							<!-- 存單號碼 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Certificate_no" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${BaseResultData.FDPNUM}</span>
									</div>
								</span>
							</div>
							<!-- 存單金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Certificate_amount" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<!-- 新台幣 -->
											<spring:message code="LB.NTD" />
											&nbsp;
											<fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.AMT }" />
											&nbsp;
											<!-- 元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!-- 計息方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Interest_calculation" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${BaseResultData.INTMTH}</span>
									</div>
								</span>
							</div>
							<!-- 起存日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Start_date" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${BaseResultData.SHOWDPISDT}</span>
									</div>
								</span>
							</div>
							<!-- 到期日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Maturity_date" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${BaseResultData.SHOWDUEDAT}</span>
									</div>
								</span>
							</div>
							<!-- 利息-->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Interest" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<!-- 新台幣 -->
											<spring:message code="LB.NTD" />
											&nbsp;
											<fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.INTPAY }" />
											&nbsp;
											<!-- 元 -->
											<spring:message code="LB.Dollar" />

										</span>
									</div>
								</span>
							</div>
							<!-- 所得稅 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Income_tax" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<!-- 新台幣 -->
											<spring:message code="LB.NTD" />
											&nbsp;
											<fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.TAX }" />
											&nbsp;
											<!-- 元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!-- 透支息-->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Overdraft" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<!-- 新台幣 -->
											<spring:message code="LB.NTD" />
											&nbsp;
											<fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.INTRCV }" />
											&nbsp;
											<!-- 元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!-- 稅後本息 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.After-tax_principal_and_interest" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<!-- 新台幣 -->
											<spring:message code="LB.NTD" />
											&nbsp;
											<fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.PAIAFTX }" />
											&nbsp;
											<!-- 元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!-- 健保費 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.NHI_premium" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<!-- 新台幣 -->
											<spring:message code="LB.NTD" />
											&nbsp;
											<fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.NHITAX }" />
											&nbsp;
											<!-- 元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!-- 交易備註 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Transfer_note" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${BaseResultData.CMTRMEMO }
										</span>
									</div>
								</span>
							</div>
						</div>
						<!--button 區域 -->
							<!-- 列印  -->
							<spring:message code="LB.Print" var="printbtn"></spring:message>
							<input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
						<!--button 區域 -->
					</div>
				</div>
				<div class="text-left">
					<!-- 		說明： -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li>
							<span><spring:message code="LB.Deposit_cancel_P3_D1" /></span>
						</li>
					</ol>
				</div>
			</form>
		</section>
		<!-- 		main-content END -->
	</main>
	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>