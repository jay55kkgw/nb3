<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">

	// column's title i18n

	
	// n110-臺幣活期性存款餘額
	i18n['ACN'] = '<spring:message code="LB.Account" />'; // 帳號
	i18n['KIND'] = '<spring:message code="LB.Deposit_type" />'; // 存款種類
	i18n['ADPIBAL'] = '<spring:message code="LB.Available_balance" />'; // 可用餘額
	i18n['BDPIBAL'] = '<spring:message code="LB.Account_balance" />'; // 帳戶餘額
	i18n['CLR'] = '<spring:message code="LB.Exchange_ticket_today" />'; // 本日交換票
	i18n['QM'] = '<spring:message code="LB.Quick_Menu" />';//快速選單
	
	var n110_columns = [
		{ "data":"ACN", "title":i18n['ACN'] },
		{ "data":"KIND", "title":i18n['KIND'] },
		{ "data":"ADPIBAL", "title":i18n['ADPIBAL'] },
		{ "data":"BDPIBAL", "title":i18n['BDPIBAL'] },
		{ "data":"CLR", "title":i18n['CLR'] },
		{ "data":"QM","title":i18n['QM'] }
	];
	
	//0置中 1置右
	var n110_align = [0,0,1,1,1,0];
	
	// Ajax_n110-臺幣活期性存款餘額
	function getMyAssets110() {
		$("#n110").show();
		$("#n110_title").show();
		createLoadingBox("n110_BOX",'');
		uri = '${__ctx}' + "/OVERVIEW/allacct_n110aj";
		console.log("allacct_n110aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax_n110, null, "n110");
	}

	// Ajax_callback
	function countAjax_n110(data){
		// 資料處理後呈現畫面
		if (data.result) {
			n110_rows = data.data.REC;
			n110_rows.forEach(function(d,i){
				var options = [];
				//活期性存款明細
				options.push(new Option("<spring:message code='LB.NTD_Demand_Deposit_Detail' />", "demand_deposit_detail"));
				if(d.ACNGROUP == '01'){
					// 當日待補票據明細
					options.push(new Option("<spring:message code='LB.X1571' />", "checking_insufficient"));
					// 已兌現票據明細
					options.push(new Option("<spring:message code='LB.Cashed_Note_Details' />", "checking_cashed"));
				}
				if(d.OUTACN == 'Y'){
					//轉帳
					options.push(new Option("<spring:message code='LB.NTD_Transfer' />", "transfer"));
					//轉入台幣綜存定存
					options.push(new Option("<spring:message code='LB.Open_Taiwan_Currency_Time_Deposit' />", "deposit_transfer"));
					//繳本行信用卡款 
					options.push(new Option("<spring:message code='LB.Payment_The_Banks_Credit_Card' />", "payment"));
					//停車費代扣繳申請
					options.push(new Option("<spring:message code='LB.X0276' />", "n614"));
				}
				if(d.SHOWSELECT=='Y'){
					var qm = $("#actionBar").clone();
					qm.children()[0].id = "n110";
					qm.children()[0].name=i;
					qm.show();
					options.forEach(function(x){
						qm[0].children[0].options.add(x);
					});
					
					d["QM"]=qm.html();
				}
				else{
					d["QM"]="";
				}
				//console.log(d);
			});
		} else {
			// 錯誤訊息
			n110_columns = [ {
					"data" : "MSGCODE",
					"title" : '<spring:message code="LB.Transaction_code" />'
				}, {
					"data" : "MESSAGE",
					"title" : '<spring:message code="LB.Transaction_message" />'
				}
			];
			n110_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];
		}
		createTable("n110_BOX",n110_rows,n110_columns,n110_align);
	}
	

</script>
<p id="n110_title" class="home-title" style="display: none"><spring:message code="LB.X2355" /></p>
<div id="n110" class="main-content-block text-left" style="display:none">
	<div id="n110_BOX">
		<form method="post" id="n110_fastBarAction" action="">
						<input type="hidden" id="n110_FDPNUM" name="n110_FDPNUM" value="">
						<input type="hidden" id="n110_ACN" name="n110_ACN" value="">
						<input type="hidden" id="n110_FGSELECT" name="n110_FGSELECT" value="">
		</form>  
	</div>
	<ol class="description-list list-decimal">
		<p>
			<spring:message code="LB.Description_of_page" />
		</p>
		<li><spring:message code="LB.Balance_query_P1_D1" /></li>
		<li><spring:message code="LB.Balance_query_P1_D2" /></li>
	</ol>
</div>

