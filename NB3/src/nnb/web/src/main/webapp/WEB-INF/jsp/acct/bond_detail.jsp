<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		datetimepickerEvent();
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		$("#CMSUBMIT").click(function(e){
			//打開驗證隱藏欄位
			$("#hideblock_CMSDATE").show();
			$("#hideblock_CMEDATE").show();
			//塞值進span內的input
			$("#validate_CMSDATE").val($("#CMSDATE").val());
			$("#validate_CMEDATE").val($("#CMEDATE").val());
		
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
	        }
	        else{
 				$("#formId").validationEngine('detach');
 				initBlockUI();
 				$("#formId").attr("action","${__ctx}/NT/ACCT/BOND/bond_detail_result");
 	  			$("#formId").submit(); 
 			}		
  		});
	}
	//		日曆欄位參數設定
	function datetimepickerEvent(){

	    $(".CMSDATE").click(function(event) {
			$('#CMSDATE').datetimepicker('show');
		});
	    $(".CMEDATE").click(function(event) {
			$('#CMEDATE').datetimepicker('show');
		});
		
		jQuery('.datetimepicker').datetimepicker({
			timepicker:false,
			closeOnDateSelect : true,
			scrollMonth : false,
			scrollInput : false,
		 	format:'Y/m/d',
		 	lang: '${transfer}'
		});
	}
	
	//下拉式選單
 	function formReset() {
 		//打開驗證隱藏欄位
		$("#hideblock_CMSDATE").show();
		$("#hideblock_CMEDATE").show();
		//塞值進span內的input
		$("#validate_CMSDATE").val($("#CMSDATE").val());
		$("#validate_CMEDATE").val($("#CMEDATE").val());
		if(!$("#formId").validationEngine("validate")){
			e = e || window.event;//forIE
			e.preventDefault();
		}
 		else{
// 	 		initBlockUI();
			if ($('#actionBar').val()=="excel"){
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/bond_detail.xls");
	 		}else if ($('#actionBar').val()=="txt"){
				$("#downloadType").val("TXT");
				$("#templatePath").val("/downloadTemplate/bond_detail.txt");
	 		}
// 			ajaxDownload("${__ctx}/NT/ACCT/BOND/bond_detail_ajaxDirectDownload","formId","finishAjaxDownload()");
			$("#formId").attr("target", "");
            $("#formId").attr("action", "${__ctx}/NT/ACCT/BOND/bond_detail_directDownload");
            $("#formId").submit();
            $('#actionBar').val("");
 		}
	}
 	function finishAjaxDownload(){
		$("#actionBar").val("");
		unBlockUI(initBlockId);
	}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 中央登錄債券查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0033" /></li>
    <!-- 明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0042" /></li>
		</ol>
	</nav>



		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.W0042" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				<form id="formId" method="post" action="">
				<!-- 下載用 -->
				<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0042" />"/>
				<input type="hidden" name="downloadType" id="downloadType"/> 					
				<input type="hidden" name="templatePath" id="templatePath"/> 
				<input type="hidden" name="CMQTIME" value="${bond_detail_result.data.CMQTIME}"/>
				<input type="hidden" name="CMPERIOD" value="${bond_detail_result.data.CMPERIOD}"/>
				<input type="hidden" name="CMRECNUM" value="${bond_detail_result.data.CMRECNUM}"/>	
				<input type="hidden" name="hasMultiRowData" value="true"/>
				<!-- EXCEL下載用 -->
				<input type="hidden" name="headerRightEnd" value="1" />
				<input type="hidden" name="headerBottomEnd" value="5" />
				<input type="hidden" name="multiRowStartIndex" value="9" />
                <input type="hidden" name="multiRowEndIndex" value="9" />
                <input type="hidden" name="multiRowCopyStartIndex" value="6" />
                <input type="hidden" name="multiRowCopyEndIndex" value="10" />
                <input type="hidden" name="multiRowDataListMapKey" value="rowListMap" />
                <input type="hidden" name="rowRightEnd" value="9" />
				<!-- TXT下載用 -->
				<input type="hidden" name="txtHeaderBottomEnd" value="5"/>
				<input type="hidden" name="txtMultiRowStartIndex" value="12"/>
				<input type="hidden" name="txtMultiRowEndIndex" value="12"/>
				<input type="hidden" name="txtMultiRowCopyStartIndex" value="7"/>
				<input type="hidden" name="txtMultiRowCopyEndIndex" value="12"/>
				<input type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap"/>
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 中央登錄債券帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> <label><spring:message code="LB.Account" /></label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select name="ACN" id="ACN" class="custom-select select-input half-input">
												<c:forEach var="dataList" items="${bond_detail.data.REC}">
													<option> ${dataList.ACN}</option>
												</c:forEach>
												<option value="">
													<spring:message code="LB.All" />
												</option>
											</select>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<!-- 交易起迄日 -->
									<span class="input-title">
										<label><spring:message code="LB.W0043" /></label>
									</span>
									<span class="input-block">
										<!-- 輸入 查詢期間-->
										<div class="ttb-input">
											<!--日期區間 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_start_date" />
												</span>
												<jsp:useBean id="now" class="java.util.Date" />
												<fmt:formatDate value="${now}" type="both" dateStyle="long" pattern="yyyy/MM/dd" var="today"/>
												<!--期間起日 -->
												<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" value="${today}" />
												<span class="input-unit CMSDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
											</div>
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_end_date" />
												</span>
												<!--期間迄日 -->
												<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" value="${today}" />
												<span class="input-unit CMEDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<span id="hideblock_date" >
												<!-- 驗證用的input -->
												<input id="odate" name="odate" value="${today}"  type="text" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.X1445" />', odate, CMSDATE, CMEDATE, false, null, null]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
										</div>
									</span>
								</div>
							</div>
							<!-- 重新輸入-->
	                        <input type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter"/>"/>
							<!-- 網頁顯示-->
							<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Display_as_web_page"/>"/>
						</div>
					</div>
				</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>