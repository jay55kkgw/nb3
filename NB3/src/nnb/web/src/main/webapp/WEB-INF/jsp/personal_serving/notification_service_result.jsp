<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	checkedShow();	
});

function checkedShow(){
	$("input[type=checkbox][value=1]").prop("checked","checked");
	$("input[type=checkbox]").attr("disabled",true);
	$(".check-block").css('color','#696969');
}

</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 通知服務設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Notification_Service" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.Notification_Service" /><!-- 通知服務 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" action="${__ctx}/PERSONAL/SERVING/notification_service" method="post">
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
						<!-- 主頁內容  -->
						<div class="ttb-input-block">
								<h4 id="titleMsg" style="text-align: center; color: red"><spring:message code="LB.Change_Settings_Successfully" /></h4><!-- 變更設定成功 -->
								<br/>
						<div class="ttb-input-item row"">
								<span class="input-title">
									<h4><spring:message code="LB.System_time" /></h4><!-- 系統時間 -->
								</span>
								<span class="input-block">
									<div class="ttb-input">
										${notifyResult.data.CMQTIME}
									</div>
								</span>
							</div>
						<div class="ttb-input-item row" id="optionArea">
								<span class="input-title">
									<h4><spring:message code="LB.Option_item" /></h4><!-- 選項 -->
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="check-block">
<!-- 										轉帳/繳費稅/匯出匯款成功通知 -->
										<spring:message code="LB.X1387" />
										<input type="checkbox"  class="authcheck1" id="cho1" name="DPNOTIFY1" value="${notifyResult.data.DPNOTIFY1}"  />
										<span class="ttb-check2"></span>
										</label>
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										代扣繳各項費用不足扣通知 -->
										<spring:message code="LB.X1388" />
										<input type="checkbox" class="authcheck1" id="cho2" name="DPNOTIFY2" value="${notifyResult.data.DPNOTIFY2}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										定存即將到期通知 -->
										<spring:message code="LB.X1389" />
										<input type="checkbox" class="authcheck1" id="cho3" name="DPNOTIFY3" value="${notifyResult.data.DPNOTIFY3}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										預約交易（轉帳/繳費稅/匯出匯款）即將扣帳通知 -->
										<spring:message code="LB.X1390" />
										<input type="checkbox" class="authcheck1" id="cho4" name="DPNOTIFY4" value="${notifyResult.data.DPNOTIFY4}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										預約交易（轉帳/繳費稅/匯出匯款）結果通知 -->
										<spring:message code="LB.X1391" />
										<input type="checkbox" class="authcheck1" id="cho5" name="DPNOTIFY5" value="${notifyResult.data.DPNOTIFY5}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										預約交易（轉帳/繳費稅/匯出匯款）到期通知 -->
										<spring:message code="LB.X1392" />
										<input type="checkbox" class="authcheck1" id="cho20" name="DPNOTIFY20" value="${notifyResult.data.DPNOTIFY20}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										國外匯入匯款通知 -->
										<spring:message code="LB.X1393" />
										<input type="checkbox" class="authcheck2" id="cho12" name="DPNOTIFY12" value="${notifyResult.data.DPNOTIFY12}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										個人戶借款寬限期滿通知 -->
										<spring:message code="LB.X1394" />
										<input type="checkbox" class="authcheck1" id="cho10" name="DPNOTIFY10" value="${notifyResult.data.DPNOTIFY10}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
										<label class="check-block">
<!-- 										企業戶借款本金即將到期通知 -->
										<spring:message code="LB.X1395" />
										<input type="checkbox" class="authcheck1" id="cho13" name="DPNOTIFY13" value="${notifyResult.data.DPNOTIFY13}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										基金申購贖回轉換交易成功通知 -->
										<spring:message code="LB.X1396" />
										<input type="checkbox"class="authcheck1" id="cho6" name="DPNOTIFY6" value="${notifyResult.data.DPNOTIFY6}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										基金預約交易結果通知 -->
										<spring:message code="LB.X1397" />
										<input type="checkbox" class="authcheck1" id="cho7" name="DPNOTIFY7" value="${notifyResult.data.DPNOTIFY7}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										基金定期定額約定變更交易成功通知 -->
										<spring:message code="LB.X1398" />
										<input type="checkbox" class="authcheck1" id="cho8" name="DPNOTIFY8" value="${notifyResult.data.DPNOTIFY8}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										基金停利/停損點通知 -->
										<spring:message code="LB.X1399" />
										<input type="checkbox" class="authcheck1" id="cho14" name="DPNOTIFY14" value="${notifyResult.data.DPNOTIFY14}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										基金自動贖回通知 -->
										<spring:message code="LB.X1400" />
										<input type="checkbox" class="authcheck1" id="cho21"  name="DPNOTIFY21" value="${notifyResult.data.DPNOTIFY21}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										借款繳款不足扣通知 -->
										<spring:message code="LB.X1401" />
										<input type="checkbox" class="authcheck1" id="cho9"  name="DPNOTIFY9" value="${notifyResult.data.DPNOTIFY9}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										信用卡繳款通知 -->
										<spring:message code="LB.X1402" />
										<input type="checkbox" id="cho11" name="DPNOTIFY11" value="${notifyResult.data.DPNOTIFY11}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										網路銀行相關服務及優惠訊息 -->
										<spring:message code="LB.X1403" />
										<input type="checkbox"  id="cho15" name="DPNOTIFY15" value="${notifyResult.data.DPNOTIFY15}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										黃金買進/回售/定期投資變更成功通知 -->
										<spring:message code="LB.X1404" />
										<input type="checkbox" class="authcheck1" id="cho16" name="DPNOTIFY16" value="${notifyResult.data.DPNOTIFY16}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										黃金存摺預約交易結果通知 -->
										<spring:message code="LB.X1405" />
										<input type="checkbox" class="authcheck1" id="cho17"  name="DPNOTIFY17" value="${notifyResult.data.DPNOTIFY17}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										黃金存摺定期定額即將扣款通知 -->
										<spring:message code="LB.X1406" />
										<input type="checkbox" class="authcheck1" id="cho18"  name="DPNOTIFY18" value="${notifyResult.data.DPNOTIFY18}"/>
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										<label class="check-block">
<!-- 										黃金存摺定期定額扣款結果通知 -->
										<spring:message code="LB.X1407" />
										<input type="checkbox" class="authcheck1" id="cho19"  name="DPNOTIFY19" value="${notifyResult.data.DPNOTIFY19}" />
										<span class="ttb-check2"></span>
										</label> 
										
										<span class="input-unit"></span>
										<br>
										
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<h4><spring:message code="LB.Mail_address" /><br/>(<spring:message code="LB.My_Email" />)</h4>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										${notifyResult.data.DPMYEMAIL}
									</div>
								</span>
							</div>
						</div>
						
					</div>
				</div>
			</form>
		</section>
		</main>
	</div>

</body>
</html>