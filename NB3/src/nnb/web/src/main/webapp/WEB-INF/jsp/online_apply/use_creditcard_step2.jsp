<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		
		
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			$("#hideblock_Cmail").hide();
			//Email 欄位若有輸入則須檢核格式
			if($("#MAIL").val() != ''){
				$("#checkmail").val($("#MAIL").val());
				$("#hideblock_Cmail").show();
			}
			//信用卡繳款通知
			if($("#NOTIFY_CCARDPAY").prop("checked")){
				$("#NOTIFY_CCARDPAY").val("Y");
				$("#checkmail").val($("#MAIL").val());
				$("#hideblock_Cmail").show();
			}
			//本行活動通知
			if($("#NOTIFY_ACTIVE").prop("checked")){
				$("#NOTIFY_ACTIVE").val("Y");
				$("#checkmail").val($("#MAIL").val());
				$("#hideblock_Cmail").show();
			}
			//申請信用卡電子帳單
// 			if($("#NOTIFY_CCARDBILL").prop("checked")){
// 				$("#NOTIFY_CCARDBILL").val("Y");
// 				$("#checkmail").val($("#MAIL").val());
// 				$("#hideblock_Cmail").show();
// 			}
			console.log("submit~~");
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
        		e.preventDefault();
        	}
			else{
				if(!processQuery()){
					return false
				}
				$("#formId").validationEngine('detach');
				// 遮罩
         		initBlockUI();
	 	  		$("#formId").submit();
			}
		});
	}
	
	function processQuery(){

	  	var main = document.getElementById("formId");

		var LOGINPIN = $('#LOGINPIN').val();
		var TRANSPIN = $('#TRANSPIN').val();
	  	main.HLOGINPIN.value = pin_encrypt(LOGINPIN);
    	main.HTRANSPIN.value = pin_encrypt(TRANSPIN);
//	alert("HLOGINPIN = " + main.HLOGINPIN.value);
//	alert("HTRANSPIN = " + main.HTRANSPIN.value);
		if(main.NOTIFY_CCARDBILL.value=="Y"){
			$("#formId").attr("action","${__ctx}/ONLINE/APPLY/use_creditcard_step3");
			return true;
		} else {
			$("#formId").attr("action","${__ctx}/ONLINE/APPLY/use_creditcard_result");
			return true;
		}
	  	return false;
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Register" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<!-- 信用卡申請網路銀行 -->
				<h2><spring:message code= "LB.X1086" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            
                         <!--信用卡號碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code= "LB.D1033" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        ${use_creditcard_step2.data.CARDNUM}
                                    </div>
                                </span>
                            </div>
                            
                         <!--電子郵箱-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0346" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="text" class="text-input" maxLength="60" size="60" id="MAIL" name="MAIL" value="">
                                    </div>
                                <span id="hideblock_Cmail"> 
									<!-- 驗證用的input --> 
									<input id="checkmail" name="checkmail" type="text"
										class="text-input validate[required,funcCall[validate_EmailCheck[checkmail]]]"
										style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
								</span>
                                </span>
                            </div>
							    
                         <!--Email通知服務-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0996" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <label class="check-block"  for="NOTIFY_CCARDPAY"> <spring:message code= "LB.D1036" />
											<input type="checkbox" name="NOTIFY_CCARDPAY" id="NOTIFY_CCARDPAY" value="">
											<span class="ttb-check"></span>
										</label>
									</div>
										 <div class="ttb-input">
										<label class="check-block" for="NOTIFY_ACTIVE"> <spring:message code= "LB.D0997" />
											<input id="NOTIFY_ACTIVE" type="checkbox" name="NOTIFY_ACTIVE" value="">
											<span class="ttb-check"></span>
										</label>
									</div>
<!-- 									 <div class="ttb-input"> -->
<%-- 										<label  class="check-block" for="NOTIFY_CCARDBILL"> <spring:message code= "LB.D1038" /> --%>
<!-- 											<input type="checkbox" id="NOTIFY_CCARDBILL" name="NOTIFY_CCARDBILL" value=""> -->
<!-- 											<span class="ttb-check"></span> -->
<!-- 										</label> -->
<!--                                     </div> -->
                                </span>
                            </div>
							
							<!--設定使用者名稱-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1002" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="text" maxLength="16" size="17" class="text-input validate[required,funcCall[validate_CheckUserName['<spring:message code= "LB.D1002" />',USERNAME]]]" id="USERNAME" name="USERNAME" value=""><br>
										<!-- 6-16位英數字，英文至少2位且區分大小寫 -->
										<span class="tbb-unit"><font color="#FF0000"> <spring:message code= "LB.D1003" /></font></span>
                                    </div>
                                </span>
                            </div>
							
							<!--確認使用者名稱-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code= "LB.D1004" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="text" maxLength="16" size="17" class="text-input validate[required,funcCall[validate_CheckUserName['<spring:message code= "LB.D1004" />',USERNAME1]],funcCall[validate_DoubleCheck[<spring:message code= "LB.Loginpage_User_name" />,USERNAME,USERNAME1]]]" id="USERNAME1" name="USERNAME1" value=""><br>
										<span class="tbb-unit"><spring:message code= "LB.D1005" /></span>
                                    </div>
                                </span>
                            </div>
							
								<!--設定簽入密碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code= "LB.D1006" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="password" class="text-input validate[required,funcCall[validate_CheckPwd[<spring:message code= "LB.login_password" />,LOGINPIN]]]" maxLength="8" size="9" id="LOGINPIN" name="LOGINPIN" value=""><br>
									<!-- 6-8位數字 -->
										<span class="tbb-unit"><font color="#FF0000"><spring:message code= "LB.D1007" /></font></span>
                                    </div>
                                </span>
                            </div>
							
								<!--確認簽入密碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1008" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="password" class="text-input validate[required,funcCall[validate_CheckPwd[<spring:message code= "LB.login_password" />,LOGINPIN1]],funcCall[validate_DoubleCheck[<spring:message code= "LB.login_password" />,LOGINPIN,LOGINPIN1]]]" maxLength="8" size="9" id="LOGINPIN1" name="LOGINPIN1" value=""><br>
										<!-- 請再次輸入簽入密碼 -->
										<span class="tbb-unit"> <spring:message code= "LB.D1009" /></span>
                                    </div>
                                </span>
                            </div>
							
							
								<!--設定交易密碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1010" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="password" class="text-input validate[required,funcCall[validate_CheckPwd[<spring:message code= "LB.SSL_password_1" />,TRANSPIN]]]" maxLength="8" size="9" id="TRANSPIN" name="TRANSPIN" value=""><br>
										<!-- 6-8位數字 -->
										<span class="tbb-unit"><font color="#FF0000"><spring:message code= "LB.D1007" /></font></span>
                                    </div>
                                </span>
                            </div>
							
							
								<!--確認交易密碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code= "LB.D1011" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="password" class="text-input validate[required,funcCall[validate_CheckPwd[<spring:message code= "LB.SSL_password_1" />,TRANSPIN1]],funcCall[validate_DoubleCheck[<spring:message code= "LB.SSL_password_1" />,TRANSPIN,TRANSPIN1]]]" maxLength="8" size="9" id="TRANSPIN1" name="TRANSPIN1" value=""><br>
									<!-- 請再次輸入交易密碼 -->
										<span class="tbb-unit"> <spring:message code= "LB.D1012" /></span>
                                    </div>
                                </span>
                            </div>
							
                        </div>
                        
                        <input class="ttb-button btn-flat-gray" id="CMRESET" name="CMRESET" type="reset" value="<spring:message code="LB.Re_enter" />" />
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
                        <input type="hidden" id="ADOPID" name="ADOPID" value="NA40">
  						<input type="hidden" id="_CUSIDN" name="_CUSIDN" value="${use_creditcard_step2.data._CUSIDN}">
  						<input type="hidden" id="CARDNUM" name="CARDNUM" value="${use_creditcard_step2.data.CARDNUM}">
					  	<input type="hidden" id="EXPDTA" name="EXPDTA" value="${use_creditcard_step2.data.EXPDTA}">
					  	<input type="hidden" id="CHECKNO" name="CHECKNO" value="${use_creditcard_step2.data.CHECKNO}">
					  	<input type="hidden" id="BIRTHDAY" name="BIRTHDAY" value="${use_creditcard_step2.data.BIRTHDAY}">
					  	<input type="hidden" id="MOBILE" name="MOBILE" value="${use_creditcard_step2.data.MOBILE}">
					  	<input type="hidden" id="PHONE_H" name="PHONE_H" value="${use_creditcard_step2.data.PHONE_H}">
					  	<input type="hidden" id="USERIP" name="USERIP" value="${use_creditcard_step2.data.USERIP}">
					  	<input type="hidden" id="CCBIRTHDATEYY" name="CCBIRTHDATEYY" value="${use_creditcard_step2.data.CCBIRTHDATEYY}">
					  	<input type="hidden" id="CCBIRTHDATEMM" name="CCBIRTHDATEMM" value="${use_creditcard_step2.data.CCBIRTHDATEMM}">
					  	<input type="hidden" id="CCBIRTHDATEDD" name="CCBIRTHDATEDD" value="${use_creditcard_step2.data.CCBIRTHDATEDD}">
					  	<input type="hidden" id="HLOGINPIN" name="HLOGINPIN" value="">
					  	<input type="hidden" id="HTRANSPIN" name="HTRANSPIN" value="">
					  	<input type="hidden" id="NOTIFY_CCARDBILL" name="NOTIFY_CCARDBILL" value="">
                    </div>
                </div>
				</form>
		
                <ol class="list-decimal description-list">
                		<p><spring:message code="LB.Description_of_page" /></p>
                <!-- 使用者名稱、密碼， 請勿設定連號、重號，如：123456、111111、AAAAAA。 -->
                   	<li><spring:message code= "LB.Use_Creditcard_P4_D1" /></li>
                   	<!-- 使用者名稱、密碼， 請勿設定與身分證字號/統一編號、生日、電話號碼、帳號等相同。 -->
					<li><spring:message code= "LB.Use_Creditcard_P4_D2" /></li>
					<!-- 使用者名稱不可與簽入密碼、交易密碼相同。 -->
					<li><spring:message code= "LB.Use_Creditcard_P4_D3" /></li>
					<!-- 使用者名稱為6-16位英數字且區分大小寫；簽入、交易密碼為6-8位數字，每次輸入時請特別注意。 -->
					<li><spring:message code= "LB.Use_Creditcard_P4_D4" /></li>
                </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>