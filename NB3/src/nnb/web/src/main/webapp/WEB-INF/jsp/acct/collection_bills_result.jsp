<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
</head>
<body>
        <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Account_Inquiry" /></li>
    <!-- 託收票據明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Bill_For_Collection_BC" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.Bill_For_Collection_BC" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 						下載為舊txt檔 -->
						<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
					</select>
				</div>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<form id="formId" action="" method="post">
								<!-- 下載用 -->
	                		<input type="hidden" id="back" name="back" value="">
								<input type="hidden" name="downloadFileName" value="<spring:message code="LB.Bill_For_Collection_BC" />"/>
								<input type="hidden" name="CMQTIME" value="${collection_bills_result.data.CMQTIME}"/>
								<input type="hidden" name="ACN" value="${collection_bills_result.data.ACN}"/>
								<input type="hidden" name="REMCNT" value="${collection_bills_result.data.REMCNT}"/>
								<input type="hidden" name="REMAMT" value="${collection_bills_result.data.REMAMT}"/>
								<input type="hidden" name="VALCNT" value="${collection_bills_result.data.VALCNT}"/>
								<input type="hidden" name="VALAMT" value="${collection_bills_result.data.VALAMT}"/>
								<input type="hidden" name="TYPE" value="${collection_bills_result.data.TYPE}"/>
								<input type="hidden" name="TYPE_STR" value="${collection_bills_result.data.TYPE_STR}"/>
								<input type="hidden" name="CMPERIOD" value="${collection_bills_result.data.CMPERIOD}"/>
								<input type="hidden" name="ACN_TEXT" value="${collection_bills_result.data.ACN_TEXT}"/>
								<input type="hidden" name="CMSDATE2" value="${collection_bills_result.data.CMSDATE2}"/>
								<input type="hidden" name="CMEDATE2" value="${collection_bills_result.data.CMEDATE2}"/>
								<input type="hidden" name="CMSDATE3" value="${collection_bills_result.data.CMSDATE3}"/>
								<input type="hidden" name="CMEDATE3" value="${collection_bills_result.data.CMEDATE3}"/>
								<input type="hidden" name="CMSDATE4" value="${collection_bills_result.data.CMSDATE4}"/>
								<input type="hidden" name="CMEDATE4" value="${collection_bills_result.data.CMEDATE4}"/>
								<input type="hidden" name="FGBRHCOD" value="${collection_bills_result.data.FGBRHCOD}"/>
								<input type="hidden" name="DPBHNO" value="${collection_bills_result.data.DPBHNO}"/>
								<input type="hidden" name="FGTYPE" value="${collection_bills_result.data.FGTYPE}"/>
								<input type="hidden" name="FGPERIOD" value="${collection_bills_result.data.FGPERIOD}"/>
								<input type="hidden" name="downloadType" id="downloadType"/>
								<input type="hidden" name="templatePath" id="templatePath"/>
								<input type="hidden" name="hasMultiRowData" value="true"/> 	
								<!-- EXCEL下載用 -->
								<input type="hidden" name="headerRightEnd" value="1" />
			                    <input type="hidden" name="headerBottomEnd" value="9" />
			                    <input type="hidden" name="multiRowStartIndex" value="13" />
			                    <input type="hidden" name="multiRowEndIndex" value="13" />
			                    <input type="hidden" name="multiRowCopyStartIndex" value="10" />
			                    <input type="hidden" name="multiRowCopyEndIndex" value="14" />
			                    <input type="hidden" name="multiRowDataListMapKey" value="TABLE" />
			                    <input type="hidden" name="rowRightEnd" value="10" />
								<!-- TXT下載用 -->
								<input type="hidden" name="txtHeaderBottomEnd" value="10"/>
								<input type="hidden" name="txtMultiRowStartIndex" value="14"/>
								<input type="hidden" name="txtMultiRowEndIndex" value="14"/>
								<input type="hidden" name="txtMultiRowCopyStartIndex" value="11"/>
								<input type="hidden" name="txtMultiRowCopyEndIndex" value="16"/>
								<input type="hidden" name="txtMultiRowDataListMapKey" value="TABLE"/>
								<!-- 繼續查詢 -->
		                        <c:if test="${ collection_bills_result.data.QUERYNEXT != Null }">
			                        <!--<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>-->
			                        	<spring:message code="LB.X0076" />
			                        <input type="button" name="CMCONTINU" id="CMCONTINU" value="<spring:message code="LB.X0151" />" class="ttb-button btn-flat-orange">
		                        </c:if>
								<ul class="ttb-result-list">
									<li>
										<!-- 查詢時間 -->
										<h3><spring:message code="LB.Inquiry_time" /></h3>
                                		<p>${collection_bills_result.data.CMQTIME }</p>
									</li>
									<li>
										<!-- 帳號 -->
										<h3><spring:message code="LB.Account"/></h3>
                                		<p>${ collection_bills_result.data.ACN_TEXT }</p>
									</li>
									<li>
										<!-- 託收總張數 -->
										<h3><spring:message code="LB.Total_number_of_Collection" /></h3>
                                		<p>${collection_bills_result.data.REMCNT}</p>
									</li>
									<li>
										<!-- 託收總金額 -->
										<h3><spring:message code="LB.X0077" /></h3>
                                		<p>${collection_bills_result.data.REMAMT}</p>
									</li>
									<li>
										<!-- 已入帳總張數 -->
										<h3><spring:message code="LB.Total_number_of_already_accounted" /></h3>
                                		<p>${collection_bills_result.data.VALCNT}</p>
									</li>	
									<li>
										<!-- 已入帳總金額 -->
										<h3><spring:message code="LB.Total_amount_of_already_accounted" /></h3>
                                		<p>${collection_bills_result.data.VALAMT}</p>
									</li>
									<li>
										<!-- 查詢項目 -->
										<h3><spring:message code="LB.Query_item" /></h3>
                                		<p>${collection_bills_result.data.TYPE_STR}</p>
									</li>
									<li>
										<!-- 查詢期間 -->
										<h3><spring:message code="LB.Inquiry_period_1" /></h3>
                                		<p>${collection_bills_result.data.CMPERIOD}</p>
									</li>	
								</ul>
								<!-- 全部  -->
								<c:forEach var="dataTable" items="${collection_bills_result.data.REC}">
									<ul class="ttb-result-list">
                            			<li>
                                			<h3><spring:message code="LB.Account" /></h3>
                                			<p>${dataTable.ACN}</p>
                           				</li>
                        			</ul>
									<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
										<thead>
											<tr>
												<th><spring:message code="LB.Collection_bank" /></th>
												<th data-breakpoints="sm xs"><spring:message code="LB.Collection_date" /></th>
												<th><spring:message code="LB.Checking_account" /></th>
												<th><spring:message code="LB.Issuers_bank_code" /></th>
												<th><spring:message code="LB.Issuers_account_no" /></th>
												<th data-breakpoints="sm xs"><spring:message code="LB.Expired_date" /></th>
												<th data-breakpoints="sm xs"><spring:message code="LB.Banknote_amount" /></th>
												<th data-breakpoints="sm xs"><spring:message code="LB.Accounted_date" /></th>
												<th data-breakpoints="sm xs"><spring:message code="LB.Unlogged_reason" /></th>	
												<th><spring:message code="LB.Supplementary_content" /></th>
											</tr>
										</thead>
										<tbody>
											<c:choose>
												<c:when test="${dataTable.TABLE.size() > 0}">
													<c:forEach var="dataList" items="${dataTable.TABLE}">
														<tr>
											                <td class="text-center">${dataList.COLLBRH }</td>
											                <td class="text-center">${dataList.SHOWREMDATE }</td>
											                <td class="text-center">${dataList.CHKNUM }</td>
											                <td class="text-center">${dataList.BNKNUM }</td>
											                <td class="text-center">${dataList.PAYACN }</td>
											                <td class="text-center">${dataList.SHOWDUEDATE }</td>
											                <td class="text-right">${dataList.AMOUNT }</td>
											                <td class="text-center">${dataList.SHOWVALDATE }</td>
											                <td class="text-center">${dataList.FAILRSN }</td>
											                <td class="text-center">${dataList.DATA }</td>					               
														</tr>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<tr>
														<td class="text-center">${dataTable.msgCode}</td>
														<td class="text-center">${dataTable.msgName}</td>
													</tr>
												</c:otherwise>
											</c:choose>
										
										</tbody>
									</table>
								</c:forEach>
							</form>
							<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />"/>
							<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
						</div>
					</div>
	
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
	

	<%@ include file="../index/footer.jsp"%>
      
    	<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);
				setTimeout("initDataTable()",100);
				
			});
			function init(){
				//initFootable();
				//上一頁按鈕
				$("#previous").click(function() {
// 					initBlockUI();
// 					fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/collection_bills','', '');
					var action = '${__ctx}/NT/ACCT/collection_bills';
					$('#back').val("Y");
					$("#formId").attr("action", action);
					initBlockUI();
					$("#formId").submit();
				});

				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"collection_bills_print",
						"jspTitle":"<spring:message code= "LB.Bill_For_Collection_BC" />",
						"CMQTIME":"${collection_bills_result.data.CMQTIME}",
						"ACN":"${collection_bills_result.data.ACN_TEXT}",
						"REMCNT":"${collection_bills_result.data.REMCNT}",
						"REMAMT":"${collection_bills_result.data.REMAMT}",
						"VALCNT":"${collection_bills_result.data.VALCNT}",
						"VALAMT":"${collection_bills_result.data.VALAMT}",
						"TYPE":"${collection_bills_result.data.TYPE_STR}",
						"CMPERIOD":"${collection_bills_result.data.CMPERIOD}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});		
				//繼續查詢
	        	$("#CMCONTINU").click(function (e) {
					e = e || window.event;
					console.log("submit~~");

// 					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").attr("action", "${__ctx}/NT/ACCT/collection_bills_result");
					$("#formId").submit();
				});
				
			}
			
			//選項
			 function formReset() {
// 			 	initBlockUI();
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/collection_bills.xls");
			 	}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/collection_bills.txt");
			 	}else if ($('#actionBar').val()=="oldtxt"){
					$("#downloadType").val("OLDTXT");
					$("#templatePath").val("/downloadTemplate/collection_billsOLD");
			 	}
// 				ajaxDownload("${__ctx}/ajaxDownload","formId","finishAjaxDownload()");

				$("form").attr("action", "${__ctx}/NT/ACCT/collection_bills_ajaxDirectDownload");
				$("#formId").attr("target", "");
		        $("#formId").submit();
		        $('#actionBar').val("");
			}
			
			function finishAjaxDownload(){
				$("#actionBar").val("");
				unBlockUI(initBlockId);
			}
		</script>
    
</body>
</html>