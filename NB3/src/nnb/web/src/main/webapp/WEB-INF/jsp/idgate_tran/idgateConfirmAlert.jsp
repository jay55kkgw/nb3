<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
  var openIdgate="${openIdgate}";	
  var idgateadopid="${idgateAdopid}";		 
  var idgatetitle="${idgateTitle}";
  //兩個全域變數
  //     var idgatesubmit= $("#formId");
  //     var idgateadopid="n070";
  //     var idgatetitle="臺灣中小企銀臺幣轉帳";
  var idgatecountdownObjheader, idgatecountdownSecHeader = 120;

  var idgateValidateFailFlag=false;
  jQuery(document).ready(function () {
  	if('Y'== openIdgate ){
  		 $("#idgate-block").bind('keydown', function (event) {
  		      if (event.keyCode == 13) { // 如果按 enter     
  		        $("#idgateCancel").click();
  		      }
  		    });
  		    //TODO 判斷是不是idgate使用者
  		    var idgateUserFlag = "${idgateUserFlag}";
  		    if (idgateUserFlag =="Y") {
//  		 	if(true){
  		        $("div[name='idgate_group']").show();
  		    }else{
  		        $("div[name='idgate_group']").hide();
  		    }

  		    //註冊btn
  		    $("#idgateCancel").click(function (e) {    
            $("#idgateCancel").html("取消");	
  		    	if(idgateValidateFailFlag){
  		    	      //關閉倒數
  		    	      closeIdgateBlock();
  		    	}else{
  		    		$("#idgate-confirm-block").show();
  		    	}
  		    })
  		    $("#idgateConfirmYes").click(function (e) {    	
  			      //TODO 發送取消ajax
  				  cancelIdgateValidate(idgateadopid);
  				  $("#idgate-confirm-block").hide();
  			      //關閉倒數
  			      closeIdgateBlock();
  			      //離開轉帳頁?
  		    });
  		    $("#idgateConfirmNo").click(function (e) {
  		    	$("#idgate-confirm-block").hide();
  		    });
  	}
  });

  /*LayOut Part*/
  //TODO 手機layout

  // 顯示showIdgateBlock
  function showIdgateBlock() {
    //發送啟動驗證ajax
    iniIdgateValidate(idgateadopid,idgatetitle);

    // 初始化倒數時間
    $("#idgate_title").html("裝置驗證中，請於兩分鐘內確認訊息是否正確<br>(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)");
    $("#idgatecountdownheader").html(idgatecountdownSecHeader);
    $("#idgatecountdownMin").html("02");
    //     $("#mobile-countdownMin").html("07");
    $("#idgatecountdownSec").html("00");
    //     $("#mobile-countdownSec").html("00");
    // 倒數
    idgatestartIntervalHeader(1, idgaterefreshCountdownHeader, []);
    $("#windowMask").show();
    $("#idgate-block").show();
  }
  function confirmcancel(){
	  
  }
  // 取消showIdgateBlock
  function closeIdgateBlock() {
    //倒數暫停
    clearInterval(idgatecountdownObjheader);
    $("#idgate-block").hide();
    $('#windowMask').hide();
    unBlockUI(initBlockId);
  }

  /*
   * Functional Part
   */

  // 啟動驗證ajax
  function iniIdgateValidate(adopiD,titlE) {
    uri = '${__ctx}' + "/IDGATE/CHECK/phone_validate_ajax"
    console.log("getSelectData>>" + uri);
    rdata = {
    	adopid: adopiD,
    	title: titlE
    };
    console.log("rdata>>" + rdata);
    data = fstop.getServerDataEx(uri, rdata, false); //非同步
    console.log("data>>", data);
    if (data != null && data.result == true) {

    } else {
    	closeIdgateBlock();
    }
  }
  // 取蕭ajax
  function cancelIdgateValidate(adopid) {
    uri = '${__ctx}' + "/IDGATE/CHECK/cancle_phone_ajax"
    console.log("getSelectData>>" + uri);
    rdata = {
      ADOPID: adopid
    };
    console.log("rdata>>" + rdata);
    data = fstop.getServerDataEx(uri, rdata, false); //    
    if (data != null && data.result == true) {
      //驗證成功或失敗
		console.log("cancel Success");
      	console.log(data);
    } else {
		console.log("cancel failed");
      	console.log(data);
    }
  }
  // 取得驗證結果ajax
  function pollIdgateValidateResult(adopid) {
    uri = '${__ctx}' + "/IDGATE/CHECK/phone_validate_result_ajax"
    console.log("getSelectData>>" + uri);
    rdata = {
      ADOPID: adopid
    };
    console.log("rdata>>" + rdata);
    data = fstop.getServerDataEx(uri, rdata, true, getidgateResult); //非同步((打後不理))     
    if (data != null && data.result == true) {
      //驗證成功或失敗

    } else {

    }
  }

  function getidgateResult(data) {
    console.log("data>>", data);
    if (data.data.status == "00") { //取得驗證成功
      $("#idgate_title").html("裝置認證成功");
      //倒數暫停
      clearInterval(idgatecountdownObjheader);
      console.log(">>>submit");
      //轉址
      $("#formId").submit();
    } else if (data.data.status == "03"){
      $("#idgateCancel").html("確認");
    	$("#idgate_title").html("裝置認證已取消本次交易");
    	idgateValidateFailFlag=true;
        //倒數暫停
        clearInterval(idgatecountdownObjheader);
    }
  }

  function idgaterefreshCountdownHeader() {
    // timeout剩餘時間
    var nextSec = parseInt($("#idgatecountdownheader").html()) - 1;
    $("#idgatecountdownheader").html(nextSec);

    // timeout
    if (nextSec == 0) {
      //closeIdgateBlock();
      $("#idgate_title").html("裝置認證失敗，如未收到認證推播訊息<br>請確認您的裝置是否為預設綁定裝置");
      $("#idgatecountdownMin").html('00');
      $("#idgatecountdownSec").html('00');

    } else if (nextSec >= 0) {
      // 倒數時間以分秒顯示
      var minutes = Math.floor(nextSec / 60);
      var seconds = nextSec - minutes * 60;
      $("#idgatecountdownMin").html(('0' + minutes).slice(-2));
      //           $("#mobile-countdownMin").html(('0' + minutes).slice(-2));
      $("#idgatecountdownSec").html(('0' + seconds).slice(-2));
      //           $("#mobile-countdownSec").html(('0' + seconds).slice(-2));
    } else if (nextSec < 0) {
      //認證成功 or失敗
      //時間暫停
      clearInterval(idgatecountdownObjheader);
    }

    //pollingAJAX
    if ((nextSec > 90) && (nextSec % 3 == 0)) pollIdgateValidateResult(idgateadopid);
    else if (90 > nextSec && nextSec > 60 && (nextSec % 2 == 0)) pollIdgateValidateResult(idgateadopid);
    else if (nextSec < 60) pollIdgateValidateResult(idgateadopid);
  }

  function idgatestartIntervalHeader(interval, func, values) {
    clearInterval(idgatecountdownObjheader);
    idgatecountdownObjheader = idgatesetRepeater(func, values, interval);
  }

  function idgatetimeLogout() {
    // 初始化登出時間
    $("#idgatecountdownheader").html(idgatecountdownSecHeader);
    $("#idgatecountdownMin").html("02");
    //       $("#mobile-countdownMin").html("02");
    $("#idgatecountdownSec").html("00");
    //       $("#mobile-countdownSec").html("00");
    // 倒數
    idgatestartIntervalHeader(1, refreshCountdownHeader, []);
  }

  function idgatesetRepeater(func, values, interval) {
    return setInterval(function () {
      func.apply(this, values);
    }, interval * 1000);
  }
</script>


<!-- IDGATE pop訊息 -->
<section id="idgate-block" class="error-block" style="display: none">
  <div style="display:none;">
    <font color="red" id="idgatecountdownheader"></font>
  </div>
  <div class="error-for-code">
    <!-- 裝置驗證中，請於兩分鐘內確認訊息是否正確-->
    <p class="error-title" id="idgate_title">裝置驗證中，請於兩分鐘內確認訊息是否正確</p>
    <!--         請輸入您的晶片金融卡密碼以繼續。 -->
    <p class="error-content"></p>
    <p class="error-info">
      <!--       <span id="idgate_content" display="none">請確認您的手機裝置是否為預設的綁定裝置</span> -->
      <span class="idgate-time">
        <img class="error-icon" src="${__ctx}/img/clock-circular-outline.svg" />
        <font color="#eda15f">
          <spring:message code="LB.Remaining" />
        </font>
        <!-- 分 -->
        <font color="#eda15f" id="idgatecountdownMin"></font>
        :
        <!-- 秒 -->
        <font color="#eda15f" id="idgatecountdownSec"></font>

        <%--         <img src="${__ctx}/img/reset-timeout-${pageContext.response.locale}.svg" onclick="idgatetimeLogout()"> --%>
      </span>
    </p>

    <button id="idgateCancel" class="btn-flat-gray ttb-pup-btn">取消</button>
    <button id="idgateForward" class="btn-flat-Orange ttb-pup-btn" style="display: none">認證成功繼續交易</button>
  </div>
</section>
<!-- IDGATE pop訊息 -->
<section id="idgate-confirm-block" class="error-block" style="display: none">
  <div class="error-for-code">
    <!-- 裝置驗證中，請於兩分鐘內確認訊息是否正確-->
    <p class="error-title" id="idgate-confirm_title">請再次確認是否要取消此筆交易</p>
    <p class="error-content"></p>
    <p class="error-info">

    </p>
    <button id="idgateConfirmNo" class="btn-flat-gray ttb-pup-btn">取消</button>
    <button id="idgateConfirmYes" class="btn-flat-Orange ttb-pup-btn">確認</button>
  </div>
</section>
<form id="idgate-block-form" action="" method="post">

</form>
