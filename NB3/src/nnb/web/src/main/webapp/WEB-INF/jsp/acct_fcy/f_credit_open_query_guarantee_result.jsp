<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            //initFootable(); // 將.table變更為footable
            setTimeout("initDataTable()",100);
            init();
        });

        function init() {
        	//繼續查詢
        	$("#CMCONTINU").click(function (e) {
				e = e || window.event;
				console.log("submit~~");

				$("#formId").validationEngine('detach');
				initBlockUI(); //遮罩
				$("#formId").attr("action", "${__ctx}/FCY/ACCT/f_credit_open_query_guarantee_result");
				$("#formId").submit();
			});
            //列印
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"f_letter_of_credit_opening_inquiry_guarantee_result_print",
					"jspTitle":'<spring:message code= "LB.X0006" />',
					"CMQTIME":"${base_result.data.CMQTIME}",
					"LCNO":"${base_result.data.LCNO}",
					"CMPERIOD":"${base_result.data.CMPERIOD}",
					"CMRECNUM":"${base_result.data.CMRECNUM}",
					"SATM":"${base_result.data.SATM}",
					"SBAL":"${base_result.data.SBAL}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/FCY/ACCT/f_credit_open_query_guarantee';
    			$('#back').val("Y");
    			$("#formId").attr("action", action);
    			initBlockUI();
    			$("#formId").submit();
    		});
        }
        
    	//選項
	 	function formReset() {
// 	 		initBlockUI();
    		if ($('#actionBar').val()=="excel"){
				$("#USERDATA_X50").val("");
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/f_letter_of_credit_opening_inquiry_guarantee_result.xls");
	 		}else if ($('#actionBar').val()=="txt"){
				$("#USERDATA_X50").val("");
				$("#downloadType").val("TXT");
			    $("#templatePath").val("/downloadTemplate/f_letter_of_credit_opening_inquiry_guarantee_result.txt");
	 		}
    		$("form").attr("action", "${__ctx}/FCY/ACCT/f_credit_open_query_guarantee_ajaxDirectDownload");
    		$("#formId").attr("target", "");
            $("#formId").submit();
            $('#actionBar').val("");
//     		ajaxDownload("${__ctx}/FCY/ACCT/f_credit_open_query_guarantee_ajaxDirectDownload","formId","finishAjaxDownload()");
		}
	 	function finishAjaxDownload(){
			$("#actionBar").val("");
			unBlockUI(initBlockId);
		}
    </script>
	<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
	<title>Insert title here</title>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 開狀查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0098" /></li>
    <!-- 帳戶明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_Demand_Deposit_Detail" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 擔保信用狀/保證函明細(未輸入) -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code= "LB.X0006" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>	
                <form id="formId" method="post">
                	<input type="hidden" id="back" name="back" value="">
					<input type="hidden" id="LCNO" name="LCNO" />
					<input type="hidden" id="RORIGAMT" name="RORIGAMT" />
					<input type="hidden" id="RBENNAME" name="RBENNAME" />
                    <input type="hidden" name="USERDATA_X50" value="${base_result.data.USERDATA_X50}" />
                    <input type="hidden" name="CMSDATE" value="${base_result.data.CMSDATE}" />
                    <input type="hidden" name="CMEDATE" value="${base_result.data.CMEDATE}" />
                    <!-- 下載用 -->
                    <input type="hidden" name="downloadFileName" value="<spring:message code= "LB.X0006" />" />
                    <input type="hidden" name="CMQTIME" value="${base_result.data.CMQTIME}" />
                    <input type="hidden" name="CMPERIOD" value="${base_result.data.CMPERIOD}" />
                    <input type="hidden" name="CMRECNUM" value="${base_result.data.CMRECNUM}" />
                    <input type="hidden" name="LCNO" value="${base_result.data.LCNO}" />
                    <input type="hidden" name="SDATM" value="${base_result.data.SDATM}" />
                    <input type="hidden" name="SDBAL" value="${base_result.data.SDBAL}" />
                    <input type="hidden" name="downloadType" id="downloadType" />
                    <input type="hidden" name="templatePath" id="templatePath" />
                    <input type="hidden" name="hasMultiRowData" value="false"/>
                    <!-- EXCEL下載用 -->
                    <input type="hidden" name="headerRightEnd" value="9" />
                    <input type="hidden" name="headerBottomEnd" value="7" />
                    <input type="hidden" name="rowStartIndex" value="8" />
                    <input type="hidden" name="multiRowDataListMapKey" value="rowListMap" />
                    <input type="hidden" name="rowRightEnd" value="9" />
                    <input type="hidden" name="footerStartIndex" value="10" />
                    <input type="hidden" name="footerEndIndex" value="13" />
                    <input type="hidden" name="footerRightEnd" value="2" />
                    <!-- TXT下載用 -->
                    <input type="hidden" name="txtHeaderBottomEnd" value="11"/>
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="true"/>
					<input type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap"/>
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 tab-content">
                            <ul class="ttb-result-list">
                                <li><!-- 查詢時間 -->
	                            	<h3><spring:message code="LB.Inquiry_time" /></h3>
	                            	<p>${base_result.data.CMQTIME}</p>
	                            </li>
	                            <li><!-- 查詢期間 -->
	                            	<h3><spring:message code="LB.Inquiry_period" /></h3>
	                                <p>${base_result.data.CMPERIOD }</p>
	                            </li>
	                            <li><!-- 擔保信用狀/保證函號碼 : -->
	                            	<h3><spring:message code="LB.W0104" /></h3>
	                            	<p>${base_result.data.LCNO}</p>
	                            </li>
	                            <li><!-- 資料總數 : -->
	                            	<h3><spring:message code="LB.Total_records" /></h3>
	                            	<p>${base_result.data.CMRECNUM} <spring:message code="LB.Rows" /><!--筆 --></p>
	                            </li>
                            </ul>
                            <!-- 表格區塊 -->
                        
                            <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                                <thead>
                                    <tr>
                                    	<!-- 擔保信用狀/保證函號碼 -->
                                        <th>
                                            <!--<spring:message code="LB.Change_date" />-->
											<spring:message code="LB.W0104" />
                                        </th>
                                        <!-- 簽發日 -->
                                        <th>
                                            <!--<spring:message code="LB.Change_date" />-->
											<spring:message code="LB.W0113" />
                                        </th>
                                        <!-- 幣別 -->
                                        <th data-breakpoints="xs sm">
                                            <spring:message code="LB.Currency" />
                                        </th>
                                        <!-- 擔保信用狀/保證函金額 -->
                                        <th data-breakpoints="xs sm">
                                            <!-- <spring:message code="LB.Withdrawal_amount" /> -->
											<spring:message code="LB.W0115" />
                                        </th>
                                        <!-- 擔保信用狀/保證函餘額 -->
                                        <th data-breakpoints="xs sm">
                                            <!-- <spring:message code="LB.Withdrawal_amount" /> -->
											<spring:message code="LB.W0116" />
                                        </th>
                                        <!--有效期限 -->
                                        <th data-breakpoints="xs sm">
                                            <!-- <spring:message code="LB.Deposit_amount" /> -->
											<spring:message code="LB.W0120" />
                                        </th>
                                        <!-- 受益人名稱/地址 -->
                                        <th data-breakpoints="xs sm">
                                            <!-- <spring:message code="LB.Account_balance_2" /> -->
											<spring:message code="LB.W0117" />
                                        </th>
                                        <!-- 通知銀行 -->
                                        <th data-breakpoints="xs sm">
                                            <!--<spring:message code="LB.Data_content" />-->
											<spring:message code="LB.W0118" />
                                        </th>
                                        <!-- 備註 -->
                                        <th>
                                            <spring:message code="LB.Note" />
                                        </th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="dataList" items="${ base_result.data.REC }">
                                        <tr>
                                            <!--擔保信用狀/保證函號碼-->
                                            <td class="text-center">${dataList.RLGNO }</td>
                                            <!--簽發日-->
                                            <td class="text-center">${dataList.ROPENDAT }</td>
                                       		<!-- 幣別 -->
                                            <td class="text-center">${dataList.RLGCCY }</td>
                                        	<!--擔保信用狀/保證函金額 -->
                                            <td class="text-right">${dataList.RLGAMT }</td>
                                        	<!--擔保信用狀/保證函餘額 -->
                                            <td class="text-right">${dataList.RLGOS }</td>
	                                        <!--有效期限 -->
                                            <td class="text-center">${dataList.REXPDATE }</td>
                                            <!-- 受益人名稱/地址 -->
                                            <td class="text-center">
                                            	${dataList.RBENNAME}<br>
                                            	${dataList.RBENADD1}<br>${dataList.RBENADD2}<br>${dataList.RBENADD3}
                                            </td>
                                        	<!-- 通知銀行 -->
                                            <td class="text-center">${dataList.RADVNM }</td>
                                        	<!-- 備註 -->
                                            <td class="text-center">${dataList.RMARK }</td>
                                        	
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            
                       		<c:set var="total_i18n">
                       			<spring:message code='LB.W0123' />
                       		</c:set>
                       		<c:set var="row_i18n">
                       			<spring:message code='LB.Rows' />
                       		</c:set>
	                        <ul class="ttb-result-list">
	                            <li>
	                                <!-- 匯入金額總金額 -->
	                                <h3>
	                                    <!-- <spring:message code="LB.Inquiry_time" /> : -->
	                                   	 <spring:message code="LB.W0121" />
	                                </h3>
                                   	<p>
                                   		<c:set var="SATM_replace" value="${ fn:replace( base_result.data.SATM, 'i18n{LB.W0123}', total_i18n) }" />
                                   		<c:set var="SATM_replace2" value="${ fn:replace( SATM_replace, 'i18n{LB.Rows}', row_i18n) }" />
                                   		${SATM_replace2}
                               		</p>
	                            </li>
	                            <li>
	                                <!-- 匯入金額總金額 -->
	                                <h3>
	                                    <!-- <spring:message code="LB.Inquiry_time" /> : -->
	                                   	 <spring:message code="LB.W0122" />
	                                </h3>
                                   	<p>
                                   		<c:set var="SBAL_replace" value="${ fn:replace( base_result.data.SBAL, 'i18n{LB.W0123}', total_i18n) }" />
                                   		<c:set var="SBAL_replace2" value="${ fn:replace( SBAL_replace, 'i18n{LB.Rows}', row_i18n) }" />
                              			${SBAL_replace2}
                               		</p>
	                            </li>
	                        </ul>
	                        <!--button 區域 -->
	                        
	                            <!--回上頁 -->
	                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
	                            <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
	                            <!-- 繼續查詢 -->
	                            <c:if test="${ base_result.data.QUERYNEXT != null && !base_result.data.QUERYNEXT.equals('') }">
		                            <!--<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>-->
		                            <input type="button" name="CMCONTINU" id="CMCONTINU" value="<spring:message code= "LB.X0151" />" class="ttb-button btn-flat-orange">
	                            </c:if>
	                            <!-- 列印  -->
	                            <spring:message code="LB.Print" var="printbtn"></spring:message>
	                            <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
	                        
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                    <c:if test="${ base_result.data.QUERYNEXT != null && !base_result.data.QUERYNEXT.equals('') }">
	                    <!-- 說明： -->
					    <ol class="description-list list-decimal">
					    	<p><spring:message code="LB.Description_of_page"/></p>
					        <li><span><spring:message code="LB.F_Credit_Open_Query_P2_D1"/></span></li>
					    </ol>
                    </c:if>
                </form>

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>