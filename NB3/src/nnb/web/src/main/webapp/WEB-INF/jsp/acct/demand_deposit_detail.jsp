<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			init();
			//日期
			datetimepickerEvent();
			//預約自動輸入今天
 			getTmr();
			//日期區間切換按鈕,如果點選指定日期則顯示起迄日
			fgperiodChageEvent();
		});

		function init() {
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
			
			//acn
			var getacn = '${demand_deposit_detail.data.Acn}';
			var getn110acn = '${demand_deposit_detail.data.n110_ACN}';
			if(getacn != null && getacn != ''){
				$("#ACN option[value= '"+ getacn +"' ]").prop("selected", true);
			}
			if(getn110acn != null && getn110acn != ''){
				$("#ACN option[value= '"+ getn110acn +"' ]").prop("selected", true);
			}
			
			//btn 
			$("#CMRESET").click(function () {
				$("#formId")[0].reset();
				$("#hideblock_FGPERIOD").hide();
				getTmr();
			});
			//btn 
			$("#CMSUBMIT").click(function (e) {
				e = e || window.event;
				//打開驗證隱藏欄位
				console.log("submit~~");
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					var odate = $('#TODAY').val(); 
					var CheckDateScopeReturn = validate_CheckDateScopeAlert('<spring:message code= "LB.Inquiry_period_1"/>', odate, 'CMSDATE', 'CMEDATE', false, 24, 6);		
						console.log('validate_CheckDateScopeReturn ' + CheckDateScopeReturn);
					if(true == CheckDateScopeReturn){
						initBlockUI(); //遮罩
						$("#formId").removeAttr("target");
						$("#formId").attr("action", "${__ctx}/NT/ACCT/demand_deposit_detail_result");
						$("#formId").submit();
					}else{
						alert(CheckDateScopeReturn);
					}
				}
			});
		} // init END
		//選項
	 	function formReset(){
	 		//打開驗證隱藏欄位
	 		if(!$("#formId").validationEngine("validate")){
				e = e || window.event;//forIE
				e.preventDefault();
			}
	 		else{
		 		//initBlockUI();
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/acct_demand_deposit_detail.xls");
		 		}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
				    $("#templatePath").val("/downloadTemplate/acct_demand_deposit_detail.txt");
		 		}else if ($('#actionBar').val()=="oldtxt"){
					$("#downloadType").val("OLDTXT");
					$("#templatePath").val("/downloadTemplate/acct_demand_deposit_detailOLD.txt");
		 		}else if($('#actionBar').val()=="pdf"){
		 			$("#downloadType").val("direct");
		 			$("#templatePath").val("");
		 			
		 			$("#formId").attr("target", "");
					$("#formId").attr("action", "${__ctx}/NT/ACCT/downloadDemandDepositDetailPDF");
					$("#formId").submit();
					return;
		 		}
				//ajaxDownload("${__ctx}/NT/ACCT/demand_deposit_detail_ajaxDirectDownload","formId","finishAjaxDownload()");
				$("#formId").attr("target", "");
				$("#formId").attr("action", "${__ctx}/NT/ACCT/demand_deposit_detail_directDownload");
				$("#formId").submit();
				$('#actionBar').val("");
	 		}
		}
		function finishAjaxDownload(){
			$("#actionBar").val("");
			unBlockUI(initBlockId);
		}
		// 日曆欄位參數設定
		function datetimepickerEvent() {
			$(".CMDATE").click(function (event) {
				$('#CMDATE').datetimepicker('show');
			});
			$(".CMSDATE").click(function (event) {
				$('#CMSDATE').datetimepicker('show');
			});
			$(".CMEDATE").click(function (event) {
				$('#CMEDATE').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
		//日期區間切換按鈕,如果點選指定日期則顯示起迄日
		function fgperiodChageEvent() {
			$('input[type=radio][name=FGPERIOD]').change(function () {
				if (this.value == 'CMPERIOD') {
					$("#hideblock_FGPERIOD").show();
				} else {
					$("#hideblock_FGPERIOD").hide();
					//預約自動輸入今天
					getTmr();
				}
			});
		}
		//預約自動輸入今天
		function getTmr() {
			var tmr = $('#TODAY').val(); 
			$('#CMSDATE').val(tmr);
			$('#CMEDATE').val(tmr);
			$('#odate').val(tmr);
		}
		

	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 帳戶查詢 -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Account_Inquiry" /></li>
    <!-- 帳戶明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_Demand_Deposit_Detail" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--臺幣活期性存款明細 -->
					<spring:message code="LB.NTD_Demand_Deposit_Detail" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
							<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 						下載為舊txt檔 -->
						<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
						<!-- 下載PDF -->
						<option value="pdf"><spring:message code="LB.X2037" /> PDF</option>
					</select>
				</div>	
				
				<form id="formId" method="post">
					<input type="hidden" id="TODAY" value="${demand_deposit_detail.data.TODAY}">
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="LB.NTD_Demand_Deposit_Detail" />
					<input type="hidden" name="downloadType" id="downloadType" />
					<input type="hidden" name="templatePath" id="templatePath" />
					<input type="hidden" name="hasMultiRowData" value="true"/>
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="1" />
                    <input type="hidden" name="headerBottomEnd" value="6" />
                    <input type="hidden" name="multiRowStartIndex" value="10" />
                    <input type="hidden" name="multiRowEndIndex" value="10" />
                    <input type="hidden" name="multiRowCopyStartIndex" value="8" />
                    <input type="hidden" name="multiRowCopyEndIndex" value="12" />
                    <input type="hidden" name="multiRowDataListMapKey" value="rowListMap" />  
                    <input type="hidden" name="rowRightEnd" value="8" />
<!--                     <input type="hidden" name="footerStartIndex" value="8" /> -->
<!--                     <input type="hidden" name="footerEndIndex" value="8" /> -->
<!--                     <input type="hidden" name="footerRightEnd" value="8" /> -->
					<!-- TXT下載用 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="6"/>
					<input type="hidden" name="txtMultiRowStartIndex" value="10"/>
					<input type="hidden" name="txtMultiRowEndIndex" value="10"/>
					<input type="hidden" name="txtMultiRowCopyStartIndex" value="7"/>
					<input type="hidden" name="txtMultiRowCopyEndIndex" value="11"/>
					<input type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 帳號 -->
											<spring:message code="LB.Account" />
										</label>
									</span>
									<span class="input-block">
										<select name="ACN" id="ACN" class="custom-select select-input half-input validate[required]">
											<c:forEach var="dataList" items="${demand_deposit_detail.data.REC}">
												<option value="${dataList.ACN}"> ${dataList.ACN}</option>
											</c:forEach>
											<option value="ALL">
												<spring:message code="LB.All" />
											</option>
										</select>
									</span>
								</div>
								<!-- 查詢區間區塊 -->
								<div class="ttb-input-item row">
									<!--查詢區間  -->
									<span class="input-title">
										<label>
										<spring:message code="LB.Inquiry_period" />
										</label>
									</span>
									<span class="input-block">
										<!--  當日交易 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Todays_transaction" />
												<input type="radio" name="FGPERIOD" id="CMTODAY" value="CMTODAY" checked />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  當月交易 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.This_months_transaction" />
												<input type="radio" name="FGPERIOD" id="CMCURMON" value="CMCURMON" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 上月交易 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Last_months_trading" />
												<input type="radio" name="FGPERIOD" id="CMLASTMON" value="CMLASTMON" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 最近一個月 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Recently_a_month" />
												<input type="radio" name="FGPERIOD" id="CMNEARMON" value="CMNEARMON" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  最近一星期 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="FGPERIOD" id="CMLASTWEEK" value="CMLASTWEEK" />
												<spring:message code="LB.Recently_a_week" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  指定日期區塊 -->
										<div class="ttb-input">
											<label class="radio-block">
												<!--  指定日期 -->
												<input type="radio" name="FGPERIOD" id="CMPERIOD" value="CMPERIOD" />
												<spring:message code="LB.Specified_period" />
												<span class="ttb-radio"></span>
											</label>
											<!-- 起迄日區塊預設隱藏 -->
											<div id = "hideblock_FGPERIOD" style="display: none;">
												<!--期間起日 -->
												<div class="ttb-input">
													<span class="input-subtitle subtitle-color">
														<spring:message code="LB.Period_start_date" />
													</span>
													<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" value="${demand_deposit_detail.data.TODAY}" />
													<span class="input-unit CMSDATE">
														<img src="${__ctx}/img/icon-7.svg" />
													</span>
													<!-- 驗證用的span預設隱藏 -->
													<span id="hideblock_CMSDATE" >
													<!-- 驗證用的input -->
													<input id="odate1" name="odate" type="text" value="${demand_deposit_detail.data.TODAY}" class="text-input datetimepicker validate[required,verification_date[CMSDATE]]" 
														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
													</span>
												</div>
												<!--期間迄日 -->
												<div class="ttb-input">
													<span class="input-subtitle subtitle-color">
														<spring:message code="LB.Period_end_date" />
													</span>
													<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" value="${demand_deposit_detail.data.TODAY}" />
													<span class="input-unit CMEDATE">
														<img src="${__ctx}/img/icon-7.svg" />
													</span>
													<!-- 驗證用的span預設隱藏 -->
													<span id="hideblock_CMEDATE" >
													<!-- 驗證用的input -->
													<input id="odate2" name="odate" type="text" value="${demand_deposit_detail.data.TODAY}" class="text-input datetimepicker validate[required,verification_date[CMEDATE]]" 
														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
													</span>
												</div>
											</div>
											<!-- 起迄日區塊 -END-->
										</div>
									</span>
								</div>
							</div>
							<!-- 網頁顯示 button-->
								<!-- 重新輸入 -->
								<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
								<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
								<!--網頁顯示 -->
								<spring:message code="LB.Display_as_web_page" var="cmSubmit"></spring:message>
								<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
							<!-- 網頁顯示 button-->
						</div>
					</div>
					<div class="text-left">
						<ol class="description-list list-decimal">
						<!-- 		說明： -->
						<p>
							<spring:message code="LB.Description_of_page"/>
						</p>
							<li><spring:message code="LB.Demand_deposit_detail_P1_D1"/></li>
							<li>
								<spring:message code= "LB.Demand_deposit_detail_P1_D2-1" />
								<a target="_blank" href='${pageContext.request.contextPath}/public/N130_TXT.htm'>
									<spring:message code= "LB.Demand_deposit_detail_P1_D2-2" />
								</a> 
								<spring:message code= "LB.Demand_deposit_detail_P1_D2-3" />
							</li>
							<li>
								<spring:message code= "LB.Demand_deposit_detail_P1_D3-1" />
								<a target="_blank" href='${pageContext.request.contextPath}/public/N130_OLDTXT.htm'>
									<spring:message code= "LB.Demand_deposit_detail_P1_D3-2" />
								</a>
							</li>
						</ol>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>