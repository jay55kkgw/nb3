<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    
<script type="text/javascript">
	$(document).ready(function(){
		init();
		//initFootable();
		setTimeout("initDataTable()",100);
	});
	function init() {
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});

		
		$('#CMSUBMIT').click(function(e){
			console.log("submit~~");
			var radioflag = $('input[type=checkbox][name="ArrayParam_SHOW"]').is(':checked') ;
			console.log('radioflag :' + radioflag);
			if(!radioflag){
				//alert("<spring:message code= "LB.Alert051" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert051' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				return false;
			}else{
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					var dataArray = new Array();
					$('input:checkbox:checked[name="ArrayParam_SHOW"]').each(function(i){ 
						dataArray[i] = this.value; 
					});
					$('#hid').val(dataArray);
					initBlockUI();
					$("#formId").validationEngine('detach');
					$("#formId").submit();
				}
			}
		});
	}
</script>
</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 信用卡掛失     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Card_Report_Loss" /></li>
		</ol>
	</nav>

	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	
	
	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
		
			<h2><spring:message code="LB.Card_Report_Loss" /></h2><!-- 信用卡掛失 -->
			
			
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/CREDIT/APPLY/card_loss_step1">
			<input type="hidden" id="PINNEW" name="PINNEW"  value="">
			<input type=hidden id="hid" name="ArrayParam" value="">
			<input type=hidden id="ADOPID" name="ADOPID" value="N816L_1">
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
					
						<div class="ttb-input-block">
							<!-- 請選擇欲掛失之信用卡 -->
							<c:choose>
								<c:when test="${card_loss.data.isHaveValidCard}">
									 <div class="ttb-message">
										<span style="color: red"><spring:message code="LB.Select_lost_credit_card" /></h3>
									 </div>
								</c:when>
								<c:otherwise>
								 	<div class="ttb-message">
										<span style="color: red"><spring:message code= "LB.X1637" /></span>
									</div>
								</c:otherwise>
							</c:choose>
								<table class="stripe table-striped ttb-table dtable" id="table1" data-toggle-column="first">
								<thead>
								<tr>
									<th data-title=''>&nbsp;</th>
									<th data-title='<spring:message code="LB.Status"/>'><spring:message code="LB.Status" /></th><!-- 狀態 -->
									<th data-title='<spring:message code="LB.Types_of_Credit_Card"/>'><spring:message code="LB.Types_of_Credit_Card" /></th><!-- 信用卡卡別 -->
									<th data-title='<spring:message code="LB.Credit_card_number"/>' data-breakpoints="xs"><spring:message code="LB.Credit_card_number" /></th><!-- 卡號 -->
									<th data-title='<spring:message code="LB.Name"/>'><spring:message code="LB.Name" /></th><!-- 姓名 -->
								</tr>
								</thead>
								<tbody>
								<c:forEach var="dataTable" items="${card_loss.data.REC}" >
								<c:if test="${dataTable.STATUS !=2 }">
						            <tr>
						                <td class="text-center">
											<!-- checkbox  -->
											<label class="check-block" style="margin-bottom:12px">&nbsp;
						                	<input type="checkbox"  name="ArrayParam_SHOW" value='{"CARDNUM":"${dataTable.CARDNUM}"."TYPENAME":"${dataTable.TYPENAME}"."EPC_FLAG":"${dataTable.EPC_FLAG }"."EXP_DATE":"${dataTable.EXP_DATE}"}' <c:if test="${dataTable.STATUS=='1'}"> disabled </c:if> />
											<span class="ttb-check" ></span>
										</label>
						                </td>
						                <td class="text-center" nowrap>
						                	<!-- 狀態  -->
						                	${dataTable.STATUS_STR}
						                </td>
						                <td class="text-center">
						                	<!-- 卡片種類  -->
						               		${dataTable.TYPENAME}
						                </td>
						                <td class="text-center">
						                	<!-- 卡號  -->
						               		${dataTable.CARDNUM_COVER}
						                </td>
						                <td class="text-center" nowrap>
						                	<!-- 姓名  -->
						               		${dataTable.CR_NAME_COVER}<c:if test="${dataTable.PT_FLG == 0}">(<spring:message code= "LB.Attached_card" />)</c:if>
						                </td>
						            </tr>
								
								</c:if>
								</c:forEach>
								</tbody>
							</table>
						<c:if test="${card_loss.data.isHaveValidCard}">
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Transaction_security_mechanism" /></h4></label><!-- 交易機制 -->
							</span> <span class="input-block">
								<!-- 交易密碼 --> 
								<div class="ttb-input">
									<label class="radio-block">
									<spring:message code="LB.SSL_password" />
									<input type="radio" name="FGTXWAY" id="FGTXWAY" value="0" onClick="" checked />
									<span class="ttb-radio"></span>
									</label>
								</div>
								<div class="ttb-input">
								<label>
									<input id="CMPASSWORD" name="CMPASSWORD" type="password" maxlength="8" class="text-input validate[required,custom[onlyLetterNumber]]" />
								</label>
								</div>
							</span>
						</div>
						</c:if>
					
					</div>
					<c:if test="${card_loss.data.isHaveValidCard}">
						<input id="CMRESET" name="CMRESET" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" /> 
						<input id="CMSUBMIT" name="CMSUBMIT" type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" />
					</c:if>
					</div>
					
					</div>
					
					<ol class="description-list list-decimal">
								<p><spring:message code="LB.Remind_you" /><!-- 提醒您 --></p>
									<c:if test="${card_loss.data.isHaveValidCard}">
									<li style="color:red; font-size:20px;">
									<spring:message code="LB.Loss_P1_D1" /><!-- 掛失就無法取消，掛失手續費：每卡新臺幣200元。 -->
									</li>
									</c:if>
									<li><spring:message code="LB.Loss_P1_D2" />
									<!-- 本系統不提供COMBO卡掛失服務，請洽本行客服02-2357-7171、0800-01-7171，有專人為您服務。 -->
									</li>
							        <li><spring:message code="LB.Loss_P1_D3" />
							       <!--  本交易僅限本行發行各種有效信用卡持卡人使用（商務卡、採購卡、簽帳金融卡及悠遊聯名金融卡恕不適用此交易）。--></li>
							        <li>
							        <spring:message code="LB.Loss_P1_D4" />
							       <!--  信用卡即時交易查詢請洽本行客服02-2357-7171、0800-01-7171。 -->
							        </li>
							        <li>
							        <spring:message code="LB.Loss_P1_D5" />
							        <!-- 本行將於次一營業日後自動為您補發卡片，郵寄帳單地址。 -->
							        </li>
							        <li>
							        <spring:message code="LB.Loss_P1_D6" />
							        <!-- 悠遊聯名卡自掛失後3小時內，客戶需承擔悠遊卡扣款交易損失。 -->
							        </li>
							        <li>
							        <spring:message code="LB.Loss_P1_D7" />
							        <!-- 手機信用卡掛失後需另洽中華電信辦理USIM卡掛失。 -->
							        </li>
								</oL>
					</form>
		</section>
	</main><!-- main-content END --> 
	
</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
