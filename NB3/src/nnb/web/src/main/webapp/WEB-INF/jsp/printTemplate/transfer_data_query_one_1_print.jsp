<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/><br/><br/>
	
	<table class="print">
		<tr>
			<!-- 交易日期 -->
			<th><spring:message code="LB.Transaction_date" /></th>
			<th>${TRADEDATE_F }</th>
		</tr>
		<tr>
			<!-- 信託編號 -->
			<th><spring:message code="LB.W0904" /></th>
			<th>${CDNO_F }</th>
		</tr>
		<tr>
			<!-- 基金名稱 -->
			<th><spring:message code="LB.W0025" /></th>
			<th>${TRANSCODE_FUNDLNAME }</th>
		</tr>
		<tr>
			<!-- 信託金額 -->
			<th><spring:message code="LB.W0026" /></th>
			<th>${CRY_CRYNAME } ${FUNDAMT_F }</th>
		</tr>
		<tr>
			<!-- 基金單位數 -->
			<th><spring:message code="LB.W0956" /></th>
			<th>${UNIT_F }</th>
		</tr>
		<tr>
			<!-- 申購單位淨值 -->
			<th><spring:message code="LB.W0957" /></th>
			<th>${TRANSCRY_CRYNAME } ${PRICE1_F }</th>
		</tr>
		<tr>
			<!-- 匯率 -->
			<th><spring:message code="LB.Exchange_rate" /></th>
			<th>${EXRATE_F }</th>
		</tr>
		<tr>
			<!-- 手續費 -->
			<th><spring:message code="LB.D0507" /></th>
			<th>${FCA2_F }</th>
		</tr>
		<tr>
			<!-- 定期定額累積扣款成功次數 -->
			<th><spring:message code="LB.W0959" /></th>
			<th>${OKCOUNT_F }</th>
		</tr>
		<tr>
			<!-- 是否已分配 -->
			<th><spring:message code="LB.W0960" /></th>
			<th>${FUNDFLAG_F }</th>
		</tr>
	</table>
	<br/><br/><br/><br/>
</body>
</html>