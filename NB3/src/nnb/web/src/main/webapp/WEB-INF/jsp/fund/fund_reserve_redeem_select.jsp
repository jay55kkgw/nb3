<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//開始查詢資料並完成畫面
	setTimeout("init()",20);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	errorBlock(
			null, 
			null,
			["<spring:message code= 'LB.X2226' />"], 
			'<spring:message code= "LB.Confirm" />', 
			null
	);
	$("#CMSUBMIT").click(function(){
		
		if(${bs.data.BILLSENDMODE == "2"}){
			if(!CheckAmount("FUNDAMT","<spring:message code= 'LB.W0978' />",null,${bs.data.OFUNDAMT}-1)){
				return false;
			}
			
			var OFUNDAMT = parseInt("${bs.data.OFUNDAMT}");
			var FUNDAMT = parseInt($("#FUNDAMT").val());
			var AMT = fstop.unFormatAmtToInt("${bs.data.FORMAT_AMT}");
			var RAMT = fstop.unFormatAmtToInt("${bs.data.FORMAT_RAMT}");
			
			if (FUNDAMT < AMT) {
				errorBlock(null,null,["<spring:message code= 'LB.X2612_1' />"],
							"<spring:message code= 'LB.Quit' />",null);
				return false;
			}
			if ((OFUNDAMT - FUNDAMT) < RAMT) {
				errorBlock(null,null,["<spring:message code= 'LB.X2612_2' />"],
						"<spring:message code= 'LB.Quit' />",null);
				return false;
			}
		}
// 		if(!confirm("<spring:message code='LB.Confirm004' />"+"${bs.data.TRADEDATEFormat}"+"\n<spring:message code='LB.Confirm005' />"))
//     		return false;
		//遮罩避免使用者重複點選
		initBlockUI();
		$("#formID").attr("action","${__ctx}/FUND/RESERVE/REDEEM/fund_reserve_redeem_confirm");
		$("#formID").submit();
	});
	$("#cancelButton").click(function(){
		//$("#formID").attr("action","${__ctx}/FUND/REDEEM/fund_redeem_data");
		//$("#formID").submit();
		location.href = "${__ctx}/FUND/REDEEM/fund_redeem_data";
	});
	$("#resetButton").click(function(){
		$("#FUNDAMT").val("");
	});
});
function init(){
	initFootable();
}
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.Funds" /></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.W1064" /></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code= "LB.W1063" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code= "LB.W1063" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
			
					<form id="formID" method="post">
						<input type="hidden" name="ADOPID" value="C033">
						<input type="hidden" name="CUSNAME" value="${bs.data.CUSNAME}">
  						<input type="hidden" name="CDNO" value="${bs.data.CDNO}"/>
						<input type="hidden" name="TRANSCODE" value="${bs.data.TRANSCODE}"/>
						<input type="hidden" name="CRY" value="${bs.data.CRY}"/>
						<input type="hidden" name="OFUNDAMT" value="${bs.data.OFUNDAMT}"/>
						<input type="hidden" name="UNIT" value="${bs.data.UNIT}"/>
						<input type="hidden" name="BILLSENDMODE" value="${bs.data.BILLSENDMODE}"/>
						<input type="hidden" name="SHORTTRADE" value="${bs.data.SHORTTRADE}"/>
						<input type="hidden" name="SHORTTUNIT" value="${bs.data.SHORTTUNIT}"/>
						<input type="hidden" name="FUNDLNAME" value="${bs.data.FUNDLNAME}"/>
						<input type="hidden" name="FDINVTYPE" value="${bs.data.FDINVTYPE}"/>
						<input type="hidden" name="TRADEDATE" value="${bs.data.TRADEDATE}"/>
						<input type="hidden" name="FUNDACN" value="${bs.data.FUNDACN}"/>    
						<input type="hidden" name="ACN3" value="${bs.data.ACN3}"/>
						<input type="hidden" name="ACN4" value="${bs.data.ACN4}"/>
						<input type="hidden" name="FEE_TYPE" value="${bs.data.FEE_TYPE}"/>
						<input type="hidden" name="FUNDT" value="${bs.data.FUNDT}"/>
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 身分證字號/統一編號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.Id_no"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.hiddenCUSIDN}</span>
										</div>
									</span>
								</div>
								<!-- 姓名 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.Name"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.hiddenNAME}</span>
										</div>
									</span>
								</div>
								<!-- 信託號碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code= "LB.W1111" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.hiddenCDNO}</span>
										</div>
									</span>
								</div>
								<!-- 基金名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code= "LB.W0025" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>（${bs.data.TRANSCODE}）${bs.data.FUNDLNAME}</span>
										</div>
										<c:if test="${bs.data.FUNDT == '1'}">
											<br>
											<font color="red"><spring:message code="LB.X2610"/></font>
										</c:if>
									</span>
								</div>
								<!-- 信託金額 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code= "LB.W0026" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.ADCCYNAME}${bs.data.OFUNDAMTDotTwo}</span>
										</div>
									</span>
								</div>
								<!-- 預定生效日期 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code= "LB.W1143" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span class='high-light'>${bs.data.TRADEDATEFormat}</span>
										</div>
									</span>
								</div>
								<!-- 單位數 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code= "LB.W0027" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.UNIT}</span>
										</div>
									</span>
								</div>
								<!-- 贖回方式 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code= "LB.W1140" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.BILLSENDMODEChinese}</span>
										</div>
									</span>
								</div>
								<!-- 贖回信託金額 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code= "LB.W0978" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span> 
											${bs.data.ADCCYNAME}&nbsp;&nbsp; 
											<c:if test="${bs.data.BILLSENDMODE == '1'}">
												${bs.data.FUNDAMT}
												<input type="hidden" name="FUNDAMT" value="${bs.data.FUNDAMT}" />
											</c:if> 
											<c:if test="${bs.data.BILLSENDMODE == '2'}">
												<input type="text" maxLength="10" size="10" id="FUNDAMT" name="FUNDAMT" class="validate[required,funcCall[validate_CheckAmount[<spring:message code= "LB.W0978" />,FUNDAMT]]]" />
												<br>
                                              	<font color="red"><spring:message code= "LB.X2611_1" />${bs.data.CRY} ${bs.data.FORMAT_AMT}，</font>
                                              	<font color="red"><spring:message code= "LB.X2611_2" />${bs.data.CRY} ${bs.data.FORMAT_RAMT}</font>
											</c:if>
										</span>
										</div>
									</span>
								</div>
								<!-- 入帳帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code= "LB.W0135" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.FUNDACN}</span>
										</div>
									</span>
								</div>
							</div>
						
<!-- 							<table class="table" data-toggle-column="first"> -->
<!-- 								<tr> -->
<%-- 									<td><spring:message code="LB.Id_no"/></td> --%>
<%-- 								 	<td colspan="5" style="text-align:left">${bs.data.hiddenCUSIDN}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<%-- 									<td><spring:message code="LB.Name"/></td> --%>
<%-- 								 	<td colspan="5" style="text-align:left">${bs.data.hiddenNAME}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>信託號碼</td> -->
<%-- 									<td colspan="5" style="text-align:left">${bs.data.hiddenCREDITNO}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>基金名稱</td> -->
<%-- 									<td colspan="5" style="text-align:left">（${bs.data.TRANSCODE}）${bs.data.FUNDLNAME}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>信託金額</td> -->
<%-- 									<td colspan="5" style="text-align:left">${bs.data.ADCCYNAME}&nbsp;&nbsp;${bs.data.OFUNDAMTDotTwo}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>生效日期</td> -->
<%-- 									<td colspan="5" style="text-align:left">${bs.data.TRADEDATEFormat}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>單位數</td> -->
<%-- 									<td colspan="5" style="text-align:left">${bs.data.UNIT}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>贖回方式</td> -->
<%-- 									<td colspan="5" style="text-align:left">${bs.data.BILLSENDMODEChinese}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr>  -->
<!-- 									<td>贖回信託金額</td> -->
<!-- 									<td colspan="5" style="text-align:left"> -->
<%-- 										${bs.data.ADCCYNAME}&nbsp;&nbsp; --%>
<%-- 										<c:if test="${bs.data.BILLSENDMODE == '1'}"> --%>
<%-- 											${bs.data.FUNDAMT} --%>
<%-- 											<input type="hidden" name="FUNDAMT" value="${bs.data.FUNDAMT}"/> --%>
<%-- 										</c:if> --%>
<%-- 										<c:if test="${bs.data.BILLSENDMODE == '2'}"> --%>
<!-- 											<input type="text" maxLength="10" size="10" id="FUNDAMT" name="FUNDAMT" class="validate[required,funcCall[validate_CheckAmount[贖回信託金額,FUNDAMT]]]"/> -->
<%-- 										</c:if> --%>
<!-- 									</td> -->
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>入帳帳號</td> -->
<%-- 									<td colspan="5" style="text-align:left">${bs.data.FUNDACN}</td> --%>
<!-- 								</tr> -->
<!-- 							</table> -->
							<input type="button" id="cancelButton" value="<spring:message code="LB.Back_to_function_home_page"/>" class="ttb-button btn-flat-gray" />
						<c:if test="${bs.data.BILLSENDMODE == '2'}">
							<input type="button" id="resetButton"value="<spring:message code="LB.Re_enter"/>"class="ttb-button btn-flat-gray" />
						</c:if>
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange" />
						</div>					
					</div>				
				</form>				
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>