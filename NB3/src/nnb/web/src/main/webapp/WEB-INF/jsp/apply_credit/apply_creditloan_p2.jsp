<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		$("#PREVIOUS").click(function(e) {
			action = '${__ctx}/APPLY/CREDIT/apply_creditloan'
			$("form").attr("action", action);
			$("form").submit();
		});
		
	}); 
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--申請小額信貸 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0543" /></a></li>
		</ol>
	</nav>
		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
			<section id="main-content" class="container">
					<h2>
					<!--申請小額信貸 -->
					<spring:message code= "LB.D0543" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="${__ctx}/APPLY/CREDIT/apply_creditloan_p3">
					<input type="hidden" id="LOANTYPE" name ="LOANTYPE" value="${sessionScope.loantype}">
					<div class="main-content-block row">
						<div class="col-12">
							<!-- 貸款方案簡介及注意事項 -->
							<div class="NA01_2">
							<c:if test="${sessionScope.loantype eq 'B'}">
								<div class="head-line ttb-message">
									 <spring:message code= "LB.X0974" />	
								</div>
								<!-- 簡介 -->
								<div align="left">
									<h4><b><spring:message code= "LB.X0975" /></b></h4>
								</div>
								<!-- 貸款方案簡介 -->
								<ol>
								<!-- 1.年滿20歲本國國民且加計貸款期限後不得逾60歲，信用狀況正常，具穩定收入之本行存款往來戶。 -->
									<li><spring:message code= "LB.X0976" /></li>            
					            <!-- 2.利率最低3.58%起機動計息。 -->
					                <li><spring:message code= "LB.X0977" /></li>
					                <!-- 3.貸款金額最高新臺幣100萬元。 -->
					                <li><spring:message code= "LB.X0978" /></li>
					                <!-- 4.年資滿半年(含)以上(職務為證券營業員或期貨營業員者，現職須滿二年以上)。 -->
					                <li><spring:message code= "LB.X0979" /></li>
								</ol>
								<!-- 注意事項 -->
								<div align="left">
									<h4><b><spring:message code= "LB.D1101" /></b></h4>
								</div>
								<!-- 貸款方案注意事項 -->
								<ol>
								<!-- 1.提醒您申請書欄位務必詳實填寫，以免影響申請的權益。 -->
									<li><spring:message code= "LB.X0980" /></li>    
									<!-- 2.目前線上申貸僅開放卓越圓夢、e網輕鬆等二項小額信用貸款產品，其餘貸款請洽詢本行各 營業單位。-->        
					                <li><spring:message code= "LB.X0981" /><a href="https://www.tbb.com.tw/5_3_2.html?" target="_blank"><spring:message code= "LB.X0982" /></a>。</li>
					                <!-- 3.於申貸過程，須上傳身分證、財力證明(如提供存摺請連同存摺封面一同傳輸)、年資資料(如最近6個月薪轉存摺明細含封面或在職證明或勞保投保明細資料)等影像檔，總檔案大小不得超過10MB。 -->
					                <li><spring:message code= "LB.X0983" /></li>
					                <!-- 4.本貸款相關規範請至本行 官方網站 或洽本行客服專線0800-00-7171(營業時間8:50AM至5:30PM)。-->
					                <li><spring:message code= "LB.X0984" /><a href="https://www.tbb.com.tw" target="_blank"><spring:message code= "LB.X0985" /></a><spring:message code= "LB.X0986" /></li>
								</ol>
							</c:if>
							<c:if test="${sessionScope.loantype eq 'E'}">
								<div class="head-line">
								<!-- e網輕鬆貸款簡介及注意事項 -->
									<spring:message code= "LB.X0987" />
								</div>
								<!-- 簡介 -->
								<div align="left">
									<h4><b><spring:message code= "LB.X0975" /></b></h4>
								</div>
								<!-- 貸款方案簡介 -->
								<ol>
								<!-- 1.年滿20歲本國國民且加計貸款期限後不得逾65歲，信用狀況正常，具穩定收入之本行網路銀行客戶。 -->
									<li><spring:message code= "LB.X0989" /></li>
									<!-- 2.貸款利率最低4.5%起機動計息。 -->            
					                <li><spring:message code= "LB.X0990" /></li>
					                <!-- 3.貸款金額最高新臺幣10萬元。 -->
					                <li><spring:message code= "LB.X0991" /></li>
								</ol>
								<!-- 注意事項 -->
								<div align="left">
									<h4><b><spring:message code= "LB.D1101" /></b></h4>
								</div>
								<!-- 貸款方案注意事項 -->
								<ol>
								<!-- 1.提醒您申請書欄位務必詳實填寫，以免影響申請的權益。 -->
									<li><spring:message code= "LB.X0980" /></li>    
									<!-- 2.目前線上申貸僅開放卓越圓夢、e網輕鬆等二項小額信用貸款產品，其餘貸款請洽詢本行各 營業單位。-->        
					                <li><spring:message code= "LB.X0981" /><a href="https://www.tbb.com.tw/5_3_2.html?" target="_blank"><spring:message code= "LB.X0982" /></a>。</li>
					                <!-- 3.於申貸過程，須上傳身分證、財力證明(如提供存摺請連同存摺封面一同傳輸)、年資資料(如最近6個月薪轉存摺明細含封面或在職證明或勞保投保明細資料)等影像檔，總檔案大小不得超過10MB。 -->
					                <li><spring:message code= "LB.X0983" /></li>
					                <!-- 4.本貸款相關規範請至本行 官方網站 或洽本行客服專線0800-00-7171(營業時間8:50AM至5:30PM)。-->
					                <li><spring:message code= "LB.X0984" /><a href="https://www.tbb.com.tw" target="_blank"><spring:message code= "LB.X0985" /></a><spring:message code= "LB.X0986" /></li>
								</ol>
							</c:if>
							</div>
							<!-- 上一步-->
							<input class="ttb-button btn-flat-gray" type="button" value=" <spring:message code= "LB.X0318" />" id="PREVIOUS" >
							<!-- 下一步-->
		                   	<input class="ttb-button btn-flat-orange" type="submit" value="<spring:message code= "LB.X0080" />" id="CMSUBMIT" name="CMSUBMIT">
						</div>
					</div>
				</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>