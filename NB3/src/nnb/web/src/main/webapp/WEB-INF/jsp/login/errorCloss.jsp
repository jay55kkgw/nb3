<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<style>
		.ttb-error-massage {
			text-align: center;
			margin: 30px auto;
		}

		.ttb-error-massage p {
			margin-bottom: 30px;
			font-size: 1.25rem;
			font-weight: bold;
			letter-spacing: 0;
			color: #696A6C;
			opacity: 1;
		}

		.ttb-error-content {
			text-align: center;
			margin: 30px auto;
		}

		.ttb-error-content p {
			max-width: 570px;
			margin: 1rem auto;
			letter-spacing: 0;
			color: #000000;
			text-align: left;
		}

		.ttb-error-content button {
			margin-top: 60px;
		}

	</style>

<script type="text/javascript">
// 	var action = '${__ctx}';
	$(document).ready(function() {
		init();
	});
	
	function init() {
		
		$("#CMCLOSE").click(function() {
// 			window.close();
// 			console.log("previous action>>" + action);
// 			var action = '${__ctx}/login';
// 			submitForm(action);
		});
	}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
		<%@ include file="../index/header_logout.jsp"%>
		</header>
		<!-- 麵包屑 -->
		<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
			<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
				<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
				<li class="ttb-breadcrumb-item"><a href="#">暫停服務</a></li>
			</ol>
		</nav>

		<!-- content row END -->
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  -->
			<section id="main-content" class="container">
<!-- 				<h2>暫停服務</h2> -->
<!-- 				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i> -->
				<form id="formId" method="post" action="">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-error-massage">
								<p>我們將儘速恢復服務</p>
							</div>
							<div class="ttb-error-content">
								<p>很抱歉，目前網銀暫時停機維護中。</p>
								<p>我們正在排除狀況中，將會在最快時間內恢復服務，請稍後再試。</p>
<!-- 								<input type="button"  id="CMCLOSE" -->
<!-- 									class="ttb-button btn-flat-orange" value="關閉" /> -->
		                    </div>
						</div>
					</div>
				</form>
			</section>
		</main>
		<!-- 		main-content END --> 
	<!-- 	content row END -->
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>