<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		// 表單驗證初始化
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
         	// 表單驗證
			if ( !$('#formId').validationEngine('validate') ) {
				e.preventDefault();
			} else {
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				
				// 通過表單驗證準備送出
				processQuery();
			}
		});
	}
	
	// 通過表單驗證準備送出
	function processQuery(){
		// SSL交易密碼sha1
		$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
		// 清除SSL密碼欄，避免儲存或傳送明碼到後端
		$('#CMPASSWORD').val("");
		// 遮罩
		initBlockUI();
		$("#formId").submit();
	}

</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 行動銀行服務     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0318" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 快速選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0318"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/ONLINE/SERVING/enable_mobile_result">
				<input type="hidden" name="MB_Switch" value="${enable_mobile_confirm.MB_Switch}">
				<input type="hidden" name="OriStatus" value="${enable_mobile_confirm.OriStatus}">
				<input type="hidden" name="PINNEW" id="PINNEW" value="">
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">	
						<!-- 請輸入交易密碼 -->
							<div class="ttb-message">
								<p><spring:message code="LB.Please_enter_the_transaction_password"/></p>
							</div>						
							<!-- 交易密碼 -->
							<div class="ttb-input-item row">
                                <span class="input-title">
                                	<label>
                                        <h4><spring:message code= "LB.SSL_password_1" />：</h4>
                                    </label>
                              	</span>
                              	<span class="input-block">
									<div class="ttb-input">
                                       <span><input type="password" id="CMPASSWORD" name="CMPASSWORD" class="text-input validate[required]"
										size="8" maxlength="8" autocomplete="off" /></span>
                                    </div>
                                </span>
                            </div>
<!-- 							<br> -->
<%-- 							<p class="text-center"> <spring:message code="LB.SSL_password_1" />： --%>
<!-- 								<span class="ttb-input"> -->
<!-- 									<input type="password" id="CMPASSWORD" name="CMPASSWORD" class="text-input validate[required]" -->
<!-- 										size="8" maxlength="8" autocomplete="off" /> -->
<!-- 								</span> -->
<!-- 							</p> -->
						</div>
		                <!-- 重新輸入-->
		                <input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter"/>"/>
						<!-- 確定-->
		                <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>"/>
					</div>
 				</div>
			</form>
		</section>
		</main>
		<!-- main-content END -->
		<!-- content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>