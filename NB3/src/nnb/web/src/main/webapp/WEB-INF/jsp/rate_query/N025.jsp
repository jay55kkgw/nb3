<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
</HEAD>
<body>
<center>
<br>　</center>
<H3 ALIGN=CENTER></H>
<center>
<H3> 放 款 利 率 查 詢</H3>
	<TABLE BORDER="0">
	<TR><TD align="right" width="200">查  詢  時  間:</td><td align="center" width="200">${N025data.data.CMQTIME}</Td></TR>
	</table>
	<c:choose>
	<c:when test="${N025data.data.RECSIZE=='0'}">
	查無資料!!!
	</c:when>
	<c:when test="${N025data.data.RECSIZE!='0'}">
	<TABLE BORDER="2" cellspacing="0">
		<TR><TH width="200">科目.期別</TH><TH width="200">機動利率</TH></TR>
		<c:forEach var="dataList" items="${N025data.data.REC}" varStatus="data">
		<TR BGCOLOR="${dataList.COLOR}">
			<TD Align="left"><FONT>${dataList.ACC}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList.ITR1}${dataList.MARK}${dataList.ITR2}</FONT>&nbsp;</td>
		</TR>
		</c:forEach>
		</TABLE>
	</c:when>
	</c:choose>
	<BR>
	<table border=0 width=400>
	<tr>
	<td width=40>備註 :</td>
	<td width=360>&nbsp;</td>
	</tr>
	<tr>
	<td colspan=2><OL><LI><font color=red>實際放款利率係依個人信用條件、擔保品狀況、授信風險等情形訂定加碼幅度。</font></LI>
	<LI><a href='/vabweb/NT_BASIC_RATE.html' target=_blank>基準利率說明</a></LI>
	<LI><a href='/vabweb/NT_FXLN_RATE.html' target=_blank>定儲利率指數說明</a></LI>
	<LI>基本放款利率＝基準利率＋4.733％</LI></OL>
	</td></tr>
	</table></center>
	<P>
	<P>
	<P>
	<P>
	<form>
	<center><table border=0>
	<tr><td><input type='button' value='回上一頁' OnClick='history.back()'></td><td><input type='button' value='登出' OnClick='javascript:self.close()'></td></tr></table></center>
	</form>
	</BODY>
	</HTML>