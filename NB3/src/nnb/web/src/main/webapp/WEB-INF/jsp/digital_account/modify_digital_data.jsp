<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/component/util/checkIdentity.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            init();
         	// 初始化驗證碼
    		setTimeout("initKapImg()", 200);
    		// 生成驗證碼
    		setTimeout("newKapImg()", 300);
        });

        function init() {
	    	$("#CMSUBMIT").click(function(e){
	        	// $("#formId").validationEngine({binded:false,promptPosition: "inline" });
				$("#formId").validationEngine({
					validationEventTriggers:'keyup blur', 
					binded:true,
					scroll:false,
					promptPosition: "inline" });
				// 	    		var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
// 				var capData = $("#formId").serializeArray();
// 				var capResult = fstop.getServerDataEx(capUri, capData, false);
// 				if (capResult.result) {

					$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
					if (!$('#formId').validationEngine('validate')) {
						e.preventDefault();
					} else {
						$("#formId").validationEngine('detach');
//	 		        	initBlockUI();
			        	var action = '${__ctx}/DIGITAL/ACCOUNT/modify_digital_data_p1';
						$("form").attr("action", action);
		    			$("form").submit();
					}
// 				} else {
// 					//驗證碼有誤
// 					alert("<spring:message code= "LB.X1082" />");
// 					changeCode();
// 				}
			});
        }

    	// 刷新驗證碼
    	function refreshCapCode() {
    		console.log("refreshCapCode...");

    		// 驗證碼
    		$('input[name="capCode"]').val('');
    		
    		// 大小版驗證碼用同一個
    		$('img[name="kaptchaImage"]').hide().attr(
    				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

    		// 登入失敗解遮罩
    		unBlockUI(initBlockId);
    	}
    	
    	// 刷新輸入欄位
    	function changeCode() {
    		console.log("changeCode...");
    		
    		// 清空輸入欄位
    		$('input[name="capCode"]').val('');
    		
    		// 刷新驗證碼
    		refreshCapCode();
    	}

    	// 初始化驗證碼
    	function initKapImg() {
    		// 大小版驗證碼用同一個
    		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
    	}

    	// 生成驗證碼
    	function newKapImg() {
    		// 大小版驗證碼用同一個
    		$('img[name="kaptchaImage"]').click(function() {
    			refreshCapCode();
    		});
    	}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 修改數位存款帳戶資料     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1474" /></li>
		</ol>
	</nav>

	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
				<!-- step1：請輸入身分證字號 -->
					修改開戶資料
				</h2>
				<div id="step-bar">
                    <ul>
                        <li class="active">身分驗證</li>
                        <li class="">開戶資料</li>
                        <li class="">確認資料</li>
						<li class="">完成修改</li>
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="ADOPID" value="N203">
				  	<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
				  	<input type="hidden" name="HTRANSPIN" id="HTRANSPIN" value="">
					<input type="hidden" name="LMTTYN" id="LMTTYN" value="">	
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 tab-content">
                        
							<div class="ttb-input-block">
							<div class="ttb-message">
                                <p>身份驗證</p>
                            </div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
										<!-- 請輸入身分證字號 -->
											<spring:message code="LB.D0025"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" name="CUSIDN"  id="CUSIDN"  value="" class="text-input validate[required, funcCall[validate_checkSYS_IDNO[CUSIDN]]]" placeholder="請輸入身分證字號">
										</div>
									</span>
								</div>
							
	                        <!--button 區域 -->
								<!-- 下一步 -->
	                        </div>
	                        <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code= "LB.X0080" />" />
                        <!--                     button 區域 -->
                        </div>  
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>