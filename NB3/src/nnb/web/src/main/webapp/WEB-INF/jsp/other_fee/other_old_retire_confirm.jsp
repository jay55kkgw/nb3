<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
	<%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 舊制勞工退休提繳費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0752" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.W0752" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>			
			<form method="post" id="formId">
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${result_data.TOKEN}" /><!-- 防止重複交易 -->
                
                <!-- ikey -->
				<input type="hidden" id="jsondc" name="jsondc" value='${ result_data.jsondc }'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" id="CMTRANPAGE" name="CMTRANPAGE" value="1">
				<input type="hidden" id="NeedSHA1" name="NeedSHA1" value="">
				<input type="hidden" id="PINNEW" name="PINNEW" value="">
				<input type="hidden" id="ACN" name="ACN" value="">
				<input type="hidden" id="OUTACN" name="OUTACN" value="">
				<input type="hidden" id="ADSVBH" name="ADSVBH" value="">
				<input type="hidden" id="ADAGREEF" name="ADAGREEF" value="">
    			<input type="hidden" id="ADOPID" name="ADOPID" value="${ result_data.ADOPID }">
			    <input type="hidden" id="TYPE" name="TYPE" value="${ result_data.TYPE }">
			    <input type="hidden" id="ITMNUM" name="ITMNUM" value="${ result_data.ITMNUM }">
			    
				<input type="hidden" id="ICDTTM" name="ICDTTM" value="">
				<input type="hidden" id="ICMEMO" name="ICMEMO" value="">
				<input type="hidden" id="TAC_Length" name="TAC_Length" value="">
				<input type="hidden" id="TAC_120space" name="TAC_120space" value="">
				<input type="hidden" id="UNTTEL" name="UNTTEL" value="${ result_data.UNTTEL }">
				<input type="hidden" id="CUSIDNUN" name="CUSIDNUN" value="${ result_data.CUSIDNUN }">
    			<input type="hidden" name="CUSNUM" value="${ result_data.CUSNUM }">
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
                                <span><spring:message code="LB.D0104" /></span>
                            </div>
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0702" />（<spring:message code="LB.W1648" />）
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${ result_data.TSFACN }</span>
									</div>
								</span>
								<input type="hidden" id="TSFACN" name="TSFACN" value="${ result_data.TSFACN }">
								<input type="hidden" id="CARDNUM" name="CARDNUM" value="${ result_data.CARDNUM }">
							</div>
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0691" />（<spring:message code="LB.W0722" />）
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${ result_data.UNTNUM }</span>
									</div>
								</span>
								<input type="hidden" id="UNTNUM" name="UNTNUM" value="${ result_data.UNTNUM }">
							</div>
							
							
							<!--交易機制  SSL 密碼 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
<!-- 									<div class="ttb-input"> -->
<!-- 										<label> -->
<%-- 								       		<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked /><spring:message code="LB.SSL_password" /> --%>
<!-- 								       	</label> -->
<!-- 										<input type="password" name = "CMPASSWORD" id="CMPASSWORD"  maxlength="8" class="text-input validate[required, pwvd[PW, CMPASSWORD], custom[onlyLetterNumber]]"> -->
<!-- 										<input type="hidden" name = "PINNEW" id="PINNEW" value=""> -->
<!-- 									</div> -->
<!-- 									使用者是否可以使用IKEY -->
									<!-- 使用者是否可以使用IKEY -->
									<c:if test = "${sessionScope.isikeyuser}">
										<!-- 即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森)-->
<%-- 										<c:if test = "${transfer_data.data.TransferType != 'NPD'}"> --%>
								
											<!-- IKEY -->
											<div class="ttb-input" onclick="hideCapCode('Y')">
												<label class="radio-block">
													<spring:message code="LB.Electronic_signature" />
													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
													<span class="ttb-radio"></span>
												</label>
											</div>
											
<%-- 										</c:if> --%>
									</c:if>
									
									<!-- 晶片金融卡 -->
									<div class="ttb-input" onclick="hideCapCode('N')">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
											
											<!-- 只能選晶片金融卡時，預設為非勾選讓使用者點擊後顯示驗證碼-->
											<c:choose>
												<c:when test="${sessionScope.isikeyuser}">
													<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
												</c:when>
												<c:otherwise>
												 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
												</c:otherwise>
											</c:choose>
											
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
										<label class="radio-block">裝置推播認證(請確您的行動裝置網路連線是否正常，及推播功能是否已開啟)
											<input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
											<span class="ttb-radio"></span>
										</label>		 
									</div>
								</span>
							</div>
							
							<!-- 驗證碼  -->
							<div class="ttb-input-item row" id="capCodeDiv" style="display:none">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Captcha" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
                                    <div class="ttb-input">
                                    <spring:message code="LB.Captcha" var="labelCapCode" />
										<input id="capCode" type="text" class="text-input input-width-125"
											name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
										<img name="kaptchaImage" class="verification-img" src="" />
										<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration"/>" />
                                    	<span class="input-remarks"><spring:message code="LB.Captcha_refence" /></span>
                                    </div>
								</span>
							</div>
							
						</div>
						<!--回上頁 -->
                        <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
                        <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
						<input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />			
					</div>
				</div>
			</form>
<!-- 			<div class="text-left"> -->
<!-- 			    		說明： -->
<%-- 				<spring:message code="LB.Description_of_page"/>: --%>
<!-- 			    <ol class="list-decimal text-left"> -->
<!-- 			        <li>如選擇晶片金融卡機制，按「確定」鍵前，請將讀卡機正確連接電腦，並插入晶片金融卡。</li> -->
<!-- 			    </ol> -->
<!-- 			</div> -->
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後0.1秒開始遮罩
			setTimeout("initBlockUI()", 100);

			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			
			init();
			
			// 過0.5秒解遮罩
			setTimeout("unBlockUI(initBlockId)", 800);
			<c:if test = "${!sessionScope.isikeyuser}">
			hideCapCode('N');
			</c:if>
		});
		
		/**
		 * 初始化BlockUI
		 */
		function initBlockUI() {
			initBlockId = blockUI();
		}

		// 交易機制選項
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
		
			switch(fgtxway) {
				case '0':
// 					alert("交易密碼(SSL)...");
	    			$("form").submit();
					break;
					
				case '1':
// 					alert("IKey...");
					var jsondc = $("#jsondc").val();
					
					// 遮罩後不給捲動
// 					document.body.style.overflow = "hidden";

					// 呼叫IKEY元件
// 					uiSignForPKCS7(jsondc);
					
					// 解遮罩後給捲動
// 					document.body.style.overflow = 'auto';
					
					useIKey();
					break;
					
				case '2':
// 					alert("晶片金融卡");

					// 遮罩後不給捲動
// 					document.body.style.overflow = "hidden";
					
					// 呼叫讀卡機元件
					listReaders();

					// 解遮罩後給捲動
// 					document.body.style.overflow = 'auto';
					
			    	break;
				case '7'://IDGATE認證		 
	               idgatesubmit= $("#formId");		 
	               showIdgateBlock();		 
	               break;
				default:
					//alert("nothing...");
					errorBlock(
							null, 
							null,
							["nothing..."], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
			
		}

	    function init(){
// 	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	    	$("#pageshow").click(function(e){			
				console.log("submit~~");
				var capResultData = false;
				if($('input[name="FGTXWAY"]:checked').val() == '1' || $('input[name="FGTXWAY"]:checked').val() == '7'){
					capResultData = true;
				}else if($('input[name="FGTXWAY"]:checked').val() == '2'){
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					var capData = $("#formId").serializeArray();
					var capResult = fstop.getServerDataEx(capUri, capData, false);
					capResultData = capResult.result;
				}
				if (capResultData) {
					if(!$('#formId').validationEngine('validate') && $('input[name="FGTXWAY"]:checked').val() == 0){
			        	e.preventDefault();
		 			}else{
	// 					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
		 				$("#formId").validationEngine('detach');
		 				initBlockUI();
	    				var action = '${__ctx}/OTHER/FEE/other_old_retire_result';
		    			$("form").attr("action", action);
		    			unBlockUI(initBlockId);
		    			processQuery();
		 			}
				} else {
					//alert("<spring:message code= "LB.X1082" />");
					errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X1082' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
					changeCode();
				}
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/OTHER/FEE/other_old_retire';
    			$('#back').val("Y");
    			$("form").attr("action", action);
    			$("form").submit();
    		});
	    }	
	    
		// 刷新驗證碼
		function refreshCapCode() {
			console.log("refreshCapCode...");

			// 驗證碼
			$('input[name="capCode"]').val('');
			
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 刷新輸入欄位
		function changeCode() {
			console.log("changeCode...");
			
			// 清空輸入欄位
			$('input[name="capCode"]').val('');
			
			// 刷新驗證碼
			refreshCapCode();
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				refreshCapCode();
			});
		}
		
		function hideCapCode(flag){
			console.log(flag);
			if(flag=='Y')
				$("#capCodeDiv").attr('style','display: none;');
			if(flag=='N')
				$("#capCodeDiv").attr('style','');
				
		}

 	</script>
</body>
</html>
 