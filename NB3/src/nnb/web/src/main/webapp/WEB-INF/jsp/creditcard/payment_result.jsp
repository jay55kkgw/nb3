<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script>
$(document).ready(function(){
	$("#printbtn").click(function(){
		var i18n = new Object();
		i18n['jspTitle']='<spring:message code="LB.Payment_The_Banks_Credit_Card" />'
		var params = {
			"jspTemplateName":"payment_result_print",
			"jspTitle":i18n['jspTitle'],
			"CMQTIME":"${payment_result_data.data.CMQTIME}",
			"BOOKDATE":"${payment_result_data.data.transfer_date }",
			"OUTACN":"${payment_result_data.data.ACN }",
			"INACN":"${payment_result_data.data.CARDNUM }",
			"AMOUNT":"${payment_result_data.data.AMOUNT_SHOW}",
			"CMTRMEMO":"${payment_result_data.data.CMTRMEMO }",
			"O_TOTBAL":"${payment_result_data.data.O_TOTBAL }",
			"O_AVLBAL":"${payment_result_data.data.O_AVLBAL }",
			"TRANSERTYPE":"${payment_result_data.data.FGTXDATE}"
		}
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 繳本行信用卡費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Payment_The_Banks_Credit_Card" /></li>
		</ol>
	</nav>


	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	

	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容 -->
			<h2>
				<spring:message code="LB.Payment_The_Banks_Credit_Card" /><!-- 繳納本行信用卡 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<div id="step-bar">
				    <ul>
				       <ul>
						<li class="finished">
								<spring:message code="LB.Enter_data" /><!-- 輸入資料 -->
						</li>
						<li class="finished">
							<spring:message code="LB.Confirm_data" /><!-- 確認資料 -->
						</li>
						<li class="active">
							<spring:message code="LB.Transaction_complete" /><!-- 交易完成 -->
						</li>
					</ul>
				    </ul>
				</div>

			<div class="main-content-block row">
				<div class="col-12">
					<div class="ttb-message">
						<span> 
							<c:if test="${payment_result_data.data.FGTXDATE =='1'}">
								<spring:message code="LB.Payment_successful" />
							</c:if>
							<c:if test="${payment_result_data.data.FGTXDATE =='2'}">
								<spring:message code="LB.W0284" /><!--預約成功 -->
							</c:if>
						</span>
					</div>
					<div class="ttb-input-block">
					<!--交易時間區塊 -->
						<div class="ttb-input-item row">
							<!--交易時間  -->
							<span class="input-title">
								<label>
									<spring:message code="LB.Trading_time" /><!-- 交易時間 -->
								</label>
							</span>
							<span class="input-block">
								${payment_result_data.data.CMQTIME}
							</span>
						</div>
					<!--預約才有轉帳日期 -->
					<c:if test="${payment_result_data.data.FGTXDATE eq '2'}">
						<div class="ttb-input-item row">
							<!--交易時間  -->
							<span class="input-title">
								<label>
									<spring:message code="LB.Transfer_date" /><!-- 轉帳日期 -->
								</label>
							</span>
							<span class="input-block">
								${payment_result_data.data.transfer_date }
							</span>
						</div>
					</c:if>
				    <!--  預約才有轉帳日期END -->
				    <!-- 轉出帳號區塊 -->
						<div class="ttb-input-item row">
							<!-- 轉出帳號 -->
							<span class="input-title">
								<label>
									<spring:message code="LB.Payers_account_no" /><!-- 轉出帳號 -->
								</label>
							</span>
							<span class="input-block">
								${payment_result_data.data.ACN }
							</span>
						</div>
					 <!-- 轉帳金額區塊 -->
						<div class="ttb-input-item row">
							<!-- 轉帳金額 -->
							<span class="input-title">
								<label>
									<spring:message code="LB.D0021" /><!-- 轉帳金額 -->
								</label>
							</span>
							<span class="input-block">
								<spring:message code="LB.NTD" />&nbsp; <!-- 新台幣 -->
								${payment_result_data.data.AMOUNT_SHOW}
					        	&nbsp;<spring:message code="LB.Dollar"/>
							</span>
						</div>
					     <!-- 交易備註區塊 -->
						<div class="ttb-input-item row">
							<!-- 交易備註 -->
							<span class="input-title">
								<label>
									<spring:message code="LB.Transfer_note" /><!-- 交易備註 -->
								</label>
							</span>
							<span class="input-block">
								${payment_result_data.data.CMTRMEMO}
							</span>
						</div>
					 <!-- 即時轉帳才顯示以下欄位 --> 
					 <c:if test="${payment_result_data.data.FGTXDATE eq '1'}">
					 	 <!-- 轉出帳號帳戶餘額區塊 -->
						<div class="ttb-input-item row">
							<!-- 轉出帳號帳戶餘額 -->
							<span class="input-title">
								<label>
									<spring:message code="LB.Payers_account_balance" /><!-- 	轉出帳號帳戶餘額 -->
								</label>
							</span>
							<span class="input-block">
								<spring:message code="LB.NTD" />&nbsp; <!-- 新台幣 -->
					        	${payment_result_data.data.O_TOTBAL}
					        	&nbsp;<spring:message code="LB.Dollar"/>
							</span>
						</div>
						<!-- 轉出帳號可用餘額區塊 -->
						<div class="ttb-input-item row">
							<!-- 轉出帳號可用餘額 -->
							<span class="input-title">
								<label>
									<spring:message code="LB.Payers_available_balance" /><!-- 轉出帳號可用餘額 -->
								</label>
							</span>
							<span class="input-block">
								<spring:message code="LB.NTD" />&nbsp; <!-- 新台幣 -->	
					        	${payment_result_data.data.O_AVLBAL}
					        	&nbsp;<spring:message code="LB.Dollar"/>
							</span>
						</div>
					 </c:if>
					<!-- 即時轉帳才顯示以下欄位end --> 
				</div>
					<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
           </div><!-- main-content-block END -->
           </div>
          			 <c:if test="${payment_result_data.data.FGTXDATE == '1'}">
	              		<ol class="description-list list-decimal">
							<p><spring:message code="LB.Description_of_page" /><p><!-- 說明 -->
								<li><spring:message code="LB.Payment_P3_D1" />
								<!-- 您的轉帳手續已完成，是否入帳成功，請於次營業日自行上網查詢或向發卡分行查詢。 -->
								</li>
	         					<li><spring:message code="LB.Payment_P3_D2" />
	         					<!-- 晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。 -->
	         					</li>
	         					<li><spring:message code="LB.Payment_P3_D3" />
	         					<!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
	         					</li>
							</ol>
					</c:if>
					<!-- 預約轉帳才顯示以下欄位 -->
					<c:if test="${payment_result_data.data.FGTXDATE == '2'}">
	              		<ol class="description-list list-decimal">
							<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
								<li><spring:message code="LB.Payment_PP3_D1" />
								<!-- 如預約之轉帳日期為曆法所無之日期，以該月之末日為轉帳日，例如：預約6月31日轉帳，因6月無31日，故會在6月30日轉帳。 -->
								</li>
								<li><spring:message code="LB.Payment_PP3_D2" />
								<!-- 預約交易請於轉帳日之前1日，存足款項於轉出帳號備扣。預約交易將於轉帳日與即時交易併計最高轉出限額。 -->
								</li>
								<li><spring:message code="LB.Payment_PP3_D3" />
								<!-- 預約成功不代表交易已完成，請於轉帳日利用『預約交易結果查詢』，以確認交易結果。 -->
								</li>
								<li><spring:message code="LB.Payment_PP3_D4" />
								<!-- 如欲取消預約，請於轉帳日之前1日辦理。交易密碼(SSL)預約交易，請以交易密碼(SSL)取消交易；晶片金融卡預約交易，請以晶片金融卡取消交易；電子簽章(i-key)預約交易，請以電子簽章(i-key)取消預約。 -->
								</li>
								<li><spring:message code="LB.Payment_PP3_D5" />
								<!-- 晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。 -->
								</li>
								<li><spring:message code="LB.Payment_PP3_D6" />
								<!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
								</li>
							</ol>
					</c:if>
       </section>    
	</main>
</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
      
</body>
</html>