<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 查詢服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.use_component_P1_D1-1" /></li>
    <!-- 黃金存摺明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1442" /></li>
		</ol>
	</nav>

		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<!-- 		主頁內容  -->
		<main class="col-12">	

			<section id="main-content" class="container">
			<!--  黃金存摺明細查詢 -->
				<h2><spring:message code="LB.W1442" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
						<!--下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
						<!--下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
					<div class="main-content-block row">
						<div class="col-12"> 						
						<form method="post" id="formId" action="${__ctx}/GOLD/PASSBOOK/gold_detail_query">
							<!-- 下載用 -->
							<!-- 下載檔名:黃金存摺明細查詢 -->
							<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W1442" />"/>
							<input type="hidden" name="downloadType" id="downloadType"/> 					
							<input type="hidden" name="templatePath" id="templatePath"/> 	
							<input type="hidden" name="hasMultiRowData" value="true"/> 	
							<!-- EXCEL下載用 -->
		                    <input type="hidden" name="headerRightEnd" value="1" />
		                    <input type="hidden" name="headerBottomEnd" value="4" />
		                    <input type="hidden" name="multiRowStartIndex" value="8" />
		                    <input type="hidden" name="multiRowEndIndex" value="8" />
		                    <input type="hidden" name="multiRowCopyStartIndex" value="6" />
		                    <input type="hidden" name="multiRowCopyEndIndex" value="9" />
		                    <input type="hidden" name="multiRowDataListMapKey" value="TABLE" />
		                    <input type="hidden" name="rowRightEnd" value="8" />
		                    <!-- TXT下載用 -->
		                    <input type="hidden" name="txtHeaderBottomEnd" value="6"/>
							<input type="hidden" name="txtMultiRowStartIndex" value="11"/>
							<input type="hidden" name="txtMultiRowEndIndex" value="11"/>
							<input type="hidden" name="txtMultiRowCopyStartIndex" value="7"/>
							<input type="hidden" name="txtMultiRowCopyEndIndex" value="12"/>
							<input type="hidden" name="txtMultiRowDataListMapKey" value="TABLE"/>
							<div class="ttb-input-block">
					        <!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 帳號 -->
											<spring:message code="LB.Account"/>
										</label>
									</span>
									<span class="input-block">
									<div class="ttb-input">
										<select name="ACN" id="ACN" class="custom-select select-input half-input validate[required]">
										<c:forEach var="dataList" items="${ gold_detail_query.data.REC }">
						           			<option value="${dataList.ACN}">${dataList.ACN}</option>
						           		</c:forEach>
						           		<option value="ALL">
											<spring:message code="LB.All" />
										</option>
										</select>
										</div>
									</span>
								</div>
								<!-- 查詢區間區塊 -->
								<div class="ttb-input-item row">
									<!--查詢區間  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Inquiry_period" />
										</label>
									</span>
									<span class="input-block">
										<!--  當日交易 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="FGPERIOD" id="CMTODAY" value="CMTODAY" checked/>
												<label><spring:message code="LB.Todays_transaction" /></label>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  當月交易 -->
										<div class="ttb-input">
											<label class="radio-block">
											<input type="radio" name="FGPERIOD" id="CMCURMON" value="CMCURMON" />
											<label><spring:message code="LB.This_months_transaction" /></label>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 上月交易 -->
										<div class="ttb-input">
											<label class="radio-block">
											<input type="radio" name="FGPERIOD" id="CMLASTMON" value="CMLASTMON" />
											<label><spring:message code="LB.Last_months_trading" /></label>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 最近一個月 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Recently_a_month" />
												<input type="radio" name="FGPERIOD" id="CMNEARMON" value="CMNEARMON" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  最近一星期 -->
										<div class="ttb-input">
											<label class="radio-block">
											<input type="radio" name="FGPERIOD" id="CMLASTWEEK" value="CMLASTWEEK" />
											<label><spring:message code="LB.Recently_a_week" /></label>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  指定日期區塊 -->
										<div class="ttb-input">								
											<label class="radio-block">
											<!--  指定日期 -->
												<spring:message code="LB.Specified_period" />
												<input type="radio" name="FGPERIOD" id="CMPERIOD" value="CMPERIOD" /> 
												<span class="ttb-radio"></span>
											</label>
											<!-- 起迄日區塊預設隱藏 -->
										<div id = "hideblock_FGPERIOD" style="display: none;">
											<!--期間起日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_start_date" />
												</span>
												<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" maxlength="10" value="" /> 
												<span class="input-unit CMSDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
											</div>
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_end_date" />
												</span>
												<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" maxlength="10" value="" /> 
												<span class="input-unit CMEDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
											</div>
													
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CheckDateScope" >
												<!-- 驗證用的input -->
												<!-- 查詢期間 -->
												<input id="odate" name="odate" type="text" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, CMSDATE, CMEDATE, false, 24, 2]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
											<!-- 起迄日區塊 -END-->
										</div>
									</span>
								</div>
							</div>
							
						</form>
						<input type="button" class="ttb-button btn-flat-orange" id="pageshow" value="<spring:message code="LB.Display_as_web_page" />"/>
					</div>
				</div>
				<div class="text-left">
				<ol class="description-list list-decimal text-left">
				<!-- 說明 -->
					<p><spring:message code="LB.Description_of_page" /></p>
						<!-- 提供最近二年內明細查詢，惟每次查詢期間最多二個月，請分次查詢，例如：欲查詢2010年1月28日起之資料，迄日最多可輸入2010年3月28日，其後資料請分次查詢。 -->
						<li><span><spring:message code= "LB.gold_detail_query_P1_D1" /></span></li>
						<li><span><spring:message code= "LB.gold_detail_query_P1_D2-1" /><a target="_blank"
							href='${pageContext.request.contextPath}/public/N191_TXT.htm'>
							<!-- 檔案欄位說明 --> 「<spring:message code="LB.File_description" />」
							<!-- 含報表表頭 -->
						</a><spring:message code= "LB.gold_detail_query_P1_D2-3" />
						</span></li>
					</ol>
				</div>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		// 初始化時隱藏span
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
		datetimepickerEvent();
		$("#hideblock_CheckDateScope").show();
		getTmr();
		console.log("odate~~"+$('#odate').val());
		initFootable();	
		fgperiodChageEvent();
		$("#ACN").children().each(function(){
			if(this.value == "${ gold_detail_query.data.FASTMENU }"){
				$(this).prop("selected", "true");
			}
		});
		//btn 
		$("#CMRESET").click(function () {
			$("#formId")[0].reset();
			$("#hideblock_FGPERIOD").hide();
			getTmr();
		});
		$("#pageshow").click(function(e){
			e = e || window.event;
			//打開驗證隱藏欄位
			$("#hideblock_CheckDateScope").show();
			console.log("submit~~");
			console.log("CMSDATE~~"+$('#CMSDATE').val());
			console.log("CMEDATE~~"+$('#CMEDATE').val());
			console.log("odate~~"+$('#odate').val());
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
 				$("#formId").validationEngine('detach');
 				initBlockUI();
 				$("#formId").removeAttr("target");
 				$("#formId").attr("action","${__ctx}/GOLD/PASSBOOK/gold_detail_query_result");
 	  			$("#formId").submit(); 
 			}		
		

  		});
		$("#hideblock_CheckDateScope").hide();
	}
	//日期區間切換按鈕,如果點選指定日期則顯示起迄日
	function fgperiodChageEvent() {
		$('input[type=radio][name=FGPERIOD]').change(function () {
			if (this.value == 'CMPERIOD') {
				$("#hideblock_FGPERIOD").show();
				$("#odate").addClass("validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, CMSDATE, CMEDATE, false, 24, 2]]]");

			} else {
				$("#hideblock_FGPERIOD").hide();
				$("#odate").removeClass("validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, CMSDATE, CMEDATE, false, 24, 2]]]");
				getTmr();
			}
		});
	}
	
	function getTmr() {
		var today = "${gold_detail_query.data.TODAY}";
		$('#CMSDATE').val(today);
		$('#CMEDATE').val(today);
		$('#odate').val(today);
	}
    
	// 日曆欄位參數設定
	function datetimepickerEvent() {
		$(".CMSDATE").click(function (event) {
			$('#CMSDATE').datetimepicker('show');
		});
		$(".CMEDATE").click(function (event) {
			$('#CMEDATE').datetimepicker('show');
		});
		jQuery('.datetimepicker').datetimepicker({
			timepicker: false,
			closeOnDateSelect: true,
			scrollMonth: false,
			scrollInput: false,
			format: 'Y/m/d',
			lang: '${transfer}'
		});
	}	
	
	//選項
	 	function formReset() {
	 		//打開驗證隱藏欄位
			$("#hideblock_CheckDateScope").show();
			if(!$("#formId").validationEngine("validate")){
				e = e || window.event;//forIE
				e.preventDefault();
			}
	 		else{
				//initBlockUI();
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/gold_detail_query.xls");
		 		}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/gold_detail_query.txt");
		 		}
		 		$("#formId").attr("target", "");
                $("#formId").attr("action", "${__ctx}/GOLD/PASSBOOK/gold_detail_query_ajaxDirectDownload");
                $("#formId").submit();
                $('#actionBar').val("");
	 		}
		}
		function finishAjaxDownload(){
			$("#actionBar").val("");
			unBlockUI(initBlockId);
		}
 	</script>
</body>
</html>
