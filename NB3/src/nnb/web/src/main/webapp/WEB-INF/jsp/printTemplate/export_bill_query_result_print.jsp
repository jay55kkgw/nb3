<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function () {
			window.print();
		});
	</script>
</head>

<body class="watermark" style="-webkit-print-color-adjust: exact;width:90%;margin:0% 0% 0% 5%" >
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
		<!-- 查詢時間 -->
		<p>
			<spring:message code="LB.Inquiry_time" /> : ${CMQTIME}
		</p>
		<!-- 查詢期間 -->
		<p>
			<spring:message code="LB.Inquiry_period" /> : ${CMPERIOD}
		</p>
		<!-- 信用狀號碼 -->
		<p>
			<spring:message code= "LB.L/C_no" /> : 
			<c:if test="${LCNO =='' }">
				<spring:message code= "${LCNO_SHOW}" />
			</c:if>
			<c:if test="${LCNO !='' }">
				${LCNO_SHOW}
			</c:if>
		</p>
		<!-- 押匯編號 : -->
		<p>
			<spring:message code= "LB.Ref_no" /> : 
			<c:if test="${REFNO =='' }">
				<spring:message code= "${REFNO_SHOW}" />
			</c:if>
			<c:if test="${REFNO !='' }">
				${REFNO_SHOW}
			</c:if>
		</p>
		<!-- 資料總數 -->
		<p>
			<spring:message code="LB.Total_records" />：${CMRECNUM} <spring:message code="LB.Rows" />
		</p>
			<table class="print">
				<thead>
					<tr>
						<th style="text-align:center"><spring:message code= "LB.W0218" /></th>
						<th style="text-align:center"><spring:message code= "LB.Ref_no" /></th>
						<th style="text-align:center"><spring:message code= "LB.L/C_no" /></th>
						<th style="text-align:center"><spring:message code= "LB.Currency" /></th>
						<th style="text-align:center"><spring:message code= "LB.W0220" /></th>
						<th style="text-align:center"><spring:message code= "LB.W0195" /></th>
						<th style="text-align:center"><spring:message code= "LB.W0196" /></th>
						<th style="text-align:center"><spring:message code= "LB.W0198" /></th>
						<th style="text-align:center"><spring:message code= "LB.W0203" /></th>
						<th style="text-align:center"><spring:message code= "LB.W0199" /></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="dataList"
						items="${dataListMap[0]}">
						<tr>
							<td style="text-align:center">${dataList.RNEGDATE}</td>
							<td style="text-align:center">${dataList.RREFNO }</td>
							<td style="text-align:center">${dataList.RLCNO }</td>
							<td style="text-align:center">${dataList.RBILLCCY }</td>
							<td style="text-align:right">${dataList.RBILLAMT }</td>
							<td style="text-align:center">${dataList.RVALDATE }</td>
							<td style="text-align:center"><spring:message code="${dataList.RUPSTAT }"/></td>
							<td style="text-align:center">${dataList.RCOUNTRY }</td>
							<td style="text-align:center">${dataList.RBILLTYP }</td>
							<td style="text-align:center">${dataList.RCMDATE1 }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<br />
			<div class="text-left">
		<p><spring:message code= "LB.W0227" />：</p>
		<c:forEach var="map2" items="${dataListMap[1]}">
			<p>&nbsp;${map2.AMTRBILLCCY}&nbsp;${map2.FXTOTAMT}&nbsp;<spring:message code= "LB.W0158" />&nbsp;${map2.FXTOTAMTRECNUM}&nbsp;<spring:message code= "LB.Rows" /></p>
		</c:forEach>
	</div>	
</body>

</html>