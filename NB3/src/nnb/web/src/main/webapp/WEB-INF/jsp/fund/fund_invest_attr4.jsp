<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<!-- DIALOG會用到 -->
<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">
<!--交易機制所需JS-->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdProcess.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<style>
 .ui-dialog-titlebar-close {
    display: none;
 }
</style>
<script type="text/javascript">
$(document).ready(function(){
	$("#formId").validationEngine({binded: false,promptPosition: "inline", scroll: false});
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	changeCode();
	
	if(${RISK != null && RISK != ""}){
		$("#SSLDIV").show();
		$("#IKEYDIV").hide();
		$("#CARDDIV").hide();
		fstop.setRadioChecked("FGTXWAY" , "0" ,true);
	}
	else{
		//20220330_H681110000015
		errorBlock(
				["<spring:message code= 'LB.KYC_alert1' />"], 
				["<li style='list-style:none;'><font color='red'><spring:message code= 'LB.KYC_alert2' /></font></li>"], 				
				null, 
				'<spring:message code= "LB.Quit" />', 
				null
		);//20220330_end
		
		$("#SSLDIV").hide();
		$("#IKEYDIV").show();
		$("#CARDDIV").show();
		fstop.setRadioChecked("FGTXWAY" , "2" ,true);
		$("#CAPTCHATR").show();
	}
	$("input[name=FGTXWAY]").click(function(){
		if($(this).val() == "2"){
			$("#CAPTCHATR").show();
		}
		else{
			$("#CAPTCHATR").hide();
		}
	});
	$("input[name=Q1][value=${Q1}]").prop("checked",true);
	$("input[name=Q2][value=${Q2}]").prop("checked",true);
	$("input[name=Q3][value=${Q3}]").prop("checked",true);
	$("input[name=Q4][value=${Q4}]").prop("checked",true);
	$("input[name=Q5][value=${Q5}]").prop("checked",true);
	$("input[name=Q6][value=${Q6}]").prop("checked",true);
	if(${Q71 != null}){
		$("#Q71").prop("checked",true);
	}
	if(${Q72 != null}){
		$("#Q72").prop("checked",true);
	}
	if(${Q73 != null}){
		$("#Q73").prop("checked",true);
	}
	if(${Q74 != null}){
		$("#Q74").prop("checked",true);
	}
	$("input[name=Q8][value=${Q8}]").prop("checked",true);
	if(${Q91 != null}){
		$("#Q91").prop("checked",true);
	}
	if(${Q92 != null}){
		$("#Q92").prop("checked",true);
	}
	if(${Q93 != null}){
		$("#Q93").prop("checked",true);
	}
	if(${Q94 != null}){
		$("#Q94").prop("checked",true);
	}
	$("input[name=Q10][value=${Q10}]").prop("checked",true);
	$("input[name=Q11][value=${Q11}]").prop("checked",true);
	$("input[name=Q12][value=${Q12}]").prop("checked",true);
	$("input[name=Q13][value=${Q13}]").prop("checked",true);
	$("input[name=Q14][value=${Q14}]").prop("checked",true);
	
	
	var w = $(window).width() < 850 ? $(window).width() : 850 ;
	$("#agreeDialog").dialog({
		autoOpen:false,
		modal:true,
		closeOnEscape:false,
		width:w,
		height:250,
		position:{
			my:"center",at:"top",of:window
		}
	});
	$("#CMSUBMIT").click(function(e){
		if (!$('#formId').validationEngine('validate')&& !$('input[name="FGTXWAY"]:checked').val() == '7') {
			e.preventDefault();
		} else {
			$("#formId").validationEngine('detach');
			var FGTXWAY = $("input[name=FGTXWAY]:checked").val();
			
			if(FGTXWAY == null){
				//alert("<spring:message code= "LB.X1532" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X1532' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
				return false;
			}
			console.log("fgtxway: " + FGTXWAY);
			switch(FGTXWAY) {
				case '0':
					if(!CheckPuzzle("CMPWD")){
						return false;
					}
					$("#CMPASSWORD").val($("#CMPWD").val());
					var PINNEW = pin_encrypt($("#CMPASSWORD").val());
					$("#PINNEW").val(PINNEW);
					//20201228
					var uri = "${__ctx}/FUND/TRANSFER/n951_aj";
					var rdata = { PINNEW: PINNEW };
					data = fstop.getServerDataEx(uri,rdata,false);
					if(data !=null && data.result == true ){
						var uri = "${__ctx}/FUND/TRANSFER/insert_hist";
						var rdata2 = $("#formId").serializeArray();
						data2 = fstop.getServerDataEx(uri,rdata2,false);
						if(data2 !=null && data2.result == true ){
							$("#FDHISTID").val(data2.data.FDHISTID);
							$("#FDSCORE").val(data2.data.iScore);
							$("#FDINVTYPE").val(data2.data.FDINVTYPE);
							if('1'==data2.data.FDINVTYPE){
								//積極型
								$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.D0945' />");
							}else if ("2"==data2.data.FDINVTYPE){
								//穩健型
								$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.X1766' />");
							}else{
								//保守型
								$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.X1767' />");
							}
							$("#kyc_insert_uuid").val(data2.data.kyc_insert_uuid);
							$("#agreeDialog").dialog("open");
						}else{
							if(data2.msgCode != null && data2.msgCode == "FE0019"){
								var message = data2.message;
								callErrorBlock(message);
								// 複寫errorBtn1 事件
								$("#errorBtn1").click(function(e) {
									$("#formId").validationEngine('detach');
							    	var action = '${__ctx}/INDEX/index';
									$("#formId").attr("action", action);
									$("#formId").submit();
								});
							}else{
								alert(data2.msgCode + "：" + data2.message);
							}
							//insert DB wrong
						}
						break;
					}else{
						//n951 error
						alert(data.msgCode + "：" + data.message);
						return false;
						break;
					}
	// 				alert("交易密碼(SSL)...");
					
				case '1':
	// 				alert("IKey...");
					useIKey();
					break;
					
				case '2':
	// 				alert("晶片金融卡");
					// 讀卡機...
					urihost = "${__ctx}";
					console.log("urihost: " + urihost);
					useCardReader();
			    	break;
				case '7'://IDGATE認證		 
	               idgatesubmit= $("#formId");		 
	               showIdgateBlock();		 
	               break;
				default:
					//alert("nothing...");
			}	
		
		}
	});
	$("#cancelButton").click(function(){
		$("#formId").validationEngine('detach');
		$("#formId").attr("action","${__ctx}/FUND/TRANSFER/fund_invest_attr3?TXID=${RTC}");
		$("#formId").submit();
	});
	//此處是同意
	$("#CMSUBMIT_Y").click(function() {
		$("#AGREE").val("Y");
		$("#DEVNAME").val(getOSAndBrowser());
		$("#formId").submit();
		$("#agreeDialog").dialog("close");
		initBlockUI();
	});
	//此處是不同意
	$("#CMSUBMIT_N").click(function() {
		$("#AGREE").val("N");
		$("#DEVNAME").val(getOSAndBrowser());
		$("#formId").submit();
		$("#agreeDialog").dialog("close");
		initBlockUI();
	});
	
	
});
function changeCode(){
	$("#capCode").val("");
	$("#kaptchaImage").hide().attr("src","${__ctx}/CAPCODE/captcha_image_trans?" + Math.floor(Math.random() * 100)).fadeIn();
}

function getOSAndBrowser() {  
    var os = navigator.platform;  
    var userAgent = navigator.userAgent;  
    var info = "";  
    var tempArray = "";  
    //判斷作業系統  
    if (os.indexOf("Win") > -1) {  
        if (userAgent.indexOf("Windows NT 5.0") > -1) {  
            info += "Win2000";  
        } else if (userAgent.indexOf("Windows NT 5.1") > -1) {  
            info += "WinXP";  
        } else if (userAgent.indexOf("Windows NT 5.2") > -1) {  
            info += "Win2003";  
        } else if (userAgent.indexOf("Windows NT 6.0") > -1) {  
            info += "WindowsVista";  
        } else if (userAgent.indexOf("Windows NT 6.1") > -1 || userAgent.indexOf("Windows 7") > -1) {  
            info += "Win7";  
        } else if (userAgent.indexOf("Windows NT 6.2") > -1 || userAgent.indexOf("Windows 8") > -1) {  
            info += "Win8";  
        } else if (userAgent.indexOf("Windows NT 6.3") > -1 || userAgent.indexOf("Windows 8.1") > -1) {  
            info += "Win8.1";  
        } else if (userAgent.indexOf("Windows NT 10.0") > -1 || userAgent.indexOf("Windows 10") > -1) {  
            info += "Win10";  
        }  
        else {  
            info += "Other";  
        }  
    } else if (os.indexOf("Mac") > -1) {  
        info += "Mac";  
    } else if (os.indexOf("X11") > -1) {  
        info += "Unix";  
    } else if (os.indexOf("Linux") > -1) {  
        info += "Linux";  
    } else {  
        info += "Other";  
    }  
    info += " ";  


    //判斷瀏覽器版本  
    var isOpera = userAgent.indexOf("Opera") > -1; //判斷是否Opera瀏覽器  
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera; //判斷是否IE瀏覽器  
    var isEdge = userAgent.toLowerCase().indexOf("edge") > -1 && !isIE; //判斷是否IE的Edge瀏覽器  
    var isIE11 = (userAgent.toLowerCase().indexOf("trident") > -1 && userAgent.indexOf("rv") > -1);  

    if (/[Ff]irefox(\/\d+\.\d+)/.test(userAgent)) {  
        tempArray = /([Ff]irefox)\/(\d+\.\d+)/.exec(userAgent);  
        info += tempArray[1] + tempArray[2];  
    } else if (isIE) {  

        var version = "";  
        var reIE = new RegExp("MSIE (\\d+\\.\\d+);");  
        reIE.test(userAgent);  
        var fIEVersion = parseFloat(RegExp["$1"]);  
        if (fIEVersion == 7)  
        { version = "IE7"; }  
        else if (fIEVersion == 8)  
        { version = "IE8"; }  
        else if (fIEVersion == 9)  
        { version = "IE9"; }  
        else if (fIEVersion == 10)  
        { version = "IE10"; }  
        else  
        { version = "0" }  

        info += version;  

    } else if (isEdge) {  
        info += "Edge";  
    } else if (isIE11) {  
        info += "IE11";  
    } else if (/[Cc]hrome\/\d+/.test(userAgent)) {  
        tempArray = /([Cc]hrome)\/(\d+)/.exec(userAgent);  
        info += tempArray[1] + tempArray[2];  
    } else if (/[Vv]ersion\/\d+\.\d+\.\d+(\.\d)* *[Ss]afari/.test(userAgent)) {  
        tempArray = /[Vv]ersion\/(\d+\.\d+\.\d+)(\.\d)* *([Ss]afari)/.exec(userAgent);  
        info += tempArray[3] + tempArray[1];  
    } else if (/[Oo]pera.+[Vv]ersion\/\d+\.\d+/.test(userAgent)) {  
        tempArray = /([Oo]pera).+[Vv]ersion\/(\d+)\.\d+/.exec(userAgent);  
        info += tempArray[1] + tempArray[2];  
    } else {  
        info += "unknown";  
    }  
    return info;  
};

//複寫checkIdProcess.js
//卡片押碼結束
function generateTACFinish(result){
	//成功
	if(result != "false"){
		var TACData = result.split(",");
		var iSeqNo = TACData[1];
		var ICSEQ = TACData[1];
		var TAC = TACData[2];
		$("#iSeqNo").val(iSeqNo);
		$("#ICSEQ").val(ICSEQ);
		$("#TAC").val(TAC);
		var ACN_Str1 = $("#ACNNO").val();
		// 遮罩
		checkIdBlockId = initBlockUI();
		var uri = "${__ctx}/FUND/TRANSFER/insert_hist";
		var rdata2 = $("#formId").serializeArray();
		data2 = fstop.getServerDataEx(uri,rdata2,false);
		if(data2 !=null && data2.result == true ){
			$("#FDHISTID").val(data2.data.FDHISTID);
			$("#FDSCORE").val(data2.data.iScore);
			$("#FDINVTYPE").val(data2.data.FDINVTYPE);
			if('1'==data2.data.FDINVTYPE){
				//積極型
				$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.D0945' />");
			}else if ("2"==data2.data.FDINVTYPE){
				//穩健型
				$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.X1766' />");
			}else{
				//保守型
				$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.X1767' />");
			}
			$("#kyc_insert_uuid").val(data2.data.kyc_insert_uuid);
			$("#agreeDialog").dialog("open");
			unBlockUI(checkIdBlockId);
		}else{
			if(data2.msgCode != null && data2.msgCode == "FE0019"){
				var message = data2.message;
				callErrorBlock(message);
				// 複寫errorBtn1 事件
				$("#errorBtn1").click(function(e) {
					$("#formId").validationEngine('detach');
			    	var action = '${__ctx}/INDEX/index';
					$("#formId").attr("action", action);
					$("#formId").submit();
				});
			}else{
				alert(data2.msgCode + "：" + data2.message);
			}
			return false;
			//insert DB wrong
		}
// 		$("#formId").submit();
	}
	//失敗
	else{
		FinalSendout("MaskArea",false);
	}
}

////複寫checkIdProcess.js
function SubmitForm()
{
	FinalSendout("MaskArea",true);
	
	if(document.getElementById("CMCARD").checked == true)
	{
		//卡片押碼
		getMainAccount();
	}
	else{
		// 遮罩
// 		initBlockUI();
		var formId = document.getElementById("formId");
		var uri = "${__ctx}/FUND/TRANSFER/insert_hist";
		var rdata2 = $("#formId").serializeArray();
		data2 = fstop.getServerDataEx(uri,rdata2,false);
		if(data2 !=null && data2.result == true ){
			$("#FDHISTID").val(data2.data.FDHISTID);
			$("#FDSCORE").val(data2.data.iScore);
			$("#FDINVTYPE").val(data2.data.FDINVTYPE);
			if('1'==data2.data.FDINVTYPE){
				//積極型
				$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.D0945' />");
			}else if ("2"==data2.data.FDINVTYPE){
				//穩健型
				$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.X1766' />");
			}else{
				//保守型
				$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.X1767' />");
			}
			$("#kyc_insert_uuid").val(data2.data.kyc_insert_uuid);
			$("#agreeDialog").dialog("open");
		}else{
			if(data2.msgCode != null && data2.msgCode == "FE0019"){
				var message = data2.message;
				callErrorBlock(message);
				// 複寫errorBtn1 事件
				$("#errorBtn1").click(function(e) {
					$("#formId").validationEngine('detach');
			    	var action = '${__ctx}/INDEX/index';
					$("#formId").attr("action", action);
					$("#formId").submit();
				});
			}else{
				alert(data2.msgCode + "：" + data2.message);
			}
			return false;
			//insert DB wrong
		}
// 		formId.submit();
	 }
}


</script>
</head>
<body>
	<!--交易機制所需畫面-->
	
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 投資屬性評估調查表－（法人版）     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1378" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1378" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formId" action="${__ctx}/FUND/TRANSFER/fund_invest_attr_s" method="post">
						<input type="hidden" name="FDQ1" value="${Q1}"/>
						<input type="hidden" name="FDQ2" value="${Q2}"/>
						<input type="hidden" name="FDQ3" value="${Q3}"/>
						<input type="hidden" name="FDQ4" value="${Q4}"/>
						<input type="hidden" name="FDQ5" value="${Q5}"/>
						<input type="hidden" name="FDQ6" value="${Q6}"/>
						<input type="hidden" name="FDQ7" value="${Q7}"/>
						<input type="hidden" name="FDQ8" value="${Q8}"/>
						<input type="hidden" name="FDQ9" value="${Q9}"/>
						<input type="hidden" name="FDQ10" value="${Q10}"/>
						<input type="hidden" name="FDQ11" value="${Q11}"/>  
						<input type="hidden" name="FDQ12" value="${Q12}"/>   
						<input type="hidden" name="FDQ13" value="${Q13}"/>
						<input type="hidden" name="FDQ14" value="${Q14}"/>  
						<input type="hidden" id="FDINVTYPE" name="FDINVTYPE" />
						<input type="hidden" id="FDSCORE" name="FDSCORE"/>
						<input type="hidden" name="TYPE" value="01"/>
						<input type="hidden" name="UPD_FLG" value="${UPD_FLG}"/>
						<input type="hidden" name="DEGREE" value="${DEGREE}"/>
						<input type="hidden" name="CAREER" value="${SRCFUND}"/>
						<input type="hidden" name="SALARY" value="${SALARY}"/>
						<input type="hidden" name="EDITON" value="10812"/>
						<input type="hidden" name="ANSWER" value="${ANSWER}"/>
						<input type="hidden" name="FDMARK1" value="${MARK1}"/>
						<input type="hidden" id="CMPASSWORD" name="CMPASSWORD"/>
						<input type="hidden" id="PINNEW" name="PINNEW"/>	 
						<input type="hidden" name="RTC" value="${RTC}"/>
						<input type="hidden" name="TXID" value="${RTC}"/>
						<input type="hidden" id="KIND" name="KIND"/>
						<!--交易機制所需欄位-->
						<input type="hidden" id="jsondc" name="jsondc" value="${jsondc}"/>
						<input type="hidden" id="ISSUER" name="ISSUER"/>
						<input type="hidden" id="ACNNO" name="ACNNO"/>
						<input type="hidden" id="TRMID" name="TRMID"/>
						<input type="hidden" id="iSeqNo" name="iSeqNo"/>
						<input type="hidden" id="ICSEQ" name="ICSEQ"/>
						<input type="hidden" id="TAC" name="TAC"/>
						<input type="hidden" id="pkcs7Sign" name="pkcs7Sign"/>
						<input type="hidden" id="AGREE" name="AGREE"/>
						<input type="hidden" id="DEVNAME" name="DEVNAME"/>
						<input type="hidden" id="FDHISTID" name="FDHISTID"/>
						<input type="hidden" id="kyc_insert_uuid" name="kyc_insert_uuid"/>
						
						<div class="main-content-block row">
							<div class="col-12 terms-block questionnaire-block">
								<div class="ttb-message">
									<p><spring:message code="LB.W1378" /></p>
								</div>
								<div class="text-left">
									<p class="form-description">請您再次確認您填答的投資屬性問卷調查</p>
					
									<ol class="list-decimal text-left description-list">
									<p><spring:message code="LB.Description_of_page"/></p>
										<li><b><spring:message code="LB.fund_invest_attr_P2_D1" /></b></li>
										<li><b><spring:message code= "LB.fund_invest_attr_P2_D2" /></b></li>
										<li><b><spring:message code= "LB.fund_invest_attr_P2_D3" /></b></li>
									</ol>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>1﹒<spring:message code="LB.W1325" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q1" disabled/>A﹒<spring:message code="LB.W1326" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q1" disabled/>B﹒<spring:message code="LB.W1327" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q1" disabled/>C﹒<spring:message code="LB.W1328" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q1" disabled/>D﹒<spring:message code="LB.W1329" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>2﹒<spring:message code="LB.W1330" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q2" disabled/>A﹒<spring:message code="LB.W1331" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q2" disabled/>B﹒<spring:message code="LB.W1332" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q2" disabled/>C﹒<spring:message code="LB.W1333" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q2" disabled/>D﹒<spring:message code="LB.W1334" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>3﹒<spring:message code="LB.W1335" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q3" disabled/>A﹒<spring:message code="LB.W1336" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q3" disabled/>B﹒<spring:message code="LB.W1337" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q3" disabled/>C﹒<spring:message code="LB.D0572" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>4﹒<spring:message code="LB.W1339" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q4" disabled/>A﹒<spring:message code="LB.W1340" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q4" disabled/>B﹒<spring:message code="LB.W1341" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q4" disabled/>C﹒<spring:message code="LB.W1342" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>5﹒<spring:message code="LB.W1343" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q5" disabled/>A﹒<spring:message code="LB.W1344" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q5" disabled/>B﹒<spring:message code="LB.W1345" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q5" disabled/>C﹒<spring:message code="LB.W1346" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>6﹒<spring:message code="LB.W1347" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q6" disabled/>A﹒<spring:message code="LB.W1348" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q6" disabled/>B﹒<spring:message code="LB.W1349" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q6" disabled/>C﹒<spring:message code="LB.W1350" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>7﹒<spring:message code="LB.W1351" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="check-block">
														<input type="checkbox" value="A" id="Q71" name="Q71" disabled/>A﹒<spring:message code="LB.W1352" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="B" id="Q72" name="Q72" disabled/>B﹒<spring:message code="LB.W1353" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="C" id="Q73" name="Q73" disabled/>C﹒<spring:message code="LB.W1354" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="D" id="Q74" name="Q74" disabled/>D﹒<spring:message code="LB.W1355" />
														<span class="ttb-check"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>8﹒<spring:message code="LB.W1356" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q8" disabled/>A﹒<spring:message code="LB.W1326" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q8" disabled/>B﹒<spring:message code="LB.W1327" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q8" disabled/>C﹒<spring:message code="LB.W1259" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>9﹒<spring:message code="LB.W1357" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="check-block">
														<input type="checkbox" value="A" id="Q91" name="Q91" disabled/>A﹒<spring:message code="LB.W1358" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="B" id="Q92" name="Q92" disabled/>B﹒<spring:message code="LB.W1359" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="C" id="Q93" name="Q93" disabled/>C﹒<spring:message code="LB.W1360" />
														<span class="ttb-check"></span>
													</label>
													<label class="check-block">
														<input type="checkbox" value="D" id="Q94" name="Q94" disabled/>D﹒<spring:message code="LB.D0930" />
														<span class="ttb-check"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>10﹒<spring:message code="LB.W1362" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q10" disabled/>A﹒<spring:message code="LB.W1363" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q10" disabled/>B﹒<spring:message code="LB.W1364" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q10" disabled/>C﹒<spring:message code="LB.W1365" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>11﹒<spring:message code="LB.W1366" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q11" disabled/>A﹒<spring:message code="LB.W1367" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q11" disabled/>B﹒<spring:message code="LB.W1368" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q11" disabled/>C﹒<spring:message code="LB.W1369" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q11" disabled/>D﹒<spring:message code="LB.W1370" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>12﹒<spring:message code="LB.W1371" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q12" disabled/>A﹒<spring:message code="LB.W1367" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q12" disabled/>B﹒<spring:message code="LB.W1368" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q12" disabled/>C﹒<spring:message code="LB.W1369" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q12" disabled/>D﹒<spring:message code="LB.W1370" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>13﹒<spring:message code="LB.W1372" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q13" disabled/>A﹒<spring:message code="LB.W1373" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q13" disabled/>B﹒<spring:message code="LB.W1374" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q13" disabled/>C﹒<spring:message code="LB.W1375" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q13" disabled/>D﹒<spring:message code="LB.D1079" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
									<ul class="questionnaire-list">
										<li>
											<div class="questionnaire-title">
												<p>14﹒<spring:message code="LB.W1377" /></p>
											</div>
											<div class="questionnaire-answer ttb-input-item">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q14" disabled/>A﹒<spring:message code="LB.X2600" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q14" disabled/>B﹒<spring:message code="LB.W1353" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q14" disabled/>C﹒<spring:message code="LB.X2601" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="D" name="Q14" disabled/>D﹒<spring:message code="LB.X2602" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div class="ttb-input-block">
									<div class="ttb-input-item row">
										<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Transaction_security_mechanism" />
											</h4>
										</label>
										</span> 
										<span class="input-block">
								        	<div id="SSLDIV" style="display:none">
												<div class="ttb-input">
													<label class="radio-block">
										        		<input type="radio" value="0" name="FGTXWAY" id="CMSSL" />&nbsp;<spring:message code="LB.SSL_password" />&nbsp;
														<span class="ttb-radio"></span>
													</label>
													<input type="password" id="CMPWD" class="text-input validate[required, pwvd[PW, CMPWD], custom[onlyLetterNumber]]" name="CMPWD" maxLength="8" size="8"/><br/>
												</div>
											</div>
											<c:if test = "${XMLCOD == '2' || XMLCOD == '4' || XMLCOD == '5' || XMLCOD == '6'}">
												<div id="IKEYDIV" style="display:none">
													<div class="ttb-input">
														<label class="radio-block">
															<input type="radio" value="1" name="FGTXWAY" id="CMIKEY"/>&nbsp;<spring:message code="LB.Electronic_signature"/>&nbsp;<br/>
															<span class="ttb-radio"></span>
														</label>	
													</div>
												</div>
											</c:if>
											<div id="CARDDIV" style="display:none">
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="2" name="FGTXWAY" id="CMCARD"/>&nbsp;<spring:message code="LB.Financial_debit_card"/>&nbsp;<br/>
														<span class="ttb-radio"></span>
													</label>	
												</div>
											</div>
											<div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
												<label class="radio-block">裝置推播認證(請確您的行動裝置網路連線是否正常，及推播功能是否已開啟)
													<input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
													<span class="ttb-radio"></span>
												</label>		 
											</div>
										</span>
									</div>
									<div class="ttb-input-item row" id="CAPTCHATR" style="display:none">
										<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Captcha"/>
											</h4>
										</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
									        	<input id="capCode" type="text" class="text-input input-width-125" 
									        		name="capCode" maxlength="6" autocomplete="off"/>
									        	<img id="kaptchaImage" class="verification-img" name="kaptchaImage"/>
												<input type="button" class="ttb-sm-btn btn-flat-gray" 
												value="<spring:message code="LB.Regeneration"/>" onclick="changeCode()"/>
											</div>
										</span>
									</div>
								</div>
								<input type="button" id="cancelButton" value="<spring:message code="LB.Re_enter"/>" class="ttb-button btn-flat-gray"/>	
								<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>

						</div>
					</form>
				</div>
			</section>
		</main>
	</div>
	<div id="agreeDialog" title="<spring:message code='LB.D0855' />–( <spring:message code='LB.X1770' /> ) ">
		<div style="text-align:center">
			<table id="careerTable" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
					<tr>
							<c:if test ="${ FDINVTYPE == '1'}">
								<spring:message code='LB.D0944' />:<spring:message code='LB.D0945' />
							</c:if>
							<c:if test ="${ FDINVTYPE == '2'}">
								<spring:message code='LB.D0944' />:<spring:message code='LB.X1766' />
							</c:if>
							<c:if test ="${ FDINVTYPE == '3'}">
								<spring:message code='LB.D0944' />:<spring:message code='LB.X1767' />
							</c:if>
					</tr>
			</table>
			<input type="button" id="CMSUBMIT_Y" value="<spring:message code="LB.X0203"/>" class="ttb-button btn-flat-orange"/>
			<input type="button" id="CMSUBMIT_N" value="<spring:message code="LB.X0204"/>" class="ttb-button btn-flat-orange"/>
		</div>
	</div>
	<%@ include file="../index/footer.jsp"%>
	<script>
		//覆寫 getidgateResult
		function getidgateResult(data) {
			    console.log("data>>", data);
			    if (data.data.status == "00") { //取得驗證成功
			      $("#idgate_title").html("裝置認證成功");
			      //倒數暫停
			      clearInterval(idgatecountdownObjheader);
			      $("#idgate-block").hide();
					var uri = "${__ctx}/FUND/TRANSFER/insert_hist";
					var rdata2 = $("#formId").serializeArray();
					data2 = fstop.getServerDataEx(uri,rdata2,false);
					if(data2 !=null && data2.result == true ){
						$("#FDHISTID").val(data2.data);
						$("#kyc_insert_uuid").val(data2.data.kyc_insert_uuid);
						$("#agreeDialog").dialog("open");
					}else{
						if(data2.msgCode != null && data2.msgCode == "FE0019"){
							var message = data2.message;
							callErrorBlock(message);
							// 複寫errorBtn1 事件
							$("#errorBtn1").click(function(e) {
								$("#formId").validationEngine('detach');
						    	var action = '${__ctx}/INDEX/index';
								$("#formId").attr("action", action);
								$("#formId").submit();
							});
						}else{
							alert(data2.msgCode + "：" + data2.message);
						}
						//insert DB wrong
					}
			      //console.log(">>>submit");
			      //轉址
			      //$("#formId").submit();
			    } else if (data.data.status == "03"){
			    	$("#idgate_title").html("裝置認證已取消本次交易");
			    	idgateValidateFailFlag=true;
			        //倒數暫停
			        clearInterval(idgatecountdownObjheader);
			    }
			  }
	</script>	
</body>
</html>