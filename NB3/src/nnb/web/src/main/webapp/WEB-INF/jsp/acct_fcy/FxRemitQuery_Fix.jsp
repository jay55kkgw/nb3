<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>


	<script type="text/javascript">
		//Client Cross Frame Scripting Attack
		if (top != self) top.location = encodeURI(self.location)

		$(document).ready(function () {
			init();
		});

		function init() {
			initDataTable(); // 將.table變更為DataTable
			setTimeout("dodatataable()",200);
		}
		
		function dodatataable(){
			var pre="上一頁";
			var next="下一頁";
			if(dt_locale == "zh_CN")
			{
				pre = "上一页";
				next = "下一页";
			}
			else if(dt_locale == "en")
			{
				pre="Previous";
				next="Next";
			}
			jQuery(function($) {
				$('.FxRemitQuery-table').DataTable({
					"columnDefs": [
		            	{ width: "60px", targets: [0]},
						{ width: "110px", targets: [1]}, 
		            ],
		            lengthChange: false,
		            scrollX: true, //水平滾動
		            sScrollX: "99%",
					bPaginate: true, //啟用或禁用分頁
					bFilter: false, //啟用或禁用數據過濾
					bDestroy: true, //如果沒有表與選擇器匹配，則將按照常規構造新的DataTable
					bSort: false, //啟用或禁用列排序
					info: false, //功能控製表信息顯示字段。
					scrollCollapse: true, 
					language:{
						"paginate":{
							"previous":pre,
							"next":next
						}
					}
				});
			});
		}
		
		function fillData(id, item) 
		{
			
			var obj_srcfund = self.opener.document.getElementById('SRCFUND');
			if (obj_srcfund != null){
				obj_srcfund.value = id;
			}
			////匯款分類項目
			var obj_srcfunddesc = self.opener.document.getElementById('SRCFUNDDESC');
			var show_srcfunddesc = self.opener.document.getElementById('SHOW_SRCFUNDDESC');
			if (obj_srcfunddesc != null) {
				obj_srcfunddesc.value = id + '-' + item;
			}
			if(show_srcfunddesc !=null){
				show_srcfunddesc.innerHTML = id + '-' + item;
			}
			//匯款分類說明				
			var obj_desc = self.opener.document.getElementById('FXRMTDESC');
			var show_desc = self.opener.document.getElementById('SHOW_FXRMTDESC');
			if (obj_desc != null) {
				obj_desc.value = desc;
			}
			if(show_desc!==null){
				show_desc.innerHTML = desc;
			}
			self.close();
		}	
			
	</script>
	<title>
		<spring:message code="LB.Title" />
	</title>
</head>

<body>
	<main class="col-12">
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<form method="post" id="formId" action="">
				<h2><spring:message code= "LB.X1467" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<ul class="ttb-result-list"></ul>
						<table class="stripe table-striped ttb-table FxRemitQuery-table" data-toggle-column="first">
							<thead>
								<tr>
									<!-- 分類編號 -->
									<th ><spring:message code= "LB.X1567" /></th>
									<!-- 項目 -->
									<th ><spring:message code= "LB.D0271" /></th>
									<!-- 說明 -->
									<th ><spring:message code= "LB.Description_of_page" /></th>
								</tr>
							</thead>
							<tbody>
<%-- 								<tr> --%>
<%-- 									<td style="color: #007bff"><a href="self" onclick="fillData('A', '<spring:message code= "LB.D0462" />');">A</a></td> --%>
<%-- 									<td><spring:message code= "LB.D0462" /></td>		  														 --%>
<%-- 									<td><spring:message code= "LB.D0462" /></td>	         --%>
<%-- 								</tr>	 --%>
	<%-- 							<tr > --%>
	<%-- 								<td><a href="self" onclick="fillData('B', '國內銀行短期外幣貸款撥款及償還');">B</a></td> --%>
	<%-- 								<td>國內銀行短期外幣貸款撥款及償還</td>	  														 --%>
	<%-- 								<td>國內銀行短期外幣貸款撥款及償還</td>	          --%>
	<%-- 							</tr>	 --%>
	<%-- 							<tr> --%>
	<%-- 								<td><a href="self" onclick="fillData('C', '外匯交易保證金之提存或撥還');">C</a></td> --%>
	<%-- 								<td>外匯交易保證金之提存或撥還</td>	  														 --%>
	<%-- 								<td>外匯交易保證金之提存或撥還</td>	          --%>
	<%-- 							</tr>	 --%>
	<%-- 							<tr> --%>
	<%-- 								<td><a href="self" onclick="fillData('D', '外匯交易已實現損失或盈餘');">D</a></td> --%>
	<%-- 								<td>外匯交易已實現損失或盈餘</td>	  														 --%>
	<%-- 								<td>外匯交易已實現損失或盈餘</td>	           --%>
	<%-- 							</tr>	 --%>
	<%-- 							  <tr> --%>
	<%-- 								<td><a href="self" onclick="fillData('E', '外幣貸款利息收支');">E</a></td> --%>
	<%-- 								<td>外幣貸款利息收支</td>	  														 --%>
	<%-- 								<td>外幣貸款利息收支</td>	           --%>
	<%-- 							  </tr>	 --%>
	<%-- 							  <tr> --%>
	<%-- 								<td><a href="self" onclick="fillData('F', '國內銀行中長期外幣貸款撥款及償還');">F</a></td> --%>
	<%-- 								<td>國內銀行中長期外幣貸款撥款及償還</td>		  														 --%>
	<%-- 								<td>國內銀行中長期外幣貸款撥款及償還</td>	         --%>
	<%-- 							  </tr>	 --%>
	<%-- 							  <tr> --%>
	<%-- 								<td><a href="self" onclick="fillData('G', '評價產生之未實現損益');">G</a></td> --%>
	<%-- 								<td>評價產生之未實現損益</td>		  														 --%>
	<%-- 								<td>評價產生之未實現損益</td>	         --%>
	<%-- 							  </tr>		   --%>
								  <tr>
									<td style="color: #007bff"><a href="self" onclick="fillData('R', '<spring:message code= "LB.D0463_R" />');">R</a></td>
									<td><spring:message code= "LB.D0463_R" /></td>	  														
									<td><spring:message code= "LB.D0463_R" /></td>	          
								  </tr>		  
								  <tr>
									<td style="color: #007bff"><a href="self" onclick="fillData('S', '<spring:message code= "LB.D0463_S" />');">S</a></td>
									<td><spring:message code= "LB.D0463_S" /></td>	  														
									<td><spring:message code= "LB.D0463_S" /></td>	          
								  </tr>		  
								  <tr>
									<td style="color: #007bff"><a href="self" onclick="fillData('T', '<spring:message code= "LB.D0463_T" />');">T</a></td>
									<td><spring:message code= "LB.D0463_T" /></td>	  														
									<td><spring:message code= "LB.D0463_T" /></td>	          
								  </tr>		  
								  <tr>
									<td style="color: #007bff"><a href="self" onclick="fillData('L', '<spring:message code= "LB.D0463_L" />');">L</a></td>
									<td><spring:message code= "LB.D0463_L" /></td>	  														
									<td><spring:message code= "LB.D0463_L" /></td>	          
								  </tr>		  
								  <tr>
									<td style="color: #007bff"><a href="self" onclick="fillData('P', '<spring:message code= "LB.D0463_P" />');">P</a></td>
									<td><spring:message code= "LB.D0463_P" /></td>	  														
									<td><spring:message code= "LB.D0463_P" /></td>	          
								  </tr>		  
								  <tr>
									<td style="color: #007bff"><a href="self" onclick="fillData('SPC', '<spring:message code= "LB.D0463" />');">SPC</a></td>
									<td><spring:message code= "LB.D0463" /></td>	  														
									<td><spring:message code= "LB.D0463" /></td>	          
								  </tr>		  	  
	<%-- 							  <tr> --%>
	<%-- 								<td><a href="self" onclick="fillData('X', '新台幣與外幣間換匯交易（SWAP）');">X</a></td> --%>
	<%-- 								<td>新台幣與外幣間換匯交易（SWAP）</td>	  														 --%>
	<%-- 								<td>新台幣與外幣間換匯交易（SWAP）</td>	           --%>
	<%-- 							  </tr>	 --%>
	<%-- 							  <tr> --%>
	<%-- 								<td><a href="self" onclick="fillData('Y', '新台幣與外幣間換匯換利交易（CCS）');">Y</a></td> --%>
	<%-- 								<td>新台幣與外幣間換匯換利交易（CCS）</td>	  														 --%>
	<%-- 								<td>新台幣與外幣間換匯換利交易（CCS）</td>	           --%>
	<%-- 							  </tr>	 --%>
	<%-- 							  <tr> --%>
	<%-- 								<td><a href="self" onclick="fillData('Z', '其他');">Z</a></td> --%>
	<%-- 								<td>其他</td>	  														 --%>
	<%-- 								<td>其他</td>	          --%>
	<%-- 							  </tr>	 --%>
								</tbody>
						</table>
					</div>
				</div>
			</form>
		</section>
	</main> <!-- 		main-content END -->
</body>

</html>