<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
	<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
		<br/><br/>
		<div style="text-align:center">
			<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
		</div>
		<br/><br/><br/>
		<div style="text-align:center">
			<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
		</div>
		
		<table class="print">
			<tr>
				<th colspan="4" style="text-align:center"><spring:message code= "LB.D0105" /></th>
			</tr>
			<tr>
				<td><spring:message code= "LB.D0106" /></td>
				<td>${CFU2}</td>
				<td><spring:message code= "LB.D0108" /></td>
				<td>
					${cardType1}<br>
					${cardType2}<br>
					${cardType3}
				</td>
			</tr>
			<tr>
				<th colspan="4" style="text-align:center"><spring:message code= "LB.D0109" /></th>
			</tr>
			<tr>
			<tr>
				<td><font color=red>＊</font><spring:message code= "LB.D0049" /></td>
				<td>${CPRIMCHNAME}</td>
				<td>
					<spring:message code= "LB.D0050" /><br>
					(<spring:message code= "LB.D0138" />)
				</td>
				<td>${CPRIMENGNAME}</td>
			</tr>
			<tr>
				<td><font color=red>＊</font><spring:message code= "LB.D0054" /></td>
				<td>${CPRIMBIRTHDAY}</td>
				<td><spring:message code= "LB.D0055" /></td>
				<td>${MPRIMEDUCATION}</td>
			</tr>
			<tr>
				<!-- 身分證號碼 -->
				<td><font color=red>＊</font><spring:message code="LB.Id_no"/></td>
				<td>${CPRIMID}</td>
				<td><spring:message code= "LB.D0057" /></td>
				<td>${MPRIMMARRIAGE}</td>
			</tr>
			<tr>
				<td><spring:message code= "LB.D0143" /></td>
				<td colspan="3">${CPRIMADDR2}</td>
			</tr>		
			<tr>
				<td><spring:message code= "LB.D0144" /></td>
				<td colspan="3">${CPRIMHOMETELNO2A}${CPRIMHOMETELNO2B}</td>
			</tr>
			<tr>
				<td><spring:message code= "LB.D0063" /></td>
				<td colspan="3">${CPRIMADDR}</td>
			</tr>							
			<tr>
				<td><spring:message code= "LB.D0065" /></td>
				<td colspan="3" >${CPRIMHOMETELNO1A}${CPRIMHOMETELNO1B}</td>
			</tr>
			<tr>
				<td><spring:message code= "LB.D0067" /></td>
				<td colspan="3">${MPRIMADDR1COND}</td>
			</tr>
			<tr>
				<td><font color=red>＊</font><spring:message code= "LB.D0069" /></td>
				<td colspan="3">${CPRIMCELLULANO1}</td>
			</tr>
			<tr>
				<td><font color=red>＊</font><spring:message code= "LB.D0149" /></td>
				<td colspan="3">${MBILLTO}</td>
			</tr>
			<tr>
				<td><font color=red>＊</font><spring:message code= "LB.D0071" /></td>
				<td colspan="3">${MCARDTO}</td>
			</tr>
			<tr>
				<!-- 電子信箱 -->
				<td ><font color=red>＊</font><spring:message code="LB.Email"/></td>
				<td colspan="3">${CPRIMEMAIL}<br>
				<c:choose>
	      				<c:when test="${VARSTR3 eq '11'}">
	      					 <input type="checkbox" checked name="V3" disabled>
	      				</c:when>
	      				<c:otherwise>
	      					 <input type="checkbox" name="V3" disabled>
	      				</c:otherwise>
      				</c:choose>
					<spring:message code= "LB.D0152" /><br>
					(<spring:message code= "LB.D0153" />)
				</td>
			</tr>
			<tr>
				<td colspan="4" style="text-align:center"><spring:message code= "LB.D0079" /></td>
			</tr>
			<tr>
				<td><spring:message code= "LB.D0155" /></td>
				<td colspan="3">
					<c:choose>
		      			<c:when test="${CPRIMJOBTYPE eq '1'}">
							<c:set var="CJT_1" value="checked" />
		      			</c:when>
		      			<c:when test="${CPRIMJOBTYPE eq '2'}">
							<c:set var="CJT_2" value="checked" />
		      			</c:when>
		      			<c:when test="${CPRIMJOBTYPE eq '3'}">
							<c:set var="CJT_3" value="checked" />
		      			</c:when>
		      			<c:when test="${CPRIMJOBTYPE eq '4'}">
							<c:set var="CJT_4" value="checked" />
		      			</c:when>
      				</c:choose>
    				<input type="checkbox" name="CJT" ${CJT_1}><spring:message code= "LB.D0081" />
					<input type="checkbox" name="CJT" ${CJT_2}><spring:message code= "LB.D0082" />
					<input type="checkbox" name="CJT" ${CJT_3}><spring:message code= "LB.D0083" />
     				<input type="checkbox" name="CJT" ${CJT_4}><spring:message code= "LB.D0084" />
				</td>
			</tr>
			<tr>
				<td><spring:message code= "LB.D0086" /></td>
				<td >${CPRIMCOMPANY}</td>
				<td><spring:message code= "LB.D0087" /></td>
				<td>${CPRIMJOBTITLE}</td>
			</tr>
			<tr>
				<td><spring:message code= "LB.D0088_1" /></td>
				<td>${CPRIMSALARY} <spring:message code= "LB.D0088_2" /></td>
				<td><spring:message code= "LB.D0089_1" /></td>
				<td>${WORKYEARS} ${WORKMONTHS}</td>
			</tr>
			<tr>
				<td><spring:message code= "LB.D0090" /></td>
				<td colspan="3">${CPRIMADDR3}</td>
			</tr>
			<tr>
				<td><spring:message code= "LB.D0094" /></td>
				<td colspan="3">${CPRIMOFFICETELNO1A}-${CPRIMOFFICETELNO1B} <spring:message code= "LB.D0095" />:${CPRIMOFFICETELNO1C}</td>
			</tr>
		</table>
		<br/><br/>
		<div class="text-left">
			<ol class="list-decimal text-left">
				<li>
					<c:choose>
		      			<c:when test="${CNOTE2 eq '1'}">
							<c:set var="c1_yes" value="checked" />
		      			</c:when>
		      			<c:when test="${CNOTE2 eq '2'}">
							<c:set var="c1_no" value="checked" />
		      			</c:when>
      				</c:choose>
					<spring:message code= "LB.D0096" />
      				<input type="checkbox" name="C1" disabled ${c1_yes}><spring:message code= "LB.D0097" />
					<input type="checkbox" name="C1" disabled ${c1_no}><spring:message code= "LB.X0204" />　<spring:message code= "LB.X1679" />
					<font color=red>(<spring:message code= "LB.X1680" />)</font>
				</li>
				<li>
					<c:choose>
		      			<c:when test="${MCASH eq '1'}">
							<c:set var="c2_yes" value="checked" />
		      			</c:when>
		      			<c:when test="${MCASH eq '2'}">
							<c:set var="c2_no" value="checked" />
		      			</c:when>
      				</c:choose>
					<spring:message code= "LB.D0096" />
      				<input type="checkbox" name="C2" disabled ${c2_yes}><spring:message code= "LB.D0097" />
					<input type="checkbox" name="C2" disabled ${c2_no}><spring:message code= "LB.X0204" />　<spring:message code= "LB.X0590" />
					<font color=red>（<spring:message code= "LB.X0591" />）</font>
				</li>
				<li>
					<c:choose>
		      			<c:when test="${CNOTE1 eq '1'}">
							<c:set var="c3_yes" value="checked" />
		      			</c:when>
		      			<c:when test="${CNOTE1 eq '2'}">
							<c:set var="c3_no" value="checked" />
		      			</c:when>
      				</c:choose>
					<spring:message code= "LB.D0096" />
      				<input type="checkbox" name="C3" disabled ${c3_yes}><spring:message code= "LB.D0097" />
					<input type="checkbox" name="C3" disabled ${c3_no}><spring:message code= "LB.X0204" />　<spring:message code= "LB.X1681" />
					<font color=red>(<spring:message code= "LB.X1682" />)</font>
				</li>
				<li>
					<c:choose>
		      			<c:when test="${CNOTE3 eq '1'}">
							<c:set var="c4_yes" value="checked" />
		      			</c:when>
		      			<c:when test="${CNOTE3 eq '2'}">
							<c:set var="c4_no" value="checked" />
		      			</c:when>
      				</c:choose>
					<spring:message code= "LB.D0096" />
      				<input type="checkbox" name="C4" disabled ${c4_yes}><spring:message code= "LB.D0097" />
					<input type="checkbox" name="C4" disabled ${c4_no}><spring:message code= "LB.X0204" />　<spring:message code= "LB.X1683" />
				</li>
			</ol>
		</div>
		<br/><br/><br/><br/>
	</body>
</html>