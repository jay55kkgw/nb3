<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>

	<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
	<title>Insert title here</title>

</head>

<body>
	<div>
		<!-- ${ menuFilterList.data.sort_menu[0].SEED_LAYER } -->
		<fmt:setLocale value="${__i18n_locale}" />
		<!-- 直接抓table，如menu.jsp功能選單 -->
		<tr>
			<td>
				<ul>
					<li>
						<spring:message code="LB.Account_Overview" />
						<!-- 帳戶總覽 -->
						<div>
							<a href="#main-fuct-2" onclick="fstop.getPage('${__ctx}/OVERVIEW/initview','','')">
								<spring:message code="LB.Account_Overview" />
							</a>
						</div>
						<!-- 我的首頁 -->
						<div>
							<a href="#main-fuct-1" onclick="fstop.getPage('${__ctx}'+'/INDEX/index','', '')">
								<spring:message code="LB.My_home" />
							</a>
						</div>
					</li>
				</ul>
			</td>

			<td>
				<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu[0].SEED_LAYER }">
					<div>
						<ul>
							<li>
								<c:if test="${ dataListLayer1.ADOPALIVE.equals('1') && dataListLayer1.ISANONYMOUS.equals('0') && !dataListLayer1.DPACCSETID.equals('1100') }">

									<c:if test="${__i18n_locale eq 'en' }">
										${dataListLayer1.ADOPENGNAME}
									</c:if>
									<c:if test="${__i18n_locale eq 'zh_TW' }">
										${dataListLayer1.ADOPNAME}
									</c:if>
									<c:if test="${__i18n_locale eq 'zh_CN' }">
										${dataListLayer1.ADOPCHSNAME}
									</c:if>
								</c:if>
							</li>
							<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }">
								<c:if test="${ dataListLayer2.ADOPALIVE.equals('1') && dataListLayer2.ISANONYMOUS.equals('0') }">
									<li>
										<ul>
											<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }">
												<c:if test="${ dataListLayer3.ADOPALIVE.equals('1') && dataListLayer3.ISANONYMOUS.equals('0') }">
													<li>
														<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
															<a href='#' onclick="fstop.getPage('${__ctx}'+'${dataListLayer3.URL}','', '')">
																<c:if test="${__i18n_locale eq 'en' }">
																	${dataListLayer3.ADOPENGNAME}
																</c:if>
																<c:if test="${__i18n_locale eq 'zh_TW' }">
																	${dataListLayer3.ADOPNAME}
																</c:if>
																<c:if test="${__i18n_locale eq 'zh_CN' }">
																	${dataListLayer3.ADOPCHSNAME}
																</c:if>
															</a>
														</c:if>
													</li>
												</c:if>
											</c:forEach>
										</ul>
									</li>
								</c:if>
							</c:forEach>
						</ul>
					</div>
				</c:forEach>
			</td>
		</tr>
	</div>

</body>

</html>