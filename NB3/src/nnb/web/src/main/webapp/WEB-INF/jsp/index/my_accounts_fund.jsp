<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<c:set var="SHWD" value=""></c:set>
<%@ include file="../fund/fund_shwd.jsp"%>
<!-- 基金資產總額 -->
<div class="account-left tab-pane fade" id="FUND" role="tabpanel" aria-labelledby="tab-FUND">
    <div class="main-account">
<!--         <span class="title"><spring:message code='LB.X2315'/><span class="description">(<spring:message code='LB.X2318'/>)</span></span> -->
        <span class="title"><spring:message code="LB.W0901" /></span>
        <span id="tot_ref" class="content"></span>
    </div>
    <div class="account-item">
        <div class="account-item-show">
<!--             <span><spring:message code='LB.X2316'/></span> -->
            <span><spring:message code="LB.X2389" /></span>
            <span id="fund_qry_tw"><a href="#" onclick="fstop.getPage('/nb3'+'/FUND/QUERY/profitloss_balance','', '')" id="twdref"></a></span>
            <span id="fund_pay_tw"><a href="#" onclick="fstop.getPage('/nb3'+'/FUND/PURCHASE/fund_purchase_select','', '')"><spring:message code="LB.W1065" /></a></span>
        </div>
        <div class="account-item-hide">
            <span><spring:message code="LB.W0933"/></span>
            <span id="twdamt"></span>
            <span></span>
        </div>
    </div>
    <div class="account-item" id="fyc_fund_qry">
        <div class="account-item-show">
<!--             <span><spring:message code='LB.X2317'/></span> -->
            <span><spring:message code="LB.X2390" /></span>
            <span id="fund_qry_fx"><a href="#" onclick="fstop.getPage('/nb3'+'/FUND/QUERY/profitloss_balance','', '')" id="fcyref"></a></span>
            <span id="fund_pay_fx"><a href="#" onclick="fstop.getPage('/nb3'+'/FUND/PURCHASE/fund_purchase_select','', '')"><spring:message code="LB.W1065" /></a></span>
        </div>
<!--         <div class="account-item-hide"> -->
<!--             <span><spring:message code="LB.W0933"/></span> -->
<!--             <span id="fcyatm"></span> -->
<!--             <span></span> -->
<!--         </div> -->
    </div>
    <div class="account-item">
        <div class="account-item-show">
        </div>
        <div class="account-item-hide">
        </div>
    </div>
    
    <div class="main-account">
        <span class="title"><spring:message code="LB.X2391" /></span>
        <span id="tot_bal" class="content"></span>
    </div>
    <div class="account-item" id="balance_qry">
        <div class="account-item-show">
            <span><spring:message code="LB.X2391" /></span>
        </div>
    </div>
</div>
        

<script type="text/JavaScript">

function getFUND_aj() {
	// 先清空資料初始化
	$("#tot_ref").empty();
	$("#twdref").empty();
	$("#twdamt").empty();
	$("#fcyref").empty();
// 	$("#fcyatm").empty();
	$("#tot_bal").empty();
	$("#assets_fund").empty();
	$("#balance_qry div").remove(".account-item-hide");
	$("#fyc_fund_qry div").remove(".account-item-hide");
	$("#tot_ref").append("<span class='content' onclick='showEyeClick()'><img src='${__ctx}/img/eye-solid-orange.svg' /></span>");
	
	
	console.log("getFUND_aj.now: " + new Date());
	
	uri = '${__ctx}' + "/INDEX/summary_FUND";

	fstop.getServerDataEx(uri, null, true, showFUND);
}

function showFUND(data) {
	// 避免打很多次回來同時顯示多次的資料，故再次清空資料
	$("#tot_ref").empty();
	$("#twdref").empty();
	$("#twdamt").empty();
	$("#fcyref").empty();
// 	$("#fcyatm").empty();
	$("#tot_bal").empty();
	$("#assets_fund").empty();
	
	console.log("showFUND.now: " + new Date());
	
// 	console.log("showFUND.summary_FUND_result: " + data.data.summary_FUND_result);
	if (data.data.summary_FUND_result != 'error' && data.data.summary_FUND_result != 'null' && data.data.summary_FUND_result != null) {
		
		$("#fund_qry_tw").show();
		$("#fund_pay_tw").show();
		$("#fund_qry_fx").show();
		$("#fund_pay_fx").show();
		
		SHWD = JSON.parse(data.data.summary_FUND_result).SHWD;
		console.log("SHWD: " + SHWD);
		shwd_prompt_init(false);
	// 	var tot_twdamt, 
		var tot_refvalue, twdatm, twdref, fcyatm, fcyref, secondRows;
	
		JSON.parse(data.data.summary_FUND_result, function(k, v) {
	// 		console.log("k= " + k + ", v=" + v);
			// 
	// 		if (k == 'TOTTWDAMTFormat') {
	// 			tot_twdamt = v;
	// 		}
			if (k == 'TOTREFVALUEFormat') {
				tot_refvalue = v;
			}
			if (k == 'NTDSUBTOTAMTFormat') {
				twdatm = v;
			}
			if (k == 'NTDSUBREFVALUEFormat') {
				twdref = v;
			}
			if (k == 'FCYSUBTOTAMTEFormat') {
				fcyatm = v;
			}
			if (k == 'FCYSUBREFVALUEFormat') {
				fcyref = v;
			}
		});
		secondRows = JSON.parse(data.data.summary_FUND_result).secondRows;
		console.log(secondRows);
	// 	$("#tot_twdamt").prepend( tot_twdamt + "元");
	
		if (typeof tot_refvalue !== 'undefined') {
// 			$("#tot_ref").prepend( "<span class='accountST' account='" + tot_refvalue + "'>" + coverSoA(tot_refvalue) + " <spring:message code='LB.D0509'/>");
		}
		
		if (typeof twdref !== 'undefined' && typeof twdatm !== 'undefined') {
			$("#twdref").prepend( "<div class='accountST' account='" + twdref + "'>" + coverSoA(twdref)  + " <spring:message code='LB.D0509'/></div>");
			$("#twdamt").prepend( "<span class='accountST' account='" + twdatm + "'>" + coverSoA(twdatm)  + " <spring:message code='LB.D0509'/></span>");
			
		} else {
			// 臺幣基金API沒給資料也塞0.00
// 			$("#twdref").prepend("<spring:message code='LB.D0017'/>");
			$("#twdref").prepend("<div class='accountST' account='" + "0.00" + "'>" + coverSoA("0.00") + " <spring:message code='LB.D0509'/></div>");
			$("#twdamt").prepend("<span class='accountST' account='" + "0.00" + "'>" + coverSoA("0.00") + " <spring:message code='LB.D0509'/></span>");
		}
		
// 		if (typeof fcyref !== 'undefined' && typeof fcyatm !== 'undefined') {
// 			$("#fcyref").prepend( "<span class='accountST' account='" + fcyref + "'>" + coverSoA(fcyref)  + " <spring:message code='LB.D0509'/>");
// 			$("#fcyatm").prepend( "<span class='accountST' account='" + fcyatm + "'>" + coverSoA(fcyatm)  + " <spring:message code='LB.D0509'/>");
// 		} else {
// 			$("#fcyref").prepend("<spring:message code='LB.D0017'/>");
// 		}
		
	    var trHTML  = '';
		if(secondRows != null && secondRows.length > 0){
		    $.each(secondRows, function (i, item) {
	            trHTML += '<div class="account-item-hide">' +
			    		  	'<span>'+ item.SUBCRY + ' ' + item.ADCCYNAME + '<spring:message code="LB.W0934" /></span>' +
			    		  	'<span class="accountST" account="' + item.SUBREFVALUEFormat + '">'+ coverSoA(item.SUBREFVALUEFormat) +'</span>' +
			    		  	'<span></span>' +
			    		  '</div>'
			    trHTML += '<div class="account-item-hide">' +
			    		  	'<span>'+ item.SUBCRY + ' ' + item.ADCCYNAME + '<spring:message code="LB.W0933" /></span>' +
			    		  	'<span class="accountST" account="' + item.SUBTOTAMTFormat + '">'+ coverSoA(item.SUBTOTAMTFormat) +'</span>' +
			    		  	'<span></span>' +
			    		  '</div>'
		    });
		    console.log("summary_BLA.trHTML: " + trHTML);
		    $('#fyc_fund_qry').append(trHTML);
		}else{
			$("#fcyref").prepend("<spring:message code='LB.D0017'/>");
		}
		
	} else {
		console.log("showFUND.error...");
		$("#fund_qry_tw").hide();
		$("#fund_pay_tw").hide();
		$("#fund_qry_fx").hide();
		$("#fund_pay_fx").hide();
		$("#tot_ref").prepend("<spring:message code='LB.D0017'/>");
	}
	if (data.data.summary_BLA_result != 'error' && data.data.summary_BLA_result != 'null' && data.data.summary_BLA_result != null) {
		console.log(JSON.parse(data.data.summary_BLA_result));
		var summary_BLA_result =  JSON.parse(data.data.summary_BLA_result);
		if(summary_BLA_result.msgCode == '0'){
			var REC =  summary_BLA_result.REC;
			$("#balance_qry").show();

		    var trHTML  = '';
		    $.each(REC, function (i, item) {
		    		// 資料總數
		//     		balance_account_fx += parseInt(item.COUNT); 
		    		// 總計金額  
		//     		 + item  
				if(item.O18 == '2'){
		            trHTML += '<div class="account-item-hide">' +
		            		  	'<span>'+ item.O08OLD + ' ' + item.O08 + '<spring:message code="LB.W0934" /></span>' +
		            		  	'<span class="accountST" account="' + item.O12 + '">'+ coverSoA(item.O12) +'</span>' +
		            		  	'<span></span>' +
		            		  '</div>'
		            trHTML += '<div class="account-item-hide">' +
			        		  	'<span>'+ item.O08OLD + ' ' + item.O08 + '<spring:message code="LB.W0933" /></span>' +
			        		  	'<span class="accountST" account="' + item.O07 + '">'+ coverSoA(item.O07) +'</span>' +
			        		  	'<span></span>' +
			        		  '</div>'
				}
		    });
		    console.log("summary_BLA.trHTML: " + trHTML);
		    $('#balance_qry').append(trHTML);
		}else {
			console.log("showBLA.error...");
			$("#balance_qry").hide();
			$("#tot_bal").prepend("<spring:message code='LB.D0017'/>");
		}
		
	} else {
		console.log("showBLA.error...");
		$("#balance_qry").hide();
		$("#tot_bal").prepend("<spring:message code='LB.D0017'/>");
	}
	
	$("#tot_ref").append("<span class='content' onclick='showEyeClick()'><img src='${__ctx}/img/eye-solid-orange.svg' /></span>");
	
	// 解遮罩
	unAcctsBlock('FUND');
}

</script>