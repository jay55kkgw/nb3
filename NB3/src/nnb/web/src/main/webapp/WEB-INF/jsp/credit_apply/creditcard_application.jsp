<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:if test="${not empty BaseResult }">
	<c:set var="bsData" value="${BaseResult.data}"></c:set>
</c:if>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>



<script type="text/javascript">

	$(document).ready(function(){
		
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()",10);
		// 開始查詢資料並完成畫面
		setTimeout("init()",20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)",500);
	
		
		$("#pdfbtn").click(function(){
			$("#formId").attr("action","${__ctx}/CREDIT/APPLY/downloadPDF");
			$("#formId").submit();
		});
	});
	//選項
 	function formReset(){
		if($("#actionBar").val()=="pdf"){
			$("#formId").attr("action","${__ctx}/CREDIT/APPLY/downloadPDF");
			$("#formId").submit();
 		}
		else if ($("#actionBar").val()=="NA023"){
			window.open("${__ctx}/term/NA02_3.pdf");
 		}
		else if ($("#actionBar").val()=="NA024"){
			window.open("${__ctx}/term/NA02_4.pdf");
 		}
		else if ($("#actionBar").val()=="NA025"){
			window.open("${__ctx}/term/NA02_5.pdf");
 		}
	}
	function init(){
		
		$("#printbtn").click(function(){
			var params = {
					jspTemplateName: 	"creditcard_application_print",
					jspTitle: 			"<spring:message code= "LB.X1649" />",
					CMQTIME: 			"${bsData.CMQTIME}",
					COUNT: 				"${bsData.CMRECNUM}",
					CFU2: 				"${bsData.CFU2}",
					cardType1: 			"${bsData.cardType1}",
					cardType2: 			"${bsData.cardType2}",
					cardType3: 			"${bsData.cardType3}",
					CPRIMCHNAME: 		"${bsData.PO.CPRIMCHNAME}",
					CPRIMENGNAME: 		"${bsData.PO.CPRIMENGNAME}",
					CPRIMBIRTHDAY: 		"${bsData.PO.CPRIMBIRTHDAY}",
 					MPRIMEDUCATION: 	"${bsData.MPRIMEDUCATION}",
 					CPRIMID: 			"${bsData.PO.CPRIMID}",
 					MPRIMMARRIAGE: 		"${bsData.MPRIMMARRIAGE}",
 					CPRIMADDR2: 		"${bsData.PO.CPRIMADDR2}",
 					CPRIMHOMETELNO2A: 	"${bsData.CPRIMHOMETELNO2A}",
 					CPRIMHOMETELNO2B: 	"${bsData.CPRIMHOMETELNO2B}",
 					CPRIMADDR: 			"${bsData.PO.CPRIMADDR}",
 					CPRIMHOMETELNO1A: 	"${bsData.CPRIMHOMETELNO1A}",
 					CPRIMHOMETELNO1B: 	"${bsData.CPRIMHOMETELNO1B}",
 					MPRIMADDR1COND: 	"${bsData.MPRIMADDR1COND}",
 					CPRIMCELLULANO1: 	"${bsData.PO.CPRIMCELLULANO1}",
 					MBILLTO: 			"${bsData.MBILLTO}",
 					MCARDTO: 			"${bsData.MCARDTO}",
 					CPRIMEMAIL: 		"${bsData.PO.CPRIMEMAIL}",
 					VARSTR3: 			"${bsData.PO.VARSTR3}",
 					CPRIMJOBTYPE: 		"${bsData.PO.CPRIMJOBTYPE}",
 					CPRIMCOMPANY: 		"${bsData.PO.CPRIMCOMPANY}",
 					CPRIMJOBTITLE: 		"${bsData.CPRIMJOBTITLE}",
 					CPRIMSALARY: 		"${bsData.PO.CPRIMSALARY}",
 					WORKYEARS: 			"${bsData.WORKYEARS}",
 					WORKMONTHS: 		"${bsData.WORKMONTHS}",
 					CPRIMADDR3: 		"${bsData.PO.CPRIMADDR3}",
 					CPRIMOFFICETELNO1A: "${bsData.PO.CPRIMOFFICETELNO1A}",
 					CPRIMOFFICETELNO1B: "${bsData.PO.CPRIMOFFICETELNO1B}",
 					CPRIMOFFICETELNO1C: "${bsData.PO.CPRIMOFFICETELNO1C}",
 					CNOTE2: 			"${bsData.PO.CNOTE2}",
 					MCASH: 				"${bsData.PO.MCASH}",
 					CNOTE1: 			"${bsData.PO.CNOTE1}",
 					CNOTE3: 			"${bsData.PO.CNOTE3}",
				};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

		});
		
	}
	
</script>
</head>
	<body>
		<!-- menu、登出窗格 -->
		<div class="content row">
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code= "LB.X1650" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
						<!-- 下載申請書-->
						<option value="pdf"><spring:message code="LB.X0845" /></option>
						<!-- 下載信用卡約定條款-->
						<option value="NA023"><spring:message code="LB.X0846" /></option>
						<!-- 下載悠遊聯名卡特別約定條款-->
						<c:if test="${bsData.CARDMEMO1 eq '1' || bsData.CARDMEMO2 eq '1' || bsData.CARDMEMO3 eq '1'}">
							<option value="NA024"><spring:message code="LB.X0847" /></option>
						</c:if>
						<!-- 下載一卡通聯名卡特別約定條款-->
						<c:if test="${bsData.CARDMEMO1 eq '2' || bsData.CARDMEMO2 eq '2' || bsData.CARDMEMO3 eq '2'}">
							<option value="NA025"><spring:message code="LB.X0849" /></option>
						</c:if>
					</select>
				</div>
				<form id="formId" method="post">
				
<%-- 					<input type="hidden" name="pdfName" value="<spring:message code= "LB.X1651" />"/> --%>
					<input type="hidden" name="templatePath" value="/pdfTemplate/Creditcard_Apply.html"/>
					<input type="hidden" name="logoPath" value="/img/TBBLogo.gif"/>
					<div class="main-content-block row">
						<div class="col-12">
							<ul class="ttb-result-list">
							</ul>
							<!-- 申請信用卡書閱覽 -->
							<table class="table table-striped" data-toggle-column="first" >
							<tbody>		
									<tr>
										<td colspan="4" class="text-center" style="background-color: #EEEEEE;font-weight: bold;"><spring:message code= "LB.D0105" /></td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0106" /></td>
										<td class="text-left">${bsData.CFU2}</td>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0108" /></td>
										<td class="text-left">
											${bsData.cardType1}
											<c:if test="${bsData.cardType2 != ''}"><br></c:if>
											${bsData.cardType2}
											<c:if test="${bsData.cardType2 != ''}"><br></c:if>
											${bsData.cardType3}
										</td>
									</tr>
									<tr>
										<td colspan="4" class="text-center" style="background-color: #EEEEEE;font-weight: bold;"><spring:message code= "LB.D0109" /></td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><font color=red>＊</font><spring:message code= "LB.D0049" /></td>
										<td class="text-left">${bsData.PO.CPRIMCHNAME}</td>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0050" /><br>
											(<spring:message code= "LB.D0138" />)
										</td>
										<td class="text-left">${bsData.PO.CPRIMENGNAME}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><font color=red>＊</font><spring:message code= "LB.D0054" /></td>
										<td class="text-left">${bsData.PO.CPRIMBIRTHDAY}</td>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0055" /></td>
										<td class="text-left">${bsData.MPRIMEDUCATION}</td>
									</tr>
									<tr>
										<!-- 身分證號碼 -->
										<td style="background-color: #EEEEEE;"><font color=red>＊</font><spring:message code="LB.Id_no"/></td>
										<td class="text-left">${bsData.PO.CPRIMID}</td>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0057" /></td>
										<td class="text-left">${bsData.MPRIMMARRIAGE}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0143" /></td>
										<td colspan="3" class="text-left">${bsData.PO.CPRIMADDR2}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0144" /></td>
										<td colspan="3" class="text-left">${bsData.CPRIMHOMETELNO2A}${bsData.CPRIMHOMETELNO2B}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0063" /></td>
										<td colspan="3" class="text-left">${bsData.PO.CPRIMADDR}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0065" /></td>
										<td colspan="3" class="text-left">${bsData.CPRIMHOMETELNO1A}${bsData.CPRIMHOMETELNO1B}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0067" /></td>
										<td colspan="3" class="text-left">${bsData.MPRIMADDR1COND}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><font color=red>＊</font><spring:message code= "LB.D0069" /></td>
										<td colspan="3" class="text-left">${bsData.PO.CPRIMCELLULANO1}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><font color=red>＊</font><spring:message code= "LB.D0149" /></td>
										<td colspan="3" class="text-left">${bsData.MBILLTO}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><font color=red>＊</font><spring:message code= "LB.D0071" /></td>
										<td colspan="3" class="text-left">${bsData.MCARDTO}</td>
									</tr>
									<tr>
										<!-- 電子信箱 -->
										<td style="background-color: #EEEEEE;"><font color=red>＊</font><spring:message code="LB.Email"/></td>
										<td colspan="3" class="text-left">
										${bsData.PO.CPRIMEMAIL}<br>
										<c:choose>
							      				<c:when test="${bsData.PO.VARSTR3 eq '11'}">
							      					 <input type="checkbox" checked name="V3" disabled>
							      				</c:when>
							      				<c:otherwise>
							      					 <input type="checkbox" name="V3" disabled>
							      				</c:otherwise>
						      				</c:choose>
											<spring:message code= "LB.D0152" /><br>
											(<spring:message code= "LB.D0153" />)
										</td>
									</tr>
									<tr>
										<td colspan="4" class="text-center" style="background-color: #EEEEEE;font-weight: bold;"><spring:message code= "LB.D0079" /></td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0155" /></td>
										<td colspan="3" class="text-left">
											<c:choose>
								      			<c:when test="${bsData.PO.CPRIMJOBTYPE eq '1'}">
													<c:set var="CJT_1" value="checked" />
								      			</c:when>
								      			<c:when test="${bsData.PO.CPRIMJOBTYPE eq '2'}">
													<c:set var="CJT_2" value="checked" />
								      			</c:when>
								      			<c:when test="${bsData.PO.CPRIMJOBTYPE eq '3'}">
													<c:set var="CJT_3" value="checked" />
								      			</c:when>
								      			<c:when test="${bsData.PO.CPRIMJOBTYPE eq '4'}">
													<c:set var="CJT_4" value="checked" />
								      			</c:when>
						      				</c:choose>
					      					<input type="checkbox" name="CJT" ${CJT_1} disabled ><spring:message code= "LB.D0081" />
											<input type="checkbox" name="CJT" ${CJT_2} disabled ><spring:message code= "LB.D0082" />
											<input type="checkbox" name="CJT" ${CJT_3} disabled ><spring:message code= "LB.D0083" />
					      					<input type="checkbox" name="CJT" ${CJT_4} disabled ><spring:message code= "LB.D0084" />
										</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0086" /></td>
										<td class="text-left">${bsData.PO.CPRIMCOMPANY}</td>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0087" /></td>
										<td class="text-left">${bsData.CPRIMJOBTITLE}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0088_1" /></td>
										<td class="text-left">${bsData.PO.CPRIMSALARY} <spring:message code= "LB.D0088_2" /></td>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0089_1" /></td>
										<td class="text-left">${bsData.WORKYEARS} ${bsData.WORKMONTHS}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0090" /></td>
										<td colspan="3" class="text-left">${bsData.PO.CPRIMADDR3}</td>
									</tr>
									<tr>
										<td style="background-color: #EEEEEE;"><spring:message code= "LB.D0094" /></td>
										<td colspan="3" class="text-left">${bsData.PO.CPRIMOFFICETELNO1A}-${bsData.PO.CPRIMOFFICETELNO1B} <spring:message code= "LB.D0095" />:${bsData.PO.CPRIMOFFICETELNO1C}</td>
									</tr>
							</tbody>		
							</table>
							<div class="text-left">
								<!-- 說明 -->
								<ol class="description-list list-decimal">
									<li>
										<c:choose>
							      			<c:when test="${bsData.PO.CNOTE2 eq '1'}">
												<c:set var="C1_yes" value="checked" />
							      			</c:when>
							      			<c:when test="${bsData.PO.CNOTE2 eq '2'}">
												<c:set var="C1_no" value="checked" />
							      			</c:when>
					      				</c:choose>
										<spring:message code= "LB.D0096" />
					      				<input type="checkbox" name="C1" disabled ${C1_yes}><spring:message code= "LB.D0097" />
										<input type="checkbox" name="C1" disabled ${C1_no}><spring:message code= "LB.D0041" />　<spring:message code= "LB.X1638" />
										<font color=red>(<spring:message code= "LB.X0591" />)</font>
									</li>
									<li>
										<c:choose>
							      			<c:when test="${bsData.PO.MCASH eq '1'}">
												<c:set var="C2_yes" value="checked" />
							      			</c:when>
							      			<c:when test="${bsData.PO.MCASH eq '2'}">
												<c:set var="C2_no" value="checked" />
							      			</c:when>
					      				</c:choose>
										<spring:message code= "LB.D0096" />
					      				<input type="checkbox" name="C2" disabled ${C2_yes}><spring:message code= "LB.D0097" />
										<input type="checkbox" name="C2" disabled ${C2_no}><spring:message code= "LB.D0041" />　<spring:message code= "LB.X0590" />。
										<font color=red>（<spring:message code= "LB.X0591" />）</font>
									</li>
									<li>
										<c:choose>
							      			<c:when test="${bsData.PO.CNOTE1 eq '1'}">
												<c:set var="C3_yes" value="checked" />
							      			</c:when>
							      			<c:when test="${bsData.PO.CNOTE1 eq '2'}">
												<c:set var="C3_no" value="checked" />
							      			</c:when>
					      				</c:choose>
										<spring:message code= "LB.D0096" />
					      				<input type="checkbox" name="C3" disabled ${C3_yes}><spring:message code= "LB.D0097" />
										<input type="checkbox" name="C3" disabled ${C3_no}><spring:message code= "LB.D0041" />　<spring:message code= "LB.X1653" />
										<font color=red>(<spring:message code= "LB.X0591" />)</font>
									</li>
									<li>
										<c:choose>
							      			<c:when test="${bsData.PO.CNOTE3 eq '1'}">
												<c:set var="C4_yes" value="checked" />
							      			</c:when>
							      			<c:when test="${bsData.PO.CNOTE3 eq '2'}">
												<c:set var="C4_no" value="checked" />
							      			</c:when>
					      				</c:choose>
										<spring:message code= "LB.D0096" />
					      				<input type="checkbox" name="C4" disabled ${C4_yes}><spring:message code= "LB.D0097" />
										<input type="checkbox" name="C4" disabled ${C4_no}><spring:message code= "LB.D0041" />　<spring:message code= "LB.X1683" />
									</li>
								</ol>
							</div>
							<!-- 列印-->
							<input type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Print"/>" id="printbtn"/>
						</div>
					</div>
				</form>
				</section>
			</main>
			<!-- main-content END -->
		</div>
		<!-- content row END -->
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>