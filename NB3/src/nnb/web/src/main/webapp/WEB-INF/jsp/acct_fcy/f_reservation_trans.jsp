<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
  	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 預約交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X1477" /></li>
    <!-- 預約交易結果查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0053" /></li>
		</ol>
	</nav>



	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.W0053" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>						
			<form method="post" id="formId">
				<!-- 下載用 -->
				<input type="hidden" name="downloadFileName" value="LB.W0053" />
				<input type="hidden" name="downloadType" id="downloadType"/> 					
				<input type="hidden" name="templatePath" id="templatePath"/> 	
				<!-- EXCEL下載用 -->
				<input type="hidden" name="headerRightEnd" value="11"/>
				<input type="hidden" name="headerBottomEnd" value="8"/>
				<input type="hidden" name="rowStartIndex" value="9"/>
				<input type="hidden" name="rowRightEnd" value="11"/>
				<!-- 					上到下長度 -->
				<input type="hidden" name="txtHeaderBottomEnd" value="10"/> 					
				<input type="hidden" name="txtHasRowData" value="true"/>
				<input type="hidden" name="txtHasFooter" value="false"/>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<!--查詢區間  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Inquiry_period" />
									</label>
								</span>
								<span class="input-block">
									<!--  指定日期區塊 -->
									<div class="ttb-input">
										<input type="hidden" name="FGPERIOD" id="CMPERIOD" value="CMPERIOD" /> 
										<!--期間起日 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_start_date" />
											</span>
											<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" value="${f_reservation_trans.data.TODAY}" />
											<span class="input-unit CMSDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
										</div>
										<!--期間迄日 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_end_date" />
											</span>
											<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" value="${f_reservation_trans.data.TODAY}" />
											<span class="input-unit CMEDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
										</div>
										<!-- 驗證用的input -->
										<input id="odate" name="odate" type="text" value="${f_reservation_trans.data.TODAY}" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, CMSDATE, CMEDATE, false, 6, null]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
									</div>
								</span>
							</div>
							
							<!-- 交易狀態 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.W0054" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<!-- 成功 -->
										<label class="radio-block"><spring:message code="LB.D1099" />
											<input type="radio" name="FGTXSTATUS" id="DPTXSTSU" value="0" checked/>
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<!-- 失敗 -->
										<label class="radio-block"><spring:message code="LB.W0056" />
											<input type="radio" name="FGTXSTATUS" id="DPTXSTFA" value="1" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<!-- 處理中 -->
										<label class="radio-block"><spring:message code="LB.W0057" />
											<input type="radio" name="FGTXSTATUS" id="DPTXSTDE" value="2" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<!-- 全部 -->
										<label class="radio-block"><spring:message code="LB.All" />
											<input type="radio" name="FGTXSTATUS" id="DPTXSTDAL" value="All" />
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							
						</div>
						<input id="CMRESET" name="CMRESET" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
						<input type="button" class="ttb-button btn-flat-orange" id="CMSUBMIT" value="<spring:message code="LB.Display_as_web_page" />"/>
					</div>
				</div>							
				</form>
				<!--說明 -->
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><span><spring:message code="LB.F_Reservation_Trans_P1_D1" /></span></li>
				</ol>
<!-- 				</form> -->
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
	$(document).ready(function() {
		init();
		//日期
		datetimepickerEvent();
		//預約自動輸入今天
// 		getTmr();
		
	});
		
		
		function init(){
			
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});	
			
			$("#CMRESET").click(function () {
				$("#formId")[0].reset();
// 				getTmr();
			});
			
			$("#CMSUBMIT").click(function(e){
				e = e || window.event;
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}
				else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				$("#formId").attr("action","${__ctx}/FCY/ACCT/RESERVATION/f_reservation_trans_result");
	 	  			$("#formId").submit(); 
	 			}		
	  		});
		}
		
//			日曆欄位參數設定
		function datetimepickerEvent(){

		    $(".CMSDATE").click(function(event) {
				$('#CMSDATE').datetimepicker('show');
			});
		    $(".CMEDATE").click(function(event) {
				$('#CMEDATE').datetimepicker('show');
			});
			
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			});
		}
		
		//預約自動輸入今天
// 		function getTmr() {
// 			var today = new Date();
// 			today.setDate(today.getDate());
// 			var y = today.getFullYear();
// 			var m = today.getMonth() + 1;
// 			var d = today.getDate();
// 			if(m<10){
// 				m = "0"+m
// 			}
// 			if(d<10){
// 				d="0"+d
// 			}
// 			var tmr = y + "/" + m + "/" + d
// 			$('#CMSDATE').val(tmr);
// 			$('#CMEDATE').val(tmr);
// 			$('#odate').val(tmr);
// 		}
		
		//選項
		 	function formReset() {
				if(!$("#formId").validationEngine("validate")){
					e = e || window.event;//forIE
					e.preventDefault();
				}
		 		else{
// 			 		initBlockUI();
					if ($('#actionBar').val()=="excel"){
						$("#downloadType").val("OLDEXCEL");
						$("#templatePath").val("/downloadTemplate/f_reservation_trans.xls");
			 		}else if ($('#actionBar').val()=="txt"){
						$("#downloadType").val("TXT");
						$("#templatePath").val("/downloadTemplate/f_reservation_trans.txt");
			 		}
// 					ajaxDownload("${__ctx}/NT/ACCT/RESERVATION/reservation_trans_ajaxDirectDownload","formId","finishAjaxDownload()");
					$("#formId").attr("target", "");
	                $("#formId").attr("action", "${__ctx}/FCY/ACCT/RESERVATION/f_reservation_trans_directDownload");
		            $("#formId").submit();
		            $('#actionBar').val("");
		 		}
			}		

 	</script>
</body>
</html>
