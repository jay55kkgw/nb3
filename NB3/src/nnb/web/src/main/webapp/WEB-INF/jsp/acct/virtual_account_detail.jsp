<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 帳戶查詢 -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Account_Inquiry" /></li>
    <!-- 虛擬帳號入帳明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Virtual_Account_Detail" /></li>
		</ol>
	</nav>



	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.Virtual_Account_Detail" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 						下載為舊txt檔 -->
						<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
					</select>
				</div>						
			<form method="post" id="formId" autocomplete="off">
				<!-- 下載用 -->
				<input type="hidden" name="downloadFileName" value="LB.Virtual_Account_Detail"/>
				<input type="hidden" name="downloadType" id="downloadType"/> 					
				<input type="hidden" name="templatePath" id="templatePath"/> 	
				<input type="hidden" name="hasMultiRowData" value="true"/>
				<!-- EXCEL下載用 -->
				<input type="hidden" name="headerRightEnd" value="5" />
				<input type="hidden" name="headerBottomEnd" value="7" />
				<input type="hidden" name="rowStartIndex" value="8"/>
				<input type="hidden" name="rowRightEnd" value="5"/>
				<!-- TXT下載用 -->
				<input type="hidden" name="txtHeaderBottomEnd" value="10"/>
				<input type="hidden" name="txtHasRowDataBoolean" value="true"/>
				<input type="hidden" name="txtHasRowData" value="true"/>
				<input type="hidden" name="txtHasFooter" value="false"/>
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">							
							<!-- 帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.Account" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">

										<input type="text" id="ACN" name="ACN" class="text-input validate[required]" 
											placeholder="<spring:message code="LB.Enter_Account" />" maxlength="11" autocomplete="off" />
									</div>
								</span>
							</div>				
							 
							<div class="ttb-input-item row">
								<!--查詢區間  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Inquiry_period" />
									</label>
								</span>
								<span class="input-block">
									<!--  指定日期區塊 -->
									<div class="ttb-input">
										<!--期間起日 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Start_date" />
											</span>
											<jsp:useBean id="now" class="java.util.Date" />
<%-- 											<fmt:formatDate value="${now}" type="both" dateStyle="long" pattern="yyyy/MM/dd" var="today"/>		 --%>
											<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" 
												Maxlength=10 value="${virtual_account_detail.data.TODAY}" autocomplete="off" /> 
											<span class="input-unit CMSDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
										</div>
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_end_date" />
											</span>
											<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" 
												Maxlength=10 value="${virtual_account_detail.data.TODAY}" autocomplete="off"/> 
											<span class="input-unit CMEDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<span id="hideblockc" >
											<!-- 驗證用的input -->
												<input id="odate" name="odate" value="${virtual_account_detail.data.TODAY}"  type="text" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.X1445" />', odate, CMSDATE, CMEDATE, false, 2, 1]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" autocomplete="off"/>
											</span>
										</div>
									</div>
								</span>
							</div>
						</div>
						<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
						<input type="button" class="ttb-button btn-flat-orange" id="CMSUBMIT" value="<spring:message code="LB.Display_as_web_page" />"/>
					</div>
				</div>								
				</form>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
<%-- 					<li><spring:message code="LB.Demand_Virtual_detail_P1_D1" /></li> --%>
<%-- 					<li><spring:message code="LB.Demand_Virtual_detail_P1_D2" /></li> --%>
					<li><span><spring:message code="LB.Demand_Virtual_detail_P1_D1" /></span></li>
					<li><span><spring:message code="LB.Demand_Virtual_detail_P1_D2" /></span></li>
					<li><span>
						<spring:message code="LB.Download_txt_file" />
						<a target="_blank" href='${pageContext.request.contextPath}/public/N615_TXT.htm'>
							<spring:message code="LB.File_description" />
						</a> 
						<spring:message code="LB.X0079" />。
					</span></li>
					<li><span>
						<spring:message code="LB.Download_oldversion_txt_file" />
						<a target="_blank" href='${pageContext.request.contextPath}/public/N615_OLDTXT.htm'>
							<spring:message code="LB.File_description" />
						</a>
						。
					</span></li>
				</ol>

<!-- 				</form> -->
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
// 		日曆欄位參數設定
		function datetimepickerEvent(){

		    $(".CMSDATE").click(function(event) {
				$('#CMSDATE').datetimepicker('show');
			});
		    $(".CMEDATE").click(function(event) {
				$('#CMEDATE').datetimepicker('show');
			});
			
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			});
		}
		//選項
		 	function formReset() {
		 		//打開驗證隱藏欄位
// 				$("#hideblocka").show();
// 				$("#hideblockb").show();
				$("#hideblockc").show();
				//塞值進span內的input
// 				$("#Monthly_DateA").val($("#CMSDATE").val());
// 				$("#Monthly_DateB").val($("#CMEDATE").val());
			
// 				if(checkTimeRange() == false )
// 				{
// 					return false;
// 				}
				if(!$("#formId").validationEngine("validate")){
					e = e || window.event;//forIE
					e.preventDefault();
				}
		 		else{
// 			 		initBlockUI();
					if ($('#actionBar').val()=="excel"){
						$("#downloadType").val("OLDEXCEL");
						$("#templatePath").val("/downloadTemplate/virtual_account_detail.xls");
			 		}else if ($('#actionBar').val()=="txt"){
						$("#downloadType").val("TXT");
						$("#templatePath").val("/downloadTemplate/virtual_account_detail.txt");
			 		}else if ($('#actionBar').val()=="oldtxt"){
						$("#downloadType").val("OLDTXT");
						$("#templatePath").val("/downloadTemplate/virtual_account_detailOLD.txt");
			 		}
// 					ajaxDownload("${__ctx}/NT/ACCT/virtual_account_detail_ajaxDirectDownload","formId","finishAjaxDownload()");
					$("#formId").attr("target", "");
	                $("#formId").attr("action", "${__ctx}/NT/ACCT/virtual_account_detail_directDownload");
		            $("#formId").submit();
		            $('#actionBar').val("");
		 		}
			}
			function finishAjaxDownload(){
				$("#actionBar").val("");
				unBlockUI(initBlockId);
			}
			function init(){
				datetimepickerEvent();
//	 			fgtxdateEvent();
				$("#formId").validationEngine({
					binded: false,
					promptPosition: "inline"
				});
				$("#CMSUBMIT").click(function(e){
					//打開驗證隱藏欄位
// 					$("#hideblocka").show();
// 					$("#hideblockb").show();
					$("#hideblockc").show();
					//塞值進span內的input
// 					$("#Monthly_DateA").val($("#CMSDATE").val());
// 					$("#Monthly_DateB").val($("#CMEDATE").val());
				
					e = e || window.event;
// 					if(checkTimeRange() == false )
// 					{
// 						return false;
// 					}
					if(!$('#formId').validationEngine('validate')){
			        	e.preventDefault();
			        }
			        else{
		 				$("#formId").validationEngine('detach');
		 				initBlockUI();
		 				$("#formId").attr("action","${__ctx}/NT/ACCT/virtual_account_detail_result");
		 	  			$("#formId").submit(); 
		 			}		
		  		});
		    }
// 			function checkTimeRange()
// 			{//判斷起迄日,起日不得小於前月份1號,迄日不得大於今日
// 				var toDay = new Date();
// 				var lastM = toDay.getMonth();
				
// 				var year = toDay.getFullYear();
// 				if(lastM == 0){//月份0~11 若值為0上月份為12月
// 					lastM = 12;
// 					year = year -1 ;
// 				}
// 				var min = new Date(year,(lastM-1),01);
// 				var startT = new Date( $('#CMSDATE').val() );
// 				var endT = new Date( $('#CMEDATE').val() );
// 				if(startT >= min && startT <= endT && endT <= toDay){
// 					return true;
// 				}	
// 				else{
// 					var msg = "起迄日最小日為前月份一號，最大日為今日，起日不得大於迄日。";
// 					alert(msg);
// 					return false;
// 				}
// 			}
		
 	</script>
</body>
</html>
