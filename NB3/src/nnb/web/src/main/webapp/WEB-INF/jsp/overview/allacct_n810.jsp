<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">

	// n810-信用卡總覽
	i18n['CRLIMITFMT'] = '<spring:message code="LB.Credit_line" />'; // 信用額度
	i18n['CSHLIMITFMT'] = '<spring:message code="LB.Cash_advanced_line" />'; // 預借現金額度
	i18n['ACCBALFMT'] = '<spring:message code="LB.Current_balance" />'; // 目前已使用額度
	i18n['CSHBALFMT'] = '<spring:message code="LB.Cash_advanceUsed" />'; // 已動用預借現金
	i18n['INT_RATEFMT'] = '<spring:message code="LB.Current_revolving_interest" />'; // 本期循環信用利率
	i18n['PAYMT_ACCT_COVER'] = '<spring:message code="LB.Auto-pay_account_number" />'; // 自動扣繳帳號
	i18n['DBCODE'] = '<spring:message code="LB.Payment_method" />'; // 扣繳方式
    i18n['ls'] = '<spring:message code="LB.Latest_statement"/>';//本期帳單
    i18n['TTAP'] = '<spring:message code="LB.The_total_amount_payable"/>';//應繳總金額
    i18n['MP'] = '<spring:message code="LB.Minimum_payment"/>';//本期最低金額
    i18n['SD'] = '<spring:message code="LB.Statement_date"/>';//結帳日
    i18n['DD'] = '<spring:message code="LB.Due_date"/>';//繳款截止日
    
	// DataTable Ajax tables
	var n810_columns = [
		{"data":"CRLIMITFMT", "title":i18n['CRLIMITFMT'] },
		{"data":"CSHLIMITFMT", "title":i18n['CSHLIMITFMT'] },
		{"data":"ACCBALFMT", "title":i18n['ACCBALFMT'] },
		{"data":"CSHBALFMT", "title":i18n['CSHBALFMT'] },
		{"data":"INT_RATEFMT", "title":i18n['INT_RATEFMT'] },
		{"data":"PAYMT_ACCT_COVER", "title":i18n['PAYMT_ACCT_COVER'] },
		{"data":"DBCODE", "title":i18n['DBCODE'] },
	];
    
    var n810_ls_columns = [
    	{"data":"CURR_BALFMT", "title":i18n['TTAP'] },
		{"data":"TOTL_DUEFMT", "title":i18n['MP'] },
		{"data":"STMT_DAY", "title":i18n['SD'] },
		{"data":"STOP_DAY", "title":i18n['DD'] },
    	
    ];

	//0置中 1置右
	var n810_align = [1,1,1,1,1,0,0];
	var n810_ls_align = [1,1,0,0];
	// Ajax_n810-信用卡總覽
	function getMyAssets810() {
		$("#n810").show();
		$("#n810_title").show();
		createLoadingBox("n810_BOX",'');
		
		uri = '${__ctx}' + "/OVERVIEW/allacct_n810aj";
		console.log("allacct_n810aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax_n810, null, "n810");
	}

	// Ajax_callback
	function countAjax_n810(data){
		// 資料處理後呈現畫面
		if (data.result) {
			console.log(data);
			n810_rows = data.data.REC;
			n810_ls_rows = [data.data];
			createLoadingBox("n810_InnerBOX1",i18n['ls']+"("+data.data.STOP_DAY+")");
			createTable("n810_InnerBOX1",n810_ls_rows,n810_ls_columns,n810_ls_align);
		} else {
			// 錯誤訊息
			n810_columns = [ {
					"data" : "MSGCODE",
					"title" : '<spring:message code="LB.Transaction_code" />'
				}, {
					"data" : "MESSAGE",
					"title" : '<spring:message code="LB.Transaction_message" />'
				}
			];
			n810_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];
			n810_align = [0,0];
		}
		// 解遮罩
		
		createTable("n810_BOX",n810_rows,n810_columns,n810_align);
		
	}

</script>
<p id="n810_title" class="home-title" style="display: none"><spring:message code="LB.Credit_Card_Holder_Overview" /></p>
<div id="n810" class="main-content-block" style="display:none"> 
<div id="n810_BOX" style="display:none"></div>
<ul class="ttb-result-list">
<li class="full-list"><p style='color:#ff0000;text-align:center'><spring:message code="LB.Overview_P1_note"/></p></li>
<BR>
</ul>
<div id="n810_InnerBOX1" style="display:none"></div>
</div>
