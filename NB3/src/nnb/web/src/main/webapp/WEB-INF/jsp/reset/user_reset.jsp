<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
	
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdProcess.js"></script>
<%-- 	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script> --%>

<script type="text/javascript">
var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
var myobj = null; // 自然人憑證用
var urihost = "${__ctx}";
$(document).ready(function() {
	// HTML載入完成後0.1秒開始遮罩
	setTimeout("initBlockUI()", 100);

	// 初始化驗證碼
	setTimeout("initKapImg()", 200);
	// 生成驗證碼
	setTimeout("newKapImg()", 300);
	
	initFootable(); // 將.table變更為footable 
	init();
	goBack();
	// 過0.5秒解遮罩
	setTimeout("unBlockUI(initBlockId)", 800);
});

/**
 * 初始化BlockUI
 */
function initBlockUI() {
	initBlockId = blockUI();
}

// 交易機制選項
function processQuery(){
	var fgtxway = $('input[name="FGTXWAY"]:checked').val();
	console.log("fgtxway: " + fgtxway);

	switch(fgtxway) {
		case '0':
//				alert("交易密碼(SSL)...");
			$('#formId').submit();
			break;
			
		case '1':
//				alert("IKey...");
			var jsondc = $("#jsondc").val();
			
			// 遮罩後不給捲動
//				document.body.style.overflow = "hidden";

			// 呼叫IKEY元件
//				uiSignForPKCS7(jsondc);
			
			// 解遮罩後給捲動
//				document.body.style.overflow = 'auto';
			
			useIKey();
			break;
			
		case '2':
//				alert("晶片金融卡");

			// 遮罩後不給捲動
//				document.body.style.overflow = "hidden";
			
			// 呼叫讀卡機元件
// 			var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
// 			useCardReader(capUri);
			useCardReader();

			// 解遮罩後給捲動
//				document.body.style.overflow = 'auto';
			
	    	break;
	    	
		case '4':
//			自然人憑證
			$('#UID').val($('#CUSIDN').val());
			useNatural();
	    	break;
	    	
		default:
			//alert("nothing...");
			errorBlock(
					null, 
					null,
					["nothing..."], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	}
	
}

function init(){
	$("#formId").validationEngine({binded: false,promptPosition: "inline"});
	$("#CMSUBMIT").click(function(e){			
		console.log("submit~~");
		$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());

		if (!$('#formId').validationEngine('validate')){
        	e.preventDefault();
		}
		else{
			$('#HLOGINPIN').val(pin_encrypt($('#LOGINPIN').val()));
			$("#UID").val($("#CUSIDN").val().toUpperCase());
				console.log("submit~~");
				$("#formId").validationEngine('detach');
				initBlockUI();
				var action = '${__ctx}/RESET/user_reset_result';
			$("#formId").attr("action", action);
			unBlockUI(initBlockId);
			processQuery();
			$('#LOGINPIN').val("");
			$('#CKLOGINPIN').val("");
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
		}
	});
}	


/**************************************/
/*          	驗證碼驗證   		          */
/**************************************/
// 刷新驗證碼
function refreshCapCode() {
	console.log("refreshCapCode...");

	// 驗證碼
	$('input[name="capCode"]').val('');
	
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

	// 登入失敗解遮罩
	unBlockUI(initBlockId);
}

// 刷新輸入欄位
function changeCode() {
	console.log("changeCode...");
	
	// 清空輸入欄位
	$('input[name="capCode"]').val('');
	
	// 刷新驗證碼
	refreshCapCode();
}

// 初始化驗證碼
function initKapImg() {
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
}

// 生成驗證碼
function newKapImg() {
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').click(function() {
		refreshCapCode();
	});
}
	
function hideNpcDiv(flag){
	console.log(flag);
	if(flag == 'Y'){
		$("#NPCDIV").show();
	}else{
		$("#NPCDIV").hide();
	}
}

/**************************************/
/*          複寫checkIdProcess         */
/**************************************/
//取得卡片主帳號結束
function getMainAccountFinish(result){
	//成功
	if(result != "false"){
		var cardACN = result;
		$("#ACNNO").val(cardACN);
		if(cardACN.length > 11){
			cardACN = cardACN.substr(cardACN.length - 11);
		}
		var UID = $("#CUSIDN").val();
//		var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//		var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
		
		var uri = urihost+"/COMPONENT/component_without_id_aj";
		var rdata = { ACN: cardACN ,UID: UID };
		fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
// 		var uri = urihost+"/COMPONENT/component_acct_aj";
// 		var rdata = { ACN: cardACN };
// 		fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
	}
	//失敗
	else{
		showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",false);
	}
}
//卡片押碼結束
function generateTACFinish(result){
	//成功
	if(result != "false"){
		var TACData = result.split(",");
		
		var main = document.getElementById("formId");
		main.iSeqNo.value = TACData[1];
		main.ICSEQ.value = TACData[1];
		main.TAC.value = TACData[2];

		var ACN_Str1 = main.ACNNO.value;
		main.CHIP_ACN.value = ACN_Str1;
		main.OUTACN.value = ACN_Str1;
		main.ACNNO.value = ACN_Str1;
		main.submit();
	}
	//失敗
	else{
		FinalSendout("MaskArea",false);
	}
}

function CheckIdResult(data) {
	console.log("data: " + data);
	if (data) {
		// login_aj回傳資料
		console.log("data.json: " + JSON.stringify(data));
		// 成功
		if("0" == data.msgCode) {
			removeThenInsertCard();
		} else {
			showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",false);
			//alert("<spring:message code= "LB.X1701" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X1701' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	}
}

//卡片押碼結束
// function generateTACFinish(result){
// 	//成功
// 	if(result != "false"){
// 		var TACData = result.split(",");
		
// 		var main = document.getElementById("formId");
// 		main.iSeqNo.value = TACData[1];
// 		main.ICSEQ.value = TACData[1];
// 		main.TAC.value = TACData[2];

// 		var ACN_Str1 = main.ACNNO.value;
// 		main.CHIP_ACN.value = ACN_Str1;
// 		main.OUTACN.value = ACN_Str1;
// 		main.ACNNO.value = ACN_Str1;
// 		main.submit();
// 	}
// 	//失敗
// 	else{
// 		FinalSendout("MaskArea",false);
// 	}
// }
	
	var sec = 120;
	var the_Timeout;

	function countDown()
	{
		var main = document.getElementById("formId");      
      	var counter = document.getElementById("CountDown");                      
       	counter.innerHTML = sec;
       	sec--;
       	
       	if (sec == -1) 
       	{              
        	main.getotp.disabled = false;
			$('#getotp').removeClass('btn-flat-gray');
        	$('#getotp').addClass('btn-flat-orange');
			sec =120;
           	return;                
       	}                                     
       
       	//網頁倒數計時(120秒)              
     	the_Timeout = setTimeout("countDown()", 1000);    
	} 
	
    //取得otp並發送簡訊
    function getsmsotp() {
    	uri = '${__ctx}' + "/RESET/smsotp_ajax";
    	$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
    	if ($("#CUSIDN").val() != "") {
			rdata = {
				ADOPID : $("#ADOPID").val(),
				CUSIDN : $("#CUSIDN").val()
			};
			
			console.log("creatSmsotp.uri: " + uri);
			console.log("creatSmsotp.rdata: " + rdata);
			
			//關閉按鈕
			$("#getotp").attr("disabled",true);
			$('#getotp').removeClass('btn-flat-orange');
			$('#getotp').addClass('btn-flat-gray');
			//觸發倒數計時
			countDown();
			//簡訊發送異常先註解
			data = fstop.getServerDataEx(uri, rdata, false,
					callbackgetsmsotp);
	    }
    	else{
    		var main = document.getElementById("formId");
    		//alert("<spring:message code= "LB.D0025" />");
    		errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.D0025' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	    	main.CUSIDN.focus();
	    	return false;
    	}
	}
    
	function callbackgetsmsotp(data) {
		console.log("data: " + data);
		//alert(response.responseText);
	    var main = document.getElementById("formId");
		//eval("var result = " + response.responseText);
// 		var ResultStr = data[0].MSGSTR;
// 		console.log("ResultStr>>>"+ResultStr);
// 		var msgcod = ResultStr.substring(0,ResultStr.indexOf("=="));		
		var msgstr = data[0].MSGSTR;
		var msgcod = data[0].MSGCOD;		
		var phone = data[0].phone;		
		if(msgcod=="0000")
		{		
			phone = phone = phone.substring(0,6)+"***"+phone.substring(9);
		    //alert("<spring:message code= "LB.Alert198" />");
		    errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X2280' />"+phone+"<br/><spring:message code= 'LB.X2281' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		    main.OTP.focus();			
		}
		else
		{
			sec =120;
      		var counter = document.getElementById("CountDown");                      
       		counter.innerHTML = sec;			
			main.getotp.disabled = false;
			$('#getotp').removeClass('btn-flat-gray');
        	$('#getotp').addClass('btn-flat-orange');
			//alert(msgcod+"："+msgstr);
        	errorBlock(
					null, 
					null,
					[msgcod+":"+msgstr], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}				
	}
	
	function fgtxwayValidateEvent(){
		$('input[name="FGTXWAY"]').change(function(event) {
			if($('input[name="FGTXWAY"]:checked').val()=='1'){
				
			}
			if($('input[name="FGTXWAY"]:checked').val()=='2'){
				
			}
		});
	}
	//上一頁按鈕 click
	function goBack() {
		// 上一頁按鈕
		$("#CLOSEBTN").click(function () {
			// 遮罩
			initBlockUI();
			// 解除表單驗證
			$("#formId").validationEngine('detach');
			// 讓Controller知道是回上一頁
			$('#back').val("Y");
			// 回上一頁
			var action = '${__ctx}';
			$("#formId").attr("action", action);
			$("#formId").submit();
		});
	}
</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code= "LB.X1573" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            
                            <!--身份證字號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code= "LB.D0025" /></h4>
                                        <h4>(<spring:message code= "LB.D0621" />)</h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="text" maxLength="10" size="11" class="text-input validate[required,funcCall[validate_checkSYS_IDNO[CUSIDN]]" id="CUSIDN" name="CUSIDN" value="" >
                                    </div>
                                </span>
                            </div>

							<!--使用者名稱-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4>(新) <spring:message code="LB.Loginpage_User_name" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                      	<input type="text" maxLength="16" size="17" class="text-input validate[required,funcCall[validate_CheckUserName[<spring:message code= "LB.Loginpage_User_name" />,USERID]]]" id="USERID" name="USERID" value="" >
                                    </div>
                                </span>
                            </div>
							
							<!--確認使用者名稱-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4>(新) <spring:message code="LB.D1004" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                      	<input type="text" maxLength="16" size="17" class="text-input validate[required,funcCall[validate_CheckUserName[<spring:message code= "LB.D1004" />,CKUSERID]],funcCall[validate_CheckSameInput['<spring:message code= "LB.D1004" />',USERID,CKUSERID]]]" id="CKUSERID" value="" >
                                    </div>
                                </span>
                            </div>
							
							<!--簽入密碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4>(新) <spring:message code="LB.login_password" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                      	<input type="password" maxLength="8" autocomplete="off" class="ttb-input text-input input-width-125 validate[required,funcCall[validate_CheckPwd['<spring:message code= "LB.login_password" />',LOGINPIN]]]" id="LOGINPIN" name="LOGINPIN" value="" >
                                    </div>
                                </span>
                            </div>
							
							<!--確認簽入密碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4>(新) <spring:message code="LB.D1008" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                      	<input type="password" maxLength="8" autocomplete="off" class="ttb-input text-input input-width-125 validate[required,funcCall[validate_CheckPwd['<spring:message code= "LB.D1008" />',CKLOGINPIN]],funcCall[validate_CheckSameInput['<spring:message code= "LB.D1008" />',LOGINPIN,CKLOGINPIN]]]" id="CKLOGINPIN" value="" >
                                    </div>
                                </span>
                            </div>
							
							<!--輸入簡訊驗證碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code= "LB.X1574" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                      <input type="text" maxLength="8" size="10" class="ttb-input text-input input-width-125 validate[required,funcCall[validate_CheckLenEqual[<spring:message code="LB.Captcha"/>,OTP,false,8,8]],funcCall[validate_CheckNumber['LB.Captcha',OTP]]]" id="OTP" name="OTP" value=""/>
                                      <input class="ttb-sm-btn btn-flat-orange" type="button" id="getotp" name="getotp" value="<spring:message code= "LB.X1576" />" onclick="javascript:getsmsotp()" />
                                    </div>
									<span class="ttb-unit"><spring:message code= "LB.X1575" />：<font id="CountDown" color="red"></font><spring:message code= "LB.Second" /></span>
                                </span>
                            </div>

							<!--圖形驗證碼-->							
							<div class="ttb-input-item row" id="chaBlock">
								<!-- 驗證碼 -->
								<span class="input-title">
									<label>
										<h4><spring:message code= "LB.D1581" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text"
										class="ttb-input text-input input-width-125" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class="verification-img" src="" />
									<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
										onclick="changeCode()" value="<spring:message code="LB.Regeneration" />" />
									</div>
								</span>
							</div>
							
							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
											<!-- 交易機制 -->
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<!-- 晶片金融卡 -->
									<div class="ttb-input" onclick="hideNpcDiv('N')">
										<label class="radio-block">
<%-- 											<spring:message code="LB.Financial_debit_card" /> --%>
											<spring:message code= "LB.X0505" /> 
											<!-- 只能選晶片金融卡時，預設為非勾選讓使用者點擊後顯示驗證碼-->
											<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked>
												
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							
                        </div>
                        <input class="ttb-button btn-flat-gray" id="CLOSEBTN" name="CLOSEBTN" type="button" value="<spring:message code="LB.Cancel" />" onclick="back();" />
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
                        <input type="hidden" id="ADOPID" name="ADOPID" value="NA72">
  						<input type="hidden" id="HLOGINPIN" name="HLOGINPIN" value="">
  						<input type="hidden" id="CHIP_ACN" name="CHIP_ACN" value="">
  						<input type="hidden" id="UID" name="UID" value="">
  						<input type="hidden" id="ACN" name="ACN" value="">
  						<input type="hidden" id="ISSUER" name="ISSUER" value="">
  						<input type="hidden" id="ACNNO" name="ACNNO" value="">
  						<input type="hidden" id="OUTACN" name="OUTACN" value="">
  						<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
  						<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
  						<input type="hidden" id="TAC" name="TAC" value="">
  						<input type="hidden" id="TRMID" name="TRMID" value="">
  						<input type="hidden" id="TRANSEQ" name="TRANSEQ" value="2500">
  						<input type="hidden" id="IP" name="IP" value="${result_data.data.IP}">
  						<input type="hidden" name="TXTOKEN" id="TXTOKEN" value="${result_data.data.TXTOKEN}">
                    </div>
                </div>
                </form>
                <ol class="list-decimal description-list">
					<p><spring:message code="LB.Description_of_page" /></p>
                  	<li><span><spring:message code= "LB.UserName_Reset_P1_D1" /></span></li>
		        	<li><span><spring:message code= "LB.UserName_Reset_P1_D2" /></span></li>
		          	<li><span><spring:message code= "LB.UserName_Reset_P1_D3" /></span></li>
		          	<li><span><font color=red><B><spring:message code= "LB.UserName_Reset_P1_D4" /></B></font></span></li>
		          	<li><span><spring:message code= "LB.UserName_Reset_P1_D5" /></span></li>
		          	<li><span><font color=red><B><spring:message code= "LB.X2231" /></B></font></span></li>
                </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>