<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="card-detail-block">
	<div class="card-center-block">

		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-1" aria-expanded="true"
				aria-controls="popup3-1">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>受理時間</span>
					</div>
				</div>
			</a>
			<div id="popup3-1" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th>臨櫃</th>
											<th>網路</th>
											<th>語音</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>營業日上午9 時至下午3 時</td>
											<td colspan="2">交易服務時間：營業日上午7 時至下午3 時 <br>
												預約服務時間：交易服務時間以外 <br> （交易服務時間截止後至次一營業日交易服務時間開始前）
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-2" aria-expanded="true"
				aria-controls="popup3-2">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>最低申購投資金額</span>
					</div>
				</div>
			</a>
			<div id="popup3-2" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">投資標的</th>
											<th>最低投資金額</th>
											<th>增加單位</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="19">單筆</th>
											<th colspan="2">國內貨幣型基金</th>
											<td colspan="2">視個別基金規定（上午10：30截止申購作業）</td>
										</tr>
										<tr>
											<th rowspan="5">國內基金</th>
											<th>臺幣信託</th>
											<td>新臺幣10,000元</td>
											<td>新臺幣1,000元</td>
										</tr>
										<tr>
											<th rowspan="4" align="center" class="f13px lineH_01 coR">外幣信託</th>
											<td>南非幣10,000元</td>
											<td>南非幣1,000元</td>
										</tr>
										<tr>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>人民幣5,000元</td>
											<td>人民幣1,000元</td>
										</tr>
										<tr>
											<td>澳幣1,000元</td>
											<td>澳幣100元</td>
										</tr>
										<tr>
											<th rowspan="13">國外基金</th>
											<th>臺幣信託</th>
											<td>新臺幣30,000元</td>
											<td>新臺幣10,000元</td>
										</tr>
										<tr>
											<th rowspan="12">外幣信託</th>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>歐元1,000元</td>
											<td>歐元100元</td>
										</tr>
										<tr>
											<td>英鎊1,000元</td>
											<td>英鎊100元</td>
										</tr>
										<tr>
											<td>澳幣1,000元</td>
											<td>澳幣100元</td>
										</tr>
										<tr>
											<td>瑞士法郎1,000元</td>
											<td>瑞士法郎100元</td>
										</tr>
										<tr>
											<td>加拿大幣1,000元</td>
											<td>加拿大幣100元</td>
										</tr>
										<tr>
											<td>瑞典幣10,000元</td>
											<td>瑞典幣1,000元</td>
										</tr>
										<tr>
											<td>日圓100,000元</td>
											<td>日圓10,000元</td>
										</tr>
										<tr>
											<td>新加坡幣1,000元</td>
											<td>新加坡幣100元</td>
										</tr>
										<tr>
											<td>紐幣1,000元</td>
											<td>紐幣100元</td>
										</tr>
										<tr>
											<td>港幣10,000元</td>
											<td>港幣1,000元</td>
										</tr>
										<tr>
											<td>南非幣10,000元</td>
											<td>南非幣1,000元</td>
										</tr>
										<tr>
											<th rowspan="19">定額/不定額</th>
											<th colspan="2">國內貨幣型基金</th>
											<td colspan="2">暫未開放</td>
										</tr>
										<tr>
											<th colspan="2" rowspan="5">國內基金</th>
											<td>新臺幣3,000元</td>
											<td>新臺幣1,000元</td>
										</tr>
										<tr>
											<td>南非幣2,000元</td>
											<td>南非幣500元</td>
										</tr>
										<tr>
											<td>美元200元</td>
											<td>美元50元</td>
										</tr>
										<tr>
											<td>人民幣1,000元</td>
											<td>人民幣500元</td>
										</tr>
										<tr>
											<td>澳幣200元</td>
											<td>澳幣50元</td>
										</tr>
										<tr>
											<th rowspan="13">國外基金</th>
											<th>臺幣信託</th>
											<td>新臺幣3,000/5,000元</td>
											<td>新臺幣1,000元</td>
										</tr>
										<tr>
											<th rowspan="12">外幣信託</th>
											<td>美元200/300元</td>
											<td>美元50元</td>
										</tr>
										<tr>
											<td>歐元200/300元</td>
											<td>歐元50元</td>
										</tr>
										<tr>
											<td>英鎊200/300元</td>
											<td>英鎊50元</td>
										</tr>
										<tr>
											<td>澳幣200/300元</td>
											<td>澳幣50元</td>
										</tr>
										<tr>
											<td>瑞士法郎200/300元</td>
											<td>瑞士法郎50元</td>
										</tr>
										<tr>
											<td>加拿大幣200/300元</td>
											<td>加拿大幣50元</td>
										</tr>
										<tr>
											<td>瑞典幣2,000/3,000元</td>
											<td>瑞典幣500元</td>
										</tr>
										<tr>
											<td>日圓20,000/30,000元</td>
											<td>日圓5,000元</td>
										</tr>
										<tr>
											<td>紐幣200/300元</td>
											<td>紐幣50元</td>
										</tr>
										<tr>
											<td>港幣2,000元</td>
											<td>港幣500元</td>
										</tr>
										<tr>
											<td>新加坡幣200元</td>
											<td>新加坡幣50元</td>
										</tr>
										<tr>
											<td>南非幣2,000元</td>
											<td>南非幣500元</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-3" aria-expanded="true"
				aria-controls="popup3-3">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>申購手續費</span>
					</div>
				</div>
			</a>
			<div id="popup3-3" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">投資標的</th>
											<th colspan="2">最低手續費</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th colspan="3">單筆</th>
											<td colspan="2">係依各基金公司規定之費率計收</td>
										</tr>
										<tr>
											<th rowspan="10">定期 <br> (不)定額
											</th>
											<th colspan="2" rowspan="3">國內非貨幣型基金</th>
											<td>新臺幣50元</td>
											<td>南非幣60元</td>
										</tr>
										<tr>
											<td>美元6元</td>
											<td>人民幣30元</td>
										</tr>
										<tr>
											<td>澳幣6元</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<th rowspan="7">國外基金</th>
											<th>臺幣信託</th>
											<td colspan="2">新臺幣150元</td>
										</tr>
										<tr>
											<th rowspan="6">外幣信託</th>
											<td>美元6元</td>
											<td>瑞士法郎6元</td>
										</tr>
										<tr>
											<td>歐元6元</td>
											<td>加拿大幣6元</td>
										</tr>
										<tr>
											<td>英鎊6元</td>
											<td>瑞典幣60元</td>
										</tr>
										<tr>
											<td>澳幣6元</td>
											<td>日圓600元</td>
										</tr>
										<tr>
											<td>港幣60元</td>
											<td>紐幣6元</td>
										</tr>
										<tr>
											<td>新加坡幣6元</td>
											<td>南非幣60元</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-4" aria-expanded="true"
				aria-controls="popup3-4">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>定期定額／不定額投資注意事項</span>
					</div>
				</div>
			</a>
			<div id="popup3-4" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li data-num="1.">
									為確保投資成功，選擇以存款帳戶扣款者，請於指定扣帳日前一營業日留存足夠扣帳金額，選擇以信用卡扣款者，委託人及持卡人應於扣款日前二個營業日，確認信用卡餘額是否足夠基金投資扣款；另委託人如同時有數筆應投資款項而餘額不足時，則同意本行以扣帳作業整理之先後順序為準。</li>

								<li data-num="2.">
									如因線路中斷或其他事由，致本行不及於委託人指定之扣款日進行扣款投資作業，委託人同意順延至障礙事由排除後之銀行營業日進行扣帳，並於完成扣帳後始進行投資。</li>

								<li data-num="3.">關於「定期定額約定變更」之說明
									<ul class="ttb-result-list terms">
										<li data-num="a.">
											本系統執行「定期定額約定變更」項下之各項約定以申請日當天生效為原則，惟欲變更的信託資料已在進行相關項目處理中，或已完成扣款作業，該約定生效將順延。
										</li>
										<li data-num="b.">
											若欲變更的信託資料，本行電腦系統已在進行相關項目處理中，將暫時無法為您提供服務，煩請於次一營業日(涉及信用卡交易須次二營業日)再行申請。
										</li>
										<li data-num="c.">
											單一憑證有兩支以上扣款標的者，網路／系統無法提供「定期定額扣款金額」變更服務，煩請洽營業單位臨櫃辦理。</li>
									</ul>
								</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-5" aria-expanded="true"
				aria-controls="popup3-5">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>投資標的轉出及帳面剩餘金額限制</span>
					</div>
				</div>
			</a>
			<div id="popup3-5" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">投資標的</th>
											<th>最低部份贖回及帳面剩餘金額</th>
											<th>增加單位</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="19">單筆</th>
											<th colspan="2">國內貨幣型基金</th>
											<td colspan="2">視個別基金規定</td>
										</tr>
										<tr>
											<th rowspan="5">國內基金</th>
											<th>臺幣信託</th>
											<td>新臺幣10,000元</td>
											<td>新臺幣1,000元</td>
										</tr>
										<tr>
											<th rowspan="4">外幣信託</th>
											<td>南非幣10,000元</td>
											<td>南非幣1,000元</td>
										</tr>
										<tr>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>人民幣5,000元</td>
											<td>人民幣1,000元</td>
										</tr>
										<tr>
											<td>澳幣1,000元</td>
											<td>澳幣100元</td>
										</tr>
										<tr>
											<th rowspan="13">國外基金</th>
											<th>臺幣信託</th>
											<td>新臺幣30,000元</td>
											<td>新臺幣10,000元</td>
										</tr>
										<tr>
											<th rowspan="12">外幣信託</th>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>歐元1,000元</td>
											<td>歐元100元</td>
										</tr>
										<tr>
											<td>英鎊1,000元</td>
											<td>英鎊100元</td>
										</tr>
										<tr>
											<td>澳幣1,000元</td>
											<td>澳幣100元</td>
										</tr>
										<tr>
											<td>瑞士法郎1,000元</td>
											<td>瑞士法郎100元</td>
										</tr>
										<tr>
											<td>加拿大幣1,000元</td>
											<td>加拿大幣100元</td>
										</tr>
										<tr>
											<td>瑞典幣10,000元</td>
											<td>瑞典幣1,000元</td>
										</tr>
										<tr>
											<td>日圓100,000元</td>
											<td>日圓10,000元</td>
										</tr>
										<tr>
											<td>新加坡幣1,000元</td>
											<td>新加坡幣100元</td>
										</tr>
										<tr>
											<td>紐幣1,000元</td>
											<td>紐幣100元</td>
										</tr>
										<tr>
											<td>港幣10,000元</td>
											<td>港幣1,000元</td>
										</tr>
										<tr>
											<td>南非幣10,000元</td>
											<td>南非幣1,000元</td>
										</tr>
										<tr>
											<th colspan="3">定期(不)定額</th>
											<td colspan="2">各投資標的須一次全部轉出，不得申請部份轉出。</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-6" aria-expanded="true"
				aria-controls="popup3-6">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>轉換手續費</span>
					</div>
				</div>
			</a>
			<div id="popup3-6" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li data-num="1.">本行收取轉換手續費每筆新臺幣500元。</li>
								<li data-num="2.">各基金之轉換費用原則以內扣方式辦理，並依各基金公司規定計收。</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-7" aria-expanded="true"
				aria-controls="popup3-7">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>部分贖回及帳面剩餘金額之限制</span>
					</div>
				</div>
			</a>
			<div id="popup3-7" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">投資標的</th>
											<th>最低部份贖回及帳面剩餘金額</th>
											<th>增加單位</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="19">單筆(不)定額</th>
											<th colspan="2">國內貨幣型基金</th>
											<td colspan="2">視個別基金規定</td>
										</tr>
										<tr>
											<th rowspan="5">國內基金</th>
											<th>臺幣信託</th>
											<td>新臺幣10,000元</td>
											<td>新臺幣1,000元</td>
										</tr>
										<tr>
											<th rowspan="4">外幣信託</th>
											<td>南非幣10,000元</td>
											<td>南非幣1,000元</td>
										</tr>
										<tr>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>人民幣5,000元</td>
											<td>人民幣1,000元</td>
										</tr>
										<tr>
											<td>澳幣1,000元</td>
											<td>澳幣100元</td>
										</tr>
										<tr>
											<th rowspan="13">國外基金</th>
											<th>臺幣信託</th>
											<td>新臺幣30,000元</td>
											<td>新臺幣10,000元</td>
										</tr>
										<tr>
											<th rowspan="12">外幣信託</th>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>歐元1,000元</td>
											<td>歐元100元</td>
										</tr>
										<tr>
											<td>英鎊1,000元</td>
											<td>英鎊100元</td>
										</tr>
										<tr>
											<td>澳幣1,000元</td>
											<td>澳幣100元</td>
										</tr>
										<tr>
											<td>瑞士法郎1,000元</td>
											<td>瑞士法郎100元</td>
										</tr>
										<tr>
											<td>加拿大幣1,000元</td>
											<td>加拿大幣100元</td>
										</tr>
										<tr>
											<td>瑞典幣10,000元</td>
											<td>瑞典幣1,000元</td>
										</tr>
										<tr>
											<td>日圓100,000元</td>
											<td>日圓10,000元</td>
										</tr>
										<tr>
											<td>新加坡幣1,000元</td>
											<td>新加坡幣100元</td>
										</tr>
										<tr>
											<td>紐幣1,000元</td>
											<td>紐幣100元</td>
										</tr>
										<tr>
											<td>港幣10,000元</td>
											<td>港幣1,000元</td>
										</tr>
										<tr>
											<td>南非幣10,000元</td>
											<td>南非幣1,000元</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-8" aria-expanded="true"
				aria-controls="popup3-8">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>信託管理費</span>
					</div>
				</div>
			</a>
			<div id="popup3-8" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th width="17%">投資標的</th>
											<th>信託管理費</th>
											<th>最低信託管理費</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>單筆</th>
											<td class="white-spacing text-left" rowspan="2">本行自委託人交付指定投資標的信託資金（即信託金額）屆滿一年之次日起至該指定投資標的贖回日止，依信託資金帳載餘額每年按年率千分之二計收信託管理費（未滿一年之部份依實際天數計收）。<br>委託人指定投資申購標的類型為國內貨幣型基金者，免收信託管理費。
											</td>
											<td>等值新臺幣100元</td>
										</tr>
										<tr>
											<th>定期定額/不定額</th>
											<td>等值新臺幣200元</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-9" aria-expanded="true"
				aria-controls="popup3-9">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>申購單位數分配日</span>
					</div>
				</div>
			</a>
			<div id="popup3-9" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th>基金種類</th>
											<th>申購單位數分配時間</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>國內基金</th>
											<td valign="top" align="center" class=" f13px lineH_01 coR">投資生效日後約3-5個金融機構營業日
											</td>
										</tr>
										<tr>
											<th>境外基金</th>
											<td>投資生效日後約3-7個金融機構營業日</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p>※上述分配期間僅提供參考，若因基金公司作業或各基金另有規定，則分配時間不在此限。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-10" aria-expanded="true"
				aria-controls="popup3-10">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>贖回款入帳日</span>
					</div>
				</div>
			</a>
			<div id="popup3-10" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th>基金種類</th>
											<th>申購單位數分配時間</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>國內基金</th>
											<td>贖回生效日後約3-10個金融機構營業日 <br> （貨幣型基金為次1金融機構營業日）
											</td>
										</tr>
										<tr>
											<th>境外基金</th>
											<td>投資生效日後約3-7個金融機構營業日</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p>※各基金贖回款之入帳日不盡相同，須依各基金之規定及作業為準，贖回匯率悉依受託人於合理處理期間內實際辦理買匯或賣匯之匯率為準。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q11 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-11" aria-expanded="true"
				aria-controls="popup3-11">
				<div class="row">
					<span class="col-1">Q11</span>
					<div class="col-11">
						<span>換匯說明</span>
					</div>
				</div>
			</a>
			<div id="popup3-11" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li data-num="1.">申購交易:依交易生效日（遇例假日順延至次一營業日），採用受託人下午三時左右之銀行牌告即時賣匯匯率為準並予以適當優惠計算。</li>
								<li data-num="2.">贖回交易:依贖回款項匯入受託人指定帳戶時，採用受託人上午十時左右之銀行牌告即時買匯匯率為準並予以適當優惠計算。</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>

		</div>
		<p>所載內容如因排版、校對等因素所致之錯誤，仍應以本行或基金公司實際作業規範及刊登內容為準，委託人如有疑義可隨時向本行諮詢。</p>
	</div>
</div>