#/bin/sh

#Batch 寫入LOG的路徑
export LOG_HOME=/TBB/nb3/batch/logs
#Batch Crontab 寫入LOG的路徑
export CRONTAB_BATLOG=/TBB/nb3/batch/batlog 

export LOG_LEVEL=trace

#Batch 執行時產生檔案的路徑
export PATH_DATAFOLDERPATHR=/TBB/nb3/data

#MS_TW PORT
export MS_TW_PORT=9086

#MS_FX PORT
export MS_FX_PORT=9088

#MS_CC PORT
export MS_CC_PORT=9090

#MS_LOAN PORT
export MS_LOAN_PORT=9091

#MS_PAY PORT
export MS_PAY_PORT=9093

#MS_PS PORT
export MS_PS_PORT=9094

export MS_FUND_PORT=9087
export MS_OLA_PORT=9095
export MS_OLS_PORT=9092
export MS_GOLD_PORT=9089
export MS_TMRA_PORT=9099
export NB3_PORT=9081
export apache_ap1=http://172.22.11.22:80/mstw
export apache_ap2=http://172.22.11.16:80/mstw

#MS_FX
export FSTOP_BATCH_MS_FX=http://localhost:9088/ms_fx/
#MS_PS
export FSTOP_BATCH_MS_PS=http://localhost:9094/ms_ps/
#BASEURL
export FSTOP_BATCH_BASEURL=http://localhost:

export LOG_BACKUP_AP_FROM_PATH=/TBB/nb3/logs
export LOG_BACKUP5Y_PATH=/backup5y
export LOG_BACKUP3M_PATH=/TBB/nb3/backup3m


export BATCH_HOME=/TBB/nb3/batch/
export BATCH_LIB_HOME=$BATCH_HOME/lib/

THE_CLASSPATH=$BATCH_HOME/classes
for i in `ls $BATCH_LIB_HOME/*.jar`
do
  THE_CLASSPATH=${THE_CLASSPATH}:${i}
done

export BATCH_CLASSPATH=$THE_CLASSPATH


