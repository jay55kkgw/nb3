emailbakdir=/backup5y/$2/email
HOSTDATA=/TBB/nb3/hostdata

echo "$emailbakdir"

if [ ! -d $emailbakdir ]; then
    echo "mkdir $emailbakdir"
    mkdir $emailbakdir
fi

if [ -f $HOSTDATA/NOTICE_$1.TXT ]; then
    rm -f $emailbakdir/NOTICE_$1.TXT
    mv $HOSTDATA/NOTICE_$1.TXT $emailbakdir/

fi

if [ -f /TBB/nb3/hostdata/OK_$1.TXT ]; then
   rm -f $emailbakdir/OK_$1.TXT
   mv $HOSTDATA/OK_$1.TXT $emailbakdir/
fi
