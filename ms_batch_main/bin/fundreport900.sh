#!/bin/sh
#. ./setenv.sh #corontab 執行會找不到路徑
. /TBB/nb3/batch/bin/setenv.sh

export LOG_FILE_NAME=batchExecMain

if [ "${1}" != "" ]
then
        execDate=${1}
	echo "exec date :" ${execDate}
else
        execDate=`/TBB/nb3/batch/bin/preDate.sh -1`
	echo "exec date :" ${execDate}
fi

if [ "${2}" != "" ]
then
        execDate1=${2}
	echo "exec date1 :" ${execDate1}
fi

java -cp $BATCH_CLASSPATH -Dfile.encoding=UTF-8 fstop.Application batchExecMain fundReport_900 ${execDate} ${execDate1}