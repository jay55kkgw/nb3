#/bin/sh

#. ./setenv.sh #corontab 執行會找不到路徑
. /TBB/nb3/batch/bin/setenv.sh

export LOG_FILE_NAME=batchExecMain


if [ "${1}" != "" ]
then
        execDate=${1}
else
        execDate=`/TBB/nb3/batch/bin/preDate.sh -1`
fi

echo "exec date :" ${execDate}

java -cp $BATCH_CLASSPATH -Dfile.encoding=UTF-8 fstop.Application batchExecMain caldayhhreport ${execDate}

