if [ "${1}" != "" ]
then
        execDate=${1}
	echo "exec date :" ${execDate}
else
	execDate1=''
fi

if [ "${2}" != "" ]
then
        execDate1=${2}
	echo "exec date1 :" ${execDate1}
else
	execDate=''
fi

#. ./setenv.sh #corontab 執行會找不到路徑
. /TBB/nb3/batch/bin/setenv.sh

export LOG_FILE_NAME=batchExecMain

curDate=`date +%Y%m%d`

java -cp $BATCH_CLASSPATH -Dfile.encoding=UTF-8 fstop.Application batchExecMain txnCreditLog ${execDate} ${execDate1}
