#!/bin/sh

if [ "${1}" != "" ]
then
        execDate=${1}
else
        execDate=`/TBB/nb3/batch/bin/preDate.sh -1`
fi

pos=`pwd`
curDate=`date +%Y%m%d`

echo "exec date :" ${execDate}

#. ./setenv.sh #corontab 執行會找不到路徑
. /TBB/nb3/batch/bin/setenv.sh

java -cp $BATCH_CLASSPATH -Dfile.encoding=UTF-8 fstop.Application batchExecMain nnbReport_D903 ${execDate}