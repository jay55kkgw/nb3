#!/bin/sh

if [ "${1}" != "" ]
then
    execDate=${1}
	echo "exec date :" ${execDate}
else
	execDate=''
fi

#. ./setenv.sh #corontab 執行會找不到路徑
. /TBB/nb3/batch/bin/setenv.sh

export LOG_FILE_NAME=batchExecMain

java -cp $BATCH_CLASSPATH -Dfile.encoding=UTF-8 fstop.Application batchExecMain nnbReport_M910 ${execDate}