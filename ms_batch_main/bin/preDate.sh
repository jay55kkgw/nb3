#!/bin/sh
#set -x

if [ "${1}" = "-h" ]
then
        exit
fi

if [ "${1}" != "" ]
then
        days2shift=${1}
else
        days2shift=0
fi

expr ${days2shift} / 1 >/dev/null 2>&1
if [ $? -gt 1 ]
then
        days2shift=0
        #echo "Illegal input. \"${days2shift}\""
        #exit
fi

day=`date +%d`
month=`date +%m`
year=`date +%Y`

day=`expr ${day} + ${days2shift}`

days_of_month()
{
_mon=`expr ${1} / 1`
_year=`expr ${2} / 1`
if [ ${_mon} -eq 9 -a ${_year} -eq 1752 ]
then
        echo "19"
else
        echo `cal ${1} ${2}`|awk '{ printf $NF }'
fi
}

days=`days_of_month ${month} ${year}`
while [ ${day} -le 0 ]
do
        month=`expr ${month} - 1`
        if [ ${month} -eq 0 ]
        then
            year=`expr ${year} - 1`
            month=12
        fi
        days=`days_of_month ${month} ${year}`
        day=`expr ${day} + ${days}`
done

while [ ${day} -gt ${days} ]
do
        day=`expr ${day} - ${days}`
        month=`expr ${month} + 1`
        if [ ${month} -eq 13 ]
        then
            month=1
            year=`expr ${year} + 1`
        fi
        days=`days_of_month ${month} ${year}`
done

month=`expr ${month} / 1`
printf "%02d%02d%02d\n" ${year} ${month} ${day}
