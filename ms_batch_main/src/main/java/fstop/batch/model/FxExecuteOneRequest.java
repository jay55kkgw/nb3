package fstop.batch.model;

import lombok.Data;

@Data
public class FxExecuteOneRequest {

    private String FXSCHNO;
    private String FXSCHTXDATE;
    private String FXUSERID;
    private String ADTXNO;
}
