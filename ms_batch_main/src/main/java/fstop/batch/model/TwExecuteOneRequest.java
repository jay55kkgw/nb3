package fstop.batch.model;

import lombok.Data;

@Data
public class TwExecuteOneRequest {

    private String DPSCHNO;
    private String DPSCHTXDATE;
    private String DPUSERID;
    private String MSADDR;
    private String ADTXNO;
}
