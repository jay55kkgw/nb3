package fstop.batch.model;

import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;

public class ValueKeeper<T> {

    private T val;
    static Method m = ReflectionUtils.findMethod(ValueKeeper.class, "getValue", new Class[]{});

    private ValueKeeper(T v) {
        this.val = v;
    }

    public static <O> ValueKeeper<O> of(O o) {
        return new ValueKeeper<>(o);
    }

    public T val() {
        try {
            return (T)m.invoke(this, null);
        } catch (Exception e) {
           throw new RuntimeException("execute error", e);
        }
    }

    public T getValue() {
        return this.val;
    }

//    public static void main(String[] a) {
//        ValueKeeper<String> s = ValueKeeper.of("ss");
//        System.out.println(s.val());
//        ValueKeeper<String> sc = ValueKeeper.of(null);
//        System.out.println(sc.val());
//
//        byte[] temp=new byte[18000];
//        ValueKeeper c = ValueKeeper.of(new Object[]{temp});
//        byte[] ary = (byte[])((Object[])c.val())[0];
//        System.out.println(ary.length);
//    }
}
