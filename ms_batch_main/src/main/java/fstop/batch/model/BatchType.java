package fstop.batch.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class BatchType {
    private String context;
    private int instancePort;
}
