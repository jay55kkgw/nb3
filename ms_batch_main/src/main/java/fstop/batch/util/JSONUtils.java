package fstop.batch.util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

public class JSONUtils {

	public static String map2json(Map m) {
		return toJson(m);
	}

	public static <T> T fromJson(String json, Type typeOfT) {
		Gson gson = new Gson();
		return (T) gson.fromJson(json, typeOfT);
	}

	public static Map<String, Object> json2map(String json) {
		return fromJson(json, Map.class);
	}

	public static String toJson(Object o) {
		Gson gson = new Gson();
		return gson.toJson(o);
	}

	public static List toList(String json) {
		return fromJson(json, ArrayList.class);

	}
}
