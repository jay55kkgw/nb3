package fstop.batch.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import fstop.batch.util.StrUtils;

/**
 * 在同時發送預約交易時, 同一個身份證字號, 不能同時發送, 避免主機的DB會 dead lock
 * 使用 next 取得預約的編號, 同時記錄這個身份證號正在使用
 * 交易執行完, 再使用 giveBack 將記錄的身份證號移除
 * 若讀取的交易對應的身份證號正有交易在執行, 會暫存在 storeList 裡, 等可以執行時, 使用 getNext 回傳
 * @author jimmy
 *
 */
public class TransferIdLoader {

	Set currentIds = new HashSet();
	private BufferedReader reader;
	private Map<String, String> storeList = new HashMap();  //txno, userid
	private Map<String, String> usedList = new HashMap();  //txno, userid
	boolean isReaderEnd = false; //是否已讀到檔案的最尾端
	public TransferIdLoader(Reader reader) {
		this.reader = new BufferedReader(reader);
	}

	/**
	 * 若已沒有任何記錄可以讀取, 則回傳 null
	 *
	 * @return
	 * @throws IOException
	 */
	public String getNext() throws IOException {
		while(true) {
			synchronized (TransferIdLoader.class) {
				String s = next();
				if(StrUtils.isNotEmpty(s))
					return s;
			}

			if(isFinished())
				return null;

			try {
				Thread.sleep(2000);
			}
			catch(Exception e){}

		}
	}

	private String next() throws IOException {
		String result = null;

		for(Entry<String, String> rm : storeList.entrySet()) {
			String userid = rm.getValue();
			String txno = rm.getKey();
			if(currentIds.contains(userid.toUpperCase())) {
				continue;
			}
			else {
				result = txno;
				markToUsed(txno, userid);
				break;
			}
		}

		if(StrUtils.isNotEmpty(result)) {
			storeList.remove(result);
			return result;
		}

		while(true) {
			//讀取的資料格式每行為 ID,USERID
			String s = reader.readLine();
			if(s != null) {
				if(s.length() == 0)
					continue;
				String[] x = s.split(",");
				String txno = StrUtils.trim(x[0]).toUpperCase();
				String userid = StrUtils.trim(x[1]).toUpperCase();
				if(currentIds.contains(userid)) {
					storeList.put(txno, userid);
					continue;
				}
				else {
					markToUsed(txno, userid);
					result = txno;
					break;
				}
			}
			else {
				isReaderEnd = true;
				break;
			}
		}

		return result;
	}

	private void markToUsed(String txno, String userid) {
		//目前使用的交易, 記錄身份證號, 及 txno 與 userid 的對應
		currentIds.add(userid);
		usedList.put(txno, userid);
	}

	public void giveBack(String txno) {
		synchronized (TransferIdLoader.class) {
			String _txno = StrUtils.trim(txno).toUpperCase();
			if(usedList.containsKey(_txno)) {
				currentIds.remove(usedList.get(_txno));
				usedList.remove(_txno);
			}
		}
	}

	private boolean isFinished() {
		return isReaderEnd && (storeList.size() == 0);
	}

}
