package fstop.batch.main;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;


import fstop.batch.model.BatchResult;
import fstop.batch.util.ESAPIUtil;
import fstop.batch.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractBatchExecMain {

	protected static int FxTxnMainPORT = 5101;
	protected static int FxReTxnMainPORT = 5102;
	protected static int TwTxnMainPORT = 5103;
	protected static int TwReTxnMainPORT = 5104;
	protected static int BatchExecMainPORT = 5105;

	@Autowired
	@Qualifier("restTemplate")
	RestTemplate restTemplate;
	
	@Autowired
	@Qualifier("echoRestTemplate")
	RestTemplate echoRestTemplate;

	@Autowired
	private EnvSetting envSetting;

	public String getServiceUrl(String funcUrl) {

		if (funcUrl.startsWith("/") == false)
			funcUrl = "/" + funcUrl;
		String s = envSetting.getPs_Url();
		if (s.endsWith("/") == true) {
			s = s.substring(0, s.length() - 1);
		}
		log.info(ESAPIUtil.vaildLog("service url: " + (s + funcUrl)));
		return s + "/batch" + funcUrl;
	}

	/**
	 * BEN 新增for台幣微服務判斷
	 * 
	 * @param contextRoot
	 * @param ms
	 * @param apiName
	 * @return
	 */
	public String getServiceUrl(String ms, String apiName) {
		String api = ms + "/batch/" + apiName;
		if (api.startsWith("/") == false)
			api = "/" + api;
		String s = envSetting.getBaseUrl();
		switch (ms) {
		case "ms_tw":
			s += envSetting.getMs_tw_p();
			break;
		case "ms_cc":
			s += envSetting.getMs_cc_p();
			break;
		case "ms_loan":
			s += envSetting.getMs_loan_p();
			break;
		case "ms_pay":
			s += envSetting.getMs_pay_p();
			break;
		case "ms_ps":
			s += envSetting.getMs_ps_p();
			break;
		case "ms_fx":
			s += envSetting.getMs_fx_p();
			break;
		case "ms_fund":
			s += envSetting.getMs_fund_p();
			break;
		case "ms_ola":
			s += envSetting.getMs_ola_p();
			break;
		case "ms_ols":
			s += envSetting.getMs_ols_p();
			break;
		case "ms_gold":
			s += envSetting.getMs_gold_p();
			break;
		case "TMRA":
			s += envSetting.getMs_tmra_p();
			break;
		case "nb3":
			s += envSetting.getNB3_p();
			break;
		}
		log.info(ESAPIUtil.vaildLog("service url: " + (s + api)));
		return s + api;
	}
	
	public BatchResult service_doBatch(String ms, String apiName, String[] args) {
		BatchResult result = null;
		String serviceUrl = getServiceUrl(ms, apiName);
		List<String> params = new ArrayList<>();
		if (args != null) {
			params.addAll(Arrays.asList(args));
		}
		log.info(ESAPIUtil.vaildLog("do service_doBatch: " + serviceUrl + ", params: " + JSONUtils.toJson(args)));
		try {
			result = restTemplate.postForObject(serviceUrl, params, BatchResult.class);
		} catch (Exception e) {
			if(e instanceof ResourceAccessException) {
				log.error("Over 2hrs , STOP BATCH");
				BatchResult Erresult = new BatchResult();
				Erresult.setSuccess(false);
				Erresult.setErrorCode("RestTemplate_Over2hr");
				return Erresult;
			}else {
				log.error("Error : {}" , e);
				log.error("Service_doBatch Error >> to {}", ms);
				log.error("WAIT 5 SECOND TO Retry 1");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException sleepE1) {
					log.error("Thread.sleep Error >> ", sleepE1);
				}
				try {
					log.error("Retry 1");
					result = restTemplate.postForObject(serviceUrl, params, BatchResult.class);
				} catch (Exception e1) {
					log.error("Retry 1 Service_doBatch Error >> to {}", ms);
					log.error("WAIT 5 SECOND TO Retry 2");
					try {
						Thread.sleep(5000);
					} catch (InterruptedException sleepE2) {
						log.error("Thread.sleep2 Error >> ", sleepE2);
					}
					try {
						log.error("Retry 2");
						result = restTemplate.postForObject(serviceUrl, params, BatchResult.class);
					} catch (Exception e2) {
						log.error("Retry 2 Service_doBatch Error >> to {} ", ms);
						log.error("Stop Retry");
						BatchResult Erresult = new BatchResult();
						Erresult.setSuccess(false);
						Erresult.setErrorCode("MS_PS_ERROR");
						return Erresult;
					}
				}
			}
		}
		return result;
	}

	public BatchResult service_doBatch4Echo(String url, Object args) {
		String serviceUrl = url;
		log.info(ESAPIUtil.vaildLog("do service_doBatch4Echo: " + serviceUrl + ", params: " + JSONUtils.toJson(args)));
		// restTemplate = setHttpConfig(3, 3);
		BatchResult result = echoRestTemplate.postForObject(serviceUrl, args, BatchResult.class);
		return result;
	}

	public static RestTemplate setHttpConfig(Integer socketTimeout, Integer connectTimeout) {
		connectTimeout = connectTimeout * 1000;
		socketTimeout = socketTimeout * 1000;
		HttpComponentsClientHttpRequestFactory rf = new HttpComponentsClientHttpRequestFactory();
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		return new RestTemplate(rf);

	}

	/**
	 * 檢測系統是否只啟動了一個實例 ServerSocket 要到後就關閉
	 */
	protected static void checkExistsInstance(int srvPort, String errorMessage) {
		try {
			ServerSocket srvSocket = new ServerSocket(srvPort); // 啟動一個ServerSocket，用以控制只啟動一個實例
			srvSocket.close();
		} catch (IOException ex) {
			log.error(errorMessage);
			System.exit(0);
		}
	}

	/**
	 * 檢測系統是否只啟動了一個實例 ServerSocket 不關閉
	 */
	protected static ServerSocket initSingleInstance(int srvPort, String errorMessage) {
		ServerSocket srvSocket = null;
		try {
			srvSocket = new ServerSocket(srvPort); // 啟動一個ServerSocket，用以控制只啟動一個實例
		} catch (IOException ex) {
			if (ex.getMessage().indexOf("Address already in use: JVM_Bind") >= 0)
				log.info(" 在一台主機上同時只能啟動一個進程(Only one instance allowed)。");
			log.error(errorMessage);
			log.error("", ex);
			System.exit(0);
		}
		
		return srvSocket;
	}

	//
	// public static void main(String[] argv) {
	// try {
	// BatchWebService service = (BatchWebService)getService(BatchWebService.class,
	// serviceUrl);
	//
	// ArrayList argList = new ArrayList(Arrays.asList(argv));
	// argList.remove(0);
	// String[] params = new String[argList.size()];
	// argList.toArray(params);
	// BatchResult result = service.doBatch(argv[0], params);
	//
	//
	// String d = result.getData();
	// if(d != null && d.length() > 0) {
	// try {
	// Map<String, Object> m = JSONUtils.json2map(d);
	// StringBuffer b = new StringBuffer();
	// for(String k : m.keySet()) {
	// Object o = m.get(k);
	// if(o == null)
	// o = "";
	// b.append(k).append("=").append(o.toString()).append("\n");
	// }
	// log.info("\n回傳訊息 :\n" + b + "\n");
	// }
	// catch(Exception e){};
	// }
	//
	// }
	// catch(Exception e) {
	// log.error("執行錯誤.", e);
	// }
	//
	// System.exit(0);
	// }

	protected static boolean testConnection(String host, int port) throws IOException {
		Socket socket = new Socket();
		socket.connect(new InetSocketAddress(host, port), 3);
		socket.close();
		return true;
	}

	public abstract void executeMain(String[] args);

}
