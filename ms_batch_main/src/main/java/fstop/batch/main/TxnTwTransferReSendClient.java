package fstop.batch.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import fstop.batch.model.BatchResult;
import fstop.batch.model.TwExecuteOneRequest;
import fstop.batch.util.CSVReader;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TxnTwTransferReSendClient extends AbstractBatchExecMain {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ExecutorService executorService;

	@Autowired
	EnvSetting envSetting;

	@Override
	public void executeMain(String[] args) {

		String MSADDR = "";

		ServerSocket socket = null;
		// 查詢預約轉即時交易完成之時間
		try {
			checkExistsInstance(TwTxnMainPORT, "[ERROR]台幣預約轉即時交易正在執行 ......");
			socket = initSingleInstance(TwReTxnMainPORT, "[ERROR]台幣重送交易正在執行 ......");
			File f = new File(envSetting.getDataFolder(), "TwTxnMain_Timestamp.txt");
			f.setWritable(true, true);
			f.setReadable(true, true);
			f.setExecutable(true, true);
			String str_FileDate = FileUtils.readFileToString(f, "UTF-8");

			log.info("臺幣一扣作業 -> TwTxnMain_Timestamp 日期 : {}", str_FileDate);

			String str_NowDate = new DateTime().toString("yyyy/MM/dd");

			log.info("臺幣一扣作業 -> 營業日期 : {} ", str_NowDate);

			// 若預約轉即時交易尚未完成,不可執行二扣
			if (!str_FileDate.equals(str_NowDate)) {

				// 台幣一扣作業尚未執行完成 -> 需發送 Email通知
				BatchResult result = service_doBatch("ms_ps", "batchmailsender",
						new String[] { "executeone", "2", "TW", str_FileDate });

				log.info("台幣二扣作業已發送狀態檢查通知 >> {}", "臺幣一扣作業尚未執行完成,不可執行二扣作業");
				try {
					log.debug("socket.close()");
					socket.close();
				} catch (IOException e1) {
					log.error("Socket close Error");
				}
				System.exit(0);
			} else {
				// 台幣一扣作業已執行完成,準備開始執行二扣作業 -> 需發送 Email通知
				BatchResult result = service_doBatch("ms_ps", "batchmailsender",
						new String[] { "executeone", "3", "TW", "" });

				log.info("台幣二扣作業已發送狀態檢查通知 >>{}", "準備開始執行臺幣二扣作業");
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				log.error("Over 2hrs , STOP BATCH");
				try {
					log.debug("socket.close()");
					socket.close();
				} catch (IOException e1) {
					log.error("Socket close Error");
				}
				System.exit(0);
			} else {
				log.error("讀取 TwTxnMain_Timestamp.txt 錯誤", e);

				// 讀取 TwTxnMain_Timestamp.txt 錯誤 -> 需發送 Email通知
				BatchResult result = service_doBatch("ms_ps", "batchmailsender",
						new String[] { "executeone", "4", "TW", e.getMessage() });

				log.info("台幣二扣作業已發送讀檔異常通知 >> {}", "臺幣二扣讀取 Timestamp 檔錯誤");
				try {
					log.debug("socket.close()");
					socket.close();
				} catch (IOException e1) {
					log.error("Socket close Error");
				}
				System.exit(0);
			}
		}

		log.info("開始執行台幣預約重送(二扣)");
		String allIds = "";
		log.info("取得所有重送交易編號開始");
		BatchResult dataResult = null;

		try {
			dataResult = service_doBatch("ms_ps", "txntwtransferresend", new String[] { "getAllList" });

			if (dataResult.isSuccess()) {
				log.info("TW_DPSCHNOs: " + dataResult.getData().get("TW_DPSCHNOs"));
				log.info("response: " + new Gson().toJson(dataResult));

				log.info("取得所有重送交易編號結束");

				StringReader r = new StringReader((String) dataResult.getData().get("TW_DPSCHNOs"));

				CSVReader reader = null;
				try {
					reader = new CSVReader(r);
					String[] line = null;
					while ((line = reader.readNext()) != null) {
						try {
							String DPSCHNO = line[0];
							String DPSCHTXDATE = line[1];
							String DPUSERID = line[2];
							// 2019/10/01 新增for不同微服務 BEN
							MSADDR = line[3].toLowerCase();
							// 2019/10/31 新增交易編號參數 BEN
							String ADTXNO = line[4];

							TwExecuteOneRequest request = new TwExecuteOneRequest();
							request.setDPSCHNO(DPSCHNO);
							request.setDPSCHTXDATE(DPSCHTXDATE);
							request.setDPUSERID(DPUSERID);
							request.setMSADDR(MSADDR);
							request.setADTXNO(ADTXNO);

							// log.info("開始執行, 預約編號 DPSCHNO: " + DPSCHNO + ", DPSCHTXDATE: " + DPSCHTXDATE +
							// ", 微服務 :" + MSADDR + ", 交易序號：" + ADTXNO);

							log.info(
									"開始執行 ： DPSCHNO >> {} , DPSCHTXDATE >> {} , DPUSERID >> {} , ADTXNO >> {} , MSADDR >> {} ",
									DPSCHNO, DPSCHTXDATE, DPUSERID, ADTXNO, MSADDR);

							BatchResult result = service_doBatch("ms_ps", "txntwtransferresend",
									new String[] { "executeone", DPSCHNO, DPSCHTXDATE, DPUSERID, ADTXNO, MSADDR });

							if (!result.isSuccess())
								// log.error("預約編號 DPSCHNO: " + DPSCHNO + ", DPSCHTXDATE: " + DPSCHTXDATE + ",
								// 發生錯誤.");
								log.error("預約編號 DPSCHNO: {} , DPSCHTXDATE: {} , DPUSERID: {} >> 發生錯誤", DPSCHNO,
										DPSCHTXDATE, DPUSERID);
							else
								log.error("預約編號 DPSCHNO: {} , DPSCHTXDATE: {} , DPUSERID: {} >> 成功", DPSCHNO,
										DPSCHTXDATE, DPUSERID);

							log.info("execute result : {} ", new Gson().toJson(result));
						} catch (Exception e) {
							log.error("執行錯誤.", e);
						}
					}

				} catch (RuntimeException e) {
					log.error("發生錯誤 , txntwtransferresend/executeone error.", e);
				} catch (Exception e) {
					log.error("發生錯誤 , txntwtransferresend/executeone error.", e);
				} finally {
					try {
						if (reader != null) {
							reader.close();
						}
					} catch (Exception e) {
					}
				}

				log.info("結束執行台幣預約重送.");

				// 紀錄二扣後特殊錯誤代碼之預約交易
				String outString = "";
				try {

					File f = new File(envSetting.getDataFolder(), "tw_failmsgfile_2call.txt");
					f.setWritable(true, true);
					f.setReadable(true, true);
					f.setExecutable(true, true);

					FileWriter fw = new FileWriter(f);
					DateTime now = new DateTime();
					String str_NowDate = now.toString("yyyyMMdd");
					BatchResult result = service_doBatch("ms_ps", "txntwtransferresend",
							new String[] { "getErrorFileList", str_NowDate });

					ReadData readData = new ReadData();
					outString = readData.readDataTw(result);

					FileUtils.writeStringToFile(f, outString, "UTF-8");

					BatchResult mailresult = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "7", "TW", outString });

					if (mailresult.isSuccess) {
						log.info("台幣二扣傳送需通知客戶資料Email成功.");
					} else {
						log.error("台幣二扣傳送需通知客戶資料Email失敗.");
					}
				} catch (Exception e) {

					log.error("寫入 tw_failmsgfile_2call.txt 錯誤.", e);

					// 寫入 tw_failmsgfile_1call.txt 錯誤 -> 需發送 Email通知
					BatchResult result = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "8", "TW", e.getMessage() });

					log.info("台幣二扣作業已發送寫檔(tw_failmsgfile_2call)異常通知.");
				}
			} else {
				BatchResult result = new BatchResult();
				log.error("發生錯誤 , txntwtransferresend/getAllList error. >> {}", dataResult.getErrorCode());
				if ("MS_PS_ERROR".equals(dataResult.getErrorCode())) {
					// PS都連線失敗了無法寄信啊啊啊
					// result = service_doBatch("ms_ps", "batchmailsender",
					// new String[] { "executeone", "10", "TW", "微服務連線失敗" });
					log.error("MS_PS 連線失敗");
				} else if ("DB_ERROR".equals(dataResult.getErrorCode())) {
					result = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "10", "TW", "資料庫連線失敗" });
				}
				if (result.isSuccess()) {
					log.info("台幣二扣作業已發送資料庫連線失敗通知.");
				} else {
					log.error("台幣二扣作業發送資料庫連線失敗通知失敗");
				}

			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				log.error("Over 2hrs , STOP BATCH");
				try {
					log.debug("socket.close()");
					socket.close();
				} catch (IOException e1) {
					log.error("Socket close Error");
				}
				System.exit(0);
			} else {
				log.error("執行錯誤.", e);
			}
		}
		log.info("結束執行台幣預約二扣交易.");
		try {
			socket.close();
		} catch (IOException e1) {
			log.error("Socket close Error");
		}

	}

}
