package fstop.batch.main;

import fstop.batch.model.BatchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class BatchService {


    @Autowired
    RestTemplate restTemplate;

    @Autowired
	private EnvSetting envSetting;

    private String getServiceUrl(String funcUrl) {
        String s = envSetting.getPs_Url();
        if(s.endsWith("/") == false)
            s = s + "/";

        if(funcUrl.startsWith("/") == true)
            funcUrl = funcUrl.substring(1);
        return s + funcUrl;
    }

    public BatchResult doBatch(String beanName, String[] args) {
        List<String> params = new ArrayList();
        if(args != null)
            params.addAll(Arrays.asList(args));

        return restTemplate.postForObject(
                getServiceUrl(beanName) + "/" + args[0], params, BatchResult.class);
    }


}
