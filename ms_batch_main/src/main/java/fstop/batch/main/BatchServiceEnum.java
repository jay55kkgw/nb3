package fstop.batch.main;

import fstop.batch.model.BatchType;

public enum BatchServiceEnum {
    TW_TRANSFER(
            BatchType.builder()
                    .context("/batch/TwSchPay")
                    .instancePort(5103)
                    .build()
    ),
    TW_ReTRANSFER(
            BatchType.builder()
                    .context("/batch/TwSchRePay")
                    .instancePort(5103)
                    .build()
    );


    private BatchType type;

    BatchServiceEnum(BatchType type) {
        this.type = type;
    }

    public BatchType getBatchType() {
        return type;
    }
}
