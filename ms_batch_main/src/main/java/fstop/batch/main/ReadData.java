package fstop.batch.main;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import fstop.batch.model.BatchResult;

public class ReadData {
	public String readDataTw(BatchResult result) {
		String outString ="";
		DateTime now = new DateTime();
        String str_NowDate = now.toString("yyyyMMdd");
		outString += str_NowDate+"\n";
		outString += getTWDATETIMEStr()+"\n\n";
		outString += "錯誤代碼   錯誤訊息                                           筆數       \n";
		outString += "---------- -------------------------------------------------- ----------- \n";
		List<Map<String, String>> msgdata = result.getListdata().get(0);
		if(msgdata.size()!=0) {
			for (Map<String, String> data : msgdata) {
				String space = " ";
				String DPEXCODE = data.get("DPEXCODE");
				for (int m = 0; m < 10 - (DPEXCODE.trim().length()) * 1; m++)
					space += " ";
				DPEXCODE = DPEXCODE + space;
				space = " ";
				String DPEXCODEMSG = data.get("DPEXCODEMSG");
				DPEXCODEMSG = cutString(DPEXCODEMSG, 50);
				int word = countWordSpace(DPEXCODEMSG);
				for (int m = 0; m < 50 - word; m++)
					space += " ";
				DPEXCODEMSG = DPEXCODEMSG + space;
				space = " ";
				String TIMES = data.get("TIMES");
				for (int m = 0; m < 10 - (TIMES.trim().length()) * 1; m++)
					space += " ";
				TIMES = space + TIMES ;
				
				outString+=DPEXCODE+DPEXCODEMSG+TIMES+"\n";
			}
		}else {
			outString+="\n";
		}
		
		outString+="\n";
		outString += "☆☆★★☆☆★★ 需通知客戶資料 ☆☆★★☆☆★★  \n\n";
		outString += "預約編號     身分證號   交易代號   轉出帳號         轉入行 轉入帳號         金額             日期     時間   錯誤代碼   錯誤訊息  \n";
		outString += "------------ ---------- ---------- ---------------- ------ ---------------- ---------------- -------- ------ ---------- -------------------------------------------------- \n";

		List<Map<String, String>> trxdata = result.getListdata().get(1);
		if (trxdata.size() != 0) {
			for (Map<String, String> data : trxdata) {
				String space = " ";
				String DPSCHNO = data.get("DPSCHNO");
				for (int m = 0; m < 12 - (DPSCHNO.trim().length()) * 1; m++)
					space += " ";
				DPSCHNO = DPSCHNO + space;
				space = " ";
				String DPUSERID = data.get("DPUSERID");
				DPUSERID = mask(DPUSERID, 2, 3, '*');
				for (int m = 0; m < 10 - (DPUSERID.trim().length()) * 1; m++)
					space += " ";
				DPUSERID = DPUSERID + space;
				space = " ";
				String ADOPID = data.get("ADOPID");
				for (int m = 0; m < 10 - (ADOPID.trim().length()) * 1; m++)
					space += " ";
				ADOPID = ADOPID + space;
				space = " ";
				String DPWDAC = data.get("DPWDAC");
				for (int m = 0; m < 16 - (DPWDAC.trim().length()) * 1; m++)
					space += " ";
				DPWDAC = DPWDAC + space;
				space = " ";
				String DPSVBH = data.get("DPSVBH");
				for (int m = 0; m < 6 - (DPSVBH.trim().length()) * 1; m++)
					space += " ";
				DPSVBH = DPSVBH + space;
				space = " ";
				String DPSVAC = data.get("DPSVAC");
				for (int m = 0; m < 16 - (DPSVAC.trim().length()) * 1; m++)
					space += " ";
				DPSVAC = DPSVAC + space;
				space = " ";
				String DPTXAMT = data.get("DPTXAMT");
				for (int m = 0; m < 16 - (DPTXAMT.trim().length()) * 1; m++)
					space += " ";
				DPTXAMT = DPTXAMT + space;
				space = " ";
				String LASTDATE = data.get("LASTDATE");
				for (int m = 0; m < 8 - (LASTDATE.trim().length()) * 1; m++)
					space += " ";
				LASTDATE = LASTDATE + space;
				space = " ";
				String LASTTIME = data.get("LASTTIME");
				for (int m = 0; m < 6 - (LASTTIME.trim().length()) * 1; m++)
					space += " ";
				LASTTIME = LASTTIME + space;
				space = " ";
				String DPEXCODE = data.get("DPEXCODE");
				for (int m = 0; m < 10 - (DPEXCODE.trim().length()) * 1; m++)
					space += " ";
				DPEXCODE = DPEXCODE + space;
				space = " ";
				String DPEXCODEMSG = data.get("DPEXCODEMSG");
				DPEXCODEMSG = cutString(DPEXCODEMSG, 50);
				int word = countWordSpace(DPEXCODEMSG);
				for (int m = 0; m < 50 - word; m++)
					space += " ";
				DPEXCODEMSG = DPEXCODEMSG + space;

				outString += DPSCHNO + DPUSERID + ADOPID + DPWDAC + DPSVBH + DPSVAC + DPTXAMT + LASTDATE + LASTTIME
						+ DPEXCODE + DPEXCODEMSG + " \n";
			}
		}else {
			outString+="\n";
		}
		outString+="\n\n";
		
		Map resultMap = result.getData();
		String TOTAL=(String) resultMap.get("total");
		String SUCCESS= (String) resultMap.get("success");
		String CUSTOM_ERROR= (String) resultMap.get("custom_error");
		String SYSTEM_ERROR= (String) resultMap.get("system_error");
		String TOTAL_FAIL= (String) resultMap.get("total_fail");
		
		outString+="TOTAL           成功筆數  客戶操作面失敗筆數  系統面失敗筆數   TOTAL_FAIL \n";
		outString+="-----------  -----------  ------------------  --------------  ----------- \n";
		String space = "";
		for (int m = 0; m < 11 - (TOTAL.trim().length()) * 1; m++)
			space += " ";
		TOTAL = space + TOTAL  ;
		space = "  ";
		for (int m = 0; m < 11 - (SUCCESS.trim().length()) * 1; m++)
			space += " ";
		SUCCESS = space + SUCCESS;
		space = "  ";
		for (int m = 0; m < 18 - (CUSTOM_ERROR.trim().length()) * 1; m++)
			space += " ";
		CUSTOM_ERROR =space +  CUSTOM_ERROR;
		space = "  ";
		for (int m = 0; m < 14 - (SYSTEM_ERROR.trim().length()) * 1; m++)
			space += " ";
		SYSTEM_ERROR = space + SYSTEM_ERROR;
		space = "  ";
		for (int m = 0; m < 11 - (TOTAL_FAIL.trim().length()) * 1; m++)
			space += " ";
		TOTAL_FAIL = space + TOTAL_FAIL;
		
		outString +=TOTAL+SUCCESS+CUSTOM_ERROR+SYSTEM_ERROR+TOTAL_FAIL;
		
		return outString;
	}
	
	public String readDataFx(BatchResult result) {
		String outString ="";
		DateTime now = new DateTime();
        String str_NowDate = now.toString("yyyyMMdd");
		outString += str_NowDate+"\n";
		outString += getTWDATETIMEStr()+"\n\n";
		outString += "錯誤代碼   錯誤訊息                                           筆數       \n";
		outString += "---------- -------------------------------------------------- ----------- \n";
		List<Map<String, String>> msgdata = result.getListdata().get(0);
		if(msgdata.size()!=0) {
			for (Map<String, String> data : msgdata) {
				String space = " ";
				String FXEXCODE = data.get("FXEXCODE");
				for (int m = 0; m < 10 - (FXEXCODE.trim().length()) * 1; m++)
					space += " ";
				FXEXCODE = FXEXCODE + space;
				space = " ";
				String FXEXCODEMSG = data.get("FXEXCODEMSG");
				FXEXCODEMSG = cutString(FXEXCODEMSG, 50);
				int word = countWordSpace(FXEXCODEMSG);
				for (int m = 0; m < 50 - word; m++)
					space += " ";
				FXEXCODEMSG = FXEXCODEMSG + space;
				space = " ";
				String TIMES = data.get("TIMES");
				for (int m = 0; m < 10 - (TIMES.trim().length()) * 1; m++)
					space += " ";
				TIMES = space + TIMES ;
				
				outString+=FXEXCODE+FXEXCODEMSG+TIMES+"\n";
			}
		}else {
			outString+="\n";
		}
		
		outString+="\n\n";
		
		Map resultMap = result.getData();
		String TOTAL=(String) resultMap.get("total");
		String SUCCESS= (String) resultMap.get("success");
		String CUSTOM_ERROR= (String) resultMap.get("custom_error");
		String SYSTEM_ERROR= (String) resultMap.get("system_error");
		String TOTAL_FAIL= (String) resultMap.get("total_fail");
		
		outString+="TOTAL           成功筆數  客戶操作面失敗筆數  系統面失敗筆數   TOTAL_FAIL \n";
		outString+="-----------  -----------  ------------------  --------------  ----------- \n";
		String space = "";
		for (int m = 0; m < 11 - (TOTAL.trim().length()) * 1; m++)
			space += " ";
		TOTAL = space + TOTAL  ;
		space = "  ";
		for (int m = 0; m < 11 - (SUCCESS.trim().length()) * 1; m++)
			space += " ";
		SUCCESS = space + SUCCESS;
		space = "  ";
		for (int m = 0; m < 18 - (CUSTOM_ERROR.trim().length()) * 1; m++)
			space += " ";
		CUSTOM_ERROR =space +  CUSTOM_ERROR;
		space = "  ";
		for (int m = 0; m < 14 - (SYSTEM_ERROR.trim().length()) * 1; m++)
			space += " ";
		SYSTEM_ERROR = space + SYSTEM_ERROR;
		space = "  ";
		for (int m = 0; m < 11 - (TOTAL_FAIL.trim().length()) * 1; m++)
			space += " ";
		TOTAL_FAIL = space + TOTAL_FAIL;
		
		outString +=TOTAL+SUCCESS+CUSTOM_ERROR+SYSTEM_ERROR+TOTAL_FAIL;
		
		return outString;
	}

	private Integer countWordSpace(final String str) {
		int count = 0;
		char character;
		for (int n = 0; n < str.length(); n++) {
			character = str.charAt(n);
			// 中文or全形算2
			if (Character.UnicodeBlock.of(character) != Character.UnicodeBlock.BASIC_LATIN || !isHalfWidth(character)) {
				count = count + 2;
				continue;
			}
			// 半形算1
			count++;
		}

		return count;
	}

	private String cutString(final String str, int len) {
		int count = 0;
		char character;
		String resultStr = "";
		String[] array = str.split("");
		List<String> convertToL = Arrays.asList(array);
		for (int i = 0; i < convertToL.size(); i++) {
			character = convertToL.get(i).charAt(0);
			if (Character.UnicodeBlock.of(character) != Character.UnicodeBlock.BASIC_LATIN || !isHalfWidth(character)) {
				count = count + 2;
			} else {
				count++;
			}
			if (count >= len) {
				break;
			}
			resultStr += convertToL.get(i).toString();

		}
		return resultStr;
	}

	private boolean isHalfWidth(char c) {
		return '\u0000' <= c && c <= '\u00FF' || '\uFF61' <= c && c <= '\uFFDC' || '\uFFE8' <= c && c <= '\uFFEE';

	}

	/**
	 * 字串遮罩
	 * 
	 * @param text
	 * @param start
	 * @param length
	 * @param maskSymbol
	 * @return
	 */
	public static String mask(String text, int start, int length, char maskSymbol) {
		if (text == null || text.isEmpty()) {
			return "";
		}
		if (start < 0) {
			start = 0;
		}
		if (length < 1) {
			return text;
		}

		StringBuilder sb = new StringBuilder();
		char[] cc = text.toCharArray();
		for (int i = 0; i < cc.length; i++) {
			if (i >= start && i < (start + length)) {
				sb.append(maskSymbol);
			} else {
				sb.append(cc[i]);
			}
		}
		return sb.toString();
	}
	
	private String getTWDATETIMEStr() {
		Date now=new Date();
		SimpleDateFormat format=new SimpleDateFormat("MM月dd日 E HH時mm分ss秒 ");
		String part2=format.format(now);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR,-1911);
		
		String part1 = "查詢時間 : 中華民國"+cal.get(Calendar.YEAR)+"年";
		return part1+part2;
	}
	
}
