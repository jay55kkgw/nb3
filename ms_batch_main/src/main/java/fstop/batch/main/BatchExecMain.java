package fstop.batch.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import fstop.batch.model.BatchResult;
import fstop.batch.util.ESAPIUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class BatchExecMain extends AbstractBatchExecMain {

    @Autowired
    RestTemplate restTemplate;
    
    @Autowired
    ExecutorService executorService;

    @Autowired
    EnvSetting envSetting;
    
    
    public void executeMain(String[] args) {

    	String apiName = "";
    	if(args ==null || args.length == 0) {
    		log.error("未輸入任何參數  BatchExecMain 未執行");
    	}
//    	if(args.length < 1) {
//    		log.error("缺少第2個參數  BatchExecMain 未執行");
//    	}
    	
//        initSingleInstance(BatchExecMainPORT, "[ERROR]BatchExecMain,["+args[0]+"]交易正在執行 ......");

        apiName = args[0];
        log.info(ESAPIUtil.vaildLog("apiName>> " + apiName));
        List<String> argsList = new ArrayList();
		argsList.addAll(Arrays.asList(args));
		if(argsList.size() > 0)
			argsList.remove(0); // 移除第一個參數 ( beanName )
		
		
        BatchResult dataResult = service_doBatch("ms_ps" , apiName ,argsList.toArray(new String[]{}));

        log.info(ESAPIUtil.vaildLog("response: " + new Gson().toJson(dataResult)));



    }
}


