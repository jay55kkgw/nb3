package fstop.batch.main;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class EnvSetting {

    @Value("${path.dataFolderPathr}")
    private String dataFolder;
    
    @Value("${fstop.batch.baseUrl}")
    private String baseUrl; //local: http://localhost:
    
    @Value("${fstop.batch.ms_ps}")
    private String ps_Url;
    
    @Value("${fstop.batch.mstw.port}")
    private String ms_tw_p;
    @Value("${fstop.batch.mscc.port}")
    private String ms_cc_p;
    @Value("${fstop.batch.msloan.port}")
    private String ms_loan_p;
    @Value("${fstop.batch.mspay.port}")
    private String ms_pay_p;
    @Value("${fstop.batch.msps.port}")
    private String ms_ps_p;
    @Value("${fstop.batch.msfx.port}")
    private String ms_fx_p;
    
    @Value("${fstop.batch.msfund.port}")
    private String ms_fund_p;
    
    @Value("${fstop.batch.msola.port}")
    private String ms_ola_p;
    
    @Value("${fstop.batch.msols.port}")
    private String ms_ols_p;
    
    @Value("${fstop.batch.msgold.port}")
    private String ms_gold_p;
    
    @Value("${fstop.batch.mstmra.port}")
    private String ms_tmra_p;
    
    @Value("${fstop.batch.nb3.port}")
    private String NB3_p;
    
    @Value("${log.backup.ap.from.path}")
    private String log_backup_ap_frome_path;
    
    @Value("${log.backup.batchlogs.from.path}")
    private String log_backup_batchlogs_from_path;
    
    @Value("${log.backup.batlog.from.path}")
    private String log_backup_batlog_from_path;
    
    
    @Value("${log.backup5y.path}")
    private String log_backup5y_path;
    @Value("${log.backup3m.path}")
    private String log_backup3m_path;
    
    
    @Value("${apache_ap1}")
    private String apache_ap1;
    @Value("${apache_ap2}")
    private String apache_ap2;
}
