package fstop.batch.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.net.ServerSocket;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import fstop.batch.model.BatchResult;
import fstop.batch.model.FxExecuteOneRequest;
import fstop.batch.util.CSVReader;
import fstop.batch.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TxnFxTransferClient extends AbstractBatchExecMain {

	@Autowired
	private EnvSetting envSetting;

	public void executeMain(String[] args) {

		ServerSocket socket = null;

		try {

			socket = initSingleInstance(FxTxnMainPORT, "[ERROR]外幣預約轉即時交易正在執行 ......");

			BatchResult dataResult = service_doBatch("ms_ps", "txnfxtransfer", new String[] { "getAllList" });
			if (dataResult.isSuccess()) {
				log.info("FX_FXSCHNOs: " + dataResult.getData().get("FX_FXSCHNOs"));
				log.info("response: " + new Gson().toJson(dataResult));

				StringReader r = new StringReader((String) dataResult.getData().get("FX_FXSCHNOs"));

				CSVReader reader = null;
				try {
					reader = new CSVReader(r);
					String[] line = null;
					while ((line = reader.readNext()) != null) {

						try {
							String FXSCHNO = line[0];
							String FXSCHTXDATE = line[1];
							String FXUSERID = line[2];

							FxExecuteOneRequest request = new FxExecuteOneRequest();
							request.setFXSCHNO(FXSCHNO);
							request.setFXSCHTXDATE(FXSCHTXDATE);
							request.setFXUSERID(FXUSERID);

							log.info("開始執行, 預約編號  FXSCHNO: " + FXSCHNO + ", FXSCHTXDATE: " + FXSCHTXDATE + ", FXUSERID:"
									+ FXUSERID);
							BatchResult result = service_doBatch("ms_ps", "txnfxtransfer",
									new String[] { "executeone", FXSCHNO, FXSCHTXDATE, FXUSERID });
							if (!result.isSuccess())
								log.error("預約編號 FXSCHNO: " + FXSCHNO + ", FXSCHTXDATE: " + FXSCHTXDATE + ", FXUSERID:"
										+ FXUSERID + ", 發生錯誤.");
							else
								log.error("預約編號 FXSCHNO: " + FXSCHNO + ", FXSCHTXDATE: " + FXSCHTXDATE + ", FXUSERID:"
										+ FXUSERID + ", 成功.");

							log.info("execute result: " + new Gson().toJson(result));
						} catch (Exception e) {
							log.error("execute error.", e);
						}
					}

				} catch (RuntimeException e) {
					log.error("發生錯誤 , txnfxtransfer/executeone error.", e);
				} catch (Exception e) {
					log.error("發生錯誤 , txnfxtransfer/executeone error.", e);

				} finally {
					try {
						if (reader != null)
							reader.close();
					} catch (Exception e) {
					}
				}

				log.info("結束執行外幣預約交易.");

				// 紀錄預約轉即時(一扣)交易完成之時間
				try {
					File f = new File(envSetting.getDataFolder(), "FxTxnMain_Timestamp.txt");
					f.setWritable(true,true);
					f.setReadable(true, true);
					f.setExecutable(true,true);

					DateTime now = new DateTime();
					String str_NowDate = now.toString("yyyy/MM/dd");

					FileUtils.writeStringToFile(f, str_NowDate, "UTF-8");

					log.info("外幣一扣作業已執行完成.");

					// 一扣完成需發送 Email通知
					BatchResult mailresult = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "0", "FX", "" });

					if (mailresult.isSuccess()) {
						log.info("外幣一扣作業已發送完成通知.\n" + JSONUtils.toJson(mailresult));
					} else {
						log.error("外幣一扣作業發送完成通知 失敗 \n" + JSONUtils.toJson(mailresult));
					}

				} catch (Exception e) {
					log.error("寫入 FxTxnMain_Timestamp.txt 錯誤.", e);

					// 寫入 FxTxnMain_Timestamp.txt 錯誤 -> 需發送 Email通知
					BatchResult mailresult = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "1", "FX", e.getMessage() });
					if (mailresult.isSuccess()) {
						log.info("外幣一扣作業已發送寫檔異常通知.");
					} else {
						log.error("外幣一扣作業發送寫檔異常通知失敗");
					}

				}

				// 紀錄一扣後特殊錯誤代碼之預約交易
				String outString = "";
				try {

					File f = new File(envSetting.getDataFolder(), "fx_failmsgfile_1call.txt");
					f.setWritable(true,true);
					f.setReadable(true, true);
					f.setExecutable(true,true);
					
					FileWriter fw = new FileWriter(f);
					DateTime now = new DateTime();
					String str_NowDate = now.toString("yyyyMMdd");
					BatchResult result = service_doBatch("ms_ps", "txnfxtransfer",
							new String[] { "getErrorFileList", str_NowDate });
					ReadData readData = new ReadData();
					outString = readData.readDataFx(result);
					FileUtils.writeStringToFile(f, outString, "UTF-8");

					BatchResult mailresult = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "5", "FX", outString });

					if (mailresult.isSuccess()) {
						log.info("外幣一扣通知Email成功.");
					} else {
						log.error("外幣一扣通知Email失敗.");
					}
				} catch (Exception e) {
					log.error("寫入 fx_failmsgfile_1call.txt 錯誤.", e);

					// 寫入 tw_failmsgfile_1call.txt 錯誤 -> 需發送 Email通知
					BatchResult mailresult = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "6", "FX", e.getMessage() });

					if (mailresult.isSuccess()) {
						log.info("外幣一扣作業已發送寫檔(fx_failmsgfile_1call)異常通知.");
					} else {
						log.info("外幣一扣作業發送寫檔(fx_failmsgfile_1call)異常通知失敗");
					}
				}
			} else {
				log.error("發生錯誤 , txnfxtransfer/getAllList error. >> {}", dataResult.getErrorCode());
				if ("MS_PS_ERROR".equals(dataResult.getErrorCode())) {
					// PS都連線失敗了無法寄信啊啊啊
					// result = service_doBatch("ms_ps", "batchmailsender",
					// new String[] { "executeone", "10", "FX", "微服務連線失敗" });
					log.error("MS_PS 連線失敗");
				} else if ("DB_ERROR".equals(dataResult.getErrorCode())) {
					dataResult = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "9", "FX", "資料庫連線失敗" });
					if (dataResult.isSuccess()) {
						log.info("外幣一扣作業已發送資料庫連線失敗通知.");
					} else {
						log.error("外幣一扣作業發送資料庫連線失敗通知失敗");
					}
				} else if ("RestTemplate_Over2hr".equals(dataResult.getErrorCode())) {
					dataResult = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "9", "FX", "批次執行超過兩小時,判定失敗" });
					if (dataResult.isSuccess()) {
						log.info("外幣一扣作業已發送超時通知.");
					} else {
						log.error("外幣一扣作業發送超時通知失敗");
					}
				}
			}
			log.info("結束執行外幣預約交易.");
		} catch (Exception e) {
			log.error("TxnFxTransferClient ERROR", e.getMessage());
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				log.error("Socket close Error");
			}
		}
	}
}
