package fstop.batch.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fstop.batch.util.ESAPIUtil;
import lombok.extern.slf4j.Slf4j;


//http://www.aqee.net/post/manipulating-files-in-java-7.html
@Slf4j
@Component
public class LogBackup extends AbstractBatchExecMain {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ExecutorService executorService;

	@Autowired
	EnvSetting envSetting;

	public void executeMain(String[] args) {
		String basePath ="" ,key = "", newPath ="";
		if (args == null || args.length == 0) {
			log.warn("未輸入任何參數  ..");
		} else {
			List<String> argsList = new ArrayList();
			argsList.addAll(Arrays.asList(args));
			log.info(ESAPIUtil.vaildLog("輸入參數為 >> "+ argsList));
			// if(argsList.size() > 0)
			// argsList.remove(0); // 移除第一個參數 ( beanName )
			key = argsList.get(0);
		}
		
		switch (key) {
		case "5y"://3m25y
			basePath = envSetting.getLog_backup3m_path();
			basePath = basePath.endsWith(File.separator)?basePath:basePath+File.separator;
			newPath = envSetting.getLog_backup5y_path();
			newPath = newPath.endsWith(File.separator)?newPath:newPath+File.separator;
			move2Backup5y(basePath ,newPath);
			break;

		default:// log2 3m
//			APlog
			basePath = envSetting.getLog_backup_ap_frome_path();
			basePath = basePath.endsWith(File.separator)?basePath:basePath+File.separator;
			newPath = envSetting.getLog_backup3m_path();
			newPath = newPath.endsWith(File.separator)?newPath:newPath+File.separator+"NB3AP";
			move2Backup3m(basePath ,newPath);
//			批次log
			basePath = envSetting.getLog_backup_batchlogs_from_path();
			basePath = basePath.endsWith(File.separator)?basePath:basePath+File.separator;
			newPath = envSetting.getLog_backup3m_path();
			newPath = newPath.endsWith(File.separator)?newPath:newPath+File.separator+"NB3BATCH";
			move2Backup3m(basePath ,newPath);
//			crontab log
			basePath = envSetting.getLog_backup_batlog_from_path();
			basePath = basePath.endsWith(File.separator)?basePath:basePath+File.separator;
			newPath = envSetting.getLog_backup3m_path();
			newPath = newPath.endsWith(File.separator)?newPath:newPath+File.separator+"NB3BATCH";
			move2Backup3m(basePath ,newPath);
			break;
		}
		// 這裡之後改發mail
		// BatchResult dataResult = service_doBatch("ms_ps" , apiName
		// ,argsList.toArray(new String[]{}));

	}

	/**
	 * 
	 * @param oldPath
	 * @param newPath
	 */
	public void moveFolder(String fromPath, String newPath) {
		// 先複製檔案
		copyFolder(fromPath, newPath);
		// 則刪除原始檔，以免複製的時候錯亂
		deleteDir(new File(fromPath));
	}
	
	
	
	
	
	/**
	 * 把昨天以前的log先搬到backup3M
	 */
	public void move2Backup3m(String basePath , String newPath) {
		log.info("move2Backup3m...");
		try {
//			String basePath = envSetting.getLog_backup_ap_frome_path();
//			String newPath = envSetting.getLog_backup3m_path();
			basePath = basePath.endsWith(File.separator)?basePath:basePath+File.separator;
			newPath = newPath.endsWith(File.separator)?newPath:newPath+File.separator;
			
			log.info("begin copyFile...");
			moveFile(basePath, newPath , 2);
			
			log.info("begin delete....");
			deleteEmptyFolder(basePath, newPath);
			
			log.info("move2Backup3m end..");
//			TODO 刪除上上月的檔案
		} catch (Exception e) {
			log.error("move2Backup3m fail..>>{}",e.toString());
		}
	}
	
	
	
	/**
	 * 把檔案移動到Backup5y
	 * @param basePath
	 * @param newPath
	 */
	public void move2Backup5y(String basePath , String newPath) {
		log.info("move2Backup5y...");
		try {
			
//			String basePath = envSetting.getLog_backup3m_path();
//			String newPath = envSetting.getLog_backup5y_path();
			basePath = basePath.endsWith(File.separator)?basePath:basePath+File.separator;
			newPath = newPath.endsWith(File.separator)?newPath:newPath+File.separator;
			log.info(ESAPIUtil.vaildLog("basePath>> "+basePath));
			log.info(ESAPIUtil.vaildLog("newPath>> "+newPath));
			log.info("begin ,move....");
			moveFile(basePath, newPath, 30*3);
			
			log.info("begin delete....");
			deleteEmptyFolder(basePath, newPath);
			log.info("move2Backup5y end..");
		} catch (Exception e) {
			log.error("move2Backup5y fail..>>{}",e.toString());
		}
		
		
	}
	
	
	
	
	public void moveKeepOne(String fromPath, String newPath ) {
		// 先複製檔案
		copyFolder(fromPath, newPath);
		// 則刪除原始檔，只保留當天和前一天
		deleteKeepOneDay(new File(fromPath) );
	}

	/**
	 * 取得輸入當前月份的的一年前 月清單格式為yyyy-MM
	 * 
	 * @param date
	 * @return
	 */
	public List<String> getMonuthList(DateTime date , Integer monthsRange) {
		List<String> monthList = new ArrayList<>();

		try {
			for (int i = 0; i < monthsRange; i++) {
				monthList.add(date.minusMonths(i).toString("yyyy-MM"));
			}
		} catch (Exception e) {
			log.error("{}", e.toString());
		}
		log.info(ESAPIUtil.vaildLog("monthList>> "+ monthList));
		return monthList;

	}
	
	/**
	 * 
	 * @param date
	 * @return
	 */
	public List<String> getDayList(DateTime date ,Integer keepDay) {
		List<String> dayList = new ArrayList<>();
		
		try {
			for (int i = 0; i < keepDay; i++) {
				dayList.add(date.minusDays(i).toString("yyyy-MM-dd"));
			}
		} catch (Exception e) {
			log.error("{}", e.toString());
		}
		log.info(ESAPIUtil.vaildLog("dayList>>"+ dayList));
		return dayList;
		
	}

	
	
	public static void copyFolder(String oldPath, String newPath) {
		
		FileInputStream input = null;
		FileOutputStream output = null;
		File temp = null;
		File newtemp = null;
		String targetPath="";
		byte[] bufferarray = null;
		
		try {
			log.info(ESAPIUtil.vaildLog("oldPath>> "+ oldPath));
			log.info("newPath>>{}", newPath);
			// 如果資料夾不存在，跳出警告訊息 結束
			// (new File(newPath)).mkdirs();
			// 讀取整個資料夾的內容到file字串陣列，下面設定一個遊標i，不停地向下移開始讀這個陣列
			File filelist = new File(oldPath);
			filelist.setWritable(true,true);
			filelist.setReadable(true, true);
			filelist.setExecutable(true,true);
			String[] file = filelist.list();
			
//			log.debug("file>>{}", file);
			
			if (!newPath.endsWith(File.separator)) {
				newPath = newPath+File.separator;
			} 
			
			
			for (int i = 0; i < file.length; i++) {
				targetPath = newPath;
				if (oldPath.endsWith(File.separator)) {
					temp = new File(oldPath + file[i]);
				} else {
					temp = new File(oldPath + File.separator + file[i]);
				}
				
				// 如果是檔案
				if (temp.isFile()) {
					input = new FileInputStream(temp);
					log.trace(ESAPIUtil.vaildLog("temp>> "+ temp.getParentFile().getPath()));
					log.trace(ESAPIUtil.vaildLog("tempgetParent>> "+ temp.getParent()));
					newtemp = new File(targetPath);
					newtemp.setWritable(true,true);
					newtemp.setReadable(true, true);
					newtemp.setExecutable(true,true);
					if (!newtemp.exists()) {
						newtemp.mkdirs();
					}
					targetPath = targetPath+temp.getName();
					// 複製
					output = new FileOutputStream(targetPath);
					bufferarray = new byte[1024 * 64];
					int prereadlength;
					while ((prereadlength = input.read(bufferarray)) != -1) {
						output.write(bufferarray, 0, prereadlength);
					}
					output.flush();
					output.close();
					input.close();
				}
				// 如果遊標遇到資料夾，略過 因為應該只有.gz
				if (temp.isDirectory()) {
					log.warn(ESAPIUtil.vaildLog("temp is isDirectory >> "+ temp.getPath()));
					// copyFolder(oldPath + "/" + file[i], newPath + "/" + file[i]);
				}
			}
		} catch (Exception e) {
			log.error("複製整個資料夾內容操作出錯");
			log.error("{}",e.toString());
		} finally {
			
			try {
				if (output != null) {
					output.flush();
					output.close();
				}
				if (input != null) {
					
					input.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error("{}",e.toString());
			}
		}
	}
	
	
	/**
	 * 
	 * 檔案複製
	 * @param oldPath 複製來源路徑
	 * @param newPath 複製目標路徑
	 * @param beforeDay 幾天前
	 */
	public static void copyFile(String oldPath, String newPath ,Integer beforeDay) {
		
		File temp = null;
		File newtemp = null;
		String targetPath="";
		byte[] bufferarray = null;
		
		try {
			log.info(ESAPIUtil.vaildLog("oldPath>> "+ oldPath));
			log.info(ESAPIUtil.vaildLog("newPath>> "+ newPath));
			// 如果資料夾不存在，跳出警告訊息 結束
			// (new File(newPath)).mkdirs();
			// 讀取整個資料夾的內容到file字串陣列，下面設定一個遊標i，不停地向下移開始讀這個陣列
			File filelist = new File(oldPath);
			String[] file = filelist.list();
//			log.debug("file>>{}", file);
			
			if (!newPath.endsWith(File.separator)) {
				newPath = newPath+File.separator;
			} 
			
			
			for (int i = 0; i < file.length; i++) {
				targetPath = newPath;
				if (oldPath.endsWith(File.separator)) {
					temp = new File(oldPath + file[i]);
				} else {
					temp = new File(oldPath + File.separator + file[i]);
				}
				log.trace(ESAPIUtil.vaildLog("isBefore>> "+new DateTime().minusDays(beforeDay).isAfter(temp.lastModified())));
				// 如果是檔案
				if (temp.isFile() && new DateTime().minusDays(beforeDay).isAfter(temp.lastModified())) {
					log.trace(ESAPIUtil.vaildLog("temp>> "+ temp.getParentFile().getPath()));
					log.trace(ESAPIUtil.vaildLog("tempgetPant>> " + temp.getParent()));
					newtemp = new File(targetPath);
					if (!newtemp.exists()) {
						newtemp.mkdirs();
					}
					targetPath = targetPath+temp.getName();
					log.trace(ESAPIUtil.vaildLog("new targetPath >> "+targetPath));
					// 複製
					
					Path source = Paths.get(temp.getPath());
					Path target = Paths.get(targetPath);
					
					Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
				}
				// 如果遊標遇到資料夾，call back
				if (temp.isDirectory()) {
					
					log.warn("temp is isDirectory >>{}", temp.getPath());
					
					targetPath = !newPath.endsWith(File.separator)?  newPath+File.separator+temp.getName() : newPath+temp.getName(); 
					log.warn(ESAPIUtil.vaildLog("newPath II >> "+ targetPath));
					copyFile(temp.getPath(), targetPath ,beforeDay);
				}
			}
		} catch (Exception e) {
			log.error("複製整個資料夾內容操作出錯");
			log.error("{}",e.toString());
		} 
	}
	
	
	/**
	 * 
	 * 檔案移動
	 * @param oldPath 來源路徑
	 * @param newPath 目標路徑
	 * @param beforeDay 幾天前
	 */
	public static void moveFile(String oldPath, String newPath ,Integer beforeDay) {
		
		File temp = null;
		File newtemp = null;
		String targetPath="";
		
		try {
			log.info(ESAPIUtil.vaildLog("oldPath>> "+ oldPath));
			log.info(ESAPIUtil.vaildLog("newPath>> "+ newPath));
			// 如果資料夾不存在，跳出警告訊息 結束
			// (new File(newPath)).mkdirs();
			// 讀取整個資料夾的內容到file字串陣列，下面設定一個遊標i，不停地向下移開始讀這個陣列
			File filelist = new File(oldPath);
			String[] file = filelist.list();
//			log.debug("file>>{}", file);
//			空目錄 整個刪掉
			if(file !=null && file.length==0) {
				try {
					Files.delete(Paths.get(temp.getPath()));
				} catch (Exception e) {
					log.warn("delete fail ..");
				}
			}
			if (!newPath.endsWith(File.separator)) {
				newPath = newPath+File.separator;
			} 
			
			
			for (int i = 0; i < file.length; i++) {
				targetPath = newPath;
				if (oldPath.endsWith(File.separator)) {
					temp = new File(oldPath + file[i]);
				} else {
					temp = new File(oldPath + File.separator + file[i]);
				}
				log.trace(ESAPIUtil.vaildLog("isBefore>> "+new DateTime().minusDays(2).isAfter(temp.lastModified())));
				// 如果是檔案
				if (temp.isFile() && new DateTime().minusDays(2).isAfter(temp.lastModified())) {
					log.trace(ESAPIUtil.vaildLog( "temp>> "+ temp.getParentFile().getPath()));
					log.trace(ESAPIUtil.vaildLog("tempgetPant>> "+ temp.getParent()));
					newtemp = new File(targetPath);
					if (!newtemp.exists()) {
						newtemp.mkdirs();
					}
					targetPath = targetPath+temp.getName();
					log.trace(ESAPIUtil.vaildLog("new targetPath >> "+targetPath));
					// 複製
					
					Path source = Paths.get(temp.getPath());
					Path target = Paths.get(targetPath);
					Files.move(source, target, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.ATOMIC_MOVE);
				}
				// 如果遊標遇到資料夾，call back
				if (temp.isDirectory()) {
					log.warn(ESAPIUtil.vaildLog("temp is isDirectory >>"+ temp.getPath()));
					targetPath = !newPath.endsWith(File.separator)?  newPath+File.separator+temp.getName() : newPath+temp.getName(); 
					log.warn(ESAPIUtil.vaildLog("newPath II >>"+ targetPath));
					moveFile(temp.getPath(), targetPath ,beforeDay);
				}
			}
		} catch (Exception e) {
			log.error("移動整個資料夾內容操作出錯");
			log.error("{}",e.toString());
		} 
	}
	
	/**
	 * 刪除空目錄
	 * @param oldPath
	 * @param newPath
	 * @param beforeDay
	 */
	public static void deleteEmptyFolder(String oldPath, String newPath) {
		
		File temp = null;
		File newtemp = null;
		String targetPath="";
		
		try {
			log.info(ESAPIUtil.vaildLog("oldPath>> "+ oldPath));
			log.info(ESAPIUtil.vaildLog("newPath>>" + newPath));
			// 如果資料夾不存在，跳出警告訊息 結束
			// (new File(newPath)).mkdirs();
			// 讀取整個資料夾的內容到file字串陣列，下面設定一個遊標i，不停地向下移開始讀這個陣列
			File filelist = new File(oldPath);
			filelist.setWritable(true,true);
			filelist.setReadable(true, true);
			filelist.setExecutable(true,true);
			String[] file = filelist.list();
//			log.debug("file>>{}", file);

			if (!newPath.endsWith(File.separator)) {
				newPath = newPath+File.separator;
			} 
			
			
			for (int i = 0; i < file.length; i++) {
				targetPath = newPath;
				if (oldPath.endsWith(File.separator)) {
					temp = new File(oldPath + file[i]);
				} else {
					temp = new File(oldPath + File.separator + file[i]);
				}
				log.trace(ESAPIUtil.vaildLog("isBefore>> "+new DateTime().minusDays(2).isAfter(temp.lastModified())));
				// 如果遊標遇到資料夾，call back
				if (temp.isDirectory()) {
					log.warn(ESAPIUtil.vaildLog("temp is isDirectory >>"+ temp.getPath()));
//					空目錄 整個刪掉
					try {
						Files.delete(Paths.get(temp.getPath()));
					} catch (Exception e) {
						log.warn("not Empty Folder delete fail...");
					}
				}
			}
		} catch (Exception e) {
			log.error("移動整個資料夾內容操作出錯");
			log.error("{}",e.toString());
		} 
	}

	/**
	 * 刪除某個目錄及目錄下的所有子目錄和檔案
	 * @param dir
	 * @return
	 */
	public static boolean deleteDir(File dir) {
		// 如果是資料夾
		if (dir.isDirectory()) {
			// 則讀出該資料夾下的的所有檔案
			String[] children = dir.list();
			// 遞迴刪除目錄中的子目錄下
			for (int i = 0; i < children.length; i++) {
				// File f=new File（String parent ，String child）
				// parent抽象路徑名用於表示目錄，child 路徑名字串用於表示目錄或檔案。
				// 連起來剛好是檔案路徑
				boolean isDelete = deleteDir(new File(dir, children[i]));
				// 如果刪完了，沒東西刪，isDelete==false的時候，則跳出此時遞迴
				if (!isDelete) {
					return false;
				}
			}
		}
		// 讀到的是一個檔案或者是一個空目錄，則可以直接刪除
		return dir.delete();
	}
	public static boolean deleteKeepOneDay(File dir ) {
//		2019-07-18
		String keep = new DateTime().toString("yyyy-MM-dd");
		String keep1 = new DateTime().minusDays(1).toString("yyyy-MM-dd");
//		String keep = "2019-07-19";
		log.info(ESAPIUtil.vaildLog("keep>> "+keep));
		// 如果是資料夾
		if (dir.isDirectory()) {
			// 則讀出該資料夾下的的所有檔案
			String[] children = dir.list();
			// 遞迴刪除目錄中的子目錄下
			for (int i = 0; i < children.length; i++) {
				// File f=new File（String parent ，String child）
				// parent抽象路徑名用於表示目錄，child 路徑名字串用於表示目錄或檔案。
				// 連起來剛好是檔案路徑
				String filename = children[i];
				log.info(ESAPIUtil.vaildLog("filename>> "+filename));
//				if(filename.indexOf(keep) >0) {
//					continue;
//				}
				boolean isDelete = deleteKeepOneDay(new File(dir, children[i]));
				// 如果刪完了，沒東西刪，isDelete==false的時候，則跳出此時遞迴
				if (!isDelete) {
					return false;
				}
			}
		}
		
		log.info(ESAPIUtil.vaildLog("dir.name>>"+dir.getName()));
		if(dir.getName().indexOf(keep)>0 || dir.getName().indexOf(keep1)>0 ) {
			return Boolean.TRUE;
		}else {
			
		return dir.delete();
		}
		// 讀到的是一個檔案或者是一個空目錄，則可以直接刪除
//		return dir.delete();
	}
}
