package fstop.batch.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import fstop.batch.model.BatchResult;
import fstop.batch.model.TwExecuteOneRequest;
import fstop.batch.util.CSVReader;
import fstop.batch.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TxnTwTransferClient extends AbstractBatchExecMain {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ExecutorService executorService;

	@Autowired
	EnvSetting envSetting;

	public void executeMain(String[] args) {

		String MSADDR = "";
		ServerSocket socket = null;
		try {

			socket = initSingleInstance(TwTxnMainPORT, "[ERROR]台幣預約轉即時交易正在執行 ......");

			BatchResult dataResult = service_doBatch("ms_ps", "txntwtransfer", new String[] { "getAllList" });
			if (dataResult.isSuccess()) {
				log.info("TW_DPSCHNOs: " + dataResult.getData().get("TW_DPSCHNOs"));
				log.info("response: " + new Gson().toJson(dataResult));

				StringReader r = new StringReader((String) dataResult.getData().get("TW_DPSCHNOs"));

				// String x = "19080718002,20190808,H120036595";
				// r = new StringReader(x);

				CSVReader reader = null;
				try {
					reader = new CSVReader(r);
					String[] line = null;
					while ((line = reader.readNext()) != null) {

						try {
							String DPSCHNO = line[0];
							String DPSCHTXDATE = line[1];
							String DPUSERID = line[2];
							// 2019/10/01 新增for不同微服務 BEN
							MSADDR = line[3].toLowerCase();
							// 2019/10/31 新增交易編號參數 BEN
							String SEQTRN = line[4];

							TwExecuteOneRequest request = new TwExecuteOneRequest();
							request.setDPSCHNO(DPSCHNO);
							request.setDPSCHTXDATE(DPSCHTXDATE);
							request.setDPUSERID(DPUSERID);
							request.setMSADDR(MSADDR);
							request.setADTXNO(SEQTRN);

							// 2019/10/01 新增for不同微服務 BEN
							log.info("開始執行, 預約編號  DPSCHNO: " + DPSCHNO + ", DPSCHTXDATE: " + DPSCHTXDATE + ", 微服務 :"
									+ MSADDR + ", 交易序號：" + SEQTRN);
							//
							// BatchResult result = service_doBatch("txntwtransfer", new
							// String[]{"executeone", DPSCHNO, DPSCHTXDATE} );
							// 2019/10/01 新增for微服務 BEN
							BatchResult result = service_doBatch("ms_ps", "txntwtransfer",
									new String[] { "executeone", DPSCHNO, DPSCHTXDATE, DPUSERID, SEQTRN, MSADDR });
							if (!result.isSuccess())
								log.error("預約編號 DPSCHNO: " + DPSCHNO + ", DPSCHTXDATE: " + DPSCHTXDATE + ", DPUSERID: "
										+ DPUSERID + ", 發生錯誤.");
							else
								log.error("預約編號 DPSCHNO: " + DPSCHNO + ", DPSCHTXDATE: " + DPSCHTXDATE + ", DPUSERID: "
										+ DPUSERID + ", 成功.");

							log.info("execute result: " + new Gson().toJson(result));
						} catch (Exception e) {
							log.error("execute error.", e);
						}

					}

				} catch (RuntimeException e) {
					log.error("發生錯誤 , txntwtransfer/executeone error.", e);
				} catch (Exception e) {
					log.error("發生錯誤 , txntwtransfer/executeone error.", e);

				} finally {
					try {
						if (reader != null)
							reader.close();
					} catch (Exception e) {
					}
				}

				// 紀錄預約轉即時(一扣)交易完成之時間
				try {
					File f = new File(envSetting.getDataFolder(), "TwTxnMain_Timestamp.txt");
					f.setWritable(true,true);
					f.setReadable(true, true);
					f.setExecutable(true,true);
					DateTime now = new DateTime();
					String str_NowDate = now.toString("yyyy/MM/dd");

					FileUtils.writeStringToFile(f, str_NowDate, "UTF-8");

					log.info("台幣一扣作業已執行完成.");

					// 一扣完成需發送 Email通知
					BatchResult result = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "0", "TW", "" });

					log.info("台幣一扣作業已發送完成通知 >> {}", JSONUtils.toJson(result));
				} catch (Exception e) {
					log.error("寫入 TwTxnMain_Timestamp.txt 錯誤.", e);

					// 寫入 TwTxnMain_Timestamp.txt 錯誤 -> 需發送 Email通知
					BatchResult result = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "1", "TW", e.getMessage() });

					log.info("台幣一扣作業已發送寫檔(TwTxnMain_Timestamp)異常通知.");
				}

				// 紀錄一扣後特殊錯誤代碼之預約交易
				String outString = "";
				try {

					File f = new File(envSetting.getDataFolder(), "tw_failmsgfile_1call.txt");
					f.setWritable(true,true);
					f.setReadable(true, true);
					f.setExecutable(true,true);
					FileWriter fw = new FileWriter(f);
					DateTime now = new DateTime();
					String str_NowDate = now.toString("yyyyMMdd");
					BatchResult result = service_doBatch("ms_ps", "txntwtransfer",
							new String[] { "getErrorFileList", str_NowDate });
					ReadData readData = new ReadData();
					outString = readData.readDataTw(result);
					FileUtils.writeStringToFile(f, outString, "UTF-8");

					BatchResult mailresult = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "5", "TW", outString });

					if (mailresult.isSuccess) {
						log.info("台幣一扣傳送需通知客戶資料Email成功.");
					} else {
						log.error("台幣一扣傳送需通知客戶資料Email失敗.");
					}
				} catch (Exception e) {
					log.error("寫入 tw_failmsgfile_1call.txt 錯誤.", e);

					// 寫入 tw_failmsgfile_1call.txt 錯誤 -> 需發送 Email通知
					BatchResult result = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "6", "TW", e.getMessage() });

					log.info("台幣一扣作業已發送寫檔(tw_failmsgfile_1call)異常通知.");
				}
			} else {
				log.error("發生錯誤 , txntwtransfer/getAllList error. >> {}", dataResult.getErrorCode());
				if ("MS_PS_ERROR".equals(dataResult.getErrorCode())) {
					// PS都連線失敗了無法寄信啊啊啊
					// result = service_doBatch("ms_ps", "batchmailsender",
					// new String[] { "executeone", "10", "TW", "微服務連線失敗" });
					log.error("MS_PS 連線失敗");
				} else if ("DB_ERROR".equals(dataResult.getErrorCode())) {
					dataResult = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "9", "TW", "資料庫連線失敗" });
					if (dataResult.isSuccess()) {
						log.info("臺幣一扣作業已發送資料庫連線失敗通知.");
					} else {
						log.error("臺幣一扣作業發送資料庫連線失敗通知失敗");
					}
				}else if("RestTemplate_Over2hr".equals(dataResult.getErrorCode())) {
					dataResult = service_doBatch("ms_ps", "batchmailsender",
							new String[] { "executeone", "9", "FX", "批次執行超過兩小時,判定失敗" });
					if (dataResult.isSuccess()) {
						log.info("臺幣一扣作業已發送超時通知.");
					} else {
						log.error("臺幣一扣作業發送超時通知失敗");
					}
				}
			}

			log.info("結束執行台幣預約交易.");

		} catch (Exception e) {
			log.error("TxnTwTransferClient ERROR",e.getMessage());
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				log.error("Socket close Error");
			}
		}

	}
}
