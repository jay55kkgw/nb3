package fstop.batch.main;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fstop.batch.model.BatchResult;
import fstop.batch.util.ESAPIUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class FindFileChangeClient extends AbstractBatchExecMain {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ExecutorService executorService;

	@Autowired
	EnvSetting envSetting;

	public void executeMain(String[] args) {
		List<String> argsList = new ArrayList();
		String key = "";
		String ms = "";
		String apiName = "com/findfilechange";
		if (args == null || args.length == 0) {
			log.warn("未輸入任何參數  ..");
		} else {
			argsList.addAll(Arrays.asList(args));
			log.info(ESAPIUtil.vaildLog("輸入參數為>> " + argsList));
			ms = argsList.get(0);
		}

		argsList.remove(0);
		if("tmra".equals(ms)) {
			ms = ms.toUpperCase();
		}
		BatchResult dataResult = service_doBatch(ms , apiName,argsList.toArray(new String[]{}));
		log.info(ESAPIUtil.vaildLog("dataResult>> "+dataResult))  ;
	}
	

	public Boolean isRange(DateTime filedate, Integer range) {
		Boolean result = Boolean.FALSE;
		DateTime date = new DateTime();
		date = date.minusHours(range);
		log.info(ESAPIUtil.vaildLog("filedate>> "+ filedate.toString("yyyy-MM-dd HH:mm:ss")));
		result = filedate.isAfter(date);
		log.info("isRange>> "+ result);
		return result;

	}
  
	
	/**
	 * https://www.itread01.com/content/1547205846.html
	 * @param file
	 */
	public void listDir(File file , List<String> list ) {
//		List<String> list = new LinkedList<String>();
		
		log.trace(ESAPIUtil.vaildLog("file.getName() >>"+file.getName()));
		if (file.isDirectory()) { // 是一個目錄
			// 列出目錄中的全部內容
			File results[] = file.listFiles();
			if (results != null) {
				for (int i = 0; i < results.length; i++) {
					listDir(results[i] ,list); // 繼續一次判斷
				}
			}
		} else { // 是檔案
			
			DateTime filedate = new DateTime(file.lastModified());
			if(isRange(filedate, 1)) {
				list.add(file.getPath());
			}
		}
		// file.delete(); //刪除!!!!! 根目錄,慎操作
		// 獲取完整路徑
		log.info(ESAPIUtil.vaildLog("list>> "+list));
	}

}
