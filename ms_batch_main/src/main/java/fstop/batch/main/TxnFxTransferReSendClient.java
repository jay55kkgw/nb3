package fstop.batch.main;

import java.io.File;
import java.io.FileWriter;
import java.io.StringReader;
import java.util.concurrent.ExecutorService;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import fstop.batch.model.BatchResult;
import fstop.batch.model.FxExecuteOneRequest;
import fstop.batch.util.CSVReader;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TxnFxTransferReSendClient extends AbstractBatchExecMain {
	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ExecutorService executorService;

	@Autowired
	private EnvSetting envSetting;

	@Override
	public void executeMain(String[] args) {

		checkExistsInstance(FxTxnMainPORT, "[ERROR]外幣預約轉即時交易正在執行 ......");
		initSingleInstance(FxReTxnMainPORT, "[ERROR]外幣重送交易正在執行 ......");

		// 查詢預約轉即時交易完成之時間
		try {
			File fr = new File(envSetting.getDataFolder(), "FxTxnMain_Timestamp.txt");
			fr.setWritable(true,true);
			fr.setReadable(true, true);
			fr.setExecutable(true,true);
			
			String str_FileDate = FileUtils.readFileToString(fr, "UTF-8");

			log.info("外幣一扣作業 -> FxTxnMain_Timestamp 日期 : {}", str_FileDate);

			String str_NowDate = new DateTime().toString("yyyy/MM/dd");

			log.info("外幣一扣作業 -> 營業日期 : {}", str_NowDate);

			// 若預約轉即時交易尚未完成,不可執行二扣
			if (!str_FileDate.equals(str_NowDate)) {

				// 外幣一扣作業尚未執行完成 -> 需發送 Email通知
				BatchResult result = service_doBatch("ms_ps", "batchmailsender",
						new String[] { "executeone", "2", "FX", str_FileDate });

				log.info("外幣二扣作業已發送狀態檢查通知 >> {}", "外幣一扣作業尚未執行完成,不可執行二扣作業");

				System.exit(0);
			} else {

				// 外幣一扣作業已執行完成,準備開始執行二扣作業 -> 需發送 Email通知
				BatchResult result = service_doBatch("ms_ps", "batchmailsender",
						new String[] { "executeone", "3", "FX", "" });

				log.info("外幣二扣作業已發送狀態檢查通知 >> {}", "準備開始執行外幣二扣作業");
			}
		} catch (Exception e) {
			log.error("TxnFxTransferReSendClient", e);

			// 讀取 TwTxnMain_Timestamp.txt 錯誤 -> 需發送 Email通知
			BatchResult result = service_doBatch("ms_ps", "batchmailsender",
					new String[] { "executeone", "4", "FX", e.getMessage() });

			log.info("外幣二扣作業已發送異常通知 >> {}", "外幣二扣讀取 Timestamp 檔錯誤");

			System.exit(0);
		}

		String defaultTimeWait = "2000";

		String timeWaitTo = defaultTimeWait;

		if (args.length > 0) {
			boolean isDigit = false;
			try {
				int waitTo = new Integer(args[0]);
				isDigit = true;
			} catch (Exception e) {
			}
			if (isDigit) {
				timeWaitTo = args[0];
			}
		}

		log.info("等待 checkServerHealth 執行到 (HHmm):" + timeWaitTo);
		try {
			BatchResult result = service_doBatch("ms_ps", "checkServerHealth", new String[] { timeWaitTo });
			if (!result.isSuccess()) {
				log.info("checkServerHealth 回傳失敗.");

				System.exit(0);
			}
		} finally {
		}

		log.info("開始執行外幣預約重送(二扣).");
		String allIds = "";
		log.info("取得所有重送交易編號開始");
		BatchResult dataResult = service_doBatch("ms_ps", "txnfxtransferresend", new String[] { "getAllList" });
		if (dataResult.isSuccess()) {
			StringReader r = new StringReader((String) dataResult.getData().get("FX_FXSCHNOs"));

			log.info("FX_FXSCHNOs: " + dataResult.getData().get("FX_FXSCHNOs"));
			log.info("response: " + new Gson().toJson(dataResult));

			CSVReader reader = null;
			try {
				reader = new CSVReader(r);
				String[] line = null;
				while ((line = reader.readNext()) != null) {

					try {
						String FXSCHNO = line[0];
						String FXSCHTXDATE = line[1];
						String FXUSERID = line[2];

						FxExecuteOneRequest request = new FxExecuteOneRequest();
						request.setFXSCHNO(FXSCHNO);
						request.setFXSCHTXDATE(FXSCHTXDATE);
						request.setFXUSERID(FXUSERID);

						log.info("開始執行, 預約編號  FXSCHNO: " + FXSCHNO + ", FXSCHTXDATE: " + FXSCHTXDATE + ", FXUSERID:"
								+ FXUSERID);
						BatchResult result = service_doBatch("ms_ps", "txnfxtransferresend",
								new String[] { "executeone", FXSCHNO, FXSCHTXDATE, FXUSERID, "AUTO" });
						if (!result.isSuccess())
							log.error("預約編號 FXSCHNO: " + FXSCHNO + ", FXSCHTXDATE: " + FXSCHTXDATE + ", 發生錯誤.");
						else
							log.error("預約編號 FXSCHNO: " + FXSCHNO + ", FXSCHTXDATE: " + FXSCHTXDATE + ", 成功.");

						log.info("execute result: " + new Gson().toJson(result));
					} catch (Exception e) {
						log.error("execute error.", e);
					}
				}

			} catch (RuntimeException e) {
				log.error("發生錯誤 , txnfxtransfer/executeone error.", e);
			} catch (Exception e) {
				log.error("發生錯誤 , txnfxtransfer/executeone error.", e);

			} finally {
				try {
					if (reader != null)
						reader.close();
				} catch (Exception e) {
				}
			}

			log.info("結束執行外幣預約交易.");

			// 紀錄二扣後特殊錯誤代碼之預約交易
			String outString = "";
			try {

				File f = new File(envSetting.getDataFolder(), "fx_failmsgfile_2call.txt");
				f.setWritable(true,true);
				f.setReadable(true, true);
				f.setExecutable(true,true);
				
				FileWriter fw = new FileWriter(f);
				DateTime now = new DateTime();
				String str_NowDate = now.toString("yyyyMMdd");
				BatchResult result = service_doBatch("ms_ps", "txnfxtransferresend",
						new String[] { "getErrorFileList", str_NowDate });
				ReadData readData = new ReadData();
				outString = readData.readDataFx(result);
				FileUtils.writeStringToFile(f, outString, "UTF-8");

				BatchResult mailresult = service_doBatch("ms_ps", "batchmailsender",
						new String[] { "executeone", "7", "FX", outString });

				if (mailresult.isSuccess) {
					log.info("外幣二扣傳送通知Email成功.");
				} else {
					log.error("外幣二扣傳送通知Email失敗.");
				}
			} catch (Exception e) {
				log.error("寫入 fx_failmsgfile_2call.txt 錯誤.", e);

				// 寫入 fw_failmsgfile_2call.txt 錯誤 -> 需發送 Email通知
				BatchResult result = service_doBatch("ms_ps", "batchmailsender",
						new String[] { "executeone", "8", "FX", e.getMessage() });

				log.info("外幣一扣作業已發送寫檔(fx_failmsgfile_2call)異常通知.");
			}
		} else {
			BatchResult result = new BatchResult();
			log.error("發生錯誤 , txnfxtransferresend/getAllList error. >> {}", dataResult.getErrorCode());
			if ("MS_PS_ERROR".equals(dataResult.getErrorCode())) {
				// PS都連線失敗了無法寄信啊啊啊
				// result = service_doBatch("ms_ps", "batchmailsender",
				// new String[] { "executeone", "10", "TW", "微服務連線失敗" });
				log.error("MS_PS 連線失敗");
			} else if ("DB_ERROR".equals(dataResult.getErrorCode())) {
				result = service_doBatch("ms_ps", "batchmailsender",
						new String[] { "executeone", "10", "FX", "資料庫連線失敗" });
			}
			if (result.isSuccess()) {
				log.info("外幣二扣作業已發送資料庫連線失敗通知.");
			} else {
				log.error("外幣二扣作業發送資料庫連線失敗通知失敗");
			}

		}
		log.info("結束執行外幣預約二扣交易.");

	}
}
