package fstop;

import fstop.batch.main.AbstractBatchExecMain;
import fstop.batch.model.ValueKeeper;
import fstop.batch.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@SpringBootApplication(scanBasePackages= {"fstop.batch"})
public class Application  {

	public static void main(String[] args) {
		log.debug("args >>>>> {} ", args.toString());
		SpringApplication.run(Application.class, args);
	}
	
	//20200602 set 2 hours timeout
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
//		builder.setConnectTimeout(Duration.ofSeconds(1)).setReadTimeout(Duration.ofSeconds(1)).build();
//		builder.setConnectTimeout(Duration.ofSeconds(1)).setReadTimeout(Duration.ofSeconds(1)).build();
		
		return builder.setConnectTimeout(Duration.ofSeconds(1)).setReadTimeout(Duration.ofSeconds(7200)).build();
	}
	/**
	 * echo 用
	 *
	 * @param builder
	 * @return
	 */
	@Bean
	public RestTemplate echoRestTemplate(RestTemplateBuilder builder) {
		return builder.setConnectTimeout(Duration.ofSeconds(3)).setReadTimeout(Duration.ofSeconds(3)).build();
	}

//	@Autowired
//	private ApplicationContext context;

	@Bean
	public CommandLineRunner run(ApplicationContext context) throws Exception {
		return args -> {

			String beanName = ValueKeeper.of(StrUtils.trim(args[0])).getValue();

//			beanName = "twTxnMain";

			AbstractBatchExecMain main = (AbstractBatchExecMain)context.getBean(beanName);
			List<String> argsList = new ArrayList();
			argsList.addAll(Arrays.asList(args));

			if(argsList.size() > 0)
				argsList.remove(0); // 移除第一個參數 ( beanName )
			main.executeMain(argsList.toArray(new String[]{}));
		};
	}
}