package fstop;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public abstract class TestFactory
{

	static
	{

		// log暫時無法設定
		// System.setProperty("LOG_HOME","E:\\mylog\\ms_batch_main");
		// System.setProperty("LOG_FILE_NAME","ms_batch_main");

		// MS_TW PORT
		System.setProperty("MS_TW_PORT", "9086");
		// MS_FX PORT
		System.setProperty("MS_FX_PORT", "9088");
		// MS_CC_PORT
		System.setProperty("MS_CC_PORT", "9090");
		// MS_LOAN_PORT
		System.setProperty("MS_LOAN_PORT", "9091");
		// MS_PAY_PORT
		System.setProperty("MS_PAY_PORT", "9093");
		// MS_PS_PORT
		System.setProperty("MS_PS_PORT", "9094");
		// FSTOP_BATCH_MS_FX
		System.setProperty("FSTOP_BATCH_MS_FX", "http://localhost:9088/ms_fx/");
		// FSTOP_BATCH_MS_PS
		System.setProperty("FSTOP_BATCH_MS_PS", "http://localhost:9094/ms_ps/");
		// BASEURL
		System.setProperty("FSTOP_BATCH_BASEURL", "http://localhost:");

		// Batch 執行時產生檔案的路徑
		System.setProperty("PATH_DATAFOLDERPATHR", "E:/mylog/TBB/nnb/data/");

	}

	@Before
	public void beforeClass() throws Exception
	{
	}

	@Test
	public void testMain() throws Exception
	{
		log.debug("abstract  testMain Test OK");
	}

}
