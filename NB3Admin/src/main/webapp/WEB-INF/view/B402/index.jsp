<% response.setHeader("X-Frame-Options", "DENY"); %>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>

<head>
<title>台企銀 後台首頁</title>
<meta charset="UTF-8">

<!-- include 後台管理 css -->
<jsp:include page="../include/admheadercs.jsp"></jsp:include>

<!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
	<!-- include master page menu -->
	<jsp:include page="../include/admmenu.jsp"></jsp:include>

	<!-- body content start -->
	<div class="container" id="main-content">
		<h2>臺幣預約失敗人工處理</h2>
		<form id="B402" name="B402" class="container main-content-block p-4"
			action="<%= request.getContextPath() %>/B402/Query" method="post">
			<div class="form-group row">
				<label class="col-4 control-label text-right">身分證/營利事業統一編號：</label>
				<div class="col-4">
					<div class="form-inline">
						<input type="text" id="UserId" name="UserId"
							class="form-control upper" maxlength="10" />
					</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="offset-4 col-8">
					<input type="button" value="重新輸入" class="btn btn-default"	aria-label="Left Align" onclick="location.reload();" />&nbsp; 
					<input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQueryNTD()" />&nbsp;
					<c:if test="${__LOGIN_ROLES__=='DC'}">
						<input id="btnexpCSV" type="button" value="匯出CSV" class="btn btn-secondary" aria-label="Left Align" onclick="onExportCSV();" />&nbsp;
					</c:if>
				</div>
			</div>
		</form>
	</div>
	<c:if test="${__LOGIN_ROLES__=='DC'}">
		<div style="padding: 0 0 0 30">
			<input type='button' value='多筆重送' class='btn btn-default' onclick='onResendChecked();' />
		</div>
	</c:if>
	<table id="mainNTD" class="display p-4 transparent" cellspacing="0"
		style="width: 97%">
		<thead>
			<tr>
				<th style="width: 8%"><input type="checkbox" id="checkAll"
					onclick="onChecked(this);">預約編號</th>
				<th style="width: 8%">轉帳日期</th>
				<th style="width: 10%">身分證/營利事業統一編號</th>
				<th style="width: 10%">轉出帳號</th>
				<th style="width: 10%">轉入帳號/<br>繳費稅代號</th>
				<th style="text-align: right;width: 8%">轉帳金額</th>
				<th style="text-align: right;width: 8%">手續費</th>
				<th style="width: 10%">跨行序號</th>
				<th style="width: 10%">交易類別</th>
				<th style="width: 8%">轉帳結果</th>
				<th style="width: 5%">重送</th>
			</tr>
		</thead>
		<tbody id="mainBody">
		</tbody>
	</table>
	<h6>可人工重送錯誤代碼包含：</h6>
	<c:forEach items="${txs}" var="tx">
		<span class="badge badge-info">${tx.ADMCODE}：${tx.ADMSGIN}</span>
	</c:forEach>
	<br /><br />

	<!-- Modal -->
	<div class="modal fade bd-example-modal-sm" id="exampleModal"
		tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">轉帳結果</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div id="model-body" class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../include/caseStatus.jsp"></jsp:include>
	<!-- body content end -->

	<!-- include master page footer -->
	<jsp:include page="../include/admfooter.jsp"></jsp:include>

	<!-- include 後台管理 js -->
	<jsp:include page="../include/admfooterjs.jsp"></jsp:include>
	<script src="<%= request.getContextPath() %>/script/respond.js"></script>
	<script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
	
	<script>
        var jqData=[];
		var role="${__LOGIN_ROLES__}";
		
        $(document).ready(function () {
            onQueryNTD();
        }); //ready

        function onChecked(obj) {
            var isChecked = obj.checked;
            
            $("#mainBody input[type='checkbox']").prop('checked', isChecked);
        }
        
        function onDataBind() {
            $("#mainNTD").dataTable({
                "responsive": true,
                "processing": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "paging": true,
                "bLengthChange": false, 
                "aaData": jqData,
                "columnDefs": [
                        { targets: 0, orderable: false },
                        { targets: 5, className: 'dt-body-right' },
                        { targets: 6, className: 'dt-body-right' },
                        { targets: 10, orderable: false }
                        ], 
                "order": [[0, "asc"]],
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "twschpaydataidentity.dpschno" },
                    { "data": "twschpaydataidentity.dpschtxdate" },
                    { "data": "twschpaydataidentity.dpuserid" },
                    { "data": "dpwdac" },
                    { "data": "dpsvbh" },
                    { "data": "dptxamt" },
                    { "data": "dpefee" },
                    { "data": "dpstanno" },
                    { "data": "dpseq" },
                    { "data": "dptxstatus" },
                    { "data": "dptxstatus" }
                ],
                "createdRow": function (row, data, index) {
					var $dpschno = $("<input type='checkbox' />");
					$dpschno.attr("value", data.twschpaydataidentity.dpschno);
					$dpschno.attr("dpschtxdate", data.twschpaydataidentity.dpschtxdate);
					$dpschno.attr("dpuserid", data.twschpaydataidentity.dpuserid);
					$dpschno.attr("adtxno", data.adtxno);
					$("td", row).eq(0).text("").append($dpschno).append(data.twschpaydataidentity.dpschno);
                    
                    var $dpschtxdate = data.twschpaydataidentity.dpschtxdate;
                    var $date = $dpschtxdate.substr(0, 4) + "/" + $dpschtxdate.substr(4, 2) + "/" + $dpschtxdate.substr(6, 2);
                    $("td", row).eq(1).text("").append($date);

                    var $accountNo = data.dpsvbh + "<br>" + data.dpsvac;
                    $("td", row).eq(4).text("").append($accountNo);

                    $("td", row).eq(5).text("").append(formatMoney(data.dptxamt));

                    $("td", row).eq(6).text("").append(formatMoney(data.dpefee));

                    var $statusWord = "";
                    var $status = "";
                    switch (data.dptxstatus) {
                        case "0":
                            $statusWord = "成功";
                            break;
                        case "1":
                            $statusWord = "失敗";
                            break;
                        case "2":
                            $statusWord = "處理中";
                            break;
                        default:
                            $statusWord = "未執行";
                            break;
                    }
                    $status = $statusWord + '<br><a href="#" onclick="showMsgCode(\'' + data.dpexcode + '\')">' + data.dpexcode + '</a>';
                    $("td", row).eq(9).text("").append($status);
                    
					if (role=="DC")
					{
						var $resend = $("<input type='button' value='重送' class='btn btn-danger' />");
	                    $resend.attr("onclick","onDuplicateCheck('"+data.twschpaydataidentity.dpschno+"','"+data.twschpaydataidentity.dpschtxdate+"','"+data.twschpaydataidentity.dpuserid+"','"+data.adtxno+"')");
	                    $("td", row).eq(10).text("").append($resend);
					}
					else
					{
						 $("td", row).eq(10).text("").append("");
					}
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            return false;
        }

        function onQueryNTD() {
            $("span.error").remove();
            
            var UserId = $("#UserId").val();
            var url = "<%= request.getContextPath() %>/B402/IndexQueryNTD";
            lockScreen();
            $.post(url, {
                "USERID" : UserId,
            }, function (data, status) {
                unlockScreen();
                if (status == "success") {
                    //alert(data);
                    jqData = data;
                    onDataBind();
                    $("#checkAll").prop("checked", false);
                }
            });
        }

		var resendLen;		// 要重送的資料數
		var result;			// 重送結果

	    // 多筆重送
        function onResendChecked() {
			var $items = $("#mainBody input[type='checkbox']:checked");
    	 	if ( $items.length == 0 ) {
				alert('無可重送交易');
				return;
         	}
			var msg = "確定重送預約序號[";
 			$items.each(function(){
				msg += (this.value)+",";
	 		}); 
   		 	msg = msg.substring(0, msg.length-1);
			msg += "]-預約轉帳日交易?";
       	 	if ( confirm(msg) ) {
       	 		lockScreen();
       	 		result = "";
           	 	resendLen = $items.length;
       		 	$items.each(function(){
           		 	var dpschno = this.value;
           		 	var dpschtxdate = $(this).attr("dpschtxdate");
           		 	var dpuserid = $(this).attr("dpuserid");
           		 	var adtxno = $(this).attr("adtxno");
                    $.ajax({
                        url: "<%= request.getContextPath() %>/B402/DuplicateCheck",
                        type: "POST",
                        async: true, 
                        data: {
                            "dpschno": dpschno,
                            "dpschtxdate": dpschtxdate,
                            "dpuserid": dpuserid,
                            "adtxno": adtxno
                        },
                        success: function (res) {
                            if (res.counts == 0 ) {
                           	  onResend(dpschno, dpschtxdate, dpuserid, adtxno, "N");                                
                            } 
                            resendLen--;
                            if (resendLen==0 ) {
                                unlockScreen();
								//alert(result);
								onQueryNTD();
                            }
                        },
                        error: function (err) {
                            resendLen--;
                            result += "預約序號["+dpschno+"]:失敗-"+err.statusText+"\r\n\r\n";
                            if ( resend==0 ) {
								alert(result);
	                            unlockScreen();
								onQueryNTD();
                            }
                        }
                    });
          		 }); 
          	 }
        }

        // 單筆重送
        function onResend(dpschno, dpschtxdate, dpuserid, adtxno, showmsg) {
	            var msg = "確定重送預約序號["+dpschno+"]-預約轉帳日["+dpschtxdate+"]交易?";
				if (showmsg==="Y"){
				 		if(confirm(msg)===false){
				 			return false;
				     	}
				 }
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B402/Resend",
                    type: "POST",
                    data: {
                        "dpschno": dpschno,
                        "dpschtxdate": dpschtxdate,
                        "dpuserid": dpuserid,
                        "adtxno": adtxno
                    },
                    success: function (res) {
                        unlockScreen();
                        if (res.status == "" ) {
                            alert("重送成功");
                            onQueryNTD();
                        } else {
                            alert(res.status);
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
        	}

     	// 單筆重送-重複交易檢核
        function onDuplicateCheck(dpschno, dpschtxdate, dpuserid, adtxno) {
            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/B402/DuplicateCheck",
                type: "POST",
                data: {
                    "dpschno": dpschno,
                    "dpschtxdate": dpschtxdate,
                    "dpuserid": dpuserid,
                    "adtxno": adtxno
                },
                success: function (res) {
                    unlockScreen();
                    if (res.counts > 0 ) {
                        var time=res.time+"";
                        if ( time.length < 6 ) {
							time="0"+time;
                        }
                        alert("當日有重覆交易(同轉出/轉入/金額), 最近一筆交易時間為 "+time.substr(0, 2)+":"+time.substr(2, 2)+":"+time.substr(4, 2));
                    } else if ( res.counts == -1 ) {
                    	alert("檢查當日重覆交易(同轉出/轉入/金額)發生錯誤");
                    }
                    onResend(dpschno, dpschtxdate, dpuserid, adtxno ,"Y");
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }

        //開啟處理狀態 Dialog, Controller 會回傳 PartialView
        function showMsgCode(id) {
            var url = "<%= request.getContextPath() %>/B402/MsgDialog/" + id;
            lockScreen();
            $.post(url, {}, function (data, status) {
                unlockScreen();
                if (status == "success") {
                    //alert(data);
                    var htmlContent = data.replace(/\r\n/g, "");
                    $("#model-body").html(htmlContent);
                    $('#exampleModal').modal({

                    });
                }
            });
        }
        
        function onExportCSV() {
            $.ajax({
                    url:"<%= request.getContextPath() %>/B402/DownloadtoCSV",
                    type:"POST",
                    data:{
                    	USERID:$("#UserId").val()
                    },
                    success: function(response, status, xhr) {
                        // check for a filename
                        var filename = "";
                        var disposition = xhr.getResponseHeader('Content-Disposition');
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                        }

                        var type = xhr.getResponseHeader('Content-Type');
                        var blob = new Blob(["\ufeff" + response], { type: type });

                        if (typeof window.navigator.msSaveBlob !== 'undefined') {
                            // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                            window.navigator.msSaveBlob(blob, filename);
                        } else {
                            var URL = window.URL || window.webkitURL;
                            var downloadUrl = URL.createObjectURL(blob);

                            if (filename) {
                                // use HTML5 a[download] attribute to specify filename
                                var a = document.createElement("a");
                                // safari doesn't support this yet
                                if (typeof a.download === 'undefined') {
                                    window.location = downloadUrl;
                                } else {
                                    a.href = downloadUrl;
                                    a.download = filename;
                                    document.body.appendChild(a);
                                    a.click();
                                }
                            } else {
                                window.location = downloadUrl;
                            }

                            setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                        }
                    },
                    error:function(){
                        alert("error");
                    }
            });                
        }
    </script>
</body>

</html>