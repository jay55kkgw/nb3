<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>使用者登入狀態查詢</h2>
        <form class="container main-content-block p-4" action="/dummy" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right">使用者統編：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="UserId" name="UserId" class="form-control upper" maxlength="50" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">查詢登入起日：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="DateFrom" autocomplete="off" name="DateFrom" class="form-control" style="width:160px"
                            maxlength="8" value="${StartDt}" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">查詢登入迄日：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="DateTo" autocomplete="off" name="DateTo" class="form-control" style="width:160px"
                            maxlength="8" value="${EndDt}" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">登入來源：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <select class="form-control" name="LoginType" id="LoginType">
                            <option value="">---請選擇---</option>
                            <option value="NB">個網銀</option>
                            <option value="MB">行動銀行</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-2 col-10">
                    <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                    <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align"
                        onclick="onQuery()" />
                </div>
            </div>
        </form>
    </div>
    <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>使用者統編</th>
                <th>登入IP</th>
                <th>登入時間</th>
                <th>上次登入時間</th>
                <th>上次登入IP</th>
                <th>登入來源</th>
                <!-- <th style="width:10%">動作</th> -->
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
    <script>
        $(document).ready(function () {
            $('#DateFrom').datetimepicker({
                lang: 'zh-TW',
                timepicker: false,
                format: 'Y/m/d',   // 顯示時分
                scrollMonth: false
            });
            $('#DateTo').datetimepicker({
                lang: 'zh-TW',
                timepicker: false,
                format: 'Y/m/d',   // 顯示時分
                scrollMonth: false
            });
            onQuery();
        }); //ready


        function onQuery() {
            var startDate = $("#DateFrom").val();
            var endDate = $("#DateTo").val();

            var sisvalid = Date.parse(startDate);
            var eisvalid = Date.parse(endDate);
            if(isNaN(sisvalid) || isNaN(eisvalid) ) { 
                alert("請輸入合法的查詢登入起日和查詢登入迄日");
                return;
            }

            if ( sisvalid > eisvalid ) {
                alert("查詢登入迄日必需大於查詢登入起日");
                return;
            }
            var today=new Date();
            if ( eisvalid>today ) {
                alert("查詢登入起日與迄日必須小於等於今天");
                return;
            }
            $("span.error").remove();

            $("#main").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "order": [[0, "asc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/B210/Query",
                    "data": {
                        "STARTDATE": startDate,
                        "ENDDATE": endDate,
                        "USERID": $("#UserId").val(),
                        "LOGINTYPE": $("#LoginType").val()
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "aduserid" },
                    { "data": "aduserip" },
                    { "data": "logintime" },
                    { "data": "lastlogintime" },
                    { "data": "lastaduserip" },
                    { "data": "logintype" }//,
                    //{ "data": null, orderable: false }
                ],
                "createdRow": function (row, data, index) {
                    var $logintime = data.logintime.replace(
                        /^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/,
                        "$1/$2/$3 $4:$5:$6");
                    $("td", row).eq(2).text("").append($logintime);

                    var $lastlogintime = data.lastlogintime.replace(
                        /^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/,
                        "$1/$2/$3 $4:$5:$6");
                    $("td", row).eq(3).text("").append($lastlogintime);

                    var $logintype;
                    switch (data.logintype) {
                        case "NB":
                            $loginout = "個網銀";
                            break;
                        case "MB":
                            $loginout = "行動銀行";
                            break;
                    }

                    $("td", row).eq(5).text("").append($loginout);
                    //var $code = $("<a>動作</a>");
                    //$code.attr("href", "#");
                    //$("td", row).eq(6).text("").append($code);
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            $("#main").show();
            return false;
        }

    </script>
</body>

</html>