<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="zh">
<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>
</head>
<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="content row">
        <main class="col-12">
            <section id="main-content" class="container">
                <div>
                    <p>系統發生錯誤</p>
                    <p>類型：${errorType}</p>
                    <p>訊息：${errorDesc}</p>
                </div>
            </section>
        </main>
    </div>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
</body>
</html>