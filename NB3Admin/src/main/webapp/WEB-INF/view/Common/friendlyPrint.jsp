<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
        
    </head>
    <body>

        <!-- body content start -->
        <div id="printArea" class="container" id="main-content">

        </div>
            <!-- report -->
        <!-- body content end -->

        <!-- include master page footer -->

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            function getPrintContent(parentPrintArea) {
                var areaArray = parentPrintArea.split(",");
                var i;
                var divArea;
                var pArea;
                for (i = 0; i < areaArray.length; i++) {
                    divArea = document.getElementById('printArea');
                    pArea = window.opener.document.getElementById(areaArray[i]);
                    if (pArea == null) {
                        continue;
                    }
                    divArea.innerHTML = divArea.innerHTML + pArea.outerHTML;
                }
            }

            $(document).ready(function () {

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": false,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 500,
                    "order": [[0, "asc"]],
                    "dom": "<'clear'>frtip",   //無工具列
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                getPrintContent('${PRINTAREA}'); 
                $("button").hide();
                $("input").prop('disabled', true);
                $("select").prop('disabled', true);
                $("a").prop('disabled', true);
                window.print();

            }); //ready
        </script>
    </body>
</html>