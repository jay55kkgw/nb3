<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="col-md-12">
    <div class="content">
        <table id="tblDialog" class="display p-4 transparent" cellspacing="0">
            <thead>
                <tr>
                    <th>序號</th>
                    <th>部門</th>
                    <th>經辦/主管</th>
                    <th>時間</th>
                    <th>狀態</th>
                    <th>意見</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="history" items="${histories}" varStatus="loop">
                    <fmt:parseDate var="CreateDateTime" value="${history.CREATEDATE}${history.CREATETIME}" type="BOTH"
                        pattern="yyyyMMddHHmmss" />
                    <tr>
                        <td>${loop.index+1}</td>
                        <td>${history.ACTIONOID}</td>
                        <td>${history.ACTIONUNAME}(${history.ACTIONUID})</td>
                        <td>
                            <fmt:formatDate value="${CreateDateTime}" pattern="yyyy/MM/dd HH:mm:ss" />
                        </td>
                        <c:choose>
				            <c:when test="${history.APPROVEFLAG=='C'}">
				                <td>已取消</td>
				            </c:when>
				            <c:otherwise>
				                 <td>${history.STEPNAME}</td>
				            </c:otherwise>
				        </c:choose>
                        <td>${history.COMMENTS}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<script>
    $("#tblDialog").DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false,
        "language": {
            "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
        }
    });
</script>