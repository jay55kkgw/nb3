<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-md-12">
    <div class="content">
        <form class="container" action="/dummy" method="post">
            <div class="form-group row">
                <label class="col-md-3 control-label text-right">選單代碼：</label>
                <div class="col-md-9">
                    <input id="ADOPID" name="ADOPID" type="text" class="form-control" maxlength="60" value="${menu.ADOPID}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="ADOPNAME">選單名稱：</label>
                <div class="col-md-9">
                    <input id="ADOPNAME" name="ADOPNAME" type="text" class="form-control" maxlength="20" value="${menu.ADOPNAME}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="ADOPMEMO">備註：</label>
                <div class="col-md-9">
                    <input id="ADOPMEMO" name="ADOPMEMO" type="text" class="form-control" maxlength="20" value="${menu.ADOPMEMO}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="URL">URL：</label>
                <div class="col-md-9">
                    <input id="URL" name="URL" type="text" class="form-control" maxlength="50" value="${menu.URL}" />
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-3 col-md-9">
                    <label>
                        <c:set var="checked" value="${menu.ADOPALIVE.equals('1') ? 'checked': ''}" />
                        <input id="ADOPALIVE" name="ADOPALIVE" type="checkbox" value="${menu.ADOPALIVE}" ${checked}  />&nbsp;顯示本連結
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-3 col-md-9">
                    <input id="exit" type="button" value="離開" class="btn btn-dark" onclick="onExit();" />&nbsp;
                    <input type="button" value="儲存" class="btn btn-info" onclick="onSave();" />
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function checkEmpty() {
        var msg = "";
        if ( $("#ADOPID").val()=="" ) {
            msg += "選單代碼不可為空值\r\n";
        }
        if ( $("#ADOPNAME").val()=="" ) {
            msg += "選單名稱不可為空值.\r\n";
        }

        if ( $("#URL").val()=="" ) {
            msg += "URL不可為空值.\r\n";
        }

        return msg;
    }
    function onSave() {
        $("span.error").remove();
        var menuId = $("#ADOPID").val();
        var menuName = $("#ADOPNAME").val();
        var msg = checkEmpty();
        if ( msg != "" ) {
            alert(msg);
            return;
        }
        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/B112/CreateSave",
            type: "POST",
            data: {
                "ADOPID": $("#ADOPID").val(),
                "ADOPNAME": $("#ADOPNAME").val(),
                "ADOPMEMO": $("#ADOPMEMO").val(),
                "ADOPALIVE": $("#ADOPALIVE").prop('checked') ? "1" : "0",
                "ADOPGROUP": "${menu.ADOPGROUP}",
                "URL": $("#URL").val()
            },
            success: function (res) {
                unlockScreen();
                if(res.validated){
                    alert("選單["+menuName+"]新增成功");   
                    createCallBack(menuId, menuName); 
                } else {
                    //Set error messages
                    $.each(res.errorMessages,function(key,value){
                        if ( key === "summary" )
                            alert(value);
                        else 
                            $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                    });
                }
            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }

    function onExit() {
        exitCallBack();
    }
</script>