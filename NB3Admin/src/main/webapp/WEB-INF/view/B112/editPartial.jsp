<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-md-12">
    <div class="content">
        <form class="container" action="/NB3SYSOP_save" method="post">
            <div class="form-group row">
                <label class="col-md-3 control-label text-right">選單代碼：</label>
                <div class="col-md-9">
                    ${menu.ADOPID}
                    <input id="ADOPID" name="ADOPID" type="hidden" value="${menu.ADOPID}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="ADOPNAME">選單名稱：</label>
                <div class="col-md-9">
                    <input id="ADOPNAME" name="ADOPNAME" type="text" class="form-control" maxlength="150" value="${menu.ADOPNAME}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="ADOPMEMO">備註：</label>
                <div class="col-md-9">
                    <input id="ADOPMEMO" name="ADOPMEMO" type="text" class="form-control" maxlength="255" value="${menu.ADOPMEMO}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="URL">URL：</label>
                <div class="col-md-9">
                    <input id="URL" name="URL" type="text" class="form-control" maxlength="500" value="${menu.URL}" />
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-3 col-md-9">
                    <label>
                        <c:set var="checked" value="${menu.ADOPALIVE.equals('1') ? 'checked': ''}" />
                        <input id="ADOPALIVE" name="ADOPALIVE" type="checkbox" value="${menu.ADOPALIVE}" ${checked}  />&nbsp;顯示本連結
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-3 col-md-9">
                    <input id="exit" type="button" value="離開" class="btn btn-dark" onclick="onExit();" />&nbsp;
                    <c:if test="${allowEdit}">
                        <input type="button" value="刪除" class="btn btn-danger" onclick="onDelete();" />&nbsp;
                        <input type="button" value="儲存" class="btn btn-info" onclick="onSave();" />
                    </c:if>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    init();

    function init() {
        if ( "${allowEdit}" == "false" ) {
            $("form input").prop("disabled", true);
            $("form select").prop("disabled", true);
            $("form input[type='button']").prop("disabled", false);
        }
    }
    function checkEmpty() {
        var msg = "";
        if ( $("#ADOPNAME").val()=="" ) {
            msg += "選單名稱不可為空值.\r\n";
        }

        if ( $("#URL").val()=="" ) {
            msg += "URL不可為空值.\r\n";
        }

        return msg;
    }
    function onSave() {
        $("span.error").remove();
        var menuName = $("#ADOPNAME").val();
        var msg = checkEmpty();
        if ( msg != "" ) {
            alert(msg);
            return;
        }
        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/B112/EditSave",
            type: "POST",
            data: {
                "ADOPID": "${menu.ADOPID}",
                "ADOPNAME": $("#ADOPNAME").val(),
                "ADOPMEMO": $("#ADOPMEMO").val(),
                "ADOPALIVE": $("#ADOPALIVE").prop('checked') ? "1" : "0",
                "ADSEQ": "${menu.ADSEQ}",
                "URL": $("#URL").val(),
                "ADOPGROUP": "${menu.ADOPGROUP}"
            },
            success: function (res) {
                unlockScreen();
                if(res.validated){
                    alert("選單["+menuName+"]修改成功");  
                    updateCallBack(menuName);  
                } else {
                    //Set error messages
                    $.each(res.errorMessages,function(key,value){
                        if ( key === "summary" )
                            alert(value);
                        else 
                            $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                    });
                }
            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }

    function onDelete() {
        var menuName = $("#ADOPNAME").val();

        if (!confirm("刪除選單[" + menuName + "]會一併刪除項下的子選單，是否確認？"))
            return;

        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/B112/Delete/${menu.ADOPID}",
            type: "POST",
            data: { },
            success: function (data) {
                unlockScreen();
                if (data === "0") {
                    // 所有可以輸入的項目均 Disable
                    $("#createForm input").prop("disabled", true);
                    $("#createForm select").prop("disabled", true);
                    $("#createForm #exit").prop("disabled", false);

                    alert("選單[" + menuName + "]刪除成功");
                    deleteCallBack();
                } else {
                    alert(data);
                }
            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }

    function onExit() {
        exitCallBack();
    }
</script>