<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!-- 20190603 DannyChou ADD -->

<html>
    <head>
        <c:set var="action" value="${allowEdit==true ? '修改': '查詢'}" />
        <title>台企銀 後台-應用系統代碼維護管理-${action}</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>應用系統代碼維護-${action}</h2>
            <form id="AdmMsgCode" class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADMCODE">訊息代碼：</label>
                    <div class="col-9">
                        <div class="col-12">
                            <label class="control-label text-left" for="Data.ADMCODE">${Data.ADMCODE}</label>
                        </div>
                        <div class="col-12">
                                <input type="checkbox" id="ADMRESEND" name="ADMRESEND" value="Y" />可否提供台幣類交易人工重送
                        </div>
                        <div class="col-12">
                                <input type="checkbox" id="ADMAUTOSEND" name="ADMAUTOSEND" value="Y" />可否提供台幣類交易自動重送
                        </div>
                        <div class="col-12">
                                <input type="checkbox" id="ADMEXCE" name="ADMEXCE" value="Y" />需列入異常事件通知否
                        </div>
                        <div class="col-12">
                                <input type="checkbox" id="ADMRESENDFX" name="ADMRESENDFX" value="Y" />可否提供外幣類交易人工重送
                        </div>
                        <div class="col-12">
                                <input type="checkbox" id="ADMAUTOSENDFX" name="ADMAUTOSENDFX" value="Y" />可否提供外幣類交易自動重送
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADMSGIN">訊息說明：</label>
                    <div class="col-9">
                        <input type="text" id="ADMSGIN" name="ADMSGIN" value="${Data.ADMSGIN}" maxLength="600" size="50" class="form-control" />
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADMSGOUT">客戶訊息內容(繁中)：</label>
                    <div class="col-9">
                        <input type="text" id="ADMSGOUT" name="ADMSGOUT" maxLength="600" rows="3" class="form-control" value="${Data.ADMSGOUT}" />
                    </div>
                </div>    
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADMSGOUTCHS">客戶訊息內容(簡中)：</label>
                    <div class="col-9">
                        <input type="text" id="ADMSGOUTCHS" name="ADMSGOUTCHS" maxLength="600" rows="3" class="form-control" value="${Data.ADMSGOUTCHS}" />
                    </div>
                </div> 
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADMSGOUTENG">客戶訊息內容(英文)：</label>
                    <div class="col-9">
                        <input type="text" id="ADMSGOUTENG" name="ADMSGOUTENG" maxLength="600" rows="3" class="form-control" value="${Data.ADMSGOUTENG}" />
                    </div>
                </div>  
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnGoback" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onGoBack();" />&nbsp;
                        <c:if test="${allowEdit}">
                            <input id="btnDelete" type="button" value="刪除" class="btn btn-danger" aria-label="Left Align" onclick="onDelete();" />&nbsp;
                            <input id="btnSave" type="button" value="儲存" class="btn btn-info" aria-label="Left Align" onclick="onCreate();" />
                        </c:if>    
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script>
            $(document).ready(function () {
                if ("${Data.ADMRESEND}" =="Y")
                    $("#ADMRESEND").prop("checked", true);
                if ("${Data.ADMAUTOSEND}" =="Y")
                    $("#ADMAUTOSEND").prop("checked", true);
                if ("${Data.ADMEXCE}" =="Y")
                    $("#ADMEXCE").prop("checked", true);
                if ("${Data.ADMRESENDFX}" =="Y")
                    $("#ADMRESENDFX").prop("checked", true);
                if ("${Data.ADMAUTOSENDFX}" =="Y")
                    $("#ADMAUTOSENDFX").prop("checked", true);
                    
            }); //ready

            $.fn.serializeObject = function() {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function() {
                    if (o[this.name]) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };

            $.fn.serializeObjectEx = function() {
                var o = {};
                //    var a = this.serializeArray();
                $(this).find('input[type="hidden"], input[type="text"], input[type="password"], input[type="checkbox"]:checked, input[type="radio"]:checked, select, textarea').each(function() {
                    if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
                        var $parent = $(this).parent();
                        var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
                        if ($chb != null) {
                            if ($chb.prop('checked')) return;
                        }
                    }
                    if (this.name === null || this.name === undefined || this.name === '')
                        return;
                    var elemValue = null;
                    if ($(this).is('select'))
                        elemValue = $(this).find('option:selected').val();
                    else elemValue = this.value;
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(elemValue || '');
                    } else {
                        o[this.name] = elemValue || '';
                    }
                });
                return o;
            }

            // 刪除
            function onDelete() {
                var message = confirm("確定刪除系統代碼[${Data.ADMCODE}]？");
                if ( message ) {
                    lockScreen();
                    $.ajax({
                        url: "<%= request.getContextPath() %>/B107/Delete/${Data.ADMCODE}",
                        type: "POST",
                        data: {},
                        success: function (res) {
                            unlockScreen();
                            if(res.validated){
                                alert("刪除成功");
                                location.href="<%= request.getContextPath() %>/B107/Index"
                            } else {
                                //Set error messages
                                $.each(res.errorMessages,function(key,value){
                                    if ( key === "summary" )
                                        alert(value);
                                    else {
                                        alert(key+":"+value);
                                    }
                                });
                            }
                        },
                        error: function (err) {
                            unlockScreen();
                            alert(err.statusText);
                        }
                    });
                }
            }

            function checkEmpty() {
                var msg = "";
                if ( $("#ADMCODE").val()=="" ) {
                    msg += "訊息代碼不可為空值.\r\n";
                }

                if ( $("#ADMSGIN").val()=="" ) {
                    msg += "訊息說明不可為空值.\r\n";
                }

                if ( $("#ADMSGOUT").val()=="" ) {
                    msg += "客戶訊息內容(繁中)不可為空值.\r\n";
                }

                if ( $("#ADMSGOUTCHS").val()=="" ) {
                    msg += "客戶訊息內容(簡中)不可為空值.\r\n";
                }

                if ( $("#ADMSGOUTENG").val()=="" ) {
                    msg += "客戶訊息內容(英文)不可為空值.\r\n";
                }
                return msg;
            }

            // 修改
            function onCreate() {
                var msg = checkEmpty();
                if ( msg != "" ) {
                    alert(msg);
                    return;
                }
                
                var formData = $("#AdmMsgCode").serializeObjectEx();
                formData["ADMCODE"] = "${Data.ADMCODE}";

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B107/Edit",
                    type: "POST",
                    data: formData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("修改成功"); 
                            location.href="<%= request.getContextPath() %>/B107"
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else {
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                                }
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onGoBack() {
                location.href="<%= request.getContextPath() %>/B107"
            }

        </script>
    </body>
</html>