<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!-- 20190603 DannyChou ADD -->

<html>
    <head>
        <title>台企銀 後台-應用系統代碼維護管理</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>應用系統代碼維護管理</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="">整批訊息代碼檔匯入：</label>
                    <div class="col-9">
                        <div class="form-inline">
                            <c:if test="${allowEdit}">
                                <input type="file" id="file" name="file" size="20" class="form-control" />&nbsp;
                                <input type="button" value="範例下載" class="btn btn-info" onclick="onSample();" />&nbsp;
                                <input type="button" value="執行匯入" class="btn btn-info" onclick="onUpLoad();" /><label class="control-label">(檔案大小限制：10Mb)</label>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADMCODE">訊息代碼：</label>
                    <div class="col-9">
                        <input type="text" id="ADMCODE" name="ADMCODE" maxLength="10" size="12" class="form-control" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADMSGOUT">客戶訊息內容：</label>
                    <div class="col-9">
                        <input type="text" id="ADMSGOUT" name="ADMSGOUT" class="form-control" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnQuery" type="button" value="查詢" class="btn btn-dark" aria-label="Left Align" onclick="onQuery();" />&nbsp;
                        <c:if test="${allowEdit}">
                            <input id="btnCreate" type="button" value="新增" class="btn btn-info" aria-label="Left Align" onclick="onCreate();" />&nbsp;
                        </c:if>
                        <input id="btnexpCSV" type="button" value="匯出CSV" class="btn btn-secondary" aria-label="Left Align" onclick="onExportCSV();" />&nbsp;
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:95%">
            <thead>
                <tr>
                    <th style="width:15%">訊息代碼</th>
                    <th style="width:40%">訊息說明</th>
                    <th style="width:40%">客戶訊息內容</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                onQuery();
            }); //ready
        
            function onQuery() {
                $("span.error").remove();

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "order": [[0, "asc"]],
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B107/Query",
                        "data": {
                            "ADMCODE": $("#ADMCODE").val(),
                            "ADMSGOUT": $("#ADMSGOUT").val()
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [
                        { "data": "admcode" },
                        { "data": "admsgin" },
                        { "data": "admsgout" }
                    ],
                    "createdRow": function (row, data, index) {
                        //記錄PK
                        var key = data.admcode.trim();
                        var $admCode = $("<a>"+key+"</a>");
                        $admCode.attr("href", "<%= request.getContextPath() %>/B107/Edit/"+key);
                        $("td", row).eq(0).text("").append($admCode);
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }

            function onSample() {
                window.location = "<%= request.getContextPath() %>/B107/Sample";
            }

            function onCreate() {
                window.location = "<%= request.getContextPath() %>/B107/Create";
            }

            function onUpLoad() {
                var fileData = new FormData();

                var file1 = $("#file").get(0);
                if (file1.files.length > 0) {
                    fileData.append("multipartFile", file1.files[0]);
                }

                if (file1.files.length == 0 ) {
                    alert("請選擇檔案");
                    return;
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B107/FileUpload",
                    type: "POST",
                    contentType: false,  // Not to set any content header
                    processData: false,  // Not to process data
                    data: fileData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("檔案匯入成功");   
                            $("#file").val('');
                            onQuery();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText+"\n"+err.responseText);
                    }
                });
            }

            function onExportCSV() {
                window.location = "<%= request.getContextPath() %>/B107/DownloadtoCSV";
            }


        </script>
    </body>
</html>