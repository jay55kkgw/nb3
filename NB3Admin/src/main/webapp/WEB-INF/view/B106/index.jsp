<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>



<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>其它資訊維護</h2>
            <form class="container main-content-block p-4" id="formData" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADSESSIONTO">使用者Session Timeout 設定：</label>
                    <div class="col-2">
                        <input type="text" id="ADSESSIONTO" name="ADSESSIONTO" value="${Data.ADSESSIONTO}"  class="form-control" maxlength="10" />
                    </div>
                    <label class="col-1 control-label text-left" >秒</label>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADOPMAIL">OP人員 Email 帳號：</label>
                    <div class="col-9">
                        <input type="text" id="ADOPMAIL" name="ADOPMAIL"  class="form-control" maxlength="500" value="${Data.ADOPMAIL}" />
                    </div>
                </div>
                <div class="form-group row"> 
                    <label class="col-3 control-label text-right" for="ADAPMAIL">AP人員 Email 帳號：</label>
                    <div class="col-9">
                        <input type="text" id="ADAPMAIL" name="ADAPMAIL"   class="form-control" maxlength="500" value="${Data.ADAPMAIL}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADSECMAIL">安控人員 Email 帳號：</label>
                    <div class="col-9">
                        <input type="text" id="ADSECMAIL" name="ADSECMAIL" v class="form-control" maxlength="500" value="${Data.ADSECMAIL}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADSPMAIL">SP人員 Email 帳號：</label>
                    <div class="col-9">
                        <input type="text" id="ADSPMAIL" name="ADSPMAIL"  class="form-control" maxlength="500" value="${Data.ADSPMAIL}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADGDMAIL">黃金掛牌通知 Email 帳號：</label>
                    <div class="col-9">
                        <input type="text" id="ADGDMAIL" name="ADGDMAIL"  class="form-control" maxlength="500" value="${Data.ADGDMAIL}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADGDTXNMAIL">黃金交易通知 Email 帳號：</label>
                    <div class="col-9">
                        <input type="text" id="ADGDTXNMAIL" name="ADGDTXNMAIL"  class="form-control" maxlength="500" value="${Data.ADGDTXNMAIL}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADBANK3MAIL">Bank 3.0線上申請統計通知 Email 帳號：</label>
                    <div class="col-9">
                        <input type="text" id="ADBANK3MAIL" name="ADBANK3MAIL"  class="form-control" maxlength="500" value="${ADBANK3MAIL}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="TXNTWAMTMAIL">次營業日跨行轉帳總金額通知 Email 帳號：</label>
                    <div class="col-9">
                        <input type="text" id="TXNTWAMTMAIL" name="TXNTWAMTMAIL"  class="form-control" maxlength="500" value="${TXNTWAMTMAIL}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADFDSVRIP">基金主機 IP：</label>
                    <div class="col-3">
                        <input type="text" id="ADFDSVRIP" name="ADFDSVRIP" value="${Data.ADFDSVRIP}"  class="form-control" maxlength="20" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADMAILSVRIP">郵件主機 IP：</label>
                    <div class="col-3">
                        <input type="text" id="ADMAILSVRIP" name="ADMAILSVRIP" value="${Data.ADMAILSVRIP}"  class="form-control" maxlength="20" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADPARKSVRIP">停車費主機 IP：</label>
                    <div class="col-3">
                        <input type="text" id="ADPARKSVRIP" name="ADPARKSVRIP" value="${Data.ADPARKSVRIP}" class="form-control" maxlength="20" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADARSVRIP">AR主機 IP：</label>
                    <div class="col-3">
                        <input type="text" id="ADARSVRIP" name="ADARSVRIP" value="${Data.ADARSVRIP}" class="form-control" maxlength="20"  />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADFDRETRYTIMES">基金主機 Retry 次數：</label>
                    <div class="col-2">
                        <input type="text" id="ADFDRETRYTIMES" name="ADFDRETRYTIMES" value="${Data.ADFDRETRYTIMES}" class="form-control" maxlength="3" />
                    </div>
                    <label class="col-1 control-label text-left" >次</label>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADPKRETRYTIMES">停車費主機 Retry 次數：</label>
                    <div class="col-2">
                        <input type="text" id="ADPKRETRYTIMES" name="ADPKRETRYTIMES" value="${Data.ADPKRETRYTIMES}"  class="form-control" maxlength="3"/>
                    </div>
                    <label class="col-1 control-label text-left" >次</label>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADRESENDTIMES">補發次數上限：</label>
                    <div class="col-2">
                        <input type="text" id="ADRESENDTIMES" name="ADRESENDTIMES" value="${Data.ADRESENDTIMES}" class="form-control" maxlength="3" />
                    </div>
                    <label class="col-1 control-label text-left" >次</label>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="">分行EMAIL 寄件帳號規則：</label>
                    <div class="col-9">
                        <div class="form-group row">
                            <label class="col-3 control-label text-right" for="">帳號啟始字元：</label>
                            <div class="col-3">
                                <input type="text" id="" name="ADLEADBYTE" class="form-control"/>
                            </div>
                            <label class="col-3 control-label text-right" for="">分行郵件 domain：</label>
                            <div class="col-3">
                                <input type="text" id="" name="ADMAILDOMAIN" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="editArea" class="form-group row">
                    <div class="offset-3 col-9">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;     
                        <input id="btnCreate" type="button" value="儲存" class="btn btn-info" aria-label="Left Align" onclick="onSave()" />
                    </div>
                </div>
            </form>
        </div>

        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
        <script>
            $(document).ready(function () {
                if ( "${allowEdit}" == "false" ) {
                    $("#editArea").hide();
                    $("form input").prop("disabled", true);
                }
            }); //ready

            function validateEmail(email) {
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
                    return true;
                } else {
                    return false;
                }
            }

            function validateEmailList(emailList) {
                var emails = emailList.split(";");
                for ( var i=0; i<emails.length; i++ ) {
                    if ( !validateEmail(emails[i]) ) {
                        return false;
                    }
                }
                return true;
            }

            function validateIPaddress(ipaddress)  {
                if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {
                    return true;
                } else {
                    return false;
                }
            }

            function validateRange(min, max, value) {
                if ( value=="" ) {
                    return false;
                }
                try {
                    var v=parseInt(value);

                    if ( v>max || v<min ) {
                        return false;
                    } else {
                        return true;
                    }
                } catch (error) {
                    return false;
                }
            }

            function onSave() {           
              var ADSESSIONTO=$("#ADSESSIONTO").val();
              var ADOPMAIL=$("#ADOPMAIL").val();
              var ADAPMAIL=$("#ADAPMAIL").val();
              var ADSECMAIL=$("#ADSECMAIL").val();
              var ADSPMAIL=$("#ADSPMAIL").val();
              var ADFDSVRIP=$("#ADFDSVRIP").val();
              var ADMAILSVRIP=$("#ADMAILSVRIP").val();
              var ADPARKSVRIP=$("#ADPARKSVRIP").val();
              var ADARSVRIP=$("#ADARSVRIP").val();
              var ADFDRETRYTIMES=$("#ADFDRETRYTIMES").val();
              var ADPKRETRYTIMES=$("#ADPKRETRYTIMES").val();
              var ADRESENDTIMES=$("#ADRESENDTIMES").val();
              var ADLEADBYTE=$("#ADLEADBYTE").val();
              var ADMAILDOMAIN=$("#ADMAILDOMAIN").val();  
              var ADGDMAIL=$("#ADGDMAIL").val(); 
              var ADGDTXNMAIL=$("#ADGDTXNMAIL").val(); 
              var ADBANK3MAIL=$("#ADBANK3MAIL").val(); 
              var TXNTWAMTMAIL=$("#TXNTWAMTMAIL").val(); 
              
              var errMsg1 = validateEmailList(ADOPMAIL);
              var errMsg2 = validateEmailList(ADAPMAIL);
              var errMsg3 = validateEmailList(ADSECMAIL);
              var errMsg4 = validateEmailList(ADSPMAIL);
              var errMsg5 = validateIPaddress(ADFDSVRIP);
              var errMsg6 = validateIPaddress(ADMAILSVRIP);
              var errMsg7 = validateIPaddress(ADPARKSVRIP);
              var errMsg8 = validateIPaddress(ADARSVRIP);
              var errMsg9 = validateRange(0, 5, ADFDRETRYTIMES);
              var errMsg10 = validateRange(0, 5, ADPKRETRYTIMES);
              var errMsg11 = validateRange(0, 5, ADRESENDTIMES);
              var errMsg12 = validateRange(300, 1800, ADSESSIONTO);
              var errMsg13 = validateEmailList(ADGDMAIL);
              var errMsg14 = validateEmailList(ADGDTXNMAIL);
              var errMsg15 = validateEmailList(ADBANK3MAIL);
              var errMsg16 = validateEmailList(TXNTWAMTMAIL);

              var errMsg = "";
              if ( !errMsg1 ) {
                  errMsg = "OP人員 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com"+"\r\n";
              }
              if ( !errMsg2 ) {
                  errMsg += "AP 人員 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com"+"\r\n";
              }
              if ( !errMsg3 ) {
                  errMsg += "安控人員 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com"+"\r\n";
              }
              if ( !errMsg4 ) {
                  errMsg += "SP 人員 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com"+"\r\n";
              }
              if ( !errMsg5 ) {
                  errMsg += "基金主機 IP 值不正確，格式為：0.0.0.0 ~ 255.255.255.255"+"\r\n";
              }
              if ( !errMsg6 ) {
                  errMsg += "郵件主機 IP 值不正確，格式為：0.0.0.0 ~ 255.255.255.255"+"\r\n";
              }
              if ( !errMsg7 ) {
                  errMsg += "停車費主機 IP 值不正確，格式為：0.0.0.0 ~ 255.255.255.255"+"\r\n";
              }
              if ( !errMsg8 ) {
                  errMsg += "AR 主機 IP 值不正確，格式為：0.0.0.0 ~ 255.255.255.255"+"\r\n";
              }
              if ( !errMsg9 ) {
                  errMsg += "基金主機 Retry 次數值不正確，範圍為 0～5"+"\r\n";
              }
              if ( !errMsg10 ) {
                  errMsg += "停車費主機 Retry 次數值不正確，範圍為 0～5"+"\r\n";
              }
              if ( !errMsg11 ) {
                  errMsg += "補發次數上限值不正確，範圍為 0～5"+"\r\n";
              }
              if ( !errMsg12 ) {
                  errMsg += "使用者 Session Timeout值不正確，範圍為 300～1800 秒"+"\r\n";
              }
              if ( !errMsg13 ) {
                  errMsg += "黃金掛牌通知 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com"+"\r\n";
              }
              if ( !errMsg14 ) {
                  errMsg += "黃金交易通知 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com"+"\r\n";
              }
              if ( !errMsg15 ) {
                  errMsg += "Bank 3.0線上申請統計通知 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com"+"\r\n";
              }
              if ( !errMsg16 ) {
                  errMsg += "次營業日跨行轉帳總金額通知 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com"+"\r\n";
              }
              if ( errMsg != "" ) {
                  alert(errMsg);
                  return;
              }
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B106/Edit",
                    type: "POST",
                    data: {
                        "ADPK":"NBSYS",
                        "ADSESSIONTO":ADSESSIONTO,
                        "ADOPMAIL":ADOPMAIL,
                        "ADAPMAIL":ADAPMAIL,
                        "ADSECMAIL":ADSECMAIL,
                        "ADSPMAIL":ADSPMAIL,
                        "ADFDSVRIP":ADFDSVRIP,
                        "ADMAILSVRIP":ADMAILSVRIP,
                        "ADPARKSVRIP":ADPARKSVRIP,
                        "ADARSVRIP":ADARSVRIP,
                        "ADFDRETRYTIMES":ADFDRETRYTIMES,
                        "ADPKRETRYTIMES":ADPKRETRYTIMES,
                        "ADRESENDTIMES":ADRESENDTIMES,
                        "ADLEADBYTE":ADLEADBYTE,
                        "ADMAILDOMAIN":ADMAILDOMAIN,
                        "ADGDMAIL":ADGDMAIL,
                        "ADGDTXNMAIL":ADGDTXNMAIL,
                        "ADBANK3MAIL":ADBANK3MAIL,
                        "TXNTWAMTMAIL":TXNTWAMTMAIL
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("其它資料維護修改成功");  
                            onData();
                        } else {
                            //Set error messages
                            $('span.error').remove();
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else {
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                                }
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }        
        </script>
    </body>
</html>