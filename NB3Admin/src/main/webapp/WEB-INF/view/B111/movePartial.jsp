<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="col-md-12">
    <div class="content">
        <form id="moveForm" action="/NB3SYSOP_save" method="post">
            <!-- 選單名稱 -->
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="ADOPNAME">選單名稱：</label>
                <div class="col-md-9">
                    <input id="ADOPNAME" name="ADOPNAME" type="text" class="form-control" maxlength="50"
                        value="${menu.ADOPNAME}" readonly />
                </div>
            </div>
            <!-- 選單ID -->
            <div class="form-group row">
                <label class="col-md-3 control-label text-right">選單代碼：</label>
                <div class="col-md-9">
                    <input id="ADOPID" name="ADOPID" type="text" class="form-control" maxlength="50"
                        value="${menu.ADOPID}" readonly />
                </div>
            </div>
            <!-- 父選單ID -->
            <div class="form-group row">
                <label class="col-md-3 control-label text-right">父選單代碼：</label>
                <div class="col-md-9">
                    <input id="ADOPGROUPID" name="ADOPGROUPID" type="hidden" value="${menu.ADOPGROUPID}" />
                </div>
            </div>
            <!-- 移動到 -->
            <div class="form-group row">
                <label class="col-md-3 control-label text-right">移動至此父選單：</label>
                <div class="col-md-9">
                    <select class="form-control" id="NewParentID0" onchange="onMenuChange(this,1);">
                        <option value="">--請選擇--</option>
                        <c:forEach items="${firstMenus}" var="menu">
                            <option value="${menu.ADOPID}">${menu.ADOPNAME}</option>
                        </c:forEach>
                    </select>
                    <br />
                    <select class="form-control" id="NewParentID1" onchange="onMenuChange(this,2);">
                        <option value="">--請選擇--</option>
                    </select>
                    <br />
                    <select class="form-control" id="NewParentID2">
                        <option value="">--請選擇--</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-3 col-md-9">
                    <input id="exit" type="button" value="離開" class="btn btn-dark" onclick="onExit();" />&nbsp;
                    <input type="button" value="儲存" class="btn btn-info" onclick="onMoveSave();" />
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function onMenuChange(obj, idx) {
        var val = $(obj).val();
        var ctlId = "#NewParentID" + idx;

        $(ctlId + " option").remove();
        $(ctlId).append($("<option>---請選擇---</option>").attr("value", ""));

        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/B111/SubMenu/" + val,
            type: "POST",
            data: {},
            cache: false,
            async: true,
            success: function (data) {
                unlockScreen();
                if (data) {
                    var menus = JSON.parse(data);
                    for (var i = 0; i < menus.length; i++) {
                        $(ctlId).append($("<option></option>").attr("value", menus[i].ADOPID).text(menus[i].ADOPNAME));
                    }
                } else {

                }
            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }

    function onMoveSave() {
        var id0 = $("#NewParentID0").val();
        var id1 = $("#NewParentID1").val();
        var id2 = $("#NewParentID2").val();
        var msg = $("#NewParentID0 option:selected").text();
        var newADOPGROUPID = id0;

        if (id1 !== "") {
            msg += "->" + $("#NewParentID1 option:selected").text();
            newADOPGROUPID = $("#NewParentID1").val();
        }

        if (id2 !== "") {
            msg += "->" + $("#NewParentID2 option:selected").text();
            newADOPGROUPID = $("#NewParentID2").val();
        }

        if (!confirm("確定將連結移至[" + msg + "]下"))
            return;

        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/B111/MoveSave",
            type: "POST",
            data: {
                "DPACCSETID": "${menu.DPACCSETID}",
                "NewADOPGROUPID": newADOPGROUPID
            },
            success: function (data) {
                unlockScreen();
                if (data === "0") {
                    // 所有可以輸入的項目均 Disable
                    $("#moveForm input").prop("disabled", true);
                    $("#moveForm select").prop("disabled", true);
                    $("#moveForm #exit").prop("disabled", false);

                    alert("選單[${menu.ADOPNAME}]移動成功");
                    moveCallBack(newADOPGROUPID);
                } else {
                    alert(data);
                }
            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }

    function onExit() {
        exitCallBack();
    }

</script>