<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>線上人數即時查詢</h2>
        <form class="container main-content-block p-4" action="/dummy" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right">更新間距(秒)：</label>
                <div class="col-2">
                    <select class="form-control" onchange="onIntervalChange(this);">
                        <option value="5000">5</option>
                        <option value="10000">10</option>
                        <option value="20000">20</option>
                        <option value="30000">30</option>
                        <option value="45000">45</option>
                        <option value="60000" selected>60</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-6 text-center">
                    <h2>一般網銀</h2>
                </div>
                <div class="col-6 text-center">
                    <h2><!-- 行動網銀 --></h2>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-6 text-center">
                    <h1 id="NB"></h1>
                </div>
                <div class="col-6 text-center">
                    <h1 id="MB"></h1>
                </div>
            </div>
        </form>
    </div>
    
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script>
        var interval=60*1000;
        var hInterval;

        $(document).ready(function () {
            onQuery();
            hInterval = setInterval(onQuery, interval);
        }); //ready

        function onIntervalChange(obj) {
            interval = $(obj).val();
            clearInterval(hInterval);
            hInterval = setInterval(onQuery, interval);
        }

        function onQuery() {
            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/B211/Concurrent",
                type: "POST",
                data: { },
                success: function (data) {
                    unlockScreen();
                    $("#NB").text(data.NB);
                    //$("#MB").text(data.MB);
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }

    </script>
</body>

</html>