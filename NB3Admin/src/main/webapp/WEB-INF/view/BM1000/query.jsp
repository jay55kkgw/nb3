<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
    
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>分行/ATM/證券據點維護-查詢</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >據點ID：</label>
                    <label class="col-4 control-label" >${Data.BHID}</label>
                    <label class="col-2 control-label text-right" >據點類型：</label>
                    <div class="col-4">
                        <select class="form-control" name="BHADTYPE" id="BHADTYPE">
                            <option value="">---請選擇---</option>
                            <option value="1">分行</option>
                            <option value="2">證券</option>
                            <option value="3">ATM</option>
                        </select> 
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >據點名稱：</label>
                    <label class="col-4 control-label" >${Data.BHNAME}</label>
                    <label class="col-2 control-label text-right" for="BHCOUNTY">縣市別：</label>
                    <div class="col-4">
                        <select class="form-control" name="BHCOUNTY" id="BHCOUNTY">
                            <option value="">---請選擇---</option>
                            <c:forEach var="city" items="${DataCity}" varStatus="loop">
                                <option value="${city}">${city}</option>
                            </c:forEach>
                        </select> 
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">地址：</label>
                    <label class="col-4 control-label" >${Data.BHADDR}</label>
                    <label class="col-2 control-label text-right">緯/經度：</label>
                    <label class="col-4 control-label" >${Data.BHLATITUDE}／${Data.BHLONGITUDE}</label>
                </div>  
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="BHTELCOUNTRY">電話-國碼/區碼：</label>
                    <label class="col-4 control-label" >${Data.BHTELCOUNTRY}／${Data.BHTELREGION}</label>  
                    <label class="col-2 control-label text-right" for="BHTEL">電話號碼：</label>
                    <label class="col-2 control-label" >${Data.BHTEL}</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="BHSTIME" >開始營業時間：</label>
                    <label class="col-4 control-label" >${Data.BHSTIME}</label>   
                    <label class="col-2 control-label text-right" for="BHETIME">結束營業時間：</label>
                    <label class="col-4 control-label" >${Data.BHETIME}</label> 
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
            
        <script>
            $(document).ready(function () {
                // set select control disabled
                $('#BHADTYPE').val("${Data.BHADTYPE}")
                $('#BHADTYPE').attr('disabled', true);

                $('#BHCOUNTY').val("${Data.BHCOUNTY}")
                $('#BHCOUNTY').attr('disabled', true);
            }); //ready

            // 回查詢頁
            function onExit() {
                location.href="<%= request.getContextPath() %>/BM1000";
            }
        </script>
    </body>
</html>