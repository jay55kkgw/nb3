<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">

</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <c:if test="${!IsEdit}">
            <h2>分行/ATM/證券據點維護-新增</h2>
        </c:if>
        <c:if test="${IsEdit}">
            <h2>分行/ATM/證券據點維護-修改</h2>
        </c:if>
        <form id="frmBranch" class="container main-content-block p-4" action="/dummy" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right">據點ID：</label>
                <div class="col-4">
                    <input type="text" id="BHID" name="BHID" value="${ data.BHID }" class="form-control"
                        readonly="readonly" />(系統自動驗號)
                </div>
                <label class="col-2 control-label text-right">據點類型：</label>
                <div class="col-4">
                    <select class="form-control" name="BHADTYPE" id="BHADTYPE">
                        <option value="">---請選擇---</option>
                        <option value="1">分行</option>
                        <option value="2">證券</option>
                        <option value="3">ATM</option>
                    </select>
                </div>
            </div>
            <div id="divFileM" class="form-group row">
                <label class="col-2 control-label text-right">據點名稱：</label>
                <div class="col-4">
                    <input type="text" id="BHNAME" name="BHNAME" value="${ data.BHNAME }" class="form-control"
                        maxlength="20" />
                </div>
                <label class="col-2 control-label text-right" for="BHCOUNTY">縣市別：</label>
                <div class="col-4">
                    <select class="form-control" name="BHCOUNTY" id="BHCOUNTY">
                        <option value="">---請選擇---</option>
                        <c:forEach var="city" items="${DataCity}" varStatus="loop">
                            <option value="${city.ADMSYSCODED_PK.CODEID}">${city.CODENAME}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div id="divFileS" class="form-group row">
                <label class="col-2 control-label text-right">地址：</label>
                <div class="col-10">
                    <input type="text" id="BHADDR" name="BHADDR" value="${ data.BHADDR }" class="form-control"
                        maxlength="83" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="BHLATITUDE">緯度：</label>
                <div class="col-4">
                    <input type="text" id="BHLATITUDE" name="BHLATITUDE" value="${ data.BHLATITUDE }"
                        class="form-control" maxlength="20" />
                </div>
                <label class="col-2 control-label text-right" for="BHLONGITUDE">經度：</label>
                <div class="col-4">
                    <input type="text" id="BHLONGITUDE" name="BHLONGITUDE" value="${ data.BHLONGITUDE }"
                        class="form-control" maxlength="20" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="BHTELCOUNTRY">電話-國碼：</label>
                <div class="col-2">
                    <input type="number" id="BHTELCOUNTRY" name="BHTELCOUNTRY" value="${ data.BHTELCOUNTRY }"
                        class="form-control"  oninput="if(value.length>5)value=value.slice(0,5)"/>
                </div>
                <label class="col-2 control-label text-right" for="BHTELREGION">電話-區碼：</label>
                <div class="col-2">
                    <input type="number" id="BHTELREGION" name="BHTELREGION" value="${ data.BHTELREGION }"
                        class="form-control"  oninput="if(value.length>5)value=value.slice(0,5)"/>
                </div>
                <label class="col-2 control-label text-right" for="BHTEL">電話號碼：</label>
                <div class="col-2">
                    <input type="number" id="BHTEL" name="BHTEL" value="${ data.BHTEL }" class="form-control"
                    oninput="if(value.length>15)value=value.slice(0,15)" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="BHSTIME">開始營業時間：</label>
                <div class="col-4">
                    <input type="text" id="BHSTIME" name="BHSTIME" value="${ data.BHSTIME }" class="form-control"
                        maxlength="10" />
                </div>
                <label class="col-2 control-label text-right" for="BHETIME">結束營業時間：</label>
                <div class="col-4">
                    <input type="text" id="BHETIME" name="BHETIME" value="${ data.BHETIME }" class="form-control"
                        maxlength="10" />
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-3 col-9">
                    <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align"
                        onclick="onExit()" />&nbsp;
                    <c:choose>
                        <c:when test="${IsEdit}">
                            <input id="btnDelete" type="button" value="刪除" class="btn btn-danger"
                                aria-label="Left Align" onclick="onDelete()" />&nbsp;
                            <input id="btnEdit" type="button" value="確定" class="btn btn-info" aria-label="Left Align"
                                onclick="onEdit()" />
                        </c:when>
                        <c:otherwise>
                            <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align"
                                onclick="onSave()" />
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </form>
    </div>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

    <script src="<%= request.getContextPath() %>/script/respond.js"></script>

    <script>
        $(document).ready(function () {
            $("#BHADTYPE").val("${data.BHADTYPE}");
            $("#BHCOUNTY").val("${data.BHCOUNTY}");
        }); //ready

        $.fn.serializeObject = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };

        $.fn.serializeObjectEx = function () {
            var o = {};
            //    var a = this.serializeArray();
            $(this).find('input[type="hidden"], input[type="text"], input[type="password"],input[type="number"], input[type="checkbox"]:checked, input[type="radio"]:checked, select').each(function () {
                if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
                    var $parent = $(this).parent();
                    var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
                    if ($chb != null) {
                        if ($chb.prop('checked')) return;
                    }
                }
                if (this.name === null || this.name === undefined || this.name === '')
                    return;
                var elemValue = null;
                if ($(this).is('select'))
                    elemValue = $(this).find('option:selected').val();
                else elemValue = this.value;
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(elemValue || '');
                } else {
                    o[this.name] = elemValue || '';
                }
            });
            return o;
        }
        // 回查詢頁
        function onExit() {
            location.href="<%= request.getContextPath() %>/BM1000";
        }

        // 新增
        function onSave() {
            var formData = $("#frmBranch").serializeObjectEx();
            $("span.error").remove();
            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/BM1000/Create",
                type: "POST",
                data: formData,
                success: function (res) {
                    unlockScreen();
                    if (res.validated) {
                        alert("分行/ATM/證券據點新增成功");
                        location.href="<%= request.getContextPath() %>/BM1000";
                    } else {
                        //Set error messages
                        $.each(res.errorMessages, function (key, value) {
                            if (key === "summary")
                                alert(value);
                            else {
                                if (key === "BHADTYPE" || key === "BHCOUNTY")
                                    $('select[name=' + key + ']').after('<span class="error">' + value + '</span>');
                                else
                                    $('input[name=' + key + ']').after('<span class="error">' + value + '</span>');
                            }

                        });
                    }
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }

        // 修改
        function onEdit() {
            var formData = $("#frmBranch").serializeObjectEx();
            $("span.error").remove();
            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/BM1000/Edit",
                type: "POST",
                data: formData,
                success: function (res) {
                    unlockScreen();
                    if (res.validated) {
                        alert("分行/ATM/證券據點修改成功");
                        location.href="<%= request.getContextPath() %>/BM1000";
                    } else {
                        //Set error messages
                        $.each(res.errorMessages, function (key, value) {
                            if (key === "summary")
                                alert(value);
                            else {
                                if (key === "BHADTYPE" || key === "BHCOUNTY")
                                    $('select[name=' + key + ']').after('<span class="error">' + value + '</span>');
                                else
                                    $('input[name=' + key + ']').after('<span class="error">' + value + '</span>');
                            }
                        });
                    }
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }

        // 刪除
        function onDelete() {
            var bhid = $("#BHID").val();
            if (!confirm("確定刪除分行/ATM/證券據點[" + bhid + "]")) {
                return;
            }
            $("span.error").remove();
            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/BM1000/Delete/" + bhid,
                type: "POST",
                data: {},
                success: function (res) {
                    unlockScreen();
                    if (res === "0") {
                        alert("分行/ATM/證券據點刪除成功");
                        location.href="<%= request.getContextPath() %>/BM1000";
                    } else {
                        alert(res);
                    }
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }
    </script>
</body>

</html>