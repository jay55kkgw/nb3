<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">

</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>分行/ATM/證券據點維護</h2>
        <form class="container main-content-block p-4" action="/dummy" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="BHADTYPE">據點類型：</label>
                <div class="col-4">
                    <select class="form-control" name="BHADTYPE" id="BHADTYPE">
                        <option value="">---請選擇---</option>
                        <option value="1">分行</option>
                        <option value="2">證券</option>
                        <option value="3">ATM</option>
                    </select>
                </div>
                <label class="col-2 control-label text-right" for="BHNAME">據點名稱：</label>
                <div class="col-4">
                    <input type="text" id="BHNAME" name="BHNAME" class="form-control" maxlength="45" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="BHCOUNTY">縣市別：</label>
                <div class="col-4">
                    <select class="form-control" name="BHCOUNTY" id="BHCOUNTY">
                        <option value="">---請選擇---</option>
                        <c:forEach var="city" items="${DataCity}" varStatus="loop">
                            <option value="${city.ADMSYSCODED_PK.CODEID}">${city.CODENAME}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="offset-2 col-10">
                <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                <input id="btnQuery" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />
                <c:if test="${allowEdit}">
                    <input id="btnCreate" type="button" value="新增" class="btn btn-primary" aria-label="Left Align" onclick="onCreate()" />
                </c:if>
            </div>
        </form>
    </div>
    <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>據點類型</th>
                <th>據點名稱</th>
                <th>地址</th>
                <th>電話</th>
                <th>開始營業時間</th>
                <th>結束營業時間</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            onQuery();
        }); //ready

        // 新增
        function onCreate() {
            location.href = "<%= request.getContextPath() %>/BM1000/Create";
        }

        function onQuery() {
            $("span.error").remove();

            $("#main").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "order": [[0, "asc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/BM1000/Query",
                    "data": {
                        "BHADTYPE": $("#BHADTYPE").val(),
                        "BHNAME": $("#BHNAME").val(),
                        "BHCOUNTY": $("#BHCOUNTY").val()
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "bhid" },
                    { "data": "bhname" },
                    { "data": "bhaddr" },
                    { "data": "bhtel" },
                    { "data": "bhstime" },
                    { "data": "bhetime" }
                ],
                "createdRow": function (row, data, index) {
                    // 處理分行代碼                
                    var key = data.bhid;
                    var text;

                    switch (data.bhadtype) {
                        case "1":
                            text = "分行";
                            break;
                        case "2":
                            text = "證券";
                            break;
                        case "3":
                            text = "ATM";
                            break;
                        default:
                            text = "";
                    }
                    var $code = $("<a>" + text + "</a>");
                    if ( "${allowEdit}" == "true" ) {
                        $code.attr("href", "<%= request.getContextPath() %>/BM1000/Edit/" + key);
                    } else {
                        $code.attr("href", "<%= request.getContextPath() %>/BM1000/Get/" + key);
                    }
                    $("td", row).eq(0).text("").append($code);
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            $("#main").show();
            return false;
        }
    </script>
</body>

</html>