<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<% response.setHeader("Content-Security-Policy", "frame-ancestors 'self'"); %>

<html>
<head>
<title>台企銀 後台首頁</title>
<meta charset="UTF-8">

<!-- include 後台管理 css -->
<jsp:include page="../include/admheadercs.jsp"></jsp:include>

<!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/script/DateTimePicker/jquery.datetimepicker.css">
</head>
<body>
	<!-- include master page menu -->
	<jsp:include page="../include/admmenu.jsp"></jsp:include>

	<!-- body content start -->
	<div class="container" id="main-content">
		<h2>首次登入資料轉檔紀錄查詢</h2>
	</div>

	<table id="main" class="display p-4 transparent" cellspacing="0"
		style="width: 100%">
		<thead>
			<tr>
				<th>使用者ID</th>
				<th>使用者設定檔</th>
				<th>台幣轉出紀錄檔</th>
				<th>台幣預約檔</th>
				<th>外幣轉出紀錄檔</th>
				<th>外幣預約檔</th>
				<th>黃金轉出紀錄檔</th>
				<th>常用帳號</th>
				<th>Email紀錄檔</th>
				<th>通訊錄設定檔</th>
				<th>KYC設定檔</th>
				<th>KYC歷史設定檔</th>
				<th>舊NNB資料表白名單</th>
				<th>推撥設定檔</th>
			</tr>
		</thead>
		<tbody>
<%-- 			<c:forEach var="Data" items="${data}"> --%>
<!-- 				<tr> -->
<%-- 					<td>${Data.CUSIDN}</td> --%>
<%-- 					<td>${Data.TXNUSER}</td> --%>
<%-- 					<td>${Data.TXNTWRECORD}</td> --%>
<%-- 					<td>${Data.TXNTWSCHPAY}</td> --%>
<%-- 					<td>${Data.TXNFXRECORD}</td> --%>
<%-- 					<td>${Data.TXNFXSCHPAY}</td> --%>
<%-- 					<td>${Data.TXNGDRECORD}</td> --%>
<%-- 					<td>${Data.TXNTRACCSET}</td> --%>
<%-- 					<td>${Data.ADMMAILLOG}</td> --%>
<%-- 					<td>${Data.TXNADDRESSBOOK}</td> --%>
<%-- 					<td>${Data.TXNCUSINVATTRIB}</td> --%>
<%-- 					<td>${Data.TXNCUSINVATTRHIST}</td> --%>
<%-- 					<td>${Data.NB3USER}</td> --%>
<%-- 					<td>${Data.TXNPHONETOKEN}</td> --%>
<!-- 				</tr> -->
<%-- 			</c:forEach> --%>
		</tbody>
	</table>
	<font style="color: red; text-decoration: underline; font-size: 20px">備註
		: "0" [成功] ，"1" [失敗] ， " "[未執行]</font>

	<jsp:include page="../include/caseStatus.jsp"></jsp:include>
	<!-- body content end -->

	<!-- include master page footer -->
	<jsp:include page="../include/admfooter.jsp"></jsp:include>

	<!-- include 後台管理 js -->
	<jsp:include page="../include/admfooterjs.jsp"></jsp:include>
	<script
		src="<%=request.getContextPath()%>/script/DateTimePicker/jquery.datetimepicker.js"></script>
	<script src="<%=request.getContextPath()%>/script/respond.js"></script>
	<script
		src="<%=request.getContextPath()%>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			//console.log("${data}");
			$("#main").DataTable({
		         "serverSide": true,
				 "responsive": true,
                 "processing": true,
                 "orderMulti": false,
                 "bFilter": false,
                 "bSortable": true,
                 "bDestroy": true,
                 "pageLength": 10,
                 "ajax": {
                     "type": "POST",
                     "url": "<%= request.getContextPath() %>/B213/Query",
                     "data": {
//                      	"ADMCODE":  admcode
                     },
                     error:function(e){
                     	console.log(e);
                     }
                 },
                 "sServerMethod": "post",
                 "dom": "<'clear'>frtip",   //無工具列
		           "columns": [
		               { "data": "cusidn" },
		               { "data": "txnuser" },
		               { "data": "txntwrecord" },
		               { "data": "txntwschpay" },
		               { "data": "txnfxrecord" },
		               { "data": "txnfxschpay" },
		               { "data": "txngdrecord" },
		               { "data": "txntraccset" },
		               { "data": "admmaillog" },
		               { "data": "txnaddressbook" },
		               { "data": "txncusinvattrib" },
		               { "data": "txncusinvattrhist" },
		               { "data": "nb3USER" },
		               { "data": "txnphonetoken" },
		           ], 
		           "createdRow": function (row, data, index) {
	                   console.log(data);
	                },
                 "language": {
                     "url": "<%=request.getContextPath()%>/script/DataTables/chinese.json"
                 }
                 });		
		}); //ready
	</script>
</body>
</html>