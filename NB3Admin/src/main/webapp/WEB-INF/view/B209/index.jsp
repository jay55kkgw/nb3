<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>客戶登入/登出紀錄查詢</h2>
        <form class="container main-content-block p-4" action="/dummy" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right">使用者統編：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="UserId" name="UserId" class="form-control upper" maxlength="50" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">查詢期間起日：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="DateFrom" autocomplete="off" name="DateFrom" class="form-control" style="width:160px"
                            maxlength="10" value="${StartDt}" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">查詢期間迄日：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="DateTo" autocomplete="off" name="DateTo" class="form-control" style="width:160px"
                            maxlength="10" value="${EndDt}" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">登入來源：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <select class="form-control" name="LoginType" id="LoginType">
                            <option value="">---請選擇---</option>
                            <option value="NB">個網銀</option>
                            <option value="MB">行動銀行</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-2 col-10">
                    <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                    <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align"
                        onclick="onQuery()" />
                </div>
            </div>
        </form>
    </div>
    <table id="main" class="display p-4 transparent" cellspacing="0" style="width:98%">
        <thead>
            <tr>
                <th style="width:20%">使用者統編</th>
                <th style="width:10%">登入/出</th>
                <th style="width:10%">登入/出 IP</th>
                <th style="width:15%">登入/出 時間</th>
                <th style="width:13%">登入來源</th>
                <th style="width:30%">使用者識別</th>
                <th style="width:30%">登入結果</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
    <script>
        $(document).ready(function () {
            $('#DateFrom').datetimepicker({
                lang: 'zh-TW',
                timepicker: false,
                format: 'Y/m/d',   // 顯示時分
                scrollMonth: false
            });
            $('#DateTo').datetimepicker({
                lang: 'zh-TW',
                timepicker: false,
                format: 'Y/m/d',   // 顯示時分
                scrollMonth: false
            });
            $("#main").hide();
        }); //ready


        function onQuery() {
            if ( $('#DateFrom').val() == "" || $('#DateTo').val() == "" ) {
                alert("請輸入查詢期間起迄日");
                return;
            }

            var startDate = $("#DateFrom").val();
            var endDate = $("#DateTo").val();

            var sisvalid = Date.parse(startDate);
            var eisvalid = Date.parse(endDate);
            if(isNaN(sisvalid) || isNaN(eisvalid) ) { 
                alert("請輸入合法的查詢期間起日和查詢期間迄日");
                return;
            }

            if ( sisvalid > eisvalid ) {
                alert("查詢期間迄日必需大於查詢期間起日");
                return;
            }
            var today=new Date();
            if ( eisvalid>today ) {
                alert("查詢期間起日與迄日必須小於等於今天");
                return;
            }

            $("span.error").remove();

            $("#main").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "order": [[3, "desc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/B209/Query",
                    "data": {
                        "STARTDATE": $("#DateFrom").val(),
                        "ENDDATE": $("#DateTo").val(),
                        "USERID": $("#UserId").val(),
                        "LOGINTYPE": $("#LoginType").val()
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "aduserid" },
                    { "data": "loginout" },
                    { "data": "aduserip" },
                    { "data": "cmyyyymm" },
                    { "data": "logintype" },
                    { "data": "tokenid" },
                    { "data": "adexcode"}
                ],
                "createdRow": function (row, data, index) {
                    var loginout;
                    switch (data.loginout) {
                        case "0":
                            loginout = "登入";
                            break;
                        case "1":
                            loginout = "登出";
                            break;
                    }
                    $("td", row).eq(1).text("").append(loginout);
                    var logintime;
                    var cmyyyymm = data.cmyyyymm;
                    var cmdd = data.cmdd;
                    var cmtime = data.cmtime;
                    var sDate = cmyyyymm.substr(0, 4) + "/" + cmyyyymm.substr(4, 2)+"/"+cmdd+" "+cmtime.substr(0,2)+":"+cmtime.substr(2,2)+":"+cmtime.substr(4,2);
                    $("td", row).eq(3).text("").append(sDate);

                    var logintype;
                    switch (data.logintype) {
                        case "NB":
                            loginout = "個網銀";
                            break;
                        case "MB":
                            loginout = "行動銀行";
                            break;
                    }
                    $("td", row).eq(4).text("").append(loginout);

					/*
					 * "": 表示成功
					 *有值: 表示失敗代碼					
					 */
                    //console.log(data.adexcode == "");
					if(data.adexcode == ""){
			            $("td", row).eq(6).text("").append("成功");
					}
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            $("#main").show();
            return false;
        }


    </script>
</body>

</html>