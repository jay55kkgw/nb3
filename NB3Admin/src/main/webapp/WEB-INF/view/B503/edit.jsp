<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
    
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <c:if test="${IsReject}">
                <h2>優惠特店管理-退回</h2>
            </c:if>
            <c:if test="${!IsReject}">
                <h2>優惠特店管理</h2>
            </c:if>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >上架日期：</label>
                    <div class="col-3">
                        <div class="form-inline">
                            <input type="text" id="SDateTime" autocomplete="off" name="SDateTime" class="form-control" style = "width:160px" maxlength="17"  />
                        </div>
                    </div>
                    <label class="col-3 control-label text-right" >下架日期：</label>
                    <div class="col-3">
                        <div class="form-inline">
                            <input type="text" id="EDateTime" autocomplete="off" name="EDateTime" class="form-control" style = "width:160px" maxlength="17" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >特店類別：</label>
                    <div class="col-3">
                        <select id="ADTYPE" class="form-control">
                            <option value="1">分期付款商店</option>
                            <option value="2">特約折扣商店</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >商店分類：</label>
                    <div class="col-3">
                        <select id="STTYPE" class="form-control">
                        <c:choose>
                        	<c:when test="${Data.ADTYPE=='1'}">
                            <option value="1">百貨量販</option>
                            <option value="2">休閒生活</option>
                            <option value="3">３Ｃ家電</option>
                            <option value="4">網路電視購物</option>
                            <option value="5">時尚品味</option>
                            <option value="6">行動通訊</option>
                            </c:when>
                            <c:when test="${Data.ADTYPE=='2'}">
                            <option value="1">美食饗宴</option>
                            <option value="2">居家生活</option>
                            <option value="3">時尚精品</option>
                            <option value="4">休閒渡假</option>
                            </c:when>
                        </c:choose>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >權重：</label>
                    <div class="col-1">
                        <input type="number" id="SORTORDER" name="SORTORDER" class="form-control" maxlength="10" value="${Data.STWEIGHT}" />
                    </div>
                </div>
                <c:choose>
                <c:when test="${Data.ADTYPE=='1'}">
	                <div class="form-group row">
	                    <label class="col-3 control-label text-right" >服務專線：</label>
	                    <div class="col-5">
	                        <input type="text" id="STTEL" name="STTEL" class="form-control" maxlength="20" value="${Data.STTEL}" />
	                    </div>
	                </div>
                </c:when>
                <c:when test="${Data.ADTYPE=='2'}">
	                <div class="form-group row">
	                    <label class="col-3 control-label text-right" >標題：</label>
	                    <div class="col-5">
	                        <input type="text" id="TxtTitle" name="TxtTitle" class="form-control" maxlength="20" value="${Data.STDADHLK}" />
	                    </div>
	                </div>
                </c:when>
                </c:choose>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >優惠內容：</label>
                    <div class="col-9">
                    	<c:choose>
                    		<c:when test="${Data.ADTYPE=='1'}">
		                    	<label>即日起～</label>
								<label><input type="text" id="StadDate" autocomplete="off" name="StadDate" class="form-control" style = "width:160px" maxlength="8" /></label>
								<label><input type="checkbox" id="StadShow" name="StadShow" >&nbsp;顯示以下內容&nbsp;</label>
								<input type="text" id="TxtCont1" name="TxtCont1" class="form-control" maxlength="30" style="display:none" >${Data.STDADCONT}</input>
							</c:when>
							<c:when test="${Data.ADTYPE=='2'}">
								<textarea  id="TxtCont2" name="TxtCont2" cols="90" rows="4" maxLength="360">${Data.STADINFO}</textarea>
							</c:when>
						</c:choose>
					</div>
                </div>
                <div class="form-group row" >
                    <label class="col-3 control-label text-right" >注意事項：</label>
                    <div class="col-9">
                        <textarea  id="StadNode" name="StadNode" cols="90" rows="3">${Data.STADNOTE}</textarea>
                    </div>
                </div>
                <c:if test="${Data.ADTYPE=='2'}">
                	<div class="form-group row">
						<label class="col-3 control-label text-right" >分店資訊：</label>
						<div class="col-9">
                        <textarea  id="ADInfo" name="ADInfo" cols="90" rows="3" maxLength="270">${Data.STADINFO}</textarea>
                    </div>
					</div>
                </c:if>
                
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >優惠特店圖檔：</label>
                    <div class="col-9">
                        <div class="form-inline">
                            <input type="file" id="file" name="file" class="form-control" maxlength="50" accept=".jpg, .gif" />&nbsp;<span></span>&nbsp;&nbsp;
                            <span>(${Data.STPICADD})</span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <div class="form-inline">
                            <input type="button" class="btn btn-info" value="上傳特店" onclick="onUpload();" />&nbsp;
                            <input id="btnPreview" type="button" class="btn btn-info" value="預覽" onclick="onPreview();" />
                            <input type="hidden" id="imgGuid" />
                        </div>
                    </div>
                </div>
                <!-- 
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >意見：</label>
                    <div class="col-9">
                        <input type="text" id="comments" name="comments" class="form-control" maxlength="255" value="${returnReason}" />
                    </div>
                </div>
                 -->
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                        <c:if test="${IsReject}">
                			<input id="btnCancel" type="button" value="取消送審" class="btn btn-danger" aria-label="Left Align" onclick="onCancel()" />&nbsp;                        
            			</c:if>
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onSave()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        
        <script>
            $(document).ready(function () {
                $('#btnPreviewFile').hide();
                $('#btnUploadFile').attr('disabled', true);
                $('#SDateTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false
                });
                $('#EDateTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false
                });
                $('#StadDate').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d',   // 顯示時分
                    timepicker: false,
                    scrollMonth: false
                });
                $("#SDateTime").val("${Data.SDATE}");
                $("#EDateTime").val("${Data.EDATE}");
                // 特店類別
                $("#ADTYPE > [value='${Data.ADTYPE}']").attr("selected", true);
                // 商店分類
                $("#STTYPE > [value='${Data.STTYPE}']").attr("selected", true);
                // 
                $("#StadDate").val("${Data.STADDATE}");
                if ("${Data.ADTYPE}" == "1" )
               	{
                	if("${Data.STADSHOW}" == "Y")
               		{
                    	$('#StadShow').prop('checked', true);
                    	$('#TxtCont1').show();
               		}
                    else
                   	{
                    	$('#StadShow').prop('checked', false);
                    	$('#TxtCont1').hide();
                   	}
               	}

                onChangeShow();
            }); //ready

            // 上傳廣告
            function onUpload() {
                var fileData = new FormData();
                var file = $("#file").get(0);

                if ( file.files.length == 0 ) {
                	alert("請選擇圖檔");
                    return;
               	} else {
                    fileData.append("multipartFile", file.files[0]);
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B503/ImageUpload/M",
                    type: "POST",
                    contentType: false,  // Not to set any content header
                    processData: false,  // Not to process data
                    data: fileData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("圖檔上傳成功");   
                            $("#imgGuid").val(res.pkey);
                            $("#btnPreview").show();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.responseText);
                    }
                });
            }

            // 回查詢頁
            function onExit() {
                history.back();
            }

            // 預覽廣告
            function onPreview() {
                var imageGuid = $("#imgGuid").val();
                
                if ( imageGuid === "" ) {
                    window.open("<%= request.getContextPath() %>/B503/CasePreview/${Data.STID}");
                } else {
                    window.open("<%= request.getContextPath() %>/B503/TmpPreview/" + imageGuid + "-M?Type=M&stid=${Data.STID}&t=");
                }
            }

            // 新增
            function onSave() {
                $("span.error").remove();
                
                if ( $("#StadNode").val() == "" ) {
                    alert("請輸入注意事項");
                    return;
                }
                
                if ( $("#SORTORDER").val() == "" ) {
                    alert("請輸入權重");
                    return;
                }
                
                var sisvalid = $("#StartDateTime").val();
                var eisvalid = $("#EndDateTime").val();

                if( sisvalid == "" || eisvalid == "" ) { 
                    alert("請輸入合法的上架日期和下架日期");
                    return;
                }

                //新增檢驗時間格式，避免手動把2位數的0去掉 EX:2020/01/01 => 2020/1/1 ，datetimepicker規則沒有強制補0
                if(($("#SDateTime").val().length != 16)  || ($("#EDateTime").val().length !=16) || ($("#StadDate").val().length!=10)){
                    alert("請輸入合法的時間格式");
                    return;						
                }
                
                var adcont="";
                if($("#ADTYPE").val()=="1")
               	{
               		if($("#TxtCont1").is(':visible'))
           			{
               			if($("#TxtCont1").val().length > 0 )
               				adcont=$("#TxtCont1").val();
           			}
               	}
                else
               	{
                	if($("#TxtCont2").val().length > 0 )
                		adcont=$("#TxtCont2").val();
               	}

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B503/ReSend",
                    type: "POST",
                    data: {
                        "STID" : "${Data.STID}",
                        "OID" : "${Data.OID}",
                        "SDateTime": $("#SDateTime").val(),
                        "EDateTime": $("#EDateTime").val(),
                        "ADTYPE": $("#ADTYPE").val(),
                        "STTYPE": $("#STTYPE").val(),
                        "STWEIGHT": $("#SORTORDER").val(),
                        "STTEL": $("#STTEL").val(),
                        "STADDATE": $("#StadDate").val(),
                        "STADSHOW": $('#StadShow').prop('checked') ? "Y" : "N",
                        "STDADCONT": adcont,
                        "STADNOTE": $("#StadNode").val(),
                        "STEPID": "${Data.STEPID}",
//                        "comments": $("#comments").val(),
                        "comments": "",
                        "imgGuid": $("#imgGuid").val(),
                        "STDADHLK": $("#ADTYPE").val()=="2" ? $("#TxtTitle").val() : "",
                        "STADINFO": $("#ADTYPE").val()=="2" ? $("#ADInfo").val() : ""
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("優惠特店重新送審成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B503";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onCancel() {
                $("span.error").remove();
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B503/Cancel",
                    type: "POST",
                    data: {
                        "STID" : "${Data.STID}",
                        "OID" : "${Data.OID}"
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("優惠特店取消送審成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B503";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
            
         	// 上傳廣告類型-檔案
//            function onUploadFile() {
//                var fileData = new FormData();
//                var file = $("#TARGETFILENAME").get(0);
//
//                if ( file.files.length == 0 ) {
//                    alert("請選擇上傳檔案");
//                    return;
//                } else {
//                    fileData.append("multipartFile", file.files[0]);
//                }
//
//                lockScreen();
//                $.ajax({
//                    url: "<%= request.getContextPath() %>/B503/UploadFile",
//                    type: "POST",
//                    contentType: false,  // Not to set any content header
//                    processData: false,  // Not to process data
//                    data: fileData,
//                    success: function (res) {
//                        unlockScreen();
//                        if(res.validated){
//                            alert("檔案上傳成功");   
//                            $("#fileGuid").val(res.pkey);
//                            $("#isTmp").val("true");
//                            $("#btnPreviewFile").show();
//                        } else {
//                            //Set error messages
//                            $.each(res.errorMessages,function(key,value){
//                                if ( key === "summary" )
//                                    alert(value);
//                                else 
//                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
//                            });
//                        }
//                    },
//                    error: function (err) {
//                        unlockScreen();
//                        alert(err.responseText);
//                    }
//                });
//            }
            
            // 預覽檔案
//            function onPreviewFile() {
//                var id="${Data.STID}";
//                var fileGuid = $("#fileGuid").val();
//                
//                if ( fileGuid === "" ) {
//                    window.open("<%=request.getContextPath()%>/B503/PreviewFile/"+id+"?isTmp="+$("#isTmp").val());
//                } else {
//                    window.open("<%=request.getContextPath()%>/B503/PreviewFile/"+fileGuid+"?isTmp="+$("#isTmp").val());
//                }
//            }
            
            function onChangeShow()
            {
            	$('[name=StadShow]').change(function () {
					if( $(this).prop("checked") )
					{
						$('#TxtCont1').show();
					}
					else
					{
						$('#TxtCont1').hide();
					}
            	})
            }
            
        </script>
    </body>
</html>