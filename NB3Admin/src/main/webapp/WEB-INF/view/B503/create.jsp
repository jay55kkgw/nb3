<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
    
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>優惠特店管理-新增</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >上架起日：</label>
                    <div class="col-3">
                        <div class="form-inline">
                            <input type="text" id="SDateTime" autocomplete="off" name="SDateTime" class="form-control" style = "width:160px" maxlength="8" />
                        </div>
                    </div>
                    <label class="col-3 control-label text-right" >下架迄日：</label>
                    <div class="col-3">
                        <div class="form-inline">
                            <input type="text" id="EDateTime" autocomplete="off" name="EDateTime" class="form-control" style = "width:160px" maxlength="8" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >特店類別：</label>
                    <div class="col-3">
                        <select id="ADTYPE" class="form-control" onchange="onChangeAdType(this);">
                            <option value="1">分期付款商店</option>
                            <option value="2">特約折扣商店</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >商店分類：</label>
                    <div class="col-3">
                        <select id="STTYPE" class="form-control">
                            <option value="1">百貨量販</option>
                            <option value="2">休閒生活</option>
                            <option value="3">３Ｃ家電</option>
                            <option value="4">網路電視購物</option>
                            <option value="5">時尚品味</option>
                            <option value="6">行動通訊</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >權重：</label>
                    <div class="col-1">
                        <input type="number" id="SORTORDER" name="SORTORDER" class="form-control" min="0" max="9" oninput="if(value.length>1)value=value.slice(0,1)" />
                    </div>
                </div>
                <div class="form-group row" id="divTxtTitle" style="display:none">
                    <label class="col-3 control-label text-right" >標題：</label>
                    <div class="col-9">
                        <input type="text" id="TxtTitle" name="TxtTitle" class="form-control" maxlength="20" />
                    </div>
                </div>
                <div class="form-group row" id="divSTTel">
                    <label class="col-3 control-label text-right" >服務專線：</label>
                    <div class="col-5">
                        <input type="text" id="STTEL" name="STTEL" class="form-control" maxlength="20" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >優惠內容：</label>
                    <div class="col-9" id="divTxtCont1">
                    	<label>即日起～</label>
						<label><input type="text" id="StadDate" autocomplete="off" name="StadDate" class="form-control" style = "width:160px" maxlength="8" /></label>
						<label><input type="checkbox" id="StadShow" name="StadShow" onchange="onChangeShow();" >&nbsp;顯示以下內容&nbsp;</label>
						<input type="text" id="TxtCont1" name="TxtCont1" class="form-control" maxlength="30" style="display:none" />
					</div>
					<div class="col-9" id="divTxtCont2" style="display:none">
                        <textarea  id="TxtCont2" name="TxtCont2" cols="90" rows="4" maxLength="360"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >注意事項：</label>
                    <div class="col-9">
                        <textarea  id="StadNode" name="StadNode" cols="90" rows="3" maxLength="270"></textarea>
                    </div>
                </div>
                <div class="form-group row" id="divADInfo" style="display:none">
                    <label class="col-3 control-label text-right" >分店資訊：</label>
                    <div class="col-9">
                        <textarea  id="ADInfo" name="ADInfo" cols="90" rows="3" maxLength="270"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >特店圖片：</label>
                    <div class="col-9">
                        <div class="form-inline">
                            <input type="file" id="file" name="file" class="form-control" maxlength="50" accept=".jpg, .gif" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <div class="form-inline">
                            <input type="button" class="btn btn-info" value="上傳特店圖片" onclick="onUpload();" />&nbsp;
                            <input id="btnPreview" type="button" class="btn btn-info" value="預覽" onclick="onPreview();" />&nbsp;(限上傳.JPG、.GIF、.BMP檔案)
                            <input type="hidden" id="imgGuid" />
                            <input type="hidden" id="TYPE" value="M" />
                        </div>
                    </div>
                </div>    
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onSave()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        
        <script>
            $(document).ready(function () {
                $("#btnPreview").hide();
                $("#fMemo").hide();
                $('#btnPreviewFile').hide();
                $('#btnUploadFile').attr('disabled', true);
                $('#SDateTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false //,
//                    step: 1
                });
                $('#EDateTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false // ,
//                    step: 1
                });
                $('#StadDate').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d',
                    scrollMonth: false,
                    timepicker: false
                });
                $('input[Type="Number"]').keypress(function (e) {
                    if ('0123456789'.indexOf(e.key)!=-1){}
                    else if (e.key=='.' && this.value!='' && (this.value.match("\.") || []).length==0){}
                    else{e.preventDefault();}
                });
//                onChangeShow();
            }); //ready

            // 上傳廣告
            function onUpload() {
                var fileData = new FormData();
                var type = $("#TYPE").val();
                var file = $("#file").get(0);

               	if ( file.files.length == 0 ) {
                	alert("請選擇圖檔");
                    return;
               	} else {
                    fileData.append("multipartFile", file.files[0]);
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B503/ImageUpload/"+type,
                    type: "POST",
                    contentType: false,  // Not to set any content header
                    processData: false,  // Not to process data
                    data: fileData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("圖檔上傳成功");   
                            $("#imgGuid").val(res.pkey);
                            $("#btnPreview").show();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.responseText);
                    }
                });
            }

            // 回查詢頁
            function onExit() {
                history.back();
            }

            // 預覽廣告
            function onPreview() {
                var obj = window.location;
                var imageGuid = $("#imgGuid").val();
                var targetType = "C";
                
               	var fileGuid = $("#fileGuid").val();
               	window.open("<%= request.getContextPath() %>/B503/TmpPreview/"+imageGuid+"?Type="+$("#TYPE").val()+"&t="+targetType);

            }

            // 新增
            function onSave() {
            	
                if ( $("#StadNode").val() == "" ) {
                    alert("請輸入注意事項");
                    return;
                }
                
                if ( $("#imgGuid").val() == "" ) {
                    alert("請先上傳廣告圖檔");
                    return;
                }

                if ( $("#SORTORDER").val() == "" ) {
                    alert("請輸入權重");
                    return;
                }
                var sisvalid = $("#SDateTime").val();
                var eisvalid = $("#EDateTime").val();

                if( sisvalid == '' || eisvalid == '' ) { 
                    alert("請輸入合法的上架日期和下架日期");
                    return;
                }

                //新增檢驗時間格式，避免手動把2位數的0去掉 EX:2020/01/01 => 2020/1/1 ，datetimepicker規則沒有強制補0
                if(($("#SDateTime").val().length != 16)  || ($("#EDateTime").val().length !=16) || ($("#StadDate").val().length!=10)){
                    alert("請輸入合法的時間格式");
                    return;						
                }
                
                var adcont="";
                if($("#ADTYPE").val()=="1")
               	{
               		if($("#TxtCont1").is(':visible'))
           			{
               			if($("#TxtCont1").val().length > 0 )
               				adcont=$("#TxtCont1").val();
           			}
               	}
                else
               	{
                	if($("#TxtCont2").val().length > 0 )
                		adcont=$("#TxtCont2").val();
               	}
                
                $("span.error").remove();
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B503/Send",
                    type: "POST",
                    data: {
                        "SDateTime": $("#SDateTime").val(),
                        "EDateTime": $("#EDateTime").val(),
                        "ADTYPE": $("#ADTYPE").val(),
                        "STTYPE": $("#STTYPE").val(),
                        "STWEIGHT": $("#SORTORDER").val(),
                        "STTEL": $("#STTEL").val(),
                        "STADDATE": $("#StadDate").val(),
                        "STADSHOW": $('#StadShow').prop('checked') ? "Y" : "N",
                        "STDADCONT": adcont,
                        "STADNOTE": $("#StadNode").val(),
                        "imgGuid": $("#imgGuid").val(),
                        "STDADHLK": $("#ADTYPE").val()=="2" ? $("#TxtTitle").val() : "",
                        "STADINFO": $("#ADTYPE").val()=="2" ? $("#ADInfo").val() : ""
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("廣告送審成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B503";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
            
            function onChangeShow()
            {
            	$('[name=StadShow]').change(function () {
					if( $(this).prop("checked") )
					{
						$('#TxtCont1').show();
					}
					else
					{
						$('#TxtCont1').hide();
					}
            	})
            }
            
            function onChangeAdType(obj)
            {
            	var AdTypeObj = new Object();
            	if($(obj).val() == "1")
           		{
                	AdTypeObj.AdTypeName = new Array("百貨量販", "休閒生活", "３Ｃ家電", "網路電視購物", "時尚品味", "行動通訊");
                	AdTypeObj.ObjItemValues = new Array("1", "2", "3", "4", "5", "6");
                	$('#divSTTel').show();
                	$('#divTxtTitle').hide();
                	$('#divTxtCont1').show();
                	onChangeShow();
                	$('#divTxtCont2').hide();
                	$('#divADInfo').hide();
           		}
            	else
           		{
                	AdTypeObj.AdTypeName = new Array("美食饗宴", "居家生活", "時尚精品", "休閒渡假");
                	AdTypeObj.AdTypeValues = new Array("1", "2", "3", "4");
                	$('#divSTTel').hide();
                	$('#divTxtTitle').show();
                	$('#divTxtCont1').hide();
                	$('#divTxtCont2').show();
                	$('#divADInfo').show();
           		}
            	
            	$("#STTYPE option").remove();
                for (i = 0; i < AdTypeObj.AdTypeName.length; i++) {
                    $("#STTYPE").append($("<option></option>").attr("value", AdTypeObj.AdTypeValues[i]).text(AdTypeObj.AdTypeName[i]));
                }
            }
        </script>
    </body>
</html>