<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>
    
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>優惠特店管理-覆核</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >上架日期：</label>
                    <label class="col-4 control-label">${SDateTime}</label>
                    <label class="col-2 control-label text-right" >下架日期：</label>
                    <label class="col-4 control-label">${EDateTime}</label>
                </div>
                <div class="form-group row">
	           	 	<label class="col-2 control-label text-right">特店類別：</label>
	            	<c:choose>
	           			<c:when test="${Data.ADTYPE=='1'}">
			                <label class="col-10 control-label">分期付款商店</label>
		               	</c:when>
		               	<c:when test="${Data.ADTYPE=='2'}">
			                <label class="col-10 control-label">特約折扣商店</label>
		               	</c:when>
		            </c:choose>
	            </div>
                <div class="form-group row">
	           	 	<label class="col-2 control-label text-right">商店分類：</label>
	           	 	<c:if test="${Data.ADTYPE=='1'}">
		            	<c:choose>
		           			<c:when test="${Data.STTYPE=='1'}">
				                <label class="col-10 control-label">百貨量販</label>
			               	</c:when>
			               	<c:when test="${Data.STTYPE=='2'}">
				                <label class="col-10 control-label">休閒生活</label>
			               	</c:when>
			               	<c:when test="${Data.STTYPE=='3'}">
				                <label class="col-10 control-label">３Ｃ家電</label>
			               	</c:when>
			               	<c:when test="${Data.STTYPE=='4'}">
				                <label class="col-10 control-label">網路電視購物</label>
			               	</c:when>
			               	<c:when test="${Data.STTYPE=='5'}">
				                <label class="col-10 control-label">時尚品味</label>
			               	</c:when>
			               	<c:when test="${Data.STTYPE=='6'}">
				                <label class="col-10 control-label">行動通訊</label>
			               	</c:when>
			            </c:choose>
		            </c:if>
		            <c:if test="${Data.ADTYPE=='2'}">
		            	<c:choose>
		           			<c:when test="${Data.STTYPE=='1'}">
				                <label class="col-10 control-label">美食饗宴</label>
			               	</c:when>
			               	<c:when test="${Data.STTYPE=='2'}">
				                <label class="col-10 control-label">居家生活</label>
			               	</c:when>
			               	<c:when test="${Data.STTYPE=='3'}">
				                <label class="col-10 control-label">時尚精品</label>
			               	</c:when>
			               	<c:when test="${Data.STTYPE=='4'}">
				                <label class="col-10 control-label">休閒渡假</label>
			               	</c:when>
			            </c:choose>
		            </c:if>
	            </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >權重：</label>
                    <label class="col-10 control-label">${Data.STWEIGHT}</label>
                </div>
                <c:choose>
		           	<c:when test="${Data.ADTYPE=='1'}">
		                <div class="form-group row">
		                    <label class="col-2 control-label text-right" >服務專線：</label>
		                    <label class="col-10 control-label">${Data.STTEL}</label>
		                </div>
                	</c:when>
                	<c:when test="${Data.ADTYPE=='2'}">
                		<div class="form-group row">
		                    <label class="col-2 control-label text-right" >標題：</label>
		                    <label class="col-10 control-label">${Data.STDADHLK}</label>
		                </div>
                	</c:when>
                </c:choose>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >優惠內容：</label>
                    <c:choose>
		           		<c:when test="${Data.ADTYPE=='1'}">
                    		<label class="col-2 control-label">即日起～${Data.STADDATE}</label>
                    		<label class="col-7 control-label"><input type="checkbox" id="StadShow" name="StadShow" disabled="disabled" >&nbsp;顯示以下內容&nbsp;</label>

	                    		<label class="col-2 control-label text-right" >&nbsp;</label>
								<label class="col-10 control-label"><input type="text" id="TxtCont1" name="TxtCont1" class="form-control" maxlength="30" disabled="disabled" value="${Data.STDADCONT}" /></label>
                    	</c:when>
                    	<c:when test="${Data.ADTYPE=='2'}">
                    		<label class="col-10 control-label"><textarea  id="TxtCont2" name="TxtCont2" cols="90" rows="4" disabled="disabled" >${Data.STDADCONT}</textarea></label>
                    	</c:when>
			    	</c:choose>
                </div>
                <div class="form-group row" id="divStadNote" >
                    <label class="col-2 control-label text-right" >注意事項：</label>
                    <label class="col-10 control-label"><textarea  id="StadNode" name="StadNode" cols="90" rows="3" disabled="disabled" >${Data.STADNOTE}</textarea></label>
                </div>
                <c:if test="${Data.ADTYPE=='2'}">
                	<div class="form-group row">
						<label class="col-2 control-label text-right" >分店資訊：</label>
						<label class="col-10 control-label"><textarea  id="StadNode" name="StadNode" cols="90" rows="3" disabled="disabled" >${Data.STADINFO}</textarea></label>
					</div>
                </c:if>
                <div class="form-group row">
                    <label id="lblFileL" class="col-2 control-label text-right" >特店圖片：</label>
                    <label class="col-10 control-label"><img src="${Data.IMGBASE64}" height="100" width="100" ></img></label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >退回原因：</label>
                    <div class="col-10">
                        <input type="text" id="Comments" name="Comments" class="form-control" maxlength="50" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                        <input id="btnPreview" type="button" class="btn btn-info" value="預覽" onclick="onPreview();" />&nbsp;                        
                        <input id="btnReject" type="button" value="退回" class="btn btn-danger" aria-label="Left Align" onclick="onReject()" />&nbsp;
                        <input id="btnApprove" type="button" value="覆核" class="btn btn-info" aria-label="Left Align" onclick="onApprove()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script>
            $(document).ready(function () {
            	if ( "${Data.ADTYPE}"=="1" && "${Data.STADSHOW}" == "Y")
           		{
            		$('#StadShow').prop('checked', true);
            		$('#divStadNote').show();
           		}
            }); //ready

            // 回查詢頁
            function onExit() {
                location.href="<%= request.getContextPath() %>/B503";
            }

            // 預覽廣告
            function onPreview() {
                window.open("<%= request.getContextPath() %>/B503/CasePreview/${Data.STID}");
            }

            // 同意
            function onApprove() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B503/Approve/${Data.STID}",
                    type: "POST",
                    data: {
                        "StepId":"${Data.STEPID}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("特店優惠覆核成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B503";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            // 退回
            function onReject() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B503/Reject/${Data.STID}",
                    type: "POST",
                    data: {
                        "StepId":"${Data.STEPID}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("廣告退回成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B503";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
        </script>
    </body>
</html>