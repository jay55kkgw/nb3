<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>
    </head>
    <body>
        <!-- include master page menu -->

        <!-- body content start -->
        <div class="container" id="main-content">
            <div class="form-group row">
            	<label class="col-2 control-label " >
            		<c:if test="${Data.ADTYPE=='1'}">
            		<c:choose>
		        		<c:when test="${Data.STTYPE=='1'}">
		               <label >百貨量販</label>
		             	</c:when>
		             	<c:when test="${Data.STTYPE=='2'}">
		               <label >休閒生活</label>
		             	</c:when>
		             	<c:when test="${Data.STTYPE=='3'}">
		               <label >３Ｃ家電</label>
		             	</c:when>
		             	<c:when test="${Data.STTYPE=='4'}">
		               <label >網路電視購物</label>
		             	</c:when>
		             	<c:when test="${Data.STTYPE=='5'}">
		               <label >時尚品味</label>
		             	</c:when>
		             	<c:when test="${Data.STTYPE=='6'}">
		               <label >行動通訊</label>
		             	</c:when>
		        	</c:choose>
		        	</c:if>
		        	<c:if test="${Data.ADTYPE=='2'}">
		        		${Data.STDADHLK}
		        	</c:if>
            	</label>
            	<label class="col-10 control-label">

	          </label>
         </div>
            <div class="form-group row">
                <label class="col-10 control-label">
                	<c:if test="${Data.IMGBASE64!=''}">
                		<img src="${Data.IMGBASE64}" height="150" width="100%" ></img>
                	</c:if>
                </label>
            </div>
            <c:if test="${Data.ADTYPE=='1'}">
	            <div class="form-group row">
	                <label class="col-2 control-label " >服務專線：</label>
	                <label class="col-10 control-label">${Data.STTEL}</label>
	            </div>
	            <div class="form-group row">
	                <label class="col-2 control-label " >活動期間：</label>
	                <label class="col-2 control-label">即日起～${Data.STADDATE}</label>
	            </div>
	            <div class="form-group row" >
	                <label class="col-2 control-label " >商店備註：</label>
	                <label class="col-10 control-label"><textarea  id="StadNode" name="StadNode" cols="90" rows="2" disabled="disabled" >${Data.STADNOTE}</textarea></label>
	            </div>
            </c:if>
            <c:if test="${Data.ADTYPE=='2'}">
	            <div class="form-group row">
	                <label class="col-2 control-label " >優惠內容：</label>
	                <label class="col-10 control-label">${Data.STDADCONT}</label>
	            </div>
	            <div class="form-group row" >
	                <label class="col-2 control-label " >注意事項：</label>
	                <label class="col-10 control-label"><textarea  id="StadNode" name="StadNode" cols="90" rows="2" disabled="disabled" >${Data.STADNOTE}</textarea></label>
	            </div>
	            <div class="form-group row" >
	                <label class="col-2 control-label " >分店資訊：</label>
	                <label class="col-10 control-label"><textarea  id="STAdInfo" name="STAdInfo" cols="90" rows="2" disabled="disabled" >${Data.STADINFO}</textarea></label>
	            </div>
            </c:if>
        </div>
        <!-- body content end -->


        <script>
            $(document).ready(function () {
            }); //ready

        </script>
    </body>
</html>