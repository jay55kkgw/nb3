<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>
    </head>
    <body>
        <!-- include master page menu -->

        <!-- body content start -->
        <div class="container" id="main-content">
            <div class="form-group row">
                <label class="col-10 control-label"><img src="${Data.IMGBASE64}" height="150" width="100%" ></img></label>
            </div>
        </div>
        <!-- body content end -->


        <script>
            $(document).ready(function () {
            }); //ready

            // 回查詢頁
            function onExit() {
                location.href="<%= request.getContextPath() %>/B503";
            }

            // 預覽廣告
            function onPreview() {
                window.open("<%= request.getContextPath() %>/B503/CasePreview/${Data.STID}");
            }

            // 同意
            function onApprove() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B503/Approve/${Data.STID}",
                    type: "POST",
                    data: {
                        "StepId":"${Data.STEPID}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("特店優惠覆核成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B503";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            // 退回
            function onReject() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B503/Reject/${Data.STID}",
                    type: "POST",
                    data: {
                        "StepId":"${Data.STEPID}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("廣告退回成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B503";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
        </script>
    </body>
</html>