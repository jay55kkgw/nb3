<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">     
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>優惠特店管理</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >上架起日：</label>
                    <div class="col-10">
                    	<div class="radio">
	                        <div class="form-inline">
	                        	<label><input type="radio" id="startfrom" name="soptradio" value="N" checked="checked" >&nbsp;不指定&nbsp;</label>
                            	<label><input type="radio" id="startto" name="soptradio" value="Y" >&nbsp;起訖&nbsp;
	                            	<input type="text" id="SDateFrom" autocomplete="off" name="SDateFrom" class="form-control" style = "width:160px" maxlength="16" value="${StartDtFrom}" disabled="disabled" />～
	                            	<input type="text" id="SDateTo" autocomplete="off" name="SDateTo" class="form-control" style = "width:160px" maxlength="16" value="${StartDtTo}" disabled="disabled" />
                            	</label>
	                        </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >下架迄日：</label>
                    <div class="col-10">
                    	<div class="radio">
	                        <div class="form-inline">
	                        	<label><input type="radio" id="endfrom" name="eoptradio" value="N" checked="checked" >&nbsp;不指定&nbsp;</label>
                            	<label><input type="radio" id="endto" name="eoptradio" value="Y" >&nbsp;起訖&nbsp;
	                            <input type="text" id="EDateFrom" autocomplete="off" name="EDateFrom" class="form-control" style = "width:160px" maxlength="10" value="${EndDtFrom}" disabled="disabled" />～
	                            <input type="text" id="EDateTo" autocomplete="off" name="EDateTo" class="form-control" style = "width:160px" maxlength="10" value="${EndDtTo}" disabled="disabled" />
                            	</label>
	                        </div>
                        </div>
                    </div>
                </div>
                 <div class="form-group row">
                    <label class="col-2 control-label text-right" >案件狀態：</label>
                    <div class="col-2">
                       	<select id="FlowFinished" class="form-control">
                       		<option value="Y">已結案(已上架)</option>
                       		<option value="N">未結案</option>
                       	</select>                    
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnClear" type="reset" value="重新輸入" class="btn btn-dark" aria-label="Left Align" onClick="window.location.reload();"/>&nbsp;
                        <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnCreate" type="button" value="新增" class="btn btn-info" aria-label="Left Align" onclick="onCreate()" />
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th>序號</th>
                    <th>特店類別</th>
                    <th>商店類別</th>
                    <th>服務專線</th>
                    <th>上版起日</th>
                    <th>上版迄日</th>
                    <th>狀態</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <span>如案件已結案，部門代碼／姓名請按已結案連結做查詢</span>

        <jsp:include page="../include/caseStatus.jsp"></jsp:include>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
        <script>
            var admAdsTmp=[];

            $(document).ready(function () {
                $("#main").hide();
                $('#SDateFrom').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false,
                    step: 1,
                    timepicker: false
                });
                $('#SDateTo').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false,
                    step: 1,
                    timepicker: false
                });
                $('#EDateFrom').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false,
                    step: 1,
                    timepicker: false
                });
                $('#EDateTo').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false,
                    step: 1,
                    timepicker: false
                });
                onChangeStart();
                onChangeEnd();
                onQuery();
            }); //ready
        
            // 新增
            function onCreate() {
                location.href = "<%= request.getContextPath() %>/B503/Create";
            }

            function onDataBind() {
                $("#main").dataTable({
                    "responsive": true,
                    "pageLength": 10, 
                    "paging": true,
                    "bFilter": false,
                    "bDestroy": true,
                    "bSort": false, 
                    "bLengthChange": false, 
                    "aaData": admStoreTmp,
                    "aoColumns": [
                        { "data": "stid" },
                        { "data": "adtype" },
                        { "data": "sttype" },
                        { "data": "sttel" },
                        { "data": "oid"},
                        { "data": "editoruname" },
                        { "data": "stepname" }
                    ],
                    "createdRow": function (row, data, index) {
                        // ID
                        var $id = $("<a>"+data.stid+"<a/>");
                        $id.attr("href", "<%= request.getContextPath() %>/B503/Query/"+data.stid);
                        $("td", row).eq(0).text("").append($id);

                        var adname = "";
                        if(data.adtype == "1")
                       	{
                        	adname="分期付款商店";
                       	}
                        else if(data.adtype == "2")
                       	{
                        	adname="特約折扣商店";
                       	}
                        else
                        	adname=data.adtype;
                        $("td", row).eq(1).text(adname);

                        var stname = "";
                        if(data.sttype == "1")
                       	{
                        	stname="百貨量販";
                       	}
                        else if(data.sttype == "2")
                       	{
                        	stname="休閒生活";
                       	}
                        else if(data.sttype == "3")
                       	{
                        	stname="３Ｃ家電";
                       	}
                        else if(data.sttype == "4")
                       	{
                        	stname="網路電視購物";
                       	}
                        else if(data.sttype == "5")
                       	{
                        	stname="時尚品味";
                       	}
                        else if(data.sttype == "6")
                       	{
                        	stname="行動通訊";
                       	}
                        else
                        	stname=data.sttype;
                        $("td", row).eq(2).text(stname);
                        
                        $("td", row).eq(3).text(data.sttel);
                        // 處理起日                
                        var startDt = data.sdate.substring(0,4)+"-"+data.sdate.substring(4,6)+"-"+data.sdate.substring(6,8);
                        startDt += " "+data.stime.substring(0,2)+":"+data.stime.substring(2,4)
                        
                        $("td", row).eq(4).text(startDt);

                        var endDt = data.edate.substring(0,4)+"-"+data.edate.substring(4,6)+"-"+data.edate.substring(6,8);
                        endDt += " "+data.etime.substring(0,2)+":"+data.etime.substring(2,4)
                        
                        $("td", row).eq(5).text(endDt);
                        var $caseStatus;
                        if ( data.stepname == "" ) {
                            $caseStatus = $("<a href='#'>已結案<a/>");
                        } else {
                            $caseStatus = $("<a href='#'>"+data.stepname+"<a/>");
                        }
                        $caseStatus.attr("onclick", "showStatus('"+data.stid+"');");
                        $("td", row).eq(6).text("").append($caseStatus);

                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }

            function onQuery() {
            	var reg =/\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}/;
            	var schecked = $('[name=soptradio]:checked');
//        	    console.log('soptradio 选中的值:', checked.val())
//				alert("soptradio 选中的值:" + schecked.val());
        	    if(schecked.val() == "Y")
       	    	{
                    var sisvalid = Date.parse($("#SDateFrom").val());
                    var eisvalid = Date.parse($("#SDateTo").val());
                  
                    if(isNaN(sisvalid) || isNaN(eisvalid)  ) { 
                        alert("請輸入合法的起日和迄日");
                        return;
                    }

                    if(!(reg.test($("#SDateFrom").val()) && reg.test($("#SDateTo").val()))){
                  	  alert("請輸入合法的起日和迄日");
                        return;
                      }
                    /*
                    if ( sisvalid > sisvalid ) {
                        alert("迄日必需大於起日");
                        return;
                    }
                    */       	    	
       	    	}

        	    var echecked = $('[name=eoptradio]:checked');
//        	    console.log('eoptradio 选中的值:', checked.val())
//        	    alert("eoptradio 选中的值:" + echecked.val());
                if(echecked.val() == "Y")
               	{
                    var sisvalid = Date.parse($("#EDateFrom").val());
                    var eisvalid = Date.parse($("#EDateTo").val());


                    
                    if(isNaN(sisvalid) || isNaN(eisvalid) ) { 
                        alert("請輸入合法的起日和迄日");
                        return;
                    }

                    if(!(reg.test($("#EDateFrom").val()) && reg.test($("#EDateTo").val()))){
                    	  alert("請輸入合法的起日和迄日");
                          return;
                        }
                    /*
                    if ( eisvalid > eisvalid ) {
                        alert("迄日必需大於起日");
                        return;
                    }
                    */
               	
               	}

                var url = "<%= request.getContextPath() %>/B503/IndexQuery";
                lockScreen();
                $.post(url, {
                	"SRedio" : schecked.val(),
                	"ERedio" : echecked.val(),
                    "SDateFrom" : schecked.val() == "N" ? "" : $("#SDateFrom").val(),
                    "SDateTo" : schecked.val() == "N" ? "" : $("#SDateTo").val(),
                    "EDateFrom" : echecked.val() == "N" ? "" : $("#EDateFrom").val(),
                    "EDateTo" : echecked.val() == "N" ? "" : $("#EDateTo").val(),
                    "FlowFinished": $("#FlowFinished").val()
                }, function (data, status) {
                    unlockScreen();
                    if (status == "success") {
                        //alert(data);
                        admStoreTmp = data;
                        onDataBind();
                    }
                });
            }

            function onChangeStart()
            {
            	$('[name=soptradio]').change(function () {
            	    var checked = $('[name=soptradio]:checked');
            	    console.log('onChangeStart 选中的值:', checked.val())
					if( checked.val() == "Y" )
					{
						$("#SDateFrom").prop('disabled', false);
						$("#SDateTo").prop('disabled', false);
					}
					else
					{
						$("#SDateFrom").prop('disabled', true);
						$("#SDateTo").prop('disabled', true);
					}
            	})
            }
            
            function onChangeEnd()
            {
            	$('[name=eoptradio]').change(function () {
            	    var checked = $('[name=eoptradio]:checked');
            	    console.log('ChangeEnd 选中的值:', checked.val())
					if( checked.val() == "Y" )
					{
						$("#EDateFrom").prop('disabled', false);
						$("#EDateTo").prop('disabled', false);
					}
					else
					{
						$("#EDateFrom").prop('disabled', true);
						$("#EDateTo").prop('disabled', true);
					}
            	})
            }

        </script>
    </body>
</html>