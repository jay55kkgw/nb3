<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html> 
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>一般網銀預約交易紀錄</h2>
            <br/><br/>
            <c:choose>
            	<c:when test="${__LOGIN_ROLES__=='OP'}">
	            	<div class="conta/c:wheniner main-content-block p-4">
	            		<div class="form-group row">
		                    <label class="col-4 control-label text-right">今日尚未執行台幣預約筆數</label>
		                    <div class="col-2">共${DataOP["notExecuteTWCnt"]}筆</div>
		  					<label class="col-4 control-label text-right">今日尚未執行外幣預約筆數</label>
		                    <div class="col-2">共${DataOP["notExecuteFXCnt"]}筆</div>
	                	</div>
	                	<hr>
	                	<div class="form-group row">
		                    <label class="col-4 control-label text-right">今日台幣預約總筆數</label>
		                    <div class="col-2">共${DataOP["notExecuteTWCnt"]+DataOP["succCnt"]+DataOP["systemReasonFailCnt"]+DataOP["userReasonFailCnt"]+DataOP["otherReasonFailCnt"]}筆</div>
		  					<label class="col-4 control-label text-right">今日台幣預約成功筆數</label>
	                		<div class="col-2">共${DataOP["succCnt"]}筆</div>
	                	</div>
		              	<div class="form-group row">
		                    <label class="col-4 control-label text-right">今日台幣預約失敗筆數(系統面)</label>
		                    <div class="col-2">共${DataOP["systemReasonFailCnt"]}筆</div>
		  					<label class="col-4 control-label text-right">今日台幣預約失敗筆數(客戶操作面)</label>
		                    <div class="col-2">共${DataOP["userReasonFailCnt"]}筆</div>
		                </div>
		               	<div class="form-group row">
		                    <label class="col-4 control-label text-right">今日台幣預約失敗筆數(其它)</label>
		                    <div class="col-2">共${DataOP["otherReasonFailCnt"]}筆</div>
		  					<label class="col-4 control-label text-right">今日預約失敗總筆數</label>
		                    <div class="col-2">共${DataOP["systemReasonFailCnt"]+DataOP["userReasonFailCnt"]+DataOP["otherReasonFailCnt"]}筆</div>
		                </div>
		            </div>
		   		</c:when>  
              	<c:otherwise>
		            <form class="conta/c:wheniner main-content-block p-4" id="B703" name="B703" action="<%= request.getContextPath() %>/B703/Query" method="post">
		                <div class="form-group row">
		                    <label class="col-2 control-label text-right" >查詢期間起~迄：</label>
		                    <div class="col-10">
		                    	<div class="form-inline">
			                    	<input type="text" id="startDate" autocomplete="off" name="startDate" class="form-control" style="width:150px" maxlength="10" />～
			                    	<input type="text" id="endDate" autocomplete="off" name="endDate" class="form-control" style="width:150px" maxlength="10" />	
		                    	</div>
		                    </div>
		                </div>
		                <div class="form-group row">
		                    <div class="offset-2 col-10"> 
		                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
		                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
		                        <input id="btnexpCSV" type="button" value="匯出CSV" class="btn btn-secondary" aria-label="Left Align" onclick="onReport();" />&nbsp;
		                    </div>
		                </div>
		            </form>
		        </div>
		
		        <table id="main" name="main" class="table table-striped table-bordered" style="width:97%">
		            <thead>
		                <tr>
		                    <th style="width:10%">日期</th>
		                    <th style="width:10%">預約成功筆數</th>
		                    <th style="width:15%">預約失敗筆數(操作因素)</th>
		                    <th style="width:15%">預約失敗筆數(系統因素)</th>
   		                    <th style="width:12%">預約失敗筆數(其它)</th>
		                    <th style="width:12%">預約失敗總筆數</th>
   		                    <th style="width:12%">預約未執行筆數</th>
		                    <th style="width:10%">預約總筆數</th>
		                </tr>
		            </thead>
		            <tbody>
		                                                                 
		            </tbody>
		        </table>
		    </c:otherwise> 
		</c:choose>       
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
		<script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            var reportData = [];           // 報表資料
			var login_Roles="${__LOGIN_ROLES__}";
			
            $(document).ready(function () {
            	$('#startDate').datetimepicker({
					lang: 'zh-TW',
					timepicker: false,
					format: 'Y-m-d',   // 顯示年月日
					scrollMonth: true,
                   	// hours12: false,
					maxTime: false,
                });
            	$('#endDate').datetimepicker({
					lang: 'zh-TW',
					timepicker: false,
					format: 'Y-m-d',   // 顯示年月日
					scrollMonth: true,
                   	// hours12: false,
					maxTime: false,
                });
            	$('#startDate').val("${Date}");
            	$('#endDate').val("${Date}");
                if (login_Roles!="OP")
               	{
                    onQuery();
               	}            
            });
			
            function onQuery() {
                $("#main").hide();
                var url = "<%= request.getContextPath() %>/B706/Query";
                var startDate = $("#startDate").val().replace(/-/g,'');
                var endDate = $("#endDate").val().replace(/-/g,'');
                lockScreen();
                $.post(url, {
                    "startDate" : startDate,
                    "endDate" : endDate,
                }, function (data, status) {
                    unlockScreen();
                    if (status == "success") {
                        var errorMsg = data.errorMsg;
                        
                        if (  errorMsg == "" ) {
                            reportData = data.reportData;

                            onReportDataBind();

                            $("#main").show();
                        } else {
                            alert(errorMsg);
                        }
                    }
                });
            }

            function onReportDataBind() {
                $("#main").dataTable({
                    "responsive": true,
                    "bPaginate": false,
                    "bFilter": false,
                    "bDestroy": true,
                    "bSort": false,
                    "info": false,
                    "autoWidth": false,
                    "aaData": reportData,
                    "aoColumns": [
                    	{ "mDataProp": "txDay" },
                        { "mDataProp": "succCnt" },
                        { "mDataProp": "userReasonFailCnt" },
                        { "mDataProp": "systemReasonFailCnt" },
                        { "mDataProp": "otherReasonFailCnt" },
                        { "mDataProp": "failCnt" },
                    	{ "mDataProp": "notExecuteCnt" },
                        { "mDataProp": "totalCnt" }
                    ],
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
            }

            function onReport() {
            	var startDate = $("#startDate").val().replace(/-/g,'');
                var endDate = $("#endDate").val().replace(/-/g,'');

                window.location = "<%= request.getContextPath() %>/B706/Report?startDate="+startDate+"&endDate="+endDate;
            }
        </script>
    </body>
</html>