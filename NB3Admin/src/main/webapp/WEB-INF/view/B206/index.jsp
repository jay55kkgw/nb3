<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>黃金存摺交易紀錄查詢</h2>
        <form id="B206" name="B206" class="container main-content-block p-4"
            action="<%= request.getContextPath() %>/B206/Send" method="post">
            <div class="form-group row">
                <label class="col-4 control-label text-right">身分證/營利事業統一編號：</label>
                <div class="col-8">
                    <div class="form-inline">
                        <input type="text" id="UserId" name="UserId" class="form-control upper" maxlength="10"
                         value="${B206Model.userId}" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-4 control-label text-right">查詢期間起：</label>
                <div class="col-8">
                    <div class="form-inline">
                        <input type="text" id="dateFrom" autocomplete="off" name="dateFrom" class="form-control" style="width:160px"
                            maxlength="19" value="${B206Model.dateFrom}" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-4 control-label text-right">查詢期間迄：</label>
                <div class="col-8">
                    <div class="form-inline">
                        <input type="text" id="dateTo" autocomplete="off" name="dateTo" class="form-control" style="width:160px"
                            maxlength="19" value="${B206Model.dateTo}" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-4 col-8">
                    <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                    <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align"
                        onclick="onQuery()" />
                </div>
            </div>
        </form>
    </div>
    <!-- report -->
    <div id="printQ">
        <div class="form-group row">
            <label class="col-12 control-label text-center">
                <h4>黃金存摺交易紀錄查詢</h4>
            </label>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right">查詢期間起：</label>
            <div class="col-10">
                <div class="form-inline">
                    ${B206Model.dateFrom}
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right">查詢期間迄：</label>
            <div class="col-10">
                <div class="form-inline">
                    ${B206Model.dateTo}
                </div>
            </div>
        </div>
    </div>
    <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>交易時間</th>
                <th>交易項目</th>
                <th>客戶統一編號</th>
                <th>黃金存摺帳號</th>
                <th>交易數量(公克)</th>
                <th>牌告單價</th>
                <th>折讓後單價</th>
                <th>即時Email通知(Email Addr.)</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
    <script>
        $(document).ready(function () {

            $('#dateFrom').datetimepicker({
                lang: 'zh-TW',
                timepicker: true,
                format: 'Y/m/d H:i:00',   // 顯示時分
                scrollMonth: false,
                hours12: false,
                maxTime: false
            });
            $('#dateTo').datetimepicker({
                lang: 'zh-TW',
                timepicker: true,
                format: 'Y/m/d H:i:00',   // 顯示時分
                scrollMonth: false,
                hours12: false,
                maxTime: false
            });

            $("#printQ").hide();
            $("#main").hide();

            var userId = "${B206Model.userId}";

            if ($("#UserId").val() != "") {

                onSearchData();
                $("#printQ").show();
            }

        }); //ready


        function onSearchData() {
            $("span.error").remove();

            $("#main").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "order": [[0, "asc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/B206/Query",
                    "data": {
                        "STARTDATE": $("#dateFrom").val(),
                        "ENDDATE": $("#dateTo").val(),
                        "USERID": $("#UserId").val()
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "lastdate" },
                    { "data": "adopid" },
                    { "data": "gduserid" },
                    { "data": null, orderable: false },
                    { "data": "gdweight" },
                    { "data": "gdprice" },
                    { "data": "gddiscount" },
                    { "data": "gdtxmails" }
                ],
                "columnDefs": [
                    { targets: 2, className: 'dt-body-right' },
                    { targets: 3, className: 'dt-body-right' },
                    { targets: 4, className: 'dt-body-right' },
                    { targets: 5, className: 'dt-body-right' },
                    { targets: 6, className: 'dt-body-right' }
                ], 
                "createdRow": function (row, data, index) {

                    var lastdate = data.lastdate;
                    var lasttime = data.lasttime;
                    var logintime = lastdate.substr(0, 4) + "/" + lastdate.substr(4, 2) + "/" + lastdate.substr(6, 2) + " " + lasttime.substr(0, 2) + ":" + lasttime.substr(2, 2) + ":" + lasttime.substr(4, 2);

                    $("td", row).eq(0).text("").append(logintime);
                    $("td", row).eq(5).text("").append(formatMoney(data.gdprice));
                    $("td", row).eq(6).text("").append(formatMoney(data.gddiscount));

                    // 放黃金存摺帳號
                    $("td", row).eq(3).text("").append(data.gdsvac);
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            $("#main").show();
            return false;
        }

        function onQuery() {
			if(($("#dateFrom").val().length!=19)||($("#dateTo").val().length!=19)){
				alert("時間格式有誤，請確認格式為'yyyy/MM/dd HH:mm:ss'");
				return;
			}
            var isError = "";
            if (($("#UserId").val() == "") || ($("#UserId").val() == null))
                isError += "請輸入身分證字號或統一編號\n";
            if (($("#dateFrom").val() == "") || ($("#dateFrom").val() == null))
                isError += "請輸入起始日期\n";
            if (($("#dateTo").val() == "") || ($("#dateTo").val() == null))
                isError += "請輸入結束日期\n";
            
          	//判斷長度如果不為19 也就是yyyy/MM/dd HH:mm:ss的格式
            if (isError != "") {
                alert(isError)
                return;
            }
            $("#B206").submit();
        }
    </script>
</body>

</html>