<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>國別檔管理</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADCTRYCODE">國別代碼：</label>
                    <div class="col-4">
                        <input type="text" id="ADCTRYCODE" name="ADCTRYCODE" class="form-control" maxlength="3" />
                    </div>
                    <label class="col-2 control-label text-right" for="ADCTRYNAME">國別繁中名稱：</label>
                    <div class="col-4">
                        <input type="text" id="ADCTRYNAME" name="ADCTRYNAME" class="form-control" maxlength="50"  />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADCTRYCHSNAME">國別簡中名稱：</label>
                    <div class="col-4">
                        <input type="text" id="ADCTRYCHSNAME" name="ADCTRYCHSNAME" class="form-control" maxlength="50"  />
                    </div>
                    <label class="col-2 control-label text-right" for="ADCTRYENGNAME">國別英文名稱：</label>
                    <div class="col-4">
                        <input type="text" id="ADCTRYENGNAME" name="ADCTRYENGNAME" class="form-control" maxlength="60"  />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnQuery" type="button" value="以上述值做查詢" class="btn btn-dark" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <c:if test="${allowEdit}">
                            <input id="btnCreate" type="button" value="以上述值做新增" class="btn btn-info" aria-label="Left Align" onclick="onCreate()" /> 
                        </c:if>    
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0">
            <thead>
                <tr>
                    <th>國別代碼</th>
                    <th>國別繁中名稱</th>
                    <th>國別簡中名稱</th>
                    <th>國別英文名稱</th>
                    <th>動作</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.all.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.exhide.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                onQuery();
            }); //ready
        
            function onQuery() {
                $("span.error").remove();

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "order": [[0, "asc"]],
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B902/Query",
                        "data": {
                            "ADCTRYCODE": $("#ADCTRYCODE").val(),
                            "ADCTRYNAME": $("#ADCTRYNAME").val(),
                            "ADCTRYCHSNAME": $("#ADCTRYCHSNAME").val(),
                            "ADCTRYENGNAME": $("#ADCTRYENGNAME").val()
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [
                        { "data": "adctrycode" },
                        { "data": "adctryname" },
                        { "data": "adctrychsname" },
                        { "data": "adctryengname" },
                        { "data": "adctrycode" }
                    ],
                    "createdRow": function (row, data, index) {
                        // 處理國家代碼
                        var key = data.adctrycode.trim();

                        var $code = $("<input type='text' class='form-control' maxlength='5' disabled='disabled' />");
                        $code.attr("id", "code_"+key);
                        $code.attr("value", key);
                        $("td", row).eq(0).text("").append($code);

                        // 處理國家繁中
                        var $name = $("<input type='text' class='form-control' disabled='disabled' />");
                        $name.attr("id", "name_"+key);
                        $name.attr("value", data.adctryname);
                        $("td", row).eq(1).text("").append($name);

                        // 處理國家簡中
                        var $chsName = $("<input type='text' class='form-control' disabled='disabled' />");
                        $chsName.attr("id", "chsName_"+key);
                        $chsName.attr("value", data.adctrychsname);
                        $("td", row).eq(2).text("").append($chsName);

                        // 處理國家英文
                        var $egName = $("<input type='text' class='form-control' disabled='disabled' />");
                        $egName.attr("id", "egName_"+key);
                        $egName.attr("value", data.adctryengname);
                        $("td", row).eq(3).text("").append($egName);

                        if ( "${allowEdit}" == "true" ) {
                            // 處理 Button
                            var $edit = $("<input type='button' value='修改' class='btn btn-primary' />");
                            $edit.attr("id", "btnEdit_"+key);
                            $edit.attr("onclick","onEdit('"+key+"')");

                            var $delete = $("<input type='button' value='刪除' class='btn btn-danger' />");
                            $delete.attr("id", "btnDelete_"+key);
                            $delete.attr("onclick","onDelete('"+key+"')");

                            var $save = $("<input type='button' value='儲存' class='btn btn-primary' style='display: none;' />");
                            $save.attr("id", "btnSave_"+key);
                            $save.attr("onclick","onSave('"+key+"')");

                            var $exit = $("<input type='button' value='離開' class='btn btn-dark' style='display: none;' />");
                            $exit.attr("id", "btnBack_"+key);
                            $exit.attr("onclick","onBack('"+key+"')");

                            var $space1 = $("<span>&nbsp;</span>");
                            var $space2 = $("<span>&nbsp;</span>");
                            $("td", row).eq(4).text("").append($edit).append($space1).append($delete).append($save).append($space2).append($exit);
                        } else {
                            $("td", row).eq(4).text("");
                        }
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }

            function checkEmpty(code, name, chsname, engname) {
                var msg = "";
                if ( code=="" ) {
                    msg += "國別代碼不可為空值.\r\n";
                }

                if ( name=="" ) {
                    msg += "國別繁中名稱不可為空值.\r\n";
                }

                if ( chsname=="" ) {
                    msg += "國別簡中名稱不可為空值.\r\n";
                }

                if ( engname=="" ) {
                    msg += "國別英文名稱不可為空值.\r\n";
                }

                return msg;
            }

            function onCreate() {
                $("span.error").remove();

                var code = $("#ADCTRYCODE").val();
                var name = $("#ADCTRYNAME").val();
                var chsName = $("#ADCTRYCHSNAME").val();
                var egName = $("#ADCTRYENGNAME").val();
                var msg = checkEmpty(code, name, chsName, egName);
                if ( msg != "" ) {
                    alert(msg);
                    return;
                }
                if ( !validateAlphaNumericEng(code) ) {
					alert("國別代碼請輸入英文或數字");
					return;
                }
                if ( !validateChinese(name) ) {
					alert("國別繁中名稱請輸入中文");
					return;
                }
                if ( !validateChinese(chsName) ) {
                	alert("國別簡中名稱請輸入中文");
					return;
                }
                if ( !validateAlphaSpace(egName) ) {
                	alert("國別英文名稱請輸入英文");
					return;
                }
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B902/Create",
                    type: "POST",
                    data: {
                        "ADCTRYCODE": code,
                        "ADCTRYNAME": name,
                        "ADCTRYCHSNAME": chsName, 
                        "ADCTRYENGNAME": egName
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("國別代碼["+code+"]新增成功");   
                            onQuery();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onEdit(id) {
                //$("#code_" + id).attr("disabled", false);
                $("#name_" + id).attr("disabled", false);
                $("#chsName_" + id).attr("disabled", false);
                $("#egName_" + id).attr("disabled", false);
                toggleButton(false, id);
            }

            function onDelete(id) {
                var code = $("#code_" + id).val();
                var message = confirm("確定刪除國別代碼[" + code + "]？");
                if (message == true) {
                    lockScreen();
                    $.ajax({
                        url: "<%= request.getContextPath() %>/B902/Delete/"+id,
                        type: "POST",
                        data: {},
                        success: function (data) {
                            unlockScreen();
                            if (data === "0") {
                                alert("國別代碼[" + code + "]刪除成功");
                                onQuery();
                            } else {
                                alert(data);
                            }
                        },
                        error: function (err) {
                            unlockScreen();
                            alert(err.statusText);
                        }
                    });
                }
            }

            function onSave(id) {
                var code = $("#code_" + id).val();
                var name = $("#name_" + id).val();
                var chsName = $("#chsName_" + id).val();
                var egName = $("#egName_" + id).val();
                var msg = checkEmpty(code, name, chsName, egName);
                if ( msg != "" ) {
                    alert(msg);
                    return;
                }
                if ( !validateChinese(name) ) {
					alert("國別繁中名稱請輸入中文");
					return;
                }
                if ( !validateChinese(chsName) ) {
                	alert("國別簡中名稱請輸入中文");
					return;
                }
                if ( !validateAlphaSpace(egName) ) {
                	alert("國別英文名稱請輸入英文");
					return;
                }
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B902/Edit",
                    type: "POST",
                    data: {
                        "ADCTRYCODE":code,
                        "ADCTRYNAME": name,
                        "ADCTRYCHSNAME": chsName, 
                        "ADCTRYENGNAME": egName
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("選單["+name+"]修改成功");  
                            $("#name_" + id).attr("disabled", true);
                            $("#chsName_" + id).attr("disabled", true);
                            $("#egName_" + id).attr("disabled", true);
                            toggleButton(true, code); 
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else {
                                    alert(key+":"+value);
                                }
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onBack(id) {
                var code = $("#code_" + id);
                var name = $("#name_" + id);
                var chsName = $("#chsName_" + id);
                var egName = $("#egName_" + id);

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B902/Get/" + id,
                    type: "POST",
                    cache: false,
                    async: false,
                    success: function (data) {
                        unlockScreen();
                        name.val(data.adctryname);
                        chsName.val(data.adctrychsname);
                        egName.val(data.adctryengname);

                        name.attr("disabled", true);
                        chsName.attr("disabled", true);
                        egName.attr("disabled", true);
                        toggleButton(true, id);
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
            function toggleButton(toQuery, id) {
                if (toQuery) {
                    $("#btnEdit_" + id).show();
                    $("#btnEdit_" + id).next().show();
                    $("#btnDelete_" + id).show();

                    $("#btnSave_" + id).hide();
                    $("#btnSave_" + id).next().hide();
                    $("#btnBack_" + id).hide();
                } else {
                    $("#btnEdit_" + id).hide();
                    $("#btnEdit_" + id).next().hide();
                    $("#btnDelete_" + id).hide();

                    $("#btnSave_" + id).show();
                    $("#btnSave_" + id).next().show();
                    $("#btnBack_" + id).show();
                }
            }
        </script>
    </body>
</html>