<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">
    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>郵件公告編輯</h2>
        <form class="container main-content-block p-4" action="Send" id="mail" name="mail">
            <div class="form-group row">
                <label class="col-3 control-label text-right" >收件人：</label>
                <div class="col-9">
                    <div class="radio">
                        <div class="form-inline">
                            <label><input type="radio" name="SendType" value="NB" checked>&nbsp;網銀所有使用者</label>
                        </div>
                    </div>
                    <br/>
                    <div class="radio">
                        <div class="form-inline">
                            <label><input type="radio" name="SendType" value="File">&nbsp;檔案匯入&nbsp;</label>
                            <input type="file" id="file" name="file" class="form-control" />&nbsp;
                            <input type="button" value="範例下載" class="btn btn-info" onclick="onSample();" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-3 control-label text-right" >公告標題：</label>
                <div class="col-9">
                    <input type="text" id="Title" name="Title" class="form-control" />        
                </div>
            </div>
            <div class="form-group row">
                <label class="col-3 control-label text-right" >公告內容：</label>
                <div class="col-9">
                    <textarea id="Content" name="Content" class="form-control" rows="3"></textarea> 
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-3 col-10">
                    <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                    <input type="button" value="確定" class="btn btn-info" onclick="onSend()" />
                </div>
            </div>
        </form>
    </div>
    <div id="queryResult">
        <label class="col-12 control-label text-center">公告郵件寄送結果</label>
        <table id="resultTbl"" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <th>收件者</th>
                <th>寄送結果</th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    
    <script>
        var resultMap = [];           // 約定轉帳資料

        $(document).ready(function () {
            $("#queryResult").hide();
        }); //ready

        function onSample() {
            window.location = "<%= request.getContextPath() %>/B603/Sample";
        }
        
        function onSend() {
            $("span.error").remove();

            var formData = new FormData();
            var file = $("#file").get(0);
            var sendType = $("#mail input[type='radio']:checked").val();

            if ( sendType == "File" && file.files.length == 0  ) {
                alert("檔案匯入需選擇檔案");
                return;
            } else {
                formData.append("multipartFile", file.files[0]);
            }
            formData.append("sendType", sendType);
            formData.append("title", encodeURIComponent($("#Title").val()));
            formData.append("content", encodeURIComponent($("#Content").val()));
            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/B603/Send",
                type: "POST",
                contentType: false,  // Not to set any content header
                processData: false,  // Not to process data
                data: formData,
                success: function (res) {
                    unlockScreen();
                    resultMap = [];
                    for( var key in res ) {
                        if ( res[key] == "" ) {
                            resultMap.push({"key":key, "value":"成功"});
                        } else {
                            if (key == "fileerror"){
                                resultMap.push({"key":key, "value":res[key]});
                            }else{
                                resultMap.push({"key":key, "value":"失敗"});
                            }
                        }
                    }
                    $("#queryResult").show();
                    onResultDataBind();
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }

        // 顯示基本資料
        function onResultDataBind() {
            $("#resultTbl").dataTable({
                "responsive": true,
                "bPaginate": false,
                "bFilter": false,
                "bDestroy": true,
                "bSort": false,
                "info": false,
                "aaData": resultMap,
                "aoColumns": [
                    { "mDataProp": "key" },
                    { "mDataProp": "value" }
                ],
                "columnDefs": [
                    { targets: 0, className: 'dt-body-center' },
                    { targets: 1, className: 'dt-body-center' }
                ], 
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
        }
    </script>
</body>

</html>