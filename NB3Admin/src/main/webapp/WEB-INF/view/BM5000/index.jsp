<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">

</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>網銀分行維護</h2>
        <form class="container main-content-block p-4" action="/dummy" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="ADBRANCHID">
                	分行代號：
                </label>
                <div class="col-1">
                    <input type="text" id="ADBRANCHID" name="ADBRANCHID" class="form-control" maxlength="3" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="ADBRANCHNAME">
                	分行名稱：
                </label>
                <div class="col-5">
                    <input type="text" id="ADBRANCHNAME" name="ADBRANCHNAME" class="form-control" maxlength="20" />
                </div>
            </div>
           
            <div class="offset-2 col-10">
                <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                <input id="btnQuery" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />
                <c:if test="${allowEdit}">
                    <input id="btnCreate" type="button" value="新增" class="btn btn-primary" aria-label="Left Align" onclick="onCreate()" />
                </c:if>
            </div>
        </form>
    </div>
    <table id="main" class="display p-4 transparent" cellspacing="0" style="width:95%">
        <thead>
            <tr>
                <th>分行代號</th>
                <th>分行名稱</th>
                <th>郵遞區號</th>
                <th>地址</th>
                <th>電話</th>
                <th>行庫及分行碼</th>
                <th>修改人</th>
                <th>修改日</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            onQuery();
        }); //ready

        // 新增
        function onCreate() {
            location.href = "<%= request.getContextPath() %>/BM5000/Create";
        }

        function onQuery() {
            $("span.error").remove();

            $("#main").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "order": [[0, "asc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/BM5000/Query",
                    "data": {
                        "ADBRANCHID": $("#ADBRANCHID").val(),
                        "ADBRANCHNAME": $("#ADBRANCHNAME").val(),
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "adbranchid" },
                    { "data": "adbranchname" },
                    { "data": "postcode" },
                    { "data": "address" },
                    { "data": "telnum" },
                    { "data": "bankcode" },
                    { "data": "lastuser" },
                    { "data": "lastdate" }
                ],
                "createdRow": function (row, data, index) {
                    // 處理分行代碼                
                    var key = data.adbranchid;
                   
                    var $code = $("<a>" + key + "</a>");
                    if ( "${allowEdit}" == "true" ) {
                        $code.attr("href", "<%= request.getContextPath() %>/BM5000/Edit/" + key);
                    } else {
                        $code.attr("href", "<%= request.getContextPath() %>/BM5000/Get/" + key);
                    }
                    $("td", row).eq(0).text("").append($code);
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            $("#main").show();
            return false;
        }
    </script>
</body>

</html>