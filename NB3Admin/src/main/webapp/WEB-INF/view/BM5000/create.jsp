<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">

</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <c:if test="${!IsEdit}">
            <h2>網銀分行維護-新增</h2>
        </c:if>
        <c:if test="${IsEdit}">
            <h2>網銀分行維護-修改</h2>
        </c:if>
        <form id="frmBranch" class="container main-content-block p-4" action="/dummy" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right">
                	分行代號：
                </label>
               	<c:if test="${IsEdit}">
               		${ data.ADBRANCHID }
               		<input type="hidden" id="ADBRANCHID" name="ADBRANCHID" class="form-control" value="${ data.ADBRANCHID }" maxlength="3"/>	 
               	</c:if>
               	<c:if test="${!IsEdit}">
	                <div class="col-1">
	                	<input type="text" id="ADBRANCHID" name="ADBRANCHID" class="form-control" maxlength="3"/>	  
	                </div>
                </c:if>
            </div>
           	<div class="form-group row">
                <label class="col-2 control-label text-right">
                	分行名稱(繁中)：
                </label>
	            <div class="col-6">
	            	<input type="text" id="ADBRANCHNAME" name="ADBRANCHNAME" class="form-control" value="${data.ADBRANCHNAME}" maxlength="30"/>	  
	            </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">
                	分行名稱(英)：
                </label>
	            <div class="col-6">
	            	<input type="text" id="ADBRANENGNAME" name="ADBRANENGNAME" class="form-control" value="${data.ADBRANENGNAME}" maxlength="180"/>	  
	            </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">
                	分行名稱(簡)：
                </label>
	            <div class="col-6">
	            	<input type="text" id="ADBRANCHSNAME" name="ADBRANCHSNAME" class="form-control" value="${data.ADBRANCHSNAME}" maxlength="30"/>	  
	            </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">
                	郵遞區號：
                </label>
	            <div class="col-1">
	            	<input type="text" id="POSTCODE" name="POSTCODE" class="form-control" value="${data.POSTCODE}" maxlength="3"/>	  
	            </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">
                	地址(繁)：
                </label>
	            <div class="col-6">
	            	<input type="text" id="ADDRESS" name="ADDRESS" class="form-control" value="${data.ADDRESS}" maxlength="40"/>	  
	            </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">
                	地址(英)：
                </label>
	            <div class="col-6">
	            	<input type="text" id="ADDRESSENG" name="ADDRESSENG" class="form-control" value="${data.ADDRESSENG}" maxlength="300"/>	  
	            </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">
                	地址(簡)：
                </label>
	            <div class="col-6">
	            	<input type="text" id="ADDRESSCHS" name="ADDRESSCHS" class="form-control" value="${data.ADDRESSCHS}" maxlength="40"/>	  
	            </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">
                	電話：
                </label>
	            <div class="col-4">
	            	<input type="text" id="TELNUM" name="TELNUM" class="form-control" value="${data.TELNUM}" maxlength="17"/>	  
	            </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">
                	行庫及分行碼：
                </label>
	            <div class="col-2">
	            	<input type="text" id="BANKCODE" name="BANKCODE" class="form-control" value="${data.BANKCODE}" maxlength="7"/>	  
	            </div>
            </div>
            <div class="form-group row">
                <div class="offset-3 col-9">
                    <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align"
                        onclick="onExit()" />&nbsp;
                    <c:choose>
                        <c:when test="${IsEdit}">
                            <input id="btnDelete" type="button" value="刪除" class="btn btn-danger"
                                aria-label="Left Align" onclick="onDelete()" />&nbsp;
                            <input id="btnEdit" type="button" value="確定" class="btn btn-info" aria-label="Left Align"
                                onclick="onEdit()" />
                        </c:when>
                        <c:otherwise>
                            <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align"
                                onclick="onSave()" />
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </form>
    </div>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

    <script src="<%= request.getContextPath() %>/script/respond.js"></script>

    <script>
        $(document).ready(function () {
          
        }); //ready

        $.fn.serializeObject = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };

        $.fn.serializeObjectEx = function () {
            var o = {};
            //    var a = this.serializeArray();
            $(this).find('input[type="hidden"], input[type="text"], input[type="password"],input[type="number"], input[type="checkbox"]:checked, input[type="radio"]:checked, select').each(function () {
                if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
                    var $parent = $(this).parent();
                    var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
                    if ($chb != null) {
                        if ($chb.prop('checked')) return;
                    }
                }
                if (this.name === null || this.name === undefined || this.name === '')
                    return;
                var elemValue = null;
                if ($(this).is('select'))
                    elemValue = $(this).find('option:selected').val();
                else elemValue = this.value;
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(elemValue || '');
                } else {
                    o[this.name] = elemValue || '';
                }
            });
            return o;
        }
        // 回查詢頁
        function onExit() {
            location.href="<%= request.getContextPath() %>/BM5000";
        }

        // 新增
        function onSave() {
         	if($("#ADBRANCHID").val() == ""){
         		alert("請輸入分行代號");
         		return false
         	}
         	
         	if($("#ADBRANCHNAME").val() == ""){
         		alert("請輸入分行名稱(繁中)");
         		return false
         	}
         	
         	if($("#POSTCODE").val() == ""){
         		alert("請輸入郵遞區號");
         		return false
         	}
         	
         	if($("#ADDRESS").val() == ""){
         		alert("請輸入地址(繁)");
         		return false
         	}
         	
         	if($("#TELNUM").val() == ""){
         		alert("請輸入電話");
         		return false
         	}
         	
         	if($("#BANKCODE").val() == ""){
         		alert("請輸入行庫及分行碼");
         		return false
         	}
         	
        	var formData = $("#frmBranch").serializeObjectEx();
            $("span.error").remove();
            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/BM5000/Create",
                type: "POST",
                data: formData,
                success: function (res) {
                    unlockScreen();
                    if (res.validated) {
                        alert("網銀分行新增成功");
                        location.href="<%= request.getContextPath() %>/BM5000";
                    } else {
                        //Set error messages
                        $.each(res.errorMessages, function (key, value) {
                            if (key === "summary"){
                                alert(value);
                            }
                        });
                    }
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }

        // 修改
        function onEdit() {
        	if($("#ADBRANCHNAME").val() == ""){
         		alert("請輸入分行名稱(繁中)");
         		return false
         	}
         	
         	if($("#POSTCODE").val() == ""){
         		alert("請輸入郵遞區號");
         		return false
         	}
         	
         	if($("#ADDRESS").val() == ""){
         		alert("請輸入地址(繁)");
         		return false
         	}
         	
         	if($("#TELNUM").val() == ""){
         		alert("請輸入電話");
         		return false
         	}
         	
         	if($("#BANKCODE").val() == ""){
         		alert("請輸入行庫及分行碼");
         		return false
         	}
            var formData = $("#frmBranch").serializeObjectEx();
            $("span.error").remove();
            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/BM5000/Edit",
                type: "POST",
                data: formData,
                success: function (res) {
                    unlockScreen();
                    if (res.validated) {
                        alert("網銀分行修改成功");
                        location.href="<%= request.getContextPath() %>/BM5000";
                    } else {
                        //Set error messages
                        $.each(res.errorMessages, function (key, value) {
                            if (key === "summary"){
                                alert(value);
                            }
                        });
                    }
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }

        // 刪除
        function onDelete() {
            var ADBRANCHID = $("#ADBRANCHID").val();
            if (!confirm("確定刪除網銀分行[" + ADBRANCHID + "]")) {
                return;
            }
            $("span.error").remove();
            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/BM5000/Delete/" + ADBRANCHID,
                type: "POST",
                data: {},
                success: function (res) {
                    unlockScreen();
                    if (res === "0") {
                        alert("網銀分行刪除成功");
                        location.href="<%= request.getContextPath() %>/BM5000";
                    } else {
                        alert(res);
                    }
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }
    </script>
</body>

</html>