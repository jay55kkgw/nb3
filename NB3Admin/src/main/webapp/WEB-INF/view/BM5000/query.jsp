<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
    
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>網銀分行維護-查詢</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >
						分行代號：
                    </label>
                    <label class="col-4 control-label" >
                    	${Data.ADBRANCHID}
                    </label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >
                    	分行名稱(繁中)：
                    </label>
                    <label class="col-8 control-label" >
                    	${Data.ADBRANCHNAME}
                    </label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >
                    	分行名稱(英)：
                    </label>
                    <label class="col-8 control-label" >
                    	${Data.ADBRANENGNAME}
                    </label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >
                    	分行名稱(簡中)：
                    </label>
                    <label class="col-8 control-label" >
                    	${Data.ADBRANCHSNAME}
                	</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >
                    	郵遞區號：
                    </label>
                    <label class="col-8 control-label" >
                    	${Data.POSTCODE}
                	</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >
                    	地址(繁)：
                    </label>
                    <label class="col-8 control-label" >
                    	${Data.ADDRESS}
                	</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >
                    	地址(英)：
                    </label>
                    <label class="col-8 control-label" >
                    	${Data.ADDRESSENG}
                	</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >
                    	地址(簡)：
                    </label>
                    <label class="col-8 control-label" >
                    	${Data.ADDRESSCHS}
                	</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >
                    	電話：
                    </label>
                    <label class="col-8 control-label" >
                    	${Data.TELNUM}
                	</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >
                    	行庫及分行碼：
                    </label>
                    <label class="col-8 control-label" >
                    	${Data.BANKCODE}
                	</label>
                </div>
                
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
            
        <script>
            $(document).ready(function () {
            }); //ready

            // 回查詢頁
            function onExit() {
                location.href="<%= request.getContextPath() %>/BM5000";
            }
        </script>
    </body>
</html>