<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html> 
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>一般網路銀行單日交易量統計表</h2>
            <form class="container main-content-block p-4" id="B703" name="B703" action="<%= request.getContextPath() %>/B703/Query" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >查詢期間：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <select class="form-control" id="YYYY" name="YYYY">
                                <option value="">--請選擇--</option>
                                <c:forEach items="${years}" var="year">
                                    <option value="${year}">${year}</option>
                                </c:forEach>
                            </select>&nbsp;年&nbsp;
                            <select class="form-control" id="MM" name="M">
                                <option value="">--請選擇--</option>
                                <c:forEach items="${months}" var="month">
                                    <option value="${month}">${month}</option>
                                </c:forEach>
                            </select>&nbsp;月
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10"> 
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnexpCSV" type="button" value="匯出CSV" class="btn btn-secondary" aria-label="Left Align" onclick="onReport();" />&nbsp;
                    </div>
                </div>
            </form>
        </div>

        <table id="main" name="main" class="display p-4 transparent" style="width:95%">
            <thead>
                <tr style="text-align: right;">
                    <th>日期</th>  
                    <th>一般網銀</th>
                    <th>行動網銀</th>
                </tr>
            <tbody>
                                                                 
            </tbody>
        </table>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            var reportData = [];           // 報表資料

            $(document).ready(function () {
                $("#YYYY").val("${YYYY}");
                $("#MM").val("${MM}");
                onQuery();
            });

            function onQuery() {
                $("#main").hide();
                var url = "<%= request.getContextPath() %>/B707/Query";
                
                lockScreen();
                $.post(url, {
                    "YYYYMM" : $("#YYYY").val()+$("#MM").val(),
                }, function (data, status) {
                    unlockScreen();
                    if (status == "success") {
                        var errorMsg = data.errorMsg;
                        
                        if (  errorMsg == "" ) {
                            reportData = data.reportData;

                            onReportDataBind();

                            $("#main").show();
                        } else {
                            alert(errorMsg);
                        }
                    }
                });
            }

            function onReportDataBind() {
                $("#main").dataTable({
                    "responsive": true,
                    "bPaginate": false,
                    "bFilter": false,
                    "bDestroy": true,
                    "bSort": false,
                    "info": false,
                    "aaData": reportData,
                    "aoColumns": [
                        { "mDataProp": "lastDate" },
                        { "mDataProp": "nbCount" },
                        { "mDataProp": "mbCount" }
                    ],
                    "columnDefs": [
                        { targets: 0, className: 'dt-body-right',width: 200 },
                        { targets: 1, className: 'dt-body-right',width: 200 },
                        { targets: 2, className: 'dt-body-right',width: 200 }
                    ], 
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
            }

            function onReport() {
                var YYYYMM = $("#YYYY").val()+$("#MM").val();

                window.location = "<%= request.getContextPath() %>/B707/Report?YYYYMM="+YYYYMM;
            }
        </script>
    </body>
</html>