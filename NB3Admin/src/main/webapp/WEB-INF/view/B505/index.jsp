<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">     
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>推廣訊息維護</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >行銷活動代號：</label>
                    <div class="col-10">
                    	<input type="text" id="ACT_TYPE" name="ACT_TYPE" autocomplete="off" maxlength="8">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >行銷次數最大次數：</label>
                    <div class="col-10">
                    	<input type="text" id="MAXCOUNT" name="MAXCOUNT" autocomplete="off" maxlength="3">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >上架起～迄日：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <input type="text" id="ACTDATE_FROM" autocomplete="off" name="ACTDATE_FROM" class="form-control" style = "width:160px" maxlength="8" value="${StartDt}" />～
                            <input type="text" id="ACTDATE_TO" autocomplete="off" name="ACTDATE_TO" class="form-control" style = "width:160px" maxlength="8" value="${EndDt}" />
                        </div>
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnClear" type="reset" value="重新輸入" class="btn btn-dark" aria-label="Left Align" />&nbsp;
                        <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnCreate" type="button" value="新增" class="btn btn-info" aria-label="Left Align" onclick="onCreate()" />
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th>行銷活動代號</th>
                    <th>行銷次數最大次數</th>
                    <th>行銷起日</th>
                    <th>行銷迄日</th>
                    <th>行銷訊息</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <span>如案件已結案，部門代碼／姓名請按已結案連結做查詢</span>

        <jsp:include page="../include/caseStatus.jsp"></jsp:include>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
        <script>

            $(document).ready(function () {
                $("#main").hide();
                $('#ACTDATE_FROM').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false,
                    timepicker: false
                });
                $('#ACTDATE_TO').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false,
                    timepicker: false
                });
                onQuery();
            }); //ready
        
            // 新增
            function onCreate() {
                location.href = "<%= request.getContextPath() %>/B505/Create";
            }
            
            function onQuery() {

            	var sisvalid = Date.parse($("#ACTDATE_FROM").val());
                var eisvalid = Date.parse($("#ACTDATE_TO").val());
                
                if(isNaN(sisvalid) || isNaN(eisvalid) ) { 
                    alert("請輸入合法的起日和迄日");
                    return;
                }

                if ( sisvalid > eisvalid ) {
                    alert("迄日必需大於起日");
                    return;
                }
                
                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "order": [[2, "asc"]],
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B505/IndexQuery",
                        "data": {
                        	"ACT_TYPE" : $("#ACT_TYPE").val(),
                            "MAXCOUNT" : $("#MAXCOUNT").val(),
                            "ACTDATE_FROM" : $("#ACTDATE_FROM").val(),
                            "ACTDATE_TO" : $("#ACTDATE_TO").val(),
                        },
                        "error": function (msg) {
                            alert("查詢失敗");
                            location.href="<%= request.getContextPath() %>/B505/Index";
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [ 
                    	 { "data": "act_TYPE" },
                         { "data": "maxcount" },
                         { "data": "actdate_FROM" },
                         { "data": "actdate_TO" },
                         { "data": "act_MSG" },
                    ],
                    "createdRow": function (row, data, index) {
                        // ID
                        var $id = $("<a>"+data.act_TYPE+"<a/>");
                        $id.attr("href", "<%= request.getContextPath() %>/B505/Edit/"+data.act_TYPE);
                        $("td", row).eq(0).text("").append($id);
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });                
                $("#main").show();
                return false;
            }
            
        </script>
    </body>
</html>