<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
    
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>推廣訊息維護-新增</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >行銷活動代號：</label>
                    <div class="col-3">
                        <input type="text" id="ACT_TYPE" name="ACT_TYPE" maxlength="8" autocomplete="off">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >行銷次數最大次數：</label>
                    <div class="col-3">
                        <input type="text" id="MAXCOUNT" name="MAXCOUNT" maxlength="3" autocomplete="off">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >行銷起日：</label>
                    <div class="col-3">
                        <div class="form-inline">
                            <input type="text" id="ACTDATE_FROM" autocomplete="off" name="ACTDATE_FROM" class="form-control" style = "width:160px" maxlength="10" />
                        </div>
                    </div>
                    <label class="col-3 control-label text-right" >行銷迄日：</label>
                    <div class="col-3">
                        <div class="form-inline">
                            <input type="text" id="ACTDATE_TO" autocomplete="off" name="ACTDATE_TO"  class="form-control" style = "width:160px" maxlength="10" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >行銷訊息：</label>
                    <div class="col-9">
                        <input type="text" id="ACT_MSG" name="ACT_MSG"  style = "width:260px"  maxlength="120" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onSave()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        
        <script>
            $(document).ready(function () {
                $('#ACTDATE_FROM').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });
                $('#ACTDATE_TO').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });
                
            }); //ready

            // 回查詢頁
            function onExit() {
                history.back();
            }

            // 新增
            function onSave() {
            	var reg =/\d{4}\/\d{2}\/\d{2}/;
                var sisvalid = Date.parse($("#ACTDATE_FROM").val());
                var eisvalid = Date.parse($("#ACTDATE_TO").val());
                
                if(isNaN(sisvalid) || isNaN(eisvalid) ) { 
                    alert("請輸入合法的上架日期和下架日期");
                    return false;
                }

                if ( sisvalid > eisvalid ) {
                    alert("下架日期必需大於上架日期");
                    return false;
                }

                if(!(reg.test($("#ACTDATE_FROM").val()) && reg.test($("#ACTDATE_TO").val()))){
              	  alert("請輸入合法的上架日期和下架日期");
                    return false;
                }  

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B505/Create",
                    type: "POST",
                    data: {
                        "ACT_TYPE": $("#ACT_TYPE").val(),
                        "MAXCOUNT": $("#MAXCOUNT").val(),
                        "ACTDATE_FROM": $("#ACTDATE_FROM").val(),
                        "ACTDATE_TO": $("#ACTDATE_TO").val(),
                        "ACT_MSG": $("#ACT_MSG").val(),
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("推廣訊息新增成功");   
                            location.href="<%= request.getContextPath() %>/B505/Index";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
        </script>
    </body>
</html>