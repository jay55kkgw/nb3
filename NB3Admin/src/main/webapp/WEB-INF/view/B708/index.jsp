<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        <!-- 20191119-Danny-Frameable Login Page\路徑 1: -->
        <meta http-equiv="X-Frame-Options" content="SAMEORIGIN" />
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>一般網路銀行峰值查詢統計表</h2>
            <form id="ADMLOGINOUT" class="container main-content-block p-4" action="<%= request.getContextPath() %>/B701/Query" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >查詢類別：</label>
                    <div class="col-10">
                        <div class="radio">
                            <div class="form-inline">
                                <label><input type="radio" name="optradio" id="optradio"  value="Stat" checked>&nbsp;查詢峰月/日/時（一年內）</label>&nbsp;                  
                            </div>
                        </div>
                        <br/>
                        <div class="radio">
                            <div class="form-inline">
                                <label><input type="radio" name="optradio" id="optradio"  value="Date" >&nbsp;查詢日期（一年內）：</label>&nbsp;
                                <input type="text" id="queryDt" name="queryDt" class="form-control" style = "width:160px" maxlength="10" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnexpCSV" type="button" value="匯出CSV" class="btn btn-secondary" aria-label="Left Align" onclick="onReport();" />
                    </div>
                </div>
            </form>
        </div>
        <div id="top3">
	        <table id="top3DateTbl" class="display p-4 transparent" cellspacing="0" style="width:100%">
	            <thead>
	                <tr style="text-align: right;">
	                    <th width="40%">峰日</th>
	                    <th width="30%">查詢筆數</th>
	                    <th width="30%">非查詢筆數</th>
	                </tr>
	            </thead>
	            <tbody>
	            </tbody>
	        </table>
	        <table id="top3MonthTbl" class="display p-4 transparent" cellspacing="0" style="width:100%">
	            <thead>
	                <tr style="text-align: right;">
	                    <th width="40%">峰月</th>
	                    <th width="30%">查詢筆數</th>
	                    <th width="30%">非查詢筆數</th>
	                </tr>
	            </thead>
	            <tbody>
	            </tbody>
	        </table>
	        <table id="top3HHTbl" class="display p-4 transparent" cellspacing="0" style="width:100%">
		            <thead>
		                <tr style="text-align: right;">
		                    <th width="40%">峰時</th>
		                    <th width="30%">查詢筆數</th>
		                    <th width="30%">非查詢筆數</th>
		                </tr>
		            </thead>
		            <tbody>
		            </tbody>
		        </table>
        </div>
        <table id="hourlyDataTbl" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr style="text-align: right;">
                    <th>交易日期</th>
                    <th colspan="2" id="tblDate"></th>
                </tr>
                <tr style="text-align: right;">
                    <th width="40%">時</th>
                    <th width="30%">查詢筆數</th>
                    <th width="30%">非查詢筆數</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            var peakDate = [];          // 前三大峰值資料
			var peakMonth = [];			//
			var peakHH = [];			//
			var peakDayHH = [];			// 每小時統計資料
            $(document).ready(function () {
                $('#queryDt').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: false,
                    format: 'Y/m/d',   
                    scrollMonth: false
                });
                
                $("#top3").hide();
                $("#hourlyDataTbl").hide();
            }); //ready

            function onTop3DataBind() {
                $("#top3DateTbl").dataTable({
                    "responsive": true,
                    "bPaginate": false,
                    "bFilter": false,
                    "bDestroy": true,
                    "bSort": false,
                    "info": false,
                    "aaData": peakDate,
                    "aoColumns": [
                        { "mDataProp": "item" },
                        { "mDataProp": "queryCnt" },
                        { "mDataProp": "nonQueryCnt" }
                    ],
                    "columnDefs": [
                        { targets: 0, className: 'dt-body-right' },
                        { targets: 1, className: 'dt-body-right' },
                        { targets: 2, className: 'dt-body-right' }
                    ], 
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#top3MonthTbl").dataTable({
                    "responsive": true,
                    "bPaginate": false,
                    "bFilter": false,
                    "bDestroy": true,
                    "bSort": false,
                    "info": false,
                    "aaData": peakMonth,
                    "aoColumns": [
                        { "mDataProp": "item" },
                        { "mDataProp": "queryCnt" },
                        { "mDataProp": "nonQueryCnt" }
                    ],
                    "columnDefs": [
                        { targets: 0, className: 'dt-body-right' },
                        { targets: 1, className: 'dt-body-right' },
                        { targets: 2, className: 'dt-body-right' }
                    ],
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#top3HHTbl").dataTable({
                    "responsive": true,
                    "bPaginate": false,
                    "bFilter": false,
                    "bDestroy": true,
                    "bSort": false,
                    "info": false,
                    "aaData": peakHH,
                    "aoColumns": [
                        { "mDataProp": "item" },
                        { "mDataProp": "queryCnt" },
                        { "mDataProp": "nonQueryCnt" }
                    ],
                    "columnDefs": [
                        { targets: 0, className: 'dt-body-right' },
                        { targets: 1, className: 'dt-body-right' },
                        { targets: 2, className: 'dt-body-right' }
                    ],
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
            }

            function onHourlyDataBind() {
                $("#hourlyDataTbl").dataTable({
                    "responsive": true,
                    "bPaginate": false,
                    "bFilter": false,
                    "bDestroy": true,
                    "bSort": false,
                    "info": false,
                    "aaData": peakDayHH,
                    "aoColumns": [
                        { "mDataProp": "hour" },
                        { "mDataProp": "queryCnt" },
                        { "mDataProp": "nonQueryCnt" }
                    ],
                    "columnDefs": [
                        { targets: 0, className: 'dt-body-right' },
                        { targets: 1, className: 'dt-body-right' },
                        { targets: 2, className: 'dt-body-right' }
                    ], 
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
            }

            function onQuery() {
                var type = $("input[name=optradio]:checked").val();
                var queryDt = "";
                if ( type == "Date") {
                    queryDt = $("#queryDt").val();
                    if ( queryDt == "" ) {
                        alert("請輸入查詢日期");
                        return;
                    }
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B708/Query",
                    type: "POST",
                    data: {
                        "Type": type,
                        "QueryDt": queryDt
                    },
                    success: function (data) {
                        $("#top3").hide();
                        $("#hourlyDataTbl").hide();
                        unlockScreen();
                        tableData = data;
                        if ( type == "Date" ) {
                        	peakDayHH = data[0];
                            onHourlyDataBind();
                            $("#tblDate").text(queryDt);
                            $("#top3").hide();
                            $("#hourlyDataTbl").show();
                        } else {
                            peakDate = data[0];
                            for ( var i=0; i<peakDate.length; i++ ) {
                                var str = peakDate[i].item+'';
                                peakDate[i].item = str.substring(0,4)+"/"+str.substring(4,6)+"/"+str.substring(6,8);
                            }
                            peakMonth = data[1];
                            for ( var i=0; i<peakMonth.length; i++ ) {
                                var str = peakMonth[i].item+'';
                                peakMonth[i].item = str.substring(0,4)+"/"+str.substring(4,6);
                            }
                            peakHH = data[2];
                            for ( var i=0; i<peakHH.length; i++ ) {
                                var str = peakHH[i].item+'';
                                peakHH[i].item = str.substring(0,4)+"/"+str.substring(4,6)+"/"+str.substring(6,8)+":"+str.substring(8,10);
                            }
                            onTop3DataBind();
                            $("#top3").show();
                            $("#hourlyDataTbl").hide();
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onReport() {
                var type = $("input[name=optradio]:checked").val();
                var queryDt = "";
                if ( type == "Date") {
                    queryDt = $("#queryDt").val();
                    if ( queryDt == "" ) {
                        alert("請輸入查詢日期");
                        return;
                    }
                }

                window.location = "<%= request.getContextPath() %>/B708/Report?Type="+type+"&QueryDt="+queryDt;
            }
        </script>
    </body>
</html>