<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>



<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css"
            href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css"
            href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">

</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>各業務營業時間維護</h2>
        <form class="container main-content-block p-4" action="/dummy" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="ADFXSHH">外匯業務 起:</label>         
                <div class="col-10">
                    <div class="form-inline">
                        <input type="number" id="ADFXSHH" name="ADFXSHH" value="${Data.ADFXSHH}" class="form-control" 
                        maxlength="2" />&nbsp;HH&nbsp;
                        <input type="number" id="ADFXSMM" name="ADFXSMM" value="${Data.ADFXSMM}" class="form-control" 
                        maxlength="2"  />&nbsp;MM&nbsp;
                        <input type="number" id="ADFXSSS" name="ADFXSSS" value="${Data.ADFXSSS}" class="form-control" 
                        maxlength="2"  />&nbsp;SS&nbsp;
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="ADFXSHH">迄:</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="number" id="ADFXEHH" name="ADFXEHH" value="${Data.ADFXEHH}" class="form-control"
                            maxlength="2" />&nbsp;HH&nbsp;
                        <input type="number" id="ADFXEMM" name="ADFXEMM" value="${Data.ADFXEMM}" class="form-control"
                            maxlength="2" />&nbsp;MM&nbsp;
                        <input type="number" id="ADFXESS" name="ADFXESS" value="${Data.ADFXESS}" class="form-control"
                            maxlength="2" />&nbsp;SS&nbsp;
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="ADFDSHH"> 基金業務 起:</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="number" id="ADFDSHH" name="ADFDSHH" value="${Data.ADFDSHH}" class="form-control" 
                        maxlength="2" />&nbsp;HH&nbsp;
                        <input type="number" id="ADFDSMM" name="ADFDSMM" value="${Data.ADFDSMM}" class="form-control" 
                        maxlength="2" />&nbsp;MM&nbsp;
                        <input type="number" id="ADFDSSS" name="ADFDSSS" value="${Data.ADFDSSS}" class="form-control" 
                        maxlength="2" />&nbsp;SS&nbsp;
                    </div>
                </div>                
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="ADFDEHH"> 迄:</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="number" id="ADFDEHH" name="ADFDEHH" value="${Data.ADFDEHH}" class="form-control" 
                        maxlength="2" />&nbsp;HH&nbsp;
                        <input type="number" id="ADFDEMM" name="ADFDEMM" value="${Data.ADFDEMM}" class="form-control" 
                        maxlength="2" />&nbsp;MM&nbsp;
                        <input type="number" id="ADFDESS" name="ADFDESS" value="${Data.ADFDESS}" class="form-control" 
                        maxlength="2" />&nbsp;SS&nbsp;
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="ADGDSHH"> 黃金業務 起:</label>
            <div class="col-10">
                <div class="form-inline">
                    <input type="number" id="ADGDSHH" name="ADGDSHH" value="${Data.ADGDSHH}" class="form-control" 
                    maxlength="2" />&nbsp;HH&nbsp;
                    <input type="number" id="ADGDSMM" name="ADGDSMM" value="${Data.ADGDSMM}" class="form-control" 
                    maxlength="2" />&nbsp;MM&nbsp;
                    <input type="number" id="ADGDSSS" name="ADGDSSS" value="${Data.ADGDSSS}" class="form-control" 
                    maxlength="2" />&nbsp;SS&nbsp;
                </div>
            </div>                
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="ADGDEHH"> 迄:</label>
            <div class="col-10">
                <div class="form-inline">
                    <input type="number" id="ADGDEHH" name="ADGDEHH" value="${Data.ADGDEHH}" class="form-control" 
                    maxlength="2" />&nbsp;HH&nbsp;
                    <input type="number" id="ADGDEMM" name="ADGDEMM" value="${Data.ADGDEMM}" class="form-control" 
                    maxlength="2" />&nbsp;MM&nbsp;
                    <input type="number" id="ADGDESS" name="ADGDESS" value="${Data.ADGDESS}" class="form-control" 
                    maxlength="2" />&nbsp;SS&nbsp;
            </div>
        </div></div>
            <div id="editArea" class="offset-2 col-10">
                <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;     
                <input id="btnCreate" type="button" value="儲存" class="btn btn-info" aria-label="Left Align" onclick="onSave()" />
            </div>
        </form>
    </div>

    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.all.js"></script>
    <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.exhide.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            if ( "${allowEdit}" == "false" ) {
                $("#editArea").hide();
                $("form input").prop("disabled", true);
            }
        }); //ready

        function checkHHMMSS(hh, mm, ss, msg) {
            if ( hh=="" || mm=="" || ss=="" ) {
                return msg+" 時/分/秒不得為空值";
            }
            try {
                var h=parseInt(hh);
                var m=parseInt(mm);
                var s=parseInt(ss);

                if ( h<0 || h>59 || m<0 || m>59 || s<0 || s>59 ) {
                    return msg+= " 時必須介於 0~23, 分必須介於 0~59, 秒必須介於 0~59";
                }
            } catch (error) {
                return msg+= " 時必須介於 0~23, 分必須介於 0~59, 秒必須介於 0~59";
            }
            return "";
        }

        function onSave() {
            var ADFXSHH = $("#ADFXSHH").val();
            var ADFXSMM = $("#ADFXSMM").val();
            var ADFXSSS = $("#ADFXSSS").val();
            var ADFXEHH = $("#ADFXEHH").val();
            var ADFXEMM = $("#ADFXEMM").val();
            var ADFXESS = $("#ADFXESS").val();
            var ADFDSHH = $("#ADFDSHH").val();
            var ADFDSMM = $("#ADFDSMM").val();
            var ADFDSSS = $("#ADFDSSS").val();
            var ADFDEHH = $("#ADFDEHH").val();
            var ADFDEMM = $("#ADFDEMM").val();
            var ADFDESS = $("#ADFDESS").val();
            var ADGDSHH = $("#ADGDSHH").val();
            var ADGDSMM = $("#ADGDSMM").val();
            var ADGDSSS = $("#ADGDSSS").val();
            var ADGDEHH = $("#ADGDEHH").val();
            var ADGDEMM = $("#ADGDEMM").val();
            var ADGDESS = $("#ADGDESS").val();
            var errMsg1 = checkHHMMSS(ADFXSHH, ADFXSMM, ADFXSSS, "外匯業務-起,");
            var errMsg2 = checkHHMMSS(ADFXEHH, ADFXEMM, ADFXESS, "外匯業務-迄,");
            var errMsg3 = checkHHMMSS(ADFDSHH, ADFDSMM, ADFDSSS, "基金業務-起,");
            var errMsg4 = checkHHMMSS(ADFDEHH, ADFDEMM, ADFDESS, "基金業務-迄,");
            var errMsg5 = checkHHMMSS(ADGDSHH, ADGDSMM, ADGDSSS, "黃金業務-起,");
            var errMsg6 = checkHHMMSS(ADGDEHH, ADGDEMM, ADGDESS, "黃金業務-迄,");

            var errMsg = "";
            if ( errMsg1 != "" ) {
                errMsg = errMsg1+"\r\n";
            }
            if ( errMsg2 != "" ) {
                errMsg += errMsg2+"\r\n";
            }
            if ( errMsg3 != "" ) {
                errMsg += errMsg3+"\r\n";
            }
            if ( errMsg4 != "" ) {
                errMsg += errMsg4+"\r\n";
            }
            if ( errMsg5 != ""){
                errMsg += errMsg5+"\r\n";
            }
            if ( errMsg6 != ""){
                errMsg += errMsg6+"\r\n";
            }
            if ( errMsg != "" ) {
                alert(errMsg);
                return;
            }

            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/B105/Edit",
                type: "POST",
                data: {
                    "ADPK": "NBSYS",
                    "ADFXSHH": ADFXSHH,
                    "ADFXSMM": ADFXSMM,
                    "ADFXSSS": ADFXSSS,
                    "ADFXEHH": ADFXEHH,
                    "ADFXEMM": ADFXEMM,
                    "ADFXESS": ADFXESS,
                    "ADFDSHH": ADFDSHH,
                    "ADFDSMM": ADFDSMM,
                    "ADFDSSS": ADFDSSS,
                    "ADFDEHH": ADFDEHH,
                    "ADFDEMM": ADFDEMM,
                    "ADFDESS": ADFDESS,
                    "ADGDSHH": ADGDSHH,
                    "ADGDSMM": ADGDSMM,
                    "ADGDSSS": ADGDSSS,
                    "ADGDEHH": ADGDEHH,
                    "ADGDEMM": ADGDEMM,
                    "ADGDESS": ADGDESS
                },
                success: function (res) {
                    unlockScreen();
                    if (res.validated) {
                        alert("各業務營業時間維護修改成功");
                        onData();
                    } else {
                        //Set error messages
                        $.each(res.errorMessages, function (key, value) {
                            if (key === "summary")
                                alert(value);
                            else {
                                alert(value);
                            }
                        });
                    }
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }        
    </script>
</body>

</html>