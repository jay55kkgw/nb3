<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>



<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">
    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>行動銀行系統啟動/關閉</h2>
        <form id="formData" class="container main-content-block p-4" action="<%= request.getContextPath() %>/BM6000/Result" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="LASTUSER">最後異動者：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="LASTUSER" name="LASTUSER" class="form-control" value="${Data.LASTUSER}" maxlength="10"  disabled  />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="LASTDATE">最後異動日期：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="LASTDATE" name="LASTDATE" class="form-control" value="${Data.LASTDATE}" maxlength="10" disabled  />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="ADMBSTATUS">行動銀行系統目前狀態：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="ADMBSTATUS" name="ADMBSTATUS" class="form-control" value="${Data.ADMBSTATUS}"  maxlength="10" disabled  />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">設定行動銀行系統狀態：</label>
                <div class="col-10">
                    <div class="radio">
                        <div class="form-inline">
                            <label><input type="radio" id="started" name="optradio" value="Y" >&nbsp;</label>&nbsp;啟動&nbsp;
                            <label><input type="radio" id="closed" name="optradio" value="N" >&nbsp;</label>&nbsp;關閉&nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">說明：</label>
                <label class="col-10 control-label">1.當行動銀行系統狀態設為關閉時客戶無法登入行動銀行，但行員仍能登入管理系統。</label>
            </div>
            <div class="form-group row">
                <div class="offset-2 col-10" >
                    <c:if test="${allowEdit}">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp; 
                        <input id="btnCreate" type="submit" value="儲存" class="btn btn-info colorBtn" aria-label="Left Align"  />
                    </c:if>
                    <input type="hidden" id="ADMBSTATUSID" name="ADMBSTATUSID" class="form-control" value="${Data.ADMBSTATUSID}" maxlength="10"/>
                </div>
            </div>
        </form>
    </div>

    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script>
        $(document).ready(function () {
            var mbStatus = "";
            if($("#ADMBSTATUS").val()=="Y" || $("#ADMBSTATUS").val() == "y"){
                mbStatus = "啟動";  
                $("#started").attr('checked', false);       
                $("#closed").attr('checked', true);      
            }     
            else{
                mbStatus = "關閉";
                $("#started").attr('checked', true);  
                $("#closed").attr('checked', false);    
            }
            //判斷狀態為啟動或關閉時，只可選擇關閉或啟動  
            $("#ADMBSTATUS").val(mbStatus) ; 
        }); //ready
    </script>
</body>
</html>