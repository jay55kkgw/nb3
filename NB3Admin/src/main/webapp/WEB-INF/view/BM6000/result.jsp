<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>應用系統啟動/關閉</h2>
        <form class="container main-content-block p-4" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right">資料時間：</label>
                <label class="col-10 control-label">${TimeStamp}</label>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">應用系統目前狀態：</label>
                <label class="col-10 control-label">${Status}</label>
            </div>
            <div class="form-group row">
                <div class="offset-2 col-10">
                    <input type="button" class="btn btn-info" value="返回" onclick="onReturn();" />
                </div>
            </div>
        </form>  
    </div>         

    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script>
        $(document).ready(function () {
        })

        function onReturn() {
            location.href="<%= request.getContextPath() %>/BM6000/Index";
        }
    </script>
</body>
</html>