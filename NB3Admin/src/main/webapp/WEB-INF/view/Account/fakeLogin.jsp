<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="zh">

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>模擬登入</h2>
        <form class="container main-content-block p-4" id="formData"
            action="<%= request.getContextPath() %>/Account/FakeLogin" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="OrgId">部門：</label>
                <div class="col-4">
                    <select id="OrgId" name="OrgId" class="form-control">
                        <c:forEach var="bh" items="${loginbranch}">
                            <option value="${bh.key}">${bh.key}-${bh.value}</option>
                        </c:forEach>  
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="UserId">登入人員：</label>
                <div class="col-4">
                    <select id="UserId" name="UserId" class="form-control">
                        <c:forEach var="user" items="${loginuser}">
                            <option value="${user.key}">${user.key}-${user.value}</option>
                        </c:forEach>  
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="RoleId">角色：</label>
                <div class="col-4">
                    <select id="RoleId" name="RoleId" class="form-control">
                        <c:forEach var="role" items="${loginrole}">
                            <option value="${role.key}">${role.value}</option>
                        </c:forEach> 
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-2 col-10">
                    <input id="btnCreate" type="submit" value="確定" class="btn btn-info" aria-label="Left Align" />
                </div>
            </div>
        </form>
    </div>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

    <script>
        $(document).ready(function () {
            var msg = "${error}";

            if (msg !== "") {
                alert(msg);
            }
        }); //ready
    </script>
</body>

</html>