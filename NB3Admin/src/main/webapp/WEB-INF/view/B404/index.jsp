<% response.setHeader("X-Frame-Options", "DENY"); %>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>

<head>
	<title>台企銀 後台首頁</title>
	<meta charset="UTF-8">
	
	<!-- include 後台管理 css -->
	<jsp:include page="../include/admheadercs.jsp"></jsp:include>
	
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
</head>

<body>
	<!-- include master page menu -->
	<jsp:include page="../include/admmenu.jsp"></jsp:include>

	<div class="container" id="main-content">
		<h2>批次執行結果查詢</h2>
	</div>
	<!-- body content start -->
	<table id="main" class="display p-4 transparent" cellspacing="0" style="width: 100%">
		<thead>
			<tr>
				<th></th>
				<th>批次作業ID</th>
				<th>批次名稱</th>
				<th>執行週期說明</th>
				<th>批次執行日期/時間</th>
				<th>批次結束日期/時間</th>
				<th>作業耗時(ms)</th>
				<th>訊息</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<!-- body content end -->

	<!-- include master page footer -->
	<jsp:include page="../include/admfooter.jsp"></jsp:include>

	<!-- include 後台管理 js -->
	<jsp:include page="../include/admfooterjs.jsp"></jsp:include>
	<script src="<%= request.getContextPath() %>/script/respond.js"></script>
	<script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
	
	<script>
        $(document).ready(function () {
            onQuery();
        }); //ready
        
        function onQuery() {
            $("#main").dataTable({
            	"responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 100,
                "order": [[0, "desc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/B404/Query",
                    "data": {},
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                	{ "data": "adbatid" },
                    { "data": "adbatid" },
                    { "data": "adbatname" },
                    { "data": "peroid" },	
                    { "data": "adbatid" },
                    { "data": "adbatid" },
                    { "data": "executiontime" },
                    { "data": "adbatid" }
                ],
                "createdRow": function (row, data, index) {
					var $adbatsdate = data.adbatsdate;
                    var $adbatsdate = $adbatsdate.substr(0, 4) + "/" + $adbatsdate.substr(4, 2) + "/" + $adbatsdate.substr(6, 2) +" ";
                    
                    var $adbatstime = data.adbatstime;
                    var $adbatstime = $adbatstime.substr(0, 2) + ":" + $adbatstime.substr(2, 2) + ":" + $adbatstime.substr(4, 2) +" ";
                    
                    $("td", row).eq(4).text("").append($adbatsdate).append($("<br/>")).append($adbatstime);

                    var $adbatedate = data.adbatedate;
                    var $adbatedate = $adbatedate.substr(0, 4) + "/" + $adbatedate.substr(4, 2) + "/" + $adbatedate.substr(6, 2) +" ";
                    
                    var $adbatetime = data.adbatetime;
                    var $adbatetime = $adbatetime.substr(0, 2) + ":" + $adbatetime.substr(2, 2) + ":" + $adbatetime.substr(4, 2) +" ";
                    
                    $("td", row).eq(5).text("").append($adbatedate).append($("<br/>")).append($adbatetime);
                    

                    var $statusWord = "";
                    if ( data.execmessage === "" ) {
                    	$("td", row).eq(0).text("");
                    	$statusWord = "成功";
                    } else {
                    	var $error = $("<span style='color: red'>*</span>");
                    	$("td", row).eq(0).text("").append($error);
                    	$statusWord = data.execmessage;
                    }
                    $("td", row).eq(7).text("").append($statusWord);
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            return false;
        }
    </script>
</body>

</html>