<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台-假日檔管理</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>假日檔管理</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="">假日資料檔匯入：</label>
                    <div class="col-9">
                        <div class="form-inline">
                            <c:if test="${allowEdit}">
                                <input type="file" id="file" name="file" class="form-control" />&nbsp;
                                <input type="button" value="執行匯入" class="btn btn-info" onclick="onUpLoad();" />
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADHOLIDAYID">日期：</label>
                    <div class="col-9">
                        <div class="form-inline">
                            <input type="text" id="ADHOLIDAYID" name="ADHOLIDAYID" class="form-control" maxlength="8" />&nbsp;(格式：YYYYMMDD)
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADREMARK">備註：</label>
                    <div class="col-9">
                        <input type="text" id="ADREMARK" name="ADREMARK" class="form-control" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnQuery" type="button" value="以上述值做查詢" class="btn btn-dark" aria-label="Left Align" onclick="onQuery();" />&nbsp;
                        <c:if test="${allowEdit}">
                            <input id="btnCreate" type="button" value="以上述值做新增" class="btn btn-info" aria-label="Left Align" onclick="onCreate();" />&nbsp;
                        </c:if>
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th style="width:15%">日期</th>
                    <th style="width:15%">備註</th>
                    <th style="width:15%">動作</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                $("#file").on("change", function(e) { onFileSelect(e); });
                onQuery();
            }); //ready
        
            // 若對 file change 有興趣，可以在以下處撰寫內容
            function onFileSelect(e) {
                var files = e.target.files;
                var fileCount = files.length;
                for ( var i=0; i<fileCount; i++ ) {
                    var file = files[i];
                    console.debug("name="+file.name+" ,size="+file.size);
                }
            }

            function onSample() {
                window.location = "<%= request.getContextPath() %>/B904/Sample";
            }

            function onUpLoad() {
                var fileData = new FormData();

                var file1 = $("#file").get(0);
                if (file1.files.length > 0) {
                    fileData.append("multipartFile", file1.files[0]);
                }

                if (file1.files.length == 0 ) {
                    alert("請選擇檔案");
                    return;
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B904/FileUpload",
                    type: "POST",
                    contentType: false,  // Not to set any content header
                    processData: false,  // Not to process data
                    data: fileData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("檔案匯入成功");   
                            onQuery();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onQuery() {
                $("span.error").remove();

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "order": [[0, "asc"]],
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B904/Query",
                        "data": {
                            "ADHOLIDAYID": $("#ADHOLIDAYID").val(),
                            "ADREMARK": $("#ADREMARK").val()
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [
                        { "data": "adholidayid" },
                        { "data": "adremark" },
                        { "data": "adholidayid" }
                    ],
                    "createdRow": function (row, data, index) {
                        // 處理日期
                        var key = data.adholidayid.trim();

                        var $holiday = $("<input type='text' class='form-control' maxlength='8' disabled='disabled' />");
                        $holiday.attr("id", "holiday_" + key);
                        $holiday.attr("value", key);
                        $("td", row).eq(0).text("").append($holiday);

                        // 處理備註
                        var $remark = $("<input type='text' class='form-control' disabled='disabled' />");
                        $remark.attr("id", "remark_" + key);
                        $remark.attr("value", data.adremark);
                        $("td", row).eq(1).text("").append($remark);

                        if ( "${allowEdit}" == "true" ) {
                            // 處理 Button
                            var $edit = $("<input type='button' value='修改' class='btn btn-primary' />");
                            $edit.attr("id", "btnEdit_"+key);
                            $edit.attr("onclick","onEdit('"+key+"')");

                            var $delete = $("<input type='button' value='刪除' class='btn btn-danger' />");
                            $delete.attr("id", "btnDelete_"+key);
                            $delete.attr("onclick","onDelete('"+key+"')");

                            var $save = $("<input type='button' value='儲存' class='btn btn-primary' style='display: none;' />");
                            $save.attr("id", "btnSave_"+key);
                            $save.attr("onclick","onSave('"+key+"')");

                            var $exit = $("<input type='button' value='離開' class='btn btn-dark' style='display: none;' />");
                            $exit.attr("id", "btnBack_"+key);
                            $exit.attr("onclick","onBack('"+key+"')");

                            var $space1 = $("<span>&nbsp;</span>");
                            var $space2 = $("<span>&nbsp;</span>");
                            $("td", row).eq(2).text("").append($edit).append($space1).append($delete).append($save).append($space2).append($exit);
                        } else {
                            $("td", row).eq(2).text("");
                        }
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }

            function onCreate() {
                $("span.error").remove();

                var holiday = $("#ADHOLIDAYID").val();
                var remark = $("#ADREMARK").val();

                if ( holiday==="" ) {
                    alert("日期不可為空值");
                    return;
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B904/Create",
                    type: "POST",
                    data: {
                        "ADHOLIDAYID": holiday,
                        "ADREMARK": remark
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("假日["+holiday+"]新增成功");   
                            onQuery();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onEdit(id) {
                $("#remark_" + id).attr("disabled", false);
                toggleButton(false, id);
            }

            function onDelete(id) {
                var message = confirm("確定刪除假日[" + id + "]？");
                if (message == true) {
                    lockScreen();
                    $.ajax({
                        url: "<%= request.getContextPath() %>/B904/Delete/"+id,
                        type: "POST",
                        data: {},
                        success: function (data) {
                            unlockScreen();
                            if (data === "0") {
                                alert("假日[" + id + "]刪除成功");
                                onQuery();
                            } else {
                                alert(data);
                            }
                        },
                        error: function (err) {
                            unlockScreen();
                            alert(err.statusText);
                        }
                    });
                }
            }

            function onSave(id) {
                var remark = $("#remark_" + id).val();

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B904/Edit",
                    type: "POST",
                    data: {
                        "ADHOLIDAYID": id,
                        "ADREMARK": remark
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("選單[" + id + "]修改成功");  
                            $("#remark_" + id).attr("disabled", true);
                            toggleButton(true, id); 
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else {
                                    alert(key+":"+value);
                                }
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onBack(id) {
                var holiday = $("#holiday_" + id);
                var remark = $("#remark_" + id);

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B904/Get/" + id,
                    type: "POST",
                    cache: false,
                    async: false,
                    success: function (data) {
                        unlockScreen();
                        remark.val(data.adremark);
                        remark.attr("disabled", true);
                        toggleButton(true, id);
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
            function toggleButton(toQuery, id) {
                if (toQuery) {
                    $("#btnEdit_" + id).show();
                    $("#btnEdit_" + id).next().show();
                    $("#btnDelete_" + id).show();

                    $("#btnSave_" + id).hide();
                    $("#btnSave_" + id).next().hide();
                    $("#btnBack_" + id).hide();
                } else {
                    $("#btnEdit_" + id).hide();
                    $("#btnEdit_" + id).next().hide();
                    $("#btnDelete_" + id).hide();

                    $("#btnSave_" + id).show();
                    $("#btnSave_" + id).next().show();
                    $("#btnBack_" + id).show();
                }
            }
        </script>
    </body>
</html>