<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>



<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>變更驗證參數交易</h2>
            <form class="container main-content-block p-4" id="formData" action="<%= request.getContextPath() %>/BM3000/Result" method="post"  >
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ValidParamRec">驗證參數(收款)：</label>
                        <div class="col-5">
                            <input type="text" id="ValidParamRec" name="ValidParamRec"  class="form-control upper" value="${ValidParamRec}" disabled />
                            <input id="btnValidParamRec" type="button" value="確定" class="btn btn-info" aria-label="Left Align"  onclick="onGenValidParamRec()"  />
                        </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ValidParam">驗證參數(付款)：</label>
                        <div class="col-5">
                            <input type="text" id="ValidParam" name="ValidParam"  class="form-control upper" value="${ValidParam}" disabled />
                            <input id="btnValidParam" type="button" value="確定" class="btn btn-info" aria-label="Left Align"  onclick="onGenValidParam()"  />
                        </div>
                </div>
<!--                 
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnReset" type="reset" value="取消" class="btn btn-dark" aria-label="Left Align" />&nbsp;
                        <input id="btnCreate" type="button" value="確定" class="btn btn-info" aria-label="Left Align"  onclick="onSave()"  />
                    </div>
                </div>
                 -->
            </form>
        </div>

        <input type="hidden" id="error" name="error"  class="form-control" maxlength="10"  value="${error}"/>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.all.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.exhide.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
               var error= $("#error").val();
              if(error !="")
              {
                  alert(error);
                  return false;
              }
              var ValidParamRec = "${ValidParamRec}";
              if(ValidParamRec == "")
           	  {
           	  		$("#ValidParamRec").prop('disabled', false);
           	  }
              else
           	  {
            	  $("#ValidParamRec").prop('disabled', true);
           	  }
            }); //ready
            
            function onGenValidParam()
            {
//               $("#formData").submit();
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/BM3000/changeKey",
                    type: "POST",
                    data: {
                        "KEY": "validParam"
                    },
                    success: function (res) {
                        unlockScreen();
                        if (res.validated) {
                            alert("變更驗證參數(付款)成功");
                            location.href="<%= request.getContextPath() %>/BM3000/Index";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages, function (key, value) {
                                if (key === "summary")
                                    alert("變更驗證參數(付款)失敗：" + value);
                                else
                                    $('input[name=' + key + ']').after('<span class="error">' + value + '</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
            
            function onGenValidParamRec()
            {
//               $("#formData").submit();
				if($("#ValidParamRec").prop('disabled') == false)
				{
					if($("#ValidParamRec").val() == "")
					{
						alert("請輸入驗證參數(收款)！！");
						return false;
					}
				}
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/BM3000/changeKey",
                    type: "POST",
                    data: {
                        "KEY": "validParam_rec",
                        "VALUE": $("#ValidParamRec").prop('disabled') ? "" : $("#ValidParamRec").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if (res.validated) {
                            alert("變更驗證參數(收款)成功");
                            location.href="<%= request.getContextPath() %>/BM3000/Index";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages, function (key, value) {
                                if (key === "summary")
                                    alert("變更驗證參數(收款)失敗：" + value);
                                else
                                    $('input[name=' + key + ']').after('<span class="error">' + value + '</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
            
        </script>
    </body>
</html>