<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>



<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>信用卡申請錯誤次數解除</h2>
            <form class="container main-content-block p-4" id="formData" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="RESETTIME">交易時間：</label>
                    <label class="col-3 control-label">${Time}</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ASKERRTIMES">原驗證參數：</label>
                    <label class="col-5 control-label">${OldValidParam}</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ASKERRTIMES">變更後驗證參數：</label>
                    <label class="col-5 control-label">${NewValidParam}</label>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                    </div>
                </div>
            </form>
        </div>

        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.all.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.exhide.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
              
            }); //ready 

            // 回查詢頁
            function onExit() {
                location.href="<%= request.getContextPath() %>/BM3000/Index";
            }      
        </script>
    </body>
</html>