<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="form-group row" style="width: 95%">
        <label class="col-12 control-label text-center">
            <h4>預約交易結果查詢</h4>
        </label>
    </div>
    <div style="width: 95%">
        <div class="form-group row">
            <label class="col-3 control-label text-right">查詢時間：</label>
            <div class="col-9">
                <label class="control-label">
                    <jsp:useBean id="now" class="java.util.Date" />
                    <fmt:formatDate var="nowDateTime" value="${now}" pattern="yyyy/MM/dd HH:mm:ss" />
                    ${nowDateTime}
                </label>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-3 control-label text-right">查詢期間：</label>
            <div class="col-9">
                <label class="control-label">${startDt}~${endDt}</label>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-3 control-label text-right">交易狀態：</label>
            <div class="col-9">
                <label class="control-label">${statusWord}</label>
            </div>
        </div>
    </div>
    <div class="form-group row" style="width: 95%">
        <label class="col-12 control-label text-center">台幣轉帳結果(預約)</label>
    </div>
    <table id="mainNTD" class="display p-4 transparent" cellspacing="0" style="width:95%;">
        <thead>
            <tr>
          	  	<th>客戶身分證<br/>營利事業統一編號</th>
                <th>預約編號</th>
                <th>預約日期<br/>執行日期</th>
                <th>轉出帳號</th>
                <th>轉入帳號/<br>繳費稅代號</th>
                <th style="text-align: right;">轉帳金額</th>
                <th style="text-align: right;">手續費</th>
                <th>跨行序號</th>
                <th>交易代碼<br/>交易名稱</th>
                <th>轉帳結果</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr />
    <div class="form-group row"></div>
    <label class="col-12 control-label text-center">外匯轉帳/匯款結果(預約)</label>
    <table id="mainFC" class="display p-4 transparent" cellspacing="0" style="width:95%">
        <thead>
            <tr>
             	<th>客戶身分證<br/>營利事業統一編號</th>
                <th>預約編號</th>
                <th>預約日期<br/>執行日期</th>
                <th>轉出帳號</th>
                <th style="text-align: right;">轉出金額</th>
                <th>銀行名稱<br/>轉入帳號</th>
                <th style="text-align: right;">轉入金額</th>
                <th>匯率</th>
                <th style="text-align: right;">手續費<br>郵電費</th>
                <th style="text-align: right;">國外費用</th>
                <th>交易代碼<br/>交易名稱</th>
                <th>轉帳結果</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <br>
    <div class="text-left">
        <label class="col-2 control-label">說明：</label>
        <label class="col-10 control-label">
            <font style="color:red;">1. 查詢轉帳結果為『處理中』者，請於10:30後查詢。</font><br>2. 提供最近六個月的預約交易轉帳結果查詢。
        </label>
    </div>
    <!-- Modal -->
    <div class="modal fade bd-example-modal-sm" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">轉帳結果</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="model-body" class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
    <script>
    
        $(document).ready(function () {
            //台幣
            onQueryNTD();
            //外幣
            onQueryFC();
        }); //ready

        //開啟處理狀態 Dialog, Controller 會回傳 PartialView
        function showMsgCode(id) {
            var url = "<%= request.getContextPath() %>/B205/MsgDialog/" + id;
            lockScreen();
            $.post(url, {}, function (data, status) {
                unlockScreen();
                if (status == "success") {
                    //alert(data);
                    var htmlContent = data.replace(/\r\n/g, "");
                    $("#model-body").html(htmlContent);
                    $('#exampleModal').modal({

                    });
                }
            });
        }

        function onQueryNTD() {

            $("#mainNTD").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "columnDefs": [
                        { targets: 4, className: 'dt-body-right' },
                        { targets: 5, className: 'dt-body-right' },
                        ], 
                "order": [[0, "asc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/B205/QueryNTD",
                    "data": {
                        "USERID": "${userId}",
                        "STATUS": "${status}",
                        "STARTDT": "${startDt}",
                        "ENDDT": "${endDt}"
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                	{ "data": "twschpaydataidentity.dpuserid" },               
                	{ "data": "twschpaydataidentity.dpschno","data": "msaddr" },
                    { "data": "twschpaydataidentity.dpschtxdate" },
                    { "data": "dpwdac" },
                    { "data": "dpsvbh" },
                    { "data": "dptxamt" },
                    { "data": "dpefee" },
                    { "data": "dpstanno" },
                    { "data": "adopid" },
                    { "data": "dptxstatus" }
                ],
                "createdRow": function (row, data, index) {
                    var $dpschno=data.twschpaydataidentity.dpschno;
                    $("td", row).eq(1).text("").append($dpschno);
                    
                    var dpschtxdate = data.twschpaydataidentity.dpschtxdate;
                    var $dpschtxdate = $("<span>"+dpschtxdate.substr(0, 4) + "/" + dpschtxdate.substr(4, 2) + "/" + dpschtxdate.substr(6, 2) +"</span>");
					var dptxdate = data.dptxdate.trim();
                    var $dptxdate = "";
                    if ( dptxdate.length >= 8 ) {
                    	$dptxdate = $("<span>"+dptxdate.substr(0, 4)+"/"+dptxdate.substr(4, 2)+"/"+dptxdate.substr(6, 2)+"</span>");
                    } else {
                    	$dptxdate = $("<span></span>");
                    }
                    $("td", row).eq(2).text("").append($dpschtxdate).append($("<br />")).append($dptxdate);

                    var $accountNo = data.dpsvbh + "<br>" + data.dpsvac;
                    $("td", row).eq(4).text("").append($accountNo);

                    $("td", row).eq(5).text("").append(formatMoney(data.dptxamt));

                    $("td", row).eq(6).text("").append(formatMoney(data.dpefee));
                    $("td", row).eq(6).attr('style', 'text-align: right;');
                    
                    var $statusWord = "";
                    var $status = "";
                    switch (data.dptxstatus) {
                        case "0":
                            $statusWord = "成功";
                            break;
                        case "1":
                            $statusWord = "失敗";
                            break;
                        case "2":
                            $statusWord = "待二扣";
                            break;
                        case "3":
                            $statusWord = "處理中";
                            break;
                        default:
                            $statusWord = "未執行";
                            break;
                    }
                    $status = $statusWord + '<br><a href="#" onclick="showMsgCode(\'' + data.dpexcode + '\')">' + data.dpexcode + '</a>';

                    // 若有中文選單名稱，會以 "選單ID，選單名稱" 方式帶回
                    if ( data.adopid.indexOf(",") > 0 ) {
                        var tmp = data.adopid.split(",");
                        
                        var $adopid = $("<span>"+tmp[0]+"</span>");
                        var $adopname = $("<span>"+tmp[1]+"</span>");
                        var $br = $("<br />");
                        $("td", row).eq(8).text("").append($adopid).append($br).append($adopname);
                    }
                    
                    $("td", row).eq(9).text("").append($status);
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            return false;
        }

        function onQueryFC() {

            $("#mainFC").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "columnDefs": [
                        { targets: 3, className: 'dt-body-right' },
                        { targets: 5, className: 'dt-body-right' },
                        { targets: 4, className: 'dt-body-right' },
                        { targets: 7, className: 'dt-body-right' },
                        { targets: 8, className: 'dt-body-right' },
                        ], 
                "order": [[0, "asc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/B205/QueryFC",
                    "data": {
                        "USERID": "${userId}",
                        "STATUS": "${status}",
                        "STARTDT": "${startDt}",
                        "ENDDT": "${endDt}"
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                	{ "data": "fxschpaydataidentity.fxuserid" },
                	{ "data": "fxschpaydataidentity.fxschno","data": "msaddr" },
                    { "data": "fxtxdate" },
                    { "data": "fxwdac" },
                    { "data": "fxwdcurr" },
                    { "data": "fxsvbh" },
                    { "data": "fxsvcurr" },
                    { "data": "fxexrate" },
                    { "data": "fxefeeccy" },
                    { "data": "fxefeeccy" },
                    { "data": "adopid" },
                    { "data": "fxtxstatus" }
                ],
                "createdRow": function (row, data, index) {
                    var $fxschno=data.fxschpaydataidentity.fxschno;
                    $("td", row).eq(1).text("").append($fxschno);

                    var fxschtxdate = data.fxschpaydataidentity.fxschtxdate;
                    var $fxschtxdate = $("<span>"+fxschtxdate.substr(0, 4) + "/"+fxschtxdate.substr(4, 2)+"/"+fxschtxdate.substr(6, 2)+"</span>");
					var fxtxdate = data.fxtxdate.trim();
                    var $fxtxdate = "";
                    if ( fxtxdate.length >= 8 ) {
                    	$fxtxdate = $("<span>"+fxtxdate.substr(0, 4)+"/"+fxtxdate.substr(4, 2)+"/"+fxtxdate.substr(6, 2)+"</span>");
                    } else {
                    	$fxtxdate = $("<span></span>");
                    }
                    $("td", row).eq(2).text("").append($fxschtxdate).append($("<br />")).append($fxtxdate);

                    var $data3 = data.fxwdcurr + "<br>" + formatMoney(data.fxwdamt);
                    $("td", row).eq(4).text("").append($data3);

                    var $data4 = data.fxsvbh + "<br>" + data.fxsvac;
                    $("td", row).eq(5).text("").append($data4);

                    var $data5 = data.fxsvcurr + "<br>" + data.fxsvamt;
                    $("td", row).eq(6).text("").append($data5);
                    $("td", row).eq(6).attr('style', 'text-align: right;');

                    var $data7 = (data.fxefeeccy==null ? "" : data.fxefeeccy) + "<br>" + formatMoney(data.fxefee) + "<br>" + formatMoney(data.fxtelfee);
                    $("td", row).eq(8).text("").append($data7);

                    var str_OURCHG = data.fxourchg;
                    if (str_OURCHG == null) {
                        str_OURCHG = "";
                    }
                    var $data8 = (data.fxefeeccy==null ? "" : data.fxefeeccy) + "<br>" + str_OURCHG;
                    $("td", row).eq(9).text("").append($data8);

                    var $statusWord = "";
                    var $status = "";
                    switch (data.fxtxstatus) {
                        case "0":
                            $statusWord = "成功";
                            break;
                        case "1":
                            $statusWord = "失敗";
                            break;
                        case "2":
                            $statusWord = "待二扣";
                            break;
                        case "3":
                            $statusWord = "處理中";
                            break;
                        default:
                            $statusWord = "未執行";
                            break;
                    }
                    $status = $statusWord + '<br><a href="#" onclick="showMsgCode(\'' + data.fxexcode + '\')">' + data.fxexcode + '</a>';

                 	// 若有中文選單名稱，會以 "選單ID，選單名稱" 方式帶回
                    if ( data.adopid.indexOf(",") > 0 ) {
                        var tmp = data.adopid.split(",");
                        
                        var $adopid = $("<span>"+tmp[0]+"</span>");
                        var $adopname = $("<span>"+tmp[1]+"</span>");
                        var $br = $("<br />");
                        $("td", row).eq(10).text("").append($adopid).append($br).append($adopname);
                    }
                    $("td", row).eq(11).text("").append($status);
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            return false;
        }
    </script>
</body>

</html>