<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>預約交易結果查詢</h2>
        <form id="B205" name="B205" class="container main-content-block p-4"
            action="<%= request.getContextPath() %>/B205/Query" method="post">
            <div class="form-group row">
                <label class="col-4 control-label text-right">身分證/營利事業統一編號：</label>
                <div class="col-8">
                    <div class="form-inline">
                        <input type="text" id="UserId" name="UserId" class="form-control upper" maxlength="10" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-4 control-label text-right">查詢期間：</label>
                <div class="col-8">
                    <div class="form-inline">
                        <input type="text" id="StartDt" autocomplete="off" name="StartDt" class="form-control" style="width:160px"
                            maxlength="8" value="${StartDt}" />～
                        <input type="text" id="EndDt" autocomplete="off" name="EndDt" class="form-control" style="width:160px"
                            maxlength="8" value="${EndDt}" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-4 control-label text-right">交易狀態：</label>
                <div class="col-8">
                    <div class="form-inline">
                        <select class="form-control" name="Status" id="Status">
                            <option value="All">全部</option>
                            <option value="0">成功</option>
                            <option value="1">失敗</option>
                            <option value="2">處理中</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-4 col-8">
                    <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                    <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align"
                        onclick="onQuery()" />
                    <input id="btnOK" type="button" value="匯出" class="btn btn-info" aria-label="Left Align"
                        onclick="onExport()" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">說明：</label>
                <label class="col-10 control-label">1.提供最近六個月的預約交易轉帳結果查詢。</label>
            </div>
        </form>
        <form id="B205Export" name="B205Export" class="container main-content-block p-4" action="<%= request.getContextPath() %>/B205/Export" target="_blank" method="post">
        	<input type="hidden" name="ExportData" id="ExportData" />
        </form>
    </div>

    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
    <script>
        $(document).ready(function () {
            $('#StartDt').datetimepicker({
                lang: 'zh-TW',
                timepicker: false,
                format: 'Y/m/d',   // 顯示時分
                scrollMonth: false
            });
            $('#EndDt').datetimepicker({
                lang: 'zh-TW',
                timepicker: false,
                format: 'Y/m/d',   // 顯示時分
                scrollMonth: false
            });

        }); //ready

        function onQuery() {
            var isError = "";
            if (($("#StartDt").val() == "") || ($("#StartDt").val() == null))
                isError += "請輸入起始日期\n";
            if (($("#EndDt").val() == "") || ($("#EndDt").val() == null))
                isError += "請輸入結束日期\n";


            if (isError != "") {
                alert(isError)
                return;
            }
            $("#B205").submit();
        }
        
        function onExport() {
        	var exportData = {
        			UserId: $("#UserId").val(),
        			StartDt: $("#StartDt").val(),
        			EndDt: $("#EndDt").val(),
        			Status: $("#Status").val()
       			};
        	
        	$("#ExportData").val(JSON.stringify(exportData));
        	$("#B205Export").submit();
        }

    </script>
</body>

</html>