<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<div class="col-md-12">
    <div class="content">
            ${ErrMsg}
    </div>
</div>
<script>
    $("#tblDialog").DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false,
        "language": {
            "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
        }
    });
</script>