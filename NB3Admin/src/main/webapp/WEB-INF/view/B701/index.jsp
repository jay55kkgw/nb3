<% response.setHeader("X-Frame-Options", "DENY"); %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>一般網路銀行客戶登入數統計表</h2>
            <form id="ADMLOGINOUT" class="container main-content-block p-4" action="<%= request.getContextPath() %>/B701/Query" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >查詢期間：</label>
                    <div class="col-10">
                        <div class="radio">
                            <div class="form-inline">
                                <label><input type="radio" name="optradio" id="optradio" checked value="Month">&nbsp;</label>
                                <select class="form-control" id="yearS" name="yearS">
                                    <option value="">--請選擇--</option>
                                    <c:forEach items="${years}" var="itemYS">
                                        <option value="${itemYS}">${itemYS}</option>
                                    </c:forEach>
                                </select>&nbsp;年&nbsp;
                                <select class="form-control" id="monthS" name="monthS">
                                    <option value="">--請選擇--</option>
                                    <c:forEach items="${months}" var="itemMS">
                                        <option value="${itemMS}">${itemMS}</option>
                                    </c:forEach>
                                </select>&nbsp;月
                                ～
                                <select class="form-control" id="yearE" name="yearE">
                                    <option value="">--請選擇--</option>
                                    <c:forEach items="${years}" var="itemYE">
                                        <option value="${itemYE}">${itemYE}</option>
                                    </c:forEach>
                                </select>&nbsp;年&nbsp;
                                <select class="form-control" id="monthE" name="monthE">
                                    <option value="">--請選擇--</option>
                                    <c:forEach items="${months}" var="itmeME">
                                        <option value="${itmeME}">${itmeME}</option>
                                    </c:forEach>
                                </select>&nbsp;月                                
                            </div>
                        </div>
                        <br/>
                        <div class="radio">
                            <div class="form-inline">
                                <label><input type="radio" name="optradio" id="optradio"  value="Date" >&nbsp;</label>
                                <input type="text" id="dateFrom" name="dateFrom" class="form-control" style = "width:160px" maxlength="10" value="${B701Model.dateFrom}" />～
                                <input type="text" id="dateTo" name="dateTo" class="form-control" style = "width:160px" maxlength="10" value="${B701Model.dateTo}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >交易來源：
                    </label>
                    <div class="col-2" class="form-control">
                        <select id="loginType" name="loginType" class="form-control">
                            <option value="'','NB','MB'">全部</option>
                            <option value="'','NB'">網銀</option>
                            <option value="'MB'">行動</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="reset" type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" />&nbsp;
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnexpCSV" type="button" value="匯出CSV" class="btn btn-secondary" aria-label="Left Align" onclick="onExportCSV();" />
                        <input id="btnPrint" type="button" value="列印" class="btn btn-secondary" aria-label="Left Align" onclick="friendlyPrint('printQ,print');" />
                    </div>
                </div>
            </form>
            <!-- report -->
	        <div id="printQ">
	            <div class="form-group row">
	                <label class="col-12 control-label text-center"><h4>一般網路銀行客戶登入數統計表</h4></label>                
	            </div>
	            <div class="form-group row">
	                <label class="col-2 control-label text-right" >查詢時間：</label>
	                <div class="col-10">
	                    <div class="radio">
	                        <div class="form-inline">
	                            <jsp:useBean id="now" class="java.util.Date" />
	                            <fmt:formatDate var="nowDateTime" value="${now}" pattern="yyyy/MM/dd HH:mm:ss" />
	                            ${nowDateTime}
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-2 control-label text-right" >查詢期間：</label>
	                <div class="col-10">
	                    <div class="radio">
	                        <div class="form-inline">
	                            <c:if test="${B701Model.optradio == 'Month'}">
	                                ${B701Model.yearS}/${B701Model.monthS}&nbsp;~&nbsp;${B701Model.yearE}/${B701Model.monthE}
	                            </c:if>
	                            <c:if test="${B701Model.optradio == 'Date'}">
	                                ${B701Model.dateFrom}&nbsp;~&nbsp;${B701Model.dateTo}
	                            </c:if>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div id="print">
	            <table id="main" class="display p-4 transparent" cellspacing="0">
	                    <thead>
	                        <tr style="text-align: right;">
	                            <th style="width:15%">期間\類别</th>
	                            <th style="width:35%">企業戶</th>
	                            <th style="width:35%">個人戶</th>
	                            <th style="width:15%">合計</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <c:forEach items="${Data}" var="item" >
	                        <tr style="text-align: center">
	                            <td>${item.CMYYYYMM}</td>
	                            <td>${item.ADENTERP}</td>
	                            <td>${item.ADPERSON}</td>
	                            <td>${item.ADSUMROW}</td>
	                        </tr>
	                    </c:forEach>
	                </tbody>
	            </table>            
	        </div>
        </div>
        
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                $('#dateFrom').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: false,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });
                $('#dateTo').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: false,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });

                if ("${B701Model.optradio}" =="Month"){
                    $("input[type='radio'][value='Month']").prop("checked", true);
                }
                else
                {
                    $("input[type='radio'][value='Date']").prop("checked", true);
                }

                $("#yearS").val("${B701Model.yearS}");
                $("#monthS").val("${B701Model.monthS}");
                $("#yearE").val("${B701Model.yearE}");
                $("#monthE").val("${B701Model.monthE}");
                $("#loginType").val("${B701Model.loginType}");

                if ( "${IsPostBack}" == "true" ) {
                    $("#main").dataTable({
                        "responsive": true,
                        "processing": true,
                        "serverSide": false,
                        "orderMulti": false,
                        "bFilter": false,
                        "bSortable": true,
                        "bDestroy": true,
                        "pageLength": 100,
                        "order": [[0, "asc"]],
                        "dom": "<'clear'>frtip",   //無工具列
                        "columnDefs": [
                        { targets: 0, className: 'dt-body-right' },
                        { targets: 1, className: 'dt-body-right' },
                        { targets: 2, className: 'dt-body-right' },
                        { targets: 3, className: 'dt-body-right' },
                        ], 
                        "language": {
                            "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                        }
                    });
                    $("#printQ").show();
                    $("#print").show();
                    $("#reset").click(function() {
                        location.href="<%= request.getContextPath() %>/B701/Index";
                    });
                } else {
                    $("#printQ").hide();
                    $("#print").hide();
                    $("#reset").click(function() {
                        location.reload(); 
                    });
                }
            }); //ready

            $.fn.serializeObjectEx = function() {
                var o = {};
                //    var a = this.serializeArray();
                $(this).find('input[type="hidden"], input[type="text"], input[type="password"], input[type="checkbox"]:checked, input[type="radio"]:checked, select').each(function() {
                    if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
                        var $parent = $(this).parent();
                        var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
                        if ($chb != null) {
                            if ($chb.prop('checked')) return;
                        }
                    }
                    if (this.name === null || this.name === undefined || this.name === '')
                        return;
                    var elemValue = null;
                    if ($(this).is('select'))
                        elemValue = $(this).find('option:selected').val();
                    else elemValue = this.value;
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(elemValue || '');
                    } else {
                        o[this.name] = elemValue || '';
                    }
                });
                return o;
            }

            function validate() {
                if ( $("input[type='radio'][value='Month']").prop("checked")==true ) {
                    if ( $('#yearS').val() == "" || $('#monthS').val() == "" || $('#yearE').val() == "" || $('#monthE').val() == ""  ) {
                        alert("請輸入查詢期間起迄日");
                        return;
                    }

                    var dateFrom = Date.parse($("#yearS").val()+"/"+$("#monthS").val()+"/1");
                    var dateTo = Date.parse($("#yearE").val()+"/"+$("#monthE").val()+"/1");

                    if ( dateFrom > dateTo ) {
                        alert("查詢迄日(年月)必需大於查詢起日(年月)");
                        return false;
                    }
                    var today=new Date();
                    var firstDay = new Date(today.getFullYear(), today.getMonth(), 1);
                    if ( dateTo > firstDay ) {
                        alert("查詢迄日(年月)必需小於等於本日(年月)");
                        return;
                    }
                } else {
                    var dateFrom = Date.parse($("#dateFrom").val());
                    var dateTo = Date.parse($("#dateTo").val());

                    if(isNaN(dateFrom) || isNaN(dateTo) ) { 
                        alert("請輸入合法的查詢起迄日");
                        return false;
                    }

                    if ( dateFrom > dateTo ) {
                        alert("查詢迄日必需大於查詢起日");
                        return false;
                    }
                    var today=new Date();
                    if ( dateTo>today ) {
                        alert("查詢迄日與必須小於等於今天");
                        return;
                    }
                    if ( $('#dateFrom').val() == "" || $('#dateTo').val() == "" ) {
                        alert("請輸入查詢期間起迄日");
                        return;
                    }

                    var startDate = $("#dateFrom").val();
                    var endDate = $("#dateTo").val();

                    var sisvalid = Date.parse(startDate);
                    var eisvalid = Date.parse(endDate);
                    if(isNaN(sisvalid) || isNaN(eisvalid) ) { 
                        alert("請輸入合法的查詢期間起日和查詢期間迄日");
                        return;
                    }

                    if ( sisvalid > eisvalid ) {
                        alert("查詢期間迄日必需大於查詢期間起日");
                        return;
                    }
                    var today=new Date();
                    if ( eisvalid>today ) {
                        alert("查詢期間起日與迄日必須小於等於今天");
                        return;
                    }
                }
                return true;
            }
            function onQuery() {
                if ( validate() ) {
                    $("#ADMLOGINOUT").submit();
                }        
            }

            function onExportCSV() {
                // window.location = "<%= request.getContextPath() %>/B701/DownloadtoCSV";
                if ( !validate() ) {
                    return;
                }
                
                $.ajax({
                        url:"<%= request.getContextPath() %>/B701/DownloadtoCSV",
                        type:"POST",
                        data:{
                            optradio: $("#optradio").val(),
                            yearS: $("#yearS").val(),
                            monthS: $("#monthS").val(),
                            yearE: $("#yearE").val(),
                            monthE: $("#monthE").val(),
                            dateFrom: $("#dateFrom").val(),
                            dateTo: $("#dateTo").val(),
                            loginType: $("#loginType").val(),
                        },
                        success: function(response, status, xhr) {
                            // check for a filename
                            var filename = "";
                            var disposition = xhr.getResponseHeader('Content-Disposition');
                            if (disposition && disposition.indexOf('attachment') !== -1) {
                                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                                var matches = filenameRegex.exec(disposition);
                                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                            }

                            var type = xhr.getResponseHeader('Content-Type');
                            var blob = new Blob(["\ufeff" + response], { type: type });

                            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                                // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                                window.navigator.msSaveBlob(blob, filename);
                            } else {
                                var URL = window.URL || window.webkitURL;
                                var downloadUrl = URL.createObjectURL(blob);

                                if (filename) {
                                    // use HTML5 a[download] attribute to specify filename
                                    var a = document.createElement("a");
                                    // safari doesn't support this yet
                                    if (typeof a.download === 'undefined') {
                                        window.location = downloadUrl;
                                    } else {
                                        a.href = downloadUrl;
                                        a.download = filename;
                                        document.body.appendChild(a);
                                        a.click();
                                    }
                                } else {
                                    window.location = downloadUrl;
                                }

                                setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                            }
                        },
                        error:function(){
                            alert("error");
                        }
                });                
            }

        </script>
    </body>
</html>