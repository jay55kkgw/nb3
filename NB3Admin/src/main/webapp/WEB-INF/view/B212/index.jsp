<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<% response.setHeader("Content-Security-Policy", "frame-ancestors 'self'"); %>

<html>
    <head>
        <title>台企銀 裝置推播查詢及管理</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">     
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>裝置推播查詢及管理</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >身分證/營利事業統一編號：</label>
                    <div class="col-10">
                    	<div class="form-inline">
                        	<input type="text" id="CustNo" name="CustNo" class="form-control" maxlength="10" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >iDGate ID：</label>
                    <div class="col-10">
                    	<div class="form-inline">
                        	<input type="text" id="IdGateID" name="IdGateID" class="form-control" maxlength="85" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >查詢類別：</label>
                    <div class="col-2">
                       	<select id="QueryType" class="form-control" onChange="onChangeQueryType(this);" >
                       		<option value="01" selected="selected">01 裝置狀態</option>
                       		<option value="02">02 驗證歷程</option>
                       	</select>                    
                    </div>
                </div>
                <div id="divSDate" class="form-group row" style="display:none">
                    <label class="col-2 control-label text-right" >開始日期：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            	<input type="text" id="SDate" autocomplete="off" name="SDate" class="form-control" style = "width:160px" maxlength="10" value="${StartDtFrom}" />
                        </div>
                    </div>
                </div>
                <div id="divEDate" class="form-group row" style="display:none">
                    <label class="col-2 control-label text-right" >結束日期：</label>
                    <div class="col-10">
                    	<div class="radio">
	                        <div class="form-inline">
	                            <input type="text" id="EDate" autocomplete="off" name="EDate" class="form-control" style = "width:160px" maxlength="10" value="${EndDtFrom}" />
	                        </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnClear" type="reset" value="重新輸入" class="btn btn-dark" aria-label="Left Align" onClick="window.location.reload();"/>&nbsp;
                        <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="return onQuery();" />&nbsp;
                    </div>
                </div>
            </form>
        </div>

        <table id="main01" class="display p-4 transparent" cellspacing="0" style="width:95%">
            <thead>
                <tr>
                    <th>IDGATE<BR>ID</th>
                    <th>裝置暱稱 </th>
                    <th>裝置狀態</th>
                    <th>作業系統</th>
                    <th>廠牌型號</th>
                    <th>安控機制</th>
                    <th>錯誤次數</th>
                    <th>註冊日期</th>
                    <th>最新異動</th>
                    <!-- <th>是否為預設</th> -->
                    <th>異動人員</th>
                    <th>註銷</th>
                    <th>是否開啟<br>快速登入</th>
                    <th>是否開啟<br>快速交易</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>


		<table id="main02" class="display p-4 transparent" cellspacing="0" style="width:95%">
            <thead>
                <tr>
                    <th>IDGATE<BR>ID</th>
                    <th>交易管道</th>
                    <th style="text-align: center;">驗證碼申請時間</th>
                    <th>綁定狀態</th>
                    <th style="text-align: center;">綁定時間</th>
                    <th>交易類型</th>
                    <th>認證狀態</th>
                    <th style="text-align: center;">交易時間</th>
                    <th>裝置廠牌</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

        
        <jsp:include page="../include/caseStatus.jsp"></jsp:include>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
        <script>

            $(document).ready(function () {
                $('#SDate').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false,
                    timepicker: false
                });
                $('#EDate').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false,
                    timepicker: false
                });
                $("#main01").hide();
                $("#main02").hide();
                $("#main01_wrapper").hide();
                $("#main02_wrapper").hide();
            }); //ready
            
            function onQuery()
            {
            	var QueryType = $('#QueryType').val();
            	if(QueryType == "01")
           		{
            		onQuery01();
           		}
            	else if(QueryType == "02")
           		{
            		onQuery02();
           		}
            	else
           		{
           			alert("無此功能！！");
           		}
       			return ;
            }

            function onQuery01() {
            	var role="${__LOGIN_ROLES__}";
            	var CustNo = $('#CustNo').val();
            	var IdGateid = $('#IdGateID').val();
            	
        	    if(CustNo == '' && IdGateid == '' )
       	    	{
					alert("請輸入統編或IdGate編號");
					return;
       	    	}

        	    var QueryType = $('#QueryType').val();
                if(QueryType == "02")
               	{
                    var eisvalid = Date.parse($("#SDate").val());
                    var eisvalid = Date.parse($("#SDate").val());
                    
                    if(isNaN(eisvalid) || isNaN(eisvalid) ) { 
                        alert("請輸入合法的起日和迄日");
                        return;
                    }
               	
               	}


                $("#main01").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "autoWidth":false,
                    "pageLength": 10,
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B212/IndexQuery",
                        "data": {
                        	"custNo" : CustNo,
                        	"idGateId" : IdGateid,
                            "QueryType" : QueryType,
                            "SDate" : QueryType == "02" ? $("#SDate").val() : "",
                            "EDate" : QueryType == "02" ? $("#EDate").val() : ""
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columnDefs": [
                        { targets: 7, className: 'dt-body-center' },
                        { targets: 8, className: 'dt-body-center' }
                        ], 
                    "columns": [
                        { "data": "pks.idgateid" },
                        { "data": "devicename" },
                        { "data": "devicestatus" },
                        { "data": "deviceos" },
                        { "data": "devicebrand"},
                        { "data": "verifytype" },
                        { "data": "errorcnt" },
                        { "data": "regdate" },
                        { "data": "lastdate" },
                       // { "data": "isdefault",
                       // 	"render": function ( data, type, row, meta ) {
                       //	    if(data =='Y'){
                       //	    	return '是';
                       // 	    }else{
                       // 	    	return '否';
                       // 	    }
                       // 	}	
                       // },
                        { "data": "updateuser" },
                        { "data": "btcancel" },
                        { "data": "qcklgin",
                        	"render": function ( data, type, row, meta ) {
                        	    if(data =='Y'){
                        	    	return '是';
                        	    }else{
                        	    	return '否';
                        	    }
                        	}	
                        },
                        { "data": "qcktran",
                        	"render": function ( data, type, row, meta ) {
                        	    if(data =='Y'){
                        	    	return '是';
                        	    }else{
                        	    	return '否';
                        	    }
                        	}		
                        }
                    ],
                    "createdRow": function (row, data, index) {
						$("td", row).eq(0).text(data.pks.idgateid);
						$("td", row).eq(1).text(data.devicename);

                        var devicestatus = "";
                        if(data.devicestatus == "0")
                       	{
                        	devicestatus="正常";
                       	}
                        else if(data.devicestatus == "1")
                       	{
                        	devicestatus="鎖定中";
                       	}
                        else if(data.devicestatus == "2")
                       	{
                        	devicestatus="驗證失敗過多鎖定";
                       	}
                        else if(data.devicestatus == "3")
                       	{
                        	devicestatus="尚未完成註冊";
                       	}
                        else if(data.devicestatus == "9")
                       	{
                        	devicestatus="帳號已註銷";
                       	}
                        else
                        	devicestatus=data.devicestatus;
                        $("td", row).eq(2).text(devicestatus);

                        $("td", row).eq(3).text(data.deviceos == null ? "" : data.deviceos);
                        
                        $("td", row).eq(4).text(data.devicebrand == null ? "" : data.devicebrand);
                        
                        var verifytype = "";
                        if(data.verifytype == "1")
                       	{
                        	verifytype="生物識別";
                       	}
                        else if(data.verifytype == "2")
                       	{
                        	verifytype="圖形";
                       	}
                        else
                        {
                        	verifytype = data.verifytype;
                        }
                        
                        $("td", row).eq(5).text(verifytype);
                        
                        $("td", row).eq(6).text(data.errorcnt);
                        
                        if(data.regdate != null && data.regtime != null)
                        {
                        // 處理起日                
                            var regdate = data.regdate.substring(0,4)+"-"+data.regdate.substring(4,6)+"-"+data.regdate.substring(6,8);
                            var regtime = data.regtime.substring(0,2)+":"+data.regtime.substring(2,4)+":"+data.regtime.substring(4,6);
                            var $datetime="<DIV class='center'>" + regdate + "</DIV>" + "<DIV class='center'>" + regtime + "</DIV>";
                            $("td", row).eq(7).text("").append($datetime);
                        }
                        else
                        	$("td", row).eq(7).text('');

                        if(data.lastdate != null && data.lasttime != null)
                        {
                        	var lastdate = data.lastdate.substring(0,4)+"-"+data.lastdate.substring(4,6)+"-"+data.lastdate.substring(6,8);
                            var lasttime = data.lasttime.substring(0,2)+":"+data.lasttime.substring(2,4)+":"+data.lasttime.substring(4,6);
                            var $datetime="<DIV class='center'>" + lastdate + "</DIV>" + "<DIV class='center'>" + lasttime + "</DIV>";
                            $("td", row).eq(8).text("").append($datetime);
                        }
                        else
                       	{
                        	$("td", row).eq(8).text("")
                       	}
                        
                        //$("td", row).eq(9).text(data.isdefault);
                        
                        $("td", row).eq(10).text(data.updateuser == null ? "" : data.updateuser);
                        
                        if (role=="BC3")
                        {
	                        if(data.devicestatus == "0" || data.devicestatus == "1")
	                       	{
	                            var $btcancel = $("<input type='button' value='註銷' />");
	                            $btcancel.attr("onclick","onCancel('"+data.pks.idgateid+"','"+data.pks.cusidn+"','"+data.deviceid+"','"+data.devicetype+"', $(this))");
	                            $("td", row).eq(10).text('').append($btcancel);
	                       	}
	                        else
	                       	{
	                        	$("td", row).eq(10).text('')
	                       	}
                        }
                        else
                       	{
                        	$("td", row).eq(10).text('')
                       	}
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main01").show();
                return false;
            }

            function onQuery02() {
            	
            	var CustNo = $('#CustNo').val();
            	var IdGateid = $('#IdGateID').val();
        	    if(CustNo == '' && IdGateid == '' )
       	    	{
					alert("請輸入統編或IdGate編號");
					return;
       	    	}

        	    var QueryType = $('#QueryType').val();
                if(QueryType == "02")
               	{
                    var eisvalid = Date.parse($("#SDate").val());
                    var eisvalid = Date.parse($("#SDate").val());
                    
                    if(isNaN(eisvalid) || isNaN(eisvalid) ) { 
                        alert("請輸入合法的起日和迄日");
                        return;
                    }
               	
               	}


                $("#main02").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B212/IndexQuery",
                        "data": {
                        	"custNo" : CustNo,
                        	"idGateId" : IdGateid,
                            "QueryType" : QueryType,
                            "SDate" : QueryType == "02" ? $("#SDate").val().replace(/\//g, '') : "",
                            "EDate" : QueryType == "02" ? $("#EDate").val().replace(/\//g, '') : ""
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columnDefs": [
                        { targets: 2, className: 'dt-body-center' },
                        { targets: 4, className: 'dt-body-center' },
                        { targets: 7, className: 'dt-body-center' }
                        ], 
                    "columns": [
                        { "data": "idgateid" },
                        { "data": "logintype" },
                        { "data": "binddate" },
                        { "data": "bindstatus" },
                        { "data": "binddate"},
                        { "data": "adopname" },
                        { "data": "status" },
                        { "data": "trandate" },
                        { "data": "devicebrand" }
                    ],
                    "createdRow": function (row, data, index) {
						$("td", row).eq(0).text(data.idgateid);

						var logintype = "";
                        if(data.logintype == "NB3")
                       	{
                        	logintype="網銀";
                       	}
                        else if(data.logintype == "0")
                       	{
                        	logintype="行動";
                       	}
                        else
                       	{
                        	logintype=data.logintype;
                       	}
                        $("td", row).eq(1).text(logintype);
                        
                        if(data.binddate != null && data.bindtime != null)
                        {
                        // 處理起日                
                            var binddate = data.binddate.substring(0,4)+"-"+data.binddate.substring(4,6)+"-"+data.binddate.substring(6,8);
                            var bindtime = data.bindtime.substring(0,2)+":"+data.bindtime.substring(2,4)+":"+data.bindtime.substring(4,6);
                            var tmpdate="<DIV class='center'>" + binddate + "</DIV>" + "<DIV class='center'>" + bindtime + "</DIV>";
                            $("td", row).eq(2).text('').append(tmpdate);

                            $("td", row).eq(4).text('').append(tmpdate);

						}
                        else
                        {
                        	$("td", row).eq(2).text('');
                        	$("td", row).eq(4).text('');

                        }
                        
                        var bindstatus = "";
                        if(data.bindstatus == null)
                       	{
                        	bindstatus="尚未綁定";
                       	}
                        else if(data.bindstatus.replace(/\s+/g, '') == "0")
                       	{
                        	bindstatus="成功";
                       	}
                        else 
                       	{
                        	bindstatus="失敗(" + data.bindstatus.replace(/\s+/g, '') + ")";
                       	}
                        
                        $("td", row).eq(3).text(bindstatus);
                        console.log("data.adopname = "+data.adopname);
						//之後要盤的時候可以用
                        //data.adopname == '' ? $("td", row).eq(5).html("<font color='red'><b>查無代碼對應功能</b></font>"+ " ("+data.adopid+")")  : $("td", row).eq(5).text(data.adopname + " ("+data.adopid+")");
						$("td", row).eq(5).text(data.adopname == '' ? data.adopid : data.adopname + " ("+data.adopid+")");
                        
                        var status = "";
                        if(data.status == null || data.status == "")
                       	{
                        	status="未知狀態";
                       	}
                        else if(data.status == "0")
                       	{
                        	status="成功";
                       	}
                        else
                        {
                        	status = "失敗(" + data.status + ")";
                        }
                        $("td", row).eq(6).text(status);
                        
                        var trandate = "";
                        var trantime = "";
                        if(data.trandate != null && data.trantime != null)
                        {
                        // 處理起日                
                            trandate = data.trandate.substring(0,4)+"-"+data.trandate.substring(4,6)+"-"+data.trandate.substring(6,8);
                            trantime = +data.trantime.substring(0,2)+":"+data.trantime.substring(2,4)+":"+data.trantime.substring(4,6);
                            tmpdate="<DIV class='center'>" + trandate + "</DIV>" + "<DIV class='center'>" + trantime + "</DIV>";
                            $("td", row).eq(7).text('').append(tmpdate);
                        }
                        else
                        	$("td", row).eq(7).text('');

                        $("td", row).eq(8).text(data.devicebrand == null ? "" : data.devicebrand);
                        
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main02").show();
                return false;
            }

            function onChangeQueryType(obj)
            {
            	if($(obj).val() == "01")
           		{
            		$('#SDate').val('');
            		$('#ESDate').val('');
            		$('#divSDate').hide();
            		$('#divEDate').hide();
           		}
            	else
           		{
            		$('#divSDate').show();
            		$('#divEDate').show();
            		var today = new Date();
            		var sday = new Date(today.getFullYear(), today.getMonth()-6, today.getDate());
            		$( '#SDate' ).val(sday.getFullYear() + "/" + pad(sday.getMonth()+1, 2) + "/" + pad(sday.getDate(), 2));
            		$( '#EDate' ).val(today.getFullYear() + "/" + pad(today.getMonth()+1, 2) + "/" + pad(today.getDate(), 2));
           		}
                $("#main01").hide();
                $("#main02").hide();
                $("#main01_wrapper").hide();
                $("#main02_wrapper").hide();
            }
            
            function onCancel(cancelidgateid, canceluserid, canceldeviceid, canceldevicetype, obj)
            {
            	var CustNo = $('#CustNo').val();
            	var IdGateid = $('#IdGateID').val();
				var url = "<%= request.getContextPath() %>/B212/Cancel";
				lockScreen();
				$.post(url, {
					"CustNo" : CustNo,
					"IdGateId" : IdGateid,
					"CancelCustNo" : canceluserid,
					"CancelIdGateId" : cancelidgateid,
					"cancelDeviceId" : canceldeviceid,
					"CancelDeviceType" : canceldevicetype
				}, 
				function (data, status) {
					unlockScreen();
					if (status == "success") {
						if (data.Validated == true)
						{
		                    var devicestatus = "";
		                    if(data.tbData.devicestatus == "0")
		                   	{
		                    	devicestatus="正常";
		                   	}
		                    else if(data.tbData.devicestatus == "1")
		                   	{
		                    	devicestatus="鎖定中";
		                   	}
		                    else if(data.tbData.devicestatus == "2")
		                   	{
		                    	devicestatus="驗證失敗過多鎖定";
		                   	}
		                    else if(data.tbData.devicestatus == "3")
		                   	{
		                    	devicestatus="尚未完成註冊";
		                   	}
		                    else if(data.tbData.devicestatus == "9")
		                   	{
		                    	devicestatus="帳號已註銷";
		                   	}
		                    else
		                    	devicestatus=data.tbData.devicestatus;
		
							$(obj).parents("tr").find("td:eq(2)").text(devicestatus);
		                    if(data.tbData.lastdate != null && data.tbData.lasttime != null)
		                    {
		                    	var lastdate = data.tbData.lastdate.substring(0,4)+"-"+data.tbData.lastdate.substring(4,6)+"-"+data.tbData.lastdate.substring(6,8);
		                        var lasttime = data.tbData.lasttime.substring(0,2)+":"+data.tbData.lasttime.substring(2,4)+":"+data.tbData.lasttime.substring(4,6);
		                        var $datetime="<DIV class='center'>" + lastdate + "</DIV>" + "<DIV class='center'>" + lasttime + "</DIV>";
		                        $(obj).parents("tr").find("td:eq(8)").text("").append($datetime);
		                    }
		                    else
		                   	{
		                    	$(obj).parents("tr").find("td:eq(8)").text("")
		                   	}
		
							$(obj).parents("tr").find("td:eq(10)").text('客服　' + (data.tbData.updateuser == null ? "" : data.tbData.updateuser));
							$(obj).parents("tr").find("td:eq(11)").remove();
							alert("註銷成功");
						}
						else
							alert("錯誤：" + data.ErrorMessages.rt + "：" + data.ErrorMessages.message);
					}
				});
            }
            
			function pad(num, size) {
			    var s = "000000000" + num;
			    return s.substr(s.length-size);
			}

        </script>
    </body>
</html>
