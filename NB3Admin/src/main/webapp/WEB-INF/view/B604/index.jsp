<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<% response.setHeader("Content-Security-Policy", "frame-ancestors 'self'"); %>

<html>

<head>
    <title>臺企銀 後台首頁</title>

    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>客戶滿意度調查管理</h2>
        <form id="B201" name="B201" class="container main-content-block p-4" >
            <div class="form-group row">
                <label class="col-3 control-label text-right">收件信箱：</label>
                <div class="col-9">
                    <input type="text" id="mails" name="mails" class="form-control" maxlength="500" value = "${mails}"/>(格式：mail1@xx.com.tw;mail2@xx.com.tw，分號區隔)
                </div>
            </div>
            <div class="form-group row">
                <label class="col-3 control-label text-right">啟用日期：</label>
                <div class="col-9">
                    <div class="form-inline">
                        <input type="text" id="startDate" autocomplete="off" readonly="readonly" name="startDate" class="form-control" style="width:160px" maxlength="4" />(格式：MMDD)
                    </div>
                </div>
            </div>
             <div class="form-group row">
                <label class="col-3 control-label text-right">停用日期：</label>
                <div class="col-9">
                    <div class="form-inline">
                        <input type="text" id="endDate" autocomplete="off" readonly="readonly" name="endDate" class="form-control" style="width:160px" maxlength="4" />(格式：MMDD)
                    </div>
                </div>
            </div>
             <div class="form-group row">
                <label class="col-3 control-label text-right">是否啟用</label>
                <div class="col-9">
                    <div class="form-inline">
                       <select class="form-control" id="isOpen" name="isOpen">
                           <option value="Y" selected="selected">啟用</option>
                           <option value="N">停用</option>
                        </select>
                    </div>
                </div>
            </div>
            <c:if test="${showAdd == true}"></c:if>
            <div class="form-group row">
                <div class="offset-3 col-9">
                    <input id="btnOK" type="button" value="儲存" class="btn btn-info" aria-label="Left Align" onclick="onSend()" />
                </div>
            </div>
        </form>
    </div>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#startDate').datetimepicker({
                lang: 'zh-TW',
                timepicker: false,
                scrollMonth: false,
                format:  'md', 
                hours12: false,
                maxTime: false,	
            });
            $('#endDate').datetimepicker({
                lang: 'zh-TW',
                timepicker: false,
                format:  'md', 
                scrollMonth: false,
                hours12: false,
                maxTime: false
            });
            $("#main").hide();
            $("#startDate").val("${startDate}");
            $("#endDate").val("${endDate}");
            $("#isOpen").val("${isOpen}");
            if($("#isOpen").val()==''){
            	   $("#isOpen").val("Y");
                }
        }); //ready


        function onSend() {
            const startDate = $("#startDate").val();
            const endDate = $("#endDate").val();
            const mails = $("#mails").val();
            const isopen = $("#isOpen").val();
			
            if ( startDate == "" || endDate == "" ) {
                alert("請選擇查詢期間起~迄");
                return;
            }
            $.ajax({
                url: "<%= request.getContextPath() %>/B604/Edit",
                type: "POST",
                data: {
                    "mails":mails,
                	"startDate":startDate,
                	"endDate":endDate,
                	"isOpen":isopen          	
                    },
                success: function (res) {
                    unlockScreen();
                    if (res.validated) {
                        alert("更新成功");
                        location.href = "<%= request.getContextPath() %>/B604";
                    } else {
                        //Set error messages
                        $.each(res.errorMessages, function (key, value) {
                            if (key === "summary") alert(value);
                            else {
                            	 var tel = /[^\.]+/;
                            	 //如果是複合主鍵，會有"."，所以改用ID
                            	 if(tel.test(value)==true){
                                	 keysplit = key.split('\.');
                                	 key = keysplit[keysplit.length-1];
                            		 $('input[id='+key+']').after('<span class="error">'+value+'</span>');
                            	    }   
                            	 else{
                              		  $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            	 }
                            }
                        });
                    }
                },
                error: function (err) {
                    unlockScreen();
                    console.log(err.statusText);
                },
            });
        }

    </script>
</body>

</html>