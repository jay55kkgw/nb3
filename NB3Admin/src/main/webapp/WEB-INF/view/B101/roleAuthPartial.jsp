<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="col-md-12">
    <div class="form-group row">
        <label class="col-2 control-label text-right">功能代碼：</label>
        <div class="col-10">
            <div class="form-inline">
                <select id="menu1" class="form-control" onchange="onMenuChange(this, 'menu2');">
                    <option value="">--請選擇--</option>
                    <c:forEach var="menu" items="${menus}">
                        <option value="${menu.ADOPID}">${menu.ADOPNAME}</option>
                    </c:forEach>
                </select>&nbsp;
                <select id="menu2" class="form-control" onchange="onMenuChange(this, 'menu3');">
                    <option value="">--請選擇--</option>
                </select>&nbsp;
                <select id="menu3" class="form-control">
                    <option value="">--請選擇--</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-2 control-label text-right">權限：</label>
        <div class="col-10">
            <label>
                <input id="ISQUERY" name="ISQUERY" type="checkbox" onchange='handleQuery(this);' />&nbsp;查詢
            </label>
            <label>
                <input id="ISEDIT" name="ISEDIT" type="checkbox" onchange='handleEditExec(this);' />&nbsp;執行
            </label>
            <label>
                <input id="ISEXEC" name="ISEXEC" type="checkbox" onchange='handleEditExec(this);' />&nbsp;放行
            </label>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-2 control-label text-right"></label>
        <div class="col-10">
            <input type="button" class="btn btn-info" value="新增" onclick="roleAuthCallback();" />
        </div>
    </div>
</div>
<script>
    /*選單選擇異動時，要發 AJAX POST，以取得下一層的子選單*/
    function onMenuChange(obj, t) {
        var menuId = $(obj).val();
        var target = $("#"+t);

        if ( menuId == "" ) {
            target.empty();
            target.append($("<option></option>").attr("value", "").text("---請選擇---"));
            return;
        }
        
        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/B112/SubMenu/"+menuId,
            type: "POST",
            data: {},
            success: function (data) {
                unlockScreen();
                target.empty();
                target.append($("<option></option>").attr("value", "").text("---請選擇---"));
                var menus = JSON.parse(data);
                for (var i = 0; i < menus.length; i++) {
                    target.append($("<option></option>").attr("value", menus[i].ADOPID).text(menus[i].ADOPNAME));
                }
            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }   

    /* 若有勾選執行或放行權限，則預設要有查詢（不可勾除） */
    function handleQuery(checkbox) {
        if ( !checkbox.checked && ( $("#ISEDIT").prop('checked') || $("#ISEXEC").prop('checked') ) ) {
            $("#ISQUERY").prop('checked', true);
        } 
    }

    /* 若有勾選執行或放行權限，則預設要有查詢（強迫給） */
    function handleEditExec(checkbox) {
        if ( checkbox.checked ) {
            $("#ISQUERY").prop('checked', checkbox.checked);
        } 
    }

    function getRoleAuth() {
        var obj = { "lastuser":"teller", "apopid":"","apopname":"","adroleno":"","adstaffno":"","adauth":"","adauthid":0,"isquery":"","isedit":"","isexec":"","lastdate":"20190101","lasttime":"120000" };
        
        var menu1 = $("#menu1").val();
        var menu2 = $("#menu2").val();
        var menu3 = $("#menu3").val();

        if ( menu3 !== "" ) {
            obj.apopid = menu3;
            obj.apopname = $("#menu3 option:selected").text();
        }
        else if ( menu2 !== "" ) {
            obj.apopid=menu2;
            obj.apopname=$("#menu2 option:selected").text();
        }
        else {
            obj.apopid=menu1;
            obj.apopname=$("#menu1 option:selected").text();
        } 

        obj.isquery=$("#ISQUERY").prop('checked') ? "1" : "0";
        obj.isedit=$("#ISEDIT").prop('checked') ? "1" : "0";
        obj.isexec=$("#ISEXEC").prop('checked') ? "1" : "0";

        return obj;
    }
</script>