<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>角色權限維護-審核</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post" >
                <div class="form-group row">
                    <label class="col-2 control-label text-right">角色代碼：</label>
                    <label id="ADROLENO" class="control-label">${roleid}</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">部門代碼：</label>
                    <label id="STAFFNO" class="control-label">${staffno}</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">角色權限：</label>
                    <div class="col-10">
                        <table id="main">
                            <thead>
                                <tr>
                                    <th>功能代碼</th>
                                    <th>功能名稱</th>
                                    <th>權限</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >退回原因：</label>
                    <div class="col-10">
                        <input type="text" id="Comments" name="Comments" class="form-control" maxlength="50" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;                      
                        <input id="btnReject" type="button" value="退回" class="btn btn-danger" aria-label="Left Align" onclick="onReject()" />&nbsp;
                        <input id="btnApprove" type="button" value="覆核" class="btn btn-info" aria-label="Left Align" onclick="onApprove()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            var roleAuths=[];

            $(document).ready(function () {
                roleAuths = JSON.parse('${roleAuths}');
                
                onRoleBind();
            }); //ready

            function onRoleBind() {
                $("#main").dataTable({
                    "responsive": true,
                    "bPaginate": false,
                    "bFilter": false,
                    "bDestroy": true,
                    "bSort": false,
                    "aaData": roleAuths,
                    "aoColumns": [
                        { "mDataProp": "apopid" },
                        { "mDataProp": "apopname" },
                        { "mDataProp": "adroleno" }
                    ],
                    "createdRow": function (row, data, index) {
                        // 2：查詢，執行，放行
                        var $query = $("<input type='checkbox' disabled='disabled' />");
                        $query.attr("value", data.isquery);
                        if ( data.isquery == "1" )
                            $query.attr("checked", "checked");
                        $("td", row).eq(2).text("").append($query).append($("<span>&nbsp;查詢&nbsp;&nbsp;</span>"));

                        var $edit = $("<input type='checkbox' disabled='disabled' />");
                        $edit.attr("value", data.isedit);
                        if ( data.isedit == "1" )
                            $edit.attr("checked", "checked");
                        $("td", row).eq(2).append($edit).append($("<span>&nbsp;執行&nbsp;&nbsp;</span>"));

                        var $exec = $("<input type='checkbox' disabled='disabled' />");
                        $exec.attr("value", data.isexec);
                        if ( data.isexec == "1" )
                            $exec.attr("checked", "checked");
                        $("td", row).eq(2).append($exec).append($("<span>&nbsp;放行&nbsp;&nbsp;</span>"));
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
            }

            // 回查詢頁
            function onExit() {
                location.href="<%= request.getContextPath() %>/B101";
            }

            // 同意
            function onApprove() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B101/Approve/${casesn}",
                    type: "POST",
                    data: {
                        "StepId":"${stepid}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("角色權限覆核成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B101";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            // 退回
            function onReject() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B101/Reject/${casesn}",
                    type: "POST",
                    data: {
                        "StepId":"${stepid}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("角色權限退回成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B101";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
        </script>
    </body>
</html>
