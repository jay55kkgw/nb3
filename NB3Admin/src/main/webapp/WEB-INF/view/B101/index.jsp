<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>角色權限維護-待辦事項</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post" >
                <div class="form-group row">
                    <div class="col-12">
                        <input type="button" value="重查待辦" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnQuery" type="button" value="查詢" class="btn btn-dark" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnCreate" type="button" value="設定" class="btn btn-info" aria-label="Left Align" onclick="onCreate()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- 待辦事項 -->
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th style="width:10%">待辦序號</th>
                    <th style="width:30%">授權角色</th>
                    <th style="width:20%">狀態</th>
                    <th style="width:20%">送件日期</th>
                    <th style="width:20%">送件者</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <jsp:include page="../include/caseStatus.jsp"></jsp:include>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                onQueryTodo();
            }); //ready
        
            function reset() {
                $("#menu1").empty();
                $("#menu1").append($("<option></option>").attr("value", "").text("---請選擇---"));
                $("#menu2").empty();
                $("#menu2").append($("<option></option>").attr("value", "").text("---請選擇---"));
                $("#main").dataTable().clear().redraw();
            }

            // 取得選單值，只記錄最後一層
            function getMenuValue() {
                var menu1 = $("#menu1").val();
                var menu2 = $("#menu2").val();
                var menu3 = $("#menu3").val();

                if ( menu3 !== "" )
                    return menu3;
                else if ( menu2 !== "" )
                    return menu2;
                else 
                    return menu1;
            }

            // 選單選擇異動時，要發 AJAX POST，以取得下一層的子選單
            function onMenuChange(obj, t) {
                var menuId = $(obj).val();
                var target = $("#"+t);

                if ( menuId == "" ) {
                    target.empty();
                    target.append($("<option></option>").attr("value", "").text("---請選擇---"));
                    return;
                }
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B112/SubMenu/"+menuId,
                    type: "POST",
                    data: {},
                    success: function (data) {
                        unlockScreen();
                        target.empty();
                        target.append($("<option></option>").attr("value", "").text("---請選擇---"));
                        var menus = JSON.parse(data);
                        for (var i = 0; i < menus.length; i++) {
                            target.append($("<option></option>").attr("value", menus[i].ADOPID).text(menus[i].ADOPNAME));
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            // 轉到查詢頁面
            function onQuery() {
                location.href = "<%= request.getContextPath() %>/B101/QueryRole";
            }

            // 分頁查詢
            function onQueryTodo() {
                $("span.error").remove();

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "order": [[1, "asc"]],
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B101/QueryTodo",
                        "data": {
                            "ADROLENO": ""
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [
                    { "data": "currentidentity.casesn"   },
                        { "data": "flowmemo" },
                        { "data": "stepname" },
                        { "data": "stepname" },
                        { "data": "stepname" }
                    ],
                    "createdRow": function (row, data, index) {
                        // ID
                        var $id = $("<a>"+data.currentidentity.casesn+"<a/>");
                        $id.attr("href", "<%= request.getContextPath() %>/B101/Query/"+data.currentidentity.casesn);
                        $("td", row).eq(0).text("").append($id);

                        var $caseStatus = $("<a href='#'>"+data.stepname+"<a/>");
                        $caseStatus.attr("onclick", "showStatus('"+data.currentidentity.casesn+"');");
                        $("td", row).eq(2).text("").append($caseStatus);

                        var date = data.createdate.substring(0, 4)+"/"+data.createdate.substring(4, 6)+"/"+data.createdate.substring(6,8);
                        var time = data.createtime.substring(0,2)+":"+data.createtime.substring(2,4)+":"+data.createtime.substring(4,6);

                        var $createdatetime = $("<span>"+date+" "+time+"</span>");
                        $("td", row).eq(3).text("").append($createdatetime);

                        var $editoruidname = $("<span>"+data.editoruid+" "+data.editoruname+"</span>");
                        $("td", row).eq(4).text("").append($editoruidname);
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }

            // 轉至新增頁面
            function onCreate() {
                location.href = "<%= request.getContextPath() %>/B101/Create"
            }
        </script>
    </body>
</html>