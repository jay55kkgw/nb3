<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<% response.setHeader("Content-Security-Policy", "frame-ancestors 'self'"); %>
<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>角色權限維護-設定</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post" >
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADROLENO">角色代碼/部門代碼：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <input type="text" id="ADROLENO" name="ADROLENO" class="form-control" maxlength="20" />&nbsp;
                            <input type="text" id="ADSTAFFNO" name="ADROLENO" class="form-control" maxlength="20" />&nbsp;
                            <input type="button" class="btn btn-info" value="查詢" onclick="onQuery();" />&nbsp;
                            <input id="btnCreate" type="button" value="新增" class="btn btn-info" aria-label="Left Align" onclick="onCreate()" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">角色權限：</label>
                    <div class="col-10">
                        <table id="main">
                            <thead>
                                <tr>
                                    <th>功能代碼</th>
                                    <th>功能名稱</th>
                                    <th>權限</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                        <input id="btnOK" type="button" value="送出" class="btn btn-info" aria-label="Left Align" onclick="onSend()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">新增角色功能</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="model-body" class="modal-body">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- body content end -->


        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            var roleAuths=[];

            // 註冊刪除方法
            Array.prototype.remove = function() {
                for(var i = 0; i < this.length; i++) {
                    var obj = this[i];

                    if( obj.apopid === arguments[0] ) {
                        this.splice(i, 1);      // 目前的位置刪一個
                        break;
                    }
                }
                onRoleBind();
            };
            $(document).ready(function () {
                $("#btnCreate").hide();
                onRoleBind();
            }); //ready

            // 顯示新增權限 Dialog
            function onCreate() {
                var url = "<%= request.getContextPath() %>/B101/RoleAuth";
                lockScreen();
                $.post(url, {}, function (data, status) {
                    unlockScreen();
                    if (status == "success") {
                        //alert(data);
                        var htmlContent = data.replace(/\r\n/g, "");
                        $("#model-body").html(htmlContent);
                        $('#exampleModal').modal({
                        });
                    }
                });
            }

            // RoleAuth Dialog callback 方法
            function roleAuthCallback() {
                var newAuth = getRoleAuth();

                if ( newAuth.apopid == "" || newAuth.apopid.substring(0, 4) == "MENU") {
                    alert("請選擇功能代碼");
                    return;
                }

                if ( newAuth.isquery == "0" && newAuth.isexec == "0" && newAuth.isedit == "0" ) {
                    alert("請勾選權限");
                    return;
                }
                
                var rebind = false;
                var result = $.grep(roleAuths, function(e){ return e.apopid == newAuth.apopid; });
                if ( result.length > 0 ) {
                    result[0].isquery = newAuth.isquery;
                    result[0].isexec = newAuth.isexec;
                    result[0].isedit = newAuth.isedit;
                } else {
                    roleAuths.push(newAuth);
                }
                onRoleBind();
                alert("已設定["+newAuth.apopid+"]權限，請記得按送出按鈕，等候放行");
            }

            function onRoleBind() {
                $("#main").dataTable({
                    "responsive": true,
                    "bPaginate": false,
                    "bFilter": false,
                    "bDestroy": true,
                    "bSort": false,
                    "aaData": roleAuths,
                    "aoColumns": [
                        { "mDataProp": "apopid" },
                        { "mDataProp": "apopname" },
                        { "mDataProp": "adroleno" },
                        { "mDataProp": "adroleno" }
                    ],
                    "createdRow": function (row, data, index) {
                        // 2：查詢，執行，放行
                        var $query = $("<input type='checkbox' disabled='disabled' />");
                        $query.attr("value", data.isquery);
                        if ( data.isquery == "1" )
                            $query.attr("checked", "checked");
                        $("td", row).eq(2).text("").append($query).append($("<span>&nbsp;查詢&nbsp;&nbsp;</span>"));

                        var $edit = $("<input type='checkbox' disabled='disabled' />");
                        $edit.attr("value", data.isedit);
                        if ( data.isedit == "1" )
                            $edit.attr("checked", "checked");
                        $("td", row).eq(2).append($edit).append($("<span>&nbsp;執行&nbsp;&nbsp;</span>"));

                        var $exec = $("<input type='checkbox' disabled='disabled' />");
                        $exec.attr("value", data.isexec);
                        if ( data.isexec == "1" )
                            $exec.attr("checked", "checked");
                        $("td", row).eq(2).append($exec).append($("<span>&nbsp;放行&nbsp;&nbsp;</span>"));

                        var $deleteBtn =  $("<input type='button' class='btn btn-danger' value='刪除' />");
                        $deleteBtn.attr("onclick", "roleAuths.remove('"+data.apopid+"')");

                        $("td", row).eq(3).text("").append($deleteBtn);
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
            }

            var editRoleId="";

            function onQuery() {
                editRoleId = $("#ADROLENO").val();
                if ( editRoleId == "" ) {
                    alert("請輸入入角色代碼");
                    return;
                }
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B101/QueryRole",
                    type: "POST",
                    data: {
                        "draw": 1, 
                        "columns[0][data]": "apopid",
                        "order[0][column]": "0",
                        "order[0][dir]": "asc",
                        "start": "0",
                        "length": "100", 
                        "ADROLENO": editRoleId,
                        "APOPID": "",
                        "ADSTAFFNO": $("#ADSTAFFNO").val()
                    },
                    success: function (data) {
                        unlockScreen();
                        roleAuths = data.data;
                        $("#btnCreate").show();
                        onRoleBind();
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                }); 
            }

            function onSend() {
                $("span.error").remove();
                var roleid = $("#ADROLENO").val();
                if ( roleid == "" ) {
                    alert("請輸入入角色代碼");
                    return;
                }
                if ( roleid != editRoleId ) {
                    alert("原本查詢的角色是["+editRoleId+"]，變更角色需重新按查詢按鈕");
                    return;
                }
                
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B101/Send",
                    type: "POST",
                    data: {
                        "ADROLENO": roleid,
                        "ADSTAFFNO": $("#ADSTAFFNO").val(),
                        "roleAuths": JSON.stringify(roleAuths)
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("角色權限設定送審成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B101";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onExit() {
                location.href="<%= request.getContextPath() %>/B101/Index";
            }
        </script>
    </body>
</html>
