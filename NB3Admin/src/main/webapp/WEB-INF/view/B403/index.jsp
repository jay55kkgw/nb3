<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>外匯交易失敗人工重送處理</h2>
        <form id="B403" name="B403" class="container main-content-block p-4" action="<%= request.getContextPath() %>/B403/Query" method="post">
            <div class="form-group row">
                <label class="col-4 control-label text-right">身分證/營利事業統一編號：</label>
                <div class="col-4">
                    <input type="text" id="UserId" name="UserId" class="form-control upper" maxlength="10" />
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-4 col-8">
                    <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                    <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQueryFC()" />
                </div>
            </div>
        </form>
    </div>
    <c:if test="${__LOGIN_ROLES__=='DC'}">
	    <div style="padding: 0 0 0 30">
	    	<input type='button' value='多筆重送' class='btn btn-default' onclick='onResendChecked();' />
	    </div>
    </c:if>
    <table id="mainFC" class="display p-4 transparent" cellspacing="0" style="width:97%">
        <thead>
            <tr>
                <th style="width: 8%"><input type="checkbox" id="checkAll" onclick="onChecked(this);">預約編號</th>
                <th style="width: 8%">轉帳日期</th>
                <th style="width: 10%">身分證/<br>營利事業統一編號</th>
                <th style="width: 8%">轉出帳號</th>
                <th style="text-align: right;width: 8%">轉出金額</th>
                <th style="width: 7%">銀行名稱<br>轉入帳號</th>
                <th style="text-align: right;width: 8%">轉入金額</th>
                <th style="width: 5%">匯率</th>
                <th style="text-align: right;width: 6%">手續費<br>郵電費</th>
                <th style="text-align: right;width: 5%">國外<br>費用</th>
                <th style="width: 8%">交易類別</th>
                <th style="width: 7%">轉帳結果</th>
                <th style="width: 8%">重送</th>
            </tr>
        </thead>
        <tbody id="mainBody">
        </tbody>
    </table>
    <h6>可人工重送錯誤代碼包含：</h6>
	<c:forEach items="${txs}" var="tx">
		<span class="badge badge-info">${tx.ADMCODE}：${tx.ADMSGIN}</span>
	</c:forEach>
	<br /><br />
	
    <!-- Modal -->
    <div class="modal fade bd-example-modal-sm" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">轉帳結果</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="model-body" class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <script>
        var jqData=[];
        var role="${__LOGIN_ROLES__}";
        
        $(document).ready(function () {
            onQueryFC();
        }); //ready

        function onChecked(obj) {
            var isChecked = obj.checked;
            
            $("#mainBody input[type='checkbox']").prop('checked', isChecked);
        }
        
        function onDataBind() {
            $("#mainFC").dataTable({
                "responsive": true,
                "processing": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "paging": true,
                "bLengthChange": false, 
                "aaData": jqData,
                "columnDefs": [
                	    { targets: 0, orderable: false },
                        { targets: 4, className: 'dt-body-right' },
                        { targets: 6, className: 'dt-body-right' },
                        { targets: 7, className: 'dt-body-right' },
                        { targets: 9, className: 'dt-body-right' },
                        { targets: 10, orderable: false }
                        ], 
                "order": [[2, "asc"]],               
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "fxschpaydataidentity.fxschno" },
                    { "data": "fxschpaydataidentity.fxschtxdate" },
                    { "data": "fxschpaydataidentity.fxuserid" }, 
                    { "data": "fxwdac" },
                    { "data": "fxwdcurr" },
                    { "data": "fxsvbh" },
                    { "data": "fxsvcurr" },
                    { "data": "fxexrate" },
                    { "data": "fxefeeccy" },
                    { "data": "fxefeeccy" },
                    { "data": "adopid" },
                    { "data": "fxtxstatus" },
                    { "data": "fxtxstatus" },
                ],
                "createdRow": function (row, data, index) {
                	var $fxschno = $("<input type='checkbox' />");
					$fxschno.attr("value", data.fxschpaydataidentity.fxschno);
					$fxschno.attr("fxschtxdate", data.fxschpaydataidentity.fxschtxdate);
					$fxschno.attr("fxuserid", data.fxschpaydataidentity.fxuserid);
					$("td", row).eq(0).text("").append($fxschno).append(data.fxschpaydataidentity.fxschno);
					
                    var $fxtxdate = data.fxtxdate;
                    var $date = "";
                    if ($fxtxdate.trim().length > 0)
                        $date = $fxtxdate.substr(0, 4) + "/" + $fxtxdate.substr(4, 2) + "/" + $fxtxdate.substr(6, 2);
                    $("td", row).eq(1).text("").append($date);

                    var $data4 = data.fxwdcurr + "<br>" + formatMoney(data.fxwdamt);
                    $("td", row).eq(4).text("").append($data4);

                    var $data5 = data.fxsvbh + "<br>" + data.fxsvac;
                    $("td", row).eq(5).text("").append($data5);

                    var $data6 = data.fxsvcurr + "<br>" + data.fxsvamt;
                    $("td", row).eq(6).text("").append($data6);

                    var $data8 = (data.fxefeeccy==null ? "" : data.fxefeeccy) + "<br>" + formatMoney(data.fxefee) + "<br>" + formatMoney(data.fxtelfee);
                    $("td", row).eq(8).text("").append($data8);

                    var str_OURCHG = data.fxourchg;
                    if (str_OURCHG == null) {
                        str_OURCHG = "";
                    }
                    var $data8 = (data.fxefeeccy==null ? "" : data.fxefeccy) + "<br>" + str_OURCHG;
                    $("td", row).eq(9).text("").append($data8);

                    var $statusWord = "";
                    var $status = "";
                    switch (data.fxtxstatus) {
                        case "0":
                            $statusWord = "成功";
                            break;
                        case "1":
                            $statusWord = "失敗";
                            break;
                        case "2":
                            $statusWord = "處理中";
                            break;
                        default:
                            $statusWord = "未執行";
                            break;
                    }
                    $status = $statusWord + '<br><a href="#" onclick="showMsgCode(\'' + data.fxexcode + '\')">' + data.fxexcode + '</a>';
                    $("td", row).eq(11).text("").append($status);

                    if (role=="DC")
					{
	                    var $resend = $("<input type='button' value='重送' class='btn btn-danger'/>");
	                    $resend.attr("onclick","onDuplicateCheck('"+data.fxschpaydataidentity.fxschno+"','"+data.fxschpaydataidentity.fxschtxdate+"','"+data.fxschpaydataidentity.fxuserid+"')");
						
	                    //重送功能已移去B405
	                    //var $terminate = $("<input type='button' value='終止' class='btn btn-danger' />");
	                    //$terminate.attr("onclick","Terminate('"+data.fxschpaydataidentity.fxschno+"','"+data.fxschpaydataidentity.fxschtxdate+"','"+data.fxschpaydataidentity.fxuserid+"')");

	                    $("td", row).eq(12).text("").append($resend);
					}
                    else
                    {
                    	$("td", row).eq(12).text("").append("");
                    }	
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            return false;
        }

        function onQueryFC() {
                $("span.error").remove();

                var UserId = $("#UserId").val();
                var url = "<%= request.getContextPath() %>/B403/IndexQueryFC";
                lockScreen();
                $.post(url, {
                    "USERID" : $("#UserId").val(),
                }, function (data, status) {
                    unlockScreen();
                    if (status == "success") {
                        //alert(data);
                        jqData = data;
                        onDataBind();
                        $("#checkAll").prop("checked", false);
                    }
                });
            }

        var resendLen;		// 要重送的資料數
		var result;			// 重送結果

	    // 多筆重送
        function onResendChecked() {
			var $items = $("#mainBody input[type='checkbox']:checked");
    	 	if ( $items.length == 0 ) {
				alert('無可重送交易');
				return;
         	}
			var msg = "確定重送預約序號[";
 			$items.each(function(){
				msg += (this.value)+",";
	 		}); 
   		 	msg = msg.substring(0, msg.length-1);
			msg += "]-預約轉帳日交易?";
       	 	if ( confirm(msg) ) {
       	 		lockScreen();
       	 		result = "";
           	 	resendLen = $items.length;
       		 	$items.each(function(){
           		 	var fxschno = this.value;
           		 	var fxschtxdate = $(this).attr("fxschtxdate");
           		 	var fxuserid = $(this).attr("fxuserid");
           		 
                    $.ajax({
                        url: "<%= request.getContextPath() %>/B403/Resend",
                        type: "POST",
                        async: true, 
                        data: {
                            "fxschno": fxschno,
                            "fxschtxdate": fxschtxdate,
                            "fxuserid": fxuserid
                        },
                        success: function (res) {
                            resendLen--;
                            if (res.status == "" ) {
                                result += "預約序號["+fxschno+"]:成功\r\n\r\n";
                            } else {
                                result += "預約序號["+fxschno+"]:失敗-"+res.status+"\r\n\r\n";
                            }
                            if ( resendLen==0 ) {
                                unlockScreen();
								alert(result);
								onQueryFC();
                            }
                        },
                        error: function (err) {
                            resendLen--;
                            result += "預約序號["+fxschno+"]:失敗-"+err.statusText+"\r\n\r\n";
                            if ( resend==0 ) {
								alert(result);
	                            unlockScreen();
	                            onQueryFC();
                            }
                        }
                    });
          		 }); 
          	 }
        }

        // 單筆重送
        function onResend(fxschno, fxschtxdate, fxuserid) {
            var msg = "確定重送預約批號["+fxschno+"]-預約轉帳日["+fxschtxdate+"]交易?";
            if ( confirm(msg)) {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B403/Resend",
                    type: "POST",
                    data: {
                        "fxschno": fxschno,
                        "fxschtxdate": fxschtxdate,
                        "fxuserid": fxuserid
                    },
                    success: function (res) {
                        unlockScreen();
                        if (res.status == "" ) {
                            alert("重送成功");
                            onQueryFC();
                        } else {
                            alert(res.status);
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
        }

     	// 單筆重送-重複交易檢核
        function onDuplicateCheck(fxschno, fxschtxdate, fxuserid) {
            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/B403/DuplicateCheck",
                type: "POST",
                data: {
                    "fxschno": fxschno,
                    "fxschtxdate": fxschtxdate,
                    "fxuserid": fxuserid
                },
                success: function (res) {
                	unlockScreen();
                    if (res.counts > 0 ) {
                        var time=res.time+"";
                        if ( time.length < 6 ) {
							time="0"+time;
                        }
                        alert("當日有重覆交易(同轉出/轉入/幣別/金額), 最近一筆交易時間為 "+time.substr(0, 2)+":"+time.substr(2, 2)+":"+time.substr(4, 2));
                    } else if ( res.counts == -1 ) {
                    	alert("檢查當日重覆交易(同轉出/轉入/幣別/金額)發生錯誤");
                    }
                    onResend(fxschno, fxschtxdate, fxuserid);
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }
        
        //開啟處理狀態 Dialog, Controller 會回傳 PartialView
        function showMsgCode(id) {
            var url = "<%= request.getContextPath() %>/B403/MsgDialog/" + id;
            lockScreen();
            $.post(url, {}, function (data, status) {
                unlockScreen();
                if (status == "success") {
                    //alert(data);
                    var htmlContent = data.replace(/\r\n/g, "");
                    $("#model-body").html(htmlContent);
                    $('#exampleModal').modal({

                    });
                }
            });
        }
    </script>
</body>

</html>