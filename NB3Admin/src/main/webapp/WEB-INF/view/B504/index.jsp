<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">     
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>行銷活動維護</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >行銷類別：</label>
                    <div class="col-10">
                    	<div class="form-inline">
	                        <select id="ADTYPE" class="form-control">
	                            <option value="1" selected="selected">網銀促銷活動</option>
	<!--                        
	                            <option value="2">信用卡優惠活動</option>
	                            <option value="3">行銷活動訊息</option>
	
	                            <option value="4">文字訊息推播</option>
	 -->
	                         </select>
                         </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >上架起～迄日：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <input type="text" id="DateFrom" autocomplete="off" name="DateFrom" class="form-control" style = "width:160px" maxlength="10" value="${StartDt}" />～
                            <input type="text" id="DateTo" autocomplete="off" name="DateTo" class="form-control" style = "width:160px" maxlength="10" value="${EndDt}" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >行銷標題：</label>
                    <div class="col-10">
                    	<div class="form-inline">
                        	<input type="text" id="MSGTITLE" name="MSGTITLE" class="form-control" maxlength="85" />
                        </div>
                    </div>
                </div>
                 <div class="form-group row">
                    <label class="col-2 control-label text-right" >案件狀態：</label>
                    <div class="col-2">
                       	<select id="FlowFinished" class="form-control">
                       		<option value="">=== 所有 ==</option>
                       		<option value="Y" selected="selected">已結案(已上架)</option>
                       		<option value="N">未結案</option>
                       	</select>                    
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnClear" type="reset" value="重新輸入" class="btn btn-dark" aria-label="Left Align" />&nbsp;
                        <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnCreate" type="button" value="新增" class="btn btn-info" aria-label="Left Align" onclick="onCreate()" />
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th>序號</th>
                    <th>行銷類別</th>
                    <th>行銷標題</th>
                    <th>行銷上架起日</th>
                    <th>行銷上架迄日</th>
                    <th>部門代碼</th>
                    <th>狀態</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <span>如案件已結案，部門代碼／姓名請按已結案連結做查詢</span>

        <jsp:include page="../include/caseStatus.jsp"></jsp:include>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
        <script>
            var admAdsTmp=[];

            $(document).ready(function () {
                $("#main").hide();
                $('#DateFrom').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false,
                    timepicker: false
                });
                $('#DateTo').datetimepicker({
                    lang: 'zh-TW',
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false,
                    timepicker: false
                });
                onQuery();
            }); //ready
        
            // 新增
            function onCreate() {
                location.href = "<%= request.getContextPath() %>/B504/Create";
            }

            function onDataBind() {
                $("#main").dataTable({
                    "responsive": true,
                    "pageLength": 10, 
                    "paging": true,
                    "bFilter": false,
                    "bDestroy": true,
                    "bSort": false, 
                    "bLengthChange": false, 
                    "aaData": admAdsTmp,
                    "aoColumns": [
                        { "data": "mtid" },
                        { "data": "mtadtype" },
                        { "data": "mtadhlk" },
                        { "data": "mtsdate" },
//                        { "data": "mtstime" },
                        { "data": "mtedate" },
//                        { "data": "mtetime" },
                        { "data": "oid"},
                        { "data": "stepname" }
                    ],
                    "createdRow": function (row, data, index) {
                        // ID
                        var $id = $("<a>"+data.mtid+"<a/>");
                        $id.attr("href", "<%= request.getContextPath() %>/B504/Query/"+data.mtid);
                        $("td", row).eq(0).text("").append($id);

                        var adname = "";
                        if(data.mtadtype == "1")
                       	{
                        	adname="網銀促銷活動";
                       	}
                        else if(data.mtadtype == "2")
                       	{
                        	adname="信用卡優惠活動";
                       	}
                        else if(data.mtadtype == "3")
                       	{
                        	adname="行銷活動訊息";
                       	}
                        else if(data.mtadtype == "4")
                       	{
                        	adname="文字訊息推播";
                       	}
                        else
                        	adname=data.mtadtype;

                        $("td", row).eq(1).text(adname);
                        
                        $("td", row).eq(2).text(data.mtadhlk);

                        // 處理起日                
                        var startDt = data.mtsdate.substring(0,4)+"-"+data.mtsdate.substring(4,6)+"-"+data.mtsdate.substring(6,8);
                        startDt += " "+data.mtstime.substring(0,2)+":"+data.mtstime.substring(2,4);
                        
                        $("td", row).eq(3).text(startDt);

                        var endDt = data.mtedate.substring(0,4)+"-"+data.mtedate.substring(4,6)+"-"+data.mtedate.substring(6,8);
                        endDt += " "+data.mtetime.substring(0,2)+":"+data.mtetime.substring(2,4);
                        
                        $("td", row).eq(4).text(endDt);


                        $("td", row).eq(5).text("").append(data.oid);

                        var $caseStatus;
                        if ( data.stepname == "" ) {
                            $caseStatus = $("<a href='#'>已結案<a/>");
                        } else {
                            $caseStatus = $("<a href='#'>"+data.stepname+"<a/>");
                        }
                        $caseStatus.attr("onclick", "showStatus('"+data.mtid+"');");
                        $("td", row).eq(6).text("").append($caseStatus);

                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }

            function onQuery() {
                var sisvalid = Date.parse($("#DateFrom").val());
                var eisvalid = Date.parse($("#DateTo").val());
                
                if(isNaN(sisvalid) || isNaN(eisvalid) ) { 
                    alert("請輸入合法的起日和迄日");
                    return;
                }

                if ( sisvalid > eisvalid ) {
                    alert("迄日必需大於起日");
                    return;
                }

                var url = "<%= request.getContextPath() %>/B504/IndexQuery";
                lockScreen();
                $.post(url, {
                	"ADTYPE" : $("#ADTYPE").val(),
                    "DateFrom" : $("#DateFrom").val(),
                    "DateTo" : $("#DateTo").val(),
                    "TITLE" : $("#MSGTITLE").val(),
                    "FlowFinished": $("#FlowFinished").val()
                }, function (data, status) {
                    unlockScreen();
                    if (status == "success") {
                        //alert(data);
                        admAdsTmp = data;
                        onDataBind();
                    }
                });
            }

            
        </script>
    </body>
</html>
