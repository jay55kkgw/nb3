<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>
    
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>行銷活動維護-覆核</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
	           	 	<label class="col-2 control-label text-right">行銷類別：</label>
	            	<c:choose>
	           			<c:when test="${Data.MTADTYPE=='1'}">
			                <label class="col-10 control-label">網銀促銷活動</label>
		               	</c:when>
		               	<c:when test="${Data.MTADTYPE=='2'}">
			                <label class="col-10 control-label">信用卡優惠活動</label>
		               	</c:when>
		               	<c:when test="${Data.MTADTYPE=='3'}">
			                <label class="col-10 control-label">行銷活動訊息</label>
		               	</c:when>
		               	<c:when test="${Data.MTADTYPE=='4'}">
			                <label class="col-10 control-label">文字訊息推播</label>
		               	</c:when>
		            </c:choose>
	            </div>
                <div class="form-group row">
	           	 	<label class="col-2 control-label text-right">是否推播：</label>
		            	<c:choose>
		           			<c:when test="${Data.MTNF=='0'}">
				                <label class="col-10 control-label">否</label>
			               	</c:when>
			               	<c:when test="${Data.MTNF=='1'}">
				                <label class="col-10 control-label">是</label>
			               	</c:when>
			            </c:choose>
	            </div>
	            <div class="form-group row">
                    <label class="col-2 control-label text-right" >上架日期：</label>
                    <label class="col-4 control-label">${StartDateTime}</label>
                    <label class="col-2 control-label text-right" >下架日期：</label>
                    <label class="col-4 control-label">${EndDateTime}</label>
                </div>
	            
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >權重：</label>
                    <label class="col-10 control-label">${Data.MTWEIGHT}</label>
                </div>
              	<div class="form-group row">
                    <label class="col-2 control-label text-right" >行銷標題：</label>
                    <label class="col-10 control-label">${Data.MTADHLK}</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >行銷內容：</label>
					<label class="col-10 control-label"><textarea  id="TxtCont" name="TxtCont" cols="40" rows="6" disabled="disabled" >${Data.MTADCON}</textarea></label>
                </div>
                <div class="form-group row">
                    <label id="lblFileL" class="col-2 control-label text-right" >行銷圖片：</label>
                    <label class="col-10 control-label"><img src="${Data.MTPICDATABASE64}" height="100" width="100" ></img></label>
                </div>
                <div class="form-group row">
                    <label id="lblFileL" class="col-2 control-label text-right" >跑馬燈圖片：</label>
                    <label class="col-10 control-label"><img src="${Data.MTMARQUEEPICBASE64}" height="100" width="100" ></img></label>
                </div>
              	<div class="form-group row">
                    <label class="col-2 control-label text-right" >行銷活動按鈕名稱：</label>
                    <label class="col-10 control-label">${Data.MTBTNNAME}</label>
                </div>
              	<div class="form-group row">
                    <label class="col-2 control-label text-right" >行銷活動超連結：</label>
                    <label class="col-10 control-label">${Data.MTADDRESS}</label>
                </div>
              	<div class="form-group row">
                    <label class="col-2 control-label text-right" >圖片位置：</label>
	            	<c:choose>
	           			<c:when test="${Data.MTPICTYPE=='1'}">
			                <label class="col-10 control-label">上方文字下方圖片</label>
		               	</c:when>
		               	<c:when test="${Data.MTPICTYPE=='2'}">
			                <label class="col-10 control-label">上方圖片下方文字</label>
		               	</c:when>
		            </c:choose>
                </div>
              	<div class="form-group row">
                    <label class="col-2 control-label text-right" >跑馬燈顯示：</label>
	            	<c:choose>
	           			<c:when test="${Data.MTRUNSHOW=='Y'}">
			                <label class="col-10 control-label">顯示</label>
		               	</c:when>
		               	<c:when test="${Data.MTRUNSHOW=='N'}">
			                <label class="col-10 control-label">不顯示</label>
		               	</c:when>
		            </c:choose>
                </div>
              	<div class="form-group row">
                    <label class="col-2 control-label text-right" >是否為廣告行銷類訊息：</label>
                    <div class="col-9">
	                    <div class="form-inline">
			            	<c:choose>
			           			<c:when test="${Data.MTCF=='Y'}">
					                <label class="col-1 control-label">是</label>
				               	</c:when>
				               	<c:when test="${Data.MTCF=='N'}">
					                <label class="col-1 control-label">否</label>
				               	</c:when>
				            </c:choose>
				            &nbsp;<span>（若為廣告行銷類訊息，須依本行「從事廣告業務招攬及營業促銷活動辦法」簽核完成）</span>
			            </div>
		            </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >退回原因：</label>
                    <div class="col-10">
                        <input type="text" id="Comments" name="Comments" class="form-control" maxlength="50" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                        <input id="btnPreview" type="button" class="btn btn-info" value="預覽" onclick="onPreview();" />&nbsp;                        
                        <input id="btnReject" type="button" value="退回" class="btn btn-danger" aria-label="Left Align" onclick="onReject()" />&nbsp;
                        <input id="btnApprove" type="button" value="覆核" class="btn btn-info" aria-label="Left Align" onclick="onApprove()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script>
            $(document).ready(function () {

            }); //ready

            // 回查詢頁
            function onExit() {
                location.href="<%= request.getContextPath() %>/B504";
            }

            // 預覽廣告
            function onPreview() {
                window.open("<%= request.getContextPath() %>/B504/CasePreview/${Data.MTID}");
            }

            // 同意
            function onApprove() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B504/Approve/${Data.MTID}",
                    type: "POST",
                    data: {
                        "StepId":"${Data.STEPID}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("行銷維護覆核成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B504";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            // 退回
            function onReject() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B504/Reject/${Data.MTID}",
                    type: "POST",
                    data: {
                        "StepId":"${Data.STEPID}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("行銷維護退回成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B504";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
        </script>
    </body>
</html>