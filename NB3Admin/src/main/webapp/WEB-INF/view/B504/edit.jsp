<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
    
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <c:if test="${IsReject}">
                <h2>行銷活動維護-退回</h2>
            </c:if>
            <c:if test="${!IsReject}">
                <h2>行銷活動維護</h2>
            </c:if>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >行銷類別：</label>
                    <div class="col-3">
                        <select id="ADTYPE" class="form-control">
                            <option value="1" selected="selected">網銀促銷活動</option>
<!--                        
                            <option value="2">信用卡優惠活動</option>
                            <option value="3">行銷活動訊息</option>
                            <option value="4">文字訊息推播</option>
 -->
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >是否推播：</label>
                    <div class="col-3">
                        <select id="ISPUSH" class="form-control">
                            <option value="1">是</option>
                            <option value="0" selected="selected">否</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >上架日期：</label>
                    <div class="col-3">
                        <div class="form-inline">
                            <input type="text" id="StartDateTime" autocomplete="off" name="StartDateTime" class="form-control" style = "width:160px" maxlength="8" />
                        </div>
                    </div>
                    <label class="col-3 control-label text-right" >下架日期：</label>
                    <div class="col-3">
                        <div class="form-inline">
                            <input type="text" id="EndDateTime" autocomplete="off" name="EndDateTime" class="form-control" style = "width:160px" maxlength="8" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >權重：</label>
                    <div class="col-1">
                        <input type="number" id="MSGWEIGHT" name="MSGWEIGHT" class="form-control" min="0" max="9" oninput="if(value.length>1)value=value.slice(0,1)" value="${Data.MTWEIGHT}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >行銷標題：</label>
                    <div class="col-9">
                        <input type="text" id="MSGTITLE" name="MSGTITLE" class="form-control" maxlength="85" value="${Data.MTADHLK}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >行銷內容：</label>
                    <div class="col-9">
                        <textarea id="MSGCONTENT" name="MSGCONTENT" cols="40" rows="6" maxlength="240" >${Data.MTADCON}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >行銷圖檔：</label>
                    <div class="col-9">
                        <div class="form-inline">
                            <input type="file" id="msgfile" name="msgfile" class="form-control" maxlength="50" accept=".jpg, .gif, .svg" />&nbsp;<span>行動APP：(280*280)</span>
                            <span>(${Data.MTPICADD})</span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <div class="form-inline">
                            <input type="button" class="btn btn-info" value="上傳行銷圖檔" onclick="onMsgUpload();" />&nbsp;
                            <input id="btnMsgPreview" type="button" class="btn btn-info" value="預覽" onclick="onPreviewFile('${Data.MTID}',$('#msgGuid').val(),'MSG');" />
                            <input type="hidden" id="msgGuid" />
                        </div>
                    </div>
                </div>    
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >跑馬燈圖檔：</label>
                    <div class="col-9">
                        <div class="form-inline">
                            <input type="file" id="marqueePic" name="marqueePic" class="form-control" maxlength="50" accept=".jpg, .gif, .svg" />&nbsp;<span>行動APP：(930*510)</span>
                            <span>(${Data.MTMARQUEEPICADD})</span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <div class="form-inline">
                            <input type="button" class="btn btn-info" value="上傳跑馬燈圖檔" onclick="onMarqueeUpload();" />&nbsp;
                            <input id="btnMarqueePreview" type="button" class="btn btn-info" value="預覽" onclick="onPreviewFile('${Data.MTID}',$('#marqueePicGuid').val(),'MAR');" />
                            <input type="hidden" id="marqueePicGuid" />
                        </div>
                    </div>
                </div>    
                
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >行銷活動按鈕名稱：</label>
                    <div class="col-9">
                        <input type="text" id="MSGBTNAME" name="MSGBTNAME" class="form-control" maxlength="30" value="${Data.MTBTNNAME}" />
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-3 control-label text-right" >行銷活動超連結：</label>
                    <div class="col-9">
                        <input type="text" id="MSGURL" name="MSGURL" class="form-control" maxlength="256" value="${Data.MTADDRESS}" />
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-3 control-label text-right" >圖片位置：</label>
                    <div class="col-3">
                        <select id="MSGPICPOS" class="form-control">
                            <option value="1" selected="selected">上方文字下方圖片</option>
                            <option value="2">上方圖片下方文字</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-3 control-label text-right" >跑馬燈顯示：</label>
                    <div class="col-3">
                        <select id="RUNSHOW" class="form-control">
                            <option value="Y">顯示</option>
                            <option value="N" selected="selected">不顯示</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-3 control-label text-right" >是否為廣告行銷類訊息：</label>
                    <div class="col-9">
                    	<div class="form-inline">
	                        <select id="msgCfStand" class="form-control">
	                            <option value="Y">是</option>
	                            <option value="N">否</option>
	                        </select>
	                        &nbsp;<span>（若為廣告行銷類訊息，須依本行「從事廣告業務招攬及營業促銷活動辦法」簽核完成）</span>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                        <c:if test="${IsReject}">
                			<input id="btnCancel" type="button" value="取消送審" class="btn btn-danger" aria-label="Left Align" onclick="onCancel()" />&nbsp;                        
            			</c:if>
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onSave()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        
        <script>
            $(document).ready(function () {
                $('#btnPreviewFile').hide();
                $('#btnUploadFile').attr('disabled', true);
                $('#StartDateTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false
                });
                $('#EndDateTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false
                });
                $("#StartDateTime").val("${StartDateTime}");
                $("#EndDateTime").val("${EndDateTime}");
                // 特店類別
                $("#ADTYPE > [value='${Data.MTADTYPE}']").attr("selected", true);
                // 商店分類
                $("#ISPUSH > [value='${Data.MTNF}']").attr("selected", true);
                // 
                $("#MSGWEIGHT").val("${Data.MTWEIGHT}");
                
                $("#MSGPICPOS").val("${Data.MTPICTYPE}");
                
                $("#RUNSHOW").val("${Data.MTRUNSHOW}");
                
                $("#msgCfStand").val("${Data.MTCF}");


            }); //ready

            // 上傳廣告
            function onMsgUpload() {
                var fileData = new FormData();
                var file = $("#msgfile").get(0);

               	if ( file.files.length == 0 ) {
                	alert("請選擇圖檔");
                    return;
               	} else {
                    fileData.append("multipartFile", file.files[0]);
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B504/UploadFile",
                    type: "POST",
                    contentType: false,  // Not to set any content header
                    processData: false,  // Not to process data
                    data: fileData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("圖檔上傳成功");   
                            $("#msgGuid").val(res.pkey);
                            $("#btnMsgPreview").show();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.responseText);
                    }
                });
            }

            // 回查詢頁
            function onExit() {
                history.back();
            }

            // 預覽廣告
            function onPreview() {
                var imageGuid = $("#msgGuid").val();
                
                if ( imageGuid === "" ) {
                    window.open("<%= request.getContextPath() %>/B504/CasePreview/${Data.MTID}");
                } else {
                    window.open("<%= request.getContextPath() %>/B504/TmpPreview/" + imageGuid + "?Type=M&mtid=${Data.MTID}&t=");
                }
            }

            // 修改
            function onSave() {
                $("span.error").remove();

                var reg =/\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}/;
                
                if ( $("#ADTYPE").val() == "" ) {
                    alert("請輸入行銷類別");
                    return false;
                }
                if ( $("#ISPUSH").val() == "" ) {
                    alert("請輸入是否推播");
                    return false;
                }
                if ( $("#MSGCONTENT").val() == "" ) {
                    alert("請輸入MSGCONTENT");
                    return false;
                }

                if ( $("#MSGTITLE").val() == "" ) {
                    alert("請輸入行銷標題");
                    return false;
                }

                if ( $("#MSGWEIGHT").val() == "" ) {
                    alert("請輸入權重");
                    return false;
                }
                
                if ( $("#msgCfStand").val() == "" ) {
                    alert("請輸入是否為廣告行銷類訊息");
                    return false;
                }
                var sisvalid = Date.parse($("#StartDateTime").val());
                var eisvalid = Date.parse($("#EndDateTime").val());
                
                if(isNaN(sisvalid) || isNaN(eisvalid) ) { 
                    alert("請輸入合法的上架日期和下架日期");
                    return false;
                }

                if ( sisvalid > eisvalid ) {
                    alert("下架日期必需大於上架日期");
                    return false;
                }

                if(!(reg.test($("#StartDateTime").val()) && reg.test($("#EndDateTime").val()))){
                	  alert("請輸入合法的上架日期和下架日期");
                      return false;
                }  

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B504/ReSend",
                    type: "POST",
                    data: {
						"MTID" : "${Data.MTID}",
						"OID" : "${Data.OID}",
                        "StartDateTime": $("#StartDateTime").val(),
                        "EndDateTime": $("#EndDateTime").val(),
                        "MTADTYPE": $("#ADTYPE").val(),
                        "MTWEIGHT": $("#MSGWEIGHT").val(),
                        "MTADHLK": $("#MSGTITLE").val(),
                        "MTADCON": $("#MSGCONTENT").val(),
                        "MTPICTYPE": $("#MSGPICPOS").val(),
                        "MTRUNSHOW": $("#RUNSHOW").val(),
                        "MTBULLETIN": 'N',
                        "msgGuid": $("#msgGuid").val(),
                        "MTBTNNAME": $("#MSGBTNAME").val(),
                        "MTADDRESS": $("#MSGURL").val(),
                        "marqueePicGuid": $("#marqueePicGuid").val() == "" ? "" : $("#marqueePicGuid").val(),
                        "MTNF": $("#ISPUSH").val(),
						"MTCF": $("#msgCfStand").val() == "" ? "" : $("#msgCfStand").val(),
						"STEPID": "${Data.STEPID}",
						"comments": ""
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("行銷維護重新送審成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B504";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onCancel() {
                $("span.error").remove();
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B504/Cancel",
                    type: "POST",
                    data: {
                        "MTID" : "${Data.MTID}",
                        "OID" : "${Data.OID}"
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("行銷維護取消送審成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B504";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
            
         	// 推播圖檔案
            function onMarqueeUpload() {
                var fileData = new FormData();
                var file = $("#marqueePic").get(0);

                if ( file.files.length == 0 ) {
                    alert("請選擇上傳檔案");
                    return;
                } else {
                    fileData.append("multipartFile", file.files[0]);
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B504/UploadFile",
                    type: "POST",
                    contentType: false,  // Not to set any content header
                    processData: false,  // Not to process data
                    data: fileData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("檔案上傳成功");   
                            $("#marqueePicGuid").val(res.pkey);
                            $("#btnMarqueePreview").show();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.responseText);
                    }
                });
            }

         	// 預覽上傳廣告類型-檔案
            function onPreviewFile(fileGuid,tmpguid, type) {
				if ( tmpguid === "" ) {
                    window.open("<%= request.getContextPath() %>/B504/CasePreview/" + fileGuid);
                } else {
                    window.open("<%= request.getContextPath() %>/B504/CasePreview/" + fileGuid + "?Tmpguid=" + tmpguid + "&Type=" + type);
                }
            }

        </script>
    </body>
</html>
