<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>
    </head>
    <body>
        <!-- include master page menu -->

        <!-- body content start -->
        <div class="container" id="main-content">
            <div class="form-group row">
                <label class="col-10 control-label"><img src="${Data.IMGBASE64}" height="150" width="100%" ></img></label>
            </div>
        </div>
        <!-- body content end -->


        <script>
            $(document).ready(function () {
            }); //ready

        </script>
    </body>
</html>