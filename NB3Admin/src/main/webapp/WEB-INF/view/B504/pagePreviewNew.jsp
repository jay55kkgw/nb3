<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>
    </head>
    <body>
        <!-- include master page menu -->

        <!-- body content start -->
        <div class="container" id="main-content">
            <div class="form-group row" >
                <label class="col-2 control-label " >上架起迄日：</label>
                <label class="col-10 control-label">${StartDateTime}～${EndDateTime}</label>
            </div>
            <div class="form-group row" >
                <label class="col-2 control-label " >行銷標題：</label>
                <label class="col-10 control-label">${Data.MTADHLK}</label>
            </div>
            <c:if test="${Data.MTPICTYPE=='1'}">
	            <div class="form-group row" >
	                <label class="col-2 control-label " >行銷內容：</label>
	                <label class="col-10 control-label">${Data.MTADCON}</label>
	            </div>
            </c:if>
            <div class="form-group row">
                <label class="col-10 control-label">
                	<c:if test="${Data.MTPICDATABASE64!=''}">
                		<div style="border-bottom-width:2px;border-bottom-color:black;border-bottom-style:dashed;padding-bottom:20px" padding-bottom=20px>
                			行銷圖檔
                			<br>
                			<img src="${Data.MTPICDATABASE64}" height="150" width="100%" ></img>
                		</div>
                		<br>
                	</c:if>
                	<c:if test="${Data.MTMARQUEEPICBASE64!=''}">
                		<div>
                			跑馬燈圖檔
                			<br>
                			<img src="${Data.MTMARQUEEPICBASE64}" height="150" width="100%" ></img>
                		</div>
                	</c:if>
                </label>
            </div>
            <c:if test="${Data.MTPICTYPE=='2'}">
	            <div class="form-group row" >
	                <label class="col-2 control-label " >行銷內容：</label>
	                <label class="col-10 control-label">${Data.MTADCON}</label>
	            </div>
            </c:if>
            <div class="form-group row" >
                <label class="col-10 control-label"><a class="btn btn-default btn-lg btn-block" href="${Data.MTADDRESS}"> ${Data.MTBTNNAME} </a></label>
            </div>
        </div>
        <!-- body content end -->


        <script>
            $(document).ready(function () {
            }); //ready

        </script>
    </body>
</html>