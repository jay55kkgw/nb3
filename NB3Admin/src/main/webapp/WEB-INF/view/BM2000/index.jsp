<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>APP版本控制</h2>
        <form class="container main-content-block p-4" action="/dummy" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right">手機系統：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <select class="form-control" name="APPOS" id="APPOS">
                            <option value="">---請選擇---</option>
                            <option value="1">IOS</option>
                            <option value="2">Android</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">手機型號：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="APPMODELNO" name="APPMODELNO" class="form-control" style="width:200px"
                            maxlength="50" value="" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">版號：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="VERNO" name="VERNO" class="form-control" style="width:200px"
                            maxlength="10" value="" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">版本日期：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="VERDATE" name="VERDATE" class="form-control" style="width:200px"
                            maxlength="10" value="" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">版本狀態：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <select class="form-control" name="VERSTATUS" id="VERSTATUS">
                            <option value="">---請選擇---</option>
                            <option value="1">可用</option>
                            <option value="2">停用</option>
                            <option value="3">提示換版</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">提示訊息：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="VERMSG" name="VERMSG" class="form-control" style="width:250px"
                            maxlength="150" value="" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">下載網址：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="VERURL" name="VERURL" class="form-control" style="width:250px"
                            maxlength="150" value="" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-2 col-10">
                    <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                    <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align"
                        onclick="onQuery()" />&nbsp;
                        <input id="btnCreate" type="button" value="新增" class="btn btn-info" aria-label="Left Align"
                        onclick="onCreate();" />&nbsp;
                </div>
            </div>
        </form>
    </div>
    <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>手機系統</th>
                <th>手機型號</th>
                <th>版號</th>
                <th>版本日期</th>
                <th>版本狀態</th>
                <th>提示訊息</th>
                <th>下載網址</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
    <script>
	    $(document).ready(function () {
	   	 $('#VERDATE').datetimepicker({
	            lang: 'zh-TW',
	            timepicker: false,
	            format: 'Y/m/d',   // 顯示時分
	            scrollMonth: false
	        });
	       onQuery();
	   }); //ready
	   
        function onCreate() {
                window.location = "<%= request.getContextPath() %>/BM2000/Create";
            }
        
        function onQuery() {
            $("span.error").remove();
            $("#main").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "order": [[3, "asc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/BM2000/Query",
                    "data": {
                        "APPOS": $("#APPOS").val(),
                        "APPMODELNO": $("#APPMODELNO").val(),
                        "VERNO": $("#VERNO").val(),
                        "VERDATE": $("#VERDATE").val(),
                        "VERSTATUS": $("#VERSTATUS").val(),
                        "VERMSG": $("#VERMSG").val(),
                        "VERURL": $("#VERURL").val()
                    },
                    "error": function (msg) {
                        // debugger
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [
                        { "data": "appos" },
                        { "data": "appmodelno" },
                        { "data": "verno" },
                        { "data": "verdate","data": "appid" },
                        { "data": "verstatus" },
                        { "data": "vermsg" },
                        { "data": "verurl" }                         
                    ],
                    "columnDefs": [ {
                        "targets": [0],
                        "orderable": false
                      } ],
                    "createdRow": function (row, data, index) {
                        
                        var $appid = $("<input type='button' class='btn btn-link' />");
                        
                        var $appos = "";
                        switch (data.appos) {
                            case "1":
                                $appos = "IOS";
                                break;
                            case "2":
                                $appos = "Android";
                                break;
                        }
                        $appid.attr("value", $appos);
                        $appid.attr("onclick", "location.href='<%= request.getContextPath() %>/BM2000/Edit/"+data.appid+"'");
                        $("td", row).eq(0).text("").append($appid);
                        
                        var verdatedata = data.verdate;
                        var $verdate = $("<span>"+verdatedata.substr(0, 4) + "/" + verdatedata.substr(4, 2) + "/" + verdatedata.substr(6, 2) +"</span>");
                        $("td", row).eq(3).text("").append($verdate);

                        var $verstatus = "";
                        switch (data.verstatus) {
                            case "1":
                                $verstatus = "可用";
                                break;
                            case "2":
                                $verstatus = "停用";
                                break;
                            case "3":
                                $verstatus = "提示換版";
                                break; 
                        }
                        $("td", row).eq(4).text("").append($verstatus);
                        
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }
    </script>
</body>

</html>