<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!-- 20190603 DannyChou ADD -->

<html>
    <head>
        <c:set var="action" value="${allowEdit==true ? '修改': '查詢'}" />
        <title>台企銀 後台-APP版本控制-${action}</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">  
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">      
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>APP版本控制</h2>
            <form id="APPVER" AdmMsgCode" class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right">手機系統：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <select class="form-control" name="APPOS" id="APPOS">
                                <option value="">---請選擇---</option>
                                <option value="1">IOS</option>
                                <option value="2">Android</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">手機型號：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <input type="text" id="APPMODELNO" name="APPMODELNO" class="form-control" style="width:200px"
                                maxlength="50" value="${Data.APPMODELNO}" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">版號：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <input type="text" id="VERNO" name="VERNO" class="form-control" style="width:200px"
                                maxlength="10" value="${Data.VERNO}" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">版本日期：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <input type="text" id="VERDATE" name="VERDATE" class="form-control" style="width:200px"
                                maxlength="10" value=""  />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">版本狀態：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <select class="form-control" name="VERSTATUS" id="VERSTATUS">
                                <option value="">---請選擇---</option>
	                            <option value="1">可用</option>
	                            <option value="2">停用</option>
	                            <option value="3">提示換版</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">提示訊息：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <input type="text" id="VERMSG" name="VERMSG" class="form-control" style="width:250px"
                                maxlength="150" value="${Data.VERMSG}" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">下載網址：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <input type="text" id="VERURL" name="VERURL" class="form-control" style="width:250px"
                                maxlength="150" value="${Data.VERURL}"  />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnGoback" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onGoBack();" />&nbsp;
                        <c:if test="${allowEdit}">
                            <input id="btnDelete" type="button" value="刪除" class="btn btn-danger" aria-label="Left Align" onclick="onDelete();" />&nbsp;
                            <input id="btnSave" type="button" value="儲存" class="btn btn-info" aria-label="Left Align" onclick="onCreate();" />
                        </c:if>    
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
            	 $('#VERDATE').datetimepicker({
                     lang: 'zh-TW',
                     timepicker: false,
                     format: 'Y/m/d',   // 顯示時分
                     scrollMonth: false
                 });
            	var verdatedata = "${Data.VERDATE}";
                var verdate = verdatedata.substr(0, 4) + "/" + verdatedata.substr(4, 2) + "/" + verdatedata.substr(6, 2);
                $('#VERDATE').val(verdate);
            	$("#APPOS").val("${Data.APPOS}");
            	$("#VERSTATUS").val("${Data.VERSTATUS}");
            }); //ready

            $.fn.serializeObject = function() {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function() {
                    if (o[this.name]) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };

            $.fn.serializeObjectEx = function() {
                var o = {};
                //    var a = this.serializeArray();
                $(this).find('input[type="hidden"], input[type="text"], input[type="password"], input[type="checkbox"]:checked, input[type="radio"]:checked, select, textarea').each(function() {
                    if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
                        var $parent = $(this).parent();
                        var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
                        if ($chb != null) {
                            if ($chb.prop('checked')) return;
                        }
                    }
                    if (this.name === null || this.name === undefined || this.name === '')
                        return;
                    var elemValue = null;
                    if ($(this).is('select'))
                        elemValue = $(this).find('option:selected').val();
                    else elemValue = this.value;
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(elemValue || '');
                    } else {
                        o[this.name] = elemValue || '';
                    }
                });
                return o;
            }

            // 刪除
            function onDelete() {
                var message = confirm("確定刪除手機型號[${Data.APPMODELNO}]？");
                if ( message ) {
                    lockScreen();
                    $.ajax({
                        url: "<%= request.getContextPath() %>/BM2000/Delete/${Data.APPID}",
                        type: "POST",
                        data: {},
                        success: function (data) {
                            unlockScreen();
                            if(data === "0"){
                                alert("刪除成功");
                                location.href="<%= request.getContextPath() %>/BM2000/Index"
                            } else {
                                alert(data);
                            }
                        },
                        error: function (err) {
                            unlockScreen();
                            alert(err.statusText);
                        }
                    });
                }
            }

            function checkEmpty() {
            	 var msg = "";
                 if ( $("#APPOS").val()=="" ) {
                     msg += "手機系統不可為空值.\r\n";
                 }

                 if ( $("#APPMODELNO").val()=="" ) {
                     msg += "手機型號不可為空值.\r\n";
                 }

                 if ( $("#VERNO").val()=="" ) {
                     msg += "版號不可為空值.\r\n";
                 }

                 if ( $("#VERDATE").val()=="" ) {
                     msg += "版本日期不可為空值.\r\n";
                 }

                 if ( $("#VERSTATUS").val()=="" ) {
                     msg += "版本狀態不可為空值.\r\n";
                 }

                 if ( $("#VERMSG").val()=="" ) {
                     msg += "提示訊息不可為空值.\r\n";
                 }

                 if ( $("#VERURL").val()=="" ) {
                     msg += "下載網址不可為空值.\r\n";
                 }

                 return msg;
            }

            // 修改
            function onCreate() {
                var msg = checkEmpty();
                if ( msg != "" ) {
                    alert(msg);
                    return;
                }
                
                var formData = $("#APPVER").serializeObjectEx();
                formData["APPID"] = "${Data.APPID}";

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/BM2000/Edit",
                    type: "POST",
                    data: formData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("修改成功"); 
                            location.href="<%= request.getContextPath() %>/BM2000"
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else {
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                                }
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onGoBack() {
                location.href="<%= request.getContextPath() %>/BM2000"
            }

        </script>
    </body>
</html>