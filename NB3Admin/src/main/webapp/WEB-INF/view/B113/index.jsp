<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台-白名單管理</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>白名單管理</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="">白名單資料檔匯入：</label>
                    <div class="col-9">
                        <div class="form-inline">
                            <c:if test="${allowEdit}">
                                <input type="file" id="file" name="file" class="form-control" />&nbsp;
                                <input type="button" value="範例下載" class="btn btn-info" onclick="onSample();" />&nbsp;
                                <input type="button" value="執行匯入" class="btn btn-info" onclick="onUpLoad();" />
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" for="ADHOLIDAYID">身份證字號/統一編號：</label>
                    <div class="col-3">
                        <input type="text" id="ADUSERID" name="ADUSERID" class="form-control upper" maxlength="10" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input id="btnQuery" type="button" value="查詢" class="btn btn-dark" aria-label="Left Align" onclick="onQuery();" />&nbsp;
                        <c:if test="${allowEdit}">
                            <input id="btnCreate" type="button" value="新增" class="btn btn-info" aria-label="Left Align" onclick="onCreate();" />&nbsp;
                        </c:if>
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th>身份證字號/統一編號</th>
                    <th>動作</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                $("#file").on("change", function(e) { onFileSelect(e); });
                onQuery();
            }); //ready
        
            function onSample() {
                window.location = "<%= request.getContextPath() %>/B113/Sample";
            }

            // 若對 file change 有興趣，可以在以下處撰寫內容
            function onFileSelect(e) {
                var files = e.target.files;
                var fileCount = files.length;
                for ( var i=0; i<fileCount; i++ ) {
                    var file = files[i];
                    console.debug("name="+file.name+" ,size="+file.size);
                }
            }

            function onUpLoad() {
                var fileData = new FormData();

                var file1 = $("#file").get(0);
                if (file1.files.length > 0) {
                    fileData.append("multipartFile", file1.files[0]);
                }

                if (file1.files.length == 0 ) {
                    alert("請選擇檔案");
                    return;
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B113/FileUpload",
                    type: "POST",
                    contentType: false,  // Not to set any content header
                    processData: false,  // Not to process data
                    data: fileData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("檔案匯入成功");   
                            onQuery();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onQuery() {
                $("span.error").remove();

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "order": [[0, "asc"]],
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B113/Query",
                        "data": {
                            "ADUSERID": $("#ADUSERID").val()
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [
                        { "data": "aduserid" },
                        { "data": "aduserid" }
                    ],
                    "createdRow": function (row, data, index) {
                        // 處理userid
                        var key = data.aduserid.trim();

                        if ( "${allowEdit}" == "true" ) {
                            // 處理 Button
                            var $delete = $("<input type='button' value='刪除' class='btn btn-danger' />");
                            $delete.attr("id", "btnDelete_"+key);
                            $delete.attr("onclick","onDelete('"+key+"')");

                            $("td", row).eq(1).text("").append($delete);
                        } else {
                            $("td", row).eq(1).text("");
                        }
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }

            function onCreate() {
                $("span.error").remove();

                var aduserid = $("#ADUSERID").val();

                if ( aduserid==="" ) {
                    alert("身份證字號/統一編號不可為空值");
                    return;
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B113/Create",
                    type: "POST",
                    data: {
                        "ADUSERID": aduserid
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("白名單["+aduserid+"]新增成功");   
                            onQuery();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onDelete(id) {
                var message = confirm("確定刪除白名單[" + id + "]？");
                if (message == true) {
                    lockScreen();
                    $.ajax({
                        url: "<%= request.getContextPath() %>/B113/Delete/"+id,
                        type: "POST",
                        data: {},
                        success: function (data) {
                            unlockScreen();
                            if (data === "0") {
                                alert("白名單[" + id + "]刪除成功");
                                onQuery();
                            } else {
                                alert(data);
                            }
                        },
                        error: function (err) {
                            unlockScreen();
                            alert(err.statusText);
                        }
                    });
                }
            }
        </script>
    </body>
</html>