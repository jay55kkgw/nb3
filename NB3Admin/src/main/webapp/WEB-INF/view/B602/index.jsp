<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <!-- #B602 電郵記錄檔
             條件頁-->
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">
    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">

</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>客戶郵件管理</h2>
        <form class="container main-content-block p-4" action="findranges" name="inquire_mail">
            <div class="form-group row">
                <label class="col-3 control-label text-right" >身分證字號/統一編號：</label>
                <div class="col-2">
                    <input type="text" id="ADUSERID" name="ADUSERID" class="form-control upper" maxlength="11" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-3 control-label text-right" >查詢期間起～迄日：</label>
                <div class="col-9">
                    <div class="form-inline">
                        <input class="form-control date" id="DateFrom" autocomplete="off"  name="DateFrom" />～<input class="form-control date" id="DateTo" autocomplete="off" name="DateTo" />
                    </div>            
                </div>
            </div>
            <div class="form-group row">
                <label class="col-3 control-label text-right" >說明：</label>
                <label class="col-9 control-label">1. 查詢期間起訖限三個月內之資料。</label>
            </div>
            <div class="form-group row">
                <div class="offset-3 col-10">
                    <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                    <input id="btnCreate" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />
                </div>
            </div>
        </form>
        <table id="main" class="display p-8 none transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th>身分證字號<br/>帳號</th>
                    <th>姓名</th>
                    <th>電子郵件</th>
                    <th>寄送狀態</th>
                    <th>寄送種類</th>
                    <th>寄送時間</th>
                    <th>動作</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content none">
                <div class="modal-header">
                    <h5 class="modal-title" id="model-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="model-body" class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>

    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <!--datepicker 2 js-->
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>

    <script>
        $(document).ready(function () {
            //輸入當地時間
            $('.date').datetimepicker({
                lang: 'zh-TW',
                timepicker: true,
                format: 'Y/m/d H:m:s',   // 顯示時分
                scrollMonth: false,
                hours12:false,
                maxTime: false
            });        
            
            $("#main").hide();
        }); //ready

        function onQuery() {
            $("span.error").remove();
            if ($("#main").hasClass("none"))
                $("#main").removeClass("none");

            var isError = "";
            if (($("#ADUSERID").val() == "")||($("#ADUSERID").val() == null))
                isError += "請輸入查詢身分證字號\n";
            if (($("#DateFrom").val() == "")||($("#DateFrom").val() == null))
                isError += "請輸入起始日期\n";
            if (($("#DateTo").val() == "")||($("#DateTo").val() == null))
                isError += "請輸入結束日期\n";

            if (isError != "") {
                alert(isError)
                return;
            }

            var tempForm = $('form[name=inquire_mail]').serialize();
            $("#main").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "order": [[0, "asc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/B602/Findranges",
                    "data":
                    {
                        "ADUSERID": $("#ADUSERID").val(),
                        "ADSENDSTATUS": "",
                        "ADQSDATE": $("#DateFrom").val(),
                        "ADQEDATE": $("#DateTo").val(),
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "adacno" },
                    { "data": "adusername" },
                    { "data": "admailacno" },
                    { "data": "adsendstatus"},
                    { "data": "adsendtype" },
                    { "data": "adsendtime" },
                    { "data": "admaillogid" }
                ],
                "createdRow": function (row, data, index) {
                    var $idno = $("<span>"+data.aduserid+"</span>");
                    var $acno = $("<span>"+data.adacno+"</span>");
                    var $br1 = $("<br />");

                    $("td", row).eq(0).text("").append($idno).append($br1).append($acno);

                    $("td", row).eq(3).text(sendstatus(data.adsendstatus));
                    $("td", row).eq(4).text(sendtype(data.adsendtype));

                    try {
                        var adsendtime = data.adsendtime;
                        var date = adsendtime.substring(0, 4)+"/"+adsendtime.substring(4, 6)+"/"+adsendtime.substring(6, 8);
                        var $date = $("<span>"+date+"</span>");
                        var time = adsendtime.substring(8, 10)+":"+adsendtime.substring(10, 12)+":"+adsendtime.substring(12, 14);
                        var $time = $("<span>"+time+"</span>");
                        var $br2 = $("<br />");

                        $("td", row).eq(5).text("").append($date).append($br2).append($time);
                    } catch (error) {
                        
                    }
                    

                    var key = data.admaillogid;

                    //處理 Button
                    var $ReadContext = $("<input type='button' value='閱讀信件文本' class='btn btn-primary' />")
                    $ReadContext.attr("onclick", "onReadMailContext('" + key + "')");

                    var $ReSent = $("<input type='button' value='重寄' class='btn btn-danger' />")
                    $ReSent.attr("onclick", "onReSent('" + data.admailacno + "','" + key + "')");

                    var $space1 = $("<span>&nbsp;</span>");
                    var $space2 = $("<span>&nbsp;</span>");
                    $("td", row).eq(6).text("").append($ReadContext).append($space1).append($ReSent);
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            $("#main").show();
            return false;
        }
        function sendstatus(statusid) {
            var status = ""
            switch (statusid) {
                case 'y':
                case 'Y':
                    status = "已寄送";
                    break
                default:
                    status = "";
            }
            return status;
        }

        function sendtype(typeid) {
            var type = ""
            switch (typeid) {
                case '1':
                    type = "基金電子對帳單";
                    break
                case '2':
                    type = "信用卡電子帳單";
                    break
                case '3':
                    type = "台外幣交易訊息通知";
                    break
                case '4':
                    type = "其它類訊息通知";
                    break
                case '5':
                    type = "線上申請服務通知";
                    break
                default:
                    type = "異常";
            }
            return type;
        }

        function onReSent(mailacno, id) {
            var message = confirm("確定重送[" + mailacno + "]？");
            if (message == true) {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B602/ReSentMail/" + id,
                    type: "POST",
                    data: {},
                    success: function (data) {
                        unlockScreen();
                        if (data === "0") {
                            alert("重送[" + mailacno + "]成功");
                            onQuery();
                        } else {
                        	alert("重送[" + mailacno + "]失敗");
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
        }

        function onReadMailContext(id) {
            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/B602/ReadMailContent/" + id,
                type: "POST",
                cache: false,
                async: false,
                statusCode: {
                    404: function () { $("#messege").text("Page not found"); },
                    406: function () { $("#messege").text("data is not defined"); },
                    500: function () { $("#messege").text("internal server error"); }
                },
                success: function (data) {
                    unlockScreen();
                    $("#model-title").text(data.admsubject);
                    $("#model-body").html(data.admailcontent);
                    $('#exampleModal').modal({
                    });
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });

        }
    </script>
</body>

</html>