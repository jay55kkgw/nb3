<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>分行外匯連絡人檔維護管理</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADBRANCHID">分行代碼：</label>
                    <div class="col-4">
                        <input type="text" id="ADBRANCHID" name="ADBRANCHID" class="form-control" maxlength="15" />
                    </div> 
                    <label class="col-2 control-label text-right" for="ADBRANCHNAME">分行名稱：</label>
                    <div class="col-4">
                        <input type="text" id="ADBRANCHNAME" name="ADBRANCHNAME" class="form-control" maxlength="15" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADCONTACTIID">分行聯絡人代號：</label>
                    <div class="col-4">
                        <input type="text" id="ADCONTACTIID" name="ADCONTACTIID" class="form-control" maxlength="10" />
                    </div>
                    <label class="col-2 control-label text-right" for="ADCONTACTNAME">分行聯絡人名稱：</label>
                    <div class="col-4">
                        <input type="text" id="ADCONTACTNAME" name="ADCONTACTNAME" class="form-control" maxlength="10" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADCONTACTEMAIL">分行聯絡人電郵：</label>
                    <div class="col-4">
                        <input type="text" id="ADCONTACTEMAIL" name="ADCONTACTEMAIL" class="form-control" maxlength="50" />
                    </div>
                    <label class="col-2 control-label text-right" for="ADCONTACTTEL">分行聯絡人電話：</label>
                    <div class="col-4">
                        <input type="text" id="ADCONTACTTEL" name="ADCONTACTTEL" class="form-control" maxlength="30" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <c:if test="${allowEdit}">
                            <input id="btnCreate" type="button" value="新增" class="btn btn-primary" aria-label="Left Align" onclick="onCreate()" />
                        </c:if>
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th>分行代碼</th>
                    <th>分行名稱</th>
                    <th>分行聯絡人代號</th>
                    <th>分行聯絡人名稱</th>
                    <th>分行聯絡人電郵</th>
                    <th>分行聯絡人電話</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                onQuery();
            }); //ready
            
            // 新增
            function onCreate() {
                location.href = "<%= request.getContextPath() %>/B905/Create";
            }

            function onQuery() {
                $("span.error").remove();

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "order": [[0, "asc"]],
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B905/Query",
                        "data": {
                            "ADBRANCHID": $("#ADBRANCHID").val(),
                            "ADBRANCHNAME": $("#ADBRANCHNAME").val(),
                            "ADCONTACTIID": $("#ADCONTACTIID").val(),
                            "ADCONTACTNAME": $("#ADCONTACTNAME").val(),
                            "ADCONTACTEMAIL": $("#ADCONTACTEMAIL").val(),
                            "ADCONTACTTEL": $("#ADCONTACTTEL").val()
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [ 
                        { "data": "adbranchid"   },
                        { "data": "adbranchname" },
                        { "data": "adcontactiid" },
                        { "data": "adcontactname" },
                        { "data": "adcontactemail" },
                        { "data": "adcontacttel" }
                    ],
                    "createdRow": function (row, data, index) {
                        // ID
                        var $id = $("<input type='button' class='btn btn-link' />");
                        $id.attr("value", data.adbranchid);
                        $id.attr("onclick", "location.href='<%= request.getContextPath() %>/B905/Edit/"+data.adbhcontid+"'");
                        $("td", row).eq(0).text("").append($id);
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }
        </script>
    </body>
</html>