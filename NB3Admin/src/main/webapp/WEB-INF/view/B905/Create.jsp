<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台-分行外匯連絡人檔維護管理-新增</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>分行外匯連絡人檔維護管理-新增</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADBRANCHID">分行代碼：</label>
                    <div class="col-4">
                        <input type="text" id="ADBRANCHID" name="ADBRANCHID" class="form-control" maxlength="15" />
                    </div>
                    <label class="col-2 control-label text-right" for="ADBRANCHNAME" >分行名稱(繁中)：</label>
                    <div class="col-4">
                        <input type="text" id="ADBRANCHNAME" name="ADBRANCHNAME" class="form-control" maxlength="45" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADBRANCHSNAME" >分行名稱(簡中)：</label>
                    <div class="col-4">
                        <input type="text" id="ADBRANCHSNAME" name="ADBRANCHSNAME" class="form-control" maxlength="45" />
                    </div>
                    <label class="col-2 control-label text-right" for="ADBRANENGNAME" >分行名稱(英文)：</label>
                    <div class="col-4">
                        <input type="text" id="ADBRANENGNAME" name="ADBRANENGNAME" class="form-control" maxlength="150" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADCONTACTIID" >分行聯絡人代號：</label>
                    <div class="col-4">
                        <input type="text" id="ADCONTACTIID" name="ADCONTACTIID" class="form-control" maxlength="10" />
                    </div>
                    <label class="col-2 control-label text-right" for="ADCONTACTNAME" >分行聯絡人名稱：</label>
                    <div class="col-4">
                        <input type="text" id="ADCONTACTNAME" name="ADCONTACTNAME" class="form-control" maxlength="150" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADCONTACTEMAIL" >分行聯絡人電郵：</label>
                    <div class="col-4">
                        <input type="text" id="ADCONTACTEMAIL" name="ADCONTACTEMAIL" class="form-control" maxlength="50" />
                    </div>
                    <label class="col-2 control-label text-right" for="ADCONTACTTEL" >分行聯絡人電話：</label>
                    <div class="col-4">
                        <input type="text" id="ADCONTACTTEL" name="ADCONTACTTEL" class="form-control" maxlength="30" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnGoback" type="button" value="回上頁" class="btn btn-dark" aria-label="Left Align" onclick="onGoBack();" />&nbsp;
                        <input id="btnSave" type="button" value="儲存" class="btn btn-info" aria-label="Left Align" onclick="onCreate();" />&nbsp;
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script>
            $(document).ready(function () {
                
            }); //ready

            function checkEmpty() {
                var msg = "";
                if ( $("#ADBRANCHID").val()=="" ) {
                    msg += "分行代碼不可為空值.\r\n";
                }
                if ( $("#ADBRANCHNAME").val()=="" ) {
                    msg += "分行名稱(繁中)不可為空值.\r\n";
                }

                if ( $("#ADBRANENGNAME").val()=="" ) {
                    msg += "分行名稱(英文)不可為空值.\r\n";
                }

                if ( $("#ADBRANCHSNAME").val()=="" ) {
                    msg += "分行名稱(簡中)不可為空值.\r\n";
                }

                return msg;
            }

            // 新增
            function onCreate() {
                $("span.error").remove();
                var ADBRANCHID =  $("#ADBRANCHID").val();
                var ADBRANCHNAME =$("#ADBRANCHNAME").val();
                var ADBRANENGNAME =$("#ADBRANENGNAME").val();
                var ADBRANCHSNAME =$("#ADBRANCHSNAME").val();
                var ADCONTACTIID = $("#ADCONTACTIID").val();
                var ADCONTACTNAME = $("#ADCONTACTNAME").val();
                var ADCONTACTEMAIL = $("#ADCONTACTEMAIL").val();
                var ADCONTACTTEL = $("#ADCONTACTTEL").val();
                var msg = checkEmpty();
                if ( msg != "" ) {
                    alert(msg);
                    return;
                }
                
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B905/Create",
                    type: "POST",
                    data: {
                        "ADBRANCHID": ADBRANCHID,
                        "ADBRANCHNAME": ADBRANCHNAME,
                        "ADBRANENGNAME": ADBRANENGNAME,
                        "ADBRANCHSNAME": ADBRANCHSNAME,
                        "ADCONTACTIID": ADCONTACTIID,
                        "ADCONTACTNAME": ADCONTACTNAME,
                        "ADCONTACTEMAIL":ADCONTACTEMAIL,
                        "ADCONTACTTEL": ADCONTACTTEL
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("分行代碼["+ADBRANCHID+"]新增成功");   
                            location.href="<%= request.getContextPath() %>/B905";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onGoBack() {
                location.href="<%= request.getContextPath() %>/B905"
            }

        </script>
    </body>
</html>