<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">   
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>匯款用途選單維護</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADRMTTYPE" >匯款用途性質：</label>
                    <div class="col-4">
                        <select id="ADRMTTYPE" name="ADRMTTYPE" class="form-control" onchange="onADRMTTYPEChange(this);">
                            <option value="">--全部--</option>
                            <option value="1">匯出</option>
                            <option value="2">匯入</option>
                        </select>
                    </div>
                    <label class="col-2 control-label text-right" for="ADRMTID" >匯款用途編號：</label>
                    <div class="col-4">
                        <input type="text" id="ADRMTID" name="ADRMTID" class="form-control" maxlength="3" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADLKINDID" onchange="" >大分類編號：</label>
                    <div class="col-4">
                        <select id="ADLKINDID" name="ADLKINDID" class="form-control" onchange="onLKindChange(this);">
                            <option value="">--全部--</option>
                        </select>
                    </div>
                    <label class="col-2 control-label text-right" for="ADMKINDID">中分類編號：</label>
                    <div class="col-4">
                        <select id="ADMKINDID" name="ADMKINDID" class="form-control">
                            <option value="">--全部--</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <c:if test="${allowEdit}">
                            <input id="btnCreate" type="button" value="新增" class="btn btn-primary" aria-label="Left Align" onclick="onCreate()" />
                        </c:if>
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th>序號</th>
                    <th>資料時間</th>
                    <th>匯款用途性質</th>
                    <th>匯款用途編號</th>
                    <th>大分類項目</th>
                    <th>大分類說明</th>
                    <th>中分類項目</th>
                    <th>中分類說明</th>
                    <th>匯款用途項目</th>
                    <th>匯款用途說明</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

        <jsp:include page="../include/caseStatus.jsp"></jsp:include>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
        <script>
            $(document).ready(function () {
                $("#main").hide();
                //onQuery();
            }); //ready

            // 新增
            function onCreate() {
                location.href = "<%= request.getContextPath() %>/B109/Create";
            }

            // AJAX for 大分類下拉選單
            function onADRMTTYPEChange(obj) {
                var rmtType = $(obj).val();
                if ( rmtType == "" ) {
                    $("#ADLKINDID option").remove();
                    $("#ADLKINDID").append($("<option value=''>--全部--</option>"));

                    $("#ADMKINDID option").remove();
                    $("#ADMKINDID").append($("<option value=''>--全部--</option>"));
                } else {
                    $.ajax({
                        url: "<%= request.getContextPath() %>/B109/LKindQuery",
                        type: "POST",
                        data: { "ADRMTTYPE": $("#ADRMTTYPE").val() },
                        cache: false,
                        async: false,   // 要用同步, 不然會有問題
                        success: function (data) {
                            $("#ADLKINDID option").remove();

                            var i;
                            $("#ADLKINDID").append($("<option value=''>--全部--</option>"));
                            for (i = 0; i < data.length; i++) {
                                $("#ADLKINDID").append($("<option></option>").attr("value", data[i].adlkindid).text(data[i].adrmtitem));
                            }
                        },
                        error: function (err) {
                            alert(err.statusText);
                        }
                    });
                }
            }

            // AJAX for 中分類下拉選單
            function onLKindChange(obj) {
                var rmtType = $("#ADRMTTYPE").val();
                var lKindId = $(obj).val();
                if ( lKindId == "" ) {
                    $("#ADMKINDID option").remove();
                    $("#ADMKINDID").append($("<option value=''>--全部--</option>"));
                } else {
                    $.ajax({
                        url: "<%= request.getContextPath() %>/B109/MKindQuery",
                        type: "POST",
                        data: { "ADRMTTYPE": $("#ADRMTTYPE").val(),
                                "ADLKINDID":  lKindId },
                        cache: false,
                        async: false,   // 要用同步, 不然會有問題
                        success: function (data) {
                            $("#ADMKINDID option").remove();

                            var i;
                            $("#ADMKINDID").append($("<option value=''>--全部--</option>"));
                            for (i = 0; i < data.length; i++) {
                                $("#ADMKINDID").append($("<option></option>").attr("value", data[i].admkindid).text(data[i].adrmtitem));
                            }
                        },
                        error: function (err) {
                            alert(err.statusText);
                        }
                    });
                }
            }

            function onQuery() {
                $("span.error").remove();

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "order": [[0, "asc"]],
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B109/IndexQuery",
                        "data": {
                            "ADRMTTYPE": $("#ADRMTTYPE").val(),
                            "ADRMTID": $("#ADRMTID").val(),
                            "ADLKINDID": $("#ADLKINDID").val(),
                            "ADMKINDID": $("#ADMKINDID").val()
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [ 
                        { "data": "adseqno"   },
                        { "data": "adseqno"   },
                        { "data": "adrmttype" },
                        { "data": "adrmtid" },
                        { "data": "adrmtitem" },
                        { "data": "adrmtdesc" },
                        { "data": "adrmtitem" },
                        { "data": "adrmtdesc" },
                        { "data": "adrmtitem" },
                        { "data": "adrmtdesc" },
                    ],
                    "createdRow": function (row, data, index) {
                        // ID
                        var $id = $("<input type='button' class='btn btn-link' />");
                        $id.attr("value", data.adseqno);
                        $id.attr("onclick", "location.href='<%= request.getContextPath() %>/B109/Edit/"+data.adseqno+"'");
                        $("td", row).eq(0).text("").append($id);

                        var $date = $("<span>"+data.lastdate+"</span>");
                        var $time = $("<span>"+data.lasttime+"</span>");
                        var $BR = $("<br/>");
                        $("td", row).eq(1).text("").append($date).append($BR).append($time);

                        if ( data.adrmttype=="1") {
                            $("td", row).eq(2).text("1(匯出)");
                        } else {
                            $("td", row).eq(2).text("2(匯入)");
                        }

                        if ( data.admkindid.trim() == "00" && data.adrmtid.trim() == "" ) {
                            //資料屬於大分類項目，其它欄位清為空白
                            $("td", row).eq(6).text("");
                            $("td", row).eq(7).text("");
                            $("td", row).eq(8).text("");
                            $("td", row).eq(9).text("");
                        }

                        if ( data.admkindid.trim() != "00" && data.adrmtid.trim() == "" ) {
                            //資料屬於中分類項目，其它欄位清為空白
                            $("td", row).eq(4).text("");
                            $("td", row).eq(5).text("");
                            $("td", row).eq(8).text("");
                            $("td", row).eq(9).text(""); 
                        }

                        if ( data.adrmtid.trim() != "" ) {
                            //資料屬於中分類項目，其它欄位清為空白
                            $("td", row).eq(4).text("");
                            $("td", row).eq(5).text("");
                            $("td", row).eq(6).text(""); 
                            $("td", row).eq(7).text("");
                        }
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }
        </script>
    </body>
</html>