<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!-- 20190603 DannyChou ADD -->

<html>
    <head>
        <c:set var="action" value="${allowEdit==true ? '修改': '查詢'}" />
        <title>台企銀 後台-匯款用途選單維護-${action}</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>匯款用途選單維護-${action}</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADRMTTYPE">匯款用途性質：</label>
                    <div class="col-4">
                        <select id="ADRMTTYPE" name="ADRMTTYPE" class="form-control" onchange="onADRMTTYPEChange(this);">
                            <option value="1">匯出</option>
                            <option value="2">匯入</option>
                        </select> 
                    </div>
                    <label class="col-2 control-label text-right" for="ADRMTID" >匯款用途編號：</label>
                    <div class="col-4">
                        <input type="text" id="ADRMTID" name="ADRMTID" class="form-control" value="${remitMenu.ADRMTID}" maxlength="3" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADLKINDID" onchange="" >大分類編號：</label>
                    <div class="col-4">
                        <select id="ADLKINDID" name="ADLKINDID" class="form-control" onchange="onLKindChange(this);">
                        </select>
                    </div>
                    <label class="col-2 control-label text-right" for="ADMKINDID">中分類編號：</label>
                    <div class="col-4">
                        <select id="ADMKINDID" name="ADMKINDID" class="form-control">
                            <option value="00">無中分類</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADRMTITEM">分類項目(繁中)：</label>
                    <div class="col-10">
                        <input type="text" id="ADRMTITEM" name="ADRMTITEM" class="form-control" value="${remitMenu.ADRMTITEM}" maxlength="100" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADRMTCHSITEM">分類項目(簡中)：</label>
                    <div class="col-10">
                        <input type="text" id="ADRMTCHSITEM" name="ADRMTCHSITEM" class="form-control" value="${remitMenu.ADRMTCHSITEM}" maxlength="100" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADRMTENGITEM">分類項目(英文)：</label>
                    <div class="col-10">
                        <input type="text" id="ADRMTENGITEM" name="ADRMTENGITEM" class="form-control" value="${remitMenu.ADRMTENGITEM}" maxlength="200" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADRMTDESC">分類說明(繁中)：</label>
                    <div class="col-10">
                        <textarea id="ADRMTDESC" name="ADRMTDESC" class="form-control" maxlength="200" rows="3">${remitMenu.ADRMTDESC}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADRMTCHSDESC">分類說明(簡中)：</label>
                    <div class="col-10">
                        <textarea id="ADRMTCHSDESC" name="ADRMTCHSDESC" class="form-control" maxlength="200" rows="3">${remitMenu.ADRMTCHSDESC}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADRMTENGDESC">分類說明(英文)：</label>
                    <div class="col-10">
                        <textarea id="ADRMTENGDESC" name="ADRMTENGDESC" class="form-control" maxlength="500" rows="3">${remitMenu.ADRMTENGDESC}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADCHKMK">文件審核：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <select id="ADCHKMK" name="ADCHKMK" class="form-control">
                                <option value="Y"> 是 </option>
                                <option value="N"> 否 </option>
                            </select> &nbsp;&nbsp;
                            
                            <c:set var="checked" value="${remitMenu.ADRMTEETYPE1.equals('Y') ? 'checked': ''}" />
                            <input id="ADRMTEETYPE1" name="ADRMTEETYPE1" type="checkbox" value="${remitMenu.ADRMTEETYPE1}" ${checked} />&nbsp;
                            <label class="control-label" for="ADRMTEETYPE1">公司/團體</label>&nbsp;&nbsp;&nbsp;

                            <c:set var="checked" value="${remitMenu.ADRMTEETYPE2.equals('Y') ? 'checked': ''}" />
                            <input id="ADRMTEETYPE2" name="ADRMTEETYPE2" type="checkbox" value="${remitMenu.ADRMTEETYPE2}" ${checked} />&nbsp;
                            <label class="control-label" for="ADRMTEETYPE2">個人/(本國國民)</label>&nbsp;&nbsp;&nbsp;

                            <c:set var="checked" value="${remitMenu.ADRMTEETYPE3.equals('Y') ? 'checked': ''}" />
                            <input id="ADRMTEETYPE3" name="ADRMTEETYPE3" type="checkbox" value="${remitMenu.ADRMTEETYPE3}" ${checked} />&nbsp;
                            <label class="control-label" for="ADRMTEETYPE2">個人(持居留證一年以上者)</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnGoback" type="button" value="回上頁" class="btn btn-dark" aria-label="Left Align" onclick="onGoBack();" />&nbsp;
                        <c:if test="${allowEdit}">
                            <input id="btnDelete" type="button" value="刪除" class="btn btn-danger" aria-label="Left Align" onclick="onDelete();" />&nbsp;
                            <input id="btnSave" type="button" value="儲存" class="btn btn-info" aria-label="Left Align" onclick="onSave();" />
                        </c:if>
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script>
            $(document).ready(function () {
                init();
                if ( "${allowEdit}" == "false" ) {
                    $("form input").prop("disabled", true);
                    $("form select").prop("disabled", true);
                    $("form textarea").prop("disabled", true);
                    $("form input[type='button']").prop("disabled", false);
                } 
                if ( "${type}" == "L" ||  "${type}" == "M" ) {
                    $("#ADRMTTYPE").prop('disabled', 'disabled');
                    $("#ADLKINDID").prop('disabled', 'disabled');
                    $("#ADMKINDID").prop('disabled', 'disabled');
                    $("#ADRMTID").attr('disabled', 'disabled');
                }
            }); //ready

            function init() {
                $("#ADRMTTYPE").val("${remitMenu.ADRMTTYPE}");

                onADRMTTYPEChange($("#ADRMTTYPE"));
                $("#ADLKINDID").val("${remitMenu.ADLKINDID.trim()}");
                
                onLKindChange($("#ADLKINDID"));
                $("#ADMKINDID").val("${remitMenu.ADMKINDID.trim()}");
            }
            
            // AJAX for 大分類下拉選單
            function onADRMTTYPEChange(obj) {
                var rmtType = $(obj).val();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B109/LKindQuery",
                    type: "POST",
                    data: { "ADRMTTYPE": $("#ADRMTTYPE").val() },
                    cache: false,
                    async: false,   // 要用同步, 不然會有問題
                    success: function (data) {
                        $("#ADLKINDID option").remove();

                        var i;
                        for (i = 0; i < data.length; i++) {
                            $("#ADLKINDID").append($("<option></option>").attr("value", data[i].adlkindid).text(data[i].adrmtitem));
                        }
                    },
                    error: function (err) {
                        alert(err.statusText);
                    }
                });
            }

            // AJAX for 中分類下拉選單
            function onLKindChange(obj) {
                var rmtType = $("#ADRMTTYPE").val();
                var lKindId = $(obj).val();
           
                $.ajax({
                    url: "<%= request.getContextPath() %>/B109/MKindQuery",
                    type: "POST",
                    data: { "ADRMTTYPE": $("#ADRMTTYPE").val(),
                            "ADLKINDID":  lKindId },
                    cache: false,
                    async: false,   // 要用同步, 不然會有問題
                    success: function (data) {
                        $("#ADMKINDID option").remove();

                        var i;
                        $("#ADMKINDID").append($("<option value='00'>無中分類</option>"));
                        for (i = 0; i < data.length; i++) {
                            $("#ADMKINDID").append($("<option></option>").attr("value", data[i].admkindid).text(data[i].adrmtitem));
                        }
                    },
                    error: function (err) {
                        alert(err.statusText);
                    }
                });
            }

            function checkEmpty() {
                var msg = "";
                if ( "${type}" == "" ) {
					if ( $("#ADLKINDID").val()=="" ) {
						 msg += "請選擇大分類編號.\r\n";
					}
					if ( $("#ADRMTID").val().trim()=="" ) {
						 msg += "請輸入匯款用途編號.\r\n";
					}
                }
                if ( $("#ADRMTITEM").val()=="" ) {
                    msg += "分類項目(繁中)不可為空值.\r\n";
                }

                if ( $("#ADRMTCHSITEM").val()=="" ) {
                    msg += "分類項目(英文)不可為空值.\r\n";
                }

                if ( $("#ADRMTENGITEM").val()=="" ) {
                    msg += "分類項目(簡中)不可為空值.\r\n";
                }

                return msg;
            }

            // 修改
            function onSave() {
                var msg = checkEmpty();
                if ( msg != "" ) {
                    alert(msg);
                    return;
                }
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B109/Edit",
                    type: "POST",
                    data: { 
                            "ADSEQNO": "${remitMenu.ADSEQNO}",
                            "ADRMTTYPE": $("#ADRMTTYPE").val(),
                            "ADRMTID": $("#ADRMTID").val(),
                            "ADLKINDID": $("#ADLKINDID").val(),
                            "ADMKINDID": $("#ADMKINDID").val(),
                            "ADRMTITEM": $("#ADRMTITEM").val(),
                            "ADRMTCHSITEM": $("#ADRMTCHSITEM").val(),
                            "ADRMTENGITEM": $("#ADRMTENGITEM").val(),
                            "ADRMTDESC": $("#ADRMTDESC").val(),
                            "ADRMTCHSDESC": $("#ADRMTCHSDESC").val(),
                            "ADRMTENGDESC": $("#ADRMTENGDESC").val(),
                            "ADCHKMK": $("#ADCHKMK").val(),
                            "ADRMTEETYPE1": $("#ADRMTEETYPE1").prop('checked') ? "Y" : "",
                            "ADRMTEETYPE2": $("#ADRMTEETYPE2").prop('checked') ? "Y" : "",
                            "ADRMTEETYPE3": $("#ADRMTEETYPE3").prop('checked') ? "Y" : ""
                          },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("修改成功");  
                            location.href="<%= request.getContextPath() %>/B109"
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else {
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                                }
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            // 刪除
            function onDelete() {
                var message = confirm("確定刪除分類項目[${remitMenu.ADRMTITEM}]？");
                if ( message ) {
                    lockScreen();
                    $.ajax({
                        url: "<%= request.getContextPath() %>/B109/Delete/${remitMenu.ADSEQNO}",
                        type: "POST",
                        data: {},
                        success: function (res) {
                            unlockScreen();
                            if(res.validated){
                                alert("刪除成功");
                                location.href="<%= request.getContextPath() %>/B109"
                            } else {
                                //Set error messages
                                $.each(res.errorMessages,function(key,value){
                                    if ( key === "summary" )
                                        alert(value);
                                    else {
                                        alert(key+":"+value);
                                    }
                                });
                            }
                        },
                        error: function (err) {
                            unlockScreen();
                            alert(err.statusText);
                        }
                    });
                }
            }

            function onGoBack() {
                location.href="<%= request.getContextPath() %>/B109"
            }

        </script>
    </body>
</html>