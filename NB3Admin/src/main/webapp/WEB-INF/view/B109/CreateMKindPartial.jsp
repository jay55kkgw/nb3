<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!-- body content start -->
<div class="container" id="main-content">
    <h2>匯款用途選單維護-新增</h2>
    <form class="container main-content-block p-4" action="/dummy" method="post">
       <div class="form-group row">
           <label class="col-2 control-label text-right" for="MADRMTTYPE">匯款用途性質：</label>
           <div class="col-4">
               <select id="MADRMTTYPE" name="MADRMTTYPE" class="form-control" onchange="onMADRMTTYPEChange(this);">
                   <option value="1">匯出</option>
                   <option value="2">匯入</option>
               </select> 
           </div>
       </div>
       <div class="form-group row">
           <label class="col-2 control-label text-right" for="MADLKINDID" onchange="" >大分類編號：</label>
           <div class="col-10">
           	<div class="form-inline">
            	<select id="MADLKINDID" name="MADLKINDID" class="form-control" onchange="onMLKindChange(this);">
                    <option value="">--請選擇--</option>
                </select>
           	</div>
           </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="MADRMTITEM">分類項目(繁中)：</label>
            <div class="col-10">
                <input type="text" id="MADRMTITEM" name="MADRMTITEM" class="form-control" maxlength="100" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="MADRMTCHSITEM">分類項目(簡中)：</label>
            <div class="col-10">
                <input type="text" id="MADRMTCHSITEM" name="MADRMTCHSITEM" class="form-control" maxlength="100" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="MADRMTENGITEM">分類項目(英文)：</label>
            <div class="col-10">
                <input type="text" id="MADRMTENGITEM" name="MADRMTENGITEM" class="form-control" maxlength="200" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="MADRMTDESC">分類說明(繁中)：</label>
            <div class="col-10">
                <textarea id="MADRMTDESC" name="MADRMTDESC" class="form-control" maxlength="200" rows="3"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="MADRMTCHSDESC">分類說明(簡中)：</label>
            <div class="col-10">
                <textarea id="MADRMTCHSDESC" name="MADRMTCHSDESC" class="form-control" maxlength="200" rows="3"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="MADRMTENGDESC">分類說明(英文)：</label>
            <div class="col-10">
                <textarea id="MADRMTENGDESC" name="MADRMTENGDESC" class="form-control" maxlength="500" rows="3"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="MADCHKMK">文件審核：</label>
            <div class="col-10">
                <div class="form-inline">
                    <select id="MADCHKMK" name="MADCHKMK" class="form-control">
                        <option value="Y"> 是 </option>
                        <option value="N"> 否 </option>
                    </select>&nbsp;&nbsp;
                    
                    <input id="MADRMTEETYPE1" name="MADRMTEETYPE1" type="checkbox" />&nbsp;
                    <label class="control-label" for="MADRMTEETYPE1">公司/團體</label>&nbsp;&nbsp;&nbsp;
                    
                    <input id="MADRMTEETYPE2" name="MADRMTEETYPE2" type="checkbox" />&nbsp;
                    <label class="control-label" for="MADRMTEETYPE2">個人/(本國國民)</label>&nbsp;&nbsp;&nbsp;

                    <input id="MADRMTEETYPE3" name="MADRMTEETYPE3" type="checkbox" />&nbsp;
                    <label class="control-label" for="MADRMTEETYPE3">個人(持居留證一年以上者)</label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="offset-2 col-10">
                <input id="btnSave" type="button" value="儲存" class="btn btn-info" aria-label="Left Align" onclick="onMCreate();" />&nbsp;
            </div>
        </div>
    </form>
</div>
        
<script>

    function checkMEmpty() {
        var msg = "";
        if ( $("#MADRMTITEM").val()=="" ) {
            msg += "分類項目(繁中)不可為空值.\r\n";
        }

        if ( $("#MADRMTCHSITEM").val()=="" ) {
            msg += "分類項目(英文)不可為空值.\r\n";
        }

        if ( $("#MADRMTENGITEM").val()=="" ) {
            msg += "分類項目(簡中)不可為空值.\r\n";
        }

        return msg;
    }

    function onMCreate() {
        var msg = checkMEmpty();
        if ( msg != "" ) {
            alert(msg);
            return;
        }

        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/B109/CreateMKindSave",
            type: "POST",
            data: { 
                    "ADRMTTYPE": $("#MADRMTTYPE").val(),
                    "ADRMTID": "",
                    "ADLKINDID": $("#MADLKINDID").val(),
                    "ADMKINDID": "",
                    "ADRMTITEM": $("#MADRMTITEM").val(),
                    "ADRMTCHSITEM": $("#MADRMTCHSITEM").val(),
                    "ADRMTENGITEM": $("#MADRMTENGITEM").val(),
                    "ADRMTDESC": $("#MADRMTDESC").val(),
                    "ADRMTCHSDESC": $("#MADRMTCHSDESC").val(),
                    "ADRMTENGDESC": $("#MADRMTENGDESC").val(),
                    "ADCHKMK": $("#MADCHKMK").val(),
                    "ADRMTEETYPE1": $("#MADRMTEETYPE1").prop('checked') ? "Y" : "",
                    "ADRMTEETYPE2": $("#MADRMTEETYPE2").prop('checked') ? "Y" : "",
                    "ADRMTEETYPE3": $("#MADRMTEETYPE3").prop('checked') ? "Y" : ""
                  },
            success: function (res) {
                unlockScreen();
                if(res.validated){
                    alert("新增中分類成功");  
                } else {
                    $.each(res.errorMessages,function(key,value){
                        if ( key === "summary" )
                            alert(value);
                        else {
                            $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                        }
                    });
                }
            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }

    onMADRMTTYPEChange($("#MADRMTTYPE"));
    
    function onMADRMTTYPEChange(obj) {
        var rmtType = $(obj).val();
        $.ajax({
            url: "<%= request.getContextPath() %>/B109/LKindQuery",
            type: "POST",
            data: { "ADRMTTYPE": $("#MADRMTTYPE").val() },
            cache: false,
            success: function (data) {
                $("#MADLKINDID option").remove();

                var i;
                for (i = 0; i < data.length; i++) {
                    $("#MADLKINDID").append($("<option></option>").attr("value", data[i].adlkindid).text(data[i].adrmtitem));
                }
            },
            error: function (err) {
                alert(err.statusText);
            }
        });
    }
</script>