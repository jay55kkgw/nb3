<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!-- body content start -->
<div class="container" id="main-content">
    <h2>匯款用途選單維護-新增</h2>
    <form class="container main-content-block p-4" action="/dummy" method="post">
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="LADRMTTYPE">匯款用途性質：</label>
            <div class="col-4">
                <select id="LADRMTTYPE" name="LADRMTTYPE" class="form-control">
                    <option value="1">匯出</option>
                    <option value="2">匯入</option>
                </select> 
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="LADRMTITEM">分類項目(繁中)：</label>
            <div class="col-10">
                <input type="text" id="LADRMTITEM" name="LADRMTITEM" class="form-control" maxlength="100" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="LADRMTCHSITEM">分類項目(簡中)：</label>
            <div class="col-10">
                <input type="text" id="LADRMTCHSITEM" name="LADRMTCHSITEM" class="form-control" maxlength="100" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="LADRMTENGITEM">分類項目(英文)：</label>
            <div class="col-10">
                <input type="text" id="LADRMTENGITEM" name="LADRMTENGITEM" class="form-control" maxlength="200" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="LADRMTDESC">分類說明(繁中)：</label>
            <div class="col-10">
                <textarea id="LADRMTDESC" name="LADRMTDESC" class="form-control" maxlength="200" rows="3"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="LADRMTCHSDESC">分類說明(簡中)：</label>
            <div class="col-10">
                <textarea id="LADRMTCHSDESC" name="LADRMTCHSDESC" class="form-control" maxlength="200" rows="3"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="LADRMTENGDESC">分類說明(英文)：</label>
            <div class="col-10">
                <textarea id="LADRMTENGDESC" name="LADRMTENGDESC" class="form-control" maxlength="500" rows="3"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 control-label text-right" for="LADCHKMK">文件審核：</label>
            <div class="col-10">
                <div class="form-inline">
                    <select id="LADCHKMK" name="LADCHKMK" class="form-control">
                        <option value="Y"> 是 </option>
                        <option value="N"> 否 </option>
                    </select>&nbsp;&nbsp;
                    
                    <input id="LADRMTEETYPE1" name="LADRMTEETYPE1" type="checkbox" />&nbsp;
                    <label class="control-label" for="LADRMTEETYPE1">公司/團體</label>&nbsp;&nbsp;&nbsp;
                    
                    <input id="LADRMTEETYPE2" name="LADRMTEETYPE2" type="checkbox" />&nbsp;
                    <label class="control-label" for="LADRMTEETYPE2">個人/(本國國民)</label>&nbsp;&nbsp;&nbsp;

                    <input id="LADRMTEETYPE3" name="LADRMTEETYPE3" type="checkbox" />&nbsp;
                    <label class="control-label" for="LADRMTEETYPE3">個人(持居留證一年以上者)</label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="offset-2 col-10">
                <input id="btnSave" type="button" value="儲存" class="btn btn-info" aria-label="Left Align" onclick="onLCreate();" />&nbsp;
            </div>
        </div>
    </form>
</div>
        
<script>

    function checkLEmpty() {
        var msg = "";
        if ( $("#LADRMTITEM").val()=="" ) {
            msg += "分類項目(繁中)不可為空值.\r\n";
        }

        if ( $("#LADRMTCHSITEM").val()=="" ) {
            msg += "分類項目(英文)不可為空值.\r\n";
        }

        if ( $("#LADRMTENGITEM").val()=="" ) {
            msg += "分類項目(簡中)不可為空值.\r\n";
        }

        return msg;
    }

    function onLCreate() {
        var msg = checkLEmpty();
        if ( msg != "" ) {
            alert(msg);
            return;
        }

        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/B109/CreateLKindSave",
            type: "POST",
            data: { 
                    "ADRMTTYPE": $("#LADRMTTYPE").val(),
                    "ADRMTID": "",
                    "ADLKINDID": "",
                    "ADMKINDID": "00",
                    "ADRMTITEM": $("#LADRMTITEM").val(),
                    "ADRMTCHSITEM": $("#LADRMTCHSITEM").val(),
                    "ADRMTENGITEM": $("#LADRMTENGITEM").val(),
                    "ADRMTDESC": $("#LADRMTDESC").val(),
                    "ADRMTCHSDESC": $("#LADRMTCHSDESC").val(),
                    "ADRMTENGDESC": $("#LADRMTENGDESC").val(),
                    "ADCHKMK": $("#LADCHKMK").val(),
                    "ADRMTEETYPE1": $("#LADRMTEETYPE1").prop('checked') ? "Y" : "",
                    "ADRMTEETYPE2": $("#LADRMTEETYPE2").prop('checked') ? "Y" : "",
                    "ADRMTEETYPE3": $("#LADRMTEETYPE3").prop('checked') ? "Y" : ""
                  },
            success: function (res) {
                unlockScreen();
                if(res.validated){
                    alert("新增大分類成功");  
                    createLKINDCallBack();
                } else {
                    $.each(res.errorMessages,function(key,value){
                        if ( key === "summary" )
                            alert(value);
                        else {
                            $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                        }
                    });
                }
            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }
</script>