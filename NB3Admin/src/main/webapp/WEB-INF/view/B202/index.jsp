<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>基金交易管理查詢</h2>
        <form id="B201" name="B201" class="container main-content-block p-4" >
            <div class="form-group row">
                <label class="col-3 control-label text-right">身分證字號/統一編號：</label>
                <div class="col-3">
                    <input type="text" id="adUserId" name="adUserId" class="form-control upper" maxlength="10"/>
                </div>
                <label class="col-3 control-label text-right">帳號：</label>
                <div class="col-3">
                	<input type="text" id="adTxAcno" name="adTxAcno" class="form-control upper" maxlength="16"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-3 control-label text-right">查詢期間起~迄：</label>
                <div class="col-9">
                    <div class="form-inline">
                        <input type="text" id="startDate" autocomplete="off" name="startDate" class="form-control" style="width:230px" maxlength="19" />～
                        <input type="text" id="endDate" autocomplete="off" name="endDate" class="form-control" style="width:230px" maxlength="19" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-3 control-label text-right">交易類別：</label>
                <div class="col-3">
                    <select class="form-control" name="adOPGroup" id="adOPGroup">
                        <option value="GPA40">基金</option>
                    </select>
                </div>
                <label class="col-3 control-label text-right">交易名稱：</label>
                <div class="col-3">
                    <input type="text" id="adOPName"" name="adOPName" class="form-control" maxlength="10" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-3 control-label text-right">交易結果：</label>
                <div class="col-3">
                    <div class="form-inline">
                        <select class="form-control" name="adExCode" id="adExCode">
                            <option value="">全部</option>
                            <option value="S">交易成功</option>
                            <option value="F">交易失敗</option>
                        </select>
                    </div>
                </div>
                <label class="col-3 control-label text-right">使用者姓名：</label>
                <div class="col-3">
                    <input type="text" id="userName"" name="userName"" class="form-control" maxlength="10" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-3 control-label text-right">交易來源：</label>
                <div class="col-3">
                    <select class="form-control" name="loginType" id="loginType">
                        <option value="">全部</option>
                        <option value="NB">網銀</option>
                        <option value="MB">行動</option>
                        <option value="QR">QRCODE</option>
                    </select>
                </div>
                <label class="col-3 control-label text-right">使用者IP：</label>
                <div class="col-3">
                    <input type="text" id="adUserIp" name="adUserIp" class="form-control" maxlength="15" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-3 control-label text-right">交易代號：</label>
                <div class="col-3">
                    <select class="form-control" name="adopid" id="adopid">
                        <option value="">全部</option>
                        <c:forEach var="adopid" items="${adopids}">
                            <option value="${adopid.ADOPID}">${adopid.ADOPID}-${adopid.ADOPNAME}</option>
                        </c:forEach>
                    </select>
                </div>
 
            </div>
            <div class="form-group row">
                <div class="offset-3 col-9">
                    <input id="btnClear" type="reset" value="重新輸入" class="btn btn-dark" aria-label="Left Align" />&nbsp;
                    <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />
                </div>
            </div>
            
        </form>
    </div>

    <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>編號</th>
                <th>交易時間</th>
                <th>帳號/卡號</th>
                <th  style="text-align: right;">交易金額</th>
                <th>交易名稱</th>
                <th>交易代號</th>
                <th>訊息代號</th>
                <th>交易來源</th>
                <th>IP位置</th>
                <th>交易機制</th>
                <th>交易內容</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-watermark">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">提交資料</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="model-body" class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
    <script type="text/javascript">
        $(document).ready(function () {
        	$("body").on("contextmenu",function(e){
       	        return false;
       	    });

       	 //Disable cut copy paste
       	    $('body').bind('cut copy paste', function (e) {
       	        e.preventDefault();
       	    });
            $('#startDate').datetimepicker({
                lang: 'zh-TW',
                timepicker: true,
                format: 'Y/m/d H:i:00',   // 顯示時分
                scrollMonth: false,
                hours12: false,
                maxTime: false
            });
            $('#endDate').datetimepicker({
                lang: 'zh-TW',
                timepicker: true,
                format: 'Y/m/d H:i:00',   // 顯示時分
                scrollMonth: false,
                hours12: false,
                maxTime: false
            });
            $("#main").hide();
            $("#startDate").val("${startDate}");
            $("#endDate").val("${endDate}");
        }); //ready


        function onQuery() {
            var startDate = $("#startDate").val();
            var endDate = $("#endDate").val();

			//判斷長度如果不為19 也就是yyyy/MM/dd HH:mm:ss的格式
			if((startDate.length != 19)||(endDate.length!=19)){
				alert("時間格式有誤，請確認格式為'yyyy/MM/dd HH:mm'");
					return;
			}
            
            if ( startDate == "" || endDate == "" ) {
                alert("請選擇查詢期間起~迄");
                return;
            }

            $("#main").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "order": [[1, "desc"]],
                "columnDefs": [
                        { targets: 3, className: 'dt-body-right' },
                        ], 
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/B202/Query",
                    "data": {
                    	 "startDate": $("#startDate").val(),
                         "endDate": $("#endDate").val(),
                         "adUserId": $("#adUserId").val(),
                         "adOPGroup": $("#adOPGroup").val(),
                         "adExCode": $("#adExCode").val(),
                         "loginType": $("#loginType").val(),
                         "adOPName": $("#adOPName").val(),
                         "adUserIp": $("#adUserIp").val(),
                         "userName": $("#userName").val(),
                         "adTxAcno": $("#adTxAcno").val(),
                         "adopid": $("#adopid").val(),
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "adreqtype" },
                    { "data": "lastdate" },
                    { "data": "adtxacno" },
                    { "data": "adtxamt" },
                    { "data": "adopid" },
                    { "data": "adopid" },
                    { "data": "adexcode" },
                    { "data": "logintype" },
                    { "data": "aduserip" },
                    { "data": "fgtxway" },
                    { "data": "adtxno" }
                ],
                "createdRow": function (row, data, index) {
                    $("td", row).eq(0).text(index+1);

                    var $date = $("<span>"+data.lastdate.substring(0, 4)+"/"+data.lastdate.substring(4, 6)+"/"+data.lastdate.substring(6, 8)+"</span>");
                    var $time = $("<span>"+data.lasttime.substring(0, 2)+":"+data.lasttime.substring(2, 4)+":"+data.lasttime.substring(4, 6)+"</span>");
                    var $br = $("<br />");
                    $("td", row).eq(1).text("").append($date).append($br).append($time);

                    // 若有中文選單名稱，會以 "選單ID，選單名稱" 方式帶回
                    if ( data.adopid.indexOf(",") > 0 ) {
                        var tmp = data.adopid.split(",");
                        $("td", row).eq(4).text(tmp[0]);
                        $("td", row).eq(5).text(tmp[1]);
                    }

					if (data.adexcode.trim().length==0 || data.adexcode == '0' || data.adexcode == '0000')
					{
						if(data.adreqtype==='S')
						{
							$("td", row).eq(6).text("預約成功");
						}	
						else
						{	
							$("td", row).eq(6).text("交易成功");
						}
					}
					else
					{
 					 	var $status= $("<a href='#'>"+data.adexcode+"</a>");
	                    $status.attr("onclick", "showMsgCode('"+data.adexcode+"');");
	                    $("td", row).eq(6).text("").append($status);
					}

                    var logintype;
                    switch (data.logintype) {
                        case "NB":
                            logintype = "個網銀";
                            break;
                        case "MB":
                            logintype = "行動銀行";
                            break;
                    }
                    $("td", row).eq(7).text(logintype);

                    var sstype=data.fgtxway;
                    switch ( data.fgtxway ) {
                        case "0":
                            sstype = "交易密碼(SSL)";
                            break;
                        case "1":
                            sstype="電子簽章(i-key)";
                            break;
                        case "2":
                            sstype="晶片金融卡";
                            break;
                        case "3":
                            sstype="OTP";
                            break;
                        case "4":
                            sstype="軟體憑證(隨護神盾)";
                            break;
                        case "5":
                            sstype="快速交易";
                            break;
                        case "6":
                            sstype="小額交易";
                            break;
                        case "7":
                        	sstype="裝置認證";
                            break;
                    }
                    $("td", row).eq(9).text(sstype);
                    var $adcontent = $("<a href='#'>查詢<a/>");
                    $adcontent.attr("onclick", "showJsonData('"+data.adtxno+"');");
                    $("td", row).eq(10).text("").append($adcontent);
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            $("#main").show();
            return false;
        }

        //開啟處理狀態 Dialog, Controller 會回傳 PartialView
        function showJsonData(id) {
            var url = "<%= request.getContextPath() %>/B202/JsonData/" + id;
            lockScreen();
            $.post(url, { }, function (data, status) {
                unlockScreen();
                if (status == "success") {
                    var htmlContent = data.replace(/\r\n/g, "");
                    $("#model-body").html(htmlContent);
                    $("#exampleModalLabel").text("電文內容");
                    $('#exampleModal').modal({
                        
                    });
                }
            });
        }

        //開啟處理狀態 Dialog, Controller 會回傳 PartialView
        function showMsgCode(id) {
            var url = "<%= request.getContextPath() %>/B202/MsgDialog/" + id;
            lockScreen();
            $.post(url, {}, function (data, status) {
                unlockScreen();
                if (status == "success") {
                    var htmlContent = data.replace(/\r\n/g, "");
                    $("#exampleModalLabel").text("錯誤訊息");
                    $("#model-body").html(htmlContent);
                    $('#exampleModal').modal({

                    });
                }
            });
        }
    </script>
</body>

</html>