<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>



<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">
    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>應用系統啟動/關閉</h2>
        <form id="formData" class="container main-content-block p-4" action="<%= request.getContextPath() %>/B102/Result" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="LASTUSER">最後異動者：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="LASTUSER" name="LASTUSER" class="form-control" value="${Data.LASTUSER}" maxlength="10"  disabled  />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="LASTDATE">最後異動日期：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="LASTDATE" name="LASTDATE" class="form-control" value="${Data.LASTDATE}" maxlength="10" disabled  />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="ADNBSTATUS">應用系統目前狀態：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <input type="text" id="ADNBSTATUS" name="ADNBSTATUS" class="form-control" value="${Data.ADNBSTATUS}"  maxlength="10" disabled  />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">設定應用系統狀態：</label>
                <div class="col-10">
                    <div class="radio">
                        <div class="form-inline">
                            <label><input type="radio" id="started" name="optradio" value="Y" >&nbsp;</label>&nbsp;啟動&nbsp;
                            <label><input type="radio" id="closed" name="optradio" value="N" >&nbsp;</label>&nbsp;關閉&nbsp;
                            <label><input type="radio" id="test" name="optradio" value="T" >&nbsp;</label>&nbsp;測試&nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right">說明：</label>
                <label class="col-10 control-label">1.當應用系統狀態設為關閉時客戶無法登入網銀系統，但行員仍能登入管理系統。</label>
            </div>
            <div class="form-group row">
                <div class="offset-2 col-10" >
                    <c:if test="${allowEdit}">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp; 
                        <input id="btnCreate" type="submit" value="儲存" class="btn btn-info colorBtn" aria-label="Left Align"  />
                    </c:if>
                    <input type="hidden" id="ADNBSTATUSID" name="ADNBSTATUSID" class="form-control" value="${Data.ADNBSTATUSID}" maxlength="10"/>
                </div>
            </div>
        </form>
        <br>
        <form>
            <div class="form-row">
                <label for="IP" class="col-3 control-label text-right" >內部封測白名單IP：</label>
                <div class="col-9">
                    <div class="form-inline">
                        <input type="text" id="IP" name="IP" class="form-control" placeholder="例如：10.10.10.1 或 10.10.10.1-10.10.10.10(c class)" size="50" /> &nbsp;
                        <input type="button" id="addBtn" value="新增" onclick="onAddACL();"/>
                    </div>
                </div>
            </div>
        </form>      
        <br />
        <table id="ipacl" class="table table-striped table-bordered"   style="width:100%">
            <thead>
                <tr>
                    <th style="width:50%">IP 或 IP 範圍</th>
                    <th style="width:50%">異動狀態</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>

    <script>
        $(document).ready(function () {
            init();
            initACL();
        }); //ready
        
        function init(){
            var mbStatus = "";
            switch ($("#ADNBSTATUS").val()) {
                case "Y":
                    mbStatus = "啟動";  
                    $("#closed").attr('checked', true); 
                    break;
                case "N":
                    mbStatus = "關閉";
                    $("#started").attr('checked', true);  
                    break;
                case "T":
                    mbStatus = "內部封測";  
                    $("#started").attr('checked', true);       
                    break;
                default:
                    break;
            }
           
            //判斷狀態為啟動或關閉時，只可選擇關閉或啟動  
            $("#ADNBSTATUS").val(mbStatus) ; 

        }

        function initACL() {
            $("#ipacl").dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                orderMulti: false,
                bFilter: false,
                bSortable: true,
                bDestroy: true,
                "info": false,
                "paging": false, 
                pageLength: 10,
                order: [[0, "asc"]],
                ajax: {
                    type: "POST",
                    url: "<%= request.getContextPath() %>/B102/QueryAclIP",
                    data: {},
                    error: function (msg) {
                        alert(msg);
                    },
                },
                sServerMethod: "post",
                dom: "<'clear'>frtip", //無工具列
                columns: [{ data: "ip" }, { data: "ip" }],
                createdRow: function (row, data, index) {
                    //記錄PK
                    var ip = data.ip;
                    //確認身分
                    var allowEdit = ${allowEdit};
                    console.log("allowEdit="+allowEdit);
                    if(allowEdit == true){
                        var delBtn = $("<input type='button' value='刪除'>")
                        delBtn.attr("onclick", "onDeleteACL('"+ip+"')");
                            
                        $("td", row).eq(1).text("").append(delBtn);
                    }else{
                        $("#addBtn").hide();
                    }
                },
                language: {
                    url: "<%= request.getContextPath() %>/script/DataTables/chinese.json",
                },
            });
            $("#ipacl").show();
            return false;
        }

        function onAddACL() {
        	var ip=$("#IP").val();
        	
        	if ( ip.trim() === "" ) {
        		alert("內部封測白名單IP");
        		return;
        	}
        	
        	lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/B102/AddAclIP",
                type: "POST",
                data: {
                    "IP": ip
                },
                success: function (res) {
                    unlockScreen();
                    if(res.validated){
                        alert("內部封測白名單IP成功");   
                        initACL();
                    } else {
                        //Set error messages
                        $.each(res.errorMessages,function(key,value){
                            alert(value);
                        });
                    }
                },
                error: function (err) {
                    unlockScreen();
                    console.log(err);
                }
            });  // end of .ajax
        }

        function onDeleteACL(ip) {
        	if ( !confirm("確認刪除封測白名單IP["+ip+"]?") ) {
        		return;
        	}
        	lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/B102/DeleteAclIP",
                type: "POST",
                data: {
                    "IP": ip
                },
                success: function (res) {
                    unlockScreen();
                    if(res.validated){
                        alert("刪除內部封測白名單IP["+ip+"]成功");   
                        initACL();
                    } else {
                        //Set error messages
                        $.each(res.errorMessages,function(key,value){
                            alert(value);
                        });
                    }
                },
                error: function (err) {
                    unlockScreen();
                    console.log(err);
                }
            });  // end of .ajax
        }
    </script>
</body>
</html>