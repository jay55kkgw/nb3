<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<meta itemprop="image" content="">

<link rel="stylesheet" href="<%= request.getContextPath() %>/css/reset.css">
<link rel="stylesheet" href="<%= request.getContextPath() %>/css/bootstrap.min.css">
<link rel="stylesheet" href="<%= request.getContextPath() %>/css/all.css">
<!-- import css -->
<link rel="stylesheet" href="<%= request.getContextPath() %>/css/tbb_common.css">

<style>
body {
  background-image: url("<%= request.getContextPath() %>/Common/WaterMark");
  background-repeat: repeat;
}

table.transparent td, table.transparent tr, table.transparent th{
    background: transparent !important;
}

.modal-watermark {
  background-image: url("<%= request.getContextPath() %>/Common/WaterMark");
  background-repeat: repeat;
}
</style>