<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">案件歷程</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="model-body" class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
            </div>
        </div>
    </div>
</div>

<script>
    //開啟處理狀態 Dialog, Controller 會回傳 PartialView
    function showStatus(id) {
        var url = "<%= request.getContextPath() %>/Common/StatusDialog/" + id;
        lockScreen();
        $.post(url, {}, function (data, status) {
            unlockScreen();
            if (status == "success") {
                //alert(data);
                var htmlContent = data.replace(/\r\n/g, "");
                $("#model-body").html(htmlContent);
                $('#exampleModal').modal({
                    
                });
            }
        });
    }
</script>