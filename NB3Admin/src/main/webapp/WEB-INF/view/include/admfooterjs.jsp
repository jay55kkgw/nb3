<script src="<%= request.getContextPath() %>/script/jquery-3.1.1.min.js"></script>
<!-- <script src="<%= request.getContextPath() %>/script/popper.js"></script> -->
<script src="<%= request.getContextPath() %>/script/bootstrap.min.js"></script>
<script src="<%= request.getContextPath() %>/script/jquery.blockUI.js"></script>
<script src="<%= request.getContextPath() %>/script/common.js"></script>

<script type="text/javascript">
    //contorl nav-menu left/right-btn
    $('#nav-left-btn').click(function () {

        $('#nav-menu').animate({
            scrollLeft: "-=151"
        }, "fast", "swing");
    });

    $('#nav-right-btn').click(function () {

        $('#nav-menu').animate({
            scrollLeft: "+=151"
        }, "fast", "swing");
    });
    //footable
/*     jQuery(function ($) {
        $('.table').footable();
    }); */

    function friendlyPrint(id) {
        var xMax = 1200;
        var yMax = 800;

        if (document.all) {
            var xMax = screen.width, yMax = screen.height
        }
        else if (document.layers) {
            var xMax = window.outerWidth, yMax = window.outerHeight
        }           
        var wid = window.open("<%= request.getContextPath() %>/Common/FriendlyPrint?printArea=" + id, "_blank", "height="+yMax+",width="+xMax+",left=0,top=0,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1");
    }
    //20191114_Eric_ID to upper
    $(function() {
    $('.upper').keyup(function() {
        this.value = this.value.toUpperCase();
    });
});    
</script>