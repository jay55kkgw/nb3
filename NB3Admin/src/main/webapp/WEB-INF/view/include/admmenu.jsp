<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<header>
    <div id="desk-header" class="row">
        <div class="header-logo col-4">

                <img src="<%= request.getContextPath() %>/images/layout_set_logo.svg" style="cursor: hand;" onclick="location.href='<%= request.getContextPath() %>/Home/Index';" />

        </div>
        <div class="col-8">
            <ul id="header-nav">
                <li>&nbsp;</li>
                <li>&nbsp;</li>
            </ul>
        </div>
    </div>
</header>
<div class="content row">
    <section id="id-and-fast">
        <c:set var="isLogin" value='${requestScope["__LOGIN_USERID__"] != "" }' />
        <c:if test="${isLogin}">
            <div id="id-block">
                <span class="id-name">親愛的 ${__LOGIN_USERNAME__}(${__LOGIN_USERID__})，您好！</span>
                <span class="id-time">${__LOGIN_TIME__} 登入成功。IP：${__LOGIN_IP__}</span>
                <!-- ${__LOGIN_ROLES__} -->
            </div>
            <button class="log-out-btn" type="button" onclick="location.href='<%= request.getContextPath() %>/Account/Logout'">登出</button>
            <button class="log-out-btn" type="button" onclick="location.href='<%= request.getContextPath() %>/Home/Index';">重查待辦</button>
        </c:if>
        <c:if test="${!isLogin}">
            <div id="id-block"></div>
        </c:if>
    </section>
    <c:if test="${isLogin}">
        <nav id="ttb-menu">
            <button id="nav-left-btn" class="triangle-left"></button>
            <ul id="nav-menu" class="menu-list d-inline-block">
                <li>
                    <a href="#main-fuct-01" class="collapsed" onclick="querySubMenus('MENUGPB1','main-fuct-01'); main-fuct-01" data-toggle="collapse"
                        role="button" aria-expanded="true" aria-controls="main-fuct-01">
                        <p class="menu-title">系統管理</p>
                    </a>
                </li>
                <li>
                    <a href="#main-fuct-02" class="collapsed" onclick="querySubMenus('MENUGPB2','main-fuct-02'); main-fuct-02" data-toggle="collapse"
                        role="button" aria-expanded="true" aria-controls="main-fuct-02">
                        <p class="menu-title">客戶管理</p>
                    </a>
                </li>
                <li>
                    <a href="#main-fuct-03" class="collapsed" onclick="querySubMenus('MENUGPB3','main-fuct-03'); main-fuct-03" data-toggle="collapse"
                        role="button" aria-expanded="true" aria-controls="main-fuct-03">
                        <p class="menu-title">系統稽核</p>
                    </a>
                </li>
                <li>
                    <a href="#main-fuct-04" class="collapsed" onclick="querySubMenus('MENUGPB4','main-fuct-04'); main-fuct-04" data-toggle="collapse"
                        role="button" aria-expanded="true" aria-controls="main-fuct-04">
                        <p class="menu-title">系統監控</p>
                    </a>
                </li>
                <li>
                    <a href="#main-fuct-05" class="collapsed" onclick="querySubMenus('MENUGPB5','main-fuct-05'); main-fuct-05" data-toggle="collapse"
                        role="button" aria-expanded="true" aria-controls="main-fuct-05">
                        <p class="menu-title">公告管理</p>
                    </a>
                </li>
                <li>
                    <a href="#main-fuct-06" class="collapsed" onclick="querySubMenus('MENUGPB6','main-fuct-06'); main-fuct-06" data-toggle="collapse"
                        role="button" aria-expanded="true" aria-controls="main-fuct-06">
                        <p class="menu-title">電郵管理</p>
                    </a>
                </li>
                <li>
                    <a href="#main-fuct-07" class="collapsed" onclick="querySubMenus('MENUGPB7','main-fuct-07'); main-fuct-06" data-toggle="collapse"
                        role="button" aria-expanded="true" aria-controls="main-fuct-07">
                        <p class="menu-title">統計報表</p>
                    </a>
                </li>
            </ul>
            <button id="nav-right-btn" class="triangle-right"></button>
        </nav>
        <!-- 第二個區塊 -->
        <nav id="ttb-sec-menu">
            <!-- 第二區塊例一 -->
            <div class="sec-menu collapse" id="main-fuct-01" aria-labelledby="main-fuct-03" data-parent="#ttb-sec-menu">
                <ul class="sec-menu-list">
                    <li>
                        <a href="#" class="sec-menu-title"></a>
                    </li>
                </ul>
            </div>
            <!-- 第二區塊例一 -->
            <div class="sec-menu collapse" id="main-fuct-02" aria-labelledby="main-fuct-03" data-parent="#ttb-sec-menu">
                <ul class="sec-menu-list">
                    <li>
                        <a href="#" class="sec-menu-title"></a>
                    </li>
                </ul>
            </div>
            <!-- 第二區塊例一 -->
            <div class="sec-menu collapse" id="main-fuct-03" aria-labelledby="main-fuct-03" data-parent="#ttb-sec-menu">
                <ul class="sec-menu-list">
                    <li>
                        <a href="#" class="sec-menu-title"></a>
                    </li>
                </ul>
            </div>
            <!-- 第二區塊例一 -->
            <div class="sec-menu collapse" id="main-fuct-04" aria-labelledby="main-fuct-03" data-parent="#ttb-sec-menu">
                <ul class="sec-menu-list">
                    <li>
                        <a href="#" class="sec-menu-title"></a>
                    </li>
                </ul>
            </div>
            <!-- 第二區塊例一 -->
            <div class="sec-menu collapse" id="main-fuct-05" aria-labelledby="main-fuct-03" data-parent="#ttb-sec-menu">
                <ul class="sec-menu-list">
                    <li>
                        <a href="#" class="sec-menu-title"></a>
                    </li>
                </ul>
            </div>
            <!-- 第二區塊例一 -->
            <div class="sec-menu collapse" id="main-fuct-06" aria-labelledby="main-fuct-03" data-parent="#ttb-sec-menu">
                <ul class="sec-menu-list">
                    <li>
                        <a href="#" class="sec-menu-title"></a>
                    </li>
                </ul>
            </div>
            <!-- 第二區塊例一 -->
            <div class="sec-menu collapse" id="main-fuct-07" aria-labelledby="main-fuct-03" data-parent="#ttb-sec-menu">
                <ul class="sec-menu-list">
                    <li>
                        <a href="#" class="sec-menu-title"></a>
                    </li>
                </ul>
            </div>
        </nav>
    </c:if>
    <c:if test="${!isLogin}">
        
    </c:if>
</div>

<script>
    function querySubMenus(groupId, targetId) {
        $("#clearTag").empty();
        var targetDiv = $("#"+targetId);
        if ( targetDiv.attr("ok") == "1" ) {
            // 已經由 AJAX 取得子層選單
            return;
        }
            
        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/Home/SubMenus/"+groupId,
            type: "POST",
            data: { },
            success: function (data) {
                unlockScreen();
                targetDiv.empty();
                targetDiv.attr("ok", "1");
                var ul = $("<ul class='sec-menu-list'></ul>");
                targetDiv.append(ul);

                for ( var i=0; i<data.length; i++ ) {
                    var link = $("<a class='sec-menu-title'>"+data[i].adopname+"</a>");
                    link.attr("href", "<%= request.getContextPath() %>/"+encodeURI(data[i].url));

                    var li = $("<li></li>");
                    li.append(link);

                    ul.append(li);
                }

            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }

    document.onkeydown = function(){
        //F5  , CTRL + R
        if (event.keyCode==116 || (event.ctrlKey && event.keyCode==82))
        {
            event.keyCode=0;
            event.returnValue=false;
        }
        //Enter
        if (event.keyCode == 13 )
        {
        	if ($(event.target)[0]==$("textarea")[0]) {
       		 	return;
        	} else {
        		event.keyCode=0;
                event.returnValue=false;
        	}
           
        }
    };


</script>

<!-- clearTag 以下的內容為引用者的內容，用來處理使用者點第一層選單時清除內容，end tag 在 admfooter.jsp 中 -->
<div id="clearTag" style="min-height: 380px">
