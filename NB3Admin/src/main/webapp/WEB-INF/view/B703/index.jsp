<% response.setHeader("X-Frame-Options", "DENY"); %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html> 
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>一般網路銀行交易量統計表</h2>
            <form class="container main-content-block p-4" id="B703" name="B703" action="<%= request.getContextPath() %>/B703/Query" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >查詢期間：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <select class="form-control" id="YYYY" name="YYYY">
                                <option value="">--請選擇--</option>
                                <c:forEach items="${years}" var="year">
                                    <option value="${year}">${year}</option>
                                </c:forEach>
                            </select>&nbsp;年&nbsp;
                            <select class="form-control" id="MM" name="MM">
                                <option value="">--請選擇--</option>
                                <c:forEach items="${months}" var="month">
                                    <option value="${month}">${month}</option>
                                </c:forEach>
                            </select>&nbsp;月
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >查詢項目：</label>
                    <div class="col-10">
                        <div class="radio">
                            <div class="form-inline">
                                <label><input type="radio" id="optradio" name="optradio" value="ALL">&nbsp;全部</label>
                            </div>
                        </div>
                        <br/>
                        <div class="radio">
                            <div class="form-inline">
                                <label><input type="radio" id="optradio" name="optradio" value="TXID">&nbsp;功能別：&nbsp;</label>
                                <select id="txId" name="txId" class="form-control"> 
                                    <option value="GPA10">台幣帳務</option>
                                    <option value="GPA30">外匯帳務</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >交易來源：</label>
                    <div class="col-2">
                        <select name="loginType" id="loginType" class="form-control">
                            <option value="'','NB','MB'">全部</option>
                            <option value="'','NB'">網銀</option>
                            <option value="'MB'">行動</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10"> 
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnexpCSV" type="button" value="匯出CSV" class="btn btn-secondary" aria-label="Left Align" onclick="onExportCSV();" />&nbsp;
                        <input id="btnPrint" type="button" value="列印" class="btn btn-secondary" aria-label="Left Align" onclick="friendlyPrint('printQ,print,print2');" />
                    </div>
                </div>
            </form>
        </div>

        <!-- report -->
        <div id="printQ">
            <div class="form-group row">
                <label class="col-12 control-label text-center"><h4>一般網路銀行交易量統計表</h4></label>                
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >查詢時間：</label>
                <div class="col-10">
                    <div class="radio">
                        <div class="form-inline">
                            <jsp:useBean id="now" class="java.util.Date" />
                            <fmt:formatDate var="nowDateTime" value="${now}" pattern="yyyy/MM/dd HH:mm:ss" />
                            ${nowDateTime}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >查詢期間：</label>
                <div class="col-10">
                    <div class="form-inline">
                        ${B703Model.YYYY}/${B703Model.MM}                        
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >查詢項目：</label>
                <div class="col-10">
                    <div class="form-inline">
                        <c:choose>
                            <c:when test="${B703Model.optradio == 'ALL'}">
                                全部
                            </c:when>
                            <c:otherwise>
                                <label id="txIdText" name="txIdText" >TEMP</label>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >單位：</label>
                <div class="col-10">
                    <div class="form-inline">
                        筆/台幣仟元
                    </div>
                </div>
            </div>
        </div>

        <div id="print" name="print">
            <table id="main" name="main" class="display p-4 transparent" style="width:100%">
                <thead>
                    <tr>
                        <th rowspan="3" >類別</th>  
                        <th rowspan="3" colspan="2">單位</th>
                        <th colspan="8">自行交易</th>
                        <th colspan="8">跨行交易</th>
                        <th rowspan="2" colspan="3">合計</th>
                    </tr>
                    <tr>
                        <th colspan="2">交易密碼</th>
                        <th colspan="2">電子簽章(i-key)</th>
                        <th colspan="2">晶片金融卡</th>
                        <th colspan="2">動態密碼(OTP)</th>
                        <th colspan="2">交易密碼(SSL)</th>
                        <th colspan="2">電子簽章(i-key)</th>
                        <th colspan="2">晶片金融卡</th>                           
                        <th colspan="2">動態密碼(OTP)</th>
                    </tr>
                    <tr style="text-align: right">
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>失敗%</th>                
                    </tr>
                </thead>
                <tbody>
                    <c:catch var="error">
                        <c:forEach items="${Data}" var="item" varStatus="loop">
                            <tr>
                                <c:if test="${loop.index % 6 == 0}">
                                    <td rowspan="6"  align="Left">${item.adopGroupName}</td>
                                </c:if>
                                <c:if test="${loop.index % 2 == 0}">
                                    <c:choose>
                                        <c:when test="${item.adUserType == -1}">
                                            <td rowspan="2" align="Left">小計</td>
                                        </c:when>
                                        <c:when test="${item.adUserType == 0}">
                                            <td rowspan="2" align="Left">個人戶</td>
                                        </c:when>
                                        <c:when test="${item.adUserType == 1}">
                                            <td rowspan="2" align="Left">企業戶</td>
                                        </c:when>
                                    </c:choose>
                                </c:if>
                                <c:choose>
                                    <c:when test="${item.qtyType == 1}">
                                        <td align="Left">筆數</td>
                                    </c:when>
                                    <c:when test="${item.qtyType == 2}">
                                        <td align="Left">金額</td>
                                    </c:when>
                                </c:choose>                                                 

                                <td align="right">${item.q00t}</td>
                                <td align="right">${item.q00f}</td>
                                <td align="right">${item.q01t}</td>
                                <td align="right">${item.q01f}</td>
                                <td align="right">${item.q02t}</td>
                                <td align="right">${item.q02f}</td>
                                <td align="right">${item.q03t}</td>
                                <td align="right">${item.q03f}</td>

                                <td align="right">${item.q10t}</td>
                                <td align="right">${item.q10f}</td>
                                <td align="right">${item.q11t}</td>
                                <td align="right">${item.q11f}</td>
                                <td align="right">${item.q12t}</td>
                                <td align="right">${item.q12f}</td>
                                <td align="right">${item.q13t}</td>
                                <td align="right">${item.q13f}</td>

                                <td align="right">${item.q99t}</td>
                                <td align="right">${item.q99f}</td>
                                <td align="right">${item.q99tf}</td>
                            </tr>
                        </c:forEach>
                    </c:catch>                                             
                </tbody>
            </table>    
            <c:if test="${error != null}">
                <br><span style="color: red;">${error.message}</span>
                <br>${error}
            </c:if>                       
        </div>

        <div id="print2" name="print2">
            <br />
            <br />
            <br />
            <br />
            <table id="main2" name="main2" class="display p-4 transparent" style="width:100%">
                <thead>
                    <tr>
                        <th rowspan="3" colspan="2">類別</th>  
                        <th rowspan="3" >單位</th>
                        <th colspan="8">自行交易</th>
                        <th rowspan="2" colspan="3">合計</th>
                    </tr>
                    <tr>
                        <th colspan="2">交易密碼</th>
                        <th colspan="2">電子簽章(i-key)</th>
                        <th colspan="2">晶片金融卡</th>
                        <th colspan="2">動態密碼(OTP)</th>
                    </tr>
                    <tr>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>失敗%</th>                
                    </tr>
                </thead>
                <tbody>
                    <c:catch var="error2">
                        <c:forEach items="${Data2}" var="item" varStatus="loop">
                            <tr> 
                                <c:choose>
                                    <c:when test="${loop.index == 0 and fundQty>0}">
                                        <td rowspan="${fundQty}"  align="Left">基金</td>
                                    </c:when>
                                    <c:when test="${loop.index == fundQty and goldQty>0}">
                                        <td rowspan="${goldQty}" align="Left">黃金</td>                                        
                                    </c:when>
                                    <c:when test="${loop.index >= fundQty+goldQty and loop.count>0}">
                                        <td colspan="2" align="Left">${item.adopGroup.equals("ZZZZZZ") ? "小計" : item.adopGroupName}</td>
                                    </c:when>
                                </c:choose>
                                <c:if test="${item.seqno == 1 or item.seqno == 2}">
                                    <td align="Left">${item.adopGroup.equals("ZZZZZZ") ? "小計" : item.adopGroupName}</td>
                                </c:if>                                
                                <c:choose>
                                    <c:when test="${item.qtyType == 1}">
                                        <td align="Left">筆數</td>
                                    </c:when>
                                    <c:when test="${item.qtyType == 2}">
                                        <td align="Left">金額</td>
                                    </c:when>
                                </c:choose>                                                 

                                <td align="right">${item.q0t}</td>
                                <td align="right">${item.q0f}</td>
                                <td align="right">${item.q1t}</td>
                                <td align="right">${item.q1f}</td>
                                <td align="right">${item.q2t}</td>
                                <td align="right">${item.q2f}</td>
                                <td align="right">${item.q3t}</td>
                                <td align="right">${item.q3f}</td>

                                <td align="right">${item.q9t}</td>
                                <td align="right">${item.q9f}</td>
                                <td align="right">${item.q9tf}</td>
                            </tr>
                        </c:forEach>
                    </c:catch>                                             
                </tbody>
            </table>    
            <c:if test="${error2 != null}">
                <br><span style="color: red;">${error2.message}</span>
                <br>${error2}
            </c:if>                       
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                $('#DateFrom').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: false,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });
                $('#DateTo').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: false,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });

                //初始資料設定
                if ("${B703Model.optradio}" =="ALL"){
                    $("input[type='radio'][value='ALL']").prop("checked", true);
                }
                else
                {
                    $("input[type='radio'][value='TXID']").prop("checked", true);
                    $("#txIdText").text($("#txId option[value='${B703Model.txId}']").text());                    
                }

                $("#YYYY").val("${B703Model.YYYY}");
                $("#MM").val("${B703Model.MM}");
                $("#txId").val("${B703Model.txId}");
                $("#loginType").val("${B703Model.loginType}");

                if (${isPostback}===true) {
	                try {
	                    $("#main2").dataTable({
	                        "responsive": true,
	                        "processing": true,
	                        "serverSide": false,
	                        "orderMulti": false,
	                        "bFilter": false,
	                        "bSortable": true,
	                        "bDestroy": true,
	                        "pageLength": 100,
	                        "order": [[0, "asc"]],
	                        "dom": "<'clear'>frtip",   //無工具列
	                        "language": {
	                            "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
	                        }
	                    });
	                }
	                catch(err) {
	                    console.log(err.message);
	                }
	
	
	                try{
	                    $("#main").dataTable({
	                        "responsive": true,
	                        "processing": true,
	                        "serverSide": false,
	                        "orderMulti": false,
	                        "bFilter": false,
	                        "bSortable": true,
	                        "bDestroy": true,
	                        "pageLength": 100,
	                        "order": [[0, "asc"]],
	                        "dom": "<'clear'>frtip",   //無工具列
	                        "language": {
	                            "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
	                        }
	                    });
	                }catch(err){
	                    console.log(err.message);
	                }
                } else {
                	$("#printQ").hide();
                    $("#main").hide();
                    $("#main2").hide();
                }

            }); //ready
        
            function onQuery() {
                $("#B703").submit();             
            }
            
            function onExportCSV() {
                // window.location = "<%= request.getContextPath() %>/B701/DownloadtoCSV";

                $.ajax({
                        url:"<%= request.getContextPath() %>/B703/DownloadtoCSV",
                        type:"POST",
                        data:{
                            optradio: $("#optradio").val(),
                            YYYY: $("#YYYY").val(),
                            MM: $("#MM").val(),
                            txId: $("#txId").val(),
                            loginType: $("#loginType").val(),
                        },
                        success: function(response, status, xhr) {
                            // check for a filename
                            var filename = "";
                            var disposition = xhr.getResponseHeader('Content-Disposition');
                            if (disposition && disposition.indexOf('attachment') !== -1) {
                                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                                var matches = filenameRegex.exec(disposition);
                                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                            }

                            var type = xhr.getResponseHeader('Content-Type');
                            var blob = new Blob(["\ufeff" + response], { type: type });

                            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                                // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                                window.navigator.msSaveBlob(blob, filename);
                            } else {
                                var URL = window.URL || window.webkitURL;
                                var downloadUrl = URL.createObjectURL(blob);

                                if (filename) {
                                    // use HTML5 a[download] attribute to specify filename
                                    var a = document.createElement("a");
                                    // safari doesn't support this yet
                                    if (typeof a.download === 'undefined') {
                                        window.location = downloadUrl;
                                    } else {
                                        a.href = downloadUrl;
                                        a.download = filename;
                                        document.body.appendChild(a);
                                        a.click();
                                    }
                                } else {
                                    window.location = downloadUrl;
                                }

                                setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                            }
                        },
                        error:function(){
                            alert("error");
                        }
                });                
            }

        </script>
    </body>
</html>