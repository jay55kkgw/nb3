<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>



<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>信用卡申請網銀帳戶重新申請</h2>
            <form class="container main-content-block p-4" id="formData" action="<%= request.getContextPath() %>/B804/Result" method="post"  >
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="DPSUERID">申請者身分證字號：</label>
                    <div class="col-3">
                        <input type="text" id="DPSUERID" name="DPSUERID"  class="form-control upper" maxlength="10"  value="${Data.DPSUERID}" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnCreate" type="reset" value="取消" class="btn btn-dark" aria-label="Left Align" />&nbsp;
                        <input id="btnCreate" type="button" value="確定" class="btn btn-info" aria-label="Left Align"  onclick="onSave()"  />
                    </div>
                </div>
            </form>
        </div>

        <input type="hidden" id="error" name="error"  class="form-control" maxlength="10"  value="${error}"/>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.all.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.exhide.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
               var error= $("#error").val();
              if(error !="")
              {
                  alert(error);
                  return false;
              }
            }); //ready
            function onSave()
            {
               if($("#DPSUERID").val().trim()=="")
               {
                  alert("請輸入申請者身分證字號");
                  return false;
               } 
               $("#formData").submit();
            }
        </script>
    </body>
</html>