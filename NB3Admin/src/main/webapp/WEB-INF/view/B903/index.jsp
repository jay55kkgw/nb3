<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁-幣別檔維護</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>幣別檔管理</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADCURRENCY">幣別代號：</label>
                    <div class="col-4">
                        <input type="text" id="ADCURRENCY" name="ADCURRENCY" class="form-control" maxlength="3" />
                    </div>
                    <label class="col-2 control-label text-right" for="ADCCYNO">幣別號碼：</label>
                    <div class="col-4">
                        <input type="text" id="ADCCYNO" name="ADCCYNO" class="form-control" maxlength="2"  />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADCCYNAME">幣別繁中名稱：</label>
                    <div class="col-4">
                        <input type="text" id="ADCCYNAME" name="ADCCYNAME" class="form-control" maxlength="10"  />
                    </div>
                    <label class="col-2 control-label text-right" for="ADCCYCHSNAME">幣別簡中名稱：</label>
                    <div class="col-4">
                        <input type="text" id="ADCCYCHSNAME" name="ADCCYCHSNAME" class="form-control" maxlength="10"  />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" for="ADCCYENGNAME">幣別英文名稱：</label>
                    <div class="col-4">
                        <input type="text" id="ADCCYENGNAME" name="ADCCYENGNAME" class="form-control" maxlength="30"  />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnQuery" type="button" value="以上述值做查詢" class="btn btn-dark" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <c:if test="${allowEdit}">
                            <input id="btnCreate" type="button" value="以上述值做新增" class="btn btn-info" aria-label="Left Align" onclick="onCreate()" />&nbsp;
                        </c:if>
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th>幣別代號</th>
                    <th>幣別號碼</th>
                    <th>幣別繁中名稱</th>
                    <th>幣別簡中名稱</th>
                    <th>幣別英文名稱</th>
                    <th>動作</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.all.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.exhide.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                onQuery();
            }); //ready
        
            function onQuery() {
                $("span.error").remove();

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "order": [[0, "asc"]],
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B903/Query",
                        "data": {
                            "ADCURRENCY": $("#ADCURRENCY").val(),
                            "ADCCYNO": $("#ADCCYNO").val(),
                            "ADCCYNAME": $("#ADCCYNAME").val(),
                            "ADCCYCHSNAME": $("#ADCCYCHSNAME").val(),
                            "ADCCYENGNAME": $("#ADCCYENGNAME").val()
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [
                        { "data": "adcurrency" },
                        { "data": "adccyno" },
                        { "data": "adccyname" },
                        { "data": "adccychsname" },
                        { "data": "adccyengname" },
                        { "data": "adcurrency" }
                    ],
                    "createdRow": function (row, data, index) {
                        // 處理幣別代號
                        var key = data.adcurrency.trim();

                        var $currency = $("<input type='text' class='form-control' maxlength='3' disabled='disabled' />");
                        $currency.attr("id", "currency_" + key);
                        $currency.attr("value", key);
                        $("td", row).eq(0).text("").append($currency);

                        // 處理幣別號碼
                        var $ccyno = $("<input type='text' class='form-control' maxlength='2' disabled='disabled' />");
                        $ccyno.attr("id", "ccyno_" + key);
                        $ccyno.attr("value", data.adccyno);
                        $("td", row).eq(1).text("").append($ccyno);

                        // 處理幣別中文名稱
                        var $ccyname = $("<input type='text' class='form-control' maxlength='10' disabled='disabled' />");
                        $ccyname.attr("id", "ccyname_"+key);
                        $ccyname.attr("value", data.adccyname);
                        $("td", row).eq(2).text("").append($ccyname);

                        // 處理幣別中文名稱
                        var $ccychsname = $("<input type='text' class='form-control' maxlength='10' disabled='disabled' />");
                        $ccychsname.attr("id", "ccychsname_"+key);
                        $ccychsname.attr("value", data.adccychsname);
                        $("td", row).eq(3).text("").append($ccychsname);

                        // 處理幣別中文名稱
                        var $ccyengname = $("<input type='text' class='form-control' maxlength='30' disabled='disabled' />");
                        $ccyengname.attr("id", "ccyengname_"+key);
                        $ccyengname.attr("value", data.adccyengname);
                        $("td", row).eq(4).text("").append($ccyengname);

                        if ( "${allowEdit}" == "true" ) {
                            // 處理 Button
                            var $edit = $("<input type='button' value='修改' class='btn btn-primary' />");
                            $edit.attr("id", "btnEdit_"+key);
                            $edit.attr("onclick","onEdit('"+key+"')");

                            var $delete = $("<input type='button' value='刪除' class='btn btn-danger' />");
                            $delete.attr("id", "btnDelete_"+key);
                            $delete.attr("onclick","onDelete('"+key+"')");

                            var $save = $("<input type='button' value='儲存' class='btn btn-primary' style='display: none;' />");
                            $save.attr("id", "btnSave_"+key);
                            $save.attr("onclick","onSave('"+key+"')");

                            var $exit = $("<input type='button' value='離開' class='btn btn-dark' style='display: none;' />");
                            $exit.attr("id", "btnBack_"+key);
                            $exit.attr("onclick","onBack('"+key+"')");

                            var $space1 = $("<span>&nbsp;</span>");
                            var $space2 = $("<span>&nbsp;</span>");
                            $("td", row).eq(5).text("").append($edit).append($space1).append($delete).append($save).append($space2).append($exit);
                        } else {
                            $("td", row).eq(5).text("");
                        }
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }
            function checkEmpty(currency, ccyno, name, chsname, engname) {
                var msg = "";
                if ( currency=="" ) {
                    msg += "幣別代號不可為空值.\r\n";
                }

                if ( ccyno=="" ) {
                    msg += "幣別號碼不可為空值.\r\n";
                }

                if ( name=="" ) {
                    msg += "幣別繁中名稱不可為空值.\r\n";
                }

                if ( chsname=="" ) {
                    msg += "幣別簡中名稱不可為空值.\r\n";
                }

                if ( engname=="" ) {
                    msg += "幣別英文名稱不可為空值.\r\n";
                }

                return msg;
            }
            function onCreate() {
                $("span.error").remove();

                var currency = $("#ADCURRENCY").val();
                var ccyno = $("#ADCCYNO").val();
                var ccyname = $("#ADCCYNAME").val();
                var ccychsname = $("#ADCCYCHSNAME").val();
                var ccyengname = $("#ADCCYENGNAME").val();
                var msg = checkEmpty(currency, ccyno, ccyname, ccychsname, ccyengname);
                if ( msg != "" ) {
                    alert(msg);
                    return;
                }
                if ( !validateAlphaUC(currency) ) {
					alert("幣別代號請輸入英文大寫");
					return;
                }
				
                if ( !validateChinese(ccyname) ) {
					alert("幣別繁中名稱請輸入中文");
					return;
                }
                
                if ( !validateChinese(ccyname) ) {
					alert("幣別繁中名稱請輸入中文");
					return;
                }
                if ( !validateChinese(ccychsname) ) {
                	alert("幣別簡中名稱請輸入中文");
					return;
                }
                if ( !validateAlphaSpace(ccyengname) ) {
                	alert("幣別英文名稱請輸入英文");
					return;
                }
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B903/Create",
                    type: "POST",
                    data: {
                        "ADCURRENCY": currency,
                        "ADCCYNO": ccyno,
                        "ADCCYNAME": ccyname,
                        "ADCCYCHSNAME": ccychsname,
                        "ADCCYENGNAME": ccyengname
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("幣別代碼["+currency+"]新增成功");   
                            onQuery();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onEdit(id) {
                //$("#currency_" + id).attr("disabled", false);
                $("#ccyno_" + id).attr("disabled", false);
                $("#ccyname_" + id).attr("disabled", false);
                $("#ccychsname_" + id).attr("disabled", false);
                $("#ccyengname_" + id).attr("disabled", false);
                toggleButton(false, id);
            }

            function onDelete(id) {
                var currency = $("#currency_"+id).val();
                var message = confirm("確定刪除幣別代碼[" + currency + "]？");
                if (message == true) {
                    lockScreen();
                    $.ajax({
                        url: "<%= request.getContextPath() %>/B903/Delete/"+id,
                        type: "POST",
                        data: {},
                        success: function (data) {
                            unlockScreen();
                            if (data === "0") {
                                alert("幣別代碼[" + currency + "]刪除成功");
                                onQuery();
                            } else {
                                alert(data);
                            }
                        },
                        error: function (err) {
                            unlockScreen();
                            alert(err.statusText);
                        }
                    });
                }
            }

            function onSave(id) {
                var currency = $("#currency_" + id).val();
                var ccyno = $("#ccyno_" + id).val();
                var ccyname = $("#ccyname_" + id).val();
                var ccychsname = $("#ccychsname_" + id).val();
                var ccyengname = $("#ccyengname_" + id).val();
                var msg = checkEmpty(currency, ccyno, ccyname, ccychsname, ccyengname);
                if ( msg != "" ) {
                    alert(msg);
                    return;
                }
                if ( !validateChinese(ccyname) ) {
					alert("幣別繁中名稱請輸入中文");
					return;
                }
                if ( !validateChinese(ccychsname) ) {
                	alert("幣別簡中名稱請輸入中文");
					return;
                }
                if ( !validateAlphaSpace(ccyengname) ) {
                	alert("幣別英文名稱請輸入英文");
					return;
                }
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B903/Edit",
                    type: "POST",
                    data: {
                        "ADCURRENCY": currency,
                        "ADCCYNO": ccyno,
                        "ADCCYNAME": ccyname,
                        "ADCCYCHSNAME": ccychsname,
                        "ADCCYENGNAME": ccyengname
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("選單[" + ccyno + "]修改成功");  
                            $("#ccyno_" + id).attr("disabled", true);
                            $("#ccyname_" + id).attr("disabled", true);
                            $("#ccychsname_" + id).attr("disabled", true);
                            $("#ccyengname_" + id).attr("disabled", true);
                            toggleButton(true, id); 
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else {
                                    alert(key+":"+value);
                                }
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onBack(id) {
                var currency = $("#currency_" + id);
                var ccyno = $("#ccyno_" + id);
                var ccyname = $("#ccyname_" + id);
                var ccychsname = $("#ccychsname_" + id);
                var ccyengname = $("#ccyengname_" + id);

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B903/Get/" + id,
                    type: "POST",
                    cache: false,
                    async: false,
                    success: function (data) {
                        unlockScreen();
                        ccyno.val(data.adccyno);
                        ccyname.val(data.adccyname);
                        ccychsname.val(data.adccychsname);
                        ccyengname.val(data.adccyengname);

                        ccyno.attr("disabled", true);
                        ccyname.attr("disabled", true);
                        ccychsname.attr("disabled", true);
                        ccyengname.attr("disabled", true);
                        toggleButton(true, id);
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
            function toggleButton(toQuery, id) {
                if (toQuery) {
                    $("#btnEdit_" + id).show();
                    $("#btnEdit_" + id).next().show();
                    $("#btnDelete_" + id).show();

                    $("#btnSave_" + id).hide();
                    $("#btnSave_" + id).next().hide();
                    $("#btnBack_" + id).hide();
                } else {
                    $("#btnEdit_" + id).hide();
                    $("#btnEdit_" + id).next().hide();
                    $("#btnDelete_" + id).hide();

                    $("#btnSave_" + id).show();
                    $("#btnSave_" + id).next().show();
                    $("#btnBack_" + id).show();
                }
            }
        </script>
    </body>
</html>