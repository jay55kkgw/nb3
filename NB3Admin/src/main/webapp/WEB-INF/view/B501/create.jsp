<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
    
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>廣告管理-新增</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >廣告類型：</label>
                    <div class="col-3">
                        <select id="TYPE" class="form-control">
                            <option value="B">橫幅廣告</option>
                            <option value="C">小廣告</option>
<!--                             <option value="M">行動APP廣告</option> -->
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >廣告提示文字：</label>
                    <div class="col-9">
                        <input type="text" id="TITLE" name="TITLE" class="form-control" maxlength="85" />（會出現在圖片的 Alt 文字）
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >小廣告標題：</label>
                    <div class="col-9">
                        <input type="text" id="CONTENT" name="CONTENT" class="form-control" maxlength="85" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >廣告圖檔：</label>
                    <div class="col-9">
                        <div class="form-inline">
                            <input type="file" id="file" name="file" class="form-control" maxlength="50" />&nbsp;<span>橫幅廣告：(1685*598) 小廣告：(184*84) 行動APP：(630*180)</span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <div class="form-inline">
                            <input type="button" class="btn btn-info" value="上傳廣告" onclick="onUpload();" />&nbsp;
                            <input id="btnPreview" type="button" class="btn btn-info" value="預覽" onclick="onPreview();" />
                            <input type="hidden" id="imgGuid" />
                        </div>
                    </div>
                </div>    
                <div class="form-group row">
                	<label class="col-3 control-label text-right" >廣告類型：</label>
                    <div class="col-9">                        
	                    <input type="radio" id="ContentType" name="ContentType" value="1" checked>網頁連結&nbsp;
						<input type="radio" id="ContentType" name="ContentType" value="2">檔案&nbsp;
						<input type="radio" id="ContentType" name="ContentType" value="3">廣告內容
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >網頁連結：</label>
                    <div class="col-9">
                        <input type="text" id="URL" name="URL" class="form-control" maxlength="512" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >檔案：</label>
                    <div class="col-9">
                        <input type="file" id="TARGETFILENAME" name="TARGETFILENAME" class="form-control" disabled="disabled" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <div class="form-inline">
                          <input id=btnUploadFile type="button" class="btn btn-info" value="上傳檔案" onclick="onUploadFile();" />&nbsp;
	                      <input id="btnPreviewFile" type="button" class="btn btn-info" value="預覽檔案" onclick="onPreviewFile();" />&nbsp;(限上傳.pdf、.jpg、.png、.gif檔案)
	                      <input type="hidden" id="fileGuid"/>
	                      <input type="hidden" id="isTmp"/>
                        </div>
                    </div>
                </div>
               	<div class="form-group row">
					<label class="col-3 control-label text-right">廣告內容：</label>
					<div class="col-9">
						<textarea  id="TARGETCONTENT" name="TARGETCONTENT" cols="90" rows="10" disabled></textarea>
					</div>
				</div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >排序：</label>
                    <div class="col-1">
                        <input type="number" id="SORTORDER" name="SORTORDER" class="form-control" oninput="if(value.length>5)value=value.slice(0,5)" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >上架日期：</label>
                    <div class="col-3">
                        <div class="form-inline">
                            <input type="text" id="StartDateTime" autocomplete="off" name="StartDateTime" class="form-control" style = "width:160px" maxlength="8" />
                        </div>
                    </div>
                    <label class="col-3 control-label text-right" >下架日期：</label>
                    <div class="col-3">
                        <div class="form-inline">
                            <input type="text" id="EndDateTime" autocomplete="off" name="EndDateTime" class="form-control" style = "width:160px" maxlength="8" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onSave()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        
        <script>
            $(document).ready(function () {
                $("#btnPreview").hide();
                $("#fMemo").hide();
                $('#btnPreviewFile').hide();
                $('#btnUploadFile').attr('disabled', true);
                $('#StartDateTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false
                });
                $('#EndDateTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false
                });
                $('input[type=radio][name=ContentType]').change(function() {
                    if (this.value == '1') {
                    	$('#URL').attr('disabled', false);
                    	$('#btnPreviewFile').hide();
                    	$('#TARGETFILENAME').attr('disabled', true);
                    	$('#btnUploadFile').attr('disabled', true);
                    	$('#TARGETCONTENT').attr('disabled', true);
                    }
                    else if (this.value == '2') {
                    	$('#URL').attr('disabled', true);
                    	$('#btnPreviewFile').hide();
                    	$('#TARGETFILENAME').attr('disabled', false);
                    	$('#btnUploadFile').attr('disabled', false);
                    	$('#TARGETCONTENT').attr('disabled', true);
                    }
                    else if (this.value == '3') {
                    	$('#URL').attr('disabled', true);
                    	$('#btnPreviewFile').hide();
                    	$('#TARGETFILENAME').attr('disabled', true);
                    	$('#btnUploadFile').attr('disabled', true);
                    	$('#TARGETCONTENT').attr('disabled', false);
                    }
                });
            }); //ready

            // 上傳廣告
            function onUpload() {
                var fileData = new FormData();
                var type = $("#TYPE").val();
                var file = $("#file").get(0);

               	if ( file.files.length == 0 ) {
                	alert("請選擇圖檔");
                    return;
               	} else {
                    fileData.append("multipartFile", file.files[0]);
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B501/ImageUpload/"+type,
                    type: "POST",
                    contentType: false,  // Not to set any content header
                    processData: false,  // Not to process data
                    data: fileData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("圖檔上傳成功");   
                            $("#imgGuid").val(res.pkey);
                            $("#btnPreview").show();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.responseText);
                    }
                });
            }

            // 回查詢頁
            function onExit() {
                history.back();
            }

            // 預覽廣告
            function onPreview() {
                var obj = window.location;
                var imageGuid = $("#imgGuid").val();
                var targetType = $('input[name=ContentType]:checked').val();
                
                if ( targetType == "1" ) {
                	var url = $("#URL").val();
                    if ( url != "" ) {
                    	url = encodeURI(url);
                    }
               	 	window.open("<%= request.getContextPath() %>/B501/TmpPreview/"+imageGuid+"?Type="+$("#TYPE").val()+"&t="+targetType+"&u="+url);
                } else if ( targetType == "2" ) {
                	var fileGuid = $("#fileGuid").val();
                	window.open("<%= request.getContextPath() %>/B501/TmpPreview/"+imageGuid+"?Type="+$("#TYPE").val()+"&t="+targetType+"&f="+fileGuid);
                } else if ( targetType == "3" ) {
                	var targetContent = $("#TARGETCONTENT").val();
                	if ( targetContent != "" ) {
                		targetContent = encodeURI(targetContent);
                    }
                	window.open("<%= request.getContextPath() %>/B501/TmpPreview/"+imageGuid+"?Type="+$("#TYPE").val()+"&t="+targetType+"&c="+targetContent);
                }
            }

            // 新增
            function onSave() {
                if ( $("#imgGuid").val() == "" ) {
                    alert("請先上傳廣告圖檔");
                    return;
                }
                if ( $("#TITLE").val() == "" ) {
                    alert("請輸入廣告標題");
                    return;
                }
                //電金要求廣告類型為「橫幅廣告」不檢核內容
                if ( $('#TYPE').val() == "C" && $("#CONTENT").val() == "" ) {
                    alert("請輸入廣告內容");
                    return;
                }
                //電金要求不檢查URL
                /*if ( $("#URL").val() == "" ) {
                    alert("請輸入URL");
                    return;
                }*/
                if ( $("#SORTORDER").val() == "" ) {
                    alert("請輸入排序");
                    return;
                }
                var sisvalid = Date.parse($("#StartDateTime").val());
                var eisvalid = Date.parse($("#EndDateTime").val());
                
                if(isNaN(sisvalid) || isNaN(eisvalid) ) { 
                    alert("請輸入合法的上架日期和下架日期");
                    return;
                }

                if ( sisvalid > eisvalid ) {
                    alert("下架日期必需大於上架日期");
                    return;
                }
				
                //新增檢驗時間格式，避免手動把2位數的0去掉 EX:2020/01/01 => 2020/1/1 ，datetimepicker規則沒有強制補0
               if(($("#StartDateTime").val().length != 16)  || ($("#EndDateTime").val().length !=16)){
            	   alert("時間格式有誤，請確認格式為'yyyy/MM/dd HH:mm'");
                   return;						
               }
                /*var today=new Date();
                if ( today>sisvalid ) {
                    alert("上架日期必須大於今天");
                    return;
                }*/
                var contentType=$('input[name=ContentType]:checked').val();
                switch(contentType)
                {
                	case "1":
                         if ( $("#URL").val() == "" ) {
                        	   alert("請輸入網頁連結");
                             return;
                         }
                    	break;	
                	case "2":
                	     if ( $("#fileGuid").val() == "" ) {
                              alert("請上傳檔案");
                              return;
                         }
                    	break;
                	case "3":
                	    if ( $("#TARGETCONTENT").val() == "") {
                	    	alert("請輸入文字敘述");
                	        return;
                	    }
                    	break;		
                }
                if ( $("#CONTENT").val() == "" ) {
                	$("#CONTENT").val(" ");
                    return;
                }
                $("span.error").remove();
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B501/Send",
                    type: "POST",
                    data: {
                        "TYPE": $("#TYPE").val(),
                        "TITLE": $("#TITLE").val(),
                        "CONTENT": $("#CONTENT").val(),
                        "URL": $("#URL").val(),
                        "StartDateTime": $("#StartDateTime").val(),
                        "EndDateTime": $("#EndDateTime").val(),
                        "SORTORDER": $("#SORTORDER").val(),
                        "imgGuid": $("#imgGuid").val(),
                        "TARGETTYPE": contentType,
                        "fileGuid": $("#fileGuid").val(),
                        "targetContentStr": $("#TARGETCONTENT").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("廣告送審成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B501";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
            
         	// 上傳廣告類型-檔案
            function onUploadFile() {
                var fileData = new FormData();
                var file = $("#TARGETFILENAME").get(0);

                if ( file.files.length == 0 ) {
                    alert("請選擇上傳檔案");
                    return;
                } else {
                    fileData.append("multipartFile", file.files[0]);
                }

                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B501/UploadFile",
                    type: "POST",
                    contentType: false,  // Not to set any content header
                    processData: false,  // Not to process data
                    data: fileData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("檔案上傳成功");   
                            $("#fileGuid").val(res.pkey);
                            $("#isTmp").val("true");
                            $("#btnPreviewFile").show();
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.responseText);
                    }
                });
            }
         	
         	// 預覽上傳廣告類型-檔案
            function onPreviewFile() {
                var fileGuid = $("#fileGuid").val();
                window.open("<%= request.getContextPath() %>/B501/PreviewFile/"+fileGuid+"?isTmp="+$("#isTmp").val());
            }
        </script>
    </body>
</html>