<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" CONTENT="-1">
	<meta content='width=device-width, initial-scale=1, maximum-scale=5' name='viewport'>
	<title>臺灣中小企業銀行 新世代網路銀行</title>

	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/reset.css">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/bootstrap-4.1.1.css">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/loadingbox.css">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/footable.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/tbb_common.css">
	<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/bootstrap-4.1.1.min.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/popper-1.14.3.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/fstop.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/footable.min.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/utility.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/TBB.js"></script>

	<style>
	
	/* 大圖 */
	@media screen and (min-width: 992px) {
		.carousel-inner .carousel-item:nth-child(1) {
			background: url('${imgLSrc}');
			background-repeat: no-repeat;
			background-size: cover;
		}
		.carousel-inner .carousel-item:nth-child(2) {
			background: url('<%= request.getContextPath() %>/img/preview/banner-lg-00.png');
			background-repeat: no-repeat;
			background-size: cover;
		}
	}
	/* 中圖 */
	@media screen and (min-width: 767px) and (max-width: 992px) {
		.carousel-inner .carousel-item:nth-child(1) {
			background: url('${imgMSrc}');
			background-repeat: no-repeat;
			background-size: cover;
		}
		.carousel-inner .carousel-item:nth-child(2) {
			background: url('<%= request.getContextPath() %>/img/preview/banner-md-00.png');
			background-repeat: no-repeat;
			background-size: cover;
		}
	}
	/* 小圖 */
	@media screen and (max-width: 767px) {
		.carousel-inner .carousel-item:nth-child(1) {
			background: url('${imgSSrc}');
			background-repeat: no-repeat;
			background-size: cover;
		}
		.carousel-inner .carousel-item:nth-child(2) {
			background: url('<%= request.getContextPath() %>/img/preview/banner-sm-00.png');
			background-repeat: no-repeat;
			background-size: cover;
		}
	}
	</style>
</head>

<body>
	<!-- LOADING遮罩區塊 -->
	<div id="loadingBox" class="Loading_box">
		<div class="sk-fading-circle">
			<div class="sk-circle1 sk-circle"></div>
			<div class="sk-circle2 sk-circle"></div>
			<div class="sk-circle3 sk-circle"></div>
			<div class="sk-circle4 sk-circle"></div>
			<div class="sk-circle5 sk-circle"></div>
			<div class="sk-circle6 sk-circle"></div>
			<div class="sk-circle7 sk-circle"></div>
			<div class="sk-circle8 sk-circle"></div>
			<div class="sk-circle9 sk-circle"></div>
			<div class="sk-circle10 sk-circle"></div>
			<div class="sk-circle11 sk-circle"></div>
			<div class="sk-circle12 sk-circle"></div>
		</div>
	</div>

	<div class="row m-0">
		<div class="col-lg-8 col-md-12 p-0">
			<!-- beLongin RWD -->
			<div class="belongin-header">
				<img class="mobile-logo" src="<%= request.getContextPath() %>/img/preview/logo-white.png">
				<!-- 多語系選項 -->
				<select id="selectLg" class="multi-lang-select" onchange="location = this.value">
				</select>
			</div>
			<div class="login-top">
				<div class="header-area">
					<div class="header-ele">
						<div class="ele-info">
							<img src="<%= request.getContextPath() %>/img/preview/icon-announcement.png">
							<!--重要公告-->
							<div class="header-info">
								重要公告
							</div>
							<div class="header-depiction">
								<marquee scrollamount="4" style="vertical-align: middle">
									每日凌晨3點系統維護，暫停交易10分鐘
								</marquee>
							</div>
						</div>
						<div class="ele-info">
							<img src="<%= request.getContextPath() %>/img/preview/icon-announcement.png">
							<!--最新資訊-->
							<div class="header-info">
								最新資訊
							</div>
							<div class="header-depiction">
								<marquee scrollamount="4" style="vertical-align: middle">
									本次新世代網路銀行新增英文版、簡體中文版語系供客戶選擇
								</marquee>
							</div>
						</div>
						<div class="header-more">
							<img src="<%= request.getContextPath() %>/img/preview/icon-more-zh_TW.svg">
						</div>
					</div>
				</div>
			</div>

			<div class="login-banner">
				<div style="position: absolute;left:0;top: 41px;z-index: 99999;" id="showRotate"><input type="button" class="btn btn-info" value="上下顛倒" onclick="onFlip();"></div>
				<div id="photo-carousel" class="banner-slogan carousel slide" data-ride="carousel">
					<!-- 輪播圖片-->
					<div class="carousel-inner">
						<!-- 預設圖片 -->
						<div class="carousel-item active">
							<img class="d-block">
						</div>
						<!-- 其他相片 -->
						<div class="carousel-item">
							<img class="d-block">
						</div>
					</div>
					<form id="formId1" method="post" action="#">
						<input name="ACRADIO" type="hidden" value="" />
						<!-- 需要變更密碼 -->
						<!-- beLongin RWD -->
						<div class="mobile-before-login">
							<!-- 身分證字號/統一編號 -->
							<div class="login-input-block">

								<input type="text" id="cusidnM" name="cusidn" placeholder="身分證字號/統一編號" maxlength="10"
									autocomplete="off" >
							</div>
							<!-- 使用者名稱 -->
							<div class="login-input-block">

								<input type="text" id="userNameM" name="userName" placeholder="使用者名稱" maxlength="16"
									autocomplete="off">
							</div>
							<!-- 簽入密碼 -->
							<div class="login-input-block">

								<input type="password" id="pwM" name="password" placeholder="簽入密碼" maxlength="8"
									autocomplete="off">
								<button type="button" class="question-mark" tabindex="-1"></button>
							</div>
							<!-- 驗證碼那一塊的程式 -->
							<div class="login-input-block">

								<input id="capCode1" type="text" class="veri-code-input" name="capCode"
									placeholder="驗證碼" maxlength="4" autocomplete="off">
								<img src="<%= request.getContextPath() %>/img/preview/verifycode.jpeg">
								<button class="code-update" type="button" name="reshow"></button>
							</div>
							<div>
								<input id="login1" class="login-btn" type="button" value="登入" />
							</div>
						</div>
					</form>
					<!-- 相片選擇鈕 -->
					<ol class="carousel-indicators">
						<li data-target="#photo-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#photo-carousel" data-slide-to="1"></li>
					</ol>
					<!-- 相片瀏覽鈕 -->
					<a class="carousel-control-prev" href="#photo-carousel" role="button" data-slide="prev"> <span
							class="carousel-control-prev-icon" aria-hidden="true"></span> <span
							class="sr-only">Previous</span>
					</a> <a class="carousel-control-next" href="#photo-carousel" role="button" data-slide="next"> <span
							class="carousel-control-next-icon" aria-hidden="true"></span> <span
							class="sr-only">Next</span>
					</a>
				</div>
				<div class="moblie-login-menu">
					<ul style="color: #fff">
						<li>
							<!-- 臺灣企銀首頁 -->
							<a href="https://www.tbb.com.tw/" target="_blank" role="button">
								臺灣企銀首頁
							</a>

						</li>
						<li><a href="https://portal.tbb.com.tw/tbbportal/CustomerLogin.jsp" target="_blank"
								role="button">
								<!-- 網路銀行 -->網路銀行
							</a></li>
						<li><a href="#" target="_blank" role="button">
								<!-- 網站導覽 -->網站導覽
							</a></li>
						<li>
							<!-- 意見信箱 -->
							<a href="https://www.tbb.com.tw/-47" target="_blank" role="button"> 意見信箱
							</a>

						</li>
					</ul>
				</div>
			</div>
			<div class="login-icon">
				<div class="icon-arrow">
					<img src="<%= request.getContextPath() %>/img/preview/arrow-left.png">
				</div>
				<div class="element-area">
					<!-- 新手上路 -->
					<div class="element-item">
						<div>
							<a href="https://portal.tbb.com.tw/tbbportal/fstop/jsp/beginner.jsp" target="_blank"> <img
									src="<%= request.getContextPath() %>/img/preview/icon-01.svg">
							</a>
						</div>
						<div>
							新手上路
						</div>
					</div>
					<!-- 常見問題 -->
					<div class="element-item">
						<div>
							<a href="https://portal.tbb.com.tw/tbbportal/CustomerLogin.jsp" target="_blank"> <img
									src="<%= request.getContextPath() %>/img/preview/icon-02.svg">
							</a>
						</div>
						<div>
							常見問題
						</div>
					</div>
					<!-- 網銀申辦 / 一般網銀 線上申請(原) -->
					<div class="element-item">
						<div>
							<a href="#" onclick="fstop.getPage('/nb3'+'/ONLINE/APPLY/online_apply_menu','', '')"> <img
									src="<%= request.getContextPath() %>/img/preview/icon-03.svg">
							</a>
						</div>
						<div>
							線上申請
						</div>
					</div>
					<!-- 數位存款 -->
					<div class="element-item">
						<div>
							<a href="http://classic-web.tbb.com.tw/resinter/H57/tbb1108/tbb/index.html" target="_blank">
								<img src="<%= request.getContextPath() %>/img/preview/icon-04.svg">
							</a>
						</div>
						<div>
							數位存款
						</div>
					</div>
					<!-- 金融資訊 -->
					<div class="element-item">
						<div>
							<a href="https://portal.tbb.com.tw/tbbportal/fstop/html/information.html" target="_blank">
								<img src="<%= request.getContextPath() %>/img/preview/icon-05.svg">
							</a>
						</div>
						<div>
							金融資訊
						</div>
					</div>
					<!-- 檔案下載 -->
					<div class="element-item">
						<div>
							<a href="https://portal.tbb.com.tw/tbbportal/fstop/html/downloads.html" target="_blank">
								<img src="<%= request.getContextPath() %>/img/preview/icon-06.svg">
							</a>
						</div>
						<div>
							檔案下載
						</div>
					</div>
				</div>
				<div class="icon-arrow">
					<img src="<%= request.getContextPath() %>/img/preview/arrow-right.png">
				</div>
			</div>
			<div class="mobile-icon">
				<div class="icon-arrow">
					<img src="<%= request.getContextPath() %>/img/preview/arrow-left.png">
				</div>
				<div class="element-area">
					<div class="element-row-1">
						<!-- 新手上路 -->
						<div class="element-item">
							<div>
								<a href="https://portal.tbb.com.tw/tbbportal/fstop/jsp/beginner.jsp" target="_blank">
									<img src="<%= request.getContextPath() %>/img/preview/icon-01.svg">
								</a>
							</div>
							<div>
								新手上路
							</div>
						</div>
						<!-- 常見問題 -->
						<div class="element-item">
							<div>
								<a href="https://portal.tbb.com.tw/tbbportal/CustomerLogin.jsp" target="_blank"> <img
										src="<%= request.getContextPath() %>/img/preview/icon-02.svg">
								</a>
							</div>
							<div>

							</div>
						</div>
						<!-- 網銀申辦 / 一般網銀 線上申請(原) -->
						<div class="element-item">
							<div>
								<a href="https://portal.tbb.com.tw/tbbportal/fstop/jsp/ApplyOnLine.jsp" target="_blank">
									<img src="<%= request.getContextPath() %>/img/preview/icon-03.svg">
								</a>
							</div>
							<div>
								線上申請
							</div>
						</div>
					</div>
					<div class="element-row-2">
						<!-- 數位存款 -->
						<div class="element-item">
							<div>
								<a href="http://classic-web.tbb.com.tw/resinter/H57/tbb1108/tbb/index.html"
									target="_blank"> <img src="<%= request.getContextPath() %>/img/preview/icon-04.svg">
								</a>
							</div>
							<div>
								數位存款
							</div>
						</div>
						<!-- 金融資訊 -->
						<div class="element-item">
							<div>
								<a href="https://portal.tbb.com.tw/tbbportal/fstop/html/information.html"
									target="_blank"> <img src="<%= request.getContextPath() %>/img/preview/icon-05.svg">
								</a>
							</div>
							<div>

							</div>
						</div>
						<!-- 檔案下載 -->
						<div class="element-item">
							<div>
								<a href="https://portal.tbb.com.tw/tbbportal/fstop/html/downloads.html" target="_blank">
									<img src="<%= request.getContextPath() %>/img/preview/icon-06.svg">
								</a>
							</div>
							<div>
								檔案下載
							</div>
						</div>
					</div>
				</div>
				<div class="icon-arrow">
					<img src="<%= request.getContextPath() %>/img/preview/arrow-right.png">
				</div>
			</div>
			<div class="login-news">
				<div class="news-area">
					<div class="news-img">
						<a href="${imgURLC}"><img
								src="${imgCSrc}"></a>
					</div>
					<div class="news-info">
						<div class="news-day"></div>
						<div class="news-content">
							${imgNews}
						</div>
					</div>
				</div>
				<div class="news-area">
					<div class="news-img">
						<a href="#"><img src="<%= request.getContextPath() %>/img/preview/photo-00.png"></a>
					</div>
					<div class="news-info">
						<div class="news-day"></div>
						<div class="news-content">
							小廣告樣張
						</div>
					</div>
				</div>
			</div>
			<div class="news-more">
				<img src="<%= request.getContextPath() %>/img/preview/icon-more-zh_TW.svg">
			</div>
			<div class="login-bottom">
				<!-- 版權所有 2018 臺灣中小企業銀行 Taiwan Business Bank -->版權所有 2018 台灣中小企業銀行
			</div>
		</div>
		<div class="lg-login p-0 col-lg-4">
			<!-- 多語系選擇 -->
			<div class="multi-lang-select" onchange="location = this.value">
				<ul>
					<li><a>繁</a></li>
					<li><a>简</a></li>
					<li><a>En</a></li>
				</ul>
			</div>
			<img class="login-logo" src="<%= request.getContextPath() %>/img/preview/logo-white@2x.png">
			<div class="lang-switch">
				<a href="#"> 網路銀行</a> 
				<a href="https://nnb.tbb.com.tw/TBBeATMAppsWeb/" target="_blank" role="button">網路ATM</a>
			</div>
			<form id="formId" method="post" action="/nb3/INDEX/index">
				<input name="ACRADIO" type="hidden" value="" />
				<!-- 需要變更密碼 -->
				<div class="login-input-block">
					<!-- 身分證字號/統一編號 -->
					<input type="text" id="cusidn" name="cusidn" placeholder="身分證字號/統一編號" maxlength="10" autocomplete="off" >
				</div>
				<div class="login-input-block">
					<!-- 使用者名稱 -->
					<input type="text" id="userName" name="userName" placeholder="使用者名稱" maxlength="16" autocomplete="off">
				</div>
				<div class="login-input-block">
					<!-- 簽入密碼 -->
					<input type="password" id="webpw" name="password" placeholder="簽入密碼" maxlength="8" autocomplete="off">
					<button type="button" class="question-mark" tabindex="-1"></button>
				</div>
				<!-- 驗證碼那一塊的程式 -->
				<div class="login-input-block">
					<input id="capCode" type="text" class="veri-code-input" name="capCode" placeholder="驗證碼" maxlength="4" autocomplete="off">
					<img name="kaptchaImage" src="" />
					<button class="code-update" type="button" name="reshow"></button>
				</div>
				<div>
					<input id="login" class="login-btn" type="button" value="登入" />
				</div>
			</form>

			<div class="login-menu">
				<ul style="color: #fff">
					<li>
						<!-- 臺灣企銀首頁 -->
						<a href="https://www.tbb.com.tw/" target="_blank" role="button">臺灣企銀首頁</a>
					</li>
					<li>
						<!-- 網路銀行 -->
						<a href="https://portal.tbb.com.tw/tbbportal/CustomerLogin.jsp" target="_blank"
							role="button">網路銀行</a>
					</li>
					<li>
						<!-- 網站導覽 -->
						<a href="/nb3/login/sitemap" target="_blank" role="button"> 網站導覽
						</a>
					</li>
					<li>
						<!-- 意見信箱 -->
						<a href="https://www.tbb.com.tw/-47" target="_blank" role="button"> 意見信箱</a>
					</li>
				</ul>
			</div>
			<div class="enterprise-info">
				<p>電話 0800-01-7171、(02)2357-7171#3</p>
				<!-- 電話 -->
				<p>基金 (02)-2559-7171#5461~5463</p>
				<!-- 基金 -->
				<p>香港 2971-0111</p>
				<!-- 香港 -->
				<p>建議瀏覽器版本:IE11或更新版本。建議最佳解析度：1280x768</p>
				<!-- IE瀏覽器建議版本11或更新版本。-->
				<!--最佳解析度1024x768 -->
			</div>
		</div>
		<nav class="footer-breadcrumb-nav">
			<ul>
				<div>
					<!-- 環境設定(原) -->
					<li><a href="https://portal.tbb.com.tw/tbbportal/fstop/jsp/envsetup_NB.jsp?QTYPE=GETTOTAL"
							target="_blank"> 環境設定
						</a></li>
					<!-- 網路安全 -->
					<li><a href="https://portal.tbb.com.tw/tbbportal/fstop/html/webSecurity.html" target="_blank"> 網路安全
						</a></li>
					<!-- 錯誤代碼 -->
					<li><a href="https://portal.tbb.com.tw/tbbportal/fstop/html/NNB_QA.html" target="_blank"> 錯誤代碼
						</a></li>
				</div>
				<div class="top-button">
					<img src="<%= request.getContextPath() %>/img/preview/icon-top-zh_TW.svg">
				</div>
				<!--<button type="button"></button> -->
			</ul>
		</nav>
		<div class="mobile-enterprise-info">
			<div class="enterprise-info-part-1">
				<div>
					<img src="<%= request.getContextPath() %>/img/preview/logo-white.png">
				</div>
				<div class="contact-info">
					<p>電話0800-01-7171、(02)2357-7171#3</p>
					<!-- 電話 -->
					<p>基金(02)2559-7171#5461~5463</p>
					<!-- 基金 -->
					<p>香港2971-0111</p>
					<!-- 香港 -->
				</div>
			</div>
			<div class="enterprise-info-part-2">
				<p>建議瀏覽器版本：IE11或更新版本。</p>
				<!-- 建議瀏覽器版本 : IE11或更新版本。 -->
				<p>建議最佳解析度：1280x768</p>
				<!-- 建議解析度： -->
				<p>版權所有 2018 台灣中小企業銀行</p>
				<!-- 版權所有 2018 臺灣中小企業銀行 Taiwan Business Bank -->
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function () {
			if ( "${showRotate}" == "false" ) {
				$("#showRotate").hide();
			}
		});
		function onFlip() {
			if ( !confirm("上下顛倒廣告?") ) {
				return;
			}
			
			$.ajax({
                url: "<%= request.getContextPath() %>/B501/Flip/${id}",
                type: "POST",
                data: {
                },
                success: function (rt) {
                    if(rt=="0"){
                    	location.reload();
                    } else {
                        alert(rt);
                    }
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
		}
	</script>
</body>

</html>