<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>
    
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>廣告管理-覆核</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >廣告標題：</label>
                    <label class="col-10 control-label">${Data.TITLE}</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >廣告內容：</label>
                    <label class="col-10 control-label">${Data.CONTENT}</label>
                </div>
                <div class="form-group row">
                    <label id="lblFileL" class="col-2 control-label text-right" >橫幅廣告(大版)：</label>
                    <label class="col-10 control-label">${Data.FILENAMEL}</label>
                </div>
                <div class="form-group row">
	           	 	<label class="col-2 control-label text-right">廣告連結類型：</label>
	            	<c:choose>
	           			<c:when test="${Data.TARGETTYPE=='1'}">
			                <label class="col-10 control-label">網頁連結</label>
		               	</c:when>
		               	<c:when test="${Data.TARGETTYPE=='2'}">
			                <label class="col-10 control-label">檔案</label>
		               	</c:when>
		               	<c:when test="${Data.TARGETTYPE=='3'}">
			                <label class="col-10 control-label">文字敘述</label>
		               	</c:when>
		            </c:choose>
	            </div>
	            <div class="form-group row">
	           	 	<c:choose>
	           			<c:when test="${Data.TARGETTYPE=='1'}">
	           				<label class="col-2 control-label text-right">網頁連結：</label>
			                <label class="col-10 control-label">${Data.URL}</label>
		               	</c:when>
		               	<c:when test="${Data.TARGETTYPE=='2'}">
			                <label class="col-2 control-label text-right">檔案：</label>
			                <label class="col-10 control-label">${Data.TARGETFILENAME}</label>
		               	</c:when>
		               	<c:when test="${Data.TARGETTYPE=='3'}">
			                <label class="col-2 control-label text-right">文字敘述：</label>
			                <label class="col-10 control-label">${targetContentStr}</label>
		               	</c:when>
		            </c:choose>
	            </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >排序：</label>
                    <label class="col-10 control-label">${Data.SORTORDER}</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >上架日期：</label>
                    <label class="col-4 control-label">${StartDateTime}</label>
                    <label class="col-2 control-label text-right" >下架日期：</label>
                    <label class="col-4 control-label">${EndDateTime}</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >退回原因：</label>
                    <div class="col-10">
                        <input type="text" id="Comments" name="Comments" class="form-control" maxlength="50" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;
                        <input id="btnPreview" type="button" class="btn btn-info" value="預覽" onclick="onPreview();" />&nbsp;                        
                        <input id="btnReject" type="button" value="退回" class="btn btn-danger" aria-label="Left Align" onclick="onReject()" />&nbsp;
                        <input id="btnApprove" type="button" value="覆核" class="btn btn-info" aria-label="Left Align" onclick="onApprove()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script>
            $(document).ready(function () {
            	if ("${Data.TYPE}" == "C") {
                    $("#lblFileL").text("小廣告：");
                } else if ("${Data.TYPE}" == "M") { 
                	$("#lblFileL").text("APP 廣告：");
                }
            }); //ready

            // 回查詢頁
            function onExit() {
                location.href="<%= request.getContextPath() %>/B501";
            }

            // 預覽廣告
            function onPreview() {
                window.open("<%= request.getContextPath() %>/B501/CasePreview/${Data.ID}");
            }

            // 同意
            function onApprove() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B501/Approve/${Data.ID}",
                    type: "POST",
                    data: {
                        "StepId":"${Data.STEPID}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("廣告覆核成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B501";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            // 退回
            function onReject() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B501/Reject/${Data.ID}",
                    type: "POST",
                    data: {
                        "StepId":"${Data.STEPID}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("廣告退回成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B501";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
        </script>
    </body>
</html>