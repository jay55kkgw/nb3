<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<style id="bg-for-banner"></style>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" CONTENT="-1">
<meta content='width=device-width, initial-scale=1, maximum-scale=5' name='viewport'>
<title>臺灣中小企業銀行 新世代網路銀行</title>

<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/reset.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/bootstrap-4.1.1.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/loadingbox.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/footable.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/tbb_common.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/hamburgers.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/fonts.css">
<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/jquery-3.3.1.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/bootstrap-4.1.1.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/popper-1.14.3.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/fstop.js"></script>
<!-- <script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/footable.min.js"></script> -->
<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/utility.js"></script>

<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/TBB-zh_TW.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/fixedColumns.dataTables.min.css">
<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/dataTables.fixedColumns.min.js"></script>
<!-- masonry -->
<script src="<%= request.getContextPath() %>/script/preview/isotope.pkgd.min.js"></script> <!-- add by TBB PoChun Chen -->
<script src="<%= request.getContextPath() %>/script/preview/fit-columns.js"></script> <!-- add by TBB PoChun Chen -->
<script>
var dt_locale = "zh_TW";
</script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/preview/keyboard.css">
<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/plugins_login.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/script/preview/loginAds.js"></script>
<script type="text/javascript">
	// 載入前準備顯示Banner廣告
	var bannerImgs = '0'; // banner廣告數量
	showBanner(bannerImgs, '/nb3'); // 顯示Banner廣告
	
	// 被踢退後進入登入頁的提示訊息
	var errorMsg = "";
	if(errorMsg == 'logout'){
		//<!-- 此帳號已從其他位置登入 -->
		//alert("此帳號已從其他位置登入");
		errorBlock(
				null, 
				null,
				["此帳號已從其他位置登入"], 
				'離開', 
				null
			);
	}
	
	// 初始資料
	var uri = "";
	var rdata, blockId;

	// 載入後初始化JS
	$(document).ready(function() {
		//initBlockUI();	// HTML載入完成後0.1秒開始遮罩
		//setTimeout("init()", 100);					// 初始化
		//setTimeout("cleanTmp()", 200); // 清session
		//setTimeout("getBulletin()", 300);			// 公告訊息
		//setTimeout("unBlockUI(initBlockId)", 500);	// 過0.5秒解遮罩
				
	});
	
	// 清資料
	function cleanTmp() {
		fstop.getServerDataEx('/nb3'+'/logout_aj',null,false);
	}
	
	// 初始化
	function init() {
		dynamicKeyboard();	// 初始化動態鍵盤
		KeyBoardF();		// 讓動態鍵盤不指定浮標也可以輸入
		initKapImg();		// 初始化驗證碼
		newKapImg();		// 生成驗證碼
		autoExit();			// 關閉窗口時自動退出
		loginInit();		// 登入按鈕觸發
		enterClick();		// CLICK ENTER KEY
		$("#cusidn").focus();
	}

	// 紀錄上次focus的欄位讓動態鍵盤不指定浮標也可以輸入
	var k_focus = null;
	function KeyBoardF() {
		$( "#cusidn" ).focus(function() {
			k_focus = $( "#cusidn" );
			});
		$( "#userName" ).focus(function() {
			k_focus = $( "#userName" );
			});
		$( "#webpw" ).focus(function() {
			k_focus = $( "#webpw" );
			});
		$( "#capCode" ).focus(function() {
			k_focus = $( "#capCode" );
			});
	}
	
	// 遮罩關閉時重新focus
	$("#errorBtn1").click(function() {
		$('#error-block').hide();
		if(k_focus) {
			k_focus.focus();
		}
	});
	
	// 初始化BlockUI
	function initBlockUI() {
		initBlockId = blockUI();
	}

	// 顯示後端回傳訊息
	function showData(data) {
		console.log("data: " + data);
		if (data) {
			// login_aj回傳資料
			console.log("data.json: " + JSON.stringify(data));
			
			// LOCAL開發
			if("local" == data.msgCode) {
					$("#formId").attr("action", "/nb3" + "/INDEX/index");
					$("#formId").submit();
					return; // 不加return雖然會submit，但還是會往下執行			
			}
			
			// 測試環境
			if("TEST" == data.msgCode) {	
					$("#formId").attr("action", "/nb3" + "/INDEX/index");
					$("#formId").submit();
					return; // 不加return雖然會submit，但還是會往下執行		
			}

			console.log("data.msgCode: " + data.msgCode);
			console.log("data.result: " + data.result);
			
			// 正式環境
			if ("0" == data.msgCode) {
				// 登入成功
				var target = data.next; // form action
				console.log("target: " + target);
				var acradio = data.previous; // 需變更簽入密碼或是交易密碼
				console.log("acradio: " + acradio);
				
				// 發mail通知登入網銀成功，非同步再submit會導致session更新資料庫重複Id的exception，故到首頁再發
// 				sendmail("login", "true");
				
				// 送交表單
				processQuery(target, acradio);

			} else {
				// 登入失敗
				errorBlock(
					null, 
					null,
					["訊息代號: " + data.msgCode , "\n" , "訊息說明: " + data.message], 
					'離開', 
					null
				);
				
				// 清空輸入欄位前發mail通知登入網銀失敗
				sendmail("login", "false");
				
				clearForm(); // 清空輸入欄位
				changeCode(); // 變更驗證碼
				loginInit() // 避免重複送出
				
			}
		}
	}

	// 發mail通知登入網銀
	function sendmail(type, result) {
		var sendmail_aj_uri = '/nb3' + "/SEND/MAIL/sendmail_aj";
		console.log("sendmail_aj.uri: " + sendmail_aj_uri);
		
		var rdata = {cusidn: $("#cusidn").val(), type: type, result: result};

		fstop.getServerDataEx(sendmail_aj_uri, rdata, true);
	}
	
	
	// 送交表單
	function processQuery(target, acradio) {
		console.log("processQuery...");

		$('input[name="ACRADIO"]').val(acradio); // 判斷該導向變更簽入密碼或是交易密碼
		$("#formId").attr("action", "/nb3" + target);
		$("#formId").submit();	
	}
	
	// 刷新驗證碼
	function refreshCapCode() {
		console.log("refreshCapCode...");

		// 驗證碼
		$('input[name="capCode"]').val('');
		
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '/nb3' + '/CAPCODE/captcha_image?' + Math.floor(Math.random() * 100)).fadeIn();
		
		// 登入失敗解遮罩
		unBlockUI(blockId);
	}
	
	// 刷新輸入欄位
	function changeCode() {
		console.log("changeCode...");
		// 刷新驗證碼
		refreshCapCode();
	}

	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "/nb3/CAPCODE/captcha_image");
	}

	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			refreshCapCode();
		});
	}

	// 關閉窗口時自動退出  
	function autoExit() {
		window.onbeforeunload = function(event) {
			if (event.clientX > 360 && event.clientY < 0 || event.altKey) {
				//alert(parent.document.location);
				errorBlock(
						null, 
						null,
						[parent.document.location], 
						'離開', 
						null
					);
			}
		};
	}

	// 登入觸發機制
	function loginInit() {
		// for web
		$("#login").click(function() {
			$('#login').off('click'); // 避免重複送出
			if(form_validate($("#cusidn"), $("#userName"), $("#webpw"))){
				login();
			}
		});
	}

	// 表單驗證
	function form_validate(cusidn, username, pw){
		// 合法字元
		var gACCOUNT_CHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_#";
		
		// 身份證/營利事業統一編號
		if (!jsChkString2(cusidn, "身份證/營利事業統一編號", gACCOUNT_CHAR, true)) {
			loginInit() // 避免重複送出
			return false;
		}
		// 使用者名稱驗證
		if (!jsChkString2(username, "使用者名稱", gACCOUNT_CHAR, true)) {
			loginInit() // 避免重複送出
			return false;
		}
		// 簽入密碼驗證
 		if (!chkPassword(pw, "簽入密碼", true)) {
 			loginInit() // 避免重複送出
 			return false;
 		}
		
 		return true;
	}

	// CLICK ENTER KEY
	function enterClick() {
		$(document).keypress(function(event) {
			// 觸發回車鍵(手機也有)
			if (event.which == 13) {
				console.log("enterClick...");
				
				// 沒有遮罩下ENTER即送交
				if ( $('#error-block').is(":hidden") ){
					$("#login").click();
					
				} // 有遮罩ENTER即關閉遮罩
				else {
					$('#error-block').hide();
				}
			}
		});
	}
	var confirmType=0;
	$("#errorBtn1").click(function(){
		if(confirmType == 0){
			return false;
		}
		else if(confirmType == 1){
			logoutforce();
		}
		else if(confirmType == 2){
			kickPortal_aj();
		}
		else if(confirmType == 3){
			logoutAll();
		}
		confirmType = 0;
		
	});
	$("#errorBtn2").click(function(){
		if(confirmType == 0){
			return false;
		}
		else if(confirmType == 1){
			unBlockUI(blockId); // 取消登入解遮罩
			loginInit() // 避免重複送出
		}
		else if(confirmType == 2){
			unBlockUI(blockId); // 取消登入解遮罩
			loginInit() // 避免重複送出
		}
		else if(confirmType == 3){
			unBlockUI(blockId); // 取消登入解遮罩
			loginInit() // 避免重複送出
		}
		confirmType = 0;
		$('#error-block').hide();
	});
	// 登入
	function login() {
		blockId = blockUI();

		var capResult = true;
		
		if('true'){
			console.log("isTest");
			
		} else {
			console.log("notTest");
			var capCheck = checkCapCode();
			// 驗證碼為空
			if(!capCheck) {
				//alert("請輸入驗證碼");
				errorBlock(
					null, 
					null,
					["請輸入驗證碼"], 
					'離開', 
					null
				);
				// 驗證碼未輸入或有誤，需清空使用者名稱、簽入密碼、驗證碼欄位
				$('input[name="userName"]').val('');	// 使用者名稱
				$('input[name="password"]').val('');	// 簽入密碼
				$('#userName').focus();
				changeCode();
				loginInit() // 避免重複送出
				return;
			}
			
			// 驗證失敗
			if(!capCheck.result) {
				capResult = false;
			}
			
		}
		
		// 驗證通過
		if(capResult) {
			var checkResult = checkLogin_aj();
			var checkPortalResult = false;
// 			var checkPortalResult = checkPortal_aj();
			
			// 檢查是否有重複登入需要後踢前
			if(checkResult.result && !checkPortalResult.result){
				// 可登入
				login_aj();
				
			} // 只有新世代網路銀行重複登入
			else if (!checkResult.result && !checkPortalResult.result) {
				
				confirmType = 1;
				errorBlock(
							null, 
							null,
							['重複登入是否繼續登入'], 
							'確定', 
							'取消'
						);
				// 重複登入詢問是否後踢前
// 				if(confirm("重複登入是否繼續登入")){
// 					// 先做強迫登出再做登入
// 					logoutforce();
// 				} else {
// 					unBlockUI(blockId); // 取消登入解遮罩
// 					loginInit() // 避免重複送出
// 				}
				
			} // 只有Portal重複登入
			else if (checkResult.result && checkPortalResult.result) {
				confirmType = 2;
				errorBlock(
							null, 
							null,
							['重複登入是否繼續登入'], 
							'確定', 
							'取消'
						);
				// 重複登入詢問是否後踢前
// 				if(confirm("PORTAL"+"重複登入是否繼續登入")){
// 					// 先做強迫登出再做登入
// 					kickPortal_aj();
// 				} else {
// 					unBlockUI(blockId); // 取消登入解遮罩
// 					loginInit() // 避免重複送出
// 				}
				
			} // 新世代網路銀行及Portal皆重複登入
			else if (!checkResult.result && checkPortalResult.result) {
				confirmType = 3;
				errorBlock(
							null, 
							null,
							["多個系統"+'重複登入是否繼續登入'], 
							'確定', 
							'取消'
						);
				// 重複登入詢問是否後踢前
// 				if(confirm("多個系統"+"重複登入是否繼續登入")){
// 					// 先做強迫登出再做登入
// 					logoutAll();
// 				} else {
// 					unBlockUI(blockId); // 取消登入解遮罩
// 					loginInit() // 避免重複送出
// 				}
				
			}
			
		} // 驗證失敗
		else {
			//alert(capResult.message);
			errorBlock(
				'驗證碼錯誤', 
				null,
				['請輸入正確的驗證碼。'], 
				'重新輸入', 
				null
			);
			// 驗證碼未輸入或有誤，需清空使用者名稱、簽入密碼、驗證碼欄位
			$('input[name="userName"]').val('');	// 使用者名稱
			$('input[name="password"]').val('');	// 簽入密碼

			$('#userName').focus();
			
			refreshCapCode(); // 不刷新驗證碼會有Session過期的問題

			loginInit() // 避免重複送出
		}
	}

	// 先做強迫登出再做登入
	function logoutAll(){
		var logoutforce_uri = '/nb3' + "/logoutforce_aj";
		var rdata;
		console.log("logoutforce_aj.uri: " + logoutforce_uri);
		rdata = $("#formId").serializeArray();	
		var logoutforce_result = fstop.getServerDataEx(logoutforce_uri, rdata, false);
		console.log("logoutforce_result: " + JSON.stringify(logoutforce_result) );
		
		// 強制登出成功後做Portal登出再登入
		if(logoutforce_result.result){
			kickPortal_aj();
			
		} else {
			//alert("error: " + logoutforce_result.message);
			errorBlock(
				null, 
				null,
				["error: " + logoutforce_result.message], 
				'離開', 
				null
			);
			unBlockUI(blockId); // 取消登入解遮罩
			loginInit() // 避免重複送出
		}
	}
	
	// 先做強迫登出再做登入
	function logoutforce(){
		var logoutforce_uri = '/nb3' + "/logoutforce_aj";
		var rdata;
		console.log("logoutforce_aj.uri: " + logoutforce_uri);
		rdata = $("#formId").serializeArray();	
		var logoutforce_result = fstop.getServerDataEx(logoutforce_uri, rdata, false);
		console.log("logoutforce_result: " + JSON.stringify(logoutforce_result) );
		
		// 強制登出成功後做登入
		if(logoutforce_result.result){
			login_aj();
			
		} else {
			//alert("error: " + logoutforce_result.message);
			errorBlock(
				null, 
				null,
				["error: " + logoutforce_result.message], 
				'離開', 
				null
			);
			unBlockUI(blockId); // 取消登入解遮罩
			loginInit() // 避免重複送出
		}
	}
	
	// 檢查是否需要後踢前
	function checkLogin_aj(){
		var rdata;
		var checkLogin_aj_uri = '/nb3' + "/checkLogin_aj";
		console.log("checkLogin_aj.uri: " + checkLogin_aj_uri);
		
		rdata = $("#formId").serializeArray();

		var checkResult = fstop.getServerDataEx(checkLogin_aj_uri, rdata, false);
		console.log("checkResult: " + JSON.stringify(checkResult) );

		return checkResult;
	}
	
	// 檢查是否需要後踢前--Portal
	function checkPortal_aj(){
		var rdata;
		var checkPortal_aj_uri = '/nb3' + "/PORTAL/getportalstatus";
		console.log("checkPortal_aj.uri: " + checkPortal_aj_uri);
		
		rdata = {
			cusidn : $("#cusidn").val(),
			account : $("#userName").val()
		};

		var checkResult = fstop.getServerDataEx(checkPortal_aj_uri, rdata, false);
		console.log("checkResult: " + JSON.stringify(checkResult) );

		return checkResult;
	}
	
	// 先做登出Portal再做登入
	function kickPortal_aj(){
		var rdata;
		var kickPortal_aj_uri = '/nb3' + "/PORTAL/kickportal";
		console.log("kickPortal_aj.uri: " + kickPortal_aj_uri);
		
		rdata = {
			cusidn : $("#cusidn").val(),
			account : $("#userName").val()
		};

		var checkResult = fstop.getServerDataEx(kickPortal_aj_uri, rdata, false);
		console.log("checkResult: " + JSON.stringify(checkResult) );
		
		// 登出Portal成功後做登入
		if(checkResult.result){
			login_aj();
			
		} else {
			//alert("error: " + logoutforce_result.message);
			errorBlock(
				null, 
				null,
				["error: " + logoutforce_result.message], 
				'離開', 
				null
			);
			unBlockUI(blockId); // 取消登入解遮罩
			loginInit() // 避免重複送出
		}
	}
	
	// 登入
	function login_aj(){
		var rdata;
		var login_uri = '/nb3' + "/login_aj";
		console.log("login_aj.login_uri: " + login_uri);
		$('#webpw').val( pin_encrypt( $('#webpw').val() ) );
		rdata = $("#formId").serializeArray();
		fstop.getServerDataEx(login_uri, rdata, true, showData);
	}

	// 驗證碼驗證
	function checkCapCode(){
		// 驗證碼驗證
		var capData;
		var capUri = '/nb3' + "/CAPCODE/captcha_valided";
		console.log("checkCapCode.capUri: " + capUri);
		
		// 電腦
		if( $("#capCode").val() != "" ){
			capData = $("#formId").serializeArray();
		} else {
			return false;
		}
		var capResult = fstop.getServerDataEx(capUri, capData, false);
		console.log("chaCode_valid: " + JSON.stringify(capResult) );

		return capResult;
	}
	
	// Escape
	$(document).keydown(function(event){
    	if(event.keyCode == 27) {
    		$('#error-block').hide();
       	}
	});

	// 小版多語系
	$(document).ready(function() {
		i18n();
	});

	// 切換語系
	function i18n() {
		var locale = "zh_TW";
		if (locale == "zh_TW") {
			$("#selectLg").append($("<option></option>").attr("value", "/nb3/login?locale=en").text("En"));
			$("#selectLg").append($("<option></option>").attr("value", "/nb3/login?locale=zh_TW").text("繁"));
			$("#selectLg").append($("<option></option>").attr("value", "/nb3/login?locale=zh_CN").text("简"));
			$("#selectLg").val('/nb3/login?locale=zh_TW');
		} else if (locale == "zh_CN") {
			$("#selectLg").append($("<option></option>").attr("value", "/nb3/login?locale=en").text("En"));
			$("#selectLg").append($("<option></option>").attr("value", "/nb3/login?locale=zh_TW").text("繁"));
			$("#selectLg").append($("<option></option>").attr("value", "/nb3/login?locale=zh_CN").text("简"));
			$("#selectLg").val('/nb3/login?locale=zh_CN');
		} else if (locale == "en") {
			$("#selectLg").append($("<option></option>").attr("value", "/nb3/login?locale=en").text("En"));
			$("#selectLg").append($("<option></option>").attr("value", "/nb3/login?locale=zh_TW").text("繁"));
			$("#selectLg").append($("<option></option>").attr("value", "/nb3/login?locale=zh_CN").text("简"));
			$("#selectLg").val('/nb3/login?locale=en');
		}
	}

	// 清空輸入表單
	function clearForm() {
		$('input[name="cusidn"]').val('');		// 身分證字號/統一編號
		$('input[name="userName"]').val('');	// 使用者名稱
		$('input[name="password"]').val('');	// 簽入密碼
		$("#cusidn").focus();
	}
	
	// 隱藏使用者名稱
	function hideUserName(){
		if($('#hid').prop('checked')){
			$('#userName').attr("type","password");
		}else{
			$('#userName').attr("type","text");
		}
	}
	
	//-------------------------------------------------------------------------------------

	/**
	 * 欄位檢查
	 * obj(form.field): 要檢查的物件
	 * field_hit: 要顯示的提示欄位名
	 * allow_char: 允許的字元
	 * bCheck: 是否檢查空字串(不輸入), true:警告 false:略過
	 */
	function jsChkString2(obj, field_hit, allow_char, bCheck) {
		if (bCheck) {
			if (!jsChkString(obj, field_hit)){
				unBlockUI(blockId); // 登入失敗解遮罩
				return false;
			}
		}

		// 若欄位不存在則傳回true
		var val;
		field_hit = '[' + field_hit + ']';

		try {
			val = fnJSTrim(obj.val());
		} catch (ex) {
			return true;
		}

		var jump = 0;
		var str = '';
		while(val.length > 0) {
			var c = val.substring(0, 1);
			val = val.substring(1, val.length);
			if (allow_char.indexOf(c)<0) {
				str = field_hit + '不合法字元' + c;
				break;
			}
			if (jump++>1000) break;
		}
		return (doAlert(obj, str));
	}

	/**
	 * 去字串空白
	 * obj(form.field): 要檢查的物件
	 * field_hit: 要顯示的提示欄位名
	 */
	function jsChkString(obj, field_hit) {
		var val;
		var str = "";
		field_hit = '[' + field_hit + ']';

		// 若欄位不存在則傳回true
		try {
			val = fnJSTrim(obj.val());
		} catch (ex) {
			return true;
		}

		if (val == 'undefined') {
			str = field_hit + '未定義';
		} else if (val == '') {
			str = '不合法字元' + field_hit + '';
		}

		return (doAlert(obj, str));
	}

	/**
	 * 去字串空白
	 */
	function fnJSTrim(sVar) {
		while (sVar.indexOf(" ")==0) {
		 	sVar = sVar.substring(1, sVar.length);
		}
		while (sVar.indexOf(" ")==sVar.length) {
			sVar = sVar.substring(0, sVar.length-1);
		}
		return sVar;
	}

	/**
	 * 簽入密碼檢核
	 */
	function chkPassword(pw, field_name, engNum) {
		var val;
		var field_name = '[' + field_name + ']';
		var result = true;
		
		var sPass = fnJSTrim(pw.val());
		if(sPass.length < 6 && sPass.length > 0) {
			pw.focus();
			unBlockUI(blockId); // 登入失敗解遮罩
			result = false;
		}
		if(sPass.length >=9 || sPass.length <=5) {
			pw.focus();
			unBlockUI(blockId); // 登入失敗解遮罩
			result = false;
		}	
		if(sPass=='' ) {
			pw.focus();
			unBlockUI(blockId); // 登入失敗解遮罩
			result = false;
		}	
		
		var iCode = 0;
		for(var i=0;i<sPass.length;i++) {
			iCode = sPass.charCodeAt(i);
			if(engNum) {
				if(!((iCode >=48 && iCode <= 57) || (iCode >=65 && iCode <= 90) || (iCode >=97 && iCode <= 122))) {
					pw.focus();
					unBlockUI(blockId); // 登入失敗解遮罩
					result = false;
				}
			}else {
				if(!(iCode >=48 && iCode <= 57)) {
					pw.focus();
					unBlockUI(blockId); // 登入失敗解遮罩
					result = false;
				}	
			}
		}
		
		// 跳出提示字，並將游標停在該欄位上
		if (!result) {
			return doAlert(pw, '密碼錯誤');
		}
		
		return result;
	}

	/**
	 * 跳出提示字，並將游標停在該欄位上
	 */
	function doAlert(obj, str) {
		if (str != '') {
			//alert(str);
			errorBlock(
				null, 
				null,
				[str], 
				'離開', 
				null
			);
			unBlockUI(blockId); // 登入失敗解遮罩
			obj.focus();
			return false;
		}
		return true;
	}
	
	// 取得公告訊息
	function getBulletin(){
		uri = '/nb3'+"/bulletin_aj";
		console.log("bulletin_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, showBulletin);
	}
	
	// 顯示公告訊息
	function showBulletin(data){
		console.log("showBulletin.data: " + JSON.stringify(data));
		if(data && data.result){
			for (i = 0; i < data.data.length; i++) {
				var urlLink = data.data[i].url; // 超連結
				var content = data.data[i].content; // 內容
				
				// 訊息類型--1:重要、0:一般、其他:已讀
				console.log("data.data["+i+"].type: " + data.data[i].type);
				if(data.data[i].type == '1'){
					//訊息公告有設定超連結
					if(urlLink!=''){
						$('[id^=IMP]').append(
							'<li>'
							+ '<a href="' + urlLink + '" target="_blank">' + content + '</a>'
							+ '</li>'
						);
					}
					else{
						$('[id^=IMP]').append(
							'<li>'
							+ content
							+ '</li>'
						);
					}
				} else if(data.data[i].type == '0'){
					//訊息公告有設定超連結
					if(urlLink!=''){
						$('[id^=GEN]').append(
							  '<li>'
							+ '<a href="' + urlLink + '" target="_blank">' 
							+ '<span>'
							+ content 
							+ '</span>'
							+ '</a>'
							+ '</li>'
						);
					}
					else{
						$('[id^=GEN]').append(
							  '<li>'
							+ '<span>'
							+ content
							+ '</span>'
							+ '</li>'
						);
					}
				} 
			}
			console.log("showBulletin...Finish!!!");
			
		} else {
			console.log("Oops");
		}
	}
	</script>
	
	<style>
	
	@media screen and (min-width: 992px) .carousel-item, .banner-slogan {
	height: calc(100vh - 86px - 288px - 48px);
	}
	@media screen and (min-width: 1440px) .carousel-item, .banner-slogan {
	    height: calc(100vh - 86px - 426px - 48px);
	}
	
	@media screen and (min-width: 992px) .carousel-inner .carousel-item:nth-child(1) {
	    background-position-y: 20%;
	}
	
	
	/* 大圖 */
	@media screen and (min-width: 992px) {
		.carousel-inner .carousel-item:nth-child(1) {
			background: url('${imgLSrc}');
			background-repeat: no-repeat; 
			background-size: cover;
		}
		.carousel-inner .carousel-item:nth-child(2) {
			background: url('<%= request.getContextPath() %>/img/preview/banner-lg-00.png');
			background-repeat: no-repeat;
			background-size: cover;
		}
	}
	/* 中圖 */
	@media screen and (min-width: 767px) and (max-width: 992px) {
		.carousel-inner .carousel-item:nth-child(1) {
			background: url('${imgMSrc}');
			background-repeat: no-repeat;
			background-size: cover;
		}
		.carousel-inner .carousel-item:nth-child(2) {
			background: url('<%= request.getContextPath() %>/img/preview/banner-md-00.png');
			background-repeat: no-repeat;
			background-size: cover;
		}
	}
	/* 小圖 */
	@media screen and (max-width: 767px) {
		.carousel-inner .carousel-item:nth-child(1) {
			background: url('${imgSSrc}');
			background-repeat: no-repeat;
			background-size: cover;
		}
		.carousel-inner .carousel-item:nth-child(2) {
			background: url('<%= request.getContextPath() %>/img/preview/banner-sm-00.png');
			background-repeat: no-repeat;
			background-size: cover;
		}
	}
	</style>

</head>
<body>
<!-- 訊息公告 -->
	<section id="text-block" class="announcement-block" style="display:none">
		<p class="announcement-title">訊息</p>
		<div class="">
			<p class="announcement-info">
				<font id="text-info"></font>
				<br/>
				<button class="btn-flat-orange announcement-btn" onclick="$('#text-block').hide();">關閉</button>
			</p>
		</div>
	</section>
	<!-- 忘記密碼 -->
	<section id="password-miss" class="modal fade more-info-block" role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header mb-0">
					<p class="ttb-pup-header">
						忘記密碼
					</p>
				</div>
				<!-- 表單顯示區  -->
				<div class="modal-body">
					<nav class="nav card-select-block" id="nav-tab1" role="tablist">
						<!-- 線上重設 -->
						<input type="button" class="nav-item ttb-sm-btn active"  data-toggle="tab" href="#nav-trans-11" role="tab" aria-selected="false" value="線上重設" />
						<!-- 臨櫃重設 -->
						<input type="button" class="nav-item ttb-sm-btn"  data-toggle="tab" href="#nav-trans-12" role="tab" aria-selected="true" value="臨櫃重設" />
						<!-- 以信用卡重設 -->
						<input type="button" class="nav-item ttb-sm-btn"  data-toggle="tab" href="#nav-trans-13" role="tab" aria-selected="true" value="以信用卡重設" />
					</nav>
					<div class="tab-content" id="nav-tabContent1">
						<div class="ttb-input-block tab-pane fade show active" id="nav-trans-11" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="card-detail-block">
<!-- 								<p>您可透過晶片金融卡及簡訊驗證身份後，即可重設使用者名稱與密碼 (簡訊將發送至您留存本行的行動電話號碼)。請選擇下方欲重設的項目進行重設：</p> -->
								<p>您可透過晶片金融卡及簡訊驗證身份後，即可重設使用者名稱與密碼 (簡訊將發送至您留存本行的行動電話號碼)。請選擇下方欲重設的項目進行重設：</p>
								<div class="text-center">
									<!-- 重設使用者名稱 -->
									<input type="submit" class="ttb-pup-btn btn-flat-orange" value="重設使用者名稱" onclick="fstop.getPage('/nb3'+'/RESET/user_reset','', '')" />
									<!-- 重設簽入密碼 -->
									<input type="submit" class="ttb-pup-btn btn-flat-orange" value="重設簽入密碼" onclick="fstop.getPage('/nb3'+'/RESET/user_reset','', '')" />
									<!-- 重設交易密碼 -->
									<input type="submit" class="ttb-pup-btn btn-flat-orange" value="重設交易密碼" onclick="fstop.getPage('/nb3'+'/RESET/ssl_reset','', '')" />
								</div>
							</div>
						</div>
						<div class="ttb-input-block tab-pane fade" id="nav-trans-12" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="card-detail-block">
<!-- 								<p>您可以攜帶相關證件至開戶分行辦理，相關證件如下：</p> -->
								<p>您可以攜帶相關證件至開戶分行辦理，相關證件如下：</p>
									<ol>
<!-- 										<li>1. 法人戶：公司登記證件、身分證、存款印鑑及存摺</li> -->
<!-- 										<li>2. 個人戶：身分證件、存款印鑑及存摺</li> -->
										<li>1. 法人戶：公司登記證件、身分證、存款印鑑及存摺</li>
										<li>2. 個人戶：身分證件、存款印鑑及存摺</li>
									</ol>
							</div>
						</div>
						<div class="ttb-input-block tab-pane fade" id="nav-trans-13" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="card-detail-block">
<!-- 								<p>若您是以本行信用卡申請網路銀行服務，可電洽本行客服0800-01-7171辦理以信用卡重設，次日起即可重新線上申請網路銀行。</p> -->
								<p>若您是以本行信用卡申請網路銀行服務，可電洽本行客服0800-01-7171辦理以信用卡重設，次日起即可重新線上申請網路銀行。</p>
							</div>
						</div>
					</div>
					
				</div>
				<div class="modal-footer ttb-pup-footer">
					<input type="submit" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="關閉" />
				</div>
			</div>
		</div>
	</section>

<section id="main-content" class="container position-absolute" style="display:none">
	<div class="pupup-block">

		<div class="card-block shadow-box terms-pup-blcok">
			<!-- n_e_e_d t_o f_i_x p_o_s_i_t_i_o_n -->
			<button type="button" class="popup-close-btn d-none"
				data-dismiss="modal" aria-label="Close">×</button>
			<h2 class="ttb-pup-h2">金融資訊</h2>

			<nav class="nav card-select-block text-center d-block" id="nav-tab"
				role="tablist">
				<input type="button" class="nav-item ttb-sm-btn active"
					id="nav-trans-1-tab" data-toggle="tab" href="#nav-trans-1"
					role="tab" aria-selected="false" value="匯率" /> 
				<input type="button" class="nav-item ttb-sm-btn" id="nav-trans-2-tab"
					data-toggle="tab" href="#nav-trans-2" role="tab"
					aria-selected="true" value="利率" /> 
				<input type="button" class="nav-item ttb-sm-btn" id="nav-trans-3-tab" data-toggle="tab"
					href="#nav-trans-3" role="tab" aria-selected="true" value="基金淨值" />
				<input type="button" class="nav-item ttb-sm-btn"
					id="nav-trans-4-tab" data-toggle="tab" href="#nav-trans-4"
					role="tab" aria-selected="true" value="黃金存摺" /> 
				<input type="button" class="nav-item ttb-sm-btn" id="nav-trans-5-tab"
					data-toggle="tab" href="#nav-trans-5" role="tab"
					aria-selected="true" value="保單" />
			</nav>
			<div class="col-12 tab-content" id="nav-tabContent">
				<div class="ttb-input-block tab-pane fade show active"
					id="nav-trans-1" role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!-- 新台幣存款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-82" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>當日<br />匯率查詢
								</span>
								</a>
							</div>
							<!-- 新台幣放款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-83" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>歷史<br />匯率查詢
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="ttb-input-block tab-pane fade" id="nav-trans-2"
					role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!-- 新台幣存款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-82" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>新台幣存款<br />利率查詢
								</span>
								</a>
							</div>
							<!-- 新台幣放款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-83" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>新台幣放款<br />利率查詢
								</span>
								</a>
							</div>
							<!-- 外幣存款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-83" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>外幣存款<br />利率查詢
								</span>
								</a>
							</div>
							<!-- 外幣放款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-83" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>外幣放款<br />利率查詢
								</span>
								</a>
							</div>
							<!-- 債券及票券利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-83" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>債券及票券<br />利率查詢
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="ttb-input-block tab-pane fade" id="nav-trans-3"
					role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!--  -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-82" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>國內基金<br />淨值查詢
								</span>
								</a>
							</div>
							<!--  -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-83" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>國外基金<br />淨值查詢
								</span>
								</a>
							</div>
							<!--  -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-83" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>連動式債券<br />淨值查詢
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="ttb-input-block tab-pane fade" id="nav-trans-4"
					role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!--  -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-82" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>當日<br />黃金牌價查詢
								</span>
								</a>
							</div>
							<!--  -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-83" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>歷史<br />黃金牌價查詢
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="ttb-input-block tab-pane fade" id="nav-trans-5"
					role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!--  -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-82" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>投資型<br />保單查詢
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<input type="BUTTON" class="ttb-button btn-flat-gray" value="關閉" name="" onclick="$('#main-content').hide();">
			</div>
		</div>
	</div>
</section>
<!-- 最新資訊 more -->
	<section id="news-more" class="modal fade more-info-block" role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered calendar-modal" role="document">
			<div class="modal-content">
				<div class="modal-header mb-0">
					<p class="ttb-pup-header">最新資訊</p>
				</div>
				<div class="modal-body">
					<ul id="GENmore">
					</ul>
				</div>
				<div class="modal-footer ttb-pup-footer">
					<input type="submit" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="關閉" />
				</div>
			</div>
		</div>
	</section>
	
	<!-- 好康消息more -->
	<section id="good-news" class="modal fade more-info-block" role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered calendar-modal" role="document">
			<div class="modal-content">
				<div class="modal-header mb-0">
					<p class="ttb-pup-header">好康消息</p>
				</div>
				<div class="modal-body">
					
						<ul>
							
								<li>
									<a href="http://www.tbb.com.tw" >
										
										<span class="news-day">2019/12/23</span>
										<span class="news-content">
											臺企銀勇奪金管會績優金融機構五獎項！
										</span>
									</a>
								</li>
							
								<li>
									<a href="https://bit.ly/2E0c1jm" >
										
										<span class="news-day">2019/12/23</span>
										<span class="news-content">
											[台灣Pay] 台灣人94要用台灣Pay，多樣好禮隨你抽！(至2020/3/15)
										</span>
									</a>
								</li>
							
								<li>
									<a href="https://hibank.tbb.com.tw/TBBHiBank/tbb/index.html" >
										
										<span class="news-day">2019/12/23</span>
										<span class="news-content">
											數位存款送優惠，尊享存款利率1.15%!
										</span>
									</a>
								</li>
							
								<li>
									<a href="https://www.taiwanpay.com.tw/content/info/news_detail.aspx?enc=9CEDBC93FC608710061A49AD2BCDC71E" >
										
										<span class="news-day">2019/12/23</span>
										<span class="news-content">
											[台灣Pay] 繳費就用台灣Pay，一張一抽獎。(至2020/3/15)
										</span>
									</a>
								</li>
							
						</ul>
					
				</div>
				<div class="modal-footer ttb-pup-footer">
					<input type="submit" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="關閉" />
				</div>
			</div>
		</div>
	</section>
	
	<!-- LOADING遮罩區塊 -->
	<div id="loadingBox" class="Loading_box">
		<div class="sk-fading-circle">
			<div class="sk-circle1 sk-circle"></div>
			<div class="sk-circle2 sk-circle"></div>
			<div class="sk-circle3 sk-circle"></div>
			<div class="sk-circle4 sk-circle"></div>
			<div class="sk-circle5 sk-circle"></div>
			<div class="sk-circle6 sk-circle"></div>
			<div class="sk-circle7 sk-circle"></div>
			<div class="sk-circle8 sk-circle"></div>
			<div class="sk-circle9 sk-circle"></div>
			<div class="sk-circle10 sk-circle"></div>
			<div class="sk-circle11 sk-circle"></div>
			<div class="sk-circle12 sk-circle"></div>
		</div>
	</div>

	<div class="container login-container">
		<div class="row">
			<div class="col-lg-8 col-md-12 col-sm-12 order-lg-1 order-2 p-0">
				<div class="login-top d-none d-lg-block">
					<div class="header-area">
						<div class="header-ele">
							<div class="ele-info">
								<!--最新資訊-->
								<div class="header-info">
									最新資訊
								</div>
								<div class="header-depiction tcontainer">
									<div class="ticker-wrap">
										<ul id="GEN">
										</ul>
									</div>
								</div>
								<a href="#" data-toggle="modal" data-target="#news-more"><img class="more-btn" src="<%= request.getContextPath() %>/img/preview/icon-more-zh_TW.svg"></a>
							</div>
							<div class="ele-info">
								<!--重要公告-->
								<div class="header-info">
									重要公告
								</div>
								<div class="header-depiction tcontainer">
									<div class="ticker-wrap">
										<ul id="IMP">
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	
				<div class="login-banner">
					<div style="position: absolute;left:0;top: 86px;z-index: 99999;" id="showRotate"><input type="button" class="btn btn-info" value="上下顛倒" onclick="onFlip();"></div>
					<div id="photo-carousel" class="banner-slogan carousel slide"
						data-ride="carousel">
						<!-- 輪播圖片-->
						<div class="carousel-inner">
							<!-- 預設圖片顯示 -->
							<div class="carousel-item active">
								<!--  <img class="d-block">  -->
							</div>
							<!-- 其他相片 -->
							<div class="carousel-item">
								<!--  <img class="d-block">  -->
							</div>
						</div>
						<!-- 相片選擇鈕 -->
						<ol class="carousel-indicators">
							<li data-target="#photo-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#photo-carousel" data-slide-to="1"></li>
						</ol>
						<!-- 相片瀏覽鈕 -->
						<a class="carousel-control-prev" href="#photo-carousel"
							role="button" data-slide="prev"> <span
							class="carousel-control-prev-icon" aria-hidden="true"></span> <span
							class="sr-only">Previous</span>
						</a> <a class="carousel-control-next" href="#photo-carousel"
							role="button" data-slide="next"> <span
							class="carousel-control-next-icon" aria-hidden="true"></span> <span
							class="sr-only">Next</span>
						</a>
					</div>
				</div>
				<div class="login-icon">
					<div class="element-area">
						<div class="icon-arrow">
							<img src="<%= request.getContextPath() %>/img/preview/arrow-left.png">
						</div>
						<!-- 新手上路 -->
						<div class="element-item">
							<a href="/nb3/CUSTOMER/SERVICE/novice" target="_self">
								<img src="<%= request.getContextPath() %>/img/preview/icon-01.svg">
								<span>新手上路</span>
							</a>
						</div>
						<!-- 常見問題 -->
						<div class="element-item">
							<a href="/nb3/CUSTOMER/SERVICE/common_problem" target="_self">
								<img src="<%= request.getContextPath() %>/img/preview/icon-02.svg">
								<span>常見問題</span>
							</a>
						</div>
						<!-- 網銀申辦 / 一般網銀 線上申請(原) -->
						<div class="element-item">
							<a href="#" onclick="fstop.getPage('/nb3'+'/ONLINE/APPLY/online_apply_menu','', '')">
								<img src="<%= request.getContextPath() %>/img/preview/icon-03.svg">
								<span>線上申請</span>
							</a>
						</div>
						<!-- 環境設定 -->
						<div class="element-item">
							<a href="/nb3/CUSTOMER/SERVICE/environment_setting" target="_self">
								<img src="<%= request.getContextPath() %>/img/preview/icon-04.svg">
								<span>環境設定</span>
							</a>
						</div>
						<!-- 金融資訊 -->
						<div class="element-item">
							<a href="#" onclick="$('#main-content').show();"> 
								<img src="<%= request.getContextPath() %>/img/preview/icon-05.svg">
								<span>金融資訊</span>
							</a>
						</div>
						<div class="icon-arrow">
							<img src="<%= request.getContextPath() %>/img/preview/arrow-right.png">
						</div>
					</div>
					<div class="login-menu  d-block d-lg-none">
						<ul>
							<li>
								<!-- 臺灣企銀首頁 -->
										<a href="https://www.tbb.com.tw/" target="_blank" role="button">
											臺灣企銀首頁
										</a>
									
							</li>
							<li>
								<!-- 網路安全 --> 
								<a href="/nb3/CUSTOMER/SERVICE/network_security" target="_self" role="button">
									網路安全
								</a>
							</li>
							<li>
								<!-- 網站導覽 -->
								<a href="/nb3/CUSTOMER/SERVICE/site_guide" target="_self" role="button">
									網站導覽
								</a>
							</li>
							<li>
								<!-- 意見信箱 -->
										<a href="https://www.tbb.com.tw/-47" target="_blank" role="button">
											意見信箱
										</a>
									
							</li>
						</ul>
					</div>
				</div>
				
				
				<div class="login-news">
					<!-- 好康消息 -->
					<p>好康消息</p>
					
					<!-- 其他 -->
							<div class="news-area">
								<a onclick="${onclick}" style="cursor: hand" >
									<div class="news-img">
										<img style="background-image:url('${imgCSrc}')">
									</div>
									<div class="news-info">
										<div class="news-day">YYYY/MM/DD</div>
										<div class="news-content">
											${imgNews}
										</div>
									</div>
								</a>
							</div>
						
							<div class="news-area">
								<a href="#" >
									<div class="news-img">
										<img style="background-image:url(../../img/preview/photo-00.png)">
									</div>
									<div class="news-info">
										
										<div class="news-day">YYYY/MM/DD</div>
										<div class="news-content">
											小廣告樣張
										</div>
									</div>
								</a>
							</div>
						
							<!-- <div class="news-area">
								<a href="#" >
									<div class="news-img">
										<img style="background-image:url(../../img/preview/photo-00.png)">
									</div>
									<div class="news-info">
										
										<div class="news-day">YYYY/MM/DD</div>
										<div class="news-content">
											小廣告樣張
										</div>
									</div>
								</a>
							</div>
						
							<div class="news-area">
								<a href="#" >
									<div class="news-img">
										<img style="background-image:url(../../img/preview/photo-00.png)">
									</div>
									<div class="news-info">
										
										<div class="news-day">YYYY/MM/DD</div>
										<div class="news-content">
											小廣告樣張
										</div>
									</div>
								</a>
							</div> -->
						
					
					<a href="#" class="login-news-more" data-toggle="modal" data-target="#good-news"><img class="more-btn" src="<%= request.getContextPath() %>/img/preview/icon-more-zh_TW.svg"></a>
				</div>
				<div class="login-bottom">
					<p class="tbb-login-name">©臺灣中小企業銀行</p><!-- 臺灣中小企業銀行 -->
					<ul>
						<!-- 隱私權聲明 -->
						<li><a href="https://www.tbb.com.tw/web/guest/-173" target="_blank">隱私權聲明</a></li>
						<!-- 安全政策 -->
						<li><a href="https://www.tbb.com.tw/web/guest/-551" target="_blank">安全政策</a></li>
						<!-- 系統/瀏覽器需求 -->
						<li><a href="/nb3/CUSTOMER/SERVICE/common_problem?tag=0" target="_blank">系統/瀏覽器需求</a></li>
					</ul>
					<div class="enterprise-info d-inline-block d-lg-none">
						<p>
							<!-- 客服 -->客服
							<a href="tel:0800-017-171">
								0800-00-7171
							</a>
							<!-- 限市話 -->(限市話),  
							<a href="tel:02-2357-7171">
								02-2357-7171
							</a>
						</p>
						<p>
							<!-- 基金 -->基金
							<a href="tel:02-2559-7171">
							02-2559-7171
							</a>
							#5461~5463
						</p>
					</div>
					<span class="facebook-icon d-lg-none">&nbsp;</span>
					<!-- 版權所有 2018 臺灣中小企業銀行 Taiwan Business Bank -->
				</div>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 order-lg-2 order-1 lg-login">
				<div class="login-title-block">
					<!-- 多語系選擇 -->
					<div class="multi-lang-select">
						<select class="login-custom-select multi-lang-select" onchange="location = this.value">
							<option value="0">Language</option>
							<option value="/nb3/login?locale=zh_TW"><a href="#">繁</a></option>
							<option value="/nb3/login?locale=zh_CN"><a href="#">简</a></option>
							<option value="/nb3/login?locale=en"><a href="#">En</a></option>
						</select>
					</div>
					<img class="login-logo" src="<%= request.getContextPath() %>/img/preview/tbb-logo-white.svg">
				</div>
				<div class="login-top d-lg-none">
					<div class="header-area">
						<div class="header-ele">
							<div class="ele-info">
								<!--最新資訊-->
								<div class="header-info">
									最新資訊
								</div>
								<div class="header-depiction tcontainer">
									<div class="ticker-wrap">
										<ul id="GEN1">
										</ul>
									</div>
								</div>
								<a href="#" data-toggle="modal" data-target="#news-more"><img class="more-btn" src="<%= request.getContextPath() %>/img/preview/icon-more-zh_TW.svg"></a>
							</div>
							<div class="ele-info">
								<!--重要公告-->
								<div class="header-info">
									重要公告
								</div>
								<div class="header-depiction tcontainer">
									<div class="ticker-wrap">
										<ul id="IMP1">
										</ul>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="web-switch">
					<a href="#" class="active">
						網路銀行
					</a>
					<a href="https://nnb.tbb.com.tw/TBBeATMAppsWeb/" target="_blank" role="button">
						網路ATM
					</a>
				</div>
				<form id="formId" method="post" action="/nb3/INDEX/index">
					<input name="ACRADIO" type="hidden" value="" />
					<!-- 需要變更密碼 -->
					<div class="login-input-block  login-checkbox">
						<!-- 身分證字號/統一編號 -->
						<input type="text" id="cusidn" name="cusidn"
							placeholder="身分證字號/統一編號" maxlength="10" autocomplete="off">
					</div>
					<div class="login-input-block  login-checkbox">
						<!-- 使用者名稱 -->
						<input type="text" id="userName" name="userName"
							placeholder="使用者名稱" maxlength="16" autocomplete="off">
						<label class="check-block">
							<input type="checkbox" id="hid" name="hid" onclick="hideUserName()" tabindex="-1" />
							<span class="ttb-check"></span>
						</label>
					</div>
					<div class="login-input-block login-checkbox">
						<!-- 簽入密碼 -->
						<input type="password" id="webpw" name="password"
							placeholder="簽入密碼" maxlength="8" autocomplete="off">

					</div>
					<!-- 驗證碼那一塊的程式 -->
					<div class="login-input-block">
						
						<input id="capCode" type="text" class="veri-code-input"
							name="capCode" placeholder="驗證碼" maxlength="4" autocomplete="off">
						
						<button class="code-update" type="button" name="reshow" onclick="refreshCapCode()">
							<img name="kaptchaImage"/>
						</button>
					</div>
					
					<!-- 動態鍵盤 -->
					<input id="openKeyboardBtn" class="login-btn openKeyboardBtn d-none d-lg-block m-auto" type="button" value="" data-toggle="tooltip" data-placement="right" title="顯示動態鍵盤" />
					<div id="keyboardBlock" class="keyboardBlock login-keyboard" style="display:none;">
						<p>螢幕小鍵盤<img id="closeKeyboardBtn" class="closeKeyboardBtn" src="<%= request.getContextPath() %>/img/preview/close.svg"></p>
				
						<div class="keyboard">
							<ul class="keyboardButtons letterKeyboard">
								<li data-role="letter" data-key="a" data-key-caps="A"></li>
								<li data-role="letter" data-key="b" data-key-caps="B"></li>
								<li data-role="letter" data-key="c" data-key-caps="C"></li>
								<li data-role="letter" data-key="d" data-key-caps="D"></li>
								<li data-role="letter" data-key="e" data-key-caps="E"></li>
								<li data-role="letter" data-key="f" data-key-caps="F"></li>
								<li data-role="letter" data-key="g" data-key-caps="G"></li>
								<li class="btnDelete" data-role="delete" data-key="" data-key-caps=""><img src="<%= request.getContextPath() %>/img/preview/delete.svg" /></li>
								<li data-role="letter" data-key="h" data-key-caps="H"></li>
								<li data-role="letter" data-key="i" data-key-caps="I"></li>
								<li data-role="letter" data-key="j" data-key-caps="J"></li>
								<li data-role="letter" data-key="k" data-key-caps="K"></li>
								<li data-role="letter" data-key="l" data-key-caps="L"></li>
								<li data-role="letter" data-key="m" data-key-caps="M"></li>
								<li data-role="letter" data-key="n" data-key-caps="N"></li>
								<li class="btnCapslock" data-role="capslock" data-key="" data-key-caps=""><img src="<%= request.getContextPath() %>/img/preview/cap.svg" /></li>
								<li data-role="letter" data-key="o" data-key-caps="O"></li>
								<li data-role="letter" data-key="p" data-key-caps="P"></li>
								<li data-role="letter" data-key="q" data-key-caps="Q"></li>
								<li data-role="letter" data-key="r" data-key-caps="R"></li>
								<li data-role="letter" data-key="s" data-key-caps="S"></li>
								<li data-role="letter" data-key="t" data-key-caps="T"></li>
								<li data-role="letter" data-key="u" data-key-caps="U"></li>
								<li class="btnReset" data-role="reset" data-key="" data-key-caps=""><img src="<%= request.getContextPath() %>/img/preview/reset.svg" style="width: 20px; color: #f3720c;"/></li>
								<li data-role="letter" data-key="v" data-key-caps="V"></li>
								<li data-role="letter" data-key="w" data-key-caps="W"></li>
								<li data-role="letter" data-key="x" data-key-caps="X"></li>
								<li data-role="letter" data-key="y" data-key-caps="Y"></li>
								<li data-role="letter" data-key="z" data-key-caps="Z"></li>
								<li class="btnConfirm" data-role="confirm" data-key="" data-key-caps="">登入</li>
							</ul>
						</div>
						<div class="keyboard numberKeyboard">
							<ul class="keyboardButtons">
								<li data-role="symbol" data-key="1" data-key-caps="1"></li>
								<li data-role="symbol" data-key="2" data-key-caps="2"></li>
								<li data-role="symbol" data-key="3" data-key-caps="3"></li>
								<li data-role="symbol" data-key="4" data-key-caps="4"></li>
								<li data-role="symbol" data-key="5" data-key-caps="5"></li>
								<li data-role="symbol" data-key="6" data-key-caps="6"></li>
								<li data-role="symbol" data-key="7" data-key-caps="7"></li>
								<li data-role="symbol" data-key="8" data-key-caps="8"></li>
								<li data-role="symbol" data-key="9" data-key-caps="9"></li>
								<li data-role="symbol" data-key="0" data-key-caps="0"></li>
							</ul>
						</div>
					</div>
					
					<!-- 登入 -->
					<div class="login-submit-block">

						<a href="#" data-toggle="modal" data-target="#password-miss">忘記使用者名稱/密碼?</a><!-- 忘記帳號/密碼? -->
						<input id="login" class="login-btn" type="button" value="登入" />
					</div>
				</form>
				<div class="login-menu d-none d-lg-block">
					<ul>
						<li>
							<!-- 臺灣企銀首頁 -->
									<a href="#" target="_blank" role="button">
										臺灣企銀首頁
									</a>
								
						</li>
						<li>
							<!-- 網路安全 --> 
							<a href="#" target="_self" role="button">
								網路安全
							</a>
						</li>
						<li>
							<!-- 網站導覽 -->
							<a href="#" target="_self" role="button">
								網站導覽
							</a>
						</li>
						<li>
							<!-- 意見信箱 -->
									<a href="#" target="_blank" role="button">
										意見信箱
									</a>
								
						</li>
					</ul>
				</div>
				
				<div class="enterprise-info  d-none d-lg-block">
					<p>
						<!-- 客服 -->客服
						<a href="tel:0800-017-171">
						0800-00-7171
						</a>
						<!-- 限市話 -->(限市話),  
						<a href="tel:02-2357-7171">
							02-2357-7171
						</a>
					</p>
					<p>
						<!-- 基金 -->基金
					    <a href="tel:02-2559-7171">
						02-2559-7171
						</a>
						#5461~5463
					</p>
					<span class="facebook-icon">&nbsp;</span>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function () {
			if ( "${showRotate}" == "false" ) {
				$("#showRotate").hide();
			}
		});
		function onFlip() {
			if ( !confirm("上下顛倒廣告?") ) {
				return;
			}
			
			$.ajax({
                url: "<%= request.getContextPath() %>/B501/Flip/${id}",
                type: "POST",
                data: {
                },
                success: function (rt) {
                    if(rt=="0"){
                    	location.reload();
                    } else {
                        alert(rt);
                    }
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
		}
	</script>
</body>
</html>