<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">

</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>信用卡卡別資料匯入</h2>
        <form class="container main-content-block p-4" action="/dummy" method="post">
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="ADTYPEID">卡別代碼：</label>
                <div class="col-4">
                    <input type="text" id="ADTYPEID" name="ADTYPEID" class="form-control" maxlength="10" />
                </div>
                <label class="col-2 control-label text-right" for="ADTYPENAME">卡別名稱(繁中)：</label>
                <div class="col-4">
                    <input type="text" id="ADTYPENAME" name="ADTYPENAME" class="form-control" maxlength="50" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="ADTYPECHSNAME">卡別名稱(簡中)：</label>
                <div class="col-4">
                    <input type="text" id="ADTYPECHSNAME" name="ADTYPECHSNAME" class="form-control" maxlength="50" />
                </div>
                <label class="col-2 control-label text-right" for="ADTYPEENGNAME">卡別名稱(英文)：</label>
                <div class="col-4">
                    <input type="text" id="ADTYPEENGNAME" name="ADTYPEENGNAME" class="form-control" maxlength="180" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" for="ADTYPEDESC">備註說明：</label>
                <div class="col-10">
                    <textarea type="text" id="ADTYPEDESC" name="ADTYPEDESC" class="form-control" cols="3" maxlength="254" /></textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-2 col-10">
                    <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                    <input id="btnQuery" type="button" value="以上述值做查詢" class="btn btn-dark" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                    <c:if test="${allowEdit}">
                        <input id="btnCreate" type="button" value="以上述值做新增" class="btn btn-info" aria-label="Left Align" onclick="onCreate()" />
                    </c:if>
                </div>
            </div>
        </form>
    </div>
    <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>卡別代碼</th>
                <th>卡別名稱(繁中)</th>
                <th>卡別名稱(簡中)</th>
                <th>卡別名稱(英文)</th>
                <th>備註說明</th>
                <th>動作</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.all.js"></script>
    <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.exhide.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            onQuery();
        }); //ready

        function onQuery() {
            $("span.error").remove();

            $("#main").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "order": [[0, "asc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/B110/Query",
                    "data": {
                        "ADTYPEID": $("#ADTYPEID").val(),
                        "ADTYPENAME": $("#ADTYPENAME").val(),
                        "ADTYPECHSNAME": $("#ADTYPECHSNAME").val(),
                        "ADTYPEENGNAME": $("#ADTYPEENGNAME").val(),
                        "ADTYPEDESC": $("#ADTYPEDESC").val()
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "adtypeid" },
                    { "data": "adtypename" },
                    { "data": "adtypechsname" },
                    { "data": "adtypeengname" },
                    { "data": "adtypedesc" },
                    { "data": "adtypeid" }
                ],
                "createdRow": function (row, data, index) {
                    // 處理卡別代碼
                    var key = data.adtypeid.trim();

                    var $typeId = $("<input type='text' class='form-control' maxlength='5' disabled='disabled' />");
                    $typeId.attr("id", "typeId_" + key);
                    $typeId.attr("value", key);
                    $("td", row).eq(0).text("").append($typeId);

                    // 處理卡別名稱(繁中)
                    var $typeName = $("<input type='text' class='form-control' disabled='disabled' />");
                    $typeName.attr("id", "typeName_" + key);
                    $typeName.attr("value", data.adtypename);
                    $("td", row).eq(1).text("").append($typeName);

                    // 處理卡別名稱(簡中)
                    var $typeChsName = $("<input type='text' class='form-control' disabled='disabled' />");
                    $typeChsName.attr("id", "typeChsName_" + key);
                    $typeChsName.attr("value", data.adtypechsname);
                    $("td", row).eq(2).text("").append($typeChsName);

                    // 處理卡別名稱(繁中)
                    var $typeEngName = $("<input type='text' class='form-control' disabled='disabled' />");
                    $typeEngName.attr("id", "typeEngName_" + key);
                    $typeEngName.attr("value", data.adtypeengname);
                    $("td", row).eq(3).text("").append($typeEngName);

                    // 處理備註說明
                    var $typeDesc = $("<input type='text' class='form-control' disabled='disabled' />");
                    $typeDesc.attr("id", "typeDesc_" + key);
                    $typeDesc.attr("value", data.adtypedesc);
                    $("td", row).eq(4).text("").append($typeDesc);

                    // 處理 Button
                    if ( "${allowEdit}" == "true" ) {
                        var $edit = $("<input type='button' value='修改' class='btn btn-primary' />");
                        $edit.attr("id", "btnEdit_" + key);
                        $edit.attr("onclick", "onEdit('" + key + "')");

                        var $delete = $("<input type='button' value='刪除' class='btn btn-danger' />");
                        $delete.attr("id", "btnDelete_" + key);
                        $delete.attr("onclick", "onDelete('" + key + "')");

                        var $save = $("<input type='button' value='儲存' class='btn btn-primary' style='display: none;' />");
                        $save.attr("id", "btnSave_" + key);
                        $save.attr("onclick", "onSave('" + key + "')");

                        var $exit = $("<input type='button' value='離開' class='btn btn-dark' style='display: none;' />");
                        $exit.attr("id", "btnBack_" + key);
                        $exit.attr("onclick", "onBack('" + key + "')");

                        var $space1 = $("<span>&nbsp;</span>");
                        var $space2 = $("<span>&nbsp;</span>");
                        $("td", row).eq(5).text("").append($edit).append($space1).append($delete).append($save).append($space2).append($exit);
                    } else {
                        $("td", row).eq(5).text("");
                    }
                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            $("#main").show();
            return false;
        }

        function checkEmpty(adtypeid, adtypename, adtypechsname, adtypeengname) {
            var msg = "";
            if ( adtypeid=="" ) {
                msg += "卡別代碼不可為空值.\r\n";
            }

            if ( adtypename=="" ) {
                msg += "卡別名稱(繁中)不可為空值.\r\n";
            }

            if ( adtypechsname=="" ) {
                msg += "卡別名稱(簡中)不可為空值.\r\n";
            }

            if ( adtypeengname=="" ) {
                msg += "卡別名稱(英文)不可為空值.\r\n";
            }

            return msg;
        }

        function onCreate() {
            
            $("span.error").remove();

            var typeId = $("#ADTYPEID").val();
            var typeName = $("#ADTYPENAME").val();
            var typeChsName = $("#ADTYPECHSNAME").val();
            var typeEngName = $("#ADTYPEENGNAME").val();
            var typeDesc = $("#ADTYPEDESC").val();

            var msg = checkEmpty(typeId, typeName, typeChsName, typeEngName);
            if ( msg != "" ) {
                alert(msg);
                return;
            }

            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/B110/Create",
                type: "POST",
                data: {
                    "ADTYPEID": typeId,
                    "ADTYPENAME": typeName,
                    "ADTYPECHSNAME": typeChsName,
                    "ADTYPEENGNAME": typeEngName,
                    "ADTYPEDESC": typeDesc
                },
                success: function (res) {
                    unlockScreen();
                    if (res.validated) {
                        alert("卡別代碼[" + typeId + "]新增成功");
                        onQuery();
                    } else {
                        //Set error messages
                        $.each(res.errorMessages, function (key, value) {
                            if (key === "summary")
                                alert(value);
                            else {
                                if (key === "ADTYPEDESC")
                                    $('<span class="error">' + value + '</span>').insertAfter("textarea");
                                else
                                    $('input[name=' + key + ']').after('<span class="error">' + value + '</span>');
                            }

                        });
                    }
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }

        function onEdit(id) {
            //$("#typeId_" + id).attr("disabled", false);
            $("#typeName_" + id).attr("disabled", false);
            $("#typeChsName_" + id).attr("disabled", false);
            $("#typeEngName_" + id).attr("disabled", false);
            $("#typeDesc_" + id).attr("disabled", false);
            toggleButton(false, id);
        }

        function onDelete(id) {
            var typeId = $("#typeId_" + id).val();
            var message = confirm("確定刪除卡別代碼[" + typeId + "]？");
            if (message == true) {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B110/Delete/" + id,
                    type: "POST",
                    data: {},
                    success: function (data) {
                        unlockScreen();
                        if (data === "0") {
                            alert("卡別代碼[" + typeId + "]刪除成功");
                            onQuery();
                        } else {
                            alert(data);
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
        }

        function onSave(id) {
            var typeId = $("#typeId_" + id).val();
            var typeName = $("#typeName_" + id).val();
            var typeChsName = $("#typeChsName_" + id).val();
            var typeEngName = $("#typeEngName_" + id).val();
            var typeDesc = $("#typeDesc_" + id).val();

            var msg = checkEmpty(typeId, typeName, typeChsName, typeEngName);
            if ( msg != "" ) {
                alert(msg);
                return;
            }

            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/B110/Edit",
                type: "POST",
                data: {
                    "ADTYPEID": typeId,
                    "ADTYPENAME": typeName,
                    "ADTYPECHSNAME": typeChsName,
                    "ADTYPEENGNAME": typeEngName,
                    "ADTYPEDESC": typeDesc
                },
                success: function (res) {
                    unlockScreen();
                    if (res.validated) {
                        alert("選單[" + typeName + "]修改成功");
                        $("#typeName_" + id).attr("disabled", true);
                        $("#typeChsName_" + id).attr("disabled", true);
                        $("#typeEngName_" + id).attr("disabled", true);
                        $("#typeDesc_" + id).attr("disabled", true);
                        toggleButton(true, typeId);
                    } else {
                        //Set error messages
                        $.each(res.errorMessages, function (key, value) {
                            if (key === "summary")
                                alert(value);
                            else {
                                alert(key + ":" + value);
                            }
                        });
                    }
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }

        function onBack(id) {
            var typeId = $("#typeId_" + id);
            var typeName = $("#typeName_" + id);
            var typeChsName = $("#typeChsName_" + id);
            var typeEngName = $("#typeEngName_" + id);
            var typeDesc = $("#typeDesc_" + id);

            lockScreen();
            $.ajax({
                url: "<%= request.getContextPath() %>/B110/Get/" + id,
                type: "POST",
                cache: false,
                async: false,
                success: function (data) {
                    unlockScreen();
                    typeName.val(data.adtypename);
                    typeChsName.val(data.adtypechsname);
                    typeEngName.val(data.adtypeengname);
                    typeDesc.val(data.adtypedesc);

                    typeName.attr("disabled", true);
                    typeChsName.attr("disabled", true);
                    typeEngName.attr("disabled", true);
                    typeDesc.attr("disabled", true);
                    toggleButton(true, id);
                },
                error: function (err) {
                    unlockScreen();
                    alert(err.statusText);
                }
            });
        }
        function toggleButton(toQuery, id) {
            if (toQuery) {
                $("#btnEdit_" + id).show();
                $("#btnEdit_" + id).next().show();
                $("#btnDelete_" + id).show();

                $("#btnSave_" + id).hide();
                $("#btnSave_" + id).next().hide();
                $("#btnBack_" + id).hide();
            } else {
                $("#btnEdit_" + id).hide();
                $("#btnEdit_" + id).next().hide();
                $("#btnDelete_" + id).hide();

                $("#btnSave_" + id).show();
                $("#btnSave_" + id).next().show();
                $("#btnBack_" + id).show();
            }
        }
    </script>
</body>

</html>