<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="content">
        <div class="container" id="main-content">
            <h2>基本資料管理查詢</h2>
            <form id="B203" name="B203" class="container main-content-block p-4" >
                <div class="form-group row">
                    <label class="col-3 control-label text-right">身分證字號/統一編號：</label>
                    <div class="col-3">
                        <input type="text" id="adUserId" name="adUserId" class="form-control upper" maxlength="10"/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input id="btnClear" type="reset" value="重新輸入" class="btn btn-dark" aria-label="Left Align" />&nbsp;
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="queryResult">
        <label class="col-12 control-label text-center">客戶基本資料</label>
        <table id="basicData"" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <tbody>
            </tbody>
        </table>
        <br />
        <label class="col-12 control-label text-center">客戶憑證資料</label>
        <table id="certData" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <tbody>
            </tbody>
        </table>
        <br />
        <label class="col-12 control-label text-center">客戶約定帳號資料</label>
        <table id="agreementData" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <th>行庫別</th>
                <th>帳號</th>
                <th>帳號統編</th>
                <th>業務類號</th>
                <th>性質別</th>
                <th>狀態</th>
                <th>處理回應</th>
                <th>建檔分行</th>
                <th>最後異動日期</th>
                <th>最後異動時間</th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    
    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
    <script>
        var basicData = [];         // 基本資料
        var certData = [];          // 憑證資料
        var telData = [];           // 約定轉帳資料

        $(document).ready(function () {
            $("#queryResult").hide();
        }); //ready

        // 顯示基本資料
        function onBasicDataBind() {
            $("#basicData").dataTable({
                "responsive": true,
                "bPaginate": false,
                "bFilter": false,
                "bDestroy": true,
                "bSort": false,
                "info": false,
                "aaData": basicData,
                "aoColumns": [
                    { "mDataProp": "key" },
                    { "mDataProp": "value" }
                ],
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
        }

        // 顯示憑證資料
        function onCertDataBind() {
            $("#certData").dataTable({
                "responsive": true,
                "bPaginate": false,
                "bFilter": false,
                "bDestroy": true,
                "bSort": false,
                "info": false,
                "aaData": certData,
                "aoColumns": [
                    { "mDataProp": "key" },
                    { "mDataProp": "value" }
                ],
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
        }

        // 顯示約定資料
        function onTelDataBind() {
            $("#agreementData").dataTable({
                "responsive": true,
                "bPaginate": false,
                "bFilter": false,
                "bDestroy": true,
                "bSort": false,
                "info": false,
                "aaData": telData,
                "aoColumns": [
                    { "mDataProp": "bankcod" },
                    { "mDataProp": "tsfacn" },
                    { "mDataProp": "cusidn" },
                    { "mDataProp": "bustype" },
                    { "mDataProp": "atrcod" },
                    { "mDataProp": "stscod" },
                    { "mDataProp": "rtncod" },
                    { "mDataProp": "updbrh" }, 
                    { "mDataProp": "chgdat" },
                    { "mDataProp": "chgtim" }
                ],
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
        }

        function onQuery() {
            if ( $("#adUserId").val() == "" ) {
                alert("請輸入身分證字號/統一編號");
                return;
            }
            $("#queryResult").hide();
            var url = "<%= request.getContextPath() %>/B203/Query/" + $("#adUserId").val();
            lockScreen();
            $.post(url, {}, function (data, status) {
                unlockScreen();
                if (status == "success") {
                    var errorMsg = data.errorMsg;
                    
                    if (  errorMsg == "" ) {
                        basicData = data.basicData;
                        certData = data.certData;
                        telData = data.telData;

                        onBasicDataBind();
                        onCertDataBind();
                        onTelDataBind();

                        $("#queryResult").show();
                    } else {
                        alert(errorMsg);
                    }
                }
            });
        }
    </script>
</body>

</html>