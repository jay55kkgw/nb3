<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>


<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>一般網路銀行約定/非約定統計表</h2>
            <form id="B704" name="B704" class="container main-content-block p-4" action="<%= request.getContextPath() %>/B704/Query" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >查詢期間：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <select class="form-control" id="yearS" name="yearS">
                                <option value="">--請選擇--</option>
                                <c:forEach items="${years}" var="year">
                                    <option value="${year}">${year}</option>
                                </c:forEach>
                            </select>&nbsp;年&nbsp;
                            <select class="form-control" id="monthS" name="monthS">
                                <option value="">--請選擇--</option>
                                <c:forEach items="${months}" var="month">
                                    <option value="${month}">${month}</option>
                                </c:forEach>
                            </select>&nbsp;月
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnexpCSV" type="button" value="匯出CSV" class="btn btn-secondary" aria-label="Left Align" onclick="onExportCSV();" />&nbsp;
                        <input id="btnPrint" type="button" value="列印" class="btn btn-secondary" aria-label="Left Align" onclick="friendlyPrint('printQ,print');" />
                    </div>
                </div>
            </form>
        </div>
        <!-- report -->
        <div id="printQ">
            <div class="form-group row">
                <label class="col-12 control-label text-center"><h4>一般網路銀行約定/非約定統計表</h4></label>                
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >查詢時間：</label>
                <div class="col-10">
                    <div class="radio">
                        <div class="form-inline">
                            <jsp:useBean id="now" class="java.util.Date" />
                            <fmt:formatDate var="nowDateTime" value="${now}" pattern="yyyy/MM/dd HH:mm:ss" />
                            ${nowDateTime}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >查詢期間：</label>
                <div class="col-10">
                    <div class="form-inline">
                        ${B704Model.yearS}/${B704Model.monthS}                        
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >單位：</label>
                <div class="col-10">
                    <div class="form-inline">
                        筆/台幣仟元
                    </div>
                </div>
            </div>
        </div>

        <div id="print">
            <table id="main" width="100%" class="display p-4 transparent" cellspacing="0" border='1'>
                <thead>
                    <tr>
                        <th rowspan="3" >交易功能</th>  
                        <th rowspan="3" colspan="2">單位</th>
                        <th colspan="6">企業戶</th>
                        <th colspan="6">個人戶</th>
                        <th rowspan="2" colspan="3">合計</th>
                    </tr>
                    <tr>
                        <th colspan="2">交易密碼</th>
                        <th colspan="2">電子簽章(i-key)</th>
                        <th colspan="2">晶片金融卡</th>
                        <th colspan="2">交易密碼(SSL)</th>
                        <th colspan="2">電子簽章(i-key)</th>
                        <th colspan="2">晶片金融卡</th>                           
                    </tr>
                    <tr>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>成功</th>
                        <th>失敗</th>
                        <th>失敗%</th>                
                    </tr>
                </thead>
                <tbody>
                    <c:catch var="error">
                        <c:forEach items="${Data}" var="item" varStatus="loop">
                            <tr>
                                <c:if test="${loop.index % 6 == 0}">
                                    <td rowspan="6"  align="Left">${item.adopid.equals("ZZZZZZ") ? "合計" : item.adopname}</td>
                                </c:if>
                                <c:if test="${loop.index % 2 == 0}">
                                    <c:choose>
                                        <c:when test="${item.adagreef == -1}">
                                            <td rowspan="2" align="Left">小計</td>
                                        </c:when>
                                        <c:when test="${item.adagreef == 0}">
                                            <td rowspan="2" align="Left">非約轉</td>
                                        </c:when>
                                        <c:when test="${item.adagreef == 1}">
                                            <td rowspan="2" align="Left">約轉</td>
                                        </c:when>
                                    </c:choose>
                                </c:if>
                                <c:choose>
                                    <c:when test="${item.qtytype == 0}">
                                        <td align="Left">筆數</td>
                                    </c:when>
                                    <c:when test="${item.qtytype == 1}">
                                        <td align="Left">金額</td>
                                    </c:when>
                                </c:choose>                                                 

                                <td align="right">${item.c00count}</td>
                                <td align="right">${item.c01count}</td>
                                <td align="right">${item.c10count}</td>
                                <td align="right">${item.c11count}</td>
                                <td align="right">${item.c20count}</td>
                                <td align="right">${item.c21count}</td>

                                <td align="right">${item.p00count}</td>
                                <td align="right">${item.p01count}</td>
                                <td align="right">${item.p10count}</td>
                                <td align="right">${item.p11count}</td>
                                <td align="right">${item.p20count}</td>
                                <td align="right">${item.p21count}</td>

                                <td align="right">${item.t99}</td>
                                <td align="right">${item.f99}</td>
                                <td align="right">${item.tf}</td>
                                <!-- <fmt:parseNumber var="t99" integerOnly="true" value="${item.t99}" />
                                <fmt:parseNumber var="f99" integerOnly="true" value="${item.f99}" />

                                <td align="right">
                                    <fmt:formatNumber type="number" maxFractionDigits="2" value="${f99 / (t99 + f99) * 100}" />
                                </td> -->

                            </tr>
                        </c:forEach>
                    </c:catch>                                             
                </tbody>
            </table>    
            <c:if test="${error != null}">
                <br><span style="color: red;">${error.message}</span>
                <br>${error}
            </c:if>                       
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                $('#DateFrom').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: false,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });
                $('#DateTo').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: false,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });

                $("#yearS").val("${B704Model.yearS}");
                $("#monthS").val("${B704Model.monthS}");

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": false,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 100,
                    "order": [[0, "asc"]],
                    "dom": "<'clear'>frtip",   //無工具列
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });

            }); //ready
        
            function onQuery() {
                $("#B704").submit();             
            }

            function onExportCSV() {
                $.ajax({
                        url:"<%= request.getContextPath() %>/B704/DownloadtoCSV",
                        type:"POST",
                        data:{
                            yearS: $("#yearS").val(),
                            monthS: $("#monthS").val(),
                        },
                        success: function(response, status, xhr) {
                            // check for a filename
                            var filename = "";
                            var disposition = xhr.getResponseHeader('Content-Disposition');
                            if (disposition && disposition.indexOf('attachment') !== -1) {
                                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                                var matches = filenameRegex.exec(disposition);
                                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                            }

                            var type = xhr.getResponseHeader('Content-Type');
                            var blob = new Blob(["\ufeff" + response], { type: type });

                            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                                // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                                window.navigator.msSaveBlob(blob, filename);
                            } else {
                                var URL = window.URL || window.webkitURL;
                                var downloadUrl = URL.createObjectURL(blob);

                                if (filename) {
                                    // use HTML5 a[download] attribute to specify filename
                                    var a = document.createElement("a");
                                    // safari doesn't support this yet
                                    if (typeof a.download === 'undefined') {
                                        window.location = downloadUrl;
                                    } else {
                                        a.href = downloadUrl;
                                        a.download = filename;
                                        document.body.appendChild(a);
                                        a.click();
                                    }
                                } else {
                                    window.location = downloadUrl;
                                }

                                setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                            }
                        },
                        error:function(){
                            alert("error");
                        }
                });                
            }

        </script>
    </body>
</html>