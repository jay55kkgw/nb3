<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<% response.setHeader("Content-Security-Policy", "frame-ancestors 'self'"); %>

<html>

<head>
    <title>台企銀 後台首頁</title>

    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>交易管理查詢</h2>
        <form id="B201" name="B201" class="container main-content-block p-4" >
            <div class="form-group row">
                <label class="col-3 control-label text-right">SELECT </label>
                <div class="col-12">
                    <input type="text" id="COLUMNS" name="COLUMN" class="form-control upper"/>
                </div>
               
            </div>
             <div class="form-group row">
             	<label class="col-3 control-label text-right"> FROM TXNLOG T WHERE </label>
                <div class="col-12">
                	<input type="text" id="CONDITION" name="CONDITION" class="form-control upper"/>
                </div>
             </div>
            
            <div class="form-group row">
                <div class="offset-3 col-9">
                    <input id="btnClear" type="reset" value="重新輸入" class="btn btn-dark" aria-label="Left Align" />&nbsp;
                    <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />
                </div>
            </div>
        </form>
    </div>
    
	<div id="result" style="white-space: normal;word-break:break-all;word-wrap:break-word;">
		
	
	</div>

    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
    <script type="text/javascript">
        $(document).ready(function () {
			$("#result").hide();

        }); //ready

        function onQuery() {
            $("#result").hide();
            var COLUMNS = $("#COLUMNS").val();
            var CONDITION = $("#CONDITION").val();
            if ( COLUMNS == "" || CONDITION == "" ) {
                alert("請輸入查詢欄位及條件");
                return;
            }

            var url = "<%= request.getContextPath() %>/B201/QueryALL";
           
            lockScreen();
            $.post(url, {
                "COLUMNS" : COLUMNS,
                "CONDITION" : CONDITION,
            }, function (data, status) {
                unlockScreen();
                console.log("data="+data)
               $("#result").html(data);
               $("#result").show();
            });
        }
    </script>
</body>

</html>