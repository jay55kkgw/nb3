<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-md-12">
    <div class="content">
        <table id="tblDialog" class="display p-4 transparent" cellspacing="0">
            <thead>
                <tr>
                    <th>欄位名稱</th>
                    <th>欄位值</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="item" items="${items}" varStatus="loop">
                    <tr>
                        <td>${item.key}</td>
                        <td>${item.value}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<script>
    $("#tblDialog").DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false,
        "language": {
            "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
        }
    });
</script>