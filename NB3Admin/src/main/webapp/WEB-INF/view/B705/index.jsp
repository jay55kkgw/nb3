<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>一般網路銀行轉帳級距統計表</h2>
            <form id="B705" name="B705" class="container main-content-block p-4" action="<%= request.getContextPath() %>/B705/Query" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >查詢期間：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <select class="form-control" id="yearS" name="yearS">
                                <option value="">--請選擇--</option>
                                <c:forEach items="${years}" var="year">
                                    <option value="${year}">${year}</option>
                                </c:forEach>
                            </select>&nbsp;年&nbsp;
                            <select class="form-control" id="monthS" name="monthS">
                                <option value="">--請選擇--</option>
                                <c:forEach items="${months}" var="month">
                                    <option value="${month}">${month}</option>
                                </c:forEach>
                            </select>&nbsp;月
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnexpCSV" type="button" value="匯出CSV" class="btn btn-secondary" aria-label="Left Align" onclick="onExportCSV();" />&nbsp;
                        <input id="btnPrint" type="button" value="列印" class="btn btn-secondary" aria-label="Left Align" onclick="friendlyPrint('printQ,print');" />
                    </div>
                </div>
            </form>
        </div>
        <!-- report -->
        <div id="printQ">
            <div class="form-group row">
                <label class="col-12 control-label text-center"><h4>一般網路銀行轉帳級距統計表</h4></label>                
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >查詢時間：</label>
                <div class="col-10">
                    <div class="radio">
                        <div class="form-inline">
                            <jsp:useBean id="now" class="java.util.Date" />
                            <fmt:formatDate var="nowDateTime" value="${now}" pattern="yyyy/MM/dd HH:mm:ss" />
                            ${nowDateTime}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >查詢期間：</label>
                <div class="col-10">
                    <div class="form-inline">
                        ${B704Model.yearS}/${B704Model.monthS}                        
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >單位：</label>
                <div class="col-10">
                    <div class="form-inline">
                        筆/台幣仟元
                    </div>
                </div>
            </div>
        </div>

        <div id="print">
            <table id="main" width="100%" class="display p-4 transparent" cellspacing="0" border='1'>
                <thead>
                    <tr>
                        <th>交易功能\金額級距</th>  
                        <th></th>
                        <th>10萬(含)以下</th>  
                        <th>10-20萬(含)</th>  
                        <th>20-30萬(含)</th>  
                        <th>30-40萬(含)</th>  
                        <th>40-50萬(含)</th>  
                        <th>50-60萬(含)</th>  
                        <th>60-70萬(含)</th>  
                        <th>70-80萬(含)</th>  
                        <th>80-90萬(含)</th>  
                        <th>90-100萬(含)</th>  
                        <th>100-150萬(含)</th>  
                        <th>150-200萬(含)</th>  
                        <th>200-250萬(含)</th>  
                        <th>250-300萬(含)</th>  
                    </tr>
                </thead>
                <tbody>
                    <c:catch var="error">
                        <c:forEach items="${Data}" var="item" varStatus="loop">
                            <tr>
                                <c:if test="${loop.index % 3 == 0}">
                                    <td rowspan="3"  align="Left">${item.adopname}</td>
                                </c:if>
                                <c:choose>
                                    <c:when test="${item.adagreef == -1}">
                                        <td align="Left">小計</td>
                                    </c:when>
                                    <c:when test="${item.adagreef == 0}">
                                        <td align="Left">非約轉</td>
                                    </c:when>
                                    <c:when test="${item.adagreef == 1}">
                                        <td align="Left">約轉</td>
                                    </c:when>
                                </c:choose>

                                <td align="right">${item.r00}</td>
                                <td align="right">${item.r01}</td>
                                <td align="right">${item.r02}</td>
                                <td align="right">${item.r03}</td>
                                <td align="right">${item.r04}</td>
                                <td align="right">${item.r05}</td>

                                <td align="right">${item.r06}</td>
                                <td align="right">${item.r07}</td>
                                <td align="right">${item.r08}</td>
                                <td align="right">${item.r09}</td>
                                <td align="right">${item.r10}</td>
                                <td align="right">${item.r11}</td>

                                <td align="right">${item.r12}</td>
                                <td align="right">${item.r13}</td>
                            </tr>
                        </c:forEach>
                    </c:catch>                                             
                </tbody>
            </table>    
            <c:if test="${error != null}">
                <br><span style="color: red;">${error.message}</span>
                <br>${error}
            </c:if>                       
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                $('#DateFrom').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: false,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });
                $('#DateTo').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: false,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });

                $("#yearS").val("${B704Model.yearS}");
                $("#monthS").val("${B704Model.monthS}");

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": false,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 100,
                    "order": [[0, "asc"]],
                    "dom": "<'clear'>frtip",   //無工具列
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });

            }); //ready
        
            function onQuery() {
                $("#B705").submit();             
            }
            
            function onExportCSV() {
                $.ajax({
                        url:"<%= request.getContextPath() %>/B705/DownloadtoCSV",
                        type:"POST",
                        data:{
                            yearS: $("#yearS").val(),
                            monthS: $("#monthS").val(),
                        },
                        success: function(response, status, xhr) {
                            // check for a filename
                            var filename = "";
                            var disposition = xhr.getResponseHeader('Content-Disposition');
                            if (disposition && disposition.indexOf('attachment') !== -1) {
                                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                                var matches = filenameRegex.exec(disposition);
                                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                            }

                            var type = xhr.getResponseHeader('Content-Type');
                            var blob = new Blob(["\ufeff" + response], { type: type });

                            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                                // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                                window.navigator.msSaveBlob(blob, filename);
                            } else {
                                var URL = window.URL || window.webkitURL;
                                var downloadUrl = URL.createObjectURL(blob);

                                if (filename) {
                                    // use HTML5 a[download] attribute to specify filename
                                    var a = document.createElement("a");
                                    // safari doesn't support this yet
                                    if (typeof a.download === 'undefined') {
                                        window.location = downloadUrl;
                                    } else {
                                        a.href = downloadUrl;
                                        a.download = filename;
                                        document.body.appendChild(a);
                                        a.click();
                                    }
                                } else {
                                    window.location = downloadUrl;
                                }

                                setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                            }
                        },
                        error:function(){
                            alert("error");
                        }
                });                
            }

        </script>
    </body>
</html>