<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">     
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>個人訊息公告內容刊登</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >上架起～迄日：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <input type="text" id="DateFrom" autocomplete="off" name="DateFrom" class="form-control" style = "width:160px" maxlength="10" value="${StartDt}" />～
                            <input type="text" id="DateTo" autocomplete="off" name="DateTo" class="form-control" style = "width:160px" maxlength="10" value="${EndDt}" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >案件狀態：</label>
                    <div class="col-2">
                       	<select id="FlowFinished" class="form-control">
                       		<option value="Y">已結案(已上架)</option>
                       		<option value="N">未結案</option>
                       	</select>                    
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >上架網站：</label>
                    <div class="col-2">
                       	<select id="ANNCHANNEL">
                       		<option value="A">全部</option>
                       		<option value="E">EATM</option>
                       		<option value="N">NB</option>
                       	</select>                    
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >公告標題：</label>
                    <div class="col-10">
                        <input type="text" id="TITLE" name="TITLE" class="form-control" maxlength="50" value=''/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnClear" type="reset" value="重新輸入" class="btn btn-dark" aria-label="Left Align" />&nbsp;
                        <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnCreate" type="button" value="新增" class="btn btn-info" aria-label="Left Align" onclick="onCreate()" />
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:97%">
            <thead>
                <tr>
                    <th style="width: 10%">序號</th>
                    <th style="width: 40%">公告標題</th>
                    <th style="width: 10%">狀態</th>
                    <th style="width: 10%">公告上架起日</th>
                    <th style="width: 10%">公告上架迄日</th>
                    <th style="width: 8%">部門代碼</th>
                    <th style="width: 9%">姓名</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <span>如案件已結案，部門代碼／姓名請按已結案連結做查詢</span>

        <jsp:include page="../include/caseStatus.jsp"></jsp:include>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
        <script>
            var admAnnTmp=[];

            $(document).ready(function () {
                $("#main").hide();
                $('#DateFrom').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });
                $('#DateTo').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });
                onQuery();
            }); //ready
        
            // 新增
            function onCreate() {
                location.href = "<%= request.getContextPath() %>/B502/Create";
            }

            function onDataBind() {
                $("#main").dataTable({
                    "responsive": true,
                    "pageLength": 10, 
                    "paging": true,
                    "bFilter": false,
                    "bDestroy": true,
                    "bSort": false, 
                    "bLengthChange": false, 
                    "aaData": admAnnTmp,
                    "autoWidth":false,
                    "aoColumns": [
                        { "data": "id" },
                        { "data": "title" },
                        { "data": "stepname" },
                        { "data": "id" },
                        { "data": "id" },
                        { "data": "oid"},
                        { "data": "lastuser" }
                    ],
                    "createdRow": function (row, data, index) {
                        // ID
                        var $id = $("<a>"+data.id+"<a/>");
                        $id.attr("href", "<%= request.getContextPath() %>/B502/Query/"+data.id);
                        $("td", row).eq(0).text("").append($id);

                        var $caseStatus;
                        if ( data.stepname == "" ) {
                            $caseStatus = $("<a href='#'>已結案<a/>");
                        } else {
                            $caseStatus = $("<a href='#'>"+data.stepname+"<a/>");
                        }
                        $caseStatus.attr("onclick", "showStatus('"+data.id+"');");
                        $("td", row).eq(2).text("").append($caseStatus);

                        // 處理起日                
                        var startDt = data.startdate.substring(0,4)+"-"+data.startdate.substring(4,6)+"-"+data.startdate.substring(6,8);
                        startDt += " "+data.starttime.substring(0,2)+":"+data.starttime.substring(2,4)
                        
                        $("td", row).eq(3).text(startDt);

                        var endDt = data.enddate.substring(0,4)+"-"+data.enddate.substring(4,6)+"-"+data.enddate.substring(6,8);
                        endDt += " "+data.endtime.substring(0,2)+":"+data.endtime.substring(2,4)
                        if($("#FlowFinished").val() !='Y'){
                            $("td", row).eq(5).text(data.editoroid);
                            $("td", row).eq(6).text(data.editoruname);
                        }
                        $("td", row).eq(4).text(endDt);
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                
                $("#main").show();
                return false;
            }

            function onQuery() {
                $("span.error").remove();

                var sisvalid = Date.parse($("#DateFrom").val());
                var eisvalid = Date.parse($("#DateTo").val());
                
                if(isNaN(sisvalid) || isNaN(eisvalid) ) { 
                    alert("請輸入合法的起日和迄日");
                    return;
                }

                if ( sisvalid > eisvalid ) {
                    alert("迄日必需大於起日");
                    return;
                }
                var url = "<%= request.getContextPath() %>/B502/IndexQuery";
                lockScreen();
                $.post(url, {
                    "DateFrom" : $("#DateFrom").val(),
                    "DateTo" : $("#DateTo").val(),
                    "TITLE" : $("#TITLE").val(),
                    "FlowFinished": $("#FlowFinished").val(),
                    "ANNCHANNEL" : $("#ANNCHANNEL").val()
                }, function (data, status) {
                    unlockScreen();
                    if (status == "success") {
                        //alert(data);
                        admAnnTmp = data;
                        onDataBind();
                    }
                });
            }
        </script>
    </body>
</html>