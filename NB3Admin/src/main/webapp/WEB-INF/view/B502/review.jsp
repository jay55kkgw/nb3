<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>
    
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>個人訊息公告內容刊登-覆核</h2>
            <form class="container main-content-block p-4" action="/dummy" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right">公告標題：</label>
                    <label class="col-10 control-label">${Data.TITLE}</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">公告名單：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            <label class="control-label">${Data.FILENAME}</label>&nbsp;&nbsp;
                            <label class="control-label">全部客戶：</label>
                            <c:choose>
                                <c:when test="${Data.ALLUSER=='1'}">
                                    <label class="control-label">是</label>
                                </c:when>
                                <c:otherwise>
                                    <label class="control-label">否</label>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${Data.ALLUSER=='0'}">
                        <div class="form-group row">
                            <label class="col-2 control-label text-right"></label>
                            <div class="col-10">
                                <c:forEach var="u" items="${User}">
                                    <label class="col-2 control-label text-right">${u}</label>
                                </c:forEach>
                            </div>
                        </div>
                    </c:when>
                </c:choose>
                 <div class="form-group row">
	                <label class="col-2 control-label text-right">公告種類：</label>
	                <c:choose>
	                    <c:when test="${Data.CONTENTTYPE=='1'}">
	                        <label class="col-10 control-label">網頁連結</label>
	                    </c:when>
	                    <c:when test="${Data.CONTENTTYPE=='2'}">
	                        <label class="col-10 control-label">檔案</label>
	                    </c:when>
	                    <c:when test="${Data.CONTENTTYPE=='3'}">
	                        <label class="col-10 control-label">文字敘述</label>
	                    </c:when>
	                </c:choose>
           		 </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">重要公告：</label>
                    <c:choose>
                        <c:when test="${Data.TYPE=='1'}">
                            <label class="col-10 control-label">是</label>
                        </c:when>
                        <c:otherwise>
                            <label class="col-10 control-label">否</label>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">行銷活動 URL：</label>
                    <label class="col-10 control-label">${Data.URL}</label>
                </div>
                <div class="form-group row">
	                <label class="col-2 control-label text-right">檔案來源：</label>
			        <c:if test="${!empty Data.SRCFILENAME}">
			        		<input id="btnPreviewFile" type="button" class="btn btn-info" value="預覽檔案" onclick="onPreviewFile();" />
			        		<input type="hidden" id="ID" value="${Data.ID}" />
			        		 <input type="hidden" id="isTmp" value="false"/>
			        </c:if>
            	</div>
            	<div class="form-group row">
                	<label class="col-2 control-label text-right">文字敘述：</label>
                	<label class="col-10 control-label">${Data.CONTENT}</label>
           		</div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">排序：</label>
                    <label class="col-10 control-label">${Data.SORTORDER}</label>
                </div>
                 <div class="form-group row">
                    <label class="col-2 control-label text-right">上架網站：</label>
                    <c:choose>
					   <c:when test="${Data.ANNCHANNEL == 'A'}">
					   		<label class="col-10 control-label">全部</label>
					   </c:when>
					     <c:when test="${Data.ANNCHANNEL == 'E'}">
					   		<label class="col-10 control-label">EATM</label>
					   </c:when>
					   <c:when test="${Data.ANNCHANNEL == 'N'}">
					   		<label class="col-10 control-label">NB</label>
					   </c:when>
					</c:choose>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right">上架日期：</label>
                    <label class="col-4 control-label">${StartDateTime}</label>
                    <label class="col-2 control-label text-right">下架日期：</label>
                    <label class="col-4 control-label">${EndDateTime}</label>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >退回原因：</label>
                    <div class="col-10">
                        <input type="text" id="Comments" name="Comments" class="form-control" maxlength="50" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input id="btnExit" type="button" value="離開" class="btn btn-dark" aria-label="Left Align" onclick="onExit()" />&nbsp;               
                        <input id="btnReject" type="button" value="退回" class="btn btn-danger" aria-label="Left Align" onclick="onReject()" />&nbsp;
                        <input id="btnApprove" type="button" value="覆核" class="btn btn-info" aria-label="Left Align" onclick="onApprove()" />
                    </div>
                </div>
            </form>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script>
            $(document).ready(function () {
            }); //ready

            // 回查詢頁
            function onExit() {
                location.href="<%= request.getContextPath() %>/B502";
            }

            // 預覽廣告
            function onPreview() {
                window.open("<%= request.getContextPath() %>/B502/CasePreview/${Data.ID}");
            }

            // 預覽檔案
            function onPreviewFile() {
                var id = $("#ID").val();
                window.open("<%= request.getContextPath() %>/B502/PreviewFile/"+id+"?isTmp="+$("#isTmp").val());
            }
            // 同意
            function onApprove() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B502/Approve/${Data.ID}",
                    type: "POST",
                    data: {
                        "StepId":"${Data.STEPID}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("個人訊息公告內容刊登覆核成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B502";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            // 退回
            function onReject() {
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>/B502/Reject/${Data.ID}",
                    type: "POST",
                    data: {
                        "StepId":"${Data.STEPID}",
                        "Comments":$("#Comments").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("個人訊息公告內容刊登退回成功,案號："+res.pkey);   
                            location.href="<%= request.getContextPath() %>/B502";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
        </script>
    </body>
</html>