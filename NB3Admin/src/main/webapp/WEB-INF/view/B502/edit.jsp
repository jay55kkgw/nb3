<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<title>台企銀 後台首頁</title>
<meta charset="UTF-8">

<!-- include 後台管理 css -->
<jsp:include page="../include/admheadercs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css"	href="<%=request.getContextPath()%>/css/bootstrap-dialog.css">
<link rel="stylesheet" type="text/css"	href="<%=request.getContextPath()%>/script/ztree/css/zTreeStyle/zTreeStyle.css">
<link rel="stylesheet" type="text/css"	href="<%=request.getContextPath()%>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"	href="<%=request.getContextPath()%>/script/DateTimePicker/jquery.datetimepicker.css">
</head>
<body>
	<!-- include master page menu -->
	<jsp:include page="../include/admmenu.jsp"></jsp:include>

	<!-- body content start -->
	<div class="container" id="main-content">
		<c:if test="${IsReject}">
			<h2>個人訊息公告內容刊登-退回</h2>
		</c:if>
		<c:if test="${!IsReject}">
			<h2>個人訊息公告內容刊登</h2>
		</c:if>
		<form class="container main-content-block p-4" action="/dummy"
			method="post">
			<input type="hidden" id="ID" value="${Data.ID}" />
			<div class="form-group row">
				<label class="col-3 control-label text-right">公告標題：</label>
				<div class="col-9">
					<input type="text" id="TITLE" name="TITLE" class="form-control"
						value="${Data.TITLE}" maxlength="20"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-3 control-label text-right">公告名單：</label>
				<div class="col-9">
					<div class="form-inline">
						<input type="file" id="file" name="file" class="form-control"
							maxlength="50" />&nbsp;  <span id="UploadName"></span>&nbsp;<label class="control-label">全部客戶：</label>
                        <input type="checkbox" id="ALLUSER" name="ALLUSER" />
                       
					</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="offset-3 col-9">
					<div class="form-inline">
						<input type="button" class="btn btn-info" value="上傳名單"
							onclick="onUpload();" /> &nbsp; <input id="btnPreview"
							type="button" class="btn btn-info" value="預覽名單"
							onclick="onPreview();" />
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-3 control-label text-right">公告種類：</label>
				<div class="col-9">
					<input type="radio" id="ContentType" name="ContentType" value="1">網頁連結&nbsp;
					<input type="radio" id="ContentType" name="ContentType" value="2">檔案&nbsp;
					<input type="radio" id="ContentType" name="ContentType" value="3">文字敘述
				</div>
			</div>
			<c:if test="${important}">
				<div class="form-group row">
					<label class="col-3 control-label text-right">重要公告：</label>
					<div class="col-1">
						<input type="checkbox" id="TYPE" name="TYPE" class="form-control" />
					</div>
				</div>
			</c:if>
			<div class="form-group row">
				<label class="col-3 control-label text-right">行銷活動 URL：</label>
				<div class="col-9">
					<input type="text" id="URL" name="URL" class="form-control"	value="${Data.URL}" maxlength="512" disabled="disabled"  />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-3 control-label text-right">來源檔案：</label>
				<div class="col-6">
					<input type="file" id="SourceFile" name="SourceFile" class="form-control" disabled="disabled" />
                </div>
                <div class="col-3">
                    <span id="uploadFileName"></span>
                </div>
			</div>
			<div class="form-group row">
				<div class="offset-3 col-9">
					<div class="form-inline">
						<input id=btnUploadFile type="button" class="btn btn-info" value="上傳檔案" onclick="onUploadFile();" />&nbsp;
						<input id="btnPreviewFile" type="button" class="btn btn-info" value="預覽檔案" onclick="onPreviewFile();" />&nbsp;(限上傳.pdf、.jpg、.png、.gif檔案)
						<input type="hidden" id="fileGuid" /> 
						<input type="hidden" id="isTmp" value="false" />
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-3 control-label text-right">文字敘述：</label>
				<div class="col-9">
					<textarea id="CONTENT" name="CONTENT" cols="90" rows="5">${Data.CONTENT}</textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-3 control-label text-right" >排序：</label>
				<div class="col-3">
					<input type="number" id="SORTORDER" name="SORTORDER" style="width: 160px"
						class="form-control" value="${Data.SORTORDER}" />
				</div>
			   <label class="col-3 control-label text-right" >上架網站：</label>
               <div class="col-3">
                   	<select id = "ANNCHANNEL" style="width: 160px">
                   		<option value = "A">全部</option>
                   		<option value = "E">eATM</option>
                   		<option value = "N">NB</option>
                   	</select>
               </div>
			</div>
			<div class="form-group row">
				<label class="col-3 control-label text-right">上架日期：</label>
				<div class="col-3">
					<div class="form-inline">
						<input type="text" id="StartDateTime" autocomplete="off"
							name="StartDateTime" class="form-control" style="width: 160px"
							maxlength="17" />
					</div>
				</div>
				<label class="col-3 control-label text-right">下架日期：</label>
				<div class="col-3">
					<div class="form-inline">
						<input type="text" id="EndDateTime" autocomplete="off"
							name="EndDateTime" class="form-control" style="width: 160px"
							maxlength="17" />
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-3 control-label text-right">意見：</label>
				<div class="col-9">
					<input type="text" id="comments" name="comments"
						class="form-control" maxlength="255" value="${returnReason}" />
				</div>
			</div>
			<div class="form-group row">
				<div class="offset-3 col-9">
					<input id="btnExit" type="button" value="離開" class="btn btn-dark"
						aria-label="Left Align" onclick="onExit()" />&nbsp; <input
						id="btnCancel" type="button" value="刪除" class="btn btn-danger"
						aria-label="Left Align" onclick="onCancel()" />&nbsp; <input
						id="btnOK" type="button" value="確定" class="btn btn-info"
						aria-label="Left Align" onclick="onSave()" />
				</div>
			</div>
		</form>
	</div>
	<!-- body content end -->

	<!-- include master page footer -->
	<jsp:include page="../include/admfooter.jsp"></jsp:include>

	<!-- include 後台管理 js -->
	<jsp:include page="../include/admfooterjs.jsp"></jsp:include>

	<script src="<%=request.getContextPath()%>/script/respond.js"></script>
	<script	src="<%=request.getContextPath()%>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
	<script	src="<%=request.getContextPath()%>/script/DateTimePicker/jquery.datetimepicker.js"></script>

	<script>
	
            var fileName="${Data.FILENAME}";        // 上傳檔名
            var users="${Data.USERLIST}";           // 名單
            
            $(document).ready(function () {
                $("#ANNCHANNEL").val("${Data.ANNCHANNEL}");
                $('#StartDateTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false
                });
                $('#EndDateTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false
                });
                $('#btnPreviewFile').hide();
                $('#btnUploadFile').attr('disabled', true);
                $("#StartDateTime").val("${StartDateTime}");
                $("#EndDateTime").val("${EndDateTime}");
                if ( "${Data.ALLUSER}" == "1" ) {
                    $("#ALLUSER").prop('checked', true);
                } else {
                    $("#ALLUSER").prop('checked', false);
                }
                if ( "${Data.TYPE}" == "1" ) {
                    $("#TYPE").prop('checked', true);
                } else {
                    $("#TYPE").prop('checked', false);
                } 
                if ( "${Data.FILENAME}" != "" ) {
                    $("#UploadName").html("${Data.FILENAME}");
                    $("#btnPreview").show();
                } else {
                    $("#btnPreview").hide();
                }
                switch ("${Data.CONTENTTYPE}")
                {
                	case "1":
               			$('input:radio[name=ContentType]')[0].checked = true;
               			$('#URL').attr('disabled', false);
                    	$('#SourceFile').attr('disabled', true);
                    	$('#btnUploadFile').attr('disabled', true);
                    	$('#CONTENT').attr('disabled', true);
                    	break;
                	case "2":
               			$('input:radio[name=ContentType]')[1].checked = true;
               			$('#URL').attr('disabled', true);
                    	$('#SourceFile').attr('disabled', false);
                    	$('#btnUploadFile').attr('disabled', false);
               			$('#btnPreviewFile').show();
               			$('#CONTENT').attr('disabled', true);
                        $("#uploadFileName").html("${Data.SRCFILENAME}");
                    	break;
                	case "3":
               			$('input:radio[name=ContentType]')[2].checked = true;
                      	$('#URL').attr('disabled', true);
                    	$('#SourceFile').attr('disabled', true);
                    	$('#btnUploadFile').attr('disabled', true);
                    	$('#CONTENT').attr('disabled', false);
                    	break;
                }
                $('input[type=radio][name=ContentType]').change(function() {
                    if (this.value == '1') {
                    	$('#URL').attr('disabled', false);
                    	$('#SourceFile').attr('disabled', true);
                    	$('#btnUploadFile').attr('disabled', true);
                    	$('#CONTENT').attr('disabled', true);
                    }
                    else if (this.value == '2') {
                    	$('#URL').attr('disabled', true);
                    	$('#SourceFile').attr('disabled', false);
                    	$('#btnUploadFile').attr('disabled', false);
                    	$('#CONTENT').attr('disabled', true);
                    }
                    else if (this.value == '3') {
                    	$('#URL').attr('disabled', true);
                    	$('#SourceFile').attr('disabled', true);
                    	$('#btnUploadFile').attr('disabled', true);
                    	$('#CONTENT').attr('disabled', false);
                    }
                });
            }); //ready

            // 上傳公告
            function onUpload() {
                var fileData = new FormData();
                var file = $("#file").get(0);

                if ( file.files.length == 0 ) {
                    alert("請選擇公告名單檔案");
                    return;
                } else {
                    fileData.append("multipartFile", file.files[0]);
                }

                lockScreen();
                $.ajax({
                    url: "<%=request.getContextPath()%>/B502/Upload",
                    type: "POST",
                    contentType: false,  // Not to set any content header
                    processData: false,  // Not to process data
                    data: fileData,
                    success: function (data) {
                        unlockScreen();
                        if ( data.status == "0" ) {
                            fileName = data.filename;
                            users = data.users;
                            $("#UploadName").html("");
                            $("#btnPreview").show();
                            alert("名單上傳成功");
                        } else {
                            alert(data.status);
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onPreview() {
                var showData = users.replace(',','\n');
                alert(showData);
            }
            
            // 回查詢頁
            function onExit() {
                location.href="<%=request.getContextPath()%>/B502";
            }

         // 上傳檔案
            function onUploadFile() {
                var fileData = new FormData();
                var file = $("#SourceFile").get(0);

                if ( file.files.length == 0 ) {
                    alert("請選擇上傳檔案");
                    return;
                } else {
                    fileData.append("multipartFile", file.files[0]);
                }

                lockScreen();
                $.ajax({
                    url: "<%=request.getContextPath()%>/B502/UploadFile",
                    type: "POST",
                    contentType: false,  // Not to set any content header
                    processData: false,  // Not to process data
                    data: fileData,
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("檔案上傳成功");   
                            $("#fileGuid").val(res.pkey);
                            $("#isTmp").val("true");
                            $("#btnPreviewFile").show();
                            $("#uploadFileName").html("");
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.responseText);
                    }
                });
            }
            
            // 預覽檔案
            function onPreviewFile() {
                var id=$("#ID").val();
                var fileGuid = $("#fileGuid").val();
                
                if ( fileGuid === "" ) {
                    window.open("<%=request.getContextPath()%>/B502/PreviewFile/"+id+"?isTmp="+$("#isTmp").val());
                } else {
                    window.open("<%=request.getContextPath()%>/B502/PreviewFile/"+fileGuid+"?isTmp="+$("#isTmp").val());
                }
            }

            // 新增
            function onSave() {
                $("span.error").remove();
                var title=$("#TITLE").val();
                if (title=="")
                {
                    alert("請輸入公告標題")
                    return;
                }
                var alluser = $("#ALLUSER").prop('checked') ? "1" : "0";
                if ( alluser == "0" && fileName == "" ) {
                    alert("請選擇公告名單檔案並上傳");
                    return;
                }
                var contentType=$('input[name=ContentType]:checked').val();
                switch(contentType)
                {
                	case "1":
                         if ( $("#URL").val() == "" ) {
                        	   alert("請輸入行銷活動URL");
                             return;
                         }
                    	break;	
                	case "2":
                	     if ( $("#fileGuid").val() == "" &&  $("#uploadFileName").html() == "") {
                              alert("請上傳檔案");
                              return;
                         }
                    	break;
                	case "3":
                	    if ($("#CONTENT").val() == "")
                	    {
                	    	alert("請輸入文字敘述");
                	        return;
                	    }
                    	break;	
                }
                var sisvalid = Date.parse($("#StartDateTime").val());
                var eisvalid = Date.parse($("#EndDateTime").val());

//                 if(isNaN(sisvalid) || isNaN(eisvalid) ) { 
//                     alert("請輸入合法的上架日期和下架日期");
//                     return;
//                 }

                if ( sisvalid > eisvalid ) {
                    alert("下架日期必需大於上架日期");
                    return;
                }
                /*var today=new Date();
                if ( today>sisvalid ) {
                    alert("上架日期必須大於今天");
                    return;
                }*/
                
                var userlist = alluser == "1" ? "" : users;
                lockScreen();
                $.ajax({
                    url: "<%=request.getContextPath()%>/B502/ReSend",
                    type: "POST",
                    data: {
                        "ID": "${Data.ID}",
                        "CONTENT": $("#CONTENT").val(),
                        "ALLUSER": alluser,
                        "USERLIST": users,
                        "FILENAME": fileName,
                        "TYPE": $("#TYPE").prop('checked') ? "1" : "0",
                        "URL": $("#URL").val(),
                        "SORTORDER": $("#SORTORDER").val(),
                        "StartDateTime": $("#StartDateTime").val(),
                        "EndDateTime": $("#EndDateTime").val(),
                        "STEPID": "${Data.STEPID}",
                        "comments": $("#comments").val(),
                        "fileGuid": $("#fileGuid").val(),
                        "TITLE": $("#TITLE").val(),
                        "CONTENTTYPE": contentType,
                        "ANNCHANNEL" :$("#ANNCHANNEL").val()
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("個人訊息公告內容刊登送審成功,案號："+res.pkey);   
                            location.href="<%=request.getContextPath()%>/B502";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                	alert(value);
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            function onCancel() {
                $("span.error").remove();
                lockScreen();
                $.ajax({
                    url: "<%=request.getContextPath()%>/B502/Cancel",
                    type: "POST",
                    data: {
                        "ID" : "${Data.ID}",
                        "OID" : "${Data.OID}"
                    },
                    success: function (res) {
                        unlockScreen();
                        if(res.validated){
                            alert("個人訊息公告內容刊登刪除成功,案號："+res.pkey);   
                            location.href="<%=request.getContextPath()%>/B502/Index";
                        } else {
                            //Set error messages
                            $.each(res.errorMessages,function(key,value){
                                if ( key === "summary" )
                                    alert(value);
                                else 
                                	alert(value);
                                    $('input[name='+key+']').after('<span class="error">'+value+'</span>');
                            });
                        }
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }
        </script>
</body>
</html>