<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>預約明細查詢</h2>
        <form id="B204" name="B204" class="container main-content-block p-4"
            action="<%= request.getContextPath() %>/B204/Query" method="post">
            <div class="form-group row">
                <label class="col-4 control-label text-right">身分證/營利事業統一編號：</label>
                <div class="col-8">
                    <div class="form-inline">
                        <input type="text" id="UserId" name="UserId" class="form-control upper" maxlength="10" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-4 col-8">
                    <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                    <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align"
                        onclick="onQuery()" />
                </div>
            </div>
        </form>
    </div>

    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
    <script>

        function onQuery() {
            var isError = "";
            if (($("#UserId").val() == "") || ($("#UserId").val() == null)) {
                isError += "請輸入身分證字號或統一編號";
            }

            if (isError != "") {
                alert(isError)
                return;
            }
            $("#B204").submit();
        }

    </script>
</body>

</html>