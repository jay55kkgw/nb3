<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="form-group row">
        <label class="col-12 control-label text-center">
            <h4>預約明細查詢</h4>
        </label>
    </div>
    <div>
        <div class="form-group row">
            <label class="col-3 control-label text-right">查詢時間：</label>
            <div class="col-9">
                <label class="control-label">
                    <jsp:useBean id="now" class="java.util.Date" />
                    <fmt:formatDate var="nowDateTime" value="${now}" pattern="yyyy/MM/dd HH:mm:ss" />
                    ${nowDateTime}
                </label>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-3 control-label text-right">客戶身分證/營利事業統一編號：</label>
            <label class="control-label">${userId}</label>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-12 control-label text-center">台幣預約明細</label>
    </div>
    <table id="mainNTD" class="display p-4 transparent" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>預約編號</th>
                <th>週期</th>
                <th>生效日<br>截止日</th>
                <th>下次轉帳日</th>
                <th>轉出帳號</th>
                <th>轉入帳號/<br>繳費稅代號</th>
                <th style="text-align: right;">轉帳金額</th>
                <th>交易類別</th>
                <th>交易機制</th>
                <th>預約狀態</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr />
    <div class="form-group row"></div>
    <label class="col-12 control-label text-center">外匯預約明細</label>
    </div>
    <table id="mainFC" class="display p-4 transparent" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>預約編號</th>
                <th>週期</th>
                <th>生效日<br>截止日</th>
                <th>下次轉帳日</th>
                <th>轉出帳號</th>
                <th style="text-align: right;">轉出金額</th>
                <th>銀行名稱<br>轉入帳號</th>
                <th style="text-align: right;">轉入金額</th>
                <th>交易類別</th>
                <th>交易機制</th>
                <th>預約狀態</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->
    <script>


        $(document).ready(function () {
            //台幣
            onQueryNTD();
            //外幣
            onQueryFC();

        }); //ready

        function onQueryNTD() {

            $("#mainNTD").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "columnDefs": [
                        { targets: 6, className: 'dt-body-right' },
                        ], 
                "order": [[0, "desc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/B204/QueryNTD",
                    "data": {
                        "USERID": "${userId}"
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "dpschno" },
                    { "data": "dppermtdate" },
                    { "data": null, orderable: false },
                    { "data": "dpnextdate", orderable: false  },
                    { "data": "dpwdac" },
                    { "data": null, orderable: false },
                    { "data": "dptxamt" },
                    { "data": "txtype",orderable: false },
                    { "data": "dptxcode" },
                    { "data": "dptxstatus" },
                ],
                "createdRow": function (row, data, index) {

                    var $dppermtdate = "";
                    switch (data.dptxtype) {
                        case "S":
                            $dppermtdate = "特定日期";
                            break;
                        default:
                            $dppermtdate = "固定每月" + data.dppermtdate + "日";
                            break;
                    }
                    $("td", row).eq(1).text("").append($dppermtdate);

                    var $dpfdate = data.dpfdate;
                    var $dpfdate = $dpfdate.substr(0, 4) + "/" + $dpfdate.substr(4, 2) + "/" + $dpfdate.substr(6, 2);
                    var $dptdate = data.dptdate;
                    var $dptdate = $dptdate.substr(0, 4) + "/" + $dptdate.substr(4, 2) + "/" + $dptdate.substr(6, 2);

                    var $sDate = $dpfdate;
                    if (data.dptdate.length > 0 && data.dpfdate != data.dptdate) {
                        $sDate = $dpfdate + "<br>" + $dptdate;
                    }
                    $("td", row).eq(2).text("").append($sDate);

                    var $dpnextdate = data.dpnextdate.substr(0, 4) + "/" + data.dpnextdate.substr(4, 2) + "/" + data.dpnextdate.substr(6, 2);
                    $("td", row).eq(3).text("").append($dpnextdate);

                    var $dpsv = data.dpsvbh + '<br>' + data.dpsvac;
                    $("td", row).eq(5).text("").append($dpsv);

                    $("td", row).eq(6).text("").append(formatMoney(data.dptxamt));
                    var $codeWord = "";
                    switch ( data.dptxcode ) {
                        case "0":
                            $codeWord = "交易密碼(SSL)";
                            break;
                        case "1":
                            $codeWord="電子簽章(i-key)";
                            break;
                        case "2":
                            $codeWord="晶片金融卡";
                            break;
                        case "3":
                            $codeWord="OTP";
                            break;
                        case "4":
                            $codeWord="軟體憑證(隨護神盾)";
                            break;
                        case "5":
                            $codeWord="快速交易";
                            break;
                        case "6":
                            $codeWord="小額交易";
                            break;
                    }
                    $("td", row).eq(8).text("").append($codeWord);

                    var $statusWord = "";
                    switch (data.dptxstatus) {
                        case "3":
                            $statusWord = "已取消預約";
                            break;
                        case "0":
                            $statusWord = "預約成功";
                            break;
                    }
                    $("td", row).eq(9).text("").append($statusWord);

                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            return false;
        }

        function onQueryFC() {

            $("#mainFC").dataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "bSortable": true,
                "bDestroy": true,
                "pageLength": 10,
                "columnDefs": [
                        { targets: 5, className: 'dt-body-right' },
                        { targets: 7, className: 'dt-body-right' },
                        ], 
                "order": [[0, "asc"]],
                "ajax": {
                    "type": "POST",
                    "url": "<%= request.getContextPath() %>/B204/QueryFC",
                    "data": {
                        "USERID": "${userId}"
                    },
                    "error": function (msg) {
                        alert(msg);
                    }
                },
                "sServerMethod": "post",
                "dom": "<'clear'>frtip",   //無工具列
                "columns": [
                    { "data": "fxschno" },
                    { "data": "fxpermtdate" },
                    { "data": null, orderable: false },
                    { "data": "fxnextdate", orderable: false },
                    { "data": "fxwdac" },
                    { "data": "fxwdamt" },
                    { "data": null, orderable: false },
                    { "data": null, orderable: false },
                    { "data": "txtype",orderable: false  },
                    { "data": "fxtxcode" },
                    { "data": "fxtxstatus" },
                ],
                "createdRow": function (row, data, index) {

                    var $fxpermtdate = "";
                    switch (data.fxtxtype) {
                        case "S":
                            $fxpermtdate = "特定日期";
                            break;
                        default:
                            $fxpermtdate = "固定每月" + data.fxpermtdate + "日";
                            break;
                    }
                    $("td", row).eq(1).text("").append($fxpermtdate);

                    var $fxfdate = data.fxfdate;
                    var $fxfdate = $fxfdate.substr(0, 4) + "/" + $fxfdate.substr(4, 2) + "/" + $fxfdate.substr(6, 2);
                    var $fxtdate = data.fxtdate;
                    var $fxtdate = $fxtdate.substr(0, 4) + "/" + $fxtdate.substr(4, 2) + "/" + $fxtdate.substr(6, 2);

                    var $sDate = $fxfdate;
                    if (data.fxtdate.length > 0 && data.fxfdate != data.fxtdate) {
                        $sDate = $fxfdate + "<br>" + $fxtdate;
                    }
                    $("td", row).eq(2).text("").append($sDate);

                    var $fxnextdate = data.fxnextdate.substr(0, 4) + "/" + data.fxnextdate.substr(4, 2) + "/" + data.fxnextdate.substr(6, 2);
                    $("td", row).eq(3).text("").append($fxnextdate);
                    
                    var $fxwd = data.fxwdcurr + '<br>' + formatMoney(data.fxwdamt);
                    $("td", row).eq(5).text("").append($fxwd);

                    var $fxsv = data.fxsvbh + '<br>' + data.fxsvac;
                    $("td", row).eq(6).text("").append($fxsv);

                    var $fxsv = data.fxsvcurr + '<br>' + formatMoney(data.fxsvamt);
                    $("td", row).eq(7).text("").append($fxsv);

                    var $codeWord = "";
                    switch ( data.fxtxcode ) {
                        case "0":
                            $codeWord = "交易密碼(SSL)";
                            break;
                        case "1":
                            $codeWord="電子簽章(i-key)";
                            break;
                        case "2":
                            $codeWord="晶片金融卡";
                            break;
                        case "3":
                            $codeWord="OTP";
                            break;
                        case "4":
                            $codeWord="軟體憑證(隨護神盾)";
                            break;
                        case "5":
                            $codeWord="快速交易";
                            break;
                        case "6":
                            $codeWord="小額交易";
                            break;
                    }
                    $("td", row).eq(9).text("").append($codeWord);

                    var $statusWord = "";
                    switch (data.fxtxstatus) {
                        case "3":
                            $statusWord = "已取消預約";
                            break;
                        case "0":
                            $statusWord = "預約成功";
                            break;
                    }
                    $("td", row).eq(10).text("").append($statusWord);

                },
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
            return false;
        }
    </script>
</body>

</html>