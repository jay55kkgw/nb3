<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>
        <div class="container" id="main-content">
            <h2>行動中台選單維護</h2>
        </div>
        <!-- body content start -->
        <div class="content row">
            <main class="col-12">
                <section id="main-content" class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-inline">
                                <input type="text" id="search" style="max-width:70%" class="form-control" />&nbsp;<input type="button" value="搜尋" class="btn btn-default" onclick="searchNodes();" />
                            </div>
                            <ul id="myTree" class="ztree"></ul>
                        </div>
                        <div class="col-md-9">
                            <div id="divBtnArea" class="row">
                                <div class="col-md-12">
                                    <c:if test="${allowEdit}">
                                        <input id="btnAddLink" type="button" value="新增子選單" class="btn btn-default" onclick="onAdd();" />&nbsp;
                                        <input id="btnLinkSort" type="button" value="子選單排序" class="btn btn-default" onclick="onSort();" />&nbsp;
                                        <input id="btnMoveLink" type="button" value="移動子選單" class="btn btn-default" onclick="onMove();" />&nbsp;
                                    </c:if>
                                </div>
                            </div>
                            <div class="row">
                                <br />
                            </div>
                            <div id="divEditArea" class="row">
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.all.js"></script>
        <script src="<%= request.getContextPath() %>/script/ztree/js/jquery.ztree.exhide.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            var setting = {
                check: {
                    enable: false
                },
                view: {
                    fontCss: getFontCss
                },
                data: {
                    simpleData: {
                        enable: true
                    }
                },
                callback: {
                    onClick: function (event, treeId, treeNode, clickFlag) {
                        cNode = treeNode;
                        onNodeClick();
                    }
                }
            };

            $(document).ready(function () {
                $("#divBtnArea").hide();
                $("#divEditArea").hide();
                initTree();
            });
            
            var zNodes = ${menus} ;
            var searchResultNodes = [];             // 記錄之前已找到的節點
            var zTree;
            var cNode;                              // 目前點到的節點
    
            function initTree() {
                $.fn.zTree.init($("#myTree"), setting, zNodes);
                zTree = $.fn.zTree.getZTreeObj("myTree");
            }

            // 搜尋選單
            function searchNodes() {
                resetZTree();
                var name = $("#search").val();
                if ( name == "" ) {
                    alert("請輸入選單名稱");
                    return;
                }
                searchResultNodes = zTree.getNodesByParamFuzzy("name", name);
                updateZTree(true);
            }

            // 搜尋選單
            function searchNodesForCaller(name) {
                resetZTree();
                $("#search").val(name);
                searchResultNodes = zTree.getNodesByParamFuzzy("name", name);
                updateZTree(true);
            }

            // 更新已搜尋到的選單顏色, 並展開已搜尋到的選單
            function updateZTree() {
                for( var i=0; i<searchResultNodes.length; i++) {
                    searchResultNodes[i].highlight = true;
                    zTree.updateNode(searchResultNodes[i]);
                    expendNode(searchResultNodes[i], true);
                }
            }

            // 展開節點遞迴函式, 由父節點先展開, 再展開子節點
            function expendNode(currNode) {
                if ( currNode.level > 0 )
                    expendNode(currNode.getParentNode());

                zTree.expandNode(currNode, true, false, false);
            }

            // 選單變顏色
            function getFontCss(treeId, treeNode) {
                return (!!treeNode.highlight) ? {color:"#A60000", "font-weight":"bold"} : {color:"#333", "font-weight":"normal"};
            }

            // 重新初始化 ZTree, 只顯示第一層及清除 MARK 顏色
            function resetZTree() {
                // 重新展開第一層選單
                var level1Nodes = zTree.getNodes()[0];
                zTree.expandAll(false);
                for ( var i=0; i<level1Nodes.length; i++ ) {
                    zTree.expandNode(level1Nodes[i], true, false, false);
                }
                // 把原本 MARK 顏色的選單清除
                for( var i=0; i<searchResultNodes.length; i++) {
                    searchResultNodes[i].highlight = false;
                    zTree.updateNode(searchResultNodes[i]);
                }
            }

            function onNodeClick() {
                if (cNode.id === "Menu") {
                    // root menu
                    $("#btnAddLink").prop("disabled", false);
                    $("#btnLinkSort").prop("disabled", false);
                    $("#btnMoveLink").hide();
                    if ( eval("${showAdd}") ) {
                        $("#divBtnArea").show();
                    }
                    $("#divEditArea").hide();
                    return;
                }
                $("#btnLinkSort").show();
                $("#btnMoveLink").show();
                lockScreen();
                $.ajax({
                    url: "<%= request.getContextPath() %>" + "/B115/Edit/"+cNode.id,
                    type: "POST",
                    data: { },
                    cache: false,
                    async: false,
                    success: function (data) {
                        unlockScreen();
                        $("#btnAddLink").prop("disabled", false);
                        $("#btnLinkSort").prop("disabled", false);
                        $("#btnMoveLink").prop("disabled", false);
                        if ( eval("${showAdd}") ) {
                            $("#divBtnArea").show();
                        }
                        $("#divEditArea").show();
                        $("#divEditArea").html(data);
                        window.scrollTo(0, 0);
                    },
                    error: function (err) {
                        unlockScreen();
                        alert(err.statusText);
                    }
                });
            }

            // 取得新增連結 Partial View
            function onAdd() {
                $.ajax({
                    url: "<%= request.getContextPath() %>" + "/B115/Create/"+cNode.id,
                    type: "POST",
                    data: { },
                    cache: false,
                    async: false,
                    success: function (data) {
                        $("#divBtnArea").show();
                        $("#divEditArea").show();
                        $("#divEditArea").html(data);
                    },
                    error: function (err) {
                        alert(err.statusText);
                    }
                });
            }

            // 取得排序連結 Partial View
            function onSort() {
                $.ajax({
                    url: "<%= request.getContextPath() %>/B115/Sort/"+cNode.id,
                    type: "POST",
                    data: { },
                    cache: false,
                    async: false,
                    success: function (data) {
                        $("#divBtnArea").show();
                        $("#divEditArea").show();
                        $("#divEditArea").html(data);
                    },
                    error: function (err) {
                        alert(err.statusText);
                    }
                });
            }

            // 取得移動連結 Partial View
            function onMove() {
                if (cNode.id === "Menu")
                    return;

                $.ajax({
                    url: "<%= request.getContextPath() %>/B115/Move/"+cNode.id,
                    type: "POST",
                    data: { },
                    cache: false,
                    async: false,
                    success: function (data) {
                        $("#divBtnArea").show();
                        $("#divEditArea").show();
                        $("#divEditArea").html(data);
                    },
                    error: function (err) {
                        alert(err.statusText);
                    }
                });
            }

            // 動態加一個節點到 zTree
            function createCallBack(id, name) {
                var zTree = $.fn.zTree.getZTreeObj("myTree");
                var newNode = { id: id, name: name };
                zTree.addNodes(cNode, newNode);
            }

            // 更新節點名稱
            function updateCallBack(name) {
                cNode.name = name;
                var zTree = $.fn.zTree.getZTreeObj("myTree")
                zTree.updateNode(cNode);
            }

            // 刪除一個節點
            function deleteCallBack() {
                var zTree = $.fn.zTree.getZTreeObj("myTree");
                zTree.removeNode(cNode);
            }

            // 移動一個節點    
            function moveCallBack(parentID) {
                var zTree = $.fn.zTree.getZTreeObj("myTree");
                var parentNode = zTree.getNodeByParam("id", parentID, null);
                zTree.moveNode(parentNode, cNode, "inner");
            }

            // 排序節點
            function sortCallBack() {
                var zTree = $.fn.zTree.getZTreeObj("myTree");
                zTree.removeChildNodes(cNode);
                $.ajax({
                    url: "<%= request.getContextPath() %>/B115/SubMenu/"+cNode.id,
                    type: "POST",
                    data: { },
                    cache: false,
                    async: false,
                    success: function (data) {
                        var menus = JSON.parse(data);
                        for (var i = 0; i < menus.length; i++) {
                            var menuData = { id: menus[i].ADOPID, name: menus[i].ADOPNAME };
                            var newNode = zTree.addNodes(cNode, menuData);
                            rebuildSubNodes(newNode[0]);
                        }
                    },
                    error: function (err) {
                        alert(err.statusText);
                    }
                });
            }

            function rebuildSubNodes(thisNode) {
                $.ajax({
                    url: "<%= request.getContextPath() %>/B115/SubMenu/"+thisNode.id,
                    type: "POST",
                    data: { },
                    cache: false,
                    async: false,
                    success: function (data) {
                        var menus = JSON.parse(data);
                        for (var i = 0; i < menus.length; i++) {
                            var menuData = { id: menus[i].ADOPID, name: menus[i].ADOPNAME };
                            var newNode = zTree.addNodes(thisNode, menuData);
                            rebuildSubNodes(newNode[0]);
                        }
                    },
                    error: function (err) {
                        alert(err.statusText);
                    }
                });
            }

            function exitCallBack() {
                $("#divBtnArea").hide();
                $("#divEditArea").hide();
            }
        </script>
    </body>
</html>