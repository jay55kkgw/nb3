<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="col-md-12">
    <section id="createForm">
        <form action="/MB3SYSOP_save" method="post">
            <!-- 排序 -->
            <div class="form-group">
                <div class="col-md-12">
                    <table id="main" class="display" displayellspacing="0" style="width:100%">
                        <thead>
                            <tr>
                                <th>選單名稱</th>
                                <th>排序值</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${fn:length(menus) gt 0}">
                                <c:forEach var="i" begin="0" end="${fn:length(menus)-1}" step="1">
                                    <tr>
                                        <td style="vertical-align: middle;">${menus[i].ADOPNAME}</td>
                                        <td style="vertical-align: middle;">
                                            <input type="hidden" id="DPACCSETID" value="${menus[i].DPACCSETID}" />
                                            <input type="number" id="ADSEQ" value="${menus[i].ADSEQ}"
                                                class="form-control" maxlength=10 />
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <input id="exit" type="button" value="離開" class="btn btn-dark" onclick="onExit();" />&nbsp;
                    <c:if test="${fn:length(menus) gt 0}">
                        <input type="button" value="儲存" class="btn btn-info" onclick="onSave();" />
                    </c:if>
                </div>
            </div>
        </form>
    </section>
</div>
<script>
    $("#main").dataTable({
        "responsive": true,
        "serverSide": false,
        "orderMulti": false,
        "bFilter": false,
        "bSortable": true,
        "bPaginate": false,
        "pageLength": 50,
        "order": [[1, "asc"]],
        "language": {
            "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
        }
    });

    function onSave() {
        //判斷是否有空值
        var checkExit = false;
        $("input[id='ADSEQ']").each(function () {
            if ($(this).val() == "") {
                alert("請輸入排序值");
                $(this).focus();
                checkExit = true;
            }
        })
        if (checkExit == true) {
            return;
        }
        
        var DPACCSETIDList = "";
        var ADSEQList = "";
        var keys = $("input[id='DPACCSETID']");
        var vals = $("input[id='ADSEQ']");
        for (var i = 0; i < keys.length; i++) {
            if (DPACCSETIDList !== "")
                DPACCSETIDList += ",";

            if (ADSEQList !== "")
                ADSEQList += ",";

            DPACCSETIDList += keys[i].value;
            ADSEQList += vals[i].value;
        }
        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/B115/SortSave",
            type: "POST",
            data: {
                "DPACCSETID": DPACCSETIDList,
                "ADSEQ": ADSEQList
            },
            success: function (res) {
                unlockScreen();
                if (res.validated) {
                    // 所有可以輸入的項目均 Disable
                    $("#createForm input").prop("disabled", true);
                    $("#createForm select").prop("disabled", true);
                    $("#createForm #exit").prop("disabled", false);

                    alert("子功能排序成功");
                    sortCallBack();
                } else {
                    //Set error messages
                    $.each(res.errorMessages, function (key, value) {
                        if (key === "summary")
                            alert(value);
                        else
                            $('input[name=' + key + ']').after('<span class="error">' + value + '</span>');
                    });
                }
            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }

    function onExit() {
        exitCallBack();
    }
</script>