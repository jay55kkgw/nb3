<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-md-12">
    <div class="content">
        <form class="container" action="/MB3SYSOP_save" method="post">
            <div class="form-group row">
                <label class="col-md-3 control-label text-right">選單代碼：</label>
                <div class="col-md-9">
                    <input id="ADOPID" name="ADOPID" type="text" class="form-control" maxlength="60"
                        value="${menu.ADOPID}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="ADOPNAME">選單名稱（繁）：</label>
                <div class="col-md-9">
                    <input id="ADOPNAME" name="ADOPNAME" type="text" class="form-control" maxlength="50"
                        value="${menu.ADOPNAME}" />
                </div>
            </div>
<!-- 
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="ADOPCHSNAME">選單名稱（簡）：</label>
                <div class="col-md-9">
                    <input id="ADOPCHSNAME" name="ADOPCHSNAME" type="text" class="form-control" maxlength="50"
                        value="${menu.ADOPCHSNAME}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="ADOPENGNAME">選單名稱（英）：</label>
                <div class="col-md-9">
                    <input id="ADOPENGNAME" name="ADOPENGNAME" type="text" class="form-control" maxlength="200"
                        value="${menu.ADOPENGNAME}" />
                </div>
            </div>
 -->
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="ADOPMEMO">備註：</label>
                <div class="col-md-9">
                    <input id="ADOPMEMO" name="ADOPMEMO" type="text" class="form-control" maxlength="85"
                        value="${menu.ADOPMEMO}" />
                </div>
            </div>
            <!-- 
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="URL">URL：</label>
                <div class="col-md-9">
                    <input id="URL" name="URL" type="text" class="form-control" maxlength="500" value="${menu.URL}" />
                </div>
            </div>
             -->
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="ADGPPARENT">分類：</label>
                <div class="col-md-9">
                    <div class="form-inline">
                        <select id="ADGPPARENT" name="ADGPPARENT" class="form-control" onchange="onGroupChange(this);">
                            <option value="">--不分類--</option>
                            <c:forEach items="${groups}" var="group">
                                <option value="${group.ADOPGROUP}">${group.ADOPGROUPNAME}</option>
                            </c:forEach>
                        </select>&nbsp;
                        <select id="ADOPGROUP" name="ADOPGROUP" class="form-control">
                            <option value="">--不分類--</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="offset-3 col-md-9">
                    <label>
                        <c:set var="checked" value="${menu.ADOPALIVE.equals('1') ? 'checked': ''}" />
                        <input id="ADOPALIVE" name="ADOPALIVE" type="checkbox" value="${menu.ADOPALIVE}"
                            ${checked} />&nbsp;顯示本連結
                    </label>
                    <!-- 
                    <label>
                        <c:set var="checked" value="${menu.ISANONYMOUS.equals('1') ? 'checked': ''}" />
                        <input id="ISANONYMOUS" name="ISANONYMOUS" type="checkbox" value="${menu.ISANONYMOUS}"
                            ${checked} />&nbsp;匿名存取
                    </label>
                    <label>
                        <c:set var="checked" value="${menu.ISPOPUP.equals('1') ? 'checked': ''}" />
                        <input id="ISPOPUP" name="ISPOPUP" type="checkbox" value="${menu.ISPOPUP}"
                            ${checked} />&nbsp;在新視窗開啟
                    </label>
                     -->
                    <!-- 
                    <label>
                        <c:set var="checked" value="${menu.ISBLOCKAML.equals('1') ? 'checked': ''}" />
                        <input id="ISBLOCKAML" name="ISBLOCKAML" type="checkbox" value="${menu.ISBLOCKAML}"
                            ${checked} />&nbsp;是否拒絕洗錢黑名單使用者使用本交易
                    </label>
                     -->
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-3 col-md-9">
                    <label>
                        <input id="ALL" name="ALL" type="checkbox" />&nbsp;ALL（全部使用者）
                    </label>
                    <label>
                        <input id="ATMT" name="ATMT" type="checkbox" />&nbsp;ATMT（金融卡申請網銀使用者-線上交易功能）
                    </label>
                    <label>
                        <input id="ATM" name="ATM" type="checkbox" />&nbsp;ATM（金融卡申請網銀使用者）
                    </label>
                    <label>
                       <input id="CRD" name="CRD" type="checkbox" />&nbsp;CRD（信用卡申請網銀使用者）
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-3 col-md-9">
                    <input id="exit" type="button" value="離開" class="btn btn-dark" onclick="onExit();" />&nbsp;
                    <input type="button" value="儲存" class="btn btn-info" onclick="onSave();" />
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    onInit();

    function onInit() {
        var auths = [];
        if ( "${menu.ADOPAUTHTYPE}".trim() !== "" ) {
            auths = "${menu.ADOPAUTHTYPE}".split(",");
            for ( var i=0; i<auths.length; i++ ) {
                if ( auths[i] === "ALL" ) {
                    $("#ALL").prop("checked", true);
                } else if ( auths[i] === "ATMT" ) {
                    $("#ATMT").prop("checked", true);
                } else if ( auths[i] === "ATM" ) {
                    $("#ATM").prop("checked", true);
                } else if ( auths[i] === "CRD" ) {
                    $("#CRD").prop("checked", true);
                }
            }
        }
    }

    function checkEmpty() {
        var msg = "";
        if ( $("#ADOPID").val()=="" ) {
            msg += "選單代碼不可為空值\r\n";
        }
        if ( $("#ADOPNAME").val()=="" ) {
            msg += "選單名稱繁中不可為空值.\r\n";
        }
//        if ( $("#ADOPCHSNAME").val()=="" ) {
//            msg += "選單名稱簡中不可為空值.\r\n";
//        }
//        if ( $("#ADOPENGNAME").val()=="" ) {
//            msg += "選單名稱英文不可為空值.\r\n";
//        }
//        if ( $("#URL").val()=="" ) {
//            msg += "URL不可為空值.\r\n";
//        }

        return msg;
    }

    function onGroupChange(obj) {
        var val = $(obj).val();

        $("#ADOPGROUP option").remove();
        $("#ADOPGROUP").append($("<option>--不分類--</option>").attr("value", ""));

        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/B115/SubGroup/" + val,
            type: "POST",
            data: {},
            cache: false,
            async: true,
            success: function (data) {
                unlockScreen();
                if (data) {
                    var menus = JSON.parse(data);
                    for (var i = 0; i < menus.length; i++) {
                        $("#ADOPGROUP").append($("<option></option>").attr("value", menus[i].ADOPGROUP).text(menus[i].ADOPGROUPNAME));
                    }
                } else {
                    alert(data);
                }
            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }

    function onSave() {
        $("span.error").remove();
        var menuId = $("#ADOPID").val();
        var menuName = $("#ADOPNAME").val();
        var auths = [];
        if ( $("#ALL").prop("checked") ) {
            auths.push("ALL");
        }
        if ( $("#ATMT").prop("checked") ) {
            auths.push("ATMT");
        }
        if ( $("#ATM").prop("checked") ) {
            auths.push("ATM");
        }
        if ( $("#CRD").prop("checked") ) {
            auths.push("CRD");
        }
        var msg = checkEmpty();
        if ( msg != "" ) {
            alert(msg);
            return;
        }
        lockScreen();
        $.ajax({
            url: "<%= request.getContextPath() %>/B115/CreateSave",
            type: "POST",
            data: {
                "ADOPID": $("#ADOPID").val(),
                "ADOPNAME": $("#ADOPNAME").val(),
                "ADOPENGNAME": "",
                "ADOPCHSNAME": "",
                "ADOPMEMO": $("#ADOPMEMO").val(),
                "ADOPALIVE": $("#ADOPALIVE").prop('checked') ? "1" : "0",
                "ADOPGROUPID": "${menu.ADOPGROUPID}",
                "ISANONYMOUS": "0",
                "URL": "",
                "ISPOPUP": "0",
                "ADGPPARENT": $("#ADGPPARENT").val(),
                "ADOPGROUP": $("#ADOPGROUP").val(),
                "ADOPAUTHTYPE": auths.join() ,
                "ISBLOCKAML": "0"
            },
            success: function (res) {
                unlockScreen();
                if (res.validated) {
                    alert("選單[" + menuName + "]新增成功");
                    createCallBack(menuId, menuName);
                } else {
                    //Set error messages
                    $.each(res.errorMessages, function (key, value) {
                        if (key === "summary")
                            alert(value);
                        else
                            $('input[name=' + key + ']').after('<span class="error">' + value + '</span>');
                    });
                }
            },
            error: function (err) {
                unlockScreen();
                alert(err.statusText);
            }
        });
    }

    function onExit() {
        exitCallBack();
    }
</script>