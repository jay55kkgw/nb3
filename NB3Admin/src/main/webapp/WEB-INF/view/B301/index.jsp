<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>${__MENU_NAME__}</h2>
            <form class="container main-content-block p-4" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >查詢期間（起～迄）：</label>
                    <div class="col-9">
                        <div class="form-inline">
                            <input type="text" autocomplete="off" id="startTime" name="startTime" class="form-control" style = "width:160px" maxlength="10" />～
                            <input type="text" autocomplete="off" id="endTime" name="endTime" class="form-control" style = "width:160px" maxlength="10" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >使用者：</label>
                    <div class="col-9" class="form-control">
                        <div class="radio">
                            <div class="form-inline">
                                <label><input type="radio" name="optradio" id="optradio" checked value="userid">請輸入使用者代號：</label>
                                <input type="text" name="loginUserId" id="loginUserId" class="form-control" maxlength="10" />
                            </div>
                        </div>
                        <br/>
                        <div class="radio">
                            <div class="form-inline">
                                <label><input type="radio" name="optradio" id="optradio"  value="" >查詢所有使用者</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >管理功能項目：</label>
                    <div class="col-3">
                        <select id="adopid" class="form-control">
                            <option value="">全部</option>
                            <c:forEach var="menu" items="${menus}">
                                <option value="${menu.ADOPID}">${menu.ADOPID}-${menu.ADOPNAME}</option>
                            </c:forEach>  
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th>序號</th>
                    <th>使用者代號</th>
                    <th>使用者IP</th>
                    <th>操作時間</th>
                    <th>操作項目名稱</th>
                    <th>操作功能</th>
                    <th>變更前內容</th>
                    <th>變更後內容</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!-- body content end -->

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">提交資料</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="model-body" class="modal-body">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                $('#startTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false
                });
                $('#endTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i',   // 顯示時分
                    scrollMonth: false
                });
                $("#main").hide();
            }); //ready

            function onQuery() {
                var ds = new Date($("#startTime").val());
                var de = new Date($("#endTime").val());

                if ( (ds instanceof Date && isNaN(ds)) || (de instanceof Date && isNaN(de)) ) {
                    alert("請輸入查詢期間（起～迄）");
                    return;
                }

                var loginUserId = "";
                if ( $("input:radio[name=optradio]:checked").val()!="" ) {
                    loginUserId = $("#loginUserId").val();
                    if ( loginUserId === "" ) {
                        alert("請輸入使用者代號");
                        return;
                    }
                }

                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "order": [[0, "asc"]],
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B301/IndexQuery",
                        "data": {
                            "startTime": $("#startTime").val(),
                            "endTime": $("#endTime").val(),
                            "loginUserId": loginUserId,
                            "adOpId": $("#adopid").val()
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [ 
                        { "data": "adtxno" },
                        { "data": "aduserid" },
                        { "data": "aduserip" },
                        { "data": "adtxdate" },
                        { "data": "adopid" },
                        { "data": "adopitem" },
                        { "data": "adtxno" },
                        { "data": "adtxno" }
                    ],
                    "createdRow": function (row, data, index) {
                        $("td", row).eq(0).text(index+1);

                        if ( data.adopitem == "1" ) {
                            $("td", row).eq(5).text("新增");
                        } else if ( data.adopitem == "2" ) {
                            $("td", row).eq(5).text("修改");
                        } else if ( data.adopitem == "3" ) {
                            $("td", row).eq(5).text("刪除");
                        } else if ( data.adopitem == "4" ) {
                            $("td", row).eq(5).text("查詢");
                        } else if ( data.adopitem == "5" ) {
                        	$("td", row).eq(5).text("送審");
                        } else if ( data.adopitem == "6" ) {
                        	$("td", row).eq(5).text("覆核");
                        } else if ( data.adopitem == "7" ) {
                        	$("td", row).eq(5).text("退回");
                        } else if ( data.adopitem == "8" ) {
                        	$("td", row).eq(5).text("設定");
                        } else {
                            $("td", row).eq(5).text("未知["+data.adopitem+"]");
                        }
                        var $adbchcon = data.adbchcon == "{}" ? $("<a>資料無異動<a/>") : $("<a href='#'>詳情<a/>");
                        $adbchcon.attr("onclick", "showJsonData('"+data.adtxno+"');");
                        $("td", row).eq(6).text("").append($adbchcon);

                        var $adachcon = data.adachcon == "{}" ? $("<a>資料無異動<a/>") : $("<a href='#'>詳情<a/>");
                        $adachcon.attr("onclick", "showJsonData('"+data.adtxno+"');");
                        $("td", row).eq(7).text("").append($adachcon);
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }

            //開啟處理狀態 Dialog, Controller 會回傳 PartialView
            function showJsonData(id) {
                var url = "<%= request.getContextPath() %>/B301/JsonData/" + id;
                lockScreen();
                $.post(url, { }, function (data, status) {
                    unlockScreen();
                    if (status == "success") {
                        //alert(data);
                        var htmlContent = data.replace(/\r\n/g, "");
                        $("#model-body").html(htmlContent);
                        $('#exampleModal').modal({
                            
                        });
                    }
                });
            }
        </script>
    </body>
</html>