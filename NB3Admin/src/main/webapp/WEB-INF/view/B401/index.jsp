<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
        
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>系統異常事件查詢</h2>
            <form class="container main-content-block p-4" method="post">
                <div class="form-group row">
                    <label class="col-3 control-label text-right" >查詢期間（起～迄）：</label>
                    <div class="col-9">
                        <div class="form-inline">
                            <input type="text" id="startTime" autocomplete="off" name="startTime" class="form-control" style = "width:230px" maxlength="10" />～
                            <input type="text" id="endTime" autocomplete="off" name="endTime" class="form-control" style = "width:230px" maxlength="10" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-3 col-9">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnOK" type="button" value="查詢" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                    </div>
                </div>
            </form>
        </div>
        <table id="main" class="display p-4 transparent" cellspacing="0" style="width:100%">
            <thead>
                <tr>
                    <th>序號</th>
                    <th>類別</th>
                    <th>機器</th>
                    <th>訊息</th>
                    <th>來源</th>
                    <th>詳情</th>
                    <th>日期</th>
                    <th>時間</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!-- body content end -->
        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                $('#startTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i:00',   // 顯示時分
                    scrollMonth: false
                });
                $('#endTime').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: true,
                    format: 'Y/m/d H:i:00',   // 顯示時分
                    scrollMonth: false
                });
                $('#startTime').val("${StartTime}");
                $('#endTime').val("${EndTime}");
                $("#main").hide();
            }); //ready

            function onQuery() {
                if ( $('#startTime').val() == "" || $('#endTime').val() == "" ) {
                    alert("請輸入查詢期間（起～迄）");
                    return;
                }
                $("#main").dataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "orderMulti": false,
                    "bFilter": false,
                    "bSortable": true,
                    "bDestroy": true,
                    "pageLength": 10,
                    "order": [[0, "asc"]],
                    "ajax": {
                        "type": "POST",
                        "url": "<%= request.getContextPath() %>/B401/IndexQuery",
                        "data": {
                            "startTime": $("#startTime").val(),
                            "endTime": $("#endTime").val()
                        },
                        "error": function (msg) {
                            alert(msg);
                        }
                    },
                    "sServerMethod": "post",
                    "dom": "<'clear'>frtip",   //無工具列
                    "columns": [ 
                        { "data": "id" },
                        { "data": "loggername" },
                        { "data": "loggerhost" },
                        { "data": "message" },
                        { "data": "source" },
                        { "data": "throwby" },
                        { "data": "logdate" },
                        { "data": "logtime" }
                    ],
                    "createdRow": function (row, data, index) {
                        
                    },
                    "language": {
                        "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                    }
                });
                $("#main").show();
                return false;
            }
        </script>
    </body>
</html>