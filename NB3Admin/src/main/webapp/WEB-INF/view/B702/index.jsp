<% response.setHeader("X-Frame-Options", "DENY"); %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>
        <title>台企銀 後台首頁</title>
        <meta charset="UTF-8">
      
        <!-- include 後台管理 css -->
        <jsp:include page="../include/admheadercs.jsp"></jsp:include>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/ztree/css/zTreeStyle/zTreeStyle.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
        
    </head>
    <body>
        <!-- include master page menu -->
        <jsp:include page="../include/admmenu.jsp"></jsp:include>

        <!-- body content start -->
        <div class="container" id="main-content">
            <h2>各類交易數統計表</h2>
            <form id="mForm" name="mForm"  class="container main-content-block p-4" action="<%= request.getContextPath() %>/B702/Query" method="post">
                <div class="form-group row">
                    <label class="col-2 control-label text-right" >查詢年月：</label>
                    <div class="col-10">
                        <div class="form-inline">
                            西元：
                            <select class="form-control" id="YYYY" name="YYYY">
                                <option value="">--請選擇--</option>
                                <c:forEach items="${years}" var="year">
                                    <option value="${year}">${year}</option>
                                </c:forEach>
                            </select>&nbsp;年&nbsp;
                            <select class="form-control" id="MM" name="MM">
                                <option value="">--請選擇--</option>
                                <c:forEach items="${months}" var="month">
                                    <option value="${month}">${month}</option>
                                </c:forEach>
                            </select>&nbsp;月
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 control-label text-right" class="form-control">交易來源：</label>
                    <div class="col-2">
                        <select id="loginType" name="loginType" class="form-control">
                            <option value="">全部</option>
                            <option value="NB">網銀</option>
                            <option value="MB">行動</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-2 col-10">
                        <input type="button" value="重新輸入" class="btn btn-default" aria-label="Left Align" onclick="location.reload();" />&nbsp;
                        <input id="btnOK" type="button" value="確定" class="btn btn-info" aria-label="Left Align" onclick="onQuery()" />&nbsp;
                        <input id="btnexpCSV" type="button" value="匯出CSV" class="btn btn-secondary" aria-label="Left Align" onclick="onExportCSV();" />&nbsp;
                        <input id="btnPrint" type="button" value="列印" class="btn btn-secondary" aria-label="Left Align" onclick="friendlyPrint('printQ,print');" />
                    </div>
                </div>
            </form>
        </div>
        <!-- report -->
        <div id="printQ">
            <div class="form-group row">
                <label class="col-12 control-label text-center"><h4>各類交易數統計表</h4></label>                
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >查詢時間：</label>
                <div class="col-10">
                    <div class="radio">
                        <div class="form-inline">
                            <jsp:useBean id="now" class="java.util.Date" />
                            <fmt:formatDate var="nowDateTime" value="${now}" pattern="yyyy/MM/dd HH:mm:ss" />
                            ${nowDateTime}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 control-label text-right" >查詢年月：</label>
                <div class="col-10">
                    <div class="form-inline">
                        ${YYYY}/${MM}                        
                    </div>
                </div>
            </div>
        </div>
        <div id="print">
            <table id="main" width="100%" class="display p-4 transparent" cellspacing="0" border='1'>
                <thead>
                    <tr>
                    	<th></th>
                        <th></th>
                        <th>類別</th>
                        <th></th>  
                        <th style="text-align: right">自行</th>
                        <th style="text-align: right">跨行</th>
                        <th style="text-align: right">合計</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${Data}" var="item">
                        <c:choose>
                            <c:when test="${item.seqNo != 5}">
                                <!--^, 類別, 筆數, 自行, ^, 合計-->
                                <!-- 以下為查詢類, 所以不分自跨行, 一律算在自行 -->
                                <tr style="text-align: right">
                                	<td><span style=" visibility: hidden">${item.seqNo}</span></td>
                                    <td></td>
                                    <td>${item.adopGroupName}</td>                                            
                                    <td>筆數</td>
                                    <td>${item.selfCount+item.crossCount}</td>
                                    <td>0</td>
                                    <td>${item.adCount}</td>
                                </tr>
                            </c:when>
                            <c:when test="${item.seqNo == 5 && item.adUserType == 1}">
                                <!-- 交易類，企業戶，筆數及金額 -->
                                <tr style="text-align: right">
                                	<td><span style=" visibility: hidden">${item.seqNo}</span></td>
                                    <td>${item.adopGroupName}</td>
                                    <td>企業戶</td>                                            
                                    <td>筆數</td>
                                    <td>${item.selfCount}</td>
                                    <td>${item.crossCount}</td>
                                    <td>${item.adCount}</td>
                                </tr>
                                <tr style="text-align: right">
                                	<td><span style=" visibility: hidden">${item.seqNo}</span></td>
                                    <td></td>
                                    <td></td>                                            
                                    <td>金額(仟)</td>
                                    <td>${item.selfAmt}</td>
                                    <td>${item.crossAmt}</td>
                                    <td>${item.adAmount}</td>
                                </tr>
                            </c:when>
                            <c:when test="${item.seqNo == 5 && item.adUserType == 0}">
                                <!-- 交易類，個人戶，筆數及金額 -->
                                <tr style="text-align: right">
                                	<td><span style=" visibility: hidden">${item.seqNo}</span></td>
                                    <td></td>
                                    <td>個人戶</td>                                            
                                    <td>筆數</td>
                                    <td>${item.selfCount}</td>
                                    <td>${item.crossCount}</td>
                                    <td>${item.adCount}</td>
                                </tr>
                                <tr style="text-align: right">
                                	<td><span style=" visibility: hidden">${item.seqNo}</span></td>
                                    <td></td>
                                    <td></td>                                            
                                    <td>金額(仟)</td>
                                    <td>${item.selfAmt}</td>
                                    <td>${item.crossAmt}</td>
                                    <td>${item.adAmount}</td>
                                </tr>
                            </c:when>
                            <c:when test="${item.seqNo == 5 && item.adUserType == -1}">
                                <!-- 交易筆數合計及金額合計 -->
                                <tr style="text-align: right">
                                	<td><span style=" visibility: hidden">${item.seqNo}</span></td>
                                    <td></td>
                                    <td></td>                                            
                                    <td>筆數合計</td>
                                    <td>${item.selfCount}</td>
                                    <td>${item.crossCount}</td>
                                    <td>${item.adCount}</td>
                                </tr>
                                <tr style="text-align: right">
                                	<td><span style=" visibility: hidden">${item.seqNo}</span></td>
                                    <td></td>
                                    <td></td>                                            
                                    <td>金額合計(仟)</td>
                                    <td>${item.selfAmt}</td>
                                    <td>${item.crossAmt}</td>
                                    <td>${item.adAmount}</td>
                                </tr>
                            </c:when>
                        </c:choose>
                    </c:forEach>
                </tbody>
            </table>    
            <c:if test="${error != null}">
                <br><span style="color: red;">${error.message}</span>
                <br>${error}
            </c:if>                       
        </div>
        <!-- body content end -->

        <!-- include master page footer -->
        <jsp:include page="../include/admfooter.jsp"></jsp:include>

        <!-- include 後台管理 js -->
        <jsp:include page="../include/admfooterjs.jsp"></jsp:include>

        <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
        <script src="<%= request.getContextPath() %>/script/respond.js"></script>
        <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function () {
                $('#DateFrom').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: false,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });
                $('#DateTo').datetimepicker({
                    lang: 'zh-TW',
                    timepicker: false,
                    format: 'Y/m/d',   // 顯示時分
                    scrollMonth: false
                });

                $("#YYYY").val("${B702Model.YYYY}");
                $("#MM").val("${B702Model.MM}");
                $("#loginType").val("${B702Model.loginType}");

                if ( "${IsPostBack}" == "true" ) {
                    $("#printQ").show();
                    $("#print").show();

                    $("#main").dataTable({
                        "responsive": true,
                        "processing": true,
                        "serverSide": false,
                        "orderMulti": false,
                        "bFilter": false,
                        "bSortable": false,
                        "ordering": false,
                        "bDestroy": true,
                        "info": false,
                        "pageLength": 100,
                        "paging": false,
                        "dom": "<'clear'>frtip",   //無工具列
                        "language": {
                            "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                        }
                    });
                } else {
                    $("#printQ").hide();
                    $("#print").hide();
                }
            }); //ready
        
            function onQuery() {
                $("#mForm").submit();             
            }

            function onExportCSV() {
                $.ajax({
                        url:"<%= request.getContextPath() %>/B702/DownloadtoCSV",
                        type:"POST",
                        data:{
                            YYYY: $("#YYYY").val(),
                            MM: $("#MM").val(),
                            loginType: $("#loginType").val(),
                        },
                        success: function(response, status, xhr) {
                            // check for a filename
                            var filename = "";
                            var disposition = xhr.getResponseHeader('Content-Disposition');
                            if (disposition && disposition.indexOf('attachment') !== -1) {
                                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                                var matches = filenameRegex.exec(disposition);
                                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                            }

                            var type = xhr.getResponseHeader('Content-Type');
                            var blob = new Blob(["\ufeff" + response], { type: type });

                            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                                // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                                window.navigator.msSaveBlob(blob, filename);
                            } else {
                                var URL = window.URL || window.webkitURL;
                                var downloadUrl = URL.createObjectURL(blob);

                                if (filename) {
                                    // use HTML5 a[download] attribute to specify filename
                                    var a = document.createElement("a");
                                    // safari doesn't support this yet
                                    if (typeof a.download === 'undefined') {
                                        window.location = downloadUrl;
                                    } else {
                                        a.href = downloadUrl;
                                        a.download = filename;
                                        document.body.appendChild(a);
                                        a.click();
                                    }
                                } else {
                                    window.location = downloadUrl;
                                }

                                setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                            }
                        },
                        error:function(){
                            alert("error");
                        }
                });                
            }

        </script>
    </body>
</html>