<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>

<head>
    <title>台企銀 後台首頁</title>
    <meta charset="UTF-8">

    <!-- include 後台管理 css -->
    <jsp:include page="../include/admheadercs.jsp"></jsp:include>

    <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap-dialog.css"> -->
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.css">
</head>

<body>
    <!-- include master page menu -->
    <jsp:include page="../include/admmenu.jsp"></jsp:include>

    <!-- body content start -->
    <div class="container" id="main-content">
        <h2>待辦事項</h2>
    </div>
    <table id="tblTodo" class="display p-4 transparent" cellspacing="0">
        <thead>
            <tr>
                <th>案件編號</th>
                <th>案件名稱</th>
                <th>送件單位</th>
                <th>送件者</th>
                <th>案件狀態</th>
                <th>案件時間</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="todo" items="${todos}" varStatus="loop">
                <fmt:parseDate var="CreateDateTime" value="${todo.CREATEDATE}${todo.CREATETIME}" type="BOTH"
                    pattern="yyyyMMddHHmmss" />
                <tr>
                    <td>
                        <a href="<%= request.getContextPath() %>/${todo.TODOURL}">${todo.CURRENTIDENTITY.CASESN}</a>
                    </td>
                    <td>${todo.FLOWMEMO}</td>
                    <td>${todo.FROMOID}</td>
                    <td>${todo.FROMUNAME}(${todo.FROMUID})</td>
                    <td>
                        <a href='#' onclick="showStatus('${todo.CURRENTIDENTITY.CASESN}'); ">${todo.STEPNAME}</a>
                    </td>
                    <td>
                        <fmt:formatDate value="${CreateDateTime}" pattern="yyyy/MM/dd HH:mm:ss" />
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

    <jsp:include page="../include/caseStatus.jsp"></jsp:include>
    <!-- body content end -->

    <!-- include master page footer -->
    <jsp:include page="../include/admfooter.jsp"></jsp:include>

    <!-- include 後台管理 js -->
    <jsp:include page="../include/admfooterjs.jsp"></jsp:include>
    <script src="<%= request.getContextPath() %>/script/DateTimePicker/jquery.datetimepicker.js"></script>
    <script src="<%= request.getContextPath() %>/script/respond.js"></script>
    <script src="<%= request.getContextPath() %>/script/DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <!-- <script src="<%= request.getContextPath() %>/script/bootstrap-dialog.js"></script> -->

    <script>
        $(document).ready(function () {
            $("#tblTodo").DataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                "searching": false,
                "language": {
                    "url": "<%= request.getContextPath() %>/script/DataTables/chinese.json"
                }
            });
        });
    </script>
</body>

</html>