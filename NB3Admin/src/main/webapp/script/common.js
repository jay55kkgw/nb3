/* lockScreen & unlockScreen */
function lockScreen(message) {
    //鎖定畫面，禁止使用者操作。
    $.blockUI({
        message: function () { if (message) return message; else return "交易進行中...請稍候" },
        css: {
            border: 'none',
            padding: '10px',
            width: '30%',
            top: '45%',
            left: '35%',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            'border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
}//lockScreen

function unlockScreen() {
    $.unblockUI();
}//unlockScreen

function formatMoney(money) {
	if (money && money !== null) {
		return Number(money).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
	}
	else {
		return "";
	}
};

function validateNumeric(str) {
    var number = /[0-9]/;
    var valid = number.test(str);
    return valid;
}

function validateAlphaUC(str) {
	var letter = /[A-Z]/; 
    var valid = letter.test(str);
    return valid;
}

function validateAlpha(str) {
	var letter = /[a-zA-Z]/; 
    var valid = letter.test(str);
    return valid;
}

function validateAlphaSpace(str) {
	var letter = /^[a-zA-Z .]*$/; 
    var valid = letter.test(str);
    return valid;
}

function validateAlphaNumericEng(str) {
	var letter = /^[A-Za-z][A-Za-z0-9]*$/; 
    var valid = letter.test(str);
    return valid;
}

function validateAplhaNumeric(str) {
	return validateNumeric(str) && validateAlpha(str);
}

function validateChinese(str) {
	for(var i = 0; i < str.length; i++) {
		if(str.charCodeAt(i) < 0x4E00 || str.charCodeAt(i) > 0x9FA5) {
			return false;
		}
	}
	return true;
}