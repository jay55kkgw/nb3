/*
 * 檢核指定名稱的Radio物件是否被點選
 * PARAM sFieldName:欄位中文描述
 * PARAM sRadioName:Radio物件名稱
 * return 
 * validate[funcCall[validate_Radio['sFieldName',sRadioName]]]
 */
function validate_Radio(field, rules, i, options) {
	
	console.log("_one_checkRadio");
	
	var sFieldName = rules[i + 2];
	var sRadioName = rules[i + 3];

	if (typeof (sFieldName) != "string") {
		return "* CheckRadio含有無效的參數sFieldName";
	}

	if (typeof (sRadioName) != "string") {
		return "* CheckRadio含有無效的參數sRadioName";
	}

	try {
		var aRadios = document.getElementsByName(sRadioName);

		if (aRadios.length == 0) {
			return "沒有可點選的" + sFieldName;
		}

		var bHasChecked = false;
		for (var i = 0; i < aRadios.length; i++) {
			if (aRadios[i].checked) {
				bHasChecked = true;
				break;
			}
		}

		if (!bHasChecked) {
			return "* 請點選" + sFieldName;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
	}
}

/*
 * 檢查數字輸入框內容(不含加號、減號、小數點)
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:數字輸入框
 * return 
 * validate[required,funcCall[validate_CheckNumber['sFieldName',oField]]]
 */
function validate_CheckNumber(field, rules, i, options) {
	
	console.log("_CheckNumber");
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	
	if (typeof (sFieldName) != "string") {
		return "* CheckNumber含有無效的參數sFieldName";
	}
    /*if (window.console) {
		console.log("_CheckNumber");
	}*/
	try {
		var Numbervalue = $("#" + oField).val();
		var regex = /[^0-9]/;
		var valid = regex.test(Numbervalue);
		if (valid) {
			return options.allrules.onlyNumberSp.alertText;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
	}
}

/*
 * 檢查金額輸入框內容
 * PARAM sFieldName:欄位中文描述
 * PARAM bIsInteger:是否整數
 * PARAM fMin:最小值，不限制時給null
 * PARAM fMin:最大值，不限制時給null
 * return 
 * 檢查金額輸入框內容 PARAM ID:金額輸入框 PARAM Min:最小值 PARAM Max:最大值 return
 * validate[required,min[fMin],max[fMin],funcCall[validate_CheckAmount[ 'sFieldName' , oField]]]
 */
function boolIsInteger(i)
{
	return i%1 == 0
}
function validate_CheckAmount(field, rules, i, options) {
	
	console.log("_CheckAmount");
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	
	if (typeof (sFieldName) != "string") {
		return "* CheckAmount含有無效的參數sFieldName";
	}
			
	try {
		
		var sAmount = $("#" + oField).val();
		bIsInteger = boolIsInteger(sAmount);
		
		if ( isNaN(sAmount) || sAmount.startsWith(' ') || sAmount.endsWith(' ')) {//數字
			return "*　"+ sFieldName + "欄位請輸入正確的金額格式";
		}

		if (sFieldName.indexOf("+") == 0) {
			return "* "+ sFieldName + "欄位請勿輸入加號";
		}
		
		if (sAmount.charAt(0) == "0" && sAmount.length > 1) {//check不以零為開頭
			return "* "+ sFieldName + "欄位請勿以零為開頭";
		}

		if (bIsInteger == true && sAmount.indexOf(".") != -1) {
			return "* "+ sFieldName + "欄位請輸入整數";
		}

		if (bIsInteger == false && sAmount.indexOf(".") == sAmount.length - 1) {
			return "* "+ sFieldName + "欄位請輸入正確的含小數點金額格式";
		}

	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
	}

}
/*
 * 檢查交易密碼
 * PARAM oField:交易密碼輸入框
 * return  
 * validate[required,funcCall[validate_CheckTxnPassword[oField]]]
 */

function validate_CheckTxnPassword(field, rules, i, options) {
	
	console.log("_CheckTxnPassword");

	var oField = rules[i + 2];
	
	try {
		var sValue =  $("#" + oField).val();
		var regex = /[^0-9]/;

		if (sValue.length < 6 || sValue.length > 8 || regex.test(sValue)) {
			return "* 交易密碼欄位請輸入6-8位數字";

		}
	} 
	catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
	}

}

/*
 * 檢核Select物件
 * PARAM sFieldName:欄位中文描述
 * PARAM aSelect:Select物件
 * PARAM sDoNotSelect:不能為選取值的字串，無限制時給null
 * return
 * validate[funcCall[validate_CheckSelect['sFieldName',aSelect,sDoNotSelect]]]
 */
function validate_CheckSelect(field, rules, i, options) {
	
	console.log("_CheckSelect");
	
	var sFieldName = rules[i + 2];
	var aSelect = rules[i + 3];
	var sDoNotSelect = rules[i + 4];
	
	if (typeof (sFieldName) != "string") {
		return "* CheckSelect含有無效的參數sFieldName";
	}
	
	try {
		
		var aSelected = document.getElementById(aSelect);
		var selectValue = $("#" + aSelect + " option:selected").val();
		
		if (aSelected.length == 0 || (aSelected.length == 1 && aSelected[0].value == sDoNotSelect)) {
			return "* 沒有可選擇的" + sFieldName;
		}
		
		if (typeof (sDoNotSelect) == "string") {
			var bHasSelected = false;

			for (var i = 0; i < aSelected.length; i++) {
				var o = aSelected[i];
				if (o.selected) {
					bHasSelected = true;

					if (o.value == sDoNotSelect) {
						return "* 請選擇" + sFieldName;
					}
				}
			}

			// 當所有的項目皆未選取時，比較第一個項目
			if (bHasSelected == false && aSelected.length > 0) {
				if (aSelected[0].value == sDoNotSelect) {
					return "* 請選擇" + sFieldName;
				}
			}
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception)
	}
}

function IsValidDate(sText)
{
///<summary>檢查指定文字是否符合1900~2099年間的"4碼年/2碼月/2碼日"日期格式</summary>
///<param name="sText">待檢查的字串</param>
///<returns>true:檢核成功 false:檢核失敗</returns>

	//先檢查字串是否符合格式
	var reDate = /(19\d{2}|20\d{2}|21\d{2})\/(0[1-9]|1[0-2])\/(0[1-9]|[12][0-9]|3[01])/;
	var result = reDate.test(sText);
	if (result == false)
		return result;
		
	//再檢查是否有符合格式卻不符合真正日期的錯誤，如2008/02/31
	var sYear = RegExp.$1;
	var sMonth = RegExp.$2;
	var sDate = RegExp.$3;
	
	var dDate = new Date(sText); //如字串"2008/02/31"轉成Date物件，月份會變3月，日期會變2號

	var iYear = dDate.getFullYear();
	var iMonth = dDate.getMonth() + 1;
	var iDate = dDate.getDate();
	
	if (sYear != iYear || sMonth != iMonth || sDate != iDate)
		return  false;
		
	return true;
}

function CyearToEyear(sCyear)
{
///<summary>轉換民國年(097)至西元年(2008)</summary>
///<param name="sCyear">前三碼為民國年之字串</param>
///<returns>前四碼為西元年之字串</returns>
	//sCyear="097/01/31";
	//傳入字串應先通過驗證符合三碼民國年格式
	var year = parseInt(sCyear.substring(0, 3), 10) + 1911;
	return year + sCyear.substring(3);
}
/*
 * 檢查日期輸入框內容
 * PARAM sFieldName:欄位中文描述
 * PARAM oDate:要檢核的日期輸入框物件
 * PARAM sMinDate:最小日期字串，無最小日限制時給null
 * PARAM sMaxDate:最大日期字串，無最大日限制時給null
 * return
 * validate[funcCall[validate_CheckCDate['sFieldName',oDate,sMinDate,sMaxDate]]]
 */
function validate_CheckDate(field, rules, i, options) {

	console.log("_CheckDate");
	
	var sFieldName = rules[i + 2];
	var oDate=rules[i + 3];
	var sMinDate=rules[i + 4];
	var sMaxDate=rules[i + 5];

	if (typeof (sFieldName) != "string") {
		return "* CheckDate含有無效的參數sFieldName";
	}

	// 檢查最小日參數
	var dMinDate = null;
	if (sMinDate != null) {
		if (IsValidDate(sMinDate)) {
			dMinDate = new Date(sMinDate);
		} else {
			return "* CheckDate含有無效的參數sMinDate";
		}
	}

	// 檢查最大日參數
	var dMaxDate = null;
	if (sMaxDate != null) {
		if (IsValidDate(sMaxDate)) {
			dMaxDate = new Date(sMaxDate);
		} else {
			return "* CheckDate含有無效的參數sMaxDate";
		}
	}

	if (dMinDate != null && dMaxDate != null && dMinDate > dMaxDate) {
		return "* CheckDate的參數sMinDate不能大於sMaxDate";
	}
	
	var ovalue = $("#" + oDate).val();
	
	try {
		if (!IsValidDate(ovalue)) {
			return "* 請輸入正確的" + sFieldName + "格式，例如 2009/01/31";
		}
		var dDate = new Date(ovalue);

		if (dMaxDate != null && dDate > dMaxDate) {
			return "* "+ sFieldName + "不能大於 " + sMaxDate;
			oDate.focus();
		}

		if (dMinDate != null && dDate < dMinDate) {
			return "* "+sFieldName + "不能小於 " + sMinDate;
			oDate.focus();
		}
	} catch (exception) {
		return "* 檢核" + sFieldName + "時發生錯誤:" + exception;
	}
}


/*
 * 檢查民國日期輸入框內容
 * PARAM sFieldName:欄位中文描述
 * PARAM oDate:要檢核的日期輸入框物件
 * PARAM sMinDate:最小日期字串，無最小日限制時給null
 * PARAM sMaxDate:最大日期字串，無最大日限制時給null
 * return 
 * validate[funcCall[validate_CheckCDate['sFieldName',oDate,sMinDate,sMaxDate]]]
 */
function validate_CheckCDate(field, rules, i, options) {

	console.log("_CheckCDate");
	
	var sFieldName = rules[i + 2];
	var oDate=rules[i + 3];
	var sMinDate=rules[i + 4];
	var sMaxDate=rules[i + 5];
	
	// 檢查是否為字串
	if (typeof (sFieldName) != "string") {
		return "* CheckCDate含有無效的參數sFieldName";
	}

	// 檢查最小日參數
	var dMinDate = null;
	if (sMinDate != null) {
		var min = CyearToEyear(sMinDate);
		
		if (IsValidDate(min)) {
			dMinDate = new Date(min);
		} else {
			return "* CheckCDate含有無效的參數sMinDate";
		}
	}

	// 檢查最大日參數
	var dMaxDate = null;
	if (sMaxDate != null) {
		var max = CyearToEyear(sMaxDate);
		if (IsValidDate(max)) {
			dMaxDate = new Date(max);
		} else {
			return "* CheckCDate含有無效的參數sMaxDate";
		}
	}

	if (dMinDate != null && dMaxDate != null && dMinDate > dMaxDate) {
		return "* CheckCDate的參數sMinDate不能大於sMaxDate";
	}
	
	var ovalue = $("#" + oDate).val();
	
	try {
		var sDate = CyearToEyear(ovalue);

		if (!IsValidDate(sDate)) {
			return "* 請輸入正確的" + sFieldName + "格式，例如 097/01/31";
			TryFocus(oDate);
		}
		var dDate = new Date(sDate);

		if (dMaxDate != null && dDate > dMaxDate) {
			return "* " +sFieldName + "不能大於 " + sMaxDate;
			TryFocus(oDate);
		}

		if (dMinDate != null && dDate < dMinDate) {
			return "* " +sFieldName + "不能小於 " + sMinDate;
			TryFocus(oDate);
		}
	} catch (exception) {
		return "*檢核" + sFieldName + "時發生錯誤:" + exception;
	}

}


/*
 * 檢查數字輸入框內容(不含加號、減號、小數點),並依指定長度作檢核
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:數字輸入框
 * PARAM len:最小值，長度限制
 * return 
 * validate[required,funcCall[validate_CheckNumWithDigit['sFieldName',oField,len]]]
 */
function validate_CheckNumWithDigit(field, rules, i, options) {

	console.log("_CheckNumWithDigit");
	
	var sFieldName = rules[i + 2];
	var oField=rules[i + 3];
	var len=rules[i + 4];
	
	if (typeof (sFieldName) != "string") {
		return "* CheckNumWithDigit含有無效的參數sFieldName";
	}

	try {
		var sNumber = $("#" + oField).val();;

		var reNumber = /[^0-9]/;
		if (reNumber.test(sNumber)) {//僅能輸入數字
			return "* " + sFieldName + "欄位只能輸入數字(0~9)";
			oField.focus();
		}

		if (sNumber.length != len) {
			return "* " + sFieldName + "欄位應為" + len + "位數字";
			oField.focus();
		}
	} catch (exception) {
		return "* 檢核" + sFieldName + "時發生錯誤:" + exception;
	}

}


/*
 * 檢查日期輸入框內容
 * PARAM oField:eMAIL輸入欄位
 * return 
 * validate[required,funcCall[validate_EmailCheck[oField]]]
 */
function validate_EmailCheck(field, rules, i, options) {
	
	console.log("_EmailCheck");
	
	var oField = rules[i + 2];
	var rexpress = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var value = $("#" + oField).val();

	if (!rexpress.test(value)) {
		return options.allrules.email.alertText;
	}

}
/*
 * 檢核日期範圍
 * PARAM sFieldName:欄位中文描述
 * PARAM oBaseDate:基準日期，可為輸入框物件或日期物件或日期字串
 * PARAM oStartDate:起始日輸入框物件
 * PARAM oEndDate:終止日輸入框物件
 * PARAM bCanOverBaseDate:起始日或終止日可否大於基準日
 * PARAM iMonthAgo:以基準日往前推算之月份，無限制時給null
 * PARAM iMonthScope:起始日與終止日之相距月份限制，無月份限制時給null
 * return 
 * validate[required,funcCall[validate_CheckDateScope['sFieldName', oBaseDate, oStartDate, oEndDate, bCanOverBaseDate, iMonthAgo, iMonthScope]]]
 */
function validate_CheckDateScope(field, rules, i, options)
{	
	console.log("_CheckDateScope");
	
	var sFieldName = rules[i + 2];
	var oBaseDate = rules[i + 3];
	var oStartDate = rules[i + 4];
	var oEndDate = rules[i + 5];
	var bCanOverBaseDate = rules[i + 6];
	var iMonthAgo = Number(rules[i + 7]);
	var iMonthScope = Number(rules[i + 8]);
	
	if  (typeof(sFieldName) != "string")
	{
		return "* CheckDateScope含有無效的參數sFieldName";
	}
	
	try
	{
		oBaseDatevalue = $("#" + oBaseDate).val();
		//基準日
		var dBaseDate;
		if (oBaseDate instanceof Date)
		{//日期物件
			dBaseDate = oBaseDatevalue;
		}
		else if (typeof(oBaseDatevalue) == "string")
		{//日期字串，如2008/08/01
			if (!IsValidDate(oBaseDatevalue))
			{
				return "* 請使用正確的基準日參數，例如 2009/01/31";
			}
			dBaseDate = new Date(oBaseDatevalue);
		}
		else
		{//輸入框物件
			if (!IsValidDate(oBaseDatevalue))
			{
				return "* 請輸入正確的基準日，例如 2009/01/31";
			}
			dBaseDate = new Date(oBaseDatevalue);
		}
		
		var oBaseYear = dBaseDate.getFullYear().toString();
		var oBaseMonth = dBaseDate.getMonth() + 1;
		oBaseMonth = oBaseMonth < 10 ? "0" + oBaseMonth : oBaseMonth.toString();
		var oBaseDay = dBaseDate.getDate();
		oBaseDay = oBaseDay < 10 ? "0" + oBaseDay : oBaseDay.toString();
		sBaseDate = oBaseYear + "/" + oBaseMonth + "/" + oBaseDay;
		
		//最小日
		var dMinDate = null;
		var sMinDate = null;
		if (iMonthAgo != null)
		{
		    var oMinMonth = null;
		    if (oBaseMonth > iMonthAgo)
		        oMinMonth = oBaseMonth - iMonthAgo - 1;
		    else
		        oMinMonth = -1 - (iMonthAgo - oBaseMonth);
		        
		    dMinDate = new Date(sBaseDate);
		    dMinDate.setMonth(oMinMonth);
		    dMinDate.setDate(1);
		    
		    oMinMonth = dMinDate.getMonth() + 1;
		    oMinMonth = oMinMonth < 10 ? "0" + oMinMonth : oMinMonth.toString();
		
			sMinDate = dMinDate.getFullYear() + "/" + oMinMonth + "/01";
		}
			
		oStartDatevalue = $("#" + oStartDate).val();
		//起始日
		if (!IsValidDate(oStartDatevalue))
		{
			return "* 請輸入正確的起始日格式，例如 2009/01/31";
		}
		var dStartDate = new Date(oStartDatevalue);
		var oStartYear = dStartDate.getFullYear().toString();
		var oStartMonth = dStartDate.getMonth() + 1;
		oStartMonth = oStartMonth < 10 ? "0" + oStartMonth : oStartMonth.toString();
		var oStartDay = dStartDate.getDate();
		var iStartDay = oStartDay;
		oStartDay = oStartDay < 10 ? "0" + oStartDay : oStartDay.toString();

		if (!bCanOverBaseDate && dStartDate > dBaseDate)
		{
			return "* 起始日不能大於 " + sBaseDate;
		}
		
		if (dMinDate != null && dStartDate < dMinDate)
		{
			return "* 起始日不能小於 " + sMinDate;
		}
		oEndDatevalue = $("#" + oEndDate).val();
		//終止日
		if (!IsValidDate(oEndDatevalue))
		{
			return "* 請輸入正確的終止日格式，例如 2009/01/31";
		}
		var dEndDate = new Date(oEndDatevalue);
		var oEndYear = dEndDate.getFullYear().toString();
		var oEndMonth = dEndDate.getMonth() + 1;
		oEndMonth = oEndMonth < 10 ? "0" + oEndMonth : oEndMonth.toString();
		var oEndDay = dEndDate.getDate();
		oEndDay = oEndDay < 10 ? "0" + oEndDay : oEndDay.toString();
		
		//最大日
		var dMaxDate = null;
		var sMaxDate = null;
		if (iMonthScope != null)
		{
			if (iMonthScope == iMonthAgo)
			{
				dMaxDate = dBaseDate;
				sMaxDate = sBaseDate;
			}
			else
			{
				oMaxMonth = dStartDate.getMonth() + 1 + iMonthScope;
				if (oMaxMonth > 12)
				{
					var i = parseInt(oMaxMonth / 12);
					oMaxMonth = oMaxMonth - 12 * i;
					if (oMaxMonth == 0)
					{
						oMaxMonth = 12;
						i = i - 1;
					}
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth.toString();
					sMaxDate = (parseInt(oStartYear) + i) + "/" + oMaxMonth + "/"  //尚未加入day
				}else
				{
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth.toString();
					sMaxDate = oStartYear + "/" + oMaxMonth + "/"    //尚未加入day
				}
				
				for (var i = parseInt(iStartDay); i > 0; i--)
				{//防止出現如2008/2/31之日期
					var oDay = i < 10 ? "0" + i.toString() : i.toString();
					var sDate = sMaxDate + oDay;
					if (IsValidDate(sDate))
					{
						sMaxDate = sDate;
						dMaxDate = new Date(sMaxDate);
						break;
					}
				}
			}
		}
		
		if (!bCanOverBaseDate && (dMaxDate == null || dMaxDate > dBaseDate))
		{//最大日不能超過基準日
			sMaxDate = sBaseDate;
			dMaxDate = dBaseDate;
		}

		if (dMaxDate != null && dEndDate > dMaxDate)
		{
			return "* 終止日不能大於 " + sMaxDate;
		}
		
		if (dEndDate < dStartDate)
		{
			return "* 終止日不能小於起始日";
		}
	}
	catch (exception)
	{
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
	}
}

function EyearToCyear(sEyear)
{
///<summary>轉換西元年(2008)至民國年(097)</summary>
///<param name="sEyear">前四碼為西元年之字串</param>
///<returns>前三碼為民國年之字串</returns>

	//傳入字串É先通過驗證符合四碼西元年格式
	var zeros = "000";
	var year = (parseInt(sEyear.substring(0, 4), 10) - 1911).toString();
	year = zeros.substring(0, 3 - year.length) + year;
	return year + sEyear.substring(4);
}

/*
 * 檢核民國日期範圍
 * PARAM sFieldName:欄位中文描述
 * PARAM oBaseDate:基準日期，可為輸入框物件或日期物件或日期字串
 * PARAM oStartDate:起始日輸入框物件
 * PARAM oEndDate:終止日輸入框物件
 * PARAM bCanOverBaseDate:起始日或終止日可否大於基準日
 * PARAM iMonthAgo:以基準日往前推算之月份，無限制時給null
 * PARAM iMonthScope:起始日與終止日之相距月份限制，無月份限制時給null
 * return
 * validate[required,funcCall[validate_CheckCDateScope['sFieldName', oBaseDate, oStartDate, oEndDate, bCanOverBaseDate, iMonthAgo, iMonthScope]]]
 */
function validate_CheckCDateScope(field, rules, i, options)
{
	console.log("_CheckCDateScope");
	
	var sFieldName = rules[i + 2];
	var oBaseDate = rules[i + 3];
	var oStartDate = rules[i + 4];
	var oEndDate = rules[i + 5];
	var bCanOverBaseDate = rules[i + 6];
	var iMonthAgo = rules[i + 7];
	var iMonthScope = rules[i + 8];
	
	if  (typeof(sFieldName) != "string")
	{
		return "* CheckDateScope含有無效的參數sFieldName";
	}
	
	try
	{
		oBaseDatevalue = $("#" + oBaseDate).val();
		//基準日
		var dBaseDate;
		if (oBaseDate instanceof Date)
		{//日期物件
			dBaseDate = oBaseDatevalue;
		}
		else if (typeof(oBaseDatevalue) == "string")
		{//日期字串，如097/08/01
			var base = CyearToEyear(oBaseDatevalue);
			if (!IsValidDate(base))
			{
				return "* 請使用正確的基準日參數，例如 097/01/31";
			}
			dBaseDate = new Date(base);
		}
		else
		{//輸入框物件
			var base = CyearToEyear(oBaseDatevalue);
			if (!IsValidDate(base))
			{
				return "* 請輸入正確的基準日，例如 097/01/31";
			}
			dBaseDate = new Date(base);
		}
		
		var oBaseYear = EyearToCyear(dBaseDate.getFullYear().toString());
		var oBaseMonth = dBaseDate.getMonth() + 1;
		oBaseMonth = oBaseMonth < 10 ? "0" + oBaseMonth : oBaseMonth.toString();
		var oBaseDay = dBaseDate.getDate();
		oBaseDay = oBaseDay < 10 ? "0" + oBaseDay : oBaseDay.toString();
	    sBaseDate = oBaseYear + "/" + oBaseMonth + "/" + oBaseDay;
		
		//最小日
		var dMinDate = null;
		var sMinDate = null;
		if (iMonthAgo != null)
		{
		    var oMinMonth = null;
		    if (oBaseMonth > iMonthAgo)
		        oMinMonth = oBaseMonth - iMonthAgo - 1;
		    else
		        oMinMonth = -1 - (iMonthAgo - oBaseMonth);
		        
		    dMinDate = new Date(CyearToEyear(sBaseDate));
		    dMinDate.setMonth(oMinMonth);
		    dMinDate.setDate(1);
		    
		    oMinMonth = dMinDate.getMonth() + 1;
		    oMinMonth = oMinMonth < 10 ? "0" + oMinMonth : oMinMonth.toString();
		
			oMinYear = EyearToCyear(dMinDate.getFullYear().toString());
			sMinDate = oMinYear + "/" + oMinMonth + "/01";
		}
		oStartDatevalue = $("#" + oStartDate).val();
		//起始日
		var start = CyearToEyear(oStartDatevalue);
		if (!IsValidDate(start))
		{
			return "* 請輸入正確的起始日格式，例如 097/01/31";
		}
		var dStartDate = new Date(start);
		var oStartYear = EyearToCyear(dStartDate.getFullYear().toString());
		var oStartMonth = dStartDate.getMonth() + 1;
		oStartMonth = oStartMonth < 10 ? "0" + oStartMonth : oStartMonth.toString();
		var oStartDay = dStartDate.getDate();
		var iStartDay = oStartDay;
		oStartDay = oStartDay < 10 ? "0" + oStartDay : oStartDay.toString();
		
		if (!bCanOverBaseDate && dStartDate > dBaseDate)
		{
			return "* 起始日不能大於 " + sBaseDate;
		}
		
		if (dMinDate != null && dStartDate < dMinDate)
		{
			return "* 起始日不能小於 " + sMinDate;
		}
		oEndDatevalue = $("#" + oEndDate).val();
		//終止日
		var end = CyearToEyear(oEndDatevalue);
		if (!IsValidDate(end))
		{
			return "* 請輸入正確的終止日格式，例如 097/01/31";
		}
		var dEndDate = new Date(end);
		var oEndYear = EyearToCyear(dEndDate.getFullYear().toString());
		var oEndMonth = dEndDate.getMonth() + 1;
		oEndMonth = oEndMonth < 10 ? "0" + oEndMonth : oEndMonth.toString();
		var oEndDay = dEndDate.getDate();
		oEndDay = oEndDay < 10 ? "0" + oEndDay : oEndDay.toString();
		
		//最大日
		var dMaxDate = null;
		var sMaxDate = null;
		if (iMonthScope != null)
		{
			if (iMonthScope == iMonthAgo)
			{
				dMaxDate = dBaseDate;
				sMaxDate = sBaseDate;
			}
			else
			{
				oMaxMonth = dStartDate.getMonth() + 1 + iMonthScope;
				if (oMaxMonth > 12)
				{
					var i = parseInt(oMaxMonth / 12);
					oMaxMonth = oMaxMonth - 12 * i;
					if (oMaxMonth == 0)
					{
						oMaxMonth = 12;
						i = i - 1;
					}
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth.toString();
					var zeros = "000";
					var max = (parseInt(oStartYear, 10) + i).toString();
					max = zeros.substring(0, 3 - max.length) + max;
					sMaxDate = max + "/" + oMaxMonth + "/"  //尚未加入day
				}else
				{
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth.toString();
					sMaxDate = oStartYear + "/" + oMaxMonth + "/"    //尚未加入day
				}
				
				for (var i = parseInt(iStartDay); i > 0; i--)
				{//防止出現如097/2/31之日期
					var oDay = i < 10 ? "0" + i.toString() : i.toString();
					var sDate = sMaxDate + oDay;
					var d = CyearToEyear(sDate);
					if (IsValidDate(d))
					{
						sMaxDate = sDate;
						dMaxDate = new Date(d);
						break;
					}
				}
			}
		}
		
		if (!bCanOverBaseDate && (dMaxDate == null || dMaxDate > dBaseDate))
		{//最大日不能超過基準日
			sMaxDate = sBaseDate;
			dMaxDate = dBaseDate;
		}
		
		if (dMaxDate != null && dEndDate > dMaxDate)
		{
			return "* 終止日不能大於 " + sMaxDate;
		}
		
		if (dEndDate < dStartDate)
		{
			return "*　終止日不能小於起始日";
		}
	}
	catch (exception)
	{
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
	}

}
//身份證字號檢查
function checkID( idname ) {
	 var id = idname.value;
	 if (id == undefined)	id = idname;
	 id=id.toUpperCase();//英文字母轉大寫
	 tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO"
   A1 = new Array (1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3 );
   A2 = new Array (0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5 );
   Mx = new Array (9,8,7,6,5,4,3,2,1,1);

   if ( id.length != 10 ) return false;
   i = tab.indexOf( id.charAt(0) );
   if ( i == -1 ) return false;
   
   
   sum = A1[i] + A2[i]*9;

   for ( i=1; i<10; i++ ) {
      v = parseInt( id.charAt(i) );
      if ( isNaN(v) ) return false;
      sum = sum + v * Mx[i];
   }
   if ( sum % 10 != 0 ) return false;
   return true;
}

//外僑居留證字號檢查
function checkID2( idname ) {
	 var id = idname.value;
	 if (id == undefined)	id = idname;
	 id=id.toUpperCase();//英文字母轉大寫
	 tab = "ABCDEFGHJKLMNPQRSTUVWXYZIO"
   A1 = new Array (1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3 );
   A2 = new Array (0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5 );
   Mx = new Array (9,8,7,6,5,4,3,2,1,1);

   i1 = tab.indexOf( id.charAt(0) );
   i2 = tab.indexOf( id.charAt(1) );
      
   front_1 = A1[i1]*1 + A2[i1]*9;
	 front_2 = A2[i2]*8;
	 sum = front_1 + front_2;

   for ( i=2; i<10; i++ ) {
      v = parseInt( id.charAt(i) );
      if ( isNaN(v) ) return false;
      sum = sum + v * Mx[i];
   }
   if ( sum % 10 != 0 ) return false;
   return true;
}

//統一編號檢查
function TaxID(sTax)
{ 
try {
	var sTaxID = sTax.value;
var i,a1,a2,a3,a4,a5;
var b1,b2,b3,b4,b5;
var c1,c2,c3,c4;
var d1,d2,d3,d4,d5,d6,d7,cd8;
 if(sTaxID.length != 8) return false; 
 var c; 
 for (i = 0; i < 8; i++) 
 { 
 c = sTaxID.charAt(i); 
 if ("0123456789".indexOf(c) == -1) return false; 
 } 
 d1 = parseInt(sTaxID.charAt(0)); 
 d2 = parseInt(sTaxID.charAt(1)); 
 d3 = parseInt(sTaxID.charAt(2)); 
 d4 = parseInt(sTaxID.charAt(3)); 
 d5 = parseInt(sTaxID.charAt(4)); 
 d6 = parseInt(sTaxID.charAt(5)); 
 d7 = parseInt(sTaxID.charAt(6)); 
 cd8 = parseInt(sTaxID.charAt(7)); 
 c1 = d1; 
 c2 = d3; 
 c3 = d5; 
 c4 = cd8; 
 a1 = parseInt((d2 * 2) / 10); 
 b1 = (d2 * 2) % 10; 
 a2 = parseInt((d4 * 2) / 10); 
 b2 = (d4 * 2) % 10; 
 a3 = parseInt((d6 * 2) / 10); 
 b3 = (d6 * 2) % 10; 
 a4 = parseInt((d7 * 4) / 10); 
 b4 = (d7 * 4) % 10; 
 a5 = parseInt((a4 + b4) / 10); 
 b5 = (a4 + b4) % 10; 
 if((a1 + b1 + c1 + a2 + b2 + c2 + a3 + b3 + c3 + a4 + b4 + c4) % 10 == 0) return true; 
 if(d7 = 7) { if((a1 + b1 + c1 + a2 + b2 + c2 + a3 + b3 + c3 + a5 + c4) % 10 == 0) return true; } 
 return false; 
 }catch(e)
 { return false; }
 }
/*
 * 檢查身份證字號及統一編號
 * PARAM obj:身份證字號輸入欄位
 * return 
 * validate[required,funcCall[validate_checkSYS_IDNO[obj]]]
 */
function validate_checkSYS_IDNO(field, rules, i, options){
	
	console.log("_checkSYS_IDNO");
	
	var obj = rules[i + 2];
	var objvalue = $("#"+obj).val();

	if(objvalue!=""){
	  if(objvalue.length==8 || objvalue.length==10){
		  if(objvalue.length==8){
			  if(!TaxID(objvalue))
			  {
				  return "* 身分證或統一編號輸入有錯，請重新輸入。";
			  }
		  }
	    
		  if(objvalue.length==10){
	    	
			  var reType1 = /[A-Z]{1}\d{9}/;
			  var reType2 = /[A-Z]{2}\d{8}/;
			  var reType3 = /\d{8}[A-Z]{2}/;      	
	    	
			  if (reType1.test(objvalue)) {      	
				  if(!checkID(objvalue))
				  {
					  return "* 身分證或統一編號輸入有錯，請重新輸入。";
				  }				 					 
			  }        
			  else if (reType2.test(objvalue)) {
				  if(!checkID2(objvalue))
				  {
					  return "* 身分證或統一編號輸入有錯，請重新輸入。";
				  }					 					         		
			  }       
			  else if (!reType3.test(objvalue)) {       
				      return "* 身分證或統一編號輸入有錯，請重新輸入。";				 					
			  }  	         		        
			  else {
				   return "* 身分證或統一編號輸入有錯，請重新輸入。";
			  }
					
		  }
	  }
	  else{
	    return "* F身分證或統一編號輸入有錯，請重新輸入。";
	  }
	}
}
/*
 * 檢查輸入框內容字數是否符合指定字數
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * PARAM bCanEmpty:可否為空字串
 * PARAM iEqualLen:必須符合的字串長度
 * return 
 * validate[required,funcCall[validate_CheckLenEqual['sFieldName',oField,bCanEmpty,iEqualLen]]]
 */
function validate_CheckLenEqual(field, rules, i, options)
{
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var bCanEmpty = rules[i + 4];
	var iEqualLen = rules[i + 5];
	
    if  (typeof(sFieldName) != "string")
	{
		return "* CheckLenEqual含有無效的參數sFieldName";
	}
	
	try
	{
	    var sNumber = $('#'+oField).val();
	    
	    if (bCanEmpty == false && sNumber == "")
	    {
	        return "* 請輸入" + sFieldName;
	    }
	    
	    var iLen = sNumber.length;
		
		if (iLen != iEqualLen)
		{
	        return "* "+sFieldName + "的內容長度應該等於" + iEqualLen;		
		}
	}
	catch (exception)
	{
	    alert("檢核" + sFieldName + "時發生錯誤:" + exception);
	}
}

/*
 * 檢查輸入框內容字數是否只有英文數字
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * return 
 * validate[required,funcCall[validate_chkChrNum['sFieldName',oField]]]
 */
function validate_chkChrNum(field, rules, i, options){
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	
	var str=$('#'+oField).val();
	
	var slength = str.length;
	for(i=0;i<slength;i++){
		var chkchar = str.charAt(i);
		if(chkchar<'0' || chkchar>'z'){
			return "* "+sFieldName+"僅能輸入英文數字";
		}
		else{
			if(chkchar>'9' && chkchar<'A'){
				return "* "+sFieldName+"僅能輸入英文數字";
			}
			if(chkchar>'Z' && chkchar<'a'){
				return "* "+sFieldName+"僅能輸入英文數字";
			}
				
		}
	}
}