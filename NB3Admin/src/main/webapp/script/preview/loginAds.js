var bannerCssTextL = ""; // banner大廣告css動態呈現
var bannerCssTextM = ""; // banner中廣告css動態呈現
var bannerCssTextS = ""; // banner小廣告css動態呈現

/**
 * 登入頁Banner廣告顯示JS
 */
function showBanner(bannerImgs, ctx) {
	console.log("login.bannerImgs: " + bannerImgs);

	if(bannerImgs>0){
		for (var i = 1; i <= bannerImgs; i++) {
			textBanner(i, ctx);
			$("#bg-for-banner").text(
				/* 大圖 */
				"@media screen and (min-width: 992px) {" + bannerCssTextL + "}" +
				/* 中圖 */
				"@media screen and (min-width: 767px) and (max-width: 992px) {" + bannerCssTextM + "}" +
				/* 小圖 */
				"@media screen and (max-width: 767px) {" + bannerCssTextS + "}"
			);
		}
		
	} else {
		textBanner(0, ctx);
		$("#bg-for-banner").text(
			/* 大圖 */
			"@media screen and (min-width: 992px) {" + bannerCssTextL + "}" +
			/* 中圖 */
			"@media screen and (min-width: 767px) and (max-width: 992px) {" + bannerCssTextM + "}" +
			/* 小圖 */
			"@media screen and (max-width: 767px) {" + bannerCssTextS + "}"
		);
	}
	
	console.log("login.showBanner.Finish!");
}

/**
 * 登入頁Banner廣告顯示css動態組成
 */
function textBanner(index, ctx) {
	// 預設圖檔跟落地圖檔不同目錄
	var directory = "banner";
	var i = index;
	
	if(index==0){
		directory = "default"
		i = index+1;
	}
	
	bannerCssTextL = bannerCssTextL +
		".carousel-inner .carousel-item:nth-child(" + i + ") {" +
			"background: url('" + ctx + "/login/" + directory + "/banner-lg-" + index + ".png');" +
			"background-repeat: no-repeat;" +
			"background-size: cover;" +
		"}";
		
	bannerCssTextM = bannerCssTextM +
		".carousel-inner .carousel-item:nth-child(" + i + ") { " +
			"background: url('" + ctx + "/login/" + directory + "/banner-md-" + index + ".png');" + 
			"background-repeat: no-repeat;" +
			"background-size: cover;" +
		"}";
		
	 bannerCssTextS = bannerCssTextS + 	
		".carousel-inner .carousel-item:nth-child(" + i + ") {" +
			"background: url('" + ctx + "/login/" + directory + "/banner-sm-" + index + ".png');" +
			"background-repeat: no-repeat;" +
			"background-size: cover;" +
		"}";
		
}

/**
 * 判斷裝置
 */
function checkBrowser() {
	var checkBrowserResult = false;
	if (navigator.userAgent.match(/Android/i)
			|| navigator.userAgent.match(/webOS/i)
			|| navigator.userAgent.match(/iPhone/i)
			|| navigator.userAgent.match(/iPad/i)
			|| navigator.userAgent.match(/iPod/i)
			|| navigator.userAgent.match(/BlackBerry/i)
			|| navigator.userAgent.match(/Windows Phone/i)) {
		// 行動裝置
		console.log("component checkBrowser not pass!!!");
	} else {
		// 可使用元件之裝置
		checkBrowserResult = true;
		console.log("component checkBrowser pass...");
	}

	console.log("checkBrowser: " + checkBrowserResult);

	return checkBrowserResult;
}


//-------------------------------------------------------------------------------------

/**
 * 動態鍵盤
 */
function dynamicKeyboard() {
	// 驗證裝置
	if(checkBrowser()){
		// 電腦
		$('.keyboard').keyboard();

//		$("#clearButton").click(function(){
//			$("#webpw").val("");
//			$("#webpw").focus();
//		});
//		$(".keyboardButtons li").mouseover(function(){
//			$("#webpw").focus();
//		});
		
		$("#openKeyboardBtn").click(function() {
			$('#keyboardBlock').show();
		});
		$("#closeKeyboardBtn").click(function(){
			$('#keyboardBlock').hide();
		});
	} else {
		// 隱藏動態鍵盤按鈕
		$('#openKeyboardBtn').hide();
		$('#keyboardBlock').hide();
	}
	
}
