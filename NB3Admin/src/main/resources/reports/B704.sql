SELECT N.adopid, 
       N.adopname, 
       0        AS ADAGREEF, 
       0        AS QtyType, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C21Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P21Count, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '0' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adcount 
             ELSE 0 
           END) AS T99, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '1' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adcount 
             ELSE 0 
           END) AS F99 
FROM   (SELECT * 
        FROM   nb3sysop 
        WHERE  adgpparent = 'GPA' 
               AND adopgroup IN ( 'GPA10', 'GPA30' )) N 
       LEFT JOIN (SELECT * 
                  FROM   admmonthreport S 
                  WHERE  S.adagreef = '0' 
                         AND S.cmyyyymm >= :SDate 
                         AND S.cmyyyymm <= :EDate) R 
              ON R.adopid = N.adopid 
GROUP  BY N.adopid, 
          N.adopname 
UNION ALL 
SELECT N.adopid, 
       N.adopname, 
       1        AS ADAGREEF, 
       0        AS QtyType, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C21Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P21Count, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '0' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adcount 
             ELSE 0 
           END) AS T99, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '1' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adcount 
             ELSE 0 
           END) AS F99 
FROM   (SELECT * 
        FROM   nb3sysop 
        WHERE  adgpparent = 'GPA' 
               AND adopgroup IN ( 'GPA10', 'GPA30' )) N 
       LEFT JOIN (SELECT * 
                  FROM   admmonthreport S 
                  WHERE  S.adagreef = '1' 
                         AND S.cmyyyymm >= :SDate 
                         AND S.cmyyyymm <= :EDate) R 
              ON R.adopid = N.adopid 
GROUP  BY N.adopid, 
          N.adopname 
UNION ALL 
SELECT N.adopid, 
       N.adopname, 
       -1       AS ADAGREEF, 
       0        AS QtyType, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C21Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P21Count, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '0' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adcount 
             ELSE 0 
           END) AS T99, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '1' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adcount 
             ELSE 0 
           END) AS F99 
FROM   (SELECT * 
        FROM   nb3sysop 
        WHERE  adgpparent = 'GPA' 
               AND adopgroup IN ( 'GPA10', 'GPA30' )) N 
       LEFT JOIN (SELECT * 
                  FROM   admmonthreport S 
                  WHERE  1 = 1 
                         AND S.cmyyyymm >= :SDate 
                         AND S.cmyyyymm <= :EDate) R 
              ON R.adopid = N.adopid 
GROUP  BY N.adopid, 
          N.adopname 
UNION ALL 
SELECT N.adopid, 
       N.adopname, 
       0        AS ADAGREEF, 
       1        AS QtyType, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C21Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P21Count, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '0' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adamount 
             ELSE 0 
           END) AS T99, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '1' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adamount 
             ELSE 0 
           END) AS F99 
FROM   (SELECT * 
        FROM   nb3sysop 
        WHERE  adgpparent = 'GPA' 
               AND adopgroup IN ( 'GPA10', 'GPA30' )) N 
       LEFT JOIN (SELECT * 
                  FROM   admmonthreport S 
                  WHERE  S.adagreef = '0' 
                         AND S.cmyyyymm >= :SDate 
                         AND S.cmyyyymm <= :EDate) R 
              ON R.adopid = N.adopid 
GROUP  BY N.adopid, 
          N.adopname 
UNION ALL 
SELECT N.adopid, 
       N.adopname, 
       1        AS ADAGREEF, 
       1        AS QtyType, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C21Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P21Count, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '0' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adamount 
             ELSE 0 
           END) AS T99, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '1' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adamount 
             ELSE 0 
           END) AS F99 
FROM   (SELECT * 
        FROM   nb3sysop 
        WHERE  adgpparent = 'GPA' 
               AND adopgroup IN ( 'GPA10', 'GPA30' )) N 
       LEFT JOIN (SELECT * 
                  FROM   admmonthreport S 
                  WHERE  S.adagreef = '1' 
                         AND S.cmyyyymm >= :SDate 
                         AND S.cmyyyymm <= :EDate) R 
              ON R.adopid = N.adopid 
GROUP  BY N.adopid, 
          N.adopname 
UNION ALL 
SELECT N.adopid, 
       N.adopname, 
       -1       AS ADAGREEF, 
       1        AS QtyType, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C21Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P21Count, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '0' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adamount 
             ELSE 0 
           END) AS T99, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '1' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adamount 
             ELSE 0 
           END) AS F99 
FROM   (SELECT * 
        FROM   nb3sysop 
        WHERE  adgpparent = 'GPA' 
               AND adopgroup IN ( 'GPA10', 'GPA30' )) N 
       LEFT JOIN (SELECT * 
                  FROM   admmonthreport S 
                  WHERE  1 = 1 
                         AND S.cmyyyymm >= :SDate 
                         AND S.cmyyyymm <= :EDate) R 
              ON R.adopid = N.adopid 
GROUP  BY N.adopid, 
          N.adopname 
UNION ALL 
SELECT 'ZZZZZZ' AS ADOPID, 
       '合計' AS ADOPNAME, 
       0        AS ADAGREEF, 
       0        AS QtyType, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C21Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P21Count, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '0' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adcount 
             ELSE 0 
           END) AS T99, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '1' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adcount 
             ELSE 0 
           END) AS F99 
FROM   (SELECT * 
        FROM   nb3sysop 
        WHERE  adgpparent = 'GPA' 
               AND adopgroup IN ( 'GPA10', 'GPA30' )) N 
       LEFT JOIN (SELECT * 
                  FROM   admmonthreport S 
                  WHERE  S.adagreef = '0' 
                         AND S.cmyyyymm >= :SDate 
                         AND S.cmyyyymm <= :EDate) R 
              ON R.adopid = N.adopid 
UNION ALL 
SELECT 'ZZZZZZ' AS ADOPID, 
       '合計' AS ADOPNAME, 
       1        AS ADAGREEF, 
       0        AS QtyType, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS C20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS C21Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adcount 
             ELSE 0 
           END) AS P20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adcount 
             ELSE 0 
           END) AS P21Count, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '0' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adcount 
             ELSE 0 
           END) AS T99, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '1' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adcount 
             ELSE 0 
           END) AS F99 
FROM   (SELECT * 
        FROM   nb3sysop 
        WHERE  adgpparent = 'GPA' 
               AND adopgroup IN ( 'GPA10', 'GPA30' )) N 
       LEFT JOIN (SELECT * 
                  FROM   admmonthreport S 
                  WHERE  S.adagreef = '1' 
                         AND S.cmyyyymm >= :SDate 
                         AND S.cmyyyymm <= :EDate) R 
              ON R.adopid = N.adopid 
UNION ALL 
SELECT 'ZZZZZZ' AS ADOPID, 
       '合計' AS ADOPNAME, 
       0        AS ADAGREEF, 
       1        AS QtyType, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C21Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P21Count, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '0' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adamount 
             ELSE 0 
           END) AS T99, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '1' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adamount 
             ELSE 0 
           END) AS F99 
FROM   (SELECT * 
        FROM   nb3sysop 
        WHERE  adgpparent = 'GPA' 
               AND adopgroup IN ( 'GPA10', 'GPA30' )) N 
       LEFT JOIN (SELECT * 
                  FROM   admmonthreport S 
                  WHERE  S.adagreef = '0' 
                         AND S.cmyyyymm >= :SDate 
                         AND S.cmyyyymm <= :EDate) R 
              ON R.adopid = N.adopid 
UNION ALL 
SELECT 'ZZZZZZ' AS ADOPID, 
       '合計' AS ADOPNAME, 
       1        AS ADAGREEF, 
       1        AS QtyType, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS C20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '1' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS C21Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P00Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '0' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P01Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P10Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '1' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P11Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '0' ) THEN R.adamount 
             ELSE 0 
           END) AS P20Count, 
       Sum(CASE 
             WHEN ( R.adusertype = '0' 
                    AND R.adtxcode = '2' 
                    AND R.adtxstatus = '1' ) THEN R.adamount 
             ELSE 0 
           END) AS P21Count, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '0' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adamount 
             ELSE 0 
           END) AS T99, 
       Sum(CASE 
             WHEN ( R.adtxstatus = '1' and R.adusertype in ('0','1') and R.adtxcode in ('0','1','2') ) THEN R.adamount 
             ELSE 0 
           END) AS F99 
FROM   (SELECT * 
        FROM   nb3sysop 
        WHERE  adgpparent = 'GPA' 
               AND adopgroup IN ( 'GPA10', 'GPA30' )) N 
       LEFT JOIN (SELECT * 
                  FROM   admmonthreport S 
                  WHERE  S.adagreef = '1' 
                         AND S.cmyyyymm >= :SDate 
                         AND S.cmyyyymm <= :EDate) R 
              ON R.adopid = N.adopid 
ORDER  BY adopid, 
          adagreef desc, 
          qtytype 