SELECT CASE
         WHEN G.adopgroup = 'GPE' THEN 3
         WHEN G.adopgroup = 'GPB' THEN 4
         WHEN G.adopgroup = 'GPC' THEN 5
         WHEN G.adopgroup = 'GPD' THEN 6
       END                AS SEQNO,
       G.adopgroup,
       G.adopgroupname,
       1                  AS QTYTYPE,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q0T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q0F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q1T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q1F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q2T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q2F,
       VALUE(Sum(CASE
                   WHEN ( (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q3T,
       VALUE(Sum(CASE
                   WHEN ( (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q3F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '0') THEN R.adcount
                   ELSE 0
                 END), 0) AS Q9T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '1') THEN R.adcount
                   ELSE 0
                 END), 0) AS Q9F
 FROM   (SELECT *
        FROM   nb3sysopgroup
            WHERE  ( adopgroup IN ( 'GPB', 'GPC', 'GPE' , 'GPD' ) ) )  G
        LEFT JOIN (SELECT *
                   FROM   nb3sysop
                   WHERE  ::WHERE::) N
               ON G.adopgroup = N.ADGPPARENT
       LEFT JOIN (SELECT *
                  FROM   admmonthreport
                  WHERE  adusertype = '0'
                         AND cmyyyymm >= :SDate
                         AND cmyyyymm <= :EDate
                         AND ::LOGINTYPE::) R
              ON N.adopid = R.adopid
 GROUP  BY G.adopgroup,
          G.adopgroupname
  UNION ALL  
 SELECT CASE
         WHEN G.adopgroup = 'GPA40' THEN 1
         WHEN G.adopgroup = 'GPA50' THEN 2
       END                   AS SEQNO,
       VALUE(N.adopid, '')   AS ADOPID,
       VALUE(N.adopname, '') AS ADOPNAME,
       1                     AS QTYTYPE,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0)    AS Q0T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0)    AS Q0F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0)    AS Q1T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0)    AS Q1F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0)    AS Q2T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0)    AS Q2F,
       VALUE(Sum(CASE
                   WHEN ( (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0)    AS Q3T,
       VALUE(Sum(CASE
                   WHEN ( (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0)    AS Q3F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0)    AS Q9T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0)    AS Q9F
 FROM   (SELECT *
        FROM   nb3sysopgroup
        WHERE  adopgroup IN ( 'GPA40', 'GPA50' )
               AND ::WHERE::) G
       LEFT JOIN (SELECT *
                  FROM   nb3sysop
                  WHERE  ::WHERE::) N
              ON G.adopgroup = N.adopgroup
       LEFT JOIN (SELECT *
                  FROM   admmonthreport
                  WHERE  adusertype = '0'
                         AND cmyyyymm >= :SDate
                         AND cmyyyymm <= :EDate
                         AND ::LOGINTYPE::) R
              ON N.adopid = R.adopid
 GROUP  BY G.adopgroup,
          N.adopid,
          N.adopname
  UNION ALL  
 SELECT CASE
         WHEN G.adopgroup = 'GPA40' THEN 1
         WHEN G.adopgroup = 'GPA50' THEN 2
       END                   AS SEQNO,
       VALUE(N.adopid, '')   AS ADOPID,
       VALUE(N.adopname, '') AS ADOPNAME,
       2                     AS QTYTYPE,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0)    AS Q0T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0)    AS Q0F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0)    AS Q1T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0)    AS Q1F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0)    AS Q2T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0)    AS Q2F,
       VALUE(Sum(CASE
                   WHEN ( (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0)    AS Q3T,
       VALUE(Sum(CASE
                   WHEN ( (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0)    AS Q3F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0)    AS Q9T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0)    AS Q9F
 FROM   (SELECT *
        FROM   nb3sysopgroup
        WHERE  adopgroup IN ( 'GPA40', 'GPA50' )
               AND ::WHERE::) G
       LEFT JOIN (SELECT *
                  FROM   nb3sysop
                  WHERE  ::WHERE::) N
              ON G.adopgroup = N.adopgroup
       LEFT JOIN (SELECT *
                  FROM   admmonthreport
                  WHERE  adusertype = '0'
                         AND cmyyyymm >= :SDate
                         AND cmyyyymm <= :EDate
                         AND ::LOGINTYPE::) R
              ON N.adopid = R.adopid
 GROUP  BY G.adopgroup,
          N.adopid,
          N.adopname
  UNION ALL  
 SELECT CASE
         WHEN G.adopgroup = 'GPA40' THEN 1
         WHEN G.adopgroup = 'GPA50' THEN 2
       END                AS SEQNO,
       'ZZZZZZ'           AS ADOPID,
       '小計'           AS ADOPNAME,
       1                  AS QTYTYPE,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q0T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q0F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q1T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q1F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q2T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q2F,
       VALUE(Sum(CASE
                   WHEN ( (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q3T,
       VALUE(Sum(CASE
                   WHEN ( (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q3F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q9T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q9F
 FROM   (SELECT *
        FROM   nb3sysopgroup
        WHERE  adopgroup IN ( 'GPA40', 'GPA50' )
               AND ::WHERE::) G
       LEFT JOIN (SELECT *
                  FROM   nb3sysop
                  WHERE  ::WHERE::) N
              ON G.adopgroup = N.adopgroup
       LEFT JOIN (SELECT *
                  FROM   admmonthreport
                  WHERE  adusertype = '0'
                         AND cmyyyymm >= :SDate
                         AND cmyyyymm <= :EDate
                         AND ::LOGINTYPE::) R
              ON N.adopid = R.adopid
 GROUP  BY G.adopgroup
  UNION ALL  
 SELECT CASE
         WHEN G.adopgroup = 'GPA40' THEN 1
         WHEN G.adopgroup = 'GPA50' THEN 2
       END                AS SEQNO,
       'ZZZZZZ'           AS ADOPID,
       '小計'           AS ADOPNAME,
       2                  AS QTYTYPE,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q0T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q0F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q1T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q1F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q2T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q2F,
       VALUE(Sum(CASE
                   WHEN ( (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q3T,
       VALUE(Sum(CASE
                   WHEN ( (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q3F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q9T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q9F
 FROM   (SELECT *
        FROM   nb3sysopgroup
        WHERE  adopgroup IN ( 'GPA40', 'GPA50' )
               AND ::WHERE::) G
       LEFT JOIN (SELECT *
                  FROM   nb3sysop
                  WHERE  ::WHERE::) N
              ON G.adopgroup = N.adopgroup
       LEFT JOIN (SELECT *
                  FROM   admmonthreport
                  WHERE  adusertype = '0'
                         AND cmyyyymm >= :SDate
                         AND cmyyyymm <= :EDate
                         AND ::LOGINTYPE::) R
              ON N.adopid = R.adopid
 GROUP  BY G.adopgroup
 ORDER  BY seqno,
          qtytype 