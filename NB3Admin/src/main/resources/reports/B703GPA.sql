SELECT G.adopgroup,
       G.adopgroupname,
       1                  AS ADUSERTYPE,
       1                  AS QTYTYPE,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q00T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q00F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q01T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q01F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q02T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q02F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q03T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q03F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q10T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q10F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q11T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q11F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q12T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q12F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q13T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q13F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q99T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q99F
 FROM   (SELECT *
        FROM   nb3sysopgroup
        WHERE  ::WHERE::) G
       LEFT JOIN nb3sysop N
              ON G.adopgroup = N.adopgroup
       LEFT JOIN (SELECT *
                  FROM   admmonthreport
                  WHERE  adusertype = '1'
                         AND cmyyyymm >= :StartDt
                         AND cmyyyymm <= :EndDt
                         AND ::LOGINTYPE:: AND adtxcode IN ('0','1','2','3') AND adtxstatus IN ('0','1')) R
              ON N.adopid = R.adopid
 GROUP  BY G.adopgroup,
          G.adopgroupname
 UNION ALL
 SELECT G.adopgroup,
       G.adopgroupname,
       0                  AS ADUSERTYPE,
       1                  AS QTYTYPE,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q00T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q00F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q01T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q01F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q02T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q02F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q03T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q03F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q10T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q10F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q11T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q11F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q12T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q12F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q13T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q13F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q99T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q99F
 FROM   (SELECT *
        FROM   nb3sysopgroup
        WHERE  ::WHERE::) G
       LEFT JOIN nb3sysop N
              ON G.adopgroup = N.adopgroup
       LEFT JOIN (SELECT *
                  FROM   admmonthreport
                  WHERE  adusertype = '0'
                         AND cmyyyymm >= :StartDt
                         AND cmyyyymm <= :EndDt
                         AND ::LOGINTYPE:: AND adtxcode IN ('0','1','2','3') AND adtxstatus IN ('0','1')) R
              ON N.adopid = R.adopid
 GROUP  BY G.adopgroup,
          G.adopgroupname
 UNION ALL
 SELECT G.adopgroup,
       G.adopgroupname,
       -1                 AS ADUSERTYPE,
       1                  AS QTYTYPE,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q00T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q00F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q01T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q01F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q02T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q02F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q03T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q03F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q10T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q10F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q11T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q11F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q12T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q12F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q13T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q13F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '0' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q99T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '1' ) THEN R.adcount
                   ELSE 0
                 END), 0) AS Q99F
 FROM   (SELECT *
        FROM   nb3sysopgroup
        WHERE  ::WHERE::) G
       LEFT JOIN nb3sysop N
              ON G.adopgroup = N.adopgroup
       LEFT JOIN (SELECT *
                  FROM   admmonthreport
                  WHERE  1 = 1
                         AND cmyyyymm >= :StartDt
                         AND cmyyyymm <= :EndDt
                         AND ::LOGINTYPE:: AND adtxcode IN ('0','1','2','3') AND adtxstatus IN ('0','1')) R
              ON N.adopid = R.adopid
 GROUP  BY G.adopgroup,
          G.adopgroupname
 UNION ALL
 SELECT G.adopgroup,
       G.adopgroupname,
       1                  AS ADUSERTYPE,
       2                  AS QTYTYPE,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q00T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q00F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q01T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q01F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q02T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q02F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q03T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q03F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q10T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q10F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q11T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q11F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q12T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q12F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q13T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q13F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q99T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q99F
 FROM   (SELECT *
        FROM   nb3sysopgroup
        WHERE  ::WHERE::) G
       LEFT JOIN nb3sysop N
              ON G.adopgroup = N.adopgroup
       LEFT JOIN (SELECT *
                  FROM   admmonthreport
                  WHERE  adusertype = '1'
                         AND cmyyyymm >= :StartDt
                         AND cmyyyymm <= :EndDt
                         AND ::LOGINTYPE:: AND adtxcode IN ('0','1','2','3') AND adtxstatus IN ('0','1')) R
              ON N.adopid = R.adopid
 GROUP  BY G.adopgroup,
          G.adopgroupname
 UNION ALL
 SELECT G.adopgroup,
       G.adopgroupname,
       0                  AS ADUSERTYPE,
       2                  AS QTYTYPE,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q00T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q00F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q01T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q01F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q02T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q02F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q03T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q03F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q10T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q10F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q11T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q11F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q12T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q12F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q13T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q13F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q99T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q99F
 FROM   (SELECT *
        FROM   nb3sysopgroup
        WHERE  ::WHERE::) G
       LEFT JOIN nb3sysop N
              ON G.adopgroup = N.adopgroup
       LEFT JOIN (SELECT *
                  FROM   admmonthreport
                  WHERE  adusertype = '0'
                         AND cmyyyymm >= :StartDt
                         AND cmyyyymm <= :EndDt
                         AND ::LOGINTYPE:: AND adtxcode IN ('0','1','2','3') AND adtxstatus IN ('0','1')) R
              ON N.adopid = R.adopid
 GROUP  BY G.adopgroup,
          G.adopgroupname
 UNION ALL
 SELECT G.adopgroup,
       G.adopgroupname,
       -1                 AS ADUSERTYPE,
       2                  AS QTYTYPE,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q00T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q00F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q01T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q01F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q02T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q02F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q03T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '0'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q03F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q10T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '0'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q10F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q11T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '1'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q11F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q12T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND R.adtxcode = '2'
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q12F,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q13T,
       VALUE(Sum(CASE
                   WHEN ( R.adbkflag = '1'
                          AND (R.adtxcode = '3' OR R.adtxcode = '4')
                          AND R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q13F,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '0' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q99T,
       VALUE(Sum(CASE
                   WHEN ( R.adtxstatus = '1' ) THEN R.adamount
                   ELSE 0
                 END), 0) AS Q99F
 FROM   (SELECT *
        FROM   nb3sysopgroup
        WHERE  ::WHERE::) G
       LEFT JOIN nb3sysop N
              ON G.adopgroup = N.adopgroup
       LEFT JOIN (SELECT *
                  FROM   admmonthreport
                  WHERE  1 = 1
                         AND cmyyyymm >= :StartDt
                         AND cmyyyymm <= :EndDt
                         AND ::LOGINTYPE:: AND adtxcode IN ('0','1','2','3') AND adtxstatus IN ('0','1')) R
              ON N.adopid = R.adopid
 GROUP  BY G.adopgroup,
          G.adopgroupname
 ORDER  BY adopgroup,
          adusertype desc,
          qtytype 