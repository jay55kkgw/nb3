SELECT N.ADOPID,N.ADOPNAME  	
	,0 as ADAGREEF                                                                                        
	,sum(CASE WHEN (S.ADLEVEL='00') THEN S.ADCOUNT ELSE 0 END) AS R00                                                       
	,sum(CASE WHEN (S.ADLEVEL='01') THEN S.ADCOUNT ELSE 0 END) AS R01                                                       
	,sum(CASE WHEN (S.ADLEVEL='02') THEN S.ADCOUNT ELSE 0 END) AS R02                                                       
	,sum(CASE WHEN (S.ADLEVEL='03') THEN S.ADCOUNT ELSE 0 END) AS R03                                                       
	,sum(CASE WHEN (S.ADLEVEL='04') THEN S.ADCOUNT ELSE 0 END) AS R04                                                       
	,sum(CASE WHEN (S.ADLEVEL='05') THEN S.ADCOUNT ELSE 0 END) AS R05                                                       
	,sum(CASE WHEN (S.ADLEVEL='06') THEN S.ADCOUNT ELSE 0 END) AS R06                                                       
	,sum(CASE WHEN (S.ADLEVEL='07') THEN S.ADCOUNT ELSE 0 END) AS R07                                                       
	,sum(CASE WHEN (S.ADLEVEL='08') THEN S.ADCOUNT ELSE 0 END) AS R08                                                       
	,sum(CASE WHEN (S.ADLEVEL='09') THEN S.ADCOUNT ELSE 0 END) AS R09                                                       
	,sum(CASE WHEN (S.ADLEVEL='10') THEN S.ADCOUNT ELSE 0 END) AS R10                                                       
	,sum(CASE WHEN (S.ADLEVEL='11') THEN S.ADCOUNT ELSE 0 END) AS R11                                                       
	,sum(CASE WHEN (S.ADLEVEL='12') THEN S.ADCOUNT ELSE 0 END) AS R12                                                       
	,sum(CASE WHEN (S.ADLEVEL='13') THEN S.ADCOUNT ELSE 0 END) AS R13                                                                                                                                                        
FROM (
	SELECT * from NB3SYSOP WHERE ADGPPARENT='GPA' AND ADOPGROUP in ('GPA10','GPA30')) N LEFT JOIN 
	( Select R.* from ADMMONTHREPORT R  WHERE R.ADAGREEF='0' AND R.ADTXSTATUS='0' AND R.CMYYYYMM>=:SDate AND R.CMYYYYMM <=:EDate ) S           
    ON S.ADOPID = N.ADOPID                                                                         
WHERE 1=1                                                                                          
	Group By N.ADOPID,N.ADOPNAME                                                                                              
Union ALL                                                                                          
SELECT N.ADOPID,N.ADOPNAME                                                                         
	,1 as ADAGREEF                                                                                        
	,sum(CASE WHEN (S.ADLEVEL='00') THEN S.ADCOUNT ELSE 0 END) AS R00                                                       
	,sum(CASE WHEN (S.ADLEVEL='01') THEN S.ADCOUNT ELSE 0 END) AS R01  
	,sum(CASE WHEN (S.ADLEVEL='02') THEN S.ADCOUNT ELSE 0 END) AS R02                                                       
	,sum(CASE WHEN (S.ADLEVEL='03') THEN S.ADCOUNT ELSE 0 END) AS R03                                                       
	,sum(CASE WHEN (S.ADLEVEL='04') THEN S.ADCOUNT ELSE 0 END) AS R04                                                       
	,sum(CASE WHEN (S.ADLEVEL='05') THEN S.ADCOUNT ELSE 0 END) AS R05                                                       
	,sum(CASE WHEN (S.ADLEVEL='06') THEN S.ADCOUNT ELSE 0 END) AS R06                                                       
	,sum(CASE WHEN (S.ADLEVEL='07') THEN S.ADCOUNT ELSE 0 END) AS R07                                                       
	,sum(CASE WHEN (S.ADLEVEL='08') THEN S.ADCOUNT ELSE 0 END) AS R08                                                       
	,sum(CASE WHEN (S.ADLEVEL='09') THEN S.ADCOUNT ELSE 0 END) AS R09                                                       
	,sum(CASE WHEN (S.ADLEVEL='10') THEN S.ADCOUNT ELSE 0 END) AS R10                                                       
	,sum(CASE WHEN (S.ADLEVEL='11') THEN S.ADCOUNT ELSE 0 END) AS R11                                                       
	,sum(CASE WHEN (S.ADLEVEL='12') THEN S.ADCOUNT ELSE 0 END) AS R12                                                       
	,sum(CASE WHEN (S.ADLEVEL='13') THEN S.ADCOUNT ELSE 0 END) AS R13                                                                                                                                                      
FROM (
	SELECT * from NB3SYSOP WHERE ADGPPARENT='GPA' AND ADOPGROUP in ('GPA10','GPA30')) N LEFT JOIN 
	( Select R.* from ADMMONTHREPORT R   WHERE R.ADAGREEF='1' AND R.ADTXSTATUS='0' AND R.CMYYYYMM>=:SDate AND R.CMYYYYMM <=:EDate ) S           
    ON S.ADOPID = N.ADOPID                                                                         
WHERE 1=1                                                                                          
	Group By N.ADOPID,N.ADOPNAME                                                                                                                                                            
Union ALL                                                                                          
SELECT N.ADOPID,N.ADOPNAME                                                                         
	,-1 as ADAGREEF                                                                                    
	,sum(CASE WHEN (S.ADLEVEL='00') THEN S.ADCOUNT ELSE 0 END) AS R00                                                       
	,sum(CASE WHEN (S.ADLEVEL='01') THEN S.ADCOUNT ELSE 0 END) AS R01                                                       
	,sum(CASE WHEN (S.ADLEVEL='02') THEN S.ADCOUNT ELSE 0 END) AS R02                                                       
	,sum(CASE WHEN (S.ADLEVEL='03') THEN S.ADCOUNT ELSE 0 END) AS R03                                                       
	,sum(CASE WHEN (S.ADLEVEL='04') THEN S.ADCOUNT ELSE 0 END) AS R04                                                       
	,sum(CASE WHEN (S.ADLEVEL='05') THEN S.ADCOUNT ELSE 0 END) AS R05                                                       
	,sum(CASE WHEN (S.ADLEVEL='06') THEN S.ADCOUNT ELSE 0 END) AS R06                                                       
	,sum(CASE WHEN (S.ADLEVEL='07') THEN S.ADCOUNT ELSE 0 END) AS R07                                                       
	,sum(CASE WHEN (S.ADLEVEL='08') THEN S.ADCOUNT ELSE 0 END) AS R08                                                       
	,sum(CASE WHEN (S.ADLEVEL='09') THEN S.ADCOUNT ELSE 0 END) AS R09                                                       
	,sum(CASE WHEN (S.ADLEVEL='10') THEN S.ADCOUNT ELSE 0 END) AS R10                                                       
	,sum(CASE WHEN (S.ADLEVEL='11') THEN S.ADCOUNT ELSE 0 END) AS R11                                                       
	,sum(CASE WHEN (S.ADLEVEL='12') THEN S.ADCOUNT ELSE 0 END) AS R12                                                       
	,sum(CASE WHEN (S.ADLEVEL='13') THEN S.ADCOUNT ELSE 0 END) AS R13                                                                                                                                                    
FROM  (
	SELECT * from NB3SYSOP WHERE ADGPPARENT='GPA' AND ADOPGROUP in ('GPA10','GPA30')) N LEFT JOIN 
	( Select R.* from ADMMONTHREPORT R  WHERE R.ADTXSTATUS='0' AND R.CMYYYYMM>=:SDate AND R.CMYYYYMM <=:EDate ) S                              
    ON S.ADOPID = N.ADOPID                                                                         
WHERE 1=1                                                                                          
	Group By N.ADOPID,N.ADOPNAME                                                                                               
Order by ADOPID,ADAGREEF Desc