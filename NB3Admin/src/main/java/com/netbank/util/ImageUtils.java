package com.netbank.util;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import javax.imageio.ImageIO;

import lombok.extern.slf4j.Slf4j;

/**
 * 影像處理工具程式
 * @author chien
 *
 */
@Slf4j
public class ImageUtils {
	public enum SCALE_BASE_ON {
		WIDTH, HEIGHT
	}
	
	/**
	 * 等比例縮放
	 * @param oriImageContent	原影像內容
	 * @param newSize			新尺寸
	 * @param scaleBaseOn		依寬或高做等比例縮放
	 * @param imgType			影像類型
	 * @return					新影像內容
	 * @throws IOException
	 */
	public static byte[] scale(byte[] oriImageContent, double newSize, SCALE_BASE_ON scaleBaseOn, String imgType) throws IOException {
		// byte array to BufferedImage
		InputStream in = new ByteArrayInputStream(oriImageContent);
		BufferedImage src = ImageIO.read(in);
		
		if ( (scaleBaseOn == SCALE_BASE_ON.WIDTH && newSize ==(double)src.getWidth() ) ||  
			(scaleBaseOn == SCALE_BASE_ON.HEIGHT && newSize ==(double)src.getHeight() )) {
			byte[] bytTmps = new byte[oriImageContent.length];
			System.arraycopy(oriImageContent, 0, bytTmps, 0, oriImageContent.length);

			log.debug("new size equal oriImageContent.size");
			
			return bytTmps;
		}

		double scale = scaleBaseOn == SCALE_BASE_ON.WIDTH ?  newSize/(double)src.getWidth() : newSize/(double)src.getHeight() ;
		
		BufferedImage result = new BufferedImage((int) (src.getWidth() * scale), (int) (src.getHeight() * scale), src.getType());

        Graphics2D g2d = result.createGraphics();
        g2d.drawImage(src, 0, 0, result.getWidth(), result.getHeight(), null);
        g2d.dispose();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(result, imgType, baos );
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();
        
		return imageInByte;
	}
	
	/**
	 * 影像做上下顛倒
	 * @param oriImageContent	原影像內容
	 * @param imgType			影像類型
	 * @return					新影像內容
	 * @throws IOException
	 */
	public static byte[] flipImage(byte[] oriImageContent, String imgType) throws IOException {
		InputStream in = new ByteArrayInputStream(oriImageContent);
		BufferedImage bi = ImageIO.read(in);
        BufferedImage flipped = new BufferedImage(
                bi.getWidth(),
                bi.getHeight(),
                bi.getType());
        AffineTransform tran = AffineTransform.getTranslateInstance(0, bi.getHeight());
        AffineTransform flip = AffineTransform.getScaleInstance(1d, -1d);
        tran.concatenate(flip);

        Graphics2D g = flipped.createGraphics();
        g.setTransform(tran);
        g.drawImage(bi, 0, 0, null);
        g.dispose();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(flipped, imgType, baos );
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();
        
		return imageInByte;
    }
	
	public static void main(String[] args) throws IOException {
		/*BufferedImage bImage = ImageIO.read(new File("C:\\Users\\chien\\Pictures\\IMG_4809.jpg"));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write(bImage, "jpg", bos );
		byte [] data = bos.toByteArray();
	  
		byte[] output = ImageUtils.scale(data, 741, SCALE_BASE_ON.WIDTH, "jpg");
	  
		
		OutputStream os1 = new FileOutputStream(new File("C:\\Users\\chien\\Pictures\\IMG_4809_1.jpg")); 
		os1.write(output); 
		os1.close(); 
		
		
		output = ImageUtils.flipImage(output, "jpg");
		OutputStream os2 = new FileOutputStream(new File("C:\\Users\\chien\\Pictures\\IMG_4809_2.jpg")); 
		os2.write(output); 
		os2.close(); */
	}
}
