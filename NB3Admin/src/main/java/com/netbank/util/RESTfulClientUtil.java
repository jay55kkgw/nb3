package com.netbank.util;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

//import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.ssl.TrustStrategy;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :電文傳送Util
 *
 */

@Slf4j
public class RESTfulClientUtil {
	
	
	
	/**
	 * 電文傳送 RESTful
	 * @param params
	 * @param url
	 * @return
	 */
	public static String send(Map params ,String url, Integer toTmraTimeout ) {
		String result = null;
		RestTemplate restTemplate = null;
		ClientHttpRequestFactory rf = null;
		try {
			log.trace("url>>{}",Sanitizer.logForgingStr(url));
			log.trace("params>>{}",Sanitizer.logForgingStr(params));
			if(!params.isEmpty()) {
				log.trace("RESTfulClient.params.toJson>>{}", Sanitizer.logForgingStr(params));
			}
//			restTemplate = setHttpConfig();
//			restTemplate = setHttpConfigI(toTmraTimeout);
			restTemplate = setHttpConfigII(toTmraTimeout);
			result = restTemplate.postForObject(url,  params ,String.class );
			log.trace("result>>{}",Sanitizer.logForgingStr(result));
			
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("",e);
		}
		
		
		return result;
		
	}
	
	

	public static RestTemplate setHttpConfig() {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = 55*1000;
		HttpComponentsClientHttpRequestFactory rf = new HttpComponentsClientHttpRequestFactory();
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		return new RestTemplate(rf);
		
	}
	
	public static RestTemplate setHttpConfigII(Integer toTmraTimeout) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = toTmraTimeout*1000;
		
		log.trace("setHttpConfigI..");
		
		HttpComponentsClientHttpRequestFactory rf =
				new HttpComponentsClientHttpRequestFactory();
		
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		log.trace("setHttpConfigI end..");
		return new RestTemplate(rf);
		
	}
}
