package com.netbank.util;

/**
 * <p>Version class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class Version implements Comparable<Version> {

    private String version;

    /**
     * <p>get.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public final String get() {
        return this.version;
    }

    /**
     * <p>Constructor for Version.</p>
     *
     * @param version a {@link java.lang.String} object.
     */
    public Version(String version) {
        if(version == null)
            throw new IllegalArgumentException("Version can not be null");
        if(!version.matches("[0-9]+(\\.[0-9]+)*"))
            throw new IllegalArgumentException("Invalid version format");
        this.version = version;
    }

    /** {@inheritDoc} */
    @Override public int compareTo(Version that) {
        if(that == null)
            return 1;
        String[] thisParts = this.get().split("\\.");
        String[] thatParts = that.get().split("\\.");
        int length = Math.max(thisParts.length, thatParts.length);
        for(int i = 0; i < length; i++) {
            int thisPart = i < thisParts.length ?
                    Integer.parseInt(thisParts[i]) : 0;
            int thatPart = i < thatParts.length ?
                    Integer.parseInt(thatParts[i]) : 0;
            if(thisPart < thatPart)
                return -1;
            if(thisPart > thatPart)
                return 1;
        }
        return 0;
    }

    /** {@inheritDoc} */
    @Override public boolean equals(Object that) {
        if(this == that)
            return true;
        if(that == null)
            return false;
        if(this.getClass() != that.getClass())
            return false;
        return this.compareTo((Version) that) == 0;
    }

}
