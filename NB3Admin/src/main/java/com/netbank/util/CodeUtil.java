package com.netbank.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * 工具程式
 */
@Slf4j
public class CodeUtil{
	
	static Gson gs = new Gson();
	
	
	public static String toJson(Object obj) {
		return gs.toJson(obj);
	}
	
	
	public static  <T> T fromJson(String json ,Class<T> to) {
		return gs.fromJson(json, to);
	}
	
	
	
//	public static void convert2BaseResult(BaseResult bs, Object obj) {
//		convert2BaseResult(bs, obj, null, null);
//	}
	
	public static <T> T objectCovert(Class<T> to , Object from  ) {
		String json = null;
		if(from == null) {
			log.error("  from Object is null");
			return null;
		}
		json = gs.toJson(from);
		T t =  gs.fromJson(json, to);
		
		return t;
	}
	
	
//	public static void convert2BaseResult(BaseResult bs, Object obj,@NonNull String msgCode ,@NonNull String message) {
//		String json = null;
//		try {
//			if (bs==null) {
//				log.warn("警告 BaseResult is null...");
//			}
//			if(StrUtils.isNotEmpty(msgCode)) {
//				bs.setMsgCode(msgCode);
//			}
//			if(StrUtils.isNotEmpty(message)) {
//				bs.setMessage(message);
//			}
//			
//			
//			json = gs.toJson(obj);
//			log.trace("convert2Baseresult.json>> {}" ,json);
//			log.trace("convert2Baseresult.jsontoMap>> {}" ,gs.fromJson(json, Map.class));
//			bs.setTBB_WS_Result(gs.fromJson(json, Map.class));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			log.error("convert2BaseResult.error>>",e);
//		}
//	}
	
	
	/**
	 * OBJECT轉成BYTE[]
	 */
	public static byte[] objectToByte(Object object){
		byte[] result = null;
		ObjectOutputStream objectOutputStream = null;
		
		try{
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(object);
			result = byteArrayOutputStream.toByteArray();
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
		if(objectOutputStream != null){
			try{
				objectOutputStream.close();
			}
			catch(Exception e){
				log.error(String.valueOf(e));
			}
		}
		return result;
	}
	/**
	 * BYTE[]轉成OBJECT
	 */
	public static Object byteToObject(byte[] byteArray){
		Object result = null;
		ByteArrayInputStream byteArrayInputStream = null;
		ObjectInputStream objectInputStream = null;
		
		try{
			byteArrayInputStream = new ByteArrayInputStream(byteArray);
			objectInputStream = new ObjectInputStream(byteArrayInputStream);
			result = objectInputStream.readObject();
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
		if(objectInputStream != null){
			try{
				objectInputStream.close();
			}
			catch(Exception e){
				log.error(String.valueOf(e));
			}
		}
		if(byteArrayInputStream != null){
			try{
				byteArrayInputStream.close();
			}
			catch(Exception e){
				log.error(String.valueOf(e));
			}
		}
		return result;
	}
	/**
	 * BYTE[]轉成HEX
	 */
	public static String ByteArrayToHexString(byte[] byteArray){
		StringBuilder stringBuilder = new StringBuilder();
		for(byte b: byteArray){
			stringBuilder.append(String.format("%02x",b&0xff));
		}
		return stringBuilder.toString();
    }
	/**
	 * HEX轉成BYTE[]
	 */
	public static byte[] hexStringToByteArray(String string){
		int length = string.length();
		byte[] data = new byte[length / 2];
		for(int i=0;i<length;i+=2){
			data[i / 2] = (byte)((Character.digit(string.charAt(i),16) << 4) + Character.digit(string.charAt(i+1),16));
        }
        return data;
    }
	/**
	 * 將兩個byte[]合併
	 */
	public static byte[] combineByte(byte[] byteOne,byte[] byteTwo){
		try{
			byte[] result = new byte[byteOne.length + byteTwo.length];

			System.arraycopy(byteOne,0,result,0,byteOne.length);
			System.arraycopy(byteTwo,0,result,byteOne.length,byteTwo.length);
			
	        return result;
		}
		catch(Exception e){
			log.error(String.valueOf(e));
			
			return null;
		}
	}
	
	

	/**
	 * 將XML轉成字串
	 */
	public static String marshalXML(Object source) {
		StringWriter stringWriter = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(source.getClass());
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.marshal(source, stringWriter);
		} catch (Exception e) {
			log.error("",e);
		}
		return stringWriter.toString();
	}

	

	/**
	 * 將字串轉成XML
	 */
	public static <T> T unmarshalXML(Object target, String xml) {
		try {
			JAXBContext context = JAXBContext.newInstance(target.getClass());
			Unmarshaller unmarshaller = context.createUnmarshaller();
			StringReader stringReader = new StringReader(xml);
			return (T) unmarshaller.unmarshal(stringReader);
		} catch (Exception e) {
			log.error("",e);
			return null;
		}
	}
	
	
	public static <T> T unmarshalXML(Class target, String xml) {
		try {
			
			
			
			JAXBContext context = JAXBContext.newInstance(target);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			StringReader stringReader = new StringReader(xml);
			return (T) unmarshaller.unmarshal(stringReader);
//			InputStream targetStream = new ByteArrayInputStream(xml.getBytes());
//			return (T) unmarshaller.unmarshal(targetStream);
			
		} catch (Exception e) {
			log.error("",e);
			return null;
		}
	}
	
	
	
	/**
	 * 組出上行電文的header  
	 * @param len 原始電文長度
	 * @return
	 */
	public static byte[] getTelHeader(Integer len) {
		byte[] header = new byte[4];
		log.trace("header.length>>{}",header.length) ;
		log.trace("(len+header.length)>>{}",(len+header.length)) ;
//		header[0] = 0x00;
//		header[1] = (byte)(len+header.length);
//		header[2] = 0x00;
//		header[3] = 0x00;
//		short a1 = 0;
		short a1 = (short)(len+header.length);
//		log.trace("a1>>{}",(a1 >> 8));
		log.trace("a2>>{}",(a1 >> 8));
		log.trace("a2_1>>{}",(a1 % 256));
		
		header[0] = (byte) (a1 >> 8);
		header[1] = (byte) (a1 % 256);
		header[2] = 0;
		header[3] = 0;
		return header;
		
	}
	
	public static String EBCDICtoBig5(byte[] ebcdic) {
		String s="";
		char[]  ebcdic_char = null;
		byte[]  big5 = null;
		try {
			ebcdic_char = new String(ebcdic,"CP937").toCharArray();
			big5 = new String(ebcdic_char).getBytes("Big5");
			s= new String(big5);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("",e);
		}
		
		return s;
		
	}
	
	/**
     *將上行電文byte array中的各byte值輸出於螢幕上
     *@param TITA 上行電文byte array
     */
    public static void printOutTITA(String txid, byte[] TITA){
    	StringBuffer buf = new StringBuffer("");
        try{
        	log.trace("-->[{}]TITA Data in ASCII.....",txid);
//        	log.trace("-->"+TITAForDB);
        	log.trace("-->TITA Byte Data sent to TMRA.....");
            
            int iValue;
            String hexValue;
            for (int i=0;i<TITA.length;i++){
                iValue = TITA[i];
                hexValue = Integer.toHexString(iValue).toUpperCase();
                if (hexValue.length() == 8){
                    hexValue = hexValue.substring(6);
                }
                buf.append(hexValue).append(" ");
            }

            log.trace("-->"+buf.toString());
        }
        catch(Exception e){
            //logger.debug(e.toString());
        	log.error("",e);
        }
    }
    /**
     *將所有收到的下行電文byte array輸出於螢幕上
     * TOTAs 所有收到的下行電文byte array
     */
    public void  printOutTOTA(String txid, byte[] TOTA)
    {
        log.trace("printOutTOTA-->[{}]TOTA Data in ASCII.....",txid);
        log.trace(" 0 1 2 3  4 5 6 7  8 9 A B  C D E F");
        log.trace("--------|--------|--------|--------|");
        log.trace(toHexString(TOTA));
        
    }
	
    /**
     * 將傳入的byte值轉為十六進位並將其轉成字元存於一StringBuffer物件中
     *@param b 被轉換的byte
     *@param buf 儲存轉換結果之StringBuffer物件
     */
    public static void byte2hex(byte b, StringBuffer buf) 
    {
         char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
         '9', 'A', 'B', 'C', 'D', 'E', 'F' };
         int high = ((b & 0xf0) >> 4);
         int low = (b & 0x0f);
         buf.append(hexChars[high]);
         buf.append(hexChars[low]);
    }
    
    
    public static String toHexString(byte[] block) 
    {
         StringBuffer buf = new StringBuffer("");
         int len = block.length;
         for (int i = 0; i < len; i++) {
              byte2hex(block[i], buf);
              if ((i+1)%16==0) buf.append("\n");
              else if ( (i+1)%4==0 ) {
                   buf.append(" ");
              }
         }
         return buf.toString();
    }
	
}