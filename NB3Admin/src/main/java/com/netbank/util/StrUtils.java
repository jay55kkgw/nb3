package com.netbank.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * <p>StrUtils class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class StrUtils {
	/**
	 * <p>isEmpty.</p>
	 *
	 * @param s a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	public static boolean isEmpty(String s) {
		return s == null || s.length() == 0;
	}
	
	/**
	 * <p>isNotEmpty.</p>
	 *
	 * @param s a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	public static boolean isNotEmpty(String s) {
		return !(s == null || s.length() == 0);
	}
	
	/**
	 * <p>trim.</p>
	 *
	 * @param s a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String trim(String s) {
		if(isEmpty(s))
			return "";
		return s.trim();
	}
	
	/**
	 * <p>right.</p>
	 *
	 * @param str a {@link java.lang.String} object.
	 * @param len a int.
	 * @return a {@link java.lang.String} object.
	 */
	public static String right(String str, int len) {
		if(str.length() <= len)
			return str;
		return str.substring(str.length() - len);
	}
	
	/**
	 * <p>left.</p>
	 *
	 * @param str a {@link java.lang.String} object.
	 * @param len a int.
	 * @return a {@link java.lang.String} object.
	 */
	public static String left(String str, int len) {
		if(str.length() <= len)
			return str;
		return str.substring(0, len);
	}
	
	/**
	 * <p>repeat.</p>
	 *
	 * @param str a {@link java.lang.String} object.
	 * @param len a int.
	 * @return a {@link java.lang.String} object.
	 */
	public static String repeat(String str, int len) {
		StringBuffer sb = new StringBuffer();
		for(int i =0; i < len ; i++) {
			sb.append(str);
		}
		return sb.toString();
	}
	
	/**
	 * 若傳入的 v 為空字串或者為null, 則回傳 default value
	 *
	 * @param v a {@link java.lang.String} object.
	 * @param defaultValue a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String nvl(String v, String defaultValue) {
		return (isEmpty(v) ? defaultValue : v);
	}

	
	/**
	 * <p>splitToSet.</p>
	 *
	 * @param delim a {@link java.lang.String} object.
	 * @param value a {@link java.lang.String} object.
	 * @return a {@link java.util.Set} object.
	 */
	public static Set<String> splitToSet(String delim, String value) {
		return new HashSet(Arrays.asList(value.split(delim)));
	}
	
	/**
	 * 將 arys 加上 delim 變成 String
	 *
	 * @param delim a {@link java.lang.String} object.
	 * @param arys an array of {@link java.lang.String} objects.
	 * @return a {@link java.lang.String} object.
	 */
	public static String implode(String delim, String[] arys) {
		if(arys == null || arys.length == 0)
			return "";
		
		StringBuffer result = new StringBuffer();
		
		for(String x : arys) {
			if(result.length() != 0)
				result.append(delim);
			result.append(x);
		}
		
		return result.toString();
	}
	
	/**
	 * <p>implode.</p>
	 *
	 * @param delim a {@link java.lang.String} object.
	 * @param arys a {@link java.util.Set} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String implode(String delim, Set<String> arys) {
		if(arys == null || arys.size() == 0)
			return "";
		
		StringBuffer result = new StringBuffer();
		
		for(String x : arys) {
			if(result.length() != 0)
				result.append(delim);
			result.append(x);
		}
		
		return result.toString();
	}
	
	/**
	 * <p>implode.</p>
	 *
	 * @param delim a {@link java.lang.String} object.
	 * @param arys a {@link java.util.List} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String implode(String delim, List<String> arys) {
		if(arys == null || arys.size() == 0)
			return "";
		
		StringBuffer result = new StringBuffer();
		
		for(String x : arys) {
			if(result.length() != 0)
				result.append(delim);
			result.append(x);
		}
		
		return result.toString();
	}
	
    /**
     * 字串複製, 最長 4096 bytes
     */
//    public static void copyStream( InputStream in, OutputStream out ) throws IOException {
//        byte[] buf = new byte[4096];
//        int len;
//        while ( ( len = in.read( buf ) ) != -1 )
//            out.write( buf, 0, len );
//    }

    /**
     * 字串複製, 最長 4096 bytes
     */
//    public static void copyStream( Reader in, Writer out ) throws IOException {
//        char[] buf = new char[4096];
//        int len;
//        while ( ( len = in.read( buf ) ) != -1 )
//            out.write( buf, 0, len );
//    }

    /**
     * 字串複製, 最長 4096 bytes
     */
//    public static void copyStream( InputStream in, Writer out ) throws IOException {
//
//        byte[] buf1 = new byte[4096];
//        char[] buf2 = new char[4096];
//        int len, i;
//        while ( ( len = in.read( buf1 ) ) != -1 ) {
//            for ( i = 0; i < len; ++i )
//                buf2[i] = (char) buf1[i];
//            out.write( buf2, 0, len );
//        }
//    }

    /**
     * 字串複製, 最長 4096 bytes
     *
     * @param in a {@link java.io.Reader} object.
     * @param out a {@link java.io.OutputStream} object.
     * @throws java.io.IOException if any.
     */
    public static void copyStream( Reader in, OutputStream out ) throws IOException
    {
        char[] buf1 = new char[4096];
        byte[] buf2 = new byte[4096];
        int len, i;
        while ( ( len = in.read( buf1 ) ) != -1 ) {
            for ( i = 0; i < len; ++i )
                buf2[i] = (byte) buf1[i];
            out.write( buf2, 0, len );
        }
    }
    /**
     * <p>toHex.</p>
     *
     * @param b an array of {@link byte} objects.
     * @return a {@link java.lang.String} object.
     */
    public static String toHex(byte[] b){
        StringBuffer hex = new StringBuffer();
        for (int i=0; i<b.length; i++) {
        hex.append(""+"0123456789ABCDEF".charAt(0xf&b[i]>>4)+"0123456789ABCDEF".charAt(b[i]&0xf));
        }
        return hex.toString(); 
    }

    /**
     * <p>Hex2Bin.</p>
     *
     * @param hex an array of {@link byte} objects.
     * @return an array of {@link byte} objects.
     */
    public static byte[] Hex2Bin(byte[] hex) {
        byte[] bin = new byte[hex.length / 2];
        for (int i = 0, j = 0; i < bin.length; i++, j += 2) {
            int iL = hex[j] - '0';
            if (iL > 9) {
                iL -= 7;
            }
            iL <<= 4;
            //
            int iR = hex[j + 1] - '0';
            if (iR > 9) {
                iR -= 7;
            }
            bin[i] = (byte) (iL | iR);
        }
        return bin;
    }
    
    //遮蔽帳號：遮7、8、9碼(含信託帳號)
    /**
     * <p>hideaccount.</p>
     *
     * @param originalStr a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String hideaccount(String originalStr)
    {
     String header="";
     String middle="";
     String tailer="";
     String newstr="";
     if(originalStr.length()>0)
     {
   	  if(originalStr.length()==9)
   	  {	  
   		  header=originalStr.substring(0,6);
   		  middle="***";
   		  tailer="";	 	  
   	  }else if(originalStr.length()>9)
   	  {
   		  header=originalStr.substring(0,6);
   		  middle="***";
   		  tailer=originalStr.substring(9);	  
   	  }	  
     }
     newstr=header+middle+tailer;  
     return newstr;
    } 

}
