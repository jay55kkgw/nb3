package com.netbank.util;

import org.joda.time.DateTime;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * <p>DateUtils class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
public class DateUtils {
	static SimpleDateFormat dtF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	static SimpleDateFormat dtD = new SimpleDateFormat("yyyyMMdd");
	static SimpleDateFormat dtT = new SimpleDateFormat("HHmmss");
	
    /**
     * <p>beEnd.</p>
     *
     * @param d a {@link java.util.Date} object.
     * @return a {@link java.util.Date} object.
     */
    public static Date beEnd(Date d) {
        DateTime dt = new DateTime(d);
        dt = dt.plusMinutes(1);
        return dt.toDate();
    }

    /**
     * <p>beStart.</p>
     *
     * @param d a {@link java.util.Date} object.
     * @return a {@link java.util.Date} object.
     */
    public static Date beStart(Date d) {
        DateTime dt = new DateTime(d);
        return dt.toDate();
    }

    /**
     * <p>formatDate.</p>
     *
     * @param date a {@link java.util.Date} object.
     * @param pattern a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String formatDate(Date date, String pattern) {
        return formatDate(date, pattern, (TimeZone)null);
    }

    /**
     * <p>formatDate.</p>
     *
     * @param date a {@link java.util.Date} object.
     * @param pattern a {@link java.lang.String} object.
     * @param timeZone a {@link java.util.TimeZone} object.
     * @return a {@link java.lang.String} object.
     */
    public static String formatDate(Date date, String pattern, TimeZone timeZone) {
        return formatDate(date, pattern, timeZone, (Locale)null);
    }

    /**
     * <p>formatDate.</p>
     *
     * @param date a {@link java.util.Date} object.
     * @param pattern a {@link java.lang.String} object.
     * @param timeZone a {@link java.util.TimeZone} object.
     * @param locale a {@link java.util.Locale} object.
     * @return a {@link java.lang.String} object.
     */
    public static String formatDate(Date date, String pattern, TimeZone timeZone, Locale locale) {
        if (date == null) {
            return null;
        } else {
            SimpleDateFormat sdf;
            if (locale == null) {
                sdf = new SimpleDateFormat(pattern);
            } else {
                sdf = new SimpleDateFormat(pattern, locale);
            }

            if (timeZone != null) {
                sdf.setTimeZone(timeZone);
            }

            return sdf.format(date);
        }
    }

    /**
     * <p>parseDate.</p>
     *
     * @param date a {@link java.lang.String} object.
     * @param pattern a {@link java.lang.String} object.
     * @return a {@link java.util.Date} object.
     */
    public static Date parseDate(String date, String pattern) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.parse(date);
        } catch(Exception e) {
            throw new RuntimeException("parseDate Error. (date: " + date + ", pattern: " + pattern, e);
        }
    }
	/**
	 * 將 1080201 格式的日期, 變成 108/02/01
	 *
	 * @param dateShort a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String addSlash2(String dateShort) {
		return dateShort.substring(0, 3) + "/" + dateShort.substring(3, 5) + "/" + dateShort.substring(5, 7);
	}
	/**
	 * 回傳 0971011
	 *
	 * @param text a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getCDateShort(String text) {
		String result = text.substring(4);
		result = new Integer(text.substring(0, 4)) - 1911 + result;
		if(result.length() == 6)
			result = "0" + result;
		return result;
	}
    
    /**
     * 傳入日期字串，時間字串，回傳日期＋時間字串
     *
     * @param date              日期字串 YYYYMMDD
     * @param time              時間字串 HHMMSS
     * @param dateDelimiter     日期分隔號
     * @param timeDelimiter     時間分隔號
     * @param includeSS a boolean.
     * @return a {@link java.lang.String} object.
     */
    public static String getDateTimeString(String date, String dateDelimiter, String time, String timeDelimiter, boolean includeSS) {
        StringBuilder sb = new StringBuilder();
        sb.append(date.substring(0,4)).append(dateDelimiter).append(date.substring(4,6)).append(dateDelimiter).append(date.substring(6,8));
        sb.append(" ");
        sb.append(time.substring(0,2)).append(timeDelimiter).append(time.substring(2,4));
        
        if ( includeSS ) {
            sb.append(timeDelimiter).append(time.substring(4,6));
        }

        return sb.toString();
    }

    /**
     * 取得現在的日期及時間
     *
     * @return              日期＋時間陣列，Array[0]：yyyyMMdd；Array[1]：HHmmss
     */
    public static String[] getCurrentDateTimeParts() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        return dateFormat.format(date).split("-");
    }
    
    /**
	 * 取得2個日期間的天數
	 * 
	 * @param firstdate
	 * @param seconddate
	 * @return
	 */
	public static int getDiffTimeStamp(String firstdate, String seconddate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		long d1 = 0;
		long d2 = 0;
		BigDecimal diffdays = new BigDecimal(0);
		try {
			calendar.setTime(sdf.parse(firstdate));
			calendar2.setTime(sdf.parse(seconddate));
			d1 = calendar.getTimeInMillis();
			d2 = calendar2.getTimeInMillis();
			// diffdays = new BigDecimal((d1-d2)/(1000*60*60*24)).abs();
			diffdays = new BigDecimal((d1 - d2) / (1000)).abs();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
            //e.printStackTrace();
            log.error("",e);
		}
		return diffdays.intValue() + 1;

	}
	
    public static String getDateString(String date, String dateDelimiter) {
        StringBuilder sb = new StringBuilder();
        sb.append(date.substring(0,4)).append(dateDelimiter).append(date.substring(4,6)).append(dateDelimiter).append(date.substring(6,8));

        return sb.toString();
    }
    
}
