package com.netbank.domain.orm.core;

/**
 * <p>JPAUtils class.</p>
 *
 * @author jimmy
 * @version V1.0
 */
public class JPAUtils {
    /**
     * <p>replaceToJpaStyle.</p>
     *
     * @param hql a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String replaceToJpaStyle(String hql) {

        char[] chars = hql.toCharArray();
        StringBuilder sb = new StringBuilder();

        int index = 0;
        for(char c : chars) {
            if(c == '?') {
                sb.append(c).append(index + 1);
                index++;
            } else {
                sb.append(c);
            }
        }

        return sb.toString();

    }
}
