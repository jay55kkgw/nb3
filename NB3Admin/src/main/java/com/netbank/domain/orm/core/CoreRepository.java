package com.netbank.domain.orm.core;


import java.io.Serializable;
import java.util.List;

/**
 * <p>CoreRepository interface.</p>
 *
 * @author jimmy
 * @version V1.0
 */
public interface CoreRepository<T, ID extends Serializable> {
    /**
     * <p>count.</p>
     *
     * @return a long.
     */
    long count();

    /**
     * <p>findById.</p>
     *
     * @param var1 a ID object.
     * @return a T object.
     */
    T findById(ID var1);

    /**
     * <p>findAll.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<T> findAll();

    /**
     * <p>save.</p>
     *
     * @param var1 a T object.
     */
    void save(T var1);

    /**
     * <p>update.</p>
     *
     * @param var1 a T object.
     * @return a T object.
     */
    T update(T var1);

    /**
     * <p>delete.</p>
     *
     * @param var1 a T object.
     */
    void delete(T var1);
}
