package com.netbank.domain.orm.core;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>SessionAdapter class.</p>
 *
 * @author jimmy
 * @version V1.0
 */
@Slf4j
public class SessionAdapter {
    private EntityManager entityManager;
    /**
     * <p>Constructor for SessionAdapter.</p>
     *
     * @param manager a {@link javax.persistence.EntityManager} object.
     */
    public SessionAdapter(EntityManager manager) {
        this.entityManager = manager;
    }

    /**
     * <p>createQuery.</p>
     *
     * @param sql a {@link java.lang.String} object.
     * @return a {@link javax.persistence.Query} object.
     */
    public Query createQuery(String sql) {
        
        return new QueryAdapter(entityManager.createNativeQuery(sql));
    }

    /**
     * <p>createSQLQuery.</p>
     * 不帶參數的nativeQuery
     * @param sql a {@link java.lang.String} object.
     * @return a {@link javax.persistence.Query} object.
     */
    public Query createSQLQuery(String sql) {
        sql = JPAUtils.replaceToJpaStyle(sql);
        return new QueryAdapter(entityManager.createNativeQuery(sql));
    }
    
    
    /**
     * @param sql a {@link java.lang.String} object.
     * @param values
     * @return a {@link javax.persistence.Query} object.
     */
    public Query createSQLQuery(String sql, Object...values) {
        sql = JPAUtils.replaceToJpaStyle(sql);
        Query query = new QueryAdapter(entityManager.createNativeQuery(sql));
        int idx=0;
        for (int i = 0; i < values.length; i++) {
            if ( values[i] instanceof ArrayList ) {
                // 呼叫端傳入的是一個 List，所以 Object... 只會有一個，型別是 ArrayList
                ArrayList subValues = (ArrayList)values[i];
                for (int j=0; j<subValues.size(); j++ ) {
                    query.setParameter(idx++, subValues.get(j));
                }
            } else {
                // 呼叫端傳入的是一個個參數，所以 Object... 會是 int, string, float...
                query.setParameter(idx++, values[i]);
            }
        }
        return query;
    }

    /**
     * @param sql a {@link java.lang.String} object.
     * @param values
     * @return a {@link javax.persistence.Query} object.
     */
    public Query createSQLQueryResultConvert(String sql, Class resultClass, Object...values) {
        sql = JPAUtils.replaceToJpaStyle(sql);
        Query query = new QueryAdapter(entityManager.createNativeQuery(sql, resultClass));
        int idx=0;
        for (int i = 0; i < values.length; i++) {
            if ( values[i] instanceof ArrayList ) {
                // 呼叫端傳入的是一個 List，所以 Object... 只會有一個，型別是 ArrayList
                ArrayList subValues = (ArrayList)values[i];
                for (int j=0; j<subValues.size(); j++ ) {
                    query.setParameter(idx++, subValues.get(j));
                }
            } else {
                // 呼叫端傳入的是一個個參數，所以 Object... 會是 int, string, float...
                query.setParameter(idx++, values[i]);
            }
        }
        return query;
    }
}
