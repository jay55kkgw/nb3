package com.netbank.domain.orm.core;

import java.io.Reader;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Transient;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>Abstract LegacyJpaRepository class.</p>
 *
 * @author jimmy
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional(
        readOnly = true,
        rollbackFor = {Throwable.class}
)
public abstract class LegacyJpaRepository<T, ID extends Serializable> implements CoreRepository<T, ID> {
    //protected static final Logger LOG = LoggerFactory.getLogger(LegacyJpaRepository.class);
    @PersistenceContext
    private EntityManager entityManager;
    
    //要排除的基本資料類型及常用類型
    private static final List<String> baseClassList = Arrays.asList("byte","short","int","long","float","char","double","boolean","Date");
    /**
     * <p>Constructor for LegacyJpaRepository.</p>
     */
    public LegacyJpaRepository() {
    }

    /**
     * <p>Getter for the field <code>entityManager</code>.</p>
     *
     * @return a {@link javax.persistence.EntityManager} object.
     */
    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    /**
     * <p>count.</p>
     *
     * @return a long.
     */
    public long count() {
        Class<T> klazz = this.getParameterizedTypeClass();
        log.trace("Getting count for {}", klazz.getSimpleName());
        Query query = this.getEntityManager().createQuery("SELECT count(c) FROM " + klazz.getSimpleName() + " c");

        try {
            return (Long)query.getSingleResult();
        } catch (NoResultException var4) {
            return 0L;
        }
    }

    /**
     * <p>findAll.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<T> findAll() {
        Class<T> klazz = this.getParameterizedTypeClass();
        log.trace("Search for all {}", klazz.getSimpleName());
        String className = klazz.getSimpleName();
        className = (String)Sanitizer.sanitize(className);
        Query query = new QueryAdapter(this.getEntityManager().createQuery("SELECT c FROM " + className + " c"));

        List<T> results = query.getResultList();
        if (null == results) {
            results = new ArrayList();
        }
        //20191119-Danny-Stored Boundary Violation\路徑 1:
        results = (List<T>)Sanitizer.validList(results);
        log.trace("Found {} {}", ((List)results).size(), klazz.getSimpleName());
        return (List)results;
    }

    /**
     * <p>findById.</p>
     *
     * @param id a ID object.
     * @return a T object.
     */
    public T findById(ID id) {
        Class<T> klazz = this.getParameterizedTypeClass();
        //Sanitizer.logForging4Class(id);
        String className = klazz.getSimpleName();
        //20191115 seteven Log Forging\路徑 16:
        //20191118 Danny 調整id不可強轉String
        log.trace("Search for a {} with id {}", Sanitizer.logForgingStr(className), Sanitizer.logForgingStr(id));
        T o = this.getEntityManager().find(klazz, id);
        if (null != o) {
            log.trace("Found a {} to id {}", Sanitizer.logForgingStr(className), Sanitizer.logForgingStr(id));
        } else {
            log.trace("Didn't find any {} to id {}", Sanitizer.logForgingStr(className), Sanitizer.logForgingStr(id));
        }

        return o;
    }

    /**
     * <p>get.</p>
     *
     * @param id a ID object.
     * @return a T object.
     */
    public T get(ID id) {
        return this.findById(id);
    }

    /**
     * <p>save.</p>
     *
     * @param entity a T object.
     */
    public void save(T entity) {
        this.getEntityManager().persist(entity);
        this.getEntityManager().flush();
    }

    /**
     * <p>update.</p>
     *
     * @param entity a T object.
     * @return a T object.
     */
    public T update(T entity) {
        entity = this.getEntityManager().merge(entity);
        this.getEntityManager().flush();
        return entity;
    }

    /**
     * <p>delete.</p>
     *
     * @param entity a T object.
     */
    public void delete(T entity) {       
        //T t = this.getEntityManager().merge(entity);
        this.getEntityManager().remove(entity);
        this.getEntityManager().flush();
    }

    /**
     * <p>removeById.</p>
     *
     * @param id a ID object.
     */
    public void removeById(ID id) {
        this.delete(findById(id));
    }

    private Class<T> getParameterizedTypeClass() {
        return (Class)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * 創建Query對像.
     * 對於需要first,max,fetchsize,cache,cacheRegion等諸多設置的函數,可以返回Query後自行設置.
     * 留意可以連續設置,如 dao.getQuery(hql).setMaxResult(100).setCacheable(true).list();
     *
     * @param values 可變參數
     *               用戶可以如下四種方式使用
     *               dao.getQuery(hql)
     *               dao.getQuery(hql,arg0);
     *               dao.getQuery(hql,arg0,arg1);
     *               dao.getQuery(hql,new Object[arg0,arg1,arg2])
     * @param hql a {@link java.lang.String} object.
     * @return a {@link javax.persistence.Query} object.
     */
    public Query getQuery(String hql, Object... values) {
        hql = JPAUtils.replaceToJpaStyle(hql);
        Query query = new QueryAdapter(entityManager.createQuery(hql));
        int idx=0;
        for (int i = 0; i < values.length; i++) {
            if ( values[i] instanceof ArrayList ) {
                // 呼叫端傳入的是一個 List，所以 Object... 只會有一個，型別是 ArrayList
                ArrayList subValues = (ArrayList)values[i];
                for (int j=0; j<subValues.size(); j++ ) {
                    query.setParameter(idx++, subValues.get(j));
                }
            } else {
                // 呼叫端傳入的是一個個參數，所以 Object... 會是 int, string, float...
                query.setParameter(idx++, values[i]);
            }
        }
        return query;
    }


    /**
     * 創建Criteria對像
     *
     * @param criteriaFunction 可指定查詢條件
     * @param entityClass a {@link java.lang.Class} object.
     * @return a {@link javax.persistence.Query} object.
     */
    public Query getCriteriaQuery(Class<T> entityClass, BiFunction< Root, CriteriaBuilder, Predicate>... criteriaFunction) {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery(entityClass);
        Root entityRoot = query.from(entityClass);
        List<Predicate> filterPredicates = new ArrayList<>();
        for(BiFunction< Root, CriteriaBuilder, Predicate> func : criteriaFunction) {
            Predicate a = func.apply(entityRoot, cb);

            filterPredicates.add(a);
        }

        Predicate[] ary =filterPredicates.toArray(new Predicate[]{});
        query.where(cb.and(ary));

        return new QueryAdapter(entityManager.createQuery(query));
    }



    /**
     * hql查詢.
     *
     * @param values 可變參數
     *               用戶可以如下四種方式使用
     *               dao.find(hql)
     *               dao.find(hql,arg0);
     *               dao.find(hql,arg0,arg1);
     *               dao.find(hql,new Object[arg0,arg1,arg2])
     * @param hql a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List find(String hql, Object... values) {
        Object[] sValues = (Object[])Sanitizer.arraySanitize(values);

        Query query = getQuery(hql, sValues);
        //20191119-Danny-CGI Stored XSS\路徑 1:
        List list = query.getResultList();        
        list = Sanitizer.validList(list);

        return list;
    }

    /**
     * 根據屬性名和屬性值查詢對像.
     *
     * @return 符合條件的對象列表
     * @param entityClass a {@link java.lang.Class} object.
     * @param name a {@link java.lang.String} object.
     * @param value a {@link java.lang.Object} object.
     */
    public List<T> findBy(Class<T> entityClass, String name, Object value) {

        Object sValue = Sanitizer.sanitize(value);
        return getCriteriaQuery(entityClass, (root, cb) -> cb.equal(root.get(name), sValue)).getResultList();
    }

    /**
     * 根據屬性名和屬性值查詢唯一對像.
     *
     * @return 符合條件的唯一對像
     * @param entityClass a {@link java.lang.Class} object.
     * @param name a {@link java.lang.String} object.
     * @param value a {@link java.lang.Object} object.
     */
    public T findUniqueBy(Class<T> entityClass, String name, Object value) {
        Object sValue = Sanitizer.sanitize(value);
        return (T) getCriteriaQuery(entityClass, (root, cb) -> cb.equal(root.get(name), sValue)).getSingleResult();
    }

    /**
     * 根據屬性名和屬性值以Like AnyWhere方式查詢對像.
     *
     * @param entityClass a {@link java.lang.Class} object.
     * @param name a {@link java.lang.String} object.
     * @param value a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<T> findByLike(Class<T> entityClass, String name, String value) {
        String sName = (String)Sanitizer.sanitize(name);
        String sValue = (String)Sanitizer.sanitize(value);
        
        return getCriteriaQuery(entityClass, (root, cb) -> cb.like(root.get(sName), "%" + (sValue == null ? "" : sValue) + "%")).getResultList();
    }


    /**
     * <p>getSession.</p>
     *
     * @return a {@link com.netbank.domain.orm.core.SessionAdapter} object.
     */
    public SessionAdapter getSession() {
        return new SessionAdapter(this.getEntityManager());
    }


    /**
     * 分頁查詢函數，使用hql.
     *
     * @param pageNo 頁號,從0開始.
     * @param hql a {@link java.lang.String} object.
     * @param pageSize a int.
     * @param values a {@link java.lang.Object} object.
     * @return a {@link com.netbank.domain.orm.core.Page} object.
     */
    public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
        //Count查詢
        String shql = removeSelect(removeOrders(hql));
        String countQueryString = " select count (*) " + shql;
        List countlist = getQuery(countQueryString, values).getResultList();
        long totalCount = (Long) countlist.get(0);

        if (totalCount < 1) return new Page();
        //實際查詢返回分頁對像
        int startIndex = Page.getStartOfPage(pageNo, pageSize);
        //TODO test log
        log.info("Query Start...");
        Query query = getQuery(hql, values);
        List list = query.setFirstResult(startIndex).setMaxResults(pageSize).getResultList();
        //TODO test log
        log.info("Query End Stored XSS Start...");
        //20191118-Danny-Stored XSS\路徑 5:
        list = Sanitizer.validList(list);
        //TODO test log
        log.info("Stored XSS End...");
        return new Page(startIndex, totalCount, pageSize, list);
    }
    
    /**
     * 分頁查詢函數，使用nativeQuery.
     * @author JinHan
     * @param pageNo 頁號,從0開始.
     * @param sql a {@link java.lang.String} object.
     * @param pageSize a int.
     * @param params a {@link java.lang.Object} object.
     * @return a {@link com.netbank.domain.orm.core.Page} object.
     */
    public Page pagedQuery_native(String hql, int pageNo, int pageSize, List<Object> params) {
    	try{
    		log.info("pagedQuery_native start...");
    		//取得子Dao寫入的PO
        	Class<T> T= getGenericType();
        	//Count查詢
            String shql = removeSelect(removeOrders(hql));
            String countQueryString = " select count (*) " + shql;
            List countlist =  getSession().createSQLQuery(countQueryString, params).getResultList();
            long totalCount = Long.parseLong(new String().valueOf(countlist.get(0)));
            log.info("countQueryString>>>{}",countQueryString);
            log.info("totalCount>>>{}",totalCount);
            if (totalCount < 1) return new Page();
            //實際查詢返回分頁對像
            int startIndex = Page.getStartOfPage(pageNo, pageSize);
            
            //TODO test log
            log.info("Query Start...");
            //生成 SQL 避免DB column順序與po不符或者後來DB追加column造成錯誤
            String sqlColumn = "*";
            StringBuilder sb = new StringBuilder();
           
            List<String> columnNameList = getColumnClassByPOJO(T, false, null);
            for(int i = 0 ; i<columnNameList.size() ; i++) {
            	String columnName = columnNameList.get(i);
            	if(i != columnNameList.size() -1) {
            		sb.append(columnName+",");
            	}else {
            		sb.append(columnName);
            	}
            }
            if(sb.length()>0) {
            	sqlColumn = sb.toString();
            }
            sqlColumn = sb.toString();
            log.info("sqlColumn>>>>{}",sqlColumn);
            countQueryString = "select " +sqlColumn+" "+ hql;
            log.debug("\r\n[SQL:]\r\n" + countQueryString);
            
            Query query = getSession().createSQLQueryResultConvert(countQueryString, T, params).setFirstResult(startIndex).setMaxResults(pageSize);
            List list= query.getResultList();
            //TODO test log
            if(list == null) {            	
            	log.info("list is null");
            	return new Page();
            }else {
            	log.info("list.size="+list.size());
            }
            
            log.info("Query End Stored XSS Start...");
            //20191118-Danny-Stored XSS\路徑 5:
            list = Sanitizer.validList(list);
            //TODO test log
            log.info("Stored XSS End...");
            log.info("pagedQuery_native End...");
            return new Page(startIndex, totalCount, pageSize, list);
    	}catch(Exception e){
    		log.error("pagedQuery_native error >>>{}",e);
    		return new Page();
    	}
    }
    
    /**
     * 分頁查詢函數，使用nativeQuery.
     * @author JinHan
     * @param pageNo 頁號,從0開始.
     * @param sql a {@link java.lang.String} object.
     * @param pageSize a int.
     * @param params a {@link java.lang.Object} object.
     * @return a {@link com.netbank.domain.orm.core.Page} object.
     */
    public Page pagedQuery_native_column(String hql, int pageNo, int pageSize, List<Object> params, String column) {
    	try{
    		log.info("pagedQuery_native start...");
    		//取得子Dao寫入的PO
    		Class<T> T = null;
 	
    		T= getGenericType();

        	//Count查詢
            String shql = removeSelect(removeOrders(hql));
            String countQueryString = " select count (*) " + shql;
            List countlist =  getSession().createSQLQuery(countQueryString, params).getResultList();
            long totalCount = Long.parseLong(new String().valueOf(countlist.get(0)));
            log.info("countQueryString>>>{}",countQueryString);
            log.info("totalCount>>>{}",totalCount);
            if (totalCount < 1) return new Page();
            //實際查詢返回分頁對像
            int startIndex = Page.getStartOfPage(pageNo, pageSize);
            
            //TODO test log
            log.info("Query Start...");
            
            log.info("sqlColumn>>>>{}",column);
            countQueryString = "select " +column+" "+ hql;
            log.debug("\r\n[SQL:]\r\n" + countQueryString);
            
            Query query = getSession().createSQLQueryResultConvert(countQueryString, T, params).setFirstResult(startIndex).setMaxResults(pageSize);
            List list= query.getResultList();
            //TODO test log
            if(list == null) {            	
            	log.info("list is null");
            	return new Page();
            }else {
            	log.info("list.size="+list.size());
            }
            
            log.info("Query End Stored XSS Start...");
            //20191118-Danny-Stored XSS\路徑 5:
            list = Sanitizer.validList(list);
            //TODO test log
            log.info("Stored XSS End...");
            log.info("pagedQuery_native End...");
            return new Page(startIndex, totalCount, pageSize, list);
    	}catch(Exception e){
    		log.error("pagedQuery_native error >>>{}",e);
    		return new Page();
    	}
    }
     
    /**
     * 取得寫入Dao所屬的PO.class
     * @return  Class<T>
     * */
    public Class<T> getGenericType() {
    	try {
    		//取出寫入Dao所設定的父類別泛型 ex:TxnLogDao extends LegacyJpaRepository<TXNLOG, String> 中的 第一個Class TXNLOG.class
    		ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
    		Type[] types = pt.getActualTypeArguments();
    		if(types.length == 0) {
    			log.error("Dao GenericType is null, please check Dao GenericType...");
    			return (Class<T>) Object.class;
    		}
    		return (Class)types[0];
    	}catch(Exception e) {
    		log.error("getGenericType error>>>>{}",e);
    		return (Class<T>) Object.class;
    	}
    }
    
    /**
     * 取得所有PO的屬性名稱及類型
     * 
     * @param Class<T> t POJO.class
     * @param boolean isClass 是否回傳List<Class>
     * @param List<String> beforList 是否需要設定columnNameList初始，內部迴圈用
     * @return List 按照 isClass 判斷要回傳 List<Class>或List<String>
     * **/
    
    public List getColumnClassByPOJO(Class<T> t, boolean isClass, List<String> beforList) {
		List<Class> columnClassList = new ArrayList<Class>();
		List<String> columnNameList = new ArrayList<String>();
		//判斷是否為內部迴圈
		if(beforList != null){
		    columnNameList = beforList;
		}
		String className = t.getSimpleName();
		
		//若為基本型別或者常用型別則回傳該類別
		if(Modifier.isFinal(t.getModifiers())){
		    return columnClassList;
		}
		if(Modifier.isInterface(t.getModifiers())){
		    return columnClassList;
		}
		
		if(baseClassList.contains(className)){
		    return columnClassList;
		}
		
		Field[] fs = t.getDeclaredFields();
		
		for(Field f: fs){
			//判斷該欄位是否有@Transient
			boolean notTransient  = f.getDeclaredAnnotation(Transient.class)==null?true:false;
			
		    List<Class> nextClass = this.getColumnClassByPOJO((Class<T>) f.getType() ,true, columnNameList);
		    
		    //判斷是否為複合鍵
		    if(nextClass.size()>0){
		        for(Class column: nextClass){
		            if(notTransient) {		            	
		            	columnClassList.add(column);
		            }
		        }
		    }else{
		        Type type = f.getGenericType();
		        className = type.getTypeName();
		        int modifiers = f.getModifiers();
		        //判斷修飾子是否為基本型態
		        boolean isColumn = isNormal(modifiers);
		        
		        if(!notTransient){
                    isColumn = false;
                }
		        if(isColumn){
		            columnClassList.add(f.getType());
		            columnNameList.add(f.getName());
		        }
		    }
		}
		
		if(isClass){
		    return columnClassList;
		}else{
		    return columnNameList;
		}
    }    
    
    /**
     * 判斷class的Modifer是否為interface或final
     * @param int modifier 修飾值
     * @return boolean 
     * */
    public boolean isNormal(int modifiers) {
    	boolean result = true;
    	 if(Modifier.isFinal(modifiers)){
    		 result = false;
	     }
	     if(Modifier.isInterface(modifiers)){
	        result = false;
	     }
    	return result;
    }
    
    /**
     * 分頁查詢函數(內容有HTML Code)，使用hql.
     *
     * @param pageNo 頁號,從0開始.
     * @param hql a {@link java.lang.String} object.
     * @param pageSize a int.
     * @param values a {@link java.lang.Object} object.
     * @return a {@link com.netbank.domain.orm.core.Page} object.
     */
    public Page pagedQueryContentHasHtml(String hql, int pageNo, int pageSize, Object... values) {
        //Count查詢
        String shql = removeSelect(removeOrders(hql));
        String countQueryString = " select count (*) " + shql;
        List countlist = getQuery(countQueryString, values).getResultList();
        long totalCount = (Long) countlist.get(0);

        if (totalCount < 1) return new Page();
        //實際查詢返回分頁對像
        int startIndex = Page.getStartOfPage(pageNo, pageSize);
        Query query = getQuery(hql, values);
        List list = query.setFirstResult(startIndex).setMaxResults(pageSize).getResultList();
        return new Page(startIndex, totalCount, pageSize, list);
    }


    /**
     * 去除hql的select 子句，未考慮union的情況,，用於pagedQuery.
     */
    private static String removeSelect(String hql) {
        Assert.hasText(hql);
        int beginPos = hql.toLowerCase().indexOf("from");
        //int endPos = hql.toLowerCase().indexOf("where");

        Assert.isTrue(beginPos != -1, " hql : " + hql + " must has a keyword 'from'");
        return hql.substring(beginPos);
    }

    /**
     * 去除hql的orderby 子句，用於pagedQuery.
     */
    private static String removeOrders(String hql) {
        Assert.hasText(hql);
        Pattern p = Pattern.compile("order\\s*by[\\w|\\W|\\s|\\S]*", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(hql);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, "");
        }
        m.appendTail(sb);
        return sb.toString();
    }
}
