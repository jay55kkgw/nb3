package com.netbank.domain.orm.core;

import javax.persistence.*;

import fstop.util.Sanitizer;

import java.util.*;

/**
 * <p>QueryAdapter class.</p>
 *
 * @author jimmy
 * @version V1.0
 */
public class QueryAdapter implements Query {
    private Query query;
    /**
     * <p>Constructor for QueryAdapter.</p>
     *
     * @param query a {@link javax.persistence.Query} object.
     */
    public QueryAdapter(Query query) {
        this.query = query;
    }


    /** {@inheritDoc} */
    @Override
    public List getResultList() {
        //20191118-Danny-Second Order SQL Injection\路徑 2:
        List results = query.getResultList();
        results = Sanitizer.validList(results);
        return results;
    }

    /** {@inheritDoc} */
    @Override
    public Object getSingleResult() {
        //20191118-Danny-Second Order SQL Injection\路徑 1:
        return Sanitizer.escapeHTML(query.getSingleResult());
    }

    /** {@inheritDoc} */
    @Override
    public int executeUpdate() {
        //20191118-Danny-Second Order SQL Injection\路徑 11
        return (int)Sanitizer.sanitize(query.executeUpdate());
    }

    /** {@inheritDoc} */
    @Override
    public Query setMaxResults(int i) {
        return query.setMaxResults(i);
    }

    /** {@inheritDoc} */
    @Override
    public int getMaxResults() {
        return query.getMaxResults();
    }

    /** {@inheritDoc} */
    @Override
    public Query setFirstResult(int i) {
        return query.setFirstResult(i);
    }

    /** {@inheritDoc} */
    @Override
    public int getFirstResult() {
        return query.getFirstResult();
    }

    /** {@inheritDoc} */
    @Override
    public Query setHint(String s, Object o) {
        return query.setHint(s, o);
    }

    /** {@inheritDoc} */
    @Override
    public Map<String, Object> getHints() {
        return query.getHints();
    }

    /** {@inheritDoc} */
    @Override
    public <T> Query setParameter(Parameter<T> parameter, T t) {
        return query.setParameter(parameter, t);
    }

    /** {@inheritDoc} */
    @Override
    public Query setParameter(Parameter<Calendar> parameter, Calendar calendar, TemporalType temporalType) {
        return query.setParameter(parameter, calendar, temporalType);
    }

    /** {@inheritDoc} */
    @Override
    public Query setParameter(Parameter<Date> parameter, Date date, TemporalType temporalType) {
        return query.setParameter(parameter, date, temporalType);
    }

    /** {@inheritDoc} */
    @Override
    public Query setParameter(String s, Object o) {
        return query.setParameter(s, o);
    }

    /** {@inheritDoc} */
    @Override
    public Query setParameter(String s, Calendar calendar, TemporalType temporalType) {
        return query.setParameter(s, calendar, temporalType);
    }

    /** {@inheritDoc} */
    @Override
    public Query setParameter(String s, Date date, TemporalType temporalType) {
        return query.setParameter(s, date, temporalType);
    }

    /** {@inheritDoc} */
    @Override
    public Query setParameter(int i, Object o) {
        return query.setParameter(i+1, o);
    }

    /** {@inheritDoc} */
    @Override
    public Query setParameter(int i, Calendar calendar, TemporalType temporalType) {
        return query.setParameter(i+1, calendar, temporalType);
    }

    /** {@inheritDoc} */
    @Override
    public Query setParameter(int i, Date date, TemporalType temporalType) {
        return query.setParameter(i+1, date, temporalType);
    }

    /** {@inheritDoc} */
    @Override
    public Set<Parameter<?>> getParameters() {
        return query.getParameters();
    }

    /** {@inheritDoc} */
    @Override
    public Parameter<?> getParameter(String s) {
        return query.getParameter(s);
    }

    /** {@inheritDoc} */
    @Override
    public <T> Parameter<T> getParameter(String s, Class<T> aClass) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public Parameter<?> getParameter(int i) {
        return query.getParameter(i + 1);
    }

    /** {@inheritDoc} */
    @Override
    public <T> Parameter<T> getParameter(int i, Class<T> aClass) {
        return query.getParameter(i + 1, aClass);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isBound(Parameter<?> parameter) {
        return query.isBound(parameter);
    }

    /** {@inheritDoc} */
    @Override
    public <T> T getParameterValue(Parameter<T> parameter) {
        return query.getParameterValue(parameter);
    }

    /** {@inheritDoc} */
    @Override
    public Object getParameterValue(String s) {
        return query.getParameter(s);
    }

    /** {@inheritDoc} */
    @Override
    public Object getParameterValue(int i) {
        return query.getParameter(i + 1);
    }

    /** {@inheritDoc} */
    @Override
    public Query setFlushMode(FlushModeType flushModeType) {
        return query.setFlushMode(flushModeType);
    }

    /** {@inheritDoc} */
    @Override
    public FlushModeType getFlushMode() {
        return query.getFlushMode();
    }

    /** {@inheritDoc} */
    @Override
    public Query setLockMode(LockModeType lockModeType) {
        return query.setLockMode(lockModeType);
    }

    /** {@inheritDoc} */
    @Override
    public LockModeType getLockMode() {
        return query.getLockMode();
    }

    /** {@inheritDoc} */
    @Override
    public <T> T unwrap(Class<T> aClass) {
        return query.unwrap(aClass);
    }
}
