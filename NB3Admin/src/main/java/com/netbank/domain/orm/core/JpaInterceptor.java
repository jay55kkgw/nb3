package com.netbank.domain.orm.core;

import org.hibernate.EmptyInterceptor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JpaInterceptor extends EmptyInterceptor {
	@Override
	public String onPrepareStatement(String sql) {
		String proxySql = sql.toUpperCase();
		if(proxySql.indexOf("FROM TXNLOG") != -1) {
			log.debug("onPrepareStatement sql>>>{}",sql);
			String isTxnlog = proxySql.substring(proxySql.indexOf("FROM TXNLOG"), proxySql.indexOf("FROM TXNLOG")+11);
			if("FROM TXNLOG".equals(isTxnlog)) {				
				sql = sql + " WITH UR ";			
			}
		}
		return sql;
	}
}
