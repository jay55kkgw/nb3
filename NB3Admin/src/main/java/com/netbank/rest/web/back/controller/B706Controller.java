package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import com.netbank.rest.web.back.model.B706Result;
import com.netbank.rest.web.back.service.NMBReportService;
import com.netbank.rest.web.back.service.PortalSSOService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.model.Rows;
import fstop.orm.po.SYSPARAMDATA;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;



@Controller
@Slf4j
@RequestMapping("/B706")
public class B706Controller {
    @Autowired
    private PortalSSOService portalSSOService;

    @Autowired
    private NMBReportService nmbReportService;

    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    	model.addAttribute("Date", dateFormat.format(new Date()));
        
        //若為OP角色才執行
        Set<String> Roles=portalSSOService.getLoginRoles();
        boolean isOP=false;
        
        for (String role:Roles)
        {
        	if(role.equals("OP"))
        	{
        		isOP=true;
        		break;
        	}
        }
        
        if (isOP==true)
        {
            try {
            	Map DataOP = nmbReportService.findResultReportOP();
                model.put("errorMsg", "");
                model.put("DataOP", DataOP);
            } catch (Exception e) {
                log.error("query error", e);
                //20191212-Danny-Information Exposure Through an Error Message
                String message = "發生錯誤，請再試一次!!";
                model.put("errorMsg", (String)Sanitizer.logForgingStr(message));
            }
        }

        return "B706/index";
    }

    @PostMapping(value = "/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody HashMap query(@RequestParam String startDate, @RequestParam String endDate) {
        HashMap model = new HashMap();
        	
        try {
            List<B706Result> reportData = nmbReportService.findScheduleTxnResultReport(startDate, endDate);
            model.put("errorMsg", "");
            model.put("reportData", reportData);
        } catch (Exception e) {
            log.error("query error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤，請再試一次!!";
            model.put("errorMsg", (String)Sanitizer.logForgingStr(message));
            return model;
        }
        return model;
    }

    @GetMapping(value = { "/Report" })
    @Authorize(userInRoleCanQuery = true)
    public void report(@RequestParam String startDate, @RequestParam String endDate, ModelMap model, HttpServletResponse response) throws IOException {
        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"B706Report.csv\"");
        
        OutputStream resOs = response.getOutputStream();
        resOs.write('\ufeef'); // emits 0xef
        resOs.write('\ufebb'); // emits 0xbb
        resOs.write('\ufebf'); // emits 0xbf

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = new CSVWriter(outputWriter, CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_ESCAPE_CHARACTER);
  
        String[] headers = { "日期", "預約成功筆數", "預約失敗筆數(操作因素)", "預約失敗筆數(系統因素)","預約失敗筆數(其它)", "預約失敗總筆數","預約未執行筆數", "預約總筆數" };
        writer.writeNext(headers);

        try {
            List<B706Result> reportData = nmbReportService.findScheduleTxnResultReport(startDate, endDate);
            for ( int i=0; i<reportData.size(); i++ ) {
                B706Result data = reportData.get(i);
                long succCnt = data.getSuccCnt();
                long failbyUserCnt = data.getUserReasonFailCnt();
                long failBySysCnt = data.getSystemReasonFailCnt();
                long failByOtherCnt = data.getOtherReasonFailCnt();
                long notExecuteCnt = data.getNotExecuteCnt();

                String[] nextLine = { data.getTxDay(), String.valueOf(succCnt), String.valueOf(failbyUserCnt),  String.valueOf(failBySysCnt),String.valueOf(failByOtherCnt), 
                                      String.valueOf(data.getFailCnt()), String.valueOf(notExecuteCnt),String.valueOf(data.getTotalCnt()) };

                writer.writeNext(nextLine);
            }
            writer.close();
        } catch (Exception e) {
            log.error("Error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤請再試一次!!";
            String[] nextLine = { (String)Sanitizer.logForgingStr(message), "", "", "", "", "" };
            writer.writeNext(nextLine);
            writer.close();
            return;
        }
    }
}