package com.netbank.rest.web.back.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.netbank.BackendApplication;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.service.ADMOPLogService;
import com.netbank.rest.web.back.service.BaseService;
import com.netbank.rest.web.back.service.SYSOPService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMOPLOG;
import fstop.orm.po.SYSOP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>使用者登入狀態查詢</p>
 * 
 * @author 簡哥
 */
@Controller
@Slf4j
@RequestMapping("/B301")
public class B301Controller {
    @Autowired
    private ADMOPLogService admOpLogService;

    @Autowired
    private SYSOPService sysOpService;

    /**
     * <p></p>
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        List<SYSOP> allMenus = sysOpService.getAllMenus();
        List<SYSOP> txMenus = allMenus.stream().filter(p->p.getURL().trim().length()>1).collect(Collectors.toList());
        model.addAttribute("menus", txMenus);
        return "B301/index";
    }

    /**
     * 分頁查詢
     * @param StartTime
     * @param EndTime
     * @param LoginUserId
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value="/IndexQuery", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse indexQuery(@RequestParam String startTime, @RequestParam String endTime, @RequestParam String loginUserId, @RequestParam String adOpId, HttpServletRequest request, HttpServletResponse response ) {
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admOpLogService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
            jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.parseDateTime("yyyy/MM/dd HH:mm",startTime), 
            (String)Sanitizer.parseDateTime("yyyy/MM/dd HH:mm", endTime), (String)Sanitizer.escapeHTML(loginUserId), (String)Sanitizer.escapeHTML(adOpId));

        List<ADMOPLOG> admOpLogs = (List<ADMOPLOG>) page.getResult();
        List<ADMOPLOG> sadmOpLogs = new ArrayList<ADMOPLOG>();

        // 交易代碼轉交易名稱
        List<SYSOP> menus = sysOpService.getAllMenus();
        for (ADMOPLOG admOpLog : admOpLogs) {
            ADMOPLOG sadmOpLog = new ADMOPLOG();
            Sanitizer.escape4Class(admOpLog, sadmOpLog);

            Optional<SYSOP> sysop = menus.stream().filter(p->p.getADOPID().equalsIgnoreCase(admOpLog.getADOPID())).findFirst();
            if ( sysop.isPresent() ) {
                sadmOpLog.setADOPID(sysop.get().getADOPNAME());
            }
            sadmOpLogs.add(sadmOpLog);
        }
        
        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), sadmOpLogs);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得 JsonData Dialog
     * @param id
     * @param SysOpId
     * @param model
     * @return
     */
    @PostMapping(value="/JsonData/{id}")
    public String jsonData(@PathVariable String id, ModelMap model) {
        ADMOPLOG log = admOpLogService.getById((String)Sanitizer.escapeHTML(id));
        Gson gson = new Gson();
            
        HashMap<String, Object> jsonBeforeData = (HashMap<String, Object>)gson.fromJson(log.getADBCHCON(), HashMap.class);
        HashMap<String, Object> jsonAfterData = (HashMap<String, Object>)gson.fromJson(log.getADACHCON(), HashMap.class);
        HashMap<String, List<Object>> tFormData = new HashMap<String, List<Object>>();
        
        Properties props = loadProperties();
        
        if ( log.getADOPITEM().equals(BaseService.CREATE) || log.getADOPITEM().equals(BaseService.SETUP)) {
            // 新增
            for (Map.Entry me : jsonAfterData.entrySet()) {
                String pKey = log.getADOPID()+"."+me.getKey();      // ex: B301.ADRMTTYPE
                String nkey = props.getProperty(pKey);
                if ( props.containsKey(pKey)) {
                    nkey = nkey+"["+me.getKey()+"]";
                } else {
                    nkey = (String)me.getKey();
                }
                List<Object> values = new ArrayList<Object>();
                values.add("");
                values.add(me.getValue());
                tFormData.put(nkey, values);
            }
        } else if ( log.getADOPITEM().equals(BaseService.UPDATE)) {
            // 修改
            for (Map.Entry me : jsonBeforeData.entrySet()) {
                String pKey = log.getADOPID()+"."+me.getKey();      // ex: B301.ADRMTTYPE
                String nkey = props.getProperty(pKey);
                if ( props.containsKey(pKey)) {
                    nkey = nkey+"["+me.getKey()+"]";
                } else {
                    nkey = (String)me.getKey();
                }
                List<Object> values = new ArrayList<Object>();
                values.add(me.getValue());
                values.add(jsonAfterData.get(me.getKey()));
                tFormData.put(nkey, values);
            }
        } else {
            // 刪除, 查詢, 送審, 覆核, 退回
            for (Map.Entry me : jsonBeforeData.entrySet()) {
                String pKey = log.getADOPID()+"."+me.getKey();      // ex: B301.ADRMTTYPE
                String nkey = props.getProperty(pKey);
                if ( props.containsKey(pKey)) {
                    nkey = nkey+"["+me.getKey()+"]";
                } else {
                    nkey = (String)me.getKey();
                }
                List<Object> values = new ArrayList<Object>();
                values.add(me.getValue());
                values.add("");
                tFormData.put(nkey, values);
            }
        }
        
        model.addAttribute("items", tFormData);
        return "B301/dataPartial";
    }
    
    // /**
    //  * 取得 FormData Dialog
    //  * @param id
    //  * @param SysOpId
    //  * @param model
    //  * @return
    //  */
    // @PostMapping(value="/FormData/{id}")
    // public String formData(@PathVariable String id, @RequestParam String SysOpId, ModelMap model) {
    //     USERACTIONLOG log = userActionLogService.getById(Integer.parseInt(id));
    //     Gson gson = new Gson();
    //     HashMap<String, Object> formData = (HashMap<String, Object>)gson.fromJson(log.getFORMDATA(), HashMap.class);
    //     HashMap<String, Object> tFormData = new HashMap<String, Object>();

    //     Properties props = loadProperties();
    //     for (Map.Entry me : formData.entrySet()) {
    //         String pKey = SysOpId+"."+me.getKey();      // ex: B301.ADRMTTYPE
    //         if ( props.containsKey(pKey)) {
    //             String nkey = props.getProperty(pKey);
    //             nkey = nkey+"["+me.getKey()+"]";
    //             tFormData.put(nkey, me.getValue());
    //         }
    //     }
    //     model.addAttribute("items", tFormData);
    //     return "B301/dataPartial";
    // }

    // /**
    //  * 取得 ParamData Dialog
    //  * @param id
    //  * @param SysOpId
    //  * @param model
    //  * @return
    //  */
    // @PostMapping(value="/ParamData/{id}")
    // public String paramData(@PathVariable String id, @RequestParam String SysOpId, ModelMap model) {
    //     USERACTIONLOG log = userActionLogService.getById(Integer.parseInt(id));
    //     Gson gson = new Gson();
    //     HashMap<String, Object> paramData = (HashMap<String, Object>)gson.fromJson(log.getPARAMDATA(), HashMap.class);
    //     HashMap<String, Object> tParamData = new HashMap<String, Object>();

    //     Properties props = loadProperties();
    //     for (Map.Entry me : paramData.entrySet()) {
    //         String pKey = SysOpId+"."+me.getKey();      // ex: B301.ADRMTTYPE
    //         if ( props.containsKey(pKey)) {
    //             String nkey = props.getProperty(pKey);
    //             nkey = nkey+"["+me.getKey()+"]";
    //             tParamData.put(nkey, me.getValue());
    //         }
    //     }
    //     model.addAttribute("items", tParamData);
    //     return "B301/dataPartial";
    // }

    /**
     * 載入欄位對應參數
     * @return
     */
    private Properties loadProperties() {
        Properties prop = new Properties();
        try (InputStream input = BackendApplication.class.getClassLoader().getResourceAsStream("fieldmapping.properties")) {

            if (input != null) {
                prop.load(input);
            }
        } catch (IOException ex) {
            //20191115-Eric-Information Exposure Through an Error Message\路徑 28:
            //ex.printStackTrace();
            log.error("loadProperties error", ex);
        }
        return prop;
    }
}