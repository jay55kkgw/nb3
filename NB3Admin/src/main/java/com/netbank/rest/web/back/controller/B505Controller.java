package com.netbank.rest.web.back.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ACT_MAINService;
import com.netbank.rest.web.back.service.ADMMRKTService;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import fstop.aop.Authorize;
import fstop.orm.po.ACT_MAIN;
import fstop.util.DateTimeUtils;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 推廣訊息維護
 *
 * @author jinhanhuang
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B505")
public class B505Controller {
    
    @Autowired
    private ACT_MAINService act_MainsService;


    /**
     * 取得查詢頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"", "/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        Date now = new Date();
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.MONTH, -6);
        Date before = cal.getTime();

        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat ("yyyy/MM/dd");
        
        model.addAttribute("StartDt", formatter.format(before));
        model.addAttribute("EndDt", formatter.format(now));
        

        return "B505/index";
    }

    /**
     * 查詢廣告(前端分頁)
     *
     * @param DateFrom  			起日
     * @param DateTo    			迄日
     * @param TITLE     			內容
     * @param FlowFinished			流程是否結案
     * @return          ADMADSTMP 	物件LIST
     */
    @PostMapping(value="/IndexQuery", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse indexQuery(@RequestParam String ACT_TYPE, @RequestParam String MAXCOUNT, @RequestParam String ACTDATE_FROM, @RequestParam String ACTDATE_TO, HttpServletRequest request,HttpServletResponse response) {

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        ACTDATE_FROM = DateTimeUtils.formatYYYYMMDD(request.getParameter("ACTDATE_FROM"));
        ACTDATE_TO = DateTimeUtils.formatYYYYMMDD(request.getParameter("ACTDATE_TO"));
        log.debug("indexQuery ACT_TYPE={}, ACTDATE_FROM={}, ACTDATE_TO={}", Sanitizer.logForgingStr(ACT_TYPE), Sanitizer.logForgingStr(ACTDATE_FROM), Sanitizer.logForgingStr(ACTDATE_TO));
        Page page = act_MainsService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(), jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(),
                                (String)Sanitizer.escapeHTML(ACT_TYPE), (String)Sanitizer.escapeHTML(MAXCOUNT), (String)Sanitizer.escapeHTML(ACTDATE_FROM), (String)Sanitizer.logForgingStr(ACTDATE_TO));

        List<ACT_MAIN> actList = (List<ACT_MAIN>)page.getResult();
        List<ACT_MAIN> sactList = new ArrayList<ACT_MAIN>();
        for(ACT_MAIN po : actList) {
        	ACT_MAIN newPo = new ACT_MAIN();
            Sanitizer.escape4Class(po, newPo);
            sactList.add(newPo);
        }
        
        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), sactList);
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得輸入頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"/Create"})
    @Authorize(userInRoleCanEdit = true)
    public String create(ModelMap model) {
        return "B505/create";
    }

    /**
     * 新增 行銷主檔
     * 
     * @param request
     * @return
    */
    @PostMapping(value="/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse create(@RequestParam String ACT_TYPE, @RequestParam String MAXCOUNT, @RequestParam String ACTDATE_FROM, @RequestParam String ACTDATE_TO, @RequestParam String ACT_MSG, HttpServletRequest request){
        JsonResponse response = new JsonResponse();
        log.info("create start ....");
        
        ACT_TYPE =(String)Sanitizer.escapeHTML(ACT_TYPE) ;
        MAXCOUNT =(String)Sanitizer.escapeHTML(MAXCOUNT) ;
        ACTDATE_FROM = (String)Sanitizer.escapeHTML(DateTimeUtils.formatYYYYMMDD(ACTDATE_FROM));
        ACTDATE_TO = (String)Sanitizer.escapeHTML(DateTimeUtils.formatYYYYMMDD(ACTDATE_TO));
        ACT_MSG = (String)Sanitizer.escapeHTML(ACT_MSG);

        ACT_MAIN po = new ACT_MAIN();

        po.setACT_TYPE(ACT_TYPE);
        po.setMAXCOUNT(MAXCOUNT);
        po.setACTDATE_FROM(ACTDATE_FROM);
        po.setACTDATE_TO(ACTDATE_TO);
        po.setACT_MSG(ACT_MSG);

        Map<String, String> errors = act_MainsService.saveACT(po);
        
        if(errors.containsKey("summary")){
            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        response.setValidated(true);
        return response;
    }

    /**
     * 取得編輯頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"/Edit/{ACT_TYPE}"})
    @Authorize(userInRoleCanEdit = true)
    public String create(@PathVariable String ACT_TYPE,ModelMap model) {

        ACT_TYPE = (String)Sanitizer.escapeHTML(ACT_TYPE);

        ACT_MAIN po = act_MainsService.getById(ACT_TYPE);
        
        String ACTDATE_FROM = po.getACTDATE_FROM();
        String ACTDATE_TO = po.getACTDATE_TO();
        
        ACTDATE_FROM = ACTDATE_FROM.substring(0, 4)+"/"+ACTDATE_FROM.substring(4, 6)+"/"+ACTDATE_FROM.substring(6, 8);
        ACTDATE_TO = ACTDATE_TO.substring(0, 4)+"/"+ACTDATE_TO.substring(4, 6)+"/"+ACTDATE_TO.substring(6, 8);
        
        model.addAttribute("ACT_TYPE", ACT_TYPE);
        model.addAttribute("MAXCOUNT", po.getMAXCOUNT());
        model.addAttribute("ACTDATE_FROM", ACTDATE_FROM);
        model.addAttribute("ACTDATE_TO", ACTDATE_TO);
        model.addAttribute("ACT_MSG", po.getACT_MSG());

        return "B505/edit";
    }


    /**
     * 編輯 行銷主檔
     * 
     * @param request
     * @return
    */
    @PostMapping(value="/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse edit(@RequestParam String ACT_TYPE, @RequestParam String MAXCOUNT, @RequestParam String ACTDATE_FROM, @RequestParam String ACTDATE_TO, @RequestParam String ACT_MSG, HttpServletRequest request){
        JsonResponse response = new JsonResponse();
        log.info("edit start ....");
        
        ACT_TYPE =(String)Sanitizer.escapeHTML(ACT_TYPE);
        MAXCOUNT =(String)Sanitizer.escapeHTML(MAXCOUNT);
        ACTDATE_FROM = (String)Sanitizer.escapeHTML(DateTimeUtils.formatYYYYMMDD(ACTDATE_FROM));
        ACTDATE_TO = (String)Sanitizer.escapeHTML(DateTimeUtils.formatYYYYMMDD(ACTDATE_TO));
        ACT_MSG = (String)Sanitizer.escapeHTML(ACT_MSG);

        ACT_MAIN po = new ACT_MAIN();

        po.setACT_TYPE(ACT_TYPE);
        po.setMAXCOUNT(MAXCOUNT);
        po.setACTDATE_FROM(ACTDATE_FROM);
        po.setACTDATE_TO(ACTDATE_TO);
        po.setACT_MSG(ACT_MSG);

        boolean result = act_MainsService.updateACT(po);
        
        response.setValidated(result);
        
        return response;
    }

    /**
     * 刪除 行銷主檔
     * 
     * @param request
     * @return
    */
    @PostMapping(value="/Delete", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse delete(@RequestParam String ACT_TYPE){
        JsonResponse response = new JsonResponse();
        log.info("delete start ....");
        
        ACT_TYPE =(String)Sanitizer.escapeHTML(ACT_TYPE);
        boolean result =  act_MainsService.deleteACT(ACT_TYPE);
        response.setValidated(result);
        
        return response;
    }
}
