package com.netbank.rest.web.back.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 供上版測試用
 * 
 * @author VincentHuang
 * */
@Controller
@RequestMapping("/MONITOR")
public class MonitorController {
	@RequestMapping(value = "/check")
	public @ResponseBody String check(){
		return "OK";
	}
}
