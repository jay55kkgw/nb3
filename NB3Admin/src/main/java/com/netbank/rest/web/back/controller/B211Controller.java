package com.netbank.rest.web.back.controller;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.netbank.rest.web.back.service.ADMLOGINService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.model.Rows;

@Controller
@RequestMapping("/B211")
public class B211Controller {
    @Autowired
    private ADMLOGINService admLogInService;

    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index() {
    	admLogInService.log4Query();
        return "B211/index";
    }

    @PostMapping(value={"/Concurrent"})
    @ResponseBody
    @Authorize(userInRoleCanQuery = true)
    public Map concurrent() {
        Map result = new HashMap();
        Rows rows = admLogInService.getCurrentLogins();
        for ( int i=0; i<rows.getSize(); i++ ) {
            Map<String, String> rm = rows.getRow(i).getValues();
            result.put(rm.keySet().toArray()[0].toString(), rm.values().toArray()[0].toString());
        }
    
        return result;
    } 
}