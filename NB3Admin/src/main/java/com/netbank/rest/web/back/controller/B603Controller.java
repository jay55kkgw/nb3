package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netbank.rest.web.back.model.B603ViewModel;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.model.UploadedFile;
import com.netbank.rest.web.back.service.ADMMAILLOGService;
import com.netbank.rest.web.back.service.TXNUSERService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import fstop.aop.Authorize;
import fstop.orm.po.TXNUSER;
import fstop.util.FileUtils;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;
import fstop.util.Sanitizer;
/**
 * <p>郵件公告編輯</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B603")
public class B603Controller {
    @Autowired
    private ADMMAILLOGService admMAILLOGService;

    @Autowired
    private TXNUSERService txnUserService;

    /**
     * 取得查詢頁面
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @Authorize(userInRoleCanQuery = true)
    @GetMapping(value = { "", "/Index" })
    public String index(ModelMap model) {
        return "B603/index";
    }

    /**
     * 送出
     * @param servletRequest
     * @param model
     * @param bindingResult
     * @return
     */
    @Authorize(userInRoleCanEdit  = true)
    @RequestMapping(value = "Send")
    public @ResponseBody Map send(HttpServletRequest servletRequest,B603ViewModel model,MultipartFile multipartFile) {
        Map<String, String> response;
        try {
            List<String> receivers = new ArrayList<String>();

            model.setTitle(URLDecoder.decode(model.getTitle(), StandardCharsets.UTF_8.name()));
            model.setContent(URLDecoder.decode(model.getContent(), StandardCharsets.UTF_8.name()));
            
            log.debug(Sanitizer.logForgingStr("Title="+model.getTitle()));
            log.debug(Sanitizer.logForgingStr("Content="+model.getContent()));
            
            if ( model.getSendType().equalsIgnoreCase("File") ) {
                //#region
                //20191204-Danny-Unrestricted File Upload
                //允許的類型
                String[] allowFileType = {"application/vnd.ms-excel","text/csv","text/plain"};
                //圖示允許的檔案大小
                int allowFileSize = 2*1024*1024; //2Mb
                String contentType = multipartFile.getContentType();
                String originalFilename = multipartFile.getOriginalFilename();
                Map<String,Object> returnMap = FileUtils.checkFile(multipartFile,originalFilename,contentType,allowFileType,allowFileSize,false);
                //#endregion
                //檢核不通過
                if("FALSE".equals(returnMap.get("result"))){
                    String errorMessage = (String)returnMap.get("summary");
                    log.debug("errorMessage={}",Sanitizer.logForgingStr(errorMessage));
                    log.error("send Error", Sanitizer.logForgingStr(errorMessage));
                    response = new HashMap<String, String>();
                    //20191115-Eric-Information Exposure Through an Error Message\路徑 41:
                    response.put("fileerror", (String)Sanitizer.logForgingStr(errorMessage));
                    //20191219-Danny:Reflected XSS All Clients
                    Map<String, String> sresponse = new HashMap<String, String>();
                    Sanitizer.escape4Class(response,sresponse);
                    return sresponse;
                }
                
                //20191115-Danny-Reflected XSS All Clients\路徑 38:
                originalFilename = (String)Sanitizer.escapeHTML(originalFilename);
                
                log.debug("fileName={}", Sanitizer.logForgingStr(originalFilename));
                //20191205-Danny-Unrestricted File Upload
                String fileContent = new String((byte[])returnMap.get("Data"), "UTF-8");
                //20191115-Danny-Reflected XSS All Clients\路徑 38:
                fileContent = (String)Sanitizer.escapeHTML(fileContent);
                String[] lines = fileContent.split("\\r?\\n");
    
                for (String line : lines) {
                    if ( !receivers.contains(line) ) {
                        receivers.add(line);
                    }
                }
            } else {
                // get all nb users
                List<TXNUSER> users = txnUserService.getValidEMails();
                for (TXNUSER user : users) {
                    if ( !receivers.contains(user.getDPMYEMAIL()) ) {
                        receivers.add(user.getDPMYEMAIL());
                    }
                }
            }
            //20191115-Danny-Reflected XSS All Clients\路徑 39:&40
            String title = (String)Sanitizer.escapeHTML(model.getTitle());
            String content = (String)Sanitizer.escapeHTML(model.getContent());

            response = admMAILLOGService.sendNotice(title, content, receivers);
        } catch (IOException e) {
            log.error("send Error", e);
            response = new HashMap<String, String>();
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤，請再試一次!!";
            response.put("error", (String)Sanitizer.logForgingStr(message));
            return response;
        }
        return response;
    }

    /**
     * 取得範例檔案
     * @param model
     * @param response
     * @throws IOException
     */
    @GetMapping(value = { "/Sample" })
    @Authorize(userInRoleCanQuery = true)
    public void sample(ModelMap model, HttpServletResponse response) throws IOException {
        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"MailList.txt\"");
        
        OutputStream resOs = response.getOutputStream();
        // resOs.write('\ufeef'); // emits 0xef
        // resOs.write('\ufebb'); // emits 0xbb
        // resOs.write('\ufebf'); // emits 0xbf

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = new CSVWriter(outputWriter, CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_ESCAPE_CHARACTER);

        String[] memo = { "以下為範例資料，上傳時請勿包含電子郵件...等欄位標題" };
        writer.writeNext(memo);

        String[] headers = { "電子郵件" };
        writer.writeNext(headers);


        String[] nextLine = { "noreply@tbb.com" };
        writer.writeNext(nextLine);
        
        writer.close();
    }
}
