package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// import fstop.orm.dao.AdmCountryDao;
import fstop.orm.dao.AdmCardTypeDao;
// import fstop.orm.po.ADMCOUNTRY;
import fstop.orm.po.ADMCARDTYPE;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>ADMCARDTYPEService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class ADMCARDTYPEService extends BaseService {
    @Autowired
    private AdmCardTypeDao admCardTypeDao;

    // data: {
    //     "ADTYPEID": typeId, 卡別代碼
    //     "ADTYPENAME": typeName, 卡別名稱
    //     "ADTYPEDESC": typeDesc 備註說明
    // },
    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo 第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy 依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param ADTYPEID 卡別代碼
     * @param ADTYPENAME 卡別名稱
     * @param ADTYPEDESC 備註說明
     * @return 分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String ADTYPEID, String ADTYPENAME, String ADTYPECHSNAME, String ADTYPEENGNAME, String ADTYPEDESC) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, ADTYPEID={}, ADTYPENAME={}, ADTYPECHSNAME={}, ADTYPEENGNAME={}, ADTYPEDESC={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ADTYPEID), Sanitizer.logForgingStr(ADTYPENAME), Sanitizer.logForgingStr(ADTYPECHSNAME), Sanitizer.logForgingStr(ADTYPEENGNAME), Sanitizer.logForgingStr(ADTYPEDESC));

        log4Query("0", new String[][] { { "ADTYPEID", ADTYPEID }, { "ADTYPENAME", ADTYPENAME }, { "ADTYPECHSNAME", ADTYPECHSNAME }, { "ADTYPEENGNAME", ADTYPEENGNAME }, { "ADTYPEDESC", ADTYPEDESC } } );
        return admCardTypeDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, ADTYPEID, ADTYPENAME, ADTYPECHSNAME, ADTYPEENGNAME, ADTYPEDESC);
    }

    /**
     * 取得國別資料
     *
     * @param adTypeId 卡別代碼
     * @return 卡片類別 POJO
     */
    public ADMCARDTYPE getByadTypeId(String adTypeId) {
        log.debug("getByADTYPEID ADTYPEID={}", Sanitizer.logForgingStr(adTypeId));

        log4Query("0", new String[][] { { "adTypeId", adTypeId } } );
        return admCardTypeDao.findById(adTypeId);
    }

    /**
     * 新增國別資料
     *
     * @param adType 卡別 POJO
     * @param creator 編輯者
     */
    public void insertCardType(ADMCARDTYPE adType, String creator) {
        adType.setLASTUSER(creator); 
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        adType.setLASTDATE(parts[0]);
        adType.setLASTTIME(parts[1]);

        admCardTypeDao.save(adType);
        log4Create(adType, "0");
    } 

    /**
     * 儲存國別資料
     *
     * @param adType 卡別 POJO
     * @param editor 編輯者
     */
    public void saveCardType(ADMCARDTYPE adType, String editor) {
        ADMCARDTYPE oriAdType = admCardTypeDao.findById(adType.getADTYPEID());
        admCardTypeDao.getEntityManager().detach(oriAdType);

        adType.setLASTUSER(editor); 

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        adType.setLASTDATE(parts[0]);
        adType.setLASTTIME(parts[1]);

        admCardTypeDao.update(adType);
        log4Update(oriAdType, adType, "0");
    }

    /**
     * 刪除卡別代碼
     *
     * @param ADTypeId 卡別ID
     */
    public void deleteCardType(String ADTypeId) {
        ADMCARDTYPE oriAdType = admCardTypeDao.findById(ADTypeId);
        admCardTypeDao.removeById(ADTypeId);  
        log4Delete(oriAdType, "0");
    }
}
