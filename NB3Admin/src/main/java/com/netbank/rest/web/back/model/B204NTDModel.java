package com.netbank.rest.web.back.model;


import fstop.orm.po.TXNTWSCHPAY;

/**
 * 預約明細查詢(台幣)
 *
 * @author Alison
 * @version V1.0
 */
public class B204NTDModel extends TXNTWSCHPAY {
    private static final long serialVersionUID = 1L;
	private String DPNEXTDATE = "";
	private String TXTYPE = "";

    /**
     * <p>getDPNEXTDATE.</p>
     *
     * @return String return the DPNEXTDATE
     */
    public String getDPNEXTDATE() {
        return DPNEXTDATE;
    }

    /**
     * <p>setDPNEXTDATE.</p>
     *
     * @param DPNEXTDATE the DPNEXTDATE to set
     */
    public void setDPNEXTDATE(String DPNEXTDATE) {
        this.DPNEXTDATE = DPNEXTDATE;
    }
	
    /** <p>getTXTYPE.</p>
     *
     * @return String return the TXTYPE
     */
    public String getTXTYPE() {
        return TXTYPE;
    }

    /**
     * <p>setTXTYPE.</p>
     *
     * @param TXTYPE the TXTYPE to set
     */
    public void setTXTYPE(String TXTYPE) {
        this.TXTYPE = TXTYPE;
    }
}
