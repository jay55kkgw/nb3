package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmBankDao;
import fstop.orm.po.ADMBANK;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 銀行管理服務
 *
 * @author 簡哥
 * @version V1.0
 */
@Slf4j
@Service
public class ADMBANKService extends BaseService {
    @Autowired
    private AdmBankDao admBankDao;

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo 第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy 依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param ADBANKID 銀行代碼
     * @param ADBANKNAME 銀行中文
     * @return 分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String ADBANKID, String ADBANKNAME, String ADBANKCHSNAME, String ADBANKENGNAME) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, ADBANKID={}, ADBANKNAME={}, ADBANKCHSNAME={}, ADBANKENGNAME={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ADBANKID), Sanitizer.logForgingStr(ADBANKNAME), Sanitizer.logForgingStr(ADBANKCHSNAME), Sanitizer.logForgingStr(ADBANKENGNAME));

        log4Query("0", new String[][] { { "ADBANKID", ADBANKID }, { "ADBANKNAME", ADBANKNAME }, { "ADBANKCHSNAME", ADBANKCHSNAME }, { "ADBANKENGNAME", ADBANKENGNAME } } );
        return admBankDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, ADBANKID, ADBANKNAME, ADBANKCHSNAME, ADBANKENGNAME);
    }

    /**
     * 取得銀行資料
     *
     * @param ADBANKID 銀行代碼
     * @return 銀行 POJO
     */
    public ADMBANK getByADBANKID(String ADBANKID, boolean logQuery) {
        log.debug("getByADBANKID ADBANKID={}", Sanitizer.logForgingStr(ADBANKID));

        if ( logQuery ) {
        	log4Query("0", new String[][] { { "ADBANKID", ADBANKID } } );
        }
        return admBankDao.findById(ADBANKID);
    }

    /**
     * 新增銀行資料
     *
     * @param bank 銀行 POJO
     * @param creator 編輯者
     */
    public void insertBank(ADMBANK bank, String creator) {
        bank.setLASTUSER(creator); 
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        bank.setLASTDATE(parts[0]);
        bank.setLASTTIME(parts[1]);
        
        admBankDao.save(bank);
        log4Create(bank, "0");
    } 

    /**
     * 儲存銀行資料
     *
     * @param bank 銀行 POJO
     * @param editor 編輯者
     */
    public void saveBank(ADMBANK bank, String editor) {
        ADMBANK oriBank = admBankDao.findById(bank.getADBANKID());
        admBankDao.getEntityManager().detach(oriBank);
        
        bank.setLASTUSER(editor); 

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        bank.setLASTDATE(parts[0]);
        bank.setLASTTIME(parts[1]);

        admBankDao.update(bank);
        log4Update(oriBank, bank, "0");
    }

    /**
     * 刪除銀行資料
     *
     * @param bankId a {@link java.lang.String} object.
     */
    public void deleteBank(String bankId) {
        ADMBANK po = admBankDao.findById(bankId);
        admBankDao.removeById(bankId);  
        log4Delete(po, "0");
    }
}
