package com.netbank.rest.web.back.model;

/**
 * 一般網路銀行峰值查詢統計表
 */
public class B708Result {
    private long item;
    private long queryCnt;
    private long nonQueryCnt;
    private long totalCnt;
    private long hour;
    
    public B708Result() {
        this.queryCnt = 0;
        this.nonQueryCnt = 0;
        this.totalCnt = 0;
    }

    public B708Result(long item, long queryCnt, long nonQueryCnt) {
        this.item = item;
        this.queryCnt = queryCnt;
        this.nonQueryCnt = nonQueryCnt;
        this.totalCnt = queryCnt+nonQueryCnt;
    }

	public long getItem() {
		return item;
	}

	public void setItem(long item) {
		this.item = item;
	}

	public long getQueryCnt() {
		return queryCnt;
	}

	public void setQueryCnt(long queryCnt) {
		this.queryCnt = queryCnt;
	}

	public long getNonQueryCnt() {
		return nonQueryCnt;
	}

	public void setNonQueryCnt(long nonQueryCnt) {
		this.nonQueryCnt = nonQueryCnt;
	}

	public long getTotalCnt() {
		return totalCnt;
	}

	public void setTotalCnt(long totalCnt) {
		// 因為之後只會查查詢類
		this.nonQueryCnt = totalCnt;		
		this.totalCnt = totalCnt;
	}

	public long getHour() {
		return hour;
	}

	public void setHour(long hour) {
		this.hour = hour;
	}
    
	
}