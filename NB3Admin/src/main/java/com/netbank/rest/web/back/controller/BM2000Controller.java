package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.google.gson.Gson;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.ADMMSGCODEEx;
import com.netbank.rest.web.back.model.BM2000ViewModel;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.model.UploadedFile;
import com.netbank.rest.web.back.service.ADMAPPVERService;
import com.netbank.rest.web.back.service.ADMMSGCODEService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import fstop.aop.Authorize;
import fstop.orm.po.ADMAPPVER;
import fstop.orm.po.ADMBHCONTACT;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 *  APP版本控制 Controller，使用另一個畫面做 CRUD
 *
 * @author DANNY CHOU
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/BM2000")
public class BM2000Controller {
    @Autowired
    private ADMAPPVERService admAPPVERService;

    @Autowired
    private PortalSSOService portalSSOService;
    
    /**
     * 取得 應用系統代碼維護 首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        if ( portalSSOService.isPermissionOK("BM2000", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        return "BM2000/index";
    }

    /**
     * 分頁查詢
     *
     * @param viewModel  代號
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @return 支援 jQuery Datatable Server 端的分頁物件
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value = "/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@ModelAttribute @Valid BM2000ViewModel viewModel, 
        HttpServletRequest request,
        HttpServletResponse response) {

    	viewModel.setVERDATE(viewModel.getVERDATE().replace("/", ""));
    	
        String jsonData = new Gson().toJson(viewModel);
        BM2000ViewModel sviewModel = new BM2000ViewModel();
        Sanitizer.escape4Class(viewModel, sviewModel);

        log.debug("query BM2000ViewModel={}",Sanitizer.logForgingStr(jsonData));

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admAPPVERService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
                jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(),
                jsonData);
        
        List<ADMAPPVER> admVerApps = (List<ADMAPPVER>) page.getResult();
        List<ADMAPPVER> sadmAppVers = new ArrayList<ADMAPPVER>();
        for (ADMAPPVER admAppVer : admVerApps) {
            ADMAPPVER sadmAppVer = new ADMAPPVER();
            Sanitizer.escape4Class(admAppVer, sadmAppVer);
            sadmAppVers.add(sadmAppVer);
        }
        
        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), sadmAppVers);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得 應用系統代碼維護 新增頁
     *
     * @return view path
     */
    @GetMapping(value = { "/Create" })
    @Authorize(userInRoleCanEdit = true)
    public String create() {
        return "BM2000/Create";
    }

    /**
     * 新增資料
     *
     * @param ADMAPPVER 系統代碼物件
     * @param result     SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value = "/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse create(@ModelAttribute @Valid ADMAPPVER ADMAPPVER, BindingResult result) {
        JsonResponse response = new JsonResponse();

        try {
            if (result.hasErrors()) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                        .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
                response.setValidated(false);
            } else {
            	ADMAPPVER.setVERDATE(ADMAPPVER.getVERDATE().replace("/", ""));
                admAPPVERService.insertADMAPPVER(ADMAPPVER, portalSSOService.getLoginUserId());
                response.setValidated(true);
        }
        } catch (Exception e) {
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "存檔發生錯誤，請確認資料是否有更新成功!!";
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", message);

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }

        return response;
    }



    /**
     * 取得修改頁面
     * @param APPID
     * @param model
     * @return
     */
    @GetMapping(value = { "/Edit/{APPID}" })
    @Authorize(userInRoleCanQuery = true)
    public String edit(@PathVariable String APPID, ModelMap model) {
    	APPID = (String)Sanitizer.logForgingStr(APPID);
    	ADMAPPVER po = admAPPVERService.getByID(Integer.parseInt(APPID));
        model.addAttribute("Data", po);
        if ( portalSSOService.isPermissionOK("BM2000", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        
        return "BM2000/Edit";
    }
    
    
    /**
     * 修改資料
     *
     * @param ADMAPPVER APP版本控制
     * @param result     SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value = "/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid ADMAPPVER ADMAPPVER, BindingResult result) {
        JsonResponse response = new JsonResponse();
        try {
            if (result.hasErrors()) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                        .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
            	ADMAPPVER saveEntity = new ADMAPPVER();
            	saveEntity.setAPPID(ADMAPPVER.getAPPID());
            	saveEntity.setAPPOS(ADMAPPVER.getAPPOS());
            	saveEntity.setAPPMODELNO(ADMAPPVER.getAPPMODELNO());
            	saveEntity.setVERNO(ADMAPPVER.getVERNO());
            	saveEntity.setVERDATE(ADMAPPVER.getVERDATE().replace("/", ""));
            	saveEntity.setVERSTATUS(ADMAPPVER.getVERSTATUS());
            	saveEntity.setVERMSG(ADMAPPVER.getVERMSG());
            	saveEntity.setVERURL(ADMAPPVER.getVERURL());
            	
            	admAPPVERService.saveADMAPPVER(saveEntity, portalSSOService.getLoginUserId());
                
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //Information Exposure Through an Error Message
            errors.put("summary", "存檔發生錯誤，請確認資料是否有更新成功!!");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }

        return response;
    }
    
    /**
     * 刪除APP控制版本資料
     *
     * @param appid 代碼
     * @return "0"：成功，非"0"：失敗原因
     */
    @PostMapping(value="/Delete/{appid}", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String delete(@PathVariable Integer appid) {
        try {
        	admAPPVERService.deleteADMAPPVER(appid);
            return "0";
        } catch (Exception e) {
            log.error("delete error", e);
            
            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }


}
