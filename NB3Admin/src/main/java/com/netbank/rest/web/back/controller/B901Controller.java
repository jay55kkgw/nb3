package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ADMBANKService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMBANK;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 銀行檔維護控制器，使用一個畫面做 CRUD
 *
 * @author 簡哥
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B901")
public class B901Controller {
    @Autowired
    private ADMBANKService admBankService;
    
    @Autowired 
    private PortalSSOService portalSSOService;
    
    /**
     * 取得首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value={"", "/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        model.addAttribute("allowEdit", portalSSOService.isPermissionOK("B901", AuthorityEnum.EDIT));
        return "B901/index";
    }

    /**
     * 分頁查詢
     *
     * @param ADBANKID 銀行代碼
     * @param ADBANKNAME 銀行繁中
     * @param ADBANKCHSNAME 銀行簡中
     * @param ADBANKENGNAME 銀行英文
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return 支援 jQuery Datatable Server 端的分頁物件
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value="/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String ADBANKID, @RequestParam String ADBANKNAME, @RequestParam String ADBANKCHSNAME, @RequestParam String ADBANKENGNAME, HttpServletRequest request, HttpServletResponse response ) {
        log.debug("query ADBANKID={}, ADBANKNAME={}", Sanitizer.logForgingStr(ADBANKID), Sanitizer.logForgingStr(ADBANKNAME));
        
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admBankService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(), 
            jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(ADBANKID), (String)Sanitizer.escapeHTML(ADBANKNAME),
                (String)Sanitizer.escapeHTML(ADBANKCHSNAME), (String)Sanitizer.escapeHTML(ADBANKENGNAME));

        List<ADMBANK> banks=(List<ADMBANK>)page.getResult();
        List<ADMBANK> sbanks= new ArrayList<ADMBANK>();
        for (ADMBANK bank : banks) {
            ADMBANK sbank=new ADMBANK();
            Sanitizer.escape4Class(bank, sbank);
            sbanks.add(sbank);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), sbanks);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得單一銀行資料
     *
     * @param ADBANKID 銀行代碼
     * @return ADMBANK Json 物件
     */
    @PostMapping(value="/Get/{ADBANKID}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody ADMBANK get(@PathVariable String ADBANKID) {
        ADBANKID = (String)Sanitizer.logForgingStr(ADBANKID);
        //Missing Content Security Policy
        ADMBANK entity = admBankService.getByADBANKID(ADBANKID, true);
        ADMBANK oEntity = new ADMBANK();
        Sanitizer.escape4Class(entity, oEntity);
        return oEntity;
    }

    /**
     * 新增銀行資料
     *
     * @param admBank 銀行物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse create(@ModelAttribute @Valid ADMBANK admBank, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                if ( admBankService.getByADBANKID(admBank.getADBANKID(), false) == null ) {
                    admBankService.insertBank(admBank, portalSSOService.getLoginUserId()); 
                    response.setValidated(true);
                } else {
	            	 Map<String, String> errors = new HashMap<String, String>();

	            	 //Reflected XSS All Clients
           		 	 String adbankid = (String)Sanitizer.escapeHTML(admBank.getADBANKID());
	                 errors.put("summary", "銀行代碼["+adbankid+"]已存在");
	
	                 response.setValidated(false);
	                 response.setErrorMessages(errors);
	                 return response;
                }
                
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 46:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 修改銀行資料
     *
     * @param admBank 銀行物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid ADMBANK admBank, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                admBankService.saveBank(admBank, portalSSOService.getLoginUserId()); 
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 47:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 刪除銀行資料
     *
     * @param ADBANKID 銀行代碼
     * @return "0"：成功，非"0"：失敗原因
     */
    @PostMapping(value="/Delete/{ADBANKID}", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String delete(@PathVariable String ADBANKID) {
        try {
            ADBANKID = (String)Sanitizer.logForgingStr(ADBANKID);
            admBankService.deleteBank(ADBANKID);
            return "0";
        } catch (Exception e) {
            log.error("delete error", e);

            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }

}
