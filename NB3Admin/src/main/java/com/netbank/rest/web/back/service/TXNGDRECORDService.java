package com.netbank.rest.web.back.service;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.TxnGdRecordDao;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 黃金存摺交易3公斤(含)以上查詢
 * 
 * @author Alison
 */
@Slf4j
@Service
public class TXNGDRECORDService extends BaseService {
    @Autowired
    private TxnGdRecordDao txnGdRecordDao;
    
    /**
     * 分頁查詢
     * 
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param orderDir
     * @param STARTDATE
     * @param ENDDATE
     * @param USERID
     * @return
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String STARTDATE, String ENDDATE,
            String USERID) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, STARTDATE={}, ENDDATE={}, USERID={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(STARTDATE), Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID));

        log4Query("0", new String[][] { { "STARTDATE", STARTDATE }, { "ENDDATE", ENDDATE }, { "USERID", USERID }} );
        return txnGdRecordDao.findPageData(pageNo, pageSize, orderBy, orderDir, STARTDATE, ENDDATE, USERID);
    }    
}