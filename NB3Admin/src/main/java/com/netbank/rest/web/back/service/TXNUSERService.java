package com.netbank.rest.web.back.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>TXNUSERService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class TXNUSERService extends BaseService {
    @Autowired
    private TxnUserDao txnUserDao;

    /**
     * 信用卡申請錯誤次數解除
     *
     * @param DPSUERID 信用卡申請錯誤次數解除資料代碼
     * @return 信用卡申請錯誤次數解除資料 POJO
     */
    public TXNUSER getByDPSUERID(String DPSUERID) {
        log.debug("getByDPSUERID DPSUERID={}", Sanitizer.logForgingStr(DPSUERID));

        log4Query("0", new String[][] { { "DPSUERID", DPSUERID } } );
        return txnUserDao.findById(DPSUERID);
    }

    /**
     * 信用卡申請錯誤次數解除
     *
     * @return 信用卡申請錯誤次數解除資料 POJO LIST
     */
    public List<TXNUSER> getAll() {
        return txnUserDao.getAll();
    }

 
    /**
     * 儲存信用卡申請錯誤次數解除
     *
     * @param txnuser 信用卡申請錯誤次數解除資料 POJO
     * @param editor 編輯者
     */
    public void Update(TXNUSER txnuser, String editor) {
        TXNUSER oriTXNUSER = txnUserDao.findById(txnuser.getDPSUERID());
        txnUserDao.getEntityManager().detach(oriTXNUSER);

        txnUserDao.update(txnuser);
        log4Update(oriTXNUSER, txnuser, "0");
    }

    /**
     * 取得有 EMail 的使用者清單
     * @return POJO LIST
     */
    public List<TXNUSER> getValidEMails() {
        return txnUserDao.getEmailUser();
    }
}
