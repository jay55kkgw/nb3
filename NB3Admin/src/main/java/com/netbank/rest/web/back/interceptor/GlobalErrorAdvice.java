package com.netbank.rest.web.back.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class GlobalErrorAdvice {
    private final String DEFAULT_ERROR_VIEW = "Common/error";

    @ExceptionHandler(value = RuntimeException.class)
    public String runTimeException(HttpServletRequest req, RuntimeException e) {
        log.error("runTimeException", e);

        req.setAttribute("errorType", "RuntimeExcpetion");
        req.setAttribute("errorDesc", "系統發生錯誤");
        log.error("runTimeException error >> {}",e); 
        return DEFAULT_ERROR_VIEW;
    }

    @ExceptionHandler(value = Exception.class)
    public String defaultrrorHandler(HttpServletRequest req, Exception e) throws Exception {
        log.error("Exception", e);

        req.setAttribute("errorType", "Exception");
        req.setAttribute("errorDesc", "系統發生錯誤");
        log.error("defaultrrorHandler error >> {}",e); 

        return DEFAULT_ERROR_VIEW;
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<String> handleMaxSizeException(MaxUploadSizeExceededException exc, HttpServletRequest request, HttpServletResponse response) {
        return new ResponseEntity<>("檔案超過限制大小（1.5MB)!", HttpStatus.SERVICE_UNAVAILABLE);
    }
}