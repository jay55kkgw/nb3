package com.netbank.rest.web.back.model;

import java.util.Date;

import fstop.orm.po.ADMADSTMP;
import fstop.orm.po.ADMMRKTTMP;
import lombok.Data;

/**
 * 行銷管理(手機管告) View Model
 *
 * @author 
 * @version V1.0
 */
@Data
public class MRKTTmpModel extends ADMMRKTTMP {
    /**
	 * 
	 */
	private static final long serialVersionUID = 9053599799592005840L;
	
	private String StartDateTime;
    private String EndDateTime;
    private String msgGuid;
    private String comments;
    private String fileGuid;
    private String MarqueePicGuid;
    
    // 前端頁面是字串, 後端用 blob 存, 存到 TARGETCONTENT
    private String targetContentStr;
    
}
