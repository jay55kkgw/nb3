package com.netbank.rest.web.back.model;

import com.netbank.rest.web.back.service.FlowService;

/**
 * 流程引擎執行結果
 *
 * @author 簡哥
 * @version V1.0
 */
public class FlowSvcResult {
    private String statusCode;
    private String statusDesc;
    private String CaseSN;
    private boolean IsFinal;
    private String NextStepId;
    private String NextStepName;

    /**
     * <p>Getter for the field <code>statusCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * <p>Setter for the field <code>statusCode</code>.</p>
     *
     * @param statusCode a {@link java.lang.String} object.
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * <p>Getter for the field <code>statusDesc</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getStatusDesc() {
        switch ( statusCode ) {
            case FlowService.ERR_CASE_NOT_ALLOWED:
                statusDesc = String.format("%s：不允許處理此案件", statusCode);
                break;

            case FlowService.ERR_CASE_PROCESSED:
                statusDesc = String.format("%s：案件已被處理", statusCode);
                break;

            case FlowService.ERR_DB_ACCESS_ERROR:
                statusDesc = String.format("%s：資料庫處理錯誤", statusCode);
                break;

            case FlowService.ERR_NODE_NOT_FOUND:
                statusDesc = String.format("%s：找不到處理節點", statusCode);
                break;

            case FlowService.ERR_NOT_START_OR_EXTERNAL_NODE:
                statusDesc = String.format("%s：儲存案件需為起始節點或外部節點", statusCode);
                break;

            case FlowService.ERR_STEP_TYPE_NOT_SUPPORT:
                statusDesc = String.format("%s：未支援此節點類型", statusCode);
                break;

            default:
                statusDesc = String.format("%s：未知的錯誤", statusCode);
                break;
        }

        return statusDesc;
    }

    /**
     * <p>Setter for the field <code>statusDesc</code>.</p>
     *
     * @param statusDesc a {@link java.lang.String} object.
     */
    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    /**
     * <p>getCaseSN.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCaseSN() {
        return CaseSN;
    }

    /**
     * <p>setCaseSN.</p>
     *
     * @param caseSN a {@link java.lang.String} object.
     */
    public void setCaseSN(String caseSN) {
        CaseSN = caseSN;
    }

    /**
     * <p>isIsFinal.</p>
     *
     * @return a boolean.
     */
    public boolean isIsFinal() {
        return IsFinal;
    }

    /**
     * <p>setIsFinal.</p>
     *
     * @param isFinal a boolean.
     */
    public void setIsFinal(boolean isFinal) {
        IsFinal = isFinal;
    }

    /**
     * <p>getNextStepId.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNextStepId() {
        return NextStepId;
    }

    /**
     * <p>setNextStepId.</p>
     *
     * @param nextStepId a {@link java.lang.String} object.
     */
    public void setNextStepId(String nextStepId) {
        NextStepId = nextStepId;
    }

    /**
     * <p>getNextStepName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNextStepName() {
        return NextStepName;
    }

    /**
     * <p>setNextStepName.</p>
     *
     * @param nextStepName a {@link java.lang.String} object.
     */
    public void setNextStepName(String nextStepName) {
        NextStepName = nextStepName;
    }
}
