package com.netbank.rest.web.back.model;

import lombok.Data;

/**
 * 
 */
@Data
public class B201ViewModel {
    private String startDate;
    private String endDate;
    private String adUserId;
    private String adOPGroup;
    private String adOPName;
    private String adTxAcno;
    private String adUserIp;
    private String adExCode;
    private String loginType;
    private String userName;
    private String adopid;
}