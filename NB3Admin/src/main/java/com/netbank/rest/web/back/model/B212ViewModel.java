package com.netbank.rest.web.back.model;

import lombok.Data;

/**
 * 
 */
@Data
public class B212ViewModel {
    private String custNo;
    private String idGateId;
    private String queryType;
    private String sDate;
    private String eDate;

    private String cancelCustNo;
    private String cancelIdGateId;
    private String cancelDeviceId;
    private String cancelDeviceType;

}