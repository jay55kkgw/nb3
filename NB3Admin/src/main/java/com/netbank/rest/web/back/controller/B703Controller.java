package com.netbank.rest.web.back.controller;

import com.netbank.rest.web.back.model.B703GPAResult;
import com.netbank.rest.web.back.model.B703GPBEResult;
import com.netbank.rest.web.back.model.B703Model;
import com.netbank.rest.web.back.service.NMBReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fstop.aop.Authorize;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>B703Controller class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B703")
public class B703Controller {
    @Autowired
    private NMBReportService nbmReportService;

    /**
     * 取得一般網路銀行交易量統計表首頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {

        // 設定初始值
        B703Model entity = new B703Model();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, -1);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        String todayYear = String.format("%04d", year);
        String todayMonth = String.format("%02d", month);
        entity.setYYYY(todayYear);
        entity.setMM(todayMonth);
        entity.setOptradio("ALL");
        entity.setLoginType("'','NB','MB'");

        List<B703GPAResult> queryData = new ArrayList<B703GPAResult>(); 	//getResultList(entity);
        model.addAttribute("Data", queryData);

        List<B703GPBEResult> queryData2 = new ArrayList<B703GPBEResult>();	//getResultList2(entity);
        model.addAttribute("Data2", queryData2);

        long fundQty = queryData2.stream().filter((s) -> s.getSeqno().equals("1")).count();
        long goldQty = queryData2.stream().filter((s) -> s.getSeqno().equals("2")).count();
        model.addAttribute("fundQty", fundQty);
        model.addAttribute("goldQty", goldQty);

        model = SetDropDownList(model, entity);
        model.addAttribute("isPostback", false);
        return "B703/index";
    }

    /**
     * 查詢資料
     *
     * @param result    SpringMVC Binding 檢核結果
     * @return Json 結果物件
     * @param entity a {@link com.netbank.rest.web.back.model.B703Model} object.
     * @param model a {@link org.springframework.ui.ModelMap} object.
     */
    @PostMapping(value = "/Query")
    @Authorize(userInRoleCanQuery = true)
    public String query(@ModelAttribute B703Model entity, BindingResult result, ModelMap model) {
        // 錯誤處理
        if (result.hasErrors()) {
            // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
            Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

            model.addAttribute("Validated", false);
            model.addAttribute("ErrMsg", errors);
            return "B703/index";
        }

        List<B703GPAResult> queryData = getResultList(entity);
        model.addAttribute("Data", queryData);

        List<B703GPBEResult> queryData2 = getResultList2(entity);
        model.addAttribute("Data2", queryData2);

        long fundQty = queryData2.stream().filter((s) -> s.getSeqno().equals("1")).count();
        long goldQty = queryData2.stream().filter((s) -> s.getSeqno().equals("2")).count();
        model.addAttribute("fundQty", fundQty);
        model.addAttribute("goldQty", goldQty);

        model = SetDropDownList(model, entity);
        model.addAttribute("isPostback", true);
        return "B703/index";
    }

    /**
     * 取得 應用系統代碼維護 修改
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @param entity a {@link com.netbank.rest.web.back.model.B703Model} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException if any.
     */
    @PostMapping(value = { "/DownloadtoCSV" })
    @Authorize(userInRoleCanQuery  = true)
    public void DownloadtoCSV(@ModelAttribute B703Model entity, ModelMap model, HttpServletResponse response)
            throws IOException {

        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"ExportSys.csv\"");
        OutputStream resOs = response.getOutputStream();
        resOs.write('\ufeef'); // emits 0xef
        resOs.write('\ufebb'); // emits 0xbb
        resOs.write('\ufebf'); // emits 0xbf

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = new CSVWriter(outputWriter);

        List<B703GPAResult> queryData = getResultList(entity);

        String[] headers = { "類别", "單位", "", "自行交易", "", "", "", "", "", "", "", "跨行交易", "", "", "", "", "", "", "",
                "合計", "", "" };
        writer.writeNext(headers);
        String[] headers1 = { "", "", "", "交易密碼", "", "電子簽章(i-key)", "", "晶片金融卡", "", "動態密碼(OTP)", "", "交易密碼", "",
                "電子簽章(i-key)", "", "晶片金融卡", "", "動態密碼(OTP)", "", "", "", "" };
        writer.writeNext(headers1);
        String[] headers2 = { "", "", "", "成功", "失敗", "成功", "失敗", "成功", "失敗", "成功", "失敗", "成功", "失敗", "成功", "失敗", "成功",
                "失敗", "成功", "失敗", "成功", "失敗", "失敗%" };
        writer.writeNext(headers2);

        for (B703GPAResult item : queryData) {
            String adUserType = "";
            switch (item.getAdUserType()) {
            case "1":
                adUserType = "企業戶";
                break;
            case "0":
                adUserType = "個人戶";
                break;
            case "-1":
                adUserType = "小計";
                break;
            }
            String[] nextLine = { item.getAdopGroupName(), adUserType, item.getQtyType().equals("1") ? "筆數" : "金額",
                    item.getQ00t(), item.getQ00f(), item.getQ01t(), item.getQ01f(), item.getQ02t(), item.getQ02f(),
                    item.getQ03t(), item.getQ03f(), item.getQ10t(), item.getQ10f(), item.getQ11t(), item.getQ11f(),
                    item.getQ12t(), item.getQ12f(), item.getQ13t(), item.getQ13f(), item.getQ99t(), item.getQ99f(),
                    item.getQ99tf() };
            writer.writeNext(nextLine);
        }

        writer.writeNext(new String[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                "", "" });
        writer.writeNext(new String[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                "", "" });
        writer.writeNext(new String[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                "", "" });
        writer.writeNext(new String[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                "", "" });

        List<B703GPBEResult> queryData2 = getResultList2(entity);

        headers = new String[] { "類别", "", "單位", "自行交易", "", "", "", "", "", "", "", "合計", "", "", "", "", "", "", "",
                "", "", "" };
        writer.writeNext(headers);
        headers1 = new String[] { "", "", "", "交易密碼", "", "電子簽章(i-key)", "", "晶片金融卡", "", "動態密碼(OTP)", "", "", "", "",
                "", "", "", "", "", "", "", "" };
        writer.writeNext(headers1);
        headers2 = new String[] { "", "", "", "成功", "失敗", "成功", "失敗", "成功", "失敗", "成功", "失敗", "成功", "失敗", "失敗%" };
        writer.writeNext(headers2);

        for (B703GPBEResult item : queryData2) {
            String firstName = "";
            String twoName = "";
            switch (item.getSeqno()) {
            case "1":
                firstName = "基金";
                twoName = item.getAdopGroupName();
                break;
            case "2":
                firstName = "黃金";
                twoName = item.getAdopGroupName();
                break;
            case "3":
                firstName = "查詢服務";
                break;
            case "4":
                firstName = "申請類";
                break;
            case "5":
                firstName = "掛失類";
                break;
            case "6":
                firstName = "個人化設定";
                break;
            case "7":
                firstName = "變更資料";
                break;
            case "8":
                firstName = "通知服務";
                break;
            }
            String[] nextLine = { firstName, twoName, item.getQtyType().equals("1") ? "筆數" : "金額", item.getQ0t(),
                    item.getQ0f(), item.getQ1t(), item.getQ1f(), item.getQ2t(), item.getQ2f(), item.getQ3t(),
                    item.getQ3f(), item.getQ9t(), item.getQ9f(), item.getQ9tf() };
            writer.writeNext(nextLine);
        }

        writer.close();
    }

    /*
     * 取回查詢資料
     */
    private List<B703GPAResult> getResultList(B703Model entity) {

        Rows qResult = new Rows();
        try {
            qResult = nbmReportService.queryB703GPA(entity);
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());
        }

        List<B703GPAResult> queryData = new ArrayList<>();
        B703GPAResult qItem;
        int rowCount = qResult.getSize();
        for (int i = 0; i < rowCount; i++) {
            qItem = new B703GPAResult();
            Row r = null;
            r = qResult.getRow(i);
            qItem.setAdopGroup(r.getValue("ADOPGROUP"));
            qItem.setAdopGroupName(r.getValue("ADOPGROUPNAME"));
            qItem.setAdUserType(r.getValue("ADUSERTYPE"));
            qItem.setQtyType(r.getValue("QTYTYPE"));

            qItem.setQ00t(r.getValue("Q00T"));
            qItem.setQ00f(r.getValue("Q00F"));
            qItem.setQ01t(r.getValue("Q01T"));
            qItem.setQ01f(r.getValue("Q01F"));
            qItem.setQ02t(r.getValue("Q02T"));
            qItem.setQ02f(r.getValue("Q02F"));
            qItem.setQ03t(r.getValue("Q03T"));
            qItem.setQ03f(r.getValue("Q03F"));
            qItem.setQ10t(r.getValue("Q10T"));
            qItem.setQ10f(r.getValue("Q10F"));
            qItem.setQ11t(r.getValue("Q11T"));
            qItem.setQ11f(r.getValue("Q11F"));
            qItem.setQ12t(r.getValue("Q12T"));
            qItem.setQ12f(r.getValue("Q12F"));
            qItem.setQ13t(r.getValue("Q13T"));
            qItem.setQ13f(r.getValue("Q13F"));
            qItem.setQ99t(r.getValue("Q99T"));
            qItem.setQ99f(r.getValue("Q99F"));

            qItem.setQ99tf(r.getValue("Q99TF"));

            queryData.add(qItem);
        }

        return queryData;
    }

    /*
     * 取回查詢資料
     */
    private List<B703GPBEResult> getResultList2(B703Model entity) {

        Rows qResult = new Rows();
        try {
            qResult = nbmReportService.queryB703GPBE(entity);
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());
        }

        List<B703GPBEResult> queryData = new ArrayList<>();
        B703GPBEResult qItem;
        int rowCount = qResult.getSize();
        for (int i = 0; i < rowCount; i++) {
            qItem = new B703GPBEResult();
            Row r = null;
            r = qResult.getRow(i);
            qItem.setSeqno(r.getValue("SEQNO"));
            qItem.setAdopGroup(r.getValue("ADOPGROUP"));
            qItem.setAdopGroupName(r.getValue("ADOPGROUPNAME"));
            qItem.setQtyType(r.getValue("QTYTYPE"));

            qItem.setQ0t(r.getValue("Q0T"));
            qItem.setQ0f(r.getValue("Q0F"));
            qItem.setQ1t(r.getValue("Q1T"));
            qItem.setQ1f(r.getValue("Q1F"));
            qItem.setQ2t(r.getValue("Q2T"));
            qItem.setQ2f(r.getValue("Q2F"));
            qItem.setQ3t(r.getValue("Q3T"));
            qItem.setQ3f(r.getValue("Q3F"));
            qItem.setQ9t(r.getValue("Q9T"));
            qItem.setQ9f(r.getValue("Q9F"));

            qItem.setQ9tf(r.getValue("Q9TF"));

            queryData.add(qItem);
        }

        return queryData;
    }

    private ModelMap SetDropDownList(ModelMap model, B703Model entity) {
        model.addAttribute("B703Model", entity);

        List<String> years = new ArrayList<String>();
        for (int i = 2019; i <= 2119; i++) {
            years.add(Integer.toString(i));
        }

        List<String> months = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            months.add(String.format("%02d", i));
        }

        model.addAttribute("years", years);
        model.addAttribute("months", months);

        //TODO: 改寫-取回功能清單
        //IMasterValuesHelper groups = commonPools.ui.getNB3GroupChilds();
        /*** log info ***/
        //log.debug("@@@ " + this.getClass().getName() + " saving acno value == ");
        //groups.dump();

        //int rowCount = groups.getValueOccurs("VALUE");
        Map<String, String> txIdMap = new HashMap<String, String>();
        //for (int i = 0; i < rowCount; i++) {
            //txIdMap.put(groups.getValueByFieldName("VALUE", i + 1), groups.getValueByFieldName("TEXT", i + 1));
        //}
        model.addAttribute("nb3txids", txIdMap);

        return model;
    }

}
