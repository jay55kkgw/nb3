package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ADMCARDTYPEService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
// import fstop.orm.po.ADMCOUNTRY;
import fstop.orm.po.ADMCARDTYPE;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 信用卡卡別資料匯入控制器，使用一個畫面做 CRUD
 *
 * @author 毓棠
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B110")
public class B110Controller {
    @Autowired
    private ADMCARDTYPEService admCARDTYPEService;

    @Autowired
    private PortalSSOService portalSSOService;
    
    /**
     * 取得首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value={"","/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        if ( portalSSOService.isPermissionOK("B110", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        return "B110/index";
    }

    /**
     * 卡別分頁查詢
     *
     * @param ADTYPEID 卡別代碼
     * @param ADTYPENAME 卡別名稱(繁中)
     * @param ADTYPECHSNAME 卡別名稱(簡中)
     * @param ADTYPEENGNAME 卡別名稱(英文)
     * @param ADTYPEDESC 備註說明
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return 支援 jQuery Datatable Server 端的分頁物件
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value="/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String ADTYPEID, @RequestParam String ADTYPENAME, @RequestParam String ADTYPECHSNAME, @RequestParam String ADTYPEENGNAME, @RequestParam String ADTYPEDESC, HttpServletRequest request, HttpServletResponse response ) {
        log.debug("query ADTYPEID={}, ADTYPENAME={}, ADTYPECHSNAME={}, ADTYPEENGNAME={}, ADTYPEDESC={}", Sanitizer.logForgingStr(ADTYPEID), 
            Sanitizer.logForgingStr(ADTYPENAME), Sanitizer.logForgingStr(ADTYPECHSNAME), Sanitizer.logForgingStr(ADTYPEENGNAME), Sanitizer.logForgingStr(ADTYPEDESC));
        
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admCARDTYPEService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(), 
            jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(ADTYPEID), 
            (String)Sanitizer.escapeHTML(ADTYPENAME), (String)Sanitizer.escapeHTML(ADTYPECHSNAME), 
            (String)Sanitizer.escapeHTML(ADTYPEENGNAME), (String)Sanitizer.escapeHTML(ADTYPEDESC));

        List<ADMCARDTYPE> cardtypes=(List<ADMCARDTYPE>)page.getResult();
        List<ADMCARDTYPE> scardtypes= new ArrayList<ADMCARDTYPE>();
        for (ADMCARDTYPE cardtype : cardtypes) {
            ADMCARDTYPE scardtype = new ADMCARDTYPE();
            Sanitizer.escape4Class(cardtype, scardtype);
            scardtypes.add(scardtype);
        }
        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), scardtypes);
        //20191115-Danny-Reflected XSS All Clients\路徑 7:
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得單一卡別物件資料
     *
     * @param ADTYPEID 卡別代碼
     * @return ADMCARDTYPE Json 物件
     */
    @Authorize(userInRoleCanQuery = true)
    @PostMapping(value="/Get/{ADTYPEID}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody ADMCARDTYPE get(@PathVariable String ADTYPEID) {
        ADTYPEID = (String)Sanitizer.logForgingStr(ADTYPEID);
        //20191202-Danny-Missing Content Security Policy\路徑 1:
        ADMCARDTYPE entity = admCARDTYPEService.getByadTypeId(ADTYPEID);
        ADMCARDTYPE oEntity = new ADMCARDTYPE();
        Sanitizer.escape4Class(entity, oEntity);
        return oEntity;
    }

    /**
     * 新增卡別物件資料
     *
     * @param admCardTyoe 卡別物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse create(@ModelAttribute @Valid ADMCARDTYPE admCardTyoe, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                if ( admCARDTYPEService.getByadTypeId(admCardTyoe.getADTYPEID()) == null ) {
                    admCARDTYPEService.insertCardType(admCardTyoe, portalSSOService.getLoginUserId());
                    response.setValidated(true);
                } else {
                    throw new Exception("卡別代碼["+admCardTyoe.getADTYPEID()+"]已存在");
                }
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 15:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }        
        return response;
    }

    /**
     * 修改卡別資料
     *
     * @param admCardTyoe 卡別物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid ADMCARDTYPE admCardTyoe, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                admCARDTYPEService.saveCardType(admCardTyoe, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 16:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 刪除卡別資料
     *
     * @param ADTYPEID 卡別代碼
     * @return "0"：成功，非"0"：失敗原因
     */
    @PostMapping(value="/Delete/{ADTYPEID}", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String delete(@PathVariable String ADTYPEID) {
        try {
            ADTYPEID = (String)Sanitizer.logForgingStr(ADTYPEID);
            admCARDTYPEService.deleteCardType(ADTYPEID);
            return "0";
        } catch (Exception e) {
            log.error("delete error", e);
            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }

}
