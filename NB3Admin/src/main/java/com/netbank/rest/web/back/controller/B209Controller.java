package com.netbank.rest.web.back.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netbank.rest.web.back.service.ADMLOGINOUTService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.netbank.domain.orm.core.Page;
import fstop.aop.Authorize;
import fstop.orm.po.ADMLOGINOUT;
import fstop.util.DateTimeUtils;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 客戶登入、登出紀錄查詢
 *
 * @author Alison
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B209")
public class B209Controller {
    @Autowired
    private ADMLOGINOUTService admLogOutService;

    /**
     * 取得查詢頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        Date now = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.MONTH, -6);

        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat("yyyy/MM/dd");

        model.addAttribute("StartDt", formatter.format(now));
        model.addAttribute("EndDt", formatter.format(now));

        return "B209/index";
    }

    /**
     * 分頁查詢
     *
     * @param STARTDATE 查詢期間起日
     * @param ENDDATE   查詢期間迄日
     * @param USERID    使用者統編
     * @param LOGINTYPE 登入來源
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @return a {@link com.netbank.rest.web.back.util.JQueryDataTableResponse} object.
     */
    @PostMapping(value = "/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String STARTDATE, @RequestParam String ENDDATE,
            @RequestParam String USERID, @RequestParam String LOGINTYPE, HttpServletRequest request,
            HttpServletResponse response) {
    	log.info("B209 Query Start...");
        log.debug("query STARTDATE={}, ENDDATE={}, USERID={},LOGINTYPE={}", Sanitizer.logForgingStr(STARTDATE), Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID), Sanitizer.logForgingStr(LOGINTYPE));

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        //20191211-Danny-EDIT:554後台管理-[客戶登入/登出紀錄]查詢-時間未排序且資料未完全撈出顯示
        STARTDATE = DateTimeUtils.formatYYYYMMDD(STARTDATE);
        ENDDATE = DateTimeUtils.formatYYYYMMDD(ENDDATE);

        STARTDATE = STARTDATE.replace("/", "");
        ENDDATE = ENDDATE.replace("/", "");

        //#region 處理日期排序的問題(因為2欄，排序時需將欄位相加)
        if (jqDataTableRq.getOrderBy().equals("cmyyyymm")){
            String orderBystr = jqDataTableRq.getOrderBy() + " " + jqDataTableRq.getOrderDir() +  ",cmtime " ;
            jqDataTableRq.setOrderBy(orderBystr);
        }

        Page page = admLogOutService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
                jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(STARTDATE), 
                (String)Sanitizer.escapeHTML(ENDDATE), (String)Sanitizer.escapeHTML(USERID), (String)Sanitizer.escapeHTML(LOGINTYPE));

        List<ADMLOGINOUT> admloginouts = (List<ADMLOGINOUT>) page.getResult();

        List<ADMLOGINOUT> sadmloginouts = new ArrayList<ADMLOGINOUT>();

        for (ADMLOGINOUT admloginout : admloginouts) {
            ADMLOGINOUT sadmloginout=new ADMLOGINOUT();
            Sanitizer.escape4Class(admloginout, sadmloginout);
            sadmloginouts.add(sadmloginout);
        }    
        
        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), admloginouts);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);
        log.info("B209 Query End...");
        return sjqDataTableRs;
    }
}
