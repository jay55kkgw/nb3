package com.netbank.rest.web.back.model;

import fstop.orm.po.ADMMSGCODE;

/**
 * 應用系統代碼維護(修改,刪除狀態)使用
 *
 * @author Danny
 * @version V1.0
 */
public class ADMMSGCODEEx extends ADMMSGCODE {
    private static final long serialVersionUID = 1L;
    private String OPType = "";

    /**
     * <p>getOPType.</p>
     *
     * @return String return the OPType
     */
    public String getOPType() {
        return OPType;
    }

    /**
     * <p>setOPType.</p>
     *
     * @param OPType the OPType to set
     */
    public void setOPType(String OPType) {
        this.OPType = OPType;
    }

}
