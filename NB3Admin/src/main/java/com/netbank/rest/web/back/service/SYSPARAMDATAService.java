package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import com.google.gson.Gson;
import com.netbank.rest.web.back.model.B105ViewModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.SysParamDao;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.po.SYSPARAM;
import fstop.orm.po.SYSPARAMDATA;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>SYSPARAMDATAService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class SYSPARAMDATAService extends BaseService {
    @Autowired
    private SysParamDataDao sysParamDataDao;
    
    @Autowired
    private SysParamDao sysParamDao;

    /**
     * 依主鍵查詢
     *
     * @param id 主鍵
     * @return a {@link fstop.orm.po.SYSPARAMDATA} object.
     */
    public SYSPARAMDATA getByADPKID(String id) {
        log.debug("getByADPKID id={}", Sanitizer.logForgingStr(id));
        log4Query("0", new String[][] { { "id", id }} );
        return sysParamDataDao.findById(id);
    }

    /**
     * 儲存各業務營業時間維護
     *
     * @param sysparamdata 各業務營業時間維護 POJO
     * @param editor       編輯者
     */
    public void saveSysparmDataB105(@Valid B105ViewModel sysparamdata, String editor) {
        SYSPARAMDATA oldSYSPARAMDATA = sysParamDataDao.findById(sysparamdata.getADPK());
        Gson gson = new Gson();
        String before = gson.toJson(oldSYSPARAMDATA);

        // 外匯開始及結束時間
        oldSYSPARAMDATA.setADFXSHH(checkLength(sysparamdata.getADFXSHH()));
        oldSYSPARAMDATA.setADFXSMM(checkLength(sysparamdata.getADFXSMM()));
        oldSYSPARAMDATA.setADFXSSS(checkLength(sysparamdata.getADFXSSS()));
        oldSYSPARAMDATA.setADFXEHH(checkLength(sysparamdata.getADFXEHH()));
        oldSYSPARAMDATA.setADFXEMM(checkLength(sysparamdata.getADFXEMM()));
        oldSYSPARAMDATA.setADFXESS(checkLength(sysparamdata.getADFXESS()));

        // 基木開始及結束時間
        oldSYSPARAMDATA.setADFDSHH(checkLength(sysparamdata.getADFDSHH()));
        oldSYSPARAMDATA.setADFDSMM(checkLength(sysparamdata.getADFDSMM()));
        oldSYSPARAMDATA.setADFDSSS(checkLength(sysparamdata.getADFDSSS()));
        oldSYSPARAMDATA.setADFDEHH(checkLength(sysparamdata.getADFDEHH()));
        oldSYSPARAMDATA.setADFDEMM(checkLength(sysparamdata.getADFDEMM()));
        oldSYSPARAMDATA.setADFDESS(checkLength(sysparamdata.getADFDESS()));


        // 黃金開始及結束時間
        oldSYSPARAMDATA.setADGDSHH(checkLength(sysparamdata.getADGDSHH()));
        oldSYSPARAMDATA.setADGDSMM(checkLength(sysparamdata.getADGDSMM()));
        oldSYSPARAMDATA.setADGDSSS(checkLength(sysparamdata.getADGDSSS()));
        oldSYSPARAMDATA.setADGDEHH(checkLength(sysparamdata.getADGDEHH()));
        oldSYSPARAMDATA.setADGDEMM(checkLength(sysparamdata.getADGDEMM()));
        oldSYSPARAMDATA.setADGDESS(checkLength(sysparamdata.getADGDESS()));

        oldSYSPARAMDATA.setLASTUSER(editor);

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        oldSYSPARAMDATA.setLASTDATE(parts[0]);
        oldSYSPARAMDATA.setLASTTIME(parts[1]);

        sysParamDataDao.update(oldSYSPARAMDATA);
        log4Update(before, oldSYSPARAMDATA, "0");
    }

    /**
     * 儲存其他資料
     *
     * @param sysparamdata 其他資料 POJO
     * @param editor       編輯者
     */
    public void saveSysparmDataB106(@Valid SYSPARAMDATA sysparamdata, SYSPARAM bank3Mail, SYSPARAM txnAmtMail, String bank3OldValue, String txnAmtOldValue, String editor) {
        SYSPARAMDATA oldSYSPARAMDATA = sysParamDataDao.findById(sysparamdata.getADPK());
        
        Gson gson = new Gson();
        String before = gson.toJson(oldSYSPARAMDATA);
        
        oldSYSPARAMDATA.setLASTUSER(editor);

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        oldSYSPARAMDATA.setLASTDATE(parts[0]);
        oldSYSPARAMDATA.setLASTTIME(parts[1]);
        
        sysParamDataDao.update(oldSYSPARAMDATA);
        
        log4Update(before, oldSYSPARAMDATA, "0");
        
        if ( bank3Mail.getADPARAMID() == null ) {
        	log4Create(bank3Mail, "0");
        } else {
        	SYSPARAM b = new SYSPARAM();
        	b.setADPARAMNAME(bank3Mail.getADPARAMNAME());
        	b.setADPARAMVALUE(bank3OldValue);
        	
        	log4Update(b, bank3Mail, "0");
        	sysParamDao.update(bank3Mail);
        }
        
        if ( txnAmtMail.getADPARAMID() == null ) {
        	log4Create(txnAmtMail, "0");
        } else {
        	SYSPARAM b = new SYSPARAM();
        	b.setADPARAMNAME(txnAmtMail.getADPARAMNAME());
        	b.setADPARAMVALUE(txnAmtOldValue);
        	
        	log4Update(b, txnAmtMail, "0");
        	sysParamDao.update(txnAmtMail);
            
        }
    }


    /**
     * <p>checkLength.</p>
     *
     * @param value a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public String checkLength(String value) {
        if (value.length() == 1)
            value = "0" + value;
        return value;

    }

}
