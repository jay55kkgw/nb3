package com.netbank.rest.web.back.util;

/**
 * <p>JQueryDataTableRequest class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class JQueryDataTableRequest {
    /// <summary>
    ///
    /// </summary>
    private int draw;

    /// <summary>
    /// 開始筆數
    /// </summary>
    private int start;

    /// <summary>
    /// 長度
    /// </summary>
    private int length;

    /// <summary>
    /// 排序欄位ID
    /// </summary>
    private String orderBy;

    /// <summary>
    /// 升降冪
    /// </summary>
    private String orderDir;

    /**
     * <p>Getter for the field <code>draw</code>.</p>
     *
     * @return the draw
     */
    public int getDraw() {
        return draw;
    }

    /**
     * <p>Setter for the field <code>draw</code>.</p>
     *
     * @param draw the draw to set
     */
    public void setDraw(int draw) {
        this.draw = draw;
    }

    /**
     * <p>Getter for the field <code>start</code>.</p>
     *
     * @return the start
     */
    public int getStart() {
        return start;
    }

    /**
     * <p>Setter for the field <code>start</code>.</p>
     *
     * @param start the start to set
     */
    public void setStart(int start) {
        this.start = start;
    }

    /**
     * <p>Getter for the field <code>length</code>.</p>
     *
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * <p>Setter for the field <code>length</code>.</p>
     *
     * @param length the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * <p>Getter for the field <code>orderBy</code>.</p>
     *
     * @return the orderBy
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * <p>Setter for the field <code>orderBy</code>.</p>
     *
     * @param orderBy the orderBy to set
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * <p>Getter for the field <code>orderDir</code>.</p>
     *
     * @return the orderDir
     */
    public String getOrderDir() {
        return orderDir;
    }

    /**
     * <p>Setter for the field <code>orderDir</code>.</p>
     *
     * @param orderDir the orderDir to set
     */
    public void setOrderDir(String orderDir) {
        this.orderDir = orderDir;
    }

    /**
     * <p>getRecordFrom.</p>
     *
     * @return a int.
     */
    public int getRecordFrom() {
        return start + 1;
    }

    /**
     * <p>getRecordTo.</p>
     *
     * @return a int.
     */
    public int getRecordTo() {
        return start + length;
    }

    /**
     * 取得頁碼，從 1 起編
     *
     * @return a int.
     */
    public int getPage() {
        return start == 0 ? 1 : (start / length) + 1;
    }
}
