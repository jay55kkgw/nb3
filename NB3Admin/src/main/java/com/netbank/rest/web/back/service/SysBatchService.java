package com.netbank.rest.web.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.domain.orm.core.Page;

import fstop.orm.dao.SysBatchDao;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SysBatchService  extends BaseService {
    @Autowired
    private SysBatchDao sysBatchDao;
    
    public Page getAll(int pageNo, int pageSize, String orderBy, String orderDir) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}",
        		Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir));

        log4Query("0");
        
        return sysBatchDao.findAll(pageNo, pageSize, orderBy, orderDir);
    }
}
