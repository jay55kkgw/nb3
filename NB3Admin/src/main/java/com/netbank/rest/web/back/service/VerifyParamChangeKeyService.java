package com.netbank.rest.web.back.service;

import java.util.HashMap;
import java.util.Map;

import com.netbank.rest.web.back.util.RESTfulClientUtil;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;


/*
 * QRCODE 驗證參數變更
 */
@Slf4j
@Service
public class VerifyParamChangeKeyService extends BaseService {
    @Value("${MS_QR_URL}")
    private String msqrURL;
    
    public Map getVerifyParam(String dpsUserId) throws Exception {
        String result = "";
        
        Integer toMsTimeout = 120;
        try {
            Map requestMap = new HashMap();

            String url = msqrURL + "com/GetVerifyParam/MSRQ";
            log.info("MS_QR URL : {}",url);
            Map rr= RESTfulClientUtil.send(requestMap, url , toMsTimeout);
        	
        	log4Query("0", new String[][] {{"dpsUserId",dpsUserId}});
        	
        	return rr;
            
        } catch (Exception e) {
        	
        	log4Query("1", new String[][] {{"dpsUserId",dpsUserId}});
            log.error("getVerifyParam error", e);
            throw e;
        }
    }
    
    public Map changeVerifyParam(String dpsUserId, String ValidParam, String val) throws Exception {
        String result = "";
        
        Integer toMsTimeout = 120;
        try {
            Map requestMap = new HashMap();

            String url = msqrURL + "qrp/MB3/changeKey";
            requestMap.put("KEY", ValidParam);
            requestMap.put("VALUE", val);
            log.info("MS_QR URL : {}",url);
            Map rr= RESTfulClientUtil.send(requestMap, url , toMsTimeout);
        	
        	log4Query("0", new String[][] {{"dpsUserId",dpsUserId}});
        	
        	return rr;
            
        } catch (Exception e) {
        	
        	log4Query("1", new String[][] {{"dpsUserId",dpsUserId}});
            log.error("getVerifyParam error", e);
            throw e;
        }
    }
    
}