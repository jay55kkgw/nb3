package com.netbank.rest.web.back.model;

import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Range;

import fstop.orm.validator.EMailValidatorConstraint;
import fstop.orm.validator.IPValidatorConstraint;

/**
 * 其它資料維護 View Model
 *
 * @author Alison
 * @version V1.0
 */
public class B106ViewModel {

    @Id
    private String ADPK;

    @NotBlank(message = "使用者 Session Timeout 設定,不能為空值")
    @Range(min=300, max=1800, message = "使用者 Session Timeout值不正確，範圍為 300～1800 秒")
    private String ADSESSIONTO = "";

    @NotBlank(message = "OP 人員 Email 帳號,不能為空值")
    @EMailValidatorConstraint(message = "OP人員 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com")
    private String ADOPMAIL = "";

    @NotBlank(message = "AP 人員 Email 帳號,不能為空值")
    @EMailValidatorConstraint(message = "AP 人員 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com")
    private String ADAPMAIL = "";

    @NotBlank(message = "安控人員 Email 帳號,不能為空值")
    @EMailValidatorConstraint(message = "安控人員 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com")
    private String ADSECMAIL = "";
    
    @NotBlank(message = "SP 人員 Email 帳號,不能為空值")
    @EMailValidatorConstraint(message = "SP 人員 Email 帳號格式不正確，格式為：name@domain.com;name2@domain.com")
    private String ADSPMAIL = "";
    
    @NotBlank(message = "基金主機 IP,不能為空值")
    @IPValidatorConstraint(message = "基金主機 IP 值不正確，格式為：0.0.0.0 ~ 255.255.255.255")
    private String ADFDSVRIP = "";
    
    @NotBlank(message = "郵件主機 IP,不能為空值")
    @IPValidatorConstraint(message = "郵件主機 IP 值不正確，格式為：0.0.0.0 ~ 255.255.255.255")
    private String ADMAILSVRIP = "";

    @NotBlank(message = "停車費主機 IP,不能為空值")
    @IPValidatorConstraint(message = "停車費主機 IP 值不正確，格式為：0.0.0.0 ~ 255.255.255.255")
    private String ADPARKSVRIP = "";

    @NotBlank(message = "AR主機 IP,不能為空值")
    @IPValidatorConstraint(message = "AR 主機 IP 值不正確，格式為：0.0.0.0 ~ 255.255.255.255")
    private String ADARSVRIP = "";

    @NotBlank(message = "基金主機 Retry 次數,不能為空值")
    @Range(min=0, max=5, message = "基金主機 Retry 次數值不正確，範圍為 0～5")
    private String ADFDRETRYTIMES = "";

    @NotBlank(message = "停車費主機 Retry 次數,不能為空值")
    @Range(min=0, max=5, message = "停車費主機 Retry 次數值不正確，範圍為 0～5")
    private String ADPKRETRYTIMES = "";

    @NotBlank(message = "補發次數上限, 不能為空值")
    @Range(min=0, max=5, message = "補發次數上限值不正確，範圍為 0～5")
    private String ADRESENDTIMES = "";

    // 以下為 2020/8/24 景森發現有缺漏的
    private String ADGDMAIL = "";
    
    private String ADGDTXNMAIL = "";
    
    // 以下這兩個欄位在 SysParam
    private String ADBANK3MAIL = "";

    private String TXNTWAMTMAIL = "";
    
    /**
     * <p>getADPK.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADPK() {
        return ADPK;
    }

    /**
     * <p>setADPK.</p>
     *
     * @param adpk a {@link java.lang.String} object.
     */
    public void setADPK(String adpk) {
        ADPK = adpk;
    }

    /**
     * <p>getADSESSIONTO.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADSESSIONTO() {
        return ADSESSIONTO;
    }

    /**
     * <p>setADSESSIONTO.</p>
     *
     * @param aADSESSIONTO a {@link java.lang.String} object.
     */
    public void setADSESSIONTO(String aADSESSIONTO) {
        ADSESSIONTO = aADSESSIONTO;
    }

    /**
     * <p>getADOPMAIL.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADOPMAIL() {
        return ADOPMAIL;
    }

    /**
     * <p>setADOPMAIL.</p>
     *
     * @param aADOPMAIL a {@link java.lang.String} object.
     */
    public void setADOPMAIL(String aADOPMAIL) {
        ADOPMAIL = aADOPMAIL;
    }

    /**
     * <p>getADAPMAIL.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADAPMAIL() {
        return ADAPMAIL;
    }

    /**
     * <p>setADAPMAIL.</p>
     *
     * @param aADAPMAIL a {@link java.lang.String} object.
     */
    public void setADAPMAIL(String aADAPMAIL) {
        ADAPMAIL = aADAPMAIL;
    }

    /**
     * <p>getADSECMAIL.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADSECMAIL() {
        return ADSECMAIL;
    }

    /**
     * <p>setADSECMAIL.</p>
     *
     * @param aADSECMAIL a {@link java.lang.String} object.
     */
    public void setADSECMAIL(String aADSECMAIL) {
        ADSECMAIL = aADSECMAIL;
    }

    /**
     * <p>getADSPMAIL.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADSPMAIL() {
        return ADSPMAIL;
    }

    /**
     * <p>setADSPMAIL.</p>
     *
     * @param aADSPMAIL a {@link java.lang.String} object.
     */
    public void setADSPMAIL(String aADSPMAIL) {
        ADSPMAIL = aADSPMAIL;
    }

    /**
     * <p>getADFDSVRIP.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFDSVRIP() {
        return ADFDSVRIP;
    }

    /**
     * <p>setADFDSVRIP.</p>
     *
     * @param aADFDSVRIP a {@link java.lang.String} object.
     */
    public void setADFDSVRIP(String aADFDSVRIP) {
        ADFDSVRIP = aADFDSVRIP;
    }

    /**
     * <p>getADMAILSVRIP.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADMAILSVRIP() {
        return ADMAILSVRIP;
    }

    /**
     * <p>setADMAILSVRIP.</p>
     *
     * @param aADMAILSVRIP a {@link java.lang.String} object.
     */
    public void setADMAILSVRIP(String aADMAILSVRIP) {
        ADMAILSVRIP = aADMAILSVRIP;
    }

    /**
     * <p>getADPARKSVRIP.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADPARKSVRIP() {
        return ADPARKSVRIP;
    }

    /**
     * <p>setADPARKSVRIP.</p>
     *
     * @param aADPARKSVRIP a {@link java.lang.String} object.
     */
    public void setADPARKSVRIP(String aADPARKSVRIP) {
        ADPARKSVRIP = aADPARKSVRIP;
    }

    /**
     * <p>getADARSVRIP.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADARSVRIP() {
        return ADARSVRIP;
    }

    /**
     * <p>setADARSVRIP.</p>
     *
     * @param aADARSVRIP a {@link java.lang.String} object.
     */
    public void setADARSVRIP(String aADARSVRIP) {
        ADARSVRIP = aADARSVRIP;
    }

    /**
     * <p>getADFDRETRYTIMES.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFDRETRYTIMES() {
        return ADFDRETRYTIMES;
    }

    /**
     * <p>setADFDRETRYTIMES.</p>
     *
     * @param aADFDRETRYTIMES a {@link java.lang.String} object.
     */
    public void setADFDRETRYTIMES(String aADFDRETRYTIMES) {
        ADFDRETRYTIMES = aADFDRETRYTIMES;
    }

    /**
     * <p>getADPKRETRYTIMES.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADPKRETRYTIMES() {
        return ADPKRETRYTIMES;
    }

    /**
     * <p>setADPKRETRYTIMES.</p>
     *
     * @param aADPKRETRYTIMES a {@link java.lang.String} object.
     */
    public void setADPKRETRYTIMES(String aADPKRETRYTIMES) {
        ADPKRETRYTIMES = aADPKRETRYTIMES;
    }

    /**
     * <p>getADRESENDTIMES.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADRESENDTIMES() {
        return ADRESENDTIMES;
    }

    /**
     * <p>setADRESENDTIMES.</p>
     *
     * @param aADRESENDTIMES a {@link java.lang.String} object.
     */
    public void setADRESENDTIMES(String aADRESENDTIMES) {
        ADRESENDTIMES = aADRESENDTIMES;
    }

	public String getADGDMAIL() {
		return ADGDMAIL;
	}

	public void setADGDMAIL(String aDGDMAIL) {
		ADGDMAIL = aDGDMAIL;
	}

	public String getADGDTXNMAIL() {
		return ADGDTXNMAIL;
	}

	public void setADGDTXNMAIL(String aDGDTXNMAIL) {
		ADGDTXNMAIL = aDGDTXNMAIL;
	}

	public String getADBANK3MAIL() {
		return ADBANK3MAIL;
	}

	public void setADBANK3MAIL(String aDBANK3MAIL) {
		ADBANK3MAIL = aDBANK3MAIL;
	}

	public String getTXNTWAMTMAIL() {
		return TXNTWAMTMAIL;
	}

	public void setTXNTWAMTMAIL(String tXNTWAMTMAIL) {
		TXNTWAMTMAIL = tXNTWAMTMAIL;
	}
    
}
