package com.netbank.rest.web.back.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.netbank.rest.web.back.model.BatchResult;
import com.netbank.rest.web.back.util.RESTfulClientUtil;

import fstop.util.DateTimeUtils;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnFxSchPayDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.orm.po.TXNFXSCHPAYDATA;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TXNFXRECORDService extends BaseService {

	@Autowired
	private TxnFxRecordDao fxRecordDao;

	@Autowired
	private TxnFxSchPayDao schPayDao;

	@Autowired
	private TxnFxSchPayDataDao schPayDataDao;

	final int toMsTimeout = 120;
	
    @Value("${FXTERMINATE.URL}")
    private String TerminateUrl;
    
    @Value("${FXREVERSAL.URL}")
    private String ReversalUrl; 
	
	/**
	 * 外匯交易失敗人工處理 之 終止功能
	 * 
	 * @author vincenthuang
	 * @param ADTXNO     交易編號
	 *
	 */
	public Map<String, String> Terminate(String ADTXNO) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			String lastdate = DateTimeUtils.format("yyyyMMdd", new Date());
//			LinkedList<Object[]> list = fxRecordDao.findById(ADTXNO);
			
			TXNFXRECORD po = fxRecordDao.findById(ADTXNO);
			
			if(po == null) {
				result.put("MsgCode", "1");
				result.put("Message", "查無對應資料");	
				return result;
			}else {
				log.info("執行 外匯交易失敗人工 終止 ，ADTXNO：" + ADTXNO);

				po.setFXTXSTATUS("2");
				fxRecordDao.update(po);
				List<String> inParam = new ArrayList<String>();
				inParam.add("U");
				inParam.add(ADTXNO);
				BatchResult Br = RESTfulClientUtil.sendBatch(inParam, TerminateUrl, toMsTimeout);
				String errorMsg = "";
                if ( Br != null && Br.isSuccess()) {
                	Map<String, Object> data = Br.getData();
                	//如果出Exception會有值
                	if(data.containsKey("errorMsg")) {
                		errorMsg += "關帳發生錯誤 ："+ data.get("errorMsg")+"\n";
            			result.put("MsgCode", "1");
            			result.put("Message", "系統錯誤，"+errorMsg);	
            			return result;
                	}
                	
                	if(data.containsKey("otherBankErrorMsg")) 
                		errorMsg += "他行關帳錯誤訊息 :"+ data.get("otherBankErrorMsg")+"\n";
                	if(data.containsKey("selfBankErrorMsg")) 
                		errorMsg += "自行關帳錯誤訊息 :"+ data.get("selfBankErrorMsg"+"\n");
                	
                	//他行關帳執行結果
                	if(data.containsKey("otherBankresult"))
                		result.put("otherBankresult", data.get("otherBankresult").toString());
                	//自行關帳執行結果
                	if(data.containsKey("selfBankresult"))
                		result.put("selfBankresult", data.get("selfBankresult").toString());
                	
                	if("".equals(errorMsg)) {
                		result.put("MsgCode", "0");
                		result.put("Message", "終止成功");
                	}else{
                    	result.put("MsgCode", "1");
            			result.put("Message", errorMsg);
                	}
                } else {
                	result.put("MsgCode", "1");
        			result.put("Message", "終止失敗");
                }
                log.info("結束 外匯交易失敗人工 終止 ，ADTXNO：" + ADTXNO);
			}
		} catch (Exception e) {
			log.error("e>>>{}", e);
			result.put("MsgCode", "1");
			result.put("Message", "系統錯誤");	
			return result;
		}
		return result;
	}
	
	public Map<String, String> Reversal(String ADTXNO) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			TXNFXRECORD po = fxRecordDao.findById(ADTXNO);
				
			if(po == null) {
				result.put("MsgCode", "1");
				result.put("Message", "查無對應資料");	
				return result;
			}else {
				log.info("執行 外匯交易失敗人工 沖回 ，ADTXNO：" + ADTXNO);
				po.setFXTXSTATUS("2");
				fxRecordDao.update(po);
				List<String> inParam = new ArrayList<String>();
				inParam.add(ADTXNO);
				BatchResult Br = RESTfulClientUtil.sendBatch(inParam, ReversalUrl, toMsTimeout);
				String errorMsg = "";
	            if ( Br != null && Br.isSuccess()) {
	               	Map<String, Object> data = Br.getData();
	               	//如果出Exception會有值
	               	List<String> terminateList = (List<String>) data.get("terminateList");
        			
	               	for(String adtxno:terminateList) {
	               		result = this.Terminate(adtxno);
		               	log.info("結束 外匯交易失敗人工 終止 ，ADTXNO：" + adtxno);
	               	}
	             	
	               	log.info("結束 外匯交易失敗人工 沖回 ，ADTXNO：" + ADTXNO);
	            }else {	            	
	            	errorMsg += "沖回發生錯誤";
	            	result.put("MsgCode", "1");
	            	result.put("Message", "系統錯誤，"+errorMsg);
	            	return result;
	            }
			}
		} catch (Exception e) {
			log.error("e>>>{}", e);
			result.put("MsgCode", "1");
			result.put("Message", "系統錯誤");	
			return result;
		}
		return result;
	}
	
	public List<TXNFXRECORD> findFxAllowTerminate(String FXUSERID,String[] pendingArr){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String today = sdf.format(new Date());
		return fxRecordDao.findFxAllowTerminate(FXUSERID ,today, pendingArr);
	}
	

}