package com.netbank.rest.web.back.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.google.gson.Gson;
import com.netbank.rest.web.back.model.AnnTmpModel;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.model.UploadedFile;
import com.netbank.rest.web.back.service.ADMADSService;
import com.netbank.rest.web.back.service.ADMANNService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.util.DateUtils;

import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import fstop.aop.Authorize;
import fstop.orm.po.ADMANNTMP;
import fstop.orm.po.ADMUPLOADTMP;
import fstop.util.DateTimeUtils;
import fstop.util.FileUtils;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 個人訊息公告內容刊登控制器，含經辦編輯與主管放行
 *
 * @author  簡哥
 * @since   2019/8/18
 */
@Controller
@Slf4j
@RequestMapping("/B502")
public class B502Controller {
    @Autowired
    private ADMADSService admADSService;
    
    @Autowired
    private ADMANNService admANNService;

    @Autowired
    private PortalSSOService portalSSOService;

    /**
     * 取得查詢頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"", "/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        Date now = new Date();
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.MONTH, -6);
        Date before = cal.getTime();

        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat ("yyyy/MM/dd");
        
        model.addAttribute("StartDt", formatter.format(before));
        model.addAttribute("EndDt", formatter.format(now));
        
        return "B502/index";
    }

    /**
     * 查詢公告(前端分頁)
     *
     * @param DateFrom  起日
     * @param DateTo    迄日
     * @param TITLE     公告標題
     * @return          ADMANNTMP 物件LIST
     */
    @PostMapping(value="/IndexQuery", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody List<ADMANNTMP> indexQuery(@RequestParam String DateFrom, @RequestParam String DateTo, @RequestParam String TITLE,  @RequestParam String FlowFinished,@RequestParam String ANNCHANNEL,
    		HttpServletRequest request, HttpServletResponse response ) {
    	
        log.debug("indexQuery DateFrom={}, DateTo={}, CONTENT={}, ANNCHANNEL={}", Sanitizer.logForgingStr(DateFrom), Sanitizer.logForgingStr(DateTo), Sanitizer.logForgingStr(TITLE),Sanitizer.logForgingStr(ANNCHANNEL));

        DateFrom = DateTimeUtils.formatYYYYMMDD(DateFrom);
        DateTo = DateTimeUtils.formatYYYYMMDD(DateTo);

        List<ADMANNTMP> annLists = admANNService.findAnnTmp(portalSSOService.getLoginBranch(), (String)Sanitizer.escapeHTML(DateFrom), (String)Sanitizer.escapeHTML(DateTo), 
        		(String)Sanitizer.escapeHTML(TITLE),portalSSOService.getLoginRoles(), (String)Sanitizer.escapeHTML(FlowFinished),(String)Sanitizer.escapeHTML(ANNCHANNEL));

        List<ADMANNTMP> sannLists = new ArrayList<ADMANNTMP>();
        for(ADMANNTMP annList : annLists) {
            ADMANNTMP sannList = new ADMANNTMP();
            FlowFinished = (String)Sanitizer.escapeHTML(FlowFinished);
            if(!"Y".equals(FlowFinished)) {
            	annList.setEDITOROID(annList.getOID());
            	annList.setEDITORUNAME(annList.getLASTUSER());
            }
            Sanitizer.escape4Class(annList, sannList);
            sannLists.add(sannList);
        }
        
        return sannLists;
    }

    /**
     * 取得輸入頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"/Create"})
    @Authorize(userInRoleCanEdit = true)
    public String create(ModelMap model) {
    	//只有H15(資訊)及H57(電金)可刊登重要公司
        if ( portalSSOService.getLoginBranch().equalsIgnoreCase("H15") || portalSSOService.getLoginBranch().equalsIgnoreCase("H57") ) {
        	model.addAttribute("important", true); 
        } else {
        	model.addAttribute("important", false);
        }

        return "B502/create";
    }

    /**
     * 經辦送出
     *
     * @param adsTmpModel   公告流程主檔 POJO
     * @param result        Binding 結果
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/Send", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse send(@ModelAttribute @Valid AnnTmpModel annTmpModel, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                Map<String, String> svcResult = admANNService.sendAnnTmp(annTmpModel, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName());
                Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
                if ( entry.getKey().equalsIgnoreCase("0") ) {
                    response.setValidated(true);
                    response.setPkey(entry.getValue());
                } else {
                    throw new Exception(entry.getKey());
                }
            }
        } catch (Exception e) {
            log.error("send", e);
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 36:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    } 

    /**
     * 由待辦清單做案件分派
     *
     * @param id        案件代碼
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"/Query/{id}"})
    @Authorize()
    public String query(@PathVariable String id, ModelMap model) {
        id = (String)Sanitizer.logForgingStr(id);
        AuthorityEnum caseStatus = admANNService.queryAnnTmp(id, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginRoles());
        ADMANNTMP po = admANNService.findById(id);
        if ( !po.getUSERLIST().isEmpty() ) {
        	try {
        		po.setUSERLIST(po.getUSERLIST().replace("\r\n", ","));
        	} catch ( Exception ignore ) {
        		log.error("replace userlist seperate char");
        	}
        }

        model.addAttribute("StartDateTime",
                DateUtils.getDateTimeString(po.getSTARTDATE(), "/", po.getSTARTTIME(), ":", false));
        model.addAttribute("EndDateTime",
                DateUtils.getDateTimeString(po.getENDDATE(), "/", po.getENDTIME(), ":", false));

        if (caseStatus == AuthorityEnum.EDIT || caseStatus == AuthorityEnum.NONE) {
            if (portalSSOService.isInRole(ADMANNService.EDITOR_ROLES)) {
                AnnTmpModel tmp = new AnnTmpModel();
                Gson gson = new Gson();
                tmp = gson.fromJson(gson.toJson(po), AnnTmpModel.class);
                model.addAttribute("Data", tmp);

                // 給前端頁面變更
                model.addAttribute("IsReject", caseStatus==AuthorityEnum.EDIT);
                
                //只有H15(資訊)及H57(電金)可刊登重要公司
                if ( portalSSOService.getLoginBranch().equalsIgnoreCase("H15") || portalSSOService.getLoginBranch().equalsIgnoreCase("H57") ) {
                	model.addAttribute("important", true); 
                } else {
                	model.addAttribute("important", false);
                }
                return "B502/edit";
            } else {
                model.addAttribute("Data", po);
                return "B502/query";
            }
        } else if ( caseStatus == AuthorityEnum.REVIEW ) {
            model.addAttribute("Data", po);
            model.addAttribute("User", po.getUSERLIST().split(","));
            return "B502/review";
        } else {
            model.addAttribute("Data", po);
            model.addAttribute("User", po.getUSERLIST().split(","));
            return "B502/query";
        }
    }

    /**
     * 同意
     *
     * @param id                案號
     * @param httpRequest a {@link javax.servlet.http.HttpServletRequest} object.
     * @param httpResponse a {@link javax.servlet.http.HttpServletResponse} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value={"/Approve/{id}"})
    @ResponseBody
    @Authorize(userInRoleCanApprove = true)
    public JsonResponse approve(@PathVariable String id, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        id = (String)Sanitizer.logForgingStr(id);
        JsonResponse response = new JsonResponse();
        try {
            String stepId = httpRequest.getParameter("StepId");
            String comments = httpRequest.getParameter("Comments");

            Map<String, String> svcResult = admANNService.approveOrRejectAnnTmp((String)Sanitizer.escapeHTML(id), stepId, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName(), comments, true);
            Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
            if ( entry.getKey().equalsIgnoreCase("0") ) {
                response.setValidated(true);
                response.setPkey(entry.getValue());
            } else {
                throw new Exception(entry.getKey());
            }
        } catch (Exception e) {
            log.error("send", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());

            response.setValidated(false);
            response.setErrorMessages(errors);
        }
        JsonResponse sresponse = new JsonResponse();
        Sanitizer.escape4Class(response, sresponse);
        return sresponse;
    } 

    /**
     * 退回
     *
     * @param id                案號
     * @param httpRequest a {@link javax.servlet.http.HttpServletRequest} object.
     * @param httpResponse a {@link javax.servlet.http.HttpServletResponse} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value={"/Reject/{id}"})
    @ResponseBody
    @Authorize(userInRoleCanApprove = true)
    public JsonResponse reject(@PathVariable String id, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        id = (String)Sanitizer.logForgingStr(id);
        JsonResponse response = new JsonResponse();
        try {
            String stepId = httpRequest.getParameter("StepId");
            String comments = httpRequest.getParameter("Comments");

            Map<String, String> svcResult = admANNService.approveOrRejectAnnTmp((String)Sanitizer.escapeHTML(id), stepId, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName(), comments, false);
            Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
            if ( entry.getKey().equalsIgnoreCase("0") ) {
                response.setValidated(true);
                response.setPkey(entry.getValue());
            } else {
                throw new Exception(entry.getKey());
            }
        } catch (Exception e) {
            log.error("send", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());

            response.setValidated(false);
            response.setErrorMessages(errors);
        }
        JsonResponse sresponse = new JsonResponse();
        Sanitizer.escape4Class(response, sresponse);
        return sresponse;
    }

    /**
     * 經辦重新送出
     *
     * @param adsTmpModel   廣告流程主檔 POJO
     * @param result        Binding 結果
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/ReSend", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse resend(@ModelAttribute @Valid AnnTmpModel annTmpModel, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                Map<String, String> svcResult = admANNService.resendAnnTmp(annTmpModel, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName());
                Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
                if ( entry.getKey().equalsIgnoreCase("0") ) {
                    response.setValidated(true);
                    response.setPkey(entry.getValue());
                } else {
                    throw new Exception(entry.getKey());
                }
            }
        } catch (Exception e) {
            log.error("resend", e);
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 38:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    } 

    /**
     * 經辦取消案件
     *
     * @param admAdsTmp a {@link fstop.orm.po.ADMADSTMP} object.
     * @param result a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/Cancel", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse cancel(@RequestParam String ID, @RequestParam String OID){
        JsonResponse response = new JsonResponse();
        try {
            Map<String, String> svcResult = admANNService.cancelAnnTmp((String)Sanitizer.escapeHTML(Sanitizer.logForgingStr(ID)), 
                portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName());
            Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
            if ( entry.getKey().equalsIgnoreCase("0") ) {
                response.setValidated(true);
                response.setPkey(ID);
            } else {
                throw new Exception(entry.getKey());
            }
        } catch (Exception e) {
            log.error("cancel", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());

            response.setValidated(false);
            response.setErrorMessages(errors);
        }
        JsonResponse sresponse = new JsonResponse();
        Sanitizer.escape4Class(response, sresponse);
        return sresponse;
    } 

    /**
     * 上傳名單
     * @param uploadedFile
     * @return
     */
    @RequestMapping(value="Upload")
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody Map<String, String> upload(MultipartFile multipartFile) {
        Map<String, String> results = new HashMap<String, String>();
        Map<String, String> sresults = new HashMap<String, String>();
       try {
            //#region
            //Reflected XSS All Clients
            //20191204-Danny-Unrestricted File Upload
            //允許的類型
            String[] allowFileType = {"application/vnd.ms-excel","text/csv","text/plain"};
            //圖示允許的檔案大小
            int allowFileSize = 2*1024*1024; //2Mb
            String contentType = multipartFile.getContentType();
            String originalFilename = multipartFile.getOriginalFilename();
            //#endregion
            if ( multipartFile.isEmpty()) {
                throw new Exception("上傳名單發生錯誤");
            }
            Map<String,Object> returnMap = FileUtils.checkFile(multipartFile,originalFilename,contentType,allowFileType,allowFileSize);
            //檢核不通過
            if("FALSE".equals(returnMap.get("result"))){
                String errorMessage = (String)returnMap.get("summary");
                log.debug("errorMessage={}",Sanitizer.logForgingStr(errorMessage));
                results.put("status", errorMessage);
                //20191219-Danny:Reflected XSS All Clients
                Sanitizer.escape4Class(results,sresults);
                return sresults;
            }
            String users = (String)returnMap.get("Data");

            //20191115-Danny-Reflected XSS All Clients\路徑 37:
            users = (String)Sanitizer.escapeHTML(users);
            users.replace('\n', ',');
            results.put("status", "0");
            results.put("filename", originalFilename);
            results.put("users", users);
        } catch (Exception e) {
            log.error("upload Error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤，請再試一次!!";
            results.put("status", (String)Sanitizer.logForgingStr(message));
            //20191219-Danny:Reflected XSS All Clients
            Sanitizer.escape4Class(results,sresults);
            return sresults;
        }
        //20191219-Danny:Reflected XSS All Clients
        Sanitizer.escape4Class(results,sresults);
        return sresults;
    }
    
    /**
     * 檔案上傳
     *
     * @param servletRequest    http request
     * @param uploadedFile a {@link com.netbank.rest.web.back.model.UploadedFile} object.
     * @param bindingResult a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     * 
     * 20191217-Danny-Unsafe Object Binding調整
     */
    @RequestMapping(value="UploadFile")
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse uploadFile(HttpServletRequest servletRequest, MultipartFile[] multipartFile) {
        JsonResponse response = new JsonResponse();
        try {
            String id=UUID.randomUUID().toString();
            response.setPkey(id);

            //#region
            //20191204-Danny-Unrestricted File Upload
            //允許的類型
            String[] allowFileType = {"application/pdf","image/jpeg","image/pjpeg","image/png","image/gif"};
            //允許的檔案大小
            int allowFileSize = 10*1024*1024; //2Mb
            String contentType = multipartFile[0].getContentType();
            String originalFilename = multipartFile[0].getOriginalFilename();
            Map<String,Object> returnMap;
 
            returnMap = FileUtils.checkFile(multipartFile[0],originalFilename,contentType,allowFileType,allowFileSize,false);
            //檢核不通過
            if("FALSE".equals(returnMap.get("result"))){
                String errorMessage = (String)returnMap.get("summary");
                log.debug("errorMessage={}",Sanitizer.logForgingStr(errorMessage));
                throw new Exception(errorMessage);
            }            
            //20191205-Danny-Unrestricted File Upload
            byte[] bData =(byte[])returnMap.get("Data");
            	admANNService.saveFileTmp(id, multipartFile[0].getOriginalFilename(), bData);
                response.setValidated(true);
        } catch (Exception e) {
            log.error("saveImage Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 33:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        return response;
    }    
    
    /**
     * 取得上傳檔案
     *
     * @param id a {@link java.lang.String} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     */
    @GetMapping(value = "/PreviewFile/{id}")
    @Authorize(userInRoleCanQuery = true)
    public void getFileAsByteArray(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) throws IOException {
        id = (String)Sanitizer.logForgingStr(id);
        boolean isTmp = Boolean.parseBoolean(request.getParameter("isTmp"));

        byte[] imageContent = null;
        String fileName = "";
        log.debug("request={}",Sanitizer.logForgingStr(request.getParameter("isTmp")));
        log.debug("isTmp={}",isTmp);
        if ( isTmp ) {
            // 從上傳暫存檔取出檔名及內容，以利呼叫端顯示圖片
            List<ADMUPLOADTMP> tmps = admADSService.getImageTmp(id);
            fileName = tmps.get(0).getFILENAME();
            imageContent = tmps.get(0).getFILECONTENT();
        } else {
            // 從暫存檔取出檔名及內容，以利呼叫端顯示圖片
            ADMANNTMP po = admANNService.findById(id);
            fileName = po.getSRCFILENAME();
            imageContent = po.getSRCCONTENT();
        } 
        String fileExt="";
        int lastIndexOf = fileName.lastIndexOf(".");
        if (lastIndexOf != -1) {
            fileExt = fileName.substring(lastIndexOf+1);
        }
        if ( fileExt.compareToIgnoreCase("jpg") == 0 || fileExt.compareToIgnoreCase("jpeg") == 0 ) {
            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        } else if ( fileExt.compareToIgnoreCase("png") == 0 ) {
            response.setContentType(MediaType.IMAGE_PNG_VALUE);
        } else if ( fileExt.compareToIgnoreCase("gif") == 0 ) {
            response.setContentType(MediaType.IMAGE_GIF_VALUE);
        } else if ( fileExt.compareToIgnoreCase("pdf") == 0 ) {
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        }
        InputStream in = new ByteArrayInputStream(imageContent);
        IOUtils.copy(in, response.getOutputStream());
    }
}
