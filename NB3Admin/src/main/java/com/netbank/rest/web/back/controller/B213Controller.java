package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import fstop.aop.Authorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.B212ViewModel;
import com.netbank.rest.web.back.service.Trns_NNB_CusdataService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import fstop.orm.po.IDGATEHISTORY;
import fstop.orm.po.QUICKLOGINMAPPING;
import fstop.orm.po.TRNS_NNB_CUSDATA;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 		交易失敗統計表
 * </p>
 *
 * @author VincentHuang
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B213")
public class B213Controller {
	
	@Autowired
	private Trns_NNB_CusdataService  nnb_CusdataService;
	
	/**
	 * 取得查詢頁面
	 *
	 */
	@GetMapping(value = { "", "/Index" })
	@Authorize(userInRoleCanQuery = true)
	public String index(ModelMap model) {
//		List<TRNS_NNB_CUSDATA> list = new ArrayList<TRNS_NNB_CUSDATA>();
//		list = nnb_CusdataService.getFaildata(); 
//		model.addAttribute("data", list);
		return "B213/index";
	}
	
	 	@PostMapping(value="/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	    @Authorize(userInRoleCanQuery = true)
	    public @ResponseBody JQueryDataTableResponse indexQuery(HttpServletRequest request) {
	        Page page = null;
	        
	        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);
			int pageNo = jqDataTableRq.getPage(), pageSize = jqDataTableRq.getLength();
			String orderBy = jqDataTableRq.getOrderBy(), orderDir = jqDataTableRq.getOrderDir();
	        page = nnb_CusdataService.pageQuery(pageNo, pageSize, orderBy, orderDir);
	        
			JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
					page.getTotalCount(), page.getTotalCount(), page.getResult());

			JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
			Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);
			
	        return sjqDataTableRs;
	    }
}
