package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>ADMHOLIDAYService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class ADMHOLIDAYService extends BaseService  {
    @Autowired
    private AdmHolidayDao admHolidayDao;

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo 第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy 依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param ADHOLIDAY 假日代號
     * @param ADREMARK 假日備註
     * @return 分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String ADHOLIDAY, String ADREMARK) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, ADHOLIDAY={}, ADREMARK={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ADHOLIDAY), Sanitizer.logForgingStr(ADREMARK));

        log4Query("0", new String[][] { { "ADHOLIDAY", ADHOLIDAY }, { "ADREMARK", ADREMARK }} );
        
        return admHolidayDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, ADHOLIDAY, ADREMARK);
    }

    /**
     * 取得假日資料
     *
     * @param holidayId 假日
     * @return 假日 POJO
     */
    public ADMHOLIDAY getHoliday(String holidayId) {
        log.debug("getByADCURRENCY holidayId={}", Sanitizer.logForgingStr(holidayId));

    	log4Query("0", new String[][] { { "holidayId", holidayId }} );
        
        return admHolidayDao.findById(holidayId);
    }

    /**
     * 新增假日資料
     *
     * @param holiday 	假日 POJO
     * @param creator 	編輯者
     * @param isBatch	批次不記 ActionLog, 這樣會太多筆
     */
    public void insertHoliday(ADMHOLIDAY holiday, String creator, boolean isBatch) {
        holiday.setLASTUSER(creator); 
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        holiday.setLASTDATE(parts[0]);
        holiday.setLASTTIME(parts[1]);

        admHolidayDao.save(holiday);
        
        if ( !isBatch ) {
        	log4Create(holiday, "0");
        } 
    } 

    /**
     * 儲存假日資料
     *
     * @param editor 編輯者
     * @param holiday a {@link fstop.orm.po.ADMHOLIDAY} object.
     */
    public void saveHoliday(ADMHOLIDAY holiday, String editor) {
        ADMHOLIDAY oriADMHOLIDAY = admHolidayDao.findById(holiday.getADHOLIDAYID());
        admHolidayDao.getEntityManager().detach(oriADMHOLIDAY);

        holiday.setLASTUSER(editor); 

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        holiday.setLASTDATE(parts[0]);
        holiday.setLASTTIME(parts[1]);

        admHolidayDao.update(holiday);
        log4Update(oriADMHOLIDAY, holiday, "0");
    }

    /**
     * 刪除假日資料
     *
     * @param holidayId 幣別ID
     */
    public void deleteHoliday(String holidayId) {
        ADMHOLIDAY oriADMHOLIDAY = admHolidayDao.findById(holidayId);
        admHolidayDao.removeById(holidayId);  
        log4Delete(oriADMHOLIDAY, "0");
    }

    /**
     * 整批上傳假日檔
     *
     * @param holidays 假日檔陣列, holiday, ex: 109010113880X  3
     * @param actor 動作者
     */
    public void uploadHoliday(String[] holidayRows, String actor) throws Exception {
        validateHoliday(holidayRows);
        List<String> addedHolidays = new ArrayList<String>();
        for ( String holidayRow : holidayRows ) {
        	String date = holidayRow.substring(0,7);
            date = String.valueOf(Integer.parseInt(date.substring(0,3))+1911).concat(date.substring(3,7));
            
            String type = holidayRow.substring(12, 13);
            if ( type.charAt(0) == 'W' || type.charAt(0) == 'X' ) {
	            if ( admHolidayDao.findById(date) == null ) {
	                ADMHOLIDAY newHoliday = new ADMHOLIDAY();
	                newHoliday.setADHOLIDAYID(date);
	                newHoliday.setADREMARK("");
	                addedHolidays.add(date);
	                insertHoliday(newHoliday, actor, true);
	            } 
            }
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("holiday", addedHolidays);
        log4Create(map, "0");
    }

    /**
     * 匯入檔案內容檢核
     * @param holidayRows
     * @throws Exception
     */
    private void validateHoliday(String[] holidayRows) throws Exception {
        for ( String holidayRow : holidayRows ) {
            // 資料為此格式: 109010113880X  3
            if ( holidayRow.length() < 13 ) {
                throw new Exception("["+holidayRow+"]"+"資料內容有誤，長度不足");
            }
            try {
            	Integer.parseInt(holidayRow.substring(0, 7));
            } catch (Exception e) {
                throw new Exception("["+holidayRow+"]"+"日期格式錯誤，應該是yyyMMdd");
            }
        }
    }
}
