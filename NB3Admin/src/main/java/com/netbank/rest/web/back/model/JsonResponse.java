package com.netbank.rest.web.back.model;

import java.util.Map;

/**
 * 通用型 Json Response
 *
 * @author 簡哥
 * @version V1.0
 */
public class JsonResponse {
    private String pkey;
    private boolean validated;
    private Map<String, String> errorMessages;

    /**
     * <p>isValidated.</p>
     *
     * @return the validated
     */
    public boolean isValidated() {
        return validated;
    }

    /**
     * <p>Setter for the field <code>validated</code>.</p>
     *
     * @param validated the validated to set
     */
    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    /**
     * <p>Getter for the field <code>errorMessages</code>.</p>
     *
     * @return the errorMessages
     */
    public Map<String, String> getErrorMessages() {
        return errorMessages;
    }

    /**
     * <p>Setter for the field <code>errorMessages</code>.</p>
     *
     * @param errorMessages the errorMessages to set
     */
    public void setErrorMessages(Map<String, String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    /**
     * <p>Getter for the field <code>pkey</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPkey() {
        return pkey;
    }

    /**
     * <p>Setter for the field <code>pkey</code>.</p>
     *
     * @param pkey a {@link java.lang.String} object.
     */
    public void setPkey(String pkey) {
        this.pkey = pkey;
    }
}
