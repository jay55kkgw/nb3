package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmCurrencyDao;
import fstop.orm.po.ADMCURRENCY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>ADMCURRENCYService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class ADMCURRENCYService extends BaseService {
    @Autowired
    private AdmCurrencyDao admCurrencyDao;

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo 第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy 依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param ADCURRENCY 幣別代號
     * @param ADCCYNO 幣別號碼
     * @param ADCCYNAME 幣別中文名稱
     * @return 分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String ADCURRENCY, String ADCCYNO, String ADCCYNAME, String ADCCYCHSNAME, String ADCCYENGNAME) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, ADCURRENCY={}, ADCCYNO={}, ADCCYNAME={}, ADCCYCHSNAME={}, ADCCYENGNAME={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ADCURRENCY), Sanitizer.logForgingStr(ADCCYNO), Sanitizer.logForgingStr(ADCCYNAME), Sanitizer.logForgingStr(ADCCYCHSNAME), Sanitizer.logForgingStr(ADCCYENGNAME));

        log4Query("0", new String[][] { { "ADCURRENCY", ADCURRENCY }, { "ADCCYNO", ADCCYNO }, { "ADCCYNAME", ADCCYNAME }, { "ADCCYCHSNAME", ADCCYCHSNAME }, { "ADCCYENGNAME", ADCCYENGNAME }} );
        return admCurrencyDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, ADCURRENCY, ADCCYNO, ADCCYNAME, ADCCYCHSNAME, ADCCYENGNAME);
    }

    /**
     * 取得幣別資料
     *
     * @param ADCURRENCY 幣別代碼
     * @return 幣別 POJO
     */
    public ADMCURRENCY getByADCURRENCY(String ADCURRENCY) {
        log.debug("getByADCURRENCY ADCURRENCY={}", Sanitizer.logForgingStr(ADCURRENCY));

    	log4Query("0", new String[][] { { "ADCURRENCY", ADCURRENCY }} );

        return admCurrencyDao.findById(ADCURRENCY);
    }

    /**
     * 新增幣別資料
     *
     * @param currency 幣別 POJO
     * @param creator 編輯者
     */
    public void insertCurrency(ADMCURRENCY currency, String creator) {
        currency.setLASTUSER(creator); 
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        currency.setLASTDATE(parts[0]);
        currency.setLASTTIME(parts[1]);

        admCurrencyDao.save(currency);
        log4Create(currency, "0");
    } 

    /**
     * 儲存幣別資料
     *
     * @param currency 幣別 POJO
     * @param editor 編輯者
     */
    public void saveCurrency(ADMCURRENCY currency, String editor) {
        ADMCURRENCY oriADMCURRENCY = admCurrencyDao.findById(currency.getADCURRENCY());
        admCurrencyDao.getEntityManager().detach(oriADMCURRENCY);

        currency.setMSGCODE(oriADMCURRENCY.getMSGCODE());
        currency.setLASTUSER(editor); 

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        currency.setLASTDATE(parts[0]);
        currency.setLASTTIME(parts[1]);

        admCurrencyDao.update(currency);
        log4Update(oriADMCURRENCY, currency, "0");
    }

    /**
     * 刪除幣別資料
     *
     * @param currencyId a {@link java.lang.String} object.
     */
    public void deleteCurrency(String currencyId) {
        ADMCURRENCY oriADMCURRENCY = admCurrencyDao.findById(currencyId);
        admCurrencyDao.removeById(currencyId); 
        log4Delete(oriADMCURRENCY, "0"); 
    }

    /**
     * 用幣別編號查詢
     * @param ADCCYNO
     * @return
     */
    public ADMCURRENCY findByNo(String ADCCYNO){
        List<ADMCURRENCY> list = admCurrencyDao.findByNo(ADCCYNO);
        if(list.size() == 1){
        	log4Query("0");
            return list.get(0);
        }
        return new ADMCURRENCY();
    }
}
