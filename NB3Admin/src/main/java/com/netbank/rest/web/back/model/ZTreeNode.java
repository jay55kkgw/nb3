package com.netbank.rest.web.back.model;

/**
 * <p>ZTreeNode class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class ZTreeNode {
    private String id;
    private String pId;
    private String name;
    private boolean open;
    private boolean checked;
    
    /// <summary>
    /// 是否要顯示勾選
    /// </summary>
    private boolean nocheck;

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * <p>isNocheck.</p>
     *
     * @return the nocheck
     */
    public boolean isNocheck() {
        return nocheck;
    }

    /**
     * <p>Setter for the field <code>nocheck</code>.</p>
     *
     * @param nocheck the nocheck to set
     */
    public void setNocheck(boolean nocheck) {
        this.nocheck = nocheck;
    }

    /**
     * <p>isChecked.</p>
     *
     * @return the checked
     */
    public boolean isChecked() {
        return checked;
    }

    /**
     * <p>Setter for the field <code>checked</code>.</p>
     *
     * @param checked the checked to set
     */
    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    /**
     * <p>isOpen.</p>
     *
     * @return the open
     */
    public boolean isOpen() {
        return open;
    }

    /**
     * <p>Setter for the field <code>open</code>.</p>
     *
     * @param open the open to set
     */
    public void setOpen(boolean open) {
        this.open = open;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>pId</code>.</p>
     *
     * @return the pId
     */
    public String getpId() {
        return pId;
    }

    /**
     * <p>Setter for the field <code>pId</code>.</p>
     *
     * @param pId the pId to set
     */
    public void setpId(String pId) {
        this.pId = pId;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
}
