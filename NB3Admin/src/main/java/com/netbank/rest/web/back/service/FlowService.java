package com.netbank.rest.web.back.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.netbank.rest.web.back.model.FlowChkResult;
import com.netbank.rest.web.back.model.FlowStep;
import com.netbank.rest.web.back.model.FlowSvcModel;
import com.netbank.rest.web.back.model.FlowSvcResult;
import com.netbank.util.DateUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmFlowCurrentDao;
import fstop.orm.dao.AdmFlowDefStepsDao;
import fstop.orm.dao.AdmFlowFinishedDao;
import fstop.orm.dao.AdmFlowHistoryDao;
import fstop.orm.po.ADMFLOWCURRENT;
import fstop.orm.po.ADMFLOWCURRENTIDENTITY;
import fstop.orm.po.ADMFLOWDEFSTEP;
import fstop.orm.po.ADMFLOWFINISHED;
import fstop.orm.po.ADMFLOWHISTORY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>FlowService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class FlowService {
    /**
     * 同意結案
     */
    public final String APPROVE_FLAG = "Y";

    /**
     * 不同意結案
     */
    public final String REJECT_FLAG = "N";

    /**
     * 退回上一關
     */
    public final String REJECT_TO_PREVIOUS = "R";

    /**
     * 退回編輯者
     */
    public final String REJECT_TO_EDIROT = "S";

    /**
     * 刪除結案
     */
    public final String CANCEL_FLAG = "C";

    /**
     * 成功
     */
    public static final String SUCCESS = "0";

    /**
     * 資料庫存取錯誤
     */
    public static final String ERR_DB_ACCESS_ERROR = "0001";

    /**
     * 不支援此節點類型
     */
    public static final String ERR_STEP_TYPE_NOT_SUPPORT = "0002";

    /**
     * 案件已被處理
     */
    public static final String ERR_CASE_PROCESSED = "0003";

    /**
     * 無權處理此案件
     */
    public static final String ERR_CASE_NOT_ALLOWED = "0004";

    /**
     * 找不到該節點
     */
    public static final String ERR_NODE_NOT_FOUND = "0005";

    /**
     * 案件不存在
     */
    public static final String ERR_CASE_NOT_FOUND = "0006";

    /**
     * 非開始或外部節點
     */
    public static final String ERR_NOT_START_OR_EXTERNAL_NODE = "0007";

    /**
     * 系統錯誤
     */
    public static final String ERR_SYSTEM = "0008";

    @Autowired
    private AdmFlowDefStepsDao admFlowDefStepsDao;

    @Autowired
    private AdmFlowCurrentDao admFlowCurrentDao;

    @Autowired
    private AdmFlowHistoryDao admFlowHistoryDao;

    @Autowired
    private AdmFlowFinishedDao admFlowFinishedDao;

    /**
     * <p>Constructor for FlowService.</p>
     */
    public FlowService() {
        
    } 

    /**
     * 啟動一個流程
     *
     * @param fromOId   流程代碼
     * @param caseSN    案件編號
     * @param fromUid   送件者代碼
     * @param fromUName 送件者姓名
     * @param flowData  流程資料
     * @param comments  意見
     * @param flowMemo  流程 MEMO
     * @param todoURL   待辦 URL
     * @param flowId a {@link java.lang.String} object.
     * @return a {@link com.netbank.rest.web.back.model.FlowSvcResult} object.
     */
    public FlowSvcResult startFlow(String flowId, String caseSN, String fromOId, String fromUid, String fromUName, String flowData,
            String comments, String flowMemo, String todoURL) {

        FlowSvcModel model = new FlowSvcModel();
        FlowSvcResult result = new FlowSvcResult();
        FlowStep head;
        
        try {
            head = getFlowStepTree(flowId, ""); 
        } catch (Exception e) {
            log.error("getFlowStepTree error", e);

            return result;
        }

        // 處理節點
        Map<String, FlowStep> resultMap = processStartStepNode(model, flowId, head, fromOId, fromUid, fromUName, caseSN, flowData, flowMemo, comments, todoURL);
        Map.Entry<String, FlowStep> entry = resultMap.entrySet().iterator().next();

        String rt = entry.getKey();
        FlowStep nextStep = entry.getValue();
        
        result.setCaseSN(caseSN);
        result.setStatusCode(rt);
        result.setNextStepId(rt.equalsIgnoreCase(SUCCESS) ? nextStep.getSTEPIDENTITY().getSTEPID() : "");
        result.setNextStepName(rt.equalsIgnoreCase(SUCCESS) ? nextStep.getSTEPNAME() : "");
        result.setIsFinal(false);

        if ( rt == SUCCESS )
            result.setStatusCode(commitDatabase(model));

        return result;
    }

    /**
     * 移動流程
     *
     * @param caseSN            案件編號
     * @param stepId            步驟代碼
     * @param actionOId         動作者單位
     * @param actionUId         動作者代碼
     * @param actionUName       動作者姓名
     * @param actionComments    動作者意見
     * @param flowData          流程資料
     * @param isApproved        是否同意
     * @return                  FlowSvcResult POJO
     */
    public FlowSvcResult moveFlow(String caseSN, String stepId, String actionOId, String actionUId, String actionUName, String actionComments, String flowData, boolean isApproved) {
        FlowSvcModel model = new FlowSvcModel();
        FlowSvcResult result = new FlowSvcResult();
        result.setCaseSN(caseSN);
        result.setIsFinal(false);
        try {
            ADMFLOWCURRENT flowCurrent = admFlowCurrentDao.findByCaseSN(caseSN).get(0);
            FlowStep currStep = getFlowStepTree(flowCurrent.getFLOWID(), flowCurrent.getSTEPID());

            // 案件是否被處理的檢核
            if ( !currStep.getSTEPIDENTITY().getSTEPID().equalsIgnoreCase(stepId) ) {
                result.setStatusCode(ERR_CASE_PROCESSED);
                result.setIsFinal(false);
                return result;
            }

            Map<String, FlowStep> resultMap = processMoveStepNode(model, flowCurrent, flowCurrent.getCURRENTIDENTITY().getBID(), actionOId, actionUId, actionUName, flowData, currStep, flowCurrent.getSTEPCOUNT(), isApproved, actionComments);
            Map.Entry<String, FlowStep> entry = resultMap.entrySet().iterator().next();

            String rt = entry.getKey();
            FlowStep nextStep = entry.getValue();
            
            result.setIsFinal(model.getFlowCurrentDel().size()>0 ? true: false);
            result.setStatusCode(rt);
            result.setNextStepId(nextStep.getSTEPIDENTITY().getSTEPID());
            result.setNextStepName(nextStep.getSTEPNAME());

            if (rt == SUCCESS) {
                result.setStatusCode(commitDatabase(model));
            }
        } catch (Exception e) {
            log.error("moveFlow Error", e);
            result.setStatusCode(ERR_SYSTEM);
        }
    
        return result;
    }

    /**
     * 案件取消
     *
     * @param caseSN            案件編號
     * @param actionOId         動作者單位
     * @param actionUId         動作者代碼
     * @param actionUName       動作者姓名
     * @return                  FlowSvcResult POJO
     */
    public FlowSvcResult cancelFlow(String caseSN, String actionOId, String actionUId, String actionUName) {
        FlowSvcModel model = new FlowSvcModel();
        FlowSvcResult result = new FlowSvcResult();
        result.setCaseSN(caseSN);
        result.setIsFinal(false);

        try {
            result.setCaseSN(caseSN);
            List<ADMFLOWCURRENT> flowCurrents = admFlowCurrentDao.findByCaseSN(caseSN);
            if (flowCurrents.size() == 0) {
                result.setStatusCode(ERR_CASE_NOT_FOUND);
            } else {
                ADMFLOWCURRENT flowCurrent = flowCurrents.get(0);

                // 新增一筆 FlowHistory, 誰同意或拒絕的
                model.getFlowHistory().add(getInsertFlowHistory(actionOId, caseSN, flowCurrent.getFLOWID(), flowCurrent.getSTEPID(), flowCurrent.getSTEPNAME(), actionOId, actionUId, actionUName, CANCEL_FLAG, "", ""));
                model.getFlowCurrentDel().add(getDeleteFlowCurrent(flowCurrent.getCURRENTIDENTITY().getCASESN(), flowCurrent.getCURRENTIDENTITY().getBID()));
                model.getFlowFinished().add(getInsertFlowFinished(flowCurrent, actionOId, actionUId, actionUName, CANCEL_FLAG));
                
                result.setStatusCode(commitDatabase(model));
                result.setIsFinal(true);
            }
        } catch (Exception e) {
            log.error("cancelFlow Error", e);
            result.setStatusCode(ERR_SYSTEM);
        }
        return result;
    }
    /**
     * 判斷同意是否造成流程結案
     *
     * @param caseSN        案件編號
     * @return              true: 結案，false: 尚未結案
     * @throws java.lang.Exception
     */
    public boolean isFinalStep(String caseSN) throws Exception {
        FlowSvcModel model = new FlowSvcModel();
        ADMFLOWCURRENT flowCurrent = admFlowCurrentDao.findByCaseSN(caseSN).get(0);
        
        FlowStep currStep = getFlowStepTree(flowCurrent.getFLOWID(), flowCurrent.getSTEPID());
        Map<String, FlowStep> resultMap = processMoveStepNode(model, flowCurrent, flowCurrent.getCURRENTIDENTITY().getBID(), flowCurrent.getORGID(), "", "", flowCurrent.getFLOWDATA(), currStep, flowCurrent.getSTEPCOUNT(), true, "");
        Map.Entry<String, FlowStep> entry = resultMap.entrySet().iterator().next();
        if ( entry.getKey().equalsIgnoreCase(SUCCESS) ) {
            // 只有當流程結束時才會 Del ADMFLOWCURRENT
            // 這裹不執行 commitDatabase();
            return model.getFlowCurrentDel().size()>0;
        } else {
            throw new Exception(entry.getKey());
        }
    }

    /**
     * 是否是我的案件，用 uid + roles 做判斷，回覆[true|false]+StepId
     *
     * @param caseSN        案件編號
     * @param uid           使用者代碼
     * @param userRoles     使用者角色
     * @return              FlowChkResult 檢核結果
     * @param oid a {@link java.lang.String} object.
     */
    public FlowChkResult IsMyCase(String caseSN, String oid, String uid, Set<String> userRoles) {
          //20191115 - steven Log Forging\路徑 11:
        log.info("caseSN={}, oid={}, uid={}",Sanitizer.logForgingStr(caseSN),Sanitizer.logForgingStr(oid),Sanitizer.logForgingStr(uid));
        FlowChkResult result = new FlowChkResult();
        result.setCaseSN(caseSN);
        result.setMyCase(false);

        List<ADMFLOWCURRENT> currents = admFlowCurrentDao.findByCaseSN(caseSN);
        if ( currents.size() == 0 ) {
            //20191115 - steven Log Forging\路徑 8:
            log.info("{} not found in ADMCURRENT", Sanitizer.logForgingStr(caseSN));
        } else {
            ADMFLOWCURRENT current = currents.get(0);
            String toOid = current.getTOOID();
            String toType = current.getTOTYPE();
            String toUID = current.getTOUID();          // 可能是 USER or ROLE or ROLE LIST

            result.setMatchStepId(current.getSTEPID());
            //20191120-Danny-Stored Log Forging\路徑 1:
            log.info("toOid={}, toType={}, toUID={}", Sanitizer.logForgingStr(toOid), Sanitizer.logForgingStr(toType), Sanitizer.logForgingStr(toUID));
            if ( toType.equalsIgnoreCase("U") && toOid.equalsIgnoreCase(oid) && toUID.equalsIgnoreCase(uid) ) {
                log.info("user comparison success");
                result.setMyCase(true);
            } else if ( toType.equalsIgnoreCase("R") && toOid.equalsIgnoreCase(oid) ) {
                String[] toRoles = toUID.split(",");
                for ( String toRole : toRoles ) {
                    for ( String userRole : userRoles ) {
                        if ( userRole.equalsIgnoreCase(toRole) ) {
                            log.info("role comparison success");
                            result.setMyCase(true);
                        }
                    }
                }
            } else {
                log.info("general comparison fail");
            }
        }
        return result;
    }

    /**
     * 依案號取得案件歷程
     *
     * @param caseSN        案件編號
     * @return a {@link java.util.List} object.
     */
    public List<ADMFLOWHISTORY> getFlowHistory(String caseSN) {
        return admFlowHistoryDao.findByCaseSN(caseSN);
    }

    /**
     * 依單位及角色取得待辦
     *
     * @param oId           單位代碼
     * @param roles         角色清單
     * @return a {@link java.util.List} object.
     */
    public List<ADMFLOWCURRENT> getTodo(String oId, String[] roles) {
        return admFlowCurrentDao.findByRole(oId, roles);
    }

    /**
     * 依流程代碼及單位代號取得待辦
     * @param oId           單位代碼
     * @param flowId        流程代碼
     * @return
     */
    public List<ADMFLOWCURRENT> getTodoByFlowId(String oId, String flowId) {
        return admFlowCurrentDao.findByFlowId(oId, flowId);
    }

    /**
     * 依案件編號取得案件資料
     * @param caseSN        案件編號
     * @return
     */
    public ADMFLOWCURRENT getTodoByCaseSN(String caseSN) {
        ADMFLOWCURRENTIDENTITY id = new ADMFLOWCURRENTIDENTITY();
        id.setCASESN(caseSN);
        id.setBID(1);
        return admFlowCurrentDao.findById(id);
    }


    /**
     * 呼叫 DAO 的方法，做新增/異動/刪除 資料表的動作
     * @return
     */
    private String commitDatabase(FlowSvcModel model) {
        List<ADMFLOWCURRENT> flowCurrentAdd = model.getFlowCurrentAdd();
        List<ADMFLOWCURRENT> flowCurrentUpd = model.getFlowCurrentUpd();
        List<ADMFLOWCURRENT> flowCurrentDel = model.getFlowCurrentDel();
        List<ADMFLOWHISTORY> flowHistory = model.getFlowHistory();
        List<ADMFLOWFINISHED> flowFinished = model.getFlowFinished();
        int i=1;
        for(ADMFLOWCURRENT o : flowCurrentAdd ) {
            //TODO：DB2 自動取號未解決，自己加一，主鍵是案號＋BID
            ADMFLOWCURRENTIDENTITY c = o.getCURRENTIDENTITY();
            c.setBID(i);
            o.setCURRENTIDENTITY(c);
            admFlowCurrentDao.save(o);
            i++;
        }
        for(ADMFLOWCURRENT o : flowCurrentUpd ) {
            admFlowCurrentDao.update(o);
        }
        for(ADMFLOWCURRENT o : flowCurrentDel ) {
            admFlowCurrentDao.delete(o);
        }
        for(ADMFLOWHISTORY o : flowHistory ) {
            admFlowHistoryDao.save(o);
        }
        for(ADMFLOWFINISHED o : flowFinished ) {
            admFlowFinishedDao.save(o);
        }
        return SUCCESS;
    }

    /**
     * 處理開始節點
     * @param model             
     * @param flowId            流程代碼
     * @param currStepNode      目前流程節點
     * @param actionOId         動作者單位代碼
     * @param actionUId         動作者員工編號
     * @param actionUName       動作者員工姓名
     * @param caseSN            案件編號
     * @param flowData          流程資料
     * @param flowMemo          流程備註
     * @param comments          意見
     * @param todoURL           待辦URL
     * @return                  HashMap, Key:結果代碼, Value:下一節點物件
     */
    private Map<String, FlowStep> processStartStepNode(FlowSvcModel model, String flowId, FlowStep currStepNode, String actionOId, String actionUId, String actionUName, String caseSN, String flowData, String flowMemo, String comments, String todoURL) {
        Map<String, FlowStep> result = new HashMap<String, FlowStep>();
        FlowStep nextStep = currStepNode.getNextFlowSteps().get(0);

        if (nextStep.getSTEPTYPE().equalsIgnoreCase("S")) {
            // 步驟為 "群組:Group" G
            // 步驟為 "角色:Role" R
            // 步驟為 "使用者:User" U
            // ToType 已經可以判斷, 直接新增一筆到 flowCurrrent
            ADMFLOWCURRENT pCurrent  = getInsertFlowCurrent(actionOId, actionUId, actionUName, caseSN, flowData, nextStep, 1, flowMemo, todoURL);
            model.getFlowCurrentAdd().add(pCurrent);

            ADMFLOWHISTORY pHistory = getInsertFlowHistory(actionOId, caseSN, flowId, nextStep.getNEXTSTEPID(), nextStep.getSTEPNAME(), actionOId, actionUId, actionUName, "", comments, flowData);
            model.getFlowHistory().add(pHistory);
            result.put(SUCCESS, nextStep);

            return result;
        } else {
            nextStep = null;
            result.put(ERR_STEP_TYPE_NOT_SUPPORT, nextStep);
            // nextStep.StepType == "C", 目前只有二種
            return result;
        }
    }

    /**
     * 處理流程移動一步
     * @param model
     * @param flowCurrent       目前案件 POJO
     * @param branchId          目前案件分支號（並簽時才會有，先預留）
     * @param actionOId         動作者單位代碼
     * @param actionUId         動作者員工編號
     * @param actionUName       動作者員工姓名
     * @param flowData          流程資料
     * @param currStepNode      目前節點
     * @param currStepCount     目前節點執行次數
     * @param isApproved        同意記號 "Y"：同意，"N"：退回
     * @param comments          意見
     * @return                  HashMap, Key:結果代碼, Value:下一節點物件
     */
    private Map<String, FlowStep> processMoveStepNode(FlowSvcModel model, ADMFLOWCURRENT flowCurrent, int branchId, String actionOId, String actionUId, String actionUName, String flowData, FlowStep currStepNode, int currStepCount, boolean isApproved, String comments) {    
        Map<String, FlowStep> resultMap = new HashMap<String, FlowStep>();
        boolean isConditionFlow = currStepNode.getNEXTSTEPID().isEmpty() ? true : false;
        FlowStep nextStep = null;

        if ( isConditionFlow )
        {
            // ConditionFlow: Y Part : N Part
            String nextStepId = isApproved ? currStepNode.getNEXTSTEPCOND().split(":")[0] : currStepNode.getNEXTSTEPCOND().split(":")[1];
            
            for ( FlowStep s : currStepNode.getNextFlowSteps() ) {
                if ( s.getSTEPIDENTITY().getSTEPID().equalsIgnoreCase(nextStepId) ) {
                    nextStep = s;
                    break;
                }
            }
        } else {
            nextStep = currStepNode.getNextFlowSteps().get(0);
        }

        String approveFlag = isApproved ? APPROVE_FLAG : REJECT_FLAG;

        if ( isEndNode(nextStep.getSTEPTYPE())) {
            // 新增一筆 FlowHistory, 端點       
            ADMFLOWHISTORY pHistory = getInsertFlowHistory(flowCurrent.getORGID(), flowCurrent.getCURRENTIDENTITY().getCASESN(), flowCurrent.getFLOWID(), nextStep.getSTEPIDENTITY().getSTEPID(), nextStep.getSTEPNAME(), actionOId, actionUId, actionUName, approveFlag, comments, flowData);
            model.getFlowHistory().add(pHistory);

            // 刪除 FlowCurrent
            ADMFLOWCURRENT pCurrent = getDeleteFlowCurrent(flowCurrent.getCURRENTIDENTITY().getCASESN(), flowCurrent.getCURRENTIDENTITY().getBID());
            model.getFlowCurrentDel().add(pCurrent);

            // 新增 FlowFinished
            ADMFLOWFINISHED pFinished = getInsertFlowFinished(flowCurrent, actionOId, actionUId, actionUName, approveFlag);
            model.getFlowFinished().add(pFinished);

            resultMap.put(SUCCESS, nextStep);
            return resultMap;
        } else {
            // 下一節點支援類型
            //  "群組:Group" G, "角色:Role" R, "使用者:User" U
            if (nextStep.getSTEPTYPE().equalsIgnoreCase("S")) {
                // 目前只支援串簽
                // 下一關為系統節點且節點擁有者為 Editor, 則退回給原編輯者
                if ( nextStep.getSTEPOWNERTYPE().equalsIgnoreCase("S")) {
                    if ( nextStep.getSTEPOWNER().equalsIgnoreCase("Editor")) {
                        nextStep.setSTEPOWNERTYPE("U");
                        nextStep.setSTEPOWNER(flowCurrent.getEDITORUID());
                    }
                }
                ADMFLOWCURRENT pCurrent = getUpdateFlowCurrent(flowCurrent.getORGID(), flowCurrent.getCURRENTIDENTITY().getCASESN(), branchId, actionOId, actionUId, actionUName, flowData, nextStep, 1, currStepNode.getSTEPIDENTITY().getSTEPID());
                model.getFlowCurrentUpd().add(pCurrent);

                // 新增一筆 FlowHistory, 誰同意或拒絕的       
                ADMFLOWHISTORY pHistory = getInsertFlowHistory(flowCurrent.getORGID(), flowCurrent.getCURRENTIDENTITY().getCASESN(), flowCurrent.getFLOWID(), nextStep.getSTEPIDENTITY().getSTEPID(), nextStep.getSTEPNAME(), actionOId, actionUId, actionUName, approveFlag, comments, flowData);
                model.getFlowHistory().add(pHistory);

                resultMap.put(SUCCESS, nextStep);
                return resultMap;
            }
            else
            {
                resultMap.put(ERR_STEP_TYPE_NOT_SUPPORT, nextStep);
                
                return resultMap;
            }
        }
    }

    /**
     * 
     * @param flowId
     * @param findStepId
     * @return
     * @throws Exception
     */
    private FlowStep getFlowStepTree(String flowId, String findStepId) throws Exception {
        
        List<ADMFLOWDEFSTEP> steps = admFlowDefStepsDao.findSteps(flowId);

        return getFlowStepTree(steps, findStepId);
    }

    /**
     * 是否為開始節點
     * @param stepType
     * @return
     */
    private boolean isStartNode(String stepType) {
        return "T0".equalsIgnoreCase(stepType);
    }

    /**
     * 是否為結束節點
     * @param stepType
     * @return
     */
    private boolean isEndNode(String stepType) {
        return "T1".equalsIgnoreCase(stepType);
    }

    /**
     * 是否為幽靈節點(即沒有頭尾)
     * @param stepType
     * @return
     */
    private boolean isGhostNode(String stepType) {
        return "G".equalsIgnoreCase(stepType);
    }

    /**
     * 建立流程串列
     * @param steps
     * @param currStep
     */
    private void buildStepsTree(List<FlowStep> steps, FlowStep currStep) {
        String nextStepId = currStep.getNEXTSTEPID();
        String nextStepCondition = currStep.getNEXTSTEPCOND();

        if ( nextStepId.isEmpty() && nextStepCondition.isEmpty()) {
            // it's final step
            return;
        }

        if ( nextStepId.isEmpty() ) {
            List<String> cNextStepId = new ArrayList<String>();       // 存放可能的 StepId

            // 由條件判斷下一關卡, 3:4
            String[] conds = nextStepCondition.split(":");
            for (String cond : conds) {
                cNextStepId.add(cond);
            }

            for (FlowStep step : steps) {
                if (cNextStepId.contains(step.getSTEPIDENTITY().getSTEPID())) {
                    // 一定要有以下的判斷, 不然會造成無限迴圈 & Stack Overflow
                    if (!currStep.getNextFlowSteps().contains(step)) {
                        currStep.getNextFlowSteps().add(step);
                        step.getPrevFlowSteps().add(currStep);
                        buildStepsTree(steps, step);
                    }
                }
            }
        } else {
            List<String> p = Arrays.asList(nextStepId.split(","));       // 存放可能的 StepId, StepId=3,4

            for (FlowStep step : steps) {
                if (p.contains(step.getSTEPIDENTITY().getSTEPID())) {
                    currStep.getNextFlowSteps().add(step);
                    step.getPrevFlowSteps().add(currStep);
                    buildStepsTree(steps, step);
                }
            }
        }
    }

    /**
     * 
     * @param admSteps
     * @param findStepId
     * @return
     */
    private FlowStep getFlowStepTree(List<ADMFLOWDEFSTEP> admSteps, String findStepId) throws Exception {

        List<FlowStep> steps = new ArrayList<FlowStep>(admSteps.size());
        FlowStep head = null;                                       // 只會有一個開始節點, 不然 StartFlow 不知從何開始
        List<FlowStep> tails = new ArrayList<FlowStep>();           // 可以有多個結束節點
            
        FlowStep currNode = null;
        for (ADMFLOWDEFSTEP admStep : admSteps) {
            FlowStep flowStep = new FlowStep(admStep);

            // 有指定要找尋的 StepGuid
            if (!findStepId.isEmpty())
                if (admStep.getSTEPIDENTITY().getSTEPID().equalsIgnoreCase(findStepId))
                    currNode = flowStep;

            // 找到 head / foot 節點
            if (isStartNode(flowStep.getSTEPTYPE()))
                head = flowStep;

            if (isEndNode(flowStep.getSTEPTYPE()))
                tails.add(flowStep);
                
            steps.add(flowStep);
        }

        if (head == null || tails.size() == 0)
            throw new Exception("流程定義錯誤");

        buildStepsTree(steps, head);

        for (FlowStep step : steps)
        {
            if (step == head)
            {
                if (step.getNextFlowSteps().size() == 0) {
                    String msg = String.format("StepId:%s 沒有後節點", step.getSTEPIDENTITY().getSTEPID()); 
                    throw new Exception(msg);
                }
            } else if (tails.contains(step)) {
                if (step.getPrevFlowSteps().size() == 0) {
                    String msg = String.format("StepId:%s 沒有前節點", step.getSTEPIDENTITY().getSTEPID());
                    throw new Exception(msg);
                }
            } else {
                if (step.getNextFlowSteps().size() == 0 || step.getPrevFlowSteps().size() == 0) {
                    if (!isGhostNode(step.getSTEPTYPE())) {
                        String msg = String.format("StepId:%s 沒有前節點或後節點", step.getSTEPIDENTITY().getSTEPID());
                        throw new Exception(msg);
                    }
                }
            }
        }

        List<String> nodePools = new ArrayList<String>();        //已 Dump 的節點代號, 用來預防無窮迴圈之 stack overflow
        dumpFlowStepTree(head, nodePools);

        if ( currNode != null ) {
            // 要找特定步驟, 所以回傳特定步驟的 pointer
            return currNode;
        } else {
            // 不找特定步驟, 所以回根步驟
            return head;
        }
    }

    /**
     * dump 流程定義
     * @param currStep
     * @param nodePools
     */
    private void dumpFlowStepTree(FlowStep currStep, List<String> nodePools) {
        String msg = "flowId=%s, stepId=%s, stepName=%s, stepType=%s, stepOwnerType=%s, stepOwner=%s, stepCount=%d, nextStepId=%s, nextStepCond=%s";
        msg = String.format(msg, currStep.getSTEPIDENTITY().getFLOWID(), currStep.getSTEPIDENTITY().getSTEPID(), currStep.getSTEPNAME(), currStep.getSTEPTYPE(), currStep.getSTEPOWNERTYPE(), currStep.getSTEPOWNER(), currStep.getSTEPCOUNT(), currStep.getNEXTSTEPID(), currStep.getNEXTSTEPCOND());
        log.debug(msg);
        nodePools.add(currStep.getSTEPIDENTITY().getSTEPID());

        for (FlowStep nextStep : currStep.getNextFlowSteps()) {
            if (!nodePools.contains(nextStep.getSTEPIDENTITY().getSTEPID()))
            dumpFlowStepTree(nextStep, nodePools);
        }
    }

    /**
     * 取得新增 ADMFLOWCURRENT POJO
     * @param fromOId
     * @param fromUId
     * @param fromUName
     * @param caseSN
     * @param flowData
     * @param nextStep
     * @param stepCount
     * @param flowMemo
     * @param todoURL
     * @return
     */
    private ADMFLOWCURRENT getInsertFlowCurrent(String fromOId, String fromUId, String fromUName, String caseSN, String flowData, FlowStep nextStep, int stepCount, String flowMemo, String todoURL) {
        ADMFLOWCURRENT p = new ADMFLOWCURRENT();

        String toOrgId;
        String toUserId;
        if ( nextStep.getSTEPOWNER().indexOf(":") > 0 ) {
            String[] tmp = nextStep.getSTEPOWNER().split(":");
            toOrgId = tmp[0];
            toUserId = tmp[1];
        }
        else {
            toOrgId = fromOId;
            toUserId = nextStep.getSTEPOWNER();
        }

        ADMFLOWCURRENTIDENTITY pk = new ADMFLOWCURRENTIDENTITY();
        pk.setCASESN(caseSN);
        pk.setBID(null);

        p.setCURRENTIDENTITY(pk);
        p.setORGID(fromOId);
        p.setFLOWID(nextStep.getSTEPIDENTITY().getFLOWID());
        p.setSTEPID(nextStep.getSTEPIDENTITY().getSTEPID());
        p.setSTEPNAME(nextStep.getSTEPNAME());
        p.setSTEPCOUNT(stepCount);
        p.setEDITOROID(fromOId);
        p.setEDITORUID(fromUId);
        p.setEDITORUNAME(fromUName);
        p.setFROMOID(fromOId);
        p.setFROMUID(fromUId);
        p.setFROMUNAME(fromUName);
        p.setTOOID(toOrgId);
        p.setTOUID(toUserId);
        p.setTOUNAME(toUserId);
        p.setTOTYPE(nextStep.getSTEPOWNERTYPE());
        p.setFLOWDATA(flowData);
        p.setFLOWMEMO(flowMemo);
        p.setTODOURL(todoURL);

        String[] datetimeParts = DateUtils.getCurrentDateTimeParts();

        p.setCREATEDATE(datetimeParts[0]);
        p.setCREATETIME(datetimeParts[1]);

        return p;
    }

    /**
     * 取得異動 ADMFLOWCURRENT 的 POJO
     * @param orgId
     * @param caseSN
     * @param branchId
     * @param actionOId
     * @param actionUId
     * @param actionUName
     * @param flowData
     * @param nextStep
     * @param stepCount
     * @param currStepId
     * @return
     */
    private ADMFLOWCURRENT getUpdateFlowCurrent(String orgId, String caseSN, int branchId, String actionOId, String actionUId, String actionUName, String flowData, FlowStep nextStep, int stepCount, String currStepId) {
        // nextStep 要跨組織, 會以 toUnino:toStepOwner 的型式表示
        String toOId;
        String toUId;
        if ( nextStep.getSTEPOWNER().indexOf(":") > 0 ) {
            String[] tmp = nextStep.getSTEPOWNER().split(":");
            toOId = tmp[0];
            toUId = tmp[1];
        }
        else {
            toOId = orgId;
            toUId = nextStep.getSTEPOWNER();
        }

        ADMFLOWCURRENT p = admFlowCurrentDao.findByCaseSNBid(caseSN, branchId).get(0);

        p.setSTEPID(nextStep.getSTEPIDENTITY().getSTEPID());
        p.setSTEPNAME(nextStep.getSTEPNAME());
        p.setFROMOID(actionOId);
        p.setFROMUID(actionUId);
        p.setFROMUNAME(actionUName);
        p.setTOOID(toOId);
        p.setTOUID(toUId);
        p.setTOUNAME(toUId);
        p.setTOTYPE(nextStep.getSTEPOWNERTYPE());
        p.setSTEPCOUNT(stepCount);
        p.setFLOWDATA(flowData);

        return p;
    }

    /**
     * 取得新增 ADMFLOWHISTORY 的參數
     * @param orgId
     * @param caseSN
     * @param flowId
     * @param stepId
     * @param stepName
     * @param actionOId
     * @param actionUId
     * @param actionUName
     * @param approvedFlag
     * @param comments
     * @param flowData
     * @return ADMFLOWHISTORY POJO
     */
    private ADMFLOWHISTORY getInsertFlowHistory(String orgId, String caseSN, String flowId, String stepId, String stepName, String actionOId, String actionUId, String actionUName, String approvedFlag, String comments, String flowData) {
        
        ADMFLOWHISTORY p = new ADMFLOWHISTORY();
        p.setORGID(orgId);
        p.setCASESN(caseSN);
        p.setFLOWID(flowId);
        p.setSTEPID(stepId);
        p.setSTEPNAME(stepName);
        p.setACTIONOID(actionOId);
        p.setACTIONUID(actionUId);
        p.setACTIONUNAME(actionUName);
        p.setCOMMENTS(comments);
        p.setFLOWDATA(flowData);
        p.setAPPROVEFLAG(approvedFlag);

        String[] datetimeParts = DateUtils.getCurrentDateTimeParts();

        p.setCREATEDATE(datetimeParts[0]);
        p.setCREATETIME(datetimeParts[1]);

        return p;
    }

    /**
     * 取得要刪除的 ADMFLOWCURRENT POJO
     * @param caseSN
     * @param branchId
     * @return
     */
    private ADMFLOWCURRENT getDeleteFlowCurrent(String caseSN, int branchId) {
        return admFlowCurrentDao.findByCaseSNBid(caseSN, branchId).get(0);
    }

    /**
     * 取得要新增的 ADMFLOWFINISHED POJO
     * @param current
     * @param approveOId
     * @param approveUId
     * @param approveUName
     * @param approveFlag
     * @return
     */
    private ADMFLOWFINISHED getInsertFlowFinished(ADMFLOWCURRENT current, String approveOId, String approveUId, String approveUName, String approveFlag) {
        ADMFLOWFINISHED p = new ADMFLOWFINISHED();
        
        p.setORGID(current.getORGID());
        p.setCASESN(current.getCURRENTIDENTITY().getCASESN());
        p.setFLOWID(current.getFLOWID());
        p.setSTEPID(current.getSTEPID());
        p.setEDITOROID(current.getEDITOROID());
        p.setEDITORUID(current.getEDITORUID());
        p.setEDITORUNAME(current.getEDITORUNAME());
        p.setAPPROVEROID(approveOId);
        p.setAPPROVERUID(approveUId);
        p.setAPPROVERUNAME(approveUName);
        p.setAPPROVEFLAG(approveFlag);
        p.setCASESTARTDATE(current.getCREATEDATE());
        p.setCASESTARTTIME(current.getCREATETIME());

        String[] datetimeParts = DateUtils.getCurrentDateTimeParts();

        p.setCASEENDDATE(datetimeParts[0]);
        p.setCASEENDTIME(datetimeParts[1]);

        return p;
    }
}
