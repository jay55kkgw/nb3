package com.netbank.rest.web.back.controller;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.TreeMap;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.B201ViewModel;
import com.netbank.rest.web.back.service.ADMMSGCODEService;
import com.netbank.rest.web.back.service.NB3SYSOPService;
import com.netbank.rest.web.back.service.TXNLOGService;
import com.netbank.rest.web.back.service.TXNTWSCHPAYDATAService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import fstop.aop.Authorize;
import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.NB3SYSOPGROUP;
import fstop.orm.po.TXNLOG;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;


/**
 * 交易管理查詢
 * 
 * @author Alison
 */
@Controller
@Slf4j
@RequestMapping("/B202")
public class B202Controller {
    @Autowired
    private TXNLOGService txnLOGService;

    @Autowired
    private NB3SYSOPService nb3SYSOPService;

    @Autowired
    private TXNTWSCHPAYDATAService txnTWSchPayDataService;

    @Autowired
    private ADMMSGCODEService admMSGCODEService;

    /**
     * 取得查詢頁
     * 
     * @param model
     * @return
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd 00:00:00");
        DateFormat endDateFormat = new SimpleDateFormat("yyyy/MM/dd 23:59:59");
        Date date = new Date();
        String now = dateFormat.format(date);
        String endDate= endDateFormat.format(date);
        
        model.addAttribute("startDate", now);
        model.addAttribute("endDate", endDate);

        List<NB3SYSOPGROUP> groups = nb3SYSOPService.getLeafGroups();
        model.addAttribute("groups", groups);
        
        List<NB3SYSOP> adopids = nb3SYSOPService.getAllOrderByADOPID();

        model.addAttribute("adopids", adopids);

        return "B202/index";
    }


    /**
     * 分頁查詢
     * @param viewModel     B202 輸入頁 View Model
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value = "/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    public @ResponseBody JQueryDataTableResponse query(@ModelAttribute @Valid B201ViewModel viewModel, HttpServletRequest request, HttpServletResponse response) {
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        viewModel.setStartDate(viewModel.getStartDate()+":00");
        viewModel.setEndDate(viewModel.getEndDate()+":00");
        
        Page page = txnLOGService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
                jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), viewModel.getStartDate(), viewModel.getEndDate(), 
                viewModel.getAdOPGroup(), viewModel.getAdExCode(), viewModel.getLoginType(), viewModel.getAdUserId(),
                viewModel.getAdTxAcno(), viewModel.getAdOPName(), viewModel.getAdUserIp(), viewModel.getUserName(), viewModel.getAdopid());

        List<NB3SYSOP> menus = nb3SYSOPService.getAllMenus();
        List<TXNLOG> txnLogs = (List<TXNLOG>) page.getResult();
        List<TXNLOG> stxnLogs = new ArrayList<TXNLOG>();
        for (TXNLOG txnLog : txnLogs ) {
            // 把交易代碼改成 "交易代碼，交易中文"
            Optional<NB3SYSOP> sysop = menus.stream().filter(p->p.getADOPID().equalsIgnoreCase(txnLog.getADOPID())).findFirst();
            if ( sysop.isPresent() ) {
                txnLog.setADOPID(txnLog.getADOPID()+","+sysop.get().getADOPNAME());
            }

            TXNLOG stxnLog = new TXNLOG();
            Sanitizer.escape4Class(txnLog, stxnLog);
            stxnLogs.add(stxnLog);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), stxnLogs);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得 JsonData Dialog
     * @param id
     * @param model
     * @return
     * @throws ScriptException 
     */
    @PostMapping(value="/JsonData/{id}")
    public String jsonData(@PathVariable String id, ModelMap model) throws ScriptException {
        TXNLOG log = txnLOGService.getById((String)Sanitizer.escapeHTML(id));
        Gson gson = new Gson();
            
        String adContent = log.getADCONTENT();
        if(adContent == null || adContent.isEmpty()) {
        	adContent="{}";
        }
        HashMap<String, Object> items = (HashMap<String, Object>)gson.fromJson(adContent, HashMap.class);
        Map<String, Object> mapItems = new HashMap<String, Object>();
        
        // 取得 javascript 運算式, 會用到變數 FGTXDATE,CMDATE
        // 所有 EXPRESSION 會用到的變數都要列入, 因為要用 replaceALL 換值
        // N070A.VAR=FGTXDATE,CMDATE
        // N070A.CMDATE.EXPRESSION='FGTXDATE'==='1'?CMDATE: 
        
        Properties props = loadProperties();
        Properties hidingProps = loadHidingProperties();
        
        // 取得變數名稱 FGTXDATE, 多個以 "," 分隔
        String varsTmp = hidingProps.getProperty(log.getADOPID()+".VAR");
        List<String> vars = new ArrayList<String>();
        if ( varsTmp != null && !varsTmp.isEmpty() ) {
        	vars = Arrays.asList(varsTmp.split(","));
        }
        
        // loop 所有 ADContent 的內容
        
        for (Map.Entry m : items.entrySet()) {
        	String key = (String)m.getKey();        	
        	String nameKey = log.getADOPID()+"."+key;
        	
        	// 顯示名稱轉換
        	String dispName=key;
        	if ( props.containsKey(nameKey) ) {
        		dispName = (String)props.getProperty(nameKey);
        	}
        	
        	Object dispVal = null;
        	dispVal = m.getValue();

        	if ( vars.contains(key) ) {
        		// 需做值的轉換, 值是透過三元運算式運算而得
    			dispVal = getEvalValue(log.getADOPID(), key, vars, items, hidingProps);
        	}
        	
    		mapItems.put(dispName, dispVal);
        }
        
        //降冪排序
        Map<String, Object> reverseSortedMap = new TreeMap<String, Object>(Collections.reverseOrder());
        reverseSortedMap.putAll(mapItems);
        
        model.addAttribute("items", reverseSortedMap);
        return "B201/dataPartial";
    }

    /**
     * 以三元運算式取值
     * @param adOpId		: 交易代碼, 例如: N070A
     * @param key			: 要換值的變數名稱, 例如: CMDATE
     * @param vars			: 變數名稱集合
     * @param items			: ADContent 內變數名稱及值 HashMap
     * @param hidingProps	: 要換值的屬性檔
     * @return				: 運算過的值
     */
    private String getEvalValue(String adOpId, String key, List<String> vars, HashMap<String, Object> items, Properties hidingProps) {
    	// N070A.CMDATE.EXPRESSION='FGTXDATE'==='1'?CMDATE: 
    	String keyExpression = adOpId+"."+key+".EXPRESSION";
    	
    	// 未處理的值
    	String dispValue = (items.get(key)).toString();
    	
    	if ( hidingProps.containsKey(keyExpression) ) {
    		String valueExp = hidingProps.getProperty(keyExpression);
    		
    		// 把運算式變數換成值, 要所有的變數內容換成變數值 ex: 'FGTXDATE'==='1'?CMDATE:  換成 '1'==='1'?20200101: 
            valueExp = replaceVar(vars, items, valueExp);
            // for ( String var: vars ) {    			
    		// 	valueExp = valueExp.replaceAll(var, (items.get(var)).toString());
    		// }
    		
    		// 處理三元運算式 '1'==='1'?20200101: 
    		if ( valueExp.indexOf("?")>-1) {
    			String eval = valueExp.split("\\?")[0];
            	String[] valueParts = valueExp.split("\\?")[1].split(":");
            	
            	ScriptEngineManager mgr = new ScriptEngineManager();
                ScriptEngine engine = mgr.getEngineByName("JavaScript");
                
                String result = "false";
				try {
					result = engine.eval(eval).toString();
				} catch (ScriptException e) {
					log.error("ScriptException", e);
					return dispValue;
				}
            	if (Boolean.parseBoolean(result) ) {
            		return valueParts[0];
            	} else {
            		return valueParts[1];
            	}
    		} else {
    			return dispValue;
    		}
    	} else {
    		return dispValue;
    	}
    }

    /**
     * 
     * @param id
     * @param model
     * @return
     */
    @PostMapping(value = "/MsgDialog/{id}")
    public String msgDialog(@PathVariable String id, ModelMap model) {
        String MsgOut = "訊息說明 : " + admMSGCODEService.getMsgIn(id);
        String MsgIn = "客戶訊息 : " + admMSGCODEService.getMsg(id);
        model.addAttribute("ErrMsg", MsgOut + "<br>" + MsgIn);
        return "B202/msgPartial";
    }
    /**
     * 載入欄位對應參數
     * @return
     */
    private Properties loadProperties() {
    	Properties props = new Properties();
    	
        try {
            ClassLoader classLoader = getClass().getClassLoader();
             
            props.load(new InputStreamReader(classLoader.getResourceAsStream("B201-mapping.properties"), "UTF-8"));
        } catch (IOException ex) {
            //20191115-Eric-Information Exposure Through an Error Message\路徑 27:
            //ex.printStackTrace();
            log.error("loadProperties error", ex);
        }
        return props;
    }
    
    /**
     * 載入欄位隱藏設定
     * @return
     */
    private Properties loadHidingProperties() {
    	Properties props = new Properties();
    	
        try {
            ClassLoader classLoader = getClass().getClassLoader();
             
            props.load(new InputStreamReader(classLoader.getResourceAsStream("B201-hiding.properties"), "UTF-8"));
        } catch (IOException ex) {
            //20191115-Eric-Information Exposure Through an Error Message\路徑 27:
            //ex.printStackTrace();
            log.error("loadHidingProperties error", ex);
        }
        return props;
    }
    
    private String fieldTranslator(Properties props, String telNo, String key, String value) {
        String pKey = telNo+"."+key+"."+value;
        if ( props.containsKey(pKey) ) {
        	//Stored XSS
        	String pp = props.getProperty(pKey);
        	pp = (String)Sanitizer.escapeHTML(pp);
            return pp;
        } else {
            return "";
        }
    }

    private String replaceVar(List<String> vars, Map<String, Object> map, String valueExp){
        log.debug("replaceVar start....");
        
        for(String var: vars){
            if(map.containsKey(var)){
                valueExp = valueExp.replace(var, new String().valueOf(map.get(var)));
            }
        }
    
        return valueExp;
    }
}