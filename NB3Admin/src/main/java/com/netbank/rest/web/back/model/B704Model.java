package com.netbank.rest.web.back.model;

/**
 * <p>B704Model class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class B704Model {
    private String yearS;
    private String monthS;

    /**
     * <p>Getter for the field <code>yearS</code>.</p>
     *
     * @return String return the yearS
     */
    public String getYearS() {
        return yearS;
    }

    /**
     * <p>Setter for the field <code>yearS</code>.</p>
     *
     * @param yearS the yearS to set
     */
    public void setYearS(String yearS) {
        this.yearS = yearS;
    }

    /**
     * <p>Getter for the field <code>monthS</code>.</p>
     *
     * @return String return the monthS
     */
    public String getMonthS() {
        return monthS;
    }

    /**
     * <p>Setter for the field <code>monthS</code>.</p>
     *
     * @param monthS the monthS to set
     */
    public void setMonthS(String monthS) {
        this.monthS = monthS;
    }

}
