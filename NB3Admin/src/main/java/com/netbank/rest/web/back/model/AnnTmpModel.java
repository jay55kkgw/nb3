package com.netbank.rest.web.back.model;

import java.util.Date;

import javax.validation.constraints.NotEmpty;

import fstop.orm.po.ADMANNTMP;

/**
 * 個人訊息公告內容刊登管理 View Model
 *
 * @author  簡哥
 * @since   2019/8/16
 */
public class AnnTmpModel extends ADMANNTMP {
    private static final long serialVersionUID = 1L;

    private Date StartDateTime;
    private Date EndDateTime;
    private String comments;
    private String fileGuid;

    /**
     * <p>Getter for the field <code>fileGuid</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFileGuid() {
        return fileGuid;
    }

    /**
     * <p>Setter for the field <code>fileGuid</code>.</p>
     *
     * @param fileGuid a {@link java.lang.String} object.
     */
    public void setFileGuid(String fileGuid) {
        this.fileGuid = fileGuid;
    }

    
    /**
     * <p>getStartDateTime.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getStartDateTime() {
        return StartDateTime;
    }

    /**
     * <p>setStartDateTime.</p>
     *
     * @param startDateTime a {@link java.util.Date} object.
     */
    public void setStartDateTime(Date startDateTime) {
        StartDateTime = startDateTime;
    }

    /**
     * <p>getEndDateTime.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getEndDateTime() {
        return EndDateTime;
    }

    /**
     * <p>setEndDateTime.</p>
     *
     * @param endDateTime a {@link java.util.Date} object.
     */
    public void setEndDateTime(Date endDateTime) {
        EndDateTime = endDateTime;
    }

    /**
     * <p>Getter for the field <code>comments</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getComments() {
        return comments;
    }

    /**
     * <p>Setter for the field <code>comments</code>.</p>
     *
     * @param comments a {@link java.lang.String} object.
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    
}
