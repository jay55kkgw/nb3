package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.model.UploadedFile;
import com.netbank.rest.web.back.service.ADMHOLIDAYService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import fstop.aop.Authorize;
import fstop.orm.po.ADMHOLIDAY;
import fstop.util.FileUtils;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

/**
 * 假日檔維護控制器，使用一個畫面做 CRUD
 *
 * @author 簡哥
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B904")
public class B904Controller {
    @Autowired
    private ADMHOLIDAYService admHOLIDAYService;
    
    @Autowired
    private PortalSSOService portalSSOService;

    /**
     * 取得假日管理首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value={"","/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        model.addAttribute("allowEdit", portalSSOService.isPermissionOK("B904", AuthorityEnum.EDIT));
        return "B904/index";
    }

    /**
     * 分頁查詢
     *
     * @param ADHOLIDAYID 假日代號
     * @param ADREMARK 假日備註
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return 支援 jQuery Datatable Server 端的分頁物件
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value="/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String ADHOLIDAYID, @RequestParam String ADREMARK, HttpServletRequest request, HttpServletResponse response ) {
        log.debug("query ADHOLIDAY={}, ADREMARK={}", Sanitizer.logForgingStr(ADHOLIDAYID), Sanitizer.logForgingStr(ADREMARK));
        
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admHOLIDAYService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(), 
            jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(ADHOLIDAYID), 
            (String)Sanitizer.escapeHTML(ADREMARK));

        List<ADMHOLIDAY> holidays=(List<ADMHOLIDAY>)page.getResult();
        List<ADMHOLIDAY> sholidays= new ArrayList<ADMHOLIDAY>();
        for (ADMHOLIDAY holiday : holidays) {
            ADMHOLIDAY sholiday = new ADMHOLIDAY();
            Sanitizer.escape4Class(holiday, sholiday);
            sholidays.add(sholiday);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), sholidays);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得單一假日資料
     *
     * @param ADHOLIDAY 假日代碼
     * @return ADMHOLIDAY Json 物件
     */
    @PostMapping(value="/Get/{ADHOLIDAY}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody ADMHOLIDAY get(@PathVariable String ADHOLIDAY) {
        ADHOLIDAY = (String)Sanitizer.logForgingStr(ADHOLIDAY);
        //20191217-Danny-Missing Content Security Policy
        ADMHOLIDAY entity = admHOLIDAYService.getHoliday(ADHOLIDAY);
        ADMHOLIDAY oEntity = new ADMHOLIDAY();
        Sanitizer.escape4Class(entity, oEntity);
        return oEntity;

    }

    /**
     * 新增假日資料
     *
     * @param admHoliday 假日物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse create(@ModelAttribute @Valid ADMHOLIDAY admHoliday, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                if ( admHOLIDAYService.getHoliday(admHoliday.getADHOLIDAYID()) == null ) {
                    admHOLIDAYService.insertHoliday(admHoliday, portalSSOService.getLoginUserId(), false);
                    response.setValidated(true);
                } else {
                	response.setValidated(false);
                	Map<String, String> errors = new HashMap<String, String>();
                	
                	//Reflected XSS All Clients
      		 	 	String holiday = (String)Sanitizer.escapeHTML(admHoliday.getADHOLIDAYID());
                    errors.put("summary", "日期["+holiday+"]已存在");
                    response.setErrorMessages(errors);
                } 
            }
        } catch (Exception e) {
            log.error("error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "存檔發生錯誤，請確認資料是否有更新成功!!";
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary",message);

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 修改假日資料
     *
     * @param admHoliday 假日物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid ADMHOLIDAY admHoliday, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                admHOLIDAYService.saveHoliday(admHoliday, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            log.error("error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "修改存檔發生錯誤，請確認資料是否有更新成功!!";
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", message);

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 刪除假日資料
     *
     * @param ADHOLIDAY 假日資料
     * @return "0"：成功，非"0"：失敗原因
     */
    @PostMapping(value="/Delete/{ADHOLIDAY}", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String delete(@PathVariable String ADHOLIDAY) {
        try {
            ADHOLIDAY = (String)Sanitizer.logForgingStr(ADHOLIDAY);
            admHOLIDAYService.deleteHoliday(ADHOLIDAY);
            return "0";
        } catch (Exception e) {
            log.error("delete error", e);
            
            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }

    /**
     * 假日檔上傳
     *
     * @param servletRequest a {@link javax.servlet.http.HttpServletRequest} object.
     * @param uploadedFile a {@link com.netbank.rest.web.back.model.UploadedFile} object.
     * @param bindingResult a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @RequestMapping(value="FileUpload")
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse saveFile(HttpServletRequest servletRequest, MultipartFile multipartFile) {
        JsonResponse response = new JsonResponse();
        try {
            //#region
            //20191204-Danny-Unrestricted File Upload
            //允許的類型
            String[] allowFileType = {};		// 改不驗 MineType
            //圖示允許的檔案大小
            int allowFileSize = 2*1024*1024; //2Mb
            String contentType = multipartFile.getContentType();
            String originalFilename = multipartFile.getOriginalFilename();
            Map<String,Object> returnMap = FileUtils.checkFile(multipartFile,originalFilename,contentType,allowFileType,allowFileSize,false);
            //#endregion
            //檢核不通過
            if("FALSE".equals(returnMap.get("result"))){
            	//Reflected XSS All Clients
                String errorMessage = (String)Sanitizer.escapeHTML(returnMap.get("summary"));
                log.debug("errorMessage={}",Sanitizer.logForgingStr(errorMessage));

                Map<String, String> errors = new HashMap<String, String>();
                errors.put("summary",  errorMessage);

                response.setValidated(false);
                response.setErrorMessages(errors);
                return response;
            }
            //20191205-Danny-Unrestricted File Upload
            byte[] bHoliday = (byte[])returnMap.get("Data");
            String fileContent = new String(bHoliday, "UTF-8");
            String[] lines = fileContent.split("\\r?\\n");
            lines = removeBOM(lines);
            
            admHOLIDAYService.uploadHoliday(lines, portalSSOService.getLoginUserId());
            response.setValidated(true);
        } catch (Exception e) {
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "存檔發生錯誤，請確認資料是否有更新成功!!";
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", message);

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        return response;
    }
    
    /**
     * 刪除資料 BOM 表頭
     * @param lines
     * @return
     */
    private String[] removeBOM(String[] lines) {
    	List<String> cleans = new ArrayList<String>();
    	for ( String line: lines ) {
        	if (line.startsWith("\uFEFF")) {   
        		String clean = line.substring(1); // 如果 String 是以 BOM 開頭, 則省略字串最前面的第一個 字元. 
        		cleans.add(clean);
            } else {
            	cleans.add(line);
            }
        }
    	return cleans.toArray(new String[0]);
    }
    

    @GetMapping(value = { "/Sample" })
    @Authorize(userInRoleCanQuery = true)
    public void sample(ModelMap model, HttpServletResponse response) throws IOException {
        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"Holiday.txt\"");
        
        OutputStream resOs = response.getOutputStream();
        // resOs.write('\ufeef'); // emits 0xef
        // resOs.write('\ufebb'); // emits 0xbb
        // resOs.write('\ufebf'); // emits 0xbf

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = new CSVWriter(outputWriter, CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_ESCAPE_CHARACTER);

        String[] memo = { "以下為範例資料，上傳時請勿包含日期...等欄位標題" };
        writer.writeNext(memo);

        String[] headers = { "日期", "備註" };
        writer.writeNext(headers);


        String[] nextLine = { "20200101", "開國紀念日" };
        writer.writeNext(nextLine);
        
        writer.close();
    }
}
