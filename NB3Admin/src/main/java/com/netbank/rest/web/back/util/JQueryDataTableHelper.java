package com.netbank.rest.web.back.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>JQueryDataTableHelper class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
public class JQueryDataTableHelper {
    /**
     * <p>GetRequest.</p>
     *
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @return a {@link com.netbank.rest.web.back.util.JQueryDataTableRequest} object.
     */
    public static JQueryDataTableRequest GetRequest(HttpServletRequest request) {
        // jquery datatable 會 POST 下列資料
        //draw=2&
        //columns[0][data]=Id&columns[0][name]=&columns[0][searchable]=true&columns[0][orderable]=true&columns[0][search][value]=&columns[0][search][regex]=false&
        //columns[1][data]=Kind&columns[1][name]=&columns[1][searchable]=true&columns[1][orderable]=true&columns[1][search][value]=&columns[1][search][regex]=false&
        //columns[2][data]=OId&columns[2][name]=&columns[2][searchable]=true&columns[2][orderable]=true&columns[2][search][value]=&columns[2][search][regex]=false&
        //columns[3][data]=Status&columns[3][name]=&columns[3][searchable]=true&columns[3][orderable]=true&columns[3][search][value]=&columns[3][search][regex]=false&
        //columns[4][data]=StatusDesc&columns[4][name]=&columns[4][searchable]=true&columns[4][orderable]=true&columns[4][search][value]=&columns[4][search][regex]=false&
        //columns[5][data]=ResendTimeStamp&columns[5][name]=&columns[5][searchable]=true&columns[5][orderable]=true&columns[5][search][value]=&columns[5][search][regex]=false&
        //order[0][column]=0&order[0][dir]=asc&
        //start=10&length=10&
        //search[value]=&search[regex]=false

        int draw = Integer.parseInt(request.getParameter("draw"));
        int start = Integer.parseInt(request.getParameter("start"));
        int length = Integer.parseInt(request.getParameter("length"));

        /*List<String> columns = new ArrayList<String>();
        Enumeration<String> enumeration = request.getParameterNames();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            if ( key.startsWith("columns[") && key.endsWith("][data]")) {
                columns.add(request.getParameterValues(key)[0]);
            }
        }*/
        int orderColumn = Integer.parseInt(request.getParameter("order[0][column]"));
        
        String orderBy = request.getParameter("columns["+orderColumn+"][data]");
        String orderDir = request.getParameter("order[0][dir]");

        JQueryDataTableRequest jqRequest = new JQueryDataTableRequest();
        jqRequest.setDraw(draw);
        jqRequest.setStart(start);
        jqRequest.setLength(length);
        jqRequest.setOrderBy(orderBy);
        jqRequest.setOrderDir(orderDir);
        return jqRequest;
    }

    /**
     * <p>GetResponse.</p>
     *
     * @param draw a int.
     * @param recordsTotal a long.
     * @param recordsFiltered a long.
     * @param data a {@link java.lang.Object} object.
     * @return a {@link com.netbank.rest.web.back.util.JQueryDataTableResponse} object.
     */
    public static JQueryDataTableResponse GetResponse(int draw, long recordsTotal, long recordsFiltered, Object data) {
        JQueryDataTableResponse rsp = new JQueryDataTableResponse();
        rsp.setDraw(draw);
        rsp.setRecordsTotal(recordsTotal);
        rsp.setRecordsFiltered(recordsFiltered);
        rsp.setData(data);

        return rsp;
    }
}
