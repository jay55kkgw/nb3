package com.netbank.rest.web.back.model;

public class B402Model {

	private String DPSCHNO;
	
	private String LASTDATE;
	
	private String LASTTIME;
	
	private String DPUSERID;
	
	private String ADOPID;
	
	private String DPWDAC;
	
	private String DPSVBH;
	
	private String DPSVAC;
	
	private String DPTXAMT;
	
	private String DPEXCODE;
	
	private String ADMSGIN;
	
	private String DPUSERNAME;
	
	private String EINPHONE;

	public String getDPSCHNO() {
		return DPSCHNO;
	}

	public void setDPSCHNO(String dPSCHNO) {
		DPSCHNO = dPSCHNO;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getDPWDAC() {
		return DPWDAC;
	}

	public void setDPWDAC(String dPWDAC) {
		DPWDAC = dPWDAC;
	}

	public String getDPSVBH() {
		return DPSVBH;
	}

	public void setDPSVBH(String dPSVBH) {
		DPSVBH = dPSVBH;
	}

	public String getDPSVAC() {
		return DPSVAC;
	}

	public void setDPSVAC(String dPSVAC) {
		DPSVAC = dPSVAC;
	}

	public String getDPTXAMT() {
		return DPTXAMT;
	}

	public void setDPTXAMT(String dPTXAMT) {
		DPTXAMT = dPTXAMT;
	}

	public String getDPEXCODE() {
		return DPEXCODE;
	}

	public void setDPEXCODE(String dPEXCODE) {
		DPEXCODE = dPEXCODE;
	}

	public String getADMSGIN() {
		return ADMSGIN;
	}

	public void setADMSGIN(String aDMSGIN) {
		ADMSGIN = aDMSGIN;
	}

	public String getDPUSERNAME() {
		return DPUSERNAME;
	}

	public void setDPUSERNAME(String dPUSERNAME) {
		DPUSERNAME = dPUSERNAME;
	}

	public String getEINPHONE() {
		return EINPHONE;
	}

	public void setEINPHONE(String eINPHONE) {
		EINPHONE = eINPHONE;
	}
	
}
