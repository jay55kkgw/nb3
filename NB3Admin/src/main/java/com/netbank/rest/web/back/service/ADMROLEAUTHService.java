package com.netbank.rest.web.back.service;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.FlowChkResult;
import com.netbank.rest.web.back.model.FlowSvcResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmRoleAuthDao;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.SysOpDao;
import fstop.orm.po.ADMFLOWCURRENT;
import fstop.orm.po.ADMROLEAUTH;
import fstop.orm.po.SYSOP;
import lombok.extern.slf4j.Slf4j;

import fstop.util.Sanitizer;

/**
 * 角色權限服務
 *
 * @author 簡哥
 * @since 2019/8/27
 */
@Slf4j
@Service
public class ADMROLEAUTHService extends BaseService {
    @Autowired
    private AdmRoleAuthDao admRoleAuthDao;

    @Autowired
    private SysOpDao sysOpDao;

    @Autowired
    private SysDailySeqDao sysDailySeqDao;
    
    @Autowired
    private FlowService flowService;

    // 要設定在 ADMFLOWDEF 資料表中
    private final String FLOW_ID="AuthFlow";

    // 取號的 APP ID
    private final String APP_ID = "ATH";

    // 編輯者 STEPID，參考 ADMFLOWDEFSTEP
    private final String[] EDITOR_STEPS = {"3"};     

    // 審核者/放行者 STEPID，參考 ADMFLOWDEFSTEP
    private final String[] REVIEWER_STEPS = {"2"};

    // 編輯者角色
    /** Constant <code>EDITOR_ROLES</code> */
    public static final String[] EDITOR_ROLES = {"DC"};;

    // 審核者角色
    /** Constant <code>REVIEWER_ROLES</code> */
    public static final String[] REVIEWER_ROLES = {"DC"};

    /**
     * 依單位代號取得待辦事項
     * @param oId       單位代號
     * @return          流程待辦清單
     */
    public List<ADMFLOWCURRENT> getTodo(String oId) {
        return flowService.getTodoByFlowId(oId, FLOW_ID);
    }

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo   第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy  依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param roleno   銀行代碼
     * @param opid     銀行中文
     * @return 分頁物件
     */
    @SuppressWarnings("unchecked")
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String roleno, String opid, String staffno) {
        //20191115-Danny-Log Forging\路徑 2:
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, roleno={}, opid={}", Sanitizer.logForgingStr(String.valueOf(pageNo)), Sanitizer.logForgingStr(String.valueOf(pageSize)),
        Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(roleno), Sanitizer.logForgingStr(opid));

        // 取得選單ID及選單名稱的 Mapping
        List<SYSOP> menus = sysOpDao.findAll();
        Map<String, String> idNameMapping = new HashMap<String, String>();
        for ( int i=0; i<menus.size(); i++ ) {
            idNameMapping.put(menus.get(i).getADOPID(), menus.get(i).getADOPNAME());
        }

        // 把選單名稱加入原選單物件的清單中
        Page page = admRoleAuthDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, roleno, opid, staffno);
        List<ADMROLEAUTH> auths = (List<ADMROLEAUTH>) page.getResult();
        
        for ( int i=0; i<auths.size(); i++ ) {
            if ( idNameMapping.containsKey(auths.get(i).getAPOPID()) )
                auths.get(i).setAPOPName(idNameMapping.get(auths.get(i).getAPOPID()));
            else 
                auths.get(i).setAPOPName(auths.get(i).getAPOPID());
        }
        log4Query("0", new String[][] { { "roleno", roleno }, { "opid", opid }} );
        return page;
    }

    /**
     * 取得角色授權資料
     *
     * @param authid 銀行代碼
     * @return 角色授權 POJO
     */
    public ADMROLEAUTH getByADAUTHID(int authid) {
        log.debug("getByADBANKID ADAUTHID={}", authid);

        log4Query("0", new String[][] { { "authid", Integer.toString(authid) }} );
        return admRoleAuthDao.findById(authid);
    }
    
    /**
     * 查詢案件的權限
     * @param caseSN
     * @param oid
     * @param uid
     * @param roles
     * @return
     */
    public AuthorityEnum queryCaseAuthority(String caseSN, String oid, String uid, Set<String> roles) {
        FlowChkResult checkResult = flowService.IsMyCase(caseSN, oid, uid, roles);
        if ( checkResult.isMyCase() ) {
            // 這個案例只有：
            // 編輯者 --> 覆核退回（3）
            // 覆核者 --> 已送出，待覆核（2）
            long matchEditor = Arrays.stream(EDITOR_STEPS).filter(p->p.equals(checkResult.getMatchStepId())).count();
            if ( matchEditor > 0 )
                return AuthorityEnum.EDIT;

            long matchReviewer = Arrays.stream(REVIEWER_STEPS).filter(p->p.equals(checkResult.getMatchStepId())).count(); 
            if ( matchReviewer > 0 ) {
                // 是覆核者案件，需加檢核覆核者不能是編輯者，依 LCC 說法，DC 同一人可以編輯及放行，故以下程式碼註解
                // ADMFLOWCURRENT flowCurrent = flowService.getTodoByCaseSN(caseSN);
                // if ( flowCurrent.getEDITORUID().equalsIgnoreCase(uid) ) {
                //     return AuthorityEnum.QUERY;
                // } else {
                    return AuthorityEnum.REVIEW;
                // }
            }
        } else {
            return AuthorityEnum.QUERY;
        }
        return AuthorityEnum.QUERY;
    }

    /**
     * 經辦送件
     * @param oid               單位代碼
     * @param uid               使用者代碼
     * @param uName             使用者姓名
     * @param roleNo            設定的角色代碼
     * @param finalRoleAuths    最終的角色權限結果（JSON字串）
     * @return                  結果 Result Map
     */
    @Transactional
    public Map<String, String> send(String oid, String uid, String uName, String roleNo, String staffNo, String finalRoleAuths) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            // 取案號, 案號＝ADS-xxxxxxxx
            String caseSN = APP_ID+"-"+ String.format("%06d", sysDailySeqDao.dailySeq(APP_ID));
            String todoURL = "B101/Query/"+caseSN;

            Map<String, String> flowDataMap = new HashMap<String, String>();
            flowDataMap.put("auth", finalRoleAuths);
            flowDataMap.put("roleid", roleNo);
            flowDataMap.put("staffno", staffNo);

            Gson gson = new Gson();
            String flowData = gson.toJson(flowDataMap);

            String memo = "角色管理：設定"+roleNo;
            if ( !staffNo.isEmpty() ) {
            	memo+= "/"+staffNo;
            }
            FlowSvcResult rt = flowService.startFlow(FLOW_ID, caseSN, oid, uid, uName, flowData, "", memo, todoURL);
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                
                result.put("0", caseSN);
            } else {
                result.put(rt.getStatusCode(), "");
            }
            
            log4Send(caseSN, rt.getStatusCode());
        } catch (Exception e) {
            log.error("send Error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤，請再試一次!!";
            result.put((String)Sanitizer.logForgingStr(message), "");
        }
        return result;
    }

    /**
     * 同意或退回結案
     * @param caseSN            案件編號
     * @param stepId            步驟代碼
     * @param oid               單位代碼
     * @param uid               員編
     * @param uName             姓名
     * @param comments          意見
     * @param isApproved        true：同意；false：退回
     * @return                  結果 Result Map
     */
    @Transactional
    public Map<String, String> approveOrReject(String caseSN, String stepId, String oid, String uid, String uName, String comments, boolean isApproved) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            ADMFLOWCURRENT flowCurrent = flowService.getTodoByCaseSN(caseSN);
            FlowSvcResult rt = flowService.moveFlow(caseSN, stepId, oid, uid, uName, comments, flowCurrent.getFLOWDATA(), isApproved);
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                if ( rt.isIsFinal() && isApproved ) {
                    
                    Map<String, String> changeMap = processDifference(flowCurrent.getEDITORUID(), flowCurrent.getFLOWDATA());

                    Gson gson = new Gson();
                    log4Setup(gson.toJson(changeMap), "0");
                    
                    // 避免兩筆資料 log4Setup & log4Approve 的 Key 重覆, 所以 Sleep 1 sec.
                    Thread.sleep(1000);
                    log4Approve(caseSN, "0");
                } else {
                	log4Reject(caseSN, "0");
                }
                result.put("0", caseSN);
            } else {
                result.put(rt.getStatusCode(), "");
            }
        } catch (Exception e) {
            log.error("approveOrReject Error", e);
            result.put(e.getMessage(), "");
        }
        return result;
    }

    /**
     * 處理同意結案的後續處理，可能的狀況有：新增授權、異動授權及刪除授權
     * @param creator       修改人員
     * @param newSettings   前端 UI 上傳的 設定
     * @return              回傳：新增 XXX，修改：XXX，刪除：XXX
     */
    private Map<String, String> processDifference(String creator, String newSettings) {
        Gson gson = new Gson();

        Type type = new TypeToken<Map<String, String>>(){}.getType();
        Map<String, String> flowDataMap = gson.fromJson(newSettings, type);
        String roleId = flowDataMap.get("roleid");
        String staffNo = flowDataMap.get("staffno");
        String roleAuthsJson = flowDataMap.get("auth");
        
        type = new TypeToken<List<ADMROLEAUTH>>(){}.getType();

        // 因為 Entity 屬性都是大寫，所以用 toUpperCase 把 Json String 轉成 List<ADMROLEAUTH>
        List<ADMROLEAUTH> nowRoleAuths = gson.fromJson(roleAuthsJson.toUpperCase(), type);
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        // 記錄異動記錄
        Map<String, String> changeMap = new HashMap<String, String>();

        // 處理新增及異動部份
        for (ADMROLEAUTH nowRoleAuth : nowRoleAuths) {
            if ( nowRoleAuth.getADAUTHID() == 0 ) {
                // 新增
                nowRoleAuth.setADAUTHID(null);
                nowRoleAuth.setADROLENO(roleId);
                nowRoleAuth.setADSTAFFNO(staffNo);
                nowRoleAuth.setLASTDATE(parts[0]);
                nowRoleAuth.setLASTTIME(parts[1]);
                nowRoleAuth.setLASTUSER(creator); 

                admRoleAuthDao.save(nowRoleAuth);
                changeMap.put("Add", gson.toJson(nowRoleAuth));
            } else {
                // 異動，用原本的那筆來改
                ADMROLEAUTH oriRoleAuth = admRoleAuthDao.findById(nowRoleAuth.getADAUTHID());

                if ( !oriRoleAuth.getAPOPID().equalsIgnoreCase(nowRoleAuth.getAPOPID()) || 
                    !oriRoleAuth.getISQUERY().equalsIgnoreCase(nowRoleAuth.getISQUERY()) || 
                    !oriRoleAuth.getISEDIT().equalsIgnoreCase(nowRoleAuth.getISEDIT()) || 
                    !oriRoleAuth.getISEXEC().equalsIgnoreCase(nowRoleAuth.getISEXEC()) ) {

                    oriRoleAuth.setAPOPID(nowRoleAuth.getAPOPID());
                    oriRoleAuth.setADSTAFFNO(nowRoleAuth.getADSTAFFNO());
                    oriRoleAuth.setISQUERY(nowRoleAuth.getISQUERY());
                    oriRoleAuth.setISEDIT(nowRoleAuth.getISEDIT());
                    oriRoleAuth.setISEXEC(nowRoleAuth.getISEXEC());
                    oriRoleAuth.setLASTDATE(parts[0]);
                    oriRoleAuth.setLASTTIME(parts[1]);
                    oriRoleAuth.setLASTUSER(creator); 
                    admRoleAuthDao.update(oriRoleAuth);
                    changeMap.put("Update", gson.toJson(nowRoleAuth));
                }
            }
        }

        // 處理刪除的部份，刪除的部份原本的該筆授權 ID 在新的清單中會找不到 
        List<ADMROLEAUTH> oriRoleAuths = admRoleAuthDao.findByADRoleNoStaffNo(roleId, staffNo);
        for (ADMROLEAUTH oriRoleAuth : oriRoleAuths) {
            // 用舊的 ID 到新的清單中查找
            Optional<ADMROLEAUTH> foundRoleAuth = nowRoleAuths.stream().filter(p->p.getADAUTHID()==oriRoleAuth.getADAUTHID()).findAny();
            if ( !foundRoleAuth.isPresent() ) {
                admRoleAuthDao.delete(oriRoleAuth);
                changeMap.put("Delete", gson.toJson(oriRoleAuth));
            }
        }

        return changeMap;
    }

    /**
     * 經辦重送
     * @param casesn                案件編號
     * @param oid                   單位代碼
     * @param uid                   員編
     * @param uName                 姓名
     * @param comments              意見
     * @param roleNo                角色代碼
     * @param finalRoleAuths        最後設定的角色授權
     * @return                      結果 Result Map
     */
    @Transactional
    public Map<String, String> resend(String casesn, String oid, String uid, String uName, String comments, String roleNo, String staffNo, String finalRoleAuths) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            // 流程往下執行(退回後重送)
            Map<String, String> flowDataMap = new HashMap<String, String>();
            flowDataMap.put("auth", finalRoleAuths);
            flowDataMap.put("roleid", roleNo);
            flowDataMap.put("staffno",  staffNo);

            Gson gson = new Gson();
            String flowData = gson.toJson(flowDataMap);

            FlowSvcResult rt = flowService.moveFlow(casesn, EDITOR_STEPS[0], oid, uid, uName, comments, flowData, true);
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                
                result.put("0", casesn);
            } else {
                result.put(rt.getStatusCode(), "");
            }
        } catch (Exception e) {
            log.error("resend Error", e);
            //20191115-Eric-Information Exposure Through an Error Message\路徑 5:
            result.put((String)Sanitizer.logForgingStr(e.getMessage()), "");
        }
        return result;
    }

    /**
     * 經辦取消案件
     * @param caseSN        案件編號
     * @param oid           單位代碼
     * @param uid           員編
     * @param uName         姓名
     * @return              結果 Result Map
     */
    @Transactional
    public Map<String, String> cancel(String caseSN, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            FlowSvcResult rt = flowService.cancelFlow(caseSN, oid, uid, uName);
            if ( rt.getStatusCode() != FlowService.SUCCESS ) {
                throw new Exception("刪除流程失敗，StatusCode="+rt.getStatusCode());
            }
            result.put("0", caseSN);
        } catch (Exception e) {
            log.error("cancel Error", e);
            result.put(e.getMessage(), "");
        }
        return result;
    }
}
