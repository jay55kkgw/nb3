package com.netbank.rest.web.back.service;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.SysErrLogDao;

@Service
public class SYSERRLOGService extends BaseService {
    @Autowired
    private SysErrLogDao sysErrLogDao;

    /**
     * 分頁查詢
     * @param pageNo        頁碼
     * @param pageSize      一頁資料量
     * @param orderBy       排序欄位
     * @param orderDir      排序方向
     * @param startTime     開始時間
     * @param endTime       結束時間
     * @return              分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String startTime, String endTime) {
        startTime = startTime.replace("/", "").replace(":", "").replace(" ", "");
        endTime = endTime.replace("/", "").replace(":", "").replace(" ", "");

        String startDate = "";
        String endDate = "";

        if ( !startTime.isEmpty() ) {
            startDate = startTime.substring(0,8);
            startTime = startTime.substring(8,14);
        }

        if ( !endTime.isEmpty() ) {
            endDate = endTime.substring(0,8);
            endTime = endTime.substring(8,14);
        }

        Page page = sysErrLogDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, startDate, startTime, endDate, endTime);
        
        log4Query("0", new String[][] {{"startDate",startDate},{"startTime",startTime},{"endDate",endDate},{"endTime",endTime}});
        
        return page;
    }
}