package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import fstop.orm.dao.SysOpDao;
import fstop.orm.po.SYSOP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 後台選單管理
 *
 * @author 簡哥
 * @version V1.0
 */
@Slf4j
@Service
public class SYSOPService extends BaseService {
    @Autowired
    private SysOpDao sysOpDao;

    /**
     * 資料庫需配合先產生一筆 ADOPID=Menu 的首筆資料
     */
    private final String ROOT_MENU = "Menu";

    /**
     * 取得所有選單資料
     *
     * @return 選單 POJO LIST
     */
    public List<SYSOP> getAllMenus() {
        return sysOpDao.getAll();
    }

    /**
     * 取得選單 POJO
     *
     * @param menuId 選單Id
     * @return a {@link fstop.orm.po.SYSOP} object.
     */
    public SYSOP getMenuByMenuId(String menuId) {
        log.debug("getMenuByMenuId, menuId={}", Sanitizer.logForgingStr(menuId));
        log4Query("0", new String[][] { { "menuId", menuId }} );
        return  sysOpDao.getByADOPID(menuId);
    }

    /**
     * 取得某一層子選單
     *
     * @param pId   父選單 Id
     * @return      子選單清單
     */
    public List<SYSOP> getSubMenus(String pId) {
        return sysOpDao.getByADOPGROUPID(pId);
    }

    /**
     * 取得快取的子選單，供登入後首頁選單快取使用，選單維護功能不能使用
     *
     * @param pId   父選單 Id
     * @return      子選單清單
     */
    @Cacheable(value="menuCache", key="{#pId}")
    public List<SYSOP> getCachedSubMenus(String pId) {
        return sysOpDao.getByADOPGROUPID(pId);
    } 

    /**
     * 取得第一層子選單
     *
     * @return a {@link java.util.List} object.
     */
    public List<SYSOP> getLevel1Menus() {
        return sysOpDao.getByADOPGROUPID(ROOT_MENU);
    }

    /**
     * 新增選單
     *
     * @param menu a {@link fstop.orm.po.SYSOP} object.
     * @param creator a {@link java.lang.String} object.
     */
    public void insertMenu(SYSOP menu, String creator) {
        menu.setLASTUSER(creator); 
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        menu.setLASTDATE(parts[0]);
        menu.setLASTTIME(parts[1]);

        // 設定排序值為目前最大值＋10
        int maxADSeq = sysOpDao.getMaxADSEQ(menu.getADOPGROUP());
        maxADSeq += 10;

        menu.setADSEQ(maxADSeq);

        sysOpDao.save(menu);
        log4Create(menu, "0");
    }

    /**
     * 儲存選單資料
     *
     * @param menu 要異動的選單 POJO
     * @param editor 修改者
     */
    public void saveMenu(SYSOP menu, String editor) {
        SYSOP oriSYSOP = sysOpDao.findById(menu.getADOPID());
        sysOpDao.getEntityManager().detach(oriSYSOP);

        menu.setLASTUSER(editor); 

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        menu.setLASTDATE(parts[0]);
        menu.setLASTTIME(parts[1]);

        sysOpDao.update(menu);
        log4Update(oriSYSOP, menu, "0");
    }

    /**
     * 以遞迴的方式刪除子選單
     * @param menu
     */
    private void deleteRecursive(SYSOP menu) {
        // 找到所有的子節點
        List<SYSOP> subMenus = getSubMenus(menu.getADOPID());
        for ( SYSOP subMenu : subMenus ) {
            deleteRecursive(subMenu);
        }
    } 

    /**
     * 刪除選單
     *
     * @param id 要刪除的選單主鍵值
     */
    public void deleteMenu(String id) {
        SYSOP tMenu = sysOpDao.get(id);
        deleteRecursive(tMenu);
        sysOpDao.removeById(id);  
        log4Delete(tMenu, "0");
    }

    /**
     * 儲存選單的排序值
     *
     * @param sortedMenus 要排序的選單，Key=DPACCSETID, Value=排序值
     */
    public void sortMenu(Map<String, String> sortedMenus) {
        for (Entry<String, String> sortedMenu : sortedMenus.entrySet()) {
            sysOpDao.updateADSEQ(sortedMenu.getValue(), sortedMenu.getKey());
        }
    }

    /**
     * <p>moveMenu.</p>
     *
     * @param id a {@link java.lang.String} object.
     * @param pId a {@link java.lang.String} object.
     */
    public void moveMenu(String id, String pId) {
        SYSOP current = sysOpDao.findById(id);
        current.setADOPGROUP(pId);
        sysOpDao.update(current);
    }

    /**
     * <p>getROOT_MENU.</p>
     *
     * @return the ROOT_MENU
     */
    public String getROOT_MENU() {
        return ROOT_MENU;
    }
}
