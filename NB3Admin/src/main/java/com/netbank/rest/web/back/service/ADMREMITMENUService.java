package com.netbank.rest.web.back.service;

import java.util.List;

import com.google.gson.Gson;
import com.netbank.domain.orm.core.Page;
import com.netbank.util.DateUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmRemitMenuDao;
import fstop.orm.po.ADMREMITMENU;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 匯款用途選單維護服務
 *
 * @author 簡哥
 * @version V1.0
 */
@Slf4j
@Service
public class ADMREMITMENUService extends BaseService {
    @Autowired
    private AdmRemitMenuDao admRemitMenuDao;

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo    第幾頁
     * @param pageSize  一頁幾筆
     * @param orderBy   依什麼欄位排序
     * @param orderDir  升冪/降冪
     * @param remitType 匯款用途性質
     * @param lKind     大分類編號
     * @param mKind     中分類編號
     * @param remitId   匯款用途編號
     * @return 分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String remitType, String lKind, String mKind, String remitId) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, remitType={}, lKind={}, mKind={}, remitId={}", 
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(remitType), Sanitizer.logForgingStr(lKind), Sanitizer.logForgingStr(mKind), Sanitizer.logForgingStr(remitId));

        Page page = admRemitMenuDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, remitType, lKind, mKind, remitId);
        
        log4Query("0", new String[][] { { "remitType", remitType }, { "lKind", lKind }, { "mKind", mKind }, { "remitId", remitId }} );
        return page;
    }

    /**
     * 取得大分類項目
     * @param rmtType   1：匯出匯款，2：匯入匯款
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<ADMREMITMENU> getLKindMenus(String rmtType) {
        return admRemitMenuDao.find("FROM ADMREMITMENU WHERE ADRMTTYPE=? AND ADMKINDID='00' AND ADRMTID=''", rmtType);
    }

    /**
     * 取得中分類項目
     * @param rmtType     1：匯出匯款，2：匯入匯款
     * @param lKindId     大分類項目ID
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<ADMREMITMENU> getMKindMenus(String rmtType, String lKindId) {
        return admRemitMenuDao.find("FROM ADMREMITMENU WHERE ADRMTTYPE=? AND ADLKINDID=? AND ADRMTID='' AND ADMKINDID<>'00'", rmtType, lKindId);
    }

    /**
     * 依序號查詢
     * @param ADSEQNO   序號
     * @return          POJO
     */
    public ADMREMITMENU getByADSEQNO(long ADSEQNO) {
        log4Query("0", new String[][] { { "ADSEQNO", Long.toString(ADSEQNO) }} );
        return admRemitMenuDao.findById(ADSEQNO);
    }

    /**
     * 新增
     * @param remitMenu     POJO
     * @param creator       建立者員編
     */
    public void create(ADMREMITMENU remitMenu, String creator) {
        String[] parts = DateUtils.getCurrentDateTimeParts();
        remitMenu.setLASTDATE(parts[0]);
        remitMenu.setLASTTIME(parts[1]);
        remitMenu.setLASTUSER(creator);

        admRemitMenuDao.save(remitMenu);
        log4Create(remitMenu, "0");
    }

    /**
     * 修改
     * @param remitMenu     POJO 
     * @param editor        編輯者員編
     */
    public void update(ADMREMITMENU remitMenu, String editor) {
        ADMREMITMENU dbRemitMenu = admRemitMenuDao.findById(remitMenu.getADSEQNO());
        Gson gson = new Gson();
        String before = gson.toJson(dbRemitMenu);

        dbRemitMenu.setADRMTTYPE(remitMenu.getADRMTTYPE());
        dbRemitMenu.setADRMTID(remitMenu.getADRMTID());
        dbRemitMenu.setADLKINDID(remitMenu.getADLKINDID());
        dbRemitMenu.setADMKINDID(remitMenu.getADMKINDID());
        dbRemitMenu.setADRMTITEM(remitMenu.getADRMTITEM());
        dbRemitMenu.setADRMTCHSITEM(remitMenu.getADRMTCHSITEM());
        dbRemitMenu.setADRMTENGITEM(remitMenu.getADRMTENGITEM());
        dbRemitMenu.setADRMTDESC(remitMenu.getADRMTDESC());
        dbRemitMenu.setADRMTCHSDESC(remitMenu.getADRMTCHSDESC());
        dbRemitMenu.setADRMTENGDESC(remitMenu.getADRMTENGDESC());
        dbRemitMenu.setADCHKMK(remitMenu.getADCHKMK());
        dbRemitMenu.setADRMTEETYPE1(remitMenu.getADRMTEETYPE1());
        dbRemitMenu.setADRMTEETYPE2(remitMenu.getADRMTEETYPE2());
        dbRemitMenu.setADRMTEETYPE3(remitMenu.getADRMTEETYPE3());
        dbRemitMenu.setLASTUSER(editor);

        String[] parts = DateUtils.getCurrentDateTimeParts();
        dbRemitMenu.setLASTDATE(parts[0]);
        dbRemitMenu.setLASTTIME(parts[1]);

        admRemitMenuDao.update(dbRemitMenu);
        log4Update(before, dbRemitMenu, "0");
    }
    /**
     * 依序號刪除資料
     * @param adseqno   序號
     */
    public void delByADSEQNO(long adseqno) {
        ADMREMITMENU dbRemitMenu = admRemitMenuDao.findById(adseqno);
        admRemitMenuDao.removeById(adseqno);
        log4Delete(dbRemitMenu, "0");
    }
    
    /**
     * 建立大分類
     * @param remitMenu
     * @param creator
     */
    public void createLKind(ADMREMITMENU remitMenu, String creator) {
        String[] parts = DateUtils.getCurrentDateTimeParts();
        remitMenu.setLASTDATE(parts[0]);
        remitMenu.setLASTTIME(parts[1]);
        remitMenu.setLASTUSER(creator);

        int maxLKindId=0;
        List<ADMREMITMENU> lKinds = getLKindMenus(remitMenu.getADRMTTYPE());
        for ( int i=0; i<lKinds.size(); i++ ) {
        	int lKindId = Integer.parseInt(lKinds.get(i).getADLKINDID().trim());
        	if ( lKindId>maxLKindId) {
        		maxLKindId=lKindId;
        	}
        }
        remitMenu.setADLKINDID(String.format("%02d", (maxLKindId+1)));
        admRemitMenuDao.save(remitMenu);
        log4Create(remitMenu, "0");
    }
    
    /**
     * 建立中分類 
     * @param remitMenu
     * @param creator
     */
    public void createMKind(ADMREMITMENU remitMenu, String creator) {
        String[] parts = DateUtils.getCurrentDateTimeParts();
        remitMenu.setLASTDATE(parts[0]);
        remitMenu.setLASTTIME(parts[1]);
        remitMenu.setLASTUSER(creator);

        int maxMKindId=0;
        List<ADMREMITMENU> mKinds = getMKindMenus(remitMenu.getADRMTTYPE(), remitMenu.getADLKINDID());
        for ( int i=0; i<mKinds.size(); i++ ) {
        	int mKindId = Integer.parseInt(mKinds.get(i).getADMKINDID().trim());
        	if ( mKindId>maxMKindId) {
        		maxMKindId=mKindId;
        	}
        }
        remitMenu.setADMKINDID(String.format("%02d", (maxMKindId+1)));
        admRemitMenuDao.save(remitMenu);
        log4Create(remitMenu, "0");
    }
    
    /**
     * 取得匯款用途性質及編號的匯款用途選單
     * @param rmtType
     * @param rmtId
     * @return
     */
    public List<ADMREMITMENU> getByRmtTypeRmtId(String rmtType, String rmtId) {
    	return admRemitMenuDao.find("From ADMREMITMENU Where ADRMTTYPE=? And ADRMTID=?", new String[] { rmtType, rmtId });
    }
}