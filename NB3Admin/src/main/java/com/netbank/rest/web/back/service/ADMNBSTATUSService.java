package com.netbank.rest.web.back.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.netbank.domain.orm.core.Page;

import fstop.orm.dao.AdmLoginAclIPDao;
import fstop.orm.dao.AdmNbStatusDao;
import fstop.orm.po.ADMLOGINACLIP;
import fstop.orm.po.ADMNBSTATUS;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>ADMNBSTATUSService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class ADMNBSTATUSService extends BaseService {
    @Autowired
    private AdmNbStatusDao admNbStatusDao;
    
	@Autowired
    private AdmLoginAclIPDao admLoginAclIPDao;

    @Autowired
	private Environment env;
    /**
     * 取得資料
     *
     * @param ADNBSTATUSID      主鍵
     * @return                  ADMNBSTATUS POJO
     */
    public ADMNBSTATUS getByADNBSTATUSID(String ADNBSTATUSID) {
        log.debug("getByADMBSTATUSID ADMBSTATUSID={}", Sanitizer.logForgingStr(ADNBSTATUSID));

        log4Query("0", new String[][] { { "ADMBSTATUSID", ADNBSTATUSID }} );
        
        return admNbStatusDao.findById(ADNBSTATUSID);
    }

	public Page getAclIPByQuery(int pageNo, int pageSize, String orderBy, String orderDir) {
		log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, CODEID={}", Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize),Sanitizer.logForgingStr(orderBy),
		Sanitizer.logForgingStr(orderDir));
		
		try {
			return admLoginAclIPDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir);
		} catch (Exception e) {
			log.debug("getAclIPByQuery getByQuery err>>{}", e.getMessage(), e);
		}
		return new Page();
    }
	
	public void changeStatus(String statusId, String status, String modifyUser) {
		ADMNBSTATUS po = admNbStatusDao.findById(statusId);
		ADMNBSTATUS oldPo = po;
        //塞入修改者以及當前時間
        po.setLASTUSER(modifyUser);
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        po.setLASTDATE(parts[0]);
        po.setLASTTIME(parts[1]);
        
        admNbStatusDao.update(po);
        log4Update(oldPo, po, "0");
    }

	/**
	 * 加入白名單
	 * @param ip
	 * @param modifyUser
	 */
	public void addIP(String ip, String modifyUser) {
    	ADMLOGINACLIP po = new ADMLOGINACLIP();
    	po.setIP(ip);
    	
    	DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");
        
    	po.setLASTDATE(parts[0]);
    	po.setLASTTIME(parts[1]);
    	po.setLASTUSER(modifyUser);
    	
        admLoginAclIPDao.save(po);
        log4Create(po, "0");
    }

	/**
	 * 刪除IP
	 * @param ip
	 */
	public void deleteIP(String ip) {
    	ADMLOGINACLIP po = admLoginAclIPDao.findById(ip);
        admLoginAclIPDao.delete(po);
        log4Delete(po, "0");
    }
    
	/**
	 * 確認白名單是否存在
	 * @param ip
	 * @return
	 */
    public boolean isExist(String ip) {
    	return admLoginAclIPDao.findById(ip) != null;
    }

    /**
     * 儲存狀態資料
     *
     * @param admmbstatus   ADMMBSTATUS POJO
     */
    public void save(ADMNBSTATUS admnbstatus) {
        ADMNBSTATUS oriADMMBSTATUS = admNbStatusDao.get(admnbstatus.getADNBSTATUSID());
        admNbStatusDao.getEntityManager().detach(oriADMMBSTATUS);

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        admnbstatus.setLASTDATE(parts[0]);
        admnbstatus.setLASTTIME(parts[1]);

        admNbStatusDao.update(admnbstatus);
        log4Update(oriADMMBSTATUS, admnbstatus, "0");
    }
    /**
     * 清除nb3快取
     *
     * @param url
     */
    private String clean(String url) {
		StringBuilder sb = new StringBuilder();
		// checkurl replace with your JSON URL
		String urlParameters = "[]";
		byte[] postData = urlParameters.getBytes();
		int postDataLength = postData.length;
		log.debug("\nSending 'POST' request to URL : " + url);
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			con.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			con.setDoOutput(true);
			con.setConnectTimeout(5 * 1000);
			con.setReadTimeout(60 * 1000);
			con.setDoInput(true);

			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.write(postData);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();

			log.debug("\nSending 'POST' request to URL : " + url);
			log.debug("Response Code : " + responseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

			String line;

			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}

			br.close();

			log.debug("RS>>>>>{}", sb.toString());
			return sb.toString();
		} catch (SocketTimeoutException e) {
			log.debug("clean error>>>>>{}", e.getMessage(), e);
			return sb.toString();
		} catch (IOException e) {
			log.debug("clean error>>>>>{}", e.getMessage(), e);
			return sb.toString();
		}
	}

    /**
     * 清除nb3快取
     *
     * @param url
     */
	public void cleanRun() {
		
		List<String> urlList = java.util.Arrays.asList(env.getProperty("cleanUrl").split(","));
		
		log.debug("urlList="+urlList);
		
		for(String url: urlList) {
			String urlFirst = url;
			String urlSecond = url;
			urlFirst = "http://"+url+"/nb3/clearAdmNbStatu";
			String json = clean(urlFirst);
			log.debug("clean result json=" + json);
			urlSecond = "http://"+url+"/nb3/clearMenuList";
			json = clean(urlSecond);
			log.debug("clean result json=" + json);
			InetAddress addr;
			try {
				addr = InetAddress.getLocalHost();
				String ip=addr.getHostAddress().toString();//獲得本機IP
				log.debug("ip="+ip);
			} catch (UnknownHostException e) {
				log.debug(e.getMessage());
			}
		}
	}
}
