package com.netbank.rest.web.back.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.Act_MainDao;

import fstop.orm.po.ACT_MAIN;

import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 推廣訊息維護
 *
 * @author jinhanhuang
 * @version V1.0
 */
@Slf4j
@Service
public class ACT_MAINService extends BaseService {
    @Autowired
    private Act_MainDao act_MainDao;

    /**
     * 以查詢條件做分頁查詢
     * 
     * @param pageNo 第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy 依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param ACT_TYPE 行銷活動代號
     * @param ACTDATE_FROM 行銷起日
     * @param ACTDATE_TO 行銷迄日
     * @return
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String ACT_TYPE, String MAXCOUNT, String ACTDATE_FROM, String ACTDATE_TO) {
        ACTDATE_FROM = ACTDATE_FROM.replace("/", "");
        ACTDATE_TO = ACTDATE_TO.replace("/", "");
        
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, ACT_TYPE={}, MAXCOUNT={}, ACTDATE_FROM={}, ACTDATE_TO={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ACT_TYPE), Sanitizer.logForgingStr(MAXCOUNT), Sanitizer.logForgingStr(ACTDATE_FROM), Sanitizer.logForgingStr(ACTDATE_TO));

        log4Query("0", new String[][] { { "ACT_TYPE", ACT_TYPE }, { "ACTDATE_FROM", ACTDATE_FROM }, { "ACTDATE_TO", ACTDATE_TO } } );
        return act_MainDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, ACT_TYPE, MAXCOUNT, ACTDATE_FROM, ACTDATE_TO);
    }

    /**
     * getAll
     * 
     * @return All
     */

    public List<ACT_MAIN> getAll(){
        return act_MainDao.findAll();
    }
    
    /**
     * 寫入行銷主檔
     * 
     * @param po
     */
    public Map<String, String> saveACT(ACT_MAIN po){

        log.debug("start saveACT...");
        Map<String, String> errors = new HashMap<String, String>();
        
        if(!this.getACT_TYPE(po.getACT_TYPE())){
            log.debug("Create ACT_MAIN error>>>>>>same act_type");
            errors.put("summary", "行銷活動代號重複");
            return errors;
        }

        String ACTDATE_FROM = po.getACTDATE_FROM().replace("/", "");
        String ACTDATE_TO = po.getACTDATE_TO().replace("/", "");
        
        po.setACTDATE_FROM(ACTDATE_FROM);
        po.setACTDATE_TO(ACTDATE_TO);

        act_MainDao.save(po);

        return errors;
    }

    /**
     * 更新行銷主檔
     * 
     * @param po
     */
    public boolean updateACT(ACT_MAIN po){
        log.debug("start updateACT...");
        boolean result = true;
        try {			
        	act_MainDao.update(po);
		} catch (Exception e) {
			log.debug("updateACT error>>>>>", e, e.getMessage());
			result = false;
		}
        
        return result;
    }

    /**
     * 刪除行銷主檔
     * 
     * @param po
     */
    public boolean deleteACT(String ACT_TYPE){
        log.debug("start deleteACT...");
        boolean result = true; 
        try {
        	ACT_MAIN po= act_MainDao.findById(ACT_TYPE);
        	act_MainDao.delete(po);        	
        } catch (Exception e) {
			log.debug("updateACT error>>>>>", e, e.getStackTrace());
			result = false;
		}
		return result;
		
    }

    /**
     * 查詢行銷活動代號
     * 
     * @param ACT_TYPE 行銷活動代號
     * @return
     */

    public boolean getACT_TYPE(String ACT_TYPE){
        log.debug("start getACT_TYPE...");
        boolean result = true;

        if(act_MainDao.getACT_TYPE(ACT_TYPE) != null){
            result = false;
        }

        return result;
    }

    /**
     * 查詢行銷活動代號
     * 
     * @param ACT_TYPE 行銷活動代號
     * @return
     */

    public ACT_MAIN getById(String ACT_TYPE){
        log.debug("start getById...");
        return act_MainDao.findById(ACT_TYPE);
    }
}
