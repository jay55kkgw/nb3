package com.netbank.rest.web.back.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ADMNBSTATUSService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMLOGINACLIP;
import fstop.orm.po.ADMNBSTATUS;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 應用系統啟動/關閉控制器
 *
 * @author Tim
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B102")
public class B102Controller {
    private final String ADNBSTATUSID="TBBNB";

    @Autowired
    private ADMNBSTATUSService admNBSTATUSService;

    @Autowired
    private PortalSSOService portalSSOService;

    /**
     * 取得現行狀態畫面
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        ADMNBSTATUS po =  admNBSTATUSService.getByADNBSTATUSID(ADNBSTATUSID);
        // 給 Index View 做 Binding
        model.addAttribute("Data", po);
        if ( portalSSOService.isPermissionOK("B102", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        return "B102/index";
    }

    /**
     * 處理狀態變更，轉至結果頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @RequestMapping(value = "/Result", method = RequestMethod.POST)
    @Authorize(userInRoleCanEdit = true)
    public String submit(ModelMap model, @RequestParam String optradio) {
        ADMNBSTATUS po = admNBSTATUSService.getByADNBSTATUSID(ADNBSTATUSID);
        String status = po.getADNBSTATUS().toString();
        // if (status.equals("Y")) {
        //     po.setADNBSTATUS("N");
        // } else {
        //     po.setADNBSTATUS("Y");
        // }
		optradio = (String) Sanitizer.logForgingStr(optradio);
		po.setADNBSTATUS(optradio);
        po.setLASTUSER(portalSSOService.getLoginUserId());
        admNBSTATUSService.save(po);
		String Status= "";
		switch (optradio) {
			case "Y":
				Status="啟動";
				break;
			case "N":
				Status="關閉";
				break;
			case "T":
				Status="內部封測";
				break;
			default:
				break;
		}

        String timeStamp = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
            Date lastDt = dateFormat.parse(po.getLASTDATE()+"-"+po.getLASTTIME());
            dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            timeStamp = dateFormat.format(lastDt);
        } catch (Exception e) {
            log.error("submit error", e);
            timeStamp = po.getLASTDATE()+"-"+po.getLASTTIME();
        }
        admNBSTATUSService.cleanRun();
        model.addAttribute("TimeStamp", timeStamp);
        model.addAttribute("Status", Status);
        return "B102/result";
    }

    /**
	 * 變更狀態
	 * 
	 * @param statusId
	 * @param status
	 * @return
	 */
	@PostMapping(value = "/changeStatus", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Authorize(userInRoleCanEdit = true)
	public JsonResponse changeStatus(@RequestParam String statusId, @RequestParam String status) {
		JsonResponse response = new JsonResponse();
		try {
			statusId = (String) Sanitizer.logForgingStr(statusId);
			status = (String) Sanitizer.logForgingStr(status);
			
			char newStatus = status.toUpperCase().charAt(0);
			
			if (newStatus != 'Y' && newStatus != 'N' && newStatus != 'T') {
				throw new Exception("status 狀態不符, 不為 Y 或 N 或 T");
			}
			admNBSTATUSService.changeStatus(statusId, status, portalSSOService.getLoginUserId());
			response.setValidated(true);
		} catch (Exception e) {
			log.error("changeStatus", e);
			Map<String, String> errors = new HashMap<String, String>();
			// 20191120-Danny-Information Exposure Through an Error Message\路徑 1:
			errors.put("summary", "系統錯誤，請重新嘗試");
			response.setValidated(false);
			response.setErrorMessages(errors);
			// Information Exposure Through an Error Message
			return response;
		}
		//清除快取
		admNBSTATUSService.cleanRun();
		return response;
	}

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/QueryAclIP", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Authorize(userInRoleCanQuery = true)
	public @ResponseBody JQueryDataTableResponse queryAclIP(HttpServletRequest request, HttpServletResponse response) {

		// 拿取分頁用的參數
		JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

		// 前端套用 paging=false, 只是為了套版方便, 以下的 page size 用固定值, 不然取得的值會是 -1, 造成取不到資料
		Page page = admNBSTATUSService.getAclIPByQuery(jqDataTableRq.getPage(), 10, jqDataTableRq.getOrderBy(),
				jqDataTableRq.getOrderDir());
		List<ADMLOGINACLIP> admLoginAclIP = (List<ADMLOGINACLIP>) page.getResult();
		List<ADMLOGINACLIP> sadmLoginAclIP = new ArrayList<ADMLOGINACLIP>();

		// 漂白
		for (ADMLOGINACLIP item : admLoginAclIP) {
			ADMLOGINACLIP sItem = new ADMLOGINACLIP();
			Sanitizer.escape4Class(item, sItem);
			sadmLoginAclIP.add(sItem);
		}
		JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
				page.getTotalCount(), page.getTotalCount(), sadmLoginAclIP);
		JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
		Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

		return sjqDataTableRs;
	}

	/**
	 * 增加白名單
	 */
	@PostMapping(value = "/AddAclIP", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Authorize(userInRoleCanEdit = true)
	public JsonResponse addAclIP(@RequestParam String IP) {
		JsonResponse response = new JsonResponse();
		try {
			IP = (String) Sanitizer.logForgingStr(IP);

			if (!isValidIPAddressRange(IP)) {
				Map<String, String> errors = new HashMap<String, String>();
				errors.put("summary", "IP或IP範圍資料有誤");
				response.setValidated(false);
				response.setErrorMessages(errors);
				// Information Exposure Through an Error Message
				return response;
			}

			if (admNBSTATUSService.isExist(IP)) {
				Map<String, String> errors = new HashMap<String, String>();
				errors.put("summary", "IP或IP範圍資料已存在");
				response.setValidated(false);
				response.setErrorMessages(errors);
				//清除nb3前台快取
				//清除快取
				admNBSTATUSService.cleanRun();
				return response;
			}
			log.debug("IP=" + Sanitizer.logForgingStr(IP));
			log.debug("LoginUserId=" + Sanitizer.logForgingStr(portalSSOService.getLoginUserId()));
			admNBSTATUSService.addIP(IP, portalSSOService.getLoginUserId());
			response.setValidated(true);
		} catch (Exception e) {
			log.error("addAclIP", e);
			Map<String, String> errors = new HashMap<String, String>();
			errors.put("summary", "系統錯誤，請重新嘗試");
			response.setValidated(false);
			response.setErrorMessages(errors);
			return response;
		}
		return response;
	}

	@PostMapping(value = "/DeleteAclIP", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Authorize(userInRoleCanEdit = true)
	public JsonResponse deleteAclIP(@RequestParam String IP) {
		JsonResponse response = new JsonResponse();
		try {
			IP = (String) Sanitizer.logForgingStr(IP);
			
			admNBSTATUSService.deleteIP(IP);
			response.setValidated(true);
			//清除nb3前台快取
			admNBSTATUSService.cleanRun();
		} catch (Exception e) {
			log.error("deleteAclIP", e);
			Map<String, String> errors = new HashMap<String, String>();
			// 20191120-Danny-Information Exposure Through an Error Message\路徑 1:
			errors.put("summary", "系統錯誤，請重新嘗試");
			response.setValidated(false);
			response.setErrorMessages(errors);
			// Information Exposure Through an Error Message
			return response;
		}
		return response;
	}

	/**
	 * IP Range 是否正確
	 * 
	 * @param ipRange
	 * @return
	 */
	private boolean isValidIPAddressRange(String ipRange) {
		String[] ips = ipRange.split("-");

		for (int i = 0; i < ips.length; i++) {
			if (!isValidIPAddress(ips[i])) {
				return false;
			}
		}

		if (ips.length == 2) {
			// 判斷 CLASS-C
			String[] ip1s = ips[0].split(Pattern.quote("."));
			String[] ip2s = ips[1].split(Pattern.quote("."));

			if (Integer.parseInt(ip1s[0]) == Integer.parseInt(ip2s[0])
					&& Integer.parseInt(ip1s[1]) == Integer.parseInt(ip2s[1])
					&& Integer.parseInt(ip1s[2]) == Integer.parseInt(ip2s[2])) {

				return true;
			} else {
				return false;
			}

		}
		return true;
	}
    
	/**
	 * IP 是否正確
	 * 
	 * @param ip
	 * @return
	 */
	private boolean isValidIPAddress(String ip) {

		// Regex for digit from 0 to 255.
		String zeroTo255 = "(\\d{1,2}|(0|1)\\d{2}|2[0-4]\\d|25[0-5])";

		// Regex for a digit from 0 to 255 and
		// followed by a dot, repeat 4 times.
		// this is the regex to validate an IP address.
		String regex = zeroTo255 + "\\." + zeroTo255 + "\\." + zeroTo255 + "\\." + zeroTo255;

		// Compile the ReGex
		Pattern p = Pattern.compile(regex);

		// If the IP address is empty
		// return false
		if (ip == null) {
			return false;
		}

		// Pattern class contains matcher() method
		// to find matching between given IP address
		// and regular expression.
		Matcher m = p.matcher(ip);

		// Return if the IP address
		// matched the ReGex
		return m.matches();
	}
}
