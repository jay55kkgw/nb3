package com.netbank.rest.web.back.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.service.SYSPARAMDATAService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.netbank.rest.web.back.model.B105ViewModel;
import fstop.aop.Authorize;
import fstop.orm.po.SYSPARAMDATA;
import lombok.extern.slf4j.Slf4j;

import fstop.util.Sanitizer;

/**
 * 各業務營業時間維護
 *
 * @author Tim
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B105")
public class B105Controller {
    @Autowired
    private SYSPARAMDATAService sysPARAMDATAService;

    @Autowired
    private PortalSSOService portalSSOService;

    /**
     * 取得各業務營業時間維護首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        SYSPARAMDATA po = null;
        po = sysPARAMDATAService.getByADPKID("NBSYS");
        // 給 Index View 做 Binding
        model.addAttribute("Data", po);
        if ( portalSSOService.isPermissionOK("B105", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        return "B105/index";
    }

    /**
     * 修改銀行資料
     *
     * @param result       SpringMVC Binding 檢核結果
     * @return Json 結果物件
     * @param sysparamdata a {@link com.netbank.rest.web.back.model.B105ViewModel} object.
     */
    @PostMapping(value = "/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid B105ViewModel sysparamdata, BindingResult result) {
        JsonResponse response = new JsonResponse();
        try {
            if (result.hasErrors()) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                        .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

                response.setValidated(false);
                response.setErrorMessages(errors);
                //Information Exposure Through an Error Message
                return response;
            } else {
                sysPARAMDATAService.saveSysparmDataB105(sysparamdata, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            log.error("edit error", e);
            //Information Exposure Through an Error Message
            String message = "修改存檔發生錯誤，請確認資料是否有更新成功!!";
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", message);

            response.setValidated(false);
            response.setErrorMessages(errors);
            //Information Exposure Through an Error Message
            return response;
        }

        return response;
    }
}
