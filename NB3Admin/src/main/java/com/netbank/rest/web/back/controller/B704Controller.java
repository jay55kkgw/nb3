package com.netbank.rest.web.back.controller;

import com.google.gson.Gson;
import com.netbank.rest.web.back.model.B704Model;
import com.netbank.rest.web.back.model.B704Result;
import com.netbank.rest.web.back.service.NMBReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fstop.aop.Authorize;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>B704Controller class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B704")
public class B704Controller {
    @Autowired
    private NMBReportService _NMBReportService;

    /**
     * 取得一般網路銀行約定/非約定統計表首頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        B704Model entity = new B704Model();

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, -1);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        String todayYear = String.format("%04d", year);
        String todayMonth = String.format("%02d", month);

        entity.setYearS(todayYear);
        entity.setMonthS(todayMonth);

        List<B704Result> queryData = getResultList(entity);
        model.addAttribute("Data", queryData);

        SetDropDownList(model, entity);
        return "B704/index";
    }

    /**
     * 查詢資料
     *
     * @param result     SpringMVC Binding 檢核結果
     * @return Json 結果物件
     * @param entity a {@link com.netbank.rest.web.back.model.B704Model} object.
     * @param model a {@link org.springframework.ui.ModelMap} object.
     */
    @PostMapping(value = "/Query")
    @Authorize(userInRoleCanQuery = true)
    public String query(@ModelAttribute B704Model entity, BindingResult result, ModelMap model) {
        log.debug("entity={}", Sanitizer.logForgingStr(entity));
        // 錯誤處理
        if (result.hasErrors()) {
            // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
            Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

            model.addAttribute("Validated", false);
            model.addAttribute("ErrMsg", errors);
            return "B704/index";
        }

        List<B704Result> queryData = getResultList(entity);
        model.addAttribute("Data", queryData);
        SetDropDownList(model, entity);

        return "B704/index";
    }

    /**
     * 取得資料
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @param entity a {@link com.netbank.rest.web.back.model.B704Model} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException if any.
     */
    @PostMapping(value = { "/DownloadtoCSV" })
    @Authorize(userInRoleCanQuery = true)
    public void DownloadtoCSV(@ModelAttribute B704Model entity, ModelMap model, HttpServletResponse response)
            throws IOException {

        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"ExportSys.csv\"");
        OutputStream resOs = response.getOutputStream();
        resOs.write('\ufeef'); // emits 0xef
        resOs.write('\ufebb'); // emits 0xbb
        resOs.write('\ufebf'); // emits 0xbf

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = new CSVWriter(outputWriter);

        List<B704Result> queryData = getResultList(entity);

        String[] headers = { "交易功能", "單位", "", "企業戶", "", "", "", "", "", "個人戶", "", "", "", "", "", "合計", "", "" };
        writer.writeNext(headers);
        String[] headers2 = { "", "", "", "交易密碼", "", "電子簽章(i-key)", "", "晶片金融卡", "", "交易密碼", "", "電子簽章(i-key)", "",
                "晶片金融卡", "", "", "", "" };
        writer.writeNext(headers2);
        String[] headers3 = { "", "", "", "成功", "失敗", "成功", "失敗", "成功", "失敗", "成功", "失敗", "成功", "失敗", "成功", "失敗", "成功",
                "失敗", "失敗%" };
        writer.writeNext(headers3);
        for (B704Result item : queryData) {
            String adagreef = "";
            if (item.getAdagreef().equals("-1"))
                adagreef = "小計";
            else if (item.getAdagreef().equals("0"))
                adagreef = "非約轉";
            else
                adagreef = "約轉";

            String[] nextLine = { item.getAdopname(), adagreef, (item.getQtytype().equals("0")) ? "筆數" : "金額",
                    item.getC00count(), item.getC01count(), item.getC10count(), item.getC11count(), item.getC20count(),
                    item.getC21count(), item.getP00count(), item.getP01count(), item.getP10count(), item.getP11count(),
                    item.getP20count(), item.getP21count(), item.getT99(), item.getF99(), item.getTf() };
            writer.writeNext(nextLine);
        }
        writer.close();
    }

    /*
     * 取回查詢資料
     */
    private List<B704Result> getResultList(B704Model entity) {

        Rows qResult = new Rows();
        try {
            qResult = _NMBReportService.queryB704(entity);
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());
        }

        List<B704Result> queryData = new ArrayList<>();
        B704Result qItem;
        int rowCount = qResult.getSize();
        for (int i = 0; i < rowCount; i++) {
            qItem = new B704Result();
            Row r = null;
            r = qResult.getRow(i);
            qItem.setAdopid(r.getValue("ADOPID"));
            qItem.setAdopname(r.getValue("ADOPNAME"));
            qItem.setAdagreef(r.getValue("ADAGREEF"));
            qItem.setQtytype(r.getValue("QTYTYPE"));

            qItem.setC00count(r.getValue("C00COUNT"));
            qItem.setC01count(r.getValue("C01COUNT"));
            qItem.setC10count(r.getValue("C10COUNT"));
            qItem.setC11count(r.getValue("C11COUNT"));
            qItem.setC20count(r.getValue("C20COUNT"));
            qItem.setC21count(r.getValue("C21COUNT"));

            qItem.setP00count(r.getValue("P00COUNT"));
            qItem.setP01count(r.getValue("P01COUNT"));
            qItem.setP10count(r.getValue("P10COUNT"));
            qItem.setP11count(r.getValue("P11COUNT"));
            qItem.setP20count(r.getValue("P20COUNT"));
            qItem.setP21count(r.getValue("P21COUNT"));

            qItem.setT99(r.getValue("T99"));
            qItem.setF99(r.getValue("F99"));
            qItem.setTf(r.getValue("TF"));

            queryData.add(qItem);
        }

        return queryData;
    }

    private void SetDropDownList(ModelMap model, B704Model entity) {
        model.addAttribute("B704Model", entity);

        List<String> years = new ArrayList<String>();
        for (int i = 2019; i <= 2119; i++) {
            years.add(Integer.toString(i));
        }

        List<String> months = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            months.add(String.format("%02d", i));
        }

        model.addAttribute("years", years);
        model.addAttribute("months", months);
        return;
    }

}
