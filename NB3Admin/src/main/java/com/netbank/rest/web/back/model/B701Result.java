package com.netbank.rest.web.back.model;

/**
 * <p>B701Result class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class B701Result  {
    private String CMYYYYMM;
    private String ADSUMROW;
    private String ADENTERP;
    private String ADPERSON;

    /**
     * <p>getCMYYYYMM.</p>
     *
     * @return String return the cmyyyymm
     */
    public String getCMYYYYMM() {
        return CMYYYYMM;
    }

    /**
     * <p>setCMYYYYMM.</p>
     *
     * @param CMYYYYMM a {@link java.lang.String} object.
     */
    public void setCMYYYYMM(String CMYYYYMM) {
        this.CMYYYYMM = CMYYYYMM;
    }

    /**
     * <p>getADSUMROW.</p>
     *
     * @return String return the ADSUMROW
     */
    public String getADSUMROW() {
        return ADSUMROW;
    }

    /**
     * <p>setADSUMROW.</p>
     *
     * @param ADSUMROW the ADSUMROW to set
     */
    public void setADSUMROW(String ADSUMROW) {
        this.ADSUMROW = ADSUMROW;
    }

    /**
     * <p>getADENTERP.</p>
     *
     * @return String return the ADENTERP
     */
    public String getADENTERP() {
        return ADENTERP;
    }

    /**
     * <p>setADENTERP.</p>
     *
     * @param ADENTERP the ADENTERP to set
     */
    public void setADENTERP(String ADENTERP) {
        this.ADENTERP = ADENTERP;
    }

    /**
     * <p>getADPERSON.</p>
     *
     * @return String return the ADPERSON
     */
    public String getADPERSON() {
        return ADPERSON;
    }

    /**
     * <p>setADPERSON.</p>
     *
     * @param ADPERSON the ADPERSON to set
     */
    public void setADPERSON(String ADPERSON) {
        this.ADPERSON = ADPERSON;
    }

}
