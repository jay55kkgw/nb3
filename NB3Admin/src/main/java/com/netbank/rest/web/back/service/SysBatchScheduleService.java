package com.netbank.rest.web.back.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.netbank.util.DateUtils;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.SYSBATCH;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@EnableAsync
public class SysBatchScheduleService {
	@Autowired
	private SysBatchDao sysBatchDao;
	
	@Autowired
	private NotifyService notifyService;

	@Value(value="${batch.mail.receiver:}")
	private String receiverList;
	
	@Scheduled(cron = "${batch.mail.cron:}")
    @Async
    public void jobStatusReportTask() {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("<html><head><style> #jobs { font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif; border-collapse: collapse; width: 100%; } ");
			sb.append("#jobs td, #jobs th { border: 1px solid #ddd; padding: 8px; } "); 
			sb.append("#jobs tr:nth-child(even){background-color: #f2f2f2; } ");
			sb.append("#jobs tr:hover {background-color: #ddd;} ");
			sb.append("#jobs th { padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #fc8c03; color: white; } "); 
			sb.append("</style></head>");
			sb.append("<body><table id='jobs'><tr><th>批次代碼</th><th>批次名稱</th><th>批次開始時間</th><th>批次結束時間</th><th>耗時(ms)</th><th>周期</th><th>結果</th></tr>");
			List<SYSBATCH> items = sysBatchDao.findAll();
			for ( SYSBATCH item : items ) {
				sb.append("<tr><td>").append(item.getADBATID()).append("</td>");
				sb.append("<td>").append(item.getADBATNAME()).append("</td>");
				
				String sdt = DateUtils.getDateTimeString(item.getADBATSDATE(), "/", item.getADBATSTIME(), ":", false);
				String edt = DateUtils.getDateTimeString(item.getADBATEDATE(), "/", item.getADBATETIME(), ":", false);
				
				sb.append("<td>").append(sdt).append("</td>");
				sb.append("<td>").append(edt).append("</td>");
				sb.append("<td>").append(item.getEXECUTIONTIME()).append("</td>");
				sb.append("<td>").append(item.getPEROID()).append("</td>");
				
				String msg = item.getEXECMESSAGE().trim().length()==0 ? "成功": item.getEXECMESSAGE();
				sb.append("<td>").append(msg).append("</td>");
			}
			sb.append("</body></html>");
			//System.out.println(sb.toString());
			
			List<String> toList = null;
			if ( receiverList.indexOf(';') != -1 ) {
				toList = Arrays.asList(receiverList.split("\\s*;\\s*"));
			} else {
				toList = Arrays.asList(receiverList.split("\\s*,\\s*"));
			}
			
			notifyService.smtpMail("新世代網路銀行批次執行結果一覽表", sb.toString(), toList, null, null);
		} catch ( Exception e ) {
			log.error("jobStatusReportTask Error", e);
		}
    }
	
    public static void main1(String[] args) throws IOException {
		//SysBatchScheduleService svc = new SysBatchScheduleService();
		StringBuilder sb = new StringBuilder();
		sb.append("<html><head><style> #jobs { font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif; border-collapse: collapse; width: 100%; } ");
		sb.append("#jobs td, #jobs th { border: 1px solid #ddd; padding: 8px; } "); 
		sb.append("#jobs tr:nth-child(even){background-color: #f2f2f2; } ");
		sb.append("#jobs tr:hover {background-color: #ddd;} ");
		sb.append("#jobs th { padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #fc8c03; color: white; } "); 
		sb.append("</style></head>");
		sb.append("<body><table id='jobs'><tr><th>批次代碼</th><th>批次名稱</th><th>批次開始時間</th><th>批次結束時間</th><th>耗時(ms)</th><th>周期</th><th>結果</th></tr>");
		sb.append("<tr><td>emailuploadall</td><td>產生電子帳單全部EMAIL異動資料</td><td>2020/05/21 07:11:22</td><td>2020/05/21 07:11:32</td><td>10</td><td>02:20 / 每月1號</td><td>成功</td></tr>");
		sb.append("</body></html>");
		//mailContent(sb.toString());
		List<String> toList = new ArrayList<String>();
		toList.add("chiensc@icloud.com");
		toList.add("chiensc@seed.net.tw");
		
		List<String> fileNameList = new ArrayList<String>();
		List<byte[]> fileContentList = new ArrayList<byte[]>();
		
		fileNameList.add("test.html");
		fileContentList.add(sb.toString().getBytes(StandardCharsets.UTF_8));
		
		fileNameList.add("test.txt");
		fileContentList.add("游堃堃綉綉難".getBytes(StandardCharsets.UTF_8));
		
		
		String mailList = "chiensj@fstop.com.tw;chiensc@seed.net.tw";
		toList = Arrays.asList(mailList.split("\\s*;\\s*"));
		
		int a=10, b=0, c;
		c=a+b;
		//MailUtil.smtpMail("smtp.gmail.com", 587, true, "chiensj5912@gmail.com", "yyrctltsaumafrnc", "chiensj5912@gmail.com", toList, "新世代網路銀行批次執行結果一覽表", sb.toString(), fileNameList, fileContentList);
	}
}
