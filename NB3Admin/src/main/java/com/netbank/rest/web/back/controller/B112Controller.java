package com.netbank.rest.web.back.controller;

import static org.junit.Assume.assumeNoException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.google.gson.Gson;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.model.ZTreeNode;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.service.SYSOPService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.SYSOP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 後台選單管理控制器
 *
 * @author 簡哥
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B112")
public class B112Controller {
    @Autowired
    private SYSOPService sysOpService;
    
    @Autowired
    private PortalSSOService portalSSOService;
    /**
     * 取得選單管理首頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"", "/Index"})
    @Authorize(userInRoleCanQuery=true)
    public String index(ModelMap model) {
        model.addAttribute("showAdd", portalSSOService.isPermissionOK("B112", AuthorityEnum.EDIT));
        try {
            List<SYSOP> menus = sysOpService.getAllMenus();
            List<ZTreeNode> nodes = new ArrayList<ZTreeNode>();

            for ( int i=0; i<menus.size(); i++ ) {
                ZTreeNode node = new ZTreeNode();
                node.setpId(menus.get(i).getADOPGROUP());     // 上層節點 Id
                node.setId(menus.get(i).getADOPID());           // 本節點 Id
                node.setName(menus.get(i).getADOPNAME());       // 本節點名稱
                node.setOpen(false);
                nodes.add(node);
            }

            Gson gson = new Gson();
            String jsonMenus = gson.toJson(nodes);
            //log.debug(Sanitizer.logForgingStr("jsonMenus="+jsonMenus)); 

            // 給 Index View 做 Binding
            model.addAttribute("menus", jsonMenus); 
            if ( portalSSOService.isPermissionOK("B112", AuthorityEnum.EDIT)) {
                model.addAttribute("allowEdit", true);
            } else {
                model.addAttribute("allowEdit", false);
            }
        } catch (Exception e) {
            log.error("index error", e);

            // 給空值，給前端做判斷
            model.addAttribute("menu", "");
        }  
        return "B112/index";
    }
    
    /**
     * 取得新增選單的部份檢視（html）
     *
     * @param ADOPID a {@link java.lang.String} object.
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/Create/{ADOPID}")
    @Authorize(userInRoleCanEdit=true)
    public String create(@PathVariable String ADOPID, ModelMap model) {
        SYSOP menu = new SYSOP();
        menu.setADOPGROUP(ADOPID);
        menu.setADOPALIVE("1");

        model.addAttribute("menu", menu);

        return "B112/createPartial";
    }

    /**
     * 取得修改選單的部份檢視（html）
     *
     * @param ADOPID 選單 Id
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/Edit/{ADOPID}")
    @Authorize(userInRoleCanQuery=true)
    public String edit(@PathVariable String ADOPID, ModelMap model) {
        model.addAttribute("showAdd", portalSSOService.isPermissionOK("B112", AuthorityEnum.EDIT));
        
        SYSOP menu = sysOpService.getMenuByMenuId(ADOPID);
        model.addAttribute("menu", menu);
        if ( portalSSOService.isPermissionOK("B112", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        return "B112/editPartial";
    }

    /**
     * 取得排序選單部份檢視（html）
     *
     * @param ADOPID 選單 Id
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/Sort/{ADOPID}")
    @Authorize(userInRoleCanEdit=true)
    public String sort(@PathVariable String ADOPID, ModelMap model) {
        List<SYSOP> menus = sysOpService.getSubMenus(ADOPID);
        model.addAttribute("menus", menus);

        return "B112/sortPartial";
    }

    /**
     * 取得移動選單部份檢視（html）
     *
     * @param ADOPID a {@link java.lang.String} object.
     * @param model 選單 Id
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/Move/{ADOPID}")
    @Authorize(userInRoleCanEdit=true)
    public String move(@PathVariable String ADOPID, ModelMap model) {
        SYSOP menu = sysOpService.getMenuByMenuId(ADOPID);

        // 要顯示的選單基本資料
        model.addAttribute("menu", menu);

        List<SYSOP> firstMenus = sysOpService.getLevel1Menus();
        model.addAttribute("firstMenus", firstMenus);

        return "B112/movePartial";
    }

    /**
     * 取得子選單清單, 不要下權限控制 Annotation，因為權限設定會用到這個方法
     *
     * @param ADOPID 選單 Id
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/SubMenu/{ADOPID}")
    public @ResponseBody String getSubMenu(@PathVariable String ADOPID) {
        List<SYSOP> subMenus = sysOpService.getSubMenus(ADOPID);
        List<SYSOP> ssubMenus = new ArrayList<SYSOP>();
        for (SYSOP subMenu : subMenus) {
            SYSOP ssubMenu = new SYSOP();
            Sanitizer.escape4Class(subMenu, ssubMenu);
            ssubMenus.add(ssubMenu);
        }
        Gson gson = new Gson();

        return (String)gson.toJson(ssubMenus);
    }

    /**
     * 新增儲存選單
     *
     * @param result a {@link org.springframework.validation.BindingResult} object.
     * @param sysop a {@link fstop.orm.po.SYSOP} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/CreateSave", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit=true)
    public JsonResponse createSave(@ModelAttribute @Valid SYSOP sysop, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
            	if ( sysOpService.getMenuByMenuId(sysop.getADOPID()) != null ) {
            		 Map<String, String> errors = new HashMap<String, String>();
            		 //Reflected XSS All Clients
            		 String adopid = (String)Sanitizer.escapeHTML(sysop.getADOPID());
                     errors.put("summary", "選單["+adopid+"]已存在");

                     response.setValidated(false);
                     response.setErrorMessages(errors);
                     return response;
            	}
        		sysOpService.insertMenu(sysop, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 20:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 修改選單存檔
     *
     * @param sysop 選單物件
     * @param result a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/EditSave", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit=true)
    public JsonResponse editSave(@ModelAttribute @Valid SYSOP sysop, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                sysOpService.saveMenu(sysop, "system");  //todo: set current login user
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 21:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 刪除選單
     *
     * @param ADOPID 選單 Id
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/Delete/{ADOPID}", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit=true)
    public @ResponseBody String delete(@PathVariable String ADOPID) {
        try {
            ADOPID = (String)Sanitizer.logForgingStr(ADOPID);
            sysOpService.deleteMenu(ADOPID);
            return "0";
        } catch (Exception e) {
            log.error("delete error", e);
            
            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }

    /**
     * 排序儲存
     *
     * @param ADSEQ 排序值，多個用 "，" 做分隔
     * @param ADOPID a {@link java.lang.String} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/SortSave")
    @Authorize(userInRoleCanEdit=true)
    public @ResponseBody JsonResponse sortSave(@RequestParam String ADOPID, @RequestParam String ADSEQ) {
        JsonResponse response = new JsonResponse();
        try {
            Map<String, String> sortedMenus = new HashMap<String, String>();
            
            String[] ADOPIDList = ((String)Sanitizer.escapeHTML(ADOPID)).split(",");
            String[] ADSEQList = ((String)Sanitizer.escapeHTML(ADSEQ)).split(",");
            for ( int i=0; i<ADOPIDList.length; i++ ) {
                sortedMenus.put(ADSEQList[i], ADOPIDList[i]);
            }

            sysOpService.sortMenu(sortedMenus);
            response.setValidated(true);
            
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 22:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        return response;
    }

    /**
     * 移動選單存檔
     *
     * @param NewADOPGROUPID    父選單 Id
     * @param ADOPID a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/MoveSave", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit=true)
    public @ResponseBody String moveSave(@RequestParam String ADOPID, @RequestParam String NewADOPGROUPID) {
        try {
            ADOPID = (String)Sanitizer.logForgingStr(ADOPID);
            sysOpService.moveMenu(ADOPID, NewADOPGROUPID);
            return "0";
            
        } catch (Exception e) {
            log.error("moveSave Error", e);

            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }
}
