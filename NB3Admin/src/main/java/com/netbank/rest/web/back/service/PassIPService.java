package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.domain.orm.core.Page;

import fstop.orm.dao.PassIPDao;
import fstop.orm.po.PASSIP;
import fstop.orm.po.NBIPIDENTITY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PassIPService extends BaseService {
    @Autowired
    private PassIPDao passIPDao;
    
    private final String DEFAULT_ADBRANCHID = "001";

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo 		第幾頁
	 * @param pageSize 		一頁幾筆
	 * @param orderBy 		依什麼欄位排序
	 * @param orderDir 		升冪/降冪
     * @param ADUserId 		客戶統編
     * @return 分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String ADUserId) {
    	//避免把pk帶入sql的orderby中，去掉PK 只留下key name 
        if(orderBy.contains(".")){
            String[] splitstr = orderBy.split("\\.");
            orderBy = splitstr[splitstr.length-1];
         }
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, ADUserId={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ADUserId));

        log4Query("0", new String[][] { { "ADUserId", ADUserId } } );
        
        return passIPDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, DEFAULT_ADBRANCHID, ADUserId);
    }

    /**
     * 取得資料
     *
     * @param passIPIdentity 分行及使用者統編
     * @return POJO
     */
    public PASSIP getPassIP(String ADUserId) {
        log4Query("0", new String[][] { {"ADUserId", ADUserId}} );
        
        NBIPIDENTITY identity = new NBIPIDENTITY();
        identity.setADBRANCHID(DEFAULT_ADBRANCHID.toCharArray());
        identity.setADBRANCHIP(ADUserId.toCharArray());
        return passIPDao.findById(identity);
    }

    /**
     * 新增資料
     *
     * @param passIP 分行及使用者統編
     * @param creator 編輯者
     */
    public void insertPassIP(PASSIP passIP, String creator) {
    	passIP.setLASTUSER(creator); 
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        passIP.setLASTDATE(parts[0]);
        passIP.setLASTTIME(parts[1]);

        passIPDao.save(passIP);
        
        log4Create(passIP, "0");
    } 

    /**
     * 儲存資料
     *
     * @param passIP  異動 POJO
     * @param editor    編輯者
     */
    public void savePassIP(PASSIP passIP, String editor) {
        PASSIP oriPassIP = passIPDao.findById(passIP.getNBIPIDENTITY());
        passIPDao.getEntityManager().detach(oriPassIP);

        passIP.setLASTUSER(editor); 

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        passIP.setLASTDATE(parts[0]);
        passIP.setLASTTIME(parts[1]);

        passIPDao.update(passIP);
        log4Update(oriPassIP, passIP, "0");
    }

    /**
     * 刪除資料
     *
     * @param ADBRANCHIP	
     */
    public void deletePassIP(String ADBRANCHIP) {
    	NBIPIDENTITY nbIPIdentity = new NBIPIDENTITY();
    	nbIPIdentity.setADBRANCHID(DEFAULT_ADBRANCHID.toCharArray());
    	nbIPIdentity.setADBRANCHIP(ADBRANCHIP.toCharArray());
    	
        PASSIP oriPassIP = passIPDao.findById(nbIPIdentity);
        passIPDao.removeById(nbIPIdentity);  
        log4Delete(oriPassIP, "0");
        
        // Heap Inspection, 把可能留在記憶體中的資料清除
        Arrays.fill(oriPassIP.getNBIPIDENTITY().getADBRANCHIP(), '\u0000');
        Arrays.fill(oriPassIP.getNBIPIDENTITY().getADBRANCHID(), '\u0000');
        Arrays.fill(nbIPIdentity.getADBRANCHIP(), '\u0000');
        Arrays.fill(nbIPIdentity.getADBRANCHID(), '\u0000');
    }

    /**
     * 整批上傳
     *
     * @param adUserIDs		使用者清單
     * @param actor 		動作者
     */
    public void uploadPassIP(String[] adUserIDs, String actor) {
        for ( String adUserID : adUserIDs ) {
            if ( !adUserID.isEmpty() ) {
            	NBIPIDENTITY identity = new NBIPIDENTITY();
            	identity.setADBRANCHID(DEFAULT_ADBRANCHID.toCharArray());
            	identity.setADBRANCHIP(adUserID.toCharArray());
            	
                if ( passIPDao.findById(identity) == null ) {
                	PASSIP newPassIP = new PASSIP();
                	
                	newPassIP.setNBIPIDENTITY(identity);
                    insertPassIP(newPassIP, actor);
                } 
            }
        }
    }
}
