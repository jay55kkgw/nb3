package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ADMCOUNTRYService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMCOUNTRY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 國別檔維護控制器，使用一個畫面做 CRUD
 *
 * @author 簡哥
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B902")
public class B902Controller {
    @Autowired
    private ADMCOUNTRYService admCountryService;
    
    @Autowired
    private PortalSSOService portalSSOService;

    /**
     * 取得國別檔維護首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value={"", "/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        model.addAttribute("allowEdit", portalSSOService.isPermissionOK("B902", AuthorityEnum.EDIT));
        return "B902/index";
    }

    /**
     * 分頁查詢
     *
     * @param ADCTRYCODE 國別代碼
     * @param ADCTRYNAME 國別繁中
     * @param ADCTRYENGNAME 國別英文
     * @param ADCTRYCHSNAME 國別簡中
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return 支援 jQuery Datatable Server 端的分頁物件
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value="/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String ADCTRYCODE, @RequestParam String ADCTRYNAME, @RequestParam String ADCTRYENGNAME, @RequestParam String ADCTRYCHSNAME, HttpServletRequest request, HttpServletResponse response ) {
        log.debug("query ADCTRYCODE={}, ADCTRYNAME={}, ADCTRYENGNAME={}", Sanitizer.logForgingStr(ADCTRYCODE), Sanitizer.logForgingStr(ADCTRYNAME), Sanitizer.logForgingStr(ADCTRYENGNAME));
        
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admCountryService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(), 
            jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(ADCTRYCODE), 
            (String)Sanitizer.escapeHTML(ADCTRYNAME), (String)Sanitizer.escapeHTML(ADCTRYENGNAME),
            (String)Sanitizer.escapeHTML(ADCTRYCHSNAME));

        List<ADMCOUNTRY> countrys=(List<ADMCOUNTRY>)page.getResult();
        List<ADMCOUNTRY> scountrys= new ArrayList<ADMCOUNTRY>();
        for (ADMCOUNTRY country : countrys) {
            ADMCOUNTRY scountry=new ADMCOUNTRY();
            Sanitizer.escape4Class(country, scountry);
            scountrys.add(scountry);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), scountrys);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得單一國別資料
     *
     * @param ADCTRYCODE 國別代碼
     * @return ADMCOUNTRY Json 物件
     */
    @PostMapping(value="/Get/{ADCTRYCODE}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody ADMCOUNTRY get(@PathVariable String ADCTRYCODE) {
        ADCTRYCODE = (String)Sanitizer.logForgingStr(ADCTRYCODE);

        //20191212-Danny-Missing Content Security Policy
        ADMCOUNTRY entity = admCountryService.getByADCTRYCODE(ADCTRYCODE, true);
        ADMCOUNTRY oEntity = new ADMCOUNTRY();
        Sanitizer.escape4Class(entity, oEntity);
        return oEntity;
        
    }

    /**
     * 新增國別資料
     *
     * @param admCountry 國別物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse create(@ModelAttribute @Valid ADMCOUNTRY admCountry, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                if ( admCountryService.getByADCTRYCODE(admCountry.getADCTRYCODE(), false) == null ) {
                    admCountryService.insertCountry(admCountry, portalSSOService.getLoginUserId());
                    response.setValidated(true);
                } else {
                	Map<String, String> errors = new HashMap<String, String>();

                	//Reflected XSS All Clients
      		 	 	String code = (String)Sanitizer.escapeHTML(admCountry.getADCTRYCODE());
                    errors.put("summary", "國別代碼["+code+"]已存在");

                    response.setValidated(false);
                    response.setErrorMessages(errors);
                    return response;
                }
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 48:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 修改國別資料
     *
     * @param admCountry 國別物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid ADMCOUNTRY admCountry, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                admCountryService.saveCountry(admCountry, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 49:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 刪除國別資料
     *
     * @param ADCTRYCODE 國別代碼
     * @return "0"：成功，非"0"：失敗原因
     */
    @PostMapping(value="/Delete/{ADCTRYCODE}", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String delete(@PathVariable String ADCTRYCODE) {
        try {
            ADCTRYCODE = (String)Sanitizer.logForgingStr(ADCTRYCODE);
            admCountryService.deleteCountry(ADCTRYCODE);
            return "0";
        } catch (Exception e) {
            log.error("delete error", e);

            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }
}
