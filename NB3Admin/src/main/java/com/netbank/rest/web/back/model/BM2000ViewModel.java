package com.netbank.rest.web.back.model;

/**
 * 
 */
public class BM2000ViewModel {
    private Integer APPID;
    private String USERID;
    private String USERNAME;
    private String TXDATE;
    private String TXTIME;
    private String APPOS;
    private String APPMODELNO;
    private String VERNO;
    private String VERDATE;
    private String VERSTATUS;
    private String VERMSG;
    private String VERURL;

    public Integer getAPPID() {
        return APPID;
    }

    public String getAPPMODELNO() {
        return APPMODELNO;
    }
    public String getVERNO() {
        return VERNO;
    }

    public String getAPPOS() {
        return APPOS;
    }
    public String getTXDATE() {
        return TXDATE;
    }

    public String getTXTIME() {
        return TXTIME;
    }


    public void setVERSTATUS(String vERSTATUS) {
        this.VERSTATUS = vERSTATUS;
    }

    
    public String getVERSTATUS() {
        return VERSTATUS;
    }

    public void setVERMSG(String vERMSG) {
        this.VERMSG = vERMSG;
    }

    
    public String getVERMSG() {
        return VERMSG;
    }


    public void setVERURL(String vERURL) {
        this.VERURL = vERURL;
    }

    
    public String getVERURL() {
        return VERURL;
    }



    public void setTXTIME(String tXTIME) {
        this.TXTIME = tXTIME;
    }

    
    public String getVERDATE() {
        return VERDATE;
    }



    public void setVERDATE(String vERDATE) {
        this.VERDATE = vERDATE;
    }




    public void setVERNO(String vERNO) {
        this.VERNO = vERNO;
    }
    public void setAPPMODELNO(String aPPMODELNO) {
        this.APPMODELNO = aPPMODELNO;
    }

    public void setAPPOS(String aPPOS) {
        this.APPOS = aPPOS;
    }

    public void setTXDATE(String tXDATE) {
        this.TXDATE = tXDATE;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public void setUSERNAME(String uSERNAME) {
        this.USERNAME = uSERNAME;
    }

    public String getUSERID() {
        return USERID;
    }

    public void setUSERID(String uSERID) {
        this.USERID = uSERID;
    }

    public void setAPPID(Integer aPPID) {
        this.APPID = aPPID;
    }

 

    
}