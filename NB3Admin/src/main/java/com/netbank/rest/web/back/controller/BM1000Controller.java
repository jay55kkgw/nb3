package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ADMAPPBRANCHService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMAPPBRANCH;
import fstop.orm.po.ADMSYSCODED;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 分行/ATM/證券據點維護控制器
 *
 * @author 簡哥
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/BM1000")
public class BM1000Controller {
    @Autowired
    private ADMAPPBRANCHService admAPPBRANCHService;

    @Autowired
    private PortalSSOService portalSSOService;

    /**
     * 取得首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        ADMAPPBRANCH po = new ADMAPPBRANCH();
        model.addAttribute("data", po);
        model.addAttribute("IsEdit", true);
        List<ADMSYSCODED> city = admAPPBRANCHService.getCounty();
        model.addAttribute("DataCity", city);
        if ( portalSSOService.isPermissionOK("BM1000", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }  
        return "BM1000/index";
    }

    /**
     * 取得輸入頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value = { "/Create" })
    @Authorize(userInRoleCanEdit = true)
    public String create(ModelMap model) {
        List<ADMSYSCODED> city = admAPPBRANCHService.getCounty();
        model.addAttribute("DataCity", city);
        return "BM1000/create";
    }

    /**
     * 分頁查詢
     *
     * @param BHADTYPE 據點類型
     * @param BHNAME   據點名稱
     * @param BHCOUNTY 縣市別
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @return a {@link com.netbank.rest.web.back.util.JQueryDataTableResponse} object.
     */
    @PostMapping(value = "/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String BHADTYPE, @RequestParam String BHNAME,
            @RequestParam String BHCOUNTY, HttpServletRequest request, HttpServletResponse response) {
        log.debug("query BHADTYPE={}, BHNAME={}, BHCOUNTY={}", Sanitizer.logForgingStr(BHADTYPE), Sanitizer.logForgingStr(BHNAME), Sanitizer.logForgingStr(BHCOUNTY));

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admAPPBRANCHService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
                jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(BHADTYPE), 
                (String)Sanitizer.escapeHTML(BHNAME), (String)Sanitizer.escapeHTML(BHCOUNTY));

        List<ADMAPPBRANCH> admAppBranches = (List<ADMAPPBRANCH>) page.getResult();
        List<ADMAPPBRANCH> sadmAppBranches = new ArrayList<ADMAPPBRANCH>();
        for (ADMAPPBRANCH admAppBranche : admAppBranches) {
            ADMAPPBRANCH sadmAppBranche = new ADMAPPBRANCH();
            Sanitizer.escape4Class(admAppBranche, sadmAppBranche);
            sadmAppBranches.add(sadmAppBranche);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), sadmAppBranches);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }
    

    /**
     * <p>Edit.</p>
     *
     * @param BHID a int.
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value = { "/Edit/{BHID}" })
    @Authorize(userInRoleCanQuery = true)
    public String Edit(@PathVariable int BHID, ModelMap model) {
        ADMAPPBRANCH pojo = admAPPBRANCHService.getByBHID(BHID);
        model.addAttribute("data", pojo);
        model.addAttribute("IsEdit", true);

        List<ADMSYSCODED> city = admAPPBRANCHService.getCounty();
        model.addAttribute("DataCity", city);
        return "BM1000/create";
    }

    /**
     * 新增資料
     *
     * @param admAppBranch 據點 POJO
     * @param result a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value = "/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse create(@ModelAttribute @Valid ADMAPPBRANCH admAppBranch, BindingResult result) {
        JsonResponse response = new JsonResponse();
        try {
            if (result.hasErrors()) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                        .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                
                admAPPBRANCHService.insertAppBranch(admAppBranch, portalSSOService.getLoginUserId(),portalSSOService.getLoginUserName());
                response.setValidated(true);
            }
        } catch (Exception e) {
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "存檔發生錯誤，請確認資料是否有更新成功!!";
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", message);

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }

        return response;
    }

    /**
     * 修改資料
     *
     * @param admAppBranch 據點 POJO
     * @param result a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value = "/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid ADMAPPBRANCH admAppBranch, BindingResult result) {
        JsonResponse response = new JsonResponse();
        try {
            if (result.hasErrors()) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                        .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                ADMAPPBRANCH saveEntity = new ADMAPPBRANCH();
                saveEntity.setBHID(admAppBranch.getBHID());

                saveEntity.setBHADTYPE(admAppBranch.getBHADTYPE());
                saveEntity.setBHNAME(admAppBranch.getBHNAME());
                saveEntity.setBHCOUNTY(admAppBranch.getBHCOUNTY());
                saveEntity.setBHADDR(admAppBranch.getBHADDR());
                saveEntity.setBHLATITUDE(admAppBranch.getBHLATITUDE());
                saveEntity.setBHLONGITUDE(admAppBranch.getBHLONGITUDE());
                saveEntity.setBHTELCOUNTRY(admAppBranch.getBHTELCOUNTRY());
                saveEntity.setBHTELREGION(admAppBranch.getBHTELREGION());
                saveEntity.setBHTEL(admAppBranch.getBHTEL());
                saveEntity.setBHSTIME(admAppBranch.getBHSTIME());
                saveEntity.setBHETIME(admAppBranch.getBHETIME());

                admAPPBRANCHService.saveAppBranch(saveEntity, portalSSOService.getLoginUserId(),portalSSOService.getLoginUserName());
                response.setValidated(true);
            }
        } catch (Exception e) {
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "存檔發生錯誤，請確認資料是否有更新成功!!";
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", message);

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }

        return response;
    }

    /**
     * 刪除資料
     *
     * @param BHID 據點 ID
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value = "/Delete/{BHID}", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String delete(@PathVariable int BHID) {
        try {
            admAPPBRANCHService.deleteAppBranch(BHID);
            return "0";
        } catch (Exception e) {
            log.error("delete error", e);
            
            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }

    @GetMapping(value = { "/Get/{BHID}" })
    @Authorize(userInRoleCanQuery = true)
    public String get(@PathVariable int BHID, ModelMap model) {
        ADMAPPBRANCH pojo = admAPPBRANCHService.getByBHID(BHID);
        model.addAttribute("Data", pojo);
        model.addAttribute("IsEdit", true);
        List<String> city = admAPPBRANCHService.getBhCounty();
        model.addAttribute("DataCity", city);
        return "BM1000/query";
    } 
}
