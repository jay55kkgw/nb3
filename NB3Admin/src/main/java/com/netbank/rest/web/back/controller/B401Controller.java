package com.netbank.rest.web.back.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.service.SYSERRLOGService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.SYSERRLOG;
import fstop.util.Sanitizer;

/**
 * <p>系統異常事件查詢</p>
 * 
 * @author 簡哥
 */
@Controller
@RequestMapping("/B401")
public class B401Controller {
    @Autowired
    private SYSERRLOGService sysErrLogService;

    /**
     * 取得查詢頁面
     * @param model
     * @return
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd 00:00:00");
        DateFormat endDateFormat = new SimpleDateFormat("yyyy/MM/dd 23:59:59");
        Date date = new Date();
        String now = dateFormat.format(date);
        String endDate= endDateFormat.format(date);
        model.addAttribute("StartTime", now);
        model.addAttribute("EndTime", endDate);
        return "B401/index";
    }

    /**
     * 分頁查詢
     * @param StartTime
     * @param EndTime
     * @param LoginUserId
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value="/IndexQuery", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    @SuppressWarnings("unchecked")
    public @ResponseBody JQueryDataTableResponse indexQuery(@RequestParam String startTime, @RequestParam String endTime, HttpServletRequest request, HttpServletResponse response ) {
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = sysErrLogService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(), jqDataTableRq.getOrderBy(), 
            jqDataTableRq.getOrderDir(), (String)Sanitizer.parseDateTime("yyyy/MM/dd HH:mm:ss",startTime), 
            (String)Sanitizer.parseDateTime("yyyy/MM/dd HH:mm:ss", endTime));

        List<SYSERRLOG> syserrlogs = (List<SYSERRLOG>)page.getResult();
        List<SYSERRLOG> ssyserrlogs = new ArrayList<SYSERRLOG>();
        
        for (SYSERRLOG syserrlog : syserrlogs) {
            SYSERRLOG ssyserrlog = new SYSERRLOG();
            Sanitizer.escape4Class(syserrlog, ssyserrlog);

            ssyserrlogs.add(ssyserrlog);
        }
        
        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), ssyserrlogs);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }
}