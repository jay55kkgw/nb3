package com.netbank.rest.web.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.SysParamDao;
import fstop.orm.po.SYSPARAM;

@Service
public class SYSPARAMService extends BaseService {
	@Autowired
    private SysParamDao sysParamDao;

	public SYSPARAM getParamName(String paramName) {
		return sysParamDao.getBySysParam(paramName);
	}
	
	public String getByParamName(String paramName) {
		return sysParamDao.getSysParam(paramName);
	}
	
	public void update(SYSPARAM entity) {
		sysParamDao.update(entity);
	}
}
	
