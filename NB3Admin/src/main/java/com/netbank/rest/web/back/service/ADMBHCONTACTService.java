package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmBhContactDao;
import fstop.orm.po.ADMBHCONTACT;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>ADMBHCONTACTService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class ADMBHCONTACTService extends BaseService{
    @Autowired
    private AdmBhContactDao admBhContactDao;

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo 第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy 依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param ADBRANCHID 分行代碼
     * @param ADBRANCHNAME 分行名稱
     * @param ADCONTACTIID 分行聯絡人代號
     * @param ADCONTACTNAME 分行聯絡人名稱
     * @param ADCONTACTEMAIL 分行聯絡人電郵
     * @param ADCONTACTTEL 分行聯絡人電話
     * @return 分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String ADBRANCHID, String ADBRANCHNAME,String ADCONTACTIID,String ADCONTACTNAME,String ADCONTACTEMAIL,String ADCONTACTTEL) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, ADBRANCHID={}, ADBRANCHNAME={},ADCONTACTIID={},ADCONTACTNAME={},ADCONTACTEMAIL={},ADCONTACTTEL={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ADBRANCHID), Sanitizer.logForgingStr(ADBRANCHNAME),Sanitizer.logForgingStr(ADCONTACTIID),Sanitizer.logForgingStr(ADCONTACTNAME),Sanitizer.logForgingStr(ADCONTACTEMAIL),Sanitizer.logForgingStr(ADCONTACTTEL));

        log4Query("0", new String[][] { { "ADBRANCHID", ADBRANCHID }, { "ADBRANCHNAME", ADBRANCHNAME }, 
            { "ADCONTACTIID", ADCONTACTIID }, { "ADCONTACTNAME", ADCONTACTNAME },
            { "ADCONTACTEMAIL", ADCONTACTEMAIL }, { "ADCONTACTTEL", ADCONTACTTEL } } );
            
        return admBhContactDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, ADBRANCHID, ADBRANCHNAME,ADCONTACTIID,ADCONTACTNAME,ADCONTACTEMAIL,ADCONTACTTEL);
    }

    /**
     * 取得分行外匯聯絡人資料
     *
     * @param ADBHCONTID 代碼
     * @return 分行外匯聯絡人 POJO
     */
    public ADMBHCONTACT getByID(Integer ADBHCONTID) {
        log.debug("getByID ADBHCONTID={}", Sanitizer.logForgingStr(ADBHCONTID));

        log4Query("0", new String[][] { { "ADBHCONTID", Integer.toString(ADBHCONTID) } } );
        return admBhContactDao.findById(ADBHCONTID);
    }


    /**
     * 新增分行外匯聯絡人資料
     *
     * @param admbhcontact 行外匯聯絡人資料 POJO
     * @param creator 編輯者
     */
    public void insert(ADMBHCONTACT admbhcontact, String creator) {
        admbhcontact.setLASTUSER(creator); 
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        admbhcontact.setLASTDATE(parts[0]);
        admbhcontact.setLASTTIME(parts[1]);

        admBhContactDao.save(admbhcontact);
        log4Create(admbhcontact, "0");
    } 

    /**
     * 儲存分行外匯聯絡人資料
     *
     * @param admbhcontact 行外匯聯絡人資料 POJO
     * @param editor 編輯者
     */
    public void save(ADMBHCONTACT admbhcontact, String editor) {
        ADMBHCONTACT oriADMBHCONTACT = admBhContactDao.findById(admbhcontact.getADBHCONTID());
        admBhContactDao.getEntityManager().detach(oriADMBHCONTACT);

        admbhcontact.setLASTUSER(editor); 

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        admbhcontact.setLASTDATE(parts[0]);
        admbhcontact.setLASTTIME(parts[1]);

        admBhContactDao.update(admbhcontact);
        log4Update(oriADMBHCONTACT, admbhcontact, "0");
    }

    /**
     * 刪除分行外匯聯絡人資料
     *
     * @param adbhcontid 代碼
     */
    public void delete(Integer adbhcontid) {
        ADMBHCONTACT oriADMBHCONTACT = admBhContactDao.findById(adbhcontid);
        admBhContactDao.removeById(adbhcontid);  
        log4Delete(oriADMBHCONTACT, "0");
    }
   
}
