package com.netbank.rest.web.back.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import fstop.orm.po.TXNLOG;
import lombok.Data;

/**
 * 
 */
@Data
public class B201Model implements Serializable{
	
	private String ADTXNO = "";//交易編號uuid

	private String ADUSERID = "";//操作者代號

	private String ADOPID = "";//操作功能ID

	private String FGTXWAY = "";//交易機制

	private String ADTXACNO = "";//交易帳號/卡號(轉出或設定)

	private String ADTXAMT = "";//轉出交易金額

	private String ADCURRENCY = "";//轉出幣別

	private String ADSVBH = "";//轉入銀行代碼

	private String ADAGREEF = "";//轉入帳號是約定或非約定帳號註記

	private String ADREQTYPE = "";//REQUEST的類型

	private String ADUSERIP = "";//操作者IP位址
		
	private String ADTOTACONTENT = ""; //下行紀錄內容
	
	private String ADEXCODE = "";//交易錯誤代碼

	private String ADGUID = "";//軌跡uuid
	
	private String ADTXID = "";  //指出哪一個電文有誤

	private String LASTDATE = "";
	
	private String LASTTIME = "";
	
	private String PSTIMEDIFF = "";//處理時間差
	
	private String LOGINTYPE = "";//判斷NB:網銀，MB:行動
}