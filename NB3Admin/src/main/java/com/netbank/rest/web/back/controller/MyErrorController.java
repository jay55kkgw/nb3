package com.netbank.rest.web.back.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>自訂錯誤頁面</p>
 * 
 * @author  簡哥
 */
@Controller
public class MyErrorController implements ErrorController {
    @RequestMapping("/error")
    public String handleError(ModelMap model, HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        
        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());
        
            model.addAttribute("errorType", statusCode.toString());
            if(statusCode == HttpStatus.NOT_FOUND.value()) {
                model.addAttribute("errorDesc", "您所要執行的網頁不存在");
            } else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                model.addAttribute("errorDesc", "伺服器發生錯誤");
            } else {
                model.addAttribute("errorDesc", "錯誤不明");
            }
        }
        return "Error";
    }

    
    @Override
    public String getErrorPath() {
        return "";
    }
}