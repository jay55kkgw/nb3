package com.netbank.rest.web.back.service;

import java.util.List;
import java.util.Map;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.B701Model;
import com.netbank.rest.web.back.model.B702Model;
import com.netbank.rest.web.back.model.B703Model;
import com.netbank.rest.web.back.model.B704Model;
import com.netbank.rest.web.back.model.B706Result;
import com.netbank.rest.web.back.model.B706ResultOP;
import com.netbank.rest.web.back.model.B707Result;
import com.netbank.rest.web.back.model.B708Result;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.dao.AdmDayHHReportDao;
import fstop.orm.dao.AdmLogInOutDao;
import fstop.orm.dao.AdmMonthReportDao;
import fstop.orm.dao.TxnLogDao;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>NMBReportService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class NMBReportService extends BaseService {

    @Autowired
    private AdmLogInOutDao admLogInOutDao;

    @Autowired
    private AdmMonthReportDao admMonthReportDao;

    @Autowired
    private AdmDayHHReportDao admDayHHReportDao;

    @Autowired
    private TxnLogDao txnLogDao;

    /**
     * 取得 ADMMONTHREPORT 資料
     *
     * @return ADMMSGCODE POJO
     * @param entity a {@link com.netbank.rest.web.back.model.B704Model} object.
     */
    public Rows queryB704(B704Model entity) {

        String stadate = entity.getYearS() + entity.getMonthS();

        Rows qresult = admMonthReportDao.findTransactionReportAgreeNew(stadate, stadate);

        log4Query("0", new String[][] {{"stadate",stadate}});
        
        return qresult;
    }

    /**
     * 取得 ADMLOGINOUT 資料
     *
     * @return ADMMSGCODE POJO
     * @param entity a {@link com.netbank.rest.web.back.model.B701Model} object.
     */
    public Rows query(B701Model entity) {

        String periodStr = "";
        // LOGINTYPE = StrUtils.trim(LOGINTYPE).replaceAll("'", "");

        Rows qresult = null;
        if (entity.getOptradio().equals("Month")) {
            String stadate = entity.getYearS() + entity.getMonthS();
            String enddate = entity.getYearE() + entity.getMonthE();
            periodStr = entity.getYearS() + "/" + entity.getMonthS() + "~" + entity.getYearE() + "/"
                    + entity.getMonthE();

            qresult = admLogInOutDao.findLoginReportByMonth(stadate, enddate, entity.getLoginType());
            log4Query("0", new String[][] {{ "stadate", stadate },{ "enddate", enddate},{ "LoginType", entity.getLoginType()}});
        } else {
            String stadate = entity.getDateFrom();
            String enddate = entity.getDateTo();
            periodStr = stadate + "~" + enddate;
            stadate = StrUtils.trim(stadate).replaceAll("/", ""); // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
            enddate = StrUtils.trim(enddate).replaceAll("/", ""); // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd

            qresult = admLogInOutDao.findLoginReportByDate(stadate, enddate, entity.getLoginType());
            log4Query("0", new String[][] {{ "stadate", stadate },{ "enddate", enddate},{ "LoginType", entity.getLoginType()}});
        }

        return qresult;
    }

    /**
     * 取得 ADMMONTHREPORT 資料
     *
     * @return ADMMSGCODE POJO
     * @param entity a {@link com.netbank.rest.web.back.model.B704Model} object.
     */
    public Rows queryB705(B704Model entity) {

        String stadate = entity.getYearS() + entity.getMonthS();

        Rows qresult = admMonthReportDao.findTransactionReportLevelNew(stadate, stadate);
        
        log4Query("0", new String[][] {{"stadate", stadate}});
        
        return qresult;
    }

    /**
     * 取得 ADMMONTHREPORT 資料
     *
     * @return Rows
     * @param entity a {@link com.netbank.rest.web.back.model.B702Model} object.
     */
    public Rows queryB702(B702Model entity) {

        String stadate = entity.getYYYY() + entity.getMM();

        Rows qresult = admMonthReportDao.findTransactionReportCountNew(stadate, stadate, entity.getLoginType());

        log4Query("0", new String[][] {{"stadate", stadate},{"LoginType",entity.getLoginType()}});
        return qresult;
    }

    /**
     * 取得資料
     *
     * @return Rows
     * @param entity a {@link com.netbank.rest.web.back.model.B703Model} object.
     */
    public Rows queryB703GPA(B703Model entity) {

        String stadate = entity.getYYYY() + entity.getMM();
        String txid = entity.getOptradio().equals("TXID") ? entity.getTxId() : "" ;
        Rows qresult = admMonthReportDao.findTransactionReportCountB703GPA(stadate, stadate, txid, entity.getLoginType());

        log4Query("0", new String[][] {{"stadate", stadate},{"txid",txid},{"logintype",entity.getLoginType()}});
        return qresult;
    }

    /**
     * 取得資料
     *
     * @return Rows
     * @param entity a {@link com.netbank.rest.web.back.model.B703Model} object.
     */
    public Rows queryB703GPBE(B703Model entity) {

        String stadate = entity.getYYYY() + entity.getMM();

        Rows qresult = admMonthReportDao.findTransactionReportCountB703GPBE(stadate, stadate, entity.getLoginType());
        
        log4Query("0", new String[][] {{"stadate", stadate},{"txid",entity.getOptradio().equals("TXID") ? entity.getTxId() : ""},{"logintype",entity.getLoginType()}});
        return qresult;
    }


	public List<B707Result> findTxnLogGroupByDateLogintype(String YYYYMM) throws Exception  {
		log4Query("0", new String[][] {{"YYYYMM", YYYYMM}});
		return txnLogDao.findTxnLogGroupByDateLogintype(YYYYMM);
    }
    
    public List<B706Result> findScheduleTxnResultReport(String startDate, String endDate) throws Exception {
    	log4Query("0", new String[][] {{"startDate", startDate},{"endDate", endDate}});
        return admMonthReportDao.findScheduleTxnResultReport(startDate, endDate);
    }
    
    public Map findResultReportOP() throws Exception {
        return admMonthReportDao.findResultReportOP();
    }
        
    public List<List<B708Result>> queryB708(String type, String queryDate) throws Exception {
    	log4Query("0", new String[][] {{"type", type},{"queryDate",queryDate}});
    	
        if ( type.compareToIgnoreCase("Stat") == 0 ) {
            return admDayHHReportDao.findByTop3Data(queryDate);
        } else {
            return admDayHHReportDao.findByHourData(queryDate);
        }
    }
}
