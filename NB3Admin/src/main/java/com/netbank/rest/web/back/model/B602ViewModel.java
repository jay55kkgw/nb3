package com.netbank.rest.web.back.model;

import javax.validation.constraints.NotEmpty;

/**
 * B602 查詢 View Model
 *
 * @author 簡哥
 * @version V1.0
 */
public class B602ViewModel {

    @NotEmpty(message="使用者ID不可為空值.")
    private String ADUSERID="";

    private String ADSENDSTATUS="";

    private String ADQSDATE=""; 

    private String ADQEDATE="";

    /**
     * <p>getADUSERID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADUSERID() {
      return ADUSERID;
    }

    /**
     * <p>setADUSERID.</p>
     *
     * @param aDUSERID a {@link java.lang.String} object.
     */
    public void setADUSERID(String aDUSERID) {
      ADUSERID = aDUSERID;
    }

    /**
     * <p>getADSENDSTATUS.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADSENDSTATUS() {
      return ADSENDSTATUS;
    }

    /**
     * <p>setADSENDSTATUS.</p>
     *
     * @param aDSENDSTATUS a {@link java.lang.String} object.
     */
    public void setADSENDSTATUS(String aDSENDSTATUS) {
      ADSENDSTATUS = aDSENDSTATUS;
    }

    /**
     * <p>getADQSDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADQSDATE() {
      return ADQSDATE;
    }

    /**
     * <p>setADQSDATE.</p>
     *
     * @param aDQSDATE a {@link java.lang.String} object.
     */
    public void setADQSDATE(String aDQSDATE) {
      ADQSDATE = aDQSDATE;
    }

    /**
     * <p>getADQEDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADQEDATE() {
      return ADQEDATE;
    }

    /**
     * <p>setADQEDATE.</p>
     *
     * @param aDQEDATE a {@link java.lang.String} object.
     */
    public void setADQEDATE(String aDQEDATE) {
      ADQEDATE = aDQEDATE;
    }
}
