package com.netbank.rest.web.back.controller;

import java.util.Calendar;
import java.util.Date;

import com.netbank.rest.web.back.service.NB3SYSOPService;
import com.netbank.rest.web.back.service.TXNGDRECORDService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import fstop.aop.Authorize;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import com.google.gson.Gson;
import com.netbank.rest.web.back.model.B206Model;

import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.TXNGDRECORD;
import fstop.util.Sanitizer;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 黃金存摺交易3公斤(含)以上查詢
 * 
 * @author Alison
 */
@Controller
@Slf4j
@RequestMapping("/B206")
public class B206Controller {
    @Autowired
    private TXNGDRECORDService txnGdRecordService;

    @Autowired
    private NB3SYSOPService nb3SYSOPService;
    
    /**
     * 取得查詢頁
     * 
     * @param model
     * @return
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {

        B206Model entity = new B206Model();
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd 00:00:00");
        DateFormat endDateFormat = new SimpleDateFormat("yyyy/MM/dd 23:59:59");
        Date date = new Date();
        String now = dateFormat.format(date);
        String endDate= endDateFormat.format(date);
        
        entity.setDateFrom(now);
        entity.setDateTo(endDate);

        model.addAttribute("B206Model", entity);

        return "B206/index";

    }

    /**
     * Form submit 顯示查詢結果
     * 
     * @param entity
     * @param result
     * @param model
     * @return
     */
    @PostMapping(value = "/Send")
    @Authorize(userInRoleCanQuery = true)
    public String send(@ModelAttribute B206Model entity, BindingResult result, ModelMap model) {
        log.debug("entity={}", Sanitizer.logForgingStr(entity));

        // 錯誤處理
        if (result.hasErrors()) {
            // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
            Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

            model.addAttribute("Validated", false);
            model.addAttribute("ErrMsg", errors);
            return "B206/index";
        }

        // List<B206Result> queryData = getResultList(entity);
        // model.addAttribute("Data", queryData);
        model.addAttribute("B206Model", entity);

        return "B206/index";
    }

    /**
     * 分頁查詢
     * 
     * @param BHADTYPE 據點類型
     * @param BHNAME   據點名稱
     * @param BHCOUNTY 縣市別
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value = "/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String STARTDATE, @RequestParam String ENDDATE,
            @RequestParam String USERID, HttpServletRequest request, HttpServletResponse response) {
        log.debug("query STARTDATE={}, ENDDATE={}, USERID={}", Sanitizer.logForgingStr(STARTDATE), Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID));

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        STARTDATE = STARTDATE.replace("/", "").replace(":", "").replace(" ", "");
        ENDDATE = ENDDATE.replace("/", "").replace(":", "").replace(" ", "");
        
        if (STARTDATE.length()==12)
        	STARTDATE=STARTDATE+"00";
        
        if (ENDDATE.length()==12)
        	ENDDATE=ENDDATE+"00";
        
        String orderBy = jqDataTableRq.getOrderBy();
        if ( orderBy.compareToIgnoreCase("LASTDATE") == 0 ) {
            orderBy += " "+jqDataTableRq.getOrderDir();
            orderBy += ", LASTTIME ";
        }

        Page page = txnGdRecordService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
                orderBy, jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(STARTDATE), 
                (String)Sanitizer.escapeHTML(ENDDATE), (String)Sanitizer.escapeHTML(USERID));

        List<NB3SYSOP> menus = nb3SYSOPService.getAllMenus();
        List<TXNGDRECORD> txnGdRecords = (List<TXNGDRECORD>)page.getResult();
        List<TXNGDRECORD> stxnGdRecords = new ArrayList<TXNGDRECORD>();
        for (TXNGDRECORD txnGdRecord : txnGdRecords) {
            // 把交易代碼改成 "交易代碼，交易中文"
            Optional<NB3SYSOP> sysop = menus.stream().filter(p->p.getADOPID().equalsIgnoreCase(txnGdRecord.getADOPID())).findFirst();
            if ( sysop.isPresent() ) {
                txnGdRecord.setADOPID(txnGdRecord.getADOPID()+","+sysop.get().getADOPNAME());
            }
            
            TXNGDRECORD stxnGdRecord=new TXNGDRECORD();
            Sanitizer.escape4Class(txnGdRecord, stxnGdRecord);
            stxnGdRecords.add(stxnGdRecord);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), stxnGdRecords);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }
    
}