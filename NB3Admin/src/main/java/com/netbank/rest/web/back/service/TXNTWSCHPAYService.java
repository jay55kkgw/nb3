package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.util.Sanitizer;
import fstop.orm.dao.NB3SysOpDao;
import lombok.extern.slf4j.Slf4j;

/**
 * 預約明細查詢(台幣)
 * 
 * @author Alison
 */
@Slf4j
@Service
public class TXNTWSCHPAYService extends BaseService {
    @Autowired
    private TxnTwSchPayDao txnTwSchPayDao;
    
    @Autowired
    private TxnTwSchPayDataDao txnTwSchPayDataDao;

    @Autowired
    private NB3SysOpDao nb3SysOpDao;

    /**
     * 分頁查詢
     * 
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param orderDir
     * @param USERID
     * @return
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String USERID) {
        //避免orderby的key包含了pk，要去掉放入sql
        if(orderBy.contains(".")){
            String[] splitstr = orderBy.split("\\.");
            orderBy = splitstr[splitstr.length-1];
         }
         
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, USERID={}", Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy),
        Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(USERID));


        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String today = dateFormat.format(date);
        log4Query("0", new String[][] { { "USERID", USERID } } );
        return txnTwSchPayDao.findPageData(pageNo, pageSize, orderBy, orderDir, USERID, today);
    }

    /**
     * 取得交易類別
     *
     * @param ADOPID
     * @return
     */
    public String getADOPNAME(String ADOPID) {
        log.debug("getADOPNAME, ADOPID={}", ADOPID);
        return nb3SysOpDao.getByADOPNAME(ADOPID);
    }

    /**
     * 取得下次轉帳日(台幣)
     *
     * @param userid
     * @param dpschno
     * @return
     */
    public String getDPNEXTDATE(String userid,String dpschno) {
        //20191205-Danny-Log Forging
        log.debug("getDPNEXTDATE, userid={},dpschno={}", Sanitizer.logForgingStr(userid),Sanitizer.logForgingStr(dpschno));
        return txnTwSchPayDataDao.getDpschtxdate(userid,dpschno);
    }


}