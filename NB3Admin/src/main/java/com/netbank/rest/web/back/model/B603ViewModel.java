package com.netbank.rest.web.back.model;

import org.springframework.web.multipart.MultipartFile;

/**
 * <p>B603ViewModel class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class B603ViewModel {
    private String sendType;
    private String title;
    private String content;

    /**
     * <p>Getter for the field <code>sendType</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSendType() {
        return sendType;
    }

    /**
     * <p>Setter for the field <code>sendType</code>.</p>
     *
     * @param sendType a {@link java.lang.String} object.
     */
    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    /**
     * <p>Getter for the field <code>title</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTitle() {
        return title;
    }

    /**
     * <p>Setter for the field <code>title</code>.</p>
     *
     * @param title a {@link java.lang.String} object.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * <p>Getter for the field <code>content</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getContent() {
        return content;
    }

    /**
     * <p>Setter for the field <code>content</code>.</p>
     *
     * @param content a {@link java.lang.String} object.
     */
    public void setContent(String content) {
        this.content = content;
    }
}
