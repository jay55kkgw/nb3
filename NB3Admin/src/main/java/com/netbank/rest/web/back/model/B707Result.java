package com.netbank.rest.web.back.model;

public class B707Result {
    private String lastDate;
    private long mbCount;
    private long nbCount;

    public B707Result() {
        mbCount=0;
        nbCount=0;
    }
    
    public String getLastDate() {
        return lastDate;
    }

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }

    public long getMbCount() {
        return mbCount;
    }

    public void setMbCount(long mbCount) {
        this.mbCount = mbCount;
    }

    public long getNbCount() {
        return nbCount;
    }

    public void setNbCount(long nbCount) {
        this.nbCount = nbCount;
    }

    
}