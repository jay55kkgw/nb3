package com.netbank.rest.web.back.model;

import java.util.ArrayList;
import java.util.List;

import fstop.orm.po.ADMFLOWDEFSTEP;
import fstop.orm.po.ADMFLOWDEFSTEPIDENTITY;

/**
 * 流程定義雙向鏈結串列
 *
 * @author 簡哥
 * @version V1.0
 */
public class FlowStep extends ADMFLOWDEFSTEP {
    private static final long serialVersionUID = 1L;

    /**
     * 指向前一節點
     */
    private List<FlowStep> PrevFlowSteps;

    /**
     * 指向下一節點
     */
    private List<FlowStep> NextFlowSteps;

    /**
     * <p>Constructor for FlowStep.</p>
     *
     * @param t a {@link fstop.orm.po.ADMFLOWDEFSTEP} object.
     */
    public FlowStep(ADMFLOWDEFSTEP t) {
        PrevFlowSteps = new ArrayList<FlowStep>();
        NextFlowSteps = new ArrayList<FlowStep>();

        ADMFLOWDEFSTEPIDENTITY pk = new ADMFLOWDEFSTEPIDENTITY();
        pk.setFLOWID(t.getSTEPIDENTITY().getFLOWID());
        pk.setSTEPID(t.getSTEPIDENTITY().getSTEPID());

        this.setSTEPIDENTITY(pk);
        this.setSTEPNAME(t.getSTEPNAME());
        this.setSTEPTYPE(t.getSTEPTYPE());
        this.setSTEPOWNER(t.getSTEPOWNER());
        this.setSTEPOWNERTYPE(t.getSTEPOWNERTYPE());
        this.setSTEPCOUNT(t.getSTEPCOUNT());
        this.setNEXTSTEPID(t.getNEXTSTEPID());
        this.setNEXTSTEPCOND(t.getNEXTSTEPCOND());
    }

    /**
     * <p>getPrevFlowSteps.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<FlowStep> getPrevFlowSteps() {
        return PrevFlowSteps;
    }

    /**
     * <p>setPrevFlowSteps.</p>
     *
     * @param prevFlowSteps a {@link java.util.List} object.
     */
    public void setPrevFlowSteps(List<FlowStep> prevFlowSteps) {
        PrevFlowSteps = prevFlowSteps;
    }

    /**
     * <p>getNextFlowSteps.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<FlowStep> getNextFlowSteps() {
        return NextFlowSteps;
    }

    /**
     * <p>setNextFlowSteps.</p>
     *
     * @param nextFlowSteps a {@link java.util.List} object.
     */
    public void setNextFlowSteps(List<FlowStep> nextFlowSteps) {
        NextFlowSteps = nextFlowSteps;
    }
}
