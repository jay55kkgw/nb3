package com.netbank.rest.web.back.service;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fstop.orm.dao.AdmOpLogDao;
import fstop.orm.po.ADMOPLOG;

@Service
public class ADMOPLogService extends BaseService {
    @Autowired
    private AdmOpLogDao admOpLogDao;

    /**
     * 依分頁查詢
     * @param pageNo            頁碼
     * @param pageSize          一頁資料大小
     * @param orderBy           排序欄位
     * @param orderDir          排序升降幕
     * @param startTime         開始時間，例如：2019/09/01 11:12:00
     * @param endTime           結束時間
     * @param loginUserId       使用者代碼
     * @param adopid            交易代碼
     * @return                  分頁 Page 物件
     */     
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String startTime, String endTime, String loginUserId, String adopid) {
        startTime = startTime.replace("/", "").replace(":", "").replace(" ", "");
        endTime = endTime.replace("/", "").replace(":", "").replace(" ", "");

        String startDate = "";
        String endDate = "";

        if ( !startTime.isEmpty() ) {
            startDate = startTime.substring(0,8);
            startTime = startTime.substring(8,14);
        }

        if ( !endTime.isEmpty() ) {
            endDate = endTime.substring(0,8);
            endTime = endTime.substring(8,14);
        }
        Page page = admOpLogDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, startDate, startTime, endDate, endTime, loginUserId, adopid);
        log4Query("0", new String[][] { { "startTime", startTime }, { "endTime", endTime }, { "loginUserId", loginUserId }, { "adopid", adopid }} );
        return page;
    } 

    /**
     * 依主鍵查詢
     * @param id        主鍵
     * @return          ADMOPLOG 物件
     */
    public ADMOPLOG getById(String id) {
        log4Query("0", new String[][] { { "id", id }} );
        return admOpLogDao.findById(id);
    }
}