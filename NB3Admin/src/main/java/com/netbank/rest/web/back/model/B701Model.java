package com.netbank.rest.web.back.model;

import javax.validation.constraints.NotEmpty;

/**
 * <p>B701Model class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class B701Model {
    private String optradio;
    private String yearS;
    private String monthS;
    private String yearE;
    private String monthE;
    private String dateFrom;
    private String dateTo;

    @NotEmpty(message = "【交易來源】不可為空值.")
    private String loginType;

    /**
     * <p>Getter for the field <code>optradio</code>.</p>
     *
     * @return String return the optradio
     */
    public String getOptradio() {
        return optradio;
    }

    /**
     * <p>Setter for the field <code>optradio</code>.</p>
     *
     * @param optradio the optradio to set
     */
    public void setOptradio(String optradio) {
        this.optradio = optradio;
    }

    /**
     * <p>Getter for the field <code>loginType</code>.</p>
     *
     * @return String return the loginType
     */
    public String getLoginType() {
        return loginType;
    }

    /**
     * <p>Setter for the field <code>loginType</code>.</p>
     *
     * @param loginType a {@link java.lang.String} object.
     */
    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    /**
     * <p>Getter for the field <code>yearS</code>.</p>
     *
     * @return String return the yearS
     */
    public String getYearS() {
        return yearS;
    }

    /**
     * <p>Setter for the field <code>yearS</code>.</p>
     *
     * @param yearS the yearS to set
     */
    public void setYearS(String yearS) {
        this.yearS = yearS;
    }

    /**
     * <p>Getter for the field <code>monthS</code>.</p>
     *
     * @return String return the monthS
     */
    public String getMonthS() {
        return monthS;
    }

    /**
     * <p>Setter for the field <code>monthS</code>.</p>
     *
     * @param monthS the monthS to set
     */
    public void setMonthS(String monthS) {
        this.monthS = monthS;
    }

    /**
     * <p>Getter for the field <code>yearE</code>.</p>
     *
     * @return String return the yearE
     */
    public String getYearE() {
        return yearE;
    }

    /**
     * <p>Setter for the field <code>yearE</code>.</p>
     *
     * @param yearE the yearE to set
     */
    public void setYearE(String yearE) {
        this.yearE = yearE;
    }

    /**
     * <p>Getter for the field <code>monthE</code>.</p>
     *
     * @return String return the monthE
     */
    public String getMonthE() {
        return monthE;
    }

    /**
     * <p>Setter for the field <code>monthE</code>.</p>
     *
     * @param monthE the monthE to set
     */
    public void setMonthE(String monthE) {
        this.monthE = monthE;
    }

    /**
     * <p>Getter for the field <code>dateFrom</code>.</p>
     *
     * @return String return the dateFrom
     */
    public String getDateFrom() {
        return dateFrom;
    }

    /**
     * <p>Setter for the field <code>dateFrom</code>.</p>
     *
     * @param dateFrom the dateFrom to set
     */
    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    /**
     * <p>Getter for the field <code>dateTo</code>.</p>
     *
     * @return String return the dateTo
     */
    public String getDateTo() {
        return dateTo;
    }

    /**
     * <p>Setter for the field <code>dateTo</code>.</p>
     *
     * @param dateTo the dateTo to set
     */
    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

}
