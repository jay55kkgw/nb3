package com.netbank.rest.web.back.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.domain.orm.core.Page;

import fstop.orm.dao.Trns_NNB_CusdataDao;
import fstop.orm.po.TRNS_NNB_CUSDATA;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class Trns_NNB_CusdataService {

	@Autowired
	private Trns_NNB_CusdataDao trns_nnb_cusdatadao;

	public List<TRNS_NNB_CUSDATA> getFaildata() {
		List<TRNS_NNB_CUSDATA> result = new ArrayList<TRNS_NNB_CUSDATA>();
		try {
			result = trns_nnb_cusdatadao.getFaildata();
		} catch (Exception e) {
			log.error("e>>{}", e);
		}
		return result;
	}
	
	public Page pageQuery(int pageNo, int pageSize, String orderBy, String orderDir) {
		Page page = new Page();
		try {
			page = trns_nnb_cusdatadao.getFaildata(pageNo, pageSize, orderBy, orderDir);
		}catch(Exception e) {
			log.error("e>>>>{}",e);
		}
		
		return page;
	}
}
