package com.netbank.rest.web.back.model;

/**
 * 
 */
public class B202ViewModel {
    private String startDate;
    private String endDate;
    private String adUserId;
    private String adOPGroup;
    private String adExCode;
    private String loginType;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAdUserId() {
        return adUserId;
    }

    public void setAdUserId(String adUserId) {
        this.adUserId = adUserId;
    }

    public String getAdOPGroup() {
        return adOPGroup;
    }

    public void setAdOPGroup(String adOPGroup) {
        this.adOPGroup = adOPGroup;
    }

    public String getAdExCode() {
        return adExCode;
    }

    public void setAdExCode(String adExCode) {
        this.adExCode = adExCode;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }
}