package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ADMCURRENCYService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMCURRENCY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 幣別維護控制器，使用一個畫面做 CRUD
 *
 * @author 毓棠
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B903")
public class B903Controller {
    @Autowired
    private ADMCURRENCYService admCurrencyService;  
    
    @Autowired
    private PortalSSOService portalSSOService;
    
    /**
     * 取得幣別維護首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value={"","/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        model.addAttribute("allowEdit", portalSSOService.isPermissionOK("B903", AuthorityEnum.EDIT));
        return "B903/index";
    }

    /**
     * 分頁查詢
     *
     * @param ADCURRENCY 幣別代號
     * @param ADCCYNO 幣別號碼
     * @param ADCCYNAME 幣別繁中名稱
     * @param ADCCYCHSNAME 幣別簡中名稱
     * @param ADCCYENGNAME 幣別英文名稱
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return 支援 jQuery Datatable Server 端的分頁物件
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value="/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String ADCURRENCY, @RequestParam String ADCCYNO, @RequestParam String ADCCYNAME, @RequestParam String ADCCYCHSNAME, @RequestParam String ADCCYENGNAME, HttpServletRequest request, HttpServletResponse response ) {
        log.debug("query ADCURRENCY={}, ADCCYNO={}, ADCCYNAME={}, ADCCYCHSNAME={}, ADCCYENGNAME={}", Sanitizer.logForgingStr(ADCURRENCY), Sanitizer.logForgingStr(ADCCYNO), Sanitizer.logForgingStr(ADCCYNAME), Sanitizer.logForgingStr(ADCCYCHSNAME), Sanitizer.logForgingStr(ADCCYENGNAME));
        
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admCurrencyService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(), 
            jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(ADCURRENCY), 
            (String)Sanitizer.escapeHTML(ADCCYNO), (String)Sanitizer.escapeHTML(ADCCYNAME),
            (String)Sanitizer.escapeHTML(ADCCYCHSNAME), (String)Sanitizer.escapeHTML(ADCCYENGNAME));

        List<ADMCURRENCY> currencys=(List<ADMCURRENCY>)page.getResult();
        List<ADMCURRENCY> scurrencys= new ArrayList<ADMCURRENCY>();
        for (ADMCURRENCY currency : currencys) {
            ADMCURRENCY scurrency = new ADMCURRENCY();
            Sanitizer.escape4Class(currency, scurrency);
            scurrencys.add(scurrency);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), scurrencys);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得單一幣別資料
     *
     * @param ADCURRENCY 幣別代碼
     * @return ADMCURRENCY Json 物件
     */
    @PostMapping(value="/Get/{ADCURRENCY}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody ADMCURRENCY get(@PathVariable String ADCURRENCY) {
        ADCURRENCY = (String)Sanitizer.logForgingStr(ADCURRENCY);
        //20191216-Danny:Missing Content Security Policy
        ADMCURRENCY entity = admCurrencyService.getByADCURRENCY(ADCURRENCY);
        ADMCURRENCY oEntity = new ADMCURRENCY();
        Sanitizer.escape4Class(entity, oEntity);
        return oEntity;
    }

    /**
     * 新增幣別資料
     *
     * @param admCurrency 幣別物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse create(@ModelAttribute @Valid ADMCURRENCY admCurrency, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                if ( admCurrencyService.getByADCURRENCY(admCurrency.getADCURRENCY()) == null ) {
                    admCurrencyService.insertCurrency(admCurrency, portalSSOService.getLoginUserId());
                    response.setValidated(true);
                } else {
                	Map<String, String> errors = new HashMap<String, String>();

                	//Reflected XSS All Clients
      		 	 	String curr = (String)Sanitizer.escapeHTML(admCurrency.getADCURRENCY());
                    errors.put("summary", "幣別代號["+curr+"]已存在");

                    response.setValidated(false);
                    response.setErrorMessages(errors);
                    return response;
                }
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 50:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 修改幣別資料
     *
     * @param admCurrency 幣別物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid ADMCURRENCY admCurrency, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                admCurrencyService.saveCurrency(admCurrency, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            log.error("error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "修改存檔發生錯誤，請確認資料是否有更新成功!!";

            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", message);

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 刪除幣別資料
     *
     * @param ADCURRENCY 幣別資料
     * @return "0"：成功，非"0"：失敗原因
     */
    @PostMapping(value="/Delete/{ADCURRENCY}", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String delete(@PathVariable String ADCURRENCY) {
        try {
            ADCURRENCY = (String)Sanitizer.logForgingStr(ADCURRENCY);
            admCurrencyService.deleteCurrency(ADCURRENCY);
            return "0";
        } catch (Exception e) {
            log.error("delete error", e);
            
            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }

}
