package com.netbank.rest.web.back.model;


import fstop.orm.po.TXNFXSCHPAY;

/**
 * 預約明細查詢(外幣)
 *
 * @author Alison
 * @version V1.0
 */
public class B204FCModel extends TXNFXSCHPAY {
    private static final long serialVersionUID = 1L;
	private String FXNEXTDATE = "";
	private String TXTYPE = "";

    /**
     * <p>getFXNEXTDATE.</p>
     *
     * @return String return the FXNEXTDATE
     */
    public String getFXNEXTDATE() {
        return FXNEXTDATE;
    }

    /**
     * <p>setFXNEXTDATE.</p>
     *
     * @param FXNEXTDATE the DPNEXTDATE to set
     */
    public void setFXNEXTDATE(String FXNEXTDATE) {
        this.FXNEXTDATE = FXNEXTDATE;
    }
	
    /** <p>getTXTYPE.</p>
     *
     * @return String return the TXTYPE
     */
    public String getTXTYPE() {
        return TXTYPE;
    }

    /**
     * <p>setTXTYPE.</p>
     *
     * @param TXTYPE the TXTYPE to set
     */
    public void setTXTYPE(String TXTYPE) {
        this.TXTYPE = TXTYPE;
    }
}
