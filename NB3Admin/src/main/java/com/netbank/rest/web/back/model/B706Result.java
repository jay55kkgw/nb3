package com.netbank.rest.web.back.model;


public class B706Result {
    private String txDay;
    private long succCnt;
    private long userReasonFailCnt;
    private long systemReasonFailCnt;
    private long otherReasonFailCnt;
    private long failCnt;
    private long notExecuteCnt;
    private long totalCnt;
    
    public B706Result() {
        succCnt=0;
        userReasonFailCnt=0;
        systemReasonFailCnt=0;
        otherReasonFailCnt=0;
        notExecuteCnt=0;
    }

    public long getOtherReasonFailCnt() {
		return otherReasonFailCnt;
	}

	public void setOtherReasonFailCnt(long otherReasonFailCnt) {
		this.otherReasonFailCnt = otherReasonFailCnt;
	}

	public long getNotExecuteCnt() {
		return notExecuteCnt;
	}

	public void setNotExecuteCnt(long notExecuteCnt) {
		this.notExecuteCnt = notExecuteCnt;
	}

	public String getTxDay() {
        return txDay;
    }

    public void setTxDay(String txDay) {
        this.txDay = txDay;
    }

    public long getSuccCnt() {
        return succCnt;
    }

    public void setSuccCnt(long succCnt) {
        this.succCnt = succCnt;
    }

    public long getUserReasonFailCnt() {
        return userReasonFailCnt;
    }

    public void setUserReasonFailCnt(long userReasonFailCnt) {
        this.userReasonFailCnt = userReasonFailCnt;
    }

    public long getSystemReasonFailCnt() {
        return systemReasonFailCnt;
    }

    public void setSystemReasonFailCnt(long systemReasonFailCnt) {
        this.systemReasonFailCnt = systemReasonFailCnt;
    }

    public long getFailCnt() {
        return this.userReasonFailCnt+this.systemReasonFailCnt+this.otherReasonFailCnt;
    }

    public long getTotalCnt() {
        return this.succCnt+this.userReasonFailCnt+this.systemReasonFailCnt+this.otherReasonFailCnt+this.notExecuteCnt;
    }
}