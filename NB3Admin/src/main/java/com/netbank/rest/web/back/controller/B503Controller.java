package com.netbank.rest.web.back.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.netbank.rest.web.back.model.AdsTmpModel;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.model.StoreTmpModel;
import com.netbank.rest.web.back.model.UploadedFile;
import com.netbank.rest.web.back.service.ADMADSService;
import com.netbank.rest.web.back.service.ADMStoreService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;
import com.netbank.util.DateUtils;
//import com.netbank.util.ImageUtil;
import com.netbank.util.ImageUtils;
import com.netbank.util.StrUtils;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import fstop.aop.Authorize;
import fstop.orm.po.ADMADSTMP;
import fstop.orm.po.ADMANNTMP;
import fstop.orm.po.ADMSTORETMP;
import fstop.orm.po.ADMUPLOADTMP;
import fstop.util.DateTimeUtils;
import fstop.util.FileUtils;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 廣告管理控制器，含經辦編輯與主管放行
 *
 * @author 
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B503")
public class B503Controller {
    @Autowired
    private ADMStoreService admStoreService;

    @Autowired
    private PortalSSOService portalSSOService;

    /**
     * 取得查詢頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"", "/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        Date now = new Date();
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.MONTH, -6);
        Date before = cal.getTime();

        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat ("yyyy/MM/dd HH:mm");
        
        model.addAttribute("StartDtFrom", formatter.format(before));
        model.addAttribute("StartDtTo", formatter.format(now));
        model.addAttribute("EndDtFrom", formatter.format(before));
        model.addAttribute("EndDtTo", formatter.format(now));
        

        return "B503/index";
    }

    /**
     * 特店維護查詢
     *
     * @param DateFrom  			起日
     * @param DateTo    			迄日
     * @param TITLE     			內容
     * @param FlowFinished			流程是否結案
     * @return          ADMADSTMP 	物件LIST
     */
    @PostMapping(value="/IndexQuery", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody List<ADMSTORETMP> indexQuery(@RequestParam String SRedio, @RequestParam String ERedio, @RequestParam String SDateFrom, @RequestParam String SDateTo, @RequestParam String FlowFinished) {
       //Log Forging\路徑 7、8:NB3Admin_20201207.pdf
    	log.debug("indexQuery SRedio={}, ERedio={}, DateFrom={}, DateTo={}", Sanitizer.logForgingStr(SRedio), Sanitizer.logForgingStr(ERedio), Sanitizer.logForgingStr(SDateFrom), Sanitizer.logForgingStr(SDateTo));
        
        List<ADMSTORETMP> adsLists = admStoreService.findStoreTmp(portalSSOService.getLoginBranch(), (String)Sanitizer.escapeHTML(SDateFrom), 
            (String)Sanitizer.escapeHTML(SDateTo),portalSSOService.getLoginRoles(),(String)Sanitizer.escapeHTML(FlowFinished));

        List<ADMSTORETMP> sadsLists = new ArrayList<ADMSTORETMP>();
        for(ADMSTORETMP adsList : adsLists) {
        	ADMSTORETMP sadsList = new ADMSTORETMP();
        	 //Log Forging\路徑 9、10:NB3Admin_20201207.pdf
        	log.info("STID = {}, ADTYPE = {}, STTYPE = {}", Sanitizer.logForgingStr(adsList.getSTID()), Sanitizer.logForgingStr(adsList.getADTYPE()), Sanitizer.logForgingStr(adsList.getSTTYPE()));
            Sanitizer.escape4Class(adsList, sadsList);
            //Stored Log Forging\路徑 6::NB3Admin_20201207.pdf
            log.info("Sanitizer STID = {}, ADTYPE = {}, STTYPE = {}", Sanitizer.logForgingStr(sadsList.getSTID()),Sanitizer.logForgingStr(sadsList.getADTYPE()), Sanitizer.logForgingStr(sadsList.getSTTYPE()));
            sadsLists.add(sadsList);
        }
        
        return sadsLists;
    }

    /**
     * 取得輸入頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"/Create"})
    @Authorize(userInRoleCanEdit = true)
    public String create(ModelMap model) {
        return "B503/create";
    }

    /**
     * 經辦送出
     *
     * @param adsTmpModel   廣告流程主檔 POJO
     * @param result        Binding 結果
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/Send", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse send(@ModelAttribute @Valid StoreTmpModel storeTmpModel, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                Map<String, String> svcResult = admStoreService.sendStoreTmp(storeTmpModel, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName());
                Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
                if ( entry.getKey().equalsIgnoreCase("0") ) {
                    response.setValidated(true);
                    response.setPkey(entry.getValue());
                } else {
                    throw new Exception(entry.getKey());
                }
            }
        } catch (Exception e) {
            //20191212-Danny-Information Exposure Through an Error Message
            log.error("send", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    } 

    /**
     * 廣告圖檔上傳
     *
     * @param servletRequest    http request
     * @param uploadedFile a {@link com.netbank.rest.web.back.model.UploadedFile} object.
     * @param bindingResult a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     * 
     * 20191217-Danny-Unsafe Object Binding調整
     */
    @RequestMapping(value="ImageUpload/{imageType}")
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse saveImage(@PathVariable String imageType, HttpServletRequest servletRequest, MultipartFile multipartFile) {
        JsonResponse response = new JsonResponse();
        try {
            String id=UUID.randomUUID().toString();
            response.setPkey(id);

            //#region
            //20191204-Danny-Unrestricted File Upload
            //允許的類型
            String[] allowFileType = {"image/jpeg","image/pjpeg","image/png","image/gif"};
            //圖示允許的檔案大小
            int allowFileSize = 10*1024*1024; // 10Mb
            String contentType = multipartFile.getContentType();
            String originalFilename = multipartFile.getOriginalFilename();
            Map<String,Object> returnMap;
           
            returnMap = FileUtils.checkFile(multipartFile, originalFilename, contentType, allowFileType, allowFileSize, false);
            //檢核不通過
            if("FALSE".equals(returnMap.get("result"))){
                String errorMessage = (String)returnMap.get("summary");
                log.debug("errorMessage={}",Sanitizer.logForgingStr(errorMessage));
                throw new Exception(errorMessage);
            }            
            //20191205-Danny-Unrestricted File Upload
            byte[] imgs=(byte[])returnMap.get("Data");

            if ( imageType.compareTo("B")==0 ) {
            	imgs = ImageUtils.scale(imgs, 1002, ImageUtils.SCALE_BASE_ON.WIDTH, FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
            	admStoreService.saveImageTmp(id, "B", multipartFile.getOriginalFilename(), imgs);
                response.setValidated(true);
            } else {
            	imgs = ImageUtils.scale(imgs, 1026, ImageUtils.SCALE_BASE_ON.WIDTH, FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
            	admStoreService.saveImageTmp(id, imageType, multipartFile.getOriginalFilename(), imgs);
                response.setValidated(true);
            }

            response.setValidated(true);
        } catch (Exception e) {
            log.error("saveImage Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 33:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        return response;
    }
    
    @RequestMapping(value="Flip/{id}")
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String flip(@PathVariable String id) {
    	List<ADMUPLOADTMP> tmps = admStoreService.getImageTmp(id);
        
        String fileName = tmps.get(0).getFILENAME();
        byte[] imageContent = tmps.get(0).getFILECONTENT();
        try {
			imageContent = ImageUtils.flipImage(imageContent, FilenameUtils.getExtension(fileName));
			admStoreService.updateImageTmp(id, imageContent);
			
			return "0";
		} catch (IOException e) {
			log.error("flip Error", e);
			return "編輯廣告圖檔發生錯誤";
		}
        
    }

    /**
     * 取得廣告圖檔
     *
     * @param id a {@link java.lang.String} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     */
    @GetMapping(value = "/ImagePreview/{id}")
    @Authorize(userInRoleCanQuery = true)
    public void getImageAsByteArray(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) throws IOException {
        id = (String)Sanitizer.logForgingStr(id);
        // 只能給 B ｜ C
        // B: Banner
        // C: Content 圖
        String size = request.getParameter("Type");
        boolean isTmp = Boolean.parseBoolean(request.getParameter("isTmp"));

        byte[] imageContent = null;
        String fileName = "";

        if ( isTmp ) {
            // 從上傳暫存檔取出檔名及內容，以利呼叫端顯示圖片
            List<ADMUPLOADTMP> tmps = admStoreService.getImageTmp(id);
           
            fileName = tmps.get(0).getFILENAME();
            imageContent = tmps.get(0).getFILECONTENT();
            
        } else {
            // 從廣告暫存檔取出檔名及內容，以利呼叫端顯示圖片
            ADMSTORETMP po = admStoreService.findById(id);
            fileName = po.getSTPICADD();
            imageContent = po.getSTPICDATA();
        }
        String fileExt="";
        int lastIndexOf = fileName.lastIndexOf(".");
        if (lastIndexOf != -1) {
            fileExt = fileName.substring(lastIndexOf+1);
        }
        if ( fileExt.compareToIgnoreCase("jpg") == 0 || fileExt.compareToIgnoreCase("jpeg") == 0 ) {
            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        } else if ( fileExt.compareToIgnoreCase("png") == 0 ) {
            response.setContentType(MediaType.IMAGE_PNG_VALUE);
        } else if ( fileExt.compareToIgnoreCase("gif") == 0 ) {
            response.setContentType(MediaType.IMAGE_GIF_VALUE);
        } else {
            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        }
        InputStream in = new ByteArrayInputStream(imageContent);
        IOUtils.copy(in, response.getOutputStream());
    }

    /**
     * 由待辦清單做案件分派
     *
     * @param id        案件代碼
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"/Query/{id}"})
    @Authorize()
    public String query(@PathVariable String id, ModelMap model) {
        id = (String)Sanitizer.logForgingStr(id);
        AuthorityEnum caseStatus = admStoreService.queryStoreTmp(id, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginRoles());
        ADMSTORETMP po = admStoreService.findById(id);

        model.addAttribute("SDateTime",
                DateUtils.getDateTimeString(po.getSDATE(), "/", po.getSTIME(), ":", false));
        model.addAttribute("EDateTime",
                DateUtils.getDateTimeString(po.getEDATE(), "/", po.getETIME(), ":", false));

        log.info("AuthorityEnum = {}",caseStatus);
        if (caseStatus == AuthorityEnum.EDIT || caseStatus == AuthorityEnum.NONE) {
            if (portalSSOService.isInRole(ADMStoreService.EDITOR_ROLES)) {
                StoreTmpModel tmp = new StoreTmpModel();
                tmp.setSTID(po.getSTID());
                tmp.setOID(po.getOID());
                tmp.setSDATE(DateUtils.getDateTimeString(po.getSDATE(), "/", po.getSTIME(), ":", false));
                log.info("SDATE = {}",tmp.getSDATE());
                tmp.setEDATE(DateUtils.getDateTimeString(po.getEDATE(), "/", po.getETIME(), ":", false));
                log.info("EDATE = {}",tmp.getEDATE());
                tmp.setADTYPE(po.getADTYPE());
                log.info("STTYPE = {}",po.getSTTYPE());
                tmp.setSTTYPE(po.getSTTYPE());
                tmp.setSTWEIGHT(po.getSTWEIGHT());
                tmp.setSTPICADD(po.getSTPICADD());
                tmp.setSTPICDATA(po.getSTPICDATA());
                tmp.setSTTEL(po.getSTTEL());
                if(StrUtils.isNotEmpty(po.getSTADDATE()))
                tmp.setSTADDATE(DateUtils.getDateString(po.getSTADDATE(), "/"));
                log.info("STADDATE = {}",tmp.getSTADDATE());
                tmp.setSTADSHOW(po.getSTADSHOW());
                tmp.setSTADNOTE(po.getSTADNOTE());
                tmp.setSTEPID(po.getSTEPID());
                tmp.setSTEPNAME(po.getSTEPNAME());
                tmp.setFLOWFINISHED(po.getFLOWFINISHED());
                
                model.addAttribute("Data", tmp);
               
                
                // 給前端頁面變更
                model.addAttribute("IsReject", caseStatus==AuthorityEnum.EDIT);
                return "B503/edit";
            } else {
            	if(StrUtils.isNotEmpty(po.getSTADDATE()))
            		po.setSTADDATE(DateUtils.getDateString(po.getSTADDATE(), "/"));
            	String base64Img="";
            	if(po.getSTPICADD().length() > 0)
            	{
            		base64Img=String.format("data:%s;base64, %s", getFileExt(po.getSTPICADD()),new String(Base64.getEncoder().encode(po.getSTPICDATA())));
            	}
            	log.info("IMGBASE64 = {}",base64Img);
            	po.setIMGBASE64(base64Img);
                model.addAttribute("Data", po);
                return "B503/query";
            }
        } else if ( caseStatus == AuthorityEnum.REVIEW ) {
        	if(StrUtils.isNotEmpty(po.getSTADDATE()))
        	{
        		po.setSTADDATE(DateUtils.getDateString(po.getSTADDATE(), "/"));
        	}
        	String base64Img="";
        	if(po.getSTPICADD().length() > 0)
        	{
        		base64Img=String.format("data:%s;base64, %s", getFileExt(po.getSTPICADD()),new String(Base64.getEncoder().encode(po.getSTPICDATA())));
        	}
        	log.info("IMGBASE64 = {}",base64Img);
        	po.setIMGBASE64(base64Img);
            model.addAttribute("Data", po);
            return "B503/review";
        } else {
        	if(StrUtils.isNotEmpty(po.getSTADDATE()))
        		po.setSTADDATE(DateUtils.getDateString(po.getSTADDATE(), "/"));
        	String base64Img="";
        	if(po.getSTPICADD().length() > 0)
        	{
        		base64Img=String.format("data:%s;base64, %s", getFileExt(po.getSTPICADD()),new String(Base64.getEncoder().encode(po.getSTPICDATA())));
        	}
        	log.info("IMGBASE64 = {}",base64Img);
        	po.setIMGBASE64(base64Img);
            model.addAttribute("Data", po);
            return "B503/query";
        }
    }

    /**
     * 上傳圖檔的首頁 Preview
     *
     * @param id        ADMUPLOADTMP.ID
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @param httpRequest a {@link javax.servlet.http.HttpServletRequest} object.
     * @param httpResponse a {@link javax.servlet.http.HttpServletResponse} object.
     * @return a {@link java.lang.String} object.
     * @throws UnsupportedEncodingException 
     */
    @GetMapping(value={"/TmpPreview/{id}"})
    @Authorize()
    public String tmpPreview(@PathVariable String id, ModelMap model, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws UnsupportedEncodingException {
        String type = httpRequest.getParameter("Type");
        String t = httpRequest.getParameter("t");
        String stid = httpRequest.getParameter("stid");
        
//        if ( type.equalsIgnoreCase("B") ) {
//            // banner
//            model.addAttribute("imgLSrc", httpRequest.getContextPath()+"/B501/ImagePreview/"+id+"?Type=B&isTmp=true");
//            model.addAttribute("imgMSrc", httpRequest.getContextPath()+"/B501/ImagePreview/"+id+"?Type=B&isTmp=true");
//            model.addAttribute("imgSSrc", httpRequest.getContextPath()+"/B501/ImagePreview/"+id+"?Type=B&isTmp=true");
//            model.addAttribute("imgCSrc", httpRequest.getContextPath()+"/img/preview/photo-00.png");
//        } else if ( type.equalsIgnoreCase("C") ) {
//            // content
//            model.addAttribute("imgLSrc", httpRequest.getContextPath()+"/img/preview/banner-lg-00.png");
//            model.addAttribute("imgMSrc", httpRequest.getContextPath()+"/img/preview/banner-md-00.png");
//            model.addAttribute("imgSSrc", httpRequest.getContextPath()+"/img/preview/banner-sm-00.png");
//            model.addAttribute("imgCSrc", httpRequest.getContextPath()+"/B501/ImagePreview/"+id+"?Size=C&isTmp=true");
//            switch ( targetType.charAt(0) ) {
//            case '1':
//            	String url = httpRequest.getParameter("u");
//                if(url != null && !url.isEmpty()) {
//                	url = URLDecoder.decode(url, "UTF-8");
//                	model.addAttribute("onclick", "window.open('"+url+"', '_blank');");
//                } else {
//                	model.addAttribute("onclick", "javascript:void(0);");
//                }                
//                break;
//            case '2':
//            	String fileGuid = httpRequest.getParameter("f");
//            	if ( fileGuid != null && !fileGuid.isEmpty() ) {
//            		url = httpRequest.getContextPath()+"/B503/PreviewFile/"+fileGuid+"?isTmp=true";
//            		model.addAttribute("onclick", "window.open('"+url+"', '_blank');");
//            	} else {
//            		model.addAttribute("onclick", "javascript:void(0);");
//                }
//            	break;
//            case '3':
//            	String targetContentStr = httpRequest.getParameter("c");
//            	String onclick = "$('#text-info').html('<p>"+targetContentStr+"</p>'); $('#text-block').show();\"";
//        
//            	model.addAttribute("onclick", onclick);
//            }
//            
//        }
//        else 
    	List<ADMUPLOADTMP> tmps = admStoreService.getImageTmp(id);
        String fileName = tmps.get(0).getFILENAME();
        byte[] imageContent = tmps.get(0).getFILECONTENT();
    	String base64Img=String.format("data:%s;base64, %s", getFileExt(fileName),new String(Base64.getEncoder().encode(imageContent)));

        if ( type.equalsIgnoreCase("M") && t.equalsIgnoreCase("C"))
        {
        	// model.addAttribute("imgSSrc", httpRequest.getContextPath()+"/B503/ImagePreview/"+id+"?Type=M&isTmp=true");
        	ADMSTORETMP po = new ADMSTORETMP();
        	po.setIMGBASE64(base64Img);
            model.addAttribute("Data", po);
        	return "B503/pagePreview";
        }
//        model.addAttribute("imgNews", "");
//        model.addAttribute("id",id+"-"+type);
//        model.addAttribute("showRotate", true);
        ADMSTORETMP po = admStoreService.findById(stid);
        po.setIMGBASE64(base64Img);
        model.addAttribute("Data", po);
        return "B503/pagePreviewNew";
    }
    

    /**
     * 案件的首頁 Preview
     *
     * @param id        ADMADSTMP.ID
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @param httpRequest a {@link javax.servlet.http.HttpServletRequest} object.
     * @param httpResponse a {@link javax.servlet.http.HttpServletResponse} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"/CasePreview/{id}"})
    @Authorize()
    public String casePreview(@PathVariable String id, ModelMap model, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        id = (String)Sanitizer.logForgingStr(id);
        ADMSTORETMP po = admStoreService.findById(id);

        model.addAttribute("imgLSrc", httpRequest.getContextPath()+"/img/preview/banner-lg-00.png");
        model.addAttribute("imgMSrc", httpRequest.getContextPath()+"/img/preview/banner-md-00.png");
        model.addAttribute("imgSSrc", httpRequest.getContextPath()+"/img/preview/banner-sm-00.png");
        model.addAttribute("imgCSrc", httpRequest.getContextPath()+"/B503/ImagePreview/"+id+"?Size=M&isTmp=false");
        model.addAttribute("imgURLB", "#");
        
        model.addAttribute("id",id+"-"+"M");
        model.addAttribute("showRotate", false);
        model.addAttribute("SDateTime",
                DateUtils.getDateTimeString(po.getSDATE(), "/", po.getSTIME(), ":", false));
        model.addAttribute("EDateTime",
                DateUtils.getDateTimeString(po.getEDATE(), "/", po.getETIME(), ":", false));
        if(StrUtils.isNotEmpty(po.getSTADDATE()))
        	po.setSTADDATE(DateUtils.getDateString(po.getSTADDATE(), "/"));
        if(po.getSTPICDATA().length > 0)
        {
	    	String base64Img=String.format("data:%s;base64, %s", getFileExt(po.getSTPICADD()),new String(Base64.getEncoder().encode(po.getSTPICDATA())));
	    	log.info("IMGBASE64 = {}",base64Img);
	    	po.setIMGBASE64(base64Img);
        }
        else
        {
        	po.setIMGBASE64("");
        }
        model.addAttribute("Data", po);
        return "B503/pagePreviewNew";
    }

    /**
     * 同意
     *
     * @param id                案號
     * @param httpRequest a {@link javax.servlet.http.HttpServletRequest} object.
     * @param httpResponse a {@link javax.servlet.http.HttpServletResponse} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value={"/Approve/{id}"})
    @ResponseBody
    @Authorize(userInRoleCanApprove = true)
    public JsonResponse approve(@PathVariable String id, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        id = (String)Sanitizer.logForgingStr(id);
        JsonResponse response = new JsonResponse();
        try {
            String stepId = httpRequest.getParameter("StepId");
            String comments = httpRequest.getParameter("Comments");

            Map<String, String> svcResult = admStoreService.approveOrRejectAdsTmp((String)Sanitizer.escapeHTML(id), stepId, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName(), comments, true);
            Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
            if ( entry.getKey().equalsIgnoreCase("0") ) {
                response.setValidated(true);
                response.setPkey(entry.getValue());
            } else {
                throw new Exception(entry.getKey());
            }
        } catch (Exception e) {
            log.error("send", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());

            response.setValidated(false);
            response.setErrorMessages(errors);
        }
        JsonResponse sresponse = new JsonResponse();
        Sanitizer.escape4Class(response, sresponse);
        return sresponse;
    } 

    /**
     * 退回
     *
     * @param id                案號
     * @param httpRequest a {@link javax.servlet.http.HttpServletRequest} object.
     * @param httpResponse a {@link javax.servlet.http.HttpServletResponse} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value={"/Reject/{id}"})
    @ResponseBody
    @Authorize(userInRoleCanApprove = true)
    public JsonResponse reject(@PathVariable String id, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        id = (String)Sanitizer.logForgingStr(id);
        JsonResponse response = new JsonResponse();
        try {
            String stepId = httpRequest.getParameter("StepId");
            String comments = httpRequest.getParameter("Comments");

            Map<String, String> svcResult = admStoreService.approveOrRejectAdsTmp((String)Sanitizer.escapeHTML(id), stepId, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName(), comments, false);
            Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
            if ( entry.getKey().equalsIgnoreCase("0") ) {
                response.setValidated(true);
                response.setPkey(entry.getValue());
            } else {
                throw new Exception(entry.getKey());
            }
        } catch (Exception e) {
            log.error("send", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());

            response.setValidated(false);
            response.setErrorMessages(errors);
        }
        JsonResponse sresponse = new JsonResponse();
        Sanitizer.escape4Class(response, sresponse);
        return sresponse;
    }

    /**
     * 經辦重新送出
     *
     * @param adsTmpModel   廣告流程主檔 POJO
     * @param result        Binding 結果
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/ReSend", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse resend(@ModelAttribute @Valid StoreTmpModel storeTmpModel, BindingResult result){
        JsonResponse response = new JsonResponse();
        //Reflected XSS All Clients\路徑 5:NB3Admin_20201207.pdf         
        JsonResponse sresponse = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                Map<String, String> svcResult = admStoreService.resendStoreTmp(storeTmpModel, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName());
                Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
                if ( entry.getKey().equalsIgnoreCase("0") ) {
                    response.setValidated(true);
                    response.setPkey(entry.getValue());
                } else {
                    throw new Exception(entry.getKey());
                }
                //Reflected XSS All Clients\路徑 5:NB3Admin_20201207.pdf            
                Sanitizer.escape4Class(response, sresponse);
            }
        } catch (Exception e) {
            log.error("resend", e);
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 34:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return sresponse;
    } 

    /**
     * 經辦取消案件
     *
     * @param admAdsTmp a {@link fstop.orm.po.ADMADSTMP} object.
     * @param result a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/Cancel", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse cancel(@ModelAttribute @Valid ADMSTORETMP admStoreTmp, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                Map<String, String> svcResult = admStoreService.cancelStoreTmp((String)Sanitizer.escapeHTML(Sanitizer.logForgingStr(admStoreTmp.getSTID())), 
                    portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName());
                Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
                if ( entry.getKey().equalsIgnoreCase("0") ) {
                    response.setValidated(true);
                    response.setPkey(entry.getValue());
                } else {
                    throw new Exception(entry.getKey());
                }
            }
        } catch (Exception e) {
            log.error("cancel", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());

            response.setValidated(false);
            response.setErrorMessages(errors);
        }
        JsonResponse sresponse = new JsonResponse();
        Sanitizer.escape4Class(response, sresponse);
        return sresponse;
    } 
    
//    @RequestMapping(value="UploadFile")
//    @Authorize(userInRoleCanEdit = true)
//    public @ResponseBody JsonResponse uploadFile(HttpServletRequest servletRequest, MultipartFile[] multipartFile) {
//        JsonResponse response = new JsonResponse();
//        try {
//            String id=UUID.randomUUID().toString();
//            response.setPkey(id);
//
//            //#region
//            //20191204-Danny-Unrestricted File Upload
//            //允許的類型
//            String[] allowFileType = {"application/pdf","image/jpeg","image/pjpeg","image/png","image/gif"};
//            //允許的檔案大小
//            int allowFileSize = 10*1024*1024; //2Mb
//            String contentType = multipartFile[0].getContentType();
//            String originalFilename = multipartFile[0].getOriginalFilename();
//            Map<String,Object> returnMap;
// 
//            returnMap = FileUtils.checkFile(multipartFile[0],originalFilename,contentType,allowFileType,allowFileSize,false);
//            //檢核不通過
//            if("FALSE".equals(returnMap.get("result"))){
//                String errorMessage = (String)returnMap.get("summary");
//                log.debug("errorMessage={}",Sanitizer.logForgingStr(errorMessage));
//                throw new Exception(errorMessage);
//            }            
//            //20191205-Danny-Unrestricted File Upload
//            byte[] bData =(byte[])returnMap.get("Data");
//            admStoreService.saveFileTmp(id, multipartFile[0].getOriginalFilename(), bData);
//                response.setValidated(true);
//        } catch (Exception e) {
//            log.error("saveImage Error", e);
//            Map<String, String> errors = new HashMap<String, String>();
//            //20191115-Eric-Information Exposure Through an Error Message\路徑 33:
//            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");
//
//            response.setValidated(false);
//            response.setErrorMessages(errors);
//            return response;
//        }
//        return response;
//    }  
    
//    @GetMapping(value = "/PreviewFile/{id}")
//    @Authorize(userInRoleCanQuery = true)
//    public void getFileAsByteArray(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) throws IOException {
//        id = (String)Sanitizer.logForgingStr(id);
//        boolean isTmp = Boolean.parseBoolean(request.getParameter("isTmp"));
//
//        byte[] imageContent = null;
//        String fileName = "";
//        log.debug("request={}",Sanitizer.logForgingStr(request.getParameter("isTmp")));
//        log.debug("isTmp={}",isTmp);
//        if ( isTmp ) {
//            // 從上傳暫存檔取出檔名及內容，以利呼叫端顯示圖片
//            List<ADMUPLOADTMP> tmps = admStoreService.getImageTmp(id);
//            fileName = tmps.get(0).getFILENAME();
//            imageContent = tmps.get(0).getFILECONTENT();
//        } else {
//            // 從暫存檔取出檔名及內容，以利呼叫端顯示圖片
//            ADMSTORETMP po = admStoreService.findById(id);
//            fileName = po.getSTPICADD();
//            imageContent = po.getSTPICDATA();
//        } 
//        String fileExt="";
//        int lastIndexOf = fileName.lastIndexOf(".");
//        if (lastIndexOf != -1) {
//            fileExt = fileName.substring(lastIndexOf+1);
//        }
//        if ( fileExt.compareToIgnoreCase("jpg") == 0 || fileExt.compareToIgnoreCase("jpeg") == 0 ) {
//            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
//        } else if ( fileExt.compareToIgnoreCase("png") == 0 ) {
//            response.setContentType(MediaType.IMAGE_PNG_VALUE);
//        } else if ( fileExt.compareToIgnoreCase("gif") == 0 ) {
//            response.setContentType(MediaType.IMAGE_GIF_VALUE);
//        } else if ( fileExt.compareToIgnoreCase("pdf") == 0 ) {
//            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
//        }
//        InputStream in = new ByteArrayInputStream(imageContent);
//        IOUtils.copy(in, response.getOutputStream());
//    }
    
	private String getFileExt(String fileName)
	{
        String fileExt="";
        int lastIndexOf = fileName.lastIndexOf(".");
        if (lastIndexOf != -1) {
            fileExt = fileName.substring(lastIndexOf+1);
        }
        
        if ( fileExt.compareToIgnoreCase("jpg") == 0 || fileExt.compareToIgnoreCase("jpeg") == 0 ) {
            return MediaType.IMAGE_JPEG_VALUE;
        } else if ( fileExt.compareToIgnoreCase("png") == 0 ) {
            return MediaType.IMAGE_PNG_VALUE;
        } else if ( fileExt.compareToIgnoreCase("gif") == 0 ) {
            return MediaType.IMAGE_GIF_VALUE;
        } else {
            return MediaType.IMAGE_JPEG_VALUE;
        }
	}
	
}
