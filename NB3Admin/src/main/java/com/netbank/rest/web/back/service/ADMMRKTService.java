package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.web.back.model.FlowChkResult;
import com.netbank.rest.web.back.model.FlowSvcResult;
import com.netbank.rest.web.back.model.MRKTTmpModel;
import com.netbank.util.StrUtils;

import fstop.orm.dao.AdmMRKTDao;
import fstop.orm.dao.AdmMRKTTmpDao;
import fstop.orm.dao.AdmUploadTmpDao;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.po.ADMMRKT;
import fstop.orm.po.ADMMRKTTMP;
import fstop.orm.po.ADMUPLOADTMP;
import fstop.util.JSONUtils;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 廣告管理服務
 *
 * @author 簡哥
 * @version V1.0
 */
@Slf4j
@Service
public class ADMMRKTService extends BaseService {
    @Autowired
    private FlowService flowService;

    @Autowired
    private AdmMRKTDao admMRKTDao;
    
    @Autowired
    private AdmMRKTTmpDao admMRKTTmpDao;

    @Autowired
    private AdmUploadTmpDao admUploadTmpDao;

    @Autowired
    private SysDailySeqDao sysDailySeqDao;

    // 要設定在 ADMFLOWDEF 資料表中
    private final String FLOW_ID="AdsFlow";

    // 取號的 APP ID
    private final String APP_ID = "MRKT";

    // 編輯者 STEPID，參考 ADMFLOWDEFSTEP
    private final String[] EDITOR_STEPS = {"3"};     

    // 審核者/放行者 STEPID，參考 ADMFLOWDEFSTEP
    private final String[] REVIEWER_STEPS = {"2"};

    // 編輯者角色
    /** Constant <code>EDITOR_ROLES</code> */
    public static final String[] EDITOR_ROLES = {"EB"};;

    // 審核者角色
    /** Constant <code>REVIEWER_ROLES</code> */
    public static final String[] REVIEWER_ROLES = {"EC"};

    /**
     * 取得廣告管理資料
     *
     * @param oid a {@link java.lang.String} object.
     * @param dateFrom a {@link java.lang.String} object.
     * @param dateTo a {@link java.lang.String} object.
     * @param roles Roles 
     * @return a {@link java.util.List} object.
     */
    public List<ADMMRKTTMP> findMRKTTmp(String oid, String dateFrom, String dateTo,Set<String> roles, String flowFinished, String ADTYPE, String TITLE) {
        String tmp = dateFrom.replace("/", "").replace(" ","").replace(":", "");
        String fromDate = tmp.length()>=8 ? tmp.substring(0,8) : "";

        tmp = dateTo.replace("/", "").replace(" ","").replace(":", "");
        String toDate = tmp.length()>=8 ? tmp.substring(0,8) : "";
        
        log4Query("0");

        return admMRKTTmpDao.findByParams(oid, fromDate, toDate, roles, flowFinished, ADTYPE, TITLE);
    }

    /**
     * 依廣告 Id 查詢廣告資料
     *
     * @param id a {@link java.lang.String} object.
     * @return a {@link fstop.orm.po.ADMADSTMP} object.
     */
    public ADMMRKTTMP findById(String id) {
        return admMRKTTmpDao.findById(id);
    }

    /**
     * 將上傳的 Banner 圖檔暫存在 DB TMP 檔
     *
     * @param id                暫存主鍵
     * @param type				類型(B|C)
     * @param fileName         	圖檔名
     * @param fileContent      	圖內容
     */
    public void saveImageTmp(String id, String type, String fileName, byte[] fileContent) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String createDate = format.format(Calendar.getInstance().getTime());

        ADMUPLOADTMP po = new ADMUPLOADTMP();
        po.setID(id+ "-" + type);
        po.setPURPOSE("B504");
        po.setFILENAME(fileName);
        po.setFILECONTENT(fileContent);
        po.setCREATEDATE(createDate);
        admUploadTmpDao.save(po);
    }
    
    public void updateImageTmp(String id, byte[] fileContent) {
    	ADMUPLOADTMP po = admUploadTmpDao.get(id);
    	po.setFILECONTENT(fileContent);
    	admUploadTmpDao.update(po);
    }

    /**
     * 取得圖檔
     *
     * @param id a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<ADMUPLOADTMP> getImageTmp(String id) {
        return admUploadTmpDao.findByIdLike(id);
    }

    /**
     * 經辦送件
     *
     * @param adsTmpModel   廣告 ViewModel
     * @param oid           單位代碼
     * @Param uid           使用者代碼
     * @param uName         使用者姓名
     * @return              結果
     * @param uid a {@link java.lang.String} object.
     */
    @Transactional
    public Map<String, String> sendMRKTTmp(MRKTTmpModel adsTmpModel, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            // 取案號, 案號＝ADS-xxxxxxxx
            String caseSN = APP_ID+"-"+ String.format("%06d", sysDailySeqDao.dailySeq(APP_ID));
            String todoURL = "B504/Query/"+caseSN;

            log.info("caseSN = {}", caseSN);
            //Log Forging\路徑 25、26、27:NB3Admin_20201207.pdf
            log.info("oid = {}", Sanitizer.logForgingStr(oid));
            log.info("uid = {}", Sanitizer.logForgingStr(uid));
            log.info("uName = {}", Sanitizer.logForgingStr(uName));
            //Log Forging\路徑 19:NB3Admin_20201207.pdf
            log.info("adsTmpModel = {}", Sanitizer.logForgingStr(JSONUtils.toJson(adsTmpModel)));
            FlowSvcResult rt = flowService.startFlow(FLOW_ID, caseSN, oid, uid, uName, "", "", "行動廣告管理", todoURL);
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                ADMMRKTTMP po = new ADMMRKTTMP();
                // 設主鍵
                po.setMTID(caseSN);

                // 設定其它資料
                po.setOID(oid);
                
                // 轉換日期時間 yyyy/MM/dd HH:mm
                String tmpDate = adsTmpModel.getStartDateTime().replace("/", "").replace(":", "");
                String[] parts = tmpDate.split(" ");
                po.setMTSDATE(parts[0]);
                po.setMTSTIME(StringUtils.rightPad(parts[1], 6));

                tmpDate = adsTmpModel.getEndDateTime().replace("/", "").replace(":", "");
                parts = tmpDate.split(" ");	
                po.setMTEDATE(parts[0]);
                po.setMTETIME(StringUtils.rightPad(parts[1], 6));

                po.setMTADTYPE(adsTmpModel.getMTADTYPE());
                po.setMTWEIGHT(adsTmpModel.getMTWEIGHT());
                po.setMTADHLK(adsTmpModel.getMTADHLK());
                po.setMTADCON(adsTmpModel.getMTADCON());
                po.setMTPICTYPE(adsTmpModel.getMTPICTYPE());
                po.setMTRUNSHOW(adsTmpModel.getMTRUNSHOW());
                po.setMTBULLETIN(adsTmpModel.getMTBULLETIN());
                
                List<ADMUPLOADTMP> imgTmps = getImageTmp(adsTmpModel.getMsgGuid());
                for (ADMUPLOADTMP imgTmp : imgTmps) {
                	po.setMTPICADD(imgTmp.getFILENAME());
                	po.setMTPICDATA(imgTmp.getFILECONTENT());
                }

                po.setMTBTNNAME(adsTmpModel.getMTBTNNAME());
                po.setMTADDRESS(StrUtils.isNotEmpty(adsTmpModel.getMTADDRESS()) ? adsTmpModel.getMTADDRESS() : "");
                
                List<ADMUPLOADTMP> marqueePicTmps = getImageTmp(adsTmpModel.getMarqueePicGuid());
                for (ADMUPLOADTMP imgTmp : marqueePicTmps) {
                	po.setMTMARQUEEPICADD(imgTmp.getFILENAME());
                	po.setMTMARQUEEPIC(imgTmp.getFILECONTENT());
                }
                
                po.setMTNF(adsTmpModel.getMTNF());
                po.setMTCF(adsTmpModel.getMTCF());
                
                po.setMTADSTAT("M");
                
                // 設流程資訊
                po.setSTEPID(rt.getNextStepId());
                po.setSTEPNAME(rt.getNextStepName());
                po.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");

                // 轉換日期時間
                DateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss");
                parts = format.format(new Date()).split("-");
                po.setLASTUSER(uid);
                po.setLASTDATE(parts[0]);
                po.setLASTTIME(parts[1]);
                
                for ( ADMUPLOADTMP imgTmp : imgTmps ) {
                    admUploadTmpDao.delete(admUploadTmpDao.findById(imgTmp.getID()));
                }
                
                for ( ADMUPLOADTMP imgTmp : marqueePicTmps ) {
                    admUploadTmpDao.delete(admUploadTmpDao.findById(imgTmp.getID()));
                }
                
                admMRKTTmpDao.save(po);
                result.put("0", caseSN);
            } else {
                result.put(rt.getStatusCode(), "");
            }
            log4Send(caseSN, rt.getStatusCode());
        } catch (Exception e) {
            log.error("sendMRKTTmp Error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "存檔發生錯誤，請確認資料是否有更新成功!!";
            result.put((String)Sanitizer.logForgingStr(message), "");
        }
        return result;
    }

    /**
     * 同意或是退回，流程往下一節點移動
     *
     * @param caseSN    案件編號
     * @param oid       單位代碼
     * @param uid       使用者 ID
     * @param uName     使用者姓名
     * @return          結果
     * @param stepId a {@link java.lang.String} object.
     * @param comments a {@link java.lang.String} object.
     * @param isApproved a boolean.
     */
    @Transactional
    public Map<String, String> approveOrRejectMRKTTmp(String caseSN, String stepId, String oid, String uid, String uName, String comments, boolean isApproved) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            // 取案號, 案號＝ADS-xxxxxxxx
            FlowSvcResult rt = flowService.moveFlow(caseSN, stepId, oid, uid, uName, comments, "", isApproved);
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                ADMMRKTTMP admAdsTmp = admMRKTTmpDao.findById(caseSN);
                
                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
                Date date = new Date();
                String[] parts = dateFormat.format(date).split("-");

                if ( rt.isIsFinal() ) {
                	ADMMRKT admMRKT = admMRKTDao.findById(caseSN);
                    boolean admMRKTExist = true;
                    if ( admMRKT == null ) {
                    	admMRKT = new ADMMRKT();
                        admMRKTExist = false;
                    }
                    admMRKT.setMTID(admAdsTmp.getMTID());
                    admMRKT.setOID(admAdsTmp.getOID());
                    admMRKT.setMTSDATE(admAdsTmp.getMTSDATE());
                    admMRKT.setMTSTIME(admAdsTmp.getMTSTIME());
                    admMRKT.setMTEDATE(admAdsTmp.getMTEDATE());
                    admMRKT.setMTETIME(admAdsTmp.getMTETIME());
                    admMRKT.setMTADTYPE(admAdsTmp.getMTADTYPE());
                    admMRKT.setMTWEIGHT(admAdsTmp.getMTWEIGHT());
                    admMRKT.setMTADHLK(admAdsTmp.getMTADHLK());
                    admMRKT.setMTADCON(admAdsTmp.getMTADCON());
                    admMRKT.setMTPICTYPE(admAdsTmp.getMTPICTYPE());
                    admMRKT.setMTRUNSHOW(admAdsTmp.getMTRUNSHOW());
                    admMRKT.setMTBULLETIN(admAdsTmp.getMTBULLETIN());
                    admMRKT.setMTPICADD(admAdsTmp.getMTPICADD());
                    admMRKT.setMTPICDATA(admAdsTmp.getMTPICDATA());
                    admMRKT.setMTBTNNAME(admAdsTmp.getMTBTNNAME());
                    admMRKT.setMTADDRESS(admAdsTmp.getMTADDRESS());
                    admMRKT.setMTMARQUEEPICADD(admAdsTmp.getMTMARQUEEPICADD());
                    admMRKT.setMTMARQUEEPIC(admAdsTmp.getMTMARQUEEPIC());
                    admMRKT.setMTNF(admAdsTmp.getMTNF());
                    admMRKT.setMTCF(admAdsTmp.getMTCF());
                    


                    admMRKT.setLASTUSER(uid);
                    admMRKT.setLASTDATE(parts[0]);
                    admMRKT.setLASTTIME(parts[1]);

                    
                    admMRKTDao.save(admMRKT);

                }
                if(isApproved == true)
                	admAdsTmp.setMTADSTAT(rt.isIsFinal() ? "C" : "M");
                else
                	admAdsTmp.setMTADSTAT("R");
                admAdsTmp.setSTEPID(rt.isIsFinal() ? "" : rt.getNextStepId());
                admAdsTmp.setSTEPNAME(rt.isIsFinal() ? "" : rt.getNextStepName());
                admAdsTmp.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");

                admAdsTmp.setLASTUSER(uid);
                admAdsTmp.setLASTDATE(parts[0]);
                admAdsTmp.setLASTTIME(parts[1]);
                
                admMRKTTmpDao.save(admAdsTmp);
                result.put("0", caseSN);
            } else {
                result.put(rt.getStatusCode(), "");
            }
            if ( isApproved ) {
            	log4Approve(caseSN, rt.getStatusCode());
            } else {
            	log4Reject(caseSN, rt.getStatusCode());
            }
        } catch (Exception e) {
            log.error("approveOrRejectAdsTmp Error", e);
            result.put(e.getMessage(), "");
        }
        return result;
    }

    /**
     * 經辦重送
     *
     * @param adsTmpModel       廣告 ViewModel
     * @param oid               編輯者單位代碼
     * @param uid               編輯者代碼
     * @param uName             編輯者姓名
     * @return a {@link java.util.Map} object.
     */
    @Transactional
    public Map<String, String> resendMRKTTmp(MRKTTmpModel adsTmpModel, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            ADMMRKTTMP po = admMRKTTmpDao.findById(adsTmpModel.getMTID());
            FlowSvcResult rt = null;

            if ( po.getFLOWFINISHED().equalsIgnoreCase("Y") ) {
                // 已結案，重送
                String todoURL = "B504/Query/"+adsTmpModel.getMTID();
                rt = flowService.startFlow(FLOW_ID, adsTmpModel.getMTID(), oid, uid, uName, "", "", "行銷維護", todoURL);
            } else {
                // 流程往下執行(退回後重送)
                rt = flowService.moveFlow(adsTmpModel.getMTID(), adsTmpModel.getSTEPID(), oid, uid, uName, adsTmpModel.getComments(), "", true);
            }
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                
                // 轉換日期時間 yyyy/MM/dd HH:mm
                String tmpDate = adsTmpModel.getStartDateTime().replace("/", "").replace(":", "");
                String[] parts = tmpDate.split(" ");
                po.setMTSDATE(parts[0]);
                po.setMTSTIME(StringUtils.rightPad(parts[1], 6));

                tmpDate = adsTmpModel.getEndDateTime().replace("/", "").replace(":", "");
                parts = tmpDate.split(" ");	
                po.setMTEDATE(parts[0]);
                po.setMTETIME(StringUtils.rightPad(parts[1], 6));
                
                // 編輯有重新設定行銷圖檔
                if (!adsTmpModel.getMsgGuid().isEmpty()) {

                    List<ADMUPLOADTMP> imgTmps = getImageTmp(adsTmpModel.getMsgGuid());
                    for (ADMUPLOADTMP imgTmp : imgTmps) {
                        po.setMTPICADD(imgTmp.getFILENAME());
                        po.setMTPICDATA(imgTmp.getFILECONTENT());
                    }
                    for ( ADMUPLOADTMP imgTmp : imgTmps ) {
                        admUploadTmpDao.delete(admUploadTmpDao.findById(imgTmp.getID()));
                    }
                }

                // 編輯有重新設定跑馬燈圖檔
                if (!adsTmpModel.getMarqueePicGuid().isEmpty()) {

                    List<ADMUPLOADTMP> imgTmps = getImageTmp(adsTmpModel.getMarqueePicGuid());
                    for (ADMUPLOADTMP imgTmp : imgTmps) {
                        po.setMTMARQUEEPICADD(imgTmp.getFILENAME());
                        po.setMTMARQUEEPIC(imgTmp.getFILECONTENT());
                    }
                    for ( ADMUPLOADTMP imgTmp : imgTmps ) {
                        admUploadTmpDao.delete(admUploadTmpDao.findById(imgTmp.getID()));
                    }
                }

                po.setMTADTYPE(adsTmpModel.getMTADTYPE());
                po.setMTWEIGHT(adsTmpModel.getMTWEIGHT());
                po.setMTADHLK(adsTmpModel.getMTADHLK());
                po.setMTADCON(adsTmpModel.getMTADCON());
                po.setMTPICTYPE(adsTmpModel.getMTPICTYPE());
                po.setMTRUNSHOW(adsTmpModel.getMTRUNSHOW());
                po.setMTBULLETIN(adsTmpModel.getMTBULLETIN());
                po.setMTBTNNAME(adsTmpModel.getMTBTNNAME());
                po.setMTADDRESS(adsTmpModel.getMTADDRESS());
                po.setMTNF(adsTmpModel.getMTNF());
                po.setMTCF(adsTmpModel.getMTCF());
                po.setMTADSTAT(rt.isIsFinal() ? "C" : "M");

                // 設流程資訊
                po.setSTEPID(rt.getNextStepId());
                po.setSTEPNAME(rt.getNextStepName());
                po.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");

                parts = DateUtils.format(new Date(), "yyyyMMdd-HHmmss").split("-");
                po.setLASTUSER(uid);
                po.setLASTDATE(parts[0]);
                po.setLASTTIME(parts[1]);

                
                admMRKTTmpDao.save(po);
                result.put("0", adsTmpModel.getMTID());
            } else {
                result.put(rt.getStatusCode(), "");
            }
            log4Send(adsTmpModel.getMTID(), rt.getStatusCode());
        } catch (Exception e) {
            log.error("resendAdsTmp Error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤，請再試一次!!";
            result.put((String)Sanitizer.logForgingStr(message), "");
        }
        return result;
    }

    /**
     * 經辦取消案件
     *
     * @param caseSN        案件編號
     * @param oid           單位代碼
     * @param uid           使用者代碼
     * @param uName         使用者姓名
     * @return a {@link java.util.Map} object.
     */
    @Transactional
    public Map<String, String> cancelMRKTTmp(String caseSN, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
        	ADMMRKTTMP adsTmp = admMRKTTmpDao.findById(caseSN);
        	if ( adsTmp.getFLOWFINISHED().compareToIgnoreCase("N") == 0 ) {
        		// 流程往下執行
                FlowSvcResult rt = flowService.cancelFlow(caseSN, oid, uid, uName);
                if ( rt.getStatusCode() != FlowService.SUCCESS ) {
                	throw new Exception("刪除流程失敗，StatusCode="+rt.getStatusCode());
                }
        	}
        	// 流程已經結束
        	adsTmp.setSTEPID("");
        	adsTmp.setSTEPNAME("");
        	adsTmp.setFLOWFINISHED("Y");
        	
        	adsTmp.setMTADSTAT("N");
        	
        	admMRKTTmpDao.update(adsTmp);
            
        	ADMMRKT ads = admMRKTDao.findById(caseSN);
            if ( ads != null ) {
            	admMRKTDao.delete(ads);
            }
            
            log4Delete(adsTmp.getMTID(), "0");
            result.put("0", caseSN);
        } catch (Exception e) {
            log.error("cancelAdsTmp Error", e);
            result.put(e.getMessage(), "");
        }
        return result;
    }

    /**
     * 查詢案件狀態
     *
     * @param id a {@link java.lang.String} object.
     * @param roles a {@link java.util.List} object.
     * @param oid a {@link java.lang.String} object.
     * @param uid a {@link java.lang.String} object.
     * @return a {@link com.netbank.rest.web.back.service.AuthorityEnum} object.
     */
    public AuthorityEnum queryMRKTTmp(String id, String oid, String uid, Set<String> roles) {
        FlowChkResult checkResult = flowService.IsMyCase(id, oid, uid, roles);
        if ( checkResult.getMatchStepId().isEmpty() ) {
            // 無流程
            ADMMRKTTMP po = admMRKTTmpDao.findById(id);
            if ( po.getFLOWFINISHED().equalsIgnoreCase("Y") ) {
                return AuthorityEnum.NONE;
            } else {
                return AuthorityEnum.EDIT;
            }
        } else {
            // 有流程
            if ( checkResult.isMyCase() ) {
                // 這個案例只有：
                // 編輯者 --> 覆核退回（3）
                // 覆核者 --> 已送出，待覆核（2）
                long matchEditor = Arrays.stream(EDITOR_STEPS).filter(p->p.equals(checkResult.getMatchStepId())).count();
                if ( matchEditor > 0 )
                    return AuthorityEnum.EDIT;
    
                long matchReviewer = Arrays.stream(REVIEWER_STEPS).filter(p->p.equals(checkResult.getMatchStepId())).count(); 
                if ( matchReviewer > 0 ) 
                    return AuthorityEnum.REVIEW;
            } else {
                return AuthorityEnum.QUERY;
            }
        }
        return AuthorityEnum.QUERY;
    }
    
    /**
     * 將上傳的檔案暫存在 DB TMP 檔
     *
     * @param id               		 暫存主鍵
     * @param fileName          檔名
     * @param fileContent       檔內容
     */
    public void saveFileTmp(String id, String fileName, byte[] fileContent) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String createDate = format.format(Calendar.getInstance().getTime());
        
        ADMUPLOADTMP po = new ADMUPLOADTMP();
        po.setID(id);
        po.setPURPOSE("B504");
        po.setFILENAME(fileName);
        po.setFILECONTENT(fileContent);
        po.setCREATEDATE(createDate);
        admUploadTmpDao.save(po);
    }
}
