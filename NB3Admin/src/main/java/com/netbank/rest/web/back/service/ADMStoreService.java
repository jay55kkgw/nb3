package com.netbank.rest.web.back.service;

import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import com.google.gson.Gson;
import com.netbank.rest.web.back.model.AdsTmpModel;
import com.netbank.rest.web.back.model.FlowChkResult;
import com.netbank.rest.web.back.model.FlowSvcResult;
import com.netbank.rest.web.back.model.StoreTmpModel;
import com.netbank.util.StrUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmAdsDao;
import fstop.orm.dao.AdmAdsTmpDao;
import fstop.orm.dao.AdmStoreDao;
import fstop.orm.dao.AdmStoreTmpDao;
import fstop.orm.dao.AdmUploadTmpDao;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.po.ADMADS;
import fstop.orm.po.ADMADSTMP;
import fstop.orm.po.ADMANN;
import fstop.orm.po.ADMANNTMP;
import fstop.orm.po.ADMSTORE;
import fstop.orm.po.ADMSTORETMP;
import fstop.orm.po.ADMUPLOADTMP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 廣告管理服務
 *
 * @author 
 * @version V1.0
 */
@Slf4j
@Service
public class ADMStoreService extends BaseService {
    @Autowired
    private FlowService flowService;
    
    @Autowired
    private AdmStoreTmpDao admStoreTmpDao;

    @Autowired
    private AdmStoreDao admStoreDao;

    @Autowired
    private AdmUploadTmpDao admUploadTmpDao;

    @Autowired
    private SysDailySeqDao sysDailySeqDao;

    // 要設定在 ADMFLOWDEF 資料表中
    private final String FLOW_ID="AdsFlow";

    // 取號的 APP ID
    private final String APP_ID = "STORE";

    // 編輯者 STEPID，參考 ADMFLOWDEFSTEP
    private final String[] EDITOR_STEPS = {"3"};     

    // 審核者/放行者 STEPID，參考 ADMFLOWDEFSTEP
    private final String[] REVIEWER_STEPS = {"2"};

    // 編輯者角色
    /** Constant <code>EDITOR_ROLES</code> */
    public static final String[] EDITOR_ROLES = {"EB"};;

    // 審核者角色
    /** Constant <code>REVIEWER_ROLES</code> */
    public static final String[] REVIEWER_ROLES = {"EC"};

    /**
     * 取得廣告管理資料
     *
     * @param oid a {@link java.lang.String} object.
     * @param dateFrom a {@link java.lang.String} object.
     * @param dateTo a {@link java.lang.String} object.
     * @param roles Roles 
     * @return a {@link java.util.List} object.
     */
    public List<ADMSTORETMP> findStoreTmp(String oid, String SdateFrom, String SdateTo,Set<String> roles, String flowFinished) {
//        String tmp = SdateFrom.replace("/", "").replace(" ","").replace(":", "");
        String SfromDate = SdateFrom.replace("/", "").replace(" ","").replace(":", "") + "00";

//        tmp = SdateTo.replace("/", "").replace(" ","").replace(":", "");
        String StoDate = SdateTo.replace("/", "").replace(" ","").replace(":", "") + "59";
        
        log4Query("0");

        return admStoreTmpDao.findByParams(oid, SfromDate, StoDate, roles, flowFinished);
    }

    /**
     * 依特店 Id 查詢特店資料
     *
     * @param id a {@link java.lang.String} object.
     * @return a {@link fstop.orm.po.ADMADSTMP} object.
     */
    public ADMSTORETMP findById(String id) {
        return admStoreTmpDao.findById(id);
    }

    /**
     * 將上傳的 Banner 圖檔暫存在 DB TMP 檔
     *
     * @param id                暫存主鍵
     * @param type				類型(B|C)
     * @param fileName         	圖檔名
     * @param fileContent      	圖內容
     */
    public void saveImageTmp(String id, String type, String fileName, byte[] fileContent) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String createDate = format.format(Calendar.getInstance().getTime());

        ADMUPLOADTMP po = new ADMUPLOADTMP();
        po.setID(id+ "-" + type);
        po.setPURPOSE("B503");
        po.setFILENAME(fileName);
        po.setFILECONTENT(fileContent);
        po.setCREATEDATE(createDate);
        admUploadTmpDao.save(po);
    }
    
    public void updateImageTmp(String id, byte[] fileContent) {
    	ADMUPLOADTMP po = admUploadTmpDao.get(id);
    	po.setFILECONTENT(fileContent);
    	admUploadTmpDao.update(po);
    }

    /**
     * 取得圖檔
     *
     * @param id a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<ADMUPLOADTMP> getImageTmp(String id) {
        return admUploadTmpDao.findByIdLike(id);
    }

    /**
     * 經辦送件
     *
     * @param adsTmpModel   廣告 ViewModel
     * @param oid           單位代碼
     * @Param uid           使用者代碼
     * @param uName         使用者姓名
     * @return              結果
     * @param uid a {@link java.lang.String} object.
     */
    @Transactional
    public Map<String, String> sendStoreTmp(StoreTmpModel adsTmpModel, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            // 取案號, 案號＝ADS-xxxxxxxx
            String caseSN = APP_ID+"-"+ String.format("%06d", sysDailySeqDao.dailySeq(APP_ID));
            String todoURL = "B503/Query/"+caseSN;

            FlowSvcResult rt = flowService.startFlow(FLOW_ID, caseSN, oid, uid, uName, "", "", "特店優惠管理", todoURL);
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                ADMSTORETMP po = new ADMSTORETMP();
                // 設主鍵
                po.setSTID(caseSN);

                // 設定其它資料
                po.setOID(oid);
                
                // 轉換日期時間 yyyy/MM/dd HH:mm
                String tmpDate = adsTmpModel.getSDateTime().replace("/", "").replace(":", "");
                String[] parts = tmpDate.split(" ");
                po.setSDATE(parts[0]);
                po.setSTIME(StringUtils.rightPad(parts[1], 6, '0'));

                tmpDate = adsTmpModel.getEDateTime().replace("/", "").replace(":", "");
                parts = tmpDate.split(" ");	
                po.setEDATE(parts[0]);
                po.setETIME(StringUtils.rightPad(parts[1], 6, '0'));

           
                po.setADTYPE(adsTmpModel.getADTYPE());
                po.setSTTYPE(adsTmpModel.getSTTYPE());
                po.setSTWEIGHT(adsTmpModel.getSTWEIGHT());


                // 設圖檔, 若有多個圖檔，Id 依序會被設定成 Id-L，Id-M，Id-S，小廣告Id-C
                List<ADMUPLOADTMP> imgTmps = getImageTmp(adsTmpModel.getImgGuid());
                for (ADMUPLOADTMP imgTmp : imgTmps) {
                	po.setSTPICADD(imgTmp.getFILENAME());
                	po.setSTPICDATA(imgTmp.getFILECONTENT());
                }
                //Log Forging\路徑 17:NB3Admin_20201207.pdf
                log.info("STTEL = {}", Sanitizer.logForgingStr(adsTmpModel.getSTTEL()));
                po.setSTTEL(adsTmpModel.getSTTEL());
                //Log Forging\路徑 16:NB3Admin_20201207.pdf
                log.info("STADDATE = {}", Sanitizer.logForgingStr(adsTmpModel.getSTADDATE()));
                po.setSTADDATE(adsTmpModel.getSTADDATE().replace("/", ""));
                //Log Forging\路徑 15:NB3Admin_20201207.pdf
                log.info("STADSHOW = {}", Sanitizer.logForgingStr(adsTmpModel.getSTADSHOW()));
                po.setSTADSHOW(adsTmpModel.getSTADSHOW());
                //Log Forging\路徑 14:NB3Admin_20201207.pdf
                log.info("STADNOTE = {}", Sanitizer.logForgingStr((adsTmpModel.getSTADNOTE())));
                po.setSTADNOTE(adsTmpModel.getSTADNOTE());
                //Log Forging\路徑 13:NB3Admin_20201207.pdf
                log.info("STADINFO = {}", Sanitizer.logForgingStr(adsTmpModel.getSTADINFO()));
                po.setSTADINFO(adsTmpModel.getSTADINFO());
                //Log Forging\路徑 12:NB3Admin_20201207.pdf
                log.info("STDADCONT = {}", Sanitizer.logForgingStr(adsTmpModel.getSTDADCONT()));
                po.setSTDADCONT(adsTmpModel.getSTDADCONT());
                //Log Forging\路徑 11:NB3Admin_20201207.pdf
                log.info("STDADHLK = {}", Sanitizer.logForgingStr(adsTmpModel.getSTDADHLK()));
                po.setSTDADHLK(adsTmpModel.getSTDADHLK());
                po.setSTADSTAT("M");
                
                // 設流程資訊
                po.setSTEPID(rt.getNextStepId());
                po.setSTEPNAME(rt.getNextStepName());
                po.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");

                // 轉換日期時間
                DateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss");
                parts = format.format(new Date()).split("-");
                po.setLASTUSER(uid);
                po.setLASTDATE(parts[0]);
                po.setLASTTIME(parts[1]);
                
                for ( ADMUPLOADTMP imgTmp : imgTmps ) {
                    admUploadTmpDao.delete(admUploadTmpDao.findById(imgTmp.getID()));
                }
                
               
                admStoreTmpDao.save(po);
                result.put("0", caseSN);
            } else {
                result.put(rt.getStatusCode(), "");
            }
            log4Send(caseSN, rt.getStatusCode());
        } catch (Exception e) {
            log.error("sendAdsTmp Error", e);
            String message = "存檔發生錯誤，請確認資料是否有更新成功!!";
            result.put((String)Sanitizer.logForgingStr(message), "");
        }
        return result;
    }

    /**
     * 同意或是退回，流程往下一節點移動
     *
     * @param caseSN    案件編號
     * @param oid       單位代碼
     * @param uid       使用者 ID
     * @param uName     使用者姓名
     * @return          結果
     * @param stepId a {@link java.lang.String} object.
     * @param comments a {@link java.lang.String} object.
     * @param isApproved a boolean.
     */
    @Transactional
    public Map<String, String> approveOrRejectAdsTmp(String caseSN, String stepId, String oid, String uid, String uName, String comments, boolean isApproved) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            // 取案號, 案號＝ADS-xxxxxxxx
            FlowSvcResult rt = flowService.moveFlow(caseSN, stepId, oid, uid, uName, comments, "", isApproved);
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                ADMSTORETMP admStoreTmp = admStoreTmpDao.findById(caseSN);
                if ( rt.isIsFinal() ) {
                    ADMSTORE admStore = admStoreDao.findById(caseSN);
                    boolean admAdsExist = true;
                    if ( admStore == null ) {
                    	admStore = new ADMSTORE();
                        admAdsExist = false;
                    }
                    admStore.setSTID(admStoreTmp.getSTID());
                    admStore.setOID(admStoreTmp.getOID());
                    admStore.setSDATE(admStoreTmp.getSDATE());
                    admStore.setSTIME(admStoreTmp.getSTIME());
                    admStore.setEDATE(admStoreTmp.getEDATE());
                    admStore.setETIME(admStoreTmp.getETIME());
                    admStore.setADTYPE(admStoreTmp.getADTYPE());
                    admStore.setSTTYPE(admStoreTmp.getSTTYPE());
                    admStore.setSTWEIGHT(admStoreTmp.getSTWEIGHT());
                    admStore.setSTPICADD(admStoreTmp.getSTPICADD());
                    admStore.setSTPICDATA(admStoreTmp.getSTPICDATA());
                    admStore.setSTTEL(admStoreTmp.getSTTEL());
                    admStore.setSTADDATE(admStoreTmp.getSTADDATE());
                    admStore.setSTADSHOW(admStoreTmp.getSTADSHOW());
                    admStore.setSTADNOTE(admStoreTmp.getSTADNOTE());
                    admStore.setSTADINFO(admStoreTmp.getSTADINFO());
                    admStore.setSTDADCONT(admStoreTmp.getSTDADCONT());
                    admStore.setSTDADHLK(admStoreTmp.getSTDADHLK());
                    admStore.setLASTUSER(uid);

                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
                    Date date = new Date();
                    String[] parts = dateFormat.format(date).split("-");

                    admStore.setLASTDATE(parts[0]);
                    admStore.setLASTTIME(parts[1]);
                    
//                    if ( admAdsExist ) {
//                        admAdsDao.update(admAds);
//                    } else {
                        admStoreDao.save(admStore);
//                    }
                    //不要 LOG，因為圖檔太大了
                    //log4Create(admAds, "0");
                }
                admStoreTmp.setSTEPID(rt.isIsFinal() ? "" : rt.getNextStepId());
                admStoreTmp.setSTEPNAME(rt.isIsFinal() ? "" : rt.getNextStepName());
                admStoreTmp.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");
                
                admStoreTmp.setSTADSTAT(rt.isIsFinal() ? "C" : "M");
                
                admStoreTmpDao.save(admStoreTmp);
                result.put("0", caseSN);
            } else {
                result.put(rt.getStatusCode(), "");
            }
            if ( isApproved ) {
            	log4Approve(caseSN, rt.getStatusCode());
            } else {
            	log4Reject(caseSN, rt.getStatusCode());
            }
        } catch (Exception e) {
            log.error("approveOrRejectStoreTmp Error", e);
            result.put(e.getMessage(), "");
        }
        return result;
    }

    /**
     * 經辦重送
     *
     * @param adsTmpModel       廣告 ViewModel
     * @param oid               編輯者單位代碼
     * @param uid               編輯者代碼
     * @param uName             編輯者姓名
     * @return a {@link java.util.Map} object.
     */
    @Transactional
    public Map<String, String> resendStoreTmp(StoreTmpModel adsTmpModel, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            ADMSTORETMP po = admStoreTmpDao.findById(adsTmpModel.getSTID());
            FlowSvcResult rt = null;

            if ( po.getFLOWFINISHED().equalsIgnoreCase("Y") ) {
                // 已結案，重送
                String todoURL = "B503/Query/"+adsTmpModel.getSTID();
                rt = flowService.startFlow(FLOW_ID, adsTmpModel.getSTID(), oid, uid, uName, "", "", "特店優惠管理", todoURL);
            } else {
                // 流程往下執行(退回後重送)
                rt = flowService.moveFlow(adsTmpModel.getSTID(), adsTmpModel.getSTEPID(), oid, uid, uName, adsTmpModel.getComments(), "", true);
            }
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
            	String tmpDate = adsTmpModel.getSDateTime().replace("/", "").replace(":", "") + "00";
            	String[] parts = tmpDate.split(" ");
                po.setSDATE(parts[0]);
                po.setSTIME(parts[1]);
                
                tmpDate = adsTmpModel.getEDateTime().replace("/", "").replace(":", "") + "00";
            	parts = tmpDate.split(" ");
                po.setEDATE(parts[0]);
                po.setETIME(parts[1]);
                
                po.setADTYPE(adsTmpModel.getADTYPE());
                po.setSTTYPE(adsTmpModel.getSTTYPE());
                
                // 編輯有重新設定圖檔
                if (!adsTmpModel.getImgGuid().isEmpty()) {
                    // 設圖檔, 若有多個圖檔，Id 依序會被設定成 Id-L，Id-M，Id-S，小廣告 Id-F
                    List<ADMUPLOADTMP> imgTmps = getImageTmp(adsTmpModel.getImgGuid());
                    for (ADMUPLOADTMP imgTmp : imgTmps) {
                        po.setSTPICADD(imgTmp.getFILENAME());
                        po.setSTPICDATA(imgTmp.getFILECONTENT());
                    }
                    for ( ADMUPLOADTMP imgTmp : imgTmps ) {
                        admUploadTmpDao.delete(admUploadTmpDao.findById(imgTmp.getID()));
                    }
                }
                po.setSTWEIGHT(adsTmpModel.getSTWEIGHT());
                po.setSTTEL(StrUtils.isNotEmpty(adsTmpModel.getSTTEL()) ? adsTmpModel.getSTTEL() : "");
                if(StrUtils.isNotEmpty(adsTmpModel.getSTADDATE()))
                	po.setSTADDATE(adsTmpModel.getSTADDATE().replace("/", ""));
                else
                	po.setSTADDATE("");
                po.setSTADSHOW(StrUtils.isNotEmpty(adsTmpModel.getSTADSHOW()) ? adsTmpModel.getSTADSHOW() : "");
                po.setSTADNOTE(adsTmpModel.getSTADNOTE());
                
                po.setSTADINFO(adsTmpModel.getSTADINFO());
                po.setSTDADCONT(adsTmpModel.getSTDADCONT());
                po.setSTDADHLK(adsTmpModel.getSTDADHLK());

                // 設流程資訊
                po.setSTEPID(rt.getNextStepId());
                po.setSTEPNAME(rt.getNextStepName());
                po.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");
                
                po.setSTADSTAT(rt.isIsFinal() ? "C" : "M");

                parts = DateUtils.format(new Date(), "yyyyMMdd-HHmmss").split("-");
                po.setLASTUSER(uid);
                po.setLASTDATE(parts[0]);
                po.setLASTTIME(parts[1]);

                admStoreTmpDao.save(po);
                result.put("0", adsTmpModel.getSTID());
            } else {
                result.put(rt.getStatusCode(), "");
            }
            log4Send(adsTmpModel.getSTID(), rt.getStatusCode());
        } catch (Exception e) {
            log.error("resendAdsTmp Error", e);
            String message = "發生錯誤，請再試一次!!";
            result.put((String)Sanitizer.logForgingStr(message), "");
        }
        return result;
    }

    /**
     * 經辦取消案件
     *
     * @param caseSN        案件編號
     * @param oid           單位代碼
     * @param uid           使用者代碼
     * @param uName         使用者姓名
     * @return a {@link java.util.Map} object.
     */
    @Transactional
    public Map<String, String> cancelStoreTmp(String caseSN, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
        	ADMSTORETMP storeTmp = admStoreTmpDao.findById(caseSN);
        	if ( storeTmp.getFLOWFINISHED().compareToIgnoreCase("N") == 0 ) {
        		// 流程往下執行
                FlowSvcResult rt = flowService.cancelFlow(caseSN, oid, uid, uName);
                if ( rt.getStatusCode() != FlowService.SUCCESS ) {
                	throw new Exception("刪除流程失敗，StatusCode="+rt.getStatusCode());
                }
        	}
        	// 流程已經結束
        	storeTmp.setSTEPID("");
        	storeTmp.setSTEPNAME("");
        	storeTmp.setFLOWFINISHED("Y");
        	storeTmp.setSTADSTAT("N");
        	
        	admStoreTmpDao.update(storeTmp);
            
            ADMSTORE store = admStoreDao.findById(caseSN);
            if ( store != null ) {
            	admStoreDao.delete(store);
            }
            
            log4Delete(storeTmp.getSTID(), "0");
            result.put("0", caseSN);
        } catch (Exception e) {
            log.error("cancelStoreTmp Error", e);
            result.put(e.getMessage(), "");
        }
        return result;
    }

    /**
     * 查詢案件狀態
     *
     * @param id a {@link java.lang.String} object.
     * @param roles a {@link java.util.List} object.
     * @param oid a {@link java.lang.String} object.
     * @param uid a {@link java.lang.String} object.
     * @return a {@link com.netbank.rest.web.back.service.AuthorityEnum} object.
     */
    public AuthorityEnum queryStoreTmp(String id, String oid, String uid, Set<String> roles) {
        FlowChkResult checkResult = flowService.IsMyCase(id, oid, uid, roles);
        if ( checkResult.getMatchStepId().isEmpty() ) {
            // 無流程
            ADMSTORETMP po = admStoreTmpDao.findById(id);
            if ( po.getFLOWFINISHED().equalsIgnoreCase("Y") ) {
                return AuthorityEnum.NONE;
            } else {
                return AuthorityEnum.EDIT;
            }
        } else {
            // 有流程
            if ( checkResult.isMyCase() ) {
                // 這個案例只有：
                // 編輯者 --> 覆核退回（3）
                // 覆核者 --> 已送出，待覆核（2）
                long matchEditor = Arrays.stream(EDITOR_STEPS).filter(p->p.equals(checkResult.getMatchStepId())).count();
                if ( matchEditor > 0 )
                    return AuthorityEnum.EDIT;
    
                long matchReviewer = Arrays.stream(REVIEWER_STEPS).filter(p->p.equals(checkResult.getMatchStepId())).count(); 
                if ( matchReviewer > 0 ) 
                    return AuthorityEnum.REVIEW;
            } else {
                return AuthorityEnum.QUERY;
            }
        }
        return AuthorityEnum.QUERY;
    }
    
    /**
     * 將上傳的檔案暫存在 DB TMP 檔
     *
     * @param id               		 暫存主鍵
     * @param fileName          檔名
     * @param fileContent       檔內容
     */
    public void saveFileTmp(String id, String fileName, byte[] fileContent) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String createDate = format.format(Calendar.getInstance().getTime());
        
        ADMUPLOADTMP po = new ADMUPLOADTMP();
        po.setID(id);
        po.setPURPOSE("B501");
        po.setFILENAME(fileName);
        po.setFILECONTENT(fileContent);
        po.setCREATEDATE(createDate);
        admUploadTmpDao.save(po);
    }
}
