package com.netbank.rest.web.back.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import com.netbank.BackendApplication;
import com.netbank.rest.web.back.model.B203KeyValuePair;
import com.netbank.rest.web.back.model.B203TelRecord;
import com.netbank.rest.web.back.model.B203ViewModel;
import com.netbank.rest.web.back.service.ADMMSGCODEService;
import com.netbank.rest.web.back.service.BasicProfileService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.gson.internal.LinkedTreeMap;
import fstop.aop.Authorize;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
import fstop.util.Sanitizer;

/**
 * 基本資料查詢管理
 *
 * @author 
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B203")
public class B203Controller {
    @Autowired
    private BasicProfileService basicProfileService;

    @Autowired
    private ADMMSGCODEService admMSGCODEService;

    /**
     * 取得查詢頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        return "B203/index";
    }

    @PostMapping(value = "/Query/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody B203ViewModel query(@PathVariable String id) {
        B203ViewModel model = new B203ViewModel();
        List<B203KeyValuePair> basicData = new ArrayList<B203KeyValuePair>();
        List<B203KeyValuePair> certData = new ArrayList<B203KeyValuePair>();
        List<B203TelRecord> telData = new ArrayList<B203TelRecord>();
            
        Properties props = loadProperties();
        List<ADMMSGCODE> msgs = admMSGCODEService.findAll();
        Hashtable<String, String> htMsgs = new Hashtable<String, String>();
        for (ADMMSGCODE msg : msgs) {
            htMsgs.put(msg.getADMCODE(), msg.getADMSGIN());
        }
        //20191115-Danny-Reflected XSS All Clients\路徑 11:
        id =(String)Sanitizer.escapeHTML(id); 

        HashMap resultMap;
        try {
            resultMap = (HashMap)basicProfileService.getTxnUserBasicProfile(id);
            String isSucceed = (String)resultMap.get("MSGCOD");
            if (isSucceed == null || ( !"0000".equals(isSucceed) && isSucceed.trim().length() != 0 )) {
                String errMsg = htMsgs.containsKey(isSucceed) ? htMsgs.get(isSucceed) : isSucceed;
                model.setErrorMsg(errMsg);
                return model;
            }
            basicData.add(new B203KeyValuePair("身分證/營利事業統一編號", id));
            basicData.add(new B203KeyValuePair("使用者名稱", (String)resultMap.get("USERID")));
            basicData.add(new B203KeyValuePair("客戶名稱", (String)resultMap.get("NAME")));
            basicData.add(new B203KeyValuePair("使用者名稱可錯誤次數", (String)resultMap.get("UIDCNT")));
            basicData.add(new B203KeyValuePair("簽入密碼可錯誤次數", (String)resultMap.get("CNTNBW1")));
            basicData.add(new B203KeyValuePair("交易密碼可錯誤次數", (String)resultMap.get("CNTNBW2")));

            String type=(String)resultMap.get("TYPE");
            type = fieldTranslator(props, "NA20", "TYPE1", type.charAt(0)+"")+ 
                fieldTranslator(props, "NA20", "TYPE2", type.charAt(1)+"")+ 
                fieldTranslator(props, "NA20", "TYPE3", type.charAt(2)+"")+ 
                fieldTranslator(props, "NA20", "TYPE4", type.charAt(3)+"")+ 
                fieldTranslator(props, "NA20", "TYPE5", type.charAt(4)+"")+ 
                fieldTranslator(props, "NA20", "TYPE6", type.charAt(5)+"")+ 
                fieldTranslator(props, "NA20", "TYPE7", type.charAt(6)+"")+ 
                fieldTranslator(props, "NA20", "TYPE8", type.charAt(7)+"");

            basicData.add(new B203KeyValuePair("業務類別", type)); 
            basicData.add(new B203KeyValuePair("使用狀態", fieldTranslator(props, "NA20", "UIDSTC", (String)resultMap.get("UIDSTC"))));
            basicData.add(new B203KeyValuePair("申請日期", (String)resultMap.get("DATAPL")));
            basicData.add(new B203KeyValuePair("上次登入日期", (String)resultMap.get("LOGINDT")));
            
            String logintm = (String)resultMap.get("LOGINTM");
            logintm = logintm.equals("") ? "" : logintm.substring(0, 2) + ":" + logintm.substring(2, 4) + ":" + logintm.substring(4);
            basicData.add(new B203KeyValuePair("上次登入時間", logintm));

            basicData.add(new B203KeyValuePair("原網路服務申請行", (String)resultMap.get("APLBRH")));
            basicData.add(new B203KeyValuePair("網路服務鍵機行", (String)resultMap.get("BRHTMP")));

            String seccod = (String)resultMap.get("SECCOD");
            if (seccod.trim().length() == 8) {
                seccod = fieldTranslator(props, "NA20", "SECCOD1", seccod.charAt(0)+"")+ 
                    fieldTranslator(props, "NA20", "SECCOD2", seccod.charAt(1)+"")+
                    fieldTranslator(props, "NA20", "SECCOD3", seccod.charAt(2)+"")+
                    fieldTranslator(props, "NA20", "SECCOD4", seccod.charAt(3)+"")+
                    fieldTranslator(props, "NA20", "SECCOD5", seccod.charAt(4)+"")+
                    fieldTranslator(props, "NA20", "SECCOD6", seccod.charAt(5)+"")+
                    fieldTranslator(props, "NA20", "SECCOD7", seccod.charAt(6)+"")+
                    fieldTranslator(props, "NA20", "SECCOD8", seccod.charAt(7)+"");
                
                basicData.add(new B203KeyValuePair("交易機制", seccod));
            } else {
                basicData.add(new B203KeyValuePair("交易機制", "[交易密碼(SSL)][晶片金融卡]"+seccod));
            }
    
            basicData.add(new B203KeyValuePair("行動銀行使用狀態", fieldTranslator(props, "NA20", "MBSTAT", (String)resultMap.get("MBSTAT"))));
            basicData.add(new B203KeyValuePair("行動銀行申請日期", (String)resultMap.get("MBOPNDT")));
            basicData.add(new B203KeyValuePair("行動銀行上次登入日期", (String)resultMap.get("MBLINDT")));

            String mblogintm = (String)resultMap.get("MBLINTM");
            mblogintm = mblogintm.equals("") ? "" : mblogintm.substring(0, 2) + ":" + mblogintm.substring(2, 4) + ":" + mblogintm.substring(4);
            basicData.add(new B203KeyValuePair("行動銀行上次登入時間", mblogintm));

            basicData.add(new B203KeyValuePair("行動銀行申請日期", (String)resultMap.get("MBOPNDT")));
            basicData.add(new B203KeyValuePair("行動銀行申請日期", (String)resultMap.get("MBOPNDT")));

            certData.add(new B203KeyValuePair("憑證CN", (String)resultMap.get("XMLCN")));
            certData.add(new B203KeyValuePair("載具序號", (String)resultMap.get("READ_NO")));
            certData.add(new B203KeyValuePair("申請日期", (String)resultMap.get("XDATAPL")));
            certData.add(new B203KeyValuePair("狀態碼", fieldTranslator(props, "NA20", "XSTSCOD", (String)resultMap.get("XSTSCOD"))));
            certData.add(new B203KeyValuePair("憑證有效起日", (String)resultMap.get("XVLSDT")));
            certData.add(new B203KeyValuePair("憑證有效迄日", (String)resultMap.get("XVLEDT")));
            
            List<LinkedTreeMap> recs = (List<LinkedTreeMap>)resultMap.get("REC");
            for ( int i=0; i<recs.size(); i++ ) {
                LinkedTreeMap rec = (LinkedTreeMap)recs.get(i);
                B203TelRecord tel = new B203TelRecord();

                String bankCOD = (String)rec.get("BANKCOD");
                if ( bankCOD.isEmpty() )
                    continue;

                tel.setBANKCOD(bankCOD);
                tel.setTSFACN((String)rec.get("TSFACN"));
                tel.setCUSIDN((String)rec.get("CUSIDN"));

                String busType = (String)rec.get("BUSTYPE");
                busType = fieldTranslator(props, "NA20", "BUSTYPE1", busType.charAt(0)+"")+
                    fieldTranslator(props, "NA20", "BUSTYPE2", busType.charAt(1)+"")+
                    fieldTranslator(props, "NA20", "BUSTYPE3", busType.charAt(2)+"")+
                    fieldTranslator(props, "NA20", "BUSTYPE4", busType.charAt(3)+"")+
                    fieldTranslator(props, "NA20", "BUSTYPE5", busType.charAt(4)+"")+
                    fieldTranslator(props, "NA20", "BUSTYPE6", busType.charAt(5)+"")+
                    fieldTranslator(props, "NA20", "BUSTYPE7", busType.charAt(6)+"")+
                    fieldTranslator(props, "NA20", "BUSTYPE8", busType.charAt(7)+"");

                tel.setBUSTYPE(busType);
                tel.setATRCOD(fieldTranslator(props, "NA20", "ATRCOD", (String)rec.get("ATRCOD")));
                tel.setSTSCOD(fieldTranslator(props, "NA20", "STSCOD", (String)rec.get("STSCOD")));

                String rtnCOD = (String)rec.get("RTNCOD");
                rtnCOD = htMsgs.containsKey(rtnCOD) ? htMsgs.get(rtnCOD) : rtnCOD;
                tel.setRTNCOD(rtnCOD);
                tel.setUPDBRH((String)rec.get("UPDBRH"));
                tel.setCHGDAT((String)rec.get("CHGDAT"));
                tel.setCHGTIM((String)rec.get("CHGTIM"));

                telData.add(tel);
            } 

            model.setErrorMsg(""); 
        } catch (Exception e) {
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "查詢發生錯誤，請再試一次!!";
            model.setErrorMsg(message);
            log.error("Query error", e);
            return model;
        }
        model.setBasicData(basicData);
        model.setCertData(certData);
        model.setTelData(telData);
        return model;
    }
    
    /**
     * 載入欄位對應參數
     * @return
     */
    private Properties loadProperties() {
        Properties prop = new Properties();
        try (InputStream input = BackendApplication.class.getClassLoader().getResourceAsStream("fieldmapping.properties")) {

            if (input != null) {
                prop.load(input);
            }
        } catch (IOException ex) {
            //20191115-Eric-Information Exposure Through an Error Message\路徑 27:
            //ex.printStackTrace();
            log.error("loadProperties error", ex);
        }
        return prop;
    }
    
    private String fieldTranslator(Properties props, String telNo, String key, String value) {
        String pKey = telNo+"."+key+"."+value;
        if ( props.containsKey(pKey) ) {
        	//Stored XSS
        	String pp = props.getProperty(pKey);
        	pp = (String)Sanitizer.escapeHTML(pp);
            return pp;
        } else {
            return "";
        }
    }
}