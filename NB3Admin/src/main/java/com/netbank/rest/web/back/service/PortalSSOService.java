package com.netbank.rest.web.back.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import com.formosoft.sso.tbb.Group;
import com.formosoft.sso.tbb.GroupCollection;
import com.formosoft.sso.tbb.Principal;
import com.formosoft.sso.tbb.SSOMgr;
import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.exception.TopMessageException;
import fstop.util.ESAPIUtils;
import fstop.util.Sanitizer;
import fstop.orm.dao.AdmRoleAuthDao;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.po.ADMROLEAUTH;
import fstop.orm.po.SYSPARAMDATA;
import lombok.extern.slf4j.Slf4j;


/**
 * 單一登入服務
 *
 * @author 簡哥
 * @version V1.0
 */
@Slf4j
@Service
public class PortalSSOService {
    @Autowired
    private HttpSession httpSession;

    @Autowired
    private AdmRoleAuthDao admRoleAuthDao;

    @Autowired
    private SysParamDataDao sysParamDataDao;

    private final String PORTAL_OBJECT = "__SSO_PORTAL_OBJECT__";
    private final String BRANCH = "__BRANDH__";
    private final String USERID = "__USERID__";
    private final String USERNAME = "__USERNAME__";
    private final String ROLE_PERMISSIONS = "__ROLEPERMISSIONS__";
    private final String LOGIN_TIME = "__LOGINTIME__";
    
    // 20200623 eportal 如使用者已登出, 網銀不能獨活, 另外要做 Session 同步
    private final String AUTH_CODE = "__AUTH_CODE__";
    private final String SIP = "__SIP__";
    
    /**
     * 取得登入者所屬單位
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLoginBranch() {
        if ( isLogins() ) {
            return (String)httpSession.getAttribute(BRANCH);
        } else {
            return "";
        }
    }

    /**
     * 取得登入者代碼
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLoginUserId() {
        if ( isLogins() ) {
            return (String)httpSession.getAttribute(USERID);
        } else {
            return "";
        }
    }
    
    /**
     * 取得登入者姓名
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLoginUserName() {
        if ( isLogins() ) {
            return (String)httpSession.getAttribute(USERNAME);
        } else {
            return "";
        }
    }

    /**
     * 取得登入時間
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLoginTime() {
        if ( isLogins() ) {
            return (String)httpSession.getAttribute(LOGIN_TIME);
        } else {
            return "";
        }
    }
    
    /**
     * 取得 AuthCode
     * @return
     */
    public String getAuthCode() {
    	if ( isLogins() ) {
    		return (String)httpSession.getAttribute(AUTH_CODE);
    	} else {
    		return "";
    	}
    }
    
    /**
     * 取得 SSOMgr
     * @return
     */
    public SSOMgr getSSOMgr() {
    	if ( isLogins() ) {
    		return (SSOMgr)httpSession.getAttribute(PORTAL_OBJECT);
    	} else {
    		return null;
    	}
    }
    
    /**
     * 取得 Client IP
     * @return
     */
    public String getSIP() {
    	if ( isLogins() ) {
    		return (String)httpSession.getAttribute(SIP);
    	} else {
    		return "";
    	}
    }

    /**
     * 取得登入者的角色清單
     *
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings("unchecked")
    public Set<String> getLoginRoles() {
        Set<String> roles = new HashSet<String>();

        if ( isLogins() ) {
            List<ADMROLEAUTH> userRoles = (List<ADMROLEAUTH>)httpSession.getAttribute(ROLE_PERMISSIONS);
            for ( ADMROLEAUTH userRole : userRoles ) {
                roles.add(userRole.getADROLENO());
            }
        } 
        return roles;
    }

    /**
     * 假登入
     *
     * @param branch a {@link java.lang.String} object.
     * @param uId a {@link java.lang.String} object.
     * @param role a {@link java.lang.String} object.
     */
    public void fakeLogin(String branch, String uId, String uName, String role) throws Exception {
        httpSession.setAttribute(BRANCH, branch);
        httpSession.setAttribute(USERID, uId);
        httpSession.setAttribute(USERNAME, uName);

        List<ADMROLEAUTH> rolePermissions = new ArrayList<ADMROLEAUTH>();
        String groupName = role;

        List<ADMROLEAUTH> roleAuths = admRoleAuthDao.findAll();
        List<ADMROLEAUTH> myRoleAuths = roleAuths.stream().filter(r->r.getADROLENO().equalsIgnoreCase(groupName) && r.getADSTAFFNO().equalsIgnoreCase(branch)).collect(Collectors.toList());
        if ( myRoleAuths.size()==0 ) {
        	// 對此角色的權限沒有設定到部門, 所以重新查詢, 不帶部門
        	myRoleAuths = roleAuths.stream().filter(r->r.getADROLENO().equalsIgnoreCase(groupName) && r.getADSTAFFNO().isEmpty()).collect(Collectors.toList());
        }
        if ( myRoleAuths.size()>0 ) {
            for ( ADMROLEAUTH myRoleAuth : myRoleAuths ) {
                ADMROLEAUTH copyAuth = new ADMROLEAUTH();
                copyAuth.setADROLENO(groupName);
                copyAuth.setAPOPID(myRoleAuth.getAPOPID());
                copyAuth.setADSTAFFNO(myRoleAuth.getADSTAFFNO());
                copyAuth.setISQUERY(myRoleAuth.getISQUERY());
                copyAuth.setISEDIT(myRoleAuth.getISEDIT());
                copyAuth.setISEXEC(myRoleAuth.getISEXEC());

                rolePermissions.add(copyAuth);
            }
        } else {
            // 使用者所屬的角色沒有任何功能的權限，為避免首頁取代辦發生錯誤，在此加一筆角色權限
            ADMROLEAUTH copyAuth = new ADMROLEAUTH();
            copyAuth.setADROLENO(groupName);
            copyAuth.setAPOPID("");
            copyAuth.setADSTAFFNO("");
            copyAuth.setISQUERY("0");
            copyAuth.setISEDIT("0");
            copyAuth.setISEXEC("0");

            rolePermissions.add(copyAuth);
        }
        
        httpSession.setAttribute(ROLE_PERMISSIONS, rolePermissions);
        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        httpSession.setAttribute(LOGIN_TIME, sdFormat.format(new Date()));
    }

    /**
     * SSO 登入
     *
     * @param authCode a {@link java.lang.String} object.
     * @param clientIP a {@link java.lang.String} object.
     * @throws java.lang.Exception
     */
    public void login(String authCode, String clientIP) throws Exception {
        // SSOMgr ssomgr =new SSOMgr("http://10.16.21.32/TBBPortalServlet/portal");
        //PortalSetting ps = new PortalSetting();
        //Properties p = ps.loadAdmPropertyFromDB();

        SYSPARAMDATA portalSettings =  sysParamDataDao.get("NBSYS");

        String str_PORTALURL = portalSettings.getADPORTALURL_ADM();
        String apName = portalSettings.getADAPNAME();
        String apiUserName = portalSettings.getADNAME();
        String apiSoCi = portalSettings.getADPWD();         

        log.debug("str_PORTALURL={}", str_PORTALURL);
        
        SSOMgr ssomgr = new SSOMgr(str_PORTALURL);

        try {
            log.debug("ssomgr.init apiUserName={}, apiSoCi={}, apName={}", apiUserName, apiSoCi, apName);

            int rt = ssomgr.init(apiUserName, apiSoCi, apName);
            log.debug("ssomgr.init rt="+Integer.toString(rt));

            if (rt == 0) { 
                if (ssomgr.checkLogon(authCode, clientIP)) {
                    Principal principal = ssomgr.getPrincipal();
                    List<ADMROLEAUTH> roleAuths = admRoleAuthDao.findAll();

                    // 取得行員所屬的群組
                    List<ADMROLEAUTH> rolePermissions = new ArrayList<ADMROLEAUTH>();
                    GroupCollection gCol = principal.getGroups();
                    if (gCol != null) {
                        Iterator it = gCol.iterator();
                        Group grp = null;
                        while (it.hasNext()) {
                            grp = (Group) it.next();
                            String groupName = grp.getName();

                            List<ADMROLEAUTH> myRoleAuths = roleAuths.stream().filter(r->r.getADROLENO().equalsIgnoreCase(groupName) && r.getADSTAFFNO().equalsIgnoreCase(principal.getDept().getCode())).collect(Collectors.toList());
                            if ( myRoleAuths.size() == 0 ) {
                            	// 對此角色的權限沒有設定到部門, 所以重新查詢, 不帶部門
                            	myRoleAuths = roleAuths.stream().filter(r->r.getADROLENO().equalsIgnoreCase(groupName) && r.getADSTAFFNO().isEmpty()).collect(Collectors.toList());
                            }
                            if ( myRoleAuths.size()>0 ) {
                                for ( ADMROLEAUTH myRoleAuth : myRoleAuths ) {
                                    ADMROLEAUTH copyAuth = new ADMROLEAUTH();
                                    copyAuth.setADROLENO(groupName);
                                    copyAuth.setAPOPID(myRoleAuth.getAPOPID());
                                    copyAuth.setADSTAFFNO(myRoleAuth.getADSTAFFNO());
                                    copyAuth.setISQUERY(myRoleAuth.getISQUERY());
                                    copyAuth.setISEDIT(myRoleAuth.getISEDIT());
                                    copyAuth.setISEXEC(myRoleAuth.getISEXEC());
    
                                    rolePermissions.add(copyAuth);
                                }
                            } else {
                                // 使用者所屬的角色沒有任何功能的權限，為避免首頁取代辦發生錯誤，在此加一筆角色權限
                                ADMROLEAUTH copyAuth = new ADMROLEAUTH();
                                copyAuth.setADROLENO(groupName);
                                copyAuth.setAPOPID("");
                                copyAuth.setISQUERY("0");
                                copyAuth.setISEDIT("0");
                                copyAuth.setISEXEC("0");

                                rolePermissions.add(copyAuth);
                            }
                        }
                    }

                    Gson gson = new Gson();
                    log.debug("gCol={}", gson.toJson(gCol));
                    log.debug("rolePermissions={}", Sanitizer.logForgingStr(gson.toJson(rolePermissions)));

                    //將Portal API物件存於session中，往後如有需要使用，直接由session中取用，不需再做checkLogon，此步驟務必實作，才能持續原來 Portal 的 session。
                    httpSession.setAttribute(PORTAL_OBJECT, ssomgr);
                    httpSession.setAttribute(ROLE_PERMISSIONS, rolePermissions);
                    httpSession.setAttribute(BRANCH, principal.getDept().getCode());
                    httpSession.setAttribute(USERID, principal.getAccount());
                    httpSession.setAttribute(USERNAME, principal.getName());
                    //Trust Boundary Violation\路徑 2、3、4、5:NB3Admin_20201111.pdf

                    httpSession.setAttribute(AUTH_CODE, ESAPIUtils.validStr(authCode));
                    httpSession.setAttribute(SIP, ESAPIUtils.validStr(clientIP)); 
                    
                    SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    httpSession.setAttribute(LOGIN_TIME, sdFormat.format(new Date()));
                } else {
                    log.error("User authorization fail! Error code: " + ssomgr.getLastError());
                    throw TopMessageException.create("Z101"); //Portal登入異常，請稍候再試，謝謝！
                }
            } else {
                log.error("login init ssomgr fail! Error code: " + ssomgr.getLastError());
                throw TopMessageException.create("Z102"); //連線逾時，請重新登入，謝謝！
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 使用者是否已經登入
     *
     * @return a boolean.
     */
    public boolean isLogins() {
        if (httpSession.getAttribute(USERID) != null && !httpSession.getAttribute(USERID).equals("") ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判斷使用者是否在某一個角色中
     *
     * @param checkRoles       要檢核的角色
     * @return a boolean.
     */
    @SuppressWarnings("unchecked")
    public boolean isInRole(String[] checkRoles) {
        if ( isLogins() ) {
            List<ADMROLEAUTH> roles = (ArrayList<ADMROLEAUTH>)httpSession.getAttribute(ROLE_PERMISSIONS);
                
            for ( String checkRole : checkRoles ) {
                if ( roles.stream().filter(r->r.getADROLENO().equalsIgnoreCase(checkRole)).count() > 0 )
                    return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * 判斷使用者是否在某一個功能是否有特定權限
     *
     * @param apOPID            功能 ID
     * @param checkAuthority    要判斷的權限，QUERY or EDIT or APPROVE
     * @return                  true：有權限，false：無權限
     */
    @SuppressWarnings("unchecked")
    public boolean isPermissionOK(String apOPID, AuthorityEnum checkAuthority) {
        if ( isLogins() ) {
            List<ADMROLEAUTH> roleAuths = (ArrayList<ADMROLEAUTH>)httpSession.getAttribute(ROLE_PERMISSIONS);
            List<ADMROLEAUTH> matchAuths = roleAuths.stream().filter(r->r.getAPOPID().equalsIgnoreCase(apOPID)).collect(Collectors.toList());
            switch ( checkAuthority ) {
                case QUERY: 
                    return matchAuths.stream().filter(r->r.getISQUERY().equals("1")).count()>0;
                
                case EDIT:
                    return matchAuths.stream().filter(r->r.getISEDIT().equals("1")).count()>0;

                case REVIEW:
                    return matchAuths.stream().filter(r->r.getISEXEC().equals("1")).count()>0;

                default:
                    return false;
            }
        } else {
            return false;
        }
    } 

    /**
     * 判斷是否要顯示選單
     * @param apOPID    選單 ID
     * @return          true：有權限；false：無權限
     */
    @SuppressWarnings("unchecked")
    public boolean isMenuOK(String apOPID) {
        if ( isLogins() ) {
            List<ADMROLEAUTH> roleAuths = (ArrayList<ADMROLEAUTH>)httpSession.getAttribute(ROLE_PERMISSIONS);
            List<ADMROLEAUTH> matchAuths = roleAuths.stream().filter(r->r.getAPOPID().equalsIgnoreCase(apOPID)).collect(Collectors.toList());
            
            boolean menuRight = matchAuths.stream().filter(r->r.getISQUERY().equals("1") || r.getISEDIT().equals("1") || r.getISEXEC().equals("1")).count()>0;

            return menuRight;
        } else {
            return false;
        }
    }

    /**
     * <p>logout.</p>
     */
    public void logout() {
        if (httpSession.getAttribute(PORTAL_OBJECT) != null)
        {
            SSOMgr ssomgr = (SSOMgr)httpSession.getAttribute(PORTAL_OBJECT);
            ssomgr.free();
        }
        httpSession.invalidate();
    }
}
