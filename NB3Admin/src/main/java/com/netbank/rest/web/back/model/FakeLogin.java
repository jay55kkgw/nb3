package com.netbank.rest.web.back.model;

/**
 * 模擬登入 ViewModel
 *
 * @author 簡哥
 * @version V1.0
 */
public class FakeLogin {
    private String orgId;
    private String userId;
    private String roleId;

    /**
     * <p>Getter for the field <code>orgId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * <p>Setter for the field <code>orgId</code>.</p>
     *
     * @param orgId a {@link java.lang.String} object.
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * <p>Getter for the field <code>userId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUserId() {
        return userId;
    }

    /**
     * <p>Setter for the field <code>userId</code>.</p>
     *
     * @param userId a {@link java.lang.String} object.
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * <p>Getter for the field <code>roleId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     * <p>Setter for the field <code>roleId</code>.</p>
     *
     * @param roleId a {@link java.lang.String} object.
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    
}
