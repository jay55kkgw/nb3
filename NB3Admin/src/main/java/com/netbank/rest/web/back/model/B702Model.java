package com.netbank.rest.web.back.model;

/**
 * <p>B702Model class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class B702Model {
    private String YYYY;
    private String MM;
    private String loginType;

    /**
     * <p>getYYYY.</p>
     *
     * @return String return the YYYY
     */
    public String getYYYY() {
        return YYYY;
    }

    /**
     * <p>setYYYY.</p>
     *
     * @param YYYY the YYYY to set
     */
    public void setYYYY(String YYYY) {
        this.YYYY = YYYY;
    }

    /**
     * <p>getMM.</p>
     *
     * @return String return the MM
     */
    public String getMM() {
        return MM;
    }

    /**
     * <p>setMM.</p>
     *
     * @param MM the MM to set
     */
    public void setMM(String MM) {
        this.MM = MM;
    }

    /**
     * <p>Getter for the field <code>loginType</code>.</p>
     *
     * @return String return the loginType
     */
    public String getLoginType() {
        return loginType;
    }

    /**
     * <p>Setter for the field <code>loginType</code>.</p>
     *
     * @param loginType the loginType to set
     */
    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

}
