package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.netbank.rest.web.back.model.B702Model;
import com.netbank.rest.web.back.model.B702Result;
import com.netbank.rest.web.back.service.NMBReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fstop.aop.Authorize;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>B702Controller class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B702")
public class B702Controller {
    @Autowired
    private NMBReportService _NMBReportService;

    /**
     * 取得各類交易數統計表首頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {

        B702Model entity = new B702Model();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, -1);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        String todayYear = String.format("%04d", year);
        String todayMonth = String.format("%02d", month);

        entity.setYYYY(todayYear);
        entity.setMM(todayMonth);
        entity.setLoginType("");

        List<B702Result> queryData = getResultList(entity);
        model.addAttribute("Data", queryData);

        SetDropDownList(model, entity);
        model.addAttribute("IsPostBack", false);
        return "B702/index";
    }

    /**
     * 查詢資料
     *
     * @param result     SpringMVC Binding 檢核結果
     * @return Json 結果物件
     * @param entity a {@link com.netbank.rest.web.back.model.B702Model} object.
     * @param model a {@link org.springframework.ui.ModelMap} object.
     */
    @PostMapping(value = "/Query")
    @Authorize(userInRoleCanQuery = true)
    public String query(@ModelAttribute B702Model entity, BindingResult result, ModelMap model) {
        log.debug("entity={}", Sanitizer.logForgingStr(entity));
        
        // 錯誤處理
        if (result.hasErrors()) {
            // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
            Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

            model.addAttribute("Validated", false);
            model.addAttribute("ErrMsg", errors);
            return "B702/index";
        }

        List<B702Result> queryData = getResultList(entity);
        model.addAttribute("Data", queryData);
        SetDropDownList(model, entity);
        model.addAttribute("IsPostBack", true);
        return "B702/index";
    }

    /**
     * 取得 應用系統代碼維護 修改
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @param entity a {@link com.netbank.rest.web.back.model.B702Model} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException if any.
     */
    @PostMapping(value = { "/DownloadtoCSV" })
    @Authorize(userInRoleCanQuery = true)
    public void DownloadtoCSV(@ModelAttribute B702Model entity, ModelMap model, HttpServletResponse response)
            throws IOException {

        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"ExportSys.csv\"");
        OutputStream resOs = response.getOutputStream();
        resOs.write('\ufeef'); // emits 0xef
        resOs.write('\ufebb'); // emits 0xbb
        resOs.write('\ufebf'); // emits 0xbf

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = new CSVWriter(outputWriter);

        List<B702Result> queryData = getResultList(entity);

        String[] headers = { "類别", "", "", "自行", "跨行", "合計" };
        writer.writeNext(headers);
        for (B702Result item : queryData) {
            if (item.getSeqNo().equals("5")) {
                String adusertype = ""; // 0. 個人戶 1. 企業戶 -1 合計

                if (item.getAdUserType().equals("-1"))
                    adusertype = "小計";
                else if (item.getAdUserType().equals("0"))
                    adusertype = "個人戶";
                else
                    adusertype = "企業戶";

                if (item.getAdUserType().equals("-1")) {
                    String[] nextLine = { item.getAdopGroupName(), "", "交易筆數合計", item.getSelfCount(),
                            item.getCrossCount(), item.getAdCount() };
                    writer.writeNext(nextLine);

                    String[] nextLine2 = { item.getAdopGroupName(), "", "交易金額合計", item.getSelfAmt(), item.getCrossAmt(),
                            item.getAdAmount() };
                    writer.writeNext(nextLine2);
                } else {
                    String[] nextLine = { item.getAdopGroupName(), adusertype, "筆數", item.getSelfCount(),
                            item.getCrossCount(), item.getAdCount() };
                    writer.writeNext(nextLine);

                    String[] nextLine2 = { item.getAdopGroupName(), adusertype, "金額(仟)", item.getSelfAmt(),
                            item.getCrossAmt(), item.getAdAmount() };
                    writer.writeNext(nextLine2);
                }
            } else {
                String[] nextLine = { item.getAdopGroupName(), "", "筆數", item.getAdCount(), "", item.getAdCount() };
                writer.writeNext(nextLine);
            }
        }
        writer.close();
    }

    /*
     * 取回查詢資料
     */
    private List<B702Result> getResultList(B702Model entity) {

        Rows qResult = new Rows();
        try {
            qResult = _NMBReportService.queryB702(entity);
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());
        }

        List<B702Result> queryData = new ArrayList<>();
        B702Result qItem;
        int rowCount = qResult.getSize();
        for (int i = 0; i < rowCount; i++) {
            qItem = new B702Result();
            Row r = null;
            r = qResult.getRow(i);
            qItem.setSeqNo(r.getValue("SEQNO"));
            qItem.setAdopGroup(r.getValue("ADOPGROUP"));
            qItem.setAdopGroupName(r.getValue("ADOPGROUPNAME"));
            qItem.setAdUserType(r.getValue("ADUSERTYPE"));

            qItem.setSelfCount(r.getValue("SELFCOUNT"));
            qItem.setCrossCount(r.getValue("CROSSCOUNT"));
            qItem.setAdCount(r.getValue("ADCOUNT"));
            qItem.setSelfAmt(r.getValue("SELFAMT"));
            qItem.setCrossAmt(r.getValue("CROSSAMT"));
            qItem.setAdAmount(r.getValue("ADAMOUNT"));

            queryData.add(qItem);
        }
        return queryData;
    }

    private void SetDropDownList(ModelMap model, B702Model entity) {
        model.addAttribute("B702Model", entity);

        model.addAttribute("YYYY", entity.getYYYY());
        model.addAttribute("MM", entity.getMM());

        List<String> years = new ArrayList<String>();
        for (int i = 2019; i <= 2119; i++) {
            years.add(Integer.toString(i));
        }

        List<String> months = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            months.add(String.format("%02d", i));
        }

        model.addAttribute("years", years);
        model.addAttribute("months", months);

        return;
    }
}
