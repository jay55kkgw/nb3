package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netbank.rest.web.back.service.FlowService;
import com.netbank.rest.web.back.service.NotifyService;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.util.CodeUtil;

import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.util.DateTimeUtils;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * <p>CommonController class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Controller
@RequestMapping({"", "/Common"})
public class CommonController {
//    @Value("${WATER_MARK}:0xD5DBDB")
	@Value("${WATER_MARK:0xD5DBDB}")
    private String waterMark;
    
    @Autowired
    private FlowService flowService;

    @Autowired
    private PortalSSOService portalSSOService;

    @Autowired
    private NotifyService notifyService;
    
    /**
     * <p>error.</p>
     *
     * @param model a {@link java.util.Map} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value="Error")
    public String error(Map<String, Object> model) {
        return "Common/error";
    }

    /**
     * 取得一般網路銀行客戶登入數統計表首頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @param printArea a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value = { "", "/FriendlyPrint" })
    public String friendlyprint(String printArea, ModelMap model) {
        log.debug("printArea={}", Sanitizer.logForgingStr(printArea));
        
        model.addAttribute("PRINTAREA", printArea);
        return "Common/friendlyPrint";
    }

    /**
     * <p>statusDialog.</p>
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @param caseSN a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/StatusDialog/{caseSN}")
    public String statusDialog(@PathVariable String caseSN, ModelMap model) {
        model.addAttribute("caseSN", caseSN);
        model.addAttribute("histories", flowService.getFlowHistory(caseSN));

        return "Common/caseStatusPartial";
    }

    @GetMapping(value="/MailTest", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public String mailTest() {
    	List<String> toList = new ArrayList<String>();
    	toList.add("shunz327@mail.tbb.com.tw");
    	
    	notifyService.smtpMail("中文 Title", "<html><body><h1>中文測試</h1></body></html>", toList, null, null);
    	
    	return "<html><header><title>Welcome</title></header><body>寄送成功</body></html>";
    }
    
    @GetMapping(value = "/WaterMark")
    public void waterMark(HttpServletRequest request, HttpServletResponse response) throws IOException {
        boolean loadFont=false;
        //System.out.println("--------waterMark--------");
    	try {
    		File fontFile = ResourceUtils.getFile("classpath:msjh.ttf");
    		
    		//Incorrect Permission Assignment For Critical Resources
    		fontFile.setReadable(true);
    		fontFile.setWritable(false);
    		fontFile.setExecutable(false);

            Font f = Font.createFont(Font.TRUETYPE_FONT, fontFile);
            GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(f);
            
            //System.out.println("--------load msjh.ttf success.--------");
            loadFont = true;
        } catch (FontFormatException | IOException e) {
        	log.error("load font error", e);
        	//System.out.println("--------"+e.getMessage()+"--------");
        	loadFont = false;
        }
    	/*GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    	String[] fontList = ge.getAvailableFontFamilyNames();
    	for ( int i=0; i<fontList.length; i++ ) {
    		System.out.println("--------"+fontList[i]+"--------");
    	}*/
    	
        BufferedImage image = new BufferedImage(300, 200, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();
        g.setColor(Color.white);
        g.fillRect(0, 0, 300, 200);

//        Color aColor = new Color(0xD5DBDB);
//        Color aColor = new Color(0xE3E8E8);
        Color aColor = null;
        String hex="";
        log.info("waterMark = {}", waterMark);
        if(waterMark.subSequence(0, 2).equals("0x") || waterMark.subSequence(0, 2).equals("0X"))
        {
        	hex=waterMark.substring(2);
        	byte[] aa=CodeUtil.hexStringToByteArray(hex);
        	log.info(String.format("R=%02X, G=%02X, B=%02X", aa[0], aa[1], aa[2]));
        	aColor = new Color(Byte.toUnsignedInt(aa[0]),Byte.toUnsignedInt(aa[1]),Byte.toUnsignedInt(aa[2]));
        }
        else
        {
        	int color=0;
        	try
        	{
        		Integer.parseUnsignedInt(waterMark);
        	}
        	catch(Exception e)
        	{
        		log.error("String Convert to Integer Exception USING 0xD5DBDB ！！",e);
        		color = 0xD5DBDB;
        	}
        	aColor = new Color(color);
        }
        g.setColor(aColor);
        
        Font aFont;
        if ( loadFont ) {
        	aFont = new Font("Microsoft JhengHei", Font.PLAIN, 24);
        } else {
        	aFont = new Font("Monospaced", Font.PLAIN, 24);
        }
        g.setFont(aFont);

        String ipAddress = request.getHeader("X-FORWARDED-FOR");  
        if (ipAddress == null) {  
            ipAddress = request.getRemoteAddr();  
        }

        if ( portalSSOService.isLogins() ) {
            g.drawString(portalSSOService.getLoginUserId()+" "+portalSSOService.getLoginUserName(), 30, 30);
            g.drawString(ipAddress, 30, 60);
            g.drawString(DateTimeUtils.getDatetime(new Date()), 30, 90);
        } else {
            g.drawString("Unknown Users", 30, 30);
            g.drawString(ipAddress, 30, 120);
            g.drawString(DateTimeUtils.getDatetime(new Date()), 30, 90);
        }

        
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", baos);
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();

        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        
        InputStream in = new ByteArrayInputStream(imageInByte);
        IOUtils.copy(in, response.getOutputStream());
    }
}
