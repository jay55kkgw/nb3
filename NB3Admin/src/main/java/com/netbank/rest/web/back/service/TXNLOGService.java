package com.netbank.rest.web.back.service;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.controller.B201Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.TxnLogDao;
import fstop.orm.po.TXNLOG;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class TXNLOGService extends BaseService {

    @Autowired
    private TxnLogDao txnLogDao;
    
    /**
     * 分頁查詢
     * @param pageNo        頁碼
     * @param pageSize      一頁資料量
     * @param orderBy       排序欄位
     * @param orderDir      排序方向
     * @param startTime     開始日期時間
     * @param endTime       結束日期時間
     * @param adOpGroup     交易群組
     * @param adExCode      成功或失敗
     * @param loginType     登入類型
     * @param adUserId      使用者統編
     * @param adTxAcno      帳號
     * @param adOPName      交易名稱
     * @param adUserIp      使用者登入 IP
     * @param userName      使用者名稱
     * @return              分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String startTime, String endTime, 
        String adOpGroup, String adExCode, String loginType, String adUserId, String adTxAcno, String adOPName, 
        String adUserIp, String userName, String adopid) {

        startTime = startTime.replace("/", "").replace(":", "").replace(" ", "");
        endTime = endTime.replace("/", "").replace(":", "").replace(" ", "");

        String startDate = "";
        String endDate = "";

        if ( !startTime.isEmpty() ) {
            startDate = startTime.substring(0,8);
            startTime = startTime.substring(8,14);
        }

        if ( !endTime.isEmpty() ) {
            endDate = endTime.substring(0,8);
            endTime = endTime.substring(8,14);
        }
        
        Page page = null;
		try {
			page = txnLogDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, startDate, startTime, endDate, endTime, adOpGroup, adExCode, loginType, adUserId, adTxAcno, adOPName, adUserIp, userName, adopid);
		} catch (Exception e) {
			log.error("TXNLOGService getByQuery error>>>{}",e);
			page = new Page();
		}
        log4Query("0", new String[][] { { "startTime", startTime }, { "endTime", endTime }, { "adOpGroup", adOpGroup }, { "adExCode", adExCode }, { "loginType", loginType } , { "adUserId", adUserId } ,
                        { "adTxAcno", adTxAcno }, { "adOPName", adOPName }, { "adUserIp", adUserIp }, {"userName", userName} , {"adopid", adopid} });
        return page;
    }

    public List<String> getByQueryALL(String COLUMNS, String CONDITION) {
    	log4Query("0", new String[][] { { "COLUMNS", COLUMNS }, { "CONDITION", CONDITION } });
    	return txnLogDao.B201Query(COLUMNS, CONDITION);
    } 
    
    /**
     * 依主鍵查詢
     * @param id        主鍵
     * @return          TXNLOG 物件
     */
    public TXNLOG getById(String id) {
        log4Query("0", new String[][] { { "id", id }} );
        return txnLogDao.findById(id);
    }
}