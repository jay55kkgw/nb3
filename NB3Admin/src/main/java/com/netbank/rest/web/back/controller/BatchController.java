package com.netbank.rest.web.back.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/batch")
public class BatchController {
	@RequestMapping(value="/com/echo")
	public @ResponseBody Map<String, Object> echo() {
	    HashMap<String, Object> map = new HashMap<>();
	    map.put("msgCode", "0");
	    map.put("message", "ok");
	    map.put("next", null);
	    map.put("previous", null);
	    map.put("result", true);
	    map.put("data", null);
	    return map;
	}
}
