package com.netbank.rest.web.back.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ADMAPPBRANCHService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.service.VerifyParamChangeKeyService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMAPPBRANCH;
import fstop.orm.po.TXNUSER;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 變更驗證參數交易
 *
 * @author 
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/BM3000")
public class BM3000Controller {
    @Autowired
    private PortalSSOService portalSSOService;
    
    @Autowired
    private VerifyParamChangeKeyService verifyParamChangeKeyService;

    /**
     * 取得首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        ADMAPPBRANCH po = new ADMAPPBRANCH();
        try
        {
	        Map<String, String> mm = verifyParamChangeKeyService.getVerifyParam(portalSSOService.getLoginUserId());
	        model.addAttribute("ValidParam", mm.get("VALIDPARAM"));
	        model.addAttribute("ValidParamRec", mm.get("VALIDPARAMREC"));
	        if ( portalSSOService.isPermissionOK("BM3000", AuthorityEnum.QUERY)) {
	            model.addAttribute("allowEdit", true);
	        } else {
	            model.addAttribute("allowEdit", false);
	        }
	        model.addAttribute("error","");
        }
        catch(Exception e)
        {
        	model.addAttribute("error", e.getMessage());
        }
        return "BM3000/index";
    }

    /*
    @RequestMapping(value = "/Result", method = RequestMethod.POST)
    @Authorize(userInRoleCanQuery = true)
    public String submit(@ModelAttribute @Valid String ValidParam, BindingResult result, ModelMap model) 
    {
        try
        {
	        Map<String, String> mm = verifyParamChangeKeyService.changeVerifyParam(portalSSOService.getLoginUserId());

	        
	        if(!mm.get("msgCode").equals("0"))
	        {
	        	model.addAttribute("NewValidParam", "");
	        	model.addAttribute("OldValidParam", ValidParam);
	        }
	        else
	        {
	        	model.addAttribute("NewValidParam", mm.get("newValidParam"));
	        	model.addAttribute("OldValidParam", mm.get("oldValidParam"));
	        }
        	model.addAttribute("error",mm.get("msgCode") + mm.get("msgName"));
        }
        catch(Exception e)
        {
        	model.addAttribute("error", e.getMessage());
        	model.addAttribute("NewValidParam", "");
        	model.addAttribute("OldValidParam", ValidParam);
        }
        DateFormat now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strBeginDate = now.format(new Date());
        model.addAttribute("Time", strBeginDate);

        return "BM3000/Result";
    }
*/
    
    @RequestMapping(value = "/changeKey", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanQuery = true)
    public HashMap changeKey(@RequestParam @Valid String KEY, @RequestParam String VALUE) 
    {
    	HashMap jsonhm = new HashMap();
    	//Reflected XSS All Clients\路徑 7、8:NB3Admin_20201207.pdf
    	//Log Forging\路徑 21、22、23、24:NB3Admin_20201207.pdf
    	String cleanKEY = (String) Sanitizer.logForgingStr(KEY);
    	String cleanVALUE = (String) Sanitizer.logForgingStr(VALUE);
        try
        {
        	log.info("ValidParam ：{}", cleanKEY);
        	log.info("ValidParam VALUE ：{}", cleanVALUE);
	        Map<String, String> mm = verifyParamChangeKeyService.changeVerifyParam(portalSSOService.getLoginUserId(),cleanKEY, cleanVALUE);

	        log.info("msgCode : {}", mm.get("msgCode"));
	        jsonhm.put("msgCode", mm.get("msgCode"));
	        jsonhm.put("msgName", mm.get("msgName"));
	        jsonhm.put("validated", ((String)mm.get("msgCode")).equals("0") ? true : false);
        }
        catch(Exception e)
        {
        	//Log Forging\路徑 20:NB3Admin_20201207.pdf
	        log.error("msgCode : {}", Sanitizer.logForgingStr(jsonhm.get("msgCode")));
	        jsonhm.put("msgCode", "FE0001");
	        //Information Exposure Through an Error Message\路徑 1:NB3Admin_20201207.pdf
	        jsonhm.put("msgName", "FE0001，請查看log");
	        jsonhm.put("validated", false);
        }

        return jsonhm;
    }
    
}
