package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.model.UploadedFile;
import com.netbank.rest.web.back.service.ADMLoginAclService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import fstop.aop.Authorize;
import fstop.orm.po.ADMLOGINACL;
import fstop.util.FileUtils;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

/**
 * 白名單管理
 * 
 * @author 簡哥
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B113")
public class B113Controller {
    @Autowired
    private ADMLoginAclService admLoginAclService;

    @Autowired
    private PortalSSOService portalSSOService;

    /**
     * 取得白名單管理首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value={"","/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        model.addAttribute("allowEdit", portalSSOService.isPermissionOK("B113", AuthorityEnum.EDIT));
        return "B113/index";
    }

    @SuppressWarnings("unchecked")
    @PostMapping(value="/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String ADUSERID, HttpServletRequest request, HttpServletResponse response ) {
        log.debug("query ADUSERID={}", Sanitizer.logForgingStr(ADUSERID));
        
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admLoginAclService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(), 
            jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(ADUSERID));

        List<ADMLOGINACL> loginACLs=(List<ADMLOGINACL>)page.getResult();
        List<ADMLOGINACL> sLoginACLs= new ArrayList<ADMLOGINACL>();
        for (ADMLOGINACL loginACL : loginACLs) {
            ADMLOGINACL sLoginACL = new ADMLOGINACL();
            Sanitizer.escape4Class(loginACL, sLoginACL);
            sLoginACLs.add(sLoginACL);
        }
        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), sLoginACLs);
        //20191115-Danny-Reflected XSS All Clients\路徑 8:
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得單一資料
     *
     * @param ADUSERID 假日代碼
     * @return ADMLOGINACL Json 物件
     */
    @PostMapping(value="/Get/{ADUSERID}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody ADMLOGINACL get(@PathVariable String ADUSERID) {
        ADUSERID = (String)Sanitizer.logForgingStr(ADUSERID);
        ADMLOGINACL entity = admLoginAclService.getLoginACL(ADUSERID);
        ADMLOGINACL oEntity = new ADMLOGINACL();
        //20191204-Danny-Missing Content Security Policy
        Sanitizer.escape4Class(entity, oEntity);
        return oEntity;
    }

    /**
     * 新增資料
     *
     * @param admLoginACL 物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse create(@ModelAttribute @Valid ADMLOGINACL admLoginACL, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
            	if ( admLoginAclService.getLoginACL(admLoginACL.getADUSERID()) != null ) {
            		Map<String, String> errors = new HashMap<String, String>();
            		//Reflected XSS All Clients
           		 	String aduserid = (String)Sanitizer.escapeHTML(admLoginACL.getADUSERID());
                    errors.put("summary", "身分證字號/統一編號["+aduserid+"]已存在");

                    response.setValidated(false);
                    response.setErrorMessages(errors);
                    return response;
            	} else {
            		admLoginAclService.insertLoginACL(admLoginACL, portalSSOService.getLoginUserId());
                    response.setValidated(true);
            	}
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 23:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 修改資料
     *
     * @param admLoginACL 物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid ADMLOGINACL admLoginACL, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                admLoginAclService.saveLoginACL(admLoginACL, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 24:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 刪除資料
     *
     * @param ADUSERID 資料
     * @return "0"：成功，非"0"：失敗原因
     */
    @PostMapping(value="/Delete/{ADUSERID}", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String delete(@PathVariable String ADUSERID) {
        try {
            ADUSERID = (String)Sanitizer.logForgingStr(ADUSERID);
            admLoginAclService.deleteLoginACL(ADUSERID);
            return "0";
        } catch (Exception e) {
            log.error("delete error", e);

            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }

    /**
     * ACL 檔上傳
     *
     * @param servletRequest a {@link javax.servlet.http.HttpServletRequest} object.
     * @param uploadedFile a {@link com.netbank.rest.web.back.model.UploadedFile} object.
     * @param bindingResult a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @RequestMapping(value="FileUpload")
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse saveFile(HttpServletRequest servletRequest,MultipartFile multipartFile) {
        JsonResponse response = new JsonResponse();
        try {
            // MultipartFile[] multipartFile = uploadedFile.getMultipartFile();

            //20191119-Danny-Unsafe Object Binding\路徑 1:
            //允許的類型
            String[] allowFileType = {"application/vnd.ms-excel","text/csv","text/plain"};
            //圖示允許的檔案大小
            int allowFileSize = 1*1024*1024;//1Mb
            String contentType = multipartFile.getContentType();
            log.debug("picFile contentType={}",Sanitizer.logForgingStr(contentType));
            String originalFilename = multipartFile.getOriginalFilename();
            log.debug("picFile originalFilename={}",Sanitizer.logForgingStr(originalFilename));
            Map<String,Object> returnMap = FileUtils.checkFile(multipartFile,originalFilename,contentType,allowFileType,allowFileSize);
            //檢核不通過
            if("FALSE".equals(returnMap.get("result"))){
                String errorMessage = (String)returnMap.get("summary");
                log.debug("errorMessage={}",Sanitizer.logForgingStr(errorMessage));
                throw new Exception(errorMessage);
            }
            String fileContent = (String)returnMap.get("Data");
            //20191119-Danny-增加處理BOM的問題
            String[] lines = FileUtils.RemoveUTF8BOM(fileContent).split("\\r?\\n");

            admLoginAclService.uploadLoginACL(lines, portalSSOService.getLoginUserId()); 
            response.setValidated(true);
            return response;
        } catch (Exception e) {
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 25:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
    }

    @GetMapping(value = { "/Sample" })
    @Authorize(userInRoleCanQuery = true)
    public void sample(ModelMap model, HttpServletResponse response) throws IOException {
        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"Whitelist.txt\"");
        
        OutputStream resOs = response.getOutputStream();
        // resOs.write('\ufeef'); // emits 0xef
        // resOs.write('\ufebb'); // emits 0xbb
        // resOs.write('\ufebf'); // emits 0xbf

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = new CSVWriter(outputWriter, CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_ESCAPE_CHARACTER);

        String[] memo = { "以下為範例資料，上傳時請勿包含身分證號...等欄位標題" };
        writer.writeNext(memo);

        String[] headers = { "身分證號" };
        writer.writeNext(headers);


        String[] nextLine = { "A123456789" };
        writer.writeNext(nextLine);
        
        writer.close();
    }
}