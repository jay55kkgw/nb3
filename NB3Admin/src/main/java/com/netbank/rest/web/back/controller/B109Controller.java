package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ADMREMITMENUService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMREMITMENU;
import fstop.orm.po.SYSOP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 匯款用途選單維護
 *
 * @author 簡哥
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B109")
public class B109Controller {
    @Autowired
    private ADMREMITMENUService admREMITMENUService;

    @Autowired
    private PortalSSOService portalSSOService;
    
    /**
     * 取得首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value={"","/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        if ( portalSSOService.isPermissionOK("B109", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        return "B109/index";
    }

    /**
     * 取得條件式查詢分頁結果
     * @param ADRMTTYPE     匯款用途性質
     * @param ADRMTID       匯款用途編號
     * @param ADLKINDID     大分類編號
     * @param ADMKINDID     中分類編號
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value="/IndexQuery", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse indexQuery(@RequestParam String ADRMTTYPE, @RequestParam String ADRMTID, @RequestParam String ADLKINDID, @RequestParam String ADMKINDID, HttpServletRequest request, HttpServletResponse response ) {
        log.debug("indexQuery ADRMTTYPE={}, ADRMTID={}, ADLKINDID={}, ADMKINDID={}", 
            Sanitizer.logForgingStr(ADRMTTYPE), Sanitizer.logForgingStr(ADRMTID), Sanitizer.logForgingStr(ADLKINDID), Sanitizer.logForgingStr(ADMKINDID));
        
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admREMITMENUService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
            jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(ADRMTTYPE), (String)Sanitizer.escapeHTML(ADLKINDID), (String)Sanitizer.escapeHTML(ADMKINDID), (String)Sanitizer.escapeHTML(ADRMTID));

        List<ADMREMITMENU> admRemitMenus = (List<ADMREMITMENU>) page.getResult();
        List<ADMREMITMENU> sadmRemitMenus = new ArrayList<ADMREMITMENU>();

        for (ADMREMITMENU admRemitMenu : admRemitMenus) {
            ADMREMITMENU sadmRemitMenu = new ADMREMITMENU();
            Sanitizer.escape4Class(admRemitMenu, sadmRemitMenu);
            sadmRemitMenus.add(sadmRemitMenu);
        }
        
        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), sadmRemitMenus);
        //20191115-Danny-Reflected XSS All Clients\路徑 6:
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得大分類頁目
     * @param ADRMTTYPE     匯款用途性質
     * @return
     */
    @PostMapping(value = "/LKindQuery", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody List<ADMREMITMENU> lkindQuery(@RequestParam String ADRMTTYPE) {
        ADRMTTYPE = (String)Sanitizer.logForgingStr(ADRMTTYPE);
        List<ADMREMITMENU> lRemitMenus= admREMITMENUService.getLKindMenus(ADRMTTYPE);
        List<ADMREMITMENU> slRemitMenus = new ArrayList<ADMREMITMENU>();
        for (ADMREMITMENU lRemitMenu : lRemitMenus) {
            ADMREMITMENU slRemitMenu = new ADMREMITMENU();
            Sanitizer.escape4Class(lRemitMenu, slRemitMenu);
            slRemitMenus.add(slRemitMenu);
        }
        

        return slRemitMenus;
    }

    /**
     * 取得中分類頁目
     * @param ADRMTTYPE     匯款用途性質
     * @param ADLKINDID     大分類項目
     * @return
     */
    @PostMapping(value = "/MKindQuery", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody List<ADMREMITMENU> mkindQuery(@RequestParam String ADRMTTYPE, @RequestParam String ADLKINDID) {
        ADRMTTYPE = (String)Sanitizer.logForgingStr(ADRMTTYPE);
        ADLKINDID = (String)Sanitizer.logForgingStr(ADLKINDID);
        List<ADMREMITMENU> mRemitMenus = admREMITMENUService.getMKindMenus(ADRMTTYPE, ADLKINDID);
        List<ADMREMITMENU> smRemitMenus = new ArrayList<ADMREMITMENU>();
        for (ADMREMITMENU mRemitMenu : mRemitMenus) {
            ADMREMITMENU smRemitMenu=new ADMREMITMENU();
            Sanitizer.escape4Class(mRemitMenu, smRemitMenu);
            smRemitMenus.add(smRemitMenu);
        }

        return smRemitMenus;
    }

    /**
     * 取得新增頁面
     * @param model
     * @return
     */
    @GetMapping(value={"/Create"})
    @Authorize(userInRoleCanEdit = true)
    public String create(ModelMap model) {
        return "B109/Create";
    }

    /**
     * 新增
     * @param remitMenu     POJO
     * @param result        Validation Result
     * @return              Result JSON
     */
    @PostMapping(value = "/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse create(@ModelAttribute @Valid ADMREMITMENU remitMenu, BindingResult result) {
        JsonResponse response = new JsonResponse();

        try {
            if (result.hasErrors()) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                        .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
            	if ( admREMITMENUService.getByRmtTypeRmtId(remitMenu.getADRMTTYPE(), remitMenu.getADRMTID()).size()>0 ) {
            		Map<String, String> errors = new HashMap<String, String>();
                    errors.put("summary", "同匯款用途性質及匯款用途編號已存在");

                    response.setValidated(false);
                    response.setErrorMessages(errors);
                    return response;
            	}
                admREMITMENUService.create(remitMenu, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 12:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }

        return response;
    }

    /**
     * 修改
     * @param ADSEQNO   序號
     * @param model
     * @return
     */
    @GetMapping(value = { "/Edit/{ADSEQNO}" })
    @Authorize(userInRoleCanQuery = true)
    public String edit(@PathVariable String ADSEQNO, ModelMap model) {
        ADSEQNO = (String)Sanitizer.logForgingStr(ADSEQNO);
        ADMREMITMENU po = admREMITMENUService.getByADSEQNO(Long.parseLong(ADSEQNO));
        model.addAttribute("remitMenu", po);
        if ( portalSSOService.isPermissionOK("B109", AuthorityEnum.EDIT)) {
        	String type = "";
        	if ( po.getADRMTID().isEmpty() ) {
        		if ( po.getADMKINDID().compareTo("00") == 0 ) {
        			type="L";
        		} else {
        			type="M";
        		}
        	} else {
        		type="";
        	}
        	model.addAttribute("type", type);
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        
        return "B109/Edit";
    }
    
    /**
     * 修改
     * @param remitMenu     POJO
     * @param result        Validation Result
     * @return              Result JSON
     */
    @PostMapping(value = "/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid ADMREMITMENU remitMenu, BindingResult result) {
        JsonResponse response = new JsonResponse();
        try {
            if (result.hasErrors()) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                        .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
            	List<ADMREMITMENU> rows = admREMITMENUService.getByRmtTypeRmtId(remitMenu.getADRMTTYPE(), remitMenu.getADRMTID());
            	for ( ADMREMITMENU row : rows ) {
            		// 使用者改匯款用途編號, 該編號又已存在
            		if ( row.getADSEQNO().longValue() != remitMenu.getADSEQNO().longValue() ) {
	            		Map<String, String> errors = new HashMap<String, String>();
	                    errors.put("summary", "同匯款用途性質及匯款用途編號已存在");
	
	                    response.setValidated(false);
	                    response.setErrorMessages(errors);
	                    return response;
            		}
            	}
                admREMITMENUService.update(remitMenu, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 13:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }

        return response;
    }
    
    /**
     * 依序號刪除資料
     * @param ADSEQNO   序號
     * @return
     */
    @PostMapping(value = "/Delete/{ADSEQNO}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse delete(@PathVariable String ADSEQNO) {
        ADSEQNO = (String)Sanitizer.logForgingStr(ADSEQNO);
        JsonResponse response = new JsonResponse();
        try {
            admREMITMENUService.delByADSEQNO(Long.parseLong(ADSEQNO));
            response.setValidated(true);
        } catch (Exception e) {
            log.error("delete error", e);

            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 14:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");
            response.setErrorMessages(errors);
            response.setValidated(false);
            return response;
        }
        return response;
    } 
    
    @PostMapping(value="/CreateLKind")
    public String createLKind(ModelMap model) {
        return "B109/CreateLKindPartial";
    }

    @PostMapping(value="/CreateMKind")
    public String createMKind(ModelMap model) {
        return "B109/CreateMKindPartial";
    }
    
    @PostMapping(value = "/CreateLKindSave", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse createLKindSave(@ModelAttribute @Valid ADMREMITMENU remitMenu, BindingResult result) {
        JsonResponse response = new JsonResponse();

        try {
            if (result.hasErrors()) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                        .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                admREMITMENUService.createLKind(remitMenu, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 12:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }

        return response;
    }
    
    @PostMapping(value = "/CreateMKindSave", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse createMKindSave(@ModelAttribute @Valid ADMREMITMENU remitMenu, BindingResult result) {
        JsonResponse response = new JsonResponse();

        try {
            if (result.hasErrors()) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                        .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                admREMITMENUService.createMKind(remitMenu, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 12:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }

        return response;
    }
}