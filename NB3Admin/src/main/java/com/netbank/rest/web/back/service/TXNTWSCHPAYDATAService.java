package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.B402Model;
import com.netbank.rest.web.back.model.BatchResult;
import com.netbank.rest.web.back.util.RESTfulClientUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.orm.po.TXNTWSCHPAYDATAIDENTITY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 預約交易結果查詢(台幣)
 * 
 * @author Alison
 */
@Slf4j
@Service
public class TXNTWSCHPAYDATAService extends BaseService {
    @Autowired
    private TxnTwSchPayDataDao txnTwSchPayDataDao;

    @Autowired
    private TxnTwRecordDao txnTwRecordDao;

    /**
     * http://172.22.11.22:80/{0}/{1}/batch/txntwtransfer
     */
    @Value("${TWRESEND.URL}")
    private String resendURL;

    /**
     * 分頁查詢
     * 
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param orderDir
     * @param STARTDATE
     * @param ENDDATE
     * @param USERID
     * @param STATUS
     * @return
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String STARTDATE, String ENDDATE,
            String USERID, String STATUS) {
        // 避免orderby的key包含了pk，要去掉放入sql
        if (orderBy.contains(".")) {
            String[] splitstr = orderBy.split("\\.");
            orderBy = splitstr[splitstr.length - 1];
        }
        log.debug(
                "getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, STARTDATE={}, ENDDATE={}, USERID={}, STATUS={}",
                Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy),
                Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(STARTDATE), Sanitizer.logForgingStr(ENDDATE),
                Sanitizer.logForgingStr(USERID), Sanitizer.logForgingStr(STATUS));

        log4Query("0", new String[][] { { "STARTDATE", STARTDATE }, { "ENDDATE", ENDDATE }, { "USERID", USERID },
                { "STATUS", STATUS } });
        return txnTwSchPayDataDao.findPageData(pageNo, pageSize, orderBy, orderDir, STARTDATE, ENDDATE, USERID, STATUS);
    }

    /**
     * 人工重作
     * 
     * @param dpschno
     * @param dpschtxdate
     * @return
     */
    public String reSend(String dpschno, String dpschtxdate, String dpuserid, String adtxno) {
        TXNTWSCHPAYDATAIDENTITY id = new TXNTWSCHPAYDATAIDENTITY();
        id.setDPSCHNO(dpschno);
        id.setDPUSERID(dpuserid);
        id.setDPSCHTXDATE(dpschtxdate);

        TXNTWSCHPAYDATA data = txnTwSchPayDataDao.findById(id);
        if (data.getDPTXSTATUS().compareTo("1") == 0 || data.getDPTXSTATUS().compareTo("2") == 0) {
            String url = resendURL;
            String tmp = data.getMSADDR();
            if (tmp.compareToIgnoreCase("ms_tw") == 0) {
                url = url.replace("{0}", "mstw");
                url = url.replace("{1}", "ms_tw");
            } else if (tmp.compareToIgnoreCase("ms_cc") == 0) {
                url = url.replace("{0}", "mscc");
                url = url.replace("{1}", "ms_cc");
            } else if (tmp.compareToIgnoreCase("ms_pay") == 0) {
                url = url.replace("{0}", "mspay");
                url = url.replace("{1}", "ms_pay");
            } else if (tmp.compareToIgnoreCase("ms_loan") == 0) {
                url = url.replace("{0}", "msloan");
                url = url.replace("{1}", "ms_loan");
            }

            Integer toMsTimeout = 120;
            try {
                List<String> inParam = new ArrayList<String>();

                inParam.add("executeone");
                inParam.add(dpschno);
                inParam.add(dpschtxdate);
                // ID DPUSERID
                inParam.add(dpuserid);
                // ADTXNO
                inParam.add(adtxno);

                // {
                // "batchName":
                // "isSuccess":
                // "errorCode":
                // }
                //
                BatchResult result = RESTfulClientUtil.sendBatch(inParam, url, toMsTimeout);
                if (result != null && result.isSuccess()) {
                    return "";
                } else {
                    return "失敗";
                }

            } catch (Exception e) {
                log.error("reSend Error", e);

                return e.getMessage();
            }
        } else {
            String msg = "預約批號[" + dpschno + "]-預約轉帳日[" + dpschtxdate + "], 交易執行狀態不為失敗，不可人工重做";
            return msg;
        }
    }

    /**
     * 取得此重送序號 TXNTWRECORDS 已出現的筆數(同轉出/轉入及金額)
     * 
     * @param dpschno
     * @param dpschtxdate
     * @param dpuserid
     * @param adtxno
     * @return
     */
    public List<TXNTWRECORD> getDuplicateTxnCounts(String dpschno, String dpschtxdate, String dpuserid, String adtxno) {
        TXNTWSCHPAYDATAIDENTITY id = new TXNTWSCHPAYDATAIDENTITY();
        id.setDPSCHNO(dpschno);
        id.setDPUSERID(dpuserid);
        id.setDPSCHTXDATE(dpschtxdate);

        // 執行時間取機器日, 因為有可能沒有執行, 就不會有值
        TXNTWSCHPAYDATA data = txnTwSchPayDataDao.findById(id);
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String dpTxDate = dateFormat.format(date);

        return txnTwRecordDao.getDuplicateTxn(data.getADOPID(), data.getTWSCHPAYDATAIDENTITY().getDPUSERID(), dpTxDate,
                data.getDPWDAC(), data.getDPSVAC(), data.getDPTXAMT());
    }

    /**
     * 
     * @param dptxdate
     * @param dpwdac
     * @param dpsvac
     * @param seqTRN
     * @param dptxamt
     * @return
     */
    public List<TXNTWRECORD> getTXNTWRECORDs(String dptxdate, String dpwdac, String dpsvac, String seqTRN,
            String dptxamt) {
        return txnTwRecordDao.getTXNTWRECORDs(dptxdate, dpwdac, dpsvac, seqTRN, dptxamt);
    }

    /**
     * 分頁查詢
     * 
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param orderDir
     * @param STARTDATE
     * @param ENDDATE
     * @param USERID
     * @return
     */
    public List<TXNTWSCHPAYDATA> find(String STARTDATE, String ENDDATE, String USERID) {
        log.debug("find STARTDATE={}, ENDDATE={}, USERID={}, STATUS={}", Sanitizer.logForgingStr(STARTDATE),
                Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID));

        log4Query("0", new String[][] { { "STARTDATE", STARTDATE }, { "ENDDATE", ENDDATE }, { "USERID", USERID } });
        return txnTwSchPayDataDao.findByParams(STARTDATE, ENDDATE, USERID);
    }

    /**
     * 分頁查詢
     * 
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param orderDir
     * @param STARTDATE
     * @param ENDDATE
     * @param USERID
     * @return
     */
    public List<B402Model> findCSV(String STARTDATE, String ENDDATE, String USERID, String status, boolean onlyResend) {
        log.debug("findCSV STARTDATE={}, ENDDATE={}, USERID={}", Sanitizer.logForgingStr(STARTDATE),
                Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID));

        log4Query("0", new String[][] { { "STARTDATE", STARTDATE }, { "ENDDATE", ENDDATE }, { "USERID", USERID } ,{ "STATUS", status }});
        return txnTwSchPayDataDao.findByParamsCSV(STARTDATE, ENDDATE, USERID, status, onlyResend);
    }
    
    public List<B402Model> findCSVByB402(String STARTDATE, String ENDDATE, String USERID) {
        log.debug("findCSV STARTDATE={}, ENDDATE={}, USERID={}", Sanitizer.logForgingStr(STARTDATE),
                Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID));

        log4Query("0", new String[][] { { "STARTDATE", STARTDATE }, { "ENDDATE", ENDDATE }, { "USERID", USERID } });
        return txnTwSchPayDataDao.findByParamsCSVByB402(STARTDATE, ENDDATE, USERID);
    }


    public static void main(String[] args) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        SimpleDateFormat formatterD = new SimpleDateFormat("yyyyMMdd");
        Calendar rightNow = Calendar.getInstance();
        
		rightNow.setTime(new java.util.Date());
	    rightNow.add(Calendar.YEAR, -1);
		Date dt2 = rightNow.getTime();
		String startDate = formatter.format(dt2)+"0101";
		String endDate = formatter.format(dt2)+"1231";
        System.out.println(startDate);
        System.out.println(endDate);
    }

}