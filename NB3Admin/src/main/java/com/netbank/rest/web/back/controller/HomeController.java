package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.FlowService;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.service.SYSOPService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.SYSOP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 首頁控制器，設定要登入才可以檢視，沒有會被 AOP 導到登入頁
 *
 * @author 簡哥
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping({"", "/Home"})
public class HomeController {
    @Autowired
    private SYSOPService sysOpService;
    
    @Autowired
    private PortalSSOService portalSSOService;

    @Autowired
    private FlowService flowService;

    /**
     * 首頁
     *
     * @param model     Model Map
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @return a {@link java.lang.String} object.
     */
    @Authorize
    @RequestMapping(value={"", "Index"})
    public String index(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
        model.put("todos", flowService.getTodo(portalSSOService.getLoginBranch(), portalSSOService.getLoginRoles().toArray(new String[0])));
        return "Home/index";
    }

    /**
     * 取得子選單
     *
     * @param ADOPGROUPID   父選單代碼
     * @return              SYSOP 清單
     */
    @PostMapping(value="/SubMenus/{ADOPGROUPID}")
    public @ResponseBody List<SYSOP> subMenus(@PathVariable String ADOPGROUPID) {
        log.debug("subMenus, ADOPGROUPID={}", Sanitizer.logForgingStr(ADOPGROUPID));
        List<SYSOP> myMenus = new ArrayList<SYSOP>();
        try {
            // 依登入者角色過濾選單，這邊呼叫快取過後的選單
            List<SYSOP> subMenus = sysOpService.getCachedSubMenus(ADOPGROUPID);
            for(SYSOP subMenu : subMenus ) {
                if ( portalSSOService.isMenuOK(subMenu.getADOPID())) {
                    SYSOP myMenu = new SYSOP();
                    myMenu.setADOPID((String)Sanitizer.escapeHTML(subMenu.getADOPID()));
                    myMenu.setADOPNAME((String)Sanitizer.escapeHTML(subMenu.getADOPNAME()));
                    myMenu.setURL(subMenu.getURL());

                    myMenus.add(myMenu);
                }
            }
        } catch (Exception e) {
            log.error("subMenus Error", e);
        }
        //20191118-Danny-Stored XSS\路徑 2:
        return (List<SYSOP>)(Object)Sanitizer.validList(myMenus);
    }
}
