package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmCountryDao;
import fstop.orm.po.ADMCOUNTRY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>ADMCOUNTRYService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class ADMCOUNTRYService extends BaseService {
    @Autowired
    private AdmCountryDao admCountryDao;

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo 第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy 依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param ADCTRYCODE 國別代碼
     * @param ADCTRYNAME 國別繁中
     * @param ADCTRYENGNAME 國別英文
     * @param ADCTRYCHSNAME 國別簡中
     * @return 分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String ADCTRYCODE, String ADCTRYNAME, String ADCTRYENGNAME, String ADCTRYCHSNAME) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, ADCTRYCODE={}, ADCTRYNAME={}, ADCTRYENGNAME={}, ADCTRYCHSNAME={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ADCTRYCODE), Sanitizer.logForgingStr(ADCTRYNAME), Sanitizer.logForgingStr(ADCTRYENGNAME), Sanitizer.logForgingStr(ADCTRYCHSNAME));

        log4Query("0", new String[][] { { "ADCTRYCODE", ADCTRYCODE }, { "ADCTRYNAME", ADCTRYNAME }, { "ADCTRYENGNAME", ADCTRYENGNAME }, { "ADCTRYCHSNAME", ADCTRYCHSNAME }} );
        return admCountryDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, ADCTRYCODE, ADCTRYNAME, ADCTRYENGNAME, ADCTRYCHSNAME);
    }

    /**
     * 取得國別資料
     *
     * @param ADCTRYCODE 國別代碼
     * @return 國別 POJO
     */
    public ADMCOUNTRY getByADCTRYCODE(String ADCTRYCODE, boolean logQuery) {
        log.debug("getByADCTRYCODE ADCTRYCODE={}", Sanitizer.logForgingStr(ADCTRYCODE));

        if ( logQuery ) {
        	log4Query("0", new String[][] { { "ADCTRYCODE", ADCTRYCODE } } );
        }
        return admCountryDao.findById(ADCTRYCODE);
    }

    /**
     * 新增國別資料
     *
     * @param country 國別 POJO
     * @param creator 編輯者
     */
    public void insertCountry(ADMCOUNTRY country, String creator) {
        country.setLASTUSER(creator); 
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        country.setLASTDATE(parts[0]);
        country.setLASTTIME(parts[1]);

        admCountryDao.save(country);
        
        Gson gson = new Gson();
        
        log4Create(gson.toJson(country), "0");
    } 

    /**
     * 儲存國別資料
     *
     * @param country 國別 POJO
     * @param editor 編輯者
     */
    public void saveCountry(ADMCOUNTRY country, String editor) {
        ADMCOUNTRY oriADMCOUNTRY = admCountryDao.findById(country.getADCTRYCODE());
        admCountryDao.getEntityManager().detach(oriADMCOUNTRY);

        country.setLASTUSER(editor); 

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        country.setLASTDATE(parts[0]);
        country.setLASTTIME(parts[1]);

        admCountryDao.update(country);
        log4Update(oriADMCOUNTRY, country, "0");
    }

    /**
     * 刪除國別資料
     *
     * @param countryId a {@link java.lang.String} object.
     */
    public void deleteCountry(String countryId) {
        ADMCOUNTRY oriADMCOUNTRY = admCountryDao.findById(countryId);
        admCountryDao.removeById(countryId);  
        log4Delete(oriADMCOUNTRY, "0");
    }
}
