package com.netbank.rest.web.back.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.service.TXNUSERService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fstop.aop.Authorize;
import fstop.orm.po.TXNUSER;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 信用卡申請網銀賬戶重新申請 Controller，使用一個畫面做 CRUD
 *
 * @author 楷翔
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B804")
public class B804Controller {
    @Autowired
    private TXNUSERService txnUSERService;


    @Autowired
    private PortalSSOService portalSSOService;

    /**
     * 取得信用卡申請網銀賬戶重新申請首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value={"", "/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {       
        log.trace("index");
        try {
            model.addAttribute("Data", new TXNUSER());
            model.addAttribute("error","");
        } catch (Exception e) {
            log.error("index error", e);
            model.addAttribute("error", e.getMessage());
        }
        
        return "B804/index";
    }

    /**
     * <p>submit.</p>
     *
     * @param txnuser a {@link fstop.orm.po.TXNUSER} object.
     * @param result a {@link org.springframework.validation.BindingResult} object.
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @RequestMapping(value = "/Result", method = RequestMethod.POST)
    @Authorize(userInRoleCanQuery = true)
    public String submit(@ModelAttribute @Valid TXNUSER txnuser, BindingResult result, ModelMap model) {
        //取原本的值
        TXNUSER oritxnuser= txnUSERService.getByDPSUERID((String)Sanitizer.logForgingStr(txnuser.getDPSUERID()));
        if(oritxnuser==null)
        {
            model.addAttribute("Data", txnuser);
            model.addAttribute("error", txnuser.getDPSUERID()+" 資料不存在");
            return "B804/index";
        } else {
            Sanitizer.logForging4Class(oritxnuser);
        
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            //替換掉修改的值     
            oritxnuser.setCCARDFLAG("0");
            oritxnuser.setRESETTIME(sdf.format(new Date()));
            txnUSERService.Update(oritxnuser, portalSSOService.getLoginUserId());
            //重新取資料
            oritxnuser=txnUSERService.getByDPSUERID((String)Sanitizer.logForgingStr(txnuser.getDPSUERID()));
            Sanitizer.logForging4Class(oritxnuser);
            
            model.addAttribute("Data", oritxnuser);
            DateFormat now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strBeginDate = now.format(new Date());
            model.addAttribute("Time", strBeginDate);
            model.addAttribute("Status", "核准");
            return "B804/Result";  
        }
        
    }
}
