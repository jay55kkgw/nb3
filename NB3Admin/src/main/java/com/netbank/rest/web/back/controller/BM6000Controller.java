package com.netbank.rest.web.back.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.netbank.rest.web.back.service.ADMMBSTATUSService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fstop.aop.Authorize;
import fstop.orm.po.ADMMBSTATUS;
import lombok.extern.slf4j.Slf4j;

/**
 * 應用系統啟動/關閉控制器
 *
 * @author Tim
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/BM6000")
public class BM6000Controller {
    private final String ADMBSTATUSID="TBBMB";

    @Autowired
    private ADMMBSTATUSService admMBSTATUSService;

    @Autowired
    private PortalSSOService portalSSOService;

    /**
     * 取得現行狀態畫面
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        ADMMBSTATUS po =  admMBSTATUSService.getByADMBSTATUSID(ADMBSTATUSID);
        // 給 Index View 做 Binding
        model.addAttribute("Data", po);
        if ( portalSSOService.isPermissionOK("BM6000", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        return "BM6000/index";
    }

    /**
     * 處理狀態變更，轉至結果頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @RequestMapping(value = "/Result", method = RequestMethod.POST)
    @Authorize(userInRoleCanEdit = true)
    public String submit(ModelMap model) {
        ADMMBSTATUS po = admMBSTATUSService.getByADMBSTATUSID(ADMBSTATUSID);
        String status = po.getADMBSTATUS().toString();
        if (status.equals("Y")) {
            po.setADMBSTATUS("N");
        } else {
            po.setADMBSTATUS("Y");
        }
        po.setLASTUSER(portalSSOService.getLoginUserId());
        admMBSTATUSService.save(po);

        String timeStamp = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
            Date lastDt = dateFormat.parse(po.getLASTDATE()+"-"+po.getLASTTIME());
            dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            timeStamp = dateFormat.format(lastDt);
        } catch (Exception e) {
            log.error("submit error", e);
            timeStamp = po.getLASTDATE()+"-"+po.getLASTTIME();
        }
        
        model.addAttribute("TimeStamp", timeStamp);
        model.addAttribute("Status", po.getADMBSTATUS().equalsIgnoreCase("Y") ? "啟動" : "關閉");
        return "BM6000/result";
    }
}
