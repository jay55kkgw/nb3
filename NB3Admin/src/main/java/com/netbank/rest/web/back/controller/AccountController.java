package com.netbank.rest.web.back.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netbank.BackendApplication;
import com.netbank.rest.web.back.model.FakeLogin;
import com.netbank.rest.web.back.service.PortalSSOService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;
import fstop.util.ESAPIUtils;
import fstop.util.Sanitizer;
/**
 * 登入控制器
 *
 * @author 簡哥
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping({"", "/Account"})
public class AccountController {
    @Autowired
    private PortalSSOService portalSSOService;
    
    @Value("${SKIP.SSO:false}")
    private boolean skipSSO;
    
    /**
     * SSO 登入
     *
     * @param model a {@link java.util.Map} object.
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @return a {@link java.lang.String} object.
     */
    @RequestMapping("/Login")
    public String login(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
        log.trace("login");
        try {
            if ( skipSSO ) {
                // 加一個 error="" 的 key 值，讓前端的 view 比較好寫
                model.put("error", "");
                model.put("loginbranch", loadProperties("fakelogin/loginbranch.properties"));
                model.put("loginrole", loadProperties("fakelogin/loginrole.properties"));
                model.put("loginuser", loadProperties("fakelogin/loginuser.properties"));
                return "Account/fakeLogin";
            } else {
                String xForwardedForHeader = request.getHeader("X-Forwarded-For");
                String clientIP = "";
    
                if (xForwardedForHeader == null) {
                    clientIP = request.getRemoteAddr();
                } else {
                    clientIP = new StringTokenizer(xForwardedForHeader, ",").nextToken().trim();
                }
                
                String authCode = request.getParameter("authCode");
                if ( authCode == null || authCode.isEmpty() ) {
                    throw new Exception("使用者尚未由 Portal 登入，authCode 不存在！！");
                } else {
                    portalSSOService.login(authCode, clientIP);
    
                    return "redirect:/Home/Index";
                }
            }
        } catch (Exception e) {
            log.error("login error", e);
            request.setAttribute("errorType", "AccountController login Error");
            request.setAttribute("errorDesc", "使用者尚未由 Portal 登入，authCode 不存在！！");
            return "forward:/Common/Error";
        }
    }

    /**
     * 模擬登入
     *
     * @param model a {@link com.netbank.rest.web.back.model.FakeLogin} object.
     * @param outMap a {@link java.util.Map} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping("/FakeLogin")
    public String fakeLogin(@ModelAttribute FakeLogin model, Map<String, Object> outMap) {
        log.trace("doLogin");
        try {
            if ( skipSSO ) {
                Properties loginbranch = loadProperties("fakelogin/loginbranch.properties");
                Properties loginrole = loadProperties("fakelogin/loginrole.properties");
                Properties loginuser = loadProperties("fakelogin/loginuser.properties");
                //20191115-Danny-Reflected XSS All Clients\路徑 36:
                String userId = (String)Sanitizer.escapeHTML(model.getUserId());
                //20191120-Danny-Trust Boundary Violation\路徑 1:
                String orgId = (String)Sanitizer.escapeHTML(model.getOrgId());
                String roleId = (String)Sanitizer.escapeHTML(model.getRoleId());  
                if ( loginbranch.containsKey(orgId) && loginuser.containsKey(userId) && loginrole.containsKey(roleId) ) {
                	
                	// Potential Stored XSS
                    portalSSOService.fakeLogin(orgId, userId, (String)Sanitizer.escapeHTML(loginuser.getProperty(userId)), roleId);
                    return "redirect:/Home/Index";
                } else {
                    outMap.put("error", "請選擇可測試的單位、人員及角色");
                    return "Account/fakeLogin";
                }
            } else {
                outMap.put("error", "非單元測試環境，不可使用模擬登入");
                return "Account/fakeLogin";
            }
        } catch (Exception e) {
            log.error("fakeLogin error", e);
            outMap.put("error", e.getMessage());
            return "Account/fakeLogin";
        }
    }

    /**
     * 登出
     *
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @return a {@link java.lang.String} object.
     */
    @RequestMapping("/Logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        try {
            portalSSOService.logout();
            request.setAttribute("__LOGIN_TIME__", "");
            request.setAttribute("__LOGIN_BRANCH__", "");
            request.setAttribute("__LOGIN_USERID__", "");
            request.setAttribute("__LOGIN_USERNAME__", "");
        
            return "Account/logout";
        } catch (Exception e) {
            log.error("logout error", e);
            return "redirect:/Common/Error";
        }
    }

    /**
     * 拒絕存取頁面
     *
     * @param model a {@link java.util.Map} object.
     * @return a {@link java.lang.String} object.
     */
    @RequestMapping("/AccessDenied")
    public String accessDenided(Map<String, Object> model) {
        log.trace("accessDenided");
        return "Account/accessDenied";
    }

    /**
     * 載入欄位對應參數
     * 
     * @param propname  屬性檔中文
     * @return
     */
    private Properties loadProperties(String propname) {
        Properties prop = new Properties();
        try (InputStream input = BackendApplication.class.getClassLoader().getResourceAsStream(propname)) {

            if (input != null) {
                prop.load(input);
            }
        } catch (IOException ex) {
            //20191115-Danny-Information Exposure Through an Error Message\路徑 1:
            //ex.printStackTrace();
            log.error("loadProperties error", ex);
        }
        return prop;
    }
}
