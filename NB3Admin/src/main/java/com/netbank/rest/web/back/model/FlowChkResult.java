package com.netbank.rest.web.back.model;

/**
 * 流程引擎檢核結果
 *
 * @author 簡哥
 * @version V1.0
 */
public class FlowChkResult {
    private String caseSN = "";
    private boolean myCase = false;
    private String matchStepId = "";

    /**
     * <p>Getter for the field <code>caseSN</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCaseSN() {
        return caseSN;
    }

    /**
     * <p>Setter for the field <code>caseSN</code>.</p>
     *
     * @param caseSN a {@link java.lang.String} object.
     */
    public void setCaseSN(String caseSN) {
        this.caseSN = caseSN;
    }


    /**
     * <p>Getter for the field <code>matchStepId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMatchStepId() {
        return matchStepId;
    }

    /**
     * <p>Setter for the field <code>matchStepId</code>.</p>
     *
     * @param matchStepId a {@link java.lang.String} object.
     */
    public void setMatchStepId(String matchStepId) {
        this.matchStepId = matchStepId;
    }

    /**
     * <p>isMyCase.</p>
     *
     * @return a boolean.
     */
    public boolean isMyCase() {
        return myCase;
    }

    /**
     * <p>Setter for the field <code>myCase</code>.</p>
     *
     * @param myCase a boolean.
     */
    public void setMyCase(boolean myCase) {
        this.myCase = myCase;
    }
}
