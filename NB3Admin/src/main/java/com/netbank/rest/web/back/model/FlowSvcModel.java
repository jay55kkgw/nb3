package com.netbank.rest.web.back.model;

import java.util.ArrayList;
import java.util.List;

import fstop.orm.po.ADMFLOWCURRENT;
import fstop.orm.po.ADMFLOWFINISHED;
import fstop.orm.po.ADMFLOWHISTORY;

/**
 * 流程引擎專用，最後要一起 Commit 的 DAO
 *
 * @author 簡哥
 * @version V1.0
 */
public class FlowSvcModel {
    private List<ADMFLOWCURRENT> flowCurrentAdd;
    private List<ADMFLOWCURRENT> flowCurrentUpd;
    private List<ADMFLOWCURRENT> flowCurrentDel;
    private List<ADMFLOWHISTORY> flowHistory;
    private List<ADMFLOWFINISHED> flowFinished;

    /**
     * <p>Constructor for FlowSvcModel.</p>
     */
    public FlowSvcModel() {
        flowCurrentAdd = new ArrayList<ADMFLOWCURRENT>();
        flowCurrentUpd = new ArrayList<ADMFLOWCURRENT>();
        flowCurrentDel = new ArrayList<ADMFLOWCURRENT>();
        flowHistory = new ArrayList<ADMFLOWHISTORY>();
        flowFinished = new ArrayList<ADMFLOWFINISHED>();
    }
    /**
     * <p>Getter for the field <code>flowCurrentAdd</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ADMFLOWCURRENT> getFlowCurrentAdd() {
        return flowCurrentAdd;
    }

    /**
     * <p>Setter for the field <code>flowCurrentAdd</code>.</p>
     *
     * @param flowCurrentAdd a {@link java.util.List} object.
     */
    public void setFlowCurrentAdd(List<ADMFLOWCURRENT> flowCurrentAdd) {
        this.flowCurrentAdd = flowCurrentAdd;
    }

    /**
     * <p>Getter for the field <code>flowCurrentUpd</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ADMFLOWCURRENT> getFlowCurrentUpd() {
        return flowCurrentUpd;
    }

    /**
     * <p>Setter for the field <code>flowCurrentUpd</code>.</p>
     *
     * @param flowCurrentUpd a {@link java.util.List} object.
     */
    public void setFlowCurrentUpd(List<ADMFLOWCURRENT> flowCurrentUpd) {
        this.flowCurrentUpd = flowCurrentUpd;
    }

    /**
     * <p>Getter for the field <code>flowCurrentDel</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ADMFLOWCURRENT> getFlowCurrentDel() {
        return flowCurrentDel;
    }

    /**
     * <p>Setter for the field <code>flowCurrentDel</code>.</p>
     *
     * @param flowCurrentDel a {@link java.util.List} object.
     */
    public void setFlowCurrentDel(List<ADMFLOWCURRENT> flowCurrentDel) {
        this.flowCurrentDel = flowCurrentDel;
    }

    /**
     * <p>Getter for the field <code>flowHistory</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ADMFLOWHISTORY> getFlowHistory() {
        return flowHistory;
    }

    /**
     * <p>Setter for the field <code>flowHistory</code>.</p>
     *
     * @param flowHistory a {@link java.util.List} object.
     */
    public void setFlowHistory(List<ADMFLOWHISTORY> flowHistory) {
        this.flowHistory = flowHistory;
    }

    /**
     * <p>Getter for the field <code>flowFinished</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ADMFLOWFINISHED> getFlowFinished() {
        return flowFinished;
    }

    /**
     * <p>Setter for the field <code>flowFinished</code>.</p>
     *
     * @param flowFinished a {@link java.util.List} object.
     */
    public void setFlowFinished(List<ADMFLOWFINISHED> flowFinished) {
        this.flowFinished = flowFinished;
    }

    
}
