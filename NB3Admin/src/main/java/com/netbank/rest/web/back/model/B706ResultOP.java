package com.netbank.rest.web.back.model;

public class B706ResultOP {

    private long twNotDoneCnt;
    private long fxNotDoneCnt;
    private long systemFailCnt;
    private long userFailCnt;
    private long succCnt;
    private long totalCnt;
    
	public long getTwNotDoneCnt() {
		return twNotDoneCnt;
	}
	public void setTwNotDoneCnt(long twNotDoneCnt) {
		this.twNotDoneCnt = twNotDoneCnt;
	}
	public long getFxNotDoneCnt() {
		return fxNotDoneCnt;
	}
	public void setFxNotDoneCnt(long fxNotDoneCnt) {
		this.fxNotDoneCnt = fxNotDoneCnt;
	}
	public long getSystemFailCnt() {
		return systemFailCnt;
	}
	public void setSystemFailCnt(long systemFailCnt) {
		this.systemFailCnt = systemFailCnt;
	}
	public long getUserFailCnt() {
		return userFailCnt;
	}
	public void setUserFailCnt(long userFailCnt) {
		this.userFailCnt = userFailCnt;
	}
	public long getSuccCnt() {
		return succCnt;
	}
	public void setSuccCnt(long succCnt) {
		this.succCnt = succCnt;
	}
	public long getTotalCnt() {
		return totalCnt;
	}
	public void setTotalCnt(long totalCnt) {
		this.totalCnt = totalCnt;
	}
    
}
