package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.google.gson.Gson;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.model.ZTreeNode;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.NB3SYSOPService;
import com.netbank.rest.web.back.service.PortalSSOService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.NB3SYSOPGROUP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 網銀端選單控制器
 *
 * @author SHIH-CHIEH CHIEN
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B111")
public class B111Controller {
    @Autowired
    private NB3SYSOPService nb3SysOpService;
    
    @Autowired
    private PortalSSOService portalSSOService;
    
    /**
     * 取得選單管理首頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value={"", "/Index"})
    @Authorize(userInRoleCanQuery=true)
    public String index(ModelMap model) {
        model.addAttribute("showAdd", portalSSOService.isPermissionOK("B111", AuthorityEnum.EDIT));
        try {
            List<NB3SYSOP> menus = nb3SysOpService.getAllMenus();
            List<ZTreeNode> nodes = new ArrayList<ZTreeNode>();

            for ( int i=0; i<menus.size(); i++ ) {
                ZTreeNode node = new ZTreeNode();
                node.setpId(menus.get(i).getADOPGROUPID());     // 上層節點 Id
                node.setId(menus.get(i).getADOPID());           // 本節點 Id
                node.setName(menus.get(i).getADOPNAME());       // 本節點名稱
                node.setOpen(false);
                nodes.add(node);
            }

            Gson gson = new Gson();
            String jsonMenus = gson.toJson(nodes);
            //log.debug((String)Sanitizer.logForgingStr("jsonMenus="+jsonMenus)); 

            // 給 Index View 做 Binding
            model.addAttribute("menus", jsonMenus); 
            if ( portalSSOService.isPermissionOK("B111", AuthorityEnum.EDIT)) {
                model.addAttribute("allowEdit", true);
            } else {
                model.addAttribute("allowEdit", false);
            }
        } catch (Exception e) {
            log.error("index error", e);

            // 給空值，給前端做判斷
            model.addAttribute("menu", "");
        }  
        return "B111/index";
    }
    
    /**
     * 取得新增選單的部份檢視（html）
     *
     * @param ADOPID a {@link java.lang.String} object.
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/Create/{ADOPID}")
    @Authorize(userInRoleCanEdit = true)
    public String create(@PathVariable String ADOPID, ModelMap model) {
        NB3SYSOP menu = new NB3SYSOP();
        menu.setADOPGROUPID(ADOPID);
        menu.setISANONYMOUS("0");
        menu.setADOPALIVE("1");
        menu.setISPOPUP("0");

        List<NB3SYSOPGROUP> groups = nb3SysOpService.getParentGroups();

        model.addAttribute("menu", menu);
        model.addAttribute("groups",groups);

        return "B111/createPartial";
    }

    /**
     * 取得修改選單的部份檢視（html）
     *
     * @param ADOPID 選單 Id
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/Edit/{ADOPID}")
    @Authorize(userInRoleCanQuery = true)
    public String edit(@PathVariable String ADOPID, ModelMap model) {
        NB3SYSOP menu = nb3SysOpService.getMenuByMenuId(ADOPID);
        model.addAttribute("menu", menu);
        model.addAttribute("showAdd", portalSSOService.isPermissionOK("B111", AuthorityEnum.EDIT));

        List<NB3SYSOPGROUP> groups = nb3SysOpService.getParentGroups();
        model.addAttribute("groups",groups);
        if ( portalSSOService.isPermissionOK("B111", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        return "B111/editPartial";
    }

    /**
     * 取得排序選單部份檢視（html）
     *
     * @param ADOPID 選單 Id
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/Sort/{ADOPID}")
    @Authorize(userInRoleCanEdit = true)
    public String sort(@PathVariable String ADOPID, ModelMap model) {
        List<NB3SYSOP> menus = nb3SysOpService.getSubMenus(ADOPID);
        model.addAttribute("menus", menus);

        return "B111/sortPartial";
    }

    /**
     * 取得移動選單部份檢視（html）
     *
     * @param ADOPID a {@link java.lang.String} object.
     * @param model 選單 Id
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/Move/{ADOPID}")
    @Authorize(userInRoleCanEdit = true)
    public String move(@PathVariable String ADOPID, ModelMap model) {
        NB3SYSOP menu = nb3SysOpService.getMenuByMenuId(ADOPID);

        // 要顯示的選單基本資料
        model.addAttribute("menu", menu);

        List<NB3SYSOP> firstMenus = nb3SysOpService.getLevel1Menus();
        model.addAttribute("firstMenus", firstMenus);

        return "B111/movePartial";
    }

    /**
     * 取得子選單清單
     *
     * @param ADOPID 選單 Id
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/SubMenu/{ADOPID}")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody String getSubMenu(@PathVariable String ADOPID) {
        List<NB3SYSOP> subMenus = nb3SysOpService.getSubMenus(ADOPID);
        List<NB3SYSOP> ssubMenus = new ArrayList<NB3SYSOP>();
        for (NB3SYSOP subMenu : subMenus) {
            NB3SYSOP ssubMenu = new NB3SYSOP();
            Sanitizer.escape4Class(subMenu, ssubMenu);
            ssubMenus.add(ssubMenu);
        }
        Gson gson = new Gson();
        return (String)(gson.toJson(ssubMenus));
    }

    /**
     * 取得交易小類
     * @param ADGPPARENT    父大類 ID
     * @return              NB3SYSOPGROUP 清單
     */
    @PostMapping(value="/SubGroup/{ADGPPARENT}")
    @Authorize(userInRoleCanEdit=true)
    public @ResponseBody String getSubGroup(@PathVariable String ADGPPARENT) {
        List<NB3SYSOPGROUP> groups = nb3SysOpService.getSubGroups(ADGPPARENT);
        List<NB3SYSOPGROUP> sgroups = new ArrayList<NB3SYSOPGROUP>();
        for (NB3SYSOPGROUP group : groups) {
            NB3SYSOPGROUP sgroup = new NB3SYSOPGROUP();
            Sanitizer.escape4Class(group, sgroup);
            sgroups.add(sgroup);
        }
        Gson gson = new Gson();
        return (String)(gson.toJson(sgroups)); 
    }

    /**
     * 新增儲存選單
     *
     * @param nb3sysop 選單物件
     * @param result a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/CreateSave", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse createSave(@ModelAttribute @Valid NB3SYSOP nb3sysop, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
            	if ( nb3SysOpService.getMenuByMenuId(nb3sysop.getADOPID()) != null ) {
               		Map<String, String> errors = new HashMap<String, String>();
               		//Reflected XSS All Clients
               		String adopid = (String)Sanitizer.escapeHTML(nb3sysop.getADOPID());
                    errors.put("summary", "選單["+adopid+"]已存在");

                    response.setValidated(false);
                    response.setErrorMessages(errors);
                    return response;
               	} else {
                    nb3SysOpService.insertMenu(nb3sysop, portalSSOService.getLoginUserId());
                    response.setValidated(true);
            	}
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 17:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 修改選單存檔
     *
     * @param nb3sysop 選單物件
     * @param result a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/EditSave", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse editSave(@ModelAttribute @Valid NB3SYSOP nb3sysop, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                nb3SysOpService.saveMenu(nb3sysop, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 18:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 刪除選單
     *
     * @param DPACCSETID 選單 Id
     * @return a {@link java.lang.String} object.
     */
    @Authorize(userInRoleCanEdit = true)
    @PostMapping(value="/Delete/{DPACCSETID}", produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String delete(@PathVariable Integer DPACCSETID) {
        try {
            nb3SysOpService.deleteMenu(DPACCSETID);
            return "0";
        } catch (Exception e) {
            log.error("delete error", e);
            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }

    /**
     * 排序儲存
     *
     * @param DPACCSETID 選單 Id 值，多個用 "，" 做分隔
     * @param ADSEQ 排序值，多個用 "，" 做分隔
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value="/SortSave")
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse sortSave(@RequestParam String DPACCSETID, @RequestParam String ADSEQ) {
        JsonResponse response = new JsonResponse();
        try {
            Map<String, Integer> sortedMenus = new HashMap<String, Integer>();
            
            String[] DPACCSETIDList = ((String)Sanitizer.escapeHTML(DPACCSETID)).split(",");
            String[] ADSEQList = ((String)Sanitizer.escapeHTML(ADSEQ)).split(",");
            for ( int i=0; i<DPACCSETIDList.length; i++ ) {
                sortedMenus.put(ADSEQList[i], Integer.parseInt(DPACCSETIDList[i]));
            }

            nb3SysOpService.sortMenu(sortedMenus);
            response.setValidated(true);
            
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 19:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        return response;
    }

    /**
     * 移動選單存檔
     *
     * @param DPACCSETID 選單 Id
     * @param NewADOPGROUPID    父選單 Id
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value="/MoveSave", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String moveSave(@RequestParam Integer DPACCSETID, @RequestParam String NewADOPGROUPID) {
        try {
            nb3SysOpService.moveMenu(DPACCSETID, NewADOPGROUPID);
            return "0";
            
        } catch (Exception e) {
            log.error("moveSave Error", e);
            
            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }
}
