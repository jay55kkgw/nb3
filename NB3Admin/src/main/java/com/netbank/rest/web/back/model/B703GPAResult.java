package com.netbank.rest.web.back.model;

/**
 * <p>B703GPAResult class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class B703GPAResult {
    private String adopGroup;
    private String adopGroupName;
    private String adUserType;
    private String qtyType;
    private String q00t;
    private String q00f;
    private String q01t;
    private String q01f;
    private String q02t;
    private String q02f;
    private String q03t;
    private String q03f;
    private String q10t;
    private String q10f;
    private String q11t;
    private String q11f;
    private String q12t;
    private String q12f;
    private String q13t;
    private String q13f;
    private String q99t;
    private String q99f;
    private String q99tf;

    /**
     * <p>Getter for the field <code>adopGroup</code>.</p>
     *
     * @return String return the adopGroup
     */
    public String getAdopGroup() {
        return adopGroup;
    }

    /**
     * <p>Setter for the field <code>adopGroup</code>.</p>
     *
     * @param adopGroup the adopGroup to set
     */
    public void setAdopGroup(String adopGroup) {
        this.adopGroup = adopGroup;
    }

    /**
     * <p>Getter for the field <code>adUserType</code>.</p>
     *
     * @return String return the adUserType
     */
    public String getAdUserType() {
        return adUserType;
    }

    /**
     * <p>Setter for the field <code>adUserType</code>.</p>
     *
     * @param adUserType the adUserType to set
     */
    public void setAdUserType(String adUserType) {
        this.adUserType = adUserType;
    }

    /**
     * <p>Getter for the field <code>qtyType</code>.</p>
     *
     * @return String return the qtyType
     */
    public String getQtyType() {
        return qtyType;
    }

    /**
     * <p>Setter for the field <code>qtyType</code>.</p>
     *
     * @param qtyType the qtyType to set
     */
    public void setQtyType(String qtyType) {
        this.qtyType = qtyType;
    }

    /**
     * <p>Getter for the field <code>q00t</code>.</p>
     *
     * @return String return the q00t
     */
    public String getQ00t() {
        return q00t;
    }

    /**
     * <p>Setter for the field <code>q00t</code>.</p>
     *
     * @param q00t the q00t to set
     */
    public void setQ00t(String q00t) {
        this.q00t = q00t;
    }

    /**
     * <p>Getter for the field <code>q00f</code>.</p>
     *
     * @return String return the q00f
     */
    public String getQ00f() {
        return q00f;
    }

    /**
     * <p>Setter for the field <code>q00f</code>.</p>
     *
     * @param q00f the q00f to set
     */
    public void setQ00f(String q00f) {
        this.q00f = q00f;
    }

    /**
     * <p>Getter for the field <code>q01t</code>.</p>
     *
     * @return String return the q01t
     */
    public String getQ01t() {
        return q01t;
    }

    /**
     * <p>Setter for the field <code>q01t</code>.</p>
     *
     * @param q01t the q01t to set
     */
    public void setQ01t(String q01t) {
        this.q01t = q01t;
    }

    /**
     * <p>Getter for the field <code>q01f</code>.</p>
     *
     * @return String return the q01f
     */
    public String getQ01f() {
        return q01f;
    }

    /**
     * <p>Setter for the field <code>q01f</code>.</p>
     *
     * @param q01f the q01f to set
     */
    public void setQ01f(String q01f) {
        this.q01f = q01f;
    }

    /**
     * <p>Getter for the field <code>q02t</code>.</p>
     *
     * @return String return the q02t
     */
    public String getQ02t() {
        return q02t;
    }

    /**
     * <p>Setter for the field <code>q02t</code>.</p>
     *
     * @param q02t the q02t to set
     */
    public void setQ02t(String q02t) {
        this.q02t = q02t;
    }

    /**
     * <p>Getter for the field <code>q02f</code>.</p>
     *
     * @return String return the q02f
     */
    public String getQ02f() {
        return q02f;
    }

    /**
     * <p>Setter for the field <code>q02f</code>.</p>
     *
     * @param q02f the q02f to set
     */
    public void setQ02f(String q02f) {
        this.q02f = q02f;
    }

    /**
     * <p>Getter for the field <code>q03t</code>.</p>
     *
     * @return String return the q03t
     */
    public String getQ03t() {
        return q03t;
    }

    /**
     * <p>Setter for the field <code>q03t</code>.</p>
     *
     * @param q03t the q03t to set
     */
    public void setQ03t(String q03t) {
        this.q03t = q03t;
    }

    /**
     * <p>Getter for the field <code>q03f</code>.</p>
     *
     * @return String return the q03f
     */
    public String getQ03f() {
        return q03f;
    }

    /**
     * <p>Setter for the field <code>q03f</code>.</p>
     *
     * @param q03f the q03f to set
     */
    public void setQ03f(String q03f) {
        this.q03f = q03f;
    }

    /**
     * <p>Getter for the field <code>q10t</code>.</p>
     *
     * @return String return the q10t
     */
    public String getQ10t() {
        return q10t;
    }

    /**
     * <p>Setter for the field <code>q10t</code>.</p>
     *
     * @param q10t the q10t to set
     */
    public void setQ10t(String q10t) {
        this.q10t = q10t;
    }

    /**
     * <p>Getter for the field <code>q10f</code>.</p>
     *
     * @return String return the q10f
     */
    public String getQ10f() {
        return q10f;
    }

    /**
     * <p>Setter for the field <code>q10f</code>.</p>
     *
     * @param q10f the q10f to set
     */
    public void setQ10f(String q10f) {
        this.q10f = q10f;
    }

    /**
     * <p>Getter for the field <code>q11t</code>.</p>
     *
     * @return String return the q11t
     */
    public String getQ11t() {
        return q11t;
    }

    /**
     * <p>Setter for the field <code>q11t</code>.</p>
     *
     * @param q11t the q11t to set
     */
    public void setQ11t(String q11t) {
        this.q11t = q11t;
    }

    /**
     * <p>Getter for the field <code>q11f</code>.</p>
     *
     * @return String return the q11f
     */
    public String getQ11f() {
        return q11f;
    }

    /**
     * <p>Setter for the field <code>q11f</code>.</p>
     *
     * @param q11f the q11f to set
     */
    public void setQ11f(String q11f) {
        this.q11f = q11f;
    }

    /**
     * <p>Getter for the field <code>q12t</code>.</p>
     *
     * @return String return the q12t
     */
    public String getQ12t() {
        return q12t;
    }

    /**
     * <p>Setter for the field <code>q12t</code>.</p>
     *
     * @param q12t the q12t to set
     */
    public void setQ12t(String q12t) {
        this.q12t = q12t;
    }

    /**
     * <p>Getter for the field <code>q12f</code>.</p>
     *
     * @return String return the q12f
     */
    public String getQ12f() {
        return q12f;
    }

    /**
     * <p>Setter for the field <code>q12f</code>.</p>
     *
     * @param q12f the q12f to set
     */
    public void setQ12f(String q12f) {
        this.q12f = q12f;
    }

    /**
     * <p>Getter for the field <code>q13t</code>.</p>
     *
     * @return String return the q13t
     */
    public String getQ13t() {
        return q13t;
    }

    /**
     * <p>Setter for the field <code>q13t</code>.</p>
     *
     * @param q13t the q13t to set
     */
    public void setQ13t(String q13t) {
        this.q13t = q13t;
    }

    /**
     * <p>Getter for the field <code>q13f</code>.</p>
     *
     * @return String return the q13f
     */
    public String getQ13f() {
        return q13f;
    }

    /**
     * <p>Setter for the field <code>q13f</code>.</p>
     *
     * @param q13f the q13f to set
     */
    public void setQ13f(String q13f) {
        this.q13f = q13f;
    }

    /**
     * <p>Getter for the field <code>q99t</code>.</p>
     *
     * @return String return the q99t
     */
    public String getQ99t() {
        return q99t;
    }

    /**
     * <p>Setter for the field <code>q99t</code>.</p>
     *
     * @param q99t the q99t to set
     */
    public void setQ99t(String q99t) {
        this.q99t = q99t;
    }

    /**
     * <p>Getter for the field <code>q99f</code>.</p>
     *
     * @return String return the q99f
     */
    public String getQ99f() {
        return q99f;
    }

    /**
     * <p>Setter for the field <code>q99f</code>.</p>
     *
     * @param q99f the q99f to set
     */
    public void setQ99f(String q99f) {
        this.q99f = q99f;
    }

    /**
     * <p>Getter for the field <code>q99tf</code>.</p>
     *
     * @return String return the q99tf
     */
    public String getQ99tf() {
        return q99tf;
    }

    /**
     * <p>Setter for the field <code>q99tf</code>.</p>
     *
     * @param q99tf the q99tf to set
     */
    public void setQ99tf(String q99tf) {
        this.q99tf = q99tf;
    }

    /**
     * <p>Getter for the field <code>adopGroupName</code>.</p>
     *
     * @return String return the adopGroupName
     */
    public String getAdopGroupName() {
        return adopGroupName;
    }

    /**
     * <p>Setter for the field <code>adopGroupName</code>.</p>
     *
     * @param adopGroupName the adopGroupName to set
     */
    public void setAdopGroupName(String adopGroupName) {
        this.adopGroupName = adopGroupName;
    }

}
