package com.netbank.rest.web.back.model;

/**
 * <p>B705Result class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class B705Result {
    private String adopid;
    private String adopname;
    private String adagreef;

    private String r00;
    private String r01;
    private String r02;
    private String r03;
    private String r04;
    private String r05;
    private String r06;
    private String r07;
    private String r08;
    private String r09;
    private String r10;
    private String r11;
    private String r12;
    private String r13;

    /**
     * <p>Getter for the field <code>adopid</code>.</p>
     *
     * @return String return the adopid
     */
    public String getAdopid() {
        return adopid;
    }

    /**
     * <p>Setter for the field <code>adopid</code>.</p>
     *
     * @param adopid the adopid to set
     */
    public void setAdopid(String adopid) {
        this.adopid = adopid;
    }

    /**
     * <p>Getter for the field <code>adopname</code>.</p>
     *
     * @return String return the adopname
     */
    public String getAdopname() {
        return adopname;
    }

    /**
     * <p>Setter for the field <code>adopname</code>.</p>
     *
     * @param adopname the adopname to set
     */
    public void setAdopname(String adopname) {
        this.adopname = adopname;
    }

    /**
     * <p>Getter for the field <code>adagreef</code>.</p>
     *
     * @return String return the adagreef
     */
    public String getAdagreef() {
        return adagreef;
    }

    /**
     * <p>Setter for the field <code>adagreef</code>.</p>
     *
     * @param adagreef the adagreef to set
     */
    public void setAdagreef(String adagreef) {
        this.adagreef = adagreef;
    }

    /**
     * <p>Getter for the field <code>r00</code>.</p>
     *
     * @return String return the r00
     */
    public String getR00() {
        return r00;
    }

    /**
     * <p>Setter for the field <code>r00</code>.</p>
     *
     * @param r00 the r00 to set
     */
    public void setR00(String r00) {
        this.r00 = r00;
    }

    /**
     * <p>Getter for the field <code>r01</code>.</p>
     *
     * @return String return the r01
     */
    public String getR01() {
        return r01;
    }

    /**
     * <p>Setter for the field <code>r01</code>.</p>
     *
     * @param r01 the r01 to set
     */
    public void setR01(String r01) {
        this.r01 = r01;
    }

    /**
     * <p>Getter for the field <code>r02</code>.</p>
     *
     * @return String return the r02
     */
    public String getR02() {
        return r02;
    }

    /**
     * <p>Setter for the field <code>r02</code>.</p>
     *
     * @param r02 the r02 to set
     */
    public void setR02(String r02) {
        this.r02 = r02;
    }

    /**
     * <p>Getter for the field <code>r03</code>.</p>
     *
     * @return String return the r03
     */
    public String getR03() {
        return r03;
    }

    /**
     * <p>Setter for the field <code>r03</code>.</p>
     *
     * @param r03 the r03 to set
     */
    public void setR03(String r03) {
        this.r03 = r03;
    }

    /**
     * <p>Getter for the field <code>r04</code>.</p>
     *
     * @return String return the r04
     */
    public String getR04() {
        return r04;
    }

    /**
     * <p>Setter for the field <code>r04</code>.</p>
     *
     * @param r04 the r04 to set
     */
    public void setR04(String r04) {
        this.r04 = r04;
    }

    /**
     * <p>Getter for the field <code>r05</code>.</p>
     *
     * @return String return the r05
     */
    public String getR05() {
        return r05;
    }

    /**
     * <p>Setter for the field <code>r05</code>.</p>
     *
     * @param r05 the r05 to set
     */
    public void setR05(String r05) {
        this.r05 = r05;
    }

    /**
     * <p>Getter for the field <code>r06</code>.</p>
     *
     * @return String return the r06
     */
    public String getR06() {
        return r06;
    }

    /**
     * <p>Setter for the field <code>r06</code>.</p>
     *
     * @param r06 the r06 to set
     */
    public void setR06(String r06) {
        this.r06 = r06;
    }

    /**
     * <p>Getter for the field <code>r07</code>.</p>
     *
     * @return String return the r07
     */
    public String getR07() {
        return r07;
    }

    /**
     * <p>Setter for the field <code>r07</code>.</p>
     *
     * @param r07 the r07 to set
     */
    public void setR07(String r07) {
        this.r07 = r07;
    }

    /**
     * <p>Getter for the field <code>r08</code>.</p>
     *
     * @return String return the r08
     */
    public String getR08() {
        return r08;
    }

    /**
     * <p>Setter for the field <code>r08</code>.</p>
     *
     * @param r08 the r08 to set
     */
    public void setR08(String r08) {
        this.r08 = r08;
    }

    /**
     * <p>Getter for the field <code>r09</code>.</p>
     *
     * @return String return the r09
     */
    public String getR09() {
        return r09;
    }

    /**
     * <p>Setter for the field <code>r09</code>.</p>
     *
     * @param r09 the r09 to set
     */
    public void setR09(String r09) {
        this.r09 = r09;
    }

    /**
     * <p>Getter for the field <code>r10</code>.</p>
     *
     * @return String return the r10
     */
    public String getR10() {
        return r10;
    }

    /**
     * <p>Setter for the field <code>r10</code>.</p>
     *
     * @param r10 the r10 to set
     */
    public void setR10(String r10) {
        this.r10 = r10;
    }

    /**
     * <p>Getter for the field <code>r11</code>.</p>
     *
     * @return String return the r11
     */
    public String getR11() {
        return r11;
    }

    /**
     * <p>Setter for the field <code>r11</code>.</p>
     *
     * @param r11 the r11 to set
     */
    public void setR11(String r11) {
        this.r11 = r11;
    }

    /**
     * <p>Getter for the field <code>r12</code>.</p>
     *
     * @return String return the r12
     */
    public String getR12() {
        return r12;
    }

    /**
     * <p>Setter for the field <code>r12</code>.</p>
     *
     * @param r12 the r12 to set
     */
    public void setR12(String r12) {
        this.r12 = r12;
    }

    /**
     * <p>Getter for the field <code>r13</code>.</p>
     *
     * @return String return the r13
     */
    public String getR13() {
        return r13;
    }

    /**
     * <p>Setter for the field <code>r13</code>.</p>
     *
     * @param r13 the r13 to set
     */
    public void setR13(String r13) {
        this.r13 = r13;
    }

}
