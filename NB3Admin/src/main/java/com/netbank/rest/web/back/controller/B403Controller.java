package com.netbank.rest.web.back.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.service.ADMMSGCODEService;
import com.netbank.rest.web.back.service.TXNFXSCHPAYDATAService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMMSGCODE;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNTWRECORD;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 預約明細查詢
 * 
 * @author Alison
 */
@Controller
@Slf4j
@RequestMapping("/B403")
public class B403Controller {
    @Autowired
    private TXNFXSCHPAYDATAService txnFXSchPayDataService;

    @Autowired
    private ADMMSGCODEService admMsgCodeService;
    
    /**
     * 取得查詢頁
     * 
     * @param model
     * @return
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
    	// 台幣可人工重送訊息代碼
        List<ADMMSGCODE> txs = admMsgCodeService.findFxManulResend();
        model.addAttribute("txs", txs);
        
        return "B403/index";
    }

    /**
     * 分頁查詢--外幣, 已不使用, 留著做參考
     * 
     * @param USERID   身分證字號/統一編號
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value = "/QueryFC", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse queryFC(@RequestParam String USERID, HttpServletRequest request, HttpServletResponse response) {
        log.debug("query USERID={}", Sanitizer.logForgingStr(USERID));

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String today = dateFormat.format(date);

        // 只查失敗的預約交易 STATUS＝"1","2"
        Page page = txnFXSchPayDataService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(), jqDataTableRq.getOrderBy(), 
            jqDataTableRq.getOrderDir(), today, today, (String) Sanitizer.escapeHTML(USERID), "1,2");

        // 外幣可人工重送訊息代碼
        List<ADMMSGCODE> txs = admMsgCodeService.findFxManulResend();

        List<TXNFXSCHPAYDATA> txnFxSchPayDatas = (List<TXNFXSCHPAYDATA>) page.getResult();
        List<TXNFXSCHPAYDATA> stxnFxSchPayDatas = new ArrayList<TXNFXSCHPAYDATA>();
        for (TXNFXSCHPAYDATA txnFxSchPayData : txnFxSchPayDatas) {
            TXNFXSCHPAYDATA stxnFxSchpayData = new TXNFXSCHPAYDATA();

            // 取得及判斷預約失敗是否可人工重送
            long matchCnt = txs.stream().filter(r->r.getADMCODE().compareToIgnoreCase(txnFxSchPayData.getFXEXCODE())==0).count();
            if ( matchCnt > 0 ) {
                Sanitizer.escape4Class(txnFxSchPayData, stxnFxSchpayData);

                //使用 FXSCHNO+FXSCHTXDATE+FXUSERID 為唯一值, 查詢重送次數
                String uniqueNo = txnFxSchPayData.getFXSCHPAYDATAIDENTITY().getFXSCHNO()+txnFxSchPayData.getFXSCHPAYDATAIDENTITY().getFXSCHTXDATE()+txnFxSchPayData.getFXSCHPAYDATAIDENTITY().getFXUSERID();
                
                
                List<TXNFXRECORD> txFxRecords = null;
                //找出TXNTWRECORD中的重送記錄
                txFxRecords = txnFXSchPayDataService.getTXNFXRECORDs(uniqueNo);

                if (txFxRecords != null && txFxRecords.size() > 0){
                    stxnFxSchpayData.setRESENDCOUNT(txFxRecords.size()-1);
                }else{
                    stxnFxSchpayData.setRESENDCOUNT(0);                    
                }
            }
            stxnFxSchPayDatas.add(stxnFxSchpayData);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), stxnFxSchPayDatas);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

        /**
     * 分頁查詢--外幣
     * 
     * @param USERID   身分證字號/統一編號
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value = "/IndexQueryFC", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody List<TXNFXSCHPAYDATA> indexWQueryFC(@RequestParam String USERID, HttpServletRequest request, HttpServletResponse response) {
        log.debug("query USERID={}", Sanitizer.logForgingStr(USERID));
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String today = dateFormat.format(date);

        // 只查失敗的預約交易 STATUS＝"1","2"
        List<TXNFXSCHPAYDATA> txnFxSchPayDatas = txnFXSchPayDataService.find(today, today, (String) Sanitizer.escapeHTML(USERID));

        // 外幣可人工重送訊息代碼
        List<ADMMSGCODE> txs = admMsgCodeService.findFxManulResend();
        List<TXNFXSCHPAYDATA> stxnFxSchPayDatas = new ArrayList<TXNFXSCHPAYDATA>();
        for (TXNFXSCHPAYDATA txnFxSchPayData : txnFxSchPayDatas) {
            TXNFXSCHPAYDATA stxnFxSchpayData = new TXNFXSCHPAYDATA();

            Sanitizer.escape4Class(txnFxSchPayData, stxnFxSchpayData);
            stxnFxSchPayDatas.add(stxnFxSchpayData);
        }
        List<TXNFXSCHPAYDATA> rsultstxnFxSchPayDatas = new ArrayList<TXNFXSCHPAYDATA>();
        Sanitizer.escape4Class(stxnFxSchPayDatas, rsultstxnFxSchPayDatas);

        return rsultstxnFxSchPayDatas;
    }

    @PostMapping(value = "/Resend", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody Map resend(@RequestParam String fxschno, @RequestParam String fxschtxdate, @RequestParam String fxuserid) {
        //20191115-Danny-Reflected XSS All Clients\路徑 25:
        fxschno = (String)Sanitizer.escapeHTML(fxschno);
        fxschtxdate = (String)Sanitizer.escapeHTML(fxschtxdate);
        fxuserid = (String)Sanitizer.escapeHTML(fxuserid);
        
        Map<String, String> response = new HashMap<String, String>();
        try {
            
            String msg = txnFXSchPayDataService.reSend((String) Sanitizer.escapeHTML(fxschno), (String) Sanitizer.escapeHTML(fxschtxdate), (String) Sanitizer.escapeHTML(fxuserid));
            response.put("status", msg);
        } catch (Exception e) {
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤，請再試一次!!";
            log.error("Query error", e);
            response.put("status", (String)Sanitizer.logForgingStr(message));
            return response;
        }
        
        return response;
    }
    
    //單筆終止 已移去B405
//    @PostMapping(value = "/Terminate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    @Authorize(userInRoleCanQuery = true)
//    public @ResponseBody Map<String,String> Terminate(@RequestParam String FXSCHNO,@RequestParam String FXSCHTXDATE,@RequestParam String FXUSERID) {
//    	Map<String,String> result  = new HashMap<String,String>();
//    	try {
//    		result = txnFxrecordService.Terminate(FXSCHNO,FXSCHTXDATE,FXUSERID);
//    	}catch(Exception e) {
//    		log.error("e>>>{}",e);
//    	}
//    	return result;
//    }
    
    /**
     * 重覆交易檢核
     * @param dpschno
     * @param dpschtxdate
     * @param dpuserid
     * @param adtxno
     * @return
     */
    @PostMapping(value = "/DuplicateCheck", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody Map duplicateCheck(@RequestParam String fxschno, @RequestParam String fxschtxdate, @RequestParam String fxuserid) {
        //20191115-Danny-Reflected XSS All Clients\路徑 25:
        fxschno = (String)Sanitizer.escapeHTML(fxschno);
        fxschtxdate = (String)Sanitizer.escapeHTML(fxschtxdate);
        fxuserid = (String)Sanitizer.escapeHTML(fxuserid);
        
        Map<String, Integer> response = new HashMap<String, Integer>();
        try {
            
        	List<TXNFXRECORD> txns=txnFXSchPayDataService.getDuplicateTxnCounts((String) Sanitizer.escapeHTML(fxschno), (String) Sanitizer.escapeHTML(fxschtxdate)
                                                , (String) Sanitizer.escapeHTML(fxuserid));
            response.put("counts", txns.size());
            if ( txns.size()>0 ) {
            	response.put("time", Integer.parseInt(txns.get(0).getFXTXTIME()));
            }
        } catch (Exception e) {
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤，請再試一次!!";
            log.error("Query error", e);
            response.put("counts", -1);
            return response;
        }
        
        return response;
    }

    /**
     * <p>
     * statusDialog.
     * </p>
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @param id    a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value = "/MsgDialog/{id}")
    public String msgDialog(@PathVariable String id, ModelMap model) {
        String MsgOut = "訊息說明 : " + admMsgCodeService.getMsgIn(id);
        String MsgIn = "客戶訊息 : " + admMsgCodeService.getMsg(id);
        model.addAttribute("ErrMsg", MsgOut + "<br>" + MsgIn);
        return "B205/msgPartial";
    }
}