
package com.netbank.rest.web.back.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RequestResponseFilter implements Filter {
    
  protected FilterConfig filterConfig;

    @Override
    public void doFilter(
      ServletRequest request, 
      ServletResponse response, 
      FilterChain chain) throws IOException, ServletException {
          HttpServletResponse resp = (HttpServletResponse) response;
      try {
        // Missing X Frame Options
        //20191204-Danny-Frameable Login Page 
        resp.setHeader("X-Frame-Options", "DENY");			
        //20191204-Danny-Missing HSTS Header
        resp.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubdomains");

        // 通過此Filter
        chain.doFilter(request, response);			
      } catch (Exception e) {
        log.error("",e);
      }

    }

    /**
    * init() : init() method called when the filter is instantiated.
    * This filter is instantiated the first time j_security_check is 
    * invoked for the application (When a protected servlet in the 
    * application is accessed).
    */
    public void init(FilterConfig filterConfig) throws ServletException {
      this.filterConfig = filterConfig;
    }


    /**
      * destroy() : destroy() method called when the filter is taken 
      * out of service.
      */
    public void destroy() {
        this.filterConfig = null;
    }

  }