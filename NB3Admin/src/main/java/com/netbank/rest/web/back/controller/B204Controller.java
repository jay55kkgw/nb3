package com.netbank.rest.web.back.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import fstop.aop.Authorize;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Type;
import java.util.ArrayList;
import com.netbank.rest.web.back.service.TXNTWSCHPAYService;
import com.netbank.rest.web.back.service.TXNFXSCHPAYService;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import com.netbank.rest.web.back.model.B204NTDModel;
import com.netbank.rest.web.back.model.B204FCModel;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.util.Sanitizer;
import org.springframework.http.MediaType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 預約明細查詢
 * 
 * @author Alison
 */
@Controller
@Slf4j
@RequestMapping("/B204")
public class B204Controller {
    @Autowired
    private TXNTWSCHPAYService txnTWSchPayService;
    @Autowired
    private TXNFXSCHPAYService txnFXSchPayService;

    /**
     * 取得查詢頁
     * 
     * @param model
     * @return
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {

        return "B204/index";

    }

    /**
     * 轉至結果頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping("/Query")
    public String query(@RequestParam(value = "UserId") String userId, ModelMap modelMap) {

        modelMap.put("userId", userId);

        return "B204/query";
    }

    /**
     * 分頁查詢--台幣
     * 
     * @param USERID   身分證字號/統一編號
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value = "/QueryNTD", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse queryNTD(@RequestParam String USERID, HttpServletRequest request,
            HttpServletResponse response) {
        log.debug("query USERID={}", Sanitizer.logForgingStr(USERID));

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = txnTWSchPayService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
                jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String) Sanitizer.escapeHTML(USERID));

        List<TXNTWSCHPAY> tmp = (List<TXNTWSCHPAY>) page.getResult();
        List<TXNTWSCHPAY> stxnTwSchPays = new ArrayList<TXNTWSCHPAY>();
        for (TXNTWSCHPAY txnTwSchPay : tmp) {
            TXNTWSCHPAY stxnTwSchPay=new TXNTWSCHPAY();
            Sanitizer.escape4Class(txnTwSchPay, stxnTwSchPay);
            stxnTwSchPays.add(stxnTwSchPay);
        }

        Gson gson = new Gson();
        String tmpStr = gson.toJson(stxnTwSchPays);

        Type listType = new TypeToken<ArrayList<B204NTDModel>>() {
        }.getType();
        List<B204NTDModel> txnTwSchPays = (List<B204NTDModel>) gson.fromJson(tmpStr, listType);
        for (B204NTDModel txnTwSchPay : txnTwSchPays) {
            String getNextDate = txnTWSchPayService.getDPNEXTDATE(USERID, txnTwSchPay.getDPSCHNO());
            String DPNEXTDATE = "";

            if (getNextDate == null || getNextDate == "") {
                // 如果是空就是生效日
                DPNEXTDATE = txnTwSchPay.getDPFDATE();
            } else {
                DPNEXTDATE = getNextDate;
            }
            txnTwSchPay.setTXTYPE((String)Sanitizer.escapeHTML(txnTWSchPayService.getADOPNAME(txnTwSchPay.getADOPID())));
            txnTwSchPay.setDPNEXTDATE((String)Sanitizer.escapeHTML(DPNEXTDATE));
        }


        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), txnTwSchPays);

        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);
        return sjqDataTableRs;
    }

    /**
     * 分頁查詢--外幣
     * 
     * @param USERID   身分證字號/統一編號
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value = "/QueryFC", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse queryFC(@RequestParam String USERID, HttpServletRequest request,
            HttpServletResponse response) {
        log.debug("query USERID={}", Sanitizer.logForgingStr(USERID));

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = txnFXSchPayService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
                jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String) Sanitizer.escapeHTML(USERID));

        List<TXNFXSCHPAY> tmp = (List<TXNFXSCHPAY>) page.getResult();

        List<TXNFXSCHPAY> stxnFxSchPays = new ArrayList<TXNFXSCHPAY>();
        for (TXNFXSCHPAY txnFxSchPay : tmp) {
            TXNFXSCHPAY stxnFxSchPay=new TXNFXSCHPAY();
            Sanitizer.escape4Class(txnFxSchPay, stxnFxSchPay);
            stxnFxSchPays.add(stxnFxSchPay);
        }


        Gson gson = new Gson();
        String tmpStr = gson.toJson(stxnFxSchPays);

        Type listType = new TypeToken<ArrayList<B204FCModel>>() {
        }.getType();
        List<B204FCModel> txnFxSchPays = (List<B204FCModel>) gson.fromJson(tmpStr, listType);
        for (B204FCModel txnFxSchPay : txnFxSchPays) {
            
            String getNextDate = txnFXSchPayService.getFXNEXTDATE(USERID, txnFxSchPay.getFXSCHNO());
            String FXNEXTDATE = "";

            if (getNextDate == null || getNextDate == "") {
                // 如果是空就是生效日
                FXNEXTDATE = txnFxSchPay.getFXFDATE();
            } else {
                FXNEXTDATE = getNextDate;
            }
            
            txnFxSchPay.setTXTYPE((String)Sanitizer.escapeHTML(txnTWSchPayService.getADOPNAME(txnFxSchPay.getADOPID())));
            txnFxSchPay.setFXNEXTDATE((String)Sanitizer.escapeHTML(FXNEXTDATE));
        }

  

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), txnFxSchPays);

        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);
        return sjqDataTableRs;
    }
}