package com.netbank.rest.web.back.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ADMROLEAUTHService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.FlowService;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.service.SYSOPService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

import fstop.aop.Authorize;
import fstop.orm.po.ADMFLOWCURRENT;
import fstop.orm.po.ADMROLEAUTH;
import fstop.orm.po.SYSOP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 角色權限維護控制器
 *
 * @author 簡哥
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B101")
public class B101Controller {
    @Autowired
    private ADMROLEAUTHService admRoleAuthService;

    @Autowired
    private SYSOPService sysOpService;
    
    @Autowired
    private PortalSSOService portalSSOService;

    @Autowired
    private FlowService flowService;

    /**
     * <p>待辦頁面</p>
     *
     * @param model         Model Map
     * @return              page name
     */
    @GetMapping(value={"","/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        List<SYSOP> menus = sysOpService.getLevel1Menus();
        model.addAttribute("menus", menus);

        if ( portalSSOService.isPermissionOK("B101", AuthorityEnum.EDIT) || 
             portalSSOService.isPermissionOK("B101", AuthorityEnum.REVIEW) ) {
            model.addAttribute("showAdd", portalSSOService.isPermissionOK("B101", AuthorityEnum.EDIT));
            return "B101/index";
        } else {
            //return "redirect:QueryRole";
        	return "forward:QueryRole";
        }
    }

    /**
     * 取得查詢頁面
     * @param model     Model Map
     * @return          page name
     */
    @GetMapping(value={"/QueryRole"})
    @Authorize(userInRoleCanQuery = true)
    public String queryRole(ModelMap model) {
        List<SYSOP> menus = sysOpService.getLevel1Menus();
        model.addAttribute("menus", menus);
            
        return "B101/query";
    }

    /**
     * 取得新增/設定頁面
     * @param model     Model Map
     * @return          page name
     */
    @GetMapping(value="/Create")
    @Authorize(userInRoleCanEdit = true)
    public String create(ModelMap model) {
        return "B101/create";
    }

    /**
     * 取得待辦事項
     * @param ADROLENO      角色代碼
     * @param request       Http Servlet Request
     * @param response      Http Servlet Response
     * @return              支援 jQuery Datatable Server 端的分頁物件
     */
    @PostMapping(value="/QueryTodo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JQueryDataTableResponse queryTodo(@RequestParam String ADROLENO, HttpServletRequest request, HttpServletResponse response ) {
        ADROLENO = (String)Sanitizer.logForgingStr(ADROLENO);
        
        List<ADMFLOWCURRENT> todos = admRoleAuthService.getTodo(portalSSOService.getLoginBranch());

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        List<ADMFLOWCURRENT> stodos = new ArrayList<ADMFLOWCURRENT>();
        for (ADMFLOWCURRENT todo : todos) {
            ADMFLOWCURRENT stodo = new ADMFLOWCURRENT();
            Sanitizer.escape4Class(todo, stodo);
            stodos.add(stodo);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), stodos.size(), stodos.size(), stodos);
        //解決：Reflected XSS All Clients\路徑 2:
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs,sjqDataTableRs);
        return sjqDataTableRs;
    }

    /**
     * 分頁查詢
     *
     * @param ADROLENO      角色代碼
     * @param APOPID        選單代碼
     * @param request       HttpServletRequest
     * @param response      HttpServletResponse
     * @return              支援 jQuery Datatable Server 端的分頁物件
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value="/QueryRole", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse queryRole(@RequestParam String ADROLENO, @RequestParam String APOPID, @RequestParam String ADSTAFFNO, HttpServletRequest request, HttpServletResponse response ) {
        //20191120-Danny-Log Forging\路徑 2:
        ADROLENO = (String)Sanitizer.escapeHTML(ADROLENO);
        APOPID = (String)Sanitizer.escapeHTML(APOPID);
        ADSTAFFNO = (String)Sanitizer.escapeHTML(ADSTAFFNO);
        
        log.debug("query ADROLENO={}, APOPID={}, ADSTAFFNO={}", 
        		Sanitizer.logForgingStr(ADROLENO), Sanitizer.logForgingStr(APOPID), Sanitizer.logForgingStr(ADSTAFFNO));

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admRoleAuthService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(), 
            (String)Sanitizer.escapeHTML(jqDataTableRq.getOrderBy()), (String)Sanitizer.escapeHTML(jqDataTableRq.getOrderDir()), (String)Sanitizer.escapeHTML(ADROLENO),
            (String)Sanitizer.escapeHTML(APOPID), (String)Sanitizer.escapeHTML(ADSTAFFNO));

        List<ADMROLEAUTH> auths=(List<ADMROLEAUTH>)page.getResult();
        List<ADMROLEAUTH> sAuths = new ArrayList<ADMROLEAUTH>();
        for (ADMROLEAUTH auth : auths) {
            ADMROLEAUTH sAuth = new ADMROLEAUTH();
            Sanitizer.escape4Class(auth, sAuth);
            sAuths.add(sAuth);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), sAuths);
        //20191115-Danny-Reflected XSS All Clients\路徑 3:
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs,sjqDataTableRs);

        return sjqDataTableRs;
    }


    /**
     * 取得子選單清單
     *
     * @param ADOPID    選單 Id
     * @return          子選單清單 JSON 字串
     */
    @PostMapping(value="/SubMenu/{ADOPID}")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody String getSubMenu(@PathVariable String ADOPID) {
        List<SYSOP> subMenus = sysOpService.getSubMenus(ADOPID);
        List<SYSOP> ssubMenus = new ArrayList<SYSOP>();
        for (SYSOP subMenu : subMenus) {
            SYSOP ssubMenu = new SYSOP();
            Sanitizer.escape4Class(subMenu, ssubMenu);
            ssubMenus.add(ssubMenu);
        }
        Gson gson = new Gson();

        return (String)gson.toJson(ssubMenus);
    }

    /**
     * 開啟選擇功能的 Dialog Partail View
     * @param model         ViewModel
     * @return              新增的 Dialog html
     */
    @PostMapping(value="/RoleAuth")
    public String roleAuthDialog(ModelMap model) {
        List<SYSOP> menus = sysOpService.getLevel1Menus();
        model.addAttribute("menus", menus);
        return "B101/roleAuthPartial";
    }

    /**
     * 經辦送出
     * @param ADROLENO      角色代碼
     * @param roleAuths     角色授權資料
     * @return              JSON Response
     */
    @PostMapping(value="/Send", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse send(@RequestParam String ADROLENO, @RequestParam String ADSTAFFNO, @RequestParam String roleAuths){
        JsonResponse response = null;
        try {
            Map<String, String> svcResult = admRoleAuthService.send(portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName(), ADROLENO, ADSTAFFNO, roleAuths);
            Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
            if ( entry.getKey().equalsIgnoreCase("0") ) {
                response = new JsonResponse();
                response.setValidated(true);
                response.setPkey(entry.getValue());
            } else {
                throw new Exception(entry.getKey());
            }
        } catch (Exception e) {
            log.error("send", e);
            Map<String, String> errors = new HashMap<String, String>();
            //20191120-Danny-Information Exposure Through an Error Message\路徑 1:
            errors.put("summary", "系統錯誤，請重新嘗試");
            response = new JsonResponse();
            response.setValidated(false);
            response.setErrorMessages(errors);
            //Information Exposure Through an Error Message
            return response;
        }
        
        return response;
    } 

    /**
     * 待辦查詢分派方法，依使用者權限做分派
     * @param id            案號
     * @param model         Model Map
     * @return              page name
     */
    @GetMapping(value={"/Query/{id}"})
    @Authorize()
    public String query(@PathVariable String id, ModelMap model) {
        id = (String)Sanitizer.logForgingStr(id);
        ADMFLOWCURRENT flowCurrent = flowService.getTodoByCaseSN(id);
        AuthorityEnum caseStatus = admRoleAuthService.queryCaseAuthority(id, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginRoles());

        Gson gson = new Gson();

        Type type = new TypeToken<Map<String, String>>(){}.getType();
        //20191202-Danny-避免FlowData內容錯誤(非Json資料)
        Map<String, String> flowDataMap;
        try {
            //20191205-Danny-處理舊資料&amp;amp;quot;
            String flowData = flowCurrent.getFLOWDATA().replace("&amp;amp;quot;", "\"");
            flowDataMap = gson.fromJson(flowData, type);
        } catch (Exception e) {
            log.error("query", e);
            throw e;
        }

        model.addAttribute("roleAuths", flowDataMap.get("auth"));
        model.addAttribute("roleid", flowDataMap.get("roleid"));
        model.addAttribute("staffno", flowDataMap.get("staffno"));
        model.addAttribute("casesn", id);
        if (caseStatus == AuthorityEnum.EDIT || caseStatus == AuthorityEnum.NONE) {
            if (portalSSOService.isInRole(ADMROLEAUTHService.EDITOR_ROLES)) {
                //model.addAttribute("roleAuths", flowCurrent.getFLOWDATA());

                // 給前端頁面變更
                model.addAttribute("casesn", flowCurrent.getCURRENTIDENTITY().getCASESN());
                model.addAttribute("stepid", flowCurrent.getSTEPID());
                return "B101/edit";
            } else {
                //model.addAttribute("roleAuths", flowCurrent.getFLOWDATA());
                return "B101/query";
            }
        } else if ( caseStatus == AuthorityEnum.REVIEW ) {
            model.addAttribute("casesn", flowCurrent.getCURRENTIDENTITY().getCASESN());
            model.addAttribute("stepid", flowCurrent.getSTEPID());
            return "B101/review";
        } else {
            //model.addAttribute("roleAuths", flowCurrent.getFLOWDATA());
            return "B101/query";
        }
    }

    /**
     * 同意
     *
     * @param id                案號
     * @param httpRequest       http request
     * @param httpResponse      http response
     * @return                  a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     */
    @PostMapping(value={"/Approve/{id}"})
    @ResponseBody
    @Authorize(userInRoleCanApprove = true)
    public JsonResponse approve(@PathVariable String id, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        id = (String)Sanitizer.logForgingStr(id);
        JsonResponse response = new JsonResponse();
        try {
            String stepId = httpRequest.getParameter("StepId");
            String comments = httpRequest.getParameter("Comments");

            Map<String, String> svcResult = admRoleAuthService.approveOrReject((String)Sanitizer.escapeHTML(id), stepId, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName(), comments, true);
            Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
            if ( entry.getKey().equalsIgnoreCase("0") ) {
                response.setValidated(true);
                response.setPkey(entry.getValue());
            } else {
                throw new Exception(entry.getKey());
            }
        } catch (Exception e) {
            log.error("send", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());

            response.setValidated(false);
            response.setErrorMessages(errors);
        }
        JsonResponse sresponse = new JsonResponse();
        Sanitizer.escape4Class(response, sresponse);
        return sresponse;
    } 

    /**
     * 退回
     *
     * @param id                案號
     * @param httpRequest       http request 
     * @param httpResponse      http response
     * @return                  JSON response
     */
    @PostMapping(value={"/Reject/{id}"})
    @ResponseBody
    @Authorize(userInRoleCanApprove = true)
    public JsonResponse reject(@PathVariable String id, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        id = (String)Sanitizer.logForgingStr(id);
        JsonResponse response = new JsonResponse();
        try {
            String stepId = httpRequest.getParameter("StepId");
            String comments = httpRequest.getParameter("Comments");

            Map<String, String> svcResult = admRoleAuthService.approveOrReject((String)Sanitizer.escapeHTML(id), stepId, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName(), comments, false);
            Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
            if ( entry.getKey().equalsIgnoreCase("0") ) {
                response.setValidated(true);
                response.setPkey(entry.getValue());
            } else {
                throw new Exception(entry.getKey());
            }
        } catch (Exception e) {
            log.error("send", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());

            response.setValidated(false);
            response.setErrorMessages(errors);
        }
        JsonResponse sresponse = new JsonResponse();
        Sanitizer.escape4Class(response, sresponse);
        return sresponse;
    }

    /**
     * 經辦重送案件
     * 
     * @param id            案號
     * @param ADROLENO      角色代碼
     * @param roleAuths     角色權限
     * @param comments      意見
     * @return              JSON Response
     */
    @PostMapping(value="/ReSend", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse resend(@RequestParam String id, @RequestParam String ADROLENO,  @RequestParam String ADSTAFFNO, @RequestParam String roleAuths, @RequestParam String comments){
        JsonResponse response = new JsonResponse();
        try {
            Map<String, String> svcResult = admRoleAuthService.resend(id, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName(),
                comments, ADROLENO, ADSTAFFNO, roleAuths);
            Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
            if ( entry.getKey().equalsIgnoreCase("0") ) {
                response.setValidated(true);
                response.setPkey(entry.getValue());
            } else {
                throw new Exception(entry.getKey());
            }
        } catch (Exception e) {
            log.error("resend", e);
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑 4:
            errors.put("summary", "發生錯誤，請確認資料是否有異動成功");
            response.setValidated(false);
            response.setErrorMessages(errors);
        }
        //20191115-Danny-解決Reflected XSS All Clients\路徑 4
        JsonResponse sresponse = new JsonResponse();
        Sanitizer.escape4Class(response, sresponse);

        return sresponse;
    } 

    /**
     * 經辦取消案件
     * 
     * @param id        案號
     * @return          JSON Response
     */
    @PostMapping(value="/Cancel", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Authorize(userInRoleCanEdit = true)
    public JsonResponse cancel(@RequestParam String id){
        JsonResponse response = new JsonResponse();
        try {
            Map<String, String> svcResult = admRoleAuthService.cancel(id, portalSSOService.getLoginBranch(), portalSSOService.getLoginUserId(), portalSSOService.getLoginUserName());
            Map.Entry<String, String> entry = svcResult.entrySet().iterator().next();
            if ( entry.getKey().equalsIgnoreCase("0") ) {
                response.setValidated(true);
                response.setPkey(entry.getValue());
            } else {
                throw new Exception(entry.getKey());
            }
        } catch (Exception e) {
            log.error("cancel", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());

            response.setValidated(false);
            response.setErrorMessages(errors);
        }
        JsonResponse sresponse = new JsonResponse();
        Sanitizer.escape4Class(response, sresponse);
        return sresponse;
    } 
}
