package com.netbank.rest.web.back.model;

/**
 * <p>B703GPBEResult class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class B703GPBEResult {
    private String seqno;
    private String adopGroup;
    private String adopGroupName;
    private String qtyType;
    private String q0t;
    private String q0f;
    private String q1t;
    private String q1f;
    private String q2t;
    private String q2f;
    private String q3t;
    private String q3f;
    private String q9t;
    private String q9f;
    private String q9tf;

    /**
     * <p>Getter for the field <code>seqno</code>.</p>
     *
     * @return String return the seqno
     */
    public String getSeqno() {
        return seqno;
    }

    /**
     * <p>Setter for the field <code>seqno</code>.</p>
     *
     * @param seqno the seqno to set
     */
    public void setSeqno(String seqno) {
        this.seqno = seqno;
    }

    /**
     * <p>Getter for the field <code>q0t</code>.</p>
     *
     * @return String return the q0t
     */
    public String getQ0t() {
        return q0t;
    }

    /**
     * <p>Setter for the field <code>q0t</code>.</p>
     *
     * @param q0t the q0t to set
     */
    public void setQ0t(String q0t) {
        this.q0t = q0t;
    }

    /**
     * <p>Getter for the field <code>q0f</code>.</p>
     *
     * @return String return the q0f
     */
    public String getQ0f() {
        return q0f;
    }

    /**
     * <p>Setter for the field <code>q0f</code>.</p>
     *
     * @param q0f the q0f to set
     */
    public void setQ0f(String q0f) {
        this.q0f = q0f;
    }

    /**
     * <p>Getter for the field <code>q1t</code>.</p>
     *
     * @return String return the q1t
     */
    public String getQ1t() {
        return q1t;
    }

    /**
     * <p>Setter for the field <code>q1t</code>.</p>
     *
     * @param q1t the q1t to set
     */
    public void setQ1t(String q1t) {
        this.q1t = q1t;
    }

    /**
     * <p>Getter for the field <code>q1f</code>.</p>
     *
     * @return String return the q1f
     */
    public String getQ1f() {
        return q1f;
    }

    /**
     * <p>Setter for the field <code>q1f</code>.</p>
     *
     * @param q1f the q1f to set
     */
    public void setQ1f(String q1f) {
        this.q1f = q1f;
    }

    /**
     * <p>Getter for the field <code>q2t</code>.</p>
     *
     * @return String return the q2t
     */
    public String getQ2t() {
        return q2t;
    }

    /**
     * <p>Setter for the field <code>q2t</code>.</p>
     *
     * @param q2t the q2t to set
     */
    public void setQ2t(String q2t) {
        this.q2t = q2t;
    }

    /**
     * <p>Getter for the field <code>q2f</code>.</p>
     *
     * @return String return the q2f
     */
    public String getQ2f() {
        return q2f;
    }

    /**
     * <p>Setter for the field <code>q2f</code>.</p>
     *
     * @param q2f the q2f to set
     */
    public void setQ2f(String q2f) {
        this.q2f = q2f;
    }

    /**
     * <p>Getter for the field <code>q3t</code>.</p>
     *
     * @return String return the q3t
     */
    public String getQ3t() {
        return q3t;
    }

    /**
     * <p>Setter for the field <code>q3t</code>.</p>
     *
     * @param q3t the q3t to set
     */
    public void setQ3t(String q3t) {
        this.q3t = q3t;
    }

    /**
     * <p>Getter for the field <code>q3f</code>.</p>
     *
     * @return String return the q3f
     */
    public String getQ3f() {
        return q3f;
    }

    /**
     * <p>Setter for the field <code>q3f</code>.</p>
     *
     * @param q3f the q3f to set
     */
    public void setQ3f(String q3f) {
        this.q3f = q3f;
    }

    /**
     * <p>Getter for the field <code>q9t</code>.</p>
     *
     * @return String return the q9t
     */
    public String getQ9t() {
        return q9t;
    }

    /**
     * <p>Setter for the field <code>q9t</code>.</p>
     *
     * @param q9t the q9t to set
     */
    public void setQ9t(String q9t) {
        this.q9t = q9t;
    }

    /**
     * <p>Getter for the field <code>q9f</code>.</p>
     *
     * @return String return the q9f
     */
    public String getQ9f() {
        return q9f;
    }

    /**
     * <p>Setter for the field <code>q9f</code>.</p>
     *
     * @param q9f the q9f to set
     */
    public void setQ9f(String q9f) {
        this.q9f = q9f;
    }

    /**
     * <p>Getter for the field <code>q9tf</code>.</p>
     *
     * @return String return the q9tf
     */
    public String getQ9tf() {
        return q9tf;
    }

    /**
     * <p>Setter for the field <code>q9tf</code>.</p>
     *
     * @param q9tf the q9tf to set
     */
    public void setQ9tf(String q9tf) {
        this.q9tf = q9tf;
    }

    /**
     * <p>Getter for the field <code>adopGroup</code>.</p>
     *
     * @return String return the adopGroup
     */
    public String getAdopGroup() {
        return adopGroup;
    }

    /**
     * <p>Setter for the field <code>adopGroup</code>.</p>
     *
     * @param adopGroup the adopGroup to set
     */
    public void setAdopGroup(String adopGroup) {
        this.adopGroup = adopGroup;
    }

    /**
     * <p>Getter for the field <code>adopGroupName</code>.</p>
     *
     * @return String return the adopGroupName
     */
    public String getAdopGroupName() {
        return adopGroupName;
    }

    /**
     * <p>Setter for the field <code>adopGroupName</code>.</p>
     *
     * @param adopGroupName the adopGroupName to set
     */
    public void setAdopGroupName(String adopGroupName) {
        this.adopGroupName = adopGroupName;
    }

    /**
     * <p>Getter for the field <code>qtyType</code>.</p>
     *
     * @return String return the qtyType
     */
    public String getQtyType() {
        return qtyType;
    }

    /**
     * <p>Setter for the field <code>qtyType</code>.</p>
     *
     * @param qtyType the qtyType to set
     */
    public void setQtyType(String qtyType) {
        this.qtyType = qtyType;
    }

}
