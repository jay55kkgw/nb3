package com.netbank.rest.web.back.service;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.dao.AdmAppBranchDao;
import fstop.orm.dao.AdmSysCodeMDao;
import fstop.orm.dao.AdmSysCodedDao;
import fstop.orm.dao.AdmZipDataDao;
import fstop.orm.po.ADMAPPBRANCH;
import fstop.orm.po.ADMSYSCODED;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
import org.apache.tools.ant.util.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 分行/ATM/證券據點維護服務
 *
 * @author 簡哥
 * @version V1.0
 */
@Slf4j
@Service
public class ADMAPPBRANCHService extends BaseService {
    @Autowired
    private AdmAppBranchDao admAppBranchDao;

    @Autowired
    private AdmZipDataDao admZipDataDao;

    @Autowired
    private AdmSysCodedDao admSysCodedDao;
    /**
     * <p>getBhCounty.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getBhCounty() {
        Rows rows = admZipDataDao.findCity();
        List<String> city = new ArrayList<String>();
        for (Row row : rows.getRows()) {
            city.add(row.getValue("CITY"));
        }
        return city;
    }
    public List<ADMSYSCODED> getCounty() {
        return admSysCodedDao.getCounty("A01");
    }
    /**
     * 分頁查詢
     *
     * @param pageNo a int.
     * @param pageSize a int.
     * @param orderBy a {@link java.lang.String} object.
     * @param orderDir a {@link java.lang.String} object.
     * @param BHADTYPE a {@link java.lang.String} object.
     * @param BHNAME a {@link java.lang.String} object.
     * @param BHCOUNTY a {@link java.lang.String} object.
     * @return a {@link com.netbank.domain.orm.core.Page} object.
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String BHADTYPE, String BHNAME,
            String BHCOUNTY) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, BHADTYPE={}, BHNAME={}, BHCOUNTY={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(BHADTYPE), Sanitizer.logForgingStr(BHNAME), Sanitizer.logForgingStr(BHCOUNTY));

        log4Query("0", new String[][] { { "BHADTYPE", BHADTYPE }, { "BHNAME", BHNAME }, { "BHCOUNTY", BHCOUNTY } } );
        return admAppBranchDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, BHADTYPE, BHNAME, BHCOUNTY);
    }

    /**
     * 取得資料
     *
     * @param bhid 據點 ID
     * @return 據點 POJO
     */
    public ADMAPPBRANCH getByBHID(int bhid) {
        log.debug("getByABHID bhid={}", Sanitizer.logForgingStr(bhid));

        log4Query("0", new String[][] { { "bhid", Integer.toString(bhid) }} );
        return admAppBranchDao.findById(bhid);
    }

    /**
     * 新增據點資料
     *
     * @param branch  據點 POJO
     * @param creator 編輯者
     * @param creatorName 編輯者姓名
     */
    public void insertAppBranch(ADMAPPBRANCH branch, String creator,String creatorName) {

        String[] parts = null;
        parts = DateUtils.format(new Date(), "yyyyMMdd-HHmmss").split("-");
        branch.setBHTXDATE(parts[0]);
        branch.setBHTXTIME(parts[1]);
        branch.setBHUSERID(creator);
        branch.setBHUSERNAME(creatorName);
        branch.setBHPICDATA("");

        admAppBranchDao.save(branch);
        log4Create(branch, "0");
    }

    /**
     * 儲存據點資料
     *
     * @param branch 據點 POJO
     * @param editor 編輯者
     * @param editorName a {@link java.lang.String} object.
     */
    public void saveAppBranch(ADMAPPBRANCH branch, String editor,String editorName) {
        ADMAPPBRANCH oriBranch = admAppBranchDao.findById(branch.getBHID());
        admAppBranchDao.getEntityManager().detach(oriBranch);

        String[] parts = null;
        parts = DateUtils.format(new Date(), "yyyyMMdd-HHmmss").split("-");
        branch.setBHTXDATE(parts[0]);
        branch.setBHTXTIME(parts[1]);
        branch.setBHUSERID(editor);
        branch.setBHUSERNAME(editorName);
        branch.setBHPICDATA("");
        
        admAppBranchDao.update(branch);
        log4Update(oriBranch, branch, "0");
    }

    /**
     * 刪除據點資料
     *
     * @param bhid 據點ID
     */
    public void deleteAppBranch(int bhid) {
        ADMAPPBRANCH oriBranch = admAppBranchDao.findById(bhid);
        admAppBranchDao.removeById(bhid);
        log4Delete(oriBranch, "0");
    }
}
