package com.netbank.rest.web.back.model;

import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Range;

/**
 * 各業務營業時間維護 View Model
 *
 * @author Alison
 * @version V1.0
 */
public class B105ViewModel {

    @Id
    private String ADPK;

    @NotBlank(message = "外匯業務 起 HH,不能為空值")
    @Range(min = 0, max = 23, message = "外匯業務 起 HH,請輸入00~23的數值")
    private String ADFXSHH = "";
    @NotBlank(message = "外匯業務 起 MM,不能為空值")
    @Range(min = 0, max = 59, message = "外匯業務 起 MM,請輸入00~59的數值")
    private String ADFXSMM = "";
    @NotBlank(message = "外匯業務 起 SS,不能為空值")
    @Range(min = 0, max = 59, message = "外匯業務 起 SS,請輸入00~59的數值")
    private String ADFXSSS = "";
    @NotBlank(message = "外匯業務 迄 HH,不能為空值")
    @Range(min = 0, max = 23, message = "外匯業務 迄 HH,請輸入00~23的數值")
    private String ADFXEHH = "";
    @NotBlank(message = "外匯業務 迄 MM,不能為空值")
    @Range(min = 0, max = 59, message = "外匯業務 迄 MM,請輸入00~59的數值")
    private String ADFXEMM = "";
    @NotBlank(message = "外匯業務 迄 SS,不能為空值")
    @Range(min = 0, max = 59, message = "外匯業務 迄 SS,請輸入00~59的數值")
    private String ADFXESS = "";
    @NotBlank(message = "基金業務 起 HH,不能為空值")
    @Range(min = 0, max = 23, message = "基金業務 起 HH,請輸入00~23的數值")
    private String ADFDSHH = "";
    @NotBlank(message = "基金業務 起 MM,不能為空值")
    @Range(min = 0, max = 59, message = "基金業務 起 MM,請輸入00~59的數值")
    private String ADFDSMM = "";
    @NotBlank(message = "基金業務 迄 SS,不能為空值")
    @Range(min = 0, max = 23, message = "基金業務 迄 SS,請輸入00~59的數值")
    private String ADFDSSS = "";
    @NotBlank(message = "基金業務 迄 HH能為空值")
    @Range(min = 0, max = 59, message = "基金業務 迄 MM,請輸入00~23的數值")
    private String ADFDEHH = "";
    @NotBlank(message = "基金業務 迄 MM,不能為空值")
    @Range(min = 0, max = 59, message = "基金業務 迄 MM,請輸入00~59的數值")
    private String ADFDEMM = "";
    @NotBlank(message = "基金業務 迄 SS,不能為空值")
    @Range(min = 0, max = 59, message = "基金業務 迄 SS,請輸入00~59的數值")
    private String ADFDESS = "";
    @NotBlank(message = "黃金業務 起 HH,不能為空值")
    @Range(min = 0, max = 23, message = "基金業務 起 HH,請輸入00~23的數值")
    private String ADGDSHH = "";
    @NotBlank(message = "黃金業務 起 MM,不能為空值")
    @Range(min = 0, max = 59, message = "基金業務 起 MM,請輸入00~59的數值")
    private String ADGDSMM = "";
    @NotBlank(message = "黃金業務 迄 SS,不能為空值")
    @Range(min = 0, max = 23, message = "基金業務 迄 SS,請輸入00~59的數值")
    private String ADGDSSS = "";
    @NotBlank(message = "黃金業務 迄 HH能為空值")
    @Range(min = 0, max = 59, message = "基金業務 迄 MM,請輸入00~23的數值")
    private String ADGDEHH = "";
    @NotBlank(message = "黃金業務 迄 MM,不能為空值")
    @Range(min = 0, max = 59, message = "基金業務 迄 MM,請輸入00~59的數值")
    private String ADGDEMM = "";
    @NotBlank(message = "黃金業務 迄 SS,不能為空值")
    @Range(min = 0, max = 59, message = "基金業務 迄 SS,請輸入00~59的數值")
    private String ADGDESS = "";

    /**
     * <p>getADPK.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADPK() {
        return ADPK;
    }

    /**
     * <p>setADPK.</p>
     *
     * @param adpk a {@link java.lang.String} object.
     */
    public void setADPK(String adpk) {
        ADPK = adpk;
    }


    /**
     * <p>getADFXSHH.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFXSHH() {
        return ADFXSHH;
    }

    /**
     * <p>setADFXSHH.</p>
     *
     * @param aADFXSHH a {@link java.lang.String} object.
     */
    public void setADFXSHH(String aADFXSHH) {
        ADFXSHH = aADFXSHH;
    }


    /**
     * <p>getADFXSMM.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFXSMM() {
        return ADFXSMM;
    }

    /**
     * <p>setADFXSMM.</p>
     *
     * @param aADFXSMM a {@link java.lang.String} object.
     */
    public void setADFXSMM(String aADFXSMM) {
        ADFXSMM = aADFXSMM;
    }

    /**
     * <p>getADFXSSS.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFXSSS() {
        return ADFXSSS;
    }

    /**
     * <p>setADFXSSS.</p>
     *
     * @param aADFXSSS a {@link java.lang.String} object.
     */
    public void setADFXSSS(String aADFXSSS) {
        ADFXSSS = aADFXSSS;
    }

    /**
     * <p>getADFXEHH.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFXEHH() {
        return ADFXEHH;
    }

    /**
     * <p>setADFXEHH.</p>
     *
     * @param aADFXEHH a {@link java.lang.String} object.
     */
    public void setADFXEHH(String aADFXEHH) {
        ADFXEHH = aADFXEHH;
    }

    /**
     * <p>getADFXEMM.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFXEMM() {
        return ADFXEMM;
    }

    /**
     * <p>setADFXEMM.</p>
     *
     * @param aADFXEMM a {@link java.lang.String} object.
     */
    public void setADFXEMM(String aADFXEMM) {
        ADFXEMM = aADFXEMM;
    }

    /**
     * <p>getADFXESS.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFXESS() {
        return ADFXESS;
    }

    /**
     * <p>setADFXESS.</p>
     *
     * @param aADFXESS a {@link java.lang.String} object.
     */
    public void setADFXESS(String aADFXESS) {
        ADFXESS = aADFXESS;
    }

    /**
     * <p>getADFDSHH.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFDSHH() {
        return ADFDSHH;
    }

    /**
     * <p>setADFDSHH.</p>
     *
     * @param aADFDSHH a {@link java.lang.String} object.
     */
    public void setADFDSHH(String aADFDSHH) {
        ADFDSHH = aADFDSHH;
    }

    /**
     * <p>getADFDSMM.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFDSMM() {
        return ADFDSMM;
    }

    /**
     * <p>setADFDSMM.</p>
     *
     * @param aADFDSMM a {@link java.lang.String} object.
     */
    public void setADFDSMM(String aADFDSMM) {
        ADFDSMM = aADFDSMM;
    }

    /**
     * <p>getADFDSSS.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFDSSS() {
        return ADFDSSS;
    }

    /**
     * <p>setADFDSSS.</p>
     *
     * @param aADFDSSS a {@link java.lang.String} object.
     */
    public void setADFDSSS(String aADFDSSS) {
        ADFDSSS = aADFDSSS;
    }

    /**
     * <p>getADFDEHH.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFDEHH() {
        return ADFDEHH;
    }

    /**
     * <p>setADFDEHH.</p>
     *
     * @param aADFDEHH a {@link java.lang.String} object.
     */
    public void setADFDEHH(String aADFDEHH) {
        ADFDEHH = aADFDEHH;
    }

    /**
     * <p>getADFDEMM.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFDEMM() {
        return ADFDEMM;
    }

    /**
     * <p>setADFDEMM.</p>
     *
     * @param aADFDEMM a {@link java.lang.String} object.
     */
    public void setADFDEMM(String aADFDEMM) {
        ADFDEMM = aADFDEMM;
    }

    /**
     * <p>getADFDESS.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADFDESS() {
        return ADFDESS;
    }

    /**
     * <p>setADFDESS.</p>
     *
     * @param aADFDESS a {@link java.lang.String} object.
     */
    public void setADFDESS(String aADFDESS) {
        ADFDESS = aADFDESS;
    }
      /**
     * <p>getADGDSHH.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADGDSHH() {
        return ADGDSHH;
    }

    /**
     * <p>setADGDSHH.</p>
     *
     * @param aADFDSHH a {@link java.lang.String} object.
     */
    public void setADGDSHH(String aADGDSHH) {
        ADGDSHH = aADGDSHH;
    }

    /**
     * <p>getADGDSMM.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADGDSMM() {
        return ADGDSMM;
    }

    /**
     * <p>setADGDSMM.</p>
     *
     * @param aADFDSMM a {@link java.lang.String} object.
     */
    public void setADGDSMM(String aADGDSMM) {
        ADGDSMM = aADGDSMM;
    }

    /**
     * <p>getADGDSSS.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADGDSSS() {
        return ADGDSSS;
    }

    /**
     * <p>setADGDSSS.</p>
     *
     * @param aADGDSSS a {@link java.lang.String} object.
     */
    public void setADGDSSS(String aADGDSSS) {
        ADGDSSS = aADGDSSS;
    }

    /**
     * <p>getADGDEHH.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADGDEHH() {
        return ADGDEHH;
    }

    /**
     * <p>setADGDEHH.</p>
     *
     * @param aADGDEHH a {@link java.lang.String} object.
     */
    public void setADGDEHH(String aADGDEHH) {
        ADGDEHH = aADGDEHH;
    }

    /**
     * <p>getADGDEMM.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADGDEMM() {
        return ADGDEMM;
    }

    /**
     * <p>setADGDEMM.</p>
     *
     * @param aADGDEMM a {@link java.lang.String} object.
     */
    public void setADGDEMM(String aADGDEMM) {
        ADGDEMM = aADGDEMM;
    }

    /**
     * <p>getADGDESS.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getADGDESS() {
        return ADGDESS;
    }

    /**
     * <p>setADGDESS.</p>
     *
     * @param aADGDESS a {@link java.lang.String} object.
     */
    public void setADGDESS(String aADGDESS) {
        ADGDESS = aADGDESS;
    }
}
