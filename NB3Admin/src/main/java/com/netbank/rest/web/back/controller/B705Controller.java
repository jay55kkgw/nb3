package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.netbank.rest.web.back.model.B704Model;
import com.netbank.rest.web.back.model.B705Result;
import com.netbank.rest.web.back.service.NMBReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fstop.aop.Authorize;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>B705Controller class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B705")
public class B705Controller {
    @Autowired
    private NMBReportService _NMBReportService;

    /**
     * 取得一般網路銀行轉帳級距統計表首頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        B704Model entity = new B704Model();

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, -1);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        String todayYear = String.format("%04d", year);
        String todayMonth = String.format("%02d", month);

        entity.setYearS(todayYear);
        entity.setMonthS(todayMonth);

        List<B705Result> queryData = getResultList(entity);
        model.addAttribute("Data", queryData);

        SetDropDownList(model, entity);
        return "B705/index";
    }

    /**
     * 查詢資料
     *
     * @param result     SpringMVC Binding 檢核結果
     * @return Json 結果物件
     * @param entity a {@link com.netbank.rest.web.back.model.B704Model} object.
     * @param model a {@link org.springframework.ui.ModelMap} object.
     */
    @PostMapping(value = "/Query")
    @Authorize(userInRoleCanQuery = true)
    public String query(@ModelAttribute B704Model entity, BindingResult result, ModelMap model) {
        log.debug("entity={}", Sanitizer.logForgingStr(entity));
        // 錯誤處理
        if (result.hasErrors()) {
            // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
            Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

            model.addAttribute("Validated", false);
            model.addAttribute("ErrMsg", errors);
            return "B705/index";
        }

        List<B705Result> queryData = getResultList(entity);
        model.addAttribute("Data", queryData);
        SetDropDownList(model, entity);

        return "B705/index";
    }

    /**
     * 取得資料
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @param entity a {@link com.netbank.rest.web.back.model.B704Model} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException if any.
     */
    @PostMapping(value = { "/DownloadtoCSV" })
    @Authorize(userInRoleCanQuery = true)
    public void DownloadtoCSV(@ModelAttribute B704Model entity, ModelMap model, HttpServletResponse response)
            throws IOException {

        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"ExportSys.csv\"");
        OutputStream resOs = response.getOutputStream();
        resOs.write('\ufeef'); // emits 0xef
        resOs.write('\ufebb'); // emits 0xbb
        resOs.write('\ufebf'); // emits 0xbf

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = new CSVWriter(outputWriter);

        List<B705Result> queryData = getResultList(entity);

        String[] headers = { "交易功能\\金額級距", "", "10萬(含)以下", "10-20萬(含)", "20-30萬(含)", "30-40萬(含)", "40-50萬(含)",
                "50-60萬(含)", "60-70萬(含)", "70-80萬(含)", "80-90萬(含)", "90-100萬(含)", "100-150萬(含)", "150-200萬(含)",
                "200-250萬(含)", "250-300萬(含)" };
        writer.writeNext(headers);
        for (B705Result item : queryData) {
            String adagreef = "";
            if (item.getAdagreef().equals("-1"))
                adagreef = "小計";
            else if (item.getAdagreef().equals("0"))
                adagreef = "非約轉";
            else
                adagreef = "約轉";

            String[] nextLine = { item.getAdopname(), adagreef, item.getR00(), item.getR01(), item.getR02(),
                    item.getR03(), item.getR04(), item.getR05(), item.getR06(), item.getR07(), item.getR08(),
                    item.getR09(), item.getR10(), item.getR11(), item.getR12(), item.getR13() };
            writer.writeNext(nextLine);
        }
        writer.close();
    }

    /*
     * 取回查詢資料
     */
    private List<B705Result> getResultList(B704Model entity) {

        Rows qResult = new Rows();
        try {
            qResult = _NMBReportService.queryB705(entity);
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());
        }

        List<B705Result> queryData = new ArrayList<>();
        B705Result qItem;
        int rowCount = qResult.getSize();
        for (int i = 0; i < rowCount; i++) {
            qItem = new B705Result();
            Row r = null;
            r = qResult.getRow(i);
            qItem.setAdopid(r.getValue("ADOPID"));
            qItem.setAdopname(r.getValue("ADOPNAME"));
            qItem.setAdagreef(r.getValue("ADAGREEF"));

            qItem.setR00(r.getValue("R00"));
            qItem.setR01(r.getValue("R01"));
            qItem.setR02(r.getValue("R02"));
            qItem.setR03(r.getValue("R03"));
            qItem.setR04(r.getValue("R04"));
            qItem.setR05(r.getValue("R05"));
            qItem.setR06(r.getValue("R06"));
            qItem.setR07(r.getValue("R07"));
            qItem.setR08(r.getValue("R08"));
            qItem.setR09(r.getValue("R09"));
            qItem.setR10(r.getValue("R10"));
            qItem.setR11(r.getValue("R11"));
            qItem.setR12(r.getValue("R12"));
            qItem.setR13(r.getValue("R13"));

            queryData.add(qItem);
        }

        return queryData;
    }

    private void SetDropDownList(ModelMap model, B704Model entity) {
        model.addAttribute("B704Model", entity);

        List<String> years = new ArrayList<String>();
        for (int i = 2019; i <= 2119; i++) {
            years.add(Integer.toString(i));
        }

        List<String> months = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            months.add(String.format("%02d", i));
        }

        model.addAttribute("years", years);
        model.addAttribute("months", months);
        return;
    }

}
