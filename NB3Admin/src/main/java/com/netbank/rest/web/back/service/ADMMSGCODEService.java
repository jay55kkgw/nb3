package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/* 
    2019/06/03  DannyChou   ADD     (三)  應用系統代碼維護
*/
/**
 * <p>ADMMSGCODEService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class ADMMSGCODEService extends BaseService {
    @Autowired
    private AdmMsgCodeDao admMsgCodeDao;

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo   第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy  依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param ADMCODE  應用系統代碼
     * @return 分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String ADMCODE, String ADMSGOUT) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, ADMCODE={}", Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy),
        Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ADMCODE), Sanitizer.logForgingStr(ADMSGOUT));

        log4Query("0", new String[][] { { "ADMCODE", ADMCODE }} );
        return admMsgCodeDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, ADMCODE ,ADMSGOUT);
    }

    /**
     * 取得 ADMMSGCODE List 資料
     *
     * @param admCode 應用系統代碼
     * @return List<ADMMSGCODE> List POJO
     */
    public List<ADMMSGCODE> getList(String admCode) {
        log.debug("getList admCode={}", admCode);
        List<ADMMSGCODE> listCodes;
        if (admCode.isEmpty())
            listCodes = admMsgCodeDao.findAll();
        else
            listCodes = admMsgCodeDao.findMsgCodeLike(admCode);
        return listCodes;
    }

    /**
     * 取得 ADMMSGCODE 資料
     *
     * @param admCode 應用系統代碼
     * @return ADMMSGCODE POJO
     */
    public ADMMSGCODE getAdmMsgCode(String admCode) {
        log.debug("getADMMSGCODE admCode={}", Sanitizer.logForgingStr(admCode));

        return admMsgCodeDao.findById(admCode);
    }

    /**
     * 新增資料
     *
     * @param creator 編輯者
     * @param AdmMsgCode a {@link fstop.orm.po.ADMMSGCODE} object.
     */
    public void insertAdmMsgCode(ADMMSGCODE AdmMsgCode, String creator) {
        AdmMsgCode.setLASTUSER(creator);

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        AdmMsgCode.setLASTDATE(parts[0]);
        AdmMsgCode.setLASTTIME(parts[1]);

        admMsgCodeDao.save(AdmMsgCode);
        log4Create(AdmMsgCode, "0");
    }

    /**
     * 儲存資料
     *
     * @param AdmMsgCode POJO
     * @param editor     編輯者
     */
    public void saveAdmMsgCode(ADMMSGCODE AdmMsgCode, String editor) {
        ADMMSGCODE oriADMMSGCODE = admMsgCodeDao.findById(AdmMsgCode.getADMCODE());
        admMsgCodeDao.getEntityManager().detach(oriADMMSGCODE);

        AdmMsgCode.setLASTUSER(editor);

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        AdmMsgCode.setLASTDATE(parts[0]);
        AdmMsgCode.setLASTTIME(parts[1]);

        admMsgCodeDao.update(AdmMsgCode);
        log4Update(oriADMMSGCODE, AdmMsgCode, "0");
    }

    /**
     * 刪除資料
     *
     * @param admCode ID
     */
    public void deleteAdmMsgCode(String admCode) {
        ADMMSGCODE oriADMMSGCODE = admMsgCodeDao.findById(admCode);
        admMsgCodeDao.removeById(admCode);
        log4Delete(oriADMMSGCODE, "0");
    }

    /**
     * 整批上傳檔
     *
     * @param actor    動作者
     * @param admMsgCodeList a {@link java.util.List} object.
     */
    public void uploadAdmMsgCode(List<ADMMSGCODE> admMsgCodeList, String actor) {
        for (ADMMSGCODE item : admMsgCodeList) {
            String admCode = item.getADMCODE();
            if (!admCode.isEmpty()) {
                if (admMsgCodeDao.findById(admCode) == null) {
                    ADMMSGCODE entity = new ADMMSGCODE();
                    entity.setADMCODE(item.getADMCODE()); // 訊息代碼
                    entity.setADMRESEND(item.getADMRESEND()); // 人工重送
                    entity.setADMEXCE(item.getADMEXCE()); // 異常事件通知
                    entity.setADMRESENDFX(item.getADMRESENDFX()); // 外幣交易人工重送
                    entity.setADMAUTOSEND(item.getADMAUTOSEND()); // 台幣交易自動重送
                    entity.setADMAUTOSENDFX(item.getADMAUTOSENDFX()); // 外幣交易自動重送
                    entity.setADMSGIN(item.getADMSGIN()); // 訊自說明
                    entity.setADMSGOUT(item.getADMSGOUT()); // 客戶訊息內容
                    entity.setADMSGOUTCHS(item.getADMSGOUTCHS()); // 客戶訊息內容-簡中
                    entity.setADMSGOUTENG(item.getADMSGOUTENG()); // 客戶訊息內容-英文
                    insertAdmMsgCode(entity, actor);
                } else {
                    saveAdmMsgCode(item, actor);
                }
            }
        }
    }

    public List<ADMMSGCODE> findAll() {
        return admMsgCodeDao.findAll();
    }

    public List<ADMMSGCODE> findTwdManulResend() {
        return admMsgCodeDao.find("FROM ADMMSGCODE WHERE ADMRESEND='Y'");
    }

    public List<ADMMSGCODE> findFxManulResend() {
        return admMsgCodeDao.find("FROM ADMMSGCODE WHERE ADMRESENDFX='Y'");
    }
    
    public List<ADMMSGCODE> findFxAllowTerminate(String[] pendingArr) {
        return admMsgCodeDao.findfindFxAllowTerminate(pendingArr);
    }
    
    /**
     * 取得失敗結果訊息
     * 客戶訊息
     * @param id 代號
     * @return a {@link java.lang.String} object.
     */
    public String getMsg(String id) {
        return admMsgCodeDao.getErrorCodeMsg(id);
    }

     /**
     * 取得失敗結果訊息
     * 訊息說明
     * @param id 代號
     * @return a {@link java.lang.String} object.
     */
    public String getMsgIn(String id) {
        return admMsgCodeDao.getErrorCodeMsgIn(id);
    }
}
