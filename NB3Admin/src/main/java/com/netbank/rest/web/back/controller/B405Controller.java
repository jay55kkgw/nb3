package com.netbank.rest.web.back.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.service.ADMMSGCODEService;
import com.netbank.rest.web.back.service.TXNFXRECORDService;
import com.netbank.rest.web.back.service.TXNFXSCHPAYDATAService;
import com.netbank.rest.web.back.service.TXNFXSCHPAYService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;
import com.netbank.util.CodeUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMMSGCODE;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNTWRECORD;
import fstop.util.Sanitizer;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 
 * @author jinhanhuang
 */
@Controller
@Slf4j
@RequestMapping("/B405")
public class B405Controller {
	
	private static final String[] pendingArr= {"0202", "0203", "0301", "0302", "0901", "0902", "0903", "0906", "0907", "3303", "W109", "Z300", "E006", "E009", "E038", "Z200", "M301", "M310", "M311", "M350", "M422", "M423", "M427", "R999", "Z034", "Z033", "Z032", "Z031", "Z030", "Z029", "Z028", "Z027", "Z026", "Z025", "W052", "W053", "W054", "W056", "W059", "W063", "W064", "W067", "W076", "W077", "W097", "W098", "Z024", "Z023", "W103", "W104", "W112", "W700", "W701", "W702", "W703", "W704", "W705", "W706", "W707", "W999", "Z001", "Z002", "Z003", "Z004", "Z005", "Z006", "Z007", "Z008", "Z009","Z010", "Z011", "Z012", "Z016", "Z019", "Z020", "W153", "W154", "W155", "M302", "W163", "W164", "W901", "W162", "W708"};    
	
    @Autowired
    private TXNFXSCHPAYDATAService txnFXSchPayDataService;

    @Autowired
    private ADMMSGCODEService admMsgCodeService;
    
    @Autowired
    private TXNFXRECORDService  txnFxrecordService;
    
    /**
     * 取得查詢頁
     * 
     * @param model
     * @return
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
    	// 台幣可人工重送訊息代碼
        List<ADMMSGCODE> txs = admMsgCodeService.findFxAllowTerminate(pendingArr);
        
        model.addAttribute("txs", txs);
        
        return "B405/index";
    }
    
        /**
     * 分頁查詢--外幣
     * 
     * @param USERID   身分證字號/統一編號
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value = "/IndexQueryFC", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody List<TXNFXRECORD> indexWQueryFC(@RequestParam String FXUSERID, HttpServletRequest request, HttpServletResponse response) {
        log.debug("query FXUSERID={}", Sanitizer.logForgingStr(FXUSERID));

        List<TXNFXRECORD> txnFxRecordDatas = txnFxrecordService.findFxAllowTerminate((String) Sanitizer.escapeHTML(FXUSERID), pendingArr);

        List<TXNFXRECORD> stxnFxRecordDatas = new ArrayList<TXNFXRECORD>();
        for (TXNFXRECORD txnFxRecordData : txnFxRecordDatas) {
        	TXNFXRECORD stxnFxRecordData = new TXNFXRECORD();
        	String FXTITAINFO = txnFxRecordData.getFXTITAINFO();
        	if(!"".equals(FXTITAINFO)) {
        		Gson gson = new Gson();
        		Map map = gson.fromJson(FXTITAINFO, Map.class);
        		
        		String STAN = (String) CodeUtil.fromJson(FXTITAINFO, Map.class).get("STAN");
        		txnFxRecordData.setSTAN(STAN);
        	}
            Sanitizer.escape4Class(txnFxRecordData, stxnFxRecordData);
            stxnFxRecordDatas.add(stxnFxRecordData);
        }
        List<TXNFXRECORD> resultstxnFxSchPayDatas = new ArrayList<TXNFXRECORD>();
        Sanitizer.escape4Class(stxnFxRecordDatas, resultstxnFxSchPayDatas);

        return resultstxnFxSchPayDatas;
    }
    
    
    /**
     * 外匯交易失敗終止處理
     * 
     * @param ADTXNO
     * @return
     */
    @PostMapping(value = "/Terminate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody Map<String,String> Terminate(@RequestParam String ADTXNO) {
    	Map<String,String> result  = new HashMap<String,String>();
    	try {
    		result = txnFxrecordService.Terminate(ADTXNO);
    	}catch(Exception e) {
    		log.error("e>>>{}",e);
    	}
    	return result;
    }
    
    
    /**
     * 外匯交易失敗沖回處理
     * 
     * @param ADTXNO
     * @return
     */
    @PostMapping(value = "/Reversal", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody Map<String,String> Reversal(@RequestParam String ADTXNO) {
    	Map<String,String> result  = new HashMap<String,String>();
    	try {
    		result = txnFxrecordService.Reversal(ADTXNO);
    	}catch(Exception e) {
    		log.error("e>>>{}",e);
    	}
    	return result;
    }
    
    /**
     * 重覆交易檢核
     * @param dpschno
     * @param dpschtxdate
     * @param dpuserid
     * @param adtxno
     * @return
     */
    @PostMapping(value = "/DuplicateCheck", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody Map duplicateCheck(@RequestParam String fxschno, @RequestParam String fxschtxdate, @RequestParam String fxuserid) {
        //20191115-Danny-Reflected XSS All Clients\路徑 25:
        fxschno = (String)Sanitizer.escapeHTML(fxschno);
        fxschtxdate = (String)Sanitizer.escapeHTML(fxschtxdate);
        fxuserid = (String)Sanitizer.escapeHTML(fxuserid);
        
        Map<String, Integer> response = new HashMap<String, Integer>();
        try {
            
        	List<TXNFXRECORD> txns=txnFXSchPayDataService.getDuplicateTxnCounts((String) Sanitizer.escapeHTML(fxschno), (String) Sanitizer.escapeHTML(fxschtxdate)
                                                , (String) Sanitizer.escapeHTML(fxuserid));
            response.put("counts", txns.size());
            if ( txns.size()>0 ) {
            	response.put("time", Integer.parseInt(txns.get(0).getFXTXTIME()));
            }
        } catch (Exception e) {
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤，請再試一次!!";
            log.error("Query error", e);
            response.put("counts", -1);
            return response;
        }
        
        return response;
    }
        
    /**
     * <p>
     * statusDialog.
     * </p>
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @param id    a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value = "/MsgDialog/{id}")
    public String msgDialog(@PathVariable String id, ModelMap model) {
        String MsgOut = "訊息說明 : " + admMsgCodeService.getMsgIn(id);
        String MsgIn = "客戶訊息 : " + admMsgCodeService.getMsg(id);
        model.addAttribute("ErrMsg", MsgOut + "<br>" + MsgIn);
        return "B205/msgPartial";
    }
}

