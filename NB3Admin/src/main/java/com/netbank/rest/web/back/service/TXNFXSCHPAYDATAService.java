package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.BatchResult;
import com.netbank.rest.web.back.util.RESTfulClientUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNFXSCHPAYDATAIDENTITY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 預約交易結果查詢(外幣)
 * 
 * @author Alison
 */
@Slf4j
@Service
public class TXNFXSCHPAYDATAService extends BaseService {
    @Autowired
    private TxnFxSchPayDataDao txnFxSchPayDataDao;

    @Autowired
    private TxnFxRecordDao txnFxRecordDao;

    @Value("${FXRESEND.URL}")
    private String resendURL; 

    /**
     * 分頁查詢
     * 
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param orderDir
     * @param STARTDATE
     * @param ENDDATE
     * @param USERID
     * @param STATUS
     * @return
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String STARTDATE, String ENDDATE,
            String USERID, String STATUS) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, STARTDATE={}, ENDDATE={}, USERID={}, STATUS={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(STARTDATE), Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID), Sanitizer.logForgingStr(STATUS));

        log4Query("0", new String[][] { { "STARTDATE", STARTDATE }, { "ENDDATE", ENDDATE }, { "USERID", USERID }, { "STATUS", STATUS }} );
        return txnFxSchPayDataDao.findPageData(pageNo, pageSize, orderBy, orderDir, STARTDATE, ENDDATE, USERID, STATUS);
    }
    
    /**
     * 預約交易結果查詢
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param orderDir
     * @param STARTDATE
     * @param ENDDATE
     * @param USERID
     * @param STATUS
     * @return
     */
    public Page getByQuery4B205(int pageNo, int pageSize, String orderBy, String orderDir, String STARTDATE, String ENDDATE,
            String USERID, String STATUS) {
        //避免orderby的key包含了pk，要去掉放入sql
        if(orderBy.contains(".")){
            String[] splitstr = orderBy.split("\\.");
            orderBy = splitstr[splitstr.length-1];
        }
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, STARTDATE={}, ENDDATE={}, USERID={}, STATUS={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(STARTDATE), Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID), Sanitizer.logForgingStr(STATUS));

        log4Query("0", new String[][] { { "STARTDATE", STARTDATE }, { "ENDDATE", ENDDATE }, { "USERID", USERID }, { "STATUS", STATUS }} );
        return txnFxSchPayDataDao.findPageData4B205(pageNo, pageSize, orderBy, orderDir, STARTDATE, ENDDATE, USERID, STATUS);
    }

    /**
     * 人工重作
     * @param dpschno
     * @param dpschtxdate
     * @return
     */
    public String reSend(String fxschno, String fxschtxdate, String fxuserid) {
        TXNFXSCHPAYDATAIDENTITY id = new TXNFXSCHPAYDATAIDENTITY();
        id.setFXSCHNO(fxschno);
        id.setFXSCHTXDATE(fxschtxdate);
        id.setFXUSERID(fxuserid);
        TXNFXSCHPAYDATA data = txnFxSchPayDataDao.findById(id);
        if ( data.getFXTXSTATUS().compareTo("1") == 0 || data.getFXTXSTATUS().compareTo("2") == 0 ) {
            String url = resendURL;
            String tmp = data.getMSADDR();
            if ( tmp.compareToIgnoreCase("ms_fx") == 0 ) {
                url = url.replace("{0}", "msfx");
                url = url.replace("{1}", "ms_fx");
            } 

            Integer toMsTimeout = 120;
            try {
                List<String> inParam = new ArrayList<String>();
                
                inParam.add("executeone");
                inParam.add(fxschno);
                inParam.add(fxschtxdate);
                //ID FXUSERID
                inParam.add(fxuserid);
                inParam.add("AUTO");        // 人工重送

                // {
                //     "batchName":
                //     "isSuccess":
                //     "errorCode":
                // }
                //
                BatchResult result = RESTfulClientUtil.sendBatch(inParam, url, toMsTimeout);
                if ( result != null && result.isSuccess()) {
                    return "";
                } else {
                    return "失敗";
                }
            } catch (Exception e) {
                log.error("reSend Error", e);
                
                return e.getMessage();
            }
        } else {
            String msg = "預約批號["+fxschno+"]-預約轉帳日["+fxschtxdate+"], 交易執行狀態不為失敗，不可人工重做";
            return msg;
        }
    }
    
    /**
     * 取得同ID/交易代號/轉出/轉入(帳號+幣別)/金額的交易
     * @param fxschno
     * @param fxschtxdate
     * @param fxuserid
     * @return
     */
    public List<TXNFXRECORD> getDuplicateTxnCounts(String fxschno, String fxschtxdate, String fxuserid) {
        TXNFXSCHPAYDATAIDENTITY id = new TXNFXSCHPAYDATAIDENTITY();
        id.setFXSCHNO(fxschno);
        id.setFXSCHTXDATE(fxschtxdate);
        id.setFXUSERID(fxuserid);
        TXNFXSCHPAYDATA data = txnFxSchPayDataDao.findById(id);
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String fxTxDate = dateFormat.format(date);
        
        // 換匯/轉帳/結購/結售 只有一邊金額有值
        String fxAmt = data.getFXWDAMT().equals("") ? data.getFXSVAMT() : data.getFXWDAMT();
        
        return txnFxRecordDao.getDuplicateTxn(data.getADOPID(), data.getFXSCHPAYDATAIDENTITY().getFXUSERID(), fxTxDate, data.getFXWDAC(), data.getFXSVAC(), data.getFXWDCURR(), data.getFXSVCURR(), fxAmt);
    }

    /**
     * 
     * @param fxMsgContent
     * @return
     */
    public List<TXNFXRECORD> getTXNFXRECORDs(String fxMsgContent) {
		return txnFxRecordDao.getTXNFXRECORDs(fxMsgContent);
	}

    /**
     * 查詢
     * 
     * @param STARTDATE
     * @param ENDDATE
     * @param USERID
     * @param STATUS
     * @return
     */
    public List<TXNFXSCHPAYDATA> find(String STARTDATE, String ENDDATE,String USERID) {
        log.debug("find STARTDATE={}, ENDDATE={}, USERID={}, STATUS={}",
          Sanitizer.logForgingStr(STARTDATE), Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID));

        log4Query("0", new String[][] { { "STARTDATE", STARTDATE }, { "ENDDATE", ENDDATE }, { "USERID", USERID }} );
        return txnFxSchPayDataDao.findByParams(STARTDATE, ENDDATE, USERID);
    }

}