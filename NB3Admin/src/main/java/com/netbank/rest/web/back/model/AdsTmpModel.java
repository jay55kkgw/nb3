package com.netbank.rest.web.back.model;

import java.util.Date;

import fstop.orm.po.ADMADSTMP;

/**
 * 廣告管理 View Model
 *
 * @author 簡哥
 * @version V1.0
 */
public class AdsTmpModel extends ADMADSTMP {
    private static final long serialVersionUID = 1L;

    private String StartDateTime;
    private String EndDateTime;
    private String imgGuid;
    private String comments;
    private String fileGuid;
    
    // 前端頁面是字串, 後端用 blob 存, 存到 TARGETCONTENT
    private String targetContentStr;
    
    /**
     * <p>getStartDateTime.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public String getStartDateTime() {
        return StartDateTime;
    }

    /**
     * <p>setStartDateTime.</p>
     *
     * @param startDateTime a {@link java.util.Date} object.
     */
    public void setStartDateTime(String startDateTime) {
        StartDateTime = startDateTime;
    }

    /**
     * <p>getEndDateTime.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public String getEndDateTime() {
        return EndDateTime;
    }

    /**
     * <p>setEndDateTime.</p>
     *
     * @param endDateTime a {@link java.util.Date} object.
     */
    public void setEndDateTime(String endDateTime) {
        EndDateTime = endDateTime;
    }

    /**
     * <p>Getter for the field <code>imgGuid</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getImgGuid() {
        return imgGuid;
    }

    /**
     * <p>Setter for the field <code>imgGuid</code>.</p>
     *
     * @param imgGuid a {@link java.lang.String} object.
     */
    public void setImgGuid(String imgGuid) {
        this.imgGuid = imgGuid;
    }

    /**
     * <p>Getter for the field <code>comments</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getComments() {
        return comments;
    }

    /**
     * <p>Setter for the field <code>comments</code>.</p>
     *
     * @param comments a {@link java.lang.String} object.
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

	public String getFileGuid() {
		return fileGuid;
	}

	public void setFileGuid(String fileGuid) {
		this.fileGuid = fileGuid;
	}

	public String getTargetContentStr() {
		return targetContentStr;
	}

	public void setTargetContentStr(String targetContentStr) {
		this.targetContentStr = targetContentStr;
	}

    
}
