package com.netbank.rest.web.back.model;

public class B203TelRecord {
    /**
     * 行庫別
     */
    private String BANKCOD;

    /**
     * 帳號
     */
    private String TSFACN;
        
    /**
     * 帳號統編
     */
    private String CUSIDN;

    /**
     * 業務類號
     */
    private String BUSTYPE;
    
    /**
     * 性質別
     */
    private String ATRCOD;
                
    /**
     * 狀態
     */
    private String STSCOD;

    /**
     * 處理回應
     */
    private String RTNCOD;

    /**
     * 建檔分行
     */
    private String UPDBRH;

    /**
     * 最後異動日期
     */
    private String CHGDAT;

    /**
     * 最後異動時間
     */
    private String CHGTIM;

    public String getBANKCOD() {
        return BANKCOD;
    }

    public void setBANKCOD(String bANKCOD) {
        BANKCOD = bANKCOD;
    }

    public String getTSFACN() {
        return TSFACN;
    }

    public void setTSFACN(String tSFACN) {
        TSFACN = tSFACN;
    }

    public String getCUSIDN() {
        return CUSIDN;
    }

    public void setCUSIDN(String cUSIDN) {
        CUSIDN = cUSIDN;
    }

    public String getBUSTYPE() {
        return BUSTYPE;
    }

    public void setBUSTYPE(String bUSTYPE) {
        BUSTYPE = bUSTYPE;
    }

    public String getATRCOD() {
        return ATRCOD;
    }

    public void setATRCOD(String aTRCOD) {
        ATRCOD = aTRCOD;
    }

    public String getSTSCOD() {
        return STSCOD;
    }

    public void setSTSCOD(String sTSCOD) {
        STSCOD = sTSCOD;
    }

    public String getRTNCOD() {
        return RTNCOD;
    }

    public void setRTNCOD(String rTNCOD) {
        RTNCOD = rTNCOD;
    }

    public String getUPDBRH() {
        return UPDBRH;
    }

    public void setUPDBRH(String uPDBRH) {
        UPDBRH = uPDBRH;
    }

    public String getCHGDAT() {
        return CHGDAT;
    }

    public void setCHGDAT(String cHGDAT) {
        CHGDAT = cHGDAT;
    }

    public String getCHGTIM() {
        return CHGTIM;
    }

    public void setCHGTIM(String cHGTIM) {
        CHGTIM = cHGTIM;
    }

}