package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.netbank.domain.orm.core.Page;

import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmAppVerDao;
import fstop.orm.po.ADMAPPVER;
import fstop.orm.po.ADMBHCONTACT;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/* 
    2019/06/03  DannyChou   ADD     (三)  應用系統代碼維護
*/
/**
 * <p>
 * ADMAPPVERCODEService class.
 * </p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class ADMAPPVERService extends BaseService {
    @Autowired
    private AdmAppVerDao AdmAppVerDao;

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo   第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy  依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param APPID    應用系統代碼
     * @return 分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir,String jsonModel) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, APPID={},USERID={},TXDATE={},TXTIME={},jsonModel={}"
        , Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy),Sanitizer.logForgingStr(orderDir),Sanitizer.logForgingStr(jsonModel));

        log4Query("0", new String[][] { { "jsonModel", jsonModel } });
        return AdmAppVerDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir,jsonModel);
    }

    /**
     * 取得 ADMAPPVERCODE List 資料
     *
     * @param admCode 應用系統代碼
     * @return List<ADMAPPVERCODE> List POJO
     */
    public List<ADMAPPVER> getList() {
        log.debug("getList");
        List<ADMAPPVER> listCodes;
        listCodes = AdmAppVerDao.findAll();
        return listCodes;
    }

    /**
     * 取得 ADMAPPVERCODE 資料
     *
     * @param admCode 應用系統代碼
     * @return ADMAPPVERCODE POJO
     */
    public ADMAPPVER getADMAPPVER(Integer APPID) {
        log.debug("getADMAPPVER APPID={}", Sanitizer.logForgingStr(APPID));

        return AdmAppVerDao.findById(APPID);
    }

    /**
     * 新增資料
     *
     * @param creator   編輯者
     * @param ADMAPPVER a {@link fstop.orm.po.ADMAPPVERCODE} object.
     */
    public void insertADMAPPVER(ADMAPPVER ADMAPPVER, String creator) {

        ADMAPPVER eAdmappver = new ADMAPPVER();

        eAdmappver.setUSERID(creator);
        eAdmappver.setUSERNAME(creator);

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");
        eAdmappver.setTXDATE(parts[0]);
        eAdmappver.setTXTIME(parts[1]);
        // VERDATE = 畫面 VERDATE
        eAdmappver.setVERDATE(ADMAPPVER.getVERDATE().replace("/", "").trim());
        // APPOS = 畫面APPOS
        eAdmappver.setAPPOS(ADMAPPVER.getAPPOS());
        // APPMODELNO = 畫面APPMODELNO
        eAdmappver.setAPPMODELNO(ADMAPPVER.getAPPMODELNO());
        // VERNO = 畫面 VERNO
        eAdmappver.setVERNO(ADMAPPVER.getVERNO());
        // VERStATUS = 畫面VERStATUS
        eAdmappver.setVERSTATUS(ADMAPPVER.getVERSTATUS());
        // VERMSG = 畫面VERMSG
        eAdmappver.setVERMSG(ADMAPPVER.getVERMSG());
        // VERURL = 畫面 VERURL
        eAdmappver.setVERURL(ADMAPPVER.getVERURL());

        AdmAppVerDao.save(eAdmappver);
        log4Create(eAdmappver, "0");
    }

    /**
     * 儲存資料
     *
     * @param ADMAPPVERCODE POJO
     * @param editor        編輯者
     */
    public void saveADMAPPVER(ADMAPPVER ADMAPPVER, String editor) {
        ADMAPPVER oriADMAPPVER = AdmAppVerDao.findById(ADMAPPVER.getAPPID());
        AdmAppVerDao.getEntityManager().detach(oriADMAPPVER);
        
        ADMAPPVER.setUSERID(editor);
        ADMAPPVER.setUSERNAME(editor);
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");
        
        ADMAPPVER.setTXDATE(parts[0]);
        ADMAPPVER.setTXTIME(parts[1]);
        

        AdmAppVerDao.update(ADMAPPVER);
        log4Update(oriADMAPPVER, ADMAPPVER, "0");
    }

    /**
     * 刪除資料
     *
     * @param appid ID
     */
    public void deleteADMAPPVER(Integer appid) {
        ADMAPPVER oriADMAPPVER = AdmAppVerDao.findById(appid);
        AdmAppVerDao.removeById(appid);
        log4Delete(oriADMAPPVER, "0");
    }

    /**
     * 取得所有選單資料
     */
    public List<ADMAPPVER> findAll() {
        return AdmAppVerDao.findAll();
    }

    /**
     * 取得APP版本控制資料
     *
     * @param APPID 
     * @return APP版本控制資料  POJO
     */
    public ADMAPPVER getByID(Integer APPID) {
        log.debug("getByID APPID={}", Sanitizer.logForgingStr(APPID));

        log4Query("0", new String[][] { { "APPID", Integer.toString(APPID) } } );
        return AdmAppVerDao.findById(APPID);
    }
    
}
