package com.netbank.rest.web.back.service;

import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import com.google.gson.Gson;
import com.netbank.rest.web.back.model.AdsTmpModel;
import com.netbank.rest.web.back.model.FlowChkResult;
import com.netbank.rest.web.back.model.FlowSvcResult;

import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmAdsDao;
import fstop.orm.dao.AdmAdsTmpDao;
import fstop.orm.dao.AdmUploadTmpDao;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.po.ADMADS;
import fstop.orm.po.ADMADSTMP;
import fstop.orm.po.ADMANN;
import fstop.orm.po.ADMANNTMP;
import fstop.orm.po.ADMUPLOADTMP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 廣告管理服務
 *
 * @author 簡哥
 * @version V1.0
 */
@Slf4j
@Service
public class ADMADSService extends BaseService {
    @Autowired
    private FlowService flowService;

    @Autowired
    private AdmAdsDao admAdsDao;
    
    @Autowired
    private AdmAdsTmpDao admAdsTmpDao;

    @Autowired
    private AdmUploadTmpDao admUploadTmpDao;

    @Autowired
    private SysDailySeqDao sysDailySeqDao;

    // 要設定在 ADMFLOWDEF 資料表中
    private final String FLOW_ID="AdsFlow";

    // 取號的 APP ID
    private final String APP_ID = "ADS";

    // 編輯者 STEPID，參考 ADMFLOWDEFSTEP
    private final String[] EDITOR_STEPS = {"3"};     

    // 審核者/放行者 STEPID，參考 ADMFLOWDEFSTEP
    private final String[] REVIEWER_STEPS = {"2"};

    // 編輯者角色
    /** Constant <code>EDITOR_ROLES</code> */
    public static final String[] EDITOR_ROLES = {"EB"};;

    // 審核者角色
    /** Constant <code>REVIEWER_ROLES</code> */
    public static final String[] REVIEWER_ROLES = {"EC"};

    /**
     * 取得廣告管理資料
     *
     * @param oid a {@link java.lang.String} object.
     * @param dateFrom a {@link java.lang.String} object.
     * @param dateTo a {@link java.lang.String} object.
     * @param roles Roles 
     * @return a {@link java.util.List} object.
     */
    public List<ADMADSTMP> findAdsTmp(String oid, String dateFrom, String dateTo,Set<String> roles, String flowFinished) {
        String tmp = dateFrom.replace("/", "").replace(" ","").replace(":", "");
        String fromDate = tmp.length()>=8 ? tmp.substring(0,8) : "";

        tmp = dateTo.replace("/", "").replace(" ","").replace(":", "");
        String toDate = tmp.length()>=8 ? tmp.substring(0,8) : "";
        
        log4Query("0");

        return admAdsTmpDao.findByParams(oid, fromDate, toDate, roles, flowFinished);
    }

    /**
     * 依廣告 Id 查詢廣告資料
     *
     * @param id a {@link java.lang.String} object.
     * @return a {@link fstop.orm.po.ADMADSTMP} object.
     */
    public ADMADSTMP findById(String id) {
        return admAdsTmpDao.findById(id);
    }

    /**
     * 將上傳的 Banner 圖檔暫存在 DB TMP 檔
     *
     * @param id                暫存主鍵
     * @param type				類型(B|C)
     * @param fileName         	圖檔名
     * @param fileContent      	圖內容
     */
    public void saveImageTmp(String id, String type, String fileName, byte[] fileContent) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String createDate = format.format(Calendar.getInstance().getTime());

        ADMUPLOADTMP po = new ADMUPLOADTMP();
        po.setID(id+ "-" + type);
        po.setPURPOSE("B501");
        po.setFILENAME(fileName);
        po.setFILECONTENT(fileContent);
        po.setCREATEDATE(createDate);
        admUploadTmpDao.save(po);
    }
    
    public void updateImageTmp(String id, byte[] fileContent) {
    	ADMUPLOADTMP po = admUploadTmpDao.get(id);
    	po.setFILECONTENT(fileContent);
    	admUploadTmpDao.update(po);
    }

    /**
     * 取得圖檔
     *
     * @param id a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<ADMUPLOADTMP> getImageTmp(String id) {
        return admUploadTmpDao.findByIdLike(id);
    }

    /**
     * 經辦送件
     *
     * @param adsTmpModel   廣告 ViewModel
     * @param oid           單位代碼
     * @Param uid           使用者代碼
     * @param uName         使用者姓名
     * @return              結果
     * @param uid a {@link java.lang.String} object.
     */
    @Transactional
    public Map<String, String> sendAdsTmp(AdsTmpModel adsTmpModel, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            // 取案號, 案號＝ADS-xxxxxxxx
            String caseSN = APP_ID+"-"+ String.format("%06d", sysDailySeqDao.dailySeq(APP_ID));
            String todoURL = "B501/Query/"+caseSN;

            FlowSvcResult rt = flowService.startFlow(FLOW_ID, caseSN, oid, uid, uName, "", "", "廣告管理", todoURL);
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                ADMADSTMP po = new ADMADSTMP();
                // 設主鍵
                po.setID(caseSN);

                // 設定其它資料
                po.setOID(oid);
                po.setTYPE(adsTmpModel.getTYPE());
                po.setTITLE(adsTmpModel.getTITLE());
                po.setCONTENT(adsTmpModel.getCONTENT());

                // 轉換日期時間 yyyy/MM/dd HH:mm
                String tmpDate = adsTmpModel.getStartDateTime().replace("/", "").replace(":", "");
                String[] parts = tmpDate.split(" ");
                po.setSTARTDATE(parts[0]);
                po.setSTARTTIME(StringUtils.rightPad(parts[1], 6));

                tmpDate = adsTmpModel.getEndDateTime().replace("/", "").replace(":", "");
                parts = tmpDate.split(" ");	
                po.setENDDATE(parts[0]);
                po.setENDTIME(StringUtils.rightPad(parts[1], 6));

                // 設圖檔, 若有多個圖檔，Id 依序會被設定成 Id-L，Id-M，Id-S，小廣告Id-C
                List<ADMUPLOADTMP> imgTmps = getImageTmp(adsTmpModel.getImgGuid());
                for (ADMUPLOADTMP imgTmp : imgTmps) {
                	po.setFILENAMEL(imgTmp.getFILENAME());
                	po.setFILECONTENTL(imgTmp.getFILECONTENT());
                }

                po.setURL(adsTmpModel.getURL());
                po.setSORTORDER(adsTmpModel.getSORTORDER());

                // 設流程資訊
                po.setSTEPID(rt.getNextStepId());
                po.setSTEPNAME(rt.getNextStepName());
                po.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");

                // 轉換日期時間
                DateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss");
                parts = format.format(new Date()).split("-");
                po.setLASTUSER(uid);
                po.setLASTDATE(parts[0]);
                po.setLASTTIME(parts[1]);
                
                for ( ADMUPLOADTMP imgTmp : imgTmps ) {
                    admUploadTmpDao.delete(admUploadTmpDao.findById(imgTmp.getID()));
                }
                
                po.setTARGETTYPE(adsTmpModel.getTARGETTYPE());
                switch ( po.getTARGETTYPE().charAt(0) ) {
                case '1' :
                	// 連結
                	po.setURL(adsTmpModel.getURL());
                	break;
                case '2' :
                	// 檔案
                	List<ADMUPLOADTMP> fileTmps = getImageTmp(adsTmpModel.getFileGuid());
                	po.setTARGETFILENAME(fileTmps.get(0).getFILENAME());
                	po.setTARGETCONTENT(fileTmps.get(0).getFILECONTENT());
                	admUploadTmpDao.delete(admUploadTmpDao.findById(adsTmpModel.getFileGuid()));
                	break;
                case '3' :
                	// 內容, 共用 TARGETCONTENT 欄位, 所以轉 byte[]
                	po.setTARGETCONTENT(adsTmpModel.getTargetContentStr().getBytes(StandardCharsets.UTF_8));
                	break;
                }
                
                admAdsTmpDao.save(po);
                result.put("0", caseSN);
            } else {
                result.put(rt.getStatusCode(), "");
            }
            log4Send(caseSN, rt.getStatusCode());
        } catch (Exception e) {
            log.error("sendAdsTmp Error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "存檔發生錯誤，請確認資料是否有更新成功!!";
            result.put((String)Sanitizer.logForgingStr(message), "");
        }
        return result;
    }

    /**
     * 同意或是退回，流程往下一節點移動
     *
     * @param caseSN    案件編號
     * @param oid       單位代碼
     * @param uid       使用者 ID
     * @param uName     使用者姓名
     * @return          結果
     * @param stepId a {@link java.lang.String} object.
     * @param comments a {@link java.lang.String} object.
     * @param isApproved a boolean.
     */
    @Transactional
    public Map<String, String> approveOrRejectAdsTmp(String caseSN, String stepId, String oid, String uid, String uName, String comments, boolean isApproved) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            // 取案號, 案號＝ADS-xxxxxxxx
            FlowSvcResult rt = flowService.moveFlow(caseSN, stepId, oid, uid, uName, comments, "", isApproved);
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                ADMADSTMP admAdsTmp = admAdsTmpDao.findById(caseSN);
                if ( rt.isIsFinal() ) {
                    ADMADS admAds = admAdsDao.findById(caseSN);
                    boolean admAdsExist = true;
                    if ( admAds == null ) {
                        admAds = new ADMADS();
                        admAdsExist = false;
                    }
                    admAds.setID(admAdsTmp.getID());
                    admAds.setOID(admAdsTmp.getOID());
                    admAds.setTITLE(admAdsTmp.getTITLE());
                    admAds.setCONTENT(admAdsTmp.getCONTENT());
                    admAds.setTYPE(admAdsTmp.getTYPE());
                    admAds.setSTARTDATE(admAdsTmp.getSTARTDATE());
                    admAds.setSTARTTIME(admAdsTmp.getSTARTTIME());
                    admAds.setENDDATE(admAdsTmp.getENDDATE());
                    admAds.setENDTIME(admAdsTmp.getENDTIME());
                    admAds.setFILENAMEL(admAdsTmp.getFILENAMEL());
                    admAds.setFILECONTENTL(admAdsTmp.getFILECONTENTL());
                    admAds.setURL(admAdsTmp.getURL());
                    admAds.setSORTORDER(admAdsTmp.getSORTORDER());
                    admAds.setLASTUSER(uid);

                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
                    Date date = new Date();
                    String[] parts = dateFormat.format(date).split("-");

                    admAds.setLASTDATE(parts[0]);
                    admAds.setLASTTIME(parts[1]);

                    admAds.setTARGETTYPE(admAdsTmp.getTARGETTYPE());
                    admAds.setTARGETCONTENT(admAdsTmp.getTARGETCONTENT());
                    admAds.setTARGETFILENAME(admAdsTmp.getTARGETFILENAME());
                    
                    if ( admAdsExist ) {
                        admAdsDao.update(admAds);
                    } else {
                        admAdsDao.save(admAds);
                    }
                    //不要 LOG，因為圖檔太大了
                    //log4Create(admAds, "0");
                }
                admAdsTmp.setSTEPID(rt.isIsFinal() ? "" : rt.getNextStepId());
                admAdsTmp.setSTEPNAME(rt.isIsFinal() ? "" : rt.getNextStepName());
                admAdsTmp.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");
                
                admAdsTmpDao.save(admAdsTmp);
                result.put("0", caseSN);
            } else {
                result.put(rt.getStatusCode(), "");
            }
            if ( isApproved ) {
            	log4Approve(caseSN, rt.getStatusCode());
            } else {
            	log4Reject(caseSN, rt.getStatusCode());
            }
        } catch (Exception e) {
            log.error("approveOrRejectAdsTmp Error", e);
            result.put(e.getMessage(), "");
        }
        return result;
    }

    /**
     * 經辦重送
     *
     * @param adsTmpModel       廣告 ViewModel
     * @param oid               編輯者單位代碼
     * @param uid               編輯者代碼
     * @param uName             編輯者姓名
     * @return a {@link java.util.Map} object.
     */
    @Transactional
    public Map<String, String> resendAdsTmp(AdsTmpModel adsTmpModel, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            ADMADSTMP po = admAdsTmpDao.findById(adsTmpModel.getID());
            FlowSvcResult rt = null;

            if ( po.getFLOWFINISHED().equalsIgnoreCase("Y") ) {
                // 已結案，重送
                String todoURL = "B501/Query/"+adsTmpModel.getID();
                rt = flowService.startFlow(FLOW_ID, adsTmpModel.getID(), oid, uid, uName, "", "", "廣告管理", todoURL);
            } else {
                // 流程往下執行(退回後重送)
                rt = flowService.moveFlow(adsTmpModel.getID(), adsTmpModel.getSTEPID(), oid, uid, uName, adsTmpModel.getComments(), "", true);
            }
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                po.setTITLE(adsTmpModel.getTITLE());
                po.setCONTENT(adsTmpModel.getCONTENT());
                
                // 轉換日期時間 yyyy/MM/dd HH:mm
                String tmpDate = adsTmpModel.getStartDateTime().replace("/", "").replace(":", "");
                String[] parts = tmpDate.split(" ");
                po.setSTARTDATE(parts[0]);
                po.setSTARTTIME(StringUtils.rightPad(parts[1], 6));

                tmpDate = adsTmpModel.getEndDateTime().replace("/", "").replace(":", "");
                parts = tmpDate.split(" ");	
                po.setENDDATE(parts[0]);
                po.setENDTIME(StringUtils.rightPad(parts[1], 6));
                
                // 編輯有重新設定圖檔
                if (!adsTmpModel.getImgGuid().isEmpty()) {
                    // 設圖檔, 若有多個圖檔，Id 依序會被設定成 Id-L，Id-M，Id-S，小廣告 Id-F
                    List<ADMUPLOADTMP> imgTmps = getImageTmp(adsTmpModel.getImgGuid());
                    for (ADMUPLOADTMP imgTmp : imgTmps) {
                        po.setFILENAMEL(imgTmp.getFILENAME());
                        po.setFILECONTENTL(imgTmp.getFILECONTENT());
                    }
                    for ( ADMUPLOADTMP imgTmp : imgTmps ) {
                        admUploadTmpDao.delete(admUploadTmpDao.findById(imgTmp.getID()));
                    }
                }
                po.setURL(adsTmpModel.getURL());
                po.setSORTORDER(adsTmpModel.getSORTORDER());

                // 設流程資訊
                po.setSTEPID(rt.getNextStepId());
                po.setSTEPNAME(rt.getNextStepName());
                po.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");

                parts = DateUtils.format(new Date(), "yyyyMMdd-HHmmss").split("-");
                po.setLASTUSER(uid);
                po.setLASTDATE(parts[0]);
                po.setLASTTIME(parts[1]);

                po.setTARGETTYPE(adsTmpModel.getTARGETTYPE());
                switch ( po.getTARGETTYPE().charAt(0) ) {
                case '1' :
                	// 連結
                	po.setURL(adsTmpModel.getURL());
                	break;
                case '2' :
                	// 檔案
                	if ( !StringUtils.isEmpty(adsTmpModel.getFileGuid()) ) {
	                	List<ADMUPLOADTMP> fileTmps = getImageTmp(adsTmpModel.getFileGuid());
	                	po.setTARGETFILENAME(fileTmps.get(0).getFILENAME());
	                	po.setTARGETCONTENT(fileTmps.get(0).getFILECONTENT());
	                	admUploadTmpDao.delete(admUploadTmpDao.findById(adsTmpModel.getFileGuid()));
                	}
                	break;
                case '3' :
                	// 內容, 共用 TARGETCONTENT 欄位, 所以轉 byte[]
                	po.setTARGETCONTENT(adsTmpModel.getTargetContentStr().getBytes(StandardCharsets.UTF_8));
                	break;
                }
                
                admAdsTmpDao.save(po);
                result.put("0", adsTmpModel.getID());
            } else {
                result.put(rt.getStatusCode(), "");
            }
            log4Send(adsTmpModel.getID(), rt.getStatusCode());
        } catch (Exception e) {
            log.error("resendAdsTmp Error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤，請再試一次!!";
            result.put((String)Sanitizer.logForgingStr(message), "");
        }
        return result;
    }

    /**
     * 經辦取消案件
     *
     * @param caseSN        案件編號
     * @param oid           單位代碼
     * @param uid           使用者代碼
     * @param uName         使用者姓名
     * @return a {@link java.util.Map} object.
     */
    @Transactional
    public Map<String, String> cancelAdsTmp(String caseSN, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
        	ADMADSTMP adsTmp = admAdsTmpDao.findById(caseSN);
        	if ( adsTmp.getFLOWFINISHED().compareToIgnoreCase("N") == 0 ) {
        		// 流程往下執行
                FlowSvcResult rt = flowService.cancelFlow(caseSN, oid, uid, uName);
                if ( rt.getStatusCode() != FlowService.SUCCESS ) {
                	throw new Exception("刪除流程失敗，StatusCode="+rt.getStatusCode());
                }
        	}
        	// 流程已經結束
        	adsTmp.setSTEPID("");
        	adsTmp.setSTEPNAME("");
        	adsTmp.setFLOWFINISHED("Y");
        	
        	admAdsTmpDao.update(adsTmp);
            
            //2020-10-14 jinhanhuang 若是修改正在運作的廣告，退回後取消會造成問題
            // ADMADS ads = admAdsDao.findById(caseSN);
            // if ( ads != null ) {
            // 	admAdsDao.delete(ads);
            // }
            
            log4Delete(adsTmp.getID(), "0");    
            result.put("0", caseSN);
        } catch (Exception e) {
            log.error("cancelAdsTmp Error", e);
            result.put(e.getMessage(), "");
        }
        return result;
    }

    /**
     * 查詢案件狀態
     *
     * @param id a {@link java.lang.String} object.
     * @param roles a {@link java.util.List} object.
     * @param oid a {@link java.lang.String} object.
     * @param uid a {@link java.lang.String} object.
     * @return a {@link com.netbank.rest.web.back.service.AuthorityEnum} object.
     */
    public AuthorityEnum queryAdsTmp(String id, String oid, String uid, Set<String> roles) {
        FlowChkResult checkResult = flowService.IsMyCase(id, oid, uid, roles);
        if ( checkResult.getMatchStepId().isEmpty() ) {
            // 無流程
            ADMADSTMP po = admAdsTmpDao.findById(id);
            if ( po.getFLOWFINISHED().equalsIgnoreCase("Y") ) {
                return AuthorityEnum.NONE;
            } else {
                return AuthorityEnum.EDIT;
            }
        } else {
            // 有流程
            if ( checkResult.isMyCase() ) {
                // 這個案例只有：
                // 編輯者 --> 覆核退回（3）
                // 覆核者 --> 已送出，待覆核（2）
                long matchEditor = Arrays.stream(EDITOR_STEPS).filter(p->p.equals(checkResult.getMatchStepId())).count();
                if ( matchEditor > 0 )
                    return AuthorityEnum.EDIT;
    
                long matchReviewer = Arrays.stream(REVIEWER_STEPS).filter(p->p.equals(checkResult.getMatchStepId())).count(); 
                if ( matchReviewer > 0 ) 
                    return AuthorityEnum.REVIEW;
            } else {
                return AuthorityEnum.QUERY;
            }
        }
        return AuthorityEnum.QUERY;
    }
    
    /**
     * 將上傳的檔案暫存在 DB TMP 檔
     *
     * @param id               		 暫存主鍵
     * @param fileName          檔名
     * @param fileContent       檔內容
     */
    public void saveFileTmp(String id, String fileName, byte[] fileContent) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String createDate = format.format(Calendar.getInstance().getTime());
        
        ADMUPLOADTMP po = new ADMUPLOADTMP();
        po.setID(id);
        po.setPURPOSE("B501");
        po.setFILENAME(fileName);
        po.setFILECONTENT(fileContent);
        po.setCREATEDATE(createDate);
        admUploadTmpDao.save(po);
    }
}
