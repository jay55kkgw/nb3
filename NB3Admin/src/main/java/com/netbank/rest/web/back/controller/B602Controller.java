package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.B602ViewModel;
import com.netbank.rest.web.back.service.ADMMAILLOGService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMMAILLOG;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 電郵記錄檔 Controller，使用一個畫面做 CRUD
 * 已合併AdmMailContent
 *
 * @author YU-TAN LIN
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B602")
public class B602Controller {
    @Autowired
    private ADMMAILLOGService admMailLogService;

    /**
     * 取得電郵記錄檔首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        return "B602/index";
    }

    /**
     * Findranges 為搜尋一個區間的時間。
     * b602 set 群集 是以 MMDDHHMMSS 方式進入資料庫查詢，因此他會回傳兩組 Start資料與 End資料
     *
     * @param b602 a {@link com.netbank.rest.web.back.model.B602ViewModel} object.
     * @param result a {@link org.springframework.validation.BindingResult} object.
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @return jqDataTableRs
     */
    @PostMapping(value = "/Findranges", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse findranges(@ModelAttribute @Valid B602ViewModel b602, BindingResult result,
            HttpServletRequest request, HttpServletResponse response) {

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);
        // //0610信以前
        // Page page = _ADMMailLogService.getByRange(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
        //         jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), b602.getADUSERID(), b602.getADSENDSTATUS(),
        //         b602.getADQSDATE(), b602.getADQSDATEHH(), b602.getADQSDATEMM(), b602.getADQSDATESS(),
        //         b602.getADQEDATE(), b602.getADQEDATEHH(), b602.getADQEDATEMM(), b602.getADQEDATESS());
        Page page = admMailLogService.getByRange(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
            jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(b602.getADUSERID()), 
            (String)Sanitizer.escapeHTML(b602.getADSENDSTATUS()), (String)Sanitizer.escapeHTML(b602.getADQSDATE()),
            (String)Sanitizer.escapeHTML(b602.getADQEDATE()));

        List<ADMMAILLOG> maillogs = (List<ADMMAILLOG>) page.getResult();
        List<ADMMAILLOG> smaillogs = new ArrayList<ADMMAILLOG>();
        for (ADMMAILLOG maillog : maillogs) {
            ADMMAILLOG smaillog = new ADMMAILLOG();
            Sanitizer.escape4Class(maillog, smaillog);
            smaillogs.add(smaillog);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), smaillogs);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);
        return sjqDataTableRs;
    }
    /**
     * ReadMailContext 利用_AdmMailContent 取得ID為 ADMAILID的信
     *
     * @return ADMMAILLOG 資料利用 APPLICATION_JSON_UTF8_VALUE傳遞
     * @param ADMAILLOGID a {@link java.lang.String} object.
     */
    @PostMapping(value = "/ReadMailContent/{ADMAILLOGID}" ,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit  = true)
    public @ResponseBody ADMMAILLOG ReadMailContext(@PathVariable String ADMAILLOGID) {
        // JsonResponse response = new JsonResponse();
        // try {
        log.debug("Controller.ReadMailContext ADMAILLOGID={}", Sanitizer.logForgingStr(ADMAILLOGID));

        ADMMAILLOG mailLog = admMailLogService.getADMailContent(ADMAILLOGID);
        return mailLog;
    }

    /**
     * 重寄出信件資料(未完成，寄出信件要掛套件)
     *
     * @return Json 結果物件
     * @param ADMAILID a {@link java.lang.String} object.
     */
    @PostMapping(value = "/ReSentMail/{ADMAILID}", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String ReSentMail(@PathVariable String ADMAILID) {

        log.debug("Controller.ReSentMail ADMAILID={}", Sanitizer.logForgingStr(ADMAILID));
        
        try {
        	String result = admMailLogService.reSend(ADMAILID);
            return result;
        } catch (Exception e) {
            log.error("ReSent error", e);

            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }
}
