package com.netbank.rest.web.back.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.util.RESTfulClientUtil;

import fstop.orm.dao.IdGateHistoryDao;
import fstop.orm.dao.QuickLoginMappingDao;
import fstop.orm.po.ADMSTORETMP;
import fstop.orm.po.IDGATEHISTORY;
import fstop.orm.po.QUICKLOGINMAPPING;
import fstop.orm.po.QUICKLOGINMAPPING_PK;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 裝置服務
 *
 * @author 
 * @version V1.0
 */
@Slf4j
@Service
public class DeviceService extends BaseService {
    
	@Value("${MS_TW_URL}")
    private String mstwurl;
    
    @Autowired
    private QuickLoginMappingDao quickLoginMappingDao;

    @Autowired
    private IdGateHistoryDao idGateHistoryDao;

    public Page findQuickLogin(String custid, String IdGateid, int pageNo, int pageSize)
    {
        
        log4Query("0");

        return quickLoginMappingDao.findQuickLogin(custid, IdGateid, pageNo, pageSize);
    }

    public Page getVerifyHis(String custid, String IdGateid, String sDate, String eDate, int pageNo, int pageSize)
    {
    	IDGATEHISTORY idgatehis=new IDGATEHISTORY();
    	
    	idgatehis.setCUSIDN(custid);
    	idgatehis.setIDGATEID(IdGateid);
    	idgatehis.setSDate(sDate.replace("/", ""));
    	idgatehis.setEDate(eDate.replace("/", ""));
        log4Query("0");

        return idGateHistoryDao.getVerifyHis_V2(idgatehis, pageNo, pageSize);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Map doCancelIdGateid(Map hm, String dpsUserId) throws Exception {
        
        Integer toMsTimeout = 120;
        try {
            Map<?, ?> requestMap = new HashMap();
            
            requestMap.putAll(hm);

            String url = mstwurl + "IDGATE/IdGateMappingDelete";
            log.info("MS_TW URL : {}",url);
            Map rr= RESTfulClientUtil.send(requestMap, url , toMsTimeout);
        	
        	log4Query("0", new String[][] {{"dpsUserId",dpsUserId}});
        	
        	return rr;
            
        } catch (Exception e) {
        	
        	log4Query("1", new String[][] {{"dpsUserId",dpsUserId}});
            log.error("getVerifyParam error", e);
            throw e;
        }
    }
    
    @Transactional
    public boolean updateCancelIdGateId(String custid, String IdGateid, String dpsUserId, Map<String, Object> tbdata)
    {
    	try
    	{
	    	QUICKLOGINMAPPING quickmap=new QUICKLOGINMAPPING();
	    	QUICKLOGINMAPPING_PK quickmap_pk=new QUICKLOGINMAPPING_PK();
	    	quickmap_pk.setCUSIDN(custid);
	    	quickmap_pk.setIDGATEID(IdGateid);
	    	quickmap.setPks(quickmap_pk);
	    	
	    	quickmap = quickLoginMappingDao.get(quickmap_pk);
	        
	    	Date now = new Date();
	    	String date=DateTimeUtils.getDateShort(now);
	    	String time=DateTimeUtils.getTimeShort(now);
	    	quickmap.setLASTDATE(date);
	    	quickmap.setLASTTIME(time);
	    	quickmap.setUPDATEUSER(dpsUserId);
	    	quickmap.setDEVICESTATUS("9");
	    	
	    	quickLoginMappingDao.update(quickmap);
	    	
	        log4Query("0", new String[][] {{"dpsUserId",dpsUserId},{"CUSIDN",custid},{"IDGATEID",IdGateid}});
	        tbdata.put("qklogin", quickmap);
	        return true;
    	}
    	catch(Exception e)
    	{
    		log.error("updateCancelIdGateId Exception！！", e);
    		log4Query("1", new String[][] {{"dpsUserId",dpsUserId},{"CUSIDN",custid},{"IDGATEID",IdGateid}});
    		return false;
    	}
    }


}
