package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmMbStatusDao;
import fstop.orm.po.ADMMBSTATUS;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>ADMMBSTATUSService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class ADMMBSTATUSService extends BaseService {
    @Autowired
    private AdmMbStatusDao admMbStatusDao;

    /**
     * 取得資料
     *
     * @param ADMBSTATUSID      主鍵
     * @return                  ADMMBSTATUS POJO
     */
    public ADMMBSTATUS getByADMBSTATUSID(String ADMBSTATUSID) {
        log.debug("getByADMBSTATUSID ADMBSTATUSID={}", ADMBSTATUSID);

        log4Query("0", new String[][] { { "ADMBSTATUSID", ADMBSTATUSID }} );
        
        return admMbStatusDao.findById(ADMBSTATUSID);
    }

    /**
     * 儲存狀態資料
     *
     * @param admmbstatus   ADMMBSTATUS POJO
     */
    public void save(ADMMBSTATUS admmbstatus) {
        ADMMBSTATUS oriADMMBSTATUS = admMbStatusDao.get(admmbstatus.getADMBSTATUSID());
        admMbStatusDao.getEntityManager().detach(oriADMMBSTATUS);

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        admmbstatus.setLASTDATE(parts[0]);
        admmbstatus.setLASTTIME(parts[1]);

        admMbStatusDao.update(admmbstatus);
        log4Update(oriADMMBSTATUS, admmbstatus, "0");
    }
}
