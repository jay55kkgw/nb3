package com.netbank.rest.web.back.service;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.model.Rows;
import fstop.orm.dao.AdmLogInDao;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
/**
 * 使用者登入狀態查詢
 * 
 * @author Alison
 */
@Slf4j
@Service
public class ADMLOGINService extends BaseService {
    @Autowired
    private AdmLogInDao admLoginDao;
    
    /**
     * 分頁查詢
     * 
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param orderDir
     * @param STARTDATE
     * @param ENDDATE
     * @param USERID
     * @param LOGINTYPE
     * @return
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String STARTDATE, String ENDDATE,
            String USERID,String LOGINTYPE) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, STARTDATE={}, ENDDATE={}, USERID={}, LOGINTYPE={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(STARTDATE), Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID),Sanitizer.logForgingStr(LOGINTYPE));

        log4Query("0", new String[][] { { "STARTDATE", STARTDATE }, { "ENDDATE", ENDDATE }, { "USERID", USERID }, { "LOGINTYPE", LOGINTYPE } } );
        
        return admLoginDao.findPageData(pageNo, pageSize, orderBy, orderDir, STARTDATE, ENDDATE, USERID,LOGINTYPE);
    }    

    /**
     * 查詢目前登入人數，依 LoginType 做 Group
     * @return
     */
    public Rows getCurrentLogins() {
        return admLoginDao.getConCurrentLogins();
    }
    
    public void log4Query() {
    	log4Query("0");
    }
}