package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;
import com.netbank.rest.web.back.model.AnnTmpModel;
import com.netbank.rest.web.back.model.FlowChkResult;
import com.netbank.rest.web.back.model.FlowSvcResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmAnnDao;
import fstop.orm.dao.AdmAnnTmpDao;
import fstop.orm.dao.AdmUploadTmpDao;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.po.ADMANN;
import fstop.orm.po.ADMANNTMP;
import fstop.orm.po.ADMUPLOADTMP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 個人訊息公告內容刊登服務
 *
 * @author  簡哥
 * @since   2019/8/16
 */
@Slf4j
@Service
public class ADMANNService extends BaseService {
    @Autowired
    private FlowService flowService;

    @Autowired
    private AdmAnnDao admAnnDao;
    
    @Autowired
    private AdmAnnTmpDao admAnnTmpDao;

    @Autowired
    private AdmUploadTmpDao admUploadTmpDao;
        
    @Autowired
    private SysDailySeqDao sysDailySeqDao;

    // 要設定在 ADMFLOWDEF 資料表中
    private final String FLOW_ID="AnnFlow";

    // 取號的 APP ID
    private final String APP_ID = "ANN";

    // 編輯者 STEPID，參考 ADMFLOWDEFSTEP
    private final String[] EDITOR_STEPS = {"3"};     

    // 審核者/放行者 STEPID，參考 ADMFLOWDEFSTEP
    private final String[] REVIEWER_STEPS = {"2"};

    // 編輯者角色
    /** Constant <code>EDITOR_ROLES</code> */
    public static final String[] EDITOR_ROLES = {"EB","AP","DC"};

    // 審核者角色
    /** Constant <code>REVIEWER_ROLES</code> */
    public static final String[] REVIEWER_ROLES = {"EC","DC"};

    /**
     * 取得個人訊息公告內容刊登資料
     *
     * @param oid           單位代碼
     * @param dateFrom      開始時間
     * @param dateTo        結束時間
     * @param title         公告標題
     * @return              ADMANNTMP LIST
     */
    public List<ADMANNTMP> findAnnTmp(String oid, String dateFrom, String dateTo, String title, Set<String> roles, String flowFinished,String ANNCHANNEL) {
        String tmp = dateFrom.replace("/", "").replace(" ","").replace(":", "");
        String fromDate = tmp.substring(0,8);

        tmp = dateTo.replace("/", "").replace(" ","").replace(":", "");
        String toDate = tmp.substring(0,8);
        
        log4Query("0");
        return admAnnTmpDao.findByParams(oid, fromDate, toDate, title, roles, flowFinished,ANNCHANNEL);
    }

    /**
     * 依Id 查詢資料
     *
     * @param id    案件編號
     * @return      ADMANNTMP
     */
    public ADMANNTMP findById(String id) {
        return admAnnTmpDao.findById(id);
    }
    

    /**
     * 取得檔案
     *
     * @param id a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<ADMUPLOADTMP> getFileTmp(String id) {
        return admUploadTmpDao.findByIdLike(id);
    }

    /**
     * 經辦送件
     *
     * @param annTmpModel   個人訊息公告內容刊登管理 ViewModel
     * @param oid           單位代碼
     * @param uid           使用者代碼
     * @param uName         使用者姓名
     * @return              結果
     */
    @Transactional
    public Map<String, String> sendAnnTmp(AnnTmpModel annTmpModel, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            // 取案號, 案號＝ADS-xxxxxxxx
            String caseSN = APP_ID+"-"+ String.format("%06d", sysDailySeqDao.dailySeq(APP_ID));
            String todoURL = "B502/Query/"+caseSN;

            FlowSvcResult rt = flowService.startFlow(FLOW_ID, caseSN, oid, uid, uName, "", "", "公告管理", todoURL);
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                ADMANNTMP po = new ADMANNTMP();
                // 設主鍵
                po.setID(caseSN);

                // 設定其它資料
                po.setOID(oid);
                po.setTITLE(annTmpModel.getTITLE());
                po.setALLUSER(annTmpModel.getALLUSER());
                po.setUSERLIST(annTmpModel.getUSERLIST());
                po.setFILENAME(annTmpModel.getFILENAME());
                po.setTYPE(annTmpModel.getTYPE());
                po.setANNCHANNEL(annTmpModel.getANNCHANNEL());
         
                po.setSORTORDER(annTmpModel.getSORTORDER());
                po.setCONTENTTYPE(annTmpModel.getCONTENTTYPE());
                switch(annTmpModel.getCONTENTTYPE())
                {
                	case "1":  //URL
                	    po.setURL(annTmpModel.getURL());
                		break;
                	case "2":  //檔案
                        List<ADMUPLOADTMP> fileTmps = getFileTmp(annTmpModel.getFileGuid());
                        for (ADMUPLOADTMP fileTmp : fileTmps) {
                        	po.setSRCFILENAME(fileTmp.getFILENAME());
                        	po.setSRCCONTENT(fileTmp.getFILECONTENT());
                        }
                        
                        for ( ADMUPLOADTMP fileTmp : fileTmps ) {
                            admUploadTmpDao.delete(admUploadTmpDao.findById(fileTmp.getID()));
                        }
                        
                		break;
                	case "3":  //文字敘述
                		 po.setCONTENT(annTmpModel.getCONTENT());
                		 break;
                }

                // 轉換日期時間
                DateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss");
                String[] parts = format.format(annTmpModel.getStartDateTime()).split("-");

                po.setSTARTDATE(parts[0]);
                po.setSTARTTIME(parts[1]);

                parts = format.format(annTmpModel.getEndDateTime()).split("-");
                po.setENDDATE(parts[0]);
                po.setENDTIME(parts[1]);
                

                // 設流程資訊
                po.setSTEPID(rt.getNextStepId());
                po.setSTEPNAME(rt.getNextStepName());
                po.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");

                parts = format.format(new Date()).split("-");;
                po.setLASTUSER(uid);
                po.setLASTDATE(parts[0]);
                po.setLASTTIME(parts[1]);
  
                admAnnTmpDao.save(po);
                result.put("0", caseSN);
            } else {
                result.put(rt.getStatusCode(), "");
            }
            log4Send(caseSN, rt.getStatusCode());
        } catch (Exception e) {
            log.error("sendAnnTmp Error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤，請再試一次!!";
            result.put((String)Sanitizer.logForgingStr(message), "");
        }
        return result;
    }

    /**
     * 同意或是退回，流程往下一節點移動
     *
     * @param caseSN    案件編號
     * @param oid       單位代碼
     * @param uid       使用者 ID
     * @param uName     使用者姓名
     * @return          結果
     * @param stepId a {@link java.lang.String} object.
     * @param comments a {@link java.lang.String} object.
     * @param isApproved a boolean.
     */
    @Transactional
    public Map<String, String> approveOrRejectAnnTmp(String caseSN, String stepId, String oid, String uid, String uName, String comments, boolean isApproved) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            // 取案號, 案號＝ADS-xxxxxxxx
            FlowSvcResult rt = flowService.moveFlow(caseSN, stepId, oid, uid, uName, comments, "", isApproved);
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
                ADMANNTMP admAnnTmp = admAnnTmpDao.findById(caseSN);
                if ( rt.isIsFinal() ) {
                    ADMANN admAnn = admAnnDao.findById(caseSN);
                    boolean admAdsExist = true;
                    if ( admAnn == null ) {
                        admAnn = new ADMANN();
                        admAdsExist = false;
                    }
                    admAnn.setANNCHANNEL(admAnnTmp.getANNCHANNEL());
                    admAnn.setID(admAnnTmp.getID());
                    admAnn.setOId(oid);
                    admAnn.setTITLE(admAnnTmp.getTITLE());
                    admAnn.setALLUSER(admAnnTmp.getALLUSER());
                    admAnn.setUSERLIST(admAnnTmp.getUSERLIST());
                    admAnn.setFILENAME(admAnnTmp.getFILENAME());
                    admAnn.setTYPE(admAnnTmp.getTYPE());
                    admAnn.setSORTORDER(admAnnTmp.getSORTORDER());
                    admAnn.setCONTENTTYPE(admAnnTmp.getCONTENTTYPE());
                    switch(admAnnTmp.getCONTENTTYPE())
                    {
                    	case "1":  //URL
                    		admAnn.setURL(admAnnTmp.getURL());
                    		admAnn.setSRCFILENAME(null);
                    		admAnn.setSRCCONTENT(null);
                    		admAnn.setCONTENT(null);
                    		break;
                    	case "2":  //檔案
                    		admAnn.setSRCFILENAME(admAnnTmp.getFILENAME());
                    		admAnn.setSRCCONTENT(admAnnTmp.getSRCCONTENT());
                    		admAnn.setURL(null);
                    		admAnn.setCONTENT(null);
                    		break;
                    	case "3":  //文字敘述
                    		admAnn.setCONTENT(admAnnTmp.getCONTENT());
                    		admAnn.setURL(null);
                    		admAnn.setSRCFILENAME(null);
                    		admAnn.setSRCCONTENT(null);
                    		break;
                    }
                    
                    admAnn.setSTARTDATE(admAnnTmp.getSTARTDATE());
                    admAnn.setSTARTTIME(admAnnTmp.getSTARTTIME());
                    admAnn.setENDDATE(admAnnTmp.getENDDATE());
                    admAnn.setENDTIME(admAnnTmp.getENDTIME());

                    admAnn.setLASTUSER(uid);

                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
                    Date date = new Date();
                    String[] parts = dateFormat.format(date).split("-");

                    admAnn.setLASTDATE(parts[0]);
                    admAnn.setLASTTIME(parts[1]);

                    if ( admAdsExist ) {
                        admAnnDao.update(admAnn);
                    } else {
                        admAnnDao.save(admAnn);
                    }
                }
                admAnnTmp.setSTEPID(rt.isIsFinal() ? "" : rt.getNextStepId());
                admAnnTmp.setSTEPNAME(rt.isIsFinal() ? "" : rt.getNextStepName());
                admAnnTmp.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");
                
                admAnnTmpDao.save(admAnnTmp);
                result.put("0", caseSN);
            } else {
                result.put(rt.getStatusCode(), "");
            }
            if ( isApproved ) {
            	log4Approve(caseSN, rt.getStatusCode());
            } else {
            	log4Reject(caseSN, rt.getStatusCode());
            }
        } catch (Exception e) {
            log.error("approveOrRejectAnnTmp Error", e);
            result.put(e.getMessage(), "");
        }
        return result;
    }

    /**
     * 經辦重送
     *
     * @param annTmpModel       個人訊息公告內容刊登管理 ViewModel
     * @param oid               編輯者單位代碼
     * @param uid               編輯者代碼
     * @param uName             編輯者姓名
     * @return a {@link java.util.Map} object.
     */
    @Transactional
    public Map<String, String> resendAnnTmp(AnnTmpModel annTmpModel, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            ADMANNTMP po = admAnnTmpDao.findById(annTmpModel.getID());
            FlowSvcResult rt = null;

            if ( po.getFLOWFINISHED().equalsIgnoreCase("Y") ) {
                // 已結案，重送
                String todoURL = "B502/Query/"+annTmpModel.getID();
                rt = flowService.startFlow(FLOW_ID, annTmpModel.getID(), oid, uid, uName, "", "", "公告管理", todoURL);
            } else {
                // 流程往下執行(退回後重送)
                rt = flowService.moveFlow(annTmpModel.getID(), annTmpModel.getSTEPID(), oid, uid, uName, annTmpModel.getComments(), "", true);
            }
            if ( rt.getStatusCode() == FlowService.SUCCESS ) {
            	po.setANNCHANNEL(annTmpModel.getANNCHANNEL());
                po.setOID(oid);
                po.setCONTENT(annTmpModel.getCONTENT());
                po.setALLUSER(annTmpModel.getALLUSER());
                po.setUSERLIST(annTmpModel.getUSERLIST());
                po.setFILENAME(annTmpModel.getFILENAME());
                po.setTYPE(annTmpModel.getTYPE());
                po.setURL(annTmpModel.getURL());
                po.setSORTORDER(annTmpModel.getSORTORDER());
                po.setCONTENTTYPE(annTmpModel.getCONTENTTYPE());
                po.setTITLE(annTmpModel.getTITLE());
                switch(annTmpModel.getCONTENTTYPE())
                {
                	case "1":  //URL
                		po.setURL(annTmpModel.getURL());
                		po.setSRCFILENAME(null);
                		po.setSRCCONTENT(null);
                		po.setCONTENT(null);
                		break;
                	case "2":  //檔案
                		// 編輯有重新上傳檔案
                        if (!annTmpModel.getFileGuid().isEmpty()) {
	                        List<ADMUPLOADTMP> fileTmps = getFileTmp(annTmpModel.getFileGuid());
	                        for (ADMUPLOADTMP fileTmp : fileTmps) {
	                        	po.setSRCFILENAME(fileTmp.getFILENAME());
	                        	po.setSRCCONTENT(fileTmp.getFILECONTENT());
	                        }
	                        
	                        for ( ADMUPLOADTMP fileTmp : fileTmps ) {
	                            admUploadTmpDao.delete(admUploadTmpDao.findById(fileTmp.getID()));
	                        }
                        }
                        po.setURL(null);
                        po.setCONTENT(null);
                		break;
                	case "3":  //文字敘述
                		 po.setCONTENT(annTmpModel.getCONTENT());
               		  	 po.setURL(null);
                		 po.setSRCFILENAME(null);
                         po.setSRCCONTENT(null);
                		 break;
                }
                
                // 轉換日期時間
                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
                String[] parts = dateFormat.format(annTmpModel.getStartDateTime()).split("-");
                po.setSTARTDATE(parts[0]);
                po.setSTARTTIME(parts[1]);

                parts = dateFormat.format(annTmpModel.getEndDateTime()).split("-");
                po.setENDDATE(parts[0]);
                po.setENDTIME(parts[1]);

                // 設流程資訊
                po.setSTEPID(rt.getNextStepId());
                po.setSTEPNAME(rt.getNextStepName());
                po.setFLOWFINISHED(rt.isIsFinal() ? "Y" : "N");

                parts = dateFormat.format(new Date()).split("-");
                po.setLASTUSER(uid);
                po.setLASTDATE(parts[0]);
                po.setLASTTIME(parts[1]);

                admAnnTmpDao.save(po);
                result.put("0", annTmpModel.getID());
            } else {
                result.put(rt.getStatusCode(), "");
            }
            log4Send(annTmpModel.getID(), rt.getStatusCode());
        } catch (Exception e) {
            log.error("resendAnnTmp Error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤，請再試一次!!";
            result.put((String)Sanitizer.logForgingStr(message), "");
        }
        return result;
    }

    /**
     * 經辦取消案件
     *
     * @param caseSN        案件編號
     * @param oid           單位代碼
     * @param uid           使用者代碼
     * @param uName         使用者姓名
     * @return a {@link java.util.Map} object.
     */
    @Transactional
    public Map<String, String> cancelAnnTmp(String caseSN, String oid, String uid, String uName) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            // 流程往下執行
            ADMANNTMP annTmp = admAnnTmpDao.findById(caseSN);
            if ( annTmp.getFLOWFINISHED().compareToIgnoreCase("N") == 0 ) {
                FlowSvcResult rt = flowService.cancelFlow(caseSN, oid, uid, uName);
                if ( rt.getStatusCode() != FlowService.SUCCESS ) {
                    throw new Exception("刪除流程失敗，StatusCode="+rt.getStatusCode());
                }
            }
            // 流程已經結束
            admAnnTmpDao.update(annTmp);
            
             //2020-10-14 jinhanhuang 若是修改正在運作的公告，退回後取消會造成問題
            // ADMANN ann = admAnnDao.findById(caseSN);
            // if ( ann != null ) {
            //     admAnnDao.delete(ann);
            // }
            
            log4Delete(annTmp, "0");
            result.put("0", caseSN);
        } catch (Exception e) {
            log.error("cancelAnnTmp Error", e);
            result.put(e.getMessage(), "");
        }
        return result;
    }

    /**
     * 查詢案件狀態
     *
     * @param id a {@link java.lang.String} object.
     * @param roles a {@link java.util.List} object.
     * @param oid a {@link java.lang.String} object.
     * @param uid a {@link java.lang.String} object.
     * @return a {@link com.netbank.rest.web.back.service.AuthorityEnum} object.
     */
    public AuthorityEnum queryAnnTmp(String id, String oid, String uid, Set<String> roles) {
        FlowChkResult checkResult = flowService.IsMyCase(id, oid, uid, roles);
        if ( checkResult.getMatchStepId().isEmpty() ) {
            // 無流程
            ADMANNTMP po = admAnnTmpDao.findById(id);
            if ( po.getFLOWFINISHED().equalsIgnoreCase("Y") ) {
                return AuthorityEnum.NONE;
            } else {
                return AuthorityEnum.EDIT;
            }
        } else {
            // 有流程
            if ( checkResult.isMyCase() ) {
                // 這個案例只有：
                // 編輯者 --> 覆核退回（3）
                // 覆核者 --> 已送出，待覆核（2）
                long matchEditor = Arrays.stream(EDITOR_STEPS).filter(p->p.equals(checkResult.getMatchStepId())).count();
                if ( matchEditor > 0 )
                    return AuthorityEnum.EDIT;
    
                long matchReviewer = Arrays.stream(REVIEWER_STEPS).filter(p->p.equals(checkResult.getMatchStepId())).count(); 
                if ( matchReviewer > 0 ) 
                    return AuthorityEnum.REVIEW;
            } else {
                return AuthorityEnum.QUERY;
            }
        }
        return AuthorityEnum.QUERY;
    }
    
    /**
     * 將上傳的檔案暫存在 DB TMP 檔
     *
     * @param id               		 暫存主鍵
     * @param fileName          檔名
     * @param fileContent       檔內容
     */
    public void saveFileTmp(String id, String fileName, byte[] fileContent) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String createDate = format.format(Calendar.getInstance().getTime());
        
        ADMUPLOADTMP po = new ADMUPLOADTMP();
        po.setID(id);
        po.setPURPOSE("B502");
        po.setFILENAME(fileName);
        po.setFILECONTENT(fileContent);
        po.setCREATEDATE(createDate);
        admUploadTmpDao.save(po);
    }
}
