package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.netbank.rest.web.back.model.B707Result;
import com.netbank.rest.web.back.service.NMBReportService;
import com.netbank.rest.web.back.service.PortalSSOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("/B707")
public class B707Controller {
    @Autowired
    private PortalSSOService portalSSOService;

    @Autowired
    private NMBReportService nmbReportService;

    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        List<String> years = new ArrayList<String>();
        for (int i = 2019; i <= 2119; i++) {
            years.add(Integer.toString(i));
        }

        List<String> months = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            months.add(String.format("%02d", i));
        }

        model.addAttribute("years", years);
        model.addAttribute("months", months);
        
        Calendar calendar = Calendar.getInstance(); 
        Date now = calendar.getTime();

        model.addAttribute("YYYY", (new SimpleDateFormat("yyyy")).format(now));
        model.addAttribute("MM", (new SimpleDateFormat("MM")).format(now));

        return "B707/index";
    }

    @PostMapping(value = "/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody HashMap query(@RequestParam String YYYYMM) {
        HashMap model = new HashMap();
        
        try {
            List<B707Result> reportData = nmbReportService.findTxnLogGroupByDateLogintype(YYYYMM);
            model.put("errorMsg", "");
            model.put("reportData", reportData);
        } catch (Exception e) {
            log.error("query error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤請再試一次!!";
            model.put("errorMsg", (String)Sanitizer.logForgingStr(message));
            return model;
        }
        return model;
    }

    @GetMapping(value = { "/Report" })
    @Authorize(userInRoleCanQuery = true)
    public void report(@RequestParam String YYYYMM, ModelMap model, HttpServletResponse response) throws IOException {
        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"B707Report.csv\"");
        
        OutputStream resOs = response.getOutputStream();
        resOs.write('\ufeef'); // emits 0xef
        resOs.write('\ufebb'); // emits 0xbb
        resOs.write('\ufebf'); // emits 0xbf

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = new CSVWriter(outputWriter, CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_ESCAPE_CHARACTER);

        String[] headers = { "交易日期", "一般網銀", "行動網銀" };
        writer.writeNext(headers);

        try {
            List<B707Result> reportData = nmbReportService.findTxnLogGroupByDateLogintype(YYYYMM);
            for ( int i=0; i<reportData.size(); i++ ) {
                String[] nextLine = { reportData.get(i).getLastDate(), String.valueOf(reportData.get(i).getNbCount()), String.valueOf(reportData.get(i).getMbCount()) };
                writer.writeNext(nextLine);
            }
            writer.close();
        } catch (Exception e) {
            log.error("Error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤請再試一次!!";
            String[] nextLine = { (String)Sanitizer.logForgingStr(message), "", "" };
            writer.writeNext(nextLine);
            writer.close();
            return;
        }
    }
}