package com.netbank.rest.web.back.service;

import java.util.HashMap;
import java.util.Map;

import com.netbank.rest.web.back.util.RESTfulClientUtil;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BasicProfileService extends BaseService {
    @Value("${MSPS.URL}")
    private String mspsURL;
    
    public Map getTxnUserBasicProfile(String dpsUserId) throws Exception {
        String result = "";
        
        Integer toMsTimeout = 120;
        try {
            Map requestMap = new HashMap();
            requestMap.put("CUSIDN", dpsUserId);

            Map rr= RESTfulClientUtil.send(requestMap, mspsURL, toMsTimeout);
        	
        	log4Query("0", new String[][] {{"dpsUserId",dpsUserId}});
        	
        	return rr;
            
        } catch (Exception e) {
        	
        	log4Query("1", new String[][] {{"dpsUserId",dpsUserId}});
            log.error("getTxnUserBasicProfile error", e);
            throw e;
        }
    }
}