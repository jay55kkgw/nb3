package com.netbank.rest.web.back.util;

/**
 * <p>JQueryDataTableResponse class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class JQueryDataTableResponse {
    /// <summary>
    /// 
    /// </summary>
    private int draw;
    
    /// <summary>
    /// 總筆數
    /// </summary>
    private long recordsTotal;

    /// <summary>
    /// 篩選筆數
    /// </summary>
    private long recordsFiltered;
    
    private Object data;

    /**
     * <p>Getter for the field <code>draw</code>.</p>
     *
     * @return the draw
     */
    public int getDraw() {
        return draw;
    }

    /**
     * <p>Setter for the field <code>draw</code>.</p>
     *
     * @param draw the draw to set
     */
    public void setDraw(int draw) {
        this.draw = draw;
    }

    /**
     * <p>Getter for the field <code>recordsTotal</code>.</p>
     *
     * @return the recordsTotal
     */
    public long getRecordsTotal() {
        return recordsTotal;
    }

    /**
     * <p>Setter for the field <code>recordsTotal</code>.</p>
     *
     * @param recordsTotal the recordsTotal to set
     */
    public void setRecordsTotal(long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    /**
     * <p>Getter for the field <code>recordsFiltered</code>.</p>
     *
     * @return the recordsFiltered
     */
    public long getRecordsFiltered() {
        return recordsFiltered;
    }

    /**
     * <p>Setter for the field <code>recordsFiltered</code>.</p>
     *
     * @param recordsFiltered the recordsFiltered to set
     */
    public void setRecordsFiltered(long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    /**
     * <p>Getter for the field <code>data</code>.</p>
     *
     * @return the data
     */
    public Object getData() {
        return data;
    }

    /**
     * <p>Setter for the field <code>data</code>.</p>
     *
     * @param data the data to set
     */
    public void setData(Object data) {
        this.data = data;
    }
}
