package com.netbank.rest.web.back.service;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sun.mail.util.MailSSLSocketFactory;
import com.sun.xml.messaging.saaj.packaging.mime.internet.MimeUtility;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NotifyService {
	/**
	 * smtp mail server, hostname or ip
	 */
	@Value(value="${spring.mail.host:10.16.21.124}")
	private String host;
	
	/**
	 * smtp mail server port
	 */
	@Value(value="${spring.mail.port:465}")
	private int port;
	
	/**
	 * 是否啟用 TLS, 暫不使用
	 */
	@Value(value="${spring.mail.properties.mail.smtp.starttls.enable:true}")
	private boolean enableTLS;
	
	/**
	 * 寄件人 email
	 */
	@Value(value="${spring.mail.from.default:}")
	private String from;
	
	/**
	 * 寄件人
	 */
	@Value(value="${spring.mail.from.name:}")
	private String fromName;
	
	/**
	 * SMTP 是否需要認證, 暫不使用
	 */
	@Value(value="${spring.mail.properties.mail.smtp.auth:true}")
	private boolean auth;
	
	/**
	 * SMTP 認證 id
	 */
	@Value(value="${spring.mail.username:eatm@mail.tbb.com.tw}")
	private String userName;
	
	/**
	 * SMTP 認證密碼
	 */
	@Value(value="${spring.mail.soci:QWIxMjM0NTY=}")
	private String soci;
	
	/**
	 * 寄送電子郵件
	 * @param subject 主旨
	 * @param htmlContent HTML 內文
	 * @param toList 收件者清單
	 * @param attachFileNameList 附件檔名清單
	 * @param attachFileList 附件檔案內容
	 */
	public void smtpMail(String subject, String htmlContent, List<String> toList, List<String> attachFileNameList, List<byte[]>attachFileList) {
        try {
	        Properties properties = System.getProperties();
	        
	        properties.put("mail.smtp.host", host);
	        final String seAcn = userName;
	        final String sePwdau = new String(Base64.getDecoder().decode(soci));
	
	        properties.put("mail.debug", "true"); // set for debug
	        properties.put("mail.smtp.port", port);
	        properties.put("mail.smtp.auth", "true");
	
	        properties.put("mail.transport.protocol", "smtp");
	        properties.put("mail.smtp.timeout", "60000");
	        properties.put("mail.smtp.connectiontimeout", "60000");
	        properties.put("mail.smtp.writetimeout", "60000");
	
	        //case1:
	        MailSSLSocketFactory sf  = new MailSSLSocketFactory();
	        sf.setTrustAllHosts(true);
	        properties.put("mail.smtp.socketFactory", sf);
	        properties.put("mail.smtp.ssl.enable", "true");
	        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        
	        javax.mail.Session session = javax.mail.Session.getInstance(properties, new javax.mail.Authenticator() {
               protected PasswordAuthentication getPasswordAuthentication() {
                       return new PasswordAuthentication(seAcn, sePwdau);
               }
	        });
	        session.setDebug(true); // for SystemOut.log check host and port

            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            log.debug("from:{}, fromName:{}", from , fromName);
            
            if ( fromName.compareToIgnoreCase("dev") == 0 ) {
            	message.setFrom(new InternetAddress(from, "臺灣企銀(開發)", "UTF-8"));
            } else if ( fromName.compareToIgnoreCase("test") == 0 ) {
            	message.setFrom(new InternetAddress(from, "臺灣企銀(測試)", "UTF-8"));
            } else {
            	message.setFrom(new InternetAddress(from, "臺灣企銀", "UTF-8"));
            }
           
            // Set To: header field of the header.
            for ( int i=0; i<toList.size(); i++ ) {
            	 message.addRecipient(Message.RecipientType.TO, new InternetAddress(toList.get(i)));
            }
           

            // Set Subject: header field
            message.setSubject(subject, "UTF-8");

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setContent(htmlContent, "text/html; charset=UTF-8");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);  
            
            if ( attachFileNameList != null && attachFileList != null ) {
	            if ( attachFileNameList.size() != attachFileList.size() ) {
	            	throw new IndexOutOfBoundsException();
	            }
	
	            for ( int i=0; i<attachFileNameList.size(); i++ ) {
	            	BodyPart bodyPart = new MimeBodyPart();
	                
	            	DataSource attachDataSource=new ByteArrayDataSource(attachFileList.get(i), "application/octet-stream");
	            	
	            	bodyPart.setDataHandler(new DataHandler(attachDataSource));
	            	bodyPart.setFileName(attachFileNameList.get(i));
	                multipart.addBodyPart(bodyPart); 
	            }
            }
            
            Transport.send(message);
            log.debug("mail2000 寄送成功");
        } catch (Exception e) {
            log.error("mailTest Error", e);
        } 
	}
}
