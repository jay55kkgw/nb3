package com.netbank.rest.web.back.model;

/**
 * <p>B702Result class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class B702Result {
    private String seqNo;
    private String adopGroup;
    private String adopGroupName;
    private String adUserType;
    private String selfCount;
    private String crossCount;
    private String adCount;
    private String selfAmt;
    private String crossAmt;
    private String adAmount;

    /**
     * <p>Getter for the field <code>seqNo</code>.</p>
     *
     * @return String return the seqNo
     */
    public String getSeqNo() {
        return seqNo;
    }

    /**
     * <p>Setter for the field <code>seqNo</code>.</p>
     *
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * <p>Getter for the field <code>adopGroup</code>.</p>
     *
     * @return String return the adopGroup
     */
    public String getAdopGroup() {
        return adopGroup;
    }

    /**
     * <p>Setter for the field <code>adopGroup</code>.</p>
     *
     * @param adopGroup the adopGroup to set
     */
    public void setAdopGroup(String adopGroup) {
        this.adopGroup = adopGroup;
    }

    /**
     * <p>Getter for the field <code>adopGroupName</code>.</p>
     *
     * @return String return the adopGroupName
     */
    public String getAdopGroupName() {
        return adopGroupName;
    }

    /**
     * <p>Setter for the field <code>adopGroupName</code>.</p>
     *
     * @param adopGroupName the adopGroupName to set
     */
    public void setAdopGroupName(String adopGroupName) {
        this.adopGroupName = adopGroupName;
    }

    /**
     * <p>Getter for the field <code>adUserType</code>.</p>
     *
     * @return String return the adUserType
     */
    public String getAdUserType() {
        return adUserType;
    }

    /**
     * <p>Setter for the field <code>adUserType</code>.</p>
     *
     * @param adUserType the adUserType to set
     */
    public void setAdUserType(String adUserType) {
        this.adUserType = adUserType;
    }

    /**
     * <p>Getter for the field <code>selfCount</code>.</p>
     *
     * @return String return the selfCount
     */
    public String getSelfCount() {
        return selfCount;
    }

    /**
     * <p>Setter for the field <code>selfCount</code>.</p>
     *
     * @param selfCount the selfCount to set
     */
    public void setSelfCount(String selfCount) {
        this.selfCount = selfCount;
    }

    /**
     * <p>Getter for the field <code>crossCount</code>.</p>
     *
     * @return String return the crossCount
     */
    public String getCrossCount() {
        return crossCount;
    }

    /**
     * <p>Setter for the field <code>crossCount</code>.</p>
     *
     * @param crossCount the crossCount to set
     */
    public void setCrossCount(String crossCount) {
        this.crossCount = crossCount;
    }

    /**
     * <p>Getter for the field <code>adCount</code>.</p>
     *
     * @return String return the adCount
     */
    public String getAdCount() {
        return adCount;
    }

    /**
     * <p>Setter for the field <code>adCount</code>.</p>
     *
     * @param adCount the adCount to set
     */
    public void setAdCount(String adCount) {
        this.adCount = adCount;
    }

    /**
     * <p>Getter for the field <code>selfAmt</code>.</p>
     *
     * @return String return the selfAmt
     */
    public String getSelfAmt() {
        return selfAmt;
    }

    /**
     * <p>Setter for the field <code>selfAmt</code>.</p>
     *
     * @param selfAmt the selfAmt to set
     */
    public void setSelfAmt(String selfAmt) {
        this.selfAmt = selfAmt;
    }

    /**
     * <p>Getter for the field <code>crossAmt</code>.</p>
     *
     * @return String return the crossAmt
     */
    public String getCrossAmt() {
        return crossAmt;
    }

    /**
     * <p>Setter for the field <code>crossAmt</code>.</p>
     *
     * @param crossAmt the crossAmt to set
     */
    public void setCrossAmt(String crossAmt) {
        this.crossAmt = crossAmt;
    }

    /**
     * <p>Getter for the field <code>adAmount</code>.</p>
     *
     * @return String return the adAmount
     */
    public String getAdAmount() {
        return adAmount;
    }

    /**
     * <p>Setter for the field <code>adAmount</code>.</p>
     *
     * @param adAmount the adAmount to set
     */
    public void setAdAmount(String adAmount) {
        this.adAmount = adAmount;
    }

}
