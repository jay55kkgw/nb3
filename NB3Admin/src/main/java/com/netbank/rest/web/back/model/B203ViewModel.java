package com.netbank.rest.web.back.model;

import java.util.List;


public class B203ViewModel {
    private List<B203KeyValuePair> basicData;
    private List<B203KeyValuePair> certData;
    private List<B203TelRecord> telData;
    private String errorMsg;
    
    public List<B203KeyValuePair> getBasicData() {
        return basicData;
    }

    public void setBasicData(List<B203KeyValuePair> basicData) {
        this.basicData = basicData;
    }

    public List<B203KeyValuePair> getCertData() {
        return certData;
    }

    public void setCertData(List<B203KeyValuePair> certData) {
        this.certData = certData;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<B203TelRecord> getTelData() {
        return telData;
    }

    public void setTelData(List<B203TelRecord> telData) {
        this.telData = telData;
    }
    
}

