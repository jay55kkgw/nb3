package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.netbank.domain.orm.core.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fstop.orm.dao.AdmLoginAclDao;
import fstop.orm.po.ADMLOGINACL;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 白名單管理 Service
 * 
 * @author      簡哥
 * @version     V1.0
 */
@Slf4j
@Service
public class ADMLoginAclService extends BaseService {
    @Autowired
    private AdmLoginAclDao admLoginAclDao;

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo 第幾頁
	 * @param pageSize 一頁幾筆
	 * @param orderBy 依什麼欄位排序
	 * @param orderDir 升冪/降冪
     * @param ADUserId 假日代號
     * @return 分頁物件
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String ADUserId) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, ADUserId={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ADUserId));

        log4Query("0", new String[][] { { "ADUserId", ADUserId } } );
        
        return admLoginAclDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, ADUserId);
    }

    /**
     * 取得資料
     *
     * @param ADUserId 使用者統編
     * @return POJO
     */
    public ADMLOGINACL getLoginACL(String ADUserId) {
        log.debug("getUserACL ADUserId={}", Sanitizer.logForgingStr(ADUserId));

        log4Query("0", new String[][] { { "ADUserId", ADUserId }} );
        
        return admLoginAclDao.findById(ADUserId);
    }

    /**
     * 新增資料
     *
     * @param holiday POJO
     * @param creator 編輯者
     */
    public void insertLoginACL(ADMLOGINACL loginACL, String creator) {
        loginACL.setLASTUSER(creator); 
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        loginACL.setLASTDATE(parts[0]);
        loginACL.setLASTTIME(parts[1]);

        admLoginAclDao.save(loginACL);
        log4Create(loginACL, "0");
    } 

    /**
     * 儲存資料
     *
     * @param loginACL  異動 POJO
     * @param editor    編輯者
     * @param holiday a {@link fstop.orm.po.ADMHOLIDAY} object.
     */
    public void saveLoginACL(ADMLOGINACL loginACL, String editor) {
        ADMLOGINACL oriLoginACL = admLoginAclDao.findById(loginACL.getADUSERID());
        admLoginAclDao.getEntityManager().detach(oriLoginACL);

        loginACL.setLASTUSER(editor); 

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        loginACL.setLASTDATE(parts[0]);
        loginACL.setLASTTIME(parts[1]);

        admLoginAclDao.update(loginACL);
        log4Update(oriLoginACL, loginACL, "0");
    }

    /**
     * 刪除資料
     *
     * @param holidayId 幣別ID
     */
    public void deleteLoginACL(String adUserId) {
        ADMLOGINACL oriLoginACL = admLoginAclDao.findById(adUserId);
        admLoginAclDao.removeById(adUserId);  
        log4Delete(oriLoginACL, "0");
    }

    /**
     * 整批上傳
     *
     * @param holidays 假日檔陣列
     * @param actor 動作者
     */
    public void uploadLoginACL(String[] adUserIDs, String actor) {
        for ( String adUserID : adUserIDs ) {
            if ( !adUserID.isEmpty() ) {
                if ( admLoginAclDao.findById(adUserID) == null ) {
                    ADMLOGINACL newLoginACL = new ADMLOGINACL();
                    newLoginACL.setADUSERID(adUserID);
                    insertLoginACL(newLoginACL, actor);
                } 
            }
        }
    }
}