package com.netbank.rest.web.back.util;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
//import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.ssl.TrustStrategy;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import fstop.util.Sanitizer;

import com.netbank.rest.web.back.model.BatchResult;
import com.netbank.util.CodeUtil;
import com.netbank.util.DateUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :電文傳送Util
 *
 */

@Slf4j
public class RESTfulClientUtil {
	
	
	
	/**
	 * 電文傳送 RESTful
	 * @param params
	 * @param url
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public static <T> T send(Object sendData , String url , Integer toMsTimeout) {
		Object result = null;
		Object retobj = null;
		String sendTime = "";
		RestTemplate restTemplate = null;
		try {
			log.trace("url>>{}",Sanitizer.logForgingStr(url));
			if ( sendData instanceof List ) {
				List<String> sendList = CodeUtil.objectCovert(ArrayList.class, sendData);
				log.trace("sendList>>{}",Sanitizer.logForgingStr(sendList));
				if(sendList ==null) {
					throw new Exception("資料轉換異常，此資料無法送出");
				}
				log.trace("sendJSON >>{}",Sanitizer.logForgingStr(sendList));
				restTemplate = setHttpConfig(toMsTimeout);
				sendTime = new DateTime().toString("yyyy-MM-dd HH:mm:ss");

				result = restTemplate.postForObject(url, sendList, HashMap.class);
				
			} else {
				HashMap<String, String> sendMap = CodeUtil.objectCovert(HashMap.class, sendData);
				log.trace("sendMap>>{}",Sanitizer.logForgingStr(sendMap));
				if(sendMap ==null) {
					throw new Exception("資料轉換異常，此資料無法送出");
				}
				log.trace("sendJSON >>{}",Sanitizer.logForgingStr(sendMap));
				restTemplate = setHttpConfig(toMsTimeout);
				sendTime = new DateTime().toString("yyyy-MM-dd HH:mm:ss");

				result = restTemplate.postForObject(url, sendMap, HashMap.class);
			}
			//20191119-Danny-Stored Log Forging\路徑 13:
			log.trace("result>>{}",Sanitizer.logForgingStr(result));
			
			log.trace("result.toJson>>{}", Sanitizer.logForgingStr(result));
			
			log.trace("rest>>{}，收送花費時間>>{}",Sanitizer.logForgingStr(url),Sanitizer.logForgingStr(DateUtils.getDiffTimeStamp(sendTime, new DateTime().toString("yyyy-MM-dd HH:mm:ss"))));
			if(result == null) {
				throw new Exception("result == null ,資料接收異常");
			}
			retobj = CodeUtil.objectCovert( HashMap.class, result);
			
		} catch (ResourceAccessException e) {
			//e.printStackTrace();
			log.error("",e);
			throw e;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("",e);
		}
		
		
		return (T) retobj;
		
	}
	
	/**
	 * 批次專用
	 * @param <T>
	 * @param sendData
	 * @param url
	 * @param toMsTimeout
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T sendBatch(Object sendData , String url , Integer toMsTimeout) {
		Object result = null;
		Object retobj = null;
		String sendTime = "";
		RestTemplate restTemplate = null;
		try {
			log.trace("url>>{}",Sanitizer.logForgingStr(url));
			if ( sendData instanceof List ) {
				List<String> sendList = CodeUtil.objectCovert(ArrayList.class, sendData);
				log.trace("sendList>>{}",Sanitizer.logForgingStr(sendList));
				if(sendList ==null) {
					throw new Exception("資料轉換異常，此資料無法送出");
				}
				log.trace("sendJSON >>{}",Sanitizer.logForgingStr(sendList));
				restTemplate = setHttpConfig(toMsTimeout);
				sendTime = new DateTime().toString("yyyy-MM-dd HH:mm:ss");

				result = restTemplate.postForObject(url, sendList, BatchResult.class);
				
			} else {
				HashMap<String, String> sendMap = CodeUtil.objectCovert(HashMap.class, sendData);
				log.trace("sendMap>>{}",Sanitizer.logForgingStr(sendMap));
				if(sendMap ==null) {
					throw new Exception("資料轉換異常，此資料無法送出");
				}
				log.trace("sendJSON >>{}",Sanitizer.logForgingStr(sendMap));
				restTemplate = setHttpConfig(toMsTimeout);
				sendTime = new DateTime().toString("yyyy-MM-dd HH:mm:ss");

				result = restTemplate.postForObject(url, sendMap, BatchResult.class);
			}

			log.trace("result>>{}",Sanitizer.logForgingStr(result));
			
			log.trace("result.toJson>>{}", Sanitizer.logForgingStr(result));
			
			log.trace("rest>>{}，收送花費時間>>{}",Sanitizer.logForgingStr(url),Sanitizer.logForgingStr(DateUtils.getDiffTimeStamp(sendTime, new DateTime().toString("yyyy-MM-dd HH:mm:ss"))));
			if(result == null) {
				throw new Exception("result == null ,資料接收異常");
			}
			retobj = CodeUtil.objectCovert(BatchResult.class, result);
			
		} catch (ResourceAccessException e) {
			// e.printStackTrace();
			log.error("",e);
			throw e;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("",e);
		}
		return (T) retobj;
	}
	

	public static RestTemplate setHttpConfig() {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = 55*1000;
		HttpComponentsClientHttpRequestFactory rf = new HttpComponentsClientHttpRequestFactory();
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		return new RestTemplate(rf);
		
	}
	
//	public static RestTemplate setHttpConfigI(Integer toTmraTimeout) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
//		Integer connectTimeout = 5*1000;
//		Integer socketTimeout = toTmraTimeout*1000;
//		
//		log.trace("setHttpConfigI..");
//		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
//
//		SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
//		        .loadTrustMaterial(null, acceptingTrustStrategy)
//		        .build();
//
//		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
//
//		CloseableHttpClient httpClient = HttpClients.custom()
//		        .setSSLSocketFactory(csf)
//		        .build();
//
//		HttpComponentsClientHttpRequestFactory rf =
//		        new HttpComponentsClientHttpRequestFactory();
//
//		rf.setHttpClient(httpClient);
//		rf.setConnectTimeout(connectTimeout);
//		rf.setReadTimeout(socketTimeout);
//		log.trace("setHttpConfigI end..");
//		return new RestTemplate(rf);
//		
//	}
	public static RestTemplate setHttpConfig(Integer toMsTimeout) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = toMsTimeout*1000;
		
		log.trace("setHttpConfig..");
		
		HttpComponentsClientHttpRequestFactory rf =
				new HttpComponentsClientHttpRequestFactory();
		
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		log.trace("setHttpConfigI end..");
		return new RestTemplate(rf);
		
	}
	
	
}
