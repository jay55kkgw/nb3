package com.netbank.rest.web.back.model;

/**
 * <p>B704Result class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class B704Result {
    private String adopid;
    private String adopname;
    private String adagreef;
    private String qtytype;

    private String c00count;
    private String c01count;
    private String c10count;
    private String c11count;
    private String c20count;
    private String c21count;

    private String p00count;
    private String p01count;
    private String p10count;
    private String p11count;
    private String p20count;
    private String p21count;

    private String t99;
    private String f99;
    private String tf;

    /**
     * <p>Getter for the field <code>adopid</code>.</p>
     *
     * @return String return the adopid
     */
    public String getAdopid() {
        return adopid;
    }

    /**
     * <p>Setter for the field <code>adopid</code>.</p>
     *
     * @param adopid the adopid to set
     */
    public void setAdopid(String adopid) {
        this.adopid = adopid;
    }

    /**
     * <p>Getter for the field <code>adopname</code>.</p>
     *
     * @return String return the adopname
     */
    public String getAdopname() {
        return adopname;
    }

    /**
     * <p>Setter for the field <code>adopname</code>.</p>
     *
     * @param adopname the adopname to set
     */
    public void setAdopname(String adopname) {
        this.adopname = adopname;
    }

    /**
     * <p>Getter for the field <code>adagreef</code>.</p>
     *
     * @return String return the adagreef
     */
    public String getAdagreef() {
        return adagreef;
    }

    /**
     * <p>Setter for the field <code>adagreef</code>.</p>
     *
     * @param adagreef the adagreef to set
     */
    public void setAdagreef(String adagreef) {
        this.adagreef = adagreef;
    }

    /**
     * <p>Getter for the field <code>qtytype</code>.</p>
     *
     * @return String return the qtytype
     */
    public String getQtytype() {
        return qtytype;
    }

    /**
     * <p>Setter for the field <code>qtytype</code>.</p>
     *
     * @param qtytype the qtytype to set
     */
    public void setQtytype(String qtytype) {
        this.qtytype = qtytype;
    }

    /**
     * <p>Getter for the field <code>c00count</code>.</p>
     *
     * @return String return the c00count
     */
    public String getC00count() {
        return c00count;
    }

    /**
     * <p>Setter for the field <code>c00count</code>.</p>
     *
     * @param c00count the c00count to set
     */
    public void setC00count(String c00count) {
        this.c00count = c00count;
    }

    /**
     * <p>Getter for the field <code>c01count</code>.</p>
     *
     * @return String return the c01count
     */
    public String getC01count() {
        return c01count;
    }

    /**
     * <p>Setter for the field <code>c01count</code>.</p>
     *
     * @param c01count the c01count to set
     */
    public void setC01count(String c01count) {
        this.c01count = c01count;
    }

    /**
     * <p>Getter for the field <code>c10count</code>.</p>
     *
     * @return String return the c10count
     */
    public String getC10count() {
        return c10count;
    }

    /**
     * <p>Setter for the field <code>c10count</code>.</p>
     *
     * @param c10count the c10count to set
     */
    public void setC10count(String c10count) {
        this.c10count = c10count;
    }

    /**
     * <p>Getter for the field <code>c11count</code>.</p>
     *
     * @return String return the c11count
     */
    public String getC11count() {
        return c11count;
    }

    /**
     * <p>Setter for the field <code>c11count</code>.</p>
     *
     * @param c11count the c11count to set
     */
    public void setC11count(String c11count) {
        this.c11count = c11count;
    }

    /**
     * <p>Getter for the field <code>c20count</code>.</p>
     *
     * @return String return the c20count
     */
    public String getC20count() {
        return c20count;
    }

    /**
     * <p>Setter for the field <code>c20count</code>.</p>
     *
     * @param c20count the c20count to set
     */
    public void setC20count(String c20count) {
        this.c20count = c20count;
    }

    /**
     * <p>Getter for the field <code>c21count</code>.</p>
     *
     * @return String return the c21count
     */
    public String getC21count() {
        return c21count;
    }

    /**
     * <p>Setter for the field <code>c21count</code>.</p>
     *
     * @param c21count the c21count to set
     */
    public void setC21count(String c21count) {
        this.c21count = c21count;
    }

    /**
     * <p>Getter for the field <code>p00count</code>.</p>
     *
     * @return String return the p00count
     */
    public String getP00count() {
        return p00count;
    }

    /**
     * <p>Setter for the field <code>p00count</code>.</p>
     *
     * @param p00count the p00count to set
     */
    public void setP00count(String p00count) {
        this.p00count = p00count;
    }

    /**
     * <p>Getter for the field <code>p01count</code>.</p>
     *
     * @return String return the p01count
     */
    public String getP01count() {
        return p01count;
    }

    /**
     * <p>Setter for the field <code>p01count</code>.</p>
     *
     * @param p01count the p01count to set
     */
    public void setP01count(String p01count) {
        this.p01count = p01count;
    }

    /**
     * <p>Getter for the field <code>p10count</code>.</p>
     *
     * @return String return the p10count
     */
    public String getP10count() {
        return p10count;
    }

    /**
     * <p>Setter for the field <code>p10count</code>.</p>
     *
     * @param p10count the p10count to set
     */
    public void setP10count(String p10count) {
        this.p10count = p10count;
    }

    /**
     * <p>Getter for the field <code>p11count</code>.</p>
     *
     * @return String return the p11count
     */
    public String getP11count() {
        return p11count;
    }

    /**
     * <p>Setter for the field <code>p11count</code>.</p>
     *
     * @param p11count the p11count to set
     */
    public void setP11count(String p11count) {
        this.p11count = p11count;
    }

    /**
     * <p>Getter for the field <code>p20count</code>.</p>
     *
     * @return String return the p20count
     */
    public String getP20count() {
        return p20count;
    }

    /**
     * <p>Setter for the field <code>p20count</code>.</p>
     *
     * @param p20count the p20count to set
     */
    public void setP20count(String p20count) {
        this.p20count = p20count;
    }

    /**
     * <p>Getter for the field <code>p21count</code>.</p>
     *
     * @return String return the p21count
     */
    public String getP21count() {
        return p21count;
    }

    /**
     * <p>Setter for the field <code>p21count</code>.</p>
     *
     * @param p21count the p21count to set
     */
    public void setP21count(String p21count) {
        this.p21count = p21count;
    }

    /**
     * <p>Getter for the field <code>t99</code>.</p>
     *
     * @return String return the t99
     */
    public String getT99() {
        return t99;
    }

    /**
     * <p>Setter for the field <code>t99</code>.</p>
     *
     * @param t99 the t99 to set
     */
    public void setT99(String t99) {
        this.t99 = t99;
    }

    /**
     * <p>Getter for the field <code>f99</code>.</p>
     *
     * @return String return the f99
     */
    public String getF99() {
        return f99;
    }

    /**
     * <p>Setter for the field <code>f99</code>.</p>
     *
     * @param f99 the f99 to set
     */
    public void setF99(String f99) {
        this.f99 = f99;
    }

    /**
     * <p>Getter for the field <code>tf</code>.</p>
     *
     * @return String return the tf
     */
    public String getTf() {
        return tf;
    }

    /**
     * <p>Setter for the field <code>tf</code>.</p>
     *
     * @param tf the tf to set
     */
    public void setTf(String tf) {
        this.tf = tf;
    }

}
