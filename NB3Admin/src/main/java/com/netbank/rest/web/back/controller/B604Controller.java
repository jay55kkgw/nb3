package com.netbank.rest.web.back.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.service.SYSPARAMService;

import fstop.aop.Authorize;
import fstop.orm.po.SYSPARAM;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 	客戶調查參數維護
 * </p>
 *
 * @author VincentHuang
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B604")
public class B604Controller {
	@Autowired
	private SYSPARAMService sysparamService;
    
    @Autowired
    private PortalSSOService portalSSOService;
    
	private String mailkey = "TxnCsssLogMail";
	private String sdate = "TxnCsssLogMailStartDate";
	private String edate = "TxnCsssLogMailEndDate";
	private String isOpen = "isOpenCsssLog";

	/**
	 * 取得查詢頁面
	 *
	 * @param model a {@link org.springframework.ui.ModelMap} object.
	 * @return a {@link java.lang.String} object.
	 */
	@GetMapping(value = { "", "/Index" })
	@Authorize(userInRoleCanQuery = true)
	public String index(ModelMap model) {
		model.addAttribute("showAdd", portalSSOService.isPermissionOK("B604", AuthorityEnum.EDIT));
		String mails = sysparamService.getByParamName(this.mailkey);
		String sdate = sysparamService.getByParamName(this.sdate);
		String edate = sysparamService.getByParamName(this.edate);
		String isOpen = sysparamService.getByParamName(this.isOpen);
		//Stored Log Forging\路徑 1:NB3Admin_20201207.pdf
		log.debug("mails>>>{},sdate>>>>{},edate>>>{},isOpen>>{}", (String) Sanitizer.logForgingStr(mails), (String) Sanitizer.logForgingStr(sdate), (String) Sanitizer.logForgingStr(edate), (String) Sanitizer.logForgingStr(isOpen));
		model.addAttribute("mails", mails);
		model.addAttribute("startDate", sdate);
		model.addAttribute("endDate", edate);
		model.addAttribute("isOpen", isOpen);
		return "B604/index";
	}

	/**
	 * 修改資料
	 *
	 * @param ADMMSGCODE 訊息代碼物件
	 * @param result     SpringMVC Binding 檢核結果
	 * @return 回傳結果map
	 */
	@PostMapping(value = "/Edit")
	@Authorize(userInRoleCanEdit = true)
	public @ResponseBody JsonResponse edit(@RequestParam String mails, @RequestParam String startDate,
			@RequestParam String endDate, @RequestParam String isOpen) {
		JsonResponse response = new JsonResponse();
		Map<String, String> errors = new HashMap<String, String>();
		try {
			if (mails.getBytes().length > 500) {
				errors.put("summary", "mail長度過長");
				response.setValidated(false);
				response.setErrorMessages(errors);
				return response;
			}
			if (startDate.getBytes().length > 4 || endDate.getBytes().length > 4) {
				errors.put("summary", "日期長度過長");
				response.setValidated(false);
				response.setErrorMessages(errors);
				return response;
			}

			response.setValidated(true);
			if (!(savemail(mails) && savesdate(startDate) && saveedate(endDate) && saveisOpen(isOpen))) {
				throw new Exception();
			}
		} catch (Exception e) {
			errors.put("summary", "系統錯誤");
			response.setValidated(false);
			response.setErrorMessages(errors);
			log.error("Edit Error>>>>{}", e.getMessage());
			return response;
		}
		return response;
	}

	private Boolean saveisOpen(String isopen) {
		try {
			SYSPARAM po = sysparamService.getParamName(this.isOpen);
			if (po == null) {
				po = new SYSPARAM();
				po.setADPARAMMEMO("客戶滿意度調查啟用與否");
				po.setADPARAMNAME(this.isOpen);
			}
			po.setADPARAMVALUE(isopen);

			sysparamService.update(po);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private Boolean savemail(String mails) {
		try {
			SYSPARAM po = sysparamService.getParamName(mailkey);
			if (po == null) {
				po = new SYSPARAM();
				po.setADPARAMMEMO("客戶滿意度調查統計信箱");
				po.setADPARAMNAME(mailkey);
			}
			po.setADPARAMVALUE(mails);

			sysparamService.update(po);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private Boolean savesdate(String sdate) {
		try {
			SYSPARAM po = sysparamService.getParamName(this.sdate);
			if (po == null) {
				po = new SYSPARAM();
				po.setADPARAMMEMO("客戶滿意度調查啟用時間");
				po.setADPARAMNAME(this.sdate);
			}
			po.setADPARAMVALUE(sdate);

			sysparamService.update(po);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private Boolean saveedate(String edate) {
		try {
			SYSPARAM po = sysparamService.getParamName(this.edate);
			if (po == null) {
				po = new SYSPARAM();
				po.setADPARAMMEMO("客戶滿意度調查停用時間");
				po.setADPARAMNAME(this.edate);
			}
			po.setADPARAMVALUE(edate);

			sysparamService.update(po);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
