package com.netbank.rest.web.back.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.util.RESTfulClientUtil;

import fstop.orm.dao.AdmMailLogDao_ex;
import fstop.orm.po.ADMMAILLOG;
import fstop.util.Sanitizer;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 電郵記錄檔 Service，使用一個畫面做 CRUD
 * 已合併AdmMailContent
 *
 * @author YU-TAN LIN
 * @version V1.0
 */

@Slf4j
@Service
public class ADMMAILLOGService extends BaseService {
    @Autowired
    private AdmMailLogDao_ex admMailLogDao;
    
    @Value("${BH.URL}")
    private String bhUrl;

    @Value("${BHMK.URL}")
    private String bhMKUrl;
    
    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo 第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy 依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param ADUSERID 身分證字號/統一編號
     * @param ADSENDSTATUS 寄送狀態，附註:為了以後增加功能用
     * @param ADQSDATE 搜尋起始時間
     * @param ADQEDATE 搜尋結束時間
     * @return a {@link com.netbank.domain.orm.core.Page} object.
     */
    public Page getByRange(int pageNo, int pageSize, String orderBy, String orderDir, String ADUSERID,
            String ADSENDSTATUS, String ADQSDATE, String ADQEDATE) {

        String _ADQSDATE = StrUtils.trim(StrUtils.trim(StrUtils.trim(ADQSDATE).replaceAll("/", "")).replaceAll(" ", ""))
                .replaceAll(":", "");
        String _ADQEDATE = StrUtils.trim(StrUtils.trim(StrUtils.trim(ADQEDATE).replaceAll("/", "")).replaceAll(" ", ""))
                .replaceAll(":", "");

        String sStart = _ADQSDATE;
        String sEnd = _ADQEDATE;

        log.debug("getByRange pageNo={}, pageSize={}, orderBy={}, orderDir={},"
                        + " ADUSERID={}, ADSENDSTATUS={}, sStart={}, sEnd={}",
                        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ADUSERID), Sanitizer.logForgingStr(ADSENDSTATUS), Sanitizer.logForgingStr(sStart), Sanitizer.logForgingStr(sEnd));

        log4Query("0", new String[][] { { "ADUSERID", ADUSERID }, { "ADSENDSTATUS", ADSENDSTATUS }, { "ADQSDATE", ADQSDATE }, { "ADQEDATE", ADQEDATE } } );
        
        return admMailLogDao.findPageDateRange(pageNo, pageSize, orderBy, orderDir, ADUSERID, ADSENDSTATUS, sStart, sEnd);
    }

    /**
     * 以ADMAILLOGID做查詢,並回傳 ADMMAILLOG 若無則回傳空值(全空)
     *
     * @param ADMAILLOGID a {@link java.lang.String} object.
     * @return ADMMAILLOG
     */
    public ADMMAILLOG getADMailContent(String ADMAILLOGID) {
        log.debug("getADMailContext MAILLOGID={}", Sanitizer.logForgingStr(ADMAILLOGID));
        try {
            log4Query("0", new String[][] { { "ADMAILLOGID", ADMAILLOGID } } );
       
            return admMailLogDao.findById(Long.parseLong(ADMAILLOGID));
        } catch (Exception e) {
            ADMMAILLOG emptyMAILCONTEXT = new ADMMAILLOG();
            return emptyMAILCONTEXT;
        }
    }
    
    /**
     * 以ADMAILLOGID做查詢,並回傳 ADMMAILLOG 若無則回傳空值(全空)
     *
     * @param ADMAILLOGID a {@link java.lang.String} object.
     * @return ADMMAILLOG
     */
    public String reSend(String ADMAILLOGID) {
        log.debug("getADMailContext MAILLOGID={}", Sanitizer.logForgingStr(ADMAILLOGID));
        String result = "";
        Integer toMsTimeout = 120;
        try {
        	//Step1 find by MAILLOGID
            ADMMAILLOG data = admMailLogDao.findById(Long.parseLong(ADMAILLOGID));
            
			//Step2 call billHunter sendMail Api
            Map<String, Object> returnMap=RESTfulClientUtil.send(data, bhUrl, toMsTimeout);
            
        	//Step3 get Result
        	result = (String) returnMap.get("sedMailResult");
        	log.debug(" Resend result={}", Sanitizer.logForgingStr(result));
        } catch (Exception e) {
            log.error("reSend Error", e);
        	result="1";
        }
        log4Create(new String[][] {{"ADMAILLOGID", ADMAILLOGID}}, result);
        return result;
    }

    /**
     * 發送郵件公告
     * @param subject       主旨
     * @param content       內容
     * @param receivers     收件者
     * @return
     */
    public Map<String, String> sendNotice(String subject, String content, List<String> receivers) {
        log.debug("sendNotice subject={}, content={}", Sanitizer.logForgingStr(subject), Sanitizer.logForgingStr(content));
        Integer toMsTimeout = 120;
        Map<String, Object> inParam = new HashMap<String, Object>();
        try {
            inParam.put("Subject", subject);
            inParam.put("Content", content);
            inParam.put("Receivers", receivers);
            
            Map<String, String> result =  RESTfulClientUtil.send(inParam, bhMKUrl, toMsTimeout);
            
            log4Create(inParam, "0");
            
            return result;
        } catch (Exception e) {
            log.error("reSend Error", e);
            Map<String, String> err = new HashMap<String, String>();
            err.put("error", e.getMessage());
            
            log4Create(inParam, "1");
            return err;
        }
    }
}
