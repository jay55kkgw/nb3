package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.MB3SysOpDao;
import fstop.orm.dao.NB3SysOpDao;
import fstop.orm.dao.NB3SysOpGroupDao;
import fstop.orm.po.MB3SYSOP;
import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.NB3SYSOPGROUP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>NB3SYSOPService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Service
public class MB3SYSOPService extends BaseService {
    @Autowired
    private MB3SysOpDao mb3SysOpDao;

    @Autowired
    private NB3SysOpGroupDao nb3SysOpGroupDao;

    /**
     * 資料庫需配合先產生一筆 ADOPID=Menu 的首筆資料
     */
    private final String ROOT_MENU = "Menu";

    /**
     * 取得所有選單資料
     *
     * @return 選單 POJO LIST
     */
    public List<MB3SYSOP> getAllMenus() {
        return mb3SysOpDao.getAll();
    }

    /**
     * 取得選單 POJO
     *
     * @param menuId 選單Id
     * @return a {@link fstop.orm.po.NB3SYSOP} object.
     */
    public MB3SYSOP getMenuByMenuId(String menuId) {
        log.debug("getMenuByMenuId, menuId={}", Sanitizer.logForgingStr(menuId));

        log4Query("0", new String[][] { { "menuId", menuId }} );
        return  mb3SysOpDao.getByADOPID(menuId);
    }

    /**
     * 取得某一層子選單
     *
     * @param pId 父選單Id
     * @return a {@link java.util.List} object.
     */
    public List<MB3SYSOP> getSubMenus(String pId) {
        return mb3SysOpDao.getByADOPGROUPID(pId);
    }

    /**
     * 取得第一層子選單
     *
     * @return a {@link java.util.List} object.
     */
    public List<MB3SYSOP> getLevel1Menus() {
        return mb3SysOpDao.getByADOPGROUPID(ROOT_MENU);
    }

    /**
     * 新增選單
     *
     * @param menu a {@link fstop.orm.po.NB3SYSOP} object.
     * @param creator a {@link java.lang.String} object.
     */
    public void insertMenu(MB3SYSOP menu, String creator) {
        menu.setLASTUSER(creator); 
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        menu.setLASTDATE(parts[0]);
        menu.setLASTTIME(parts[1]);

        // 設定排序值為目前最大值＋10
        int maxADSeq = mb3SysOpDao.getMaxADSEQ(menu.getADOPGROUPID());
        maxADSeq += 10;

        menu.setADSEQ(maxADSeq);

        mb3SysOpDao.save(menu);
        log4Create(menu, "0");
    }

    /**
     * 儲存選單資料
     *
     * @param menu 要異動的選單 POJO
     * @param editor 修改者
     */
    public void saveMenu(MB3SYSOP menu, String editor) {
    	MB3SYSOP oriMenu = mb3SysOpDao.findById(menu.getDPACCSETID());
        mb3SysOpDao.getEntityManager().detach(oriMenu);
        
        menu.setLASTUSER(editor); 

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String[] parts = dateFormat.format(date).split("-");

        menu.setLASTDATE(parts[0]);
        menu.setLASTTIME(parts[1]);

        mb3SysOpDao.update(menu);
        log4Update(oriMenu, menu, "0");
    }

    /**
     * 以遞迴的方式刪除子選單
     * @param menu
     */
    private void deleteRecursive(MB3SYSOP menu) {
        // 找到所有的子節點
        List<MB3SYSOP> subMenus = getSubMenus(menu.getADOPID());
        for ( MB3SYSOP subMenu : subMenus ) {
            deleteRecursive(subMenu);
        }
    } 

    /**
     * 刪除選單
     *
     * @param id 要刪除的選單主鍵值
     */
    public void deleteMenu(int id) {
    	MB3SYSOP oriMenu = mb3SysOpDao.findById(id);
    	MB3SYSOP tMenu = mb3SysOpDao.get(id);
        deleteRecursive(tMenu);
        mb3SysOpDao.removeById(id);  
        log4Delete(oriMenu, "0");
    }

    /**
     * 儲存選單的排序值
     *
     * @param sortedMenus 要排序的選單，Key=DPACCSETID, Value=排序值
     */
    public void sortMenu(Map<String, Integer> sortedMenus) {
        for (Entry<String, Integer> sortedMenu : sortedMenus.entrySet()) {
        	mb3SysOpDao.updateADSEQ(sortedMenu.getValue(), sortedMenu.getKey());
        }
    }

    /**
     * <p>moveMenu.</p>
     *
     * @param id a int.
     * @param pId a {@link java.lang.String} object.
     */
    public void moveMenu(int id, String pId) {
    	MB3SYSOP current = mb3SysOpDao.findById(id);
        current.setADOPGROUPID(pId);
        mb3SysOpDao.update(current);
    }

    /**
     * <p>getROOT_MENU.</p>
     *
     * @return the ROOT_MENU
     */
    public String getROOT_MENU() {
        return ROOT_MENU;
    }

    /**
     * <p>取得所有交易群組大類</p>
     * @return  NB3SYSOPGROUP 清單
     */
    public List<NB3SYSOPGROUP> getParentGroups() {
        return nb3SysOpGroupDao.getParentGroups();
    }

    /**
     * <p>取得所有交易群組小類</p>
     * @param parentId  父交易群組大類
     * @return          NB3SYSOPGROUP 清單
     */
    public List<NB3SYSOPGROUP> getSubGroups(String parentId) {
        return nb3SysOpGroupDao.getChildGroups(parentId);
    }

    /**
     * <p>取得所有交易群組（無子分類者）</p>
     * @return          NB3SYSOPGROUP 清單
     */
    public List<NB3SYSOPGROUP> getLeafGroups() {
        return nb3SysOpGroupDao.getLeafGroups();
    }
}
