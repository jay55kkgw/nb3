package com.netbank.rest.web.back.service;

/**
 * 案件權限 ENUM
 *
 * @author chiensj
 * @version V1.0
 */
public enum AuthorityEnum {
    QUERY,          // 只能查詢
    EDIT,           // 只能編輯
    REVIEW,         // 只能審核
    NONE,           // 已結案
}
