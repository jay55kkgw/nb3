package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import fstop.orm.dao.AdmOpLogDao;
import fstop.orm.po.ADMOPLOG;
import lombok.extern.slf4j.Slf4j;

/**
 * 基礎類別，主要用來記錄 ADMOPLOG
 */
@Slf4j
public abstract class BaseService {
    @Autowired
    private AdmOpLogDao admOpLogDao;

    @Autowired
    private PortalSSOService portalSSOService;

    public static final String CREATE = "1";
    public static final String UPDATE = "2";
    public static final String DELETE = "3";
    public static final String INQUERY = "4";
    public static final String SEND = "5";			// 送件
    public static final String APPROVE = "6";		// 覆核
    public static final String REJECT = "7";		// 退件
    public static final String SETUP = "8";			// 設定

    /**
     * 記錄查詢記錄
     * @param excode        交易錯誤代碼
     */
    protected void log4Query(String excode) {
        try {
            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(INQUERY);
            po.setADBCHCON("{}");
            po.setADACHCON("{}");
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
        } catch (Exception e) {
            log.error("log4Query error", e);
        }
    }

    /**
     * 記錄查詢記錄
     * @param excode        交易錯誤代碼    
     * @param queryParams   查詢參數
     */
    protected void log4Query(String excode, String[][] queryParams) {
        try {
            Map<String, String> queryMap = Stream.of(queryParams).collect(Collectors.toMap(data -> data[0], data -> data[1]));

            Gson gson = new Gson();
            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(INQUERY);
            po.setADBCHCON(gson.toJson(queryMap));
            po.setADACHCON("{}");
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
        } catch (Exception e) {
            log.error("log4Query error", e);
        }
    }

    /**
     * 記錄新增後資料
     * @param <T>           泛型
     * @param after         新增後 PO
     * @param excode        交易錯誤代碼
     */
    protected <T> void log4Create(T after, String excode) {
        try {
            Gson gson = new Gson();

            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(CREATE);
            po.setADBCHCON("{}");
            po.setADACHCON(gson.toJson(after));
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
        } catch (Exception e) {
            log.error("log4Create error", e);
        }   
    }

    /**
     * 記錄新增後資料
     * @param after         新增後 PO JSON String
     * @param excode        交易錯誤代碼
     */
    protected void log4Create(String after, String excode) {
        try {
            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(CREATE);
            po.setADBCHCON("{}");
            po.setADACHCON(after);
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
        } catch (Exception e) {
            log.error("log4Create error", e);
        }   
    }

    /**
     * 記錄異動前後資料
     * @param <T>           泛型
     * @param before        修改前 PO
     * @param after         修改後 PO
     * @param excode        交易錯誤代碼
     */
    protected <T> void log4Update(T before, T after, String excode) {
        try {
            Gson gson = new Gson();

            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(UPDATE);
            po.setADBCHCON(gson.toJson(before));
            po.setADACHCON(gson.toJson(after));
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
        } catch (Exception e) {
            log.error("log4Update error", e);
        }
    }

    /**
     * 記錄異動前後資料
     * @param <T>           泛型
     * @param before        修改前 PO（已轉成 JSON String）
     * @param after         修改後 PO
     * @param excode        交易錯誤代碼
     */
    protected <T> void log4Update(String before, T after, String excode) {
        try {
            Gson gson = new Gson();

            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(UPDATE);
            po.setADBCHCON(before);
            po.setADACHCON(gson.toJson(after));
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
        } catch (Exception e) {
            log.error("log4Update error", e);
        }
    }

    /**
     * 記錄刪除前資料
     * @param <T>           泛型
     * @param before        刪除前 PO
     * @param excode        交易錯誤代碼
     */
    protected <T> void log4Delete(T before, String excode) {
        try {
            Gson gson = new Gson();

            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(DELETE);
            po.setADBCHCON(gson.toJson(before));
            po.setADACHCON("{}");
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
        } catch (Exception e) {
            log.error("log4Delete error", e);
        }
    }
    
    protected void log4Delete(String before, String excode) {
        try {
            Gson gson = new Gson();

            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(DELETE);
            po.setADBCHCON(before);
            po.setADACHCON("{}");
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
        } catch (Exception e) {
            log.error("log4Delete error", e);
        }
    }
    
    /**
     * 記錄送審資料
     * @param caseSN	流程案號
     * @param excode	結果代碼
     */
    protected void log4Send(String caseSN, String excode) {
    	try {
    		Gson gson = new Gson();

    		Map<String, String> flowData = new HashMap<String, String>();
    		flowData.put("caseSN", caseSN);
    		
            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(SEND);
            po.setADBCHCON(gson.toJson(flowData));
            po.setADACHCON("{}");
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
    	} catch (Exception e) {
            log.error("log4Send error", e);
        }
    }
    
    /**
     * 記錄審核資料
     * @param caseSN	流程案號
     * @param excode	結果代碼
     */
    protected void log4Approve(String caseSN, String excode) {
    	try {
    		Gson gson = new Gson();
    		
    		Map<String, String> flowData = new HashMap<String, String>();
    		flowData.put("caseSN", caseSN);
    		
            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(APPROVE);
            po.setADBCHCON(gson.toJson(flowData));
            po.setADACHCON("{}");
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
    	} catch (Exception e) {
            log.error("log4Approve error", e);
        }
    }
    
    /**
     * 記錄退件資料
     * @param caseSN	流程案號
     * @param excode	結果代碼
     */
    protected void log4Reject(String caseSN, String excode) {
    	try {
    		Gson gson = new Gson();

    		Map<String, String> flowData = new HashMap<String, String>();
    		flowData.put("caseSN", caseSN);
    		
            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(REJECT);
            po.setADBCHCON(gson.toJson(flowData));
            po.setADACHCON("{}");
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
    	} catch (Exception e) {
            log.error("log4Approve error", e);
        }
    }
    
    /**
     * 記錄設定前後資料
     * @param <T>           泛型
     * @param after         修改後 PO（已轉成 JSON String）
     * @param excode        交易錯誤代碼
     */
    protected <T> void log4Setup(String after, String excode) {
        try {
            Gson gson = new Gson();

            ADMOPLOG po = getAdmOpLog();
            po.setADOPITEM(SETUP);
            po.setADBCHCON("{}");
            po.setADACHCON(after);
            po.setADEXCODE(excode);

            admOpLogDao.save(po);
        } catch (Exception e) {
            log.error("log4Setup error", e);
        }
    }
    
    /**
     * 取得 ADMOPLOG PO
     * @return
     */
    private ADMOPLOG getAdmOpLog() {
        ADMOPLOG po = new ADMOPLOG();

        DateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss-SSS");
        String[] parts = format.format(new Date()).split("-");
        String txno = parts[0]+parts[1]+parts[2]+portalSSOService.getLoginUserId();

        if ( txno.length()>24 ) {
            po.setADTXNO(txno.substring(0,23));
        } else {
            po.setADTXNO(txno);
        }
        
        po.setADUSERID(portalSSOService.getLoginUserId());
        po.setADTXDATE(parts[0]);
        po.setADTXTIME(parts[1]);
        po.setADTXITEM("");             // ? 不知道要放什麼
        po.setADLOGGING("");            // ? 不知道要放什麼

        // 由 URI 找對應的 ADOPID
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        
        // URI 找對應的 ADOPID, URI 有可能是 
        // /B101/Index?t=xx or /nb3admin/B101/Index?t=xx
        // /B101/Index/XXX?t=xx or /nb3admin/B101/Index?t=xx
        // /nb3admin/B101/Index?t=xx or /nb3admin/B101/Index?t=xx
        // /nb3admin/B101/Index/XXX?t=xx or /nb3admin/B101/Index?t=xx
        String contextPath = request.getContextPath();
		String requestADOPID = request.getRequestURI().substring(1);
		String[] tmps = requestADOPID.split("/");
		if ( contextPath.compareToIgnoreCase("") == 0 ) {
			requestADOPID = tmps[0];
		} else {
			requestADOPID = tmps[1];
		}

        po.setADOPID(requestADOPID);

        // 取得前端使用者 IP
        String ipAddress = request.getHeader("X-FORWARDED-FOR");  
        if (ipAddress == null) {  
            ipAddress = request.getRemoteAddr();  
        }

        po.setADUSERIP(ipAddress);

        return po;
    }
}