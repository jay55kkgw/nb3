package com.netbank.rest.web.back.service;

import com.netbank.domain.orm.core.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fstop.orm.dao.UserActionLogDao;
import fstop.orm.po.USERACTIONLOG;

/**
 * 
 */
@Service
public class UserActionLogService {
    @Autowired
    private UserActionLogDao userActionLogDao;
    
    /**
     * 分頁查詢
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param orderDir
     * @param startTime
     * @param endTime
     * @param loginUserId
     * @return
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String startTime, String endTime, String loginUserId) {
        Page page = userActionLogDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, startTime, endTime, loginUserId);
        
        return page;
    }

    /**
     * 依 Id 取得 UserActionLog POJO
     * @param id        Id
     * @return          POJO
     */
    public USERACTIONLOG getById(int id) {
        return userActionLogDao.findById(id);
    }
}