package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.netbank.rest.web.back.model.B708Result;
import com.netbank.rest.web.back.service.NMBReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import aj.org.objectweb.asm.Type;
import fstop.aop.Authorize;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("/B708")
public class B708Controller {
    @Autowired
    private NMBReportService nmbReportService;
    
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        
        return "B708/index";
    }

    @PostMapping(value = "/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody List<List<B708Result>> query(@RequestParam String Type, @RequestParam String QueryDt) {
        try {
            if ( QueryDt.isEmpty() ) {
                Calendar calendar = Calendar.getInstance(); 
                Date now = calendar.getTime();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                QueryDt = formatter.format(now);
            } else {
                QueryDt = QueryDt.replace("/", "");
            }
            //20191219-Danny:Missing Content Security Policy
            List<List<B708Result>> listEntity = nmbReportService.queryB708(Type, QueryDt);
			
			return listEntity;
        } catch (Exception e) {
            log.error("query error", e);
            List<List<B708Result>> results = new ArrayList<List<B708Result>>();

            return results;
        }
    }

    @GetMapping(value = { "/Report" })
    @Authorize(userInRoleCanQuery = true)
    public void report(@RequestParam String Type, @RequestParam String QueryDt, HttpServletResponse response) throws IOException {
        
        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"B708Report.csv\"");
        
        OutputStream resOs = response.getOutputStream();
        resOs.write('\ufeef'); // emits 0xef
        resOs.write('\ufebb'); // emits 0xbb
        resOs.write('\ufebf'); // emits 0xbf

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = new CSVWriter(outputWriter, CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_ESCAPE_CHARACTER);
        //20191204-Danny-CGI Reflected XSS All Clients
        QueryDt = (String)Sanitizer.escapeHTML(QueryDt);
       

        try {
            if ( QueryDt.isEmpty() ) {
                Calendar calendar = Calendar.getInstance(); 
                Date now = calendar.getTime();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                QueryDt = formatter.format(now);
            } else {
                QueryDt = QueryDt.replace("/", "");
            }
            List<List<B708Result>> results = nmbReportService.queryB708(Type, QueryDt);
            
            if ( Type.compareToIgnoreCase("Stat") == 0 ) {
            	String[][] headers = { {"峰日", "查詢筆數", "非查詢筆數"}, {"峰月", "查詢筆數", "非查詢筆數"}, {"峰時", "查詢筆數", "非查詢筆數"} };
                for ( int i=0; i<results.size(); i++ ) {
                	writer.writeNext(headers[i]);
                	for ( B708Result r : results.get(i) ) {
                		writer.writeNext(new String[] { String.valueOf(r.getItem()), String.valueOf(r.getQueryCnt()), String.valueOf(r.getNonQueryCnt()) });
                	}
                }
            } else {
            	String[][] headers = { {"峰時", "查詢筆數", "非查詢筆數"} };
                for ( int i=0; i<results.size(); i++ ) {
                	writer.writeNext(headers[i]);
                	for ( B708Result r : results.get(i) ) {
                		writer.writeNext(new String[] { String.valueOf(r.getHour()), String.valueOf(r.getQueryCnt()), String.valueOf(r.getNonQueryCnt()) });
                	}
                }
            }
            
            writer.close();
        } catch (Exception e) {
            log.error("Error", e);
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "發生錯誤請再試一次!!";
            String[] nextLine = { (String)Sanitizer.logForgingStr(message), "", "" };
            writer.writeNext(nextLine);
            writer.close();
            return;
        }
    }
}