package com.netbank.rest.web.back.service;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmBranchDao;
import fstop.orm.po.ADMBRANCH;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 網銀分行維護服務
 *
 * @author jinhanhuang
 * @version V1.0
 */
@Slf4j
@Service
public class ADMBRANCHService extends BaseService {
    @Autowired
    private AdmBranchDao admBranchDao;

    /**
     * 分頁查詢
     *
     * @param pageNo a int.
     * @param pageSize a int.
     * @param orderBy a {@link java.lang.String} object.
     * @param orderDir a {@link java.lang.String} object.
     * @param BHADTYPE a {@link java.lang.String} object.
     * @param BHNAME a {@link java.lang.String} object.
     * @param BHCOUNTY a {@link java.lang.String} object.
     * @return a {@link com.netbank.domain.orm.core.Page} object.
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String ADBRANCHID, String ADBRANCHNAME) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, ADBRANCHID={}, ADBRANCHNAME={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(ADBRANCHID), Sanitizer.logForgingStr(ADBRANCHNAME));

        log4Query("0", new String[][] { { "ADBRANCHID", ADBRANCHID }, { "ADBRANCHNAME", ADBRANCHNAME } } );
        return admBranchDao.findPageDataByLike(pageNo, pageSize, orderBy, orderDir, ADBRANCHID, ADBRANCHNAME);
    }

    /**
     * 取得資料
     *
     * @param bhid 據點 ID
     * @return 據點 POJO
     */
    public ADMBRANCH getByADBRANCHID(String ADBRANCHID) {
        log.debug("getByADBRANCHID ADBRANCHID={}", Sanitizer.logForgingStr(ADBRANCHID));

        log4Query("0", new String[][] { { "ADBRANCHID", ADBRANCHID }} );
        return admBranchDao.findById(ADBRANCHID);
    }

    /**
     * 新增據點資料
     *
     * @param branch  據點 POJO
     * @param creator 編輯者
     * @param creatorName 編輯者姓名
     */
    public void insertBranch(ADMBRANCH branch ,String creatorName) {

        Map<String, String> map = getTime();
        
        branch.setLASTUSER(creatorName);
        branch.setLASTDATE(map.get("lastdate"));
        branch.setLASTTIME(map.get("lasttime"));
        
        admBranchDao.save(branch);
        log4Create(branch, "0");
    }

    /**
     * 儲存分行資料
     *
     * @param branch 據點 POJO
     * @param editor 編輯者
     * @param editorName a {@link java.lang.String} object.
     */
    public void saveBranch(ADMBRANCH branch, String editorName) {
    	ADMBRANCH oriBranch = admBranchDao.findById(branch.getADBRANCHID());
    	admBranchDao.getEntityManager().detach(oriBranch);
    	
    	Map<String, String> map = getTime();
         
        branch.setLASTUSER(editorName);
        branch.setLASTDATE(map.get("lastdate"));
        branch.setLASTTIME(map.get("lasttime"));
    	
        admBranchDao.update(branch);
        log4Update(oriBranch, branch, "0");
    }

    /**
     * 刪除據點資料
     *
     * @param ADBRANCHID 分行代號
     */
    public void deleteBranch(String ADBRANCHID) {
        ADMBRANCH oriBranch = admBranchDao.findById(ADBRANCHID);
        admBranchDao.removeById(ADBRANCHID);
        log4Delete(oriBranch, "0");
    }
    
    public Map<String, String> getTime(){
    	 SimpleDateFormat sdfD = new SimpleDateFormat("yyyyMMdd");
         SimpleDateFormat sdfT = new SimpleDateFormat("HHmmss");
         
         String lastdate = sdfD.format(new Date());
         String lasttime = sdfT.format(new Date());
         
         Map<String, String> map = new HashMap<String, String>();
         map.put("lastdate", lastdate);
         map.put("lasttime", lasttime);
         
         return map;
    }
}
