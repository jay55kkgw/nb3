package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.service.SysBatchService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import fstop.aop.Authorize;
import fstop.orm.po.SYSBATCH;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 批次執行結果查詢
 * @author chien
 *
 */
@Controller
@Slf4j
@RequestMapping("/B404")
public class B404Controller {
	@Autowired
	private SysBatchService sysBatchService;
	
	@GetMapping(value = { "", "/Index" })
	@Authorize(userInRoleCanQuery = true)
	public String index(ModelMap model) {
		
		return "B404/index";
	}
	
	@SuppressWarnings("unchecked")
    @PostMapping(value="/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(HttpServletRequest request, HttpServletResponse response ) {
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = sysBatchService.getAll(jqDataTableRq.getPage(), jqDataTableRq.getLength(), jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir());
            
        List<SYSBATCH> jobs=(List<SYSBATCH>)page.getResult();
        List<SYSBATCH> sjobs= new ArrayList<SYSBATCH>();
        for (SYSBATCH job : jobs) {
        	SYSBATCH sjob = new SYSBATCH();

        	Sanitizer.escape4Class(job, sjob);
        	sjobs.add(sjob);
        }
        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), sjobs);

        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

}
