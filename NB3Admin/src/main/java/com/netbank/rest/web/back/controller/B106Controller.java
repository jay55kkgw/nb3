package com.netbank.rest.web.back.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.netbank.rest.web.back.model.B106ViewModel;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.service.SYSPARAMDATAService;
import com.netbank.rest.web.back.service.SYSPARAMService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import fstop.aop.Authorize;
import fstop.orm.po.SYSPARAM;
import fstop.orm.po.SYSPARAMDATA;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 其他資料維護 Controller，使用一個畫面做 CRUD
 *
 * @author 楷翔
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B106")
public class B106Controller {
    @Autowired
    private SYSPARAMDATAService sysPARAMDATAService;
    
    @Autowired
    private SYSPARAMService sysPARAMService;

    @Autowired
    private PortalSSOService portalSSOService;
    
    /**
     * 取得其他資料維護首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value={"", "/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) { 
        SYSPARAMDATA data = sysPARAMDATAService.getByADPKID("NBSYS");
        model.addAttribute("Data", data);  
        
        model.addAttribute("ADBANK3MAIL", sysPARAMService.getByParamName("ADBANK3MAIL"));
        model.addAttribute("TXNTWAMTMAIL", sysPARAMService.getByParamName("TXNTWAMTMAIL"));
        
        if ( portalSSOService.isPermissionOK("B106", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }  
        return "B106/index";
    }
    
    /**
     * 修改其他資料資料
     *
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     * @param sysparamdata a {@link com.netbank.rest.web.back.model.B106ViewModel} object.
     */
    @PostMapping(value="/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid B106ViewModel sysparamdata, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage,
                            (err1, err2) -> {
                                return err1.concat(";").concat(err2);
                            })
                    );
                
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                //取原本的值
                SYSPARAMDATA orgsysparamdata = new SYSPARAMDATA();
                orgsysparamdata= sysPARAMDATAService.getByADPKID((String)Sanitizer.logForgingStr(sysparamdata.getADPK()));
                //替換掉修改的值     
                orgsysparamdata.setADSESSIONTO(sysparamdata.getADSESSIONTO());
                orgsysparamdata.setADOPMAIL(sysparamdata.getADOPMAIL());
                orgsysparamdata.setADAPMAIL(sysparamdata.getADAPMAIL());
                orgsysparamdata.setADSECMAIL(sysparamdata.getADSECMAIL());
                orgsysparamdata.setADSPMAIL(sysparamdata.getADSPMAIL());
                orgsysparamdata.setADFDSVRIP(sysparamdata.getADFDSVRIP());
                orgsysparamdata.setADMAILSVRIP(sysparamdata.getADMAILSVRIP());
                orgsysparamdata.setADPARKSVRIP(sysparamdata.getADPARKSVRIP());
                orgsysparamdata.setADARSVRIP(sysparamdata.getADARSVRIP());
                orgsysparamdata.setADFDRETRYTIMES(sysparamdata.getADFDRETRYTIMES());
                orgsysparamdata.setADPKRETRYTIMES(sysparamdata.getADPKRETRYTIMES());           
                orgsysparamdata.setADRESENDTIMES(sysparamdata.getADRESENDTIMES());
               // orgsysparamdata.setADLEADBYTE(sysparamdata.getADLEADBYTE());
                //orgsysparamdata.setADMAILDOMAIN(sysparamdata.getADMAILDOMAIN());    
                
                // 以下為 2020/8/24 景森發現有缺漏的
                orgsysparamdata.setADGDMAIL(sysparamdata.getADGDMAIL());
                orgsysparamdata.setADGDTXNMAIL(sysparamdata.getADGDTXNMAIL());
                
                SYSPARAM bank3Mail = sysPARAMService.getParamName("ADBANK3MAIL");
                String bank3MailOldValue = "";
                if ( bank3Mail == null ) {
                	bank3Mail = new SYSPARAM();
                	bank3Mail.setADPARAMNAME("ADBANK3MAIL");
                	bank3Mail.setADPARAMMEMO("Bank3.0線上申請統計通知 Email");
                } else {
                	bank3MailOldValue = bank3Mail.getADPARAMVALUE();
                }
                bank3Mail.setADPARAMVALUE(sysparamdata.getADBANK3MAIL());
                
                SYSPARAM txnAmtMail= sysPARAMService.getParamName("TXNTWAMTMAIL");
                String txnAmtOldValue = "";
                if ( txnAmtMail == null ) {
                	txnAmtMail = new SYSPARAM();
                	txnAmtMail.setADPARAMNAME("TXNTWAMTMAIL");
                	txnAmtMail.setADPARAMMEMO("次營業日跨行轉帳總金額通知 Email");
                } else {
                	txnAmtOldValue = txnAmtMail.getADPARAMVALUE();
                }
                txnAmtMail.setADPARAMVALUE(sysparamdata.getTXNTWAMTMAIL());
                
                sysPARAMDATAService.saveSysparmDataB106(orgsysparamdata, bank3Mail, txnAmtMail, bank3MailOldValue, txnAmtOldValue, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            log.error("edit Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            //Information Exposure Through an Error Message
            errors.put("summary", "修改存檔發生錯誤，請確認資料是否有更新成功!!");

            response.setValidated(false);
            response.setErrorMessages(errors);
            //Information Exposure Through an Error Message
            return response;
        }
        
        return response;
    }
}
