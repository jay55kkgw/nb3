package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.B402Model;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ADMMSGCODEService;
import com.netbank.rest.web.back.service.TXNTWSCHPAYDATAService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.po.ADMMSGCODE;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

/**
 * 臺幣預約失敗人工處理
 * 
 * @author Alison
 */
@Controller
@Slf4j
@RequestMapping("/B402")
public class B402Controller {
	@Autowired
	private TXNTWSCHPAYDATAService txnTWSchPayDataService;

	@Autowired
	private ADMMSGCODEService admMsgCodeService;

	/**
	 * 取得查詢頁
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping(value = { "", "/Index" })
	@Authorize(userInRoleCanQuery = true)
	public String index(ModelMap model) {
		try {			
			// 台幣可人工重送訊息代碼
			List<ADMMSGCODE> txs = admMsgCodeService.findTwdManulResend();
			model.addAttribute("txs", txs);
			
			return "B402/index";
		}catch(Exception e) {
			log.error("B402 index error...",e);
			return "Home/index";
		}
	}

	/**
	 * 分頁查詢--台幣
	 * 
	 * @param USERID   身分證字號/統一編號, 已不使用, 留著做參考
	 * @param request
	 * @param response
	 * @return
	 */
	@PostMapping(value = "/QueryNTD", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@SuppressWarnings("unchecked")
	@Authorize(userInRoleCanQuery = true)
	public @ResponseBody JQueryDataTableResponse queryNTD(@RequestParam String USERID, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			
			log.debug("query USERID={}", Sanitizer.logForgingStr(USERID));
			
			JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			Date date = new Date();
			String today = dateFormat.format(date);
			
			// 只查失敗的預約交易 STATUS＝"1","2"
			Page page = txnTWSchPayDataService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
					jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), today, today,
					(String) Sanitizer.escapeHTML(USERID), "1,2");
			
			// 台幣可人工重送訊息代碼
			List<ADMMSGCODE> txs = admMsgCodeService.findTwdManulResend();
			
			List<TXNTWSCHPAYDATA> txnTwSchPayDatas = (List<TXNTWSCHPAYDATA>) page.getResult();
			List<TXNTWSCHPAYDATA> stxnTwSchPayDatas = new ArrayList<TXNTWSCHPAYDATA>();
			for (TXNTWSCHPAYDATA txnTwSchpayData : txnTwSchPayDatas) {
				TXNTWSCHPAYDATA stxnTWSchpayData = new TXNTWSCHPAYDATA();
				
				// 取得及判斷預約失敗是否可人工重送
				long matchCnt = txs.stream()
						.filter(r -> r.getADMCODE().compareToIgnoreCase(txnTwSchpayData.getDPEXCODE()) == 0).count();
				if (matchCnt > 0) {
					Sanitizer.escape4Class(txnTwSchpayData, stxnTWSchpayData);
					// 取回DPTITAINFO資料中的SEQTRN 廢棄
					Map<String, Object> javaRootMapObject = new Gson().fromJson(txnTwSchpayData.getDPTITAINFO(), Map.class);
					String seqTRN = (String) javaRootMapObject.get("SEQTRN");
					
					List<TXNTWRECORD> txTwRecords = null;
					if (seqTRN != null && !seqTRN.isEmpty()) {
						// 找出TXNTWRECORD中的重送記錄
						txTwRecords = txnTWSchPayDataService.getTXNTWRECORDs(txnTwSchpayData.getDPTXDATE(),
								txnTwSchpayData.getDPWDAC(), txnTwSchpayData.getDPSVAC(), seqTRN,
								txnTwSchpayData.getDPTXAMT());
					}
					
					if (txTwRecords != null && txTwRecords.size() > 0) {
						stxnTWSchpayData.setRESENDCOUNT(txTwRecords.size() - 1);
					} else {
						stxnTWSchpayData.setRESENDCOUNT(0);
					}
					stxnTwSchPayDatas.add(stxnTWSchpayData);
				}
			}
			
			JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
					page.getTotalCount(), page.getTotalCount(), stxnTwSchPayDatas);
			// 20191115-Danny-Reflected XSS All Clients
			JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
			Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);
			return sjqDataTableRs;
		}catch(Exception e) {
			log.error("B402 QueryNTD error...",e);
			JQueryDataTableResponse sjqDataTableRs = JQueryDataTableHelper.GetResponse(0, 0, 0, new ArrayList<TXNTWSCHPAYDATA>());
			return sjqDataTableRs;
		}

	}

	/**
	 * 分頁查詢--台幣
	 * 
	 * @param USERID   身分證字號/統一編號
	 * @param request
	 * @param response
	 * @return
	 */
	@PostMapping(value = "/IndexQueryNTD", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Authorize(userInRoleCanQuery = true)
	public @ResponseBody List<TXNTWSCHPAYDATA> indexQueryNTD(@RequestParam String USERID, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			
			log.debug("query USERID={}", Sanitizer.logForgingStr(USERID));
			
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			Date date = new Date();
			String today = dateFormat.format(date);
			
			// 只查失敗的預約交易 STATUS＝"1","2"
			List<TXNTWSCHPAYDATA> txnTwSchPayDatas = txnTWSchPayDataService.find(today, today,
					(String) Sanitizer.escapeHTML(USERID));
			
			// 台幣可人工重送訊息代碼
			List<ADMMSGCODE> txs = admMsgCodeService.findTwdManulResend();
			
			List<TXNTWSCHPAYDATA> stxnTwSchPayDatas = new ArrayList<TXNTWSCHPAYDATA>();
			for (TXNTWSCHPAYDATA txnTwSchpayData : txnTwSchPayDatas) {
				TXNTWSCHPAYDATA stxnTWSchpayData = new TXNTWSCHPAYDATA();
				Sanitizer.escape4Class(txnTwSchpayData, stxnTWSchpayData);
				stxnTwSchPayDatas.add(stxnTWSchpayData);
			}
			List<TXNTWSCHPAYDATA> rsultstxnTwSchPayDatas = new ArrayList<TXNTWSCHPAYDATA>();
			
			Sanitizer.escape4Class(stxnTwSchPayDatas, rsultstxnTwSchPayDatas);
			return rsultstxnTwSchPayDatas;
		}catch(Exception e) {
			log.error("B402 IndexQueryNTD error...",e);
			return new ArrayList<TXNTWSCHPAYDATA>();
		}

	}

	@PostMapping(value = "/Resend", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Authorize(userInRoleCanQuery = true)
	public @ResponseBody Map resend(@RequestParam String dpschno, @RequestParam String dpschtxdate,
			@RequestParam String dpuserid, @RequestParam String adtxno) {
		// 20191115-Danny-Reflected XSS All Clients\路徑 22:
		dpschno = (String) Sanitizer.escapeHTML(dpschno);
		dpschtxdate = (String) Sanitizer.escapeHTML(dpschtxdate);
		dpuserid = (String) Sanitizer.escapeHTML(dpuserid);

		Map<String, String> response = new HashMap<String, String>();
		try {

			String msg = txnTWSchPayDataService.reSend((String) Sanitizer.escapeHTML(dpschno),
					(String) Sanitizer.escapeHTML(dpschtxdate), (String) Sanitizer.escapeHTML(dpuserid),
					(String) Sanitizer.escapeHTML(adtxno));
			response.put("status", msg);
		} catch (Exception e) {
			// 20191212-Danny-Information Exposure Through an Error Message
			String message = "發生錯誤，請再試一次!!";
			log.error("Query error", e);
			response.put("status", (String) Sanitizer.logForgingStr(message));
			return response;
		}

		return response;
	}

	/**
	 * 重覆交易檢核
	 * 
	 * @param dpschno
	 * @param dpschtxdate
	 * @param dpuserid
	 * @param adtxno
	 * @return
	 */
	@PostMapping(value = "/DuplicateCheck", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Authorize(userInRoleCanQuery = true)
	public @ResponseBody Map duplicateCheck(@RequestParam String dpschno, @RequestParam String dpschtxdate,
			@RequestParam String dpuserid, @RequestParam String adtxno) {
		dpschno = (String) Sanitizer.escapeHTML(dpschno);
		dpschtxdate = (String) Sanitizer.escapeHTML(dpschtxdate);
		dpuserid = (String) Sanitizer.escapeHTML(dpuserid);

		Map<String, Integer> response = new HashMap<String, Integer>();
		try {

			List<TXNTWRECORD> txns = txnTWSchPayDataService.getDuplicateTxnCounts(
					(String) Sanitizer.escapeHTML(dpschno), (String) Sanitizer.escapeHTML(dpschtxdate),
					(String) Sanitizer.escapeHTML(dpuserid), (String) Sanitizer.escapeHTML(adtxno));
			response.put("counts", txns.size());
			if (txns.size() > 0) {
				response.put("time", Integer.parseInt(txns.get(0).getDPTXTIME()));
			}
		} catch (Exception e) {
			// 20191212-Danny-Information Exposure Through an Error Message
			String message = "發生錯誤，請再試一次!!";
			log.error("Query error", e);
			response.put("counts", -1);
			return response;
		}

		return response;
	}

	/**
	 * <p>
	 * statusDialog.
	 * </p>
	 *
	 * @param model a {@link org.springframework.ui.ModelMap} object.
	 * @param id    a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	@PostMapping(value = "/MsgDialog/{id}")
	public String msgDialog(@PathVariable String id, ModelMap model) {
		String MsgOut = "訊息說明 : " + admMsgCodeService.getMsgIn(id);
        String MsgIn = "客戶訊息 : " + admMsgCodeService.getMsg(id);
        model.addAttribute("ErrMsg", MsgOut + "<br>" + MsgIn);
		return "B205/msgPartial";
	}

	/**
	 * 下載 CSV
	 *
	 * @param entity   a {@link com.netbank.rest.web.back.model.B701Model} object.
	 * @param model    a {@link org.springframework.ui.ModelMap} object.
	 * @param response a {@link javax.servlet.http.HttpServletResponse} object.
	 * @throws java.io.IOException
	 */
	@Authorize(userInRoleCanQuery = true)
	@PostMapping(value = { "/DownloadtoCSV" })
	public void DownloadtoCSV(@RequestParam String USERID, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		OutputStream resOs = null;
    	OutputStream buffOs = null;
    	OutputStreamWriter outputWriter = null;
    	CSVWriter writer = null;
    	
		try {			
			response.setContentType("text/csv;charset=utf-8");
			response.setHeader("Content-Disposition", "attachment; filename=\"ExportSys.csv\"");
			resOs = response.getOutputStream();
			resOs.write('\ufeef'); // emits 0xef
			resOs.write('\ufebb'); // emits 0xbb
			resOs.write('\ufebf'); // emits 0xbf
			
			buffOs = new BufferedOutputStream(resOs);
			outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
			writer = new CSVWriter(outputWriter);
			String[] headers = { "預約編號", "轉帳日期", "轉帳時間", "身分證/營利事業統一編號", "交易代號", "轉出帳號", "轉入行", "轉入帳號/繳費稅代號", "轉帳金額",
					"錯誤代碼", "錯誤訊息", "姓名", "電話" };
			writer.writeNext(headers);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			Date date = new Date();
			String today = dateFormat.format(date);
			
			// 只查失敗的預約交易 STATUS＝"1","2"
			List<B402Model> queryData = txnTWSchPayDataService.findCSVByB402(today, today, (String) Sanitizer.escapeHTML(USERID));
			
			for (B402Model item : queryData) {
				String[] nextLine = { item.getDPSCHNO(), item.getLASTDATE(), "\t" + item.getLASTTIME(), item.getDPUSERID(),
						item.getADOPID(), "\t" + item.getDPWDAC(), "\t" + item.getDPSVBH(), "\t" + item.getDPSVAC(),
						item.getDPTXAMT(), item.getDPEXCODE(), item.getADMSGIN(), item.getDPUSERNAME(),
						"\t" + item.getEINPHONE() };
				
				writer.writeNext(nextLine);
			}
		}catch(Exception e){
			log.error("B402 DownloadtoCSV error...",e);
		}finally {			
			if (writer != null) {
  	          try {
  	        	  writer.close();
  	          } catch (IOException e) {
  	             log.error("B402 DownloadtoCSV error...",e);
  	          }
  	       }
		}
	}

}