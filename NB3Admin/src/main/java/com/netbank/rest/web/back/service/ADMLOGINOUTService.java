package com.netbank.rest.web.back.service;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.AdmLogInOutDao;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
/**
 * 客戶登入/登出紀錄查詢
 *
 * @author Alison
 * @version V1.0
 */
@Slf4j
@Service
public class ADMLOGINOUTService extends BaseService{
    @Autowired
    private AdmLogInOutDao admLoginOutDao;

    /**
     * 分頁查詢
     *
     * @param pageNo a int.
     * @param pageSize a int.
     * @param orderBy a {@link java.lang.String} object.
     * @param orderDir a {@link java.lang.String} object.
     * @param STARTDATE a {@link java.lang.String} object.
     * @param ENDDATE a {@link java.lang.String} object.
     * @param USERID a {@link java.lang.String} object.
     * @param LOGINTYPE a {@link java.lang.String} object.
     * @return a {@link com.netbank.domain.orm.core.Page} object.
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String STARTDATE, String ENDDATE,
            String USERID,String LOGINTYPE) {
        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, STARTDATE={}, ENDDATE={}, USERID={}, LOGINTYPE={}",
        Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(STARTDATE), Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID),Sanitizer.logForgingStr(LOGINTYPE));

        log4Query("0", new String[][] { { "STARTDATE", STARTDATE }, { "ENDDATE", ENDDATE }, { "USERID", USERID }, { "LOGINTYPE", LOGINTYPE } } );
        return admLoginOutDao.findPageData(pageNo, pageSize, orderBy, orderDir, STARTDATE, ENDDATE, USERID,LOGINTYPE);
    }    
}
