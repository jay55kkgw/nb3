package com.netbank.rest.web.back.model;

import java.util.Date;

import fstop.orm.po.ADMSTORETMP;
import lombok.*;

/**
 * 特店優惠管理 View Model
 *
 * @author 
 * @version V1.0
 */
@Getter
@Setter
public class StoreTmpModel extends ADMSTORETMP {
    private static final long serialVersionUID = 1L;

    private String SDateTime;
    private String EDateTime;
    private String imgGuid;
    private String comments;
    
    // 前端頁面是字串, 後端用 blob 存, 存到 TARGETCONTENT
    private String targetContentStr;
    
}
