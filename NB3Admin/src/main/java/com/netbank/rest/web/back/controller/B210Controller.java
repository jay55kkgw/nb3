package com.netbank.rest.web.back.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netbank.rest.web.back.service.ADMLOGINService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.netbank.domain.orm.core.Page;
import fstop.aop.Authorize;
import fstop.orm.po.ADMLOGIN;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 使用者登入狀態查詢
 * 
 * @author Alison
 */
@Controller
@Slf4j
@RequestMapping("/B210")
public class B210Controller {
    @Autowired
    private ADMLOGINService admLogInService;

    /**
     * 取得查詢頁
     * 
     * @param model
     * @return
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        Date now = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.MONTH, -6);

        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat("yyyy/MM/dd");

        model.addAttribute("StartDt", formatter.format(now));
        model.addAttribute("EndDt", formatter.format(now));

        return "B210/index";
    }

    /**
     * 分頁查詢
     * 
     * @param STARTDATE 查詢期間起日
     * @param ENDDATE   查詢期間迄日
     * @param USERID    使用者統編
     * @param LOGINTYPE 登入來源
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value = "/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String STARTDATE, @RequestParam String ENDDATE,
            @RequestParam String USERID, @RequestParam String LOGINTYPE, HttpServletRequest request,
            HttpServletResponse response) {
        log.debug("query STARTDATE={}, ENDDATE={}, USERID={},LOGINTYPE={}", Sanitizer.logForgingStr(STARTDATE), Sanitizer.logForgingStr(ENDDATE), Sanitizer.logForgingStr(USERID), Sanitizer.logForgingStr(LOGINTYPE));

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        STARTDATE = STARTDATE.replace("/", "") + "000000";
        ENDDATE = ENDDATE.replace("/", "") + "235959";

        Page page = admLogInService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
                jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(STARTDATE), 
                (String)Sanitizer.escapeHTML(ENDDATE), (String)Sanitizer.escapeHTML(USERID), (String)Sanitizer.escapeHTML(LOGINTYPE));

        List<ADMLOGIN> admlogins = (List<ADMLOGIN>) page.getResult();
        List<ADMLOGIN> sadmlogins = new ArrayList<ADMLOGIN>();
        for (ADMLOGIN admlogin : admlogins) {
            ADMLOGIN sadmlogin=new ADMLOGIN();
            Sanitizer.escape4Class(admlogin, sadmlogin);
            sadmlogins.add(sadmlogin);
        }
        
        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), sadmlogins);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }
}