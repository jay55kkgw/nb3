package com.netbank.rest.web.back.model;


public class B206Model {

    private String dateFrom;

    private String dateTo;

    private String userId;


	public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }


    public String getDateTo() {
        return dateTo;
    }


    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}