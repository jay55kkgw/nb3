package com.netbank.rest.web.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.B201ViewModel;
import com.netbank.rest.web.back.model.B212ViewModel;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.DeviceService;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import fstop.aop.Authorize;
import fstop.orm.po.IDGATEHISTORY;
import fstop.orm.po.QUICKLOGINMAPPING;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/B212")
public class B212Controller {
    @Autowired
    private PortalSSOService portalSSOService;
    
	@Autowired
	public DeviceService deviceService;

    @GetMapping(value={"", "/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model)
    {
    	return "B212/index";
    }
    
    @PostMapping(value="/IndexQuery", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse indexQuery(@ModelAttribute @Valid B212ViewModel viewModel, HttpServletRequest request) {
        log.debug("indexQuery CustNo={}, IdGateId={}, QueryType={}, SDate={}, EDate={}", Sanitizer.logForgingStr(viewModel.getCustNo()), Sanitizer.logForgingStr(viewModel.getIdGateId()), Sanitizer.logForgingStr(viewModel.getQueryType()), Sanitizer.logForgingStr(viewModel.getSDate()), Sanitizer.logForgingStr(viewModel.getEDate()));
        Page page = null;
        
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);
        if(viewModel.getQueryType().equals("01"))
        {
            page = deviceService.findQuickLogin((String)Sanitizer.escapeHTML(viewModel.getCustNo()), (String)Sanitizer.escapeHTML(viewModel.getIdGateId()), jqDataTableRq.getPage(), jqDataTableRq.getLength());
            List<QUICKLOGINMAPPING> resultList = (List<QUICKLOGINMAPPING>) page.getResult();
            if(resultList != null) {
            	//Stored Log Forging\路徑 6:NB3Admin_20201207.pdf
            	log.info("QUICKLOGINMAPPING count = {}", resultList.size());
            	log.info("QUICKLOGINMAPPING IDGATEID[0] = {}",(String) Sanitizer.logForgingStr((resultList.size()>0?resultList.get(0).getPks().getIDGATEID():"")));
            }
        }
        else
        {
        	page = deviceService.getVerifyHis((String)Sanitizer.escapeHTML(viewModel.getCustNo()), (String)Sanitizer.escapeHTML(viewModel.getIdGateId()), 
        			(String)Sanitizer.escapeHTML(viewModel.getSDate()), (String)Sanitizer.escapeHTML(viewModel.getEDate()), 
        			jqDataTableRq.getPage(), jqDataTableRq.getLength());
        	log.info("IDGATEHISTORY count = {}", ((List<IDGATEHISTORY>) page.getResult()).size());
        }
        
		JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
				page.getTotalCount(), page.getTotalCount(), page.getResult());

		JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
		Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);
		
        return sjqDataTableRs;
    }
    
    @PostMapping(value="/Cancel", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody Map cancel(@ModelAttribute @Valid B212ViewModel viewModel, HttpServletRequest request)
    {
    	Map<String, Object> jsresp=new HashMap<String, Object>();
    	Gson gson = new Gson();
    	 //Log Forging\路徑 6:NB3Admin_20201207.pdf
    	log.info("indexQuery Requset Json = {}", Sanitizer.logForgingStr(gson.toJson(viewModel)));
        try
        {
        	
        	Map<String, String> hmReq=new HashMap<String,String>();
        	//Reflected XSS All Clients\路徑 1、2、3、4:NBAdmin_20201208.pdf
        	hmReq.put("CUSIDN", Sanitizer.logForgingStr(viewModel.getCancelCustNo()));
        	hmReq.put("IDGATEID", Sanitizer.logForgingStr(viewModel.getCancelIdGateId()));
        	hmReq.put("DEVICEID", Sanitizer.logForgingStr(viewModel.getCancelDeviceId()));
        	hmReq.put("TYPE", Sanitizer.logForgingStr(viewModel.getCancelDeviceType()));
	        @SuppressWarnings("unchecked")
			Map<String, String> mm = deviceService.doCancelIdGateid(hmReq, portalSSOService.getLoginUserId());
	        //Log Forging\路徑 1、2、3、4、5:NB3Admin_20201207.pdf
	        log.info("msgCode : {}",Sanitizer.logForgingStr( mm.get("msgCode")));
	        if(mm.get("msgCode").equals("0"))
	        {
	        	Map<String,Object> htdata=new HashMap<String, Object>();
	        	boolean rc = deviceService.updateCancelIdGateId(viewModel.getCancelCustNo(), viewModel.getCancelIdGateId(), portalSSOService.getLoginUserId(), htdata);
	        	if(rc==true)
	        	{	        		
	        		jsresp.put("Validated", true);
	        		ObjectMapper oMapper = new ObjectMapper();
	        		jsresp.put("tbData", oMapper.convertValue(htdata.get("qklogin"), Map.class));
	        		Map<String, String> err = new HashMap<String, String>();
	        		err.put("rt", "0");
	        		err.put("message", "成功。");
	        		jsresp.put("ErrorMessages", err);
	        	}
	        	else
	        	{
	        		Map<String, String> err = new HashMap<String, String>();
	        		err.put("rt", "X999");
	        		err.put("message", "資料更新失敗！！");
	        		jsresp.put("Validated", false);
	        		jsresp.put("ErrorMessages", err);
	        	}
	        }
	        else
	        {
	        	mm.put("rt", "error");
	        	mm.put("message", "資料更新失敗！！");
	        	jsresp.put("Validated", false);
	        	jsresp.put("ErrorMessages", mm);
	        }
	        //Reflected XSS All Clients\路徑 1、2、3、4:NB3Admin_20201207.pdf
	        Map<String, Object> sjsresp = new HashMap<String, Object>();
			Sanitizer.escape4Class(jsresp, sjsresp);
        }
        catch(Exception e)
        {
	        log.error("call doCancelIdGateid Exception！！", e);
	        jsresp.put("Validated", false);
	        Map<String, String> mm = new HashMap<String, String>();
	        mm.put("rt", "FE0003");
    		mm.put("message", "系統連線異常，如為轉帳/繳費稅/定存等，請執行存款明細查詢，確認交易是否成功。");
	        jsresp.put("Validated", false);
	        jsresp.put("ErrorMessages", mm);
        }
//    	JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);
//
//        Page page = deviceService.findQuickLogin((String)Sanitizer.escapeHTML(viewModel.getCancelCustNo()), (String)Sanitizer.escapeHTML(viewModel.getCancelIdGateId()), jqDataTableRq.getPage(), jqDataTableRq.getLength());
//
//        log.info("QUICKLOGINMAPPING count = {}", ((List<QUICKLOGINMAPPING>) page.getResult()).size());
//        log.info("QUICKLOGINMAPPING IDGATEID[0] = {}", ((List<QUICKLOGINMAPPING>) page.getResult()).get(0).getPks().getIDGATEID());
//
//		JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
//				page.getTotalCount(), page.getTotalCount(), page.getResult());
//
//        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
//		Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);
//		
//        return sjqDataTableRs;
        return jsresp;
    }
    

}
