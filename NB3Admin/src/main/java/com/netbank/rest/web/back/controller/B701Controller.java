package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.netbank.rest.web.back.model.B701Model;
import com.netbank.rest.web.back.model.B701Result;
import com.netbank.rest.web.back.service.NMBReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fstop.aop.Authorize;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>B701Controller class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B701")
public class B701Controller {
    @Autowired
    private NMBReportService nMBReportService;

    /**
     * 取得一般網路銀行客戶登入數統計表首頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        B701Model entity = new B701Model();
        entity.setOptradio("Month");

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        String today = year + "/" + String.format("%02d", month) + "/" + String.format("%02d", day);
        String todayYear = String.format("%04d", year);
        String todayMonth = String.format("%02d", month);

        entity.setYearS(todayYear);
        entity.setMonthS(todayMonth);
        entity.setDateFrom(today);

        entity.setYearE(todayYear);
        entity.setMonthE(todayMonth);
        entity.setDateTo(today);
        entity.setLoginType("'','NB','MB'");

        SetDropDownList(model, entity);
        model.addAttribute("IsPostBack", false);
        return "B701/index";
    }

    /**
     * Form submit 顯示查詢結果
     *
     * @param entity a {@link com.netbank.rest.web.back.model.B701Model} object.
     * @param result a {@link org.springframework.validation.BindingResult} object.
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value = "/Query")
    @Authorize(userInRoleCanQuery = true)
    public String query(@ModelAttribute B701Model entity, BindingResult result, ModelMap model) {
        log.debug("entity={}", Sanitizer.logForgingStr(entity));

        // 錯誤處理
        if (result.hasErrors()) {
            // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
            Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

            model.addAttribute("Validated", false);
            model.addAttribute("ErrMsg", errors);
            return "B701/index";
        }

        List<B701Result> queryData = getB701ResultList(entity);
        model.addAttribute("Data", queryData);
        SetDropDownList(model, entity);
        model.addAttribute("IsPostBack", true);
        return "B701/index";
    }

    /**
     * 下載 CSV
     *
     * @param entity a {@link com.netbank.rest.web.back.model.B701Model} object.
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException
     */
    @Authorize(userInRoleCanQuery = true)
    @PostMapping(value = { "/DownloadtoCSV" })
    public void DownloadtoCSV(@ModelAttribute B701Model entity, ModelMap model, HttpServletResponse response)
            throws IOException {

        response.setContentType("text/csv;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"ExportSys.csv\"");
        OutputStream resOs = response.getOutputStream();
        resOs.write('\ufeef'); // emits 0xef
        resOs.write('\ufebb'); // emits 0xbb
        resOs.write('\ufebf'); // emits 0xbf

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = new CSVWriter(outputWriter);

        List<B701Result> queryData = getB701ResultList(entity);

        String[] headers = { "月份\\類别", "企業戶", "個人戶", "合計" };
        writer.writeNext(headers);
        for (B701Result item : queryData) {
            String[] nextLine = { item.getCMYYYYMM(), item.getADENTERP(), item.getADPERSON(), item.getADSUMROW() };
            writer.writeNext(nextLine);
        }
        writer.close();
    }

    /*
     * 取回查詢資料
     */
    private List<B701Result> getB701ResultList(B701Model entity) {

        Rows qResult = new Rows();
        try {
            qResult = nMBReportService.query(entity);
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", e.getMessage());
        }

        List<B701Result> queryData = new ArrayList<>();
        B701Result qItem;
        int rowCount = qResult.getSize();
        int ADENTERP = 0;
        int ADPERSON = 0;
        int ADSUMROW = 0;
        for (int i = 0; i < rowCount; i++) {
            qItem = new B701Result();
            Row r = null;
            r = qResult.getRow(i);
            qItem.setCMYYYYMM(r.getValue("CMYYYYMM"));
            qItem.setADENTERP(r.getValue("ADENTERP"));
            qItem.setADPERSON(r.getValue("ADPERSON"));
            qItem.setADSUMROW(r.getValue("ADSUMROW"));

            ADENTERP += Integer.parseInt(r.getValue("ADENTERP"));
            ADPERSON += Integer.parseInt(r.getValue("ADPERSON"));
            ADSUMROW += Integer.parseInt(r.getValue("ADSUMROW"));

            queryData.add(qItem);
        }

        qItem = new B701Result();
        qItem.setCMYYYYMM("合計");
        qItem.setADENTERP(Integer.toString(ADENTERP));
        qItem.setADPERSON(Integer.toString(ADPERSON));
        qItem.setADSUMROW(Integer.toString(ADSUMROW));
        queryData.add(qItem);

        return queryData;
    }

    private void SetDropDownList(ModelMap model, B701Model entity) {
        model.addAttribute("B701Model", entity);

        List<String> years = new ArrayList<String>();
        for (int i = 2019; i <= 2119; i++) {
            years.add(Integer.toString(i));
        }

        List<String> months = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            // months.add(Integer.toString(i));
            months.add(String.format("%02d", i));
        }

        model.addAttribute("years", years);
        model.addAttribute("months", months);
        return;
    }
}
