package com.netbank.rest.web.back.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import fstop.aop.Authorize;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import java.util.Date;
import java.util.HashMap;

import com.netbank.rest.web.back.service.TXNTWSCHPAYDATAService;
import com.netbank.rest.web.back.model.B402Model;
import com.netbank.rest.web.back.service.ADMMSGCODEService;
import com.netbank.rest.web.back.service.NB3SYSOPService;
import com.netbank.rest.web.back.service.TXNFXSCHPAYDATAService;

import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import java.util.Optional;

import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;

import org.springframework.http.MediaType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PostMapping;
import java.util.Calendar;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.ArrayList;

/**
 * 預約明細查詢
 * 
 * @author Alison
 */
@Controller
@Slf4j
@RequestMapping("/B205")
public class B205Controller {
    @Autowired
    private TXNTWSCHPAYDATAService txnTWSchPayDataService;
    
    @Autowired
    private TXNFXSCHPAYDATAService txnFXSchPayDataService;

    @Autowired
    private ADMMSGCODEService admMSGCODEService;

    @Autowired
    private NB3SYSOPService nb3SYSOPService;
    
    /**
     * 取得查詢頁
     * 
     * @param model
     * @return
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
    	try {    		
    		Date now = new Date();
    		
    		Calendar cal = Calendar.getInstance();
    		cal.setTime(now);
    		
    		SimpleDateFormat formatter;
    		formatter = new SimpleDateFormat("yyyy/MM/dd");
    		
    		model.addAttribute("StartDt", formatter.format(now));
    		model.addAttribute("EndDt", formatter.format(now));
    		
    		return "B205/index";
    	}catch(Exception e) {
    		log.error("B205 index Error...",e);
    		return "Home/index";
    	}

    }

    /**
     * 轉至結果頁
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping("/Query")
    public String query(@RequestParam(value = "UserId") String userId, @RequestParam(value = "Status") String status,
            @RequestParam(value = "StartDt") String startDt, @RequestParam(value = "EndDt") String endDt,
            ModelMap modelMap) {
        String statusWord = null;

        switch (status) {
        case "All":
            statusWord = "全部";
            break;
        case "0":
            statusWord = "成功";
            break;
        case "1":
            statusWord = "失敗";
            break;
        case "2":
            statusWord = "處理中";
            break;
        }
        modelMap.put("userId", userId);
        modelMap.put("statusWord", statusWord);
        modelMap.put("status", status);
        modelMap.put("startDt", startDt);
        modelMap.put("endDt", endDt);

        return "B205/query";
    }

    /**
     * 分頁查詢--台幣
     * 
     * @param USERID   身分證字號/統一編號
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value = "/QueryNTD", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse queryNTD(@RequestParam String USERID, @RequestParam String STATUS,
            @RequestParam String STARTDT, @RequestParam String ENDDT, HttpServletRequest request,
            HttpServletResponse response) {
        try {        	
        	log.debug("query USERID={} STATUS={}, STARTDT={}, ENDDT={}", Sanitizer.logForgingStr(USERID), Sanitizer.logForgingStr(STATUS), Sanitizer.logForgingStr(STARTDT), Sanitizer.logForgingStr(ENDDT));
        	
        	JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);
        	
        	STARTDT = STARTDT.replace("/", "");
        	ENDDT = ENDDT.replace("/", "");
        	
        	List<NB3SYSOP> menus = nb3SYSOPService.getAllMenus();
        	Page page = txnTWSchPayDataService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
        			jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String) Sanitizer.escapeHTML(STARTDT),
        			(String) Sanitizer.escapeHTML(ENDDT), (String) Sanitizer.escapeHTML(USERID),
        			(String) Sanitizer.escapeHTML(STATUS));
        	
        	List<TXNTWSCHPAYDATA> txnTwSchPayDatas = (List<TXNTWSCHPAYDATA>) page.getResult();
        	List<TXNTWSCHPAYDATA> stxnTwSchPayDatas = new ArrayList<TXNTWSCHPAYDATA>();
        	for (TXNTWSCHPAYDATA txnTwSchpayData : txnTwSchPayDatas) {
        		// 把交易代碼改成 "交易代碼，交易中文"
        		Optional<NB3SYSOP> sysop = menus.stream().filter(p->p.getADOPID().equalsIgnoreCase(txnTwSchpayData.getADOPID())).findFirst();
        		if ( sysop.isPresent() ) {
        			txnTwSchpayData.setADOPID(txnTwSchpayData.getADOPID()+","+sysop.get().getADOPNAME());
        		}
        		
        		TXNTWSCHPAYDATA stxnTWSchpayData = new TXNTWSCHPAYDATA();
        		Sanitizer.escape4Class(txnTwSchpayData, stxnTWSchpayData);
        		stxnTwSchPayDatas.add(stxnTWSchpayData);
        	}
        	
        	JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
        			page.getTotalCount(), page.getTotalCount(), stxnTwSchPayDatas);
        	
        	//20191115-Danny-Reflected XSS All Clients
        	JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        	Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);
        	return sjqDataTableRs;
        }catch(Exception e){
        	log.error("B205 QueryNTD error...",e);
        	JQueryDataTableResponse sjqDataTableRs = JQueryDataTableHelper.GetResponse(0, 0, 0, new ArrayList<TXNTWSCHPAYDATA>());
        	return sjqDataTableRs;
        }
    }

    /**
     * 分頁查詢--外幣
     * 
     * @param USERID   身分證字號/統一編號
     * @param request
     * @param response
     * @return
     */
    @PostMapping(value = "/QueryFC", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse queryFC(@RequestParam String USERID, @RequestParam String STATUS,
            @RequestParam String STARTDT, @RequestParam String ENDDT, HttpServletRequest request,
            HttpServletResponse response) {
        try{
        	
        	log.debug("query USERID={} STATUS={}, STARTDT={}, ENDDT={}", Sanitizer.logForgingStr(USERID), Sanitizer.logForgingStr(STATUS), Sanitizer.logForgingStr(STARTDT), Sanitizer.logForgingStr(ENDDT));
        	
        	JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);
        	
        	STARTDT = STARTDT.replace("/", "");
        	ENDDT = ENDDT.replace("/", "");
        	
        	List<NB3SYSOP> menus = nb3SYSOPService.getAllMenus();
        	Page page = txnFXSchPayDataService.getByQuery4B205(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
        			jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String) Sanitizer.escapeHTML(STARTDT),
        			(String) Sanitizer.escapeHTML(ENDDT), (String) Sanitizer.escapeHTML(USERID),
        			(String) Sanitizer.escapeHTML(STATUS));
        	
        	List<TXNFXSCHPAYDATA> txnFxSchPayDatas = (List<TXNFXSCHPAYDATA>) page.getResult();
        	List<TXNFXSCHPAYDATA> stxnFxSchPayDatas = new ArrayList<TXNFXSCHPAYDATA>();
        	for (TXNFXSCHPAYDATA txnFxSchPayData : txnFxSchPayDatas) {
        		// 把交易代碼改成 "交易代碼，交易中文"
        		Optional<NB3SYSOP> sysop = menus.stream().filter(p->p.getADOPID().equalsIgnoreCase(txnFxSchPayData.getADOPID())).findFirst();
        		if ( sysop.isPresent() ) {
        			txnFxSchPayData.setADOPID(txnFxSchPayData.getADOPID()+","+sysop.get().getADOPNAME());
        		}
        		
        		TXNFXSCHPAYDATA stxnFxSchpayData = new TXNFXSCHPAYDATA();
        		Sanitizer.escape4Class(txnFxSchPayData, stxnFxSchpayData);
        		stxnFxSchPayDatas.add(stxnFxSchpayData);
        	}
        	
        	JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
        			page.getTotalCount(), page.getTotalCount(), stxnFxSchPayDatas);
        	
        	//20191115-Danny-Reflected XSS All Clients
        	JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        	Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);
        	return sjqDataTableRs;
        }catch(Exception e) {
        	log.error("B205 QueryFC error...",e);
        	JQueryDataTableResponse sjqDataTableRs = JQueryDataTableHelper.GetResponse(0, 0, 0, new ArrayList<TXNFXSCHPAYDATA>());
        	return sjqDataTableRs;
        }
    }

    /**
     * <p>
     * statusDialog.
     * </p>
     *
     * @param model a {@link org.springframework.ui.ModelMap} object.
     * @param id    a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @PostMapping(value = "/MsgDialog/{id}")
    public String msgDialog(@PathVariable String id, ModelMap model) {
        String MsgOut = "訊息說明 : " + admMSGCODEService.getMsgIn(id);
        String MsgIn = "客戶訊息 : " + admMSGCODEService.getMsg(id);
        model.addAttribute("ErrMsg", MsgOut + "<br>" + MsgIn);
        return "B205/msgPartial";
    }
    
    @PostMapping("/Export")
    public void export(@RequestParam(value = "ExportData") String exportData, HttpServletResponse response) {
    	OutputStream resOs = null;
    	OutputStream buffOs = null;
    	OutputStreamWriter outputWriter = null;
    	CSVWriter writer = null;
    	try {    		
    		Gson gson = new Gson();
    		B205Export data = gson.fromJson(exportData, B205Export.class);
    		
    		data.StartDt = (String)Sanitizer.escapeHTML(data.StartDt);
    		data.EndDt = (String)Sanitizer.escapeHTML(data.EndDt);
    		data.UserId = (String)Sanitizer.escapeHTML(data.UserId);
    		data.Status = (String)Sanitizer.escapeHTML(data.Status);
    		
    		response.setContentType("text/csv;charset=utf-8");
    		response.setHeader("Content-Disposition", "attachment; filename=\"ExportSys.csv\"");
    		resOs = response.getOutputStream();
    		resOs.write('\ufeef'); // emits 0xef
    		resOs.write('\ufebb'); // emits 0xbb
    		resOs.write('\ufebf'); // emits 0xbf
    		
    		buffOs = new BufferedOutputStream(resOs);
    		outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
    		writer = new CSVWriter(outputWriter);
    		String[] headers = { "預約編號", "轉帳日期", "轉帳時間", "身分證/營利事業統一編號", "交易代號", "轉出帳號", "轉入行", "轉入帳號/繳費稅代號", "轉帳金額",
    				"錯誤代碼", "錯誤訊息", "姓名", "電話" };
    		writer.writeNext(headers);
    		
    		String fromDate = data.StartDt.replace("/", "");
    		String endDate = data.EndDt.replace("/",  "");
    		
    		List<B402Model> queryData = txnTWSchPayDataService.findCSV(fromDate, endDate, data.UserId, data.Status, false);
    		
    		for (B402Model item : queryData) {
    			String[] nextLine = { item.getDPSCHNO(), item.getLASTDATE(), "\t" + item.getLASTTIME(), item.getDPUSERID(),
    					item.getADOPID(), "\t" + item.getDPWDAC(), "\t" + item.getDPSVBH(), "\t" + item.getDPSVAC(),
    					item.getDPTXAMT(), item.getDPEXCODE(), item.getADMSGIN(), item.getDPUSERNAME(),
    					"\t" + item.getEINPHONE() };
    			
    			writer.writeNext(nextLine);
    		}
    	}catch(Exception e) {
    		log.error("B205 Export error...",e);
    	}finally {
    		if (writer != null) {
    	          try {
    	        	  writer.close();
    	          } catch (IOException e) {
    	             log.error("B205 Export error...",e);
    	          }
    	       }
    	}
    }

    @Data
    private class B205Export {
    	String UserId;
    	String StartDt;
    	String EndDt;
    	String Status;
    }
}