package com.netbank.rest.web.back.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.ADMMSGCODEEx;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.model.UploadedFile;
import com.netbank.rest.web.back.service.ADMMSGCODEService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import fstop.aop.Authorize;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.Sanitizer;
import fstop.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

/**
 * 應用系統代碼維護 Controller，使用另一個畫面做 CRUD
 *
 * @author DANNY CHOU
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B107")
public class B107Controller {
    @Autowired
    private ADMMSGCODEService admMSGCODEService;

    @Autowired
    private PortalSSOService portalSSOService;
    
    /**
     * 取得 應用系統代碼維護 首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value = { "", "/Index" })
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        if ( portalSSOService.isPermissionOK("B107", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        return "B107/index";
    }

    /**
     * 分頁查詢
     *
     * @param ADMCODE  代號
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @return 支援 jQuery Datatable Server 端的分頁物件
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value = "/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String ADMCODE, @RequestParam String ADMSGOUT, HttpServletRequest request,
            HttpServletResponse response) {

        ADMCODE = (String)Sanitizer.logForgingStr(ADMCODE);
        ADMSGOUT = (String)Sanitizer.logForgingStr(ADMSGOUT);

        log.debug("query ADMCODE={}", Sanitizer.logForgingStr(ADMCODE));
        log.debug("query ADMSGOUT={}", Sanitizer.logForgingStr(ADMSGOUT));

        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admMSGCODEService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(),
                jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(ADMCODE), (String)Sanitizer.escapeHTML(ADMSGOUT));

        List<ADMMSGCODE> admMsgCodes = (List<ADMMSGCODE>) page.getResult();
        List<ADMMSGCODE> sadmMsgCodes = new ArrayList<ADMMSGCODE>();
        for (ADMMSGCODE admMsgCode : admMsgCodes) {
            ADMMSGCODE sadmMsgCode = new ADMMSGCODE();
            Sanitizer.escape4Class(admMsgCode, sadmMsgCode);
            sadmMsgCodes.add(sadmMsgCode);
        }
        
        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(),
                page.getTotalCount(), page.getTotalCount(), sadmMsgCodes);
        //20191115-Danny-Reflected XSS All Clients\路徑 5:
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得 應用系統代碼維護 新增頁
     *
     * @return view path
     */
    @GetMapping(value = { "/Create" })
    @Authorize(userInRoleCanEdit = true)
    public String create() {
        return "B107/Create";
    }

    /**
     * 新增資料
     *
     * @param ADMMSGCODE 系統代碼物件
     * @param result     SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value = "/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse create(@ModelAttribute @Valid ADMMSGCODE ADMMSGCODE, BindingResult result) {
        JsonResponse response = new JsonResponse();

        try {
            if (result.hasErrors()) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                        .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                ADMMSGCODE tmp = admMSGCODEService.getAdmMsgCode(ADMMSGCODE.getADMCODE());
                if ( tmp != null ) {
                	Map<String, String> errors = new HashMap<String, String>();
                	//Reflected XSS All Clients
                	String atmCode = (String)Sanitizer.escapeHTML(ADMMSGCODE.getADMCODE());
                    errors.put("summary", "錯誤代碼["+atmCode+"]資料已存在");

                    response.setValidated(false);
                    response.setErrorMessages(errors);
                    return response;
                } else {
                    ADMMSGCODE.setADMRESEND(ADMMSGCODE.getADMRESEND().equalsIgnoreCase("Y") ? "Y" : "N");
                    ADMMSGCODE.setADMEXCE(ADMMSGCODE.getADMEXCE().equalsIgnoreCase("Y") ? "Y" : "N");
                    ADMMSGCODE.setADMRESENDFX(ADMMSGCODE.getADMRESENDFX().equalsIgnoreCase("Y") ? "Y" : "N");
                    ADMMSGCODE.setADMAUTOSEND(ADMMSGCODE.getADMAUTOSEND().equalsIgnoreCase("Y") ? "Y" : "N");
                    ADMMSGCODE.setADMAUTOSENDFX(ADMMSGCODE.getADMAUTOSENDFX().equalsIgnoreCase("Y") ? "Y" : "N");

                    admMSGCODEService.insertAdmMsgCode(ADMMSGCODE, portalSSOService.getLoginUserId());
                    response.setValidated(true);
                }
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //Information Exposure Through an Error Message
            errors.put("summary", "存檔發生錯誤，請確認資料是否有更新成功!!");

            response.setValidated(false);
            response.setErrorMessages(errors);
            //Information Exposure Through an Error Message
            return response;
        }

        return response;
    }

    /**
     * 取得 應用系統代碼維護 修改
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     * @param ADMCODE a {@link java.lang.String} object.
     */
    @GetMapping(value = { "/Edit/{ADMCODE}" })
    @Authorize(userInRoleCanQuery = true)
    public String edit(@PathVariable String ADMCODE, ModelMap model) {
        //20191211-Danny-避免中文時會亂碼
        ADMCODE = (String)Sanitizer.sanitize(ADMCODE);
        ADMMSGCODE po = admMSGCODEService.getAdmMsgCode(ADMCODE);
        model.addAttribute("Data", po);
        if ( portalSSOService.isPermissionOK("B107", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        
        return "B107/Edit";
    }

    /**
     * 修改資料
     *
     * @param ADMMSGCODE 應用系統物件
     * @param result     SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value = "/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid ADMMSGCODEEx ADMMSGCODE, BindingResult result) {
        JsonResponse response = new JsonResponse();
        try {
            if (result.hasErrors()) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                        .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                ADMMSGCODE saveEntity = new ADMMSGCODE();
                saveEntity.setADMCODE(ADMMSGCODE.getADMCODE());
                saveEntity.setADMRESEND(ADMMSGCODE.getADMRESEND().equalsIgnoreCase("Y") ? "Y" : "N");
                saveEntity.setADMEXCE(ADMMSGCODE.getADMEXCE().equalsIgnoreCase("Y") ? "Y" : "N");
                saveEntity.setADMRESENDFX(ADMMSGCODE.getADMRESENDFX().equalsIgnoreCase("Y") ? "Y" : "N");
                saveEntity.setADMAUTOSEND(ADMMSGCODE.getADMAUTOSEND().equalsIgnoreCase("Y") ? "Y" : "N");
                saveEntity.setADMAUTOSENDFX(ADMMSGCODE.getADMAUTOSENDFX().equalsIgnoreCase("Y") ? "Y" : "N");
                saveEntity.setADMSGIN(ADMMSGCODE.getADMSGIN());
                saveEntity.setADMSGOUT(ADMMSGCODE.getADMSGOUT());
                saveEntity.setADMSGOUTCHS(ADMMSGCODE.getADMSGOUTCHS());
                saveEntity.setADMSGOUTENG(ADMMSGCODE.getADMSGOUTENG());
                admMSGCODEService.saveAdmMsgCode(saveEntity, portalSSOService.getLoginUserId());
                
                response.setValidated(true);
            }
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            //Information Exposure Through an Error Message
            errors.put("summary", "存檔發生錯誤，請確認資料是否有更新成功!!");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }

        return response;
    }
    
    /**
     * 刪除資料
     *
     * @param ADMCODE 應用系統代碼
     * @return "0"：成功，非"0"：失敗原因
     */
    @PostMapping(value = "/Delete/{ADMCODE}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse delete(@PathVariable String ADMCODE) {
        ADMCODE = (String)Sanitizer.sanitize(ADMCODE);
        JsonResponse response = new JsonResponse();
        try {
            admMSGCODEService.deleteAdmMsgCode(ADMCODE);
            response.setValidated(true);
        } catch (Exception e) {
            log.error("delete error", e);

            Map<String, String> errors = new HashMap<String, String>();
            //Information Exposure Through an Error Message
            errors.put("summary", "刪除發生錯誤，請確認資料是否有成功刪除!!");
            response.setErrorMessages(errors);
            response.setValidated(false);
            return response;
        }
        return response;
    }


    /**
     * 下載 應用系統代碼維護
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException if any.
     */
    @GetMapping(value = { "/DownloadtoCSV" })
    @Authorize(userInRoleCanQuery = true)
    public void DownloadtoCSV(ModelMap model, HttpServletResponse response) throws IOException {

        Download(response, true);
    }

    /**
     * 下載全部或範本
     * @param response
     * @param all       true: 全部，false: 範本
     * @throws IOException
     */
    private void Download(HttpServletResponse response, boolean all) throws IOException {
        response.setContentType("text/csv;charset=utf-8");

        OutputStream resOs = response.getOutputStream();
        if ( all ) {
            resOs.write('\ufeef'); // emits 0xef
            resOs.write('\ufebb'); // emits 0xbb
            resOs.write('\ufebf'); // emits 0xbf
            response.setHeader("Content-Disposition", "attachment; filename=\"ExportSys.csv\"");
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=\"Sample.txt\"");
        }
        

        OutputStream buffOs = new BufferedOutputStream(resOs);
        OutputStreamWriter outputWriter = new OutputStreamWriter(buffOs, "UTF-8");
        CSVWriter writer = null;
        
        if ( all ) {
            writer = new CSVWriter(outputWriter);
        } else {
            writer = new CSVWriter(outputWriter, CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_ESCAPE_CHARACTER);
        }

        if ( !all ) {
            String[] headers = { "以下為範例資料，上傳時請勿包含訊息代碼...等欄位標題" };
            writer.writeNext(headers);
        }
        String[] headers = { "訊息代碼", "可否提供台幣類交易人工重送", "可否提供台幣類交易自動重送", "需列入異常事件通知否", "可否提供外幣類交易人工重送", "可否提供外幣類交易自動重送",
                "訊息說明", "客戶訊息內容", "客戶訊息內容-簡中", "客戶訊息內容-英文" };
        writer.writeNext(headers);

        if ( all ) {
            List<ADMMSGCODE> admMsgCodes = admMSGCODEService.getList("");
            for (ADMMSGCODE item : admMsgCodes) {
                String[] nextLine = { item.getADMCODE(), item.getADMRESEND(), item.getADMAUTOSEND(), item.getADMEXCE(),
                        item.getADMRESENDFX(), item.getADMAUTOSENDFX(), item.getADMSGIN(), item.getADMSGOUT(), 
                        item.getADMSGOUTCHS(), item.getADMSGOUTENG() };

                writer.writeNext(nextLine);
            }
        } else {
            ADMMSGCODE item = admMSGCODEService.getAdmMsgCode("R000");
            String[] nextLine = { item.getADMCODE(), item.getADMRESEND(), item.getADMAUTOSEND(), item.getADMEXCE(),
                    item.getADMRESENDFX(), item.getADMAUTOSENDFX(), item.getADMSGIN(), item.getADMSGOUT(), 
                    "交易成功", "Transcation Success." };

            writer.writeNext(nextLine);
        }
        
        writer.close();
    }

    /**
     * 應用系統檔上傳
     *
     * @param servletRequest a {@link javax.servlet.http.HttpServletRequest} object.
     * @param uploadedFile a {@link com.netbank.rest.web.back.model.UploadedFile} object.
     * @param bindingResult a {@link org.springframework.validation.BindingResult} object.
     * @return a {@link com.netbank.rest.web.back.model.JsonResponse} object.
     * 20191118     DannyChou   EDIT:處理上傳檔案時BOM \uFEFF 需刪除。
     */
    @PostMapping(value = "FileUpload")
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse saveFile(HttpServletRequest servletRequest,
            @ModelAttribute UploadedFile uploadedFile, BindingResult bindingResult) {
        JsonResponse response = new JsonResponse();
        try {
            MultipartFile[] multipartFile = uploadedFile.getMultipartFile();
            // String fileName = multipartFile[0].getOriginalFilename();
            // log.debug("fileName={}", fileName);

            // byte[] bContent = multipartFile[0].getBytes();
            // if ( bContent.length > 10*1024*1024 ) {
            //     throw new Exception("檔案大小超過 10Mb");
            // }
            // String fileContent = new String(bContent, "UTF-8");
            // String[] lines = fileContent.split("\\r?\\n");
            //20191120-Danny-Unrestricted File Upload\路徑 1:
            //允許的類型
            String[] allowFileType = {"application/vnd.ms-excel","text/csv","text/plain"};
            //圖示允許的檔案大小
            int allowFileSize = 10*1024*1024;//1Mb
            String contentType = multipartFile[0].getContentType();
            log.debug("picFile contentType={}",Sanitizer.logForgingStr(contentType));
            String originalFilename = multipartFile[0].getOriginalFilename();
            log.debug("picFile originalFilename={}",Sanitizer.logForgingStr(originalFilename));
            Map<String,Object> returnMap = uploadedFile.checkFile(multipartFile[0],originalFilename,contentType,allowFileType,allowFileSize);
            //檢核不通過
            if("FALSE".equals(returnMap.get("result"))){
                String errorMessage = (String)returnMap.get("summary");
                log.debug("errorMessage={}",Sanitizer.logForgingStr(errorMessage));
                throw new Exception(errorMessage);
            }
            String fileContent = (String)returnMap.get("Data");
            String[] lines = uploadedFile.RemoveUTF8BOM(fileContent).split("\\r?\\n");

            List<ADMMSGCODE> admMsgCodeList = new ArrayList<ADMMSGCODE>();
            for (String item : lines) {
                //20191119-Danny-將處理BOM的方法移到UploadedFile
                //20191212-Danny-Split若是欄位有空白會沒有產生陣列元素，第2參數加<0參數即可
                String[] admMsgCodeDatas = uploadedFile.RemoveUTF8BOM(item).split(",",-1);
                ADMMSGCODE entity = new ADMMSGCODE();
                entity.setADMCODE(admMsgCodeDatas[0]); // 訊息代碼
                entity.setADMRESEND(admMsgCodeDatas[1]); // 可否提供台幣類交易人工重送
                entity.setADMAUTOSEND(admMsgCodeDatas[2]); // 可否提供台幣類交易自動重送
                entity.setADMEXCE(admMsgCodeDatas[3]); // 需列入異常事件通知否
                entity.setADMRESENDFX(admMsgCodeDatas[4]); // 可否提供外幣類交易人工重送
                entity.setADMAUTOSENDFX(admMsgCodeDatas[5]); // 可否提供外幣類交易自動重送
                entity.setADMSGIN(admMsgCodeDatas[6]); // 訊息說明
                entity.setADMSGOUT(admMsgCodeDatas[7]); // 客戶訊息內容
                entity.setADMSGOUTCHS(admMsgCodeDatas[8]); // 客戶訊息內容-簡中
                entity.setADMSGOUTENG(admMsgCodeDatas[9]); // 客戶訊息內容-英文
                admMsgCodeList.add(entity);
            }

            admMSGCODEService.uploadAdmMsgCode(admMsgCodeList, portalSSOService.getLoginUserId());
            response.setValidated(true);
        } catch (Exception e) {
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            //20191115-Eric-Information Exposure Through an Error Message\路徑11:
            errors.put("summary", "存檔發生錯誤，請確認資料是否有更新成功!!");

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        return response;
    }

    @GetMapping(value = { "/Sample" })
    @Authorize(userInRoleCanQuery = true)
    public void sample(ModelMap model, HttpServletResponse response) throws IOException {
        Download(response, false);
    }

}
