package com.netbank.rest.web.back.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.netbank.domain.orm.core.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.dao.TxnFxSchPayDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 預約交易結果查詢(外幣)
 * 
 * @author Alison
 */
@Slf4j
@Service
public class TXNFXSCHPAYService extends BaseService {
    @Autowired
    private TxnFxSchPayDao txnFxSchPayDao;
    @Autowired
    private TxnFxSchPayDataDao txnFxSchPayDataDao;
    /**
     * 分頁查詢
     * 
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param orderDir
     * @param USERID
     * @return
     */
    public Page getByQuery(int pageNo, int pageSize, String orderBy, String orderDir, String USERID) {
        //避免orderby的key包含了pk，要去掉放入sql
        if(orderBy.contains(".")){
            String[] splitstr = orderBy.split("\\.");
            orderBy = splitstr[splitstr.length-1];
         }

        log.debug("getByQuery pageNo={}, pageSize={}, orderBy={}, orderDir={}, USERID={}", Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy),
        Sanitizer.logForgingStr(orderDir), Sanitizer.logForgingStr(USERID));

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String today = dateFormat.format(date);

        log4Query("0", new String[][] { { "USERID", USERID } } );
        return txnFxSchPayDao.findPageData(pageNo, pageSize, orderBy, orderDir, USERID, today);
    }

    /**
     * 取得下次轉帳日(外幣)
     *
     * @param ADOPID
     * @return
     */
    public String getFXNEXTDATE(String userid, String dpschno) {
        log.debug("getFXNEXTDATE, userid={},dpschno={}",Sanitizer.logForgingStr(userid), Sanitizer.logForgingStr(dpschno));
        return txnFxSchPayDataDao.getFxschtxToday(userid, dpschno);
    }

    
}