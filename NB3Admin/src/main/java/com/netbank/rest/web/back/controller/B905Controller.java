package com.netbank.rest.web.back.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.JsonResponse;
import com.netbank.rest.web.back.service.ADMBHCONTACTService;
import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;
import com.netbank.rest.web.back.util.JQueryDataTableHelper;
import com.netbank.rest.web.back.util.JQueryDataTableRequest;
import com.netbank.rest.web.back.util.JQueryDataTableResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;
import fstop.orm.dao.AdmBhContactDao;
import fstop.orm.po.ADMBHCONTACT;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 分行外匯聯絡人檔維護控制器，使用一個畫面做 CRUD
 *
 * @author 楷翔
 * @version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/B905")
public class B905Controller {
    @Autowired
    private ADMBHCONTACTService admBhcontactService;
    
    @Autowired
    private PortalSSOService portalSSOService;
    
    /**
     * 取得分行外匯聯絡人首頁
     *
     * @param model 給 SpringMVC view EL binding 的物件
     * @return view path
     */
    @GetMapping(value={"", "/Index"})
    @Authorize(userInRoleCanQuery = true)
    public String index(ModelMap model) {
        if ( portalSSOService.isPermissionOK("B905", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        return "B905/index";
    }

    /**
     * 分頁查詢
     *
     * @param ADBRANCHID 分行代碼
     * @param ADBRANCHNAME 分行名稱
     * @param ADCONTACTIID 分行聯絡人代號
     * @param ADCONTACTNAME 分行聯絡人名稱
     * @param ADCONTACTEMAIL 分行聯絡人電郵
     * @param ADCONTACTTEL 分行聯絡人電話
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return 支援 jQuery Datatable Server 端的分頁物件
     */

    @PostMapping(value="/Query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    @Authorize(userInRoleCanQuery = true)
    public @ResponseBody JQueryDataTableResponse query(@RequestParam String ADBRANCHID,@RequestParam String ADBRANCHNAME,@RequestParam String ADCONTACTIID,@RequestParam String ADCONTACTNAME,@RequestParam String ADCONTACTEMAIL,@RequestParam String ADCONTACTTEL,HttpServletRequest request, HttpServletResponse response ) {
        log.debug("query ADBRANCHID={}, ADBRANCHNAME={},ADCONTACTIID={},ADCONTACTNAME={},ADCONTACTEMAIL={},ADCONTACTTEL={}", Sanitizer.logForgingStr(ADBRANCHID), Sanitizer.logForgingStr(ADBRANCHNAME),Sanitizer.logForgingStr(ADCONTACTIID),Sanitizer.logForgingStr(ADCONTACTNAME),Sanitizer.logForgingStr(ADCONTACTEMAIL),Sanitizer.logForgingStr(ADCONTACTTEL));
        
        JQueryDataTableRequest jqDataTableRq = JQueryDataTableHelper.GetRequest(request);

        Page page = admBhcontactService.getByQuery(jqDataTableRq.getPage(), jqDataTableRq.getLength(), 
            jqDataTableRq.getOrderBy(), jqDataTableRq.getOrderDir(), (String)Sanitizer.escapeHTML(ADBRANCHID), 
            (String)Sanitizer.escapeHTML(ADBRANCHNAME), (String)Sanitizer.escapeHTML(ADCONTACTIID), 
            (String)Sanitizer.escapeHTML(ADCONTACTNAME), (String)Sanitizer.escapeHTML(ADCONTACTEMAIL) ,
            (String)Sanitizer.escapeHTML(ADCONTACTTEL));


        //Page page = admBhContactDao.pagedQuery("FROM ADMBHCONTACT WHERE ADBRANCHID LIKE ?", jqDataTableRq.getPage(), jqDataTableRq.getLength(), "%"+ADBRANCHID+"%");

        List<ADMBHCONTACT> admbhcontacts=(List<ADMBHCONTACT>)page.getResult();
        List<ADMBHCONTACT> sadmbhcontacts=new ArrayList<ADMBHCONTACT>();
        for (ADMBHCONTACT admbhcontact : admbhcontacts) {
            ADMBHCONTACT sadmbhcontact = new ADMBHCONTACT();
            Sanitizer.escape4Class(admbhcontact, sadmbhcontact);
            sadmbhcontacts.add(sadmbhcontact);
        }

        JQueryDataTableResponse jqDataTableRs = JQueryDataTableHelper.GetResponse(jqDataTableRq.getDraw(), page.getTotalCount(), page.getTotalCount(), sadmbhcontacts);
        //20191115-Danny-Reflected XSS All Clients
        JQueryDataTableResponse sjqDataTableRs = new JQueryDataTableResponse();
        Sanitizer.escape4Class(jqDataTableRs, sjqDataTableRs);

        return sjqDataTableRs;
    }

    /**
     * 取得新增頁面
     * @param model
     * @return
     */
    @GetMapping(value={"/Create"})
    @Authorize(userInRoleCanEdit = true)
    public String create(ModelMap model) {
        return "B905/Create";
    }

    /**
     * 新增分行外匯聯絡人資料
     *
     * @param admbhcontact 分行物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse create(@ModelAttribute @Valid ADMBHCONTACT admbhcontact, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                admBhcontactService.insert(admbhcontact, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "存檔發生錯誤，請確認資料是否有更新成功!!";
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", message);

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 取得修改頁面
     * @param ADBHCONTID
     * @param model
     * @return
     */
    @GetMapping(value = { "/Edit/{ADBHCONTID}" })
    @Authorize(userInRoleCanQuery = true)
    public String edit(@PathVariable String ADBHCONTID, ModelMap model) {
        ADBHCONTID = (String)Sanitizer.logForgingStr(ADBHCONTID);
        ADMBHCONTACT po = admBhcontactService.getByID(Integer.parseInt(ADBHCONTID));
        model.addAttribute("Data", po);
        if ( portalSSOService.isPermissionOK("B905", AuthorityEnum.EDIT)) {
            model.addAttribute("allowEdit", true);
        } else {
            model.addAttribute("allowEdit", false);
        }
        
        return "B905/Edit";
    }

    /**
     * 修改分行外匯聯絡人資料
     *
     * @param admbhcontact 分行物件
     * @param result SpringMVC Binding 檢核結果
     * @return Json 結果物件
     */
    @PostMapping(value="/Edit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody JsonResponse edit(@ModelAttribute @Valid  ADMBHCONTACT admbhcontact, BindingResult result){
        JsonResponse response = new JsonResponse();
        try {
            if ( result.hasErrors() ) {
                // 使用 POJO 的 Validation，將檢核錯誤回傳給前端頁面做顯示
                Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
                
                response.setValidated(false);
                response.setErrorMessages(errors);
            } else {
                admBhcontactService.save(admbhcontact, portalSSOService.getLoginUserId());
                response.setValidated(true);
            }
        } catch (Exception e) {
            //20191212-Danny-Information Exposure Through an Error Message
            String message = "存檔發生錯誤，請確認資料是否有更新成功!!";
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("summary", message);

            response.setValidated(false);
            response.setErrorMessages(errors);
            return response;
        }
        
        return response;
    }

    /**
     * 刪除分行外匯聯絡人資料
     *
     * @param adbhcontid 代碼
     * @return "0"：成功，非"0"：失敗原因
     */
    @PostMapping(value="/Delete/{adbhcontid}", produces = MediaType.TEXT_PLAIN_VALUE)
    @Authorize(userInRoleCanEdit = true)
    public @ResponseBody String delete(@PathVariable Integer adbhcontid) {
        try {
            admBhcontactService.delete(adbhcontid);
            return "0";
        } catch (Exception e) {
            log.error("delete error", e);
            
            //Missing Content Security Policy
            return (String)Sanitizer.sanitize(e.getMessage());
        }
    }
       

}
