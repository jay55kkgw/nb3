package com.netbank.rest.configuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import com.netbank.rest.web.back.filter.RequestResponseFilter;

import org.apache.cxf.spring.boot.autoconfigure.CxfAutoConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;
import org.yaml.snakeyaml.Yaml;
import fstop.util.PathSetting;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>BeanConfig class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Configuration
@EnableAutoConfiguration(exclude = {
        DataSourceAutoConfiguration.class, CxfAutoConfiguration.class,
        LiquibaseAutoConfiguration.class})
@Slf4j
public class BeanConfig {

    /**
     * <p>yaml.</p>
     *
     * @return a {@link org.yaml.snakeyaml.Yaml} object.
     * @throws java.lang.Exception if any.
     */
    @Bean
    protected Yaml yaml() throws Exception {
        return new Yaml();
    }

    /**
     * <p>setting.</p>
     *
     * @return a {@link java.util.Map} object.
     * @throws java.lang.Exception if any.
     */
    @Bean
    protected Map setting() throws Exception {
        return new HashMap<String, String>();
    }

    /**
     * <p>pathSetting.</p>
     *
     * @return a {@link fstop.util.PathSetting} object.
     * @throws java.lang.Exception if any.
     */
    @Bean
    protected PathSetting pathSetting() throws Exception {
        return new PathSetting();
    }

    /**
     * <p>viewResolver.</p>
     *
     * @return a {@link org.springframework.web.servlet.view.InternalResourceViewResolver} object.
     */
    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/view/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    // TODO: 設定了會導致 multipart 上傳發生錯誤，待查原因
    /**
     * 設定檔案上傳大小限制
     * @return
     */
    // @Bean
    // public MultipartResolver multipartResolver() {
    //     CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
    //     multipartResolver.setMaxUploadSize(50*1024*1024);  //50Mb
    //     return multipartResolver;
    // }

    /**
     * <p>urlRewrite.</p>
     *
     * @return a {@link org.springframework.boot.web.servlet.FilterRegistrationBean} object.
     */
    @Bean
    public FilterRegistrationBean urlRewrite() {
        UrlRewriteFilter rewriteFilter = new UrlRewriteFilter();
        FilterRegistrationBean registration = new FilterRegistrationBean(rewriteFilter);
        registration.setUrlPatterns(Arrays.asList("/*"));
        Map initParam = new HashMap();
        initParam.put("confPath", "/WEB-INF/urlrewrite.xml");
        initParam.put("logLevel", "INFO");
//        initParam.put("logLevel", "DEBUG");
        registration.setInitParameters(initParam);
        return registration;
    }

    @Bean
    public FilterRegistrationBean<RequestResponseFilter> loggingFilter(){
        FilterRegistrationBean<RequestResponseFilter> registrationBean = new FilterRegistrationBean<>();
             
        registrationBean.setFilter(new RequestResponseFilter());
        registrationBean.addUrlPatterns("/*");
             
        return registrationBean;    
    }
    /**
     * <p>getJavaMailSender.</p>
     *
     * @param smtpAuth a {@link java.lang.String} object.
     * @param host a {@link java.lang.String} object.
     * @param port a {@link java.lang.String} object.
     * @return a {@link org.springframework.mail.javamail.JavaMailSender} object.
     */
    @Bean
    public JavaMailSender getJavaMailSender(
            @Value("${spring.mail.properties.mail.smtp.auth:}") String smtpAuth,
            @Value("${spring.mail.host:}") String host,
            @Value("${spring.mail.port:}") String port) {


        log.info("MAIL Config >> smtpAuth:" + smtpAuth + ", host: " + host + ", port: " + port);

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        try {
            mailSender.setPort(Integer.valueOf(port));
        } catch (Exception e) {
        }

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", smtpAuth);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.connectiontimeout", "3000");
        props.put("mail.smtp.timeout", "3000");
        props.put("mail.smtp.writetimeout", "3000");
        props.put("mail.debug", "true");

        return mailSender;
    }

    /**
     * <p>jacksonObjectMapperCustomization.</p>
     *
     * @return a {@link org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer} object.
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jacksonObjectMapperCustomization() {
        return builder -> {
            builder.timeZone(TimeZone.getDefault());
//            builder.dateFormat(new ISO8601DateFormat());
        };
    }
}
