package com.netbank.rest.configuration;

import com.ulisesbocchio.jasyptspringboot.EncryptablePropertyDetector;

/**
 * <p>MyEncryptablePropertyDetector class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class MyEncryptablePropertyDetector implements EncryptablePropertyDetector {

    /** {@inheritDoc} */
    @Override
    public boolean isEncrypted(String value) {
        if (value != null) {
            return value.startsWith("ENC@");
        }
        return false;
    }

    /** {@inheritDoc} */
    @Override
    public String unwrapEncryptedValue(String value) {
        return value.substring("ENC@".length());
    }
}
