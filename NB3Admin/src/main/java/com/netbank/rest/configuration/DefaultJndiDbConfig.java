package com.netbank.rest.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * <p>DefaultJndiDbConfig class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactory",
        basePackages = {"fstop.orm.dao"}
)
@ConditionalOnProperty(name = "db.source", matchIfMissing=true, havingValue = "JNDI")
public class DefaultJndiDbConfig {
//    @Primary
//    @Bean(name = "dataSource")
//    @ConfigurationProperties(prefix = "spring.datasource")
//    public DataSource dataSource() {
//        return DataSourceBuilder.create().build();
//    }

    /**
     * <p>jndiDataSource.</p>
     *
     * @return a {@link javax.sql.DataSource} object.
     * @throws java.lang.IllegalArgumentException if any.
     * @throws javax.naming.NamingException if any.
     */
    @Primary
    @Bean(name="jndiDataSource")
//	spring.datasource.jndi-name
//    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource jndiDataSource() throws IllegalArgumentException, NamingException {
        JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
        bean.setJndiName("jdbc/nb3");
        bean.setProxyInterface(DataSource.class);
        bean.setLookupOnStartup(false);
        bean.afterPropertiesSet();
        return (DataSource)bean.getObject();
    }

    /**
     * <p>entityManagerFactory.</p>
     *
     * @param builder a {@link org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder} object.
     * @param dataSource a {@link javax.sql.DataSource} object.
     * @return a {@link org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean} object.
     */
    @Primary
    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory( EntityManagerFactoryBuilder builder,
                                                                        @Qualifier("jndiDataSource") DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("fstop.orm.po")
                .persistenceUnit("default")
                .build();
    }
//
//    @Primary
//    @Bean(name = "entityManagerFactory")
//    public LocalContainerEntityManagerFactoryBean entityManagerFactory( EntityManagerFactoryBuilder builder,
//    		@Qualifier("dataSource") DataSource dataSource) {
//    	return builder
//    			.dataSource(dataSource)
//    			.packages("fstop.orm.po")
//    			.persistenceUnit("default")
//    			.build();
//    }



    /**
     * <p>transactionManager.</p>
     *
     * @param entityManagerFactory a {@link javax.persistence.EntityManagerFactory} object.
     * @return a {@link org.springframework.transaction.PlatformTransactionManager} object.
     */
    @Primary
    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(
            @Qualifier("entityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
