package com.netbank.rest.configuration;

import com.ulisesbocchio.jasyptspringboot.EncryptablePropertyDetector;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * 在 property 檔內的密碼加密
 *
 * @author chiensj
 * @version V1.0
 */
@Configuration
public class EncryptConfig {

    /**
     * https://github.com/ulisesbocchio/jasypt-spring-boot
     * Use Custom Encryptor
     *
     * @return a {@link org.jasypt.encryption.StringEncryptor} object.
     */
    @Bean("jasyptStringEncryptor")
    public StringEncryptor stringEncryptor() {

        StringEncryptor stringEncryptor = new StringEncryptor() {

            @Override
            public String encrypt(String s) {
                return base64Encode(base64Encode(s));
            }

            @Override
            public String decrypt(String s) {
                return base64Decode(base64Decode(s));
            }
        };

        return stringEncryptor;
    }

    /**
     * <p>encryptablePropertyDetector.</p>
     *
     * @return a {@link com.ulisesbocchio.jasyptspringboot.EncryptablePropertyDetector} object.
     */
    @Bean(name = "encryptablePropertyDetector")
    public EncryptablePropertyDetector encryptablePropertyDetector() {
        return new MyEncryptablePropertyDetector();
    }


    private static String base64Encode(String s) {
        try {
            return new String(Base64.getEncoder().encode(s.getBytes("UTF-8")), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("base64Encode Error !", e);
        }
    }

    private static String base64Decode(String s) {
        try {
            return new String(Base64.getDecoder().decode(s.getBytes("UTF-8")), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("base64Decode Error !", e);
        }
    }

}
