package com.netbank.rest.configuration;

import net.sf.ehcache.Cache;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * Cache 設定 / ApplicationContext-cache.xml
 *
 * @author chiensj
 * @version V1.0
 */
@Configuration
public class CacheManagerConfig {

    @Resource
    private EhCacheCacheManager cacheManager;

    /**
     * <p>readlogsCache.</p>
     *
     * @return a {@link net.sf.ehcache.Cache} object.
     */
    @Bean
    protected Cache readlogsCache() {
        return cacheManager.getCacheManager().getCache("fstop.READLOGS_CACHE");
    }


    /**
     * <p>sysObjectCache.</p>
     *
     * @return a {@link net.sf.ehcache.Cache} object.
     */
    @Bean
    protected Cache sysObjectCache() {
        return cacheManager.getCacheManager().getCache("sysObjectCache");
    }

    /**
     * <p>userPoolCache.</p>
     *
     * @return a {@link net.sf.ehcache.Cache} object.
     */
    @Bean
    protected Cache userPoolCache() {
        return cacheManager.getCacheManager().getCache("userPoolCache");
    }

    /**
     * <p>distributeCache.</p>
     *
     * @return a {@link net.sf.ehcache.Cache} object.
     */
    @Bean
    protected Cache distributeCache() {
        return cacheManager.getCacheManager().getCache("distributeCache");
    }

    /**
     * <p>fileSyncCache.</p>
     *
     * @return a {@link net.sf.ehcache.Cache} object.
     */
    @Bean
    protected Cache fileSyncCache()  {
        return cacheManager.getCacheManager().getCache("fileSyncCache");
    }

}
