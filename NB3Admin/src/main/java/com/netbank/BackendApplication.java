package com.netbank;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.TimeZone;

/**
 * <p>NB3 後台管理 Spring Boot 主程式</p>
 *
 * @author jimmy
 * @version V1.0
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.netbank.rest", 
		"fstop.util",
		"fstop.orm.dao",
		"fstop.services",
		"fstop.notifier",
		"com.netbank.telcomm", "webBranch.utility", "fstop.aop"})
@EntityScan( basePackages = {"com.netbank.rest.db", "fstop.orm.po"})
@EnableCaching
@Slf4j
@EnableScheduling
public class BackendApplication extends SpringBootServletInitializer {

	@Autowired ConfigurableApplicationContext ctx;


	/**
	 * 啟動修改 測試
	 *
	 * @param args an array of {@link java.lang.String} objects.
	 */
	public static void main(String[] args) {
		//System.exit(SpringApplication.exit(SpringApplication.run(MonitorApplication.class, args)));
		SpringApplication application = new SpringApplication(BackendApplication.class);

//		Properties properties = new Properties();
//		properties.put("spring.mvc.throw-exception-if-no-handler-found", true);
//		properties.put("spring.resources.add-mappings", false);
//		application.setDefaultProperties(properties);
		
		application.run(args);


	}

	/**
	 * <p>applicationClosedListener.</p>
	 *
	 * @return a {@link org.springframework.context.ApplicationListener} object.
	 */
	@Bean
	public ApplicationListener applicationClosedListener() {
		return (ApplicationListener<ContextClosedEvent>) event -> {
			log.info("Application shutdown .....");

		};
	}

	/**
	 * <p>applicationStartListener.</p>
	 *
	 * @return a {@link org.springframework.context.ApplicationListener} object.
	 */
	@Bean
	public ApplicationListener applicationStartListener() {
		return (ApplicationListener<ContextStartedEvent>) event -> {
			log.info("Application start .....");
			log.info("TimeZone.getDefault().getDisplayName() : " + TimeZone.getDefault().getDisplayName());
		};
	}

	/**
	 * <p>run.</p>
	 *
	 * @return a {@link org.springframework.boot.CommandLineRunner} object.
	 */
	@Bean
	public CommandLineRunner run(ApplicationContext appContext) {
		//關於JAP查詢，可以參考以下文件
		//http://www.ityouknow.com/springboot/2016/08/20/springboot(五)-spring-data-jpa的使用.html
		//
		return (args) -> {
			//String[] beans = appContext.getBeanDefinitionNames();
            //Arrays.stream(beans).sorted().forEach(System.out::println);
		};
	}
}
