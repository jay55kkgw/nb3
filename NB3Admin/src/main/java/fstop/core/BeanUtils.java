package fstop.core;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 擴展Apache Commons BeanUtils, 提供一些反射方面缺失的封裝.
 *
 * @author chiensj
 * @version V1.0
 */
public class BeanUtils extends org.apache.commons.beanutils.BeanUtils {
	private static Logger log = Logger.getLogger(BeanUtils.class);

	private BeanUtils() {
	}

	/**
	 * 暴力獲取當前類聲明的private/protected變量
	 *
	 * @param object object
	 * @param propertyName propertyName
	 * @return object
	 * @throws java.lang.IllegalAccessException 非法存取
	 * @throws java.lang.NoSuchFieldException 無此欄位
	 */
	static public Object getDeclaredProperty(Object object, String propertyName) throws IllegalAccessException, NoSuchFieldException {
		Assert.notNull(object);
		Assert.hasText(propertyName);
		Field field = object.getClass().getDeclaredField(propertyName);
		return getDeclaredProperty(object, field);
	}

	/**
	 * <p>getDeclaredProperty.</p>
	 *
	 * @param object a {@link java.lang.Object} object.
	 * @param field a {@link java.lang.reflect.Field} object.
	 * @throws java.lang.IllegalAccessException
	 * @return a {@link java.lang.Object} object.
	 */
	static public Object getDeclaredProperty(Object object, Field field) throws IllegalAccessException {
		Assert.notNull(object);
		Assert.notNull(field);
		boolean accessible = field.isAccessible();
		field.setAccessible(true);
		Object result = field.get(object);
		field.setAccessible(accessible);
		return result;
	}

	/**
	 * <p>setDeclaredProperty.</p>
	 *
	 * @param object a {@link java.lang.Object} object.
	 * @param propertyName a {@link java.lang.String} object.
	 * @param newValue a {@link java.lang.Object} object.
	 * @throws java.lang.IllegalAccessException
	 * @throws java.lang.NoSuchFieldException
	 */
	static public void setDeclaredProperty(Object object, String propertyName, Object newValue) throws IllegalAccessException, NoSuchFieldException {
		Assert.notNull(object);
		Assert.hasText(propertyName);

		Field field = object.getClass().getDeclaredField(propertyName);
		setDeclaredProperty(object, field, newValue);
	}

	/**
	 * <p>setDeclaredProperty.</p>
	 *
	 * @param object a {@link java.lang.Object} object.
	 * @param field a {@link java.lang.reflect.Field} object.
	 * @param newValue a {@link java.lang.Object} object.
	 * @throws java.lang.IllegalAccessException
	 */
	static public void setDeclaredProperty(Object object, Field field, Object newValue) throws IllegalAccessException {
		boolean accessible = field.isAccessible();
		field.setAccessible(true);
		field.set(object, newValue);
		field.setAccessible(accessible);
	}

	/**
	 * <p>invokePrivateMethod.</p>
	 *
	 * @param object a {@link java.lang.Object} object.
	 * @param methodName a {@link java.lang.String} object.
	 * @param params a {@link java.lang.Object} object.
	 * @throws java.lang.NoSuchMethodException
	 * @throws java.lang.IllegalAccessException
	 * @throws java.lang.reflect.InvocationTargetException
	 * @return a {@link java.lang.Object} object.
	 */
	static public Object invokePrivateMethod(Object object, String methodName, Object... params) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		Assert.notNull(object);
		Assert.hasText(methodName);
		Class[] types = new Class[params.length];
		for (int i = 0; i < params.length; i++) {
			types[i] = params[i].getClass();
		}
		Method method = object.getClass().getDeclaredMethod(methodName, types);

		boolean accessible = method.isAccessible();
		method.setAccessible(true);
		Object result = method.invoke(object, params);
		method.setAccessible(accessible);
		return result;
	}

	/**
	 * 按Filed的類型取得Field列表
	 *
	 * @param object a {@link java.lang.Object} object.
	 * @param type a {@link java.lang.Class} object.
	 * @return a {@link java.util.List} object.
	 */
	static public List<Field> getFieldsByType(Object object, Class type) {
		ArrayList<Field> list = new ArrayList<Field>();
		Field[] fields = object.getClass().getDeclaredFields();
		for (Field field : fields) {
			if (field.getType().isAssignableFrom(type)) {
				list.add(field);
			}
		}
		return list;
	}

	/**
	 * 獲得field的getter名稱
	 *
	 * @param type a {@link java.lang.Class} object.
	 * @param fieldName a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getAccessorName(Class type, String fieldName) {
		Assert.hasText(fieldName, "FieldName required");
		Assert.notNull(type, "Type required");

		if (type.getName().equals("boolean")) {
			return "is" + StringUtils.capitalize(fieldName);
		} else {
			return "get" + StringUtils.capitalize(fieldName);
		}
	}

	/**
	 * 獲得field的getter名稱
	 *
	 * @param type a {@link java.lang.Class} object.
	 * @param fieldName a {@link java.lang.String} object.
	 * @return a {@link java.lang.reflect.Method} object.
	 */
	public static Method getAccessor(Class type, String fieldName) {
		try {
			return type.getMethod(getAccessorName(type, fieldName));
		} catch (NoSuchMethodException e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}
}
