package fstop.aop;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>Authorize class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Authorize {
    /**
     * 判斷是否有查詢權限，會與 ADMROLEAUTH.ISQUERY 的權限做比較
     * @return true：是，false：否
     */
    boolean userInRoleCanQuery() default false;

    /**
     * 判斷是否有編輯權限，會與 ADMROLEAUTH.ISEDIT 的權限做比較
     * @return true：是，false：否
     */
    boolean userInRoleCanEdit() default false;

    /**
     * 判斷是否有放行權限，會與 ADMROLEAUTH.ISEXEC 的權限做比較
     * @return true：是，false：否
     */
    boolean userInRoleCanApprove() default false;
}
