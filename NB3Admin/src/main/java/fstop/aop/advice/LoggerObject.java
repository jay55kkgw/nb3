package fstop.aop.advice;

import java.util.Date;

/**
 * <p>LoggerObject class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class LoggerObject {
	
	public boolean isLogging = false;
	
	private String GUID = ""; 

	private String uid = "";
	
	private String adopid = "";

	private String jsp = "";

	private String remoteIP = "";

	private String boname = "";
	
	private String requestData = "";
	
	private String methodName = "";
	
	private String exceptionCode = "";
	
	private Date startTime = new Date();

	private Date endTime = new Date();

	/**
	 * <p>isLogging.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isLogging() {
		return isLogging;
	}

	/**
	 * <p>setLogging.</p>
	 *
	 * @param isLogging a boolean.
	 */
	public void setLogging(boolean isLogging) {
		this.isLogging = isLogging;
	}

	/**
	 * <p>Getter for the field <code>exceptionCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getExceptionCode() {
		return exceptionCode;
	}
 
	/**
	 * <p>Setter for the field <code>exceptionCode</code>.</p>
	 *
	 * @param exceptionCode a {@link java.lang.String} object.
	 */
	public void setExceptionCode(String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	/**
	 * <p>Getter for the field <code>methodName</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * <p>Setter for the field <code>methodName</code>.</p>
	 *
	 * @param methodName a {@link java.lang.String} object.
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * <p>Getter for the field <code>requestData</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getRequestData() {
		return requestData;
	}

	/**
	 * <p>Setter for the field <code>requestData</code>.</p>
	 *
	 * @param requestData a {@link java.lang.String} object.
	 */
	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}

	/**
	 * <p>Getter for the field <code>uid</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * <p>Setter for the field <code>uid</code>.</p>
	 *
	 * @param uid a {@link java.lang.String} object.
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**
	 * <p>Getter for the field <code>boname</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBoname() {
		return boname;
	}

	/**
	 * <p>Setter for the field <code>boname</code>.</p>
	 *
	 * @param boname a {@link java.lang.String} object.
	 */
	public void setBoname(String boname) {
		this.boname = boname;
	}

	/**
	 * <p>Getter for the field <code>remoteIP</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getRemoteIP() {
		return remoteIP;
	}

	/**
	 * <p>Setter for the field <code>remoteIP</code>.</p>
	 *
	 * @param remoteIP a {@link java.lang.String} object.
	 */
	public void setRemoteIP(String remoteIP) {
		this.remoteIP = remoteIP;
	}

	/**
	 * <p>Getter for the field <code>jsp</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getJsp() {
		return jsp;
	}

	/**
	 * <p>Setter for the field <code>jsp</code>.</p>
	 *
	 * @param jsp a {@link java.lang.String} object.
	 */
	public void setJsp(String jsp) {
		this.jsp = jsp;
	}

	/**
	 * <p>Getter for the field <code>adopid</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAdopid() {
		return adopid;
	}

	/**
	 * <p>Setter for the field <code>adopid</code>.</p>
	 *
	 * @param adopid a {@link java.lang.String} object.
	 */
	public void setAdopid(String adopid) {
		this.adopid = adopid;
	}

	/**
	 * <p>getGUID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getGUID() {
		return GUID;
	}

	/**
	 * <p>setGUID.</p>
	 *
	 * @param guid a {@link java.lang.String} object.
	 */
	public void setGUID(String guid) {
		GUID = guid;
	}

	/**
	 * <p>Getter for the field <code>endTime</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * <p>Setter for the field <code>endTime</code>.</p>
	 *
	 * @param endTime a {@link java.util.Date} object.
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * <p>Getter for the field <code>startTime</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * <p>Setter for the field <code>startTime</code>.</p>
	 *
	 * @param startTime a {@link java.util.Date} object.
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
}
