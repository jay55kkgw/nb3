package fstop.aop;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import com.netbank.rest.web.back.service.AuthorityEnum;
import com.netbank.rest.web.back.service.PortalSSOService;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import fstop.orm.dao.SysOpDao;
import fstop.orm.po.SYSOP;
import fstop.util.ESAPIUtils;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 授權 AOP Aspect，需攔截的 Controller 需在 Class 上加上 @Aurhorize()
 *
 * @author 簡哥
 * @version V1.0
 */
@Aspect
@Component
@Slf4j
public class AuthorizeAspect {
    @Autowired
    private PortalSSOService portalSSOService;

    @Autowired
    private SysOpDao sysOpDao;

    @Value("${SKIP.SSO:false}")
    private boolean skipSSO;

    private final String ACCESS_DENIED = "redirect:/Account/AccessDenied";

    private final String LOGIN = "redirect:/Account/Login";

    private final String ERROR = "forward:/Common/Error";

    // pointCut
    /**
     * <p>log.</p>
     */
    @Pointcut("@annotation(fstop.aop.Authorize)")
    public void log() {

    }

    /**
     * AOP，欄截有設 @Authorize 的 Controller，若無權限，則重導至拒絕存取頁面
     *
     * @param joinPoint a {@link org.aspectj.lang.ProceedingJoinPoint} object.
     * @return a {@link java.lang.Object} object.
     */
    @Around("log()")
    public Object doAroundController(ProceedingJoinPoint joinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        
        if (!portalSSOService.isLogins()) {
            if (skipSSO) {
                return LOGIN;
            } else {
                String authCode = request.getParameter("authCode");
                if (authCode != null && !authCode.isEmpty()) {
                    // SSO 有帶 authCode
                    try {
                        //20191120-Danny-Log Forging\路徑 1:
                        log.debug("user not logins, from SSO, authCode={}", Sanitizer.logForgingStr(authCode));

                        return LOGIN + "?authCode=" + URLEncoder.encode(authCode, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        log.error("UnsupportedEncodingException", e);

                        request.setAttribute("errorType", "UnsupportedEncodingException");
                        request.setAttribute("errorDesc", e.getMessage());
                        return ERROR;
                    }
                } else {
                    log.debug("user not logins, from SSO, authCode not exist !!");
                    request.setAttribute("errorType", "Portal Error");
                    request.setAttribute("errorDesc", "使用者尚未由 Portal 登入，authCode 不存在！！");
                    return ERROR;
                }
            }
        }
  
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();                                      // ex: /Home/Index
        Authorize action = method.getAnnotation(Authorize.class);                   // 取得 Controller 設定的 @Authorize 檢核項目

        //20191118-Danny-Second Order SQL Injection\路徑 2:
        ArrayList<SYSOP> newResult = new ArrayList<SYSOP>();
        List<SYSOP> newList = (List<SYSOP>)(Object)sysOpDao.findAll();
        newResult.addAll(newList);
        ArrayList<SYSOP> menus =newResult;

        // Check 使用者有沒有角色的權限
        //ArrayList<SYSOP> menus = (ArrayList<SYSOP>)sysOpDao.findAll();
        
        // URI 找對應的 ADOPID, URI 有可能是
        // /B101/Index?t=xx or /nb3admin/B101/Index?t=xx
        // /B101/Index/XXX?t=xx or /nb3admin/B101/Index?t=xx
        // /nb3admin/B101/Index?t=xx or /nb3admin/B101/Index?t=xx
        // /nb3admin/B101/Index/XXX?t=xx or /nb3admin/B101/Index?t=xx
        String contextPath = request.getContextPath();
        log.debug("contextPath={}", contextPath);
        //Trust Boundary Violation\路徑 1:NB3Admin_20201111.pdf
        String URI = request.getRequestURI();
        String cleanURI = ESAPIUtils.validStr(URI);
        
		String requestADOPID = cleanURI.substring(1);
		String[] tmps = requestADOPID.split("/");
		if ( contextPath.compareToIgnoreCase("") == 0 ) {
			requestADOPID = tmps[0];
		} else {
			requestADOPID = tmps[1];
		}

        Optional<SYSOP> menu = findMenu(menus, requestADOPID);
        if ( !menu.isPresent() ) {
            // 在選單上沒有定義，就不會有角色有此功能，故不檢核 Controller 上設定的權限
            // 例如：Home/Index，不會定在後台選單上
            //try {
                return joinPoint.proceed();
            //} catch (Throwable e) {
            //    log.warn("AuthorizeAspect joinPoint.proceed error", e);
                //return ERROR;
            //    throw e;
            //}
        }

        String adOPID = menu.get().getADOPID();
        String adOPName = menu.get().getADOPNAME();

        boolean queryMatch = false;
        if ( action.userInRoleCanQuery() ) {
            // check ISQUERY flag
            queryMatch = portalSSOService.isPermissionOK(adOPID, AuthorityEnum.QUERY );
        } else {
            queryMatch = true;
        }

        boolean editMatch = false;
        if ( action.userInRoleCanEdit() ) {
            // check ISEDIT flag
            editMatch = portalSSOService.isPermissionOK(adOPID, AuthorityEnum.EDIT );
        } else {
            editMatch = true;
        }

        boolean execMatch = false;
        if ( action.userInRoleCanApprove() ) {
            // check ISEXEC flag
            execMatch = portalSSOService.isPermissionOK(adOPID, AuthorityEnum.REVIEW );
        } else {
            execMatch = true;
        }
        
        if ( queryMatch && editMatch && execMatch ) {
        	// 預留這個屬性, 如客戶要求全部功能 Title 要與 SYSOP 設定一致時, 可以參考 B301/index.jsp 的改法(使用:${__MENU_NAME__})
        	request.setAttribute("__MENU_NAME__", adOPName);
            return joinPoint.proceed();
        } else {
            return ACCESS_DENIED;
        } 
    }

    private Optional<SYSOP> findMenu(ArrayList<SYSOP> menus, String adopid) {
        return menus.stream().filter(m->m.getADOPID().equalsIgnoreCase(adopid)).findFirst();
    }
}
