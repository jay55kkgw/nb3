package fstop.aop;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * <p> Spring Utility </p>
 * @author 簡哥
 * @since
 */
@Component
public class SpringUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext = null;

    /**
     * 設定 Spring ApplicationContext
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if(SpringUtil.applicationContext == null){
            SpringUtil.applicationContext  = applicationContext;
        }
    }

    /**
     * 取得 Spring ApplicationContext
     * @return          Spring ApplicationContext
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
    
    /**
     * 取得 Spring 管理的 Bean
     * @param name      物件名稱
     * @return          找到的類別
     */
    public static Object getBean(String name){
        return getApplicationContext().getBean(name);

    }

    /**
     * 取得 Spring 管理的 Bean
     * @param <T>       要找的泛型類別
     * @param clazz     類別
     * @return          找到的類別
     */
    public static <T> T getBean(Class<T> clazz){
        return getApplicationContext().getBean(clazz);
    }

    /**
     * 取得 Spring 管理的 Bean
     * @param <T>       要找的泛型類別
     * @param name      物件名稱
     * @param clazz     類別
     * @return          找到的類別
     */
    public static <T> T getBean(String name,Class<T> clazz){
        return getApplicationContext().getBean(name, clazz);
    }

}