package fstop.aop;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;

import com.google.common.base.Splitter;
import com.google.gson.Gson;
import com.netbank.rest.web.back.service.PortalSSOService;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import fstop.orm.dao.UserActionLogDao;
import fstop.orm.po.USERACTIONLOG;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

import org.slf4j.MDC;


import fstop.util.Sanitizer;

/**
 * 給登入頁/首頁顯示登入者資訊用
 *
 * @author 簡哥
 * @version V1.0
 */
@Aspect
@Component
@Slf4j
public class LoginInfoAspect  {
    @Autowired
    private PortalSSOService portalSSOService;
    
    @Autowired
    private UserActionLogDao userActionLogDao;
    
    @Value("${SKIP.SSO:false}")
    private boolean skipSSO;
    
    @Value("${USERACTIONLOG.LOGAJAX:false}")
    private boolean logAJAX;

    /**
     * <p>設定使用者登入相關資訊到 HttpRequest Attributes 中，以利 View 可以使用 JSTL 顯示使用者資訊</p>
     *
     * @param joinPoint a {@link org.aspectj.lang.JoinPoint} object.
     * @return a {@link java.lang.Object} object.
     */
    @Before("within(com.netbank.rest.web.back.controller..*)")
    public Object doBeforeController(JoinPoint joinPoint) {
    try {
    	log.info(joinPoint.getTarget().getClass().getName()+"."+joinPoint.getSignature().getName()+ "， Before Time : "+new SimpleDateFormat("HH:mm:ss.S").format(new Date()));
    	
        log.trace("doBeforeController, user isLogins="+portalSSOService.isLogins());
        
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest(); 
        //20191115-Danny-Reflected XSS All Clients\路徑 1:
        String ipAddress = request.getHeader("X-FORWARDED-FOR");  
        ipAddress =(String)Sanitizer.escapeHTML(ipAddress); 
        if (ipAddress == null) {  
            ipAddress = request.getRemoteAddr();  
        }

        request.setAttribute("__SKIP_SSO__", skipSSO);
        request.setAttribute("__LOGIN_TIME__", portalSSOService.getLoginTime());
        request.setAttribute("__LOGIN_BRANCH__", portalSSOService.getLoginBranch());
        request.setAttribute("__LOGIN_USERID__", portalSSOService.getLoginUserId());
        request.setAttribute("__LOGIN_USERNAME__", portalSSOService.getLoginUserName());
        request.setAttribute("__LOGIN_IP__", ipAddress);

        

        request.setAttribute("__LOGIN_ROLES__", String.join(",", portalSSOService.getLoginRoles()));
      
            MDC.put("loginid", portalSSOService.getLoginUserId());
        } catch (Exception e) {
            log.error("MDC.put error", e.getMessage());
        }
        
        return true;
    }
    
    //檢查時間差
    @After("within(com.netbank.rest.web.back.controller..*)")
    public Object doAfterController(JoinPoint joinPoint) {   
    	try {
    		log.info( joinPoint.getSignature().getDeclaringType().getName()+"."+joinPoint.getSignature().getName() +"， After Time : "+new SimpleDateFormat("HH:mm:ss.S").format(new Date()));
    	}catch(RuntimeException e) {
    		log.error("e>>>{}",e);
    	}
    	return joinPoint;
    }
    
    //檢查時間差
    @Before("within(com.netbank.rest.web.back.service..*) && !within(com.netbank.rest.web.back.service.PortalSSOService)")
    public Object doBeforeServiceController(JoinPoint joinPoint) {
    	try {
    		log.info( joinPoint.getSignature().getDeclaringType().getName()+"."+joinPoint.getSignature().getName() +"，  Before Time : "+new SimpleDateFormat("HH:mm:ss.S").format(new Date()));
    	}catch(RuntimeException e) {
    		log.error("e>>>{}",e);
    	}
    	return joinPoint;
    }

    
    //檢查時間差
    @After("within(com.netbank.rest.web.back.service..*) && !within(com.netbank.rest.web.back.service.PortalSSOService)")
    public Object doAfterServiceController(JoinPoint joinPoint) {
    	try {
    		log.info(joinPoint.getSignature().getDeclaringType().getName()+"."+joinPoint.getSignature().getName()  +"， After Time : "+new SimpleDateFormat("HH:mm:ss.S").format(new Date()));
    	}catch(RuntimeException e) {
    		log.error("e>>>{}",e);
    	}
    	return joinPoint;
    }
    
    //檢查時間差
    @Before("within(fstop.orm.dao..*)")
    public Object doBeforeDAOController(JoinPoint joinPoint) {
    	try {
    		log.info( joinPoint.getSignature().getDeclaringType().getName()+"."+joinPoint.getSignature().getName()  + " ， Before Time : "+new SimpleDateFormat("HH:mm:ss.S").format(new Date()));
    	}catch(RuntimeException e) {
    		log.error("e>>>{}",e);
    	}
    	return joinPoint;
    }

    
    //檢查時間差
    @After("within(fstop.orm.dao..*)")
    public Object doAfterDAOController(JoinPoint joinPoint) {
	    try {
	    	log.info( joinPoint.getSignature().getDeclaringType().getName()+"."+joinPoint.getSignature().getName() +" After Time : "+new SimpleDateFormat("HH:mm:ss.S").format(new Date()));
		}catch(RuntimeException e) {
			log.error("e>>>{}",e);
		}
    	return joinPoint;
    }


    /**
     * 記錄使用者軌跡記錄, TBB 暫時不用了, 所以以下程式碼註解起來
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("within(com.netbank.rest.web.back.controller..*)")
    public Object doAroundController(ProceedingJoinPoint joinPoint) throws Throwable {

    
        Object result = joinPoint.proceed();
        try {
            MDC.remove("loginid");
        } catch (Exception e) {
            log.error("MDC.remove error", e.getMessage());
        }
        return result;
        // Date requestDt = new Date();

        // // 取得前端使用者 IP
        // HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        // String ipAddress = request.getHeader("X-FORWARDED-FOR");  
        // if (ipAddress == null) {  
        //     ipAddress = request.getRemoteAddr();  
        // }

        // Object ctrlResult = joinPoint.proceed();
        
        // //String headerName = request.getHeader("x-requested-with");

        // String uri = request.getRequestURI();       // uri=/B109/IndexQuery
        // String opId = uri.split("/")[1];

        // Future<String> logResult = logUserAction(portalSSOService.getLoginUserId(), request.getRequestURI(), request.getMethod(), ipAddress,
        //     requestDt, new Date(), getFormData(request), getRequestParams(request), opId);
        
        // log.debug("logResult={}", logResult.get());

        // return ctrlResult;
    }

    /**
     * 取得 Query String，轉成 JSON String
     * @param request   Http Request 
     * @return          JSON String
     */
    private String getRequestParams(HttpServletRequest request) {
        String url = request.getRequestURL().toString();
        if ( url.contains("?") ) {
            Map<String, String> map = Splitter.on('&').trimResults().withKeyValueSeparator('=').split(url);
            return new Gson().toJson(map);
        } else {
            return new Gson().toJson(new HashMap<String, String>());
        }
    }
    
    /**
     * 取得 Form Data，轉成 JSON String
     * @param request   Http Request
     * @return          JSON String
     */
    private String getFormData(HttpServletRequest request) {
        Enumeration paramNames = request.getParameterNames();
        Map<String, Object> map = new HashMap<String, Object>();
        
        while(paramNames.hasMoreElements()) {
            String paramName = (String)paramNames.nextElement();
            String[] paramValues = request.getParameterValues(paramName);

            switch ( paramValues.length ) {
                case 0:
                    continue;
                
                case 1:
                    map.put(paramName, paramValues[0]);
                    break;

                default:
                    map.put(paramName, paramValues);
            }
        }

        return new Gson().toJson(map);
    }
    
    /**
     * 記錄使用者記錄
     * @param loginUser     記錄者代碼
     * @param url           Request URI
     * @param method        POST/GET
     * @param userIP        記錄者 IP
     * @param reqDt         Request Datetime
     * @param respDt        Response Datetime
     * @param formData      Request Form Data
     * @param paramData     Request Parameters
     * @param opId          交易代號
     * @return
     */
    @Async
    private Future<String> logUserAction(String loginUser, String url, String method, String userIP, Date reqDt, Date respDt, String formData, String paramData, String opId) {
        USERACTIONLOG po = new USERACTIONLOG();
        try {
            po.setLOGINUSER(loginUser);
            po.setURL(url);
            po.setMETHOD(method);
            po.setUSERIP(userIP);
            po.setRQTIMESTAMP(DateTimeUtils.getDatetime(reqDt));
            po.setRSTIMESTAMP(DateTimeUtils.getDatetime(respDt));
            po.setFORMDATA(formData);
            po.setPARAMDATA(paramData);
            po.setOPID(opId);
            userActionLogDao.save(po);

            return new AsyncResult<String>("0");
        } catch (Exception e) {
            log.error("log UserActionLog error", e);
            return new AsyncResult<String>("1");
        }
    }
}
