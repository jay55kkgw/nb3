package fstop.model;

import java.util.List;
import java.util.Map;

/**
 * <p>IMasterValuesHelper interface.</p>
 *
 * @author Administrator
 * @version V1.0
 */
public interface IMasterValuesHelper {

    ///**
    // * 取得組成 IMasterValuesHelper 所有電文交易過程
    // * @return
    // */
    //public List getTelcommProcesses();

    /*
     * 由JSP上傳的所有 Parameter 資料
     */
    /**
     * <p>getInputQueryData.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    public Map getInputQueryData();

    /*
     * 取得 fieldName 所對應的內容
     */
    /**
     * <p>getValueByFieldName.</p>
     *
     * @param fieldName a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public String getValueByFieldName(String fieldName);

    /*
     * 若 fieldName 所對應的內容為多筆資料, 則回傳第 index 筆資料 , index 由 1 開始,
     * 若非多筆資料, 則會回傳 getValueByFieldName(String fieldName) 的資料
     */
    /**
     * <p>getValueByFieldName.</p>
     *
     * @param fieldName a {@link java.lang.String} object.
     * @param index a int.
     * @return a {@link java.lang.String} object.
     */
    public String getValueByFieldName(String fieldName, int index);


    /**
     * <p>getTableCount.</p>
     *
     * @return a int.
     */
    public int getTableCount();

    /**
     * <p>getTableKeys.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List getTableKeys();

    /**
     * <p>getTableByIndex.</p>
     *
     * @param index a int.
     * @return a {@link fstop.model.IMasterValuesHelper} object.
     */
    public IMasterValuesHelper getTableByIndex(int index);

    /**
     * <p>getTableByName.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @return a {@link fstop.model.IMasterValuesHelper} object.
     */
    public IMasterValuesHelper getTableByName(String name);


    /*
     * 取得某一個 field 的迴圈次數
     */
    /**
     * <p>getValueOccurs.</p>
     *
     * @param fieldName a {@link java.lang.String} object.
     * @return a int.
     */
    public int getValueOccurs(String fieldName);

    /*
     * 回傳 filedName 所對應的內容是否為多筆資料
     */
    /**
     * <p>isOccurs.</p>
     *
     * @param fieldName a {@link java.lang.String} object.
     * @return a boolean.
     */
    public boolean isOccurs(String fieldName);

    /**
     * <p>translate.</p>
     *
     * @return a {@link fstop.model.IMasterValuesHelper} object.
     */
    public IMasterValuesHelper translate();

    /**
     * <p>dump.</p>
     */
    public void dump();
    /**
     * <p>dumpToString.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String dumpToString();
}
