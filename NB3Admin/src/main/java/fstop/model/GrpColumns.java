package fstop.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Map;
import java.util.Set;

/**
 * <p>GrpColumns class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class GrpColumns {
	Map<String, String> values;  //colname, value
	String[] cols;
	/**
	 * <p>Constructor for GrpColumns.</p>
	 *
	 * @param values a {@link java.util.Map} object.
	 * @param cols an array of {@link java.lang.String} objects.
	 */
	public GrpColumns(Map<String, String> values, String[] cols){
		this.values = values;
		this.cols = cols;
	}
	
	/**
	 * <p>getColumnNames.</p>
	 *
	 * @return a {@link java.util.Set} object.
	 */
	public Set<String> getColumnNames() {
		return values.keySet();
	}
	
	/**
	 * <p>getValue.</p>
	 *
	 * @param colname a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String getValue(String colname) {
		return values.get(colname);
	}
	
	/**
	 * <p>setValue.</p>
	 *
	 * @param columnName a {@link java.lang.String} object.
	 * @param value a {@link java.lang.String} object.
	 */
	public void setValue(String columnName, String value) {
		values.put(columnName, value);
	}
	
	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		StringBuffer result = new StringBuffer();
		for(String s : this.cols) {
			if(result.length() > 0)
				result.append("_");
			result.append(values.get(s));
		}
		return result.toString();
	}
	
	/** {@inheritDoc} */
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if (!(other instanceof GrpColumns))
			return false;
		GrpColumns castOther = (GrpColumns) other;
		EqualsBuilder b = new EqualsBuilder();
		for(String col : cols) {
			b.append(this.getValue(col), castOther.getValue(col));
		}
		
		return b.isEquals();
	}

	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode() {
		HashCodeBuilder h = new HashCodeBuilder();
		for(String col : cols) {
			h.append(col);
		}
		
		return h.toHashCode();
	}
	
}
