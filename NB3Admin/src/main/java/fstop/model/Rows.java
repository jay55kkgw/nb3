package fstop.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>Rows class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class Rows implements Serializable {
	private List<Row> rows;  //colname, value
	
	/**
	 * <p>Constructor for Rows.</p>
	 */
	public Rows() {
		this.rows = new ArrayList<Row>();
	}
	
	/**
	 * <p>Constructor for Rows.</p>
	 *
	 * @param rows a {@link java.util.List} object.
	 */
	public Rows(List<Row> rows){
		this.rows = rows; 
	}
	
	/**
	 * <p>getColumnNames.</p>
	 *
	 * @return a {@link java.util.Set} object.
	 */
	public Set<String> getColumnNames() {
		if(rows != null && rows.size() > 0) {
			return rows.get(rows.size()-1).getColumnNames();
		}
		else {
			return new HashSet();
		}
	}
	
	/**
	 * <p>addRow.</p>
	 *
	 * @param row a {@link fstop.model.Row} object.
	 */
	public void addRow(Row row) {
		rows.add(row);
	}

	/**
	 * <p>addRows.</p>
	 *
	 * @param addrows a {@link fstop.model.Rows} object.
	 */
	public void addRows(Rows addrows) {
		for(int i=0; addrows != null && i < addrows.getSize(); i++) {
			this.rows.add(addrows.getRow(i));
		}
	}
	
	/**
	 * <p>removeRow.</p>
	 *
	 * @param row a {@link fstop.model.Row} object.
	 */
	public void removeRow(Row row) {
		rows.remove(row);
	}
	
	/**
	 * <p>getRow.</p>
	 *
	 * @param index a int.
	 * @return a {@link fstop.model.Row} object.
	 */
	public Row getRow(int index) {
		return rows.get(index);
	}

	/**
	 * <p>getSize.</p>
	 *
	 * @return a int.
	 */
	public int getSize() {
		return rows.size();
	}
	
	/**
	 * <p>isIncludeColumn.</p>
	 *
	 * @param name a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	public boolean isIncludeColumn(String name) {
		return  getColumnNames().contains(name);
	}

	/**
	 * <p>Getter for the field <code>rows</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Row> getRows() {
		return rows;
	}
	
	/**
	 * <p>setValue.</p>
	 *
	 * @param columnName a {@link java.lang.String} object.
	 * @param value a {@link java.lang.String} object.
	 */
	public void setValue(String columnName, String value) {
		for(Row r : rows) {
			r.setValue(columnName, value);
		}
	}

}
