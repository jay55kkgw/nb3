package fstop.model;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * <p>Row class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class Row implements Serializable {
	Map<String, String> row;  //colname, value
	/**
	 * <p>Constructor for Row.</p>
	 *
	 * @param row a {@link java.util.Map} object.
	 */
	public Row(Map<String, String> row){
		this.row = row;
	}
	
	/**
	 * <p>getColumnNames.</p>
	 *
	 * @return a {@link java.util.Set} object.
	 */
	public Set<String> getColumnNames() {
		return row.keySet();
	}
	
	/**
	 * <p>getValue.</p>
	 *
	 * @param colname a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String getValue(String colname) {
		return row.get(colname);
	}
	
	/**
	 * <p>setValue.</p>
	 *
	 * @param columnName a {@link java.lang.String} object.
	 * @param value a {@link java.lang.String} object.
	 */
	public void setValue(String columnName, String value) {
		row.put(columnName, value);
	}
	
	/**
	 * <p>getValues.</p>
	 *
	 * @return a {@link java.util.Map} object.
	 */
	public Map<String, String> getValues() {
		return row;
	}	
}
