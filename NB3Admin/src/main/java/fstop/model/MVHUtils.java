package fstop.model;

import fstop.util.StrUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <p>MVHUtils class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class MVHUtils {

	/**
	 * 對指定的 occur key 做 group by 的動作, 並寫到 table 去
	 *
	 * @param telcommResult a {@link fstop.model.MVHImpl} object.
	 * @param groupName an array of {@link java.lang.String} objects.
	 * @return a {@link java.util.Map} object.
	 */
	public static Map<String, IMasterValuesHelper> groupByToTables(MVHImpl telcommResult, String[] groupName) {
		Map<String, IMasterValuesHelper> result = new LinkedHashMap();
		try {
			RowsGroup rowsGroup = new RowsGroup(telcommResult.getOccurs());
			rowsGroup.setGroupColumn(groupName);
			
			Map<GrpColumns, RowsSelecter> groups = rowsGroup.getGroups();
			for(GrpColumns grpCol : groups.keySet()) {
				String grpName = grpCol.toString();
				
				Rows rows = new Rows(groups.get(grpCol).getSelectedRows());
				MVHImpl mvh = new MVHImpl(rows);
				
				for(String col : grpCol.getColumnNames()) {
					if(rows.getSize() > 0) {
						mvh.getFlatValues().put(col, rows.getRow(0).getValue(col));
					}
				}
				
				result.put(grpName, mvh);
			}
		}
		catch(Exception e) {
			throw new ModelException("MVHUtils.groupByToTables 發生錯誤 !", e);
		}
		return result;
	}
	
	/**
	 * <p>addTables.</p>
	 *
	 * @param mater a {@link fstop.model.MVHImpl} object.
	 * @param tables a {@link java.util.Map} object.
	 */
	public static void addTables(MVHImpl mater, Map<String, IMasterValuesHelper> tables) {
		for(Entry<String, IMasterValuesHelper> o : tables.entrySet()) {
			mater.addTable(o.getValue(), o.getKey());
		}
	}
	
	/**
	 * <p>group.</p>
	 *
	 * @param ros a {@link fstop.model.Rows} object.
	 * @param groupName an array of {@link java.lang.String} objects.
	 * @return a {@link fstop.model.RowsGroup} object.
	 */
	public static RowsGroup group(Rows ros, String[] groupName) {
		RowsGroup rowsGroup = new RowsGroup(ros);
		rowsGroup.setGroupColumn(groupName);
		return rowsGroup;
	}

	/**
	 * <p>group.</p>
	 *
	 * @param ros a {@link java.util.List} object.
	 * @param groupName an array of {@link java.lang.String} objects.
	 * @return a {@link fstop.model.RowsGroup} object.
	 */
	public static RowsGroup group(List<Row> ros, String[] groupName) {
		RowsGroup rowsGroup = new RowsGroup(ros);
		rowsGroup.setGroupColumn(groupName);
		return rowsGroup;
	}

	/**
	 * <p>select.</p>
	 *
	 * @param ros a {@link fstop.model.Rows} object.
	 * @param rowFilter a {@link fstop.model.RowFilterCallback} object.
	 * @return a {@link fstop.model.RowsSelecter} object.
	 */
	public static RowsSelecter select(Rows ros, RowFilterCallback rowFilter) {
		RowsSelecter rowsSelecter = new RowsSelecter(ros);
		rowsSelecter.setFilter(rowFilter);
		return rowsSelecter;
	}

	/**
	 * <p>toIMasterValuesHelperImpl.</p>
	 *
	 * @param rowList a {@link java.util.List} object.
	 * @return a {@link fstop.model.MVHImpl} object.
	 */
	public static MVHImpl toIMasterValuesHelperImpl(List<Row> rowList) {
		Rows rows = new Rows(rowList);
		return new MVHImpl(rows);
	}

	/**
	 * <p>toFlatValues.</p>
	 *
	 * @param col a {@link fstop.model.GrpColumns} object.
	 * @param keyPrefix a {@link java.lang.String} object.
	 * @param index a int.
	 * @return a {@link java.util.Map} object.
	 */
	public static Map<String, String> toFlatValues(GrpColumns col, String keyPrefix, int index) {
		Map<String, String> result = new HashMap();
		for(String colName : col.getColumnNames()) {
			result.put(StrUtils.trim(keyPrefix) + colName + "_" + index, col.getValue(colName) + "");
		}
		return result;
	}
	
	/**
	 * 對 MVH 裡的所有 Table 裡的某一個 occurs 欄位, 做加總
	 *
	 * @param mvh a {@link fstop.model.MVHImpl} object.
	 * @param column a {@link java.lang.String} object.
	 * @return a {@link java.lang.Double} object.
	 */
	public static Double sumTablesOccurColumn(MVHImpl mvh, String column) {
		Double result = 0d;
		
		for(int i = 0; i < mvh.getTableCount(); i++) {
			MVHImpl imv = (MVHImpl)mvh.getTableByIndex(i + 1);
			for(int x = 0; imv.isOccurs(column) && x < imv.getValueOccurs(column); x++) {
				
				try {
					String v = imv.getOccurs().getRow(x).getValue(column);
					result += new Double(v);
				}
				catch(Exception e) {}
			}
		}
		
		return result;
	}
	
	/**
	 * 對 MVH 裡的所有 Table 裡的某一個不是 occurs 的欄位, 做加總
	 *
	 * @param mvh a {@link fstop.model.MVHImpl} object.
	 * @param column a {@link java.lang.String} object.
	 * @return a {@link java.lang.Double} object.
	 */
	public static Double sumTablesFlatColumn(MVHImpl mvh, String column) {
		Double result = 0d;
		
		for(int i = 0; i < mvh.getTableCount(); i++) {
			MVHImpl imv = (MVHImpl)mvh.getTableByIndex(i + 1);
			String v = imv.getFlatValues().get(column);
			try {
				result += new Double(v);
			}
			catch(Exception e) {}
		}
		
		return result;
	}
	
	/**
	 * <p>sumTablesOccurs.</p>
	 *
	 * @param mvh a {@link fstop.model.MVHImpl} object.
	 * @return a {@link java.lang.Long} object.
	 */
	public static Long sumTablesOccurs(MVHImpl mvh) {
		Long result = 0L;
		
		for(int i = 0; i < mvh.getTableCount(); i++) {
			MVHImpl imv = (MVHImpl)mvh.getTableByIndex(i + 1);
			result += imv.getOccurs().getSize();
		}
		
		return result;
	}
	
	
}
