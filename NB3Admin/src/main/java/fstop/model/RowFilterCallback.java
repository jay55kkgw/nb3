package fstop.model;

/**
 * <p>RowFilterCallback interface.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public interface RowFilterCallback {
	/**
	 * <p>accept.</p>
	 *
	 * @param row a {@link fstop.model.Row} object.
	 * @return a boolean.
	 */
	public boolean accept(Row row);
}
