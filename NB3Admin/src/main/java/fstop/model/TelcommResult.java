package fstop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fstop.util.StrUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.*;

/**
 * <p>TelcommResult class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Data
@Slf4j
public class TelcommResult extends MVHImpl implements Serializable {
	@JsonIgnore
	private Logger logger = Logger.getLogger(TelcommResult.class);

	@JsonIgnore
	private List telcommVector = null;
	
	private List<String> TOPMSG = new ArrayList<>();
	
	/**
	 * <p>Constructor for TelcommResult.</p>
	 *
	 * @param telcommVector a {@link java.util.List} object.
	 */
	public TelcommResult(List telcommVector) {
		this(telcommVector, null);
	}
	
	/**
	 * <p>Constructor for TelcommResult.</p>
	 *
	 * @param telcommVector a {@link java.util.List} object.
	 * @param occursFieldName a {@link java.lang.String} object.
	 */
	public TelcommResult(List telcommVector, String occursFieldName) {
		super();
		
		int totalOccursCount = 0;
		Set<String> occursKey = new HashSet();
		Rows rows = new Rows();
		if(telcommVector != null) {
			this.telcommVector = telcommVector;
			for(Object o : telcommVector) {
				
				int count = -1;
				
				if(o instanceof Map) {
					Map rec = (Map)o;

					// 因為電文回傳的資料, 若定義固定的 occurs 次, 則會回傳的 Vector.size() = occurs 
					// 所以要依照定義的 COUNT , 裁減多的資料  (一定要定義), 或者設定 OccursNumFromValue 
					for(Object key : rec.keySet()) {
						Object content = rec.get(key);

						if(content instanceof String){
							if(key != null && content != null) {
								getFlatValues().put((String)key, StrUtils.trim((String)content));
								if("TOPMSG".equalsIgnoreCase((String)key))
										TOPMSG.add(StrUtils.trim((String)content));
							}
						}
					} 
					
					boolean isUsingOccursKey = (occursFieldName != null && getFlatValues().containsKey(occursFieldName));
					if(isUsingOccursKey) {
						try {
							count = Integer.parseInt((String)getFlatValues().get(occursFieldName));
							totalOccursCount += count;
						}
						catch(Exception e) {}
					}
					
					for(Object key: rec.keySet()) {
						Object content = rec.get(key);
						if(content instanceof List) {
							if("OCCURS_VECTOR_FOR_MULTIPLE_INDEX".equalsIgnoreCase((String)key)) {
								//do nothing
							}
							else {
								
								if(!isUsingOccursKey) {
									try {
										if(count == -1) {
											count = ((List)content).size();
											totalOccursCount += count;
										}
									}
									catch(Exception e) {}
								}
								
								//logger.debug("OCCURS FIELD NAME: " +  (occursFieldName != null ? occursFieldName : "NULL" ) );
								//logger.debug("OCCURS RECNO: " + count);
								
								
								String itemKey = (String)key;
								occursKey.add(itemKey);
								if(count != -1) {
									int begin = totalOccursCount - count;
									for(int i=0; count != -1 && i < count; i++) {
										
										int currentRow = begin+i;
										Row r = null;
										try {
											r = rows.getRow(currentRow);
										}
										catch(IndexOutOfBoundsException e) {
											r = new Row(new HashMap());
											rows.addRow(r);
										}
										
										String v = (String)((List)content).get(i);
										
										rows.getRow(currentRow).setValue(itemKey, v);
									}
									
								}
								
//								String itemKey = (String)key;
//								
//								if(!mIndexRec.containsKey(itemKey))
//									mIndexRec.put((String)itemKey, new Integer(0));
//
//								int currentLoop = -1;
//								for(Object val : (List)content) {
//									//先將 "欄位名稱_index", 及 "值" 塞入 
//									currentLoop++;
//									if(count != -1 && currentLoop >= count)
//										break;
//									
//									tempValues.put(itemKey + "_" + (mIndexRec.get(itemKey) + 1), StrUtils.trim((String)val));
//									mIndexRec.put(itemKey, (mIndexRec.get(itemKey) + 1));
//								}
							}
						}						
					}
					
				}
				else {
					log.info("[TelcommVectorHelper] 錯誤的型態 !!");
				}
			}
		} 
/*
		//對 Occurs 的部份做處理
		Set<String> colnames = mIndexRec.keySet();
		int max = 0;
		for(Integer index : mIndexRec.values()) {
			if(index > max)
				max = index;
		}
		for(int i=0; i < max; i++) {
			Map<String, String> r = new HashMap();
			for(String coln : colnames) {
				//將Hashmap 變成 "欄位名稱", 及 "值"
				String key = coln + "_" + (i+1);
				r.put(coln, (String)tempValues.get(key));
			}
			getOccurs().addRow(new Row(r));
		}		
		*/
		if(getOccurs().getSize() > 0) {
			for(String key : occursKey) {
				if(StrUtils.isEmpty(getOccurs().getRow(0).getValue(key))) {
					getOccurs().getRow(0).setValue(key, "");
				}
			}
		}
		
		getOccurs().addRows(rows);
		
		//請勿移除, TopMsgDecter 使用
		getFlatValues().put("__OCCURS", totalOccursCount + "");
		
	}
//
//	public List getTelcommVector() {
//		return telcommVector;
//	}
//
//	public void setTelcommVector(Vector telcommVector) {
//		this.telcommVector = telcommVector;
//	}
//
//	public List<String> getTOPMSG() {
//		return TOPMSG;
//	}


}
