package fstop.model;

import org.apache.log4j.Logger;

import java.util.*;
import java.util.Map.Entry;

/**
 * <p>RowsGroup class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class RowsGroup {
	private Logger logger = Logger.getLogger(getClass());

	private Rows rows = null;
	private Map<GrpColumns, RowsSelecter> groups = null;
	private RowGroupFilter groupFilter = null;
	private String[] groupColumn;

	/**
	 * <p>Constructor for RowsGroup.</p>
	 *
	 * @param _rows a {@link java.util.List} object.
	 */
	public RowsGroup(List<Row> _rows) {
		this(_rows, null);
	}

	/**
	 * <p>Constructor for RowsGroup.</p>
	 *
	 * @param _rows a {@link java.util.List} object.
	 * @param groupFilter a {@link fstop.model.RowGroupFilter} object.
	 */
	public RowsGroup(List<Row> _rows, RowGroupFilter groupFilter) {
		rows = new Rows();
		for(Row r: _rows) {
			rows.addRow(r);
		}
		if(groupFilter != null)
			setGroupFilter(groupFilter);
	}

	/**
	 * <p>Constructor for RowsGroup.</p>
	 *
	 * @param rows a {@link fstop.model.Rows} object.
	 */
	public RowsGroup(Rows rows) {
		this(rows, null);
	}

	/**
	 * <p>Constructor for RowsGroup.</p>
	 *
	 * @param rows a {@link fstop.model.Rows} object.
	 * @param groupFilter a {@link fstop.model.RowGroupFilter} object.
	 */
	public RowsGroup(Rows rows, RowGroupFilter groupFilter) {
		this.rows = rows;
		if(groupFilter != null)
			setGroupFilter(groupFilter);
	}

	/**
	 * <p>Setter for the field <code>groupColumn</code>.</p>
	 *
	 * @param groupColumn an array of {@link java.lang.String} objects.
	 */
	public void setGroupColumn(final String[] groupColumn) {
		this.groupColumn = groupColumn;
		setGroupFilter(new RowGroupFilter(){

			public String groupKey(Row row) {
				StringBuffer result = new StringBuffer();
				if(groupColumn != null) {
					for(int i=0; i < groupColumn.length; i++) {
						if(result.length() > 0) {
							result.append("_");
						}
						result.append(row.getValue(groupColumn[i]));
					}
				}
				return result.toString();
			}});
	}
	
	private void setGroupFilter(RowGroupFilter grouper) {
		if(grouper == null)
			new ModelException("傳入的 GroupFilter 不可為 Null.");
		this.groups = new LinkedHashMap<GrpColumns, RowsSelecter>();
		this.groupFilter = grouper;
		LinkedHashMap<String, List<Row>> gr = new LinkedHashMap();
		for(int i=0; i < rows.getSize(); i++) {
			String groupKey = grouper.groupKey(rows.getRow(i));
			if(!gr.containsKey(groupKey))
				gr.put(groupKey, new ArrayList<Row>());
			gr.get(groupKey).add(rows.getRow(i));
		}
		for(Entry<String, List<Row>> ent : gr.entrySet()) {
			Map<String, String> c = new HashMap();
			for(String s : this.groupColumn) {
				c.put(s, ent.getValue().get(0).getValue(s));
			}		
			this.groups.put(new GrpColumns(c, this.groupColumn), new RowsSelecter(new Rows(ent.getValue())));
		}
	}

	/**
	 * <p>groupIterator.</p>
	 *
	 * @return a {@link java.util.Iterator} object.
	 */
	public Iterator<RowsSelecter> groupIterator() {
		return groups.values().iterator();
	}
	
	/**
	 * <p>Getter for the field <code>groups</code>.</p>
	 *
	 * @return a {@link java.util.Map} object.
	 */
	public Map<GrpColumns, RowsSelecter> getGroups() {
		return groups;
	}
	
	/**
	 * <p>sum.</p>
	 *
	 * @param columnName a {@link java.lang.String} object.
	 * @return a {@link java.util.Map} object.
	 */
	public Map<GrpColumns, Double> sum(String columnName) {
		Map<GrpColumns, Double> result = new LinkedHashMap();
		for(Entry<GrpColumns, RowsSelecter> ent : groups.entrySet()) {
			result.put(ent.getKey(), ent.getValue().sum(columnName));
		}
		return result;
	}
	
	/**
	 * <p>sumtoColumn.</p>
	 *
	 * @param columnName a {@link java.lang.String} object.
	 * @return a {@link java.util.Map} object.
	 */
	public Map<GrpColumns, Integer> sumtoColumn(String columnName) {
		Map<GrpColumns, Integer> result = new LinkedHashMap();
		for(Entry<GrpColumns, RowsSelecter> ent : groups.entrySet()) {
			result.put(ent.getKey(), ent.getValue().sumtoColumn(columnName));
		}
		return result;
	}
	
	/**
	 * <p>avg.</p>
	 *
	 * @param columnName a {@link java.lang.String} object.
	 * @return a {@link java.util.Map} object.
	 */
	public Map<GrpColumns, Double> avg(String columnName) {
		Map<GrpColumns, Double> result = new LinkedHashMap();
		for(Entry<GrpColumns, RowsSelecter> ent : groups.entrySet()) {
			//logger.debug("key : " + ent.getKey().getValue("CRY"));
			result.put(ent.getKey(), ent.getValue().avg(columnName));
		}
		return result;
	}
	
}
