package fstop.model;

import fstop.util.CustomProperties;
import fstop.util.StrUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Observable;
import java.util.Observer;

/**
 * 透過檔案取得 name, value 所實際對上的中文描述
 *
 * @author jimmy
 * @version V1.0
 */
public class DefaultTranslator implements Observer {
	
	private static DefaultTranslator instance = null;
	
	private CustomProperties realValue = new CustomProperties();
	
	/**
	 * <p>Getter for the field <code>instance</code>.</p>
	 *
	 * @return a {@link fstop.model.DefaultTranslator} object.
	 */
	public static DefaultTranslator getInstance() {
		if(instance == null) {
			synchronized (DefaultTranslator.class) {
				if(instance == null) {
					instance = new DefaultTranslator();
				}
			}
		}
		return instance;
	}
	
	private DefaultTranslator() {
	}
	
	/**
	 * <p>transfer.</p>
	 *
	 * @param scope a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 * @param value a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String transfer(String scope, String name, String value) {
		String all = (StrUtils.trim(name) + "." + StrUtils.trim(value)).toUpperCase();
		String scopeKey = (StrUtils.trim(scope) + "." + StrUtils.trim(name) + "." + StrUtils.trim(value)).toUpperCase();
		if(realValue.containsKey(scopeKey)) {
			return (String)realValue.get(scopeKey);
		}
		else if(realValue.containsKey(all)) {
			return (String)realValue.get(all);
		}
		else {
			return "";
		}
	}
	
	/** {@inheritDoc} */
	public void update(Observable o, Object arg) {
		//只對 arg 為 "TranslatorPath:properties file path" 處理
		
		if(arg instanceof String) {
			String path = (String)arg;
			if(path != null && path.toUpperCase().startsWith("TranslatorPath".toUpperCase())) {
				path = path.substring(path.indexOf(":") + 1);
				try {
					realValue.load(new InputStreamReader(new FileInputStream(new File(path)), "UTF8"));
				}
				catch(Exception e) {
					throw new ModelException("載入 FieldTranslator Properties 錯誤 !", e);
				}
			}
		}
	}

}
