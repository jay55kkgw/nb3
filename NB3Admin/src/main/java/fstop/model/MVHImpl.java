package fstop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fstop.util.DumpUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.*;

/**
 * 只要是 key 為 OCCURS_ 開頭的, 會被放到 occurs 裡去, 只有在 constructer 時才會動作
 *
 * @author jimmy
 * @version V1.0
 */
public class MVHImpl implements MVH, Serializable {

	@JsonIgnore
	private Logger logger = Logger.getLogger(MVHImpl.class);

	private Map flatValues = null;
	private Rows occurs = null;   //發生 occurs 的地方
	
	private HashMap<String, IMasterValuesHelper> tables_alias = new LinkedHashMap();
	private HashMap<String, IMasterValuesHelper> tables = new LinkedHashMap();

	private MVHFieldTranslatorProxy translator = new MVHFieldTranslatorProxy(this);
	
	/**
	 * <p>Constructor for MVHImpl.</p>
	 */
	public MVHImpl() {
		this(new HashMap());
	}	
	
	/**
	 * <p>Constructor for MVHImpl.</p>
	 *
	 * @param flatValues a {@link java.util.Map} object.
	 */
	public MVHImpl(Map<String, String> flatValues) {
		this(flatValues, new Rows());
	}
	
	/**
	 * <p>Constructor for MVHImpl.</p>
	 *
	 * @param occurs a {@link fstop.model.Rows} object.
	 */
	public MVHImpl(Rows occurs) {
		this(new HashMap(), occurs);
	}
	
	/**
	 * <p>Constructor for MVHImpl.</p>
	 *
	 * @param flatValues a {@link java.util.Map} object.
	 * @param occurs a {@link fstop.model.Rows} object.
	 */
	public MVHImpl(Map<String, String> flatValues, Rows occurs) {
		this.flatValues = flatValues;
		this.occurs = occurs;
		
		Set<String> colnames = new HashSet<String>();
		int max = 0;
		for(String key : flatValues.keySet()) {
			if(key.toUpperCase().startsWith("OCCURS_") && key.indexOf('_', 7) >= 0 ) {
				colnames.add(key.substring(7, key.indexOf('_', 7)));
				int index = new Integer(key.substring(key.indexOf('_', 7) + 1));
				if(index > max)
					max = index;
			}
		}
		
		for(int i=0; i < max; i++) {
			Map<String, String> r = new HashMap();
			for(String coln : colnames) {
				String key = "OCCURS_" + coln + "_" + (i+1);
				r.put(coln, flatValues.get(key));
				flatValues.remove(key);
			}
			occurs.addRow(new Row(r));
		}
	}
	
//	public void setFlatValues(Map result) {
//		this.flatValues = result;
//	}
	
	/**
	 * <p>Getter for the field <code>flatValues</code>.</p>
	 *
	 * @return a {@link java.util.Map} object.
	 */
	public Map<String, String> getFlatValues() {
		return this.flatValues;
	}
	
	/**
	 * <p>Getter for the field <code>occurs</code>.</p>
	 *
	 * @return a {@link fstop.model.Rows} object.
	 */
	public Rows getOccurs() {
		return this.occurs;
	}	
	
	/**
	 * <p>getOccursByUID.</p>
	 *
	 * @param uidColumnName a {@link java.lang.String} object.
	 * @param realUID a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 */
	public Rows getOccursByUID(String uidColumnName, String realUID) {

		Rows returnValues = new Rows();
		
		for (int i=0; i < occurs.getSize(); i++) {
			
			Row row = occurs.getRow(i);
			String telcommUID = row.getValue(uidColumnName);
									
			//若下行電文之UID欄位值 == 登入之UID值 => 加入回傳之結構中
			if (telcommUID.equals(realUID)) {				
				returnValues.addRow(row);
			}		
		}// end for i

		return returnValues;
	}	
	
	
	/**
	 * <p>Setter for the field <code>inputQueryData</code>.</p>
	 *
	 * @param inputQueryData a {@link java.util.Map} object.
	 */
	public void setInputQueryData(Map inputQueryData) {
		this.inputQueryData = inputQueryData;
	}

	/**
	 * <p>Getter for the field <code>inputQueryData</code>.</p>
	 *
	 * @return a {@link java.util.Map} object.
	 */
	public Map getInputQueryData() {
		return inputQueryData;
	}


	/** {@inheritDoc} */
	public String getValueByFieldName(String fieldName) {
		if("BOQUERYNEXT".equals(fieldName)) {
			Map h = new HashMap();
			h.putAll(getInputQueryData());
			return JSONUtils.map2json(h);
		}
		return StrUtils.trim((String)flatValues.get(fieldName));
	}

	/** {@inheritDoc} */
	public String getValueByFieldName(String fieldName, int index) {
		
		
		String result = "";
		if(occurs.isIncludeColumn(fieldName)) {
			try {
				result = StrUtils.trim(occurs.getRow(index -1).getValue(fieldName));
				return result;
			}
			catch(Exception e) {
				//在 rows 找不到
				e.printStackTrace();
			}
		}
		else if(occurs.getSize() >= index && occurs.getRow(index-1).getColumnNames().contains(fieldName)) {
			try {
				result = StrUtils.trim(occurs.getRow(index -1).getValue(fieldName));
				return result;
			}
			catch(Exception e) {
				//在 rows 找不到
				e.printStackTrace();
			}
		}
		else if(flatValues.containsKey(fieldName + "_1")) {
			result = (String)flatValues.get(fieldName + "_" + index);
		}
		else {
			result = (String)flatValues.get(fieldName);
		}
		return StrUtils.trim(result);
	}

	/** {@inheritDoc} */
	public int getValueOccurs(String fieldName) {
		if(occurs.isIncludeColumn(fieldName)) {
			return occurs.getSize();
		}
		else if(flatValues.containsKey(fieldName + "_1")) {
			int result = 2;
			while(flatValues.containsKey(fieldName + "_" + result)) {
				result++;
			}
			return result - 1;
		}
		else if(flatValues.containsKey(fieldName)) {
			return 1;
		}
		else {
			return 0;
		}
	}

	/** {@inheritDoc} */
	public boolean isOccurs(String fieldName) {
		return occurs.isIncludeColumn(fieldName) ||
			flatValues.containsKey(fieldName + "_1") ;
	}

	
	/**
	 * <p>addTable.</p>
	 *
	 * @param table a {@link fstop.model.IMasterValuesHelper} object.
	 */
	public void addTable(IMasterValuesHelper table) {
		tables_alias.put("table_" + (tables.size() + 1), table);
		tables.put("table_" + (tables.size() + 1), table);
	}

	/**
	 * 將目前這個 table 加成 tables.size() + 1 個 table, 並加上 alias name
	 *
	 * @param table a {@link fstop.model.IMasterValuesHelper} object.
	 * @param alias a {@link java.lang.String} object.
	 */
	public void addTable(IMasterValuesHelper table, String alias) {
		if(!tables.containsKey(alias)) {
			tables.put(alias, table);
			tables_alias.put("table_" + (tables_alias.size() + 1), table);
		}
		else {
			tables.put(alias, table);
		}
		
	}

	/** {@inheritDoc} */
	public IMasterValuesHelper getTableByIndex(int index) {
		return tables_alias.get("table_" + index);
	}

	/** {@inheritDoc} */
	public IMasterValuesHelper getTableByName(String name) {
		return tables.get(name);
	}

	/**
	 * <p>getTableCount.</p>
	 *
	 * @return a int.
	 */
	public int getTableCount() {
		return tables.size();
	}

	/**
	 * <p>getTableKeys.</p>
	 *
	 * @return a {@link java.util.Vector} object.
	 */
	public Vector getTableKeys() {
		return new Vector(tables.keySet());
	}

	
	/**
	 * <p>getOccurKeys.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<String> getOccurKeys() {
		ArrayList result = new ArrayList();
		result.addAll(occurs.getColumnNames());
		return result;
	}
	

	/**
	 * <p>dumpToString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String dumpToString() {
		StringBuffer result = new StringBuffer();
		result.append(DumpUtils.dumpToString(flatValues)).append("\r\n");
		result.append(DumpUtils.dumpToString(this.getOccurs())).append("\r\n");
		
		for(String tableName : (Vector<String>)this.getTableKeys()) {
			result.append("@@Table Result [ " + tableName+ " ]: ------------------\r\n");
			result.append(this.getTableByName(tableName).dumpToString()).append("\r\n");
		}		
		
		return result.toString();
	}
	
	/**
	 * <p>dump.</p>
	 */
	public void dump() {
		DumpUtils.dump(flatValues);
		DumpUtils.dump(this.getOccurs());
		
		for(String tableName : (Vector<String>)this.getTableKeys()) {
			//logger.debug("@@Table Result [ " + tableName+ " ]: ------------------");
			this.getTableByName(tableName).dump();
		}
	}

	private Map inputQueryData = new HashMap();
	/**
	 * <p>translate.</p>
	 *
	 * @return a {@link fstop.model.IMasterValuesHelper} object.
	 */
	public IMasterValuesHelper translate() {
		return translator;
	}

}
