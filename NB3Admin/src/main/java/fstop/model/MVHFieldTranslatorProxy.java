package fstop.model;

import java.util.List;
import java.util.Map;

/**
 * <p>MVHFieldTranslatorProxy class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class MVHFieldTranslatorProxy implements MVH {

	private MVH mvh;
	private DefaultTranslator translator = DefaultTranslator.getInstance();
	
	/**
	 * <p>Constructor for MVHFieldTranslatorProxy.</p>
	 *
	 * @param mvh a {@link fstop.model.MVH} object.
	 */
	public MVHFieldTranslatorProxy(MVH mvh) {
		this.mvh = mvh;
	}
	
	/**
	 * <p>dump.</p>
	 */
	public void dump() {
		this.mvh.dump();
	}

	/**
	 * <p>dumpToString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String dumpToString() {
		return this.mvh.dumpToString();
	}

	/** {@inheritDoc} */
	public IMasterValuesHelper getTableByIndex(int index) {
		return this.mvh.getTableByIndex(index);
	}

	/** {@inheritDoc} */
	public IMasterValuesHelper getTableByName(String name) {
		return mvh.getTableByName(name);
	}

	/**
	 * <p>getTableCount.</p>
	 *
	 * @return a int.
	 */
	public int getTableCount() {
		return mvh.getTableCount();
	}

	/**
	 * <p>getTableKeys.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List getTableKeys() {
		return mvh.getTableKeys();
	}

	/** {@inheritDoc} */
	public String getValueByFieldName(String fieldName) {
			return translator.transfer(mvh.getValueByFieldName("TXID"), fieldName, mvh.getValueByFieldName(fieldName));
	}

	/** {@inheritDoc} */
	public String getValueByFieldName(String fieldName, int index) {
		return translator.transfer(mvh.getValueByFieldName("TXID"), fieldName, mvh.getValueByFieldName(fieldName, index));
	}

	/** {@inheritDoc} */
	public int getValueOccurs(String fieldName) {
		return mvh.getValueOccurs(fieldName);
	}

	/** {@inheritDoc} */
	public boolean isOccurs(String fieldName) {
		return mvh.isOccurs(fieldName);
	}

	/**
	 * <p>translate.</p>
	 *
	 * @return a {@link fstop.model.IMasterValuesHelper} object.
	 */
	public IMasterValuesHelper translate() {
		return this;
	}

	/**
	 * <p>getInputQueryData.</p>
	 *
	 * @return a {@link java.util.Map} object.
	 */
	public Map getInputQueryData() {
		return mvh.getInputQueryData();
	}
}
