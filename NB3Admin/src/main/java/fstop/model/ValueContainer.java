package fstop.model;

/**
 * <p>ValueContainer class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class ValueContainer<T> {
	public T value;

	/**
	 * <p>Getter for the field <code>value</code>.</p>
	 *
	 * @return a T object.
	 */
	public T getValue() {
		return value;
	}

	/**
	 * <p>Setter for the field <code>value</code>.</p>
	 *
	 * @param value a T object.
	 */
	public void setValue(T value) {
		this.value = value;
	}
	
	
}
