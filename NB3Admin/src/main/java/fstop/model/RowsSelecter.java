package fstop.model;

import fstop.util.StrUtils;
//import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>RowsSelecter class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class RowsSelecter {
//	private Logger logger = Logger.getLogger(getClass());
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private Rows rows = null;
	private List<Integer> selected = null;
	private RowFilterCallback filter = null;

	/**
	 * <p>Constructor for RowsSelecter.</p>
	 *
	 * @param rows a {@link fstop.model.Rows} object.
	 */
	public RowsSelecter(Rows rows) {
		this(rows, null);
	}

	/**
	 * <p>Constructor for RowsSelecter.</p>
	 *
	 * @param rows a {@link fstop.model.Rows} object.
	 * @param filter a {@link fstop.model.RowFilterCallback} object.
	 */
	public RowsSelecter(Rows rows, RowFilterCallback filter) {
		this.rows = rows;
		setFilter(filter);
	}

	/**
	 * <p>Setter for the field <code>filter</code>.</p>
	 *
	 * @param filter a {@link fstop.model.RowFilterCallback} object.
	 */
	public void setFilter(RowFilterCallback filter) {
		this.selected = new ArrayList();
		for(int i=0; i < rows.getSize(); i++) {
			if(filter != null ) {
				if(filter.accept(rows.getRow(i)))
					selected.add(i);
			}
			else {
				selected.add(i);
			}
		}
	}

//	public double sum(String column) {
//		double result = 0;
//		String v = "";
//		String v2 = "";
//		try {
//			for(Integer i : selected) {
//				logger.debug("RowsSelecter.java [column] =>"+column);
//				v = StrUtils.trim(rows.getRow(i).getValue(column));
//				logger.debug("RowsSelecter.java [v1] =>"+v);
//				if((v.length() > 0) && ((v.endsWith("-") || v.endsWith(" ") || v.endsWith("+")))) {
//					v2 = v.substring(0, v.length() -1);
//					logger.debug("RowsSelecter.java [v2] =>"+v2);
//					rows.getRow(i).setValue(column, v.substring(v.length() -1) + v2);
//					logger.debug("RowsSelecter.java rows.getRow(i).getValue(column) =>"+rows.getRow(i).getValue(column));
//				}
//				try {
////					result += new Double(rows.getRow(i).getValue(column));
//					logger.debug("doubleValue =>"+new BigDecimal(new BigInteger(rows.getRow(i).getValue(column)),2).doubleValue());
//					
//					result += new BigDecimal(new BigInteger(rows.getRow(i).getValue(column)),2).doubleValue();
//					logger.debug("RowsSelecter.java [result] =>"+result);
//				}
//				catch(Exception e) {
//					logger.error("無法對 column '" + column + "', value: '" + v + "' 做加總計算.", e);
//				}
//			}
//		}
//		catch(Exception e) {
//			throw new ModelException("無法對 column [" + column + "] 做加總計算.");
//		}
//		return result;
//	}
	
	
//	舊code
	
	/**
	 * <p>sum.</p>
	 *
	 * @param column a {@link java.lang.String} object.
	 * @return a double.
	 */
	public double sum(String column) {
		double result = 0;
		try {
			logger.debug("RowsSelecter sum begin {} =>"+new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
			for(Integer i : selected) {
//				logger.debug("RowsSelecter.java [column] =>"+column);
				String v = StrUtils.trim(rows.getRow(i).getValue(column));
//				logger.debug("RowsSelecter.java [v1] =>"+v);
				if((v.length() > 0) && ((v.endsWith("-") || v.endsWith(" ") || v.endsWith("+")))) {
					v = v.substring(0, v.length() -1);
//					logger.debug("RowsSelecter.java [v2] =>"+v);
					rows.getRow(i).setValue(column, v.substring(v.length() -1) + v);
//					logger.debug("RowsSelecter.java rows.getRow(i).getValue(column) =>"+rows.getRow(i).getValue(column));
				}
				try {
					result += new Double(v);
//					logger.debug("RowsSelecter.java [result] =>"+result);
				}
				catch(Exception e) {
					logger.error("無法對 column '" + column + "', value: '" + v + "' 做加總計算.", e);
				}
			}
		}
		catch(Exception e) {
			throw new ModelException("無法對 column [" + column + "] 做加總計算.");
		}
		logger.debug("RowsSelecter sum end {} =>"+new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
		logger.debug("RowsSelecter.java [result] =>"+result);
		return result;
	}

	/**
	 * <p>sumtoColumn.</p>
	 *
	 * @param column a {@link java.lang.String} object.
	 * @return a int.
	 */
	public int sumtoColumn(String column) {
		int result = 0;
		try {
			for(Integer i : selected) {
				logger.debug("RowsSelecter.java [column] =>"+column);
				String v = StrUtils.trim(rows.getRow(i).getValue(column));
				logger.debug("RowsSelecter.java [v1] =>"+v);
				try {
					result += new Integer(v.length()-(v.length()-1));
					logger.debug("RowsSelecter.java [result] =>"+result);
				}
				catch(Exception e) {
					logger.error("無法對 column '" + column + "', value: '" + v + "' 做加總計算.", e);
				}
			}
		}
		catch(Exception e) {
			throw new ModelException("無法對 column [" + column + "] 做加總計算.");
		}
		return result;
	}

	/**
	 * <p>avg.</p>
	 *
	 * @param column a {@link java.lang.String} object.
	 * @return a double.
	 */
	public double avg(String column) {
		double result = 0;
		double resultTotal = 0;
		try {
			double c = 0;
			for(Integer i : selected) {
				c++;

				String v = StrUtils.trim(rows.getRow(i).getValue(column));
				//logger.debug(column + ":" + v);
				//將正負號移到前面
				if((v.length() > 0) && ((v.endsWith("-") || v.endsWith(" ") || v.endsWith("+")))) {
					v = v.substring(0, v.length() -1);
					rows.getRow(i).setValue(column, v.substring(v.length() -1) + v);
				}

				try {
					resultTotal += new Double(v);
				}
				catch(Exception e) {
					logger.error("無法對 column '" + column + "', value: '" + v + "' 做平圴計算.", e);
				}
			}
			if(c == 0)
				result = 0d;
			else {
				result = resultTotal / c;
				//logger.debug("resultTotal: " + resultTotal + ", c: " + c + " = " + result);
			}
		}
		catch(Exception e) {
			throw new ModelException("無法對 column [" + column + "] 做平圴計算.");
		}
		return result;
	}

	/**
	 * <p>each.</p>
	 *
	 * @param eachRow a {@link fstop.model.EachRowCallback} object.
	 */
	public void each(EachRowCallback eachRow) {
		for(Row r : getSelectedRows()) {
			eachRow.current(r);
		}
	}

	/**
	 * <p>getSelectedRows.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Row> getSelectedRows() {
		List<Row> result = new ArrayList<Row>();
		for(Integer i : selected) {
			result.add(rows.getRow(i));
		}
		return result;
	}
}
