package fstop.model;

/**
 * <p>EachRowCallback interface.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public interface EachRowCallback {
	/**
	 * <p>current.</p>
	 *
	 * @param row a {@link fstop.model.Row} object.
	 */
	public void current(Row row);
}
