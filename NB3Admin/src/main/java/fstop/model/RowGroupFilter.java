package fstop.model;

/**
 * <p>RowGroupFilter interface.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public interface RowGroupFilter {
	/**
	 * <p>groupKey.</p>
	 *
	 * @param row a {@link fstop.model.Row} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String groupKey(Row row);
}
