package fstop.model;

import fstop.exception.UncheckedException;

/**
 * <p>ModelException class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class ModelException extends UncheckedException { 

	private static final long serialVersionUID = 7034496227113613816L;
	
	/**
	 * <p>Constructor for ModelException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 */
	public ModelException(String msg) {
		super(msg);
	}
	/**
	 * <p>Constructor for ModelException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public ModelException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
