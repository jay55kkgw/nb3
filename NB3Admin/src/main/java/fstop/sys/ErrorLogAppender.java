package fstop.sys;

import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.springframework.scheduling.annotation.Async;

import fstop.aop.SpringUtil;
import fstop.orm.dao.SysErrLogDao;
import fstop.orm.po.SYSERRLOG;
import lombok.extern.slf4j.Slf4j;

/**
 * <p> 自製 Log4j Appender </p>
 * @author 簡哥
 */
@Slf4j
@Plugin(name = "ErrorLogAppender", category = Core.CATEGORY_NAME, elementType = Appender.ELEMENT_TYPE)
public class ErrorLogAppender extends AbstractAppender { 
    private SysErrLogDao sysErrLogDao;
    
    protected ErrorLogAppender(String name, Filter filter) {
        super(name, filter, null);
    }
 
    @PluginFactory
    public static ErrorLogAppender createAppender(@PluginAttribute("name") String name, @PluginElement("Filter") Filter filter) {
        return new ErrorLogAppender(name, filter);
    }
 
    /**
     * 增加一筆 log
     */
    @Override
    @Async
    public void append(LogEvent event) {
        //eventMap.put(Instant.now().toString(), event);
        if ( event.getLevel() == Level.ERROR ) {
            try {
                if ( sysErrLogDao == null ) {
                    synchronized(this) {
                        if ( sysErrLogDao == null ) {
                            sysErrLogDao = SpringUtil.getBean(SysErrLogDao.class);
                        }
                    }
                }
                SYSERRLOG po = new SYSERRLOG();
                po.setMESSAGE(event.getMessage().toString());
                po.setSOURCE(event.getSource().toString());
                po.setTHROWBY(event.getThrown().toString());
                po.setLOGGERNAME(event.getLoggerName());
                po.setLOGGERHOST(InetAddress.getLocalHost().getHostName());

                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
                Date date = new Date();
                String[] parts = dateFormat.format(date).split("-");
                
                po.setLOGDATE(parts[0]);
                po.setLOGTIME(parts[1]);

                sysErrLogDao.save(po);
            } catch (Exception e) {
                log.error("",e);
            }
        }

    }
}