package fstop.services;

import java.util.List;
import java.util.Map;

/**
 * <p>BOService interface.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public interface BOService {
	/**
	 * <p>execute.</p>
	 *
	 * @param params a {@link java.util.Map} object.
	 * @return a {@link java.util.List} object.
	 */
	List execute(Map params);
}
