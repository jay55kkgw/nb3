package fstop.services;

import fstop.model.MVH;

import java.util.Map;


/**
 * <p>ParamDecorator interface.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public interface ParamDecorator {
	/**
	 * <p>before.</p>
	 *
	 * @param m a {@link java.util.Map} object.
	 */
	public void before(Map m);
	/**
	 * <p>after.</p>
	 *
	 * @param orgparams a {@link java.util.Map} object.
	 * @param params a {@link java.util.Map} object.
	 * @param imaster a {@link fstop.model.MVH} object.
	 */
	public void after(Map orgparams, Map params, MVH imaster);
}
