package fstop.services;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

/**
 * FOR NA30 & NA40 線上申請網路銀行使用
 * 不檢查 session 裡頭是否有 UID
 *
 * @author Albert
 * @version V1.0
 */
@Component
public class SpecialParamDecorator implements ParamDecorator {


	/** {@inheritDoc} */
	public void before(Map _params) {
		Map<String, String> params = _params;

		String trancode = params.get("trancode");
		if(!StrUtils.trim(trancode).startsWith("B")) {  //不是後台的 BO
			params.put("CUSIDN", StrUtils.trim((String) params.get("UID")));
		}
		else {   //後台的 BO			
			if(!trancode.equals("B107_Q1_SHOW"))
				params.put("LASTUSER", params.get("ADMUID"));
			else
				params.put("LASTUSER", "SYS");
		}

		params.put("__TXID", StrUtils.trim(params.get("TXID")));
		params.remove("HOSTMSGID");
	}

	/** {@inheritDoc} */
	public void after(Map _org_params, Map _params, MVH imaster) {
		Map<String, String> params = _params;
		MVHImpl mvh = (MVHImpl) imaster;
		mvh.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		mvh.getFlatValues().put("TXID", StrUtils.trim(params.get("__TXID")));
		params.remove("__TXID");
		params.remove("BOQUERYNEXT");
		params.remove("QUERYNEXT");

		_org_params.remove("BOQUERYNEXT");
		_org_params.remove("QUERYNEXT");
		_org_params.remove("__BOQUERYNEXT");

		mvh.setInputQueryData((Hashtable) _org_params);
	}
}
