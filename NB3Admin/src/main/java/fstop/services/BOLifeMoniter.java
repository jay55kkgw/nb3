package fstop.services;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>BOLifeMoniter class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class BOLifeMoniter {
	static ThreadLocal threadValues = new ThreadLocal();
	
	
	/**
	 * <p>clearValues.</p>
	 */
	public static void clearValues() {
		if(threadValues.get() == null)
			threadValues.set(new HashMap());
		else 
			((Map)threadValues.get()).clear();
	}
	
	/**
	 * <p>setValue.</p>
	 *
	 * @param key a {@link java.lang.String} object.
	 * @param obj a {@link java.lang.Object} object.
	 */
	public static void setValue(String key, Object obj) {
		if(threadValues.get() == null)
			clearValues();
		((Map)threadValues.get()).put(key, obj);
	}
	
	/**
	 * <p>getValue.</p>
	 *
	 * @param key a {@link java.lang.String} object.
	 * @return a {@link java.lang.Object} object.
	 */
	public static Object getValue(String key) {
		if(threadValues.get() == null)
			clearValues();
		
		return ((Map)threadValues.get()).get(key);
	}
	
}
