package fstop.services;

import fstop.exception.UncheckedException;

/**
 * <p>ServiceExecuteException class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class ServiceExecuteException extends UncheckedException { 

	private static final long serialVersionUID = 1L;
	/**
	 * <p>Constructor for ServiceExecuteException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 */
	public ServiceExecuteException(String msg) {
		super(msg);
	}
	/**
	 * <p>Constructor for ServiceExecuteException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public ServiceExecuteException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
