package fstop.services;

import fstop.model.TelcommResult;
import fstop.orm.po.ADMMSGCODE;

/**
 * <p>TopMsgDetecter interface.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public interface TopMsgDetecter {

	/**
	 * <p>isError.</p>
	 *
	 * @param topmsg a {@link java.lang.String} object.
	 * @param telcommResult a {@link fstop.model.TelcommResult} object.
	 * @return a {@link fstop.orm.po.ADMMSGCODE} object.
	 */
	public ADMMSGCODE isError(String topmsg, TelcommResult telcommResult);

}

