package fstop.services;

import fstop.exception.UncheckedException;

/**
 * <p>ServiceBindingException class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class ServiceBindingException extends UncheckedException { 
	/**
	 * <p>Constructor for ServiceBindingException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 */
	public ServiceBindingException(String msg) {
		super(msg);
	}
	/**
	 * <p>Constructor for ServiceBindingException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public ServiceBindingException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
