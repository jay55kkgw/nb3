package fstop.services;

import fstop.util.SpringBeanFactory;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.apache.log4j.Logger;

/**
 * <p>CacheService class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class CacheService {
	private Logger logger = Logger.getLogger(getClass());
	private Cache cache = null;
	
	/**
	 * 取得／設定 Cache
	 *
	 * TODO 加上 Exception 的 handle
	 *
	 * @param cacheId a {@link java.lang.String} object.
	 */
	public CacheService(String cacheId)
	{
		cache = (Cache) SpringBeanFactory.getBean(cacheId);
	}
	
	/**
	 * <p>Setter for the field <code>cache</code>.</p>
	 *
	 * @param CacheKey a {@link java.lang.String} object.
	 * @param CacheValue a {@link java.lang.Object} object.
	 */
	public void setCache(String CacheKey, Object CacheValue)
	{
		Element elm = new Element(CacheKey, CacheValue);
		cache.put(elm);
	}
	
	/**
	 * <p>Getter for the field <code>cache</code>.</p>
	 *
	 * @param CacheKey a {@link java.lang.String} object.
	 * @return a {@link java.lang.Object} object.
	 */
	@SuppressWarnings("unchecked")
	public Object getCache(String CacheKey)
	{
		Object ResultObj = null;
		//String cacheValue = null;
		//Hashtable<String, Object> cacheValue = new Hashtable<String, Object>();
		
		Element elm = cache.get(CacheKey);
		//if(elm != null)cacheValue.put(CacheKey, elm.getValue());
		if(elm != null)ResultObj = elm.getValue();
		
		return ResultObj;
	}
}
