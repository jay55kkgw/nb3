package fstop.services;

import fstop.model.MVHImpl;

/**
 * <p>AfterSuccessQuery interface.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public interface AfterSuccessQuery {
	/**
	 * <p>execute.</p>
	 *
	 * @param result a {@link fstop.model.MVHImpl} object.
	 */
	public void execute(MVHImpl result);
}
