package fstop.services;

import java.util.Map;

import fstop.model.MVHImpl;

/**
 * <p>TransferListener interface.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public interface TransferListener {
	/**
	 * <p>before.</p>
	 *
	 * @param params a {@link java.util.Map} object.
	 */
	public void before(Map params);
	
	/**
	 * <p>doSuccess.</p>
	 */
	public void doSuccess();
	
	/**
	 * <p>doFail.</p>
	 */
	public void doFail();
	
	/**
	 * <p>doFinally.</p>
	 *
	 * @param queryresult a {@link fstop.model.MVHImpl} object.
	 */
	public void doFinally(MVHImpl queryresult);

}
