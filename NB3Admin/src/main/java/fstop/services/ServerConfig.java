package fstop.services;

import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.context.ServletContextAware;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>ServerConfig class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
public class ServerConfig implements ServletContextAware{     
	private Logger logger = Logger.getLogger(getClass());
	private String serverRootUrl;     
	private String contextPath;
	private ServletContext servletContext;
	
	/**
	 * <p>Getter for the field <code>serverRootUrl</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getServerRootUrl()
	{ 
		return serverRootUrl; 
	}
	
	/**
	 * <p>Getter for the field <code>contextPath</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getContextPath()
	{ 
		return contextPath; 
	}
	
	/**
	 * <p>getgetResourceAsStream.</p>
	 *
	 * @param FileName a {@link java.lang.String} object.
	 * @return a {@link java.io.InputStream} object.
	 */
	public InputStream getgetResourceAsStream(String FileName)
	{
		  return servletContext.getResourceAsStream(FileName);
	}

	/** {@inheritDoc} */
	public void setServletContext(ServletContext servletContext)
	{         
		this.serverRootUrl=servletContext.getRealPath("");
		
		log.info("serverRootUrl = " + serverRootUrl);
		
		this.contextPath = "/" + servletContext.getServletContextName();
		
		log.info("ServletContextName = " + servletContext.getServletContextName());
		log.info("ServerInfo = " + servletContext.getServerInfo());
		
		this.servletContext=servletContext;
	} 
} 
