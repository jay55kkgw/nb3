package fstop.services;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import fstop.model.MVHImpl;


/**
 * <p>TransferListenerWrapper class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class TransferListenerWrapper implements Observer, TransferListener {
	private TransferListener listener;
	
	/**
	 * <p>Getter for the field <code>listener</code>.</p>
	 *
	 * @return a {@link fstop.services.TransferListener} object.
	 */
	public TransferListener getListener() {
		return listener;
	}

	/**
	 * <p>Constructor for TransferListenerWrapper.</p>
	 *
	 * @param listener a {@link fstop.services.TransferListener} object.
	 */
	public TransferListenerWrapper(TransferListener listener) {
		this.listener = listener;
	}
	
	/** {@inheritDoc} */
	public void before(Map params) {
		listener.before(params);
	}
	
	/**
	 * <p>doSuccess.</p>
	 */
	public void doSuccess(){
		listener.doSuccess();
	}
	
	/**
	 * <p>doFail.</p>
	 */
	public void doFail(){
		listener.doFail();
	}
	
	/** {@inheritDoc} */
	public void doFinally(MVHImpl result){
		listener.doFinally(result);
	}
	
	/** {@inheritDoc} */
	public void update(Observable o, Object arg) {
		Object[] args = (Object[])arg;
		if("before".equalsIgnoreCase((String)args[0]))
			before((Map)args[1]);
		if("doFinally".equalsIgnoreCase((String)args[0]))
			doFinally((MVHImpl)args[1]);
		if("doFail".equalsIgnoreCase((String)args[0]))
			doFail();
		if("doSuccess".equalsIgnoreCase((String)args[0]))
			doSuccess();
	}
	
	/** {@inheritDoc} */
	public boolean equals(Object o) {
		boolean result = false;
		if(this == o)
			return true;
		if(listener != null) {
			if(o instanceof TransferListenerWrapper) {
				if(listener == ((TransferListenerWrapper)o).getListener())
					return true;
			}
		}
		return false;
	}
	
	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode() {
		if(listener != null)
			return listener.hashCode();
		else {
			return 0;
		}
	}
}
