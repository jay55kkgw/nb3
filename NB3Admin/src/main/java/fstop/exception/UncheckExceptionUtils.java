package fstop.exception;

/**
 * <p>Abstract UncheckExceptionUtils class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public abstract class UncheckExceptionUtils {

	/**
	 * Build a message for the given base message and root cause.
	 *
	 * @param message the base message
	 * @param cause the root cause
	 * @return the full exception message
	 */
	public static String buildMessage(String message, Throwable cause) {
		if (cause != null) {
			StringBuffer buf = new StringBuffer();
			if (message != null) {
				buf.append(message).append("; ");
			}
			buf.append("nested exception is ").append(cause);
			return buf.toString();
		}
		else {
			return message;
		}
	}

}
