package fstop.exception;


/**
 * <p>MsgTemplateNotFoundException class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class MsgTemplateNotFoundException extends ToRuntimeException { 
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * <p>Constructor for MsgTemplateNotFoundException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 */
	public MsgTemplateNotFoundException(String msg) {
		super(msg);
	}
	
	/**
	 * <p>Constructor for MsgTemplateNotFoundException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public MsgTemplateNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
