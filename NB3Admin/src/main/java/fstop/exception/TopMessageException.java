package fstop.exception;

import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.SpringBeanFactory;


/**
 * 電文發送後, TOPMSG 裡為錯誤代碼..
 *
 * @author Owner
 * @version V1.0
 */
public class TopMessageException extends UncheckedException {

	private static final long serialVersionUID = 8088350703708513225L;

	private String appCode;
	
	private String msgcode;
	
	private String msgin;
	
	private String msgout;

	/**
	 * <p>Getter for the field <code>msgcode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMsgcode() {
		return msgcode;
	}

	/**
	 * <p>Getter for the field <code>msgin</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMsgin() {
		return msgin;
	}

	/**
	 * <p>Getter for the field <code>msgout</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMsgout() {
		return msgout;
	}

	/**
	 * <p>Constructor for TopMessageException.</p>
	 *
	 * @param msgcode a {@link java.lang.String} object.
	 * @param msgin a {@link java.lang.String} object.
	 * @param msgout a {@link java.lang.String} object.
	 */
	public TopMessageException(String msgcode, String msgin, String msgout) {
		super(msgcode);
		this.msgcode = msgcode;
		this.msgin = msgin;
		this.msgout = msgout;
		
	}
	
	/**
	 * <p>Constructor for TopMessageException.</p>
	 *
	 * @param msgcode a {@link java.lang.String} object.
	 * @param msgin a {@link java.lang.String} object.
	 * @param msgout a {@link java.lang.String} object.
	 * @param adopid a {@link java.lang.String} object.
	 */
	public TopMessageException(String msgcode, String msgin, String msgout, String adopid) {
		super(msgcode);
		this.msgcode = msgcode;
		this.msgin = msgin;
		this.msgout = msgout;
		this.appCode = adopid;		
	}

	/**
	 * <p>Constructor for TopMessageException.</p>
	 *
	 * @param msgcode a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public TopMessageException(String msgcode, Throwable cause) {
		super(msgcode, cause);
	}

	/**
	 * <p>Getter for the field <code>appCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAppCode() {
		return appCode;
	}

	/**
	 * <p>Setter for the field <code>appCode</code>.</p>
	 *
	 * @param appCode a {@link java.lang.String} object.
	 */
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	
	
	/**
	 * <p>create.</p>
	 *
	 * @param msgcode a {@link java.lang.String} object.
	 * @return a {@link fstop.exception.TopMessageException} object.
	 */
	public static TopMessageException create(String msgcode) {
		return TopMessageException.create(msgcode, null);
	}
	
	/**
	 * <p>create.</p>
	 *
	 * @param msgcode a {@link java.lang.String} object.
	 * @param adopid a {@link java.lang.String} object.
	 * @return a {@link fstop.exception.TopMessageException} object.
	 */
	public static TopMessageException create(String msgcode, String adopid) {
		AdmMsgCodeDao admMsgCodeDao = (AdmMsgCodeDao)SpringBeanFactory.getBean("admMsgCodeDao");
		ADMMSGCODE result = admMsgCodeDao.isError(msgcode);
		if(result != null) {
			return new TopMessageException(msgcode, result.getADMSGIN(), result.getADMSGOUT(), adopid);
		}
		else {
			return new TopMessageException(msgcode, "", "", adopid);
		}
	}
	
	/**
	 * <p>isInDB.</p>
	 *
	 * @param msgcode a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	public static boolean isInDB(String msgcode) {
		AdmMsgCodeDao admMsgCodeDao = (AdmMsgCodeDao)SpringBeanFactory.getBean("admMsgCodeDao");
		ADMMSGCODE result = admMsgCodeDao.isError(msgcode);
		if(result != null) {
			return true;
		}
		else {
			return false;
		}
	}
		
}
