package fstop.exception;


/**
 * <p>RecordExistsException class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class RecordExistsException extends DBAccessException { 

	private static final long serialVersionUID = 7034496227113613816L;
	
	/**
	 * <p>Constructor for RecordExistsException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 */
	public RecordExistsException(String msg) {
		super(msg);
	}
	/**
	 * <p>Constructor for RecordExistsException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public RecordExistsException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
