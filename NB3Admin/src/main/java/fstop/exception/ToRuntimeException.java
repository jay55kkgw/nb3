package fstop.exception;

/**
 * <p>ToRuntimeException class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class ToRuntimeException extends UncheckedException {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * <p>Constructor for ToRuntimeException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 */
	public ToRuntimeException(String msg) {
		super(msg);
	}
	
	/**
	 * <p>Constructor for ToRuntimeException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public ToRuntimeException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
