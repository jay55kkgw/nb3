package fstop.exception;


/**
 * <p>SendMailException class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class SendMailException extends UncheckedException { 
	/**
	 * <p>Constructor for SendMailException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 */
	public SendMailException(String msg) {
		super(msg);
	}
	/**
	 * <p>Constructor for SendMailException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public SendMailException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
