package fstop.exception;

/**
 * <p>LoginException class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class LoginException extends UncheckedException { 
	/**
	 * <p>Constructor for LoginException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 */
	public LoginException(String msg) {
		super(msg);
	}
	/**
	 * <p>Constructor for LoginException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public LoginException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
