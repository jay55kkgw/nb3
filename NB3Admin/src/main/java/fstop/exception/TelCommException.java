package fstop.exception;


/**
 * <p>TelCommException class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class TelCommException extends UncheckedException { 
	/**
	 * <p>Constructor for TelCommException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 */
	public TelCommException(String msg) {
		super(msg);
	}
	/**
	 * <p>Constructor for TelCommException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public TelCommException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
