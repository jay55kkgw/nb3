package fstop.exception;


/**
 * <p>DBAccessException class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class DBAccessException extends UncheckedException { 

	private static final long serialVersionUID = 7034496227113613816L;
	
	/**
	 * <p>Constructor for DBAccessException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 */
	public DBAccessException(String msg) {
		super(msg);
	}
	/**
	 * <p>Constructor for DBAccessException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public DBAccessException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
