package fstop.exception;


/**
 * <p>RecordNotExistException class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class RecordNotExistException extends DBAccessException { 

	private static final long serialVersionUID = 7034496227113613816L;
	
	/**
	 * <p>Constructor for RecordNotExistException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 */
	public RecordNotExistException(String msg) {
		super(msg);
	}
	/**
	 * <p>Constructor for RecordNotExistException.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public RecordNotExistException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
