package fstop.util;

import fstop.model.AdmoplogTranslator;
import fstop.model.B201Translator;
import fstop.model.DefaultTranslator;
//import fstop.notifier.NotifyAngent;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletContext;

/**
 * <p>EnvPathRegister class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class EnvPathRegister extends java.util.Observable {
	
	private static EnvPathRegister instance = null;
	
	/** Constant <code>HOST_IP=""</code> */
	public static String HOST_IP = "";

	/**
	 * <p>Getter for the field <code>instance</code>.</p>
	 *
	 * @return a {@link fstop.util.EnvPathRegister} object.
	 */
	public static EnvPathRegister getInstance() {
		if(instance == null) {
			synchronized (EnvPathRegister.class) {
				if(instance == null) {
					instance = new EnvPathRegister();
				}
			} 
		}
		return instance;
	}
	
	static {
		DefaultTranslator t = DefaultTranslator.getInstance();
		AdmoplogTranslator a = AdmoplogTranslator.getInstance();
		B201Translator     b = B201Translator.getInstance();
//		SpringBeanFactory s = SpringBeanFactory.getInstance();
		//NotifyAngent m = NotifyAngent.getInstance();
		EnvPathRegister.getInstance().addObserver(t);
		EnvPathRegister.getInstance().addObserver(a);
		EnvPathRegister.getInstance().addObserver(b);
//		EnvPathRegister.getInstance().addObserver(s);
		//EnvPathRegister.getInstance().addObserver(m);
	}
	
	private EnvPathRegister(){
	}
	
	
	/**
	 * <p>registerTranslatorPath.</p>
	 *
	 * @param path a {@link java.lang.String} object.
	 */
	public static void registerTranslatorPath(String path) {
		getInstance().setChanged();
		getInstance().notifyObservers("TranslatorPath:" + path);
	}
	
	/**
	 * <p>registerServletContext.</p>
	 *
	 * @param context a {@link javax.servlet.ServletContext} object.
	 */
	public static void registerServletContext(ServletContext context) {
		getInstance().setChanged();
		getInstance().notifyObservers(context);
	}
	
	/**
	 * <p>registerApplicationContext.</p>
	 *
	 * @param context a {@link org.springframework.context.ApplicationContext} object.
	 */
	public static void registerApplicationContext(ApplicationContext context) {
		getInstance().setChanged();
		getInstance().notifyObservers(context);
	}
	
	/**
	 * <p>destoryContext.</p>
	 */
	public static void destoryContext() {
		getInstance().setChanged();
		getInstance().notifyObservers("DESTORY_CONTEXT");
	}
	/**
	 * <p>registerAdmoplogTranslatorPath.</p>
	 *
	 * @param path a {@link java.lang.String} object.
	 */
	public static void registerAdmoplogTranslatorPath(String path) {
		getInstance().setChanged();
		getInstance().notifyObservers("admOpLogTranslatorPath:" + path);
	}
	/**
	 * <p>registerB201TranslatorPath.</p>
	 *
	 * @param path a {@link java.lang.String} object.
	 */
	public static void registerB201TranslatorPath(String path) {
		getInstance().setChanged();
		getInstance().notifyObservers("B201TranslatorPath:" + path);
	}
}
