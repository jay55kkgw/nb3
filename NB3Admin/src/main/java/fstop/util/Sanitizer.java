package fstop.util;

import java.lang.reflect.Field;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.web.util.HtmlUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 對輸入的 Obejct 做 SQL Injection 的消毒
 * </p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
public class Sanitizer {
    /**
     * 對要進入/出 DB 的資料做消毒的動作
     * @param input 傳入的資料，可能是 String，Integer，Double...
     * @return
     * 20191202-Danny-Replace後要回傳新字串
     */
    public static Object sanitize(Object input) {
        Object output;
       if (input instanceof String) {
            String tmp = (String)input;
            tmp = tmp.replace("'","")
            .replace("|", " ")
            .replace("--", " ")
            .replace("\n", " ")
            .replace("\t", " ");
            output=tmp;
        } else {
            output = input;
        }
        return output;
    }

    /**
     * 對要進入/出 DB 的資料做消毒的動作
     * @param <T>   泛型類別
     * @param input 泛型物件
     */
    public static <T> void sanitize4Class(T input) {
        Field[] fields = input.getClass().getDeclaredFields();
        for ( int i=0; i<fields.length; i++ ) {
            Field field = fields[i];
            field.setAccessible(true);
            
            try {
                Object value = field.get(input);
                if ( value instanceof String ) {
                    String tmp = (String)value;
                    //20191202-Danny-Replace後要回傳新字串
                    tmp = tmp.replace("'","")
                    .replace("|", " ")
                    .replace("--", " ")
                    .replace("\n", " ")
                    .replace("\t", " ");
                    field.set(input, tmp);
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                log.warn("get field value error", e);
            }
        }
    }

    /**
     * 對資料做消毒的動作
     * @param inputs    傳入的資料陣列，可能是 String，Integer，Double...
     * @return
     */
    public static Object[] arraySanitize(Object[] inputs) {
        Object[] outputs = new Object[inputs.length];
        for ( int i=0; i<inputs.length; i++ ) {
            if (inputs[i] instanceof String) {
                String tmp = (String)inputs[i];
                //20191202-Danny-Replace後要回傳新字串
                tmp = tmp.replace("'","")
                .replace("|", " ")
                .replace("--", " ")
                .replace("\n", " ")
                .replace("\t", " ");
                outputs[i]=tmp;
            } else {
                outputs[i] = inputs[i];
            }
        }
        
        return outputs;
    }

    /**
     * 對資料做 Encode 的動作
     * @param input 傳入的資料，可能是 String，Integer，Double...
     * @return
     */
    public static Object escapeHTML(Object input) {
        Object output;
        if (input instanceof String) {
            String tmp = (String)input;
            
            output=HtmlUtils.htmlEscape(tmp);
        } else {
            output = input;
        }
        return output;
    }

    /**
     * 對 URL 網址做 EncodeURL 的動作
     * @param input     傳入的 URL
     * @return          Encode 後的 URL
     */
    public static String encodeURL(String input) {
        try {
            return ESAPI.encoder().encodeForURL(input);
        } catch (Exception e) {
            log.error("encodeURL error", e);
            return input;
        }
    }

    /**
     * 使用泛型將含字串的欄位做 html escape
     * @param <T>   泛型類別
     * @param input 泛型物件
     */
    public static <T> void escape4Class(T input, T output) {

        Field[] fieldsi = input.getClass().getDeclaredFields();
        Field[] fieldso = output.getClass().getDeclaredFields();

        for ( int i=0; i<fieldsi.length; i++ ) {
            Field fieldi = fieldsi[i];
            Field fieldo = fieldso[i];
            fieldi.setAccessible(true);
            fieldo.setAccessible(true);
            
            try {
                Object value = fieldi.get(input);
                if ( value instanceof String ) {
                    fieldo.set(output, Sanitizer.escapeHTML((String)value));
                } else {
                    try {
                        fieldo.set(output, value);
                    } catch (Exception e) {
                        //TODO: handle exception
                    }
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                log.warn("get field value error", e);
            }
        }
    }

	// 解決 Stored XSS
	public static <T> List<Object> validList(List<T> results){
		List<Object> resultList = new LinkedList<Object>();
		try{
			//log.trace("ESAPI.validList.srcList: {}", results);
			// 依序取出需驗證的List內之物件
			for(Object obj : results) {
				boolean result = false;
                // 將物件toJson後做ESAPI驗證
                Gson gson = new GsonBuilder().setVersion(0).create();
				String json = gson.toJson(obj);
				String[] strArrays = StrUtils.splitStringByLength(json, 200);
				
				//log.trace("ESAPI.validList.strArrays: {}", strArrays.toString());
				
				//對toJson後的物件做ESAPI驗證
				for(String str : strArrays) {
					//log.trace("ESAPI.validList.str: {}", str);
					result = Sanitizer.isValidInput(str, "GeneralString", true);
					//log.trace("ESAPI.validList.result: {}", result);
					// 驗證失敗跳出迴圈
					if(result==false) {
						break;
					}
				}
				// 驗證失敗做下一個
				if(result==false) {
					continue;
				}
				// 驗證通過放入resultList
				if(result) {
					resultList.add(obj);
				}
			}
			//log.trace("ESAPI.validList.resultList: {}", resultList);
		}
		catch(Exception e){
			log.error("ESAPIUtil error={}",e);
		}
		return resultList;
    }

	/*
	 * 驗證輸入值 input-->輸入值 validateType-->驗證的類型
	 * allowBlank-->輸入值可為空值或null
	 */
	public static boolean isValidInput(String input,String validateType,boolean allowBlank) {
		boolean result = true;
		try{
			if(input != null && !"".equals(input)){
				result = ESAPI.validator().isValidInput("isValidInput",input,validateType,input.length(),allowBlank);
			}
		}
        catch(Exception e){
            log.error("isValidInput error={}",e);
            result = false;
        }
		return result;
	}
    
    /**
     * Log Forging 弱掃處理
     * @param message
     * @return
     */
    public static String logForgingStr(Object message) {
        Gson gson = new Gson();
        String jsonData = gson.toJson(message).replace("\"","");
        // String clean  = jsonData.replace( '\n' ,  '_' ).replace( '\r' , '_' )
        //   .replace( '\t' , '_' );
        //   clean = ESAPI.encoder().encodeForHTML( clean );
        String clean = "";
        try {
            clean = jsonData.replaceAll( "\n" ,  "" ).replaceAll( "\r" , "" )
                      .replaceAll( "\t" , "_" );
        } catch (Exception e) {
            log.error("logForgingStr.error>>{}",e.toString());
        }          
        return clean;
    }

    /**
     * 處理 Log Forging 物件，只處理欄位是字串，只能是寫 log 需求的才要呼叫
     * @param <T>   泛型類別
     * @param input 泛型物件
     */
    public static <T> void logForging4Class(T input) {
        Field[] fields = input.getClass().getDeclaredFields();
        for ( int i=0; i<fields.length; i++ ) {
            Field field = fields[i];
            field.setAccessible(true);
            
            try {
                Object value = field.get(input);
                if ( value instanceof String ) {
                    String output = (String)value;
                    output = output.replaceAll("\r", "");
                    output = output.replaceAll("\n", "");
                    field.set(input, output);
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                log.warn("get field value error", e);
            }
        }
    }

	/**
	 * Log Forging 漏洞校驗
	 * @param log
	 * @return
	 */
	public static String vaildLog(String log)
	{
		List<String> list = new ArrayList<String>();
		list.add("%0d");
		list.add("\r");
		list.add("%0a");
		list.add("\n");
		String encode = Normalizer.normalize(log, Normalizer.Form.NFKC);
		for (int i = 0; i < list.size(); i++)
		{
			encode = encode.replace(list.get(i), "");
		}
		return encode;

    }

    /**
     * 字串轉日期再轉回字串
     * @param dtPattern     格式，例如：yyyy/MM/dd HH:mm:ss
     * @param s             要轉換的值
     * @return
     */
    public static String parseDateTime(String dtPattern, String s) {
        Date d = DateTimeUtils.parse(dtPattern, s);
        return DateTimeUtils.getDatetime(d);
    }
}