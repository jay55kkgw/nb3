package fstop.util;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * 2019 jimmy 重寫
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Component
@Data
public class SpringBeanFactory {

    @Autowired
    private static ApplicationContext context;

    /**
     * <p>setApplicationContext.</p>
     *
     * @param _context a {@link org.springframework.context.ApplicationContext} object.
     */
    public static void setApplicationContext(ApplicationContext _context) {
        context = _context;
    }


    /**
     * <p>getBean.</p>
     *
     * @param beanid a {@link java.lang.String} object.
     * @return a {@link java.lang.Object} object.
     */
    public static Object getBean(String beanid) {
        return context.getBean(beanid);
    }

    /**
     * <p>getBean.</p>
     *
     * @param beanCls a {@link java.lang.Class} object.
     * @return a {@link java.lang.Object} object.
     */
    public static Object getBean(Class beanCls) {
        return context.getBean(beanCls);
    }
}
