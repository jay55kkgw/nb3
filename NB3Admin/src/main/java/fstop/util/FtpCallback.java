package fstop.util;

import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;

/**
 * <p>FtpCallback interface.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public interface FtpCallback {

	/**
	 * <p>doFtpAction.</p>
	 *
	 * @param ftpclient a {@link org.apache.commons.net.ftp.FTPClient} object.
	 * @return a int.
	 * @throws java.io.IOException if any.
	 */
	public int doFtpAction(FTPClient ftpclient) throws IOException;
	
}
