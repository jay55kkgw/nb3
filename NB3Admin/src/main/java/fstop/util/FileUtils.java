package fstop.util;

import java.io.*;
import java.util.*;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;


/**
 * <p>StrUtils class.</p>
 *
 * @author Danny
 * @version V1.0
 */
@Slf4j
public class FileUtils {

    /**
	 * 檔案檢核
	 * @param file
	 * @param fileName
	 * @param fileContentType
	 * @param allowFileType
	 * @param allowFileSize
	 * @return
	 */
    public static Map<String,Object> checkFile(MultipartFile file,String fileName,String fileContentType,String[] allowFileType,int allowFileSize){
		return checkFile(file,fileName,fileContentType,allowFileType,allowFileSize,true);
	}

	/**
	 * 
	 * @param file
	 * @param fileName
	 * @param fileContentType
	 * @param allowFileType
	 * @param allowFileSize
	 * @return
	 */
	public static Map<String,Object> checkFile(MultipartFile file,String fileName,String fileContentType,String[] allowFileType,int allowFileSize,boolean dataIsStr){
		Map<String,Object> returnMap = new HashMap<String,Object>();
		returnMap.put("result","FALSE");
		returnMap.put("summary","");
		try{
			//判斷格式
			for(int x=0;x<allowFileType.length;x++){
				if(allowFileType[x].equals(fileContentType)){
					break;
				}
				if(x == allowFileType.length - 1){
					returnMap.put("summary","檔案格式不符");
				}
			}
			long size = file.getSize();
			//判斷大小
			if(size > allowFileSize){
				returnMap.put("summary","檔案過大，不能超過" + allowFileSize / 1024 + "KB");//檔案檔案過大，不能超過
			}
			//判斷檔名是否過長
			if(fileName.length() > 300){
				returnMap.put("summary","檔名過長");//檔名過長
			}
			//判斷副檔名是否過長
			if(FilenameUtils.getExtension(fileName).length() > 10){
				returnMap.put("summary","副檔名過長");//副檔名過長
			}

			if("".equals(returnMap.get("summary"))){
				//20191204-Danny-Unrestricted File Upload
				if(file.getSize() <= allowFileSize){
					//檢核通過
					returnMap.put("result","TRUE");
					
					byte[] bHoliday = file.getBytes();
					//20191205-Danny-Unrestricted File Upload
					if (dataIsStr){
						String fileContent = new String(bHoliday);
						returnMap.put("Data",fileContent);	
					}else{
						returnMap.put("Data",bHoliday);	
					}
				}								
			}
		}
		catch(Exception e){
			if(returnMap.get("summary").equals("")) {
                //returnMap.put("summary",i18n.getMsg("LB.X2131"));
                returnMap.put("summary","系統錯誤，請重新嘗試");
			}
//			returnMap.put("message",String.valueOf(e.getMessage()));
		}
		return returnMap;
	}

	// FEFF because this is the Unicode char represented by the UTF-8 byte order mark (EF BB BF).  
	private static final String UTF8_BOM = "\uFEFF";  

	public static String RemoveUTF8BOM(String s) {  
		if (s.startsWith(UTF8_BOM)) {   
			s = s.substring(1); // 如果 String 是以 BOM 開頭, 則省略字串最前面的第一個 字元.  
		}  
		return s;  
	}  
	
}
