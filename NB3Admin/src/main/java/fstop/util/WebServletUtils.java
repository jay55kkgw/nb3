package fstop.util;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 將 Request 及 Response 放至到 ThreadLocal
 * 需配合 SysFilter 作用
 *
 * @author Owner
 * @version V1.0
 */
public class WebServletUtils {
	private static ThreadLocal req = new ThreadLocal();
	private static ThreadLocal res = new ThreadLocal();
	private static ServletConfig servletConfig = null;
	private static ThreadLocal uploadFiles = new ThreadLocal();
	
	/**
	 * <p>initThreadLocal.</p>
	 *
	 * @param request a {@link javax.servlet.http.HttpServletRequest} object.
	 * @param response a {@link javax.servlet.http.HttpServletResponse} object.
	 */
	public static void initThreadLocal(HttpServletRequest request, HttpServletResponse response) {
		req.set(request);
		res.set(response);
	}
	
	/**
	 * <p>removeThreadLocal.</p>
	 */
	public static void removeThreadLocal() {
		req.set(null);
		res.set(null);
		uploadFiles.set(null);
	}
	
	/**
	 * <p>getRequest.</p>
	 *
	 * @return a {@link javax.servlet.http.HttpServletRequest} object.
	 */
	public static HttpServletRequest getRequest() {
		return (HttpServletRequest)req.get();
	}

	/**
	 * <p>getResponse.</p>
	 *
	 * @return a {@link javax.servlet.http.HttpServletResponse} object.
	 */
	public static HttpServletResponse getResponse() {
		return (HttpServletResponse)res.get();
	}
	
	/**
	 * <p>Getter for the field <code>servletConfig</code>.</p>
	 *
	 * @return a {@link javax.servlet.ServletConfig} object.
	 */
	public static ServletConfig getServletConfig() {
		return servletConfig;
	}

	/**
	 * <p>Setter for the field <code>servletConfig</code>.</p>
	 *
	 * @param config a {@link javax.servlet.ServletConfig} object.
	 */
	public static void setServletConfig(ServletConfig config) {
		servletConfig = config;
	}
	
	/**
	 * <p>Setter for the field <code>uploadFiles</code>.</p>
	 *
	 * @param u a {@link fstop.util.UploadFiles} object.
	 */
	public static void setUploadFiles(UploadFiles u) {
		uploadFiles.set(u);
	}

	/**
	 * <p>Getter for the field <code>uploadFiles</code>.</p>
	 *
	 * @return a {@link fstop.util.UploadFiles} object.
	 */
	public static UploadFiles getUploadFiles() {
		return (UploadFiles)uploadFiles.get();
	}

}
