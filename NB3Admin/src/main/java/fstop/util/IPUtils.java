package fstop.util;

import fstop.exception.ToRuntimeException;
import fstop.util.thread.WebServletUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
//import fstop.util.thread.WebServletUtils;

/**
 * <p>IPUtils class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class IPUtils {
	/**
	 * <p>getLocalIp.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public static String getLocalIp() {
		try {
		    InetAddress addr = InetAddress.getLocalHost();

		    byte[] ipAddr = addr.getAddress();

		    String s =  (int)(ipAddr[0] & 0x00ff) + "." + (int)(ipAddr[1] & 0x00ff)  + "." +  (int)(ipAddr[2] & 0x00ff)  + "." + (int)(ipAddr[3] & 0x00ff);
		    return s;
		}
		catch(Exception e) {
			throw new ToRuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * <p>isLocalTestIp.</p>
	 *
	 * @return a boolean.
	 */
	public static boolean isLocalTestIp() {
		if("10.10.215.95".equals(getLocalIp()))
			return true;
		if(getLocalIp().startsWith("192.168.11."))
			return true;

		return false;
	}


	/**
	 * <p>getRemoteIP.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public static String getRemoteIP() {
		HttpServletRequest request = WebServletUtils.getRequest();
		if(request != null) {
			//String ip = request.getHeader("x-forwarded-for");
			String ip = request.getRemoteAddr();
			if (ip == null || ip.length() == 0) {
				ip = request.getRemoteAddr();
			}

			//ip = request.getHeader("x-forwarded-for");
			if (StrUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("Proxy-Client-IP");
			}

			if (StrUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("WL-Proxy-Client-IP");
			}

			if (StrUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getRemoteAddr();
			}

			return ip;
		}
		else {
			return getLocalIp();
		}
	}

}
