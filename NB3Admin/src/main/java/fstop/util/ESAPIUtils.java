package fstop.util;

import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.EncodingException;
import org.owasp.esapi.errors.ValidationException;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * ESAPIUtils class.
 * </p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
public class ESAPIUtils {
	/**
	 * <p>
	 * canonicalize.
	 * </p>
	 *
	 * @param str a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String canonicalize(String str) {
		String result;
		try {
			result = ESAPI.encoder().canonicalize(str);
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("canonicalize Error", e);
			result = str;
		}
		return result;
	}

	/**
	 * 20131128 huangpu 從session取出的變數在轉型為字串陣列前的過濾
	 *
	 * @param arg0 a {@link java.lang.Object} object.
	 * @return an array of {@link java.lang.String} objects.
	 */
	public static String[] canonicalizeForStringArray(Object arg0) {
		String[] result = null;
		if (arg0 != null) {
			try {
				String[] temp = (String[]) arg0;
				result = new String[temp.length];
				for (int i = 0; i < temp.length; i++) {
					result[i] = ESAPI.encoder().canonicalize(temp[i]);
				}
			} catch (Exception e) {
				// e.printStackTrace();
				log.error("", e);
			}
		}

		return result;
	}

	/**
	 * <p>
	 * canonicalize.
	 * </p>
	 *
	 * @param stringArray an array of {@link java.lang.String} objects.
	 * @return an array of {@link java.lang.String} objects.
	 */
	public static String[] canonicalize(String[] stringArray) {
		String[] result = null;
		if (stringArray != null) {
			result = new String[stringArray.length];
			for (int i = 0; i < stringArray.length; i++) {
				try {
					result[i] = ESAPI.encoder().canonicalize(stringArray[i]);
				} catch (Exception e) {
					// e.printStackTrace();
					log.error("canonicalize Error", e);
					result[i] = stringArray[i];
				}
			}
		}
		return result;
	}

	/**
	 * <p>
	 * canonicalize.
	 * </p>
	 *
	 * @param str a {@link java.lang.String} object.
	 * @param b   a boolean.
	 * @return a {@link java.lang.String} object.
	 */
	public static String canonicalize(String str, boolean b) {
		String result;
		try {
			result = ESAPI.encoder().canonicalize(str, b);
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("canonicalize Error", e);
			result = str;
		}

		return result;
	}

	/**
	 * <p>
	 * encodeURL.
	 * </p>
	 *
	 * @param arg0 a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String encodeURL(String arg0) {
		String result = "";
		try {
			result = ESAPI.encoder().encodeForURL(arg0);
		} catch (EncodingException e) {
			// e.printStackTrace();
			log.error("error >> {}", e);
		}
		return result;
	}

	/**
	 * <p>
	 * encodeJS.
	 * </p>
	 *
	 * @param arg0 a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String encodeJS(String arg0) {
		return ESAPI.encoder().encodeForJavaScript(arg0);
	}

	/**
	 * <p>
	 * encodeHTML.
	 * </p>
	 *
	 * @param arg0 a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String encodeHTML(String arg0) {
		return ESAPI.encoder().encodeForHTML(arg0);
	}

	/**
	 * <p>
	 * encodeHTMLAttr.
	 * </p>
	 *
	 * @param arg0 a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String encodeHTMLAttr(String arg0) {
		return ESAPI.encoder().encodeForHTMLAttribute(arg0);
	}

	/**
	 * Returns the named parameter from the HttpServletRequest after canonicalizing
	 * and filtering out any dangerous characters.
	 *
	 * @param orig a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getParameter(String orig) {
		String clean = orig;
		try {
			if (clean != null && !"".equals(clean)) {
				clean = ESAPI.validator().getValidInput("getValidInput:" + clean, clean, "SafeString", clean.length(),
						true);
			}
		} catch (ValidationException e) {
			// e.printStackTrace();
			log.error("error >> {}", e);
		}
		return clean;
	}

	/**
	 * validateByteArray
	 * 
	 * @param byteArray  a byte array
	 * @param allowBlank a boolean.
	 * @return byte[]
	 */
	public static byte[] validateByteArray(byte[] byteArray, boolean allowBlank) {
		try {
			byteArray = ESAPI.encoder().decodeFromBase64(ESAPI.encoder().encodeForBase64(byteArray, allowBlank));
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("error >> {}", e);
		}
		return byteArray;
	}

	/**
	 * <p>
	 * validInput.
	 * </p>
	 *
	 * @param input        a {@link java.lang.String} object.
	 * @param validateType a {@link java.lang.String} object.
	 * @param maxLength    a int.
	 * @param allowBlank   a boolean.
	 * @return a {@link java.lang.String} object.
	 */
	public static String validInput(String input, String validateType, int maxLength, boolean allowBlank) {
		try {
			input = ESAPI.validator().getValidInput("validInput", input, validateType, maxLength, allowBlank);
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("error >> {}", e);
		}
		return input;
	}

	/**
	 * <p>
	 * main.
	 * </p>
	 *
	 * @param args an array of {@link java.lang.String} objects.
	 * @throws java.lang.Exception if any.
	 */
	public static void main(String[] args) throws Exception {
		String a = "測試字串!@#$%^&*()_+?&";
		String b = "jdbc:sqlserver://TDB0601:1433;databasename=WSPDB";
		// String b = "http://10.1.3.26/wpl/logon.jsp";
		String c = "http://10.1.3.26/wpl/logon.jsp?SRNO1=103BFA23015&YEAR=103";
		String d = "<span style='width: 100%;'></span>";
		String e = "<span style=\"width: 100%;\"></span>";
		String f = "100";

		log.info("canonicalize(a) = " + canonicalize(a));
		log.info("canonicalize(b) = " + canonicalize(b));
		log.info("canonicalize(c) = " + canonicalize(c));
		log.info("canonicalize(d) = " + canonicalize(d));
		log.info("canonicalize(e) = " + canonicalize(e));
		log.info("canonicalize(f) = " + canonicalize(f));

		log.info("encodeJS(a) = " + encodeJS(a));
		log.info("encodeJS(b) = " + encodeJS(b));
		log.info("encodeJS(c) = " + encodeJS(c));
		log.info("encodeJS(d) = " + encodeJS(d));
		log.info("encodeJS(e) = " + encodeJS(e));
		log.info("encodeJS(f) = " + encodeJS(f));

		log.info("encodeURL(a) = " + encodeURL(a));
		log.info("encodeURL(b) = " + encodeURL(b));
		log.info("encodeURL(c) = " + encodeURL(c));
		log.info("encodeURL(d) = " + encodeURL(d));
		log.info("encodeURL(e) = " + encodeURL(e));
		log.info("encodeURL(f) = " + encodeURL(f));

		log.info("encodeHTML(a) = " + encodeHTML(a));
		log.info("encodeHTML(b) = " + encodeHTML(b));
		log.info("encodeHTML(c) = " + encodeHTML(c));
		log.info("encodeHTML(d) = " + encodeHTML(d));
		log.info("encodeHTML(e) = " + encodeHTML(e));
		log.info("encodeHTML(f) = " + encodeHTML(f));

		log.info("encodeHTMLAttr(a) = " + encodeHTMLAttr(a));
		log.info("encodeHTMLAttr(b) = " + encodeHTMLAttr(b));
		log.info("encodeHTMLAttr(c) = " + encodeHTMLAttr(c));
		log.info("encodeHTMLAttr(d) = " + encodeHTMLAttr(d));
		log.info("encodeHTMLAttr(e) = " + encodeHTMLAttr(e));
		log.info("encodeHTMLAttr(f) = " + encodeHTMLAttr(f));

		log.info(ESAPI.validator().getValidInput("HTTP context path: ", "/tbbeatm", "HTTPContextPath", 150, false));
	}

	/**
	 * <p>
	 * toHex.
	 * </p>
	 *
	 * @param b a byte array
	 * @return Hex String
	 */
	public static String toHex(byte[] b) {
		StringBuffer hex = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			hex.append("" + "0123456789ABCDEF".charAt(0xf & b[i] >> 4) + "0123456789ABCDEF".charAt(b[i] & 0xf));
		}
		return hex.toString();
	}

	/**
	 * <p>
	 * Hex2Bin.
	 * </p>
	 *
	 * @param hex a byte array
	 * @return byte array
	 */
	public static byte[] Hex2Bin(byte[] hex) {
		byte[] bin = new byte[hex.length / 2];
		for (int i = 0, j = 0; i < bin.length; i++, j += 2) {
			int iL = hex[j] - '0';
			if (iL > 9) {
				iL -= 7;
			}
			iL <<= 4;
			//
			int iR = hex[j + 1] - '0';
			if (iR > 9) {
				iR -= 7;
			}
			bin[i] = (byte) (iL | iR);
		}
		return bin;
	}

	public static String validStr(String str) {
		String cleanstr = str;
		if (cleanstr.isEmpty()) {
			return str;
		}
		cleanstr = cleanstr.replaceAll("(?:%|\n|\r|\\|\r\n|>|<|\\$)", "");

		return ESAPIUtils.isValidInput(cleanstr, "GeneralString", true) == true ? str
				: ESAPIUtils.validInput(cleanstr, "GeneralString", true);
	}
	
	/**
	 * 使用 validation.properties 中設定的驗證規則來檢核.
	 * 
	 * @param input - input data to validate
	 * @param validateType - validation type： SafeString, Email, IPAddress, URL, CreditCard, SSN
	 * @param allowBlank - is allow blank string
	 * @return valid input string
	 */
	public static String validInput(String input, String validateType, boolean allowBlank)
	{
		try
		{
			if (input != null && !"".equals(input))
			{
				input = ESAPI.validator().getValidInput("validInput", input, validateType, input.length(), allowBlank);
			}
		}
		catch (Exception e)
		{
			log.error("ESAPIUtil error={}", e);
		}
		return input;
	}
	
	/*
	 * 驗證輸入值 input-->輸入值 validateType-->驗證的類型 allowBlank-->輸入值可為空值或null
	 */
	public static boolean isValidInput(String input, String validateType, boolean allowBlank)
	{
		boolean result = true;
		try
		{
			if (input != null && !"".equals(input))
			{
				result = ESAPI.validator().isValidInput("isValidInput", input, validateType, input.length(), allowBlank);
			}
		}
		catch (Exception e)
		{
			log.error("ESAPIUtil error={}", e);
		}
		return result;
	}

}
