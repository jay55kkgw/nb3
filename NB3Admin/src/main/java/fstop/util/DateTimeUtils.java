package fstop.util;

import fstop.exception.ToRuntimeException;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * <p>DateTimeUtils class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
public class DateTimeUtils {
	static SimpleDateFormat dtF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	static SimpleDateFormat dtD = new SimpleDateFormat("yyyyMMdd");
	static SimpleDateFormat dtT = new SimpleDateFormat("HHmmss");
	static Map<String, SimpleDateFormat> pool = new HashMap();

	/**
	 * 將指定的 names 把 params 裡的值的日期, 如 2008/02/01 格式的日期, 變成 20080201
	 *
	 * @param params a {@link java.util.Hashtable} object.
	 * @param names an array of {@link java.lang.String} objects.
	 */
	public static void removeSlash(Hashtable params, String[] names) {
		if(names != null && params != null) {
			for(String o : names) {
				String s = ((String)params.get(o)).replaceAll("/", "");
				params.put(o, s);
			}
		}
	}
	
	/**
	 * 將 20080201 格式的日期, 變成 2008/02/01
	 *
	 * @param dateShort a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String addSlash(String dateShort) {
		return dateShort.substring(0, 4) + "/" + dateShort.substring(4, 6) + "/" + dateShort.substring(6, 8);
	}
	
	/**
	 * <p>format.</p>
	 *
	 * @param pattern a {@link java.lang.String} object.
	 * @param d a {@link java.util.Date} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String format(String pattern, Date d) {
		SimpleDateFormat sdf = getDateFormatFromPool(pattern);
		return sdf.format(d);		
	}

	/**
	 * <p>parse.</p>
	 *
	 * @param pattern a {@link java.lang.String} object.
	 * @param value a {@link java.lang.String} object.
	 * @return a {@link java.util.Date} object.
	 */
	public static Date parse(String pattern, String value) {
		try {
			SimpleDateFormat sdf = getDateFormatFromPool(pattern);
			return sdf.parse(value);
		}
		catch(ParseException e) {
			throw new ToRuntimeException("無法 Parse [" + value+ "] 成 Date Object.", e);
		}
	}

	private static SimpleDateFormat getDateFormatFromPool(String pattern) {
		SimpleDateFormat sdf = null;
		if(!pool.containsKey(pattern)) {
			sdf = new SimpleDateFormat(pattern);
			pool.put(pattern, sdf);
		}
		else {
			sdf = pool.get(pattern);
		}
		return sdf;
	}
	
	/**
	 * 回傳 2008/10/22 20:12:00
	 *
	 * @param d a {@link java.util.Date} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getDatetime(Date d) {
		return dtF.format(d);
	}
	
	/**
	 * 回傳 2008/10/22
	 *
	 * @param d a {@link java.util.Date} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getDateShort(Date d) {
		return dtD.format(d);
	}
	/**
	 * 回傳 0971011
	 *
	 * @param d a {@link java.util.Date} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getCDateShort(Date d) {
		String dt = dtD.format(d);
		String result = dt.substring(4);
		result = new Integer(dt.substring(0, 4)) - 1911 + result;
		if(result.length() == 6)
			result = "0" + result;
		return result;
	}
	
	/**
	 * <p>getTimeShort.</p>
	 *
	 * @param d a {@link java.util.Date} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getTimeShort(Date d) {
		return dtT.format(d);
	}
	
	
	/**
	 * 傳入民國日期及日期, 如 "0970501", "103020", 回傳 097/05/01 10:30:20
	 *
	 * @param cDateSort a {@link java.lang.String} object.
	 * @param timeSort a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getCDateTime(String cDateSort, String timeSort) {
		String result = cDateSort + " " + timeSort;
		return result.replaceAll("(\\d{3})(\\d{2})(\\d{2})(\\s)(\\d{2})(\\d{2})(\\d{2})", "$1/$2/$3 $5:$6:$7");
	}
	
	/**
	 * 傳入民國年月日, 如 "0970501"  回傳 Date 物件 -> 20080501
	 *
	 * @param cDateSort a {@link java.lang.String} object.
	 * @return a {@link java.util.Date} object.
	 */
	public static Date getCDate2Date(String cDateSort) {
		String s = (new Integer(cDateSort.substring(0, 3)) + 1911) + "";
		return DateTimeUtils.parse("yyyyMMdd", s + cDateSort.substring(3));
	}
	
	/**
	 * 加入時間符號 ":"
	 *
	 * @param dateShort a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String addColon(String dateShort) {
		return dateShort.substring(0, 2) + ":" + dateShort.substring(2, 4) + ":" + dateShort.substring(4, 6);
	}
	/**
	 * <p>getDate.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public static String getDate(){
		  SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd");
		  Date dd = new Date();
		  return ft.format(dd);
	}

	 /**
	  * 算出time1跟time2間隔幾日

	  * @param time1 yyyy/MM/dd
	  * @param time2 yyyy/MM/dd
	  * @return 日差
	  */
	public static long getQuot(String time1, String time2){
		  long quot = 0;
		  SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd");
		  try {
		   Date date1 = ft.parse( time1 );
		   Date date2 = ft.parse( time2 );
		   quot = date1.getTime() - date2.getTime();
		   quot = quot / 1000 / 60 / 60 / 24;
		  } catch (ParseException e) {
			//    e.printStackTrace(); 
			log.error("",e);
		  }
		  return quot;
	}


	/**
	 * 得到本月的第一天
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public static String getMonthFirstDay() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	    return dtD.format(calendar.getTime()); 
	}
	
	/**
	 * 得到本月的最後一天
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public static String getMonthLastDay() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	    return dtD.format(calendar.getTime()); 
	}
	
	/**
	 * 得到上個月的第一天
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public static String getPrevMonthFirstDay() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	    return dtD.format(calendar.getTime()); 
	}
	
	/**
	 * 得到上個月的最後一天
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public static String getPrevMonthLastDay() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	    return dtD.format(calendar.getTime()); 
	}  
	/**
	 * 取得下個日期
	 *
	 * @param retPattern 輸入回傳的格式 ex yyyy/MM/dd =2016/07/16
	 * @return a {@link java.lang.String} object.
	 */
	public static String getNextDate(String retPattern) {
		  SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		  Calendar calendar = null;
		  Date date = null;
		try {
			date = format.parse(getDate(""));
			calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			format.applyPattern(retPattern);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("",e);
		}
		//System.out.println("next date>>"+format.format(calendar.getTime()));
		  return format.format(calendar.getTime()); 
		}	
	/**
	 * <p>getDate.</p>
	 *
	 * @param sign a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getDate(String sign){
		if(StrUtils.isEmpty(sign)){sign="";}
		Calendar cal = Calendar.getInstance();
		String theDate = null;
		String month = String.valueOf((cal.get(Calendar.MONTH)+1)/10) +""+ String.valueOf((cal.get(Calendar.MONTH)+1)%10);
		String day= String.valueOf(cal.get(Calendar.DAY_OF_MONTH)/10) +""+ String.valueOf(cal.get(Calendar.DAY_OF_MONTH)%10);
			   theDate = cal.get(Calendar.YEAR) +sign+ month+sign+ day;
			   //System.out.println("theDate>>"+theDate);
		return theDate; 
	}
	/**
	 * <p>checkleapyear.</p>
	 *
	 * @param year a int.
	 * @return a boolean.
	 */
	public static boolean checkleapyear(int year) {
		boolean leapyear=false;
		//設定欲判斷之年
		System.out.print("西元" + year + "年");
		loop4:
		if (year%4==0)//可被4除盡
		{
			if (year%100==0)
			//可被4、100除盡
			{
				if (year%400==0)
				//可被4、100、400除盡
				{
					log.info("是閏年！");
					leapyear=true;
					break loop4;
				}
				else
				//可被4、100除盡，不可被400除盡
				{
					log.info("不是閏年！");
					leapyear=false;
					break loop4;
				}
			}
			log.info("是閏年！");
			leapyear=true;
			//可被4除盡，不可被100除盡
		}
		else {
			//不可被4除盡
			log.info("不是閏年！");
			leapyear=false;
		}
		return leapyear;
	}

	/**
	 * 算出台幣預約轉帳下一轉帳日
	 * @param time1 週期執行日
	 * @param time2 週期性的開始日
	 * @param time3 週期性的截止日
	 * @return NextDate
	 */ 
	public static String getTWNextDate(String time1, String time2, String time3){
		Calendar date=Calendar.getInstance();
		int year= new Integer(date.get(Calendar.YEAR));
		int month=new Integer(date.get(Calendar.MONTH)+1);
		int nowday=new Integer(date.get(Calendar.DAY_OF_MONTH));
		int timehour=new Integer(date.get(Calendar.HOUR_OF_DAY));
		String PERMTDATE="";
		int [] days = {31,28,31,30,31,30,31,31,30,31,30,31};
		boolean checkleapyear=checkleapyear(year);   //判斷是不是閏年
		if(checkleapyear)
			days[1]=29;
		String NextDate="";
		Date d = new Date();
		int today = Integer.parseInt(DateTimeUtils.format("yyyyMMdd", d));
		int startdate = Integer.parseInt(time2);
		int enddate = Integer.parseInt(time3);
		time1 = (time1 == null || time1.equals("")) ?  "0" : time1;
		int executeday = Integer.parseInt(time1);
		if(!time1.equals("0")) {
			if(((startdate<today) && (nowday<executeday) && (enddate>today)) || ((startdate>=today) && (nowday<executeday) && (enddate>today)))
			{
				int startmon=Integer.parseInt(String.valueOf(startdate).substring(4,6));
				int todaymon=Integer.parseInt(String.valueOf(today).substring(4,6));
				if(startdate<=today)
				{
					if(executeday<10)
						PERMTDATE="0"+ String.valueOf(executeday);
					else
					{
						if(executeday>days[month-1])
							executeday=days[month-1];
						PERMTDATE=String.valueOf(executeday);
					}
					NextDate=DateTimeUtils.format("yyyyMMdd", d).substring(0,6)+PERMTDATE;
				}
				else
				{
					checkleapyear=checkleapyear(Integer.parseInt(String.valueOf(startdate).substring(0,4)));
					if(checkleapyear)
						days[1]=29;
					if(executeday<10)
						PERMTDATE="0"+ String.valueOf(executeday);
					else
					{
						if(executeday>days[startmon-1])
							executeday=days[startmon-1];
						PERMTDATE=String.valueOf(executeday);
					}
					NextDate=String.valueOf(startdate).substring(0,6)+PERMTDATE;
				}
			}
			else if(((startdate<today) && (nowday>=executeday) && (enddate>today)) || ((startdate>=today) && (nowday>=executeday) && (enddate>today)))
			{
				String amonth;
				int startmon=Integer.parseInt(String.valueOf(startdate).substring(4,6));
				int todaymon=Integer.parseInt(String.valueOf(today).substring(4,6));
				int startyear=Integer.parseInt(String.valueOf(startdate).substring(0,4));
				int startym=Integer.parseInt(String.valueOf(startdate).substring(0,6));
				int todayym=Integer.parseInt(String.valueOf(today).substring(0,6));
				int startday=Integer.parseInt(String.valueOf(startdate).substring(6,8));
				if(startdate<=today)
				{
					if((timehour>=11 && nowday==executeday) || (nowday>executeday))
						month=month+1;
					if(timehour<11 && nowday==executeday)
						month=month;
					if(month>12)
					{
						month=1;
						year=year+1;
					}
					if(month<10)
						amonth="0"+String.valueOf(month);
					else
						amonth=String.valueOf(month);
					if(executeday<10)
						PERMTDATE="0"+ String.valueOf(executeday);
					else
					{
						if(executeday>days[month-1])
							executeday=days[month-1];
						PERMTDATE=String.valueOf(executeday);
					}
					NextDate=String.valueOf(year)+amonth+PERMTDATE;
				}
				else
				{
					if((timehour>=11 && nowday==executeday && startym==todayym) || (nowday>executeday && startym==todayym) || (nowday>executeday && startym>todayym && startday>executeday))
						startmon=startmon+1;
					if((timehour<11 && nowday==executeday && startym==todayym) || (nowday>executeday && startym>todayym && startday<=executeday))
						startmon=startmon;

					if(startmon>12)
					{
						startmon=1;
						startyear=startyear+1;
					}
					if(startmon<10)
						amonth="0"+String.valueOf(startmon);
					else
						amonth=String.valueOf(startmon);
					checkleapyear=checkleapyear(startyear);
					if(checkleapyear)
						days[1]=29;
					if(executeday<10)
						PERMTDATE="0"+ String.valueOf(executeday);
					else
					{
						if(executeday>days[startmon-1])
							executeday=days[startmon-1];
						PERMTDATE=String.valueOf(executeday);
					}
					NextDate=String.valueOf(startyear)+amonth+PERMTDATE;
				}
			}
			else
				NextDate="";
		}
		else
			NextDate=time3;

		return NextDate;
	}

	/**
	 * <p>main.</p>
	 *
	 * @param s an array of {@link java.lang.String} objects.
	 */
	public static void main(String[] s) {

	}	
	
	/**
	 * <p>format.</p>
	 *
	 * @param pattern a {@link java.lang.String} object.
	 * @param d a {@link java.util.Date} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String formatYYYYMMDD(String value) {
		SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd");
		Date date1 = new Date();
		try {
			date1 = ft.parse(value);
		} catch (ParseException e) {
			log.error("",e);			
		}
		return dtD.format(date1);		
	}	
}
