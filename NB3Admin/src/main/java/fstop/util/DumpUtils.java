package fstop.util;

import com.netbank.util.StrUtils;
import fstop.model.Row;
import fstop.model.Rows;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.Map.Entry;

/**
 * <p>DumpUtils class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
public class DumpUtils {

	private static String crlf = System.getProperty("line.separator");

	private static void dump(StringBuffer sb, String prefix, List v) {
		if(v == null)
			return;
		List enumList = v;

		dumpObject(sb, prefix, getClsName(v.getClass().getName()), enumList);
	}

	private static void dump(StringBuffer sb, String prefix, Rows v) {
		if(v == null)
			return;
		List<Row> enumList = v.getRows();

		dumpObject(sb, prefix, getClsName(v.getClass().getName()), enumList);
	}

	private static void sortEntries(List<Entry> entryList) {
		Collections.sort(entryList, new java.util.Comparator<Entry>(){
			public int compare(Entry o1, Entry o2) {
				String key1 = (String)o1.getKey();
				String key2 = (String)o2.getKey();
				return key1.compareTo(key2);
			}});
	}

	private static void dump(StringBuffer sb, String prefix, Dictionary d) {
		if(d == null)
			return;
		List<Entry> enumList = Collections.list(d.elements());
		sortEntries(enumList);
		dumpObject(sb, prefix, getClsName(d.getClass().getName()), enumList);
	}

	private static void dump(StringBuffer sb, String prefix, Map m) {
		if(m == null)
			return;

		List<Entry> enumList = new ArrayList(m.entrySet());
		sortEntries(enumList);


		dumpObject(sb, prefix, getClsName(m.getClass().getName()), enumList);
	}

	private static void dumpObject(StringBuffer sb, String prefix, String clsName, List enumList) {
		if(enumList == null)
			return;

		sb.append(crlf).append(String.format(prefix + "{type: '%s', size: %s, value: [ ", clsName, enumList.size()));
		StringBuffer stm = new StringBuffer();
		for(Object o : enumList) {
			printValue(stm, prefix + "\t", o);
		}
		String s = "";
		if(stm.length() > 0)
			s = stm.substring(0, stm.lastIndexOf(","));
		sb.append(s).append(crlf).append(prefix).append("]},");
	}

	/**
	 * 是否為敏感的欄位
	 * @param key
	 * @return
	 */
	private static boolean isSensitive(String key) {
		return (key.equals("CMPASSWORD")
				|| key.equals("N950PASSWORD")
				|| key.equals("CMPWD")
				|| key.equals("CUSIDN")
				|| key.equals("DPUSERNAME")
				|| key.equals("UID")
				|| key.equals("BOQUERYNEXT")
				|| key.equals("__BOQUERYNEXT"));
	}

	private static String filterEntryAppend(String prefix, String key, String value) {
		String star = "****";
		if (isSensitive(key))
		{
			if (key.equals("UID") || key.equals("CUSIDN"))
	  		{
				if(!StrUtils.isEmpty(value) && value.length()>7)
				value = value.substring(0, 3)+star+value.substring(7);
	  		  	return String.format("%s{type: 'Entry', key: '%s', value: '%s'}, ", prefix, key, value);
	  		}
			return String.format("%s{type: 'Entry', key: '%s', value: '%s'}, ", prefix, key, "------");
		}
		else {
			return String.format("%s{type: 'Entry', key: '%s', value: '%s'}, ", prefix, key, value);
		}
	}

	private static String filterRowAppend(String prefix, String key, String value) {
		if (isSensitive(key)) {
			return String.format("%s{type: 'Row', key: '%s', value: '%s'}", prefix, key, "------");
		}
		else {
			return String.format("%s{type: 'Row', key: '%s', value: '%s'}", prefix, key, value);
		}
	}

	private static void printValue(StringBuffer sb, String prefix, Object o) {

		if (o instanceof Rows) {
			dump(sb, prefix, (Rows) o);
		} else if (o instanceof List) {
			dump(sb, prefix, (List) o);
		} else if (o instanceof Map) {
			dump(sb, prefix, (Map) o);
		} else if (o instanceof Dictionary) {
			dump(sb, prefix, (Dictionary) o);
		} else if (o instanceof Map.Entry) {
			if(isPrimitive(((Entry)o).getValue())) {
				//sb.append(crlf).append(String.format("%s{type: 'Entry', key: '%s', value: '%s'}, ", prefix, ((Entry)o).getKey(), ((Entry)o).getValue()));
				String key = "";
				String value = "";
				if(((Entry)o).getKey() != null)
					key = (((Entry)o).getKey()).toString();

				if(((Entry)o).getValue() != null)
					value = (((Entry)o).getValue()).toString();

				sb.append(crlf).append(filterEntryAppend( prefix, key, value));
			}
			else {
				sb.append(crlf).append(String.format("%s{type: 'Entry', key: '%s', value:  ", prefix, ((Entry)o).getKey()));
				printValue(sb, prefix + "\t", ((Entry)o).getValue());
			}
		} else if (o instanceof Row) {
			Row r = (Row)o;
			for(String name : r.getColumnNames()) {
				//sb.append(crlf).append(String.format("%s{type: 'Row', key: '%s', value: '%s'}", prefix, name, r.getValue(name)));
				sb.append(crlf).append(filterRowAppend(prefix, name, r.getValue(name)));
			}
		}
		else { // only dump value
			sb.append(crlf).append(prefix).append("'").append(o).append("',");
		}
	}

	/**
	 * <p>dump.</p>
	 *
	 * @param o a {@link java.lang.Object} object.
	 */
	public static void dump(Object o) {
		StringBuffer sb = new StringBuffer(512);
		printValue(sb, "", o);
		String s =  "";
		if(sb.length() > 0)
			s = sb.substring(0, sb.lastIndexOf(","));

		log.debug(s);
	}

	/**
	 * <p>dumpToString.</p>
	 *
	 * @param o a {@link java.lang.Object} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String dumpToString(Object o) {
		StringBuffer sb = new StringBuffer(512);
		printValue(sb, "", o);
		String s =  "";
		if(sb.length() > 0)
			s = sb.substring(0, sb.lastIndexOf(","));

		return s;
	}

	/**
	 * <p>dump.</p>
	 *
	 * @param msg a {@link java.lang.String} object.
	 * @param o a {@link java.lang.Object} object.
	 */
	public static void dump(String msg, Object o) {
		log.debug(msg);
		dump(o);
	}

	private static boolean isPrimitive(Object o) {
		boolean result = true;
		if (o instanceof List) {
			return false;
		} else if (o instanceof Map) {
			return false;
		} else if (o instanceof Dictionary) {
			return false;
		} else if (o instanceof Rows) {
			return false;
		}

		return result;
	}

	private static String getClsName(String cls) {
		return cls.substring(cls.lastIndexOf(".") + 1);
	}

}
