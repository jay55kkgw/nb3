package fstop.util;

import org.apache.commons.fileupload.FileItem;

import java.util.Map;

/**
 * <p>UploadFiles class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class UploadFiles {
	
	private Map parameters;
	
	private Map<String, FileItem> fileItems;
	
	/**
	 * <p>Constructor for UploadFiles.</p>
	 *
	 * @param parameters a {@link java.util.Map} object.
	 * @param fileItems a {@link java.util.Map} object.
	 */
	public UploadFiles(Map parameters, Map<String, FileItem> fileItems) {
		this.parameters = parameters;
		this.fileItems = fileItems;
	}

	/**
	 * <p>Getter for the field <code>fileItems</code>.</p>
	 *
	 * @return a {@link java.util.Map} object.
	 */
	public Map<String, FileItem> getFileItems() {
		return fileItems;
	}

	/**
	 * <p>Setter for the field <code>fileItems</code>.</p>
	 *
	 * @param fileItems a {@link java.util.Map} object.
	 */
	public void setFileItems(Map<String, FileItem> fileItems) {
		this.fileItems = fileItems;
	}

	/**
	 * <p>Getter for the field <code>parameters</code>.</p>
	 *
	 * @return a {@link java.util.Map} object.
	 */
	public Map getParameters() {
		return parameters;
	}

	/**
	 * <p>Setter for the field <code>parameters</code>.</p>
	 *
	 * @param parameters a {@link java.util.Map} object.
	 */
	public void setParameters(Map parameters) {
		this.parameters = parameters;
	}
}
