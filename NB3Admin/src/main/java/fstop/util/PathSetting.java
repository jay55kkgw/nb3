package fstop.util;

/**
 * <p>PathSetting class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class PathSetting {
	private String tmpPath = "/usr/IBM/fstop/logs/temp";
	
	private String advPath = "/usr/IBM/fstop/adv"; //廣告放置路徑

	private String cfgPath = "/usr/IBM/fstop/cfg"; //cfg 放置路徑

	private String logsPath = "/usr/IBM/fstop/logs"; //logs路徑

	private String transferDataPath = "/usr/IBM/fstop/data";  //轉帳產生的暫存檔放的位置
	
	private String cachePath = "/usr/IBM/fstop/cache";  //Cache 檔放置位置
	
	/**
	 * <p>Getter for the field <code>cachePath</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCachePath() {
		return cachePath;
	}

	/**
	 * <p>Setter for the field <code>cachePath</code>.</p>
	 *
	 * @param cachePath a {@link java.lang.String} object.
	 */
	public void setCachePath(String cachePath) {
		this.cachePath = cachePath;
	}

	/**
	 * <p>Getter for the field <code>logsPath</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLogsPath() {
		return logsPath;
	}

	/**
	 * <p>Setter for the field <code>logsPath</code>.</p>
	 *
	 * @param logsPath a {@link java.lang.String} object.
	 */
	public void setLogsPath(String logsPath) {
		this.logsPath = logsPath;
	}

	/**
	 * <p>Getter for the field <code>transferDataPath</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTransferDataPath() {
		return transferDataPath;
	}

	/**
	 * <p>Setter for the field <code>transferDataPath</code>.</p>
	 *
	 * @param transferDataPath a {@link java.lang.String} object.
	 */
	public void setTransferDataPath(String transferDataPath) {
		this.transferDataPath = transferDataPath;
	}

	/**
	 * <p>Getter for the field <code>advPath</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAdvPath() {
		return advPath;
	}

	/**
	 * <p>Setter for the field <code>advPath</code>.</p>
	 *
	 * @param advPath a {@link java.lang.String} object.
	 */
	public void setAdvPath(String advPath) {
		this.advPath = advPath;
	}

	/**
	 * <p>Getter for the field <code>tmpPath</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTmpPath() {
		return tmpPath;
	}

	/**
	 * <p>Setter for the field <code>tmpPath</code>.</p>
	 *
	 * @param tmpPath a {@link java.lang.String} object.
	 */
	public void setTmpPath(String tmpPath) {
		this.tmpPath = tmpPath;
	}

	/**
	 * <p>Getter for the field <code>cfgPath</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCfgPath() {
		return cfgPath;
	}

	/**
	 * <p>Setter for the field <code>cfgPath</code>.</p>
	 *
	 * @param cfgPath a {@link java.lang.String} object.
	 */
	public void setCfgPath(String cfgPath) {
		this.cfgPath = cfgPath;
	}
	
	
}
