package fstop.util.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

/**
 * <p>ThreadsExec class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class ThreadsExec {
	Logger logger = Logger.getLogger(getClass());
	private List<Runnable> threadList = new ArrayList();
	boolean isStart = false;  
	private int poolsize = 0;
	
	/**
	 * <p>Constructor for ThreadsExec.</p>
	 */
	public ThreadsExec() {
		this(10);
	}
	
	/**
	 * <p>Constructor for ThreadsExec.</p>
	 *
	 * @param poolsize a int.
	 */
	public ThreadsExec(int poolsize) {
		this.poolsize = poolsize; 
	}
	
	/**
	 * <p>addThread.</p>
	 *
	 * @param t a {@link java.lang.Runnable} object.
	 */
	public void addThread(Runnable t) {
		threadList.add(t);
	}
/*	
	public boolean isDone() {
//		if(!isStart)
//			throw new RuntimeException("尚未啟動全部的 Thread .");
//		boolean result = true;
//		for(Runnable t : threadList) {
//			result = (result && (t.getState() == t.getState().TERMINATED));
//		}
		
		//service.shutdown();
		return service.isTerminated();
		//return result;
	}
*/	
	
	/**
	 * 執行所有的 runnable, 並等待所有的 runnable 都執行完畢
	 */
	public void startAll() {
		//final CountDownLatch begin = new CountDownLatch(1);
//		 結束的倒數鎖
		final CountDownLatch end = new CountDownLatch(threadList.size());
		
		ExecutorService exec = Executors.newFixedThreadPool(poolsize);

		for (final Runnable r : threadList) {
			Runnable run = new Runnable() {
				public void run() {
					
					try {
						//begin.await();
						
						r.run();
						
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
					}
					finally {
						end.countDown();
					}
				}
			};
			exec.execute(run);
		}
		
		//begin.countDown();
		try {
			end.await();
		} catch (InterruptedException e) {
		}
		// 退出 Thread Pool
		exec.shutdown();
		
		isStart = true;
	}
	
}
