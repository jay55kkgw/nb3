package fstop.util.thread;

import org.apache.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 執行Runnable物件, 但同時只能有 available 個 runnable 執行
 * 在execute 時, 若同時執行的 runnable 已達到 available 個, 則 execute 會等待, 等到某一個 runnable 執行結束
 * 請在使用完 ThreadSemaphore 後, 呼叫 shutdown()
 *
 * 使用方式 :
 * ThreadSemaphore threadSemaphore = new ThreadSemaphore(10);
 * Runnable run = new Runnable() {
 * 	public void run() {
 * 		System.out.println("do run");
 * 	}
 *
 * };
 *
 * threadSemaphore.execute(run);
 *
 * threadSemaphore.shutdown();   //等到所有的 Thread 都執行完畢, 才繼續往下做
 *
 * @author Jimmy
 * @version V1.0
 */
public class ThreadSemaphore {
	
	private Logger logger = Logger.getLogger(getClass());
	
	private final Semaphore sem;
	
	private final ExecutorService executor;
	
	/**
	 * max 同時間會有多少的 Thread 在執行中
	 *
	 * @param available a int.
	 */
	public ThreadSemaphore(int available) {
		sem = new Semaphore(available);
		executor = Executors.newFixedThreadPool(available);
	}
	
	/**
	 * <p>execute.</p>
	 *
	 * @param runnable a {@link java.lang.Runnable} object.
	 */
	public void execute(final Runnable runnable) {
		try {
			sem.acquire();
		}
		catch(InterruptedException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		
		try {
			Runnable run = new Runnable() {
				public void run() {
					try {
						runnable.run();
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
					}
					finally {
						sem.release();
					}
				}
			};			
			executor.execute(run);
		}
		finally {
			
		}
	}
	
	/**
	 * blocking shutdown
	 */
	public void shutdown() {
		executor.shutdown();
		
		while(true) {
			if(executor.isTerminated())
				break;
			try {
				Thread.sleep(2000L);
			}
			catch(Exception e) {}
		}
		
	}
}
