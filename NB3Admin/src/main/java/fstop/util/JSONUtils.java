package fstop.util;

import fstop.core.BeanUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p>JSONUtils class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
public class JSONUtils {
	/**
	 * <p>map2json.</p>
	 *
	 * @param m a {@link java.util.Map} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String map2json(Map m) {
		return JSONObject.fromObject(m).toString();
	}
	
	/**
	 * <p>json2map.</p>
	 *
	 * @param json a {@link java.lang.String} object.
	 * @return a {@link java.util.Map} object.
	 */
	public static Map json2map(String json) {
		return JSONObject.fromObject(json);
	}
	
	/**
	 * <p>toJson.</p>
	 *
	 * @param o a {@link java.lang.Object} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String toJson(Object o) {
		if(o instanceof List)
			return JSONArray.fromObject(o).toString(); 
		else if(o instanceof Map)
			return JSONObject.fromObject(o).toString();
		else {
			try {
				Map m = BeanUtils.describe(o);
				return JSONObject.fromObject(m).toString();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return "";
	}
	
	/**
	 * 產生JSON字串(以Html替代符號替換JSON字串內的單引號，以防網頁上以單引號括住的JSON字串被誤判)
	 *
	 * @param o a {@link java.lang.Object} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String toHtmlJson(Object o) {
		if(o instanceof List)
			return JSONArray.fromObject(o).toString().replaceAll("'", "&#039"); 
		else if(o instanceof Map)
			return JSONObject.fromObject(o).toString().replaceAll("'", "&#039");
		else {
			try {
				Map m = BeanUtils.describe(o);
				return JSONObject.fromObject(m).toString().replaceAll("'", "&#039");
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return "";
	}

	/**
	 * <p>toList.</p>
	 *
	 * @param json a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public static List toList(String json) {
		return new ArrayList(Arrays.asList(JSONArray.fromObject(json).toArray()));
		
	}
	/**
	 * <p>checkjsonstring.</p>
	 *
	 * @param json a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	public static boolean checkjsonstring(String json) {
		try{
		    JSONObject jsonObject = JSONObject.fromObject( json ); 
		    return true;
		}catch(Exception e){
		    return false;
		}		
	}
}
