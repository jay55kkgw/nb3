package fstop.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;

/**
 * Property Files Utility Class
 */
public class PropertyUtils {

    /**
     * 取得 Resrouce 檔案內容
     * @param path      resource 檔案名稱<ex>foler/xxx.properties
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public String getResourceAsString(String path) throws FileNotFoundException, IOException {
		ClassLoader classLoader = getClass().getClassLoader();

		URL resource = classLoader.getResource(path);
		String url = "";
        if (resource == null) {
            throw new IllegalArgumentException(".sql file not found!");
        } else {
			//Start : 20191113-Danny-處理中文百分比編碼
			try {
				// 進行 URL 百分比解碼
				url = URLDecoder.decode(resource.getFile(), "UTF-8");
			} catch (Exception e) {
			}	
			//END : 20191113-Danny-處理中文百分比編碼  
		}
		StringBuilder sb = new StringBuilder();
		//20191120-Danny-Incorrect Permission Assignment For Critical Resources\路徑 1:
		try (FileReader reader = new FileReader(url); BufferedReader br = new BufferedReader(reader)) {
			String line;
			//20191118-Danny-Second Order SQL Injection\路徑 8:
			while ((line = br.readLine()) != null) {
				line = (String)Sanitizer.sanitize(line.replace("'", "@"));
				sb.append(line.replace("@", "'"));
			}
		}
		return sb.toString();
	}
}