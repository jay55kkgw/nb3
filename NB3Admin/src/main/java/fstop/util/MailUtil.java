package fstop.util;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 *
 * @version 1.0
 */

import fstop.exception.SendMailException;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

public class MailUtil {
	private String host = "";

	private String port = "";

	private String user = "";

	/**
	 * 請使用台語發音
	 */
	private String soci = "";
	
	/** Constant <code>NullCC="null"</code> */
	public final static String NullCC = null;

	/**
	 * <p>Setter for the field <code>host</code>.</p>
	 *
	 * @param host a {@link java.lang.String} object.
	 * @param port a {@link java.lang.String} object.
	 */
	public void setHost(String host, String port) {
		this.host = host;
		this.port = port;
	}

	/**
	 * <p>setAccount.</p>
	 *
	 * @param user a {@link java.lang.String} object.
	 * @param soci a {@link java.lang.String} object.
	 */
	public void setAccount(String user, String soci) {
		this.user = user;
		this.soci = soci;
	}

	/**
	 * <p>sendAuth.</p>
	 *
	 * @param isAuth a {@link java.lang.String} object.
	 * @param from a {@link java.lang.String} object.
	 * @param to a {@link java.lang.String} object.
	 * @param subject a {@link java.lang.String} object.
	 * @param htmContent a {@link java.lang.String} object.
	 */
	public void sendAuth(String isAuth, String from, String to, String subject,
			String htmContent) {
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", host); // 指定SMTP服務器
		props.put("mail.smtp.port", port); // 指定SMTP服務器

		props.put("mail.smtp.auth", isAuth); // 指定是否需要SMTP驗證
		Transport transport = null;
		try {
			Session mailSession = Session.getDefaultInstance(props);

			mailSession.setDebug(false); // 是否在控制台顯示debug信息

			Message message = prepareMessageContent(from, to, MailUtil.NullCC, subject, htmContent, mailSession, new Properties());

			transport = mailSession.getTransport();
			transport.connect(host, user, soci);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} catch (Exception e) {
			throw new SendMailException("Email 發送錯誤.", e);
		}

	}
	
	/**
	 * <p>sendHtml.</p>
	 *
	 * @param from a {@link java.lang.String} object.
	 * @param to a {@link java.lang.String} object.
	 * @param subject a {@link java.lang.String} object.
	 * @param htmContent a {@link java.lang.String} object.
	 */
	public void sendHtml(String from, String to, String subject,
			String htmContent) {
		sendHtml(from, to, MailUtil.NullCC, subject, htmContent, new Properties());
	}
	
	/**
	 * <p>sendHtml.</p>
	 *
	 * @param from a {@link java.lang.String} object.
	 * @param to a {@link java.lang.String} object.
	 * @param cc a {@link java.lang.String} object.
	 * @param subject a {@link java.lang.String} object.
	 * @param htmContent a {@link java.lang.String} object.
	 * @param props a {@link java.util.Properties} object.
	 */
	public void sendHtml(String from, String to, String cc, String subject,
			String htmContent, Properties props) {
		props.setProperty("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", host); // 指定SMTP服務器
		props.put("mail.smtp.port", port); // 指定SMTP服務器
		
		if(!props.containsKey("mail.smtp.auth"))
			props.put("mail.smtp.auth", "false"); // 指定是否需要SMTP驗證
		
		Transport transport = null;
		try {
			Session mailSession = Session.getDefaultInstance(props);

			mailSession.setDebug(false); // 是否在控制台顯示debug信息

			Message message = prepareMessageContent(from, to, cc, subject, htmContent, mailSession, props);
			transport = mailSession.getTransport();
			transport.connect();
			transport.sendMessage(message,  message.getAllRecipients());	
			
		} catch (Exception e) {
			throw new SendMailException("Email 發送錯誤.", e);
		}
		finally {
			try {
				transport.close();
			}
			catch(Exception e) {
			}
		}

	}

	private Message prepareMessageContent(String from, String to, String cc, String subject, String htmContent, Session mailSession, Properties props) throws MessagingException, AddressException, UnsupportedEncodingException {
		Message message = new MimeMessage(mailSession);
		//message.setFrom(new InternetAddress(from)); // 發件人
//		try {
//			from = new String(from.getBytes("MS950"), "8859_1");
//		}
//		catch(Exception e) {}
		InternetAddress ittr = null;
		if(props.containsKey("mail.from.sendername")) {
			ittr = new InternetAddress(from, (String)props.get("mail.from.sendername"), "MS950");
		}
		else {
			ittr = new InternetAddress(from);
		}
		message.setFrom(ittr);
		if(cc != null) {
			String[] ts = cc.split(",");
			for (int i = 0; ts != null && i < ts.length; i++) {
				message.addRecipient(Message.RecipientType.CC,
						new InternetAddress(ts[i])); // 密件收件人
			}
		}
		
		if (to != null) {
			String[] ts = to.split(",");
			for (int i = 0; ts != null && i < ts.length; i++) {
				message.addRecipient(Message.RecipientType.TO,
						new InternetAddress(ts[i])); // 收件人
			}
		}
		
		
		try {
			subject = javax.mail.internet.MimeUtility.encodeText(subject,
		            "big5", null);
		} catch (UnsupportedEncodingException e) {
		    //
		}
		message.setSubject(subject); // 郵件主題
		
		//message.setHeader("Content-Disposition", "inline");
		//message.setHeader("Content-Type", "text/html; charset=utf-8");
		//message.setHeader("Content-Transfer-Encoding", "8bit");
		
		//message.setContent(htmContent, "text/html; charset=utf-8");
		message.setContent(htmContent, "text/html; charset=big5");
		return message;
	}

	/**
	 * <p>sendFile.</p>
	 *
	 * @param isAuth a {@link java.lang.String} object.
	 * @param from a {@link java.lang.String} object.
	 * @param to a {@link java.lang.String} object.
	 * @param subject a {@link java.lang.String} object.
	 * @param content a {@link java.lang.String} object.
	 * @param sendfile a {@link java.lang.String} object.
	 */
	public void sendFile(String isAuth, String from, String to, String subject,
			String content, String sendfile) {
		Properties props = new Properties();
		props.put("mail.smtp.host", host); // 指定SMTP服務器
		props.put("mail.smtp.port", port); // 指定SMTP服務器
		if (isAuth == null || isAuth.length() == 0
				|| "false".compareToIgnoreCase(isAuth) == 0) {
			sendFileByMail(props, from, to, "", "", subject, content, sendfile);
			return;
		}
		props.put("mail.smtp.auth", isAuth); // 指定是否需要SMTP驗證
		try {
			Session mailSession = Session.getDefaultInstance(props);

			mailSession.setDebug(false); // 是否在控制台顯示debug信息

			Message message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(from)); // 發件人

			if (to != null) {
				String[] ts = to.split(",");
				for (int i = 0; ts != null && i < ts.length; i++) {
					message.addRecipient(Message.RecipientType.TO,
							new InternetAddress(ts[i])); // 收件人
				}
			}

			message.setSubject(subject); // 郵件主題
			message.setText(content); // 郵件內容
			message.saveChanges();

			Transport transport = mailSession.getTransport("smtp");
			transport.connect(host, user, soci);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} catch (Exception e) {
			throw new SendMailException("Email 發送錯誤.", e);

		}

	}

	private void sendTextByMail(Properties pros, String from, String to,
			String cc, String bcc, String subject, String body) {
		try {
			Session session = Session.getInstance(pros, null);
			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress
					.parse(to));

			if (cc.length() > 0)
				message.setRecipients(Message.RecipientType.CC, InternetAddress
						.parse(cc));

			if (bcc.length() > 0)
				message.setRecipients(Message.RecipientType.BCC,
						InternetAddress.parse(bcc));

			message.setContent(body, "text/plain");
			message.setHeader("Content-Disposition", "inline");
			message.setHeader("Content-Type", "text/plain; charset=big5");
			message.setHeader("Content-Transfer-Encoding", "8bit");
			message.setHeader("Subject", subject);

			Transport.send(message);
		} catch (Exception e) {
			throw new SendMailException("Email 發送錯誤.", e);
		}
	}

	private void sendFileByMail(Properties pros, String from, String to,
			String cc, String bcc, String subject, String body, String sendfile) {
		try {
			Session session = Session.getInstance(pros, null);
			Message message = new MimeMessage(session);

			message.setFrom(new InternetAddress(from));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setRecipients(Message.RecipientType.BCC, InternetAddress
					.parse(bcc));
			message.setSubject(subject);

			BodyPart bodyPartMain = new MimeBodyPart();
			bodyPartMain.setText(body);
			BodyPart bodyPart = new MimeBodyPart();
			FileDataSource fileDataSource = new FileDataSource(sendfile);
			bodyPart.setDataHandler(new DataHandler(fileDataSource));
			bodyPart.setFileName(fileDataSource.getName());

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(bodyPartMain);
			multipart.addBodyPart(bodyPart);

			message.setContent(multipart);
			Transport.send(message);

		} catch (Exception e) {
			throw new SendMailException("Email 發送錯誤.", e);
		}

	}
}
