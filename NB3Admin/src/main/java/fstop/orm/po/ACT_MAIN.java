package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * 行銷主檔
 *
 * @author jinhanhuang
 * @version V1.0 20210413
 * 
 */
@Entity
@Table(name = "ACT_MAIN")
@Data   
public class ACT_MAIN implements Serializable {

    @Id
    private String ACT_TYPE;//行銷活動代號

    private String MAXCOUNT;//行銷次數最大次數(長度3)
    private String ACTDATE_FROM;//行銷起日(長度8)
    private String ACTDATE_TO;//行銷迄日(長度8)
    private String ACT_MSG;//行銷訊息(長度120)
    

}
