package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class NBIPIDENTITY implements Serializable {
	private char[] ADBRANCHID;
	private char[] ADBRANCHIP;
	
	public char[] getADBRANCHID() {
		return ADBRANCHID;
	}
	public void setADBRANCHID(char[] aDBRANCHID) {
		ADBRANCHID = aDBRANCHID;
	}
	public char[] getADBRANCHIP() {
		return ADBRANCHIP;
	}
	public void setADBRANCHIP(char[] aDBRANCHIP) {
		ADBRANCHIP = aDBRANCHIP;
	}
}
