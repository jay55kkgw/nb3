package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang.builder.ToStringBuilder;

import fstop.orm.validator.DateValidatorConstraint;

/**
 * <p>ADMHOLIDAY class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMHOLIDAY")
public class ADMHOLIDAY implements Serializable {
	@Id
	@NotBlank(message = "日期不可為空值")
	@DateValidatorConstraint()
	private String ADHOLIDAYID;

	private String ADREMARK;

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this)
				.append("adholidayid", getADHOLIDAYID()).toString();
	}

	/**
	 * <p>getADHOLIDAYID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADHOLIDAYID() {
		return ADHOLIDAYID;
	}

	/**
	 * <p>setADHOLIDAYID.</p>
	 *
	 * @param adholidayid a {@link java.lang.String} object.
	 */
	public void setADHOLIDAYID(String adholidayid) {
		ADHOLIDAYID = adholidayid;
	}

	/**
	 * <p>getADREMARK.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADREMARK() {
		return ADREMARK;
	}

	/**
	 * <p>setADREMARK.</p>
	 *
	 * @param adremark a {@link java.lang.String} object.
	 */
	public void setADREMARK(String adremark) {
		ADREMARK = adremark;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}

	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

}
