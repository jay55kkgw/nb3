package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

//分行/ATM/證券據點檔
/**
 * <p>ADMAPPBRANCH class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMAPPBRANCH")
public class ADMAPPBRANCH implements Serializable{
	
	private Integer BHID; // 據點ID

	private String BHUSERID = ""; // 使用者代號

	private String BHUSERNAME = ""; // 使用者名稱

	private String BHTXDATE = ""; // 交易日期

	private String BHTXTIME = ""; // 交易時間
	@NotBlank(message = "請選擇據點類型")
	private String BHADTYPE ="";		 //據點類型
	
	@NotBlank(message = "請輸入據點名稱")
	private String BHNAME ="";		     //據點名稱
	@NotBlank(message = "請選擇縣市別")
	private String BHCOUNTY ="";		 //縣市別
	
	private String BHREGION ="";		 //行政區
	@NotBlank(message = "請輸入地址")
	private String BHADDR ="";		     //地址
	@NotBlank(message = "請輸入緯度")
	private String BHLATITUDE ="";		 //緯度
	@NotBlank(message = "請輸入經度")
	private String BHLONGITUDE ="";	     //經度
	@NotBlank(message = "請輸入電話-國碼")
	private String BHTELCOUNTRY ="";	 //電話-國碼
	@NotBlank(message = "請輸入電話-區碼")
	private String BHTELREGION ="";	     //電話-區碼
	@NotBlank(message = "請輸入電話號碼")
	private String BHTEL ="";		     //電話號碼
	
	private String BHPICADD ="";		 //據點照片路徑
	
	private String BHPICDATA;		     //據點照片
	@NotBlank(message = "請輸入開始營業時間")
	private String BHSTIME ="";		     //開始營業時間
	@NotBlank(message = "請輸入結束營業時間")
	private String BHETIME ="";		     //結束營業時間
	/**
	 * <p>getBHID.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getBHID() {
		return BHID;
	}

	/**
	 * <p>setBHID.</p>
	 *
	 * @param bhid a {@link java.lang.Integer} object.
	 */
	public void setBHID(Integer bhid) {
		BHID = bhid;
	}

	/**
	 * <p>getBHUSERID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHUSERID() {
		return BHUSERID;
	}

	/**
	 * <p>setBHUSERID.</p>
	 *
	 * @param bhuserid a {@link java.lang.String} object.
	 */
	public void setBHUSERID(String bhuserid) {
		BHUSERID = bhuserid;
	}

	/**
	 * <p>getBHUSERNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHUSERNAME() {
		return BHUSERNAME;
	}

	/**
	 * <p>setBHUSERNAME.</p>
	 *
	 * @param bhusername a {@link java.lang.String} object.
	 */
	public void setBHUSERNAME(String bhusername) {
		BHUSERNAME = bhusername;
	}

	/**
	 * <p>getBHTXDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHTXDATE() {
		return BHTXDATE;
	}

	/**
	 * <p>setBHTXDATE.</p>
	 *
	 * @param bhtxdate a {@link java.lang.String} object.
	 */
	public void setBHTXDATE(String bhtxdate) {
		BHTXDATE = bhtxdate;
	}

	/**
	 * <p>getBHTXTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHTXTIME() {
		return BHTXTIME;
	}

	/**
	 * <p>setBHTXTIME.</p>
	 *
	 * @param bhtxtime a {@link java.lang.String} object.
	 */
	public void setBHTXTIME(String bhtxtime) {
		BHTXTIME = bhtxtime;
	}

	/**
	 * <p>getBHADTYPE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHADTYPE() {
		return BHADTYPE;
	}

	/**
	 * <p>setBHADTYPE.</p>
	 *
	 * @param bhadtype a {@link java.lang.String} object.
	 */
	public void setBHADTYPE(String bhadtype) {
		BHADTYPE = bhadtype;
	}

	/**
	 * <p>getBHNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHNAME() {
		return BHNAME;
	}

	/**
	 * <p>setBHNAME.</p>
	 *
	 * @param bhname a {@link java.lang.String} object.
	 */
	public void setBHNAME(String bhname) {
		BHNAME = bhname;
	}

	/**
	 * <p>getBHCOUNTY.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHCOUNTY() {
		return BHCOUNTY;
	}

	/**
	 * <p>setBHCOUNTY.</p>
	 *
	 * @param bhcounty a {@link java.lang.String} object.
	 */
	public void setBHCOUNTY(String bhcounty) {
		BHCOUNTY = bhcounty;
	}

	/**
	 * <p>getBHREGION.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHREGION() {
		return BHREGION;
	}

	/**
	 * <p>setBHREGION.</p>
	 *
	 * @param bhregion a {@link java.lang.String} object.
	 */
	public void setBHREGION(String bhregion) {
		BHREGION = bhregion;
	}

	/**
	 * <p>getBHADDR.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHADDR() {
		return BHADDR;
	}

	/**
	 * <p>setBHADDR.</p>
	 *
	 * @param bhaddr a {@link java.lang.String} object.
	 */
	public void setBHADDR(String bhaddr) {
		BHADDR = bhaddr;
	}

	/**
	 * <p>getBHLATITUDE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHLATITUDE() {
		return BHLATITUDE;
	}

	/**
	 * <p>setBHLATITUDE.</p>
	 *
	 * @param latitude a {@link java.lang.String} object.
	 */
	public void setBHLATITUDE(String latitude) {
		BHLATITUDE = latitude;
	}

	/**
	 * <p>getBHLONGITUDE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHLONGITUDE() {
		return BHLONGITUDE;
	}

	/**
	 * <p>setBHLONGITUDE.</p>
	 *
	 * @param longitude a {@link java.lang.String} object.
	 */
	public void setBHLONGITUDE(String longitude) {
		BHLONGITUDE = longitude;
	}

	/**
	 * <p>getBHTELCOUNTRY.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHTELCOUNTRY() {
		return BHTELCOUNTRY;
	}

	/**
	 * <p>setBHTELCOUNTRY.</p>
	 *
	 * @param country a {@link java.lang.String} object.
	 */
	public void setBHTELCOUNTRY(String country) {
		BHTELCOUNTRY = country;
	}

	/**
	 * <p>getBHTELREGION.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHTELREGION() {
		return BHTELREGION;
	}

	/**
	 * <p>setBHTELREGION.</p>
	 *
	 * @param region a {@link java.lang.String} object.
	 */
	public void setBHTELREGION(String region) {
		BHTELREGION = region;
	}

	/**
	 * <p>getBHTEL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHTEL() {
		return BHTEL;
	}

	/**
	 * <p>setBHTEL.</p>
	 *
	 * @param bhtel a {@link java.lang.String} object.
	 */
	public void setBHTEL(String bhtel) {
		BHTEL = bhtel;
	}

	/**
	 * <p>getBHPICADD.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHPICADD() {
		return BHPICADD;
	}

	/**
	 * <p>setBHPICADD.</p>
	 *
	 * @param bhpicadd a {@link java.lang.String} object.
	 */
	public void setBHPICADD(String bhpicadd) {
		BHPICADD = bhpicadd;
	}

	/**
	 * <p>getBHPICDATA.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHPICDATA() {
		return BHPICDATA;
	}

	/**
	 * <p>setBHPICDATA.</p>
	 *
	 * @param bhpicdata a {@link java.lang.String} object.
	 */
	public void setBHPICDATA(String bhpicdata) {
		BHPICDATA = bhpicdata;
	}

	/**
	 * <p>getBHSTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHSTIME() {
		return BHSTIME;
	}

	/**
	 * <p>setBHSTIME.</p>
	 *
	 * @param bhstime a {@link java.lang.String} object.
	 */
	public void setBHSTIME(String bhstime) {
		BHSTIME = bhstime;
	}

	/**
	 * <p>getBHETIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBHETIME() {
		return BHETIME;
	}

	/**
	 * <p>setBHETIME.</p>
	 *
	 * @param bhetime a {@link java.lang.String} object.
	 */
	public void setBHETIME(String bhetime) {
		BHETIME = bhetime;
	}

}
