package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>ADMBANK class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMBANK")
public class ADMBANK implements Serializable {

	@Id
	@NotBlank(message = "請輸入銀行代碼")
	private String ADBANKID;

	@NotBlank(message = "請輸入銀行繁中名稱")
	private String ADBANKNAME;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	@NotBlank(message = "請輸入銀行簡中名稱")
	private String ADBANKCHSNAME;

	@NotBlank(message = "請輸入銀行英文代碼")
	private String ADBANKENGNAME;

//	private Set TXNTWRECORDS;
//
//	private Set TXNFXRECORDS;
//
//	private Set TXNTRACCSETS;
//
//	private Set TXNFXSCHEDULES;
//
//	private Set TXNTWSCHEDULES;

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("adbankid", getADBANKID())
				.toString();
	}

	/**
	 * <p>getADBANKID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADBANKID() {
		return ADBANKID;
	}

	/**
	 * <p>setADBANKID.</p>
	 *
	 * @param adbankid a {@link java.lang.String} object.
	 */
	public void setADBANKID(String adbankid) {
		ADBANKID = adbankid;
	}

	/**
	 * <p>getADBANKNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADBANKNAME() {
		return ADBANKNAME;
	}

	/**
	 * <p>setADBANKNAME.</p>
	 *
	 * @param adbankname a {@link java.lang.String} object.
	 */
	public void setADBANKNAME(String adbankname) {
		ADBANKNAME = adbankname;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}

	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}
//
//	public Set getTXNFXRECORDS() {
//		return TXNFXRECORDS;
//	}
//
//	public void setTXNFXRECORDS(Set txnfxrecords) {
//		TXNFXRECORDS = txnfxrecords;
//	}
//
//	public Set getTXNFXSCHEDULES() {
//		return TXNFXSCHEDULES;
//	}
//
//	public void setTXNFXSCHEDULES(Set txnfxschedules) {
//		TXNFXSCHEDULES = txnfxschedules;
//	}
//
//	public Set getTXNTRACCSETS() {
//		return TXNTRACCSETS;
//	}
//
//	public void setTXNTRACCSETS(Set txntraccsets) {
//		TXNTRACCSETS = txntraccsets;
//	}
//
//	public Set getTXNTWRECORDS() {
//		return TXNTWRECORDS;
//	}
//
//	public void setTXNTWRECORDS(Set txntwrecords) {
//		TXNTWRECORDS = txntwrecords;
//	}
//
//	public Set getTXNTWSCHEDULES() {
//		return TXNTWSCHEDULES;
//	}
//
//	public void setTXNTWSCHEDULES(Set txntwschedules) {
//		TXNTWSCHEDULES = txntwschedules;
//	}

	public String getADBANKCHSNAME() {
		return ADBANKCHSNAME;
	}

	public void setADBANKCHSNAME(String aDBANKCHSNAME) {
		ADBANKCHSNAME = aDBANKCHSNAME;
	}

	public String getADBANKENGNAME() {
		return ADBANKENGNAME;
	}

	public void setADBANKENGNAME(String aDBANKENGNAME) {
		ADBANKENGNAME = aDBANKENGNAME;
	}

}
