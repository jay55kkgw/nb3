package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Since;

import org.apache.poi.util.IntList;

/**
 * ADMFLOWHISTORY 流程歷程 POJO
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMFLOWHISTORY")
public class ADMFLOWHISTORY implements Serializable {
    @Id    
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer ID;
    private String CASESN;
    private String ORGID;
    private String FLOWID;
    private String STEPID;
    private String STEPNAME;
    private String ACTIONOID;
    private String ACTIONUID;
    private String ACTIONUNAME;
    private String APPROVEFLAG;
    private String COMMENTS;
    @Since(1)
    private String FLOWDATA;
    private String CREATEDATE;
    private String CREATETIME;

    /**
     * <p>getID.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getID() {
        return ID;
    }

    /**
     * <p>setID.</p>
     *
     * @param iD a {@link java.lang.Integer} object.
     */
    public void setID(Integer iD) {
        ID = iD;
    }

    /**
     * <p>getCASESN.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCASESN() {
        return CASESN;
    }

    /**
     * <p>setCASESN.</p>
     *
     * @param cASESN a {@link java.lang.String} object.
     */
    public void setCASESN(String cASESN) {
        CASESN = cASESN;
    }

    /**
     * <p>getORGID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getORGID() {
        return ORGID;
    }

    /**
     * <p>setORGID.</p>
     *
     * @param oRGID a {@link java.lang.String} object.
     */
    public void setORGID(String oRGID) {
        ORGID = oRGID;
    }

    /**
     * <p>getFLOWID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFLOWID() {
        return FLOWID;
    }

    /**
     * <p>setFLOWID.</p>
     *
     * @param fLOWID a {@link java.lang.String} object.
     */
    public void setFLOWID(String fLOWID) {
        FLOWID = fLOWID;
    }

    /**
     * <p>getSTEPID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPID() {
        return STEPID;
    }

    /**
     * <p>setSTEPID.</p>
     *
     * @param sTEPID a {@link java.lang.String} object.
     */
    public void setSTEPID(String sTEPID) {
        STEPID = sTEPID;
    }

    /**
     * <p>getSTEPNAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPNAME() {
        return STEPNAME;
    }

    /**
     * <p>setSTEPNAME.</p>
     *
     * @param sTEPNAME a {@link java.lang.String} object.
     */
    public void setSTEPNAME(String sTEPNAME) {
        STEPNAME = sTEPNAME;
    }

    /**
     * <p>getACTIONOID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getACTIONOID() {
        return ACTIONOID;
    }

    /**
     * <p>setACTIONOID.</p>
     *
     * @param aCTIONOID a {@link java.lang.String} object.
     */
    public void setACTIONOID(String aCTIONOID) {
        ACTIONOID = aCTIONOID;
    }

    /**
     * <p>getACTIONUID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getACTIONUID() {
        return ACTIONUID;
    }

    /**
     * <p>setACTIONUID.</p>
     *
     * @param aCTIONUID a {@link java.lang.String} object.
     */
    public void setACTIONUID(String aCTIONUID) {
        ACTIONUID = aCTIONUID;
    }

    /**
     * <p>getACTIONUNAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getACTIONUNAME() {
        return ACTIONUNAME;
    }

    /**
     * <p>setACTIONUNAME.</p>
     *
     * @param aCTIONUNAME a {@link java.lang.String} object.
     */
    public void setACTIONUNAME(String aCTIONUNAME) {
        ACTIONUNAME = aCTIONUNAME;
    }

    /**
     * <p>getAPPROVEFLAG.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAPPROVEFLAG() {
        return APPROVEFLAG;
    }

    /**
     * <p>setAPPROVEFLAG.</p>
     *
     * @param aPPROVEFLAG a {@link java.lang.String} object.
     */
    public void setAPPROVEFLAG(String aPPROVEFLAG) {
        APPROVEFLAG = aPPROVEFLAG;
    }

    /**
     * <p>getCOMMENTS.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCOMMENTS() {
        return COMMENTS;
    }

    /**
     * <p>setCOMMENTS.</p>
     *
     * @param cOMMENTS a {@link java.lang.String} object.
     */
    public void setCOMMENTS(String cOMMENTS) {
        COMMENTS = cOMMENTS;
    }

    /**
     * <p>getFLOWDATA.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFLOWDATA() {
        return FLOWDATA;
    }

    /**
     * <p>setFLOWDATA.</p>
     *
     * @param fLOWDATA a {@link java.lang.String} object.
     */
    public void setFLOWDATA(String fLOWDATA) {
        FLOWDATA = fLOWDATA;
    }

    /**
     * <p>getCREATEDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCREATEDATE() {
        return CREATEDATE;
    }

    /**
     * <p>setCREATEDATE.</p>
     *
     * @param cREATEDATE a {@link java.lang.String} object.
     */
    public void setCREATEDATE(String cREATEDATE) {
        CREATEDATE = cREATEDATE;
    }

    /**
     * <p>getCREATETIME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCREATETIME() {
        return CREATETIME;
    }

    /**
     * <p>setCREATETIME.</p>
     *
     * @param cREATETIME a {@link java.lang.String} object.
     */
    public void setCREATETIME(String cREATETIME) {
        CREATETIME = cREATETIME;
    }
    
    
}
