package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * <p>ADMMBSTATUS class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMNBSTATUS")
public class ADMNBSTATUS implements Serializable{

	@Id
	private String ADNBSTATUSID;	//網銀應用系統狀態檔ID	
	
	private String ADNBSTATUS;		//網銀應用系統狀態	
	
	private String LASTUSER;		//最後修改者	
	
	private String LASTDATE;		//最後修改日	
	
	private String LASTTIME;		//最後修改時間	

	public String getADNBSTATUSID() {
		return ADNBSTATUSID;
	}

	public void setADNBSTATUSID(String aDNBSTATUSID) {
		ADNBSTATUSID = aDNBSTATUSID;
	}

	public String getADNBSTATUS() {
		return ADNBSTATUS;
	}

	public void setADNBSTATUS(String aDNBSTATUS) {
		ADNBSTATUS = aDNBSTATUS;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lASTUSER) {
		LASTUSER = lASTUSER;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	
	
}
