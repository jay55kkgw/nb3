package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>ADMDAYHHREPORT class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMDAYHHREPORT")
public class ADMDAYHHREPORT implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -7441519938756788684L;
	
	@Id
	private String CMYYYYMMDD;
	private String CMHH;
	private String ADOPID;
	private String ADUSERTYPE;
	private String ADAGREEF;
	private String ADBKFLAG;
	private String ADLEVEL;
	private String ADTXSTATUS;
	private String ADTXCODE;
	private String ADCOUNT;
	private Long ADAMOUNT;
	private String LOGINTYPE;

	public String getCMYYYYMMDD() {
		return CMYYYYMMDD;
	}

	public void setCMYYYYMMDD(String cMYYYYMMDD) {
		CMYYYYMMDD = cMYYYYMMDD;
	}

	public String getCMHH() {
		return CMHH;
	}

	public void setCMHH(String cMHH) {
		CMHH = cMHH;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getADUSERTYPE() {
		return ADUSERTYPE;
	}

	public void setADUSERTYPE(String aDUSERTYPE) {
		ADUSERTYPE = aDUSERTYPE;
	}

	public String getADAGREEF() {
		return ADAGREEF;
	}

	public void setADAGREEF(String aDAGREEF) {
		ADAGREEF = aDAGREEF;
	}

	public String getADBKFLAG() {
		return ADBKFLAG;
	}

	public void setADBKFLAG(String aDBKFLAG) {
		ADBKFLAG = aDBKFLAG;
	}

	public String getADLEVEL() {
		return ADLEVEL;
	}

	public void setADLEVEL(String aDLEVEL) {
		ADLEVEL = aDLEVEL;
	}

	public String getADTXSTATUS() {
		return ADTXSTATUS;
	}

	public void setADTXSTATUS(String aDTXSTATUS) {
		ADTXSTATUS = aDTXSTATUS;
	}

	public String getADTXCODE() {
		return ADTXCODE;
	}

	public void setADTXCODE(String aDTXCODE) {
		ADTXCODE = aDTXCODE;
	}

	public String getADCOUNT() {
		return ADCOUNT;
	}

	public void setADCOUNT(String aDCOUNT) {
		ADCOUNT = aDCOUNT;
	}

	public Long getADAMOUNT() {
		return ADAMOUNT;
	}

	public void setADAMOUNT(Long aDAMOUNT) {
		ADAMOUNT = aDAMOUNT;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}
	

}
