package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name = "IDGATEHISTORY")
@Data
public class IDGATEHISTORY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8978724485758893029L;
	@Id
	private String ID;
	private String CUSIDN;
	private String IDGATEID;
	private String DEVICEBRAND;
	private String BINDSTATUS;
	private String BINDDATE ;
	private String BINDTIME;
	private String ADOPID;
	private String STATUS;
	private String TRANDATE;
	private String TRANTIME;
	private String LOGINTYPE;
	
	// UI輸入的參數
	@Transient
	private String SDate;
	@Transient
	private String EDate;
	@Transient
	private String ADOPNAME;
	
	public IDGATEHISTORY() {
		super();
	}
	
	//HQL查詢用
	public IDGATEHISTORY(String iD, String cUSIDN, String iDGATEID, String dEVICEBRAND, String bINDSTATUS, String bINDDATE,
			String bINDTIME, String aDOPID, String sTATUS, String tRANDATE, String tRANTIME, String lOGINTYPE,String aDOPNAME) {
		super();
		ID = iD;
		CUSIDN = cUSIDN;
		IDGATEID = iDGATEID;
		DEVICEBRAND = dEVICEBRAND;
		BINDSTATUS = bINDSTATUS;
		BINDDATE = bINDDATE;
		BINDTIME = bINDTIME;
		ADOPID = aDOPID;
		STATUS = sTATUS;
		TRANDATE = tRANDATE;
		TRANTIME = tRANTIME;
		LOGINTYPE = lOGINTYPE;
		ADOPNAME = aDOPNAME;
	}
	
}
