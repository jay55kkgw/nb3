package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "ADMLOGINACLIP")
public class ADMLOGINACLIP implements Serializable {
	@Id
    private String IP = "";
	private String LASTUSER;
    private String LASTDATE;
    private String LASTTIME;
}
