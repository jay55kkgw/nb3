package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * <p>ADMMBSTATUS class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMMBSTATUS")
public class ADMMBSTATUS implements Serializable{

	private String ADMBSTATUSID;	//行動應用系統狀態檔ID	
	
	private String ADMBSTATUS;	//行動銀行應用系統狀態	
	
	private String LASTUSER;	//最後修改者	
	
	private String LASTDATE;	//最後修改日	
	
	private String LASTTIME;	//最後修改時間	
    /**
     * <p>getADMBSTATUSID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    @Id
	public String getADMBSTATUSID() {
		return ADMBSTATUSID;
	}

	/**
	 * <p>setADMBSTATUSID.</p>
	 *
	 * @param admbstatusid a {@link java.lang.String} object.
	 */
	public void setADMBSTATUSID(String admbstatusid) {
		ADMBSTATUSID = admbstatusid;
	}

	/**
	 * <p>getADMBSTATUS.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMBSTATUS() {
		return ADMBSTATUS;
	}

	/**
	 * <p>setADMBSTATUS.</p>
	 *
	 * @param admbstatus a {@link java.lang.String} object.
	 */
	public void setADMBSTATUS(String admbstatus) {
		ADMBSTATUS = admbstatus;
	}

	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}

	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

}
