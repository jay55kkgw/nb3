package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Since;

/**
 * 上傳附件暫存檔案
 *
 * @author chiensj
 * @version V1.0
 * 20191121-Danny-處理Byte[]欄位檢核時會錯誤排除
 */
@Entity
@Table(name = "ADMUPLOADTMP")
public class ADMUPLOADTMP implements Serializable {
    @Id
    private String ID;
    private String PURPOSE;
    @Since(1)
    private String FILENAME;
    @Since(1)
    private byte[] FILECONTENT;
    private String CREATEDATE;

    /**
     * <p>getID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getID() {
        return ID;
    }

    /**
     * <p>setID.</p>
     *
     * @param iD a {@link java.lang.String} object.
     */
    public void setID(String iD) {
        ID = iD;
    }

    /**
     * <p>getPURPOSE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPURPOSE() {
        return PURPOSE;
    }

    /**
     * <p>setPURPOSE.</p>
     *
     * @param pURPOSE a {@link java.lang.String} object.
     */
    public void setPURPOSE(String pURPOSE) {
        PURPOSE = pURPOSE;
    }

    /**
     * <p>getFILENAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFILENAME() {
        return FILENAME;
    }

    /**
     * <p>setFILENAME.</p>
     *
     * @param fILENAME a {@link java.lang.String} object.
     */
    public void setFILENAME(String fILENAME) {
        FILENAME = fILENAME;
    }

    /**
     * <p>getFILECONTENT.</p>
     *
     * @return an array of {@link byte} objects.
     */
    public byte[] getFILECONTENT() {
        return FILECONTENT;
    }

    /**
     * <p>setFILECONTENT.</p>
     *
     * @param fILECONTENT an array of {@link byte} objects.
     */
    public void setFILECONTENT(byte[] fILECONTENT) {
        FILECONTENT = fILECONTENT;
    }

    /**
     * <p>getCREATEDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCREATEDATE() {
        return CREATEDATE;
    }

    /**
     * <p>setCREATEDATE.</p>
     *
     * @param cREATEDATE a {@link java.lang.String} object.
     */
    public void setCREATEDATE(String cREATEDATE) {
        CREATEDATE = cREATEDATE;
    }

    

    
}
