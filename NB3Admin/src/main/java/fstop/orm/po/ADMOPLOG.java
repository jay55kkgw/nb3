package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Since;

/**
 * 系統交易記錄檔
 */
@Entity
@Table(name = "ADMOPLOG")
public class ADMOPLOG implements Serializable { 
    /**
     * 交易編號：交易時間(年月日時分秒)14位+操作者代號10位
     */
    @Id
    private String ADTXNO;

    /**
     * 操作者代號
     */
    private String ADUSERID;

    /**
     * 操作者IP位址
     */
    private String ADUSERIP;

    /**
     * 交易日期
     */
    private String ADTXDATE;

    /**
     * 交易時間
     */
    private String ADTXTIME;

    /**
     * 管理功能項目
     */
    private String ADTXITEM;

    /**
     * 還原頁面名稱
     */
    private String ADLOGGING;

    /**
     * 操作功能ID：SysOp.ADOPID
     */
    private String ADOPID;

    /**
     * 操作項目：1.新增、2.修改、3.刪除、4.查詢
     */
    private String ADOPITEM;

    /**
     * 變更前內容：PO JSON
     */
    @Since(1)
    private String ADBCHCON;

    /**
     * 變更後內容：PO JSON
     */
    @Since(1)
    private String ADACHCON;

    /**
     * 交易錯誤代碼
     */
    private String ADEXCODE;

    public String getADTXNO() {
        return ADTXNO;
    }

    public void setADTXNO(String aDTXNO) {
        ADTXNO = aDTXNO;
    }

    public String getADUSERID() {
        return ADUSERID;
    }

    public void setADUSERID(String aDUSERID) {
        ADUSERID = aDUSERID;
    }

    public String getADUSERIP() {
        return ADUSERIP;
    }

    public void setADUSERIP(String aDUSERIP) {
        ADUSERIP = aDUSERIP;
    }

    public String getADTXDATE() {
        return ADTXDATE;
    }

    public void setADTXDATE(String aDTXDATE) {
        ADTXDATE = aDTXDATE;
    }

    public String getADTXTIME() {
        return ADTXTIME;
    }

    public void setADTXTIME(String aDTXTIME) {
        ADTXTIME = aDTXTIME;
    }

    public String getADTXITEM() {
        return ADTXITEM;
    }

    public void setADTXITEM(String aDTXITEM) {
        ADTXITEM = aDTXITEM;
    }

    public String getADLOGGING() {
        return ADLOGGING;
    }

    public void setADLOGGING(String aDLOGGING) {
        ADLOGGING = aDLOGGING;
    }

    public String getADOPID() {
        return ADOPID;
    }

    public void setADOPID(String aDOPID) {
        ADOPID = aDOPID;
    }

    public String getADOPITEM() {
        return ADOPITEM;
    }

    public void setADOPITEM(String aDOPITEM) {
        ADOPITEM = aDOPITEM;
    }

    public String getADBCHCON() {
        return ADBCHCON;
    }

    public void setADBCHCON(String aDBCHCON) {
        ADBCHCON = aDBCHCON;
    }

    public String getADACHCON() {
        return ADACHCON;
    }

    public void setADACHCON(String aDACHCON) {
        ADACHCON = aDACHCON;
    }

    public String getADEXCODE() {
        return ADEXCODE;
    }

    public void setADEXCODE(String aDEXCODE) {
        ADEXCODE = aDEXCODE;
    }

}