package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.annotations.Since;

import javax.persistence.Id;


/**
 * <p>
 * TXNFXSCHPAY class.
 * </p>
 *
 * @author Alison
 * @version V1.0
 */
@Entity
@Table(name = "TXNFXSCHPAY")
public class TXNFXSCHPAY implements Serializable {

	private Integer FXSCHID;

	private String ADOPID;

	private String FXUSERID;

	private String FXTXTYPE;

	private String FXPERMTDATE;

	private String FXFDATE;

	private String FXTDATE;

	private String FXWDAC;

	private String FXWDCURR;

	private String FXWDAMT;

	private String FXSVBH;

	private String FXSVAC;

	private String FXSVCURR;

	private String FXSVAMT;

	private String FXTXMEMO;

	private String FXTXMAILS;

	private String FXTXMAILMEMO;

	private String FXTXCODE;

	private String FXSDATE;

	private String FXSTIME;

	private String FXTXSTATUS;

	private String XMLCA;

	private String XMLCN;

	private String MAC;

	@Since(1)
	private String FXTXINFO;

	private String LASTDATE;
	
	private String LASTTIME;

	private String FXSCHNO;

	private String LOGINTYPE;

	/**
	 * <p>getFXSCHID.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	@Id
	public Integer getFXSCHID() {
		return FXSCHID;
	}

	/**
	 * <p>setFXSCHID.</p>
	 *
	 * @param fxschid a {@link java.lang.Integer} object.
	 */
	public void setFXSCHID(Integer fxschid) {
		FXSCHID = fxschid;
	}

	/**
	 * <p>
	 * getADOPID.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADOPID() {
		return ADOPID;
	}

	/**
	 * <p>
	 * setADOPID.
	 * </p>
	 *
	 * @param adopid a {@link java.lang.String} object.
	 */
	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	/**
	 * <p>
	 * getFXUSERID.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXUSERID() {
		return FXUSERID;
	}

	/**
	 * <p>
	 * setFXUSERID.
	 * </p>
	 *
	 * @param fxuserid a {@link java.lang.String} object.
	 */
	public void setFXUSERID(String fxuserid) {
		FXUSERID = fxuserid;
	}

	/**
	 * <p>
	 * getFXTXTYPE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXTYPE() {
		return FXTXTYPE;
	}

	/**
	 * <p>
	 * setFXTXTYPE.
	 * </p>
	 *
	 * @param fxtxtype a {@link java.lang.String} object.
	 */
	public void setFXTXTYPE(String fxtxtype) {
		FXTXTYPE = fxtxtype;
	}

	/**
	 * <p>
	 * getFXPERMTDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXPERMTDATE() {
		return FXPERMTDATE;
	}

	/**
	 * <p>
	 * setFXPERMTDATE.
	 * </p>
	 *
	 * @param fxpermtdate a {@link java.lang.String} object.
	 */
	public void setFXPERMTDATE(String fxpermtdate) {
		FXPERMTDATE = fxpermtdate;
	}

	/**
	 * <p>
	 * getFXFDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXFDATE() {
		return FXFDATE;
	}

	/**
	 * <p>
	 * setFXFDATE.
	 * </p>
	 *
	 * @param fxfdate a {@link java.lang.String} object.
	 */
	public void setFXFDATE(String fxfdate) {
		FXFDATE = fxfdate;
	}

	/**
	 * <p>
	 * getFXTDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTDATE() {
		return FXTDATE;
	}

	/**
	 * <p>
	 * setFXTDATE.
	 * </p>
	 *
	 * @param fxtdate a {@link java.lang.String} object.
	 */
	public void setFXTDATE(String fxtdate) {
		FXTDATE = fxtdate;
	}

	

	/**
	 * <p>
	 * getFXWDAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXWDAC() {
		return FXWDAC;
	}

		/**
	 * <p>
	 * setFXWDAC.
	 * </p>
	 *
	 * @param fxwdac a {@link java.lang.String} object.
	 */
	public void setFXWDAC(String fxwdac) {
		FXWDAC = fxwdac;
	}
	


	/**
	 * <p>
	 * getFXWDCURR.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXWDCURR() {
		return FXWDCURR;
	}

	/**
	 * <p>
	 * setFXWDCURR.
	 * </p>
	 *
	 * @param fxwdcurr a {@link java.lang.String} object.
	 */
	public void setFXWDCURR(String fxwdcurr) {
		FXWDCURR = fxwdcurr;
	}

	/**
	 * <p>
	 * getFXWDAMT.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXWDAMT() {
		return FXWDAMT;
	}

	/**
	 * <p>
	 * setFXWDAMT.
	 * </p>
	 *
	 * @param fxwdamt a {@link java.lang.String} object.
	 */
	public void setFXWDAMT(String fxwdamt) {
		FXWDAMT = fxwdamt;
	}

	/**
	 * <p>
	 * getFXSVBH.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXSVBH() {
		return FXSVBH;
	}

	/**
	 * <p>
	 * setFXSVBH.
	 * </p>
	 *
	 * @param fxsvbh a {@link java.lang.String} object.
	 */
	public void setFXSVBH(String fxsvbh) {
		FXSVBH = fxsvbh;
	}

	/**
	 * <p>
	 * getFXSVAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXSVAC() {
		return FXSVAC;
	}

	/**
	 * <p>
	 * setFXSVAC.
	 * </p>
	 *
	 * @param fxsvac a {@link java.lang.String} object.
	 */
	public void setFXSVAC(String fxsvac) {
		FXSVAC = fxsvac;
	}

	/**
	 * <p>
	 * getFXSVCURR.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXSVCURR() {
		return FXSVCURR;
	}

	/**
	 * <p>
	 * setFXSVCURR.
	 * </p>
	 *
	 * @param fxsvcurr a {@link java.lang.String} object.
	 */
	public void setFXSVCURR(String fxsvcurr) {
		FXSVCURR = fxsvcurr;
	}

	/**
	 * <p>
	 * getFXSVAMT.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXSVAMT() {
		return FXSVAMT;
	}

	/**
	 * <p>
	 * setFXSVAMT.
	 * </p>
	 *
	 * @param fxsvamt a {@link java.lang.String} object.
	 */
	public void setFXSVAMT(String fxsvamt) {
		FXSVAMT = fxsvamt;
	}

	/**
	 * <p>
	 * getFXTXMEMO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXMEMO() {
		return FXTXMEMO;
	}

	/**
	 * <p>
	 * setFXTXMEMO.
	 * </p>
	 *
	 * @param fxtxmemo a {@link java.lang.String} object.
	 */
	public void setFXTXMEMO(String fxtxmemo) {
		FXTXMEMO = fxtxmemo;
	}

	/**
	 * <p>
	 * getFXTXMAILS.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXMAILS() {
		return FXTXMAILS;
	}

	/**
	 * <p>
	 * setFXTXMAILS.
	 * </p>
	 *
	 * @param fxtxmails a {@link java.lang.String} object.
	 */
	public void setFXTXMAILS(String fxtxmails) {
		FXTXMAILS = fxtxmails;
	}

	/**
	 * <p>
	 * getFXTXMAILMEMO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXMAILMEMO() {
		return FXTXMAILMEMO;
	}

	/**
	 * <p>
	 * setFXTXMAILMEMO.
	 * </p>
	 *
	 * @param fxtxmailmemo a {@link java.lang.String} object.
	 */
	public void setFXTXMAILMEMO(String fxtxmailmemo) {
		FXTXMAILMEMO = fxtxmailmemo;
	}

	/**
	 * <p>
	 * getFXTXCODE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXCODE() {
		return FXTXCODE;
	}

	/**
	 * <p>
	 * setFXTXCODE.
	 * </p>
	 *
	 * @param fxtxcode a {@link java.lang.String} object.
	 */
	public void setFXTXCODE(String fxtxcode) {
		FXTXCODE = fxtxcode;
	}

	/**
	 * <p>
	 * getFXSDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXSDATE() {
		return FXSDATE;
	}

	/**
	 * <p>
	 * setFXSDATE.
	 * </p>
	 *
	 * @param fxsdate a {@link java.lang.String} object.
	 */
	public void setFXSDATE(String fxsdate) {
		FXSDATE = fxsdate;
	}

	/**
	 * <p>
	 * getFXSTIME.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXSTIME() {
		return FXSTIME;
	}

	/**
	 * <p>
	 * setFXSTIME.
	 * </p>
	 *
	 * @param fxstime a {@link java.lang.String} object.
	 */
	public void setFXSTIME(String fxstime) {
		FXSTIME = fxstime;
	}

	/**
	 * <p>
	 * getFXTXSTATUS.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXSTATUS() {
		return FXTXSTATUS;
	}

	/**
	 * <p>
	 * setFXTXSTATUS.
	 * </p>
	 *
	 * @param fxtxstatus a {@link java.lang.String} object.
	 */
	public void setFXTXSTATUS(String fxtxstatus) {
		FXTXSTATUS = fxtxstatus;
	}

	/**
	 * <p>
	 * getXMLCA.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getXMLCA() {
		return XMLCA;
	}

	/**
	 * <p>
	 * setXMLCA.
	 * </p>
	 *
	 * @param xmlca a {@link java.lang.String} object.
	 */
	public void setXMLCA(String xmlca) {
		XMLCA = xmlca;
	}

	/**
	 * <p>
	 * getXMLCN.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getXMLCN() {
		return XMLCN;
	}

	/**
	 * <p>
	 * setXMLCN.
	 * </p>
	 *
	 * @param xmlcn a {@link java.lang.String} object.
	 */
	public void setXMLCN(String xmlcn) {
		XMLCN = xmlcn;
	}

	/**
	 * <p>
	 * getMAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMAC() {
		return MAC;
	}

	/**
	 * <p>
	 * setMAC.
	 * </p>
	 *
	 * @param mac a {@link java.lang.String} object.
	 */
	public void setMAC(String mac) {
		MAC = mac;
	}

	/**
	 * <p>
	 * getFXTXINFO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXINFO() {
		return FXTXINFO;
	}

	/**
	 * <p>
	 * setFXTXINFO.
	 * </p>
	 *
	 * @param fxtxinfo a {@link java.lang.String} object.
	 */
	public void setFXTXINFO(String fxtxinfo) {
		FXTXINFO = fxtxinfo;
	}

	/**
	 * <p>
	 * getLASTDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>
	 * setLASTDATE.
	 * </p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}
	/**
	 * <p>
	 * getLASTTIME.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>
	 * setLASTTIME.
	 * </p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}
		/**
	 * <p>
	 * getFXSCHNO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXSCHNO() {
		return FXSCHNO;
	}

	/**
	 * <p>
	 * setFXSCHNO.
	 * </p>
	 *
	 * @param fxschno a {@link java.lang.String} object.
	 */
	public void setFXSCHNO(String fxschno) {
		FXSCHNO = fxschno;
	}
		/**
	 * <p>
	 * getLOGINTYPE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	/**
	 * <p>
	 * setLOGINTYPE.
	 * </p>
	 *
	 * @param logintype a {@link java.lang.String} object.
	 */
	public void setLOGINTYPE(String logintype) {
		LOGINTYPE = logintype;
	}
}
