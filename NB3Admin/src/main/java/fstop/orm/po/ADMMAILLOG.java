package fstop.orm.po;

import java.io.Serializable;
import java.sql.Clob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.sound.sampled.Clip;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>ADMMAILLOG class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMMAILLOG")
public class ADMMAILLOG implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADMAILLOGID;

	private String ADBATCHNO = "";

	private String ADUSERID = "";

	private String ADUSERNAME = "";

	private String ADMAILACNO = ""; // 批次作業代號 varchar(10)

	private String ADACNO = "";

	@Transient 
	private Long ADMAILID;

	private String ADMSUBJECT = "";

	@Lob
	private String ADMAILCONTENT;

	private String ADSENDTYPE = "";

	private String ADSENDTIME = "";

	private String ADSENDSTATUS = "";

	/**
	 * <p>getADMAILID.</p>
	 *
	 * @return a {@link java.lang.Long} object.
	 */
	public Long getADMAILID() {
	return ADMAILID;
	}

	/**
	 * <p>setADMAILID.</p>
	 *
	 * @param admailid a {@link java.lang.Long} object.
	 */
	public void setADMAILID(Long admailid) {
	ADMAILID = admailid;
	}

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("admaillogid", getADMAILLOGID()).toString();
	}

	/**
	 * <p>getADMAILCONTENT.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMAILCONTENT() {
		return ADMAILCONTENT;
	} 
	
	/**
	 * <p>setADMAILCONTENT.</p>
	 *
	 * @param admailcontent a {@link java.lang.String} object.
	 */
	public void setADMAILCONTENT(String admailcontent) {
		ADMAILCONTENT = admailcontent;
	}

	/**
	 * <p>getADMSUBJECT.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMSUBJECT() {
		return ADMSUBJECT;
	}

	/**
	 * <p>getADMSUBJECT.</p>
	 *
	 * @param admsubject a {@link java.lang.String} object.
	 */
	public void getADMSUBJECT(String admsubject) {
		ADMSUBJECT = admsubject;
	}

	/**
	 * <p>getADACNO.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADACNO() {
		return ADACNO;
	}

	/**
	 * <p>setADACNO.</p>
	 *
	 * @param adacno a {@link java.lang.String} object.
	 */
	public void setADACNO(String adacno) {
		ADACNO = adacno;
	}

	/**
	 * <p>getADBATCHNO.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADBATCHNO() {
		return ADBATCHNO;
	}

	/**
	 * <p>setADBATCHNO.</p>
	 *
	 * @param adbatchno a {@link java.lang.String} object.
	 */
	public void setADBATCHNO(String adbatchno) {
		ADBATCHNO = adbatchno;
	}

	/**
	 * <p>getADMAILACNO.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMAILACNO() {
		return ADMAILACNO;
	}

	/**
	 * <p>setADMAILACNO.</p>
	 *
	 * @param admailacno a {@link java.lang.String} object.
	 */
	public void setADMAILACNO(String admailacno) {
		ADMAILACNO = admailacno;
	}
 
	/**
	 * <p>getADMAILLOGID.</p>
	 *
	 * @return a {@link java.lang.Long} object.
	 */
	public Long getADMAILLOGID() {
		return ADMAILLOGID;
	}

	/**
	 * <p>setADMAILLOGID.</p>
	 *
	 * @param admaillogid a {@link java.lang.Long} object.
	 */
	public void setADMAILLOGID(Long admaillogid) {
		ADMAILLOGID = admaillogid;
	}

	/**
	 * <p>getADSENDSTATUS.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADSENDSTATUS() {
		return ADSENDSTATUS;
	}

	/**
	 * <p>setADSENDSTATUS.</p>
	 *
	 * @param adsendstatus a {@link java.lang.String} object.
	 */
	public void setADSENDSTATUS(String adsendstatus) {
		ADSENDSTATUS = adsendstatus;
	}

	/**
	 * <p>getADSENDTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADSENDTIME() {
		return ADSENDTIME;
	}

	/**
	 * <p>setADSENDTIME.</p>
	 *
	 * @param adsendtime a {@link java.lang.String} object.
	 */
	public void setADSENDTIME(String adsendtime) {
		ADSENDTIME = adsendtime;
	}

	/**
	 * <p>getADUSERID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADUSERID() {
		return ADUSERID;
	}

	/**
	 * <p>setADUSERID.</p>
	 *
	 * @param aduserid a {@link java.lang.String} object.
	 */
	public void setADUSERID(String aduserid) {
		ADUSERID = aduserid;
	}

	/**
	 * <p>getADUSERNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADUSERNAME() {
		return ADUSERNAME;
	}

	/**
	 * <p>setADUSERNAME.</p>
	 *
	 * @param adusername a {@link java.lang.String} object.
	 */
	public void setADUSERNAME(String adusername) {
		ADUSERNAME = adusername;
	}

	/**
	 * <p>getADSENDTYPE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADSENDTYPE() {
		return ADSENDTYPE;
	}

	/**
	 * <p>setADSENDTYPE.</p>
	 *
	 * @param adsendtype a {@link java.lang.String} object.
	 */
	public void setADSENDTYPE(String adsendtype) {
		ADSENDTYPE = adsendtype;
	}

}
