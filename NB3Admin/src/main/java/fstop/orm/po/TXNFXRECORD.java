package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Since;

import lombok.Data;


/**
 * <p>TXNFXRECORD class.</p>
 *
 * @author chiensj
 * @author jinhanahuang
 * @version V1.1
 */
@Entity
@Data
@Table(name = "TXNFXRECORD")
public class TXNFXRECORD implements Serializable {

    @Id
    private String ADTXNO;
    private String ADOPID;
    private String FXUSERID;
    private String FXTXDATE;
    private String FXTXTIME;
    private String FXWDAC;
    private String FXWDCURR;
    private String FXWDAMT;
    private String FXSVBH;
    private String FXSVAC;
    private String FXSVCURR;
    private String FXSVAMT;
    private String FXTXMEMO;
    private String FXTXMAILS;
    private String FXTXMAILMEMO;
    private String FXTXCODE;
    @Since(1)
    private String FXTITAINFO  ;
    @Since(1)
    private String FXTOTAINFO  ;
    private String FXSCHID     ;
    private String FXEFEECCY   ;
    private String FXEFEE      ;
    private String FXTELFEE    ;
    private String FXOURCHG    ;
    private String FXEXRATE    ;
    private String FXREMAIL    ;
    private String FXTXSTATUS  ;
    private String FXEXCODE    ;
    private String FXMSGSEQNO  ;
    @Since(1)
    private String FXMSGCONTENT;
    private String LASTDATE    ;
    private String LASTTIME    ;
    private String FXCERT      ;
    private String LOGINTYPE   ;
    @Transient
    private String STAN = "";//交易序號 從FXTITAINFO取出

   
}
