package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>NB3SYSOP class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "MB3SYSOP")
public class MB3SYSOP implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1939408985013613468L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer DPACCSETID;

    @NotBlank(message = "選單代碼不可為空值.")
    private String ADOPID = "";

    @NotBlank(message = "選單名稱（繁）不可為空值.")
    private String ADOPNAME = "";

    private String ADOPENGNAME = "";

    private String ADOPCHSNAME = "";
    private String ADOPMEMO = "";
    private String ADOPALIVE = "";
    private String LASTUSER = "";
    private String LASTDATE = "";
    private String LASTTIME = "";

    private int ADSEQ = 0;
    private String ADOPGROUPID = "";
    private String ISANONYMOUS = "";

//    @NotBlank(message = "URL不可為空值.")
    private String URL = "";
    private String ISPOPUP = "";

    private String ADGPPARENT="";
    private String ADOPGROUP=""; 
    private String ADOPAUTHTYPE="";

    private String ISBLOCKAML="0";
    /**
     * <p>getDPACCSETID.</p>
     *
     * @return the dPACCSETID
     */
    public Integer getDPACCSETID() {
        return DPACCSETID;
    }

    /**
     * <p>setDPACCSETID.</p>
     *
     * @param dPACCSETID the dPACCSETID to set
     */
    public void setDPACCSETID(Integer dPACCSETID) {
        DPACCSETID = dPACCSETID;
    }

    /**
     * <p>getADOPID.</p>
     *
     * @return the aDOPID
     */
    public String getADOPID() {
        return ADOPID;
    }

    /**
     * <p>setADOPID.</p>
     *
     * @param aDOPID the aDOPID to set
     */
    public void setADOPID(String aDOPID) {
        ADOPID = aDOPID;
    }

    /**
     * <p>getADOPNAME.</p>
     *
     * @return the aDOPNAME
     */
    public String getADOPNAME() {
        return ADOPNAME;
    }

    /**
     * <p>setADOPNAME.</p>
     *
     * @param aDOPNAME the aDOPNAME to set
     */
    public void setADOPNAME(String aDOPNAME) {
        ADOPNAME = aDOPNAME;
    }

    /**
     * <p>getADOPENGNAME.</p>
     *
     * @return the aDOPENGNAME
     */
    public String getADOPENGNAME() {
        return ADOPENGNAME;
    }

    /**
     * <p>setADOPENGNAME.</p>
     *
     * @param aDOPENGNAME the aDOPENGNAME to set
     */
    public void setADOPENGNAME(String aDOPENGNAME) {
        ADOPENGNAME = aDOPENGNAME;
    }

    /**
     * <p>getADOPCHSNAME.</p>
     *
     * @return the aDOPCHSNAME
     */
    public String getADOPCHSNAME() {
        return ADOPCHSNAME;
    }

    /**
     * <p>setADOPCHSNAME.</p>
     *
     * @param aDOPCHSNAME the aDOPCHSNAME to set
     */
    public void setADOPCHSNAME(String aDOPCHSNAME) {
        ADOPCHSNAME = aDOPCHSNAME;
    }

    /**
     * <p>getADOPMEMO.</p>
     *
     * @return the aDOPMEMO
     */
    public String getADOPMEMO() {
        return ADOPMEMO;
    }

    /**
     * <p>setADOPMEMO.</p>
     *
     * @param aDOPMEMO the aDOPMEMO to set
     */
    public void setADOPMEMO(String aDOPMEMO) {
        ADOPMEMO = aDOPMEMO;
    }

    /**
     * <p>getADOPALIVE.</p>
     *
     * @return the aDOPALIVE
     */
    public String getADOPALIVE() {
        return ADOPALIVE;
    }

    /**
     * <p>setADOPALIVE.</p>
     *
     * @param aDOPALIVE the aDOPALIVE to set
     */
    public void setADOPALIVE(String aDOPALIVE) {
        ADOPALIVE = aDOPALIVE;
    }

    /**
     * <p>getLASTUSER.</p>
     *
     * @return the lASTUSER
     */
    public String getLASTUSER() {
        return LASTUSER;
    }

    /**
     * <p>setLASTUSER.</p>
     *
     * @param lASTUSER the lASTUSER to set
     */
    public void setLASTUSER(String lASTUSER) {
        LASTUSER = lASTUSER;
    }

    /**
     * <p>getLASTDATE.</p>
     *
     * @return the lASTDATE
     */
    public String getLASTDATE() {
        return LASTDATE;
    }

    /**
     * <p>setLASTDATE.</p>
     *
     * @param lASTDATE the lASTDATE to set
     */
    public void setLASTDATE(String lASTDATE) {
        LASTDATE = lASTDATE;
    }

    /**
     * <p>getLASTTIME.</p>
     *
     * @return the lASTTIME
     */
    public String getLASTTIME() {
        return LASTTIME;
    }

    /**
     * <p>setLASTTIME.</p>
     *
     * @param lASTTIME the lASTTIME to set
     */
    public void setLASTTIME(String lASTTIME) {
        LASTTIME = lASTTIME;
    }

    /**
     * <p>getADSEQ.</p>
     *
     * @return the aDSEQ
     */
    public int getADSEQ() {
        return ADSEQ;
    }

    /**
     * <p>setADSEQ.</p>
     *
     * @param aDSEQ the aDSEQ to set
     */
    public void setADSEQ(int aDSEQ) {
        ADSEQ = aDSEQ;
    }

    /**
     * <p>getADOPGROUPID.</p>
     *
     * @return the aDOPGROUPID
     */
    public String getADOPGROUPID() {
        return ADOPGROUPID;
    }

    /**
     * <p>setADOPGROUPID.</p>
     *
     * @param aDOPGROUPID the aDOPGROUPID to set
     */
    public void setADOPGROUPID(String aDOPGROUPID) {
        ADOPGROUPID = aDOPGROUPID;
    }

    /**
     * <p>getISANONYMOUS.</p>
     *
     * @return the iSANONYMOUS
     */
    public String getISANONYMOUS() {
        return ISANONYMOUS;
    }

    /**
     * <p>setISANONYMOUS.</p>
     *
     * @param iSANONYMOUS the iSANONYMOUS to set
     */
    public void setISANONYMOUS(String iSANONYMOUS) {
        ISANONYMOUS = iSANONYMOUS;
    }

    /**
     * <p>getURL.</p>
     *
     * @return the uRL
     */
    public String getURL() {
        return URL;
    }

    /**
     * <p>setURL.</p>
     *
     * @param uRL the uRL to set
     */
    public void setURL(String uRL) {
        URL = uRL;
    }

    /**
     * <p>getISPOPUP.</p>
     *
     * @return the iSPOPUP
     */
    public String getISPOPUP() {
        return ISPOPUP;
    }

    /**
     * <p>setISPOPUP.</p>
     *
     * @param iSPOPUP the iSPOPUP to set
     */
    public void setISPOPUP(String iSPOPUP) {
        ISPOPUP = iSPOPUP;
    }

    
    /**
     * <p>toString.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String toString() {
		return new ToStringBuilder(this).append("DPACCSETID", getDPACCSETID())
				.toString();
	}

    /**
     * 取得交易大類
     * @return
     */
    public String getADGPPARENT() {
        return ADGPPARENT;
    }

    /**
     * 設定交易大類
     * @param aDGPPARENT
     */
    public void setADGPPARENT(String aDGPPARENT) {
        ADGPPARENT = aDGPPARENT;
    }

    /**
     * 取得交易小類
     * @return
     */
    public String getADOPGROUP() {
        return ADOPGROUP;
    }

    /**
     * 設定交易小類
     * @param aDOPGROUP
     */
    public void setADOPGROUP(String aDOPGROUP) {
        ADOPGROUP = aDOPGROUP;
    }

    /**
     * 取得交易角色權限
     * @return
     */
    public String getADOPAUTHTYPE() {
        return ADOPAUTHTYPE;
    }

    /**
     * 設定交易角色權限
     * @param aDOPAUTHTYPE
     */
    public void setADOPAUTHTYPE(String aDOPAUTHTYPE) {
        ADOPAUTHTYPE = aDOPAUTHTYPE;
    }

    public String getISBLOCKAML() {
        return ISBLOCKAML;
    }

    public void setISBLOCKAML(String iSBLOCKAML) {
        ISBLOCKAML = iSBLOCKAML;
    }

    
}
