package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>ADMCARDTYPE class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMCARDTYPE")
public class ADMCARDTYPE implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@NotBlank(message = "卡別代碼不可為空值.")
	private String ADTYPEID;

	@NotBlank(message = "卡別名稱(繁中)不可為空值.")
	private String ADTYPENAME;

	private String ADTYPEDESC;

	@NotBlank(message = "卡別名稱(簡中)不可為空值.")
	private String ADTYPECHSNAME;

	@NotBlank(message = "卡別名稱(英文)不可為空值.")
	private String ADTYPEENGNAME;

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("adtypeid", getADTYPEID())
				.toString();
	}

	/**
	 * <p>getADTYPEDESC.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADTYPEDESC() {
		return ADTYPEDESC;
	}

	/**
	 * <p>setADTYPEDESC.</p>
	 *
	 * @param adtypedesc a {@link java.lang.String} object.
	 */
	public void setADTYPEDESC(String adtypedesc) {
		ADTYPEDESC = adtypedesc;
	}

	/**
	 * <p>getADTYPEID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADTYPEID() {
		return ADTYPEID;
	}

	/**
	 * <p>setADTYPEID.</p>
	 *
	 * @param adtypeid a {@link java.lang.String} object.
	 */
	public void setADTYPEID(String adtypeid) {
		ADTYPEID = adtypeid;
	}

	/**
	 * <p>getADTYPENAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADTYPENAME() {
		return ADTYPENAME;
	}

	/**
	 * <p>setADTYPENAME.</p>
	 *
	 * @param adtypename a {@link java.lang.String} object.
	 */
	public void setADTYPENAME(String adtypename) {
		ADTYPENAME = adtypename;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}

	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	public String getADTYPECHSNAME() {
		return ADTYPECHSNAME;
	}

	public void setADTYPECHSNAME(String aDTYPECHSNAME) {
		ADTYPECHSNAME = aDTYPECHSNAME;
	}

	public String getADTYPEENGNAME() {
		return ADTYPEENGNAME;
	}

	public void setADTYPEENGNAME(String aDTYPEENGNAME) {
		ADTYPEENGNAME = aDTYPEENGNAME;
	}

}
