package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SYSERRLOG")
public class SYSERRLOG implements Serializable {
    @Id    
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer ID;

    private String LOGGERNAME;
    private String LOGGERHOST;
    private String MESSAGE;
    private String SOURCE;
    private String THROWBY;
	private String LOGDATE;
    private String LOGTIME;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer iD) {
        ID = iD;
    }

    public String getLOGGERNAME() {
        return LOGGERNAME;
    }

    public void setLOGGERNAME(String lOGGERNAME) {
        LOGGERNAME = lOGGERNAME;
    }

    public String getLOGGERHOST() {
        return LOGGERHOST;
    }

    public void setLOGGERHOST(String lOGGERHOST) {
        LOGGERHOST = lOGGERHOST;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String mESSAGE) {
        MESSAGE = mESSAGE;
    }

    public String getSOURCE() {
        return SOURCE;
    }

    public void setSOURCE(String sOURCE) {
        SOURCE = sOURCE;
    }

    public String getTHROWBY() {
        return THROWBY;
    }

    public void setTHROWBY(String tHROWBY) {
        THROWBY = tHROWBY;
    }

    public String getLOGDATE() {
        return LOGDATE;
    }

    public void setLOGDATE(String lOGDATE) {
        LOGDATE = lOGDATE;
    }

    public String getLOGTIME() {
        return LOGTIME;
    }

    public void setLOGTIME(String lOGTIME) {
        LOGTIME = lOGTIME;
    }

   
}