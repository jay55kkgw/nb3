package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Since;

/**
 * 廣告暫存檔，已放行的廣告會由 Temp 檔搬到主檔
 *
 * @author 簡哥
 * @version V1.0
 * 20191121-Danny-處理Byte[]欄位檢核時會錯誤排除
 */
@Entity
@Table(name = "ADMADSTMP")
public class ADMADSTMP implements Serializable {
    @Id
    private String ID;

    private String OID;
    private String TITLE;
    private String CONTENT;
    private String TYPE;
    private String STARTDATE;
    private String STARTTIME;
    private String ENDDATE;
    private String ENDTIME;
    private String FILENAMEL;
    private String FILENAMEM;
    private String FILENAMES;
    @Since(1)
    private byte[] FILECONTENTL;
    private byte[] FILECONTENTM;
    private byte[] FILECONTENTS;
    private String URL ;
    private int SORTORDER;
    private String LASTUSER;
    private String LASTDATE;
    private String LASTTIME;
    private String STEPID;
    private String STEPNAME;
    private String FLOWFINISHED;

    // 20200608 依婉婷需求新增廣告連結類型及相關內容
    private String TARGETTYPE;
    private byte[] TARGETCONTENT;
    private String TARGETFILENAME;
    
    @Transient
    private String EDITORUNAME;

    /**
     * <p>getID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getID() {
        return ID;
    }

    /**
     * <p>setID.</p>
     *
     * @param iD a {@link java.lang.String} object.
     */
    public void setID(String iD) {
        ID = iD;
    }

    /**
     * <p>getOID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOID() {
        return OID;
    }

    /**
     * <p>setOID.</p>
     *
     * @param oID a {@link java.lang.String} object.
     */
    public void setOID(String oID) {
        OID = oID;
    }

    /**
     * <p>getTITLE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTITLE() {
        return TITLE;
    }

    /**
     * <p>setTITLE.</p>
     *
     * @param tITLE a {@link java.lang.String} object.
     */
    public void setTITLE(String tITLE) {
        TITLE = tITLE;
    }

    /**
     * <p>getCONTENT.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCONTENT() {
        return CONTENT;
    }

    /**
     * <p>setCONTENT.</p>
     *
     * @param cONTENT a {@link java.lang.String} object.
     */
    public void setCONTENT(String cONTENT) {
        CONTENT = cONTENT;
    }

    /**
     * <p>getTYPE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTYPE() {
        return TYPE;
    }

    /**
     * <p>setTYPE.</p>
     *
     * @param tYPE a {@link java.lang.String} object.
     */
    public void setTYPE(String tYPE) {
        TYPE = tYPE;
    }

    /**
     * <p>getSTARTDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTARTDATE() {
        return STARTDATE;
    }

    /**
     * <p>setSTARTDATE.</p>
     *
     * @param sTARTDATE a {@link java.lang.String} object.
     */
    public void setSTARTDATE(String sTARTDATE) {
        STARTDATE = sTARTDATE;
    }

    /**
     * <p>getSTARTTIME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTARTTIME() {
        return STARTTIME;
    }

    /**
     * <p>setSTARTTIME.</p>
     *
     * @param sTARTTIME a {@link java.lang.String} object.
     */
    public void setSTARTTIME(String sTARTTIME) {
        STARTTIME = sTARTTIME;
    }

    /**
     * <p>getENDDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getENDDATE() {
        return ENDDATE;
    }

    /**
     * <p>setENDDATE.</p>
     *
     * @param eNDDATE a {@link java.lang.String} object.
     */
    public void setENDDATE(String eNDDATE) {
        ENDDATE = eNDDATE;
    }

    /**
     * <p>getENDTIME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getENDTIME() {
        return ENDTIME;
    }

    /**
     * <p>setENDTIME.</p>
     *
     * @param eNDTIME a {@link java.lang.String} object.
     */
    public void setENDTIME(String eNDTIME) {
        ENDTIME = eNDTIME;
    }

    /**
     * <p>getFILENAMEL.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFILENAMEL() {
        return FILENAMEL;
    }

    /**
     * <p>setFILENAMEL.</p>
     *
     * @param fILENAMEL a {@link java.lang.String} object.
     */
    public void setFILENAMEL(String fILENAMEL) {
        FILENAMEL = fILENAMEL;
    }

    /**
     * <p>getFILENAMEM.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFILENAMEM() {
        return FILENAMEM;
    }

    /**
     * <p>setFILENAMEM.</p>
     *
     * @param fILENAMEM a {@link java.lang.String} object.
     */
    public void setFILENAMEM(String fILENAMEM) {
        FILENAMEM = fILENAMEM;
    }

    /**
     * <p>getFILENAMES.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFILENAMES() {
        return FILENAMES;
    }

    /**
     * <p>setFILENAMES.</p>
     *
     * @param fILENAMES a {@link java.lang.String} object.
     */
    public void setFILENAMES(String fILENAMES) {
        FILENAMES = fILENAMES;
    }

    /**
     * <p>getFILECONTENTL.</p>
     *
     * @return an array of {@link byte} objects.
     */
    public byte[] getFILECONTENTL() {
        return FILECONTENTL;
    }

    /**
     * <p>setFILECONTENTL.</p>
     *
     * @param fILECONTENTL an array of {@link byte} objects.
     */
    public void setFILECONTENTL(byte[] fILECONTENTL) {
        FILECONTENTL = fILECONTENTL;
    }

    /**
     * <p>getFILECONTENTM.</p>
     *
     * @return an array of {@link byte} objects.
     */
    public byte[] getFILECONTENTM() {
        return FILECONTENTM;
    }

    /**
     * <p>setFILECONTENTM.</p>
     *
     * @param fILECONTENTM an array of {@link byte} objects.
     */
    public void setFILECONTENTM(byte[] fILECONTENTM) {
        FILECONTENTM = fILECONTENTM;
    }

    /**
     * <p>getFILECONTENTS.</p>
     *
     * @return an array of {@link byte} objects.
     */
    public byte[] getFILECONTENTS() {
        return FILECONTENTS;
    }

    /**
     * <p>setFILECONTENTS.</p>
     *
     * @param fILECONTENTS an array of {@link byte} objects.
     */
    public void setFILECONTENTS(byte[] fILECONTENTS) {
        FILECONTENTS = fILECONTENTS;
    }

    /**
     * <p>getURL.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getURL() {
        return URL;
    }

    /**
     * <p>setURL.</p>
     *
     * @param uRL a {@link java.lang.String} object.
     */
    public void setURL(String uRL) {
        URL = uRL;
    }

    /**
     * <p>getSORTORDER.</p>
     *
     * @return a int.
     */
    public int getSORTORDER() {
        return SORTORDER;
    }

    /**
     * <p>setSORTORDER.</p>
     *
     * @param sORTORDER a int.
     */
    public void setSORTORDER(int sORTORDER) {
        SORTORDER = sORTORDER;
    }

    /**
     * <p>getLASTUSER.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLASTUSER() {
        return LASTUSER;
    }

    /**
     * <p>setLASTUSER.</p>
     *
     * @param lASTUSER a {@link java.lang.String} object.
     */
    public void setLASTUSER(String lASTUSER) {
        LASTUSER = lASTUSER;
    }

    /**
     * <p>getLASTDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLASTDATE() {
        return LASTDATE;
    }

    /**
     * <p>setLASTDATE.</p>
     *
     * @param lASTDATE a {@link java.lang.String} object.
     */
    public void setLASTDATE(String lASTDATE) {
        LASTDATE = lASTDATE;
    }

    /**
     * <p>getLASTTIME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLASTTIME() {
        return LASTTIME;
    }

    /**
     * <p>setLASTTIME.</p>
     *
     * @param lASTTIME a {@link java.lang.String} object.
     */
    public void setLASTTIME(String lASTTIME) {
        LASTTIME = lASTTIME;
    }

    /**
     * <p>getSTEPID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPID() {
        return STEPID;
    }

    /**
     * <p>setSTEPID.</p>
     *
     * @param sTEPID a {@link java.lang.String} object.
     */
    public void setSTEPID(String sTEPID) {
        STEPID = sTEPID;
    }

    /**
     * <p>getSTEPNAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPNAME() {
        return STEPNAME;
    }

    /**
     * <p>setSTEPNAME.</p>
     *
     * @param sTEPNAME a {@link java.lang.String} object.
     */
    public void setSTEPNAME(String sTEPNAME) {
        STEPNAME = sTEPNAME;
    }

    /**
     * <p>getFLOWFINISHED.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFLOWFINISHED() {
        return FLOWFINISHED;
    }

    /**
     * <p>setFLOWFINISHED.</p>
     *
     * @param fLOWFINISHED a {@link java.lang.String} object.
     */
    public void setFLOWFINISHED(String fLOWFINISHED) {
        FLOWFINISHED = fLOWFINISHED;
    }
    
    public String getEDITORUNAME() {
        return EDITORUNAME;
    }

    public void setEDITORUNAME(String eDITORUNAME) {
        EDITORUNAME = eDITORUNAME;
    }

	public String getTARGETTYPE() {
		return TARGETTYPE;
	}

	public void setTARGETTYPE(String tARGETTYPE) {
		TARGETTYPE = tARGETTYPE;
	}

	public byte[] getTARGETCONTENT() {
		return TARGETCONTENT;
	}

	public void setTARGETCONTENT(byte[] tARGETCONTENT) {
		TARGETCONTENT = tARGETCONTENT;
	}

	public String getTARGETFILENAME() {
		return TARGETFILENAME;
	}

	public void setTARGETFILENAME(String tARGETFILENAME) {
		TARGETFILENAME = tARGETFILENAME;
	}
}
