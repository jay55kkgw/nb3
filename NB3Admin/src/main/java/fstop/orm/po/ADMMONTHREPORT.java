package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>ADMMONTHREPORT class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMMONTHREPORT")
public class ADMMONTHREPORT implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADREPORTID; // 報表資料ID

	private String CMYYYYMM; // 西元年月

	private String ADOPID; // 操作功能ID

	private String ADUSERTYPE; // 用戶類別

	private String ADAGREEF; // 約定/非約定

	private String ADBKFLAG; // 自行/跨行

	private String ADLEVEL; // 金額級距筆數

	private String ADTXSTATUS; // 交易狀態

	private String ADTXCODE; // 交易機制

	private String ADCOUNT; // 筆數

	private Long ADAMOUNT; // 台幣金額
	
	private String LOGINTYPE = ""; //NB:網銀 ， MB:行動 ，預設為NB


	/**
	 * <p>getADAGREEF.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADAGREEF() {
		return ADAGREEF;
	}

	/**
	 * <p>setADAGREEF.</p>
	 *
	 * @param adagreef a {@link java.lang.String} object.
	 */
	public void setADAGREEF(String adagreef) {
		ADAGREEF = adagreef;
	}

	/**
	 * <p>getADAMOUNT.</p>
	 *
	 * @return a {@link java.lang.Long} object.
	 */
	public Long getADAMOUNT() {
		return ADAMOUNT;
	}

	/**
	 * <p>setADAMOUNT.</p>
	 *
	 * @param adamount a {@link java.lang.Long} object.
	 */
	public void setADAMOUNT(Long adamount) {
		ADAMOUNT = adamount;
	}

	/**
	 * <p>getADBKFLAG.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADBKFLAG() {
		return ADBKFLAG;
	}

	/**
	 * <p>setADBKFLAG.</p>
	 *
	 * @param adbkflag a {@link java.lang.String} object.
	 */
	public void setADBKFLAG(String adbkflag) {
		ADBKFLAG = adbkflag;
	}

	/**
	 * <p>getADCOUNT.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCOUNT() {
		return ADCOUNT;
	}

	/**
	 * <p>setADCOUNT.</p>
	 *
	 * @param adcount a {@link java.lang.String} object.
	 */
	public void setADCOUNT(String adcount) {
		ADCOUNT = adcount;
	}

	/**
	 * <p>getADLEVEL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADLEVEL() {
		return ADLEVEL;
	}

	/**
	 * <p>setADLEVEL.</p>
	 *
	 * @param adlevel a {@link java.lang.String} object.
	 */
	public void setADLEVEL(String adlevel) {
		ADLEVEL = adlevel;
	}

	/**
	 * <p>getADOPID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADOPID() {
		return ADOPID;
	}

	/**
	 * <p>setADOPID.</p>
	 *
	 * @param adopid a {@link java.lang.String} object.
	 */
	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	/**
	 * <p>getADREPORTID.</p>
	 *
	 * @return a {@link java.lang.Long} object.
	 */
	public Long getADREPORTID() {
		return ADREPORTID;
	}

	/**
	 * <p>setADREPORTID.</p>
	 *
	 * @param adreportid a {@link java.lang.Long} object.
	 */
	public void setADREPORTID(Long adreportid) {
		ADREPORTID = adreportid;
	}

	/**
	 * <p>getADTXCODE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADTXCODE() {
		return ADTXCODE;
	}

	/**
	 * <p>setADTXCODE.</p>
	 *
	 * @param adtxcode a {@link java.lang.String} object.
	 */
	public void setADTXCODE(String adtxcode) {
		ADTXCODE = adtxcode;
	}

	/**
	 * <p>getADTXSTATUS.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADTXSTATUS() {
		return ADTXSTATUS;
	}

	/**
	 * <p>setADTXSTATUS.</p>
	 *
	 * @param adtxstatus a {@link java.lang.String} object.
	 */
	public void setADTXSTATUS(String adtxstatus) {
		ADTXSTATUS = adtxstatus;
	}

	/**
	 * <p>getADUSERTYPE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADUSERTYPE() {
		return ADUSERTYPE;
	}

	/**
	 * <p>setADUSERTYPE.</p>
	 *
	 * @param adusertype a {@link java.lang.String} object.
	 */
	public void setADUSERTYPE(String adusertype) {
		ADUSERTYPE = adusertype;
	}

	/**
	 * <p>getCMYYYYMM.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCMYYYYMM() {
		return CMYYYYMM;
	}

	/**
	 * <p>setCMYYYYMM.</p>
	 *
	 * @param cmyyyymm a {@link java.lang.String} object.
	 */
	public void setCMYYYYMM(String cmyyyymm) {
		CMYYYYMM = cmyyyymm;
	}

	/**
	 * <p>getLOGINTYPE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLOGINTYPE() {
		return LOGINTYPE;
	}
	
	/**
	 * <p>setLOGINTYPE.</p>
	 *
	 * @param logintype a {@link java.lang.String} object.
	 */
	public void setLOGINTYPE(String logintype) {
		LOGINTYPE = logintype;
	}
}
