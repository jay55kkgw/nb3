package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@Table(name = "ADMLOGIN")
public class ADMLOGIN implements Serializable {


	private String ADUSERID = "";
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String TOKENID = "";

	private String ADUSERTYPE = "";  // 0.個人戶,  1.企業戶

	private String ADUSERIP = "";

	private String LOGINOUT = "";  // 0.登入, 1.登出

	private String LOGINTIME = "";

	private String LOGOUTTIME = "";

	private String FORCELOGOUTTIME = "";

	private String LASTLOGINTIME = "";	
	
	private String LASTADUSERIP = "";	
	
	private String LOGINTYPE = "";	
	
	private String DPUSERNAME = "";	
	
	private String MMACOD = "";	
	
	private String XMLCOD = "";	
	
	private String BRTHDY = "";	
	
	private String TELNUM = "";	
	
	private String STAFF = "";	
	
	private String MBSTAT = "";	
	
	private String MBOPNDT = "";	
	
	private String MBOPNTM = "";	
	
	private String IDNCOD = "";	
	
	private String APATR = "";

	public String getADUSERID() {
		return ADUSERID;
	}

	public void setADUSERID(String aDUSERID) {
		ADUSERID = aDUSERID;
	}

	public String getTOKENID() {
		return TOKENID;
	}

	public void setTOKENID(String tOKENID) {
		TOKENID = tOKENID;
	}

	public String getADUSERTYPE() {
		return ADUSERTYPE;
	}

	public void setADUSERTYPE(String aDUSERTYPE) {
		ADUSERTYPE = aDUSERTYPE;
	}

	public String getADUSERIP() {
		return ADUSERIP;
	}

	public void setADUSERIP(String aDUSERIP) {
		ADUSERIP = aDUSERIP;
	}

	public String getLOGINOUT() {
		return LOGINOUT;
	}

	public void setLOGINOUT(String lOGINOUT) {
		LOGINOUT = lOGINOUT;
	}

	public String getLOGINTIME() {
		return LOGINTIME;
	}

	public void setLOGINTIME(String lOGINTIME) {
		LOGINTIME = lOGINTIME;
	}

	public String getLOGOUTTIME() {
		return LOGOUTTIME;
	}

	public void setLOGOUTTIME(String lOGOUTTIME) {
		LOGOUTTIME = lOGOUTTIME;
	}

	public String getFORCELOGOUTTIME() {
		return FORCELOGOUTTIME;
	}

	public void setFORCELOGOUTTIME(String fORCELOGOUTTIME) {
		FORCELOGOUTTIME = fORCELOGOUTTIME;
	}

	public String getLASTLOGINTIME() {
		return LASTLOGINTIME;
	}

	public void setLASTLOGINTIME(String lASTLOGINTIME) {
		LASTLOGINTIME = lASTLOGINTIME;
	}

	public String getLASTADUSERIP() {
		return LASTADUSERIP;
	}

	public void setLASTADUSERIP(String lASTADUSERIP) {
		LASTADUSERIP = lASTADUSERIP;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}

	public String getDPUSERNAME() {
		return DPUSERNAME;
	}

	public void setDPUSERNAME(String dPUSERNAME) {
		DPUSERNAME = dPUSERNAME;
	}

	public String getMMACOD() {
		return MMACOD;
	}

	public void setMMACOD(String mMACOD) {
		MMACOD = mMACOD;
	}

	public String getXMLCOD() {
		return XMLCOD;
	}

	public void setXMLCOD(String xMLCOD) {
		XMLCOD = xMLCOD;
	}

	public String getBRTHDY() {
		return BRTHDY;
	}

	public void setBRTHDY(String bRTHDY) {
		BRTHDY = bRTHDY;
	}

	public String getTELNUM() {
		return TELNUM;
	}

	public void setTELNUM(String tELNUM) {
		TELNUM = tELNUM;
	}

	public String getSTAFF() {
		return STAFF;
	}

	public void setSTAFF(String sTAFF) {
		STAFF = sTAFF;
	}

	public String getMBSTAT() {
		return MBSTAT;
	}

	public void setMBSTAT(String mBSTAT) {
		MBSTAT = mBSTAT;
	}

	public String getMBOPNDT() {
		return MBOPNDT;
	}

	public void setMBOPNDT(String mBOPNDT) {
		MBOPNDT = mBOPNDT;
	}

	public String getMBOPNTM() {
		return MBOPNTM;
	}

	public void setMBOPNTM(String mBOPNTM) {
		MBOPNTM = mBOPNTM;
	}

	public String getIDNCOD() {
		return IDNCOD;
	}

	public void setIDNCOD(String iDNCOD) {
		IDNCOD = iDNCOD;
	}

	public String getAPATR() {
		return APATR;
	}

	public void setAPATR(String aPATR) {
		APATR = aPATR;
	}

}
