package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 * TXNTWSCHPAYDATAIDENTITY Composite Primary Keys
 *
 * @author Alison
 * @version V1.0
 */
@Embeddable
public class TXNTWSCHPAYDATAIDENTITY implements Serializable {
    
    private String DPSCHNO;
    private String DPUSERID;
    private String DPSCHTXDATE;
    
	public String getDPSCHNO() {
		return DPSCHNO;
	}
	public void setDPSCHNO(String dPSCHNO) {
		DPSCHNO = dPSCHNO;
	}
	public String getDPUSERID() {
		return DPUSERID;
	}
	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}
	public String getDPSCHTXDATE() {
		return DPSCHTXDATE;
	}
	public void setDPSCHTXDATE(String dPSCHTXDATE) {
		DPSCHTXDATE = dPSCHTXDATE;
	}
}
