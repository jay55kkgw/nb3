package fstop.orm.po;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>SYSDAILYSEQ class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "SYSDAILYSEQ")
public class SYSDAILYSEQ {
	@Id
	private String ADSEQID;

	private Long ADSEQ;

	private String ADSEQMEMO;

	/**
	 * <p>getADSEQ.</p>
	 *
	 * @return a {@link java.lang.Long} object.
	 */
	public Long getADSEQ() {
		return ADSEQ;
	}

	/**
	 * <p>setADSEQ.</p>
	 *
	 * @param adseq a {@link java.lang.Long} object.
	 */
	public void setADSEQ(Long adseq) {
		ADSEQ = adseq;
	}

	/**
	 * <p>getADSEQID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADSEQID() {
		return ADSEQID;
	}

	/**
	 * <p>setADSEQID.</p>
	 *
	 * @param adseqid a {@link java.lang.String} object.
	 */
	public void setADSEQID(String adseqid) {
		ADSEQID = adseqid;
	}

	/**
	 * <p>getADSEQMEMO.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADSEQMEMO() {
		return ADSEQMEMO;
	}

	/**
	 * <p>setADSEQMEMO.</p>
	 *
	 * @param adseqmemo a {@link java.lang.String} object.
	 */
	public void setADSEQMEMO(String adseqmemo) {
		ADSEQMEMO = adseqmemo;
	}

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("adseqid", getADSEQID())
				.toString();
	}
}
