package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>ADMBHCONTACT class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity        
@Table(name = "ADMBHCONTACT")
public class ADMBHCONTACT implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer ADBHCONTID;

	@NotBlank(message = "請輸入分行代碼")
	private String ADBRANCHID;

	@NotBlank(message = "請輸入分行名稱(繁中)")
	private String ADBRANCHNAME;
	
	@NotBlank(message = "請輸入分行名稱(英文)")
	private String ADBRANENGNAME;

	@NotBlank(message = "請輸入分行名稱(簡中)")
	private String ADBRANCHSNAME;

	private String ADCONTACTIID;

	private String ADCONTACTNAME;

	private String ADCONTACTEMAIL;

	private String ADCONTACTTEL;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	/**
	 * <p>getADBHCONTID.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getADBHCONTID() {
		return ADBHCONTID;
	}

	/**
	 * <p>setADBHCONTID.</p>
	 *
	 * @param adbhcontid a {@link java.lang.Integer} object.
	 */
	public void setADBHCONTID(Integer adbhcontid) {
		ADBHCONTID = adbhcontid;
	}

	/**
	 * <p>getADBRANCHID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADBRANCHID() {
		return ADBRANCHID;
	}

	/**
	 * <p>setADBRANCHID.</p>
	 *
	 * @param adbranchid a {@link java.lang.String} object.
	 */
	public void setADBRANCHID(String adbranchid) {
		ADBRANCHID = adbranchid;
	}
	
	/**
	 * <p>setADBRANCHNAME.</p>
	 *
	 * @param adbranchname a {@link java.lang.String} object.
	 */
	public void setADBRANCHNAME(String adbranchname) {
		ADBRANCHNAME = adbranchname;
	}

	/**
	 * <p>getADBRANCHNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADBRANCHNAME() {
		return ADBRANCHNAME;
	}
	
	/**
	 * <p>getADCONTACTEMAIL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCONTACTEMAIL() {
		return ADCONTACTEMAIL;
	}

	/**
	 * <p>setADCONTACTEMAIL.</p>
	 *
	 * @param adcontactemail a {@link java.lang.String} object.
	 */
	public void setADCONTACTEMAIL(String adcontactemail) {
		ADCONTACTEMAIL = adcontactemail;
	}

	/**
	 * <p>getADCONTACTIID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCONTACTIID() {
		return ADCONTACTIID;
	}

	/**
	 * <p>setADCONTACTIID.</p>
	 *
	 * @param adcontactiid a {@link java.lang.String} object.
	 */
	public void setADCONTACTIID(String adcontactiid) {
		ADCONTACTIID = adcontactiid;
	}

	/**
	 * <p>getADCONTACTNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCONTACTNAME() {
		return ADCONTACTNAME;
	}

	/**
	 * <p>setADCONTACTNAME.</p>
	 *
	 * @param adcontactname a {@link java.lang.String} object.
	 */
	public void setADCONTACTNAME(String adcontactname) {
		ADCONTACTNAME = adcontactname;
	}

	/**
	 * <p>getADCONTACTTEL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCONTACTTEL() {
		return ADCONTACTTEL;
	}

	/**
	 * <p>setADCONTACTTEL.</p>
	 *
	 * @param adcontacttel a {@link java.lang.String} object.
	 */
	public void setADCONTACTTEL(String adcontacttel) {
		ADCONTACTTEL = adcontacttel;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}

	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	public String getADBRANENGNAME() {
		return ADBRANENGNAME;
	}

	public void setADBRANENGNAME(String aDBRANENGNAME) {
		ADBRANENGNAME = aDBRANENGNAME;
	}

	public String getADBRANCHSNAME() {
		return ADBRANCHSNAME;
	}

	public void setADBRANCHSNAME(String aDBRANCHSNAME) {
		ADBRANCHSNAME = aDBRANCHSNAME;
	}

}
