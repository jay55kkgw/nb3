package fstop.orm.po;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>SYSPARAMDATA class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "SYSPARAMDATA")
public class SYSPARAMDATA  {

	@Id
	private String ADPK;

	private String ADNAME = "";

	private String ADPWD = "";

	private String ADAPNAME = "";
	
	private String ADFXSHH = "";

	private String ADFXSMM = "";

	private String ADFXSSS = "";
	
	
	private String ADFXEHH = "";
	
	private String ADFXEMM = "";

	private String ADFXESS = "";

	private String ADFDSHH = "";
	
	private String ADFDSMM = "";
	
	private String ADFDSSS = "";

	private String ADFDEHH = "";

	private String ADFDEMM = "";

	private String ADFDESS = "";

	private String ADPORTALJNDI = "";

	private String ADNBJNDI = "";

	private String ADSESSIONTO = "";

	private String ADOPMAIL = "";

	private String ADAPMAIL = "";

	private String ADSECMAIL = "";

	private String ADSPMAIL = "";

	private String ADFDSVRIP = "";

	private String ADMAILSVRIP = "";

	private String ADPARKSVRIP = "";

	private String ADARSVRIP = "";

	private String ADFDRETRYTIMES = "";

	private String ADPKRETRYTIMES = "";

	private String ADRESENDTIMES = "";

	private String ADBHMAILACNORULE = "";

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	private String ADPORTALURL = "";

	private String ADPORTALURL_ADM = "";
	
	private String ADGDMAIL = "";
	
	private String ADGDTXNMAIL = "";

	private String ADGDSHH = "";

	private String ADGDSMM = "";

	private String ADGDSSS = "";

	private String ADGDEHH = "";

	private String ADGDEMM = "";

	private String ADGDESS = "";
	
	private String KYCDATE = "";
	private String SCN070 = "";
	private String SCF002 = "";
	
	/**
	 * table not contain there variable, comment by @transient for compatible
	 */
	@Transient
	private String ADFX1SHH = "";
	
	@Transient
	private String ADFX1SMM = "";

	@Transient
	private String ADFX1SSS = "";

	@Transient
	private String ADFXE1HH = "";

	@Transient
	private String ADFXE1MM = "";

	@Transient
	private String ADFXE1SS = "";

	
	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("adpk", getADPK()).toString();
	}

	/**
	 * <p>getADAPMAIL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADAPMAIL() {
		return ADAPMAIL;
	}

	/**
	 * <p>setADAPMAIL.</p>
	 *
	 * @param adapmail a {@link java.lang.String} object.
	 */
	public void setADAPMAIL(String adapmail) {
		ADAPMAIL = adapmail;
	}

	/**
	 * <p>getADAPNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADAPNAME() {
		return ADAPNAME;
	}

	/**
	 * <p>setADAPNAME.</p>
	 *
	 * @param adapname a {@link java.lang.String} object.
	 */
	public void setADAPNAME(String adapname) {
		ADAPNAME = adapname;
	}

	/**
	 * <p>getADARSVRIP.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADARSVRIP() {
		return ADARSVRIP;
	}

	/**
	 * <p>setADARSVRIP.</p>
	 *
	 * @param adarsvrip a {@link java.lang.String} object.
	 */
	public void setADARSVRIP(String adarsvrip) {
		ADARSVRIP = adarsvrip;
	}

	/**
	 * <p>getADBHMAILACNORULE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADBHMAILACNORULE() {
		return ADBHMAILACNORULE;
	}

	/**
	 * <p>setADBHMAILACNORULE.</p>
	 *
	 * @param adbhmailacnorule a {@link java.lang.String} object.
	 */
	public void setADBHMAILACNORULE(String adbhmailacnorule) {
		ADBHMAILACNORULE = adbhmailacnorule;
	}

	/**
	 * <p>getADFDEHH.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFDEHH() {
		return ADFDEHH;
	}

	/**
	 * <p>setADFDEHH.</p>
	 *
	 * @param adfdehh a {@link java.lang.String} object.
	 */
	public void setADFDEHH(String adfdehh) {
		ADFDEHH = adfdehh;
	}

	/**
	 * <p>getADFDEMM.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFDEMM() {
		return ADFDEMM;
	}

	/**
	 * <p>setADFDEMM.</p>
	 *
	 * @param adfdemm a {@link java.lang.String} object.
	 */
	public void setADFDEMM(String adfdemm) {
		ADFDEMM = adfdemm;
	}

	/**
	 * <p>getADFDESS.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFDESS() {
		return ADFDESS;
	}

	/**
	 * <p>setADFDESS.</p>
	 *
	 * @param adfdess a {@link java.lang.String} object.
	 */
	public void setADFDESS(String adfdess) {
		ADFDESS = adfdess;
	}

	/**
	 * <p>getADFDRETRYTIMES.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFDRETRYTIMES() {
		return ADFDRETRYTIMES;
	}

	/**
	 * <p>setADFDRETRYTIMES.</p>
	 *
	 * @param adfdretrytimes a {@link java.lang.String} object.
	 */
	public void setADFDRETRYTIMES(String adfdretrytimes) {
		ADFDRETRYTIMES = adfdretrytimes;
	}

	/**
	 * <p>getADFDSHH.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFDSHH() {
		return ADFDSHH;
	}

	/**
	 * <p>setADFDSHH.</p>
	 *
	 * @param adfdshh a {@link java.lang.String} object.
	 */
	public void setADFDSHH(String adfdshh) {
		ADFDSHH = adfdshh;
	}

	/**
	 * <p>getADFDSMM.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFDSMM() {
		return ADFDSMM;
	}

	/**
	 * <p>setADFDSMM.</p>
	 *
	 * @param adfdsmm a {@link java.lang.String} object.
	 */
	public void setADFDSMM(String adfdsmm) {
		ADFDSMM = adfdsmm;
	}

	/**
	 * <p>getADFDSSS.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFDSSS() {
		return ADFDSSS;
	}

	/**
	 * <p>setADFDSSS.</p>
	 *
	 * @param adfdsss a {@link java.lang.String} object.
	 */
	public void setADFDSSS(String adfdsss) {
		ADFDSSS = adfdsss;
	}

	/**
	 * <p>getADFDSVRIP.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFDSVRIP() {
		return ADFDSVRIP;
	}

	/**
	 * <p>setADFDSVRIP.</p>
	 *
	 * @param adfdsvrip a {@link java.lang.String} object.
	 */
	public void setADFDSVRIP(String adfdsvrip) {
		ADFDSVRIP = adfdsvrip;
	}

	/**
	 * <p>getADFXEHH.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFXEHH() {
		return ADFXEHH;
	}

	/**
	 * <p>setADFXEHH.</p>
	 *
	 * @param adfxehh a {@link java.lang.String} object.
	 */
	public void setADFXEHH(String adfxehh) {
		ADFXEHH = adfxehh;
	}

	/**
	 * <p>getADFXEMM.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFXEMM() {
		return ADFXEMM;
	}

	/**
	 * <p>setADFXEMM.</p>
	 *
	 * @param adfxemm a {@link java.lang.String} object.
	 */
	public void setADFXEMM(String adfxemm) {
		ADFXEMM = adfxemm;
	}

	/**
	 * <p>getADFXESS.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFXESS() {
		return ADFXESS;
	}

	/**
	 * <p>setADFXESS.</p>
	 *
	 * @param adfxess a {@link java.lang.String} object.
	 */
	public void setADFXESS(String adfxess) {
		ADFXESS = adfxess;
	}

	/**
	 * <p>getADFXSHH.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFXSHH() {
		return ADFXSHH;
	}

	/**
	 * <p>setADFXSHH.</p>
	 *
	 * @param adfxshh a {@link java.lang.String} object.
	 */
	public void setADFXSHH(String adfxshh) {
		ADFXSHH = adfxshh;
	}

	/**
	 * <p>getADFXSMM.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFXSMM() {
		return ADFXSMM;
	}

	/**
	 * <p>setADFXSMM.</p>
	 *
	 * @param adfxsmm a {@link java.lang.String} object.
	 */
	public void setADFXSMM(String adfxsmm) {
		ADFXSMM = adfxsmm;
	}

	/**
	 * <p>getADFXSSS.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFXSSS() {
		return ADFXSSS;
	}

	/**
	 * <p>setADFXSSS.</p>
	 *
	 * @param adfxsss a {@link java.lang.String} object.
	 */
	public void setADFXSSS(String adfxsss) {
		ADFXSSS = adfxsss;
	}

	/**
	 * <p>getADMAILSVRIP.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMAILSVRIP() {
		return ADMAILSVRIP;
	}

	/**
	 * <p>setADMAILSVRIP.</p>
	 *
	 * @param admailsvrip a {@link java.lang.String} object.
	 */
	public void setADMAILSVRIP(String admailsvrip) {
		ADMAILSVRIP = admailsvrip;
	}

	/**
	 * <p>getADNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADNAME() {
		return ADNAME;
	}

	/**
	 * <p>setADNAME.</p>
	 *
	 * @param adname a {@link java.lang.String} object.
	 */
	public void setADNAME(String adname) {
		ADNAME = adname;
	}

	/**
	 * <p>getADNBJNDI.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADNBJNDI() {
		return ADNBJNDI;
	}

	/**
	 * <p>setADNBJNDI.</p>
	 *
	 * @param adnbjndi a {@link java.lang.String} object.
	 */
	public void setADNBJNDI(String adnbjndi) {
		ADNBJNDI = adnbjndi;
	}

	/**
	 * <p>getADOPMAIL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADOPMAIL() {
		return ADOPMAIL;
	}

	/**
	 * <p>setADOPMAIL.</p>
	 *
	 * @param adopmail a {@link java.lang.String} object.
	 */
	public void setADOPMAIL(String adopmail) {
		ADOPMAIL = adopmail;
	}

	/**
	 * <p>getADPARKSVRIP.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADPARKSVRIP() {
		return ADPARKSVRIP;
	}

	/**
	 * <p>setADPARKSVRIP.</p>
	 *
	 * @param adparksvrip a {@link java.lang.String} object.
	 */
	public void setADPARKSVRIP(String adparksvrip) {
		ADPARKSVRIP = adparksvrip;
	}

	/**
	 * <p>getADPK.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADPK() {
		return ADPK;
	}

	/**
	 * <p>setADPK.</p>
	 *
	 * @param adpk a {@link java.lang.String} object.
	 */
	public void setADPK(String adpk) {
		ADPK = adpk;
	}

	/**
	 * <p>getADPKRETRYTIMES.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADPKRETRYTIMES() {
		return ADPKRETRYTIMES;
	}

	/**
	 * <p>setADPKRETRYTIMES.</p>
	 *
	 * @param adpkretrytimes a {@link java.lang.String} object.
	 */
	public void setADPKRETRYTIMES(String adpkretrytimes) {
		ADPKRETRYTIMES = adpkretrytimes;
	}

	/**
	 * <p>getADPORTALJNDI.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADPORTALJNDI() {
		return ADPORTALJNDI;
	}

	/**
	 * <p>setADPORTALJNDI.</p>
	 *
	 * @param adportaljndi a {@link java.lang.String} object.
	 */
	public void setADPORTALJNDI(String adportaljndi) {
		ADPORTALJNDI = adportaljndi;
	}

	/**
	 * <p>getADPWD.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADPWD() {
		return ADPWD;
	}

	/**
	 * <p>setADPWD.</p>
	 *
	 * @param adpwd a {@link java.lang.String} object.
	 */
	public void setADPWD(String adpwd) {
		ADPWD = adpwd;
	}

	/**
	 * <p>getADRESENDTIMES.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADRESENDTIMES() {
		return ADRESENDTIMES;
	}

	/**
	 * <p>setADRESENDTIMES.</p>
	 *
	 * @param adresendtimes a {@link java.lang.String} object.
	 */
	public void setADRESENDTIMES(String adresendtimes) {
		ADRESENDTIMES = adresendtimes;
	}

	/**
	 * <p>getADSECMAIL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADSECMAIL() {
		return ADSECMAIL;
	}

	/**
	 * <p>setADSECMAIL.</p>
	 *
	 * @param adsecmail a {@link java.lang.String} object.
	 */
	public void setADSECMAIL(String adsecmail) {
		ADSECMAIL = adsecmail;
	}

	/**
	 * <p>getADSESSIONTO.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADSESSIONTO() {
		return ADSESSIONTO;
	}

	/**
	 * <p>setADSESSIONTO.</p>
	 *
	 * @param adsessionto a {@link java.lang.String} object.
	 */
	public void setADSESSIONTO(String adsessionto) {
		ADSESSIONTO = adsessionto;
	}

	/**
	 * <p>getADSPMAIL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADSPMAIL() {
		return ADSPMAIL;
	}

	/**
	 * <p>setADSPMAIL.</p>
	 *
	 * @param adspmail a {@link java.lang.String} object.
	 */
	public void setADSPMAIL(String adspmail) {
		ADSPMAIL = adspmail;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}

	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}
	
	/**
	 * <p>getADPORTALURL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADPORTALURL() {
		return ADPORTALURL;
	}

	/**
	 * <p>setADPORTALURL.</p>
	 *
	 * @param url a {@link java.lang.String} object.
	 */
	public void setADPORTALURL(String url) {
		ADPORTALURL = url;
	}
	
	/**
	 * <p>getADPORTALURL_ADM.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADPORTALURL_ADM() {
		return ADPORTALURL_ADM;
	}

	/**
	 * <p>setADPORTALURL_ADM.</p>
	 *
	 * @param url a {@link java.lang.String} object.
	 */
	public void setADPORTALURL_ADM(String url) {
		ADPORTALURL_ADM = url;
	}	

	/**
	 * <p>getADGDMAIL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADGDMAIL() {
		return ADGDMAIL;
	}

	/**
	 * <p>setADGDMAIL.</p>
	 *
	 * @param adgdmail a {@link java.lang.String} object.
	 */
	public void setADGDMAIL(String adgdmail) {
		ADGDMAIL = adgdmail;
	}
	
	/**
	 * <p>getADGDTXNMAIL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADGDTXNMAIL() {
		return ADGDTXNMAIL;
	}

	/**
	 * <p>setADGDTXNMAIL.</p>
	 *
	 * @param adgdtxnmail a {@link java.lang.String} object.
	 */
	public void setADGDTXNMAIL(String adgdtxnmail) {
		ADGDTXNMAIL = adgdtxnmail;
	}	
	
	/**
	 * <p>getADGDSHH.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADGDSHH() {
		return ADGDSHH;
	}

	/**
	 * <p>setADGDSHH.</p>
	 *
	 * @param adgdshh a {@link java.lang.String} object.
	 */
	public void setADGDSHH(String adgdshh) {
		ADGDSHH = adgdshh;
	}

	/**
	 * <p>getADGDSMM.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADGDSMM() {
		return ADGDSMM;
	}

	/**
	 * <p>setADGDSMM.</p>
	 *
	 * @param adgdsmm a {@link java.lang.String} object.
	 */
	public void setADGDSMM(String adgdsmm) {
		ADGDSMM = adgdsmm;
	}

	/**
	 * <p>getADGDSSS.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADGDSSS() {
		return ADGDSSS;
	}

	/**
	 * <p>setADGDSSS.</p>
	 *
	 * @param adgdsss a {@link java.lang.String} object.
	 */
	public void setADGDSSS(String adgdsss) {
		ADGDSSS = adgdsss;
	}

	/**
	 * <p>getADGDEHH.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADGDEHH() {
		return ADGDEHH;
	}

	/**
	 * <p>setADGDEHH.</p>
	 *
	 * @param adgdehh a {@link java.lang.String} object.
	 */
	public void setADGDEHH(String adgdehh) {
		ADGDEHH = adgdehh;
	}

	/**
	 * <p>getADGDEMM.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADGDEMM() {
		return ADGDEMM;
	}

	/**
	 * <p>setADGDEMM.</p>
	 *
	 * @param adgdemm a {@link java.lang.String} object.
	 */
	public void setADGDEMM(String adgdemm) {
		ADGDEMM = adgdemm;
	}

	/**
	 * <p>getADGDESS.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADGDESS() {
		return ADGDESS;
	}

	/**
	 * <p>setADGDESS.</p>
	 *
	 * @param adgdess a {@link java.lang.String} object.
	 */
	public void setADGDESS(String adgdess) {
		ADGDESS = adgdess;
	}
	/**
	 * <p>getKYCDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getKYCDATE() {
		return KYCDATE;
	}

	/**
	 * <p>setKYCDATE.</p>
	 *
	 * @param kycdate a {@link java.lang.String} object.
	 */
	public void setKYCDATE(String kycdate) {
		KYCDATE= kycdate;
	}		
	/**
	 * <p>getSCN070.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSCN070() {
		return SCN070;
	}

	/**
	 * <p>setSCN070.</p>
	 *
	 * @param scn070 a {@link java.lang.String} object.
	 */
	public void setSCN070(String scn070) {
		SCN070= scn070;
	}	
	/**
	 * <p>getSCF002.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSCF002() {
		return SCF002;
	}

	/**
	 * <p>setSCF002.</p>
	 *
	 * @param scf002 a {@link java.lang.String} object.
	 */
	public void setSCF002(String scf002) {
		SCF002= scf002;
	}
	/**
	 * <p>getADFXE1HH.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFXE1HH() {
		return ADFXE1HH;
	}

	/**
	 * <p>setADFXE1HH.</p>
	 *
	 * @param adfxehh a {@link java.lang.String} object.
	 */
	public void setADFXE1HH(String adfxehh) {
		ADFXE1HH = adfxehh;
	}

	/**
	 * <p>getADFXE1MM.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFXE1MM() {
		return ADFXE1MM;
	}

	/**
	 * <p>setADFXE1MM.</p>
	 *
	 * @param adfxemm a {@link java.lang.String} object.
	 */
	public void setADFXE1MM(String adfxemm) {
		ADFXE1MM = adfxemm;
	}

	/**
	 * <p>getADFXE1SS.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFXE1SS() {
		return ADFXE1SS;
	}

	/**
	 * <p>setADFXE1SS.</p>
	 *
	 * @param adfxess a {@link java.lang.String} object.
	 */
	public void setADFXE1SS(String adfxess) {
		ADFXE1SS = adfxess;
	}

	/**
	 * <p>getADFX1SHH.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFX1SHH() {
		return ADFX1SHH;
	}

	/**
	 * <p>setADFX1SHH.</p>
	 *
	 * @param adfxshh a {@link java.lang.String} object.
	 */
	public void setADFX1SHH(String adfxshh) {
		ADFX1SHH = adfxshh;
	}

	/**
	 * <p>getADFX1SMM.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFX1SMM() {
		return ADFX1SMM;
	}

	/**
	 * <p>setADFX1SMM.</p>
	 *
	 * @param adfxsmm a {@link java.lang.String} object.
	 */
	public void setADFX1SMM(String adfxsmm) {
		ADFX1SMM = adfxsmm;
	}

	/**
	 * <p>getADFX1SSS.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADFX1SSS() {
		return ADFX1SSS;
	}

	/**
	 * <p>setADFX1SSS.</p>
	 *
	 * @param adfxsss a {@link java.lang.String} object.
	 */
	public void setADFX1SSS(String adfxsss) {
		ADFX1SSS = adfxsss;
	}	
}
