package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "QUICKLOGINMAPPING")
@Data
public class QUICKLOGINMAPPING implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5236075994281599923L;

	@Id
	private QUICKLOGINMAPPING_PK pks;

	private String DEVICEID;
	private String DEVICENAME;
	private String DEVICETYPE;
	private String DEVICESTATUS;
	private String DEVICEBRAND;
	private String DEVICEOS;
	private String VERIFYTYPE;
	private String ERRORCNT;
	private String ISDEFAULT;//是否為預設
	private String LASTDATE;
	private String LASTTIME;
	private String REGDATE;
	private String REGTIME;
	private String QUICK_CALLBACK_TIME;
	private String QUICKID;
	private String UPDATEUSER;
	private String QCKLGIN="N";//是否快速登入;Y/N
	private String QCKTRAN="N";//是否開啟快速交易;Y/N
	
	/**
	 * 重送次數, for UI 顯示之擴充欄位
	 */
	@Transient
	private int BTCANCEL;


	
}
