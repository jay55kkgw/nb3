package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Since;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>TXNTWRECORD class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "TXNTWRECORD")
public class TXNTWRECORD implements Serializable {

	@Id
	private String ADTXNO         ;
	private String ADOPID         ;
	private String DPUSERID       ;
	private String DPTXDATE       ;
	private String DPTXTIME       ;
	private String DPWDAC         ;
	private String DPSVBH         ;
	private String DPSVAC         ;
	private String DPTXAMT        ;
	private String DPTXMEMO       ;
	private String DPTXMAILS      ;
	private String DPTXMAILMEMO   ;
	private String DPSCHID        ;
	private String DPEFEE         ;
	private String PCSEQ          ;
	private String DPTXNO         ;
    private String DPTXCODE       ;
    @Since(1)
	private String DPTITAINFO     ;
    @Since(1)
	private String DPTOTAINFO     ;
	private String DPREMAIL       ;
	private String DPTXSTATUS     ;
	private String DPEXCODE       ;
	private String LASTDATE       ;
	private String LASTTIME       ;
	private String LOGINTYPE      ;


    /**
     * @return String return the ADTXNO
     */
    public String getADTXNO() {
        return ADTXNO;
    }

    /**
     * @param ADTXNO the ADTXNO to set
     */
    public void setADTXNO(String ADTXNO) {
        this.ADTXNO = ADTXNO;
    }

    /**
     * @return String return the ADOPID
     */
    public String getADOPID() {
        return ADOPID;
    }

    /**
     * @param ADOPID the ADOPID to set
     */
    public void setADOPID(String ADOPID) {
        this.ADOPID = ADOPID;
    }

    /**
     * @return String return the DPUSERID
     */
    public String getDPUSERID() {
        return DPUSERID;
    }

    /**
     * @param DPUSERID the DPUSERID to set
     */
    public void setDPUSERID(String DPUSERID) {
        this.DPUSERID = DPUSERID;
    }

    /**
     * @return String return the DPTXDATE
     */
    public String getDPTXDATE() {
        return DPTXDATE;
    }

    /**
     * @param DPTXDATE the DPTXDATE to set
     */
    public void setDPTXDATE(String DPTXDATE) {
        this.DPTXDATE = DPTXDATE;
    }

    /**
     * @return String return the DPTXTIME
     */
    public String getDPTXTIME() {
        return DPTXTIME;
    }

    /**
     * @param DPTXTIME the DPTXTIME to set
     */
    public void setDPTXTIME(String DPTXTIME) {
        this.DPTXTIME = DPTXTIME;
    }

    /**
     * @return String return the DPWDAC
     */
    public String getDPWDAC() {
        return DPWDAC;
    }

    /**
     * @param DPWDAC the DPWDAC to set
     */
    public void setDPWDAC(String DPWDAC) {
        this.DPWDAC = DPWDAC;
    }

    /**
     * @return String return the DPSVBH
     */
    public String getDPSVBH() {
        return DPSVBH;
    }

    /**
     * @param DPSVBH the DPSVBH to set
     */
    public void setDPSVBH(String DPSVBH) {
        this.DPSVBH = DPSVBH;
    }

    /**
     * @return String return the DPSVAC
     */
    public String getDPSVAC() {
        return DPSVAC;
    }

    /**
     * @param DPSVAC the DPSVAC to set
     */
    public void setDPSVAC(String DPSVAC) {
        this.DPSVAC = DPSVAC;
    }

    /**
     * @return String return the DPTXAMT
     */
    public String getDPTXAMT() {
        return DPTXAMT;
    }

    /**
     * @param DPTXAMT the DPTXAMT to set
     */
    public void setDPTXAMT(String DPTXAMT) {
        this.DPTXAMT = DPTXAMT;
    }

    /**
     * @return String return the DPTXMEMO
     */
    public String getDPTXMEMO() {
        return DPTXMEMO;
    }

    /**
     * @param DPTXMEMO the DPTXMEMO to set
     */
    public void setDPTXMEMO(String DPTXMEMO) {
        this.DPTXMEMO = DPTXMEMO;
    }

    /**
     * @return String return the DPTXMAILS
     */
    public String getDPTXMAILS() {
        return DPTXMAILS;
    }

    /**
     * @param DPTXMAILS the DPTXMAILS to set
     */
    public void setDPTXMAILS(String DPTXMAILS) {
        this.DPTXMAILS = DPTXMAILS;
    }

    /**
     * @return String return the DPTXMAILMEMO
     */
    public String getDPTXMAILMEMO() {
        return DPTXMAILMEMO;
    }

    /**
     * @param DPTXMAILMEMO the DPTXMAILMEMO to set
     */
    public void setDPTXMAILMEMO(String DPTXMAILMEMO) {
        this.DPTXMAILMEMO = DPTXMAILMEMO;
    }

    /**
     * @return String return the DPSCHID
     */
    public String getDPSCHID() {
        return DPSCHID;
    }

    /**
     * @param DPSCHID the DPSCHID to set
     */
    public void setDPSCHID(String DPSCHID) {
        this.DPSCHID = DPSCHID;
    }

    /**
     * @return String return the DPEFEE
     */
    public String getDPEFEE() {
        return DPEFEE;
    }

    /**
     * @param DPEFEE the DPEFEE to set
     */
    public void setDPEFEE(String DPEFEE) {
        this.DPEFEE = DPEFEE;
    }

    /**
     * @return String return the PCSEQ
     */
    public String getPCSEQ() {
        return PCSEQ;
    }

    /**
     * @param PCSEQ the PCSEQ to set
     */
    public void setPCSEQ(String PCSEQ) {
        this.PCSEQ = PCSEQ;
    }

    /**
     * @return String return the DPTXNO
     */
    public String getDPTXNO() {
        return DPTXNO;
    }

    /**
     * @param DPTXNO the DPTXNO to set
     */
    public void setDPTXNO(String DPTXNO) {
        this.DPTXNO = DPTXNO;
    }

    /**
     * @return String return the DPTXCODE
     */
    public String getDPTXCODE() {
        return DPTXCODE;
    }

    /**
     * @param DPTXCODE the DPTXCODE to set
     */
    public void setDPTXCODE(String DPTXCODE) {
        this.DPTXCODE = DPTXCODE;
    }

    /**
     * @return String return the DPTITAINFO
     */
    public String getDPTITAINFO() {
        return DPTITAINFO;
    }

    /**
     * @param DPTITAINFO the DPTITAINFO to set
     */
    public void setDPTITAINFO(String DPTITAINFO) {
        this.DPTITAINFO = DPTITAINFO;
    }

    /**
     * @return String return the DPTOTAINFO
     */
    public String getDPTOTAINFO() {
        return DPTOTAINFO;
    }

    /**
     * @param DPTOTAINFO the DPTOTAINFO to set
     */
    public void setDPTOTAINFO(String DPTOTAINFO) {
        this.DPTOTAINFO = DPTOTAINFO;
    }

    /**
     * @return String return the DPREMAIL
     */
    public String getDPREMAIL() {
        return DPREMAIL;
    }

    /**
     * @param DPREMAIL the DPREMAIL to set
     */
    public void setDPREMAIL(String DPREMAIL) {
        this.DPREMAIL = DPREMAIL;
    }

    /**
     * @return String return the DPTXSTATUS
     */
    public String getDPTXSTATUS() {
        return DPTXSTATUS;
    }

    /**
     * @param DPTXSTATUS the DPTXSTATUS to set
     */
    public void setDPTXSTATUS(String DPTXSTATUS) {
        this.DPTXSTATUS = DPTXSTATUS;
    }

    /**
     * @return String return the DPEXCODE
     */
    public String getDPEXCODE() {
        return DPEXCODE;
    }

    /**
     * @param DPEXCODE the DPEXCODE to set
     */
    public void setDPEXCODE(String DPEXCODE) {
        this.DPEXCODE = DPEXCODE;
    }

    /**
     * @return String return the LASTDATE
     */
    public String getLASTDATE() {
        return LASTDATE;
    }

    /**
     * @param LASTDATE the LASTDATE to set
     */
    public void setLASTDATE(String LASTDATE) {
        this.LASTDATE = LASTDATE;
    }

    /**
     * @return String return the LASTTIME
     */
    public String getLASTTIME() {
        return LASTTIME;
    }

    /**
     * @param LASTTIME the LASTTIME to set
     */
    public void setLASTTIME(String LASTTIME) {
        this.LASTTIME = LASTTIME;
    }

    /**
     * @return String return the LOGINTYPE
     */
    public String getLOGINTYPE() {
        return LOGINTYPE;
    }

    /**
     * @param LOGINTYPE the LOGINTYPE to set
     */
    public void setLOGINTYPE(String LOGINTYPE) {
        this.LOGINTYPE = LOGINTYPE;
    }

}
