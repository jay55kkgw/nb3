package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Since;


/**
 * <p>
 * TXNTWSCHPAYDATA class.
 * </p>
 *
 * @author Alison
 * @version V1.0
 */
@Entity
@Table(name = "TXNTWSCHPAYDATA")
public class TXNTWSCHPAYDATA implements Serializable {

	@EmbeddedId
    private TXNTWSCHPAYDATAIDENTITY TWSCHPAYDATAIDENTITY;

	private String ADOPID;

	private String DPWDAC;

	private String DPSVBH;

	private String DPSVAC;

	private String DPTXAMT;

	private String DPTXMEMO;

	private String DPTXMAILS;

	private String DPTXMAILMEMO;

	private String DPTXCODE;

	private String XMLCA;

	private String XMLCN;

	private String MAC;

	private String DPTXDATE;

	private String DPTXTIME;

	private String DPSEQ;

	private String ADTXNO;

	@Since(1)
	private String DPTITAINFO;

	@Since(1)
	private String DPTOTAINFO;

	private String DPSTANNO;

	private String DPEFEE;

	private String DPTXSTATUS;

	private String DPEXCODE;

	private String DPRESEND;

	private String LASTDATE;

	private String LASTTIME;

	private String MSADDR;

	/**
	 * 重送次數, for UI 顯示之擴充欄位
	 */
	@Transient
	private int RESENDCOUNT;
	
	 /**
     * <p>getTWSCHPAYDATAIDENTITY.</p>
     *
     * @return a {@link fstop.orm.po.TXNTWSCHPAYDATAIDENTITY} object.
     */
    public TXNTWSCHPAYDATAIDENTITY getTWSCHPAYDATAIDENTITY() {
        return TWSCHPAYDATAIDENTITY;
    }

    /**
     * <p>setTWSCHPAYDATAIDENTITY.</p>
     *
     * @param tWSCHPAYDATAIDENTITY a {@link fstop.orm.po.TXNTWSCHPAYDATAIDENTITY} object.
     */
    public void setTWSCHPAYDATAIDENTITY(TXNTWSCHPAYDATAIDENTITY tWSCHPAYDATAIDENTITY) {
        TWSCHPAYDATAIDENTITY = tWSCHPAYDATAIDENTITY;
    }

	/**
	 * <p>
	 * getADOPID.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADOPID() {
		return ADOPID;
	}

	/**
	 * <p>
	 * setADOPID.
	 * </p>
	 *
	 * @param adopid a {@link java.lang.String} object.
	 */
	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	/**
	 * <p>
	 * getDPWDAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPWDAC() {
		return DPWDAC;
	}

	/**
	 * <p>
	 * setDPWDAC.
	 * </p>
	 *
	 * @param dpwdac a {@link java.lang.String} object.
	 */
	public void setDPWDAC(String dpwdac) {
		DPWDAC = dpwdac;
	}

	/**
	 * <p>
	 * getDPSVBH.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPSVBH() {
		return DPSVBH;
	}

	/**
	 * <p>
	 * setDPSVBH.
	 * </p>
	 *
	 * @param dpsvbh a {@link java.lang.String} object.
	 */
	public void setDPSVBH(String dpsvbh) {
		DPSVBH = dpsvbh;
	}

	/**
	 * <p>
	 * getDPSVAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPSVAC() {
		return DPSVAC;
	}

	/**
	 * <p>
	 * setDPSVAC.
	 * </p>
	 *
	 * @param dpsvac a {@link java.lang.String} object.
	 */
	public void setDPSVAC(String dpsvac) {
		DPSVAC = dpsvac;
	}

	/**
	 * <p>
	 * getDPTXAMT.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXAMT() {
		return DPTXAMT;
	}

	/**
	 * <p>
	 * setDPTXAMT.
	 * </p>
	 *
	 * @param dptxamt a {@link java.lang.String} object.
	 */
	public void setDPTXAMT(String dptxamt) {
		DPTXAMT = dptxamt;
	}

	/**
	 * <p>
	 * getDPTXMEMO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXMEMO() {
		return DPTXMEMO;
	}

	/**
	 * <p>
	 * setDPTXMEMO.
	 * </p>
	 *
	 * @param dptxmemo a {@link java.lang.String} object.
	 */
	public void setDPTXMEMO(String dptxmemo) {
		DPTXMEMO = dptxmemo;
	}

	/**
	 * <p>
	 * getDPTXMAILS.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXMAILS() {
		return DPTXMAILS;
	}

	/**
	 * <p>
	 * setDPTXMAILS.
	 * </p>
	 *
	 * @param dptxmails a {@link java.lang.String} object.
	 */
	public void setDPTXMAILS(String dptxmails) {
		DPTXMAILS = dptxmails;
	}

	/**
	 * <p>
	 * getDPTXMAILMEMO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXMAILMEMO() {
		return DPTXMAILMEMO;
	}

	/**
	 * <p>
	 * setDPTXMAILMEMO.
	 * </p>
	 *
	 * @param dptxmailmemo a {@link java.lang.String} object.
	 */
	public void setDPTXMAILMEMO(String dptxmailmemo) {
		DPTXMAILMEMO = dptxmailmemo;
	}

	/**
	 * <p>
	 * getDPTXCODE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXCODE() {
		return DPTXCODE;
	}

	/**
	 * <p>
	 * setDPTXCODE.
	 * </p>
	 *
	 * @param dptxcode a {@link java.lang.String} object.
	 */
	public void setDPTXCODE(String dptxcode) {
		DPTXCODE = dptxcode;
	}

	/**
	 * <p>
	 * getXMLCA.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getXMLCA() {
		return XMLCA;
	}

	/**
	 * <p>
	 * setXMLCA.
	 * </p>
	 *
	 * @param xmlca a {@link java.lang.String} object.
	 */
	public void setXMLCA(String xmlca) {
		XMLCA = xmlca;
	}

	/**
	 * <p>
	 * getXMLCN.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getXMLCN() {
		return XMLCN;
	}

	/**
	 * <p>
	 * setXMLCN.
	 * </p>
	 *
	 * @param xmlcn a {@link java.lang.String} object.
	 */
	public void setXMLCN(String xmlcn) {
		XMLCN = xmlcn;
	}

	/**
	 * <p>
	 * getMAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMAC() {
		return MAC;
	}

	/**
	 * <p>
	 * setMAC.
	 * </p>
	 *
	 * @param mac a {@link java.lang.String} object.
	 */
	public void setMAC(String mac) {
		MAC = mac;
	}

	/**
	 * <p>
	 * getDPTXDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXDATE() {
		return DPTXDATE;
	}

	/**
	 * <p>
	 * setDPTXDATE.
	 * </p>
	 *
	 * @param dptxdate a {@link java.lang.String} object.
	 */
	public void setDPTXDATE(String dptxdate) {
		DPTXDATE = dptxdate;
	}

	/**
	 * <p>
	 * getDPTXTIME.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXTIME() {
		return DPTXTIME;
	}

	/**
	 * <p>
	 * setDPTXTIME.
	 * </p>
	 *
	 * @param dptxtime a {@link java.lang.String} object.
	 */
	public void setDPTXTIME(String dptxtime) {
		DPTXTIME = dptxtime;
	}

	/**
	 * <p>
	 * getDPSEQ.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPSEQ() {
		return DPSEQ;
	}

	/**
	 * <p>
	 * setDPSEQ.
	 * </p>
	 *
	 * @param dpseq a {@link java.lang.String} object.
	 */
	public void setDPSEQ(String dpseq) {
		DPSEQ = dpseq;
	}

	/**
	 * <p>
	 * getADTXNO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADTXNO() {
		return ADTXNO;
	}

	/**
	 * <p>
	 * setADTXNO.
	 * </p>
	 *
	 * @param adtxno a {@link java.lang.String} object.
	 */
	public void setADTXNO(String adtxno) {
		ADTXNO = adtxno;
	}

	/**
	 * <p>
	 * getDPTITAINFO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTITAINFO() {
		return DPTITAINFO;
	}

	/**
	 * <p>
	 * setDPTITAINFO.
	 * </p>
	 *
	 * @param dptitainfo a {@link java.lang.String} object.
	 */
	public void setDPTITAINFO(String dptitainfo) {
		DPTITAINFO = dptitainfo;
	}

	/**
	 * <p>
	 * getDPTOTAINFO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTOTAINFO() {
		return DPTOTAINFO;
	}

	/**
	 * <p>
	 * setDPTOTAINFO.
	 * </p>
	 *
	 * @param dptotainfo a {@link java.lang.String} object.
	 */
	public void setDPTOTAINFO(String dptotainfo) {
		DPTOTAINFO = dptotainfo;
	}

	/**
	 * <p>
	 * getDPSTANNO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPSTANNO() {
		return DPSTANNO;
	}

	/**
	 * <p>
	 * setDPSTANNO.
	 * </p>
	 *
	 * @param dpstanno a {@link java.lang.String} object.
	 */
	public void setDPSTANNO(String dpstanno) {
		DPSTANNO = dpstanno;
	}

	/**
	 * <p>
	 * getDPEFEE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPEFEE() {
		return DPEFEE;
	}

	/**
	 * <p>
	 * setDPEFEE.
	 * </p>
	 *
	 * @param dpefee a {@link java.lang.String} object.
	 */
	public void setDPEFEE(String dpefee) {
		DPEFEE = dpefee;
	}

	/**
	 * <p>
	 * getDPTXSTATUS.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXSTATUS() {
		return DPTXSTATUS;
	}

	/**
	 * <p>
	 * setDPTXSTATUS.
	 * </p>
	 *
	 * @param dptxstatus a {@link java.lang.String} object.
	 */
	public void setDPTXSTATUS(String dptxstatus) {
		DPTXSTATUS = dptxstatus;
	}

	/**
	 * <p>
	 * getDPEXCODE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPEXCODE() {
		return DPEXCODE;
	}

	/**
	 * <p>
	 * setDPEXCODE.
	 * </p>
	 *
	 * @param dpexcode a {@link java.lang.String} object.
	 */
	public void setDPEXCODE(String dpexcode) {
		DPEXCODE = dpexcode;
	}

	/**
	 * <p>
	 * getDPRESEND.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPRESEND() {
		return DPRESEND;
	}

	/**
	 * <p>
	 * setDPRESEND.
	 * </p>
	 *
	 * @param dpresend a {@link java.lang.String} object.
	 */
	public void setDPRESEND(String dpresend) {
		DPRESEND = dpresend;
	}

	/**
	 * <p>
	 * getLASTDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>
	 * setLASTDATE.
	 * </p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>
	 * getLASTTIME.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>
	 * setLASTTIME.
	 * </p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getMSADDR() {
		return MSADDR;
	}

	public void setMSADDR(String mSADDR) {
		MSADDR = mSADDR;
	}

	public int getRESENDCOUNT() {
		return RESENDCOUNT;
	}

	public void setRESENDCOUNT(int rESENDCOUNT) {
		RESENDCOUNT = rESENDCOUNT;
	}
}
