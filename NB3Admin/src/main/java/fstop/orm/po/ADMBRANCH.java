package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

/**
 * <p>ADMBRANCH class.</p>
 *
 * @author chiensj
 * @version V1.0
 * @author jinhanhuang 2021
 * @version V2.0
 */
@Entity
@Data
@Table(name = "ADMBRANCH")
public class ADMBRANCH implements Serializable {

	@Id
	private String ADBRANCHID;//分行代號

	private String ADBRANCHNAME;//分行名稱(繁中)
	
	private String ADBRANENGNAME;//分行名稱(英)
		
	private String ADBRANCHSNAME;//分行名稱(簡中)

	private String LASTUSER;//最後修改者

	private String LASTDATE;//最後修改日

	private String LASTTIME;//最後修改時間

	private String POSTCODE;//郵遞區號

	private String ADDRESS;//地址(繁)
	
	private String ADDRESSENG;//地址(英)
	
	private String ADDRESSCHS;//地址(簡)
	
	private String TELNUM;//電話
	
	private String BANKCODE;//行庫及分行碼
}
