package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Since;

import lombok.Data;

/**
 * <p>交易紀錄失敗統計 PO</p>
 * 
 * @author VincentHuang 
 * 
 */

@Data
@Entity
@Table(name = "TRNS_NNB_CUSDATA")
public class TRNS_NNB_CUSDATA implements Serializable {

//	使用者ID
    @Id
    private String CUSIDN;
    
//	使用者設定檔是否成功
    private String TXNUSER;
    
//	台幣轉出紀錄檔是否成功
    private String TXNTWRECORD;
    
//	台幣預約檔是否成功
    private String TXNTWSCHPAY;
    
//	外幣轉出紀錄檔是否成功
    private String TXNFXRECORD;
    
//	外幣預約檔是否成功
    private String TXNFXSCHPAY;
    
//	黃金轉出紀錄檔是否成功
    private String TXNGDRECORD;
    
//	常用帳號是否成功
    private String TXNTRACCSET;
    
//	Email紀錄檔是否成功
    private String ADMMAILLOG;
    
//	通訊錄設定檔是否成功
    private String TXNADDRESSBOOK;
    
//	KYC設定檔是否成功
    private String TXNCUSINVATTRIB;
    
//	KYC歷史設定檔是否成功
    private String TXNCUSINVATTRHIST;
    
//	Insert舊NNB資料表白名單是否成功
    private String NB3USER;
    
//	推撥設定檔是否成功
    private String TXNPHONETOKEN;
    
    
}