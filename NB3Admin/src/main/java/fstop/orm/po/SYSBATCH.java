package fstop.orm.po;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "SYSBATCH")
public class SYSBATCH {
    @Id
    private String ADBATID;
    
    private String ADBATNAME;
    private String ADBATSDATE;
    private String ADBATSTIME;
    private String ADBATEDATE;
    private String ADBATETIME;
    private String LOCKSTATUS;
    private String INVALIDFLAG;
    private String PEROID;
    private String EXECUTIONTIME;
    private String EXECMESSAGE;
}
