package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * <p>ADMSYSCODEM class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMSYSCODEM")
public class ADMSYSCODEM implements Serializable{

	private String CODEKIND;	//代碼種類
	private String CODEPARENT;	//父代碼種類
	private String CODENAME	;//代碼內容
	private String CODESNAME;	//代碼簡稱
	private String ISVISIBLE;	//是否顯示
	private String CODESYSID;	//系統別
	private String EDITFLAG;	//可維護
	/**
	 * <p>getCODEKIND.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	@Id
	public String getCODEKIND() {
		return CODEKIND;
	}
	/**
	 * <p>setCODEKIND.</p>
	 *
	 * @param codekind a {@link java.lang.String} object.
	 */
	public void setCODEKIND(String codekind) {
		CODEKIND = codekind;
	}
	/**
	 * <p>getCODEPARENT.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCODEPARENT() {
		return CODEPARENT;
	}
	/**
	 * <p>setCODEPARENT.</p>
	 *
	 * @param codeparent a {@link java.lang.String} object.
	 */
	public void setCODEPARENT(String codeparent) {
		CODEPARENT = codeparent;
	}
	/**
	 * <p>getCODENAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCODENAME() {
		return CODENAME;
	}
	/**
	 * <p>setCODENAME.</p>
	 *
	 * @param codename a {@link java.lang.String} object.
	 */
	public void setCODENAME(String codename) {
		CODENAME = codename;
	}
	/**
	 * <p>getCODESNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCODESNAME() {
		return CODESNAME;
	}
	/**
	 * <p>setCODESNAME.</p>
	 *
	 * @param codesname a {@link java.lang.String} object.
	 */
	public void setCODESNAME(String codesname) {
		CODESNAME = codesname;
	}
	/**
	 * <p>getISVISIBLE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getISVISIBLE() {
		return ISVISIBLE;
	}
	/**
	 * <p>setISVISIBLE.</p>
	 *
	 * @param isvisible a {@link java.lang.String} object.
	 */
	public void setISVISIBLE(String isvisible) {
		ISVISIBLE = isvisible;
	}
	/**
	 * <p>getCODESYSID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCODESYSID() {
		return CODESYSID;
	}
	/**
	 * <p>setCODESYSID.</p>
	 *
	 * @param codesysid a {@link java.lang.String} object.
	 */
	public void setCODESYSID(String codesysid) {
		CODESYSID = codesysid;
	}
	/**
	 * <p>getEDITFLAG.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getEDITFLAG() {
		return EDITFLAG;
	}
	/**
	 * <p>setEDITFLAG.</p>
	 *
	 * @param editflag a {@link java.lang.String} object.
	 */
	public void setEDITFLAG(String editflag) {
		EDITFLAG = editflag;
	}
	
	

}
