package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>SYSOP class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "SYSOP")
public class SYSOP implements Serializable {

	@Id
	// @GeneratedValue(strategy = GenerationType.AUTO)
	@NotBlank(message = "選單代碼不可為空值.")
	private String ADOPID;

	@NotBlank(message = "選單名稱不可為空值.")
	private String ADOPNAME;

	private String ADOPMEMO;

	private String ADOPALIVE;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	private int ADSEQ=0;

	private String ADOPGROUP = "";
	@NotBlank(message = "URL不可為空值.")
	private String URL = "";

	/**
	 * 只在 View 中使用，Table 沒有對應的欄位
	 */
	@Transient
	private int ChildCount;

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("adopid", getADOPID())
				.toString();
	}

	/**
	 * <p>getADOPALIVE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADOPALIVE() {
		return ADOPALIVE;
	}

	/**
	 * <p>setADOPALIVE.</p>
	 *
	 * @param adopalive a {@link java.lang.String} object.
	 */
	public void setADOPALIVE(String adopalive) {
		ADOPALIVE = adopalive;
	}

	/**
	 * <p>getADOPID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADOPID() {
		return ADOPID;
	}

	/**
	 * <p>setADOPID.</p>
	 *
	 * @param adopid a {@link java.lang.String} object.
	 */
	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	/**
	 * <p>getADOPMEMO.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADOPMEMO() {
		return ADOPMEMO;
	}

	/**
	 * <p>setADOPMEMO.</p>
	 *
	 * @param adopmemo a {@link java.lang.String} object.
	 */
	public void setADOPMEMO(String adopmemo) {
		ADOPMEMO = adopmemo;
	}

	/**
	 * <p>getADOPNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADOPNAME() {
		return ADOPNAME;
	}

	/**
	 * <p>setADOPNAME.</p>
	 *
	 * @param adopname a {@link java.lang.String} object.
	 */
	public void setADOPNAME(String adopname) {
		ADOPNAME = adopname;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}

	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

    /**
     * <p>getADSEQ.</p>
     *
     * @return the aDSEQ
     */
    public int getADSEQ() {
        return ADSEQ;
    }

    /**
     * <p>setADSEQ.</p>
     *
     * @param aDSEQ the aDSEQ to set
     */
    public void setADSEQ(int aDSEQ) {
        ADSEQ = aDSEQ;
	}
	
    /**
     * <p>getADOPGROUP.</p>
     *
     * @return the aDOPGROUP
     */
    public String getADOPGROUP() {
        return ADOPGROUP;
    }

    /**
     * <p>setADOPGROUP.</p>
     *
     * @param aDOPGROUP the aDOPGROUP to set
     */
    public void setADOPGROUP(String aDOPGROUP) {
        ADOPGROUP = aDOPGROUP;
    }

	/**
	 * <p>getChildCount.</p>
	 *
	 * @return a int.
	 */
	public int getChildCount() {
		return ChildCount;
	}

	/**
	 * <p>setChildCount.</p>
	 *
	 * @param childCount a int.
	 */
	public void setChildCount(int childCount) {
		ChildCount = childCount;
	}

	/**
	 * <p>getURL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getURL() {
		return URL;
	}

	/**
	 * <p>setURL.</p>
	 *
	 * @param uRL a {@link java.lang.String} object.
	 */
	public void setURL(String uRL) {
		URL = uRL;
	}
}
