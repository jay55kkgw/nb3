package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Since;


/**
 * <p>
 * TXNFXSCHPAYDATA class.
 * </p>
 *
 * @author Alison
 * @version V1.0
 */
@Entity
@Table(name = "TXNFXSCHPAYDATA")
public class TXNFXSCHPAYDATA implements Serializable {

	@EmbeddedId
    private TXNFXSCHPAYDATAIDENTITY FXSCHPAYDATAIDENTITY;

	private String ADOPID;

	private String FXWDAC;

	private String FXWDCURR;

	private String FXWDAMT;

	private String FXSVBH;

	private String FXSVAC;

	private String FXSVCURR;

	private String FXSVAMT;

	private String FXTXMEMO;

	private String FXTXMAILS;

	private String FXTXMAILMEMO;

	private String FXTXCODE;

	private String XMLCA;

	private String XMLCN;

	private String MAC;

	private String FXTXDATE;

	private String FXTXTIME;

	private String PCSEQ;

	private String ADTXNO;

	@Since(1)
	private String FXTITAINFO;

	@Since(1)
	private String FXTOTAINFO;

	private String FXEFEE;

	private String FXTELFEE;

	private String FXEXRATE;

	private String FXTXSTATUS;
	
	private String FXEXCODE;

	private String FXRESEND;

	private String FXMSGSEQNO;

	private String FXCERT;

	private String LASTDATE;

	private String LASTTIME;

	private String FXEFEECCY;

	private String FXOURCHG;

	private String MSADDR;

	/**
	 * 重送次數, for UI 顯示之擴充欄位
	 */
	@Transient
	private int RESENDCOUNT;
	
	 /**
     * <p>getFXSCHPAYDATAIDENTITY.</p>
     *
     * @return a {@link fstop.orm.po.TXNFXSCHPAYDATAIDENTITY} object.
     */
    public TXNFXSCHPAYDATAIDENTITY getFXSCHPAYDATAIDENTITY() {
        return FXSCHPAYDATAIDENTITY;
    }

    /**
     * <p>setFXSCHPAYDATAIDENTITY.</p>
     *
     * @param fXSCHPAYDATAIDENTITY a {@link fstop.orm.po.TXNFXSCHPAYDATAIDENTITY} object.
     */
    public void setFXSCHPAYDATAIDENTITY(TXNFXSCHPAYDATAIDENTITY fXSCHPAYDATAIDENTITY) {
        FXSCHPAYDATAIDENTITY = fXSCHPAYDATAIDENTITY;
	}

	/**
	 * <p>
	 * getADOPID.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADOPID() {
		return ADOPID;
	}

	/**
	 * <p>
	 * setADOPID.
	 * </p>
	 *
	 * @param adopid a {@link java.lang.String} object.
	 */
	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	/**
	 * <p>
	 * getFXWDAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXWDAC() {
		return FXWDAC;
	}

	/**
	 * <p>
	 * setFXWDAC.
	 * </p>
	 *
	 * @param fxwdac a {@link java.lang.String} object.
	 */
	public void setFXWDAC(String fxwdac) {
		FXWDAC = fxwdac;
	}

	/**
	 * <p>
	 * getFXWDCURR.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXWDCURR() {
		return FXWDCURR;
	}

	/**
	 * <p>
	 * setFXWDCURR.
	 * </p>
	 *
	 * @param fxwdcurr a {@link java.lang.String} object.
	 */
	public void setFXWDCURR(String fxwdcurr) {
		FXWDCURR = fxwdcurr;
	}

	/**
	 * <p>
	 * getFXWDAMT.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXWDAMT() {
		return FXWDAMT;
	}

	/**
	 * <p>
	 * setFXWDAMT.
	 * </p>
	 *
	 * @param fxwdamt a {@link java.lang.String} object.
	 */
	public void setFXWDAMT(String fxwdamt) {
		FXWDAMT = fxwdamt;
	}

	/**
	 * <p>
	 * getFXSVBH.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXSVBH() {
		return FXSVBH;
	}

	/**
	 * <p>
	 * setFXSVBH.
	 * </p>
	 *
	 * @param fxsvbh a {@link java.lang.String} object.
	 */
	public void setFXSVBH(String fxsvbh) {
		FXSVBH = fxsvbh;
	}

	/**
	 * <p>
	 * getFXSVAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXSVAC() {
		return FXSVAC;
	}

	/**
	 * <p>
	 * setFXSVAC.
	 * </p>
	 *
	 * @param fxsvac a {@link java.lang.String} object.
	 */
	public void setFXSVAC(String fxsvac) {
		FXSVAC = fxsvac;
	}

	/**
	 * <p>
	 * getFXSVCURR.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXSVCURR() {
		return FXSVCURR;
	}

	/**
	 * <p>
	 * setFXSVCURR.
	 * </p>
	 *
	 * @param fxsvcurr a {@link java.lang.String} object.
	 */
	public void setFXSVCURR(String fxsvcurr) {
		FXSVCURR = fxsvcurr;
	}

	/**
	 * <p>
	 * getFXSVAMT.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXSVAMT() {
		return FXSVAMT;
	}

	

	/**
	 * <p>
	 * setFXSVAMT.
	 * </p>
	 *
	 * @param fxsvamt a {@link java.lang.String} object.
	 */
	public void setFXSVAMT(String fxsvamt) {
		FXSVAMT = fxsvamt;
	}


	/**
	 * <p>
	 * getFXTXMEMO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXMEMO() {
		return FXTXMEMO;
	}

	/**
	 * <p>
	 * setFXTXMEMO.
	 * </p>
	 *
	 * @param dptxmailmemo a {@link java.lang.String} object.
	 */
	public void setFXTXMEMO(String dptxmailmemo) {
		FXTXMEMO = dptxmailmemo;
	}

	/**
	 * <p>
	 * getFXTXMAILS.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXMAILS() {
		return FXTXMAILS;
	}

	/**
	 * <p>
	 * setFXTXMAILS.
	 * </p>
	 *
	 * @param fxtxmails a {@link java.lang.String} object.
	 */
	public void setFXTXMAILS(String fxtxmails) {
		FXTXMAILS = fxtxmails;
	}

	/**
	 * <p>
	 * getFXTXMAILMEMO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXMAILMEMO() {
		return FXTXMAILMEMO;
	}

	/**
	 * <p>
	 * setFXTXMAILMEMO.
	 * </p>
	 *
	 * @param fxtxmailmemo a {@link java.lang.String} object.
	 */
	public void setFXTXMAILMEMO(String fxtxmailmemo) {
		FXTXMAILMEMO = fxtxmailmemo;
	}

	/**
	 * <p>
	 * getFXTXCODE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXCODE() {
		return FXTXCODE;
	}

	/**
	 * <p>
	 * setFXTXCODE.
	 * </p>
	 *
	 * @param fxtxcode a {@link java.lang.String} object.
	 */
	public void setFXTXCODE(String fxtxcode) {
		FXTXCODE = fxtxcode;
	}

	/**
	 * <p>
	 * getXMLCA.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getXMLCA() {
		return XMLCA;
	}

	/**
	 * <p>
	 * setXMLCA.
	 * </p>
	 *
	 * @param xmlca a {@link java.lang.String} object.
	 */
	public void setXMLCA(String xmlca) {
		XMLCA = xmlca;
	}

	/**
	 * <p>
	 * getXMLCN.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getXMLCN() {
		return XMLCN;
	}

	/**
	 * <p>
	 * setXMLCN.
	 * </p>
	 *
	 * @param xmlcn a {@link java.lang.String} object.
	 */
	public void setXMLCN(String xmlcn) {
		XMLCN = xmlcn;
	}

	/**
	 * <p>
	 * getMAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMAC() {
		return MAC;
	}

	/**
	 * <p>
	 * setMAC.
	 * </p>
	 *
	 * @param mac a {@link java.lang.String} object.
	 */
	public void setMAC(String mac) {
		MAC = mac;
	}

	/**
	 * <p>
	 * getFXTXDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXDATE() {
		return FXTXDATE;
	}

	/**
	 * <p>
	 * setFXTXDATE.
	 * </p>
	 *
	 * @param fxtxdate a {@link java.lang.String} object.
	 */
	public void setFXTXDATE(String fxtxdate) {
		FXTXDATE = fxtxdate;
	}

	/**
	 * <p>
	 * getFXTXTIME.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXTIME() {
		return FXTXTIME;
	}

	/**
	 * <p>
	 * setFXTXTIME.
	 * </p>
	 *
	 * @param fxtxtime a {@link java.lang.String} object.
	 */
	public void setFXTXTIME(String fxtxtime) {
		FXTXTIME = fxtxtime;
	}

	/**
	 * <p>
	 * getPCSEQ.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPCSEQ() {
		return PCSEQ;
	}

	/**
	 * <p>
	 * setPCSEQ.
	 * </p>
	 *
	 * @param pcseq a {@link java.lang.String} object.
	 */
	public void setPCSEQ(String pcseq) {
		PCSEQ = pcseq;
	}

	/**
	 * <p>
	 * getADTXNO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADTXNO() {
		return ADTXNO;
	}

	/**
	 * <p>
	 * setADTXNO.
	 * </p>
	 *
	 * @param adtxno a {@link java.lang.String} object.
	 */
	public void setADTXNO(String adtxno) {
		ADTXNO = adtxno;
	}

	/**
	 * <p>
	 * getFXTITAINFO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTITAINFO() {
		return FXTITAINFO;
	}

	/**
	 * <p>
	 * setFXTITAINFO.
	 * </p>
	 *
	 * @param fxtitainfo a {@link java.lang.String} object.
	 */
	public void setFXTITAINFO(String fxtitainfo) {
		FXTITAINFO = fxtitainfo;
	}

	/**
	 * <p>
	 * getFXTOTAINFO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTOTAINFO() {
		return FXTOTAINFO;
	}

	/**
	 * <p>
	 * setFXTOTAINFO.
	 * </p>
	 *
	 * @param fxtotainfo a {@link java.lang.String} object.
	 */
	public void setFXTOTAINFO(String fxtotainfo) {
		FXTOTAINFO = fxtotainfo;
	}

	/**
	 * <p>
	 * getFXEFEE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXEFEE() {
		return FXEFEE;
	}

	/**
	 * <p>
	 * setFXEFEE.
	 * </p>
	 *
	 * @param fxefee a {@link java.lang.String} object.
	 */
	public void setFXEFEE(String fxefee) {
		FXEFEE = fxefee;
	}

	/**
	 * <p>
	 * getFXTELFEE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTELFEE() {
		return FXTELFEE;
	}

	/**
	 * <p>
	 * setFXTELFEE.
	 * </p>
	 *
	 * @param fxtelfee a {@link java.lang.String} object.
	 */
	public void setFXTELFEE(String fxtelfee) {
		FXTELFEE = fxtelfee;
	}

	/**
	 * <p>
	 * getFXEXRATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXEXRATE() {
		return FXEXRATE;
	}

	/**
	 * <p>
	 * setFXEXRATE.
	 * </p>
	 *
	 * @param fxexrate a {@link java.lang.String} object.
	 */
	public void setFXEXRATE(String fxexrate) {
		FXEXRATE = fxexrate;
	}

	/**
	 * <p>
	 * getFXTXSTATUS.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXTXSTATUS() {
		return FXTXSTATUS;
	}

	/**
	 * <p>
	 * setFXTXSTATUS.
	 * </p>
	 *
	 * @param fxtxstatus a {@link java.lang.String} object.
	 */
	public void setFXTXSTATUS(String fxtxstatus) {
		FXTXSTATUS = fxtxstatus;
	}

	/**
	 * <p>
	 * getFXEXCODE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXEXCODE() {
		return FXEXCODE;
	}

	/**
	 * <p>
	 * setFXEXCODE.
	 * </p>
	 *
	 * @param fxexcode a {@link java.lang.String} object.
	 */
	public void setFXEXCODE(String fxexcode) {
		FXEXCODE = fxexcode;
	}
	/**
	 * <p>
	 * getFXRESEND.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXRESEND() {
		return FXRESEND;
	}

	/**
	 * <p>
	 * setFXRESEND.
	 * </p>
	 *
	 * @param fxresend a {@link java.lang.String} object.
	 */
	public void setFXRESEND(String fxresend) {
		FXRESEND = fxresend;
	}
		/**
	 * <p>
	 * getFXMSGSEQNO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXMSGSEQNO() {
		return FXMSGSEQNO;
	}

	/**
	 * <p>
	 * setFXMSGSEQNO.
	 * </p>
	 *
	 * @param fxmsgseqno a {@link java.lang.String} object.
	 */
	public void setFXMSGSEQNO(String fxmsgseqno) {
		FXMSGSEQNO = fxmsgseqno;
	}
		/**
	 * <p>
	 * getFXCERT.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXCERT() {
		return FXCERT;
	}

	/**
	 * <p>
	 * setFXCERT.
	 * </p>
	 *
	 * @param fxcert a {@link java.lang.String} object.
	 */
	public void setFXCERT(String fxcert) {
		FXCERT = fxcert;
	}
			/**
	 * <p>
	 * getLASTDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>
	 * setLASTDATE.
	 * </p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}
			/**
	 * <p>
	 * getLASTTIME.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>
	 * setLASTTIME.
	 * </p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}
			/**
	 * <p>
	 * getFXEFEECCY.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXEFEECCY() {
		return FXEFEECCY;
	}

	/**
	 * <p>
	 * setFXEFEECCY.
	 * </p>
	 *
	 * @param fxefeeccy a {@link java.lang.String} object.
	 */
	public void setFXEFEECCY(String fxefeeccy) {
		FXEFEECCY = fxefeeccy;
	}
			/**
	 * <p>
	 * getFXOURCHG.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXOURCHG() {
		return FXOURCHG;
	}

	/**
	 * <p>
	 * setFXOURCHG.
	 * </p>
	 *
	 * @param fxourchg a {@link java.lang.String} object.
	 */
	public void setFXOURCHG(String fxourchg) {
		FXOURCHG = fxourchg;
	}

	public String getMSADDR() {
		return MSADDR;
	}

	public void setMSADDR(String mSADDR) {
		MSADDR = mSADDR;
	}

	public int getRESENDCOUNT() {
		return RESENDCOUNT;
	}

	public void setRESENDCOUNT(int rESENDCOUNT) {
		RESENDCOUNT = rESENDCOUNT;
	}
}
