package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 使用者軌跡記錄，TBB 這邊暫時不用
 */
@Entity
@Table(name = "USERACTIONLOG")
public class USERACTIONLOG implements Serializable {
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id    
    private Integer ID;

    private String LOGINUSER;
    private String URL;
    private String METHOD;
    private String FORMDATA;
    private String PARAMDATA;
    private String RQTIMESTAMP;
    private String RSTIMESTAMP;
    private String USERIP;
    private String OPID;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer iD) {
        ID = iD;
    }

    public String getLOGINUSER() {
        return LOGINUSER;
    }

    public void setLOGINUSER(String lOGINUSER) {
        LOGINUSER = lOGINUSER;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String uRL) {
        URL = uRL;
    }

    public String getMETHOD() {
        return METHOD;
    }

    public void setMETHOD(String mETHOD) {
        METHOD = mETHOD;
    }

    public String getFORMDATA() {
        return FORMDATA;
    }

    public void setFORMDATA(String fORMDATA) {
        FORMDATA = fORMDATA;
    }

    public String getPARAMDATA() {
        return PARAMDATA;
    }

    public void setPARAMDATA(String pARAMDATA) {
        PARAMDATA = pARAMDATA;
    }

    public String getUSERIP() {
        return USERIP;
    }

    public void setUSERIP(String uSERIP) {
        USERIP = uSERIP;
    }

    public String getRQTIMESTAMP() {
        return RQTIMESTAMP;
    }

    public void setRQTIMESTAMP(String rQTIMESTAMP) {
        RQTIMESTAMP = rQTIMESTAMP;
    }

    public String getRSTIMESTAMP() {
        return RSTIMESTAMP;
    }

    public void setRSTIMESTAMP(String rSTIMESTAMP) {
        RSTIMESTAMP = rSTIMESTAMP;
    }

    public String getOPID() {
        return OPID;
    }

    public void setOPID(String oPID) {
        OPID = oPID;
    }
}