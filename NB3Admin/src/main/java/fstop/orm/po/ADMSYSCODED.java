package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Since;

import lombok.Data;

/**
 * 上傳附件暫存檔案
 *
 * @author chiensj
 * @version V1.0
 * 20191121-Danny-處理Byte[]欄位檢核時會錯誤排除
 */
@Data
@Entity
@Table(name = "ADMSYSCODED")
public class ADMSYSCODED implements Serializable {
    public ADMSYSCODED(){
		super();
	}
	
    private static final long serialVersionUID = -76839347394374865L;
  
    @EmbeddedId
    private ADMSYSCODED_PK ADMSYSCODED_PK;	//代碼種類
	private String CODEPARENT;	//父代碼
	private String CODENAME;	//代碼內容
	private String CODESNAME;	//代碼簡稱
	private String ISENABLE;	//可使用
	private String SORTORDER;	//排序
	private String MEMO;	//備註
}
