package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.annotations.Since;

import javax.persistence.Id;


/**
 * <p>
 * TXNTWSCHPAY class.
 * </p>
 *
 * @author Alison
 * @version V1.0
 */
@Entity
@Table(name = "TXNTWSCHPAY")
public class TXNTWSCHPAY implements Serializable {

	private Integer DPSCHID;

	private String ADOPID;

	private String DPUSERID;

	private String DPTXTYPE;

	private String DPPERMTDATE;

	private String DPFDATE;

	private String DPTDATE;

	private String DPWDAC;

	private String DPSVBH;

	private String DPSVAC;

	private String DPTXAMT;

	private String DPTXMEMO;

	private String DPTXMAILS;

	private String DPTXMAILMEMO;

	private String DPTXCODE;

	private String DPSDATE;

	private String DPSTIME;

	private String DPTXSTATUS;

	private String XMLCA;

	private String XMLCN;

	private String MAC;
	@Since(1)
	private String DPTXINFO;

	private String LASTDATE;

	private String LASTTIME;

	private String DPSCHNO;

	private String LOGINTYPE;
	
	

	/**
	 * <p>getDPSCHID.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	@Id
	public Integer getDPSCHID() {
		return DPSCHID;
	}

	/**
	 * <p>setDPSCHID.</p>
	 *
	 * @param dpschid a {@link java.lang.Integer} object.
	 */
	public void setDPSCHID(Integer dpschid) {
		DPSCHID = dpschid;
	}

	/**
	 * <p>
	 * getADOPID.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADOPID() {
		return ADOPID;
	}

	/**
	 * <p>
	 * setADOPID.
	 * </p>
	 *
	 * @param adopid a {@link java.lang.String} object.
	 */
	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	/**
	 * <p>
	 * getDPUSERID.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPUSERID() {
		return DPUSERID;
	}

	/**
	 * <p>
	 * setDPUSERID.
	 * </p>
	 *
	 * @param dpuserid a {@link java.lang.String} object.
	 */
	public void setDPUSERID(String dpuserid) {
		DPUSERID = dpuserid;
	}

	/**
	 * <p>
	 * getDPTXTYPE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXTYPE() {
		return DPTXTYPE;
	}

	/**
	 * <p>
	 * setDPTXTYPE.
	 * </p>
	 *
	 * @param dptxtype a {@link java.lang.String} object.
	 */
	public void setDPTXTYPE(String dptxtype) {
		DPTXTYPE = dptxtype;
	}

	/**
	 * <p>
	 * getDPPERMTDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPPERMTDATE() {
		return DPPERMTDATE;
	}

	/**
	 * <p>
	 * setDPPERMTDATE.
	 * </p>
	 *
	 * @param dppermtdate a {@link java.lang.String} object.
	 */
	public void setDPPERMTDATE(String dppermtdate) {
		DPPERMTDATE = dppermtdate;
	}

	/**
	 * <p>
	 * getDPFDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPFDATE() {
		return DPFDATE;
	}

	/**
	 * <p>
	 * setDPFDATE.
	 * </p>
	 *
	 * @param dpfdate a {@link java.lang.String} object.
	 */
	public void setDPFDATE(String dpfdate) {
		DPFDATE = dpfdate;
	}

	/**
	 * <p>
	 * getDPTDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTDATE() {
		return DPTDATE;
	}

	/**
	 * <p>
	 * setDPTDATE.
	 * </p>
	 *
	 * @param dptdate a {@link java.lang.String} object.
	 */
	public void setDPTDATE(String dptdate) {
		DPTDATE = dptdate;
	}

	/**
	 * <p>
	 * getDPWDAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPWDAC() {
		return DPWDAC;
	}

	/**
	 * <p>
	 * setDPWDAC.
	 * </p>
	 *
	 * @param dpwdac a {@link java.lang.String} object.
	 */
	public void setDPWDAC(String dpwdac) {
		DPWDAC = dpwdac;
	}

	/**
	 * <p>
	 * getDPSVBH.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPSVBH() {
		return DPSVBH;
	}

	

	/**
	 * <p>
	 * setDPSVBH.
	 * </p>
	 *
	 * @param dpsvbh a {@link java.lang.String} object.
	 */
	public void setDPSVBH(String dpsvbh) {
		DPSVBH = dpsvbh;
	}


	/**
	 * <p>
	 * getDPSVAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPSVAC() {
		return DPSVAC;
	}

	/**
	 * <p>
	 * setDPSVAC.
	 * </p>
	 *
	 * @param dpsvac a {@link java.lang.String} object.
	 */
	public void setDPSVAC(String dpsvac) {
		DPSVAC = dpsvac;
	}

	/**
	 * <p>
	 * getDPTXAMT.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXAMT() {
		return DPTXAMT;
	}

	/**
	 * <p>
	 * setDPTXAMT.
	 * </p>
	 *
	 * @param dptxamt a {@link java.lang.String} object.
	 */
	public void setDPTXAMT(String dptxamt) {
		DPTXAMT = dptxamt;
	}

	/**
	 * <p>
	 * getDPTXMEMO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXMEMO() {
		return DPTXMEMO;
	}

	/**
	 * <p>
	 * setDPTXMEMO.
	 * </p>
	 *
	 * @param dptxmemo a {@link java.lang.String} object.
	 */
	public void setDPTXMEMO(String dptxmemo) {
		DPTXMEMO = dptxmemo;
	}

	/**
	 * <p>
	 * getDPTXMAILS.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXMAILS() {
		return DPTXMAILS;
	}

	/**
	 * <p>
	 * setDPTXMAILS.
	 * </p>
	 *
	 * @param dptxmails a {@link java.lang.String} object.
	 */
	public void setDPTXMAILS(String dptxmails) {
		DPTXMAILS = dptxmails;
	}

	/**
	 * <p>
	 * getDPTXMAILMEMO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXMAILMEMO() {
		return DPTXMAILMEMO;
	}

	/**
	 * <p>
	 * setDPTXMAILMEMO.
	 * </p>
	 *
	 * @param dptxmailmemo a {@link java.lang.String} object.
	 */
	public void setDPTXMAILMEMO(String dptxmailmemo) {
		DPTXMAILMEMO = dptxmailmemo;
	}

	/**
	 * <p>
	 * getDPTXCODE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXCODE() {
		return DPTXCODE;
	}

	/**
	 * <p>
	 * setDPTXCODE.
	 * </p>
	 *
	 * @param xmlcn a {@link java.lang.String} object.
	 */
	public void setDPTXCODE(String dptxcode) {
		DPTXCODE = dptxcode;
	}

	/**
	 * <p>
	 * getDPSDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPSDATE() {
		return DPSDATE;
	}

	/**
	 * <p>
	 * setDPSDATE.
	 * </p>
	 *
	 * @param dpsdate a {@link java.lang.String} object.
	 */
	public void setDPSDATE(String dpsdate) {
		DPSDATE = dpsdate;
	}

	/**
	 * <p>
	 * getDPSTIME.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPSTIME() {
		return DPSTIME;
	}

	/**
	 * <p>
	 * setDPSTIME.
	 * </p>
	 *
	 * @param dpstime a {@link java.lang.String} object.
	 */
	public void setDPSTIME(String dpstime) {
		DPSTIME = dpstime;
	}

	/**
	 * <p>
	 * getDPTXSTATUS.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXSTATUS() {
		return DPTXSTATUS;
	}

	/**
	 * <p>
	 * setDPTXSTATUS.
	 * </p>
	 *
	 * @param dptxstatus a {@link java.lang.String} object.
	 */
	public void setDPTXSTATUS(String dptxstatus) {
		DPTXSTATUS = dptxstatus;
	}

	/**
	 * <p>
	 * getXMLCA.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getXMLCA() {
		return XMLCA;
	}

	/**
	 * <p>
	 * setXMLCA.
	 * </p>
	 *
	 * @param fxtxcode a {@link java.lang.String} object.
	 */
	public void setXMLCA(String xmlca) {
		XMLCA = xmlca;
	}

	/**
	 * <p>
	 * getXMLCN.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getXMLCN() {
		return XMLCN;
	}

	/**
	 * <p>
	 * setXMLCN.
	 * </p>
	 *
	 * @param xmlcn a {@link java.lang.String} object.
	 */
	public void setXMLCN(String xmlcn) {
		XMLCN = xmlcn;
	}

	/**
	 * <p>
	 * getMAC.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMAC() {
		return MAC;
	}

	/**
	 * <p>
	 * setMAC.
	 * </p>
	 *
	 * @param mac a {@link java.lang.String} object.
	 */
	public void setMAC(String mac) {
		MAC = mac;
	}

	/**
	 * <p>
	 * getDPTXINFO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPTXINFO() {
		return DPTXINFO;
	}

	/**
	 * <p>
	 * setDPTXINFO.
	 * </p>
	 *
	 * @param dptxinfo a {@link java.lang.String} object.
	 */
	public void setDPTXINFO(String dptxinfo) {
		DPTXINFO = dptxinfo;
	}

	/**
	 * <p>
	 * getLASTDATE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>
	 * setLASTDATE.
	 * </p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>
	 * getLASTTIME.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>
	 * setLASTTIME.
	 * </p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>
	 * getDPSCHNO.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPSCHNO() {
		return DPSCHNO;
	}

	/**
	 * <p>
	 * setDPSCHNO.
	 * </p>
	 *
	 * @param dpschno a {@link java.lang.String} object.
	 */
	public void setDPSCHNO(String dpschno) {
		DPSCHNO = dpschno;
	}

	/**
	 * <p>
	 * getLOGINTYPE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	/**
	 * <p>
	 * setLOGINTYPE.
	 * </p>
	 *
	 * @param logintype a {@link java.lang.String} object.
	 */
	public void setLOGINTYPE(String logintype) {
		LOGINTYPE = logintype;
	}	
}
