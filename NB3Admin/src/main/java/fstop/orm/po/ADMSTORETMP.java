package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import com.google.gson.annotations.Since;

import lombok.*;

/**
 * 特店優惠主檔，已放行的特店會由 Temp 檔搬到主檔
 *
 * @author 
 * @version V1.0
 * 
 */
@Entity
@Table(name = "ADMSTORETMP")
@Getter
@Setter
public class ADMSTORETMP implements Serializable {
    @Id
    private String STID;

    private String OID;

    private String SDATE;

    private String STIME;

    private String EDATE;

    private String ETIME;
    
    // 特店類別
    private String ADTYPE;
    
    // 商店分類
    private String STTYPE;
    // 權重
    private String STWEIGHT;
    // 圖片路徑
    private String STPICADD;
    // 圖片
    @Since(1)
    private byte[] STPICDATA;
    // 服務專線
    private String STTEL;
    // 特店優惠-優惠內容日期
    private String STADDATE;
    // 優惠內容顯示以下內容
    private String STADSHOW;
    // 注意事項
    private String STADNOTE;
    // 流程步驟ID
    private String STEPID;
    // 流程步驟名稱
    private String STEPNAME;
    // 流程是否結束
    private String FLOWFINISHED;
    // 最後修改者
    private String LASTUSER;
    // 最後修改日
    private String LASTDATE;
    // 最後修改時間
    private String LASTTIME;
    // 分店資訊
    private String STADINFO;
    // 優惠內容
    private String STDADCONT;
    // 優惠標題
    private String STDADHLK;
    // 特店狀態
    private String STADSTAT;
    
    @Transient
    private String EDITORUNAME;
    
    @Transient
    private String IMGBASE64;

}
