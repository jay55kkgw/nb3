package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>ADMREMITMENU class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMREMITMENU")
public class ADMREMITMENU implements Serializable { 

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADSEQNO;

	private String ADRMTTYPE = "";
	private String ADRMTID = "";
	private String ADLKINDID = "";
	private String ADMKINDID = "";

	@NotBlank(message = "分類項目(繁中)不可為空值.")
	private String ADRMTITEM = "";

	@NotBlank(message = "分類項目(簡中)不可為空值.")
	private String ADRMTENGITEM = "";

	@NotBlank(message = "分類項目(英文)不可為空值.")
	private String ADRMTCHSITEM = "";

	private String ADRMTDESC = "";
	private String ADRMTENGDESC = "";
	private String ADRMTCHSDESC = "";
	
	private String ADCHKMK = "";
	private String ADRMTEETYPE1 = "";
	private String ADRMTEETYPE2 = "";
	private String ADRMTEETYPE3 = "";
	private String LASTUSER = "";
	private String LASTDATE = "";
	private String LASTTIME = "";

	

	
	
	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("adseqno", getADSEQNO())
				.toString();
	}


	/**
	 * <p>getADCHKMK.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCHKMK() {
		return ADCHKMK;
	}


	/**
	 * <p>setADCHKMK.</p>
	 *
	 * @param adchkmk a {@link java.lang.String} object.
	 */
	public void setADCHKMK(String adchkmk) {
		ADCHKMK = adchkmk;
	}


	/**
	 * <p>getADLKINDID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADLKINDID() {
		return ADLKINDID.trim();
	}


	/**
	 * <p>setADLKINDID.</p>
	 *
	 * @param adlkindid a {@link java.lang.String} object.
	 */
	public void setADLKINDID(String adlkindid) {
		ADLKINDID = adlkindid;
	}


	/**
	 * <p>getADMKINDID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMKINDID() {
		return ADMKINDID.trim();
	}


	/**
	 * <p>setADMKINDID.</p>
	 *
	 * @param admkindid a {@link java.lang.String} object.
	 */
	public void setADMKINDID(String admkindid) {
		ADMKINDID = admkindid;
	}


	/**
	 * <p>getADRMTDESC.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADRMTDESC() {
		return ADRMTDESC;
	}


	/**
	 * <p>setADRMTDESC.</p>
	 *
	 * @param adrmtdesc a {@link java.lang.String} object.
	 */
	public void setADRMTDESC(String adrmtdesc) {
		ADRMTDESC = adrmtdesc;
	}


	/**
	 * <p>getADRMTEETYPE1.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADRMTEETYPE1() {
		return ADRMTEETYPE1;
	}


	/**
	 * <p>setADRMTEETYPE1.</p>
	 *
	 * @param adrmteetype1 a {@link java.lang.String} object.
	 */
	public void setADRMTEETYPE1(String adrmteetype1) {
		ADRMTEETYPE1 = adrmteetype1;
	}


	/**
	 * <p>getADRMTEETYPE2.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADRMTEETYPE2() {
		return ADRMTEETYPE2;
	}


	/**
	 * <p>setADRMTEETYPE2.</p>
	 *
	 * @param adrmteetype2 a {@link java.lang.String} object.
	 */
	public void setADRMTEETYPE2(String adrmteetype2) {
		ADRMTEETYPE2 = adrmteetype2;
	}


	/**
	 * <p>getADRMTEETYPE3.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADRMTEETYPE3() {
		return ADRMTEETYPE3;
	}


	/**
	 * <p>setADRMTEETYPE3.</p>
	 *
	 * @param adrmteetype3 a {@link java.lang.String} object.
	 */
	public void setADRMTEETYPE3(String adrmteetype3) {
		ADRMTEETYPE3 = adrmteetype3;
	}


	/**
	 * <p>getADRMTID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADRMTID() {
		return ADRMTID.trim();
	}


	/**
	 * <p>setADRMTID.</p>
	 *
	 * @param adrmtid a {@link java.lang.String} object.
	 */
	public void setADRMTID(String adrmtid) {
		ADRMTID = adrmtid;
	}


	/**
	 * <p>getADRMTITEM.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADRMTITEM() {
		return ADRMTITEM;
	}


	/**
	 * <p>setADRMTITEM.</p>
	 *
	 * @param adrmtitem a {@link java.lang.String} object.
	 */
	public void setADRMTITEM(String adrmtitem) {
		ADRMTITEM = adrmtitem;
	}


	/**
	 * <p>getADRMTTYPE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADRMTTYPE() {
		return ADRMTTYPE;
	}


	/**
	 * <p>setADRMTTYPE.</p>
	 *
	 * @param adrmttype a {@link java.lang.String} object.
	 */
	public void setADRMTTYPE(String adrmttype) {
		ADRMTTYPE = adrmttype;
	}


	/**
	 * <p>getADSEQNO.</p>
	 *
	 * @return a {@link java.lang.Long} object.
	 */
	public Long getADSEQNO() {
		return ADSEQNO;
	}


	/**
	 * <p>setADSEQNO.</p>
	 *
	 * @param adseqno a {@link java.lang.Long} object.
	 */
	public void setADSEQNO(Long adseqno) {
		ADSEQNO = adseqno;
	}


	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}


	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}


	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}


	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}


	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}


	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	public String getADRMTENGITEM() {
		return ADRMTENGITEM;
	}

	public void setADRMTENGITEM(String aDRMTENGITEM) {
		ADRMTENGITEM = aDRMTENGITEM;
	}

	public String getADRMTCHSITEM() {
		return ADRMTCHSITEM;
	}

	public void setADRMTCHSITEM(String aDRMTCHSITEM) {
		ADRMTCHSITEM = aDRMTCHSITEM;
	}

	public String getADRMTENGDESC() {
		return ADRMTENGDESC;
	}

	public void setADRMTENGDESC(String aDRMTENGDESC) {
		ADRMTENGDESC = aDRMTENGDESC;
	}

	public String getADRMTCHSDESC() {
		return ADRMTCHSDESC;
	}

	public void setADRMTCHSDESC(String aDRMTCHSDESC) {
		ADRMTCHSDESC = aDRMTCHSDESC;
	}


}
