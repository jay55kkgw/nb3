package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 流程定義步驟 POJO
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMFLOWDEFSTEP")
public class ADMFLOWDEFSTEP implements Serializable {

	@EmbeddedId
    private ADMFLOWDEFSTEPIDENTITY STEPIDENTITY;

    private String STEPNAME;
    private String STEPTYPE;
    private String STEPOWNER;
    private String STEPOWNERTYPE;
    private int STEPCOUNT;
    private String NEXTSTEPID;
    private String NEXTSTEPCOND;
    private String LASTUSER;
    private String LASTDATE;
    private String LASTTIME;

    /**
     * <p>getSTEPIDENTITY.</p>
     *
     * @return a {@link fstop.orm.po.ADMFLOWDEFSTEPIDENTITY} object.
     */
    public ADMFLOWDEFSTEPIDENTITY getSTEPIDENTITY() {
        return STEPIDENTITY;
    }

    /**
     * <p>setSTEPIDENTITY.</p>
     *
     * @param sTEPIDENTITY a {@link fstop.orm.po.ADMFLOWDEFSTEPIDENTITY} object.
     */
    public void setSTEPIDENTITY(ADMFLOWDEFSTEPIDENTITY sTEPIDENTITY) {
        STEPIDENTITY = sTEPIDENTITY;
    }

    /**
     * <p>getSTEPNAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPNAME() {
        return STEPNAME;
    }

    /**
     * <p>setSTEPNAME.</p>
     *
     * @param sTEPNAME a {@link java.lang.String} object.
     */
    public void setSTEPNAME(String sTEPNAME) {
        STEPNAME = sTEPNAME;
    }

    /**
     * <p>getSTEPTYPE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPTYPE() {
        return STEPTYPE;
    }

    /**
     * <p>setSTEPTYPE.</p>
     *
     * @param sTEPTYPE a {@link java.lang.String} object.
     */
    public void setSTEPTYPE(String sTEPTYPE) {
        STEPTYPE = sTEPTYPE;
    }

    /**
     * <p>getSTEPOWNER.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPOWNER() {
        return STEPOWNER;
    }

    /**
     * <p>setSTEPOWNER.</p>
     *
     * @param sTEPOWNER a {@link java.lang.String} object.
     */
    public void setSTEPOWNER(String sTEPOWNER) {
        STEPOWNER = sTEPOWNER;
    }

    /**
     * <p>getSTEPOWNERTYPE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPOWNERTYPE() {
        return STEPOWNERTYPE;
    }

    /**
     * <p>setSTEPOWNERTYPE.</p>
     *
     * @param sTEPOWNERTYPE a {@link java.lang.String} object.
     */
    public void setSTEPOWNERTYPE(String sTEPOWNERTYPE) {
        STEPOWNERTYPE = sTEPOWNERTYPE;
    }

    /**
     * <p>getSTEPCOUNT.</p>
     *
     * @return a int.
     */
    public int getSTEPCOUNT() {
        return STEPCOUNT;
    }

    /**
     * <p>setSTEPCOUNT.</p>
     *
     * @param sTEPCOUNT a int.
     */
    public void setSTEPCOUNT(int sTEPCOUNT) {
        STEPCOUNT = sTEPCOUNT;
    }

    /**
     * <p>getNEXTSTEPID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNEXTSTEPID() {
        return NEXTSTEPID;
    }

    /**
     * <p>setNEXTSTEPID.</p>
     *
     * @param nEXTSTEPID a {@link java.lang.String} object.
     */
    public void setNEXTSTEPID(String nEXTSTEPID) {
        NEXTSTEPID = nEXTSTEPID;
    }

    /**
     * <p>getNEXTSTEPCOND.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNEXTSTEPCOND() {
        return NEXTSTEPCOND;
    }

    /**
     * <p>setNEXTSTEPCOND.</p>
     *
     * @param nEXTSTEPCOND a {@link java.lang.String} object.
     */
    public void setNEXTSTEPCOND(String nEXTSTEPCOND) {
        NEXTSTEPCOND = nEXTSTEPCOND;
    }

    /**
     * <p>getLASTUSER.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLASTUSER() {
        return LASTUSER;
    }

    /**
     * <p>setLASTUSER.</p>
     *
     * @param lASTUSER a {@link java.lang.String} object.
     */
    public void setLASTUSER(String lASTUSER) {
        LASTUSER = lASTUSER;
    }

    /**
     * <p>getLASTDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLASTDATE() {
        return LASTDATE;
    }

    /**
     * <p>setLASTDATE.</p>
     *
     * @param lASTDATE a {@link java.lang.String} object.
     */
    public void setLASTDATE(String lASTDATE) {
        LASTDATE = lASTDATE;
    }

    /**
     * <p>getLASTTIME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLASTTIME() {
        return LASTTIME;
    }

    /**
     * <p>setLASTTIME.</p>
     *
     * @param lASTTIME a {@link java.lang.String} object.
     */
    public void setLASTTIME(String lASTTIME) {
        LASTTIME = lASTTIME;
    }
}
