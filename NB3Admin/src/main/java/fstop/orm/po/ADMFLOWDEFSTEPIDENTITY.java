package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.Embeddable;
import com.sun.istack.NotNull;

/**
 * ADMFLOWDEFSTEP Composite Primary Keys
 *
 * @author chiensj
 * @version V1.0
 */
@Embeddable
public class ADMFLOWDEFSTEPIDENTITY implements Serializable {

    @NotNull
    private String FLOWID;

    @NotNull
    private String STEPID;

    /**
     * <p>getFLOWID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFLOWID() {
        return FLOWID;
    }

    /**
     * <p>setFLOWID.</p>
     *
     * @param fLOWID a {@link java.lang.String} object.
     */
    public void setFLOWID(String fLOWID) {
        FLOWID = fLOWID;
    }

    /**
     * <p>getSTEPID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPID() {
        return STEPID;
    }

    /**
     * <p>setSTEPID.</p>
     *
     * @param sTEPID a {@link java.lang.String} object.
     */
    public void setSTEPID(String sTEPID) {
        STEPID = sTEPID;
    }
}
