package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.annotations.Since;

/**
 * 流程案件 POJO
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMFLOWCURRENT")
public class ADMFLOWCURRENT implements Serializable {

	@EmbeddedId
    private ADMFLOWCURRENTIDENTITY CURRENTIDENTITY;

    private String ORGID;
    private String FLOWID;
    private String STEPID;
    private String STEPNAME;
    private int STEPCOUNT;
    private String EDITOROID;
    private String EDITORUID;
    private String EDITORUNAME;
    private String FROMOID;
    private String FROMUID;
    private String FROMUNAME;
    private String TOOID;
    private String TOUID;
    private String TOUNAME;
    private String TOTYPE;
    @Since(1)
    private String FLOWDATA;
    private String FLOWMEMO;
    private String CREATEDATE;
    private String CREATETIME;
    private String TODOURL;

    /**
     * <p>getCURRENTIDENTITY.</p>
     *
     * @return a {@link fstop.orm.po.ADMFLOWCURRENTIDENTITY} object.
     */
    public ADMFLOWCURRENTIDENTITY getCURRENTIDENTITY() {
        return CURRENTIDENTITY;
    }

    /**
     * <p>setCURRENTIDENTITY.</p>
     *
     * @param cURRENTIDENTITY a {@link fstop.orm.po.ADMFLOWCURRENTIDENTITY} object.
     */
    public void setCURRENTIDENTITY(ADMFLOWCURRENTIDENTITY cURRENTIDENTITY) {
        CURRENTIDENTITY = cURRENTIDENTITY;
    }

    /**
     * <p>getORGID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getORGID() {
        return ORGID;
    }

    /**
     * <p>setORGID.</p>
     *
     * @param oRGID a {@link java.lang.String} object.
     */
    public void setORGID(String oRGID) {
        ORGID = oRGID;
    }

    /**
     * <p>getFLOWID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFLOWID() {
        return FLOWID;
    }

    /**
     * <p>setFLOWID.</p>
     *
     * @param fLOWID a {@link java.lang.String} object.
     */
    public void setFLOWID(String fLOWID) {
        FLOWID = fLOWID;
    }

    /**
     * <p>getSTEPID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPID() {
        return STEPID;
    }

    /**
     * <p>setSTEPID.</p>
     *
     * @param sTEPID a {@link java.lang.String} object.
     */
    public void setSTEPID(String sTEPID) {
        STEPID = sTEPID;
    }

    /**
     * <p>getSTEPNAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPNAME() {
        return STEPNAME;
    }

    /**
     * <p>setSTEPNAME.</p>
     *
     * @param sTEPNAME a {@link java.lang.String} object.
     */
    public void setSTEPNAME(String sTEPNAME) {
        STEPNAME = sTEPNAME;
    }

    /**
     * <p>getSTEPCOUNT.</p>
     *
     * @return a int.
     */
    public int getSTEPCOUNT() {
        return STEPCOUNT;
    }

    /**
     * <p>setSTEPCOUNT.</p>
     *
     * @param sTEPCOUNT a int.
     */
    public void setSTEPCOUNT(int sTEPCOUNT) {
        STEPCOUNT = sTEPCOUNT;
    }

    /**
     * <p>getEDITOROID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEDITOROID() {
        return EDITOROID;
    }

    /**
     * <p>setEDITOROID.</p>
     *
     * @param eDITOROID a {@link java.lang.String} object.
     */
    public void setEDITOROID(String eDITOROID) {
        EDITOROID = eDITOROID;
    }

    /**
     * <p>getEDITORUID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEDITORUID() {
        return EDITORUID;
    }

    /**
     * <p>setEDITORUID.</p>
     *
     * @param eDITORUID a {@link java.lang.String} object.
     */
    public void setEDITORUID(String eDITORUID) {
        EDITORUID = eDITORUID;
    }

    /**
     * <p>getEDITORUNAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEDITORUNAME() {
        return EDITORUNAME;
    }

    /**
     * <p>setEDITORUNAME.</p>
     *
     * @param eDITORUNAME a {@link java.lang.String} object.
     */
    public void setEDITORUNAME(String eDITORUNAME) {
        EDITORUNAME = eDITORUNAME;
    }

    /**
     * <p>getFROMOID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFROMOID() {
        return FROMOID;
    }

    /**
     * <p>setFROMOID.</p>
     *
     * @param fROMOID a {@link java.lang.String} object.
     */
    public void setFROMOID(String fROMOID) {
        FROMOID = fROMOID;
    }

    /**
     * <p>getFROMUID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFROMUID() {
        return FROMUID;
    }

    /**
     * <p>setFROMUID.</p>
     *
     * @param fROMUID a {@link java.lang.String} object.
     */
    public void setFROMUID(String fROMUID) {
        FROMUID = fROMUID;
    }

    /**
     * <p>getFROMUNAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFROMUNAME() {
        return FROMUNAME;
    }

    /**
     * <p>setFROMUNAME.</p>
     *
     * @param fROMUNAME a {@link java.lang.String} object.
     */
    public void setFROMUNAME(String fROMUNAME) {
        FROMUNAME = fROMUNAME;
    }

    /**
     * <p>getTOOID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTOOID() {
        return TOOID;
    }

    /**
     * <p>setTOOID.</p>
     *
     * @param tOOID a {@link java.lang.String} object.
     */
    public void setTOOID(String tOOID) {
        TOOID = tOOID;
    }

    /**
     * <p>getTOUID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTOUID() {
        return TOUID;
    }

    /**
     * <p>setTOUID.</p>
     *
     * @param tOUID a {@link java.lang.String} object.
     */
    public void setTOUID(String tOUID) {
        TOUID = tOUID;
    }

    /**
     * <p>getTOUNAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTOUNAME() {
        return TOUNAME;
    }

    /**
     * <p>setTOUNAME.</p>
     *
     * @param tOUNAME a {@link java.lang.String} object.
     */
    public void setTOUNAME(String tOUNAME) {
        TOUNAME = tOUNAME;
    }

    /**
     * <p>getTOTYPE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTOTYPE() {
        return TOTYPE;
    }

    /**
     * <p>setTOTYPE.</p>
     *
     * @param tOTYPE a {@link java.lang.String} object.
     */
    public void setTOTYPE(String tOTYPE) {
        TOTYPE = tOTYPE;
    }

    /**
     * <p>getFLOWDATA.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFLOWDATA() {
        return FLOWDATA;
    }

    /**
     * <p>setFLOWDATA.</p>
     *
     * @param fLOWDATA a {@link java.lang.String} object.
     */
    public void setFLOWDATA(String fLOWDATA) {
        FLOWDATA = fLOWDATA;
    }

    /**
     * <p>getFLOWMEMO.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFLOWMEMO() {
        return FLOWMEMO;
    }

    /**
     * <p>setFLOWMEMO.</p>
     *
     * @param fLOWMEMO a {@link java.lang.String} object.
     */
    public void setFLOWMEMO(String fLOWMEMO) {
        FLOWMEMO = fLOWMEMO;
    }

    /**
     * <p>getCREATEDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCREATEDATE() {
        return CREATEDATE;
    }

    /**
     * <p>setCREATEDATE.</p>
     *
     * @param cREATEDATE a {@link java.lang.String} object.
     */
    public void setCREATEDATE(String cREATEDATE) {
        CREATEDATE = cREATEDATE;
    }

    /**
     * <p>getCREATETIME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCREATETIME() {
        return CREATETIME;
    }

    /**
     * <p>setCREATETIME.</p>
     *
     * @param cREATETIME a {@link java.lang.String} object.
     */
    public void setCREATETIME(String cREATETIME) {
        CREATETIME = cREATETIME;
    }

    /**
     * <p>getTODOURL.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTODOURL() {
        return TODOURL;
    }

    /**
     * <p>setTODOURL.</p>
     *
     * @param tODOURL a {@link java.lang.String} object.
     */
    public void setTODOURL(String tODOURL) {
        TODOURL = tODOURL;
    }

}
