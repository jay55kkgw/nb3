package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>TXNUSER class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "TXNUSER")
public class TXNUSER implements Serializable {

	@Id
	private String DPSUERID;

	private String DPUSERNAME = "";

	private String DPBILL  = "";

	private String DPMENU  = "";

	private String DPOVERVIEW  = "";

	private String DPMYEMAIL  = "";

	private String DPNOTIFY  = "";

	private String DPFUNDBILL  = "";

	private String DPCARDBILL  = "";

	private String LASTDATE  = "";

	private String LASTTIME  = "";

	private String BILLDATE = "";

	private String FUNDBILLDATE = "";

	private String CARDBILLDATE = "";

	private String EMAILDATE = "";

	private Integer SCHCOUNT;

	private String SCHCNTDATE = "";

	private String ADBRANCHID = "";

	private String FUNDAGREEVERSION = "";

	private String FUNDBILLSENDMODE = "";

	private Integer ASKERRTIMES;

	private String CCARDFLAG = "";
	
	private String RESETTIME = "";

	private String FXREMITFLAG = "";
	
	
	/**
	 * <p>getDPUSERNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPUSERNAME() {
		return DPUSERNAME;
	}

	/**
	 * <p>setDPUSERNAME.</p>
	 *
	 * @param dpusername a {@link java.lang.String} object.
	 */
	public void setDPUSERNAME(String dpusername) {
		DPUSERNAME = dpusername;
	}

	/**
	 * <p>getSCHCNTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSCHCNTDATE() {
		return SCHCNTDATE;
	}

	/**
	 * <p>setSCHCNTDATE.</p>
	 *
	 * @param schcntdate a {@link java.lang.String} object.
	 */
	public void setSCHCNTDATE(String schcntdate) {
		SCHCNTDATE = schcntdate;
	}

	/**
	 * <p>getSCHCOUNT.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getSCHCOUNT() {
		return SCHCOUNT;
	}

	/**
	 * <p>setSCHCOUNT.</p>
	 *
	 * @param schcount a {@link java.lang.Integer} object.
	 */
	public void setSCHCOUNT(Integer schcount) {
		SCHCOUNT = schcount;
	}

	/**
	 * <p>getBILLDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBILLDATE() {
		return BILLDATE;
	}

	/**
	 * <p>setBILLDATE.</p>
	 *
	 * @param billdate a {@link java.lang.String} object.
	 */
	public void setBILLDATE(String billdate) {
		BILLDATE = billdate;
	}

	/**
	 * <p>getCARDBILLDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCARDBILLDATE() {
		return CARDBILLDATE;
	}

	/**
	 * <p>setCARDBILLDATE.</p>
	 *
	 * @param cardbilldate a {@link java.lang.String} object.
	 */
	public void setCARDBILLDATE(String cardbilldate) {
		CARDBILLDATE = cardbilldate;
	}

	/**
	 * <p>getEMAILDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getEMAILDATE() {
		return EMAILDATE;
	}

	/**
	 * <p>setEMAILDATE.</p>
	 *
	 * @param emaildate a {@link java.lang.String} object.
	 */
	public void setEMAILDATE(String emaildate) {
		EMAILDATE = emaildate;
	}

	/**
	 * <p>getFUNDBILLDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFUNDBILLDATE() {
		return FUNDBILLDATE;
	}

	/**
	 * <p>setFUNDBILLDATE.</p>
	 *
	 * @param fundbilldate a {@link java.lang.String} object.
	 */
	public void setFUNDBILLDATE(String fundbilldate) {
		FUNDBILLDATE = fundbilldate;
	}

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("dpsuerid", getDPSUERID())
				.toString();
	}

	/**
	 * <p>getDPBILL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPBILL() {
		return DPBILL;
	}

	/**
	 * <p>setDPBILL.</p>
	 *
	 * @param dpbill a {@link java.lang.String} object.
	 */
	public void setDPBILL(String dpbill) {
		DPBILL = dpbill;
	}

	/**
	 * <p>getDPMENU.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPMENU() {
		return DPMENU;
	}

	/**
	 * <p>setDPMENU.</p>
	 *
	 * @param dpmenu a {@link java.lang.String} object.
	 */
	public void setDPMENU(String dpmenu) {
		DPMENU = dpmenu;
	}

	/**
	 * <p>getDPMYEMAIL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}

	/**
	 * <p>setDPMYEMAIL.</p>
	 *
	 * @param dpmyemail a {@link java.lang.String} object.
	 */
	public void setDPMYEMAIL(String dpmyemail) {
		DPMYEMAIL = dpmyemail;
	}

	/**
	 * <p>getDPNOTIFY.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPNOTIFY() {
		return DPNOTIFY;
	}

	/**
	 * <p>setDPNOTIFY.</p>
	 *
	 * @param dpnotify a {@link java.lang.String} object.
	 */
	public void setDPNOTIFY(String dpnotify) {
		DPNOTIFY = dpnotify;
	}

	/**
	 * <p>getDPOVERVIEW.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPOVERVIEW() {
		return DPOVERVIEW;
	}

	/**
	 * <p>setDPOVERVIEW.</p>
	 *
	 * @param dpoverview a {@link java.lang.String} object.
	 */
	public void setDPOVERVIEW(String dpoverview) {
		DPOVERVIEW = dpoverview;
	}

	/**
	 * <p>getDPSUERID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPSUERID() {
		return DPSUERID;
	}

	/**
	 * <p>setDPSUERID.</p>
	 *
	 * @param dpsuerid a {@link java.lang.String} object.
	 */
	public void setDPSUERID(String dpsuerid) {
		DPSUERID = dpsuerid;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>getDPCARDBILL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPCARDBILL() {
		return DPCARDBILL;
	}

	/**
	 * <p>setDPCARDBILL.</p>
	 *
	 * @param dpcardbill a {@link java.lang.String} object.
	 */
	public void setDPCARDBILL(String dpcardbill) {
		DPCARDBILL = dpcardbill;
	}

	/**
	 * <p>getDPFUNDBILL.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDPFUNDBILL() {
		return DPFUNDBILL;
	}

	/**
	 * <p>setDPFUNDBILL.</p>
	 *
	 * @param dpfundbill a {@link java.lang.String} object.
	 */
	public void setDPFUNDBILL(String dpfundbill) {
		DPFUNDBILL = dpfundbill;
	}

	/**
	 * <p>getADBRANCHID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADBRANCHID() {
		return ADBRANCHID;
	}

	/**
	 * <p>setADBRANCHID.</p>
	 *
	 * @param adbranchid a {@link java.lang.String} object.
	 */
	public void setADBRANCHID(String adbranchid) {
		ADBRANCHID = adbranchid;
	}

	/**
	 * <p>getFUNDAGREEVERSION.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFUNDAGREEVERSION() {
		return FUNDAGREEVERSION;
	}

	/**
	 * <p>setFUNDAGREEVERSION.</p>
	 *
	 * @param version a {@link java.lang.String} object.
	 */
	public void setFUNDAGREEVERSION(String version) {
		FUNDAGREEVERSION = version;
	}

	/**
	 * <p>getFUNDBILLSENDMODE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFUNDBILLSENDMODE() {
		return FUNDBILLSENDMODE;
	}

	/**
	 * <p>setFUNDBILLSENDMODE.</p>
	 *
	 * @param mode a {@link java.lang.String} object.
	 */
	public void setFUNDBILLSENDMODE(String mode) {
		FUNDBILLSENDMODE = mode;
	}

	/**
	 * <p>getASKERRTIMES.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getASKERRTIMES() {
		return ASKERRTIMES;
	}

	/**
	 * <p>setASKERRTIMES.</p>
	 *
	 * @param askerrtimes a {@link java.lang.Integer} object.
	 */
	public void setASKERRTIMES(Integer askerrtimes) {
		ASKERRTIMES = askerrtimes;
	}

	/**
	 * <p>getCCARDFLAG.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCCARDFLAG() {
		return CCARDFLAG;
	}

	/**
	 * <p>setCCARDFLAG.</p>
	 *
	 * @param ccardflag a {@link java.lang.String} object.
	 */
	public void setCCARDFLAG(String ccardflag) {
		CCARDFLAG = ccardflag;
	}

	/**
	 * <p>getFXREMITFLAG.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFXREMITFLAG() {
		return FXREMITFLAG;
	}

	/**
	 * <p>setFXREMITFLAG.</p>
	 *
	 * @param fxremitflag a {@link java.lang.String} object.
	 */
	public void setFXREMITFLAG(String fxremitflag) {
		FXREMITFLAG = fxremitflag;
	}

	/**
	 * <p>getRESETTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getRESETTIME() {
		return RESETTIME;
	}

	/**
	 * <p>setRESETTIME.</p>
	 *
	 * @param resettime a {@link java.lang.String} object.
	 */
	public void setRESETTIME(String resettime) {
		RESETTIME = resettime;
	}


}
