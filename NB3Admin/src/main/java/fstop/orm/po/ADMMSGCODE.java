package fstop.orm.po;

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import java.io.Serializable;

/**
 * <p>ADMMSGCODE class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMMSGCODE")
public class ADMMSGCODE implements Serializable {

	@Id
	@NotBlank(message = "訊息代碼不可為空值.")
	private String ADMCODE;

	private String ADMRESEND = "";

	private String ADMEXCE = ""; // 需列入異常事件通知否

	@NotBlank(message = "訊息說明不可為空值.")
	private String ADMSGIN = "";

	@NotBlank(message = "客戶訊息內容(繁中)不可為空值.")
	private String ADMSGOUT = "";

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	private String ADMRESENDFX = "";

	private String ADMAUTOSEND = "";

	private String ADMAUTOSENDFX = "";

	@NotBlank(message = "客戶訊息內容(簡中)不可為空值.")
	private String ADMSGOUTCHS = "";

	@NotBlank(message = "客戶訊息內容(英文)不可為空值.")
	private String ADMSGOUTENG = "";

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("admcode", getADMCODE()).toString();
	}

	/**
	 * <p>getADMCODE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMCODE() {
		return ADMCODE;
	}

	/**
	 * <p>setADMCODE.</p>
	 *
	 * @param admcode a {@link java.lang.String} object.
	 */
	public void setADMCODE(String admcode) {
		ADMCODE = admcode;
	}

	/**
	 * <p>getADMEXCE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMEXCE() {
		return ADMEXCE;
	}

	/**
	 * <p>setADMEXCE.</p>
	 *
	 * @param admexce a {@link java.lang.String} object.
	 */
	public void setADMEXCE(String admexce) {
		ADMEXCE = admexce;
	}

	/**
	 * <p>getADMRESEND.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMRESEND() {
		return ADMRESEND;
	}

	/**
	 * <p>setADMRESEND.</p>
	 *
	 * @param admresend a {@link java.lang.String} object.
	 */
	public void setADMRESEND(String admresend) {
		ADMRESEND = admresend;
	}

	/**
	 * <p>getADMSGIN.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMSGIN() {
		return ADMSGIN;
	}

	/**
	 * <p>setADMSGIN.</p>
	 *
	 * @param admsgin a {@link java.lang.String} object.
	 */
	public void setADMSGIN(String admsgin) {
		ADMSGIN = admsgin;
	}

	/**
	 * <p>getADMSGOUT.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMSGOUT() {
		return ADMSGOUT;
	}

	/**
	 * <p>setADMSGOUT.</p>
	 *
	 * @param admsgout a {@link java.lang.String} object.
	 */
	public void setADMSGOUT(String admsgout) {
		ADMSGOUT = admsgout;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}

	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	/**
	 * <p>getADMRESENDFX.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMRESENDFX() {
		return ADMRESENDFX;
	}

	/**
	 * <p>setADMRESENDFX.</p>
	 *
	 * @param admresendfx a {@link java.lang.String} object.
	 */
	public void setADMRESENDFX(String admresendfx) {
		ADMRESENDFX = admresendfx;
	}

	/**
	 * <p>getADMAUTOSEND.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMAUTOSEND() {
		return ADMAUTOSEND;
	}

	/**
	 * <p>setADMAUTOSEND.</p>
	 *
	 * @param admautosend a {@link java.lang.String} object.
	 */
	public void setADMAUTOSEND(String admautosend) {
		ADMAUTOSEND = admautosend;
	}

	/**
	 * <p>getADMAUTOSENDFX.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADMAUTOSENDFX() {
		return ADMAUTOSENDFX;
	}

	/**
	 * <p>setADMAUTOSENDFX.</p>
	 *
	 * @param admautosendfx a {@link java.lang.String} object.
	 */
	public void setADMAUTOSENDFX(String admautosendfx) {
		ADMAUTOSENDFX = admautosendfx;
	}

	public String getADMSGOUTCHS() {
		return ADMSGOUTCHS;
	}

	public void setADMSGOUTCHS(String aDMSGOUTCHS) {
		ADMSGOUTCHS = aDMSGOUTCHS;
	}

	public String getADMSGOUTENG() {
		return ADMSGOUTENG;
	}

	public void setADMSGOUTENG(String aDMSGOUTENG) {
		ADMSGOUTENG = aDMSGOUTENG;
	}
}
