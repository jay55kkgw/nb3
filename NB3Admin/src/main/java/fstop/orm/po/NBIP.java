package fstop.orm.po;

import java.io.Serializable;

/**
 * 為了 Heap Inspection 所新增的 Class
 * @author chien
 *
 */
public class NBIP implements Serializable {
    private NBIPIDENTITY NBIPIDENTITY;
    private String LASTUSER;
    private String LASTDATE;
    private String LASTTIME;
    
	public NBIPIDENTITY getNBIPIDENTITY() {
		return NBIPIDENTITY;
	}
	public void setNBIPIDENTITY(NBIPIDENTITY nBIPIDENTITY) {
		NBIPIDENTITY = nBIPIDENTITY;
	}
	public String getLASTUSER() {
		return LASTUSER;
	}
	public void setLASTUSER(String lASTUSER) {
		LASTUSER = lASTUSER;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
}
