package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Since;

import lombok.Data;

/**
 * 行銷暫存檔，已放行的行銷會由 Temp 檔搬到主檔
 *
 * @author 
 * @version V1.0
 */
@Entity
@Table(name = "ADMMRKTTMP")
@Data
public class ADMMRKTTMP implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4276836553159254838L;

	@Id
    private String MTID;

    private String OID;
    private String MTSDATE;
    private String MTSTIME;
    private String MTEDATE;
    private String MTETIME;
    private String MTADTYPE;
    private String MTWEIGHT;
    private String MTADHLK;
    private String MTADCON;
    private String MTPICTYPE;
    private String MTRUNSHOW;
    private String MTBULLETIN;
    private String MTPICADD;
    @Since(1)
    private byte[] MTPICDATA;
    private String MTBTNNAME ;
    private String MTADDRESS;
    private String MTMARQUEEPICADD;
    @Since(1)
    private byte[] MTMARQUEEPIC;
    
    private String MTNF;
    private String MTNFSTAT;
    private String MTNFDATE;
    private String MTNFTIME;
    private String MTCF;
    private String MTADSTAT;
    
    private String STEPID;
    private String STEPNAME;
    private String FLOWFINISHED;

    private String LASTUSER;
    private String LASTDATE;
    private String LASTTIME;

    
    @Transient
    private String EDITORUNAME;
    
    @Transient
    private String MTPICDATABASE64;

    @Transient
    private String MTMARQUEEPICBASE64;

}
