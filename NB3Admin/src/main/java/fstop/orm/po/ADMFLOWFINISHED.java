package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>ADMFLOWFINISHED class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMFLOWFINISHED")
public class ADMFLOWFINISHED implements Serializable {
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id    
    private Integer ID;

    private String ORGID;
    private String CASESN;
    private String FLOWID;
    private String STEPID;
    private String EDITOROID;
    private String EDITORUID;
    private String EDITORUNAME;
    private String APPROVEROID;
    private String APPROVERUID;
    private String APPROVERUNAME;
    private String APPROVEFLAG;
    private String CASESTARTDATE;
    private String CASESTARTTIME;
    private String CASEENDDATE;
    private String CASEENDTIME;

    /**
     * <p>getID.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getID() {
        return ID;
    }

    /**
     * <p>setID.</p>
     *
     * @param iD a {@link java.lang.Integer} object.
     */
    public void setID(Integer iD) {
        ID = iD;
    }

    /**
     * <p>getORGID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getORGID() {
        return ORGID;
    }

    /**
     * <p>setORGID.</p>
     *
     * @param oRGID a {@link java.lang.String} object.
     */
    public void setORGID(String oRGID) {
        ORGID = oRGID;
    }

    /**
     * <p>getCASESN.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCASESN() {
        return CASESN;
    }

    /**
     * <p>setCASESN.</p>
     *
     * @param cASESN a {@link java.lang.String} object.
     */
    public void setCASESN(String cASESN) {
        CASESN = cASESN;
    }

    /**
     * <p>getFLOWID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFLOWID() {
        return FLOWID;
    }

    /**
     * <p>setFLOWID.</p>
     *
     * @param fLOWID a {@link java.lang.String} object.
     */
    public void setFLOWID(String fLOWID) {
        FLOWID = fLOWID;
    }

    /**
     * <p>getSTEPID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSTEPID() {
        return STEPID;
    }

    /**
     * <p>setSTEPID.</p>
     *
     * @param sTEPID a {@link java.lang.String} object.
     */
    public void setSTEPID(String sTEPID) {
        STEPID = sTEPID;
    }

    /**
     * <p>getEDITOROID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEDITOROID() {
        return EDITOROID;
    }

    /**
     * <p>setEDITOROID.</p>
     *
     * @param eDITOROID a {@link java.lang.String} object.
     */
    public void setEDITOROID(String eDITOROID) {
        EDITOROID = eDITOROID;
    }

    /**
     * <p>getEDITORUID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEDITORUID() {
        return EDITORUID;
    }

    /**
     * <p>setEDITORUID.</p>
     *
     * @param eDITORUID a {@link java.lang.String} object.
     */
    public void setEDITORUID(String eDITORUID) {
        EDITORUID = eDITORUID;
    }

    /**
     * <p>getEDITORUNAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEDITORUNAME() {
        return EDITORUNAME;
    }

    /**
     * <p>setEDITORUNAME.</p>
     *
     * @param eDITORUNAME a {@link java.lang.String} object.
     */
    public void setEDITORUNAME(String eDITORUNAME) {
        EDITORUNAME = eDITORUNAME;
    }

    /**
     * <p>getAPPROVEROID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAPPROVEROID() {
        return APPROVEROID;
    }

    /**
     * <p>setAPPROVEROID.</p>
     *
     * @param aPPROVEROID a {@link java.lang.String} object.
     */
    public void setAPPROVEROID(String aPPROVEROID) {
        APPROVEROID = aPPROVEROID;
    }

    /**
     * <p>getAPPROVERUID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAPPROVERUID() {
        return APPROVERUID;
    }

    /**
     * <p>setAPPROVERUID.</p>
     *
     * @param aPPROVERUID a {@link java.lang.String} object.
     */
    public void setAPPROVERUID(String aPPROVERUID) {
        APPROVERUID = aPPROVERUID;
    }

    /**
     * <p>getAPPROVERUNAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAPPROVERUNAME() {
        return APPROVERUNAME;
    }

    /**
     * <p>setAPPROVERUNAME.</p>
     *
     * @param aPPROVERUNAME a {@link java.lang.String} object.
     */
    public void setAPPROVERUNAME(String aPPROVERUNAME) {
        APPROVERUNAME = aPPROVERUNAME;
    }

    /**
     * <p>getAPPROVEFLAG.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAPPROVEFLAG() {
        return APPROVEFLAG;
    }

    /**
     * <p>setAPPROVEFLAG.</p>
     *
     * @param aPPROVEFLAG a {@link java.lang.String} object.
     */
    public void setAPPROVEFLAG(String aPPROVEFLAG) {
        APPROVEFLAG = aPPROVEFLAG;
    }

    /**
     * <p>getCASESTARTDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCASESTARTDATE() {
        return CASESTARTDATE;
    }

    /**
     * <p>setCASESTARTDATE.</p>
     *
     * @param cASESTARTDATE a {@link java.lang.String} object.
     */
    public void setCASESTARTDATE(String cASESTARTDATE) {
        CASESTARTDATE = cASESTARTDATE;
    }

    /**
     * <p>getCASESTARTTIME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCASESTARTTIME() {
        return CASESTARTTIME;
    }

    /**
     * <p>setCASESTARTTIME.</p>
     *
     * @param cASESTARTTIME a {@link java.lang.String} object.
     */
    public void setCASESTARTTIME(String cASESTARTTIME) {
        CASESTARTTIME = cASESTARTTIME;
    }

    /**
     * <p>getCASEENDDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCASEENDDATE() {
        return CASEENDDATE;
    }

    /**
     * <p>setCASEENDDATE.</p>
     *
     * @param cASEENDDATE a {@link java.lang.String} object.
     */
    public void setCASEENDDATE(String cASEENDDATE) {
        CASEENDDATE = cASEENDDATE;
    }

    /**
     * <p>getCASEENDTIME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCASEENDTIME() {
        return CASEENDTIME;
    }

    /**
     * <p>setCASEENDTIME.</p>
     *
     * @param cASEENDTIME a {@link java.lang.String} object.
     */
    public void setCASEENDTIME(String cASEENDTIME) {
        CASEENDTIME = cASEENDTIME;
    }

    
}  
