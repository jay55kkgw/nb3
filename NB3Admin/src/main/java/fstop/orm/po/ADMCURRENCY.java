package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>ADMCURRENCY class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMCURRENCY")
public class ADMCURRENCY implements Serializable {

	@Id
	@NotEmpty(message="幣別代碼不可為空值")
	private String ADCURRENCY;

	@NotEmpty(message="幣別號碼不可為空值")
	private String ADCCYNO = "";

	@NotEmpty(message="幣別繁中名稱不可為空值")
	private String ADCCYNAME = "";
	
	private String MSGCODE = "";

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	@NotEmpty(message="幣別簡中名稱不可為空值")
	private String ADCCYCHSNAME = "";

	@NotEmpty(message="幣別英文名稱不可為空值")
	private String ADCCYENGNAME = "";

//	private ADMCOUNTRY ADMCOUNTRY;
//
//	private Set TXNFXRECORDSBYFXSVCURR;
//
//	private Set TXNFXRECORDSBYFXWDCURR;
//
//	private Set TXNFXSCHEDULESBYFXSVCURR;
//
//	private Set TXNFXSCHEDULESBYFXWDCURR;

	/**
	 * <p>getADCURRENCY.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCURRENCY() {
		return ADCURRENCY;
	}

	/**
	 * <p>setADCURRENCY.</p>
	 *
	 * @param adcurrency a {@link java.lang.String} object.
	 */
	public void setADCURRENCY(String adcurrency) {
		ADCURRENCY = adcurrency;
	}

	/**
	 * <p>getADCCYNO.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCCYNO() {
		return ADCCYNO;
	}

	/**
	 * <p>setADCCYNO.</p>
	 *
	 * @param adccyno a {@link java.lang.String} object.
	 */
	public void setADCCYNO(String adccyno) {
		ADCCYNO = adccyno;
	}

	/**
	 * <p>getADCCYNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCCYNAME() {
		return ADCCYNAME;
	}

	/**
	 * <p>setADCCYNAME.</p>
	 *
	 * @param adccyname a {@link java.lang.String} object.
	 */
	public void setADCCYNAME(String adccyname) {
		ADCCYNAME = adccyname;
	}

	/**
	 * <p>getMSGCODE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMSGCODE() {
		return(MSGCODE);
	}
	
	/**
	 * <p>setMSGCODE.</p>
	 *
	 * @param msgcode a {@link java.lang.String} object.
	 */
	public void setMSGCODE(String msgcode) {
		MSGCODE = msgcode;
	}

	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}

	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("adcurrency", getADCURRENCY())
				.toString();
	}

	public String getADCCYCHSNAME() {
		return ADCCYCHSNAME;
	}

	public void setADCCYCHSNAME(String aDCCYCHSNAME) {
		ADCCYCHSNAME = aDCCYCHSNAME;
	}

	public String getADCCYENGNAME() {
		return ADCCYENGNAME;
	}

	public void setADCCYENGNAME(String aDCCYENGNAME) {
		ADCCYENGNAME = aDCCYENGNAME;
	}
}
