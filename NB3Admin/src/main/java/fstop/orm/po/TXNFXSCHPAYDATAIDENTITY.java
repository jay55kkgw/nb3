package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 * TXNFXSCHPAYDATAIDENTITY Composite Primary Keys
 *
 * @author Alison
 * @version V1.0
 */
@Embeddable
public class TXNFXSCHPAYDATAIDENTITY implements Serializable {
    
	private String FXSCHNO;
	private String FXUSERID;
	private String FXSCHTXDATE;
	
	public String getFXSCHNO() {
		return FXSCHNO;
	}
	public void setFXSCHNO(String fXSCHNO) {
		FXSCHNO = fXSCHNO;
	}
	public String getFXUSERID() {
		return FXUSERID;
	}
	public void setFXUSERID(String fXUSERID) {
		FXUSERID = fXUSERID;
	}
	public String getFXSCHTXDATE() {
		return FXSCHTXDATE;
	}
	public void setFXSCHTXDATE(String fXSCHTXDATE) {
		FXSCHTXDATE = fXSCHTXDATE;
	}
}
