package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 白名單 PO
 */
@Entity
@Table(name = "ADMLOGINACL")
public class ADMLOGINACL implements Serializable {

    @Id
    private String ADUSERID = "";
    private String LASTUSER;
    private String LASTDATE;
    private String LASTTIME;

    public String getADUSERID() {
        return ADUSERID;
    }

    public void setADUSERID(String aDUSERID) {
        ADUSERID = aDUSERID;
    }

    public String getLASTUSER() {
        return LASTUSER;
    }

    public void setLASTUSER(String lASTUSER) {
        LASTUSER = lASTUSER;
    }

    public String getLASTDATE() {
        return LASTDATE;
    }

    public void setLASTDATE(String lASTDATE) {
        LASTDATE = lASTDATE;
    }

    public String getLASTTIME() {
        return LASTTIME;
    }

    public void setLASTTIME(String lASTTIME) {
        LASTTIME = lASTTIME;
    }
}