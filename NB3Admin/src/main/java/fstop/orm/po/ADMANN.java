package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Since;

/**
 * <p>個人訊息公告內容刊登 PO</p>
 * 
 * @author 簡哥
 * 
 * 20191121-Danny-處理欄位檢核時會錯誤排除
 */
@Entity
@Table(name = "ADMANN")
public class ADMANN implements Serializable {
    /**
     * 案件編號
     */
    @Id
    private String ID;

    private String OId;
    private String TITLE;
    private String CONTENT;
    private String ALLUSER;
    private String USERLIST;
    @Since(1)
    private String FILENAME;
    private String TYPE;
    private String URL ;
    private int SORTORDER;
    private String STARTDATE;
    private String STARTTIME;
    private String ENDDATE;
    private String ENDTIME;
    private String LASTUSER;
    private String LASTDATE;
    private String LASTTIME;
    private String CONTENTTYPE;
    private String SRCFILENAME;
    private byte[]  SRCCONTENT;
    private String ANNCHANNEL;
    
    
    public String getANNCHANNEL() {
		return ANNCHANNEL;
	}

	public void setANNCHANNEL(String aNNCHANNEL) {
		ANNCHANNEL = aNNCHANNEL;
	}

	public String getID() {
        return ID;
    }

    public void setID(String iD) {
        ID = iD;
    }
    
    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String tITLE) {
    	TITLE = tITLE;
    }

    public String getCONTENT() {
        return CONTENT;
    }

    public void setCONTENT(String cONTENT) {
        CONTENT = cONTENT;
    }

    public String getALLUSER() {
        return ALLUSER;
    }

    public void setALLUSER(String aLLUSER) {
        ALLUSER = aLLUSER;
    }

    public String getUSERLIST() {
        return USERLIST;
    }

    public void setUSERLIST(String uSERLIST) {
        USERLIST = uSERLIST;
    }

    public String getFILENAME() {
        return FILENAME;
    }

    public void setFILENAME(String fILENAME) {
        FILENAME = fILENAME;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String tYPE) {
        TYPE = tYPE;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String uRL) {
        URL = uRL;
    }

    public int getSORTORDER() {
        return SORTORDER;
    }

    public void setSORTORDER(int sORTORDER) {
        SORTORDER = sORTORDER;
    }

    public String getSTARTDATE() {
        return STARTDATE;
    }

    public void setSTARTDATE(String sTARTDATE) {
        STARTDATE = sTARTDATE;
    }

    public String getSTARTTIME() {
        return STARTTIME;
    }

    public void setSTARTTIME(String sTARTTIME) {
        STARTTIME = sTARTTIME;
    }

    public String getENDDATE() {
        return ENDDATE;
    }

    public void setENDDATE(String eNDDATE) {
        ENDDATE = eNDDATE;
    }

    public String getENDTIME() {
        return ENDTIME;
    }

    public void setENDTIME(String eNDTIME) {
        ENDTIME = eNDTIME;
    }
    
    public String getLASTUSER() {
        return LASTUSER;
    }

    public void setLASTUSER(String lASTUSER) {
        LASTUSER = lASTUSER;
    }

    public String getLASTDATE() {
        return LASTDATE;
    }

    public void setLASTDATE(String lASTDATE) {
        LASTDATE = lASTDATE;
    }

    public String getLASTTIME() {
        return LASTTIME;
    }

    public void setLASTTIME(String lASTTIME) {
        LASTTIME = lASTTIME;
    }

    public String getOId() {
        return OId;
    }

    public void setOId(String oId) {
        OId = oId;
    }
    public String getCONTENTTYPE() {
        return CONTENTTYPE;
    }

    public void setCONTENTTYPE(String cONTENTTYPE) {
    	CONTENTTYPE = cONTENTTYPE;
    }
    
    public String getSRCFILENAME() {
        return SRCFILENAME;
    }

    public void setSRCFILENAME(String sRCFILENAME) {
    	SRCFILENAME = sRCFILENAME;
    }
    
    public byte[]  getSRCCONTENT() {
        return SRCCONTENT;
    }
    
    public void setSRCCONTENT(byte[] sRCCONTENT) {
    	SRCCONTENT = sRCCONTENT;
    }
}