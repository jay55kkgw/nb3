package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NB3SYSOPGROUP")
public class NB3SYSOPGROUP implements Serializable {
    @Id
    private Integer ADOPGROUPID;  
            
    private String ADOPGROUP="";
    private String ADOPGROUPNAME="";
    private Integer ADSEQ;
    private String ADISPARENT="";
    private String ADGPPARENT="";
    private String LASTUSER=""; 
    private String LASTDATE="";
    private String LASTTIME="";

    public Integer getADOPGROUPID() {
        return ADOPGROUPID;
    }

    public void setADOPGROUPID(Integer aDOPGROUPID) {
        ADOPGROUPID = aDOPGROUPID;
    }

    public String getADOPGROUP() {
        return ADOPGROUP;
    }

    public void setADOPGROUP(String aDOPGROUP) {
        ADOPGROUP = aDOPGROUP;
    }

    public String getADOPGROUPNAME() {
        return ADOPGROUPNAME;
    }

    public void setADOPGROUPNAME(String aDOPGROUPNAME) {
        ADOPGROUPNAME = aDOPGROUPNAME;
    }

    public Integer getADSEQ() {
        return ADSEQ;
    }

    public void setADSEQ(Integer aDSEQ) {
        ADSEQ = aDSEQ;
    }

    public String getADISPARENT() {
        return ADISPARENT;
    }

    public void setADISPARENT(String aDISPARENT) {
        ADISPARENT = aDISPARENT;
    }

    public String getADGPPARENT() {
        return ADGPPARENT;
    }

    public void setADGPPARENT(String aDGPPARENT) {
        ADGPPARENT = aDGPPARENT;
    }

    public String getLASTUSER() {
        return LASTUSER;
    }

    public void setLASTUSER(String lASTUSER) {
        LASTUSER = lASTUSER;
    }

    public String getLASTDATE() {
        return LASTDATE;
    }

    public void setLASTDATE(String lASTDATE) {
        LASTDATE = lASTDATE;
    }

    public String getLASTTIME() {
        return LASTTIME;
    }

    public void setLASTTIME(String lASTTIME) {
        LASTTIME = lASTTIME;
    }

    
}