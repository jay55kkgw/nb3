package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>SYSPARAM class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "SYSPARAM")
public class SYSPARAM implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer ADPARAMID;

	private String ADPARAMNAME;

	private String ADPARAMVALUE;

	private String ADPARAMMEMO;
	
	/**
	 * <p>getADPARAMID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public Integer getADPARAMID() {
		return ADPARAMID;
	}

	/**
	 * <p>setADPARAMID.</p>
	 *
	 * @param adparamid a {@link java.lang.String} object.
	 */
	public void setADPARAMID(Integer adparamid) {
		ADPARAMID = adparamid;
	}

	/**
	 * <p>getADPARAMMEMO.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADPARAMMEMO() {
		return ADPARAMMEMO;
	}

	/**
	 * <p>setADPARAMMEMO.</p>
	 *
	 * @param adparammemo a {@link java.lang.String} object.
	 */
	public void setADPARAMMEMO(String adparammemo) {
		ADPARAMMEMO = adparammemo;
	}

	/**
	 * <p>getADPARAMNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADPARAMNAME() {
		return ADPARAMNAME;
	}

	/**
	 * <p>setADPARAMNAME.</p>
	 *
	 * @param adparamname a {@link java.lang.String} object.
	 */
	public void setADPARAMNAME(String adparamname) {
		ADPARAMNAME = adparamname;
	}

	/**
	 * <p>getADPARAMVALUE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADPARAMVALUE() {
		return ADPARAMVALUE;
	}

	/**
	 * <p>setADPARAMVALUE.</p>
	 *
	 * @param adparamvalue a {@link java.lang.String} object.
	 */
	public void setADPARAMVALUE(String adparamvalue) {
		ADPARAMVALUE = adparamvalue;
	}

}
