package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>ADMMYREMITMENU class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMMYREMITMENU")
public class ADMMYREMITMENU implements Serializable { 

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADSEQNO;

	private String ADUID = "";
	private String ADRMTTYPE = "";	
	private String ADRMTID = "";

	private String LASTUSER = "";
	private String LASTDATE = "";
	private String LASTTIME = "";


	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("adseqno", getADSEQNO())
				.toString();
	}


	/**
	 * <p>getADRMTID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADRMTID() {
		return ADRMTID;
	}


	/**
	 * <p>setADRMTID.</p>
	 *
	 * @param adrmtid a {@link java.lang.String} object.
	 */
	public void setADRMTID(String adrmtid) {
		ADRMTID = adrmtid;
	}


	/**
	 * <p>getADSEQNO.</p>
	 *
	 * @return a {@link java.lang.Long} object.
	 */
	public Long getADSEQNO() {
		return ADSEQNO;
	}


	/**
	 * <p>setADSEQNO.</p>
	 *
	 * @param adseqno a {@link java.lang.Long} object.
	 */
	public void setADSEQNO(Long adseqno) {
		ADSEQNO = adseqno;
	}


	/**
	 * <p>getADUID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADUID() {
		return ADUID;
	}


	/**
	 * <p>setADUID.</p>
	 *
	 * @param aduid a {@link java.lang.String} object.
	 */
	public void setADUID(String aduid) {
		ADUID = aduid;
	}

	/**
	 * <p>getADRMTTYPE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADRMTTYPE() {
		return ADRMTTYPE;
	}


	/**
	 * <p>setADRMTTYPE.</p>
	 *
	 * @param adrmttype a {@link java.lang.String} object.
	 */
	public void setADRMTTYPE(String adrmttype) {
		ADRMTTYPE = adrmttype;
	}
	
	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}


	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}


	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}


	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}


	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}


	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}



}
