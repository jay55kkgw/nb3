package fstop.orm.po;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.aop.Authorize;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.netbank.rest.web.back.controller.BM2000Controller;
import com.netbank.rest.web.back.model.JsonResponse;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * ADMAPPVER class.
 * </p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMAPPVER")
public class ADMAPPVER implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 800758926278527527L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer APPID;
	private String APPOS = "";
	private String APPMODELNO = "";
	private String VERNO = "";
	private String VERDATE = "";
	private String VERSTATUS = "";
	private String VERMSG = "";
	private String VERURL = "";
	private String TXDATE = "";
	private String TXTIME = "";
	private String USERID = "";
	private String USERNAME = "";

	public String getAPPOS() {
		return APPOS;
	}

	public String getVERNO() {
		return VERNO;
	}

	public void setVERNO(String vERNO) {
		this.VERNO = vERNO;
	}

	public Integer getAPPID() {
		return APPID;
	}

	public void setAPPID(Integer aPPID) {
		this.APPID = aPPID;
	}

	public String getAPPMODELNO() {
		return APPMODELNO;
	}

	public void setAPPMODELNO(String aPPMODELNO) {
		this.APPMODELNO = aPPMODELNO;
	}

	public void setAPPOS(String aPPOS) {
		this.APPOS = aPPOS;
	}

	public String getVERDATE() {
		return VERDATE;
	}

	public void setVERDATE(String vERDATE) {
		VERDATE = vERDATE;
	}

	public String getVERSTATUS() {
		return VERSTATUS;
	}

	public void setVERSTATUS(String vERSTATUS) {
		VERSTATUS = vERSTATUS;
	}

	public String getVERMSG() {
		return VERMSG;
	}

	public void setVERMSG(String vERMSG) {
		VERMSG = vERMSG;
	}

	public String getVERURL() {
		return VERURL;
	}

	public void setVERURL(String vERURL) {
		VERURL = vERURL;
	}

	public String getTXDATE() {
		return TXDATE;
	}

	public void setTXDATE(String tXDATE) {
		TXDATE = tXDATE;
	}

	public String getTXTIME() {
		return TXTIME;
	}

	public void setTXTIME(String tXTIME) {
		TXTIME = tXTIME;
	}

	public String getUSERID() {
		return USERID;
	}

	public void setUSERID(String uSERID) {
		USERID = uSERID;
	}

	public String getUSERNAME() {
		return USERNAME;
	}

	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}

	   /**
     * <p>toString.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String toString() {
		return new ToStringBuilder(this).append("APPID", getAPPID())
				.toString();
	}
}
