package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.google.gson.annotations.Since;

import lombok.*;

/**
 * 廣告主檔，已放行的廣告會由 Temp 檔搬到主檔
 *
 * @author 簡哥
 * @version V1.0
 * 
 * 20191121-Danny-處理Byte[]欄位檢核時會錯誤排除
 */
@Entity
@Table(name = "ADMSTORE")
@Getter
@Setter
public class ADMSTORE implements Serializable {
    @Id
    private String STID;

    @NotEmpty(message="上稿單位不可為空值.")
    private String OID;

    @NotEmpty(message="上架日期不可為空值.")
    private String SDATE;

    @NotEmpty(message="上架時間不可為空值.")
    private String STIME;

    @NotEmpty(message="下架日期不可為空值.")
    private String EDATE;

    @NotEmpty(message="下架時間不可為空值.")
    private String ETIME;
    
    // 特店類別
    private String ADTYPE;
    
    // 商店分類
    private String STTYPE;
    // 權重
    private String STWEIGHT;
    // 圖片路徑
    private String STPICADD;
    // 圖片
    @Since(1)
    private byte[] STPICDATA;
    // 服務專線
    private String STTEL;
    // 特店優惠-優惠內容日期
    private String STADDATE;
    // 優惠內容顯示以下內容
    private String STADSHOW;
    // 注意事項
    private String STADNOTE;
    // 最後修改者
    private String LASTUSER;
    // 最後修改日
    private String LASTDATE;
    // 最後修改時間
    private String LASTTIME;
    // 分店資訊
    private String STADINFO;
    // 優惠內容
    private String STDADCONT;
    // 優惠標題
    private String STDADHLK;
}
