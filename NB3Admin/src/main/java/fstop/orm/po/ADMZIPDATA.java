package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>ADMZIPDATA class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMZIPDATA")
public class ADMZIPDATA implements Serializable {

	@Id
	private String ZIPCODE;

	private String CITY;

	private String AREA;

	private String ROAD;


	/**
	 * <p>getZIPCODE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getZIPCODE() {
		return ZIPCODE;
	}

	/**
	 * <p>setZIPCODE.</p>
	 *
	 * @param zipcode a {@link java.lang.String} object.
	 */
	public void setZIPCODE(String zipcode) {
		ZIPCODE = zipcode;
	}

	/**
	 * <p>getCITY.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCITY() {
		return CITY;
	}

	/**
	 * <p>setCITY.</p>
	 *
	 * @param city a {@link java.lang.String} object.
	 */
	public void setCITY(String city) {
		CITY = city;
	}

	/**
	 * <p>getAREA.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAREA() {
		return AREA;
	}

	/**
	 * <p>setAREA.</p>
	 *
	 * @param area a {@link java.lang.String} object.
	 */
	public void setAREA(String area) {
		AREA = area;
	}

	/**
	 * <p>getROAD.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getROAD() {
		return ROAD;
	}

	/**
	 * <p>setROAD.</p>
	 *
	 * @param road a {@link java.lang.String} object.
	 */
	public void setROAD(String road) {
		ROAD = road;
	}
}
