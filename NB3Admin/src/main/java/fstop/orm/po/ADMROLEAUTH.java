package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>ADMROLEAUTH class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMROLEAUTH")
public class ADMROLEAUTH implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer ADAUTHID;

	@NotEmpty(message = "角色代碼不可為空值")
	private String ADROLENO;

	private String ADSTAFFNO;

	private String ADAUTH;

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	@NotEmpty(message = "功能代碼不可為空值")
	private String APOPID = "";

	/**
	 * 給 View 做顯示用，不會進資料庫
	 */
	@Transient
	private String APOPName = "";

	private String ISQUERY = "";

	private String ISEDIT = "";

	private String ISEXEC = "";

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("adauthid", getADAUTHID())
				.toString();
	}

	/**
	 * <p>getADAUTH.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADAUTH() {
		return ADAUTH;
	}

	/**
	 * <p>setADAUTH.</p>
	 *
	 * @param adauth a {@link java.lang.String} object.
	 */
	public void setADAUTH(String adauth) {
		ADAUTH = adauth;
	}

	/**
	 * <p>getADAUTHID.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getADAUTHID() {
		return ADAUTHID;
	}

	/**
	 * <p>setADAUTHID.</p>
	 *
	 * @param adauthid a {@link java.lang.Integer} object.
	 */
	public void setADAUTHID(Integer adauthid) {
		ADAUTHID = adauthid;
	}

	/**
	 * <p>getADROLENO.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADROLENO() {
		return ADROLENO;
	}

	/**
	 * <p>setADROLENO.</p>
	 *
	 * @param adroleno a {@link java.lang.String} object.
	 */
	public void setADROLENO(String adroleno) {
		ADROLENO = adroleno;
	}

	/**
	 * <p>getADSTAFFNO.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADSTAFFNO() {
		return ADSTAFFNO;
	}

	/**
	 * <p>setADSTAFFNO.</p>
	 *
	 * @param adstaffno a {@link java.lang.String} object.
	 */
	public void setADSTAFFNO(String adstaffno) {
		ADSTAFFNO = adstaffno;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}

	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	/**
	 * <p>getAPOPID.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAPOPID() {
		return APOPID;
	}

	/**
	 * <p>setAPOPID.</p>
	 *
	 * @param aPOPID a {@link java.lang.String} object.
	 */
	public void setAPOPID(String aPOPID) {
		APOPID = aPOPID;
	}

	/**
	 * <p>getISQUERY.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getISQUERY() {
		return ISQUERY;
	}

	/**
	 * <p>setISQUERY.</p>
	 *
	 * @param iSQUERY a {@link java.lang.String} object.
	 */
	public void setISQUERY(String iSQUERY) {
		ISQUERY = iSQUERY;
	}

	/**
	 * <p>getISEDIT.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getISEDIT() {
		return ISEDIT;
	}

	/**
	 * <p>setISEDIT.</p>
	 *
	 * @param iSEDIT a {@link java.lang.String} object.
	 */
	public void setISEDIT(String iSEDIT) {
		ISEDIT = iSEDIT;
	}

	/**
	 * <p>getISEXEC.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getISEXEC() {
		return ISEXEC;
	}

	/**
	 * <p>setISEXEC.</p>
	 *
	 * @param iSEXEC a {@link java.lang.String} object.
	 */
	public void setISEXEC(String iSEXEC) {
		ISEXEC = iSEXEC;
	}

	/**
	 * <p>getAPOPName.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAPOPName() {
		return APOPName;
	}

	/**
	 * <p>setAPOPName.</p>
	 *
	 * @param aPOPName a {@link java.lang.String} object.
	 */
	public void setAPOPName(String aPOPName) {
		APOPName = aPOPName;
	}

	
}
