package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 流程定義 POJO
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMFLOWDEF")
public class ADMFLOWDEF implements Serializable {

	@Id
    private String FLOWID;
    
    private String FLOWNAME;

    private String LASTUSER;
    
    private String LASTDATE;
    
    private String LASTTIME;

    /**
     * <p>getFLOWID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFLOWID() {
        return FLOWID;
    }

    /**
     * <p>setFLOWID.</p>
     *
     * @param fLOWID a {@link java.lang.String} object.
     */
    public void setFLOWID(String fLOWID) {
        FLOWID = fLOWID;
    }

    /**
     * <p>getFLOWNAME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFLOWNAME() {
        return FLOWNAME;
    }

    /**
     * <p>setFLOWNAME.</p>
     *
     * @param fLOWNAME a {@link java.lang.String} object.
     */
    public void setFLOWNAME(String fLOWNAME) {
        FLOWNAME = fLOWNAME;
    }

    /**
     * <p>getLASTUSER.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLASTUSER() {
        return LASTUSER;
    }

    /**
     * <p>setLASTUSER.</p>
     *
     * @param lASTUSER a {@link java.lang.String} object.
     */
    public void setLASTUSER(String lASTUSER) {
        LASTUSER = lASTUSER;
    }

    /**
     * <p>getLASTDATE.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLASTDATE() {
        return LASTDATE;
    }

    /**
     * <p>setLASTDATE.</p>
     *
     * @param lASTDATE a {@link java.lang.String} object.
     */
    public void setLASTDATE(String lASTDATE) {
        LASTDATE = lASTDATE;
    }

    /**
     * <p>getLASTTIME.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLASTTIME() {
        return LASTTIME;
    }

    /**
     * <p>setLASTTIME.</p>
     *
     * @param lASTTIME a {@link java.lang.String} object.
     */
    public void setLASTTIME(String lASTTIME) {
        LASTTIME = lASTTIME;
    }

    /**
     * <p>toString.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String toString() {
		return new ToStringBuilder(this).append("FLOWID", getFLOWID()).toString();
	}
}  
