package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>ADMCOUNTRY class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMCOUNTRY")
public class ADMCOUNTRY implements Serializable {

	private static final long serialVersionUID = -3827793514299545659L;

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@NotEmpty(message="國別代碼不可為空值")
	private String ADCTRYCODE;

	@NotEmpty(message="國別繁中名稱不可為空值")
	private String ADCTRYNAME;

	@NotEmpty(message="國別英文名稱不可為空值")
	private String ADCTRYENGNAME;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	@NotEmpty(message="國別簡中名稱不可為空值")
	private String ADCTRYCHSNAME;

//	private Set ADMCURRENCIES;

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return new ToStringBuilder(this).append("adctrycode", getADCTRYCODE())
				.toString();
	}

	/**
	 * <p>getADCTRYCODE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCTRYCODE() {
		return ADCTRYCODE;
	}

	/**
	 * <p>setADCTRYCODE.</p>
	 *
	 * @param adctrycode a {@link java.lang.String} object.
	 */
	public void setADCTRYCODE(String adctrycode) {
		ADCTRYCODE = adctrycode;
	}

	/**
	 * <p>getADCTRYENGNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCTRYENGNAME() {
		return ADCTRYENGNAME;
	}

	/**
	 * <p>setADCTRYENGNAME.</p>
	 *
	 * @param adctryengname a {@link java.lang.String} object.
	 */
	public void setADCTRYENGNAME(String adctryengname) {
		ADCTRYENGNAME = adctryengname;
	}

	/**
	 * <p>getADCTRYNAME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADCTRYNAME() {
		return ADCTRYNAME;
	}

	/**
	 * <p>setADCTRYNAME.</p>
	 *
	 * @param adctryname a {@link java.lang.String} object.
	 */
	public void setADCTRYNAME(String adctryname) {
		ADCTRYNAME = adctryname;
	}

	/**
	 * <p>getLASTDATE.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTDATE() {
		return LASTDATE;
	}

	/**
	 * <p>setLASTDATE.</p>
	 *
	 * @param lastdate a {@link java.lang.String} object.
	 */
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	/**
	 * <p>getLASTTIME.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTTIME() {
		return LASTTIME;
	}

	/**
	 * <p>setLASTTIME.</p>
	 *
	 * @param lasttime a {@link java.lang.String} object.
	 */
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	/**
	 * <p>getLASTUSER.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLASTUSER() {
		return LASTUSER;
	}

	/**
	 * <p>setLASTUSER.</p>
	 *
	 * @param lastuser a {@link java.lang.String} object.
	 */
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	public String getADCTRYCHSNAME() {
		return ADCTRYCHSNAME;
	}

	public void setADCTRYCHSNAME(String aDCTRYCHSNAME) {
		ADCTRYCHSNAME = aDCTRYCHSNAME;
	}

}
