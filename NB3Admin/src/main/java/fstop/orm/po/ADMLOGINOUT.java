package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * ADMLOGINOUT class.
 * </p>
 *
 * @author chiensj
 * @version V1.0
 */
@Entity
@Table(name = "ADMLOGINOUT")
public class ADMLOGINOUT implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer ADLOGINOUTID;

	private String ADUSERTYPE = "";

	private String ADUSERID = "";

	private String ADUSERIP = "";

	private String CMYYYYMM = "";

	private String CMDD = "";

	private String CMTIME = "";

	private String LOGINOUT = ""; // 0. 登入, 1. 登出

	private String LOGINTYPE = ""; // NB:網銀 ， MB:行動 ，預設為NB

	private String TOKENID = "";

	// 2021/01/15新增
	/*
	 * 新增ADEXCODE varchar(10), not null, default ""
	 * 值相當於 TXNLOG.ADEXCODE 
	 * "": 表示成功
	 * 有值: 表示失敗代碼
	 */

	private String ADEXCODE = "";

	
	
	public String getADEXCODE() {
		return ADEXCODE;
	}

	public void setADEXCODE(String aDEXCODE) {
		ADEXCODE = aDEXCODE;
	}

	/**
	 * <p>
	 * getADLOGINOUTID.
	 * </p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getADLOGINOUTID() {
		return ADLOGINOUTID;
	}

	/**
	 * <p>
	 * setADLOGINOUTID.
	 * </p>
	 *
	 * @param adloginoutid a {@link java.lang.Integer} object.
	 */
	public void setADLOGINOUTID(Integer adloginoutid) {
		ADLOGINOUTID = adloginoutid;
	}

	/**
	 * <p>
	 * getADUSERID.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADUSERID() {
		return ADUSERID;
	}

	/**
	 * <p>
	 * setADUSERID.
	 * </p>
	 *
	 * @param aduserid a {@link java.lang.String} object.
	 */
	public void setADUSERID(String aduserid) {
		ADUSERID = aduserid;
	}

	/**
	 * <p>
	 * getADUSERIP.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADUSERIP() {
		return ADUSERIP;
	}

	/**
	 * <p>
	 * setADUSERIP.
	 * </p>
	 *
	 * @param aduserip a {@link java.lang.String} object.
	 */
	public void setADUSERIP(String aduserip) {
		ADUSERIP = aduserip;
	}

	/**
	 * <p>
	 * getADUSERTYPE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getADUSERTYPE() {
		return ADUSERTYPE;
	}

	/**
	 * <p>
	 * setADUSERTYPE.
	 * </p>
	 *
	 * @param adusertype a {@link java.lang.String} object.
	 */
	public void setADUSERTYPE(String adusertype) {
		ADUSERTYPE = adusertype;
	}

	/**
	 * <p>
	 * getCMDD.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCMDD() {
		return CMDD;
	}

	/**
	 * <p>
	 * setCMDD.
	 * </p>
	 *
	 * @param cmdd a {@link java.lang.String} object.
	 */
	public void setCMDD(String cmdd) {
		CMDD = cmdd;
	}

	/**
	 * <p>
	 * getCMTIME.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCMTIME() {
		return CMTIME;
	}

	/**
	 * <p>
	 * setCMTIME.
	 * </p>
	 *
	 * @param cmtime a {@link java.lang.String} object.
	 */
	public void setCMTIME(String cmtime) {
		CMTIME = cmtime;
	}

	/**
	 * <p>
	 * getCMYYYYMM.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCMYYYYMM() {
		return CMYYYYMM;
	}

	/**
	 * <p>
	 * setCMYYYYMM.
	 * </p>
	 *
	 * @param cmyyyymm a {@link java.lang.String} object.
	 */
	public void setCMYYYYMM(String cmyyyymm) {
		CMYYYYMM = cmyyyymm;
	}

	/**
	 * <p>
	 * getLOGINOUT.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLOGINOUT() {
		return LOGINOUT;
	}

	/**
	 * <p>
	 * setLOGINOUT.
	 * </p>
	 *
	 * @param loginout a {@link java.lang.String} object.
	 */
	public void setLOGINOUT(String loginout) {
		LOGINOUT = loginout;
	}

	/**
	 * <p>
	 * getLOGINTYPE.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	/**
	 * <p>
	 * setLOGINTYPE.
	 * </p>
	 *
	 * @param logintype a {@link java.lang.String} object.
	 */
	public void setLOGINTYPE(String logintype) {
		LOGINTYPE = logintype;
	}

	/**
	 * <p>
	 * getTOKENID.
	 * </p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTOKENID() {
		return TOKENID;
	}

	/**
	 * <p>
	 * setTOKENID.
	 * </p>
	 *
	 * @param tOKENID a {@link java.lang.String} object.
	 */
	public void setTOKENID(String tOKENID) {
		TOKENID = tOKENID;
	}

}
