package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sun.istack.NotNull;

/**
 * ADMFLOWCURRENTIDENTITY Composite Primary Keys
 *
 * @author chiensj
 * @version V1.0
 */
@Embeddable
public class ADMFLOWCURRENTIDENTITY implements Serializable {
    private String CASESN;

    //@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer BID;

    /**
     * <p>getCASESN.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCASESN() {
        return CASESN;
    }

    /**
     * <p>setCASESN.</p>
     *
     * @param cASESN a {@link java.lang.String} object.
     */
    public void setCASESN(String cASESN) {
        CASESN = cASESN;
    }

    /**
     * <p>getBID.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getBID() {
        return BID;
    }

    /**
     * <p>setBID.</p>
     *
     * @param bID a {@link java.lang.Integer} object.
     */
    public void setBID(Integer bID) {
        BID = bID;
    }
}
