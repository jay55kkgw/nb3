package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;

/**
 * 上傳附件暫存檔案
 *
 * @author chiensj
 * @version V1.0
 * 20191121-Danny-處理Byte[]欄位檢核時會錯誤排除
 */
@Data
@Embeddable
public class ADMSYSCODED_PK implements Serializable {
	private static final long serialVersionUID = 1L;

    private String CODEKIND;	//代碼種類
	private String CODEID;	//代碼
}
