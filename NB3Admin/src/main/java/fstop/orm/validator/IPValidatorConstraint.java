package fstop.orm.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Payload;
import javax.validation.Constraint;

@Documented
@Constraint(validatedBy = IPValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface IPValidatorConstraint {
    String message() default "IP 格式錯誤，格式為：0.0.0.0 ~ 255.255.255.255";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}