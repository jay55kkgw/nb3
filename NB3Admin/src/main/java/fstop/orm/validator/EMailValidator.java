package fstop.orm.validator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EMailValidator implements ConstraintValidator<EMailValidatorConstraint, String> {
 
    @Override
    public void initialize(EMailValidatorConstraint contactNumber) {
    }
 
    @Override
    public boolean isValid(String contactField, ConstraintValidatorContext cxt) {
        Pattern EMAIL_PATTERN = Pattern.compile("^\\w+\\.*\\w+@(\\w+\\.){1,5}[a-zA-Z]{2,3}$");

        if ( contactField.isEmpty() )
            return true;

        boolean result = true;
        String[] emails = contactField.split("\\;");
        for (String email : emails) {
            if (!EMAIL_PATTERN.matcher(email).matches()) {
                result = false;
                break;
            }
        }
        
        return result;
    }
}