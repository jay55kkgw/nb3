package fstop.orm.validator;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateValidator implements ConstraintValidator<DateValidatorConstraint, String> {
 
    @Override
    public void initialize(DateValidatorConstraint contactNumber) {
    }
 
    @Override
    public boolean isValid(String contactField, ConstraintValidatorContext cxt) {
        if ( contactField.isEmpty() )
            return true;
            
        int len = contactField.length();
        if ( len != 8 ) {
            return false;
        }
        //設定日期格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        sdf.setLenient(false);
        try {
            Date dd = sdf.parse(contactField);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}