package fstop.orm.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Payload;
import javax.validation.Constraint;

@Documented
@Constraint(validatedBy = EMailValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface EMailValidatorConstraint {
    String message() default "EMail 格式錯誤，格式為：name@domain.com;name2@domain.com";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}