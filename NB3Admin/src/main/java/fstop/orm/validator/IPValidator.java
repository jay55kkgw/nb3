package fstop.orm.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IPValidator implements ConstraintValidator<IPValidatorConstraint, String> {
 
    @Override
    public void initialize(IPValidatorConstraint contactNumber) {
    }
 
    @Override
    public boolean isValid(String contactField, ConstraintValidatorContext cxt) {
        if ( contactField.isEmpty() )
            return true;

        try {
            boolean result = true;
            String[] tmp = contactField.split("\\.");
            if (tmp.length<=4) {
                for (int i = 0; i < tmp.length; i++) {
                    if (Integer.parseInt(tmp[i]) > 255) {
                        result = false;
                        break;
                    }
                }
                return result;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}