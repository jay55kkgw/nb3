package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMHOLIDAY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>AdmHolidayDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmHolidayDao extends LegacyJpaRepository<ADMHOLIDAY, String> {
	/**
	 * 以查詢條件做分頁查詢
	 *
	 * @param pageNo 第幾頁
	 * @param pageSize 一頁幾筆
	 * @param orderBy 依什麼欄位排序
	 * @param orderDir 升冪/降冪
	 * @param ADHOLIDAYID 假日代碼
	 * @param ADREMARK 備註
	 * @return 分頁物件
	 */
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ADHOLIDAYID, String ADREMARK) {
		String hql = "FROM ADMHOLIDAY WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if(ADHOLIDAYID != null && !ADHOLIDAYID.trim().isEmpty()) {
			hql += "AND ADHOLIDAYID LIKE ? ";
			params.add("%"+ADHOLIDAYID+"%");
		}
		if(ADREMARK != null && !ADREMARK.trim().isEmpty()) {
			hql += "AND ADREMARK LIKE ? ";
			params.add("%"+ADREMARK+"%");
		}
		hql += " ORDER BY "+ orderBy+" "+orderDir;

		log.trace(Sanitizer.logForgingStr(hql));

		switch ( params.size() ) {
			case 0:
				return this.pagedQuery(hql, pageNo, pageSize);

			case 1:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0));

			case 2:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));
				
			default:
				return this.pagedQuery(hql, pageNo, pageSize);
		}
	}
}
