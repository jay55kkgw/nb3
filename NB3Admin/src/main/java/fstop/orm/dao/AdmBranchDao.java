package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMBRANCH;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>AdmBranchDao class.</p>
 *
 * @author jinhanhuang
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmBranchDao extends LegacyJpaRepository<ADMBRANCH, String> {
	protected String _BHTYPE;

	/**
	 * <p>findPageDataByLike.</p>
	 *
	 * @param pageNo a int.
	 * @param pageSize a int.
	 * @param orderBy a {@link java.lang.String} object.
	 * @param orderDir a {@link java.lang.String} object.
	 * @param BHADTYPE a {@link java.lang.String} object.
	 * @param BHNAME a {@link java.lang.String} object.
	 * @param BHCOUNTY a {@link java.lang.String} object.
	 * @return a {@link com.netbank.domain.orm.core.Page} object.
	 */
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ADBRANCHID,
			String ADBRANCHNAME) {
		String hql = "FROM ADMBRANCH WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if (ADBRANCHID != null && !ADBRANCHID.trim().isEmpty()) {
			hql += "AND ADBRANCHID LIKE ? ";
			params.add("%" + ADBRANCHID + "%");
		}
		if (ADBRANCHNAME != null && !ADBRANCHNAME.trim().isEmpty()) {
			hql += "AND ADBRANCHNAME LIKE ? ";
			params.add("%" + ADBRANCHNAME + "%");
		}
		
		hql += " ORDER BY " + orderBy + " " + orderDir;

		log.trace(Sanitizer.logForgingStr(hql));

		return this.pagedQuery(hql, pageNo, pageSize, params);
	}
}
