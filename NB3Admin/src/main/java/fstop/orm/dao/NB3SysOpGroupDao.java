package fstop.orm.dao;

import java.util.List;

import javax.transaction.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import org.springframework.stereotype.Repository;

import fstop.orm.po.NB3SYSOPGROUP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>前台交易大類 DAO</p>
 * 
 * @author  簡哥
 */
@Slf4j
@Repository
@Transactional
public class NB3SysOpGroupDao extends LegacyJpaRepository<NB3SYSOPGROUP, Integer> {
    /**
     * <p>取得大類</p>
     * @return  NB3SYSOPGROUP 清單
     */
    public List<NB3SYSOPGROUP> getParentGroups() {
        String hql = "FROM NB3SYSOPGROUP WHERE ADISPARENT=? OR ADISPARENT=? ORDER BY ADSEQ ASC";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[2];
        params[0] = "Y";
        params[1] = "";

        return find(hql, params);
    }

    /**
     * <p>取得小類</p>
     * @param adpparent     parent group 代號
     * @return              NB3SYSOPGROUP 清單
     */
    public List<NB3SYSOPGROUP> getChildGroups(String adpparent) {
        String hql = "FROM NB3SYSOPGROUP WHERE ADGPPARENT=? ORDER BY ADSEQ ASC";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[1];
        params[0] = adpparent;

        return find(hql, params);
    }

    /**
     * <p>取得交易分類（無子分類者）</p>
     * @return      NB3SYSOPGROUP 清單
     */
    public List<NB3SYSOPGROUP> getLeafGroups() {
        String hql = "FROM NB3SYSOPGROUP WHERE ADGPPARENT in ( ?, ? ) ORDER BY ADSEQ ASC";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[2];
        params[0] = "N";
        params[1] = "";

        return find(hql, params);
    }
}