package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import fstop.orm.po.PASSIP;
import fstop.orm.po.NBIPIDENTITY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class PassIPDao extends LegacyJpaRepository<PASSIP, NBIPIDENTITY> {
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ADBRANCHID, String ADBRANCHIP) {
        String hql = "FROM PASSIP WHERE ADBRANCHID=? ";
        List<Object> params = new ArrayList<Object>();
        params.add(ADBRANCHID);

        if(ADBRANCHIP != null && !ADBRANCHIP.trim().isEmpty()) {
            hql += "AND ADBRANCHIP LIKE ? ";
            params.add("%"+ADBRANCHIP+"%");
        }
            
        log.trace(Sanitizer.logForgingStr(hql));

        return this.pagedQuery(hql, pageNo, pageSize, params);
    }
}
