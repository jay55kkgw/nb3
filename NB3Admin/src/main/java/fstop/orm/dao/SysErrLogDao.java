package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.springframework.stereotype.Repository;

import fstop.orm.po.SYSERRLOG;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class SysErrLogDao extends LegacyJpaRepository<SYSERRLOG, Integer> {
    /**
     * 依調件查詢
     * @param pageNo        頁碼
     * @param pageSize      一頁資料量
     * @param orderBy       排序欄位
     * @param orderDir      排序方向
     * @param startDate     開始日期
     * @param startTime     開始時間
     * @param endDate       結束日時
     * @param endTime       結束時間
     * @return              分頁物件
     */
    public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String startDate, String startTime, String endDate, String endTime) {
		String hql = "FROM SYSERRLOG WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if((!startDate.isEmpty() && !startTime.isEmpty()) && (!endDate.isEmpty() && !endTime.isEmpty())) {
			hql += "AND ( CONCAT(LOGDATE,LOGTIME)>=? AND CONCAT(LOGDATE,LOGTIME)<=? ) ";
			params.add(startDate+startTime);
			params.add(endDate+endTime);
		}  
		if(!startDate.isEmpty() && endTime.isEmpty()) {
			hql += "AND CONCAT(LOGDATE,LOGTIME)>=? ";
			params.add(startDate+startTime);
		}
		if(startTime.isEmpty() && !endTime.isEmpty()) {
			hql += "AND CONCAT(LOGDATE,LOGTIME)<=? ";
			params.add(endDate+endTime);
		} 
		hql += " ORDER BY "+ orderBy+" "+orderDir;

		log.debug(Sanitizer.logForgingStr(hql));

		switch ( params.size() ) {
			case 0:
				return this.pagedQuery(hql, pageNo, pageSize);

			case 1:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0));

			case 2:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));

			case 3:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2));

			default:
				return this.pagedQuery(hql, pageNo, pageSize);
		}
	}
}