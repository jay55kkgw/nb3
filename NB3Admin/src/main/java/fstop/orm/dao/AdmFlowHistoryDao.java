package fstop.orm.dao;

import java.util.List;

import javax.transaction.Transactional;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import org.springframework.stereotype.Repository;
import fstop.orm.po.ADMFLOWHISTORY;

/**
 * <p>流程歷程 DAO</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Repository
@Transactional
public class AdmFlowHistoryDao extends LegacyJpaRepository<ADMFLOWHISTORY, String> {
    /**
     * 依案號取得案件歷程
     *
     * @param caseSN a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings("unchecked")
    public List<ADMFLOWHISTORY> findByCaseSN(String caseSN) {
        String hql = "From ADMFLOWHISTORY Where CASESN=? ORDER BY ID DESC";
        return find(hql, caseSN);
    }
}
