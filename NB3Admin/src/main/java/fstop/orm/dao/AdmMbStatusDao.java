package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMMBSTATUS;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


/**
 * <p>AdmMbStatusDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Repository
public class AdmMbStatusDao extends LegacyJpaRepository<ADMMBSTATUS,String> {
	
	protected Logger logger = Logger.getLogger(getClass());

}
