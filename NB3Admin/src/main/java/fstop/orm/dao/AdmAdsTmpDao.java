package fstop.orm.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMADSTMP;


/**
 * <p>廣告管理暫存檔 DAO</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Repository
@Transactional
public class AdmAdsTmpDao extends LegacyJpaRepository<ADMADSTMP, String> {

     // 超級使用者
    private final String SUPER_ROLES = "DC";

    /**
     * 依單位查詢廣告, 不要用 JPA 標準的方法查詢，因為有 BLOB 欄位，會變得很慢
     *
     * @param oid 單位代碼
     * @param startDate a {@link java.lang.String} object.
     * @param endDate a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<ADMADSTMP> findByParams(String oid, String startDate, String endDate, Set<String> roles, String flowFinished) {
        // 只查需要的欄位
        String sql = "SELECT a.ID, a.TITLE, a.STEPNAME, a.STARTDATE, a.ENDDATE, a.STARTTIME, a.ENDTIME, a.OID, b.EDITORUNAME FROM ADMADSTMP a ";
        sql += "LEFT JOIN ADMFLOWCURRENT b ON a.ID=b.CASESN  WHERE 1=1 And FLOWFINISHED=? ";
        
        List<Object> params = new ArrayList<Object>();
        
        params.add(flowFinished);
        
        if (!roles.contains(SUPER_ROLES)){
            sql += " AND OID=? ";
            params.add(oid);
        }

        if(!startDate.trim().isEmpty() && !endDate.trim().isEmpty() ) {
            sql += " AND ((STARTDATE>=?) AND (STARTDATE<=?)) ";
            params.add(startDate);
            params.add(endDate);
        }
        if(startDate.trim().isEmpty() && !endDate.trim().isEmpty() ) {
			sql += " AND STARTDATE<=? ";
            params.add(endDate);
        }
        if(!startDate.trim().isEmpty() && endDate.trim().isEmpty() ) {
			sql += " STARTDATE>=? ";
            params.add(startDate);
        }

        Query query = getSession().createSQLQuery(sql);
        for ( int i=0; i<params.size(); i++ ) {
            query.setParameter(i, params.get(i));
        }
 
        List<ADMADSTMP> queryResults = new ArrayList<ADMADSTMP>();
        List qresult = query.getResultList();
        Iterator it = qresult.iterator();
		while (it.hasNext()) {
            Object[] o = (Object[]) it.next();
			ADMADSTMP tmp = new ADMADSTMP();

			tmp.setID(o[0].toString());
            tmp.setTITLE(o[1].toString());
            tmp.setSTEPNAME(o[2].toString());
            tmp.setSTARTDATE(o[3].toString());
            tmp.setENDDATE(o[4].toString());
            tmp.setSTARTTIME(o[5].toString());
            tmp.setENDTIME(o[6].toString());
            tmp.setOID(o[7]==null ? "" : o[7].toString());
            tmp.setEDITORUNAME(o[8]==null ? "" : o[8].toString());
			queryResults.add(tmp);
        }
		return  queryResults;
    }
}
