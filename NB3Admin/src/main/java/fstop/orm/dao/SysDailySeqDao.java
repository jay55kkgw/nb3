package fstop.orm.dao;

import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.SYSDAILYSEQ;

import javax.persistence.Query;


/**
 * <p>SysDailySeqDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class SysDailySeqDao  extends LegacyJpaRepository<SYSDAILYSEQ, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	private Object mutex = new Object();
	
	/**
	 * <p>dailySeq.</p>
	 *
	 * @param appid a {@link java.lang.String} object.
	 * @return a {@link java.lang.Long} object.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Long dailySeq(String appid) {
		synchronized (mutex) {

			Query query = getSession().createSQLQuery("UPDATE SYSDAILYSEQ SET ADSEQ=ADSEQ WHERE ADSEQID = ? ");
			query.setParameter(0, appid);
			int r = query.executeUpdate();
			log.info("appid={}, r={}", appid, r);
			if(r == 0) {
				SYSDAILYSEQ seq = new SYSDAILYSEQ();
				seq.setADSEQID(appid);
				seq.setADSEQMEMO("");
				seq.setADSEQ(1L);
				save(seq);
				return 1L;
			}
			Query add1 = getSession().createSQLQuery("UPDATE SYSDAILYSEQ SET ADSEQ=ADSEQ+1 WHERE ADSEQID = ? ");
			add1.setParameter(0, appid);
			int r2 = add1.executeUpdate();

			Query query2 = getSession().createSQLQuery("SELECT ADSEQ FROM SYSDAILYSEQ WHERE ADSEQID = ? ");
			query2.setParameter(0, appid);
			List obj = query2.getResultList();
			
			return ((Integer)obj.get(0)).longValue();
		}
	}
	
}
