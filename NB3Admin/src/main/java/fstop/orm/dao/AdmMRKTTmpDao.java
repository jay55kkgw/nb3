package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.rest.web.back.controller.B504Controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMADSTMP;
import fstop.orm.po.ADMMRKTTMP;
import fstop.orm.po.ADMSTORE;
import fstop.orm.po.ADMSTORETMP;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>行銷(行動廣告)暫存檔 DAO</p>
 *
 * @author 
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmMRKTTmpDao extends LegacyJpaRepository<ADMMRKTTMP, String> {
    
	// 超級使用者
	private final String SUPER_ROLES = "DC";

	public List<ADMMRKTTMP> findByParams(String oid, String SFromDate, String SToDate, Set<String> roles, String flowFinished, String ADTYPE, String TITLE) {
        // 只查需要的欄位
        String sql = "SELECT a.MTID, a.MTADTYPE, a.MTADHLK, a.MTSDATE, a.MTEDATE, a.MTSTIME, a.MTETIME, a.OID, a.STEPNAME, a.FLOWFINISHED, b.EDITORUNAME FROM ADMMRKTTMP a ";
        sql += "LEFT JOIN ADMFLOWCURRENT b ON a.MTID=b.CASESN  WHERE 1=1 ";
        
        List<Object> params = new ArrayList<Object>();
        
        if (!roles.contains(SUPER_ROLES)){
            sql += " AND OID=? ";
            params.add(oid);
        }

        if(!SFromDate.trim().isEmpty() && !SToDate.trim().isEmpty() ) {
            sql += " AND (((MTSDATE>=?) AND (MTSDATE<=?)) ";
            params.add(SFromDate);
            params.add(SToDate);
            sql += " OR ((MTEDATE>=?) AND (MTEDATE<=?))) ";
            params.add(SFromDate);
            params.add(SToDate);
        }
        
      if(flowFinished != null && !flowFinished.trim().isEmpty() ) {
			sql += " AND FLOWFINISHED=? ";
          params.add(flowFinished);
      }

      if(ADTYPE != null && !ADTYPE.trim().isEmpty() ) {
			sql += " AND a.MTADTYPE=? ";
          params.add(ADTYPE);
      }

      if(TITLE != null && !TITLE.trim().isEmpty() ) {
			sql += " AND a.MTADHLK LIKE ? ";
          params.add( "%"+ TITLE + "%");
      }


        Query query = getSession().createSQLQuery(sql);
        for ( int i=0; i<params.size(); i++ ) {
            query.setParameter(i, params.get(i));
        }
 
        List<ADMMRKTTMP> queryResults = new ArrayList<ADMMRKTTMP>();
        List qresult = query.getResultList();
        Iterator it = qresult.iterator();
        log.info("qresult count = {}",qresult.size());
		while (it.hasNext()) {
            Object[] o = (Object[]) it.next();
            ADMMRKTTMP tmp = new ADMMRKTTMP();

			tmp.setMTID(o[0].toString());
            tmp.setMTADTYPE(o[1].toString());
            tmp.setMTADHLK(o[2].toString());
            tmp.setMTSDATE(o[3].toString());
            tmp.setMTEDATE(o[4].toString());
            tmp.setMTSTIME(o[5].toString());
            tmp.setMTETIME(o[6].toString());
            tmp.setOID(o[7]==null ? "" : o[7].toString());
            tmp.setSTEPNAME(o[8].toString());
            tmp.setFLOWFINISHED(o[9].toString());
            tmp.setEDITORUNAME(o[10]==null ? "" : o[10].toString());
			queryResults.add(tmp);
        }
		return  queryResults;
    }
}
