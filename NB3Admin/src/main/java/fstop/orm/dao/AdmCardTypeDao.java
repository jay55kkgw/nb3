package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import fstop.orm.po.ADMCARDTYPE;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>AdmCardTypeDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmCardTypeDao extends LegacyJpaRepository<ADMCARDTYPE, String> {
	/**
	 * <p>findPageDataByLike.</p>
	 *
	 * @param pageNo a int.
	 * @param pageSize a int.
	 * @param orderBy a {@link java.lang.String} object.
	 * @param orderDir a {@link java.lang.String} object.
	 * @param ADTYPEID a {@link java.lang.String} object.
	 * @param ADTYPENAME a {@link java.lang.String} object.
	 * @param ADTYPECHSNAME a {@link java.lang.String} object.
	 * @param ADTYPEENGNAME a {@link java.lang.String} object.
	 * @param ADTYPEDESC a {@link java.lang.String} object.
	 * @return a {@link com.netbank.domain.orm.core.Page} object.
	 */
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ADTYPEID, String ADTYPENAME, String ADTYPECHSNAME, String ADTYPEENGNAME, String ADTYPEDESC) {
		String hql = "FROM ADMCARDTYPE WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if(ADTYPEID != null && !ADTYPEID.trim().isEmpty()) {
			hql += "AND ADTYPEID LIKE ? ";
			params.add("%"+ADTYPEID+"%");
		}
		if(ADTYPENAME != null && !ADTYPENAME.trim().isEmpty()) {
			hql += "AND ADTYPENAME LIKE ? ";
			params.add("%"+ADTYPENAME+"%");
		}
		if(ADTYPECHSNAME != null && !ADTYPECHSNAME.trim().isEmpty()) {
			hql += "AND ADTYPECHSNAME LIKE ? ";
			params.add("%"+ADTYPECHSNAME+"%");
		}
		if(ADTYPEENGNAME != null && !ADTYPEENGNAME.trim().isEmpty()) {
			hql += "AND ADTYPEENGNAME LIKE ? ";
			params.add("%"+ADTYPEENGNAME+"%");
		}
		if(ADTYPEDESC != null && !ADTYPEDESC.trim().isEmpty()) {
			hql += "AND ADTYPEDESC LIKE ? ";
			params.add("%"+ADTYPEDESC+"%");
		}
		hql += " ORDER BY "+ orderBy+" "+orderDir;

		log.trace(Sanitizer.logForgingStr(hql));

		switch ( params.size() ) {
			case 0:
				return this.pagedQuery(hql, pageNo, pageSize);

			case 1:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0));

			case 2:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));

			case 3:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2));
			
			case 4:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3));

			case 5:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3), params.get(4));

			default:
				return this.pagedQuery(hql, pageNo, pageSize);
		}
	}
}
