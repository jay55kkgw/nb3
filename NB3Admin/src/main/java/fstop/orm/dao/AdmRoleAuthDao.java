package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import fstop.orm.po.ADMROLEAUTH;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>AdmRoleAuthDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmRoleAuthDao extends LegacyJpaRepository<ADMROLEAUTH, Integer> {
	/**
	 * <p>findPageDataByLike.</p>
	 *
	 * @param pageNo a int.
	 * @param pageSize a int.
	 * @param orderBy a {@link java.lang.String} object.
	 * @param orderDir a {@link java.lang.String} object.
	 * @param ADROLENO a {@link java.lang.String} object.
	 * @param APOPID a {@link java.lang.String} object.
	 * @return a {@link com.netbank.domain.orm.core.Page} object.
	 */
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ADROLENO, String APOPID, String ADSTAFFNO) {
		String hql = "FROM  ADMROLEAUTH WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if(ADROLENO != null && !ADROLENO.trim().isEmpty()) {
			hql += "AND ADROLENO LIKE ? ";
			params.add("%"+ADROLENO+"%");
		}
		if(APOPID != null && !APOPID.trim().isEmpty()) {
			hql += "AND APOPID LIKE ? ";
			params.add("%"+APOPID+"%");
		}
		hql += "AND ADSTAFFNO = ? ";
		params.add(ADSTAFFNO);

		hql += " ORDER BY "+ orderBy+" "+orderDir;
        //20191115 steven Log Forging\路徑 7:
		log.trace(Sanitizer.logForgingStr(hql));

		return this.pagedQuery(hql, pageNo, pageSize, params);
	}

	public List<ADMROLEAUTH> findByADRoleNo(String adRoleNo) {
		String hql = "FROM  ADMROLEAUTH WHERE ADROLENO=? ";

		return this.find(hql, adRoleNo);
	}
	
	public List<ADMROLEAUTH> findByADRoleNoStaffNo(String adRoleNo, String adStaffNo) {
		String hql = "FROM  ADMROLEAUTH WHERE ADROLENO=? AND ADSTAFFNO=? ";

		return this.find(hql, adRoleNo, adStaffNo);
	}
	
	// /**
	//  * <p>queryAll.</p>
	//  *
	//  * @return a {@link java.util.List} object.
	//  */
	// public List queryAll() {
	// 	return find("FROM ADMROLEAUTH ORDER BY ADROLENO, ADSTAFFNO");
	// }

	// /**
	//  * <p>findRolenoStaffnoExists.</p>
	//  *
	//  * @param roleno a {@link java.lang.String} object.
	//  * @param staffno a {@link java.lang.String} object.
	//  * @return a boolean.
	//  */
	// public boolean findRolenoStaffnoExists(String roleno, String staffno) {
	// 	return ((Long)find("SELECT COUNT(*) FROM ADMROLEAUTH WHERE ADROLENO=? AND ADSTAFFNO=? ", roleno, staffno).iterator().next()) > 0;
	// }
	
	// /**
	//  * <p>findStaffnoExists.</p>
	//  *
	//  * @param roleno a {@link java.lang.String} object.
	//  * @return a boolean.
	//  */
	// public boolean findStaffnoExists(String roleno) {
	// 	return ((Long)find("SELECT COUNT(*) FROM ADMROLEAUTH WHERE ADROLENO=? AND ADSTAFFNO<>'' ", roleno).iterator().next()) > 0;
	// }
	
	// /**
	//  * <p>findRolenoAuth.</p>
	//  *
	//  * @param roleno a {@link java.lang.String} object.
	//  * @return a {@link java.lang.String} object.
	//  */
	// public String findRolenoAuth(String roleno) {
	// 	String result = "";
	// 	List qresult = find("SELECT ADAUTH FROM ADMROLEAUTH WHERE ADROLENO=? AND ADSTAFFNO='' ", roleno);
		
	// 	if(qresult.size() > 0)
	// 		result = (String)qresult.iterator().next();
		
	// 	return result;
	// }
	
	// /**
	//  * <p>findRolenoAuth.</p>
	//  *
	//  * @param roleno a {@link java.lang.String} object.
	//  * @param staffno a {@link java.lang.String} object.
	//  * @return a {@link java.lang.String} object.
	//  */
	// public String findRolenoAuth(String roleno, String staffno) {
	// 	String result = "";
	// 	List qresult = find("SELECT ADAUTH FROM ADMROLEAUTH WHERE ADROLENO=? AND ADSTAFFNO=?  ", roleno, staffno);
		
	// 	if(qresult.size() > 0)
	// 		result = (String)qresult.iterator().next();
		
	// 	return result;
	// }
}
