package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.MB3SYSOP;
import fstop.orm.po.NB3SYSOP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * NB3SysOpDao class.
 * </p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class MB3SysOpDao extends LegacyJpaRepository<MB3SYSOP, Integer> {

    /**
     * <p>
     * getAll.
     * </p>
     *
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings({ "unchecked" })
    public List<MB3SYSOP> getAll() {
        List<MB3SYSOP> result = new ArrayList<MB3SYSOP>();

        String hql = "FROM MB3SYSOP ORDER BY ADSEQ";
        log.trace(Sanitizer.logForgingStr(hql));

        result = find(hql);
        return result;
    }

    /**
     * <p>
     * getByADOPID.
     * </p>
     *
     * @param ADOPID a {@link java.lang.String} object.
     * @return a {@link fstop.orm.po.NB3SYSOP} object.
     */
    @SuppressWarnings({ "unchecked" })
    public MB3SYSOP getByADOPID(String ADOPID) {
        List<MB3SYSOP> result = new ArrayList<MB3SYSOP>();

        String hql = "FROM MB3SYSOP WHERE ADOPID=? ";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[1];
        params[0] = ADOPID;

        result = find(hql, params);
        if (result.size() > 0)
            return result.get(0);
        else
            return null;
    }

    /**
     * <p>
     * getByADOPNAME.
     * </p>
     *
     * @param ADOPID a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @SuppressWarnings({ "unchecked" })
    public String getByADOPNAME(String ADOPID) {
        List<MB3SYSOP> result = new ArrayList<MB3SYSOP>();
        String hql = "FROM MB3SYSOP WHERE ADOPID=?";

        Object[] params = new Object[1];
        params[0] = ADOPID;

        result = find(hql, params);
        if (result.size() > 0)
            return result.get(0).getADOPNAME();
        else
            return "";
    }

    /**
     * <p>
     * getByADOPGROUPID.
     * </p>
     *
     * @param ADOPGROUPID a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings({ "unchecked" })
    public List<MB3SYSOP> getByADOPGROUPID(String ADOPGROUPID) {
        List<MB3SYSOP> result = new ArrayList<MB3SYSOP>();

        String hql = "FROM MB3SYSOP WHERE ADOPGROUPID=? ORDER BY ADSEQ ASC";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[1];
        params[0] = ADOPGROUPID;

        result = find(hql, params);
        return result;
    }

    /**
     * 依父選單ID，取得子選單中最大的選單序號
     *
     * @param ADOPGROUPID a {@link java.lang.String} object.
     * @return a int.
     */
    @SuppressWarnings({ "unchecked" })
    public int getMaxADSEQ(String ADOPGROUPID) {
        List<MB3SYSOP> result = new ArrayList<MB3SYSOP>();

        String hql = "FROM MB3SYSOP WHERE ADOPGROUPID=? ORDER BY ADSEQ DESC  ";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[1];
        params[0] = ADOPGROUPID;

        result = find(hql, params);
        if (result.size() > 0)
            return result.get(0).getADSEQ();
        else
            return 0;
    }

    /**
     * <p>
     * updateADSEQ.
     * </p>
     *
     * @param DPACCSETID a {@link java.lang.Integer} object.
     * @param ADSEQ      a {@link java.lang.String} object.
     */
    public void updateADSEQ(Integer DPACCSETID, String ADSEQ) {
        String hql = "update MB3SYSOP set ADSEQ=:ADSEQ where DPACCSETID=:DPACCSETID";
        Query query = this.getSession().createQuery(hql);
        query.setParameter("ADSEQ", ADSEQ);
        query.setParameter("DPACCSETID", DPACCSETID);

        int rowAffected = query.executeUpdate();
    }
}
