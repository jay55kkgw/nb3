package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import fstop.orm.po.ADMLOGINACLIP;

import java.text.ParseException;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


/**
 * <p>AdmLoginAclIPDao class.</p>
 *
 * @author jinhanhuang
 * @version V1.0
 */
@Repository
@Transactional
public class AdmLoginAclIPDao extends LegacyJpaRepository<ADMLOGINACLIP, String> {
	
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir) throws ParseException {
        StringBuffer hql = new StringBuffer();
        hql.append("FROM ADMLOGINACLIP ");      
        hql.append("ORDER BY " + orderBy + " " + orderDir);
       
        return this.pagedQueryContentHasHtml(hql.toString(), pageNo, pageSize);
    }

}
