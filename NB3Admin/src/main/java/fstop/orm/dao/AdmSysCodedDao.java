package fstop.orm.dao;

import java.util.List;

import javax.transaction.Transactional;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import org.springframework.stereotype.Repository;

import fstop.orm.po.ADMSYSCODED;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>AdmSysCodedDao class.</p>
 *
 * @author jinhanhuang
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmSysCodedDao extends LegacyJpaRepository<ADMSYSCODED, String> {
    /**
     * <p>findByIdLike.</p>
     *
     * @param id a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings("unchecked")
    public List<ADMSYSCODED> getCounty(String codekind) {
        log.trace("codekind={}", Sanitizer.logForgingStr(codekind));
        return find("FROM ADMSYSCODED WHERE 1=1 AND CODEKIND='A01'");
    }
}
