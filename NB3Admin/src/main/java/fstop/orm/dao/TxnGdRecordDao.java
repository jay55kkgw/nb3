package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNGDRECORD;
import fstop.util.Sanitizer;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Repository
@Transactional
@Slf4j
public class TxnGdRecordDao extends LegacyJpaRepository<TXNGDRECORD, String> {
	
	public Page findPageData(int pageNo, int pageSize, String orderBy, String orderDir, String startDate,
			String endDate, String userId) {

		List<Object> params = new ArrayList<Object>();
		String hql = "FROM TXNGDRECORD WHERE concat(trim(LASTDATE),LASTTIME) >= ? and concat(trim(LASTDATE),LASTTIME) <= ? AND GDTXSTATUS=? ";

		params.add(startDate);
		params.add(endDate);
		params.add("0");			// 只查成功部份

		if (userId != null && !userId.trim().isEmpty()) {
			hql += " AND GDUSERID = ? ";
			params.add(userId);
		}
		hql += " ORDER BY " + orderBy + " " + orderDir;		

		log.trace(Sanitizer.logForgingStr(hql));
		
		return this.pagedQuery(hql, pageNo, pageSize, params);
	}



	@SuppressWarnings("unchecked")
	public Page findByDateRange(String startdt, String enddt, String ADUSERID, int start, int limit) {
		List<String> condition = new ArrayList();
		List<String> params = new ArrayList();
		
		condition.add(" LASTDATE||LASTTIME >= ? and LASTDATE||LASTTIME <= ? ");
		
		params.add(startdt);
		params.add(enddt);
		
		if(StrUtils.isNotEmpty(ADUSERID)) {
			condition.add(" GDUSERID = ? ");
			params.add(ADUSERID);
		}
		
		String[] values = new String[params.size()];
		params.toArray(values);
		
		StringBuffer c = new StringBuffer();
		for(String s : condition) {
			if(c.length() > 0)
				c.append(" AND ");
			c.append(s);
		}
		
		return pagedQuery("FROM TXNGDRECORD " +
				  		"WHERE " + c.toString() + 
						" order by LASTDATE desc, LASTTIME desc, ADOPID asc", (start / limit) + 1, limit, values);
	}	

	// @Autowired
	// TxnReqInfoDao txnReqInfoDao;

	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public void writeTxnRecord(TXNREQINFO info ,TXNGDRECORD record) {
	// 	if(record != null)
	// 		logger.debug("WRITE NEW TXNGDRECORD ADTXNO:" + record.getADTXNO());

	// 	BOLifeMoniter.setValue("__TXNRECORD", record);
	// 	if(info != null) {
	// 		BOLifeMoniter.setValue("__TITAINFO", info);
	// 		txnReqInfoDao.save(info);
	// 		record.setGDTITAINFO(info.getREQID());
	// 	}
	// 	save( record);
	// }

	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public void rewriteTxnRecord(TXNGDRECORD record) {
	// 	BOLifeMoniter.setValue("__TXNRECORD", record);
	// 	save( record);
	// }

}
