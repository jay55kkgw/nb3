package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import fstop.orm.po.ADMCOUNTRY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * 國別檔 DAO
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmCountryDao extends LegacyJpaRepository<ADMCOUNTRY, String> {
	protected Logger logger = Logger.getLogger(getClass());

	/**
	 * <p>findUniqueByCntrid.</p>
	 *
	 * @param cntrid a {@link java.lang.String} object.
	 * @return a {@link fstop.orm.po.ADMCOUNTRY} object.
	 */
	public ADMCOUNTRY findUniqueByCntrid(String cntrid) {
		try {
			return (ADMCOUNTRY)find("FROM ADMCOUNTRY WHERE ADCTRYCODE = ? ", cntrid)
						.get(0);
		}
		catch(IndexOutOfBoundsException e) {}
		
		return null;
	}	

	/**
	 * 分頁查詢
	 *
	 * @param pageNo 頁碼，從 1 編起
	 * @param pageSize 一頁幾筆
	 * @param orderBy 依什麼欄位排序
	 * @param orderDir 升冪/降冪
	 * @param ADCTRYCODE 國別代碼
	 * @param ADCTRYNAME 國別中文
	 * @param ADCTRYENGNAME 國別英文
	 * @return 分頁物件
	 */
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ADCTRYCODE, String ADCTRYNAME, String ADCTRYENGNAME, String ADCTRYCHSNAME) {
		String hql = "FROM ADMCOUNTRY WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if(ADCTRYCODE != null && !ADCTRYCODE.trim().isEmpty()) {
			hql += "AND ADCTRYCODE LIKE ? ";
			params.add("%"+ADCTRYCODE+"%");
		}
		if(ADCTRYNAME != null && !ADCTRYNAME.trim().isEmpty()) {
			hql += "AND ADCTRYNAME LIKE ? ";
			params.add("%"+ADCTRYNAME+"%");
		}
		if(ADCTRYENGNAME != null && !ADCTRYENGNAME.trim().isEmpty()) {
			hql += "AND ADCTRYENGNAME LIKE ? ";
			params.add("%"+ADCTRYENGNAME+"%");
		}
		if(ADCTRYCHSNAME != null && !ADCTRYCHSNAME.trim().isEmpty()) {
			hql += "AND ADCTRYCHSNAME LIKE ? ";
			params.add("%"+ADCTRYCHSNAME+"%");
		}
		hql += " ORDER BY "+ orderBy+" "+orderDir;

		log.trace(Sanitizer.logForgingStr(hql));

		switch ( params.size() ) {
			case 0:
				return this.pagedQuery(hql, pageNo, pageSize);

			case 1:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0));

			case 2:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));

			case 3:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2));

			case 4:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3));

			default:
				return this.pagedQuery(hql, pageNo, pageSize);
		}
	}
}
