package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMAPPBRANCH;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>AdmAppBranchDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmAppBranchDao extends LegacyJpaRepository<ADMAPPBRANCH, Integer> {
	protected Logger logger = Logger.getLogger(getClass());
	protected String _BHTYPE;

	/**
	 * <p>findPageDataByLike.</p>
	 *
	 * @param pageNo a int.
	 * @param pageSize a int.
	 * @param orderBy a {@link java.lang.String} object.
	 * @param orderDir a {@link java.lang.String} object.
	 * @param BHADTYPE a {@link java.lang.String} object.
	 * @param BHNAME a {@link java.lang.String} object.
	 * @param BHCOUNTY a {@link java.lang.String} object.
	 * @return a {@link com.netbank.domain.orm.core.Page} object.
	 */
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String BHADTYPE,
			String BHNAME, String BHCOUNTY) {
		String hql = "FROM ADMAPPBRANCH WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if (BHADTYPE != null && !BHADTYPE.trim().isEmpty()) {
			hql += "AND BHADTYPE LIKE ? ";
			params.add("%" + BHADTYPE + "%");
		}
		if (BHNAME != null && !BHNAME.trim().isEmpty()) {
			hql += "AND BHNAME LIKE ? ";
			params.add("%" + BHNAME + "%");
		}
		if (BHCOUNTY != null && !BHCOUNTY.trim().isEmpty()) {
			hql += "AND BHCOUNTY=? ";
			params.add(BHCOUNTY);
		}
		hql += " ORDER BY " + orderBy + " " + orderDir;

		log.trace(Sanitizer.logForgingStr(hql));

		switch (params.size()) {
		case 0:
			return this.pagedQuery(hql, pageNo, pageSize);

		case 1:
			return this.pagedQuery(hql, pageNo, pageSize, params.get(0));

		case 2:
			return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));

		case 3:
			return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2));

		default:
			return this.pagedQuery(hql, pageNo, pageSize);
		}
	}

	// 設定行銷類別 { 1:分行 2:ATM 3:證券 4:海外分行 }
	/**
	 * <p>setBHTYPE.</p>
	 *
	 * @param BHType a {@link java.lang.String} object.
	 */
	public void setBHTYPE(String BHType) {
		_BHTYPE = BHType;
	}

	// @Transactional(propagation=Propagation.NOT_SUPPORTED)
	// public ADMAPPBRANCH getByACN(String acn) {
	// return get(acn.substring(0, 3));
	// }

	/**
	 * <p>getAllValue.</p>
	 *
	 * @param adlotype a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 */
	// @SuppressWarnings("unchecked")
	// public Rows getAllValuexx(String adlotype) {

	// 	Rows result = new Rows();
	// 	StringBuffer sql = new StringBuffer();

	// 	sql.append(" SELECT * FROM ADMAPPBRANCH WHERE BHADTYPE ='" + adlotype + "' ");
	// 	sql.append(" ORDER BY BHID ");
	// 	sql.append(" WITH UR ");

	// 	try {
	// 		Query query = getSession().createSQLQuery(sql.toString());

	// 		query.unwrap(NativeQuery.class).addScalar("BHID", IntegerType.INSTANCE)
	// 				.addScalar("BHUSERID", StringType.INSTANCE).addScalar("BHUSERNAME", StringType.INSTANCE)
	// 				.addScalar("BHTXDATE", StringType.INSTANCE).addScalar("BHTXTIME", StringType.INSTANCE)
	// 				.addScalar("BHADTYPE", StringType.INSTANCE).addScalar("BHNAME", StringType.INSTANCE)
	// 				.addScalar("BHCOUNTY", StringType.INSTANCE).addScalar("BHREGION", StringType.INSTANCE)
	// 				.addScalar("BHADDR", StringType.INSTANCE).addScalar("BHLATITUDE", StringType.INSTANCE)
	// 				.addScalar("BHLONGITUDE", StringType.INSTANCE).addScalar("BHTELCOUNTRY", StringType.INSTANCE)
	// 				.addScalar("BHTELREGION", StringType.INSTANCE).addScalar("BHTEL", StringType.INSTANCE)
	// 				.addScalar("BHPICADD", StringType.INSTANCE).addScalar("BHPICDATA", ClobType.INSTANCE)
	// 				.addScalar("BHSTIME", StringType.INSTANCE).addScalar("BHETIME", StringType.INSTANCE);

	// 		List qresult = query.getResultList();
	// 		Iterator it = qresult.iterator();
	// 		while (it.hasNext()) {
	// 			Object[] o = (Object[]) it.next();
	// 			Map rm = new HashMap();
	// 			int t = -1;
	// 			rm.put("BHID", o[++t].toString());
	// 			rm.put("BHUSERID", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHUSERNAME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTXDATE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTXTIME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHADTYPE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHNAME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHCOUNTY", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHREGION", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHADDR", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHLATITUDE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHLONGITUDE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTELCOUNTRY", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTELREGION", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTEL", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHPICADD", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHPICDATA", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHSTIME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHETIME", (o[++t] == null ? "" : o[t].toString()));

	// 			Row r = new Row(rm);
	// 			result.addRow(r);
	// 		}

	// 		return result;
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		throw (TopMessageException.create("ZX99"));
	// 	}

	// }

	// public Rows findAll() -> 請改用 findAllRows
	/**
	 * <p>findAllRows.</p>
	 *
	 * @return a {@link fstop.model.Rows} object.
	 */
	// public Rows findAllRows() {
	// 	Rows result = new Rows();
	// 	StringBuffer sql = new StringBuffer();

	// 	sql.append(" SELECT * FROM ADMAPPBRANCH a LEFT JOIN ADMSYSCODED d ");
	// 	sql.append(" ON a.BHCOUNTY = d.CODEID ");
	// 	sql.append(" WHERE d.CODEKIND='A01' ");
	// 	sql.append(" ORDER BY BHID ");
	// 	sql.append(" WITH UR ");

	// 	try {
	// 		Query query = getSession().createSQLQuery(sql.toString());

	// 		query.unwrap(NativeQuery.class).addScalar("BHID", IntegerType.INSTANCE)
	// 				.addScalar("BHUSERID", StringType.INSTANCE).addScalar("BHUSERNAME", StringType.INSTANCE)
	// 				.addScalar("BHTXDATE", StringType.INSTANCE).addScalar("BHTXTIME", StringType.INSTANCE)
	// 				.addScalar("BHADTYPE", StringType.INSTANCE).addScalar("BHNAME", StringType.INSTANCE)
	// 				.addScalar("BHCOUNTY", StringType.INSTANCE).addScalar("BHREGION", StringType.INSTANCE)
	// 				.addScalar("BHADDR", StringType.INSTANCE).addScalar("BHLATITUDE", StringType.INSTANCE)
	// 				.addScalar("BHLONGITUDE", StringType.INSTANCE).addScalar("BHTELCOUNTRY", StringType.INSTANCE)
	// 				.addScalar("BHTELREGION", StringType.INSTANCE).addScalar("BHTEL", StringType.INSTANCE)
	// 				.addScalar("BHPICADD", StringType.INSTANCE).addScalar("BHPICDATA", ClobType.INSTANCE)
	// 				.addScalar("BHSTIME", StringType.INSTANCE).addScalar("BHETIME", StringType.INSTANCE)
	// 				.addScalar("CODEKIND", StringType.INSTANCE).addScalar("CODEID", StringType.INSTANCE)
	// 				.addScalar("CODEPARENT", StringType.INSTANCE).addScalar("CODENAME", StringType.INSTANCE)
	// 				.addScalar("CODESNAME", StringType.INSTANCE).addScalar("ISENABLE", StringType.INSTANCE)
	// 				.addScalar("SORTORDER", StringType.INSTANCE).addScalar("MEMO", StringType.INSTANCE);

	// 		List qresult = query.getResultList();
	// 		Iterator it = qresult.iterator();
	// 		while (it.hasNext()) {
	// 			Object[] o = (Object[]) it.next();
	// 			Map rm = new HashMap();
	// 			int t = -1;
	// 			rm.put("BHID", o[++t].toString());
	// 			rm.put("BHUSERID", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHUSERNAME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTXDATE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTXTIME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHADTYPE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHNAME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHCOUNTY", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHREGION", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHADDR", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHLATITUDE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHLONGITUDE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTELCOUNTRY", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTELREGION", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTEL", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHPICADD", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHPICDATA", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHSTIME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHETIME", (o[++t] == null ? "" : o[t].toString()));

	// 			rm.put("CODEKIND", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("CODEID", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("CODEPARENT", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("CODENAME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("CODESNAME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("ISENABLE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("SORTORDER", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("MEMO", (o[++t] == null ? "" : o[t].toString()));

	// 			Row r = new Row(rm);
	// 			result.addRow(r);
	// 		}

	// 		return result;
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		throw (TopMessageException.create("ZX99"));
	// 	}

	// }

	/*
	 * author: 1：分行 2：證券 3：ATM
	 */
	/**
	 * <p>findByAdloType.</p>
	 *
	 * @param ADLOTYPE a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 */
	// public Rows findByAdloType(String ADLOTYPE) {
	// 	Rows result = new Rows();
	// 	StringBuffer sql = new StringBuffer();

	// 	sql.append(
	// 			" SELECT a.*,d.CODENAME FROM ADMAPPBRANCH a LEFT JOIN (SELECT * FROM ADMSYSCODED WHERE CODEKIND='A01') d ");
	// 	sql.append(" ON a.BHCOUNTY = d.CODEID ");
	// 	sql.append(" WHERE 1=1 ");

	// 	// 1：分行
	// 	// 2：證券
	// 	// 3：ATM
	// 	if (!ADLOTYPE.equals("null") && !"".equals(ADLOTYPE)) {
	// 		sql.append(" AND a.BHADTYPE='" + ADLOTYPE + "' ");
	// 	}

	// 	sql.append(" ORDER BY BHID ");
	// 	sql.append(" WITH UR ");

	// 	try {

	// 		Query query = getSession().createSQLQuery(sql.toString());

	// 		query.unwrap(NativeQuery.class).addScalar("BHID", IntegerType.INSTANCE)
	// 				.addScalar("BHUSERID", StringType.INSTANCE).addScalar("BHUSERNAME", StringType.INSTANCE)
	// 				.addScalar("BHTXDATE", StringType.INSTANCE).addScalar("BHTXTIME", StringType.INSTANCE)
	// 				.addScalar("BHADTYPE", StringType.INSTANCE).addScalar("BHNAME", StringType.INSTANCE)
	// 				.addScalar("BHCOUNTY", StringType.INSTANCE).addScalar("BHREGION", StringType.INSTANCE)
	// 				.addScalar("BHADDR", StringType.INSTANCE).addScalar("BHLATITUDE", StringType.INSTANCE)
	// 				.addScalar("BHLONGITUDE", StringType.INSTANCE).addScalar("BHTELCOUNTRY", StringType.INSTANCE)
	// 				.addScalar("BHTELREGION", StringType.INSTANCE).addScalar("BHTEL", StringType.INSTANCE)
	// 				.addScalar("BHPICADD", StringType.INSTANCE).addScalar("BHPICDATA", ClobType.INSTANCE)
	// 				.addScalar("BHSTIME", StringType.INSTANCE).addScalar("BHETIME", StringType.INSTANCE)
	// 				.addScalar("CODENAME", StringType.INSTANCE);

	// 		List qresult = query.getResultList();
	// 		Iterator it = qresult.iterator();
	// 		while (it.hasNext()) {
	// 			Object[] o = (Object[]) it.next();
	// 			Map rm = new HashMap();
	// 			int t = -1;
	// 			rm.put("BHID", o[++t].toString());
	// 			rm.put("BHUSERID", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHUSERNAME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTXDATE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTXTIME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHADTYPE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHNAME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHCOUNTY", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHREGION", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHADDR", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHLATITUDE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHLONGITUDE", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTELCOUNTRY", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTELREGION", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHTEL", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHPICADD", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHPICDATA", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHSTIME", (o[++t] == null ? "" : o[t].toString()));
	// 			rm.put("BHETIME", (o[++t] == null ? "" : o[t].toString()));

	// 			rm.put("CODENAME", (o[++t] == null ? "" : o[t].toString()));

	// 			Row r = new Row(rm);
	// 			result.addRow(r);
	// 		}

	// 		return result;
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		throw (TopMessageException.create("ZX99"));
	// 	}

	// }

}
