package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import fstop.orm.po.ADMCURRENCY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;


/**
 * <p>AdmCurrencyDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmCurrencyDao extends LegacyJpaRepository<ADMCURRENCY, String> {
	/**
	 * 以查詢條件做分頁查詢
	 *
	 * @param pageNo 第幾頁
	 * @param pageSize 一頁幾筆
	 * @param orderBy 依什麼欄位排序
	 * @param orderDir 升冪/降冪
	 * @param ADCURRENCY 幣別代號
	 * @param ADCCYNO 幣別號碼
	 * @param ADCCYNAME 幣別中文名稱
	 * @return 分頁物件
	 */
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ADCURRENCY, String ADCCYNO, String ADCCYNAME, String ADCCYCHSNAME, String ADCCYENGNAME) {
		String hql = "FROM ADMCURRENCY WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if(ADCURRENCY != null && !ADCURRENCY.trim().isEmpty()) {
			hql += "AND ADCURRENCY LIKE ? ";
			params.add("%"+ADCURRENCY+"%");
		}
		if(ADCCYNO != null && !ADCCYNO.trim().isEmpty()) {
			hql += "AND ADCCYNO LIKE ? ";
			params.add("%"+ADCCYNO+"%");
		}
		if(ADCCYNAME != null && !ADCCYNAME.trim().isEmpty()) {
			hql += "AND ADCCYNAME LIKE ? ";
			params.add("%"+ADCCYNAME+"%");
		}
		if(ADCCYCHSNAME != null && !ADCCYCHSNAME.trim().isEmpty()) {
			hql += "AND ADCCYCHSNAME LIKE ? ";
			params.add("%"+ADCCYCHSNAME+"%");
		}
		if(ADCCYENGNAME != null && !ADCCYENGNAME.trim().isEmpty()) {
			hql += "AND ADCCYENGNAME LIKE ? ";
			params.add("%"+ADCCYENGNAME+"%");
		}
		hql += " ORDER BY "+ orderBy+" "+orderDir;

		log.trace(Sanitizer.logForgingStr(hql));

		switch ( params.size() ) {
			case 0:
				return this.pagedQuery(hql, pageNo, pageSize);

			case 1:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0));

			case 2:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));

			case 3:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2));

			case 4:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3));

			case 5:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3), params.get(4));

			default:
				return this.pagedQuery(hql, pageNo, pageSize);
		}
	}
	
	@SuppressWarnings({ "unchecked" })
	public List<ADMCURRENCY> findByNo(String ADCCYNO){
		String hql = "FROM ADMCURRENCY WHERE ADCCYNO = ?";
		List<Object> params =new ArrayList<Object>();
		params.add(ADCCYNO);
		Query query = getQuery(hql);
		for ( int i=0; i<params.size(); i++ ) {
			query.setParameter(i, params.get(i));
		}
		
		List qresult = query.getResultList();
		return qresult;
	}
}
