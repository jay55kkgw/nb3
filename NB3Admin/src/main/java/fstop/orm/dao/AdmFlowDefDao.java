package fstop.orm.dao;

import javax.transaction.Transactional;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import org.springframework.stereotype.Repository;
import fstop.orm.po.ADMFLOWDEF;

/**
 * <p>流程定義 DAO</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Repository
@Transactional
public class AdmFlowDefDao extends LegacyJpaRepository<ADMFLOWDEF, String> {
}
