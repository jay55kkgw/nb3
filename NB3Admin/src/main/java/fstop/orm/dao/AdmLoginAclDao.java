package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.springframework.stereotype.Repository;
import fstop.orm.po.ADMLOGINACL;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class AdmLoginAclDao extends LegacyJpaRepository<ADMLOGINACL, String>  {
    /**
     * 分頁查詢
     * @param pageNo    第幾頁
	 * @param pageSize  一頁幾筆
	 * @param orderBy   依什麼欄位排序
	 * @param orderDir  升冪/降冪
     * @param ADUSERID  使用者統編
     * @return
     */
    public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ADUSERID) {
        String hql = "FROM ADMLOGINACL WHERE 1=1 ";
        List<Object> params = new ArrayList<Object>();

        if(ADUSERID != null && !ADUSERID.trim().isEmpty()) {
            hql += "AND ADUSERID LIKE ? ";
            params.add("%"+ADUSERID+"%");
        }
            
        log.trace(Sanitizer.logForgingStr(hql));

        return this.pagedQuery(hql, pageNo, pageSize, params);
    }
}