package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMZIPDATA;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * <p>AdmZipDataDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmZipDataDao extends LegacyJpaRepository<ADMZIPDATA, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	/**
	 * <p>getAll.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<ADMZIPDATA> getAll() {
		return find("FROM ADMZIPDATA ORDER BY ZIPCODE");
	}
	
	/**
	 * 查詢City
	 *
	 * @return a {@link fstop.model.Rows} object.
	 */
	public Rows findCity() {
		String sql="SELECT CITY FROM ADMZIPDATA GROUP BY CITY";
		Query query = getSession().createSQLQuery(sql);
		Rows result = new Rows();
		List qresult = query.getResultList();
		
		Iterator it = qresult.iterator();
		while(it.hasNext()) {
			Map rm = new HashMap();
	        String element = it.next().toString();

			rm.put("CITY", element);			
			//System.out.println("AdmZipDataDao.java City:"+ element);
			Row r = new Row(rm);
			result.addRow(r);
		}
		//System.out.println("AdmZipDataDao.java findcity size:"+result.getSize());		
		return result;
	}	
	/**
	 * 查詢Area
	 *
	 * @param city a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 */
	// public Rows findArea(String city) {
		
	// 	String sql = "SELECT AREA FROM ADMZIPDATA WHERE CITY = ? GROUP BY AREA";
	// 	Query query = getSession().createSQLQuery(sql);
	// 	query.setParameter(0, city);
	// 	Rows result = new Rows();
	// 	List qresult = query.getResultList();
		
	// 	Iterator it = qresult.iterator();
	// 	while(it.hasNext()) {
	// 		Map rm = new HashMap();
	//         String element = it.next().toString();

	// 		rm.put("AREA", element);			
	// 		System.out.println("AdmZipDataDao.java area:"+ element);
	// 		Row r = new Row(rm);
	// 		result.addRow(r);
	// 	}
	// 	System.out.println("AdmZipDataDao.java findarea size:"+result.getSize());		
	// 	return result;
	// }	
	/**
	 * 查詢ZIPCODE By CITY AREA
	 *
	 * @param city a {@link java.lang.String} object.
	 * @param area a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	// public String findZipBycityarea(String city,String area) {
		
	// 	Query query=getSession().createSQLQuery("SELECT ZIPCODE FROM ADMZIPDATA WHERE CITY = ? AND AREA = ? GROUP BY ZIPCODE");
	// 	query.setParameter(0, city);
	// 	query.setParameter(1, area);
	// 	List qresult = query.getResultList();
	// 	return qresult.get(0).toString();
	// }	
}
