package fstop.orm.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;
import javax.transaction.Transactional;
import com.netbank.domain.orm.core.LegacyJpaRepository;

import org.springframework.stereotype.Repository;
import fstop.orm.po.ADMANNTMP;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>個人訊息公告內容刊登暫存檔 DAO</p>
 * 
 * @author 簡哥
 * @since 2019/8/16
 */
@Slf4j
@Repository
@Transactional
public class AdmAnnTmpDao extends LegacyJpaRepository<ADMANNTMP, String> {

    // 超級使用者
    private final String SUPER_ROLES = "DC";

    /**
     * 依時間查詢
     *
     * @param startDate 	開始時間
     * @param endDate   	結束時間
     * @param title		   	公告標題
     * @param flowFinished	是否已結案
     * @return          	ADMANNTMP LIST
     */
    public List<ADMANNTMP> findByParams(String oid, String startDate, String endDate, String title, Set<String> roles, String flowFinished, String ANNCHANNEL) {
        String hql = "SELECT a.ID,  a.TITLE, a.OID, a.ALLUSER, a.USERLIST, a.FILENAME, a.TYPE, a.URL, a.SORTORDER, a.STARTDATE, a.STARTTIME, a.ENDDATE, a.ENDTIME, a.STEPID, a.STEPNAME, a.FLOWFINISHED, a.LASTUSER, a.LASTDATE, a.LASTTIME, a.ANNCHANNEL, b.EDITOROID, b.EDITORUNAME FROM ADMANNTMP a ";
        hql += "LEFT JOIN ADMFLOWCURRENT b ON a.ID=b.CASESN ";
        hql += "WHERE (STARTDATE>=? And ENDDATE<=?) AND TITLE LIKE ? AND FLOWFINISHED = ?  ";
        
        List<Object> params = new ArrayList<Object>();
        params.add(startDate);
        params.add(endDate);
        params.add("%"+title+"%");
        params.add(flowFinished);
        
        if (!("A".equals(ANNCHANNEL))){
            hql += " AND (ANNCHANNEL = ? OR  ANNCHANNEL = 'A' )";
            params.add(ANNCHANNEL);
        }
        
        if (!roles.contains(SUPER_ROLES)){
            hql += " AND OID=? ";
            params.add(oid);
        }


        Query query = getSession().createSQLQuery(hql);
        for ( int i=0; i<params.size(); i++ ) {
            query.setParameter(i, params.get(i));
        }

        log.debug(hql);
 
        List<ADMANNTMP> queryResults = new ArrayList<ADMANNTMP>();
        List qresult = query.getResultList();
        Iterator it = qresult.iterator();
		while (it.hasNext()) {
            Object[] o = (Object[]) it.next();
			ADMANNTMP tmp = new ADMANNTMP();

            tmp.setID(o[0].toString());
            tmp.setTITLE(o[1].toString());
            tmp.setOID(o[2].toString());
            tmp.setALLUSER(o[3].toString());
            tmp.setUSERLIST(o[4].toString());
            tmp.setFILENAME(o[5].toString());
            tmp.setTYPE(o[6].toString());

            tmp.setURL(o[7] == null ? "" : o[7].toString());
            tmp.setSORTORDER(Integer.parseInt(o[8].toString()));
            tmp.setSTARTDATE(o[9].toString());
            tmp.setSTARTTIME(o[10].toString());
            tmp.setENDDATE(o[11].toString());
            tmp.setENDTIME(o[12].toString());
            tmp.setSTEPID(o[13].toString());
            tmp.setSTEPNAME(o[14].toString());
            tmp.setFLOWFINISHED(o[15].toString());
            tmp.setLASTUSER(o[16].toString());
            tmp.setLASTDATE(o[17].toString());
            tmp.setLASTTIME(o[18].toString());
            tmp.setANNCHANNEL(o[19] == null ? "A" :o[19].toString());
            tmp.setEDITOROID(o[20] == null ? "" : o[20].toString());
            tmp.setEDITORUNAME(o[21] == null ? "" : o[21].toString());
			queryResults.add(tmp);
        }
		return  queryResults;
        
        // String hql = "FROM ADMANNTMP WHERE OId=? AND (STARTDATE>=? And ENDDATE<=?) AND CONTENT LIKE ? ";
		// return find(hql, oid, startDate, endDate, "%"+content+"%");
    }
}