package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMADSTMP;
import fstop.orm.po.ADMSTORE;
import fstop.orm.po.ADMSTORETMP;


/**
 * <p>優惠特店檔 DAO</p>
 *
 * @author 
 * @version V1.0
 */
@Repository
@Transactional
public class AdmStoreTmpDao extends LegacyJpaRepository<ADMSTORETMP, String> {
    
	// 超級使用者
	private final String SUPER_ROLES = "DC";

	public List<ADMSTORETMP> findByParams(String oid, String SFromDate, String SToDate, Set<String> roles, String flowFinished) {
        // 只查需要的欄位
        String sql = "SELECT a.STID, a.ADTYPE, a.STTYPE, a.STTEL, a.SDATE, a.EDATE, a.STIME, a.ETIME, a.OID, a.STEPNAME, a.FLOWFINISHED, b.EDITORUNAME FROM ADMSTORETMP a ";
        sql += "LEFT JOIN ADMFLOWCURRENT b ON a.STID=b.CASESN  WHERE 1=1 And FLOWFINISHED=? ";
        
        List<Object> params = new ArrayList<Object>();
        
        params.add(flowFinished);
        
        if (!roles.contains(SUPER_ROLES)){
            sql += " AND OID=? ";
            params.add(oid);
        }

        if(!SFromDate.trim().isEmpty() && !SToDate.trim().isEmpty() ) {
            sql += " AND ((CONCAT(sdate,stime)>=?) AND (CONCAT(sdate,stime)<=?)) ";
            params.add(SFromDate);
            params.add(SToDate);
        }
//        if(startDate.trim().isEmpty() && !endDate.trim().isEmpty() ) {
//			sql += " AND SDATE<=? ";
//            params.add(endDate);
//        }
//        if(!startDate.trim().isEmpty() && endDate.trim().isEmpty() ) {
//			sql += " SDATE>=? ";
//            params.add(startDate);
//        }

        Query query = getSession().createSQLQuery(sql);
        for ( int i=0; i<params.size(); i++ ) {
            query.setParameter(i, params.get(i));
        }
 
        List<ADMSTORETMP> queryResults = new ArrayList<ADMSTORETMP>();
        List qresult = query.getResultList();
        Iterator it = qresult.iterator();
		while (it.hasNext()) {
            Object[] o = (Object[]) it.next();
            ADMSTORETMP tmp = new ADMSTORETMP();

			tmp.setSTID(o[0].toString());
            tmp.setADTYPE(o[1].toString());
            tmp.setSTTYPE(o[2].toString());
            tmp.setSTTEL(o[3].toString());
            tmp.setSDATE(o[4].toString());
            tmp.setEDATE(o[5].toString());
            tmp.setSTIME(o[6].toString());
            tmp.setETIME(o[7].toString());
            tmp.setOID(o[8]==null ? "" : o[8].toString());
            tmp.setSTEPNAME(o[9].toString());
            tmp.setFLOWFINISHED(o[10].toString());
            tmp.setEDITORUNAME(o[11]==null ? "" : o[11].toString());
			queryResults.add(tmp);
        }
		return  queryResults;
    }
}
