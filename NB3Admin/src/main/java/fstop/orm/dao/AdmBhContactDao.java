package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMBHCONTACT;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>AdmBhContactDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmBhContactDao extends LegacyJpaRepository<ADMBHCONTACT, Integer> {
	/**
	 * 分頁查詢
	 *
	 * @param pageNo 頁碼，從 1 編起
	 * @param pageSize 一頁幾筆
	 * @param orderBy 一頁幾筆
	 * @param orderDir 升冪/降冪
	 * @param ADBRANCHID 分行代碼
	 * @param ADBRANCHNAME 分行名稱
	 * @param ADCONTACTIID 分行聯絡人代號
	 * @param ADCONTACTNAME 分行聯絡人名稱
	 * @param ADCONTACTEMAIL 分行聯絡人電郵
	 * @param ADCONTACTTEL 分行聯絡人電話
	 * @return 分頁物件
	 */
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ADBRANCHID, String ADBRANCHNAME,String ADCONTACTIID,String ADCONTACTNAME,String ADCONTACTEMAIL,String ADCONTACTTEL) {
		String hql = "FROM  ADMBHCONTACT WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if(ADBRANCHID != null && !ADBRANCHID.trim().isEmpty()) {
			hql += "AND ADBRANCHID LIKE ? ";
			params.add("%"+ADBRANCHID+"%");
		}
		if(ADBRANCHNAME != null && !ADBRANCHNAME.trim().isEmpty()) {
			hql += "AND ADBRANCHNAME LIKE ? ";
			params.add("%"+ADBRANCHNAME+"%");
		}
		if(ADCONTACTIID != null && !ADCONTACTIID.trim().isEmpty()) {
			hql += "AND ADCONTACTIID LIKE ? ";
			params.add("%"+ADCONTACTIID+"%");
		}
		if(ADCONTACTNAME != null && !ADCONTACTNAME.trim().isEmpty()) {
			hql += "AND ADCONTACTNAME LIKE ? ";
			params.add("%"+ADCONTACTNAME+"%");
		}
		if(ADCONTACTEMAIL != null && !ADCONTACTEMAIL.trim().isEmpty()) {
			hql += "AND ADCONTACTEMAIL LIKE ? ";
			params.add("%"+ADCONTACTEMAIL+"%");
		}
		if(ADCONTACTTEL != null && !ADCONTACTTEL.trim().isEmpty()) {
			hql += "AND ADCONTACTTEL LIKE ? ";
			params.add("%"+ADCONTACTTEL+"%");
		}
		hql += " ORDER BY "+ orderBy+" "+orderDir;

		log.trace(Sanitizer.logForgingStr(hql));

		switch ( params.size() ) {
			case 0:
				return this.pagedQuery(hql, pageNo, pageSize);

			case 1:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0));

			case 2:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));

			case 3:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2));
				
			case 4:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3));

			case 5:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3), params.get(4));

			case 6:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3), params.get(4), params.get(5));
			
			default:
				return this.pagedQuery(hql, pageNo, pageSize);
		}
	}
}



