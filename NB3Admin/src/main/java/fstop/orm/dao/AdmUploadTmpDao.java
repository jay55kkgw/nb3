package fstop.orm.dao;

import java.util.List;

import javax.transaction.Transactional;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import org.springframework.stereotype.Repository;
import fstop.orm.po.ADMUPLOADTMP;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>AdmUploadTmpDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmUploadTmpDao extends LegacyJpaRepository<ADMUPLOADTMP, String> {
    /**
     * <p>findByIdLike.</p>
     *
     * @param id a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings("unchecked")
    public List<ADMUPLOADTMP> findByIdLike(String id) {
        log.trace("id={}", Sanitizer.logForgingStr(id));
        return find("FROM ADMUPLOADTMP WHERE ID LIKE ?", id+"%");
    }
}
