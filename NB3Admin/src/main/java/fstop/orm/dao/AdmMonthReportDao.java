package fstop.orm.dao;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.Query;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.rest.web.back.model.B706Result;
import com.netbank.rest.web.back.model.B706ResultOP;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMMONTHREPORT;
import fstop.util.PropertyUtils;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

/**
 * <p>
 * AdmMonthReportDao class.
 * </p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmMonthReportDao extends LegacyJpaRepository<ADMMONTHREPORT, Long> {
	protected Logger logger = Logger.getLogger(getClass());

	/**
	 * 2019/06/14 DannyChou ADD B704(四) 一般網路銀行約定/非約定統計表 使用 2019/12/20 DannyChou EDIT
	 * 增加 AND ADOPGROUP in ('GPA10','GPA30') 只顯示台幣及外幣交易
	 *
	 * @param startYYMM a {@link java.lang.String} object.
	 * @param endYYMM   a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 */
	public Rows findTransactionReportAgreeNew(String startYYMM, String endYYMM) {

		String sql = "";
		try {
			PropertyUtils utility = new PropertyUtils();
			sql = utility.getResourceAsString("reports/B704.sql");
		} catch (Exception e) {
			logger.error("getResourceAsString reports/B704.sql 發生錯誤解!!");
		}

		Query query = getSession().createSQLQuery(sql);
		query.setParameter("SDate", startYYMM);
		query.setParameter("EDate", endYYMM);

		Rows result = new Rows();
		List qresult = query.getResultList();
		DecimalFormat df = new DecimalFormat("##0");
		DecimalFormat df2 = new DecimalFormat("##0.00");
		Iterator it = qresult.iterator();

		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();

			Map rm = new HashMap();

			rm.put("ADOPID", o[0].toString()); // ZZZZZZ =>合計
			rm.put("ADOPNAME", o[1].toString()); // ZZZZZZ =>合計
			rm.put("ADAGREEF", o[2].toString()); // 0. 非約定1. 約定,-1 合計
			rm.put("QTYTYPE", o[3].toString()); // 0 筆數,1金額

			// 第一碼：C企業,P個人
			// 第二碼：交易密碼(0.交易密碼、1.IKey電子簽章、2.晶片金融卡(IC卡)
			// 第三碼：3.動態密碼(OTP)),成功(0.成功1.失敗)
			if (o[3].toString().equals("0")) {
				rm.put("C00COUNT", df.format(o[4]));
				rm.put("C01COUNT", df.format(o[5]));
				rm.put("C10COUNT", df.format(o[6]));
				rm.put("C11COUNT", df.format(o[7]));
				rm.put("C20COUNT", df.format(o[8]));
				rm.put("C21COUNT", df.format(o[9]));
				rm.put("P00COUNT", df.format(o[10]));
				rm.put("P01COUNT", df.format(o[11]));
				rm.put("P10COUNT", df.format(o[12]));
				rm.put("P11COUNT", df.format(o[13]));
				rm.put("P20COUNT", df.format(o[14]));
				rm.put("P21COUNT", df.format(o[15]));
				// 成功合計
				rm.put("T99", df.format(o[16]));
				// 失敗合計
				rm.put("F99", df.format(o[17]));

				Double denominator = (Double.parseDouble(o[16].toString()) + Double.parseDouble(o[17].toString()));
				if (denominator == 0D) {
					rm.put("TF", df2.format(0D));
				} else {
					rm.put("TF", df2.format(Double.parseDouble(o[17].toString()) / denominator * 100));
				}
			} else {
				rm.put("C00COUNT", df.format(Double.parseDouble(o[4].toString()) / 1000));
				rm.put("C01COUNT", df.format(Double.parseDouble(o[5].toString()) / 1000));
				rm.put("C10COUNT", df.format(Double.parseDouble(o[6].toString()) / 1000));
				rm.put("C11COUNT", df.format(Double.parseDouble(o[7].toString()) / 1000));
				rm.put("C20COUNT", df.format(Double.parseDouble(o[8].toString()) / 1000));
				rm.put("C21COUNT", df.format(Double.parseDouble(o[9].toString()) / 1000));
				rm.put("P00COUNT", df.format(Double.parseDouble(o[10].toString()) / 1000));
				rm.put("P01COUNT", df.format(Double.parseDouble(o[11].toString()) / 1000));
				rm.put("P10COUNT", df.format(Double.parseDouble(o[12].toString()) / 1000));
				rm.put("P11COUNT", df.format(Double.parseDouble(o[13].toString()) / 1000));
				rm.put("P20COUNT", df.format(Double.parseDouble(o[14].toString()) / 1000));
				rm.put("P21COUNT", df.format(Double.parseDouble(o[15].toString()) / 1000));
				// 成功合計
				rm.put("T99", df.format(Double.parseDouble(o[16].toString()) / 1000));
				// 失敗合計
				rm.put("F99", df.format(Double.parseDouble(o[17].toString()) / 1000));

				Double denominator = (Double.parseDouble(o[16].toString()) + Double.parseDouble(o[17].toString()));
				if (denominator == 0D) {
					rm.put("TF", df2.format(0D));
				} else {
					rm.put("TF", df2.format(Double.parseDouble(o[17].toString()) / denominator * 100));
				}
			}

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	/**
	 * 2019/06/17 DannyChou ADD B705(五) 一般網路銀行轉帳級距統計表 2019/12/20 DannyChou EDIT 增加
	 * AND ADOPGROUP in ('GPA10','GPA30') 只顯示台幣及外幣交易 2020/01/16 Chien 將 SQL 移到
	 * B705.sql, 方便 DEBUG SQL
	 *
	 * @param startYYMM a {@link java.lang.String} object.
	 * @param endYYMM   a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 */
	public Rows findTransactionReportLevelNew(String startYYMM, String endYYMM) {
		String sql = "";
		try {
			PropertyUtils utility = new PropertyUtils();
			sql = utility.getResourceAsString("reports/B705.sql");
		} catch (Exception e) {
			logger.error("getResourceAsString reports/B705.sql 發生錯誤解!!");
		}

		Query query = getSession().createSQLQuery(sql);
		query.setParameter("SDate", startYYMM);
		query.setParameter("EDate", startYYMM);

		logger.debug("\r\n[SQL:]\r\n" + sql);

		Rows result = new Rows();
		List qresult = query.getResultList();

		DecimalFormat df = new DecimalFormat("##0");
		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();

			rm.put("ADOPID", o[0].toString());
			rm.put("ADOPNAME", o[1].toString());
			rm.put("ADAGREEF", o[2].toString());
			rm.put("R00", df.format(o[3]));
			rm.put("R01", df.format(o[4]));
			rm.put("R02", df.format(o[5]));
			rm.put("R03", df.format(o[6]));
			rm.put("R04", df.format(o[7]));
			rm.put("R05", df.format(o[8]));
			rm.put("R06", df.format(o[9]));
			rm.put("R07", df.format(o[10]));
			rm.put("R08", df.format(o[11]));
			rm.put("R09", df.format(o[12]));
			rm.put("R10", df.format(o[13]));
			rm.put("R11", df.format(o[14]));
			rm.put("R12", df.format(o[15]));
			rm.put("R13", df.format(o[16]));

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	/**
	 * 2019/06/18 DannyChou ADD 一般網路銀行交易量統計表
	 *
	 * @param startYYMM a {@link java.lang.String} object.
	 * @param endYYMM   a {@link java.lang.String} object.
	 * @param logintype a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 * @throws FileNotFoundException
	 */
	public Rows findTransactionReportCountNew(String startYYMM, String endYYMM, String logintype) {

		// String condition = " AND CMYYYYMM>='" + startYYMM + "' AND CMYYYYMM <='" +
		// endYYMM + "' AND LOGINTYPE IN ("
		// + logintype + ")";

		// /*
		// * ADUSERTYPE：0. 個人戶 1. 企業戶 , -1 合計 ADBKFLAG：0. 自行1. 跨行 1 GPE 查詢服務 2 GPB 申請類 3
		// * GPC 掛失類 4 GPD 個人服務類 5 GPA 交易類 5 GPA 交易類 5 GPA 交易類
		// */
		// String sql = "SELECT "
		// + "CASE "
		// + " WHEN G.ADOPGROUP='GPE' THEN 1 "
		// + " WHEN G.ADOPGROUP='GPB' THEN 2 "
		// + " WHEN G.ADOPGROUP='GPC' THEN 3 "
		// + " WHEN G.ADOPGROUP='GPD' THEN 4 "
		// + " WHEN G.ADOPGROUP='GPA' THEN 5 "
		// + "END AS SeqNo "
		// + ",G.ADOPGROUP "
		// + ",G.ADOPGROUPNAME "
		// + ",0 AS ADUSERTYPE "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '0' THEN R.ADCOUNT "
		// + "ELSE 0 "
		// + "END),0) AS SelfCOUNT "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '1' THEN R.ADCOUNT "
		// + "ELSE 0 "
		// + "END),0) AS CrossCOUNT "
		// + ",value(SUM(R.ADCOUNT),0) AS ADCOUNT "
		// + " "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '0' THEN R.ADAMOUNT "
		// + "ELSE 0 "
		// + "END),0) AS SelfAMT "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '1' THEN R.ADAMOUNT "
		// + "ELSE 0 "
		// + "END),0) AS CrossAMT "
		// + ",value(SUM(R.ADAMOUNT),0) AS ADAMOUNT "
		// + "FROM (SELECT * FROM NB3SYSOPGROUP WHERE ADISPARENT='Y') G "
		// + "LEFT JOIN NB3SYSOP N ON N.ADGPPARENT = G.ADOPGROUP "
		// + "LEFT JOIN (SELECT * FROM ADMMONTHREPORT WHERE ADTXSTATUS='0' " + condition
		// + " ) R ON N.ADOPID = R.ADOPID "
		// + "WHERE G.ADOPGROUP<>'GPA' "
		// + "GROUP BY G.ADOPGROUP,G.ADOPGROUPNAME "
		// + " "
		// + "UNION ALL "
		// + "SELECT "
		// + "CASE "
		// + " WHEN G.ADOPGROUP='GPE' THEN 1 "
		// + " WHEN G.ADOPGROUP='GPB' THEN 2 "
		// + " WHEN G.ADOPGROUP='GPC' THEN 3 "
		// + " WHEN G.ADOPGROUP='GPD' THEN 4 "
		// + " WHEN G.ADOPGROUP='GPA' THEN 5 "
		// + "END AS SeqNo "
		// + ",G.ADOPGROUP "
		// + ",G.ADOPGROUPNAME "
		// + ",0 AS ADUSERTYPE "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '0' THEN R.ADCOUNT "
		// + "ELSE 0 "
		// + "END),0) AS SelfCOUNT "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '1' THEN R.ADCOUNT "
		// + "ELSE 0 "
		// + "END),0) AS CrossCOUNT "
		// + ",value(SUM(R.ADCOUNT),0) AS ADCOUNT "
		// + " "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '0' THEN R.ADAMOUNT "
		// + "ELSE 0 "
		// + "END),0) AS SelfAMT "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '1' THEN R.ADAMOUNT "
		// + "ELSE 0 "
		// + "END),0) AS CrossAMT "
		// + ",value(SUM(R.ADAMOUNT),0) AS ADAMOUNT "
		// + "FROM (SELECT * FROM NB3SYSOPGROUP WHERE ADISPARENT='Y') G "
		// + "LEFT JOIN NB3SYSOP N ON N.ADGPPARENT = G.ADOPGROUP "
		// + "LEFT JOIN (SELECT * FROM ADMMONTHREPORT WHERE ADTXSTATUS='0' AND
		// ADUSERTYPE='0' " + condition
		// + " ) R ON N.ADOPID = R.ADOPID "
		// + "WHERE G.ADOPGROUP='GPA' "
		// + "GROUP BY G.ADOPGROUP,G.ADOPGROUPNAME "
		// + " "
		// + "UNION ALL "
		// + "SELECT "
		// + "CASE "
		// + " WHEN G.ADOPGROUP='GPE' THEN 1 "
		// + " WHEN G.ADOPGROUP='GPB' THEN 2 "
		// + " WHEN G.ADOPGROUP='GPC' THEN 3 "
		// + " WHEN G.ADOPGROUP='GPD' THEN 4 "
		// + " WHEN G.ADOPGROUP='GPA' THEN 5 "
		// + "END AS SeqNo "
		// + ",G.ADOPGROUP "
		// + ",G.ADOPGROUPNAME "
		// + ",1 AS ADUSERTYPE "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '0' THEN R.ADCOUNT "
		// + "ELSE 0 "
		// + "END),0) AS SelfCOUNT "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '1' THEN R.ADCOUNT "
		// + "ELSE 0 "
		// + "END),0) AS CrossCOUNT "
		// + ",value(SUM(R.ADCOUNT),0) AS ADCOUNT "
		// + " "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '0' THEN R.ADAMOUNT "
		// + "ELSE 0 "
		// + "END),0) AS SelfAMT "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '1' THEN R.ADAMOUNT "
		// + "ELSE 0 "
		// + "END),0) AS CrossAMT "
		// + ",value(SUM(R.ADAMOUNT),0) AS ADAMOUNT "
		// + "FROM (SELECT * FROM NB3SYSOPGROUP WHERE ADISPARENT='Y') G "
		// + "LEFT JOIN NB3SYSOP N ON N.ADGPPARENT = G.ADOPGROUP "
		// + "LEFT JOIN (SELECT * FROM ADMMONTHREPORT WHERE ADTXSTATUS='0' AND
		// ADUSERTYPE='1' " + condition
		// + " ) R ON N.ADOPID = R.ADOPID "
		// + "WHERE G.ADOPGROUP='GPA' "
		// + "GROUP BY G.ADOPGROUP,G.ADOPGROUPNAME "
		// + " "
		// + "UNION ALL "
		// + "SELECT "
		// + "CASE "
		// + " WHEN G.ADOPGROUP='GPE' THEN 1 "
		// + " WHEN G.ADOPGROUP='GPB' THEN 2 "
		// + " WHEN G.ADOPGROUP='GPC' THEN 3 "
		// + " WHEN G.ADOPGROUP='GPD' THEN 4 "
		// + " WHEN G.ADOPGROUP='GPA' THEN 5 "
		// + "END AS SeqNo "
		// + ",G.ADOPGROUP "
		// + ",G.ADOPGROUPNAME "
		// + ",-1 AS ADUSERTYPE "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '0' THEN R.ADCOUNT "
		// + "ELSE 0 "
		// + "END),0) AS SelfCOUNT "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '1' THEN R.ADCOUNT "
		// + "ELSE 0 "
		// + "END),0) AS CrossCOUNT "
		// + ",value(SUM(R.ADCOUNT),0) AS ADCOUNT "
		// + " "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '0' THEN R.ADAMOUNT "
		// + "ELSE 0 "
		// + "END),0) AS SelfAMT "
		// + ",value(SUM(CASE "
		// + "WHEN R.ADBKFLAG = '1' THEN R.ADAMOUNT "
		// + "ELSE 0 "
		// + "END),0) AS CrossAMT "
		// + ",value(SUM(R.ADAMOUNT),0) AS ADAMOUNT "
		// + "FROM (SELECT * FROM NB3SYSOPGROUP WHERE ADISPARENT='Y') G "
		// + "LEFT JOIN NB3SYSOP N ON N.ADGPPARENT = G.ADOPGROUP "
		// + "LEFT JOIN (SELECT * FROM ADMMONTHREPORT WHERE ADTXSTATUS='0' " + condition
		// + " ) R ON N.ADOPID = R.ADOPID "
		// + "WHERE G.ADOPGROUP='GPA' "
		// + "GROUP BY G.ADOPGROUP,G.ADOPGROUPNAME "
		// + " "
		// + "ORDER BY SEQNO,ADUSERTYPE DESC ";

		String sql = "";
		List<String> whereLoginTypes = new ArrayList<String>(); // String[] loginTypes = logintype.replace("'",
																// "").split(","); //'','NB','MB'
		if (logintype.compareToIgnoreCase("") == 0) {
			whereLoginTypes.add("NB");
			whereLoginTypes.add("MB");
		} else if (logintype.compareToIgnoreCase("NB") == 0) {
			whereLoginTypes.add("NB");
			whereLoginTypes.add("NB");
		} else {
			whereLoginTypes.add("MB");
			whereLoginTypes.add("MB");
		}

		try {
			PropertyUtils utility = new PropertyUtils();
			sql = utility.getResourceAsString("reports/B702.sql");
		} catch (Exception e) {
			// TODO: handle exception
		}

		// (SELECT * FROM ADMMONTHREPORT WHERE ADTXSTATUS='0' AND ADUSERTYPE='0' AND
		// CMYYYYMM>=? AND CMYYYYMM <=? AND LOGINTYPE IN (?,?) )
		Query query = getSession().createSQLQuery(sql);
		for (int i = 0; i < 4; i++) {
			query.setParameter(i * 4, startYYMM);
			query.setParameter(i * 4 + 1, endYYMM);
			query.setParameter(i * 4 + 2, whereLoginTypes.get(0));
			query.setParameter(i * 4 + 3, whereLoginTypes.get(1));
		}

		logger.debug("\r\n[SQL:]\r\n" + sql);

		Rows result = new Rows();
		List qresult = query.getResultList();

		DecimalFormat df = new DecimalFormat("##0");
		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();

			rm.put("SEQNO", df.format(o[0]));
			rm.put("ADOPGROUP", o[1].toString());
			rm.put("ADOPGROUPNAME", o[2].toString());
			rm.put("ADUSERTYPE", df.format(o[3]));
			rm.put("SELFCOUNT", df.format(o[4]));
			rm.put("CROSSCOUNT", df.format(o[5]));
			rm.put("ADCOUNT", df.format(o[6]));
			rm.put("SELFAMT", df.format(Double.parseDouble(o[7].toString()) / 1000));
			rm.put("CROSSAMT", df.format(Double.parseDouble(o[8].toString()) / 1000));
			rm.put("ADAMOUNT", df.format(Double.parseDouble(o[9].toString()) / 1000));

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	/**
	 * 2019/06/18 DannyChou ADD 一般網路銀行交易量統計表(台幣帳務、外匯帳務)
	 *
	 * @param startYYMM a {@link java.lang.String} object.
	 * @param endYYMM   a {@link java.lang.String} object.
	 * @param adopGroup : 交易群組代碼(空白表示全部)
	 * @param logintype a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 */
	public Rows findTransactionReportCountB703GPA(String startYYMM, String endYYMM, String adopGroup,
			String logintype) {
		/*
		 * ADUSERTYPE：0. 個人戶 1. 企業戶 -1 小計 QTYTYPE ： 1筆數 2 金額
		 * 
		 * ADBKFLAG：0. 自行1. 跨行 ADTXCODE：0.交易密碼 1.IKey電子簽章 2.晶片金融卡(IC卡) 3.動態密碼(OTP)
		 * ADTXSTATUS：0. 成功1. 失敗
		 */
		String sql = "";
		try {
			PropertyUtils utility = new PropertyUtils();
			sql = utility.getResourceAsString("reports/B703GPA.sql");
		} catch (Exception e) {
			logger.error("getResourceAsString reports/B703GPA.sql 發生錯誤解!!");
		}

		String conditionLoginType = " LOGINTYPE IN (" + logintype + ")";
		String conditionGroup = " ADOPGROUP IN ('GPA10','GPA30') ";

		if (!adopGroup.isEmpty()) {
			conditionGroup = " ADOPGROUP IN ('GPA10','GPA30') AND ADOPGROUP = '" + adopGroup + "' ";
		}

		sql = sql.replaceAll("::WHERE::", conditionGroup).replaceAll("::LOGINTYPE::", conditionLoginType);

		Query query = getSession().createSQLQuery(sql);
		query.setParameter("StartDt", startYYMM);
		query.setParameter("EndDt", endYYMM);
		Rows result = new Rows();
		List qresult = query.getResultList();

		DecimalFormat df = new DecimalFormat("##0");
		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();

			rm.put("ADOPGROUP", o[0].toString());
			rm.put("ADOPGROUPNAME", o[1].toString());
			rm.put("ADUSERTYPE", o[2].toString());
			rm.put("QTYTYPE", o[3].toString());

			if (o[3].toString().equals("1")) {
				rm.put("Q00T", df.format(o[4]));
				rm.put("Q00F", df.format(o[5]));
				rm.put("Q01T", df.format(o[6]));
				rm.put("Q01F", df.format(o[7]));
				rm.put("Q02T", df.format(o[8]));
				rm.put("Q02F", df.format(o[9]));
				rm.put("Q03T", df.format(o[10]));
				rm.put("Q03F", df.format(o[11]));
				rm.put("Q10T", df.format(o[12]));
				rm.put("Q10F", df.format(o[13]));
				rm.put("Q11T", df.format(o[14]));
				rm.put("Q11F", df.format(o[15]));
				rm.put("Q12T", df.format(o[16]));
				rm.put("Q12F", df.format(o[17]));
				rm.put("Q13T", df.format(o[18]));
				rm.put("Q13F", df.format(o[19]));
				rm.put("Q99T", df.format(o[20]));
				rm.put("Q99F", df.format(o[21]));
			} else {
				rm.put("Q00T", df.format(Double.parseDouble(o[4].toString()) / 1000));
				rm.put("Q00F", df.format(Double.parseDouble(o[5].toString()) / 1000));
				rm.put("Q01T", df.format(Double.parseDouble(o[6].toString()) / 1000));
				rm.put("Q01F", df.format(Double.parseDouble(o[7].toString()) / 1000));
				rm.put("Q02T", df.format(Double.parseDouble(o[8].toString()) / 1000));
				rm.put("Q02F", df.format(Double.parseDouble(o[9].toString()) / 1000));
				rm.put("Q03T", df.format(Double.parseDouble(o[10].toString()) / 1000));
				rm.put("Q03F", df.format(Double.parseDouble(o[11].toString()) / 1000));
				rm.put("Q10T", df.format(Double.parseDouble(o[12].toString()) / 1000));
				rm.put("Q10F", df.format(Double.parseDouble(o[13].toString()) / 1000));
				rm.put("Q11T", df.format(Double.parseDouble(o[14].toString()) / 1000));
				rm.put("Q11F", df.format(Double.parseDouble(o[15].toString()) / 1000));
				rm.put("Q12T", df.format(Double.parseDouble(o[16].toString()) / 1000));
				rm.put("Q12F", df.format(Double.parseDouble(o[17].toString()) / 1000));
				rm.put("Q13T", df.format(Double.parseDouble(o[18].toString()) / 1000));
				rm.put("Q13F", df.format(Double.parseDouble(o[19].toString()) / 1000));
				rm.put("Q99T", df.format(Double.parseDouble(o[20].toString()) / 1000));
				rm.put("Q99F", df.format(Double.parseDouble(o[21].toString()) / 1000));
			}
			Double denominator = (Double.parseDouble(o[20].toString()) + Double.parseDouble(o[21].toString()));
			if (denominator == 0D) {
				rm.put("Q99TF", df.format(0D));
			} else {
				rm.put("Q99TF", df.format(Double.parseDouble(o[21].toString()) / denominator * 100));
			}

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	/**
	 * 2019/06/18 DannyChou ADD 一般網路銀行交易量統計表 GPA40 基金 GPA50 黃金 GPE 查詢服務 GPB 申請類 GPC
	 * 掛失類 GPD10 個人化設定 GPD20 變更資料 GPD30 通知服務
	 *
	 * @param startYYMM a {@link java.lang.String} object.
	 * @param endYYMM   a {@link java.lang.String} object.
	 * @param logintype a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 */
	public Rows findTransactionReportCountB703GPBE(String startYYMM, String endYYMM, String logintype) {

		/*
		 * N: ADOPID,ADOPNAME
		 * 
		 * QTYTYPE ： 1筆數 2 金額
		 * 
		 * ADBKFLAG：0. 自行1. 跨行 ADTXCODE：0.交易密碼 1.IKey電子簽章 2.晶片金融卡(IC卡) 3.動態密碼(OTP)
		 * ADTXSTATUS：0. 成功1. 失敗 ::WHERE:: ::LOGINTYPE::
		 */
		String sql = "";
		try {
			PropertyUtils utility = new PropertyUtils();
			sql = utility.getResourceAsString("reports/B703GPBE.sql");
		} catch (Exception e) {
			logger.error("getResourceAsString reports/B703GPBE.sql 發生錯誤解!!");
		}

		String conditionLoginType = " LOGINTYPE IN (" + logintype + ")";
		String conditionGroup = " 1=1 ";

		sql = sql.replaceAll("::WHERE::", conditionGroup).replaceAll("::LOGINTYPE::", conditionLoginType);
		Query query = getSession().createSQLQuery(sql);
		query.setParameter("SDate", startYYMM);
		query.setParameter("EDate", endYYMM);

		Rows result = new Rows();
		List qresult = query.getResultList();

		DecimalFormat df = new DecimalFormat("##0");
		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();

			rm.put("SEQNO", df.format(o[0]));
			rm.put("ADOPGROUP", o[1].toString());
			rm.put("ADOPGROUPNAME", o[2].toString());
			rm.put("QTYTYPE", o[3].toString());

			if (o[3].toString().equals("1")) {
				rm.put("Q0T", df.format(o[4]));
				rm.put("Q0F", df.format(o[5]));
				rm.put("Q1T", df.format(o[6]));
				rm.put("Q1F", df.format(o[7]));
				rm.put("Q2T", df.format(o[8]));
				rm.put("Q2F", df.format(o[9]));
				rm.put("Q3T", df.format(o[10]));
				rm.put("Q3F", df.format(o[11]));
				rm.put("Q9T", df.format(o[12]));
				rm.put("Q9F", df.format(o[13]));
			} else {
				rm.put("Q0T", df.format(Double.parseDouble(o[4].toString()) / 1000));
				rm.put("Q0F", df.format(Double.parseDouble(o[5].toString()) / 1000));
				rm.put("Q1T", df.format(Double.parseDouble(o[6].toString()) / 1000));
				rm.put("Q1F", df.format(Double.parseDouble(o[7].toString()) / 1000));
				rm.put("Q2T", df.format(Double.parseDouble(o[8].toString()) / 1000));
				rm.put("Q2F", df.format(Double.parseDouble(o[9].toString()) / 1000));
				rm.put("Q3T", df.format(Double.parseDouble(o[10].toString()) / 1000));
				rm.put("Q3F", df.format(Double.parseDouble(o[11].toString()) / 1000));
				rm.put("Q9T", df.format(Double.parseDouble(o[12].toString()) / 1000));
				rm.put("Q9F", df.format(Double.parseDouble(o[13].toString()) / 1000));
			}
			Double denominator = (Double.parseDouble(o[12].toString()) + Double.parseDouble(o[13].toString()));
			if (denominator == 0D) {
				rm.put("Q9TF", df.format(0D));
			} else {
				rm.put("Q9TF", df.format(Double.parseDouble(o[13].toString()) / denominator * 100));
			}

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	/**
	 * 取得預約結果日統計表
	 * 
	 * @param YYYYMM 某月
	 * @return
	 * @throws Exception
	 */
	public List<B706Result> findScheduleTxnResultReport(String startDate, String endDate) throws Exception {
		try {
			PropertyUtils utility = new PropertyUtils();
			String sql = utility.getResourceAsString("reports/B706.sql");

			Query query = getSession().createSQLQuery(sql);
			
			query.setParameter(0, startDate); // 第一段成功 SQL
			query.setParameter(1, endDate);
			query.setParameter(2, startDate); // 第二段失敗（使用者原因）SQL
			query.setParameter(3, endDate);
			query.setParameter(4, startDate); // 第三段失敗（系統原因）SQL
			query.setParameter(5, endDate);
			query.setParameter(6, startDate); // 第四段失敗（其它原因）SQL
			query.setParameter(7, endDate);
			query.setParameter(8, startDate); // 第五段未執行SQL
			query.setParameter(9, endDate);
			
			logger.debug("\r\n[SQL:]\r\n" + sql);

			List qresult = query.getResultList();

			/*
			 * qresult 格式如下，R：成功，E1：失敗（使用者原因），E2：失敗（系統原因），E3：失敗（其它），N：未執行 20190822 R 2
			 * 20190823 E2 1 20190823 E1 2 20190823 E2 1 20190823 N 1 20190824 E2 1 20190827
			 * R 1 20191005 R 4
			 */
			/*
			 * 改組成以下的 List，日期-成功筆數-失敗筆數（使用者）-失敗筆數（系統） 20190808 2 0 0 20190823 0 2 1
			 */
			Iterator it = qresult.iterator();
			Map<String, B706Result> results = new TreeMap<String, B706Result>();
			while (it.hasNext()) {
				Object[] o = (Object[]) it.next();
				String date = o[0].toString();
				String type = o[1].toString();
				long count = Long.parseLong(o[2].toString());
				B706Result result = null;
				if (results.containsKey(date)) {
					result = (B706Result) results.get(date);
					results.remove(date);
				} else {
					result = new B706Result();
					result.setTxDay(date);
				}
				if (type.compareTo("R") == 0) {
					result.setSuccCnt(count);
				} else if (type.compareTo("E1") == 0) {
					result.setUserReasonFailCnt(count);
				} else if (type.compareTo("E2") == 0) {
					result.setSystemReasonFailCnt(count);
				} else if (type.compareTo("E3") == 0) {
					result.setOtherReasonFailCnt(count);
				} else if (type.compareTo("N") == 0) {
					result.setNotExecuteCnt(count);
				}
				results.put(date, result);
			}
			// 20191118-Danny-Stored XSS\路徑 3:
			ArrayList<B706Result> newResult = new ArrayList<B706Result>(results.values());
			List<B706Result> newList = (List<B706Result>) (Object) Sanitizer.validList(newResult);
			newResult.clear();
			newResult.addAll(newList);
			return newResult;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * 取得預約結果日統計表（OP）
	 * 
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map findResultReportOP() throws Exception {

		try {
			String startDate = "";
			String endDate = "";
			
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			Date date = new Date();
			String today = dateFormat.format(date);
			startDate = today;
			endDate = today;
			
			PropertyUtils utility = new PropertyUtils();
			String sql = utility.getResourceAsString("reports/B706_OP.sql");

			Query query = getSession().createSQLQuery(sql);
			
			query.setParameter(0, startDate); // 第一段未執行（台幣） SQL
			query.setParameter(1, endDate);
			query.setParameter(2, startDate); // 第二段未執行（外幣）SQL
			query.setParameter(3, endDate);
			query.setParameter(4, startDate); // 第三段成功 SQL
			query.setParameter(5, endDate);
			query.setParameter(6, startDate); // 第四段失敗（使用者原因）SQL
			query.setParameter(7, endDate);
			query.setParameter(8, startDate); // 第五段失敗（系統原因）SQL
			query.setParameter(9, endDate);
			query.setParameter(10, startDate); // 第六段失敗（其它）SQL
			query.setParameter(11, endDate);

			logger.debug("\r\n[SQL:]\r\n" + sql);

			Rows result = new Rows();
			List qresult = query.getResultList();

			Iterator it = qresult.iterator();
			Map rm = new HashMap();

			if (it.hasNext()) {
				Object[] o = (Object[]) it.next();
				
				rm.put("notExecuteTWCnt", o[0]);
				rm.put("notExecuteFXCnt", o[1]);
				rm.put("succCnt", o[2]);
				rm.put("userReasonFailCnt", o[3]);
				rm.put("systemReasonFailCnt", o[4]);
				rm.put("otherReasonFailCnt", o[5]);
		

			}
			return rm;
		} catch (Exception e) {
			throw e;
		}

	}
}
