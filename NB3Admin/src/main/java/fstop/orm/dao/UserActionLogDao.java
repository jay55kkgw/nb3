package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.springframework.stereotype.Repository;

import fstop.orm.po.USERACTIONLOG;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>使用者軌跡記錄，TBB 暫時不用</p>
 *
 * @author chiensj
 * @version V1.0
 * 
 */
@Slf4j
@Repository
@Transactional
public class UserActionLogDao extends LegacyJpaRepository<USERACTIONLOG, Integer> {
	/**
	 * 分頁查詢資料
	 * @param pageNo
	 * @param pageSize
	 * @param orderBy
	 * @param orderDir
	 * @param startTime
	 * @param endTime
	 * @param loginUserId
	 * @return
	 */
    public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String startTime, String endTime, String loginUserId) {
		String hql = "FROM USERACTIONLOG WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if((startTime != null && !startTime.trim().isEmpty()) && (endTime != null && !endTime.trim().isEmpty())) {
			hql += "AND ( RQTIMESTAMP>=? AND RQTIMESTAMP<=? ) ";
			params.add(startTime);
			params.add(endTime);
		}  
		if((startTime != null && !startTime.trim().isEmpty()) && (endTime == null || endTime.trim().isEmpty())) {
			hql += "AND RQTIMESTAMP>=? ";
			params.add(startTime);
		}
		if((startTime == null || startTime.trim().isEmpty()) && (endTime != null && !endTime.trim().isEmpty())) {
			hql += "AND RQTIMESTAMP<=? ";
			params.add(endTime);
		} 
		if(loginUserId != null && !loginUserId.trim().isEmpty()) {
			hql += "AND LOGINUSER like ? ";
			params.add("%"+loginUserId+"%");
		}
		hql += " ORDER BY "+ orderBy+" "+orderDir;

		log.debug(Sanitizer.logForgingStr(hql));

		switch ( params.size() ) {
			case 0:
				return this.pagedQuery(hql, pageNo, pageSize);

			case 1:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0));

			case 2:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));

			case 3:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2));

			default:
				return this.pagedQuery(hql, pageNo, pageSize);
		}
	}
}