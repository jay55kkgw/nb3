package fstop.orm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.IntegerType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNUSER;
import fstop.util.DateTimeUtils;
import fstop.util.Sanitizer;
import fstop.util.StrUtils;

import javax.persistence.Query;


/**
 * <p>TxnUserDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class TxnUserDao extends LegacyJpaRepository<TXNUSER, String> {
	protected Logger logger = Logger.getLogger(getClass());

	/**
	 * <p>getAll.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TXNUSER> getAll() {

		return find("FROM TXNUSER ORDER BY DPSUERID");
	}
	/**
	 * <p>getEmailUser.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TXNUSER> getEmailUser() {
		return find("FROM TXNUSER WHERE DPMYEMAIL <> '' ORDER BY DPSUERID");		
	}
	/**
	 * <p>getBillUser.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TXNUSER> getBillUser() {
		return find("FROM TXNUSER WHERE DPMYEMAIL <> '' AND (DPBILL <> '' OR DPFUNDBILL <> '' OR DPCARDBILL <> '') ORDER BY DPSUERID");		
	}	
	/**
	 * <p>getAllFundBillUser.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TXNUSER> getAllFundBillUser() {
		return find("FROM TXNUSER WHERE DPFUNDBILL <> '' and FUNDBILLDATE <> '' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");		
	}

	/**
	 * <p>getAllFundStopNotifyUser.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TXNUSER> getAllFundStopNotifyUser() {
		return find("FROM TXNUSER WHERE DPNOTIFY like '%14%' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");		
	}

	/**
	 * <p>getAllBillUserLast2Day.</p>
	 *
	 * @param tomorrow a {@link java.lang.String} object.
	 * @param yesterday a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public List<TXNUSER> getAllBillUserLast2Day(String tomorrow, String yesterday) {
		/*
		return find("FROM TXNUSER WHERE ((BILLDATE < '" + tomorrow + "' and BILLDATE >= '" + yesterday + "') " + 
					" or (FUNDBILLDATE < '" + tomorrow + "' and FUNDBILLDATE >= '" + yesterday + "') " +
					" or (CARDBILLDATE < '" + tomorrow + "' and CARDBILLDATE >= '" + yesterday + "')) " +
				    " ORDER BY DPSUERID");		
		*/
		String subyesterday=yesterday.substring(0,7);
		return find("FROM TXNUSER WHERE ((BILLDATE like '" + subyesterday + "%') " + 
				" or (FUNDBILLDATE like '" + subyesterday + "%') " +
				" or (CARDBILLDATE like '" + subyesterday + "%')) " +
			    " ORDER BY DPSUERID");
	}
	
	/**
	 * <p>checkFundAgreeVersion.</p>
	 *
	 * @param version a {@link java.lang.String} object.
	 * @param uid a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public boolean checkFundAgreeVersion(String version, String uid) {

		TXNUSER user = findById(StrUtils.trim(uid));

		if (version.equals(user.getFUNDAGREEVERSION()))
			return true;
		else
			return false;
	}

	/**
	 * <p>chkRecord.</p>
	 *
	 * @param DPSUERID a {@link java.lang.String} object.
	 * @param DPUSERNAME a {@link java.lang.String} object.
	 * @return a {@link fstop.orm.po.TXNUSER} object.
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public TXNUSER chkRecord(String DPSUERID, String DPUSERNAME) {
	//public TXNUSER chkRecord(String DPSUERID) {
		
		String dfNotify = "16,17,18,19";//黃金存摺相關的要預設啟用通知
		TXNUSER user = null;
		int isUserExist = -1;
		try {
			user = findById(StrUtils.trim(DPSUERID));
			log.info("" + user.getDPSUERID());
			isUserExist = ((user == null) ? 0 : 1);
		}
		catch(Exception e) {
			isUserExist = 0;
		}
		if(isUserExist == 0 ) {
			TXNUSER u = new TXNUSER();
			u.setDPSUERID(DPSUERID);
			u.setSCHCOUNT(new Integer(0));
			u.setASKERRTIMES(new Integer(0));
			u.setDPUSERNAME(DPUSERNAME);
            u.setDPNOTIFY(dfNotify);//黃金存摺相關的要預設啟用通知
			save(u);
			return u;
		}
		else if(isUserExist == 1) {
			user.setDPUSERNAME(DPUSERNAME);
			save(user);

			return user;
		}

		return null;
	}

	/**
	 * <p>updateAdBranchID.</p>
	 *
	 * @param DPSUERID a {@link java.lang.String} object.
	 * @param ADBRANCHID a {@link java.lang.String} object.
	 * @return a int.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int updateAdBranchID(String DPSUERID,String ADBRANCHID) {

		Query add1=this.getSession().createSQLQuery("UPDATE TXNUSER SET ADBRANCHID = ? WHERE DPSUERID = ?");

		add1.setParameter(0, ADBRANCHID);
		add1.setParameter(1, DPSUERID);
		//20191120-Danny-Second Order SQL Injection\路徑 1:
		int r2 = (int)Sanitizer.sanitize(add1.executeUpdate());
		return(r2);
	}

	/**
	 * 信用卡、金融卡申請網銀，如果使用者不在時建立一個使用者。
	 *
	 * @param DPSUERID a {@link java.lang.String} object.
	 * @return a {@link fstop.orm.po.TXNUSER} object.
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public TXNUSER chkRecord(String DPSUERID) {

		TXNUSER user = null;
		int isUserExist = -1;
		try {
//			user = super.get(StrUtils.trim(DPSUERID));
			user = findById(StrUtils.trim(DPSUERID));
//			System.out.println("" + user.getDPSUERID());
			isUserExist = ((user == null) ? 0 : 1);
		}
		catch(Exception e) {
			isUserExist = 0;
		}
		if(isUserExist == 0 ) {
			TXNUSER u = new TXNUSER();
			u.setDPSUERID(DPSUERID);
			u.setSCHCOUNT(new Integer(0));
			u.setASKERRTIMES(new Integer(0));
			u.setCCARDFLAG("0");

			save(u);
			return u;
		}
		else if(isUserExist == 1) {
			return user;
		}

		return null;
	}
}
