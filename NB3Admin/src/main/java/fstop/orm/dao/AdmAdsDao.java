package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMADS;


/**
 * <p>廣告管理主檔 DAO</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Repository
@Transactional
public class AdmAdsDao extends LegacyJpaRepository<ADMADS, String> {
    
}
