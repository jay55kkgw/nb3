package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMSYSCODEM;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


/**
 * <p>AdmSysCodeMDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
public class AdmSysCodeMDao extends LegacyJpaRepository<ADMSYSCODEM, String> {
	protected Logger logger = Logger.getLogger(getClass());
}
