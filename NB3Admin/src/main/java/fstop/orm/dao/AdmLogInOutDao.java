package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMLOGINOUT;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.netbank.domain.orm.core.Page;
import java.util.ArrayList;

/**
 * <p>AdmLogInOutDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional

public class AdmLogInOutDao extends LegacyJpaRepository<ADMLOGINOUT, Integer> {
	protected Logger logger = Logger.getLogger(getClass());

	/**
	 * <p>findPageData.</p>
	 *
	 * @param pageNo a int.
	 * @param pageSize a int.
	 * @param orderBy a {@link java.lang.String} object.
	 * @param orderDir a {@link java.lang.String} object.
	 * @param startDate a {@link java.lang.String} object.
	 * @param endDate a {@link java.lang.String} object.
	 * @param userId a {@link java.lang.String} object.
	 * @param loginType a {@link java.lang.String} object.
	 * @return a {@link com.netbank.domain.orm.core.Page} object.
	 */
	public Page findPageData(int pageNo, int pageSize, String orderBy, String orderDir, String startDate,
			String endDate, String userId, String loginType) {

		List<Object> params = new ArrayList<Object>();
		String hql = "FROM ADMLOGINOUT WHERE (CMYYYYMM>=? AND CMYYYYMM<=?) AND concat(CMYYYYMM,CMDD) >= ? and concat(CMYYYYMM,CMDD) <= ? ";
		
		params.add(startDate.substring(0, 6));
		params.add(endDate.substring(0, 6));
		params.add(startDate);
		params.add(endDate);

		if (userId != null && !userId.trim().isEmpty()) {
			hql += " AND ADUSERID = ? ";
			params.add(userId);
		}
		if (loginType != null && !loginType.trim().isEmpty()) {
			hql += " AND LOGINTYPE = ? ";
			params.add(loginType);
		}
		hql += " ORDER BY " + orderBy + " " + orderDir;

		log.trace(Sanitizer.logForgingStr(hql));

		return this.pagedQuery(hql, pageNo, pageSize, params);
	}

	/*
	 * 
	 */
	/**
	 * <p>findLoginReportByMonth.</p>
	 *
	 * @param startYYMM a {@link java.lang.String} object.
	 * @param endYYMM a {@link java.lang.String} object.
	 * @param logintype a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 */
	public Rows findLoginReportByMonth(String startYYMM, String endYYMM, String logintype) {
		Rows result = new Rows();

		Query query = getSession().createSQLQuery("" + " SELECT varchar(CMYYYYMM) as cmyyyymm, count(*) as ADSUMROW,"
				+ "  sum(case when ADUSERTYPE = '1' then 1 else 0 end ) as ADENTERP , "
				+ "  sum(case when ADUSERTYPE = '0' then 1 else 0 end ) as ADPERSON  " +

				" FROM ADMLOGINOUT WHERE LOGINOUT='0' and CMYYYYMM >= ? and CMYYYYMM <= ? " + " and LOGINTYPE in ("
				+ logintype + ") " + " GROUP BY CMYYYYMM ORDER BY CMYYYYMM ASC ");
		query.setParameter(0, startYYMM);
		query.setParameter(1, endYYMM);

		List qresult = query.getResultList();
		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map<String, String> rm = new HashMap<String, String>();
			rm.put("CMYYYYMM", o[0].toString());
			rm.put("ADSUMROW", o[1].toString());
			rm.put("ADENTERP", o[2].toString());
			rm.put("ADPERSON", o[3].toString());

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	/**
	 * <p>findLoginReportByDate.</p>
	 *
	 * @param startYYMMDD a {@link java.lang.String} object.
	 * @param endYYMMDD a {@link java.lang.String} object.
	 * @param logintype a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 */
	public Rows findLoginReportByDate(String startYYMMDD, String endYYMMDD, String logintype) {
		Rows result = new Rows();

		Query query = getSession()
				.createSQLQuery("" + " SELECT varchar(CMYYYYMM) || varchar(CMDD) AS yyyymmdd, count(*) as ADSUMROW, "
						+ " sum(case when ADUSERTYPE = '1' then 1 else 0 end ) as ADENTERP , "
						+ " sum(case when ADUSERTYPE = '0' then 1 else 0 end ) as ADPERSON  " +

						" FROM ADMLOGINOUT WHERE LOGINOUT='0' and (CMYYYYMM >= ? and CMYYYYMM <= ?) and CMYYYYMM || CMDD >= ? and CMYYYYMM || CMDD <= ? "
						+ " and LOGINTYPE in (" + logintype + ") "
						+ " GROUP BY CMYYYYMM, CMDD ORDER BY CMYYYYMM, CMDD ASC ");

		query.setParameter(0, startYYMMDD.substring(0, 6));
		query.setParameter(1, endYYMMDD.substring(0, 6));
		query.setParameter(2, startYYMMDD);
		query.setParameter(3, endYYMMDD);
		// query.setParameter(2, logintype);

		List qresult = query.getResultList();
		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map<String, String> rm = new HashMap<String, String>();
			rm.put("CMYYYYMM", o[0].toString());
			rm.put("ADSUMROW", o[1].toString());
			rm.put("ADENTERP", o[2].toString());
			rm.put("ADPERSON", o[3].toString());

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}
}
