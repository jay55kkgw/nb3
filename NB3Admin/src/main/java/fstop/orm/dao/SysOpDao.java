package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.SYSOP;
import fstop.util.Sanitizer;

import javax.persistence.Query;

/**
 * <p>SysOpDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class SysOpDao extends LegacyJpaRepository<SYSOP, String> {

	/**
	 * <p>getAll.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	@SuppressWarnings( { "unchecked" })
	public List<SYSOP> getAll()
	{
		List<SYSOP> result = new ArrayList<SYSOP>();

        String hql = "FROM SYSOP ORDER BY ADSEQ";
        log.trace(Sanitizer.logForgingStr(hql));
        
        result = find(hql);
		return result;
    }

	/**
	 * <p>getByADOPID.</p>
	 *
	 * @param ADOPID a {@link java.lang.String} object.
	 * @return a {@link fstop.orm.po.SYSOP} object.
	 */
	@SuppressWarnings({ "unchecked" })
    public SYSOP getByADOPID(String ADOPID) {
        List<SYSOP> result = new ArrayList<SYSOP>();

        String hql = "FROM SYSOP WHERE ADOPID=? ";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[1];
        params[0] = ADOPID;

        result = find(hql, params);
        if ( result.size()>0 )
            return result.get(0);
        else
            return null;
	}
	
	/**
	 * <p>getByADOPGROUPID.</p>
	 *
	 * @param ADOPGROUPID a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	@SuppressWarnings({ "unchecked" })
    public List<SYSOP> getByADOPGROUPID(String ADOPGROUPID) {
        List<SYSOP> result = new ArrayList<SYSOP>();

        String hql = "FROM SYSOP WHERE ADOPGROUP=? ORDER BY ADSEQ ASC";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[1];
        params[0] = ADOPGROUPID;

        result = find(hql, params);
        return result;
	}
	
    /**
     * 依父選單ID，取得子選單中最大的選單序號
     *
     * @param ADOPGROUPID a {@link java.lang.String} object.
     * @return a int.
     */
    @SuppressWarnings({ "unchecked" })
    public int getMaxADSEQ(String ADOPGROUPID) {
        List<SYSOP> result = new ArrayList<SYSOP>();

        String hql = "FROM SYSOP WHERE ADOPGROUP=? ORDER BY ADSEQ DESC  ";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[1];
        params[0] = ADOPGROUPID;

        result = find(hql, params);
        if ( result.size()>0 )
            return result.get(0).getADSEQ();
        else
            return 0;
	}
	
	/**
	 * <p>updateADSEQ.</p>
	 *
	 * @param ADOPID a {@link java.lang.String} object.
	 * @param ADSEQ a {@link java.lang.String} object.
	 */
	public void updateADSEQ(String ADOPID, String ADSEQ) {
        String hql = "update SYSOP set ADSEQ=:ADSEQ where ADOPID=:ADOPID";
        Query query = this.getSession().createQuery(hql);
        query.setParameter("ADSEQ", ADSEQ);
        query.setParameter("ADOPID", ADOPID);

        int rowAffected = query.executeUpdate();
        log.debug("rowAffected={}", rowAffected);
    }
} 
