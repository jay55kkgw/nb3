package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import fstop.orm.po.ADMMAILLOG;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * 電郵記錄檔 dao 使用一個畫面做 CRUD
 * 已合併AdmMailContent
 *
 * @author YU-TAN LIN
 * @version V1.0
 */

@Slf4j
@Repository
@Transactional
public class AdmMailLogDao_ex extends LegacyJpaRepository<ADMMAILLOG, Long> {
	/**
	 * 以查詢條件做分頁查詢
	 *
	 * @param pageNo 第幾頁
	 * @param pageSize 一頁幾筆
	 * @param orderBy 依什麼欄位排序
	 * @param orderDir 升冪/降冪
	 * @param ADUSERID 身分證字號/統一編號
	 * @param ADSENDSTATUS 寄送狀態，附註:為了以後增加功能用
	 * @param startDateTime 搜尋起始時間
	 * @param endDatetime 搜尋結束時間
	 * @return a {@link com.netbank.domain.orm.core.Page} object.
	 */
	public Page findPageDateRange(int pageNo, int pageSize, String orderBy, String orderDir, 
		String  ADUSERID, String ADSENDSTATUS, String startDateTime, String endDatetime) {
		
		log.debug("dao.findPageDateRange pageNo={}, pageSize={}, orderBy={}, orderDir={},"
                        +" ADUSERID={}, ADSENDSTATUS={}, sStart={}, sEnd={}",
						Sanitizer.logForgingStr(pageNo), Sanitizer.logForgingStr(pageSize), Sanitizer.logForgingStr(orderBy), Sanitizer.logForgingStr(orderDir), 
		Sanitizer.logForgingStr(ADUSERID), Sanitizer.logForgingStr(ADSENDSTATUS), Sanitizer.logForgingStr(startDateTime), Sanitizer.logForgingStr(endDatetime));

		String hql = "FROM ADMMAILLOG WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();
		
		if(ADUSERID != null && !ADUSERID.trim().isEmpty()) {
			hql += "AND ADUSERID like ? ";
			params.add("%"+ADUSERID+"%");
		}
		if(ADSENDSTATUS != null && !ADSENDSTATUS.trim().isEmpty()) {
			hql += "AND ADSENDSTATUS = ? ";
			params.add(ADSENDSTATUS);
		}
		if(startDateTime != null && !startDateTime.trim().isEmpty()) {
			hql += "AND ADSENDTIME >= ? ";
			params.add(startDateTime);
		}
		if(endDatetime != null && !endDatetime.trim().isEmpty()) {
			hql += "AND ADSENDTIME <= ? ";
			params.add(endDatetime);
		}
		hql += "AND ADMAILACNO <> ? ";
		params.add("");

		hql += " ORDER BY "+ orderBy+" "+orderDir;

		log.trace(Sanitizer.logForgingStr(hql));
		//20191119-Danny-內容含有HTML改用沒有驗證的查詢
		return this.pagedQueryContentHasHtml(hql, pageNo, pageSize, params);
	}
	
}
