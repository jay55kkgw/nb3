package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMNBSTATUS;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


/**
 * <p>AdmNbStatusDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Repository
public class AdmNbStatusDao extends LegacyJpaRepository<ADMNBSTATUS, String> {
	
	protected Logger logger = Logger.getLogger(getClass());

}
