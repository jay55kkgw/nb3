package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNTWRECORD;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TxnTwRecordDao extends LegacyJpaRepository<TXNTWRECORD, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
    /**
     * 
     * @param dptxdate
     * @param dpwdac
     * @param dpsvac
     * @param seqTRN
     * @param dptxamt
     * @return
     */
    public List<TXNTWRECORD> getTXNTWRECORDs(String dptxdate, String dpwdac, String dpsvac, String seqTRN, String dptxamt) {
		String hql = "FROM TXNTWRECORD WHERE DPTXDATE='"+ dptxdate + "' " + 
						" AND DPWDAC='"+ dpwdac + "' " + 
						" AND DPSVAC='"+ dpsvac + "' " + 
						" AND DPTITAINFO like '%"+ seqTRN + "%' " + 
						" AND DPTXAMT='"+ dptxamt + "' " ;
		return find(hql);
	}	
    
    /**
     * 查詢同交易代號, 身分證號, 交易日, 轉出帳號, 轉入帳號及金額
     * @param adOpId
     * @param dpUserId
     * @param dpTxDate
     * @param dpwDAC
     * @param dpsVAC
     * @param dpTxAmt
     * @return
     */
    public List<TXNTWRECORD> getDuplicateTxn(String adOpId, String dpUserId, String dpTxDate, String dpwDAC, String dpsVAC, String dpTxAmt) {
    	String hql = "FROM TXNTWRECORD Where ADOPID=? AND DPUSERID=? AND DPTXDATE=? AND DPWDAC=? AND DPSVAC=? AND DPTXAMT=? AND DPEXCODE=? ORDER BY DPTXTIME DESC";
    	
    	List<Object> params = new ArrayList<Object>();
		params.add(adOpId);	
		params.add(dpUserId);
		params.add(dpTxDate);
		params.add(dpwDAC);
		params.add(dpsVAC);
		params.add(dpTxAmt);
		params.add("");

		log.trace(Sanitizer.logForgingStr(hql));

		return this.find(hql, params);
    }
}
