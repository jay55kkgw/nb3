package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNFXSCHPAYDATAIDENTITY;
import lombok.extern.slf4j.Slf4j;
import fstop.util.Sanitizer;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Query;

@Slf4j
@Repository
@Transactional
public class TxnFxSchPayDataDao extends LegacyJpaRepository<TXNFXSCHPAYDATA, TXNFXSCHPAYDATAIDENTITY> {
	protected Logger logger = Logger.getLogger(getClass());

	public Page findPageData(int pageNo, int pageSize, String orderBy, String orderDir, String startDate,
			String endDate, String userId, String status) {

		List<Object> params = new ArrayList<Object>();
		String hql = "FROM TXNFXSCHPAYDATA WHERE ((FXSCHTXDATE >= ? AND FXSCHTXDATE <= ?) OR (FXTXDATE >= ? AND FXTXDATE <= ?) OR (FXTXDATE ='' AND FXSCHTXDATE < ?)) ";
		
		params.add(startDate);
		params.add(endDate);
		params.add(startDate);
		params.add(endDate);
		params.add(endDate);

		if ( !userId.isEmpty() ) {
			hql += " AND FXUSERID = ? ";
			params.add(userId);
		}
		
		if (!status.equals("All")) {
			hql += " AND FXTXSTATUS = ? ";
			params.add(status);
		}
		//按送出及按日期排序，則以日期排序
		if (orderBy.equals("msaddr") || orderBy.equals("fxtxdate"))
		{
			hql += " ORDER BY  FXTXDATE "  + orderDir;
		}
		else
		{
			hql += " ORDER BY " + orderBy.replace("fxschpaydataidentity.", "") + " " + orderDir;
		}

		log.trace(Sanitizer.logForgingStr(hql));
		
		return this.pagedQuery(hql, pageNo, pageSize, params);
	}
	
	/**
	 * 預約交易結果查詢
	 * @param pageNo
	 * @param pageSize
	 * @param orderBy
	 * @param orderDir
	 * @param startDate
	 * @param endDate
	 * @param userId
	 * @param status
	 * @return
	 */
	public Page findPageData4B205(int pageNo, int pageSize, String orderBy, String orderDir, String startDate,
			String endDate, String userId, String status) {

		List<Object> params = new ArrayList<Object>();
		String hql = "FROM TXNFXSCHPAYDATA X WHERE (FXSCHTXDATE >= ? AND FXSCHTXDATE <= ?) ";
		
		/*
		 * 因查詢延遲問題，暫時移除條件，之後視情況調整 
		 * + "OR (FXTXDATE >= ? AND FXTXDATE <= ?) ) ";
		 */
		
		params.add(startDate);
		params.add(endDate);
//		params.add(startDate);
//		params.add(endDate);

		//排除已取消預約
		hql += " AND FXSCHNO IN (SELECT FXSCHNO FROM TXNFXSCHPAY WHERE FXUSERID=X.FXSCHPAYDATAIDENTITY.FXUSERID AND FXTXSTATUS NOT IN(?)) ";
		params.add("3");
		
		if ( !userId.isEmpty() ) {
			hql += " AND FXUSERID = ? ";
			params.add(userId);
		}
		
		if (!status.equals("All")) {
			hql += " AND FXTXSTATUS = ? ";
			params.add(status);
		}
		//按送出及按日期排序，則以日期排序
		if (orderBy.equals("msaddr") || orderBy.equals("fxtxdate"))
		{
			hql += " ORDER BY  FXTXDATE "  + orderDir;
		}
		else
		{
			hql += " ORDER BY " + orderBy.replace("fxschpaydataidentity.", "") + " " + orderDir;
		}

		log.trace(Sanitizer.logForgingStr(hql));
		
		return this.pagedQuery(hql, pageNo, pageSize, params);
	}

	// 取得下次轉帳日(外幣)
	public String getFxschtxToday(String userid, String fxschno) {

		userid = (String) Sanitizer.sanitize(userid);
		fxschno = (String) Sanitizer.sanitize(fxschno);
		try {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String today = sdf.format(date);
			String queryString = "SELECT FXSCHTXDATE FROM TXNFXSCHPAYDATA WHERE FXUSERID  = ? AND FXSCHNO = ? AND FXSCHTXDATE >= ? ";
			Query query = getSession().createQuery(queryString);
			query.setParameter(0, userid);
			query.setParameter(1, fxschno);
			query.setParameter(2, today);

			List result = query.getResultList();
			if (result.size() == 0)
				return "";

			return result.get(0).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public List<TXNFXSCHPAYDATA> findByParams(String startDate,String endDate, String userId) {

		List<Object> params = new ArrayList<Object>();
		String hql = "FROM TXNFXSCHPAYDATA WHERE (FXSCHTXDATE >= ? AND FXSCHTXDATE <= ?) AND FXTXSTATUS NOT IN('','0') AND FXEXCODE IN (SELECT ADMCODE FROM ADMMSGCODE WHERE ADMRESEND= 'Y')";

		params.add(startDate);
		params.add(endDate);

		if ( !userId.isEmpty() ) {
			hql += " AND FXUSERID = ? ";
			params.add(userId);
		}

//		if (!status.equals("All")) {
//			String[] aStatus = status.split(",");
//			if ( aStatus.length>1  ) {
//				switch ( aStatus.length ) {
//				case 2:
//					hql +=  " AND FXTXSTATUS in ( ?,? ) ";
//					params.add(aStatus[0]);
//					params.add(aStatus[1]);
//					break;
//					
//				case 3:
//					hql += " AND FXTXSTATUS in ( ?,?,? ) ";
//					params.add(aStatus[0]);
//					params.add(aStatus[1]);
//					params.add(aStatus[2]);
//					break;
//				}
//			} else {
//				hql += " AND FXTXSTATUS = ? ";
//				params.add(status);
//			}
//		}

		Query query = getQuery(hql);
		for ( int i=0; i<params.size(); i++ ) {
			query.setParameter(i, params.get(i));
		}

		log.trace(Sanitizer.logForgingStr(hql));

		List qresult = query.getResultList();

		return qresult;
	}

}
