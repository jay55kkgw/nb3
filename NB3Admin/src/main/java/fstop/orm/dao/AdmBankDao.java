package fstop.orm.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMBANK;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>AdmBankDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmBankDao extends LegacyJpaRepository<ADMBANK, Serializable> {
	/**
	 * 分頁查詢
	 *
	 * @param pageNo 頁碼，從 1 編起
	 * @param pageSize 一頁幾筆
	 * @param orderBy 依什麼欄位排序
	 * @param orderDir 升冪/降冪
	 * @param ADBANKID 銀行代碼
	 * @param ADBANKNAME 銀行中文
	 * @return 分頁物件
	 */
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ADBANKID, String ADBANKNAME, String ADBANKCHSNAME, String ADBANKENGNAME) {
		String hql = "FROM  ADMBANK WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if(ADBANKID != null && !ADBANKID.trim().isEmpty()) {
			hql += "AND ADBANKID LIKE ? ";
			params.add("%"+ADBANKID+"%");
		}
		if(ADBANKNAME != null && !ADBANKNAME.trim().isEmpty()) {
			hql += "AND ADBANKNAME LIKE ? ";
			params.add("%"+ADBANKNAME+"%");
		}
		if(ADBANKCHSNAME != null && !ADBANKCHSNAME.trim().isEmpty()) {
			hql += "AND ADBANKCHSNAME LIKE ? ";
			params.add("%"+ADBANKCHSNAME+"%");
		}
		if(ADBANKENGNAME != null && !ADBANKENGNAME.trim().isEmpty()) {
			hql += "AND ADBANKENGNAME LIKE ? ";
			params.add("%"+ADBANKENGNAME+"%");
		}
		hql += " ORDER BY "+ orderBy+" "+orderDir;

		log.trace(Sanitizer.logForgingStr(hql));

		switch ( params.size() ) {
			case 0:
				return this.pagedQuery(hql, pageNo, pageSize);

			case 1:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0));

			case 2:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));

			case 3:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2));
				
			case 4:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3));

			case 5:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3), params.get(4));

			default:
				return this.pagedQuery(hql, pageNo, pageSize);
		}
	}
}
