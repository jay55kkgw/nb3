package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.B402Model;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMADSTMP;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.orm.po.TXNTWSCHPAYDATAIDENTITY;
import lombok.extern.slf4j.Slf4j;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.persistence.Query;
import fstop.util.Sanitizer;

@Slf4j
@Repository
@Transactional
public class TxnTwSchPayDataDao extends LegacyJpaRepository<TXNTWSCHPAYDATA, TXNTWSCHPAYDATAIDENTITY> {
	protected Logger logger = Logger.getLogger(getClass());

	public Page findPageData(int pageNo, int pageSize, String orderBy, String orderDir, String startDate,
			String endDate, String userId, String status) {

		List<Object> params = new ArrayList<Object>();
		//20191223-Danny-增加實際轉帳日為今天的
		String hql = "FROM TXNTWSCHPAYDATA X WHERE (DPSCHTXDATE >= ? AND DPSCHTXDATE <= ?) ";
				
		/*
		 * 因查詢延遲問題，暫時移除條件，之後視情況調整 
		 * + "OR (DPTXDATE >= ? AND DPTXDATE <= ?) ) ";
		 */
		params.add(startDate);
		params.add(endDate);
//		params.add(startDate);
//		params.add(endDate);

		//排除已取消預約
		hql += " AND DPSCHNO IN (SELECT DPSCHNO FROM TXNTWSCHPAY WHERE DPUSERID=X.TWSCHPAYDATAIDENTITY.DPUSERID AND DPTXSTATUS NOT IN(?)) ";
		params.add("3");
		
		if ( !userId.isEmpty() ) {
			hql += " AND DPUSERID= ? ";
			params.add(userId);
		}

		if (!status.equals("All")) {
			hql += " AND DPTXSTATUS = ? ";
			params.add(status);
		} else {
			hql += " AND DPTXSTATUS <> ? ";
			params.add("");
		}
		

		//按送出及按日期排序，則以日期排序
		if (orderBy.equals("msaddr") || orderBy.equals("dpschtxdate"))
		{
			hql += " ORDER BY  DPSCHTXDATE "  + orderDir;
		}
		else
		{
			hql += " ORDER BY " + orderBy.replace("twschpaydataidentity.", "") + " " + orderDir;
		}
		log.trace(Sanitizer.logForgingStr(hql));

		return this.pagedQuery(hql, pageNo, pageSize, params);
	}

	// 取得下次轉帳日(台幣)
	public String getDpschtxdate(String userid, String dpschno) {

		userid = (String) Sanitizer.sanitize(userid);
		dpschno = (String) Sanitizer.sanitize(dpschno);
		try {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String today = sdf.format(date);
			String queryString = "SELECT DPSCHTXDATE FROM TXNTWSCHPAYDATA WHERE DPUSERID  = ? AND DPSCHNO = ? AND DPSCHTXDATE >= ? ";
			Query query = getSession().createQuery(queryString);
			query.setParameter(0, userid);
			query.setParameter(1, dpschno);
			query.setParameter(2, today);

			List result = query.getResultList();
			if (result.size() == 0)
				return "";

			return result.get(0).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public List<TXNTWSCHPAYDATA> findByParams(String startDate, String endDate, String userId) {

		List<Object> params = new ArrayList<Object>();
		//20191223-Danny-增加實際轉帳日為今天的
		String hql = "FROM TXNTWSCHPAYDATA WHERE (DPSCHTXDATE >= ? AND DPSCHTXDATE <= ?) AND ADTXNO !='' AND DPTXSTATUS NOT IN('','0') AND DPEXCODE IN (SELECT ADMCODE FROM ADMMSGCODE WHERE ADMRESEND= 'Y' )";

		params.add(startDate);
		params.add(endDate);
		
		if ( !userId.isEmpty() ) {
			hql += " AND DPUSERID= ? ";
			params.add(userId);
		}

		Query query = getQuery(hql);
		for ( int i=0; i<params.size(); i++ ) {
			query.setParameter(i, params.get(i));
		}

		log.trace(Sanitizer.logForgingStr(hql));

		List qresult = query.getResultList();

		return qresult;
	}

	
	public List<B402Model> findByParamsCSV(String startDate,
			String endDate, String userId, String status, boolean onlyResend) {

			List<Object> params = new ArrayList<Object>();
			
			String sql = ""; 
			if ( onlyResend ) {
				//用 as 旨在向前相容
				sql = "SELECT a.DPSCHNO,a.DPTXDATE,a.LASTTIME,a.DPUSERID,a.ADOPID,a.DPWDAC,a.DPSVBH,a.DPSVAC,a.DPTXAMT,a.DPEXCODE,c.ADMSGIN,z.DPUSERNAME,b.MOBTEL,b.MOBTEL AS EINPHONE  FROM TXNTWSCHPAYDATA a";
			    sql += " LEFT JOIN ADMLOGIN b ON a.DPUSERID = b.ADUSERID  ";
			    sql += " LEFT JOIN ADMMSGCODE c ON a.DPEXCODE = c.ADMCODE  ";
			    sql += " LEFT JOIN TXNUSER z ON a.DPUSERID = z.DPSUERID ";
			    sql += " WHERE a.DPEXCODE IN (SELECT ADMCODE FROM ADMMSGCODE WHERE ADMRESEND= ? )  ";
			    sql += " AND ((a.DPSCHTXDATE >= ? AND a.DPSCHTXDATE <= ?) OR (a.DPTXDATE >= ? AND a.DPTXDATE <= ?)) ";
			    sql += " AND a.ADTXNO !='' AND a.DPTXSTATUS NOT IN('','0')";
			    //排除已取消預約
			    sql += " AND a.DPSCHNO IN (SELECT DPSCHNO FROM TXNTWSCHPAY WHERE DPTXSTATUS NOT IN(?)) ";
			    
			    params.add("Y");
				params.add(startDate);
				params.add(endDate);
				params.add(startDate);
				params.add(endDate);
				params.add("3");
			} else {
				//用 as 旨在向前相容
				sql = "SELECT a.DPSCHNO,a.LASTDATE,a.LASTTIME,a.DPUSERID,a.ADOPID,a.DPWDAC,a.DPSVBH,a.DPSVAC,a.DPTXAMT,a.DPEXCODE,c.ADMSGIN,z.DPUSERNAME,b.MOBTEL,b.MOBTEL AS EINPHONE FROM TXNTWSCHPAYDATA a";
			    sql += " LEFT JOIN ADMLOGIN b ON a.DPUSERID = b.ADUSERID  ";
			    sql += " LEFT JOIN ADMMSGCODE c ON a.DPEXCODE = c.ADMCODE  ";
			    sql += " LEFT JOIN TXNUSER z ON a.DPUSERID = z.DPSUERID ";
			    sql += " WHERE ((a.DPSCHTXDATE >= ? AND a.DPSCHTXDATE <= ?) OR (a.DPTXDATE >= ? AND a.DPTXDATE <= ?)) ";
			    sql += " AND a.ADTXNO !='' AND a.DPTXSTATUS NOT IN('','0')";
			    //排除已取消預約
			    sql += " AND a.DPSCHNO IN (SELECT DPSCHNO FROM TXNTWSCHPAY WHERE DPTXSTATUS NOT IN(?)) ";
			    
				params.add(startDate);
				params.add(endDate);
				params.add(startDate);
				params.add(endDate);
				params.add("3");
			}

		    
			
			if ( !userId.isEmpty() ) {
				sql += " AND a.DPUSERID= ? ";
				params.add(userId);
			}
			
            if (!status.equals("All")) {
                String[] aStatus = status.split(",");
                if ( aStatus.length>1  ) {
                    switch ( aStatus.length ) {
                    case 2:
                        sql +=  " AND DPTXSTATUS in ( ?,? ) ";
                        params.add(aStatus[0]);
                        params.add(aStatus[1]);
                        break;
                        
                    case 3:
                        sql += " AND DPTXSTATUS in ( ?,?,? ) ";
                        params.add(aStatus[0]);
                        params.add(aStatus[1]);
                        params.add(aStatus[2]);
                        break;
                    }
                } else {
                    sql += " AND DPTXSTATUS = ? ";
                    params.add(status);
                }
            } else {
                sql +=  " AND DPTXSTATUS<>? ";
                params.add("");
            }


	        Query query = getSession().createSQLQuery(sql);
	        for ( int i=0; i<params.size(); i++ ) {
	            query.setParameter(i, params.get(i));
	        }
	 
	        List<B402Model> queryResults = new ArrayList<B402Model>();
	        List qresult = query.getResultList();
	        
	        //20200608-chiensj-CGI Stored XSS
	        qresult = Sanitizer.validList(qresult);
	        
	        Iterator it = qresult.iterator();
			while (it.hasNext()) {
	            Object[] o = (Object[]) it.next();
	            B402Model tmp = new B402Model();
				tmp.setDPSCHNO(o[0]==null ? "" : o[0].toString());
	            tmp.setLASTDATE(o[1]==null ? "" : o[1].toString());
	            tmp.setLASTTIME(o[2]==null ? "" : o[2].toString());
	            tmp.setDPUSERID(o[3]==null ? "" : o[3].toString());
	            tmp.setADOPID(o[4]==null ? "" : o[4].toString());
	            tmp.setDPWDAC(o[5]==null ? "" : o[5].toString());
	            tmp.setDPSVBH(o[6]==null ? "" : o[6].toString());
	            tmp.setDPSVAC(o[7]==null ? "" : o[7].toString());
	            tmp.setDPTXAMT(o[8]==null ? "" : o[8].toString());
	            tmp.setDPEXCODE(o[9]==null ? "" : o[9].toString());
	            tmp.setADMSGIN(o[10]==null ? "" : o[10].toString());
	            tmp.setDPUSERNAME(o[11]==null ? "" : o[11].toString());
	            tmp.setEINPHONE(o[12]==null ? "" : o[12].toString());
				queryResults.add(tmp);
	        }
			return  queryResults;
	}
	
	public List<B402Model> findByParamsCSVByB402(String startDate,
			String endDate, String userId) {

			List<Object> params = new ArrayList<Object>();
			
			String sql = ""; 
			
			//用 as 旨在向前相容
			sql = "SELECT a.DPSCHNO,a.DPTXDATE,a.DPTXTIME,a.DPUSERID,a.ADOPID,a.DPWDAC,a.DPSVBH,a.DPSVAC,a.DPTXAMT,a.DPEXCODE,c.ADMSGIN,z.DPUSERNAME,b.MOBTEL AS EINPHONE  FROM TXNTWSCHPAYDATA a";
			sql += " LEFT JOIN ADMLOGIN b ON a.DPUSERID = b.ADUSERID  ";
			sql += " LEFT JOIN ADMMSGCODE c ON a.DPEXCODE = c.ADMCODE ";
			sql += " LEFT JOIN TXNUSER z ON a.DPUSERID = z.DPSUERID ";
			sql += " WHERE a.DPEXCODE IN (SELECT ADMCODE FROM ADMMSGCODE WHERE ADMRESEND= 'Y' ) ";
			sql += " AND (a.DPSCHTXDATE >= ? AND a.DPSCHTXDATE <= ?) ";
			sql += " AND a.ADTXNO !='' AND a.DPTXSTATUS NOT IN('','0')";

			params.add(startDate);
			params.add(endDate);

			if ( !userId.isEmpty() ) {
				sql += " AND a.DPUSERID= ? ";
				params.add(userId);
			}

	        Query query = getSession().createSQLQuery(sql);
	        for ( int i=0; i<params.size(); i++ ) {
	            query.setParameter(i, params.get(i));
	        }
	 
	        List<B402Model> queryResults = new ArrayList<B402Model>();
	        List qresult = query.getResultList();
	        
	        //20200608-chiensj-CGI Stored XSS
	        qresult = Sanitizer.validList(qresult);
	        
	        Iterator it = qresult.iterator();
			while (it.hasNext()) {
	            Object[] o = (Object[]) it.next();
	            B402Model tmp = new B402Model();
				tmp.setDPSCHNO(o[0]==null ? "" : o[0].toString());
	            tmp.setLASTDATE(o[1]==null ? "" : o[1].toString());
	            tmp.setLASTTIME(o[2]==null ? "" : o[2].toString());
	            tmp.setDPUSERID(o[3]==null ? "" : o[3].toString());
	            tmp.setADOPID(o[4]==null ? "" : o[4].toString());
	            tmp.setDPWDAC(o[5]==null ? "" : o[5].toString());
	            tmp.setDPSVBH(o[6]==null ? "" : o[6].toString());
	            tmp.setDPSVAC(o[7]==null ? "" : o[7].toString());
	            tmp.setDPTXAMT(o[8]==null ? "" : o[8].toString());
	            tmp.setDPEXCODE(o[9]==null ? "" : o[9].toString());
	            tmp.setADMSGIN(o[10]==null ? "" : o[10].toString());
	            tmp.setDPUSERNAME(o[11]==null ? "" : o[11].toString());
	            tmp.setEINPHONE(o[12]==null ? "" : o[12].toString());
				queryResults.add(tmp);
	        }
			return  queryResults;
	}
}
