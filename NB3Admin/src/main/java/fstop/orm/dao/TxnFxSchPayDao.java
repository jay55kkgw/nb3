package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNFXSCHPAY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TxnFxSchPayDao extends LegacyJpaRepository<TXNFXSCHPAY, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public Page findPageData(int pageNo, int pageSize, String orderBy, String orderDir, String userId, String dtpDate) {

		List<Object> params = new ArrayList<Object>();
		String hql = "FROM TXNFXSCHPAY WHERE FXUSERID= ? AND FXTDATE>= ? AND FXTXSTATUS IN ( ?, ? )";
		hql += " ORDER BY " + orderBy + " " + orderDir;	
		
		params.add(userId);	
		params.add(dtpDate);
		params.add("0");		// 0: 成功
		params.add("3");		// 3: 取消預約

		log.trace(Sanitizer.logForgingStr(hql));

		return this.pagedQuery(hql, pageNo, pageSize, params);
	}

}
