package fstop.orm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.Query;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.B201Model;
import com.netbank.rest.web.back.model.B707Result;
import com.netbank.util.CodeUtil;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import fstop.orm.po.TXNLOG;
import fstop.util.PropertyUtils;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
import net.sf.ehcache.hibernate.HibernateUtil;

/**
 * TXNLOG DAO
 */
@Slf4j
@Repository
public class TxnLogDao extends LegacyJpaRepository<TXNLOG, String> {
    /**
     * 分頁查詢
     * @param pageNo        頁碼
     * @param pageSize      一頁資料量
     * @param orderBy       排序欄位
     * @param orderDir      排序方向
     * @param startDate     開始日期
     * @param startTime     開始時間
     * @param endDate       結束日期
     * @param endTime       結束時間
     * @param adOpGroup     交易群組
     * @param adExCode      成功或失敗
     * @param loginType     登入類型
	 * @param adUserId		使用者統編
	 * @param adTxAcno      帳號
     * @param adOPName      交易名稱
     * @param adUserIp      使用者登入 IP
	 * @param userName		使用者名稱
     * @return				分頁物件
     * @throws Exception 
     * @throws IllegalAccessException 
     */
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String startDate, String startTime, 
		String endDate, String endTime, String adOpGroup, String adExCode, String loginType, String adUserId, 
		String adTxAcno, String adOPName, String adUserIp, String userName, String adopid) {
	
		log.debug("orderBy="+Sanitizer.logForgingStr(orderBy));
		
        String hql = "FROM TXNLOG WHERE ";
		List<Object> params = new ArrayList<Object>();

		if((!startDate.isEmpty() && !startTime.isEmpty()) && (!endDate.isEmpty() && !endTime.isEmpty())) {
			hql += "(LASTDATE>=? AND LASTDATE<=?) AND ( CONCAT(LASTDATE,LASTTIME)>=? AND CONCAT(LASTDATE,LASTTIME)<=? ) ";
			params.add(startDate);
			params.add(endDate);
			params.add(startDate+startTime);
			params.add(endDate+endTime);
		}  
		if(!startDate.isEmpty() && endTime.isEmpty()) {
			hql += " CONCAT(LASTDATE,LASTTIME)>=? ";
			params.add(startDate+startTime);
		}
		if(startTime.isEmpty() && !endTime.isEmpty()) {
			hql += " CONCAT(LASTDATE,LASTTIME)<=? ";
			params.add(endDate+endTime);
		} 
		if(adOpGroup != null && !adOpGroup.trim().isEmpty()) {
			// 若使用者有點交易類別, 一定可以撈到有交易代號+交易名稱的資料
			hql += "AND ADOPID IN ( SELECT ADOPID FROM NB3SYSOP WHERE ADOPGROUP=? OR ADGPPARENT=? ) ";
			params.add(adOpGroup);
			params.add(adOpGroup);
        } else {
        	// 沒有定在交易選單的 TXNLOG 資料不要取出
        	hql += "AND ADOPID IN ( SELECT ADOPID FROM NB3SYSOP WHERE ADGPPARENT<>? ) ";
        	params.add("");
        }
        if(adExCode != null && !adExCode.trim().isEmpty()) {
			if ( adExCode.equalsIgnoreCase("S") ) {
				hql += "AND ( ADEXCODE = ? OR ADEXCODE = ?  OR ADEXCODE=? OR ADEXCODE=? OR ADEXCODE=? OR ADEXCODE=?) ";
				params.add("");
				params.add("ENRD");
				params.add("0");
				params.add("00");
				params.add("000");
				params.add("0000");
			}
			if ( adExCode.equalsIgnoreCase("F") ) {
				hql += "AND ( ADEXCODE <> ? AND ADEXCODE <> ? AND ADEXCODE<> ?  AND ADEXCODE<> ?  AND ADEXCODE <> ?  AND ADEXCODE <> ? ) ";
				params.add("");
				params.add("ENRD");
				params.add("0");
				params.add("00");
				params.add("000");
				params.add("0000");
			}
        }
        if(loginType != null && !loginType.trim().isEmpty()) {
			hql += "AND LOGINTYPE = ? ";
			params.add(loginType);
		} else {
			hql += "AND ( LOGINTYPE in (?,?,?) ) ";
			params.add("MB");
			params.add("NB");
			params.add("QR");
		}
		if(adUserId !=null && !adUserId.trim().isEmpty()) {
				hql += "AND ADUSERID = ? ";				
				params.add(adUserId);
		}
		if(adTxAcno !=null && !adTxAcno.trim().isEmpty()) {
			hql += "AND ADTXACNO = ? ";
			params.add(adTxAcno);
		}
		if(adOPName != null && !adOPName.trim().isEmpty()) {
			hql += "AND ADOPID IN ( SELECT ADOPID FROM NB3SYSOP WHERE ADOPNAME = ? ) ";
			params.add(adOPName);
        }
		if(adUserIp !=null && !adUserIp.trim().isEmpty()) {
			hql += "AND ADUSERIP = ? ";
			params.add(adUserIp);
		}
		if(userName != null && !userName.trim().isEmpty()) {
			hql += "AND ADUSERID IN ( SELECT DPSUERID FROM TXNUSER WHERE DPUSERNAME = ? )";
			params.add(userName);
		}
		 if(adopid != null && !adopid.trim().isEmpty()) {
				hql += "AND ADOPID = ? ";
				params.add(adopid);
			}
		//按送出及按日期排序，則以日期排序
		if (orderBy.compareToIgnoreCase("lastdate")==0)
		{
			hql += " ORDER BY  CONCAT(LASTDATE,LASTTIME)  "+orderDir;
		}
		else
		{
			hql += " ORDER BY "+ orderBy+" "+orderDir;	
		}
		
		log.debug(Sanitizer.logForgingStr(hql));
		
		return this.pagedQuery_native(hql, pageNo, pageSize, params);
			
		
		//nativeQuery
		//HQLQuery
//		return this.pagedQuery(hql, pageNo, pageSize, params);
	}
	
	public Page findPageDataByLikeA(int pageNo, int pageSize, String orderBy, String orderDir, String startDate, String startTime, 
			String endDate, String endTime, String adOpGroup, String adExCode, String loginType, String adUserId, 
			String adTxAcno, String adOPName, String adUserIp, String userName, String adopid) {
		
			log.debug("orderBy="+Sanitizer.logForgingStr(orderBy));
			
	        String hql = "FROM TXNLOG WHERE ";
			List<Object> params = new ArrayList<Object>();

			if((!startDate.isEmpty() && !startTime.isEmpty()) && (!endDate.isEmpty() && !endTime.isEmpty())) {
				hql += "(LASTDATE>=? AND LASTDATE<=?) AND ( CONCAT(LASTDATE,LASTTIME)>=? AND CONCAT(LASTDATE,LASTTIME)<=? ) ";
				params.add(startDate);
				params.add(endDate);
				params.add(startDate+startTime);
				params.add(endDate+endTime);
			}  
			if(!startDate.isEmpty() && endTime.isEmpty()) {
				hql += " CONCAT(LASTDATE,LASTTIME)>=? ";
				params.add(startDate+startTime);
			}
			if(startTime.isEmpty() && !endTime.isEmpty()) {
				hql += " CONCAT(LASTDATE,LASTTIME)<=? ";
				params.add(endDate+endTime);
			} 
			if(adOpGroup != null && !adOpGroup.trim().isEmpty()) {
				// 若使用者有點交易類別, 一定可以撈到有交易代號+交易名稱的資料
				hql += "AND ADOPID IN ( SELECT ADOPID FROM NB3SYSOP WHERE ADOPGROUP=? OR ADGPPARENT=? ) ";
				params.add(adOpGroup);
				params.add(adOpGroup);
	        } else {
	        	// 沒有定在交易選單的 TXNLOG 資料不要取出
	        	hql += "AND ADOPID IN ( SELECT ADOPID FROM NB3SYSOP WHERE ADGPPARENT<>? ) ";
	        	params.add("");
	        }
	        if(adExCode != null && !adExCode.trim().isEmpty()) {
				if ( adExCode.equalsIgnoreCase("S") ) {
					hql += "AND ( ADEXCODE = ? OR ADEXCODE = ?  OR ADEXCODE=? OR ADEXCODE=? OR ADEXCODE=? OR ADEXCODE=?) ";
					params.add("");
					params.add("ENRD");
					params.add("0");
					params.add("00");
					params.add("000");
					params.add("0000");
				}
				if ( adExCode.equalsIgnoreCase("F") ) {
					hql += "AND ( ADEXCODE <> ? AND ADEXCODE <> ? AND ADEXCODE<> ?  AND ADEXCODE<> ?  AND ADEXCODE <> ?  AND ADEXCODE <> ? ) ";
					params.add("");
					params.add("ENRD");
					params.add("0");
					params.add("00");
					params.add("000");
					params.add("0000");
				}
	        }
	        if(loginType != null && !loginType.trim().isEmpty()) {
				hql += "AND LOGINTYPE = ? ";
				params.add(loginType);
			} else {
				hql += "AND ( LOGINTYPE in (?,?,?) ) ";
				params.add("MB");
				params.add("NB");
				params.add("QR");
			}
			if(adUserId !=null && !adUserId.trim().isEmpty()) {
				hql += "AND ADUSERID like ? ";
				params.add("%"+adUserId+"%");
			}
			if(adTxAcno !=null && !adTxAcno.trim().isEmpty()) {
				hql += "AND ADTXACNO like ? ";
				params.add("%"+adTxAcno+"%");
			}
			if(adOPName != null && !adOPName.trim().isEmpty()) {
				hql += "AND ADOPID IN ( SELECT ADOPID FROM NB3SYSOP WHERE ADOPNAME like ? ) ";
				params.add("%"+adOPName+"%");
	        }
			if(adUserIp !=null && !adUserIp.trim().isEmpty()) {
				hql += "AND ADUSERIP like ? ";
				params.add("%"+adUserIp+"%");
			}
			if(userName != null && !userName.trim().isEmpty()) {
				hql += "AND ADUSERID IN ( SELECT DPSUERID FROM TXNUSER WHERE DPUSERNAME like ? )";
				params.add("%"+userName+"%");
			}
			 if(adopid != null && !adopid.trim().isEmpty()) {
					hql += "AND ADOPID = ? ";
					params.add(adopid);
				}
			//TODO
			 String order = "";
			//按送出及按日期排序，則以日期排序
			if (orderBy.compareToIgnoreCase("lastdate")==0)
			{
				order += " ORDER BY  CONCAT(LASTDATE,LASTTIME)  "+orderDir;
			}
			else
			{
				order += " ORDER BY "+ orderBy+" "+orderDir;	
			}
			
			log.debug(Sanitizer.logForgingStr(hql));
			
			String column= "ADTXNO,ADUSERID,ADOPID,FGTXWAY,ADTXACNO,ADTXAMT,ADCURRENCY,ADSVBH,ADAGREEF,ADREQTYPE,ADUSERIP,ADTOTACONTENT,ADEXCODE,ADGUID,ADTXID,LASTDATE,LASTTIME,PSTIMEDIFF,LOGINTYPE";
			
			//return this.pagedQuery_native_column(hql, order, pageNo, pageSize, params, column);
			return B201Query(hql, order, pageNo, pageSize, column, params);
			
			//nativeQuery
			//HQLQuery
//			return this.pagedQuery(hql, pageNo, pageSize, params);
		}
	
	private Page B201Query(String hql, String order, int pageNo, int pageSize, String column, Object...params) {
		Page page = null;
		List<B201Model> list = null;
		//Count查詢
        String countQueryString = " select count (*) " + hql;
        List countlist =  getSession().createSQLQuery(countQueryString, params).getResultList();
        long totalCount = Long.parseLong(new String().valueOf(countlist.get(0)));
        log.info("countQueryString>>>{}",countQueryString);
        log.info("totalCount>>>{}",totalCount);
        if (totalCount < 1) return new Page();
        //實際查詢返回分頁對像
        int startIndex = Page.getStartOfPage(pageNo, pageSize);
        
        //TODO test log
        log.info("Query Start...");
        log.info("sqlColumn>>>>{}",column);
        countQueryString = "select " +column+" "+ hql + " " + order;
        log.debug("\r\n[SQL:]\r\n" + countQueryString);
        Query query = getSession().createSQLQuery(countQueryString);
		
        int idx=0;
        for (int i = 0; i < params.length; i++) {
            if ( params[i] instanceof ArrayList ) {
                // 呼叫端傳入的是一個 List，所以 Object... 只會有一個，型別是 ArrayList
                ArrayList subValues = (ArrayList)params[i];
                for (int j=0; j<subValues.size(); j++ ) {
                    query.setParameter(idx++, subValues.get(j));
                }
            } else {
                // 呼叫端傳入的是一個個參數，所以 Object... 會是 int, string, float...
                query.setParameter(idx++, params[i]);
            }
        }
        List<B201Model> rsList = new ArrayList<B201Model>();
        List qresult = query.setFirstResult(startIndex).setMaxResults(pageSize).getResultList();
        Iterator it = qresult.iterator();

		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			B201Model model = new B201Model();
			model.setADTXNO(o[0].toString());
			model.setADUSERID(o[1].toString());
			model.setADOPID(o[2].toString());
			model.setFGTXWAY(o[3].toString());
			model.setADTXACNO(o[4].toString());
			model.setADTXAMT(o[5].toString());
			model.setADCURRENCY(o[6].toString());
			model.setADSVBH(o[7].toString());
			model.setADAGREEF(o[8].toString());
			model.setADREQTYPE(o[9].toString());
			model.setADUSERIP(o[10].toString());
			model.setADTOTACONTENT(o[11].toString());
			model.setADEXCODE(o[12].toString());
			model.setADGUID(o[13].toString());
			model.setADTXID(o[14].toString());
			model.setLASTDATE(o[15].toString());
			model.setLASTTIME(o[16].toString());
			model.setPSTIMEDIFF(o[17].toString());
			model.setLOGINTYPE(o[18].toString());
			rsList.add(model);
		}
		
		try {			
			String testSQL = "SELECT ADTXNO FROM TXNLOG WHERE JSON_VAL(ADCONTENT, 'PAYREMIT','s:30')='TWD'";
			Query testQuery = getSession().createSQLQuery(testSQL);
			List testList = testQuery.setFirstResult(1).setMaxResults(10).getResultList();
			log.info("testList.size>>>{}",testList.size());
			Iterator testIt = qresult.iterator();
			
			while (testIt.hasNext()) {
				Object[] o = (Object[]) it.next();
				String adtxno = o[0].toString();
				String result = o[1].toString();
				log.info("adtxno>>>{}",adtxno);
				log.info("result>>>{}",result);
			}
		}catch(Exception e) {
			log.debug("test1 false");
		}
		
		try {
			String testSQL = "SELECT * FROM TXNLOG WHERE JSON_VAL(ADCONTENT, 'PAYREMIT','s:30')='TWD'";
			Query testQuery2 = getSession().createSQLQuery(testSQL);
			List testList = testQuery2.setFirstResult(1).setMaxResults(10).getResultList();
			log.info("testList2.size>>>{}",testList.size());
			Iterator testIt = qresult.iterator();
			
			while (testIt.hasNext()) {
				Object[] o = (Object[]) it.next();
				String adtxno = o[0].toString();
				String result = o[1].toString();
				log.info("adtxno2>>>{}",adtxno);
				log.info("result2>>>{}",result);
			}			
		}catch(Exception e) {
			log.debug("test2 false");
		}
		return new Page(startIndex, totalCount, pageSize, rsList);
	}

	public List<String> B201Query(String COLUMNS, String CONDITION) {

        //TODO test log
        log.info("Query Start...");
        log.info("sqlColumn>>>>{}",COLUMNS);
        String countQueryString = "SELECT " +COLUMNS+" FROM TXNLOG T WHERE "+ CONDITION;
        log.debug("\r\n[SQL:]\r\n" + countQueryString);
        Query query = getSession().createSQLQuery(countQueryString);
	
        List<String> rsList = new ArrayList<String>();
        List qresult = query.getResultList();
        Iterator it = qresult.iterator();
        
        log.debug("qresult="+new String().valueOf(qresult));
        
		while (it.hasNext()) {
			
			Object ob = it.next();
			StringBuilder sb = new StringBuilder();
			if(ob instanceof Object[]) {				
				Object[] o = (Object[])ob;
				for(Object obj:o) {
					sb.append(new String().valueOf(obj));
				}
			}else {
				sb.append(new String().valueOf(ob));
			}
			
			rsList.add(sb.toString());
		}
		
		return rsList;
	}
	
	/**
	 * 依日期與登入管道統計交易筆數
	 * @param startYYYYMM	資料年月
	 * @return
	 */
	public List<B707Result> findTxnLogGroupByDateLogintype(String YYYYMM) throws Exception {
		try {
			PropertyUtils utility = new PropertyUtils();
			String sql = utility.getResourceAsString("reports/B707.sql");

			Query query = getSession().createSQLQuery(sql);
			query.setParameter(0, YYYYMM+"00");
			query.setParameter(1, YYYYMM+"99");
			query.setParameter(2, YYYYMM+"00");
			query.setParameter(3, YYYYMM+"99");		// 雖然沒有 2019xx99 的日期，但以字串的比較來說是允許的，不用刻意取該用的真正最大日
			log.debug("\r\n[SQL:]\r\n" + sql);

			Map<String, B707Result> results = new TreeMap<String, B707Result>();
			//20191119-Danny-CGI Stored XSS\路徑 2:
			List qresult = query.getResultList();        
			qresult = Sanitizer.validList(qresult);
	   

			/**
			 * qresult 查詢結果：lastdate，logintype，count
			 * 20190702	MB	2
			 * 20190705	NB	58
			 */
			Iterator it = qresult.iterator();
			while (it.hasNext()) {
				Object[] o = (Object[]) it.next();
				String lastDate = o[0].toString();
				String loginType = o[1].toString();
				long count = Long.parseLong(o[2].toString());

				B707Result result;

				if ( results.containsKey(lastDate)) {
					result = (B707Result)results.get(lastDate);
					results.remove(lastDate);
				} else {
					result = new B707Result();
					result.setLastDate(lastDate);
				}
				if( loginType.compareToIgnoreCase("NB")== 0 ) {
					result.setNbCount(count);
				} else if ( loginType.compareToIgnoreCase("MB") == 0) {
					result.setMbCount(count);
				}
				results.put(lastDate, result);
			}

			//20191118-Danny-Stored XSS\路徑 4:
			ArrayList<B707Result> newResult = new ArrayList<B707Result>(results.values());
			List<B707Result> newList = (List<B707Result>)(Object)Sanitizer.validList(newResult);
			newResult.clear();
			newResult.addAll(newList);
			return newResult;
			
			//return new ArrayList<B707Result>(results.values());

		} catch (Exception e) {
			log.error("findTxnLogGroupByDateLogintype error", e);
			throw e;
		}
	}
}