package fstop.orm.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import fstop.orm.po.SYSBATCH;

@Repository
@Transactional
public class SysBatchDao  extends LegacyJpaRepository<SYSBATCH, String> {
	public Page findAll(int pageNo, int pageSize, String orderBy, String orderDir) {
        String hql = "FROM SYSBATCH ";
        hql += "ORDER BY "+orderBy+" "+orderDir;
        
        return this.pagedQuery(hql, pageNo, pageSize);
    }
}
