package fstop.orm.dao;

import java.util.List;

import javax.transaction.Transactional;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import org.springframework.stereotype.Repository;
import fstop.orm.po.ADMFLOWCURRENT;
import fstop.orm.po.ADMFLOWCURRENTIDENTITY;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>流程待辦 DAO</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmFlowCurrentDao extends LegacyJpaRepository<ADMFLOWCURRENT, ADMFLOWCURRENTIDENTITY> {
    /**
     * 依案號及分支號取得案件
     *
     * @param caseSN a {@link java.lang.String} object.
     * @param bid a int.
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings("unchecked")
    public List<ADMFLOWCURRENT> findByCaseSNBid(String caseSN, int bid) {
        String hql = "FROM ADMFLOWCURRENT WHERE CASESN=? AND BID=?";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[2];
        params[0] = caseSN;
        params[1] = bid;

        return (List<ADMFLOWCURRENT>)find(hql, params);
    }

    /**
     * <p>findByCaseSN.</p>
     *
     * @param caseSN a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings("unchecked")
    public List<ADMFLOWCURRENT> findByCaseSN(String caseSN) {
        String hql = "FROM ADMFLOWCURRENT WHERE CASESN=?";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[1];
        params[0] = caseSN;

        return (List<ADMFLOWCURRENT>)find(hql, params);
    }

    /**
     * <p>findByRole.</p>
     *
     * @param oId a {@link java.lang.String} object.
     * @param roles an array of {@link java.lang.String} objects.
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings("unchecked")
    public List<ADMFLOWCURRENT> findByRole(String oId, String[] roles) {
        String hql = "FROM ADMFLOWCURRENT WHERE TOOID=? And ( ";
        String[] tmps = new String[roles.length];

        for ( int i=0; i<tmps.length; i++ ) {
            tmps[i]=" TOUID like ? ";
        }
        
        hql += String.join("OR", tmps) + " )";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[1+roles.length];
        params[0] = oId;
        for ( int i=0; i<roles.length; i++ ) {
            params[i+1] = "%"+roles[i]+"%";
        }

        return (List<ADMFLOWCURRENT>)find(hql, params);
    }
    
    @SuppressWarnings("unchecked")
    public List<ADMFLOWCURRENT> findByFlowId(String oId, String flowId) {
        String hql = "FROM ADMFLOWCURRENT WHERE TOOID=? And FlowId=?";
        log.trace(Sanitizer.logForgingStr(hql));

        return (List<ADMFLOWCURRENT>)find(hql, oId, flowId);
    }
}
