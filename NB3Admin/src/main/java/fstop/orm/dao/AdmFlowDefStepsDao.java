package fstop.orm.dao;

import java.util.List;

import javax.transaction.Transactional;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import org.springframework.stereotype.Repository;
import fstop.util.Sanitizer;
import fstop.orm.po.ADMFLOWDEFSTEP;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>流程定義步驟 DAO</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmFlowDefStepsDao extends LegacyJpaRepository<ADMFLOWDEFSTEP, String> {
    /**
     * 依流程代碼取得流程步驟
     *
     * @param flowId a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings("unchecked")
    public List<ADMFLOWDEFSTEP> findSteps(String flowId) {        
        log.debug("flowId={}", Sanitizer.logForgingStr(flowId));

        return find("From ADMFLOWDEFSTEP Where FLOWID=?", flowId);
    }
}
