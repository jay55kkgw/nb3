package fstop.orm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import fstop.orm.po.TRNS_NNB_CUSDATA;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class Trns_NNB_CusdataDao extends LegacyJpaRepository<TRNS_NNB_CUSDATA, String> {

	/**
	 * 查詢失敗的資料(除去PK以外的欄位串起來是欄位數0 = (皆成功)的不查)
	 * 
	 * */
	public List<TRNS_NNB_CUSDATA> getFaildata() {
		String hql = "FROM TRNS_NNB_CUSDATA  WHERE (TXNUSER || TXNTWRECORD || TXNTWSCHPAY || TXNFXRECORD || TXNFXSCHPAY || TXNGDRECORD || TXNTRACCSET || ADMMAILLOG || TXNADDRESSBOOK || TXNCUSINVATTRIB || TXNCUSINVATTRHIST || NB3USER || TXNPHONETOKEN) <> '0000000000000'";
		List<TRNS_NNB_CUSDATA> result = find(hql);
		return result;
	}	
	
	public Page getFaildata(int pageNo, int pageSize, String orderBy, String orderDir) {
		String hql = "FROM TRNS_NNB_CUSDATA  WHERE (TXNUSER || TXNTWRECORD || TXNTWSCHPAY || TXNFXRECORD || TXNFXSCHPAY || TXNGDRECORD || TXNTRACCSET || ADMMAILLOG || TXNADDRESSBOOK || TXNCUSINVATTRIB || TXNCUSINVATTRHIST || NB3USER || TXNPHONETOKEN) <> '0000000000000' ";
		hql += " ORDER BY "+orderBy +" "+orderDir;
		return this.pagedQuery(hql, pageNo, pageSize);
	}	
}
