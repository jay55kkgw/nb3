package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMREMITMENU;
import fstop.util.Sanitizer;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>AdmRemitMenuDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmRemitMenuDao extends LegacyJpaRepository<ADMREMITMENU, Long> {
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String remitType, String lKind, String mKind, String remitId) {
		String hql = "FROM ADMREMITMENU WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if(remitType != null && !remitType.trim().isEmpty()) {
			hql += "AND ADRMTTYPE=? ";
			params.add(remitType);
		}
		if(lKind != null && !lKind.trim().isEmpty()) {
			hql += "AND ADLKINDID=? ";
			params.add(lKind);
		}
		if(mKind != null && !mKind.trim().isEmpty()) {
			hql += "AND ADMKINDID=? ";
			params.add(mKind);
		}
		if(remitId != null && !remitId.trim().isEmpty()) {
			hql += "AND ADRMTID like ? ";
			params.add("%"+remitId+"%");
		}
		hql += " ORDER BY "+ orderBy+" "+orderDir;

		log.debug(Sanitizer.logForgingStr(hql));

		return this.pagedQuery(hql, pageNo, pageSize, params);
	}

	/**
	 * 依據傳入參數取得 AdmRemitMenu table符合之資料, 若全部傳入值皆為 ""時,表帶出所有大類(ADLKINDID)資料
	 *
	 * @param ADRMTTYPE a {@link java.lang.String} object.
	 * @param ADLKINDID a {@link java.lang.String} object.
	 * @param ADMKINDID a {@link java.lang.String} object.
	 * @param ADRMTID a {@link java.lang.String} object.
	 * @param custype a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public List<ADMREMITMENU> getRemitMenu(String ADRMTTYPE, String ADLKINDID, String ADMKINDID,
			String ADRMTID, String custype) {

		StringBuffer qp = new StringBuffer();
		List<String> pp = new ArrayList();
		if(StrUtils.isNotEmpty(ADLKINDID)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADLKINDID = ? ");
			pp.add(ADLKINDID);
		}
		
		if(StrUtils.isNotEmpty(ADMKINDID)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADMKINDID = ? ");
			pp.add(ADMKINDID);
		}
		else {
			qp.append(" and ");
			qp.append(" ADMKINDID <> '00' ");			
		}
		
		if(StrUtils.isNotEmpty(ADRMTID)) {
			if(qp.length() > 0)
				qp.append(" and ");
			
			if ("*".equals(ADRMTID))
				qp.append(" ADRMTID <> '' ");
			else {
				qp.append(" ADRMTID = ? ");			
				pp.add(ADRMTID);
			}
		}
		else {
			qp.append(" and ");
			qp.append(" ADRMTID = '' ");			
		}
		
		if(StrUtils.isNotEmpty(ADRMTTYPE)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADRMTTYPE = ? ");
			pp.add(ADRMTTYPE);			
		}
		
		if(StrUtils.isNotEmpty(custype)) {
			if(qp.length() > 0)
				qp.append(" and ");
			
			if (custype.equals("1") || custype.equals("2"))
				custype = "1";
			else if (custype.equals("3"))
				custype = "2";
			else
				custype = "3";
				
			qp.append(" ADRMTEETYPE" + custype + " = 'Y' ");
		}
		
		String[] params = new String[pp.size()];
		pp.toArray(params);
		
		log.debug("SQL : FROM ADMREMITMENU WHERE " + qp.toString());
		
		return find("FROM ADMREMITMENU WHERE " + qp.toString(), params);

	}

	/**
	 * 依據傳入參數取得 AdmRemitMenu table 符合之資料
	 * (管理端選單維護使用)
	 *
	 * @param ADRMTTYPE a {@link java.lang.String} object.
	 * @param ADLKINDID a {@link java.lang.String} object.
	 * @param ADMKINDID a {@link java.lang.String} object.
	 * @param ADRMTID a {@link java.lang.String} object.
	 * @param custype a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public List<ADMREMITMENU> getAdminRemitMenu(String ADRMTTYPE, 
												String ADLKINDID, 
												String ADMKINDID,
												String ADRMTID, 
												String custype) {

		StringBuffer qp = new StringBuffer();
		List<String> pp = new ArrayList();
		if(StrUtils.isNotEmpty(ADLKINDID)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADLKINDID = ? ");
			pp.add(ADLKINDID);
		}
		
		if(StrUtils.isNotEmpty(ADMKINDID)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" (ADMKINDID = '00' or ADMKINDID = ?) ");
			pp.add(ADMKINDID);
		}		
		
		if(StrUtils.isNotEmpty(ADRMTTYPE)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADRMTTYPE = ? ");
			pp.add(ADRMTTYPE);			
		}
		
		if(StrUtils.isNotEmpty(custype)) {
			if(qp.length() > 0)
				qp.append(" and ");
			
			if (custype.equals("1") || custype.equals("2"))
				custype = "1";
			else if (custype.equals("3"))
				custype = "2";
			else
				custype = "3";
			
			qp.append(" ADRMTEETYPE" + custype + " = 'Y' ");
		}
		
		String[] params = new String[pp.size()];
		pp.toArray(params);
		
		log.debug("SQL : FROM ADMREMITMENU WHERE " + qp.toString());
		
		return find("FROM ADMREMITMENU WHERE " + qp.toString(), params);
	}
		
	/**
	 * <p>getAllAdlKind.</p>
	 *
	 * @param adrmttype a {@link java.lang.String} object.
	 * @return a {@link fstop.model.Rows} object.
	 */
	// public Rows getAllAdlKind(String adrmttype) {
	// 	Query query = getSession().createQuery("SELECT DISTINCT ADLKINDID, ADMKINDID, ADRMTITEM, ADRMTDESC " +
	// 			" FROM ADMREMITMENU WHERE ADRMTTYPE = ? and ADRMTID = '' and ADMKINDID = '00' ORDER BY ADLKINDID ");
	// 	query.setParameter(0, adrmttype);
	// 	List lresult = query.getResultList();
		
	// 	Rows result = new Rows();
		
	// 	for(Object o : lresult) {
	// 		Object[] or = (Object[])o;
	// 		Map<String, String> rowdata = new HashMap();
	// 		rowdata.put("ADLKINDID", or[0].toString());
	// 		rowdata.put("ADMKINDID", or[1].toString());
	// 		rowdata.put("ADRMTITEM", or[2].toString());
	// 		rowdata.put("ADRMTDESC", or[3].toString());
	// 		logger.debug("Object type = " + o.getClass().getSimpleName());
	// 		Row row = new Row(rowdata);
	// 		result.addRow(row);
	// 	}
		
	// 	return result;

	// }
	
	/**
	 * 找到與資料庫的同一筆資料
	 *
	 * @param po a {@link fstop.orm.po.ADMREMITMENU} object.
	 * @return a {@link java.util.List} object.
	 */
	public List<ADMREMITMENU> findAttrsEquals(ADMREMITMENU po) {
		String[] fields = {
				"ADRMTID",
				"ADRMTITEM",
				"ADRMTDESC",
				"ADCHKMK",
				"ADRMTEETYPE1",
				"ADRMTEETYPE2",
				"ADRMTEETYPE3"
			};
		String[] params = {
				po.getADRMTID(),
				po.getADRMTITEM(),
				po.getADRMTDESC(),
				po.getADCHKMK(),
				po.getADRMTEETYPE1(),
				po.getADRMTEETYPE2(),
				po.getADRMTEETYPE3()
			}; 
		String where = StrUtils.implode(" = ? and ", fields);
		return this.find("FROM ADMREMITMENU WHERE " + where + " 1=1 ", params);
	}

	/**
	 * 就查詢全部資料, 依ADLKINDID 、ADMKINDID、 ADRMTID 順序做排序
	 *
	 * @param ADRMTTYPE a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public List<ADMREMITMENU> findAllAndSort(String ADRMTTYPE) {
		return find("FROM ADMREMITMENU WHERE ADRMTTYPE='"+ ADRMTTYPE +"' ORDER BY ADRMTTYPE, ADLKINDID, ADMKINDID,  ADRMTID ");
	}

	/**
	 * <p>findUniqueByRmtid.</p>
	 *
	 * @param rmttype a {@link java.lang.String} object.
	 * @param rmtid a {@link java.lang.String} object.
	 * @return a {@link fstop.orm.po.ADMREMITMENU} object.
	 */
	public ADMREMITMENU findUniqueByRmtid(String rmttype, String rmtid) {
		try {
			return (ADMREMITMENU)find("FROM ADMREMITMENU WHERE ADRMTTYPE = ? and ADRMTID = ? ", rmttype, rmtid)
						.get(0);
		}
		catch(IndexOutOfBoundsException e) {}
		
		return null;
	}
}
