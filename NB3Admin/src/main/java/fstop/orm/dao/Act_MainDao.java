package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ACT_MAIN;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>行銷主檔 DAO</p>
 *
 * @author jinhanhuang
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class Act_MainDao extends LegacyJpaRepository<ACT_MAIN, String> {
    
    /**
     * 分頁查詢
     * 
     * @param pageNo 頁碼，從 1 編起
     * @param pageSize 一頁幾筆
     * @param orderBy 依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param ACT_TYPE 行銷活動代號
     * @param ACTDATE_FROM 行銷起日
     * @param ACTDATE_TO 行銷迄日
     * @return
     */
	public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ACT_TYPE, String MAXCOUNT, String ACTDATE_FROM, String ACTDATE_TO){
		String hql = "FROM ACT_MAIN WHERE 1=1 ";
		try {
			List<String> params = new ArrayList<String>();
			
			if(ACT_TYPE != null && !ACT_TYPE.trim().isEmpty()) {
				hql += "AND ACT_TYPE LIKE ?";
				params.add("%"+ACT_TYPE+"%");
			}
			
			if(MAXCOUNT != null && !MAXCOUNT.trim().isEmpty()) {
				hql += "AND MAXCOUNT=? ";
				params.add(MAXCOUNT);
			}
			
			if(ACTDATE_FROM != null && !ACTDATE_FROM.trim().isEmpty()) {
				hql += "AND ACTDATE_FROM>=? ";
				params.add(ACTDATE_FROM);
			}
			
			if(ACTDATE_TO != null && !ACTDATE_TO.trim().isEmpty()) {
				hql += "AND ACTDATE_TO<=? ";
				params.add(ACTDATE_TO);
			}
			
			hql += " ORDER BY "+ orderBy+" "+orderDir;
			
			log.trace(Sanitizer.logForgingStr(hql));
			
			switch ( params.size() ) {
			case 0:
				return this.pagedQuery(hql, pageNo, pageSize);
				
			case 1:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0));
				
			case 2:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));
				
			case 3:
				return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2));
				
			default:
				return this.pagedQuery(hql, pageNo, pageSize);
			}
			
		}catch(Exception e){
			log.debug("SQL error>>>",e,e.getMessage());
			return this.pagedQuery(hql, pageNo, pageSize);
		}
	}

    public ACT_MAIN getACT_TYPE(String ACT_TYPE){
        List<ACT_MAIN> result = new ArrayList<ACT_MAIN>();
        String hql = "FROM ACT_MAIN WHERE 1=1 AND ACT_TYPE=?";
        log.trace(Sanitizer.logForgingStr(hql));

        Object[] params = new Object[1];
        params[0] = ACT_TYPE;

        result = find(hql, params);
        if ( result.size()>0 )
            return result.get(0);
        else
            return null;
    }
}
