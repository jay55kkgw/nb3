package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.util.StrUtils;

import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.QUICKLOGINMAPPING;
import fstop.orm.po.QUICKLOGINMAPPING_PK;
import lombok.extern.slf4j.Slf4j;

@Transactional
@Repository
@Slf4j
public class QuickLoginMappingDao extends LegacyJpaRepository<QUICKLOGINMAPPING,QUICKLOGINMAPPING_PK>{

	
	public Page findQuickLogin(String custid, String IdGateid, int pageNo, int pageSize)
	{
		List<Object> params = new ArrayList<Object>();
		StringBuilder hql=new StringBuilder();

		hql.append("FROM QUICKLOGINMAPPING WHERE 1=1 ");
		if(StrUtils.isNotEmpty(custid))
		{
			hql.append("AND CUSIDN  = ? "); 
			params.add(custid);
		}

		if(StrUtils.isNotEmpty(IdGateid))
		{
			hql.append("AND IDGATEID  = ? "); 
			params.add(IdGateid);
		}
		
		hql.append("ORDER BY DEVICESTATUS, REGDATE, REGTIME ");
		
		return this.pagedQuery(hql.toString(), pageNo, pageSize, params);
	}
}