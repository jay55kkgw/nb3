package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.QueryAdapter;

import fstop.orm.po.ADMMSGCODE;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>AdmMsgCodeDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
public class AdmMsgCodeDao extends LegacyJpaRepository<ADMMSGCODE, String> {
	
    /**
     * <p>getErrorCodeMsg.</p>
     *
     * @param err_code a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @Transactional(readOnly = true)
    public String getErrorCodeMsg(String err_code) {
        List qresult = find("SELECT ADMSGOUT FROM ADMMSGCODE WHERE ADMCODE=?", err_code);

        String result = "";
        if (qresult.size() > 0) {
            result = (String) qresult.iterator().next();
        }
        return result;
    }

    /**
     * <p>getErrorCodeMsgIn.</p>
     *
     * @param err_code a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @Transactional(readOnly = true)
    public String getErrorCodeMsgIn(String err_code) {
        List qresult = find("SELECT ADMSGIN FROM ADMMSGCODE WHERE ADMCODE=?", err_code);

        String result = "";
        if (qresult.size() > 0) {
            result = (String) qresult.iterator().next();
        }
        return result;
    }

        /**
     * <p>getErrorCodeMsg.</p>
     *
     * @param err_code a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @Transactional(readOnly = true)
    public String getErrorCodeMsgSign(String err_code) {
        List qresult = find("SELECT ADMSGIN FROM ADMMSGCODE WHERE ADMCODE=?", err_code);

        String result = "";
        if (qresult.size() > 0) {
            result = (String) qresult.iterator().next();
        }
        return result;
    }

    /**
     * <p>findMsgCodeLike.</p>
     *
     * @param msgCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    @Transactional(readOnly = true)
    public List<ADMMSGCODE> findMsgCodeLike(String msgCode) {
        List<ADMMSGCODE> qresult = find("FROM ADMMSGCODE WHERE ADMCODE like ? ", "%" + msgCode + "%");

        return qresult;
    }

    /**
     * <p>isError.</p>
     *
     * @param topmsg a {@link java.lang.String} object.
     * @return a {@link fstop.orm.po.ADMMSGCODE} object.
     */
    @Transactional(readOnly = true)
    public ADMMSGCODE isError(String topmsg) {
        topmsg = (String)Sanitizer.sanitize(topmsg);
        try {
            Query q = getQuery("FROM ADMMSGCODE WHERE ADMCODE = '" + topmsg + "'");

            List qresult = q.getResultList();
            ADMMSGCODE result = null;
            if (qresult.size() > 0) {
                result = (ADMMSGCODE) qresult.get(0);
            }
            Sanitizer.sanitize4Class(topmsg);
            return result;
        } catch (Exception e) {
            log.error("取得 ADMMSGCODE 有誤 .", e);
        }
        return null;
    }

    /**
     * <p>findAll.</p>
     *
     * @return a {@link java.util.List} object.
     */
    @Transactional(readOnly = true)
    public List<ADMMSGCODE> findAll() {
        List<ADMMSGCODE> qresult = find("FROM ADMMSGCODE ORDER BY ADMCODE ");

        return qresult;
    }

    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo   第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy  依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param ADMCODE  應用系統代碼
     * @return 分頁物件
     *
     *         20190603 DannyChou ADD 分頁查詢
     */
    public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String ADMCODE, String ADMSGOUT) {
        String hql = "FROM ADMMSGCODE WHERE 1=1 ";
        List<Object> params = new ArrayList<Object>();

        if (ADMCODE != null && !ADMCODE.trim().isEmpty()) {
            hql += "AND ADMCODE LIKE ? ";
            params.add(ADMCODE + "%");
        }
        
        if (ADMSGOUT != null && !ADMSGOUT.trim().isEmpty()) {
            hql += "AND ADMSGOUT LIKE ? ";
            params.add("%"+ ADMSGOUT + "%");
        }
        
        hql += " ORDER BY " + orderBy + " " + orderDir;

        log.trace(Sanitizer.logForgingStr(hql));

        switch (params.size()) {
        case 0:
            return this.pagedQuery(hql, pageNo, pageSize);

        case 1:
            return this.pagedQuery(hql, pageNo, pageSize, params.get(0));

        case 2:
            return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));

        default:
            return this.pagedQuery(hql, pageNo, pageSize);
        }
    }
    
    public List<ADMMSGCODE> findfindFxAllowTerminate(String[] pendingArr){
    	
    	StringBuffer sqlBuffer = new StringBuffer();
    	sqlBuffer.append("FROM ADMMSGCODE WHERE ADMCODE IN(");
    	    	
    	for(int i=0 ; i<pendingArr.length ; i++) {
    		if(i == pendingArr.length-1) {
    			sqlBuffer.append("'"+pendingArr[i]+"')");
    		}else {
    			sqlBuffer.append("'"+pendingArr[i]+"',");
    		}
    	}
    	    	
    	return find(sqlBuffer.toString());
    }
}
