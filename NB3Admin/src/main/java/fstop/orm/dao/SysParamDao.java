package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.SYSPARAM;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

/**
 * <p>SysParamDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class SysParamDao extends LegacyJpaRepository<SYSPARAM, Integer> {

	protected Logger logger = Logger.getLogger(getClass());

	/**
	 * <p>getAll.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<SYSPARAM> getAll() {
		return find("FROM SYSPARAM");
	}
	
	/**
	 * <p>getSysParam.</p>
	 *
	 * @param paramName a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String getSysParam(String paramName) {
		List<SYSPARAM> result = find("FROM SYSPARAM WHERE ADPARAMNAME = ?", paramName);
		if (result.size()>0)
			return result.get(0).getADPARAMVALUE();
		else
			return null;
	}
	
	public SYSPARAM getBySysParam(String paramName) {
		List<SYSPARAM> result = find("FROM SYSPARAM WHERE ADPARAMNAME = ?", paramName);
		if (result.size()>0)
			return result.get(0);
		else
			return null;
	}
}
