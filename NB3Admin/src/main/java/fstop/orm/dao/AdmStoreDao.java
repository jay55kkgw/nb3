package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMADSTMP;
import fstop.orm.po.ADMSTORE;


/**
 * <p>優惠特店檔 DAO</p>
 *
 * @author 
 * @version V1.0
 */
@Repository
@Transactional
public class AdmStoreDao extends LegacyJpaRepository<ADMSTORE, String> {

	
}
