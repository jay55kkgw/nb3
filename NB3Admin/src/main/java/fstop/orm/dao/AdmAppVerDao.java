package fstop.orm.dao;

import com.google.gson.Gson;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.rest.web.back.model.BM2000ViewModel;

import fstop.orm.po.ADMAPPVER;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>AdmAppVerDao class.</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
public class AdmAppVerDao extends LegacyJpaRepository<ADMAPPVER, Integer> {


    /**
     * <p>findAll.</p>
     *
     * @return a {@link java.util.List} object.
     */
    // @Transactional(readOnly = true)
    // public List<ADMAPPVER> findAll() {
    //     List<ADMAPPVER> qresult = find("FROM ADMAPPVER ORDER BY APPID ");

    //     return qresult;
    // }
    /**
     * <p>
     * getAll.
     * </p>
     *
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings({ "unchecked" })
    public List<ADMAPPVER> findAll() {
        List<ADMAPPVER> result = new ArrayList<ADMAPPVER>();

        String hql = "FROM ADMAPPVER ORDER BY APPID";
        log.trace(Sanitizer.logForgingStr(hql));

        result = find(hql);
        return result;
    }
    /**
     * 以查詢條件做分頁查詢
     *
     * @param pageNo   第幾頁
     * @param pageSize 一頁幾筆
     * @param orderBy  依什麼欄位排序
     * @param orderDir 升冪/降冪
     * @param jsonModel  應用系統代碼
     * @return 分頁物件
     *
     *         20190603 DannyChou ADD 分頁查詢
     */
    public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String jsonModel) {
        BM2000ViewModel model =new Gson().fromJson(jsonModel, BM2000ViewModel.class);
        
        String hql = "FROM ADMAPPVER WHERE 1=1 ";
        List<Object> params = new ArrayList<Object>();

        if (model.getAPPOS() != null && !model.getAPPOS().trim().isEmpty()) {
            hql += "AND APPOS = ? ";
            params.add(model.getAPPOS());
        }
        
        if (model.getAPPMODELNO() != null && !model.getAPPMODELNO().trim().isEmpty()) {
            hql += "AND APPMODELNO LIKE ? ";
            params.add("%" + model.getAPPMODELNO() + "%");
        	
        }
        if (model.getVERNO() != null && !model.getVERNO().trim().isEmpty()) {
            hql += "AND VERNO LIKE ? ";
            params.add("%" + model.getVERNO() + "%");
        }
        if (model.getVERDATE() != null && !model.getVERDATE().trim().isEmpty()) {
            hql += "AND VERDATE LIKE ? ";
            params.add("%" + model.getVERDATE() + "%");
        }
        if (model.getVERSTATUS() != null && !model.getVERSTATUS().trim().isEmpty()) {
            hql += "AND VERSTATUS LIKE ? ";
            params.add("%" + model.getVERSTATUS() + "%");
        }
        if (model.getVERMSG() != null && !model.getVERMSG().trim().isEmpty()) {
            hql += "AND VERMSG LIKE ? ";
            params.add("%" + model.getVERMSG() + "%");
        }
        if (model.getVERURL() != null && !model.getVERURL().trim().isEmpty()) {
            hql += "AND VERURL LIKE ? ";
            params.add("%" + model.getVERURL() + "%");
        }
        hql += " ORDER BY " + orderBy + " " + orderDir;

        log.trace(Sanitizer.logForgingStr(hql));

        switch (params.size()) {
        case 0:
            return this.pagedQuery(hql, pageNo, pageSize);

        case 1:
            return this.pagedQuery(hql, pageNo, pageSize, params.get(0));

        case 2:
            return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1));
            
        case 3:
            return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2));
            
        case 4:
            return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3));
            
        case 5:
            return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3), params.get(4));
            
        case 6:
            return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3), params.get(4), params.get(5));
            
        case 7:
            return this.pagedQuery(hql, pageNo, pageSize, params.get(0), params.get(1), params.get(2), params.get(3), params.get(4), params.get(5), params.get(6));
     
        default:
            return this.pagedQuery(hql, pageNo, pageSize);
            
        }
    }

}
