package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMLOGIN;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import com.netbank.domain.orm.core.Page;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

@Slf4j
@Repository
@Transactional

public class AdmLogInDao extends LegacyJpaRepository<ADMLOGIN, Integer> {
	protected Logger logger = Logger.getLogger(getClass());

	public Page findPageData(int pageNo, int pageSize, String orderBy, String orderDir, String startDate,
			String endDate, String userId, String loginType) {

		List<Object> params = new ArrayList<Object>();
		String hql = "FROM ADMLOGIN WHERE LOGINOUT=? AND ( LOGINTIME >= ? AND LOGINTIME <= ? ) ";

		params.add("0");			// 使用者己登入 0：已登入，1：已登出
		params.add(startDate);
		params.add(endDate);

		if (userId != null && !userId.trim().isEmpty()) {
			hql += " AND ADUSERID LIKE ? ";
			params.add("%"+userId+"%");
		}
		if (loginType != null && !loginType.trim().isEmpty()) {
			hql += " AND LOGINTYPE LIKE ? ";
			params.add("%"+loginType+"%");
		}
		if ( !orderBy.isEmpty() ) {
			hql += " ORDER BY " + orderBy + " " + orderDir;
		}

		log.trace(Sanitizer.logForgingStr(hql));
		
		return this.pagedQuery(hql, pageNo, pageSize, params);
	}

	/**
	 * 查詢目前登入人數，依 LoginType 做 Group
	 * @return
	 */
	public Rows getConCurrentLogins() {
		Rows result = new Rows();
		Calendar date = Calendar.getInstance();
		long t= date.getTimeInMillis();
		Date befpre30Mins=new Date(t - (30 * 60*1000));

		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		String sbefpre30Mins = format.format(befpre30Mins);

		Query query = getSession().createSQLQuery("SELECT LOGINTYPE, count(*) FROM ADMLOGIN WHERE LOGINOUT=? AND LOGINTIME>=? GROUP BY LOGINTYPE");
		query.setParameter(0, "0");				// 使用者己登入 0：已登入，1：已登出
		query.setParameter(1, sbefpre30Mins);	// 30 分鐘內登入

		List qresult = query.getResultList();
		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[])it.next();
			Map<String, String> rm = new HashMap<String, String>();
			rm.put(o[0].toString(), o[1].toString());
			
			Row r = new Row(rm);
			result.addRow(r);
		}
		return result;
	}
}
