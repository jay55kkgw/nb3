package fstop.orm.dao;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNFXRECORD;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TxnFxRecordDao extends LegacyJpaRepository<TXNFXRECORD, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
    /**
     * 
     * @param fxtxdate
     * @param sTAN
     * @return
     */
    public List<TXNFXRECORD> getTXNFXRECORDs(String fxMsgContent) {
      String hql = "FROM TXNFXRECORD WHERE FXMSGCONTENT='"+ fxMsgContent + "' ";
		return find(hql);
	}	
    
    /**
     * 取得同ID/交易代號/轉出/轉入(帳號+幣別)及金額的資料
     * @param adopId
     * @param fxUserId
     * @param fxTxDate
     * @param fxWDAC
     * @param fxSVAC
     * @param fxWDCurr
     * @param fxSVCurr
     * @param fxAmt
     * @return
     */
    public List<TXNFXRECORD> getDuplicateTxn(String adopId, String fxUserId, String fxTxDate, String fxWDAC, String fxSVAC, String fxWDCurr, String fxSVCurr, String fxAmt) {
    	String hql = "FROM TXNFXRECORD WHERE ADOPID=? AND FXUSERID=? AND FXTXDATE=? AND FXWDAC=? AND FXSVAC=? AND FXWDCURR=? AND FXSVCURR=? AND ( FXWDAMT=? OR FXSVAMT=?) AND FXEXCODE=? ";
    	
    	List<Object> params = new ArrayList<Object>();
		params.add(adopId);	
		params.add(fxUserId);
		params.add(fxTxDate);
		params.add(fxWDAC);
		params.add(fxSVAC);
		params.add(fxWDCurr);
		params.add(fxSVCurr);
		params.add(fxAmt);
		params.add(fxAmt);
		params.add("");
		
		log.trace(Sanitizer.logForgingStr(hql));

		return this.find(hql, params);
    }
    
    public List<TXNFXRECORD> findByDate(String Date){
    	String hql = "FROM TXNFXRECORD WHERE FXTXDATE = ?";
    	
    	List<Object> params = new ArrayList<Object>();
    	params.add(Date);
    	
    	return this.find(hql, params);
    }
    
    /**
     * 
     * 用TXNFXSCHPAYDATA的FXSCHNO去查詢
     * 
     * */
    public LinkedList<Object[]> findFromSchPay(String FXSCHNO,String FXSCHTXDATE,String FXUSERID, String Date) throws Exception{
    	String hql = "\r\n" + 
    			"FROM TXNFXSCHPAY pay \r\n" + 
    			"INNER JOIN TXNFXSCHPAYDATA paydata ON pay.FXUSERID = paydata.FXSCHPAYDATAIDENTITY.FXUSERID \r\n" + 
    			"AND paydata.FXSCHPAYDATAIDENTITY.FXSCHNO = pay.FXSCHNO \r\n" + 
    			"INNER JOIN TXNFXRECORD rec ON rec.FXUSERID = pay.FXUSERID \r\n" + 
    			"AND rec.FXSCHID = pay.FXSCHID \r\n" + 
    			"WHERE paydata.FXSCHPAYDATAIDENTITY.FXSCHNO = ? "
    			+ "AND paydata.FXSCHPAYDATAIDENTITY.FXUSERID = ? "
    			+ "AND paydata.FXSCHPAYDATAIDENTITY.FXSCHTXDATE = ? "
    			+ "AND rec.FXTXDATE = ?";
    	
    	List<Object> params = new ArrayList<Object>();
    	params.add(FXSCHNO);
    	params.add(FXUSERID);
    	params.add(FXSCHTXDATE);
    	params.add(Date);
    	
    	return  (LinkedList<Object[]>) this.find(hql, params);
    	
    }
    
    public List<TXNFXRECORD> findFxAllowTerminate(String FXUSERID, String today,String[] pendingArr){
    	StringBuffer sqlBuffer = new StringBuffer();
    	sqlBuffer.append("FROM TXNFXRECORD WHERE FXTXSTATUS='1' AND FXTXDATE=? ");
    	
    	List<Object> params = new ArrayList<Object>();
    	params.add(today);
    	
    	if(FXUSERID != null && !FXUSERID.trim().isEmpty()) {
    		sqlBuffer.append("AND FXUSERID=? ");
    		params.add(FXUSERID);
    	}
    	
    	sqlBuffer.append("AND FXEXCODE IN(");
    	
    	for(int i=0 ; i<pendingArr.length ; i++) {
    		if(i == pendingArr.length-1) {
    			sqlBuffer.append("'"+pendingArr[i]+"')");
    		}else {
    			sqlBuffer.append("'"+pendingArr[i]+"',");
    		}
    	}
    	List<TXNFXRECORD> list = this.find(sqlBuffer.toString(), params);
    	return list;
    }
}
