package fstop.orm.dao;

import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import javax.persistence.Query;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.rest.web.back.model.B706Result;
import com.netbank.rest.web.back.model.B708Result;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMDAYHHREPORT;
import fstop.util.PropertyUtils;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * AdmDayHHReportDao class.
 * </p>
 *
 * @author chiensj
 * @version V1.0
 */
@Slf4j
@Repository
@Transactional
public class AdmDayHHReportDao extends LegacyJpaRepository<ADMDAYHHREPORT, String> {

	/**
	 * 取某一天的每一小時交易統計（分查詢類及非查詢類）
	 * @param cmYYYYMMDD
	 * @return
	 * @throws Exception
	 */
 	public List<List<B708Result>> findByHourData(String cmYYYYMMDD) throws Exception {
		try {
			PropertyUtils utility = new PropertyUtils();
			String sql = utility.getResourceAsString("reports/B708_1.sql");

			Query query = getSession().createSQLQuery(sql);

			query.setParameter(0, cmYYYYMMDD);		// 查詢日期
			query.setParameter(1, "GPE");			// GPE：查詢類
			
			// 因為 TXNLOG 記了一些沒有分類的交易, 所要用正向表列
			query.setParameter(2, cmYYYYMMDD);		// 
			query.setParameter(3, "GPA");
			query.setParameter(4, "GPB");
			query.setParameter(5, "GPC");
			query.setParameter(6, "GPD");
		
			log.debug("[SQL:]" + sql);

			List qresult = query.getResultList();

			/* qresult 格式如下，QRYTYPE 0：查詢 1：非查詢
				CMHH	QRYTYPE	QUERYCNT
				01		0		3
				01		1		12
				02		1		1
				03		0		10
				04		1		3
			*/

			Iterator it = qresult.iterator();
			Map<Long, B708Result> results = new TreeMap<Long, B708Result>();
			while (it.hasNext()) {
				Object[] o = (Object[]) it.next();
				long hour = Long.parseLong(o[0].toString());
				int qryType = Integer.parseInt(o[1].toString());
				long count = Long.parseLong(o[2].toString());

				B708Result result = null;
				if ( results.containsKey(hour) ) {
					result = (B708Result)results.get(hour);
				} else {
					result = new B708Result();
					results.put(hour, result);
				}
				result.setHour(hour);
				if ( qryType == 0 ) {
					result.setQueryCnt(count);
				} else {
					result.setNonQueryCnt(count);
				}
			}
			ArrayList<B708Result> newResult = new ArrayList<B708Result>(results.values());
			List<B708Result> newList = (List<B708Result>)(Object)Sanitizer.validList(newResult);
			newResult.clear();
			newResult.addAll(newList);
			
			List<List<B708Result>> newResults = new ArrayList<List<B708Result>>();
			newResults.add(newResult);
			
			return newResults;
		} catch ( Exception e ) {
			throw e;
		}
	}

 	/**
 	 * 取得峰日/峰月/峰時之總交易數
 	 * @param cmYYYYMMDD
 	 * @return
 	 */
 	private List<Map<String, Long>> findTop3Data(String cmYYYYMMDD) {
 		try {
			PropertyUtils utility = new PropertyUtils();
			String sql = utility.getResourceAsString("reports/B708_3.sql");

			Query query = getSession().createSQLQuery(sql);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date currentDate = sdf.parse(cmYYYYMMDD);

			Calendar c = Calendar.getInstance();
			c.setTime(currentDate);
			c.add(Calendar.YEAR, -1);
			Date lastYearDate = c.getTime();
			String cmYYYYMMDDLast = sdf.format(lastYearDate);

			query.setParameter(0, cmYYYYMMDDLast);		// 查詢起日（1年前）
			query.setParameter(1, cmYYYYMMDD);			// 查詢迄日
			query.setParameter(2, "GPA");
			query.setParameter(3, "GPB");
			query.setParameter(4, "GPC");
			query.setParameter(5, "GPD");
			query.setParameter(6, "GPE");
			
			query.setParameter(7, cmYYYYMMDDLast.substring(0, 6));
			query.setParameter(8, cmYYYYMMDD.substring(0, 6));
			query.setParameter(9, "GPA");
			query.setParameter(10, "GPB");
			query.setParameter(11, "GPC");
			query.setParameter(12, "GPD");
			query.setParameter(13, "GPE");
			
			query.setParameter(14, cmYYYYMMDDLast);
			query.setParameter(15, cmYYYYMMDD);
			query.setParameter(16, "GPA");
			query.setParameter(17, "GPB");
			query.setParameter(18, "GPC");
			query.setParameter(19, "GPD");
			query.setParameter(20, "GPE");
			
			log.debug("[SQL:]" + sql);

			List qresult = query.getResultList();

			/* qresult 格式如下，QTYPE 0:日, 1:月, 2:時
			 * QTYPE	DD	TSUM
				0	20200107  	5653
				0	20191219  	992
				0	20191213  	809
				1	201911    	7670
				1	201908    	299
				2	2020010713	4353
				2	2020010712	1202
				2	2019121910	770
			*/

			Iterator it = qresult.iterator();
			Map<String, Long> peakDate = new LinkedHashMap<String, Long>();
			Map<String, Long> peakMonth = new LinkedHashMap<String, Long>();
			Map<String, Long> peakDayHH = new LinkedHashMap<String, Long>();
			
			while (it.hasNext()) {
				Object[] o = (Object[]) it.next();
				char qType = o[0].toString().charAt(0);
				String dd = o[1].toString();
				long tsum = Long.parseLong(o[2].toString());
				
				if ( qType == '0' ) {
					peakDate.put(dd, tsum);
				} else if ( qType == '1' ) {
					peakMonth.put(dd, tsum);
				} else {
					peakDayHH.put(dd, tsum);
				}
			}
			
			List<Map<String, Long>> peakData = new ArrayList<Map<String, Long>>();
			peakData.add(peakDate);
			peakData.add(peakMonth);
			peakData.add(peakDayHH);
			
			return peakData;
 		} catch ( Exception e ) {
			log.error("findTop3Data Error", e);
			return null;
		}
 	}
	/**
	 * 取一年內月/日/時 峰時筆數交易統計（分查詢類及非查詢類）
	 * @param cmYYYYMMDD
	 * @return
	 * @throws Exception
	 */
	public List<List<B708Result>> findByTop3Data(String cmYYYYMMDD) throws Exception {
		try {
			// 取得峰日總量, List 內容依序是
			// 0: 峰日(最多3筆)
			// 1: 峰月(最多3筆)
			// 2: 峰時(最多3筆)
			List<Map<String, Long>> peakData = findTop3Data(cmYYYYMMDD);
			Map<String, Long> peakDate = peakData.get(0);
			Map<String, Long> peakMonth = peakData.get(1);
			Map<String, Long> peakDayHH = peakData.get(2);
			
			List<B708Result> resultDate = new ArrayList<B708Result>();
			List<B708Result> resultMonth = new ArrayList<B708Result>();
			List<B708Result> resultDayHH = new ArrayList<B708Result>();
			
			for (Map.Entry<String, Long> entry : peakDate.entrySet() ) {
				B708Result r = new B708Result();
				r.setItem(Long.parseLong(entry.getKey().trim()));
				r.setTotalCnt(entry.getValue().longValue());
				resultDate.add(r);
			}
			
			for (Map.Entry<String, Long> entry : peakMonth.entrySet() ) {
				B708Result r = new B708Result();
				r.setItem(Long.parseLong(entry.getKey().trim()));
				r.setTotalCnt(entry.getValue().longValue());
				resultMonth.add(r);
			}
			
			for (Map.Entry<String, Long> entry : peakDayHH.entrySet() ) {
				B708Result r = new B708Result();
				r.setItem(Long.parseLong(entry.getKey().trim()));
				r.setTotalCnt(entry.getValue().longValue());
				resultDayHH.add(r);
			}
			
			// 以下的 SQL 只查峰日/峰月/峰時的前三筆查詢量, 總量-查詢量=非查詢量
			PropertyUtils utility = new PropertyUtils();
			String sql = utility.getResourceAsString("reports/B708_2.sql");

			Query query = getSession().createSQLQuery(sql);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date currentDate = sdf.parse(cmYYYYMMDD);

			Calendar c = Calendar.getInstance();
			c.setTime(currentDate);
			c.add(Calendar.YEAR, -1);
			Date lastYearDate = c.getTime();
			String cmYYYYMMDDLast = sdf.format(lastYearDate);
			String cmYYYYMM = cmYYYYMMDD.substring(0, 6);
			String cmYYYYMMLast = cmYYYYMMDDLast.substring(0, 6);
			
			String[] peakDA = peakDate.keySet().toArray(new String[peakDate.keySet().size()]);		// 峰日 Array
			String[] peakMA = peakMonth.keySet().toArray(new String[peakMonth.keySet().size()]);	// 峰月 Array
			String[] peakHA = peakDayHH.keySet().toArray(new String[peakDayHH.keySet().size()]);	// 峰時 Array
			
			// SQL 均保留三個 in 的條件, 用來查峰日/峰月/峰時
			query.setParameter(0, cmYYYYMMDDLast);		// 查詢起日（1年前）
			query.setParameter(1, cmYYYYMMDD);			// 查詢迄日
			query.setParameter(2, "GPE");				// GPE：查詢類
			query.setParameter(3, peakDA[0]);
			query.setParameter(4, peakDA.length>1 ? peakDA[1] : peakDA[0]);		// 讓 Where in 補足三個
			query.setParameter(5, peakDA.length>2 ? peakDA[2] : peakDA[0]);		// 讓 Where in 補足三個
			query.setParameter(6, cmYYYYMMLast.subSequence(0, 6));				// 查詢起月年前）
			query.setParameter(7, cmYYYYMM.substring(0, 6));					// 查詢迄月
			query.setParameter(8, "GPE");				// GPE：查詢類
			query.setParameter(9, peakMA[0]);
			query.setParameter(10, peakMA.length>1 ? peakMA[1] : peakMA[0]);
			query.setParameter(11, peakMA.length>2 ? peakMA[2] : peakMA[0]);
			query.setParameter(12, cmYYYYMMDDLast);		// 查詢起日（1年前）
			query.setParameter(13, cmYYYYMMDD);			// 查詢迄日
			query.setParameter(14, "GPE");				// GPE：查詢類
			query.setParameter(15, peakHA[0]);
			query.setParameter(16, peakHA.length>1 ? peakHA[1] : peakHA[0]);
			query.setParameter(17, peakHA.length>2 ? peakHA[2] : peakHA[0]);

			log.debug("[SQL:]" + sql);

			List qresult = query.getResultList();

			/* qresult 格式如下，QTYPE 0：峰日 1：峰月 2：峰時
			 * 	DD			QTYPE	QUERYCNT	
				20191219  	0		193
				20191213  	0		218
				20200107  	0		1888
				201911    	1		1406
				2019121910	2		172
				2020010713	2		869
				2020010712	2		1018
			*/

			Iterator it = qresult.iterator();
		
			while (it.hasNext()) {
				Object[] o = (Object[]) it.next();
				long dd = Long.parseLong(o[0].toString().trim());
				int qryType = Integer.parseInt(o[1].toString());
				long queryCnt = Long.parseLong(o[2].toString());
				
				List<B708Result> pr;
				switch ( qryType ) {
					case 0:
						pr = resultDate;
						break;
					case 1:
						pr = resultMonth;
						break;
					default:
						pr = resultDayHH;
				}
				for ( int i=0; i<pr.size(); i++ ) {
					if ( pr.get(i).getItem() == dd ) {
						long total = pr.get(i).getTotalCnt();
						pr.get(i).setQueryCnt(queryCnt);
						pr.get(i).setNonQueryCnt(total-queryCnt);
					}
				}
			}
			List<List<B708Result>> newResult = new ArrayList<List<B708Result>>();
			newResult.add(resultDate);
			newResult.add(resultMonth);
			newResult.add(resultDayHH);
			
			return newResult;
		} catch ( Exception e ) {
			throw e;
		}
	}
}
