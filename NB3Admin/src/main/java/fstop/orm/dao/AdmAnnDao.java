package fstop.orm.dao;

import javax.transaction.Transactional;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import org.springframework.stereotype.Repository;
import fstop.orm.po.ADMANN;

/**
 * <p>個人訊息公告內容刊登 DAO</p>
 * 
 * @author 簡哥
 * @since 2019/8/16
 */
@Repository
@Transactional
public class AdmAnnDao extends LegacyJpaRepository<ADMANN, String> {
}