package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.util.StrUtils;

import fstop.orm.po.IDGATEHISTORY;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class IdGateHistoryDao extends LegacyJpaRepository<IDGATEHISTORY,String>{

	public Page getVerifyHis(IDGATEHISTORY idgatehis, int pageNo, int pageSize)
	{
		List<Object> params = new ArrayList<Object>();
		StringBuilder hql=new StringBuilder();

		hql.append("FROM IDGATEHISTORY WHERE 1=1 ");
		if(StrUtils.isNotEmpty(idgatehis.getCUSIDN()))
		{
			hql.append("AND CUSIDN  = ? "); 
			params.add(idgatehis.getCUSIDN());
		}

		if(StrUtils.isNotEmpty(idgatehis.getIDGATEID()))
		{
			hql.append("AND IDGATEID  = ? "); 
			params.add(idgatehis.getIDGATEID());
		}
		
		if(StrUtils.isNotEmpty(idgatehis.getSDate()))
		{
			hql.append("AND TRANDATE  >= ? "); 
			params.add(idgatehis.getSDate());
		}
		
		if(StrUtils.isNotEmpty(idgatehis.getEDate()))
		{
			hql.append("AND TRANDATE <= ? "); 
			params.add(idgatehis.getEDate());
		}
		
		hql.append("ORDER BY TRANDATE,TRANTIME ");
		
		return this.pagedQuery(hql.toString(), pageNo, pageSize, params);
	}
	
	public Page getVerifyHis_V2(IDGATEHISTORY idgatehis, int pageNo, int pageSize)
	{
		List<Object> params = new ArrayList<Object>();
		StringBuilder hql=new StringBuilder();

		hql.append(" SELECT NEW fstop.orm.po.IDGATEHISTORY(i.ID,i.CUSIDN,i.IDGATEID,i.DEVICEBRAND,i.BINDSTATUS,i.BINDDATE,i.BINDTIME,i.ADOPID,i.STATUS,i.TRANDATE,i.TRANTIME,i.LOGINTYPE,COALESCE(nb.ADOPNAME,mb.ADOPNAME,'') as ADOPNAME) FROM IDGATEHISTORY i  "
				+ " LEFT JOIN MB3SYSOP mb ON i.ADOPID = mb.ADOPID "
				+ " LEFT JOIN NB3SYSOP nb ON i.ADOPID = nb.ADOPID " 
				+ " WHERE 1=1 ");
		if(StrUtils.isNotEmpty(idgatehis.getCUSIDN()))
		{
			hql.append("AND i.CUSIDN  = ? "); 
			params.add(idgatehis.getCUSIDN());
		}

		if(StrUtils.isNotEmpty(idgatehis.getIDGATEID()))
		{
			hql.append("AND i.IDGATEID  = ? "); 
			params.add(idgatehis.getIDGATEID());
		}
		
		if(StrUtils.isNotEmpty(idgatehis.getSDate()))
		{
			hql.append("AND i.TRANDATE  >= ? "); 
			params.add(idgatehis.getSDate());
		}
		
		if(StrUtils.isNotEmpty(idgatehis.getEDate()))
		{
			hql.append("AND i.TRANDATE <= ? "); 
			params.add(idgatehis.getEDate());
		}
		
		hql.append(" ORDER BY  i.TRANDATE,i.TRANTIME ");
		
		return this.pagedQuery(hql.toString(), pageNo, pageSize, params);
	}
}
