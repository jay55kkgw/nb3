package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import org.springframework.stereotype.Repository;

import fstop.orm.po.ADMOPLOG;
import fstop.util.Sanitizer;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Slf4j
@Repository
public class AdmOpLogDao extends LegacyJpaRepository<ADMOPLOG, String> {

	/**
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @param orderBy
	 * @param orderDir
	 * @param startDate
	 * @param startTime
	 * @param endDate
	 * @param endTime
	 * @param loginUserId
	 * @param adopid
	 * @return
	 */
    public Page findPageDataByLike(int pageNo, int pageSize, String orderBy, String orderDir, String startDate, String startTime, String endDate, String endTime, String loginUserId, String adopid) {
	
        String hql = "FROM ADMOPLOG WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();

		if((!startDate.isEmpty() && !startTime.isEmpty()) && (!endDate.isEmpty() && !endTime.isEmpty())) {
			hql += "AND ( CONCAT(ADTXDATE,ADTXTIME)>=? AND CONCAT(ADTXDATE,ADTXTIME)<=? ) ";
			params.add(startDate+startTime);
			params.add(endDate+endTime);
		}  
		if(!startDate.isEmpty() && endTime.isEmpty()) {
			hql += "AND CONCAT(ADTXDATE,ADTXTIME)>=? ";
			params.add(startDate+startTime);
		}
		if(startTime.isEmpty() && !endTime.isEmpty()) {
			hql += "AND CONCAT(ADTXDATE,ADTXTIME)<=? ";
			params.add(endDate+endTime);
		} 
		if(loginUserId != null && !loginUserId.trim().isEmpty()) {
			hql += "AND ADUSERID like ? ";
			params.add("%"+loginUserId+"%");
        }
        if(adopid != null && !adopid.trim().isEmpty()) {
			hql += "AND ADOPID like ? ";
			params.add("%"+adopid+"%");
		}
		hql += " ORDER BY "+ orderBy+" "+orderDir;

		log.debug(Sanitizer.logForgingStr(hql));

		return this.pagedQuery(hql, pageNo, pageSize, params);
	}
}