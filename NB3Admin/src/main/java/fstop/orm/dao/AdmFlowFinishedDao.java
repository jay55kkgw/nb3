package fstop.orm.dao;

import javax.transaction.Transactional;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import org.springframework.stereotype.Repository;

import fstop.orm.po.ADMFLOWFINISHED;

/**
 * <p>流程完成 DAO</p>
 *
 * @author chiensj
 * @version V1.0
 */
@Repository
@Transactional
public class AdmFlowFinishedDao extends LegacyJpaRepository<ADMFLOWFINISHED, String> {
}
